module Organizations

  def apply_organization_permissions(user)
    ############ Organizatons
    can [:read, :write], ::Organization do |org|
      org.users.where(id: user.id).exists?
    end
  end
end