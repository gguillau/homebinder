require "cancancan"
class Ability
  include CanCan::Ability
  include Common
  include Admin
  include Homeowner
  include Partners
  include Organizations
  include Agent
  
  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user 
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. 
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities
    if not user.nil?
      # basic permissions based on role
      if user.is_admin?
        apply_admin_permissions(user)
      elsif user.is_agent?
        apply_agent_permissions(user)
      elsif user.belongs_to_partner?
        apply_partner_permissions(user)
      elsif user.is_homeowner?
        apply_homeowner_permissions(user)
      end
      # basic permissions if user is also member of org
      if user.belongs_to_organization?
        apply_organization_permissions(user)
      end
    end
  end
end
