module Agent

  def apply_agent_permissions(user)
    ############ User
    can :read, User do |u|
      true
    end

    can :write, User do |u|
      if u.role === "homeowner"
        true
      else
        user.id == u.id
      end
    end

    can :destroy, User do |u|
      user.id == u.id
    end
    ############# TemplateAppliance
    can :read, Template::Appliance do |template|
      true
    end
    
    ############# TemplateMaintenance
    can :read, Template::Maintenance do |template|
      true
    end
    
    apply_common_permissions(user)
  end
end

