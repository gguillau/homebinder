module Admin

  def apply_admin_permissions(user)
    if user.role === 'admin'
      can :manage, :all
    else
      can [:read, :write], :all
    end
  end
  
end