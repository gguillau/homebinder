module Partners

  def apply_partner_permissions(user)
    ############ User
    can :read, User do |u|
      can_partner_read_user?(u, user)
    end
    can :write, User do |u|
      can_partner_write_user?(u, user)
    end
    can :destroy, User do |u|
      # user being removed is not current user and the current user is an admin
      PartnerUser.where(:user_id => user.id, :role => "admin").exists? &&
      user != u.id && user.partners.first.users.where(id: u.id).exists?
    end
    ############# RepairPricer
    can :access_repair_pricer, Binder do |binder|
      UserBinder.where(:user_id => user.id, :binder_id => binder.id, :access_repair_pricer => true).exists?
    end
    ############# Partners
    can :create, ::Partner do |partner|
      user.role === "inspector" || user.role === "broker"
    end
    can :read, ::Partner do |partner|
      partner.users.where(id: user.id).exists?
    end
    can :write, ::Partner do |partner|
      partner.users.where(id: user.id).exists?
    end
    can :destroy, ::Partner do |partner|
      PartnerUser.where(:partner_id => partner.id, :user_id => user.id, :role => :admin).exists?
    end
    ############ PartnerUsers
    can [:read, :write], PartnerUser do |partner_user|
      ::PartnerUser.where(:partner_id => user.partner_ids, id: partner_user.id).exists?
    end
    ############ Accounts
    can :read, Account do |account|
      account.partners.where(:id => user.partners.pluck(:id)).exists? || account.manager_id == user.partners.first.id
    end
    ############# Binders
    can :write, UserBinder do |user_binder|
      # any member of a partner can write the binders of any other member
      binder_owned_by_partner?(user, user_binder.binder)
    end
    can :destroy, UserBinder do |user_binder|
      # any member of a partner can destroy the binders of any other member
      binder_owned_by_partner?(user, user_binder.binder)
    end
    ############# Binders
    can :read, Binder do |binder|
      # any member of a partner can read the binders of any other member
      binder_linked_to_user?(user, binder)
    end
    can :write, Binder do |binder|
      # any member of a partner can write the binders of any other member
      binder_owned_by_partner?(user, binder)
    end
    can :destroy, Binder do |binder|
      # any member of a partner can destroy the binders of any other member
      binder_linked_to_partner?(user, binder)
    end
    ############ Transfers
    can :transfer, Binder do |binder|
      # any member of a partner can transfer the binders of any other member
      can_transfer?(user, binder)
    end
    ############ Shares
    can :share, Binder do |binder|
      # any member of a partner can share the binders of any other member
      binder_owned_by_partner?(user, binder)
    end
    ########### Brandings
    can :read, Binder::BinderBranding do |item|
      can_partner_read_item?(user, item)
    end
    can :write, Binder::BinderBranding do |item|
      can_partner_write_item?(user, item)
    end
    can :destroy, Binder::BinderBranding do |item|
      can_partner_destroy_item?(user, item)
    end
    ########### Structures
    can :read, Binder::Structure do |item|
      can_partner_read_item?(user, item)
    end
    can :write, Binder::Structure do |item|
      can_partner_write_item?(user, item)
    end
    can :destroy, Binder::Structure do |item|
      can_partner_destroy_item?(user, item)
    end
    ############ Areas
    can :read, Binder::Area do |item|
      can_partner_read_item?(user, item)
    end
    can :write, Binder::Area do |item|
      can_partner_write_item?(user, item)
    end
    can :destroy, Binder::Area do |item|
      can_partner_destroy_item?(user, item)
    end
    ############ Appliances
    can :read, Binder::Appliance do |item|
      can_partner_read_item?(user, item)
    end
    can :write, Binder::Appliance do |item|
      can_partner_write_item?(user, item)
    end
    can :destroy, Binder::Appliance do |item|
      can_partner_destroy_item?(user, item)
    end
    ############ ApplianceRecalls
    can :read, Binder::Appliance::ApplianceRecall do |item|
      item = item.appliance
      can_partner_read_item?(user, item)
    end
    can :write, Binder::Appliance::ApplianceRecall do |item|
      item = item.appliance
      can_partner_write_item?(user, item)
    end
    can :destroy, Binder::Appliance::ApplianceRecall do |item|
      item = item.appliance
      can_partner_destroy_item?(user, item)
    end
    ############# BinderContractors
    can :read, Binder::BinderContractor do |item|
      can_partner_read_item?(user, item)
    end
    can :write, Binder::BinderContractor do |item|
      can_partner_write_item?(user, item)
    end
    can :destroy, Binder::BinderContractor do |item|
      can_partner_destroy_item?(user, item)
    end
    ############# Finishes
    can :read, Binder::Finish do |item|
      can_partner_read_item?(user, item)
    end
    can :write, Binder::Finish do |item|
      can_partner_write_item?(user, item)
    end
    can :destroy, Binder::Finish do |item|
      can_partner_destroy_item?(user, item)
    end
    ############# Paints
    can :read, Binder::Paint do |item|
      can_partner_read_item?(user, item)
    end
    can :write, Binder::Paint do |item|
      can_partner_write_item?(user, item)
    end
    can :destroy, Binder::Paint do |item|
      can_partner_destroy_item?(user, item)
    end
    ############# Permits
    can :read, Binder::Permit do |item|
      can_partner_read_item?(user, item)
    end
    can :write, Binder::Permit do |item|
      can_partner_write_item?(user, item)
    end
    can :destroy, Binder::Permit do |item|
      can_partner_destroy_item?(user, item)
    end
    ############# Projects
    can :read, Binder::Project do |item|
      can_partner_read_item?(user, item)
    end
    can :write, Binder::Project do |item|
      can_partner_write_item?(user, item)
    end
    can :destroy, Binder::Project do |item|
      can_partner_destroy_item?(user, item)
    end
    ############# MaintenanceItems
    can :read, Binder::MaintenanceItem do |item|
      can_partner_read_item?(user, item)
    end
    can :write, Binder::MaintenanceItem do |item|
      can_partner_write_item?(user, item)
    end
    can :destroy, Binder::MaintenanceItem do |item|
      can_partner_destroy_item?(user, item)
    end
    ############# MaintenanceEvents
    can :read, Binder::MaintenanceItem::MaintenanceEvent do |item|
      mi = Binder::MaintenanceItem.find(item.maintenance_item_id)
      can_partner_read_item?(user, mi)
    end
    can :write, Binder::MaintenanceItem::MaintenanceEvent do |item|
      mi = Binder::MaintenanceItem.find(item.maintenance_item_id)
      can_partner_write_item?(user, mi)
    end
    can :destroy, Binder::MaintenanceItem::MaintenanceEvent do |item|
      mi = Binder::MaintenanceItem.find(item.maintenance_item_id)
      can_partner_destroy_item?(user, mi)
    end
    ############# InventoryItems
    can :read, Binder::InventoryItem do |item|
      can_partner_read_item?(user, item)
    end
    can :write, Binder::InventoryItem do |item|
      can_partner_write_item?(user, item)
    end
    can :destroy, Binder::InventoryItem do |item|
      can_partner_destroy_item?(user, item)
    end
    ############# Receipts
    can :read, Binder::Receipt do |item|
      can_partner_read_item?(user, item)
    end
    can :write, Binder::Receipt do |item|
      can_partner_write_item?(user, item)
    end
    can :destroy, Binder::Receipt do |item|
      can_partner_destroy_item?(user, item)
    end
    ############# Documents
    can :read, Binder::Document do |item|
      can_partner_read_item?(user, item)
    end
    can :write, Binder::Document do |item|
      can_partner_write_item?(user, item)
    end
    can :destroy, Binder::Document do |item|
      can_partner_destroy_item?(user, item)
    end
    ############# Images
    can :read, Binder::Image do |item|
      can_partner_read_item?(user, item)
    end
    can :write, Binder::Image do |item|
      can_partner_write_item?(user, item)
    end
    can :destroy, Binder::Image do |item|
      can_partner_destroy_item?(user, item)
    end
    ############# Shares
    can :read, Binder::Share do |item|
      can_partner_read_item?(user, item)
    end
    can :write, Binder::Share do |item|
      can_partner_write_item?(user, item)
    end
    can :destroy, Binder::Share do |item|
      can_partner_destroy_item?(user, item)
    end
    ############# Transfers
    can :read, Binder::Transfer do |item|
      can_partner_read_item?(user, item)
    end
    can :write, Binder::Transfer do |item|
      can_partner_write_item?(user, item)
    end
    can :destroy, Binder::Transfer do |item|
      can_partner_destroy_item?(user, item)
    end
    ############# SellerReports
    can :read, SellerReport do |item|
      can_partner_read_item?(user, item)
    end
    can :write, SellerReport do |item|
      can_partner_write_item?(user, item)
    end
    can :destroy, SellerReport do |item|
      can_partner_destroy_item?(user, item)
    end
    ############# Configuration
    can :read, ::Partner::Configuration do |config|
      partner = ::Partner.joins(:partner_configuration).where(partner_configurations: { id: config.id }).first
      can? :read, partner
    end
    can :write, ::Partner::Configuration do |config|
      partner = ::Partner.joins(:partner_configuration).where(partner_configurations: { id: config.id }).first
      can? :write, partner
    end
    can :destroy, ::Partner::Configuration do |config|
      partner = ::Partner.joins(:partner_configuration).where(partner_configurations: { id: config.id }).first
      can? :write, partner
    end
    ############# BinderTemplates
    can :read, BinderTemplate do |template|
      partner = ::Partner.joins(:partner_configuration).where(partner_configurations: { id: template.partner_configuration_id }).first
      can? :read, partner
    end
    can :write, BinderTemplate do |template|
      partner = ::Partner.joins(:partner_configuration).where(partner_configurations: { id: template.partner_configuration_id }).first
      can? :write, partner
    end
    can :destroy, BinderTemplate do |template|
      partner = ::Partner.joins(:partner_configuration).where(partner_configurations: { id: template.partner_configuration_id }).first
      partner.users.where(id: user.id).exists?
    end
    ############# TemplateAppliances
    can :read, Template::Appliance do |template|
      can_read_binder_template?(user, template)
    end
    can :write, Template::Appliance do |template|
      can_write_binder_template?(user, template)
    end
    can :destroy, Template::Appliance do |template|
      can_destroy_template_item?(user, template)
    end
    ############# TemplateContractors
    can :read, Template::Contractor do |template|
      can_read_binder_template?(user, template)
    end
    can :write, Template::Contractor do |template|
      can_write_binder_template?(user, template)
    end
    can :destroy, Template::Contractor do |template|
      can_destroy_template_item?(user, template)
    end
    ############# TemplateMaintenanceItems
    can :read, Template::Maintenance do |template|
      can_read_binder_template?(user, template)
    end
    can :write, Template::Maintenance do |template|
      can_write_binder_template?(user, template)
    end
    can :destroy, Template::Maintenance do |template|
      can_destroy_template_item?(user, template)
    end
    ############# TemplateDocuments
    can :read, Template::Document do |template|
      can_read_binder_template?(user, template)
    end
    can :write, Template::Document do |template|
      can_write_binder_template?(user, template)
    end
    can :destroy, Template::Document do |template|
      can_destroy_template_item?(user, template)
    end
    ############# TemplateDocuments
    can :read, WarrantyConfiguration do |config|
      user.partners.where(:id => config.partner_id).exists?
    end
    can :write, WarrantyConfiguration do |config|
      user.partners.where(:id => config.partner_id).exists?
    end
    ############# Partner ISN User
    can [:read, :write], ::Partner::Configuration::IsnUser do |isn_user|
      partner = ::Partner.joins(:partner_configuration).where(partner_configurations: { id: isn_user.partner_configuration_id }).first
      isn_user.partner_configuration_id == partner.partner_configuration.id and can? :read, partner
    end
    can :destroy, ::Partner::Configuration::IsnUser do |isn_user|
      partner = ::Partner.joins(:partner_configuration).where(partner_configurations: { id: isn_user.partner_configuration_id }).first
      partner.users.where(id: user.id).exists?
    end
    ############ APR
    can :read, AnnualPropertyReview do |apr|
      if apr.reviewer_id == user.id
        true
      else
        PartnerUser.where(user_id: user.id, partner_id: apr.reviewer_company_id).exists?
      end
    end
    can :write, AnnualPropertyReview do |apr|
      if apr.status_completed?
        false
      elsif apr.reviewer_id == user.id
        true
      else
        PartnerUser.where(user_id: user.id, partner_id: apr.reviewer_company_id).exists?
      end
    end
    ############# APR Finding
    can :read, AnnualPropertyReviewFinding do |finding|
      apr = AnnualPropertyReview.find(finding.annual_property_review_id)
      can :read, apr
    end
    can :write, AnnualPropertyReviewFinding do |finding|
      apr = AnnualPropertyReview.find(finding.annual_property_review_id)
      can :write, apr
    end
    can :destroy, AnnualPropertyReviewFinding do |finding|
      apr = AnnualPropertyReview.find(finding.annual_property_review_id)
      can :write, apr
    end
    ############# APR Capital Item
    can :read, AnnualPropertyReviewCapitalItem do |item|
      apr = AnnualPropertyReview.find(item.annual_property_review_id)
      can :read, apr
    end
    can :write, AnnualPropertyReviewCapitalItem do |item|
      apr = AnnualPropertyReview.find(item.annual_property_review_id)
      can :write, apr
    end
    can :destroy, AnnualPropertyReviewCapitalItem do |item|
      apr = AnnualPropertyReview.find(item.annual_property_review_id)
      can :write, apr
    end
    ############ Dashboards
    can [:read, :write, :destroy], Dashboard do |board|
      Partner.joins(:partner_users).where(:partner_users => {:user_id => user.id}, :id => board.partner_id).where("partner_users.role = ? OR partner_users.role = ?", "admin", "member").exists?
    end
    ############ Widgets
    can [:read, :write, :destroy], Widget do |widget|
      Partner.joins(:partner_users).where(:id => widget.partner_restriction_id, :partner_users => {:user_id => user.id}).where("partner_users.role = ? OR partner_users.role = ?", "admin", "member").exists?
    end
    can :create, Widget
    ############ PartnerWidgets
    can [:read, :write, :destroy], ::PartnerWidget do |partner_widget|
      PartnerWidget.where(:partner_id => user.partner_ids).exists?
    end
    can [:read, :write, :destroy], PartnerContractor do |partner_contractor|
      partner = ::Partner.joins(:partner_users).where(:partner_users => {:user_id => user.id}).where("partner_users.role = ? OR partner_users.role = ?", "admin", "member").first
      return false if partner.nil?
      
      partner.id == partner_contractor.partner_id
    end
  end

  # check if the user or partner have any roles on the binder
  # combines binder_owned_by_partner userBinder lookup

  def binder_linked_to_user?(user, binder)
    # check if partner user has a non traditional role on the binder
    return true if UserBinder.where(:binder_id => binder.id, :user_id => user.id).where("role = ? OR role = ?", UserBinderRoles::READER, UserBinderRoles::WRITER).exists?
    # all else fails we check if the binder can be read by the partner
    return can_read_binder(user, binder)
  end

  def can_read_binder(user, binder)
    partner = ::Partner.joins(:partner_users).where(:partner_users => {:user_id => user.id}).where("partner_users.role = ? OR partner_users.role = ?", "admin", "member").first
    return false if partner.nil?

    # get the owner
    owner = binder.user_binders.where(role: UserBinderRoles::OWNER).first
    # check if user is a co_owner
    co_owner = binder.user_binders.where(:user_id => user.id).where(:role => UserBinderRoles::CO_OWNER).first
    # get the partner binder
    partner_binder = PartnerBinder.where(:partner_id => partner.id, :binder_id => binder.id).first

    # check if there is an owner
    if owner.nil? && co_owner.nil? && partner_binder.nil?
      return false
    end

    # first check on the co-owner
    if co_owner.present?
      # check if the co_owner belongs to a partner
      if co_owner.user.belongs_to_partner?
        # check if the co_owner belongs to the same partner as the user
        return false if user.partners.count < 1
        
        co_owner_partner = ::Partner.joins(:partner_users).where(:partner_users => {:user_id => co_owner.user.id}).where("partner_users.role = ? OR partner_users.role = ?", "admin", "member").first
        user_partner = ::Partner.joins(:partner_users).where(:partner_users => {:user_id => user.id}).where("partner_users.role = ? OR partner_users.role = ?", "admin", "member").first
        
        return co_owner_partner.id == user_partner.id
      end
    end

    # check if the owner belongs to a partner
    if owner.present? and owner.user.present? and owner.user.partners.exists?
      return false if user.partners.count < 1
      
      owner_partner = ::Partner.joins(:partner_users).where(:partner_users => {:user_id => owner.user.id}).where("partner_users.role = ? OR partner_users.role = ?", "admin", "member").first
      user_partner = ::Partner.joins(:partner_users).where(:partner_users => {:user_id => user.id}).where("partner_users.role = ? OR partner_users.role = ?", "admin", "member").first
      
      return false if owner_partner.nil?
      return false if user_partner.nil?
      
      # check if the owner belongs to the same partner as the user and user has same role as partner
      owner_partner.id == user_partner.id and owner.user.role === owner_partner.partner_type
    end
  end

  def binder_owned_by_partner?(user, binder)
    partner = ::Partner.joins(:partner_users).where(:partner_users => {:user_id => user.id}).where("partner_users.role = ? OR partner_users.role = ?", "admin", "member").first
    return false if partner.nil?

    # get the partner binder
    partner_binder = PartnerBinder.where(:partner_id => partner.id, :binder_id => binder.id).first

    # partners can always write to a binder as long as the binder is a partner binder
    if partner_binder.present? and binder.transfer_status(user) != "created"
      return true
    end
    return can_read_binder(user, binder)
  end

  # two use cases: user has created binder <- if so then they can transfer
  # user has already transferred binder <- then they can also transfer is there a pending transfer

  def can_transfer?(user, binder)
    if binder_linked_to_user?(user, binder)
      return true
    end
    return binder_linked_to_partner?(user, binder) && binder.pending_transfer
  end

  def binder_linked_to_partner?(user, binder)
    return false if user.partners.count < 1
    partner = Partner.joins(:partner_users).where(:partner_users => {:user_id => user.id}).where("partner_users.role = ? OR partner_users.role = ?", "admin", "member").first
    return false if partner.nil?
    PartnerBinder.where(partner_id: partner.id, binder_id: binder.id).exists?
  end

  def can_partner_read_item?(user, item)
    binder = Binder.find(item.binder_id)
    can? :read, binder
  end

  def can_partner_write_item?(user, item)
    binder = Binder.find(item.binder_id)
    can? :write, binder
  end

  def can_partner_destroy_item?(user, item)
    binder = Binder.find(item.binder_id)
    binder_owned_by_partner? user, binder
  end

  def can_read_binder_template?(user, template)
    if template.respond_to?(:library_template) and template.library_template
      true
    else
      bt = BinderTemplate.where(:id => template.binder_template_id).first
      partner = ::Partner.joins(:partner_configuration).where(partner_configurations: { id: bt.partner_configuration_id }).first
      can? :read, partner
    end
  end

  def can_write_binder_template?(user, template)
    if template.respond_to?(:library_template) and template.library_template
        false
    else
        bt = BinderTemplate.where(:id => template.binder_template_id).first
        partner = ::Partner.joins(:partner_configuration).where(partner_configurations: { id: bt.partner_configuration_id }).first
        can? :write, partner
    end
  end

  def can_destroy_template_item?(user, template)
    if template.respond_to?(:library_template) and template.library_template
      false
    else
      bt = BinderTemplate.where(:id => template.binder_template_id).first
      partner = ::Partner.joins(:partner_configuration).where(partner_configurations: { id: bt.partner_configuration_id }).first
      partner.users.where(id: user.id).exists?
    end
  end
  
  def can_partner_read_user?(user, current_user)
    # allow partners to view any user data but client side will prevent
    # user from editing users who have already signed in
    true
  end

  def can_partner_write_user?(user, current_user)
    # allow partners to write homeowner data but client side will prevent
    # user from editing users who have already signed in
    if user.role === "homeowner"
      true
    elsif user.id === current_user.id
      true
    else
      user.partners.joins(:users).where(:users => {:id => current_user.id}).exists?
    end
  end

end