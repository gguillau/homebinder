module Homeowner

  def apply_homeowner_permissions(user)
    ############ User
    can [:read, :write, :destroy], User do |u|
      user.id == u.id
    end
    
    ############# TemplateAppliance
    can :read, Template::Appliance do |template|
      true
    end
    
    ############# TemplateMaintenance
    can :read, Template::Maintenance do |template|
      true
    end
    
    ############# PartnerBinder
    can :write, PartnerBinder do |partner_binder|
      user.id === partner_binder.client_id
    end
    
    apply_common_permissions(user)
  end
end