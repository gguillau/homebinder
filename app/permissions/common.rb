module Common

  def apply_common_permissions(user)
    ############ Binder
    can :read, Binder do |binder|
      # new way
      if UserBinder.where(user_id: user.id, binder_id: binder.id, role: UserBinderRoles::OWNER)
                   .or(UserBinder.where(user_id: user.id, binder_id: binder.id, role: UserBinderRoles::CO_OWNER))
                   .or(UserBinder.where(user_id: user.id, binder_id: binder.id, role: UserBinderRoles::READER))
                   .or(UserBinder.where(user_id: user.id, binder_id: binder.id, role: UserBinderRoles::READER_WRITER)).count > 0
        true
      end
    end
    can :write, Binder do |binder|
      # new way
      if UserBinder.where(user_id: user.id, binder_id: binder.id, role: UserBinderRoles::OWNER)
                   .or(UserBinder.where(user_id: user.id, binder_id: binder.id, role: UserBinderRoles::CO_OWNER))
                   .or(UserBinder.where(user_id: user.id, binder_id: binder.id, role: UserBinderRoles::WRITER))
                   .or(UserBinder.where(user_id: user.id, binder_id: binder.id, role: UserBinderRoles::READER_WRITER)).count > 0
        true
      end
    end
    can :destroy, Binder do |binder|
      UserBinder.where(user_id: user.id, binder_id: binder.id, role: "owner").count > 0
    end
    ############ BinderItems
    can :read, BinderItem do |item|
      can_user_read_item?(user, item)
    end
    can :write, BinderItem do |item|
      can_user_write_item?(user, item)
    end
    ############ Appliances
    # Contractors
    can [:read, :write, :destroy], Contractor do |cont|
        user.has_role?(:admin) || cont.created_by === user.id
    end
    ########### Brandings
    can :read, Binder::BinderBranding do |item|
      can_user_read_item?(user, item)
    end
    can :write, Binder::BinderBranding do |item|
      can_user_read_item?(user, item)
    end
    can :destroy, Binder::BinderBranding do |item|
      can_user_read_item?(user, item)
    end
    ########### Structures
    can :read, Binder::Structure do |item|
      can_user_read_item?(user, item)
    end
    can :write, Binder::Structure do |item|
      can_user_write_item?(user, item)
    end
    can :destroy, Binder::Structure do |item|
      can_user_destroy_item?(user, item)
    end
    ############ Areas
    can :read, Binder::Area do |item|
      can_user_read_item?(user, item)
    end
    can :write, Binder::Area do |item|
      can_user_write_item?(user, item)
    end
    can :destroy, Binder::Area do |item|
      can_user_destroy_item?(user, item)
    end
    ############ Appliances
    can :read, Binder::Appliance do |item|
      can_user_read_item?(user, item)
    end
    can :write, Binder::Appliance do |item|
      can_user_write_item?(user, item)
    end
    can :destroy, Binder::Appliance do |item|
      can_user_destroy_item?(user, item)
    end
    ############ ApplianceRecalls
    can :read, Binder::Appliance::ApplianceRecall do |item|
      item = item.appliance
      can_user_read_item?(user, item)
    end
    can :write, Binder::Appliance::ApplianceRecall do |item|
      item = item.appliance
      can_user_write_item?(user, item)
    end
    can :destroy, Binder::Appliance::ApplianceRecall do |item|
      item = item.appliance
      can_user_destroy_item?(user, item)
    end
    ############# BinderContractors
    can :read, Binder::BinderContractor do |item|
      can_user_read_item?(user, item)
    end
    can :write, Binder::BinderContractor do |item|
      can_user_write_item?(user, item)
    end
    can :destroy, Binder::BinderContractor do |item|
      can_user_destroy_item?(user, item)
    end
    ############# Finishes
    can :read, Binder::Finish do |item|
      can_user_read_item?(user, item)
    end
    can :write, Binder::Finish do |item|
      can_user_write_item?(user, item)
    end
    can :destroy, Binder::Finish do |item|
      can_user_destroy_item?(user, item)
    end
    ############# Paints
    can :read, Binder::Paint do |item|
      can_user_read_item?(user, item)
    end
    can :write, Binder::Paint do |item|
      can_user_write_item?(user, item)
    end
    can :destroy, Binder::Paint do |item|
      can_user_destroy_item?(user, item)
    end
    ############# Permits
    can :read, Binder::Permit do |item|
      can_user_read_item?(user, item)
    end
    can :write, Binder::Permit do |item|
      can_user_write_item?(user, item)
    end
    can :destroy, Binder::Permit do |item|
      can_user_destroy_item?(user, item)
    end
    ############# Projects
    can :read, Binder::Project do |item|
      can_user_read_item?(user, item)
    end
    can :write, Binder::Project do |item|
      can_user_write_item?(user, item)
    end
    can :destroy, Binder::Project do |item|
      can_user_destroy_item?(user, item)
    end
    ############# MaintenanceItems
    can :read, Binder::MaintenanceItem do |item|
      can_user_read_item?(user, item)
    end
    can :write, Binder::MaintenanceItem do |item|
      can_user_write_item?(user, item)
    end
    can :destroy, Binder::MaintenanceItem do |item|
      can_user_destroy_item?(user, item)
    end
    ############# MaintenanceEvents
    can :read, Binder::MaintenanceItem::MaintenanceEvent do |item|
      mi = Binder::MaintenanceItem.find(item.maintenance_item_id)
      can_user_read_item?(user, mi)
    end
    can :write, Binder::MaintenanceItem::MaintenanceEvent do |item|
      mi = Binder::MaintenanceItem.find(item.maintenance_item_id)
      can_user_write_item?(user, mi)
    end
    can :destroy, Binder::MaintenanceItem::MaintenanceEvent do |item|
      mi = Binder::MaintenanceItem.find(item.maintenance_item_id)
      can_user_destroy_item?(user, mi)
    end
    ############# InventoryItems
    can :read, Binder::InventoryItem do |item|
      can_user_read_item?(user, item)
    end
    can :write, Binder::InventoryItem do |item|
      can_user_write_item?(user, item)
    end
    can :destroy, Binder::InventoryItem do |item|
      can_user_destroy_item?(user, item)
    end
    ############# Receipts
    can :read, Binder::Receipt do |item|
      can_user_read_item?(user, item)
    end
    can :write, Binder::Receipt do |item|
      can_user_write_item?(user, item)
    end
    can :destroy, Binder::Receipt do |item|
      can_user_destroy_item?(user, item)
    end
    ############# Documents
    can :read, Binder::Document do |item|
      can_user_read_item?(user, item)
    end
    can :write, Binder::Document do |item|
      can_user_write_item?(user, item)
    end
    can :destroy, Binder::Document do |item|
      can_user_destroy_item?(user, item)
    end
    ############# Images
    can :read, Binder::Image do |item|
      can_user_read_item?(user, item)
    end
    can :write, Binder::Image do |item|
      can_user_write_item?(user, item)
    end
    can :destroy, Binder::Image do |item|
      can_user_destroy_item?(user, item)
    end
    ############# Shares
    can :read, Binder::Share do |item|
      can_user_read_item?(user, item)
    end
    can :write, Binder::Share do |item|
      can_user_write_item?(user, item)
    end
    can :destroy, Binder::Share do |item|
      can_user_destroy_item?(user, item)
    end
    ############# Transfers
    can :read, Binder::Transfer do |item|
      can_user_read_item?(user, item)
    end
    can :write, Binder::Transfer do |item|
      can_user_write_item?(user, item)
    end
    can :destroy, Binder::Transfer do |item|
      can_user_destroy_item?(user, item)
    end
    ############# SellerReports
    can :read, SellerReport do |item|
      can_user_read_item?(user, item)
    end
    can :write, SellerReport do |item|
      can_user_write_item?(user, item)
    end
    can :destroy, SellerReport do |item|
      can_user_destroy_item?(user, item)
    end
    ############# Transfer
    can :transfer, Binder do |binder|
      user_is_binder_owner?(user, binder)
    end
    ############# Share
    can :share, Binder do |binder|
      user_is_binder_owner?(user, binder)
    end
    ############# RepairPricer
    can :access_repair_pricer, Binder do |binder|
      UserBinder.where(:user_id => user.id, :binder_id => binder.id, :access_repair_pricer => true).exists?
    end
    ############## Dashboards
    can [:read, :write, :destroy], Dashboard do |board|
      board.user_id == user.id
    end
    ############## Dashboards
    can [:read, :write, :destroy], DashboardWidget do |dashboard_widget|
      dashboard_widget.dashboard.user_id == user.id
    end
    ############# Subscriptions
    can :read, Subscription do |subscription|
      can_user_read_item?(user, subscription)
    end
    can :read, UserContractor do |user_contractor|
      user.id == user_contractor.user_id
    end

    can :write, UserContractor do |user_contractor|
      user.id == user_contractor.user_id
    end

    can :destroy, UserContractor do |user_contractor|
      user.id == user_contractor.user_id
    end
  end

  def user_is_binder_owner?(user, binder)
    if UserBinder.where(user_id: user.id, binder_id: binder.id, role: UserBinderRoles::OWNER)
                 .or(UserBinder.where(user_id: user.id, binder_id: binder.id, role: UserBinderRoles::CO_OWNER)).count > 0
      true
    end
  end

  def can_user_read_item?(user, item)
    binder = Binder.find(item.binder_id)
    can? :read, binder
  end

  def can_user_write_item?(user, item)
    binder = Binder.find(item.binder_id)
    can? :write, binder
  end

  def can_user_destroy_item?(user, item)
    binder = Binder.find(item.binder_id)
    user_is_binder_owner? user, binder
  end

end