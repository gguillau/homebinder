class DashboardScope
  BINDER = :binder
  USER = :user
  ADMIN = :admin
  INSPECTOR = :inspector
  BROKER = :broker
  PARTNER = :partner
  AGENT = :agent
end