# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  confirmation_token     :string(255)
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string(255)
#  account_type           :string(255)      default("user")
#  admin                  :boolean          default(FALSE)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  authentication_token   :string(255)
#  create_method          :string(30)
#  accepted_transfer_at   :date
#  role                   :string(100)
#  created_by             :integer
#  salesforce_id          :string
#  intercom_id            :string
#  completed_onboarding   :boolean          default(FALSE)
#  last_intercom_error    :datetime
#
require "securerandom"

class User < ApplicationRecord
    include User::UserActions

    has_secure_password
    has_secure_token :access_token

    # associations
    has_one :user_profile,  :dependent => :destroy
    has_one :contractor, :class_name => "Contractor"

    has_many :contractors, :class_name => "UserContractor"
    has_many :organization_users, :dependent => :destroy
    has_many :organizations, :through => :organization_users
    has_many :partner_users, :dependent => :destroy
    has_many :partners, :through => :partner_users

    has_many :user_contractors, :dependent => :destroy

    has_many :partner_clients, :dependent => :destroy, :foreign_key => "client_id"
    has_many :partner_binders, :foreign_key => "client_id"
    has_many :companies, :through => :partner_clients, :foreign_key => "partner_id", :source => "partner"

    has_many :user_binders, :dependent => :destroy
    has_many :binders, :through => :user_binders
    has_many :binders_created, :foreign_key => "created_by"
    has_many :dashboards, :dependent => :destroy
    belongs_to :creator, :class_name => "User", :foreign_key => "created_by"

    has_many :binder_items

    # transfers
    has_many :transfers_sent, :foreign_key => "sender_id", :class_name => "Binder::Transfer"
    has_many :transfers_received, :foreign_key => "receiver_id", :class_name => "Binder::Transfer"

    # shares
    has_many :shares_sent, :foreign_key => "shared_by_id", :class_name => "Binder::Share"
    has_many :shares_received, :foreign_key => "shared_with_id", :class_name => "Binder::Share"

    # sessions
    has_many :sessions,  :dependent => :destroy
    
    # transactions
    has_many :user_binder_transactions,  :dependent => :destroy
    has_many :binder_transactions, :through => :user_binder_transactions, :class_name => "Binder::Transaction"
    
    # marketing resources
    has_many :user_resources,  :dependent => :destroy
    has_many :marketing_resources, :through => :user_resources
    
    accepts_nested_attributes_for :user_profile
    accepts_nested_attributes_for :user_binders

    validates_uniqueness_of :email, allow_blank: false, if: :email_changed?
    validates_format_of     :email, with: /\A[^@\s]+@[^@\s]+\z/, allow_blank: false, if: :email_changed?

    validates_presence_of   :password, :on => create
    validates_length_of     :password, within: 8..128, allow_blank: true

    validates_length_of     :role, within: 2..20
    validates_presence_of   :role

    # make sure to downcase emails
    before_validation :format_email

    after_create :create_additional_associations

    after_save :update_contractor

    def format_email
        return if email.nil?
        self.email = Partner::Automation::EmailParser.new.format_value(["", "email"], email.downcase.gsub(/\s+/, ""))
    end

    def create_additional_associations
        Users::CreateAdditionalAssociationsJob.perform_async(id)
    end

    def update_contractor
        Users::UpdateContractorJob.perform_async(id)
    end

    def ability
        @ability = @ability || Ability.new(self)
        @ability
    end

    # determines if the user is an admin
    def is_admin?
        return role == UserGlobalRoles::ADMIN.to_s || role === "limited_admin"
    end

    def is_homeowner?
        return role == UserGlobalRoles::HOMEOWNER.to_s
    end

    def is_agent?
        return role == UserGlobalRoles::AGENT.to_s
    end

    # determines if the user is part of a partner group
    def belongs_to_partner?
        return false if role === "homeowner"
        return true if role === "broker" || role === "inspector" || role === "homepro" || role === "property_manager" || role === "builder" || role === "lender"
        return partners.count > 0
    end

    # determine if the user is directly part of an organization
    def belongs_to_organization?
        return organizations.count > 0
    end

    def allow_calls
        # check if created_by user exists and if a partner user who allows client calls
        return User.joins(:partners, :partners => :partner_configuration).where(:id => self.created_by, :partner_configurations => {:allow_client_calls  => true}).count > 0
    end

    def authorize!(action, object)
        self.ability.authorize! action, object, :message => "You are not authorized to #{action} #{object.class.name} with ID: #{object.id}."
    end

    def add_role(role, item = nil)
        role = role.to_s

        case role
        when "admin"
            self.role = "admin"
            self.save!
        when "partner_admin"
            PartnerUser.create!(:partner_id => item.id, :user_id => self.id, :role => "admin")
        when "member"
            PartnerUser.create!(:partner_id => item.id, :user_id => self.id, :role => "member")
        when "agent"
            PartnerUser.create!(:partner_id => item.id, :user_id => self.id, :role => "agent")
        else
            item_class = item.class.to_s
            case item_class
            when "Binder"
                UserBinder.create!(:binder_id => item.id, :user_id => self.id, :role => role)
            end
        end
    end

    def has_role?(role, item = nil)
        role = role.to_s
        case role
        when "admin"
            self.role === "admin"
        when "partner_admin"
            PartnerUser.where(:partner_id => item.id, :user_id => self.id, :role => "admin").count > 0
        when "member"
            PartnerUser.where(:partner_id => item.id, :user_id => self.id).where("role = 'member'").count > 0
        when "agent"
            PartnerUser.where(:partner_id => item.id, :user_id => self.id, :role => "agent").count > 0
        else
            item_class = item.class.to_s
            case item_class
            when "Binder"
                UserBinder.where(:binder_id => item.id, :user_id => self.id, :role => role).count > 0
            end
        end
    end

    def remove_role(role, item = nil)
        role = role.to_s
        case role
        when "admin"
            self.role = "homeowner"
            self.save!
        when "partner_admin"
            PartnerUser.where(:partner_id => item.id, :user_id => self.id, :role => "admin").delete_all
        when "member"
            PartnerUser.where(:partner_id => item.id, :user_id => self.id, :role => "member").delete_all
        else
            item_class = item.class.to_s
            case item_class
            when "Binder"
                UserBinder.where(:binder_id => item.id, :user_id => self.id, :role => role).delete_all
            end
        end
    end

    def self.with_role(role, item = nil)
        if item.present?
            role = role.to_s
            case role
            when "partner_admin"
                joins(:partner_users).where(:partner_users => {:partner_id => item.id, :role => "admin"})
            when "member"
                joins(:partner_users).where(:partner_users => {:partner_id => item.id, :role => "member"})
            else
                item_class = item.class.to_s
                case item_class
                when "Binder"
                    joins(:user_binders).where(:user_binders => {:binder_id => item.id, :role => role})
                end
            end
        else
            []
        end
    end

    def generate_authentication_token(length = 20)
        loop do
            rlength = (length * 3) / 4
            token = SecureRandom.urlsafe_base64(rlength).tr('lIO0', 'sxyz')
            break token unless Session.where(token: token).first
        end
    end

    def valid_password?(password)
        return authenticate(password)
    end

    def welcome_path
        case role
        when "admin"
            return role
        when "homeowner", "agent"
            return role.pluralize
        else
            return "partners"
        end
    end
end