# == Schema Information
#
# Table name: project_statuses
#
#  id   :integer          not null, primary key
#  name :string(255)
#

class ProjectStatus < ApplicationRecord
  # attr_accessible :title, :body
  
  # validations
  validates :name, presence: true, length: { maximum: 250 }, unique: {}
end
