# == Schema Information
#
# Table name: user_binder_transactions
#
#  id                    :integer          not null, primary key
#  binder_transaction_id :integer          not null
#  user_id               :integer          not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class UserBinderTransaction < ApplicationRecord
    
    # associations
    belongs_to :user
    belongs_to :binder_transaction, :class_name => "Binder::Transaction"

    # validations
    validates_presence_of   :binder_transaction_id,     :message => I18n.t(:err_value_required)
    validates_presence_of   :user_id,                   :message => I18n.t(:err_value_required)
    
    validates_uniqueness_of :user_id, scope: :binder_transaction_id
end
