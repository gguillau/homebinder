# == Schema Information
#
# Table name: property_types
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_by :integer
#  verified   :boolean
#

class PropertyType < ApplicationRecord
    
    # validations
    validates :name, presence: true, length: { maximum: 250 }, unique: {}
end
