# == Schema Information
#
# Table name: partner_widgets
#
#  id               :integer    not null, primary key
#  partner_id       :integer    not null
#  widget_id        :integer    not null
#  configuration    :text
#  created_at       :datetime   not null
#  updated_at       :datetime   not null
#

class PartnerWidget < ApplicationRecord
    self.table_name = "partner_widgets" 
    
    # associations
    belongs_to :partner
    belongs_to :widget

    # validations
    validates_presence_of   :partner,   :message => I18n.t(:err_value_required)
    validates_presence_of   :widget,    :message => I18n.t(:err_value_required)
    
    # one unique widget per partner
    validates_uniqueness_of :widget_id, scope: :partner_id, :message => "is already a partner widget"
    
    # add widget to all pre-existing binders when partner_widget is created and destroy them when the partner_binder is destroyed
    after_create -> { add_custom_widget }
    after_destroy -> { destroy_custom_widget }
    
    def self.query_arguments
        ["partner_id", "partnerId"]
    end

    def self.query_arguments_hash(query, value)
        case query
        when "partner_id"
            {query.to_sym => value }
        when "partnerId"
            {:partner_id => value }
        end
    end
    
    def add_custom_widget
        binder_ids = Binder::BinderBranding.where(:partner_id => self.partner_id, :scope => "binder").pluck(:binder_id)
        
        binder_ids.each do |binder_id|
            user_binder = UserBinder.where(:binder_id => binder_id, :role => "owner").first
            next if user_binder.nil?
            
            owner = User.find_by_id(user_binder.user_id)
            next if owner.nil?
            
            dashboard = Dashboards::DashboardLookup.new(owner).for_binder({:id => binder_id})
            next if dashboard.nil?
            next if dashboard.system? && dashboard.default?
            
            if !DashboardWidget.where(:dashboard_id => dashboard.id, :widget_id => self.widget_id).exists?
                DashboardWidget.create!(:dashboard_id => dashboard.id, :widget_id => self.widget_id, :index => 1)
            end
        end
    end
    
    def destroy_custom_widget
        binder_ids = Binder::BinderBranding.where(:partner_id => self.partner_id, :scope => "binder").pluck(:binder_id)
        
        binder_ids.each do |binder_id|
            user_binder = UserBinder.where(:binder_id => binder_id, :role => "owner").first
            next if user_binder.nil?
            
            owner = User.find_by_id(user_binder.user_id)
            next if owner.nil?
            
            dashboard = Dashboards::DashboardLookup.new(owner).for_binder({:id => binder_id})
            next if dashboard.nil?
            next if dashboard.system? && dashboard.default?
            
            if DashboardWidget.where(:dashboard_id => dashboard.id, :widget_id => self.widget_id).exists?
                DashboardWidget.where(:dashboard_id => dashboard.id, :widget_id => self.widget_id).destroy_all
            end
        end
    end
    
end