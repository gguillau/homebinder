class UserGlobalRoles
  ADMIN = :admin
  HOMEOWNER = :homeowner
  PARTNER = :partner
  INSPECTOR = :inspector
  BROKER = :broker
  AGENT = :agent
  LENDER = :lender
  HOMEPRO = :homepro
  BUILDER = :builder
  PROPERTYMANAGER = :property_manager
  HOMEOWNER_ASSOCIATION = :hoa
  INSURANCE = :insurance
  
  def self.map_partner_type(partner_type)
    if partner_type == PartnerTypes::INSPECTOR.to_s
      return INSPECTOR
    elsif partner_type == PartnerTypes::BROKER.to_s
      return BROKER
    elsif partner_type == LENDER.to_s
      return LENDER
    elsif partner_type == HOMEPRO.to_s
      return HOMEPRO
    elsif partner_type == BUILDER.to_s
      return BUILDER
    elsif partner_type == PROPERTYMANAGER.to_s
      return PROPERTYMANAGER
    elsif partner_type == HOMEOWNER_ASSOCIATION.to_s
      return HOMEOWNER_ASSOCIATION
    elsif partner_type == INSURANCE.to_s
      return INSURANCE
    else
      return PARTNER
    end
  end
end