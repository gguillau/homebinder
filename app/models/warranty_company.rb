# == Schema Information
#
# Table name: warranty_companies
#
#  id              :integer          not null, primary key
#  name            :string(100)
#  contact         :string(100)
#  email           :string(100)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  secondary_email :string
#

class WarrantyCompany < ApplicationRecord
    include Warranty::Company::Actions
    
    # associations
    has_many :warranty_plans, :dependent => :destroy
    has_many :warranty_configurations, :dependent => :destroy
    has_many :warranties
    
    # validations
    validates_presence_of   :name,  :message => I18n.t(:err_value_required)
    validates_length_of     :name,  :maximum => 100
    
    validates_presence_of   :email,  :message => I18n.t(:err_value_required)
    validates_length_of     :email,  :maximum => 100
    
    validates_presence_of   :contact,  :message => I18n.t(:err_value_required)
    validates_length_of     :contact,  :maximum => 100
    
    validates_uniqueness_of :email
    
    def warranties
        Warranty.joins(:warranty_plan).where(:warranty_plans => {:warranty_company_id => self.id})
    end
end
