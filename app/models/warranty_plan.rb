# == Schema Information
#
# Table name: warranty_plans
#
#  id                  :integer          not null, primary key
#  name                :string(100)
#  duration            :integer
#  cycle               :string(10)
#  description         :string(1000)
#  price               :money            not null
#  warranty_company_id :integer          not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class WarrantyPlan < ApplicationRecord
    include Warranty::Plan::Actions
    
    # associations
    belongs_to :warranty_company
    has_many :warranties
    
    # validations
    validates_presence_of   :name,  :message => I18n.t(:err_value_required)
    validates_length_of     :name,  :maximum => 100
    
    validates_presence_of   :duration,  :message => I18n.t(:err_value_required)
    
    validates_presence_of   :cycle,  :message => I18n.t(:err_value_required)
    validates_length_of     :cycle,  :maximum => 10
    
    validates_presence_of   :description,  :message => I18n.t(:err_value_required)
    validates_length_of     :description,  :maximum => 1000
    
    validates_presence_of   :price,  :message => I18n.t(:err_value_required)

    validates_presence_of   :warranty_company,  :message => I18n.t(:err_value_required)
    validates_uniqueness_of :name, scope: :warranty_company_id

end
