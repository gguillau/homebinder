# == Schema Information
#
# Table name: partner_binder_transactions
#
#  id                    :integer          not null, primary key
#  binder_transaction_id :integer          not null
#  partner_id            :integer          not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class PartnerBinderTransaction < ApplicationRecord
    
    # associations
    belongs_to :partner
    belongs_to :binder_transaction, :class_name => "Binder::Transaction"

    # validations
    validates_presence_of   :binder_transaction,     :message => I18n.t(:err_value_required)
    validates_presence_of   :partner,                :message => I18n.t(:err_value_required)
    
    validates_uniqueness_of :partner_id, scope: :binder_transaction_id
end
