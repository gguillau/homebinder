class UserContractorType < ApplicationRecord
    self.table_name = "user_contractor_types"

    belongs_to  :user_contractor,   :class_name => "UserContractor"
    belongs_to  :contractor_category,   :class_name => "Contractor::Category"

    # validations
    validates_uniqueness_of :user_contractor, scope: :contractor_category, :message => "already exists"
    validates_presence_of       :user_contractor, :message => I18n.t(:err_value_required), :on => :update
    validates_presence_of       :contractor_category, :message => I18n.t(:err_value_required)
end
