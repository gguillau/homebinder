module BuildFaxReport::Actions

    def self.included(klass)
        klass.extend ClassMethods 
    end
    
    module ClassMethods
        
        def base_query
            "CAST(build_fax_reports.id as varchar(25)) LIKE :search OR
            CAST(build_fax_reports.partner_id as varchar(25)) LIKE :search OR
            CAST(build_fax_reports.binder_id as varchar(25)) LIKE :search OR
            LOWER(build_fax_reports.status) LIKE :search OR
            LOWER(build_fax_reports.report_file_name) LIKE :search OR
            LOWER(CONCAT(addresses.address1, ' ', addresses.address2, ' ', addresses.city, ', ', addresses.state, ' ', addresses.zip)) LIKE :search".freeze
        end
        
        def index(hb_request, params)
            if hb_request.role != "admin"
                raise BadRequestException.new "User must be an admin or partner" unless params[:partnerId].present?
                
                partner = Partner.find(params[:partnerId])
                
                hb_request.ability.authorize! :read, partner, :message => "You are not authorized to access partner ID: #{partner.id}."
            end
            
            super(hb_request, params)
        end
        
        def build(hb_request, params)
            # check for required fields
            raise BadRequestException.new "Report Address required" unless params[:build_fax_report].present?
            raise BadRequestException.new "User required" unless hb_request.user.present?
            
            build_fax_report = params[:build_fax_report]
            address = "#{build_fax_report[:address]} #{build_fax_report[:city]} #{build_fax_report[:state]} #{build_fax_report[:zip]}"
            partner_id = params[:build_fax_report][:partner_id]
            
            file = File.new("#{Rails.root}/tmp/#{build_fax_report[:address]}.pdf", "wb")
            file << Buildfax::Request.new.download_report(address)

            build_fax_report = {
                address_attributes: {
                    address1: build_fax_report[:address],
                    city: build_fax_report[:city],
                    state: build_fax_report[:state],
                    zip: build_fax_report[:zip]
                },
                partner_id: partner_id,
                report: file,
                status: "not_paid"
            }

            # create the report
            report = BuildFaxReport.new(build_fax_report)
            
            # save the report
            if not report.save
                raise UnprocessableException.new(report)
            end
            
            # return the report
            return report
        end
        
        def search_buildfax(hb_request, params)
            # check for required fields
            raise BadRequestException.new "Search Address required" unless params[:search].present?
            raise BadRequestException.new "User required" unless hb_request.present?
            raise BadRequestException.new "User required" unless hb_request.user.present?
            
            address = "#{params[:search][:address]} #{params[:search][:city]} #{params[:search][:state]} #{params[:search][:zip]}"
            
            return Buildfax::Client.new(hb_request, nil).send_request(address)
        end
        
        def add_custom_args(where, where_not, params, includes, joins)
            # check if user is searching by partnerId
            if params[:partnerId]
                where.merge!({:partner_id => params[:partnerId]})
            end

            # check if user is searching by status
            if params[:status]
                where.merge!({:status => params[:status]})
            end

            if params[:state]
                joins.push(:address)
                where.merge!({:addresses => {:state => params[:state]}})
            end
        end
    end
end