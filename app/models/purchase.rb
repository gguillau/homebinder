# == Schema Information
#
# Table name: purchases
#
#  id                :integer          not null, primary key
#  store_id          :integer
#  date              :date
#  price_cents       :integer          default(0), not null
#  price_currency    :string(255)      default("USD"), not null
#  created_by        :integer
#  purchaseable_id   :integer
#  purchaseable_type :string(255)
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#

class Purchase < ApplicationRecord
  
  monetize :price_cents, :allow_nil => true
  
  validates :price_cents, presence: {:message => I18n.t(:err_value_required)}
  
  def store
    Store.find(self.store_id).name.titleize unless self.store_id.nil?
  end
  
  def store=(str)
    self.store_id = Store.where(:name => str.downcase).first_or_create(:verified => false, :created_by => self.created_by).id unless str.nil?
  end
end
