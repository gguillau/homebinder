require 'securerandom'

module PromoCode::Actions
  
    def self.included(klass)
        klass.extend ClassMethods 
    end
  
    module ClassMethods
        include ActionView::Helpers::NumberHelper
        
        # checks for a valid promo code
        # checks if expired
        # returns 0 for amount off when invalid

        def verify_promo_code(amount, promo_code)
            return 0 if promo_code.nil?
            code = PromoCode.find_by_name(promo_code)
            return 0 if code.nil?
            return 0 if code.expiration_date.present? and code.expiration_date < Date.today
            if code.percent_off > 0
                amount_off = amount.to_f * (code.percent_off.to_f/100.to_f)
                return number_with_precision(amount_off,:precision => 4).to_i
            elsif code.amount_off > 0
                return code.amount_off * 100
            end
        end
        
    end
end