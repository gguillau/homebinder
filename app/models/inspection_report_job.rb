class InspectionReportJob < ApplicationRecord
    
    # relationships
    belongs_to :binder
    belongs_to :partner
    
    validates_presence_of :status, :message => I18n.t(:err_value_required)
    
    has_attached_file :inspection_report, :path => "/inspection_report_jobs:id/:filename"
    
    enum status: {
        created: 0,
        pending: 1,
        processing: 2,
        error: 3,
        complete: 4,
        binder_created: 5
    }, _prefix: :status
    
    validates_attachment :inspection_report, :content_type => {
        :content_type => [
            "application/pdf",
            "application/mspowerpoint",
            "application/powerpoint",
            "application/vnd.ms-powerpoint",
            "application/x-mspowerpoint",
            "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
            "application/msword",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "application/excel",
            "application/vnd.ms-excel",
            "application/x-excel",
            "application/x-msexcel",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "text/plain"
        ]
    }
    
    settings do
        mappings dynamic: false do
            indexes :id,                        type: :integer
            indexes :job_id,                    type: :keyword
            indexes :status,                    type: :keyword
            indexes :binder_id,                 type: :integer
            indexes :created_at,                type: :date
            indexes :updated_at,                type: :date
            
            indexes :partner do
                indexes :name,                type: :text
                indexes :phone,               type: :text
                indexes :email,               type: :text
                indexes :created_at,          type: :date
                indexes :contact,             type: :text
                indexes :partner_type,        type: :keyword
                indexes :website,             type: :text
                indexes :partner_key,         type: :text
                indexes :partner_configuration do
                    indexes :default_binder_action,                 type: :keyword
                    indexes :automation_binder_action,              type: :boolean
                    indexes :binder_action_delay_length,            type: :integer
                    indexes :send_agents_transfer_notification,     type: :keyword
                    indexes :show_isn_tab,                          type: :boolean
                    indexes :default_user_branding_option,          type: :keyword
                    indexes :allow_client_calls,                    type: :boolean
                    indexes :active_warranty_account,               type: :boolean
                    indexes :repair_pricer_enabled,                 type: :boolean
                    indexes :repair_pricer_pre_paid_reports,        type: :boolean
                    indexes :repair_pricer_messaging_enabled,       type: :boolean
                end
            end
            
            indexes :binder do
                indexes :name,              type: :text
                indexes :created_at,        type: :date
                indexes :create_method,     type: :keyword
                indexes :status,            type: :keyword
                indexes :details,           type: :text
            end
        end
    end
    
    def as_indexed_json(options={})
        self.as_json(
            {
                only: [:id, :job_id, :status, :binder_id, :created_at, :updated_at],
                include: {
                    :binder => {
                        :only => [:name, :created_at, :create_method, :status, :details]
                    },
                    :partner => {
                        :only => [:name, :phone, :email, :created_at, :contact, :partner_type, :website, :partner_key], 
                        :include => [
                            :partner_configuration => {
                                :only => [:default_binder_action, :automation_binder_action, :binder_action_delay_length, :binder_action_delay_length,
                                :send_agents_transfer_notification, :show_isn_tab, :default_user_branding_option, :allow_client_calls,
                                :repair_pricer_enabled, :repair_pricer_pre_paid_reports, :repair_pricer_messaging_enabled]
                            }
                        ]
                    }
                }
            }
        )
    end
end
