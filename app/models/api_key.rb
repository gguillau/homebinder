# == Schema Information
#
# Table name: api_keys
#
#  id               :integer          not null, primary key
#  company_name     :string(255)
#  application_name :string(255)
#  key              :string(255)
#  contact_email    :string(255)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  partner_id       :integer
#

class ApiKey < ApplicationRecord
    include ApiKey::ApiKeyBuilder
    
    has_secure_token :key
  
    # associations
    belongs_to :partner
    
    # validations
    validates_presence_of   :company_name,          :message => I18n.t(:err_value_required)
    validates_presence_of   :application_name,      :message => I18n.t(:err_value_required)
    validates_presence_of   :contact_email,         :message => I18n.t(:err_value_required)
    
    # one unique key
    validates_uniqueness_of :key
    
    # one unique partner
    validates_uniqueness_of :partner_id, :message => "already has an API key"
end
