# == Schema Information
#
# Table name: seller_report_items
#
#  id                     :integer          not null, primary key
#  seller_reportable_id   :integer
#  seller_reportable_type :string(255)
#  include                :boolean
#  sort_index             :integer
#

class SellerReportItem < ApplicationRecord
end
