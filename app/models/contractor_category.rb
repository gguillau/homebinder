class ContractorCategory < ApplicationRecord
    self.table_name = "contractor_types"
    
    belongs_to  :contractor
    belongs_to  :contractor_category,   :class_name => "Contractor::Category"
    
    # validations
    validates_uniqueness_of     :contractor, scope: :contractor_category, :message => "already exists"
    validates_presence_of       :contractor, :message => I18n.t(:err_value_required), :on => :update
    validates_presence_of       :contractor_category, :message => I18n.t(:err_value_required)
    
    accepts_nested_attributes_for :contractor_category
end
