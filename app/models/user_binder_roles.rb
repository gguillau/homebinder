class UserBinderRoles
  OWNER = :owner
  CO_OWNER = :co_owner
  READER = :reader
  WRITER = :writer
  READER_WRITER = :reader_writer
  BUYER_AGENT = :buyer_agent
  SELLER_AGENT = :seller_agent
  
  def self.map(strRole)
    if strRole == OWNER.to_s
      return OWNER
    elsif strRole == CO_OWNER.to_s
      return CO_OWNER
    elsif strRole == READER.to_s
      return READER
    elsif strRole == WRITER.to_s
      return WRITER
    elsif strRole == READER_WRITER.to_s
      return READER_WRITER
    elsif strRole == BUYER_AGENT.to_s
      return BUYER_AGENT
    elsif strRole == SELLER_AGENT.to_s
      return SELLER_AGENT
    else
      raise BadRequestException.new
    end
  end
end