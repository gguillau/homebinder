# == Schema Information
#
# Table name: annual_property_review_capital_items
#
#  id                        :integer          not null, primary key
#  annual_property_review_id :integer
#  name                      :string(255)      not null
#  eul                       :integer          default("less_one"), not null
#  cost_range                :integer          default("less_five_hun"), not null
#  comments                  :text
#

class AnnualPropertyReviewCapitalItem < ApplicationRecord
  
  belongs_to :annual_property_review
  has_many :annual_property_review_photos,            :dependent => :destroy
  
  validates :name, presence: true, length: { maximum: 255 }
  validates :eul, presence: true, length: { maximum: 30 }
  validates :cost_range, presence: true, length: { maximum: 30 }
  
  enum eul: {
    less_one: 0,
    one_to_two: 1,
    two_to_three: 2,
    three_to_five: 3,
    five_to_ten: 4,
    ten_to_fifteen: 5,
    fifteen_to_twenty: 6,
    twenty_plus: 7,
    unknown: 8,
    none: 9,
    not_applicable: 10
  }, _prefix: :eul
  enum cost_range: {
    less_five_hun: 0,
    five_hun_to_one_thou: 1,
    one_thou_to_twentyfive_hun: 2,
    twentyfive_hun_to_five_thou: 3,
    five_thou_to_ten_thou: 4,
    ten_thou_to_fifteen_thou: 5,
    fifteen_thou_to_twenty_thou: 6,
    twenty_thou_plus: 7,
    unknown: 8 
  }, _prefix: :cost_range
  
  def cost
    case self.cost_range
    when "less_five_hun"
      return "Less than $500"
    when "five_hun_to_one_thou"
      return "$500 - $1,000"
    when "one_thou_to_twentyfive_hun"
      return "$1,000 - $2,500"
    when "twentyfive_hun_to_five_thou"
      return "$2,500 to $5,000"
    when "five_thou_to_ten_thou"
      return "$5,000 - $10,000"
    when "ten_thou_to_fifteen_thou"
      return "$10,000 - $15,000"
    when "fifteen_thou_to_twenty_thou"
      return "$15,000 - $20,000"
    when "twenty_thou_plus"
      return "$20,000+"
    when "unknown"
      return "Unknown"
    end
  end
  
  def rul
    case self.eul
    when "less_one"
      return "Less than 1 year"
    when "one_to_two"
      return "1 to 2 years"
    when "two_to_three"
      return "2 to 3 years"
    when "three_to_five"
      return "3 to 5 years"
    when "five_to_ten"
      return "5 to 10 years"
    when "ten_to_fifteen"
      return "10 to 15 years"
    when "fifteen_to_twenty"
      return "15 to 20 years"
    when "twenty_plus"
      return "20+ years"
    when "none"
      return "None"
    when "not_applicable"
      return "N/A"
    when "unknown"
      return "Unknown"
    end
  end
  
end
