# == Schema Information
#
# Table name: partner_clients
#
#  id         :integer          not null, primary key
#  partner_id :integer          not null
#  client_id  :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class PartnerClient < ApplicationRecord

    # associations
    belongs_to :partner
    belongs_to :client, :class_name => "User", :foreign_key => "client_id"

    # validations
    validates_presence_of   :partner,    :message => I18n.t(:err_value_required)
    validates_presence_of   :client,     :message => I18n.t(:err_value_required)

    # one unique homeowner/client per partner
    validates_uniqueness_of :client_id, scope: :partner_id, :message => "is already a partner client"

    def self.query_arguments
        ["partner_id", "client_id"]
    end

    def self.query_arguments_hash(query, value)
        case query
        when "partner_id", "client_id"
          {query.to_sym => value }
        end
    end
    
    settings do
        mappings dynamic: false do
            indexes :partner_id,                    type: :integer
            indexes :client_id,                     type: :integer
            indexes :created_at,                    type: :date
            
            indexes :partner do
                indexes :name,                type: :text
                indexes :phone,               type: :text
                indexes :email,               type: :text
                indexes :created_at,          type: :date
                indexes :contact,             type: :text
                indexes :partner_type,        type: :keyword
                indexes :website,             type: :text
                indexes :partner_key,         type: :text
                indexes :partner_configuration do
                    indexes :default_binder_action,                 type: :keyword
                    indexes :automation_binder_action,              type: :boolean
                    indexes :binder_action_delay_length,            type: :integer
                    indexes :send_agents_transfer_notification,     type: :keyword
                    indexes :show_isn_tab,                          type: :boolean
                    indexes :default_user_branding_option,          type: :keyword
                    indexes :allow_client_calls,                    type: :boolean
                    indexes :active_warranty_account,               type: :boolean
                    indexes :repair_pricer_enabled,                 type: :boolean
                    indexes :repair_pricer_pre_paid_reports,        type: :boolean
                    indexes :repair_pricer_messaging_enabled,       type: :boolean
                end
            end

            indexes :client do
                indexes :email,                 type: :text
                indexes :role,                  type: :keyword
                indexes :sign_in_count,         type: :integer
                indexes :created_at,            type: :date
                indexes :last_sign_in_at,       type: :date
                indexes :create_method,         type: :keyword
                indexes :user_profile do
                    indexes :first_name,        type: :text
                    indexes :last_name,         type: :text
                    indexes :mobile_phone,      type: :text
                end
            end
        end
    end
    
    def as_indexed_json(options={})
        self.as_json(
            {
                only: [:client_id, :partner_id, :created_at],
                include: {
                    :partner => {
                        :only => [:name, :phone, :email, :created_at, :contact, :partner_type, :website, :partner_key], 
                        :include => [
                            :partner_configuration => {
                                :only => [:default_binder_action, :automation_binder_action, :binder_action_delay_length, :binder_action_delay_length,
                                :send_agents_transfer_notification, :show_isn_tab, :default_user_branding_option, :allow_client_calls,
                                :repair_pricer_enabled, :repair_pricer_pre_paid_reports, :repair_pricer_messaging_enabled]
                            }
                        ]
                    },
                    :client => {
                        :only => [:email, :role, :sign_in_count, :created_at, :last_sign_in_at, :create_method], 
                        :include => [
                            :user_profile => {
                                :only => [:first_name, :last_name, :mobile_phone]
                            }
                        ]
                    }
                }
            }
        )
    end
end
