module UserBinder::Actions
    def self.included(klass)
        klass.extend ClassMethods
    end
    
    module ClassMethods
        def find_by_access_token(current_user, params)
            raise BadRequestException.new "Missing Repair Pricer token" if params[:id].nil?

            # get the user_binder
            user_binder = find_by_repair_pricer_token(params[:id])

            # check if user_binder exists
            raise BadRequestException.new "Invalid Repair Pricer token" if user_binder.nil?

            # return the user
            return user_binder
        end
        
        def find_by_property_address(current_user, params)
            raise BadRequestException.new "Missing Country" if params[:country].nil?
            raise BadRequestException.new "Missing Address" if params[:address1].nil?
            raise BadRequestException.new "Missing City" if params[:city].nil?
            raise BadRequestException.new "Missing State" if params[:state].nil?
            
            # get binders with zip code
            binders = Binder.joins(:property, :transfers).where("LOWER(properties.address1) LIKE :search", {search: '%' + params[:address1].downcase + '%'}).where(:properties => {:country => params[:country], :city => params[:city], :state => params[:state], :zip => params[:zip]}, :transfers => {:status => ["pending", "created", "accepted"]})
            
            # if no binders were found, seach again without zip param
            if binders.empty?
                binders = Binder.joins(:property, :transfers).where("LOWER(properties.address1) LIKE :search", {search: '%' + params[:address1].downcase + '%'}).where(:properties => {:country => params[:country], :city => params[:city], :state => params[:state]}, :transfers => {:status => ["pending", "created", "accepted"]})
            end
            
            # get owner and transfer information
            results = []
            binders.each do |binder|
                owner = {
                    :email => binder.owner.email,
                    :phone => binder.owner.user_profile.mobile_phone
                }
                results.push({:owner => owner, :access_token => binder.transfers.last.access_token, :transfer_status => binder.transfers.last.status})
            end
            
            # return the binders
            return results
        end
    end
end