module Widget::WidgetBuilder

    def self.included(klass)
        klass.extend ClassMethods 
    end
    
    module ClassMethods
        def base_query
            "CAST(widgets.id as varchar(25)) LIKE :search OR
                     LOWER(widgets.name) LIKE :search OR
                     LOWER(widgets.category) LIKE :search".freeze
        end
    end
end