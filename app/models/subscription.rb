# == Schema Information
#
# Table name: subscriptions
#
#  id             :integer          not null, primary key
#  binder_id      :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  plan_id        :string(255)
#  customer_id    :string(255)
#  payment_status :string(255)
#

require "stripe"

class Subscription < ApplicationRecord
  belongs_to :binder
  
  # validations
  validates_presence_of   :binder,  :message => I18n.t(:err_value_required)
  
  # one unique subscription per binder
  validates_uniqueness_of :binder
  
  before_destroy :clean_stripe
  
  PLAN_FREE = "free".freeze
  PAYMENT_STATUS_PAID = "paid".freeze
  PAYMENT_STATUS_FAILED = "failed".freeze
  
  def is_free?
    return self.plan_id == PLAN_FREE
  end
  
  def is_paid?
    return self.payment_status == PAYMENT_STATUS_PAID
  end
  
  def is_payment_failed?
    return self.payment_status == PAYMENT_STATUS_FAILED
  end
  
  private
  
  def clean_stripe
    if not self.customer_id.nil?
      begin
        cust = Stripe::Customer.retrieve(self.customer_id)
        cust.delete
      rescue => e
        ErrorService.perform_async(e.message, {subscription_id: id})
      end
    end
  end
   
end
