class DocumentType < ApplicationRecord
    # validations
    validates_presence_of :name, :message => I18n.t(:err_value_required)
end