# == Schema Information
#
# Table name: annual_property_review_photos
#
#  id                                     :integer          not null, primary key
#  annual_property_review_id              :integer
#  annual_property_review_finding_id      :integer
#  annual_property_review_capital_item_id :integer
#  name                                   :string(255)      not null
#  description                            :text
#  file_file_name                         :string
#  file_content_type                      :string
#  file_file_size                         :integer
#  file_updated_at                        :datetime
#

class AnnualPropertyReviewPhoto < ApplicationRecord
  include AnnualPropertyReview::AnnualPropertyReviewPhotoBuilder
  belongs_to :annual_property_review
  belongs_to :annual_property_review_finding
  belongs_to :annual_property_review_capital_item
  
  validates :annual_property_review_id, presence: true
  validates :name, length: { maximum: 255 }
  validates :description, length: { maximum: 500 }

  has_attached_file :file,
    :path => "/apr:annual_property_review_id/images/:style/:filename",
    :source_file_options => { all: '-auto-orient' },
    :convert_options => { :all => "-quality 100" },
    :styles => {
      :thumb => ["75x75>",:jpg],
      :medium => ["350x350>",:jpg],
      :large => ["800x800>", :jpg]
    }
  
  validates_attachment :file, :content_type => {
    :content_type => [
      "image/jpeg",
      "image/jpg",
      "image/png",
      "image/gif",
      "image/bmp"]}
      
  # ".png", ".JPG", ".jpg", ".PNG", ".jpeg", ".bmp", ".JPEG"
  # Validate filename
  #validates_attachment_file_name :file, matches: [/png\z/i, /jpe?g\z/i, /jpg\z/i, /bmp\z/i, /gif\z/i], message: "%{value} is invalid"
  
  #validates :file_file_name, :file_unique => true
end
