# == Schema Information
#
# Table name: partners
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  phone                :string(255)
#  email                :string(255)
#  contact              :string(255)
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  code                 :string(255)
#  partner_type         :string(255)
#  website              :string(250)
#  affiliate_code       :string(50)
#  email_display_name   :string(50)
#  partner_key          :string(255)
#  account_id           :integer
#  completed_onboarding :boolean          default(FALSE)
#  logo_file_name       :string
#  logo_content_type    :string
#  logo_file_size       :integer
#  logo_updated_at      :datetime
#

require 'phony_rails'

class Partner < ApplicationRecord
  include Partner::PartnerBuilder
  include Kpi::Partners

  QUERY_BY_SEARCH = "CAST(partners.id as varchar(25)) LIKE :search OR
              LOWER(partners.name) LIKE :search OR
              LOWER(partners.code) LIKE :search OR
              LOWER(partners.partner_type) LIKE :search OR
              LOWER(partners.email) LIKE :search OR
              LOWER(partners.affiliate_code) LIKE :search OR
              LOWER(partners.email_display_name) LIKE :search OR
              LOWER(partners.partner_key) LIKE :search".freeze

  QUERY_BY_SEARCH_AND_TYPE = "partner_type = :partner_type AND
              CAST(partners.id as varchar(25)) LIKE :search OR
              LOWER(partners.contact) LIKE :search OR
              LOWER(partners.email)   LIKE :search".freeze

  CONFIGURATION_CLASS = "Partner::Configuration".freeze

  INSPECTOR = "inspector".freeze
  BROKER = "broker".freeze
  PROPERTY_MANAGER = "propmgr".freeze
  BUILDER = "builder".freeze
  RELOCATION = "reloco".freeze

  DEFAULT_COUNTRY = "US".freeze

  has_one :address, :dependent => :destroy
  belongs_to :contractor
  has_one :partner_configuration, :dependent => :destroy,
                                  class_name: CONFIGURATION_CLASS

  has_one :warranty_configuration, :dependent => :destroy

  has_many :tags, :as => :taggable, :dependent => :destroy
  has_many :coupons, :dependent => :destroy
  has_many :organization_partners, :dependent => :destroy
  has_many :organizations, :through => :organization_partners
  has_many :partner_users, :dependent => :destroy
  has_many :users, :through => :partner_users
  has_many :partner_binders, :dependent => :destroy
  has_many :binders, :through => :partner_binders
  has_many :dashboards, :dependent => :destroy
  has_many :partner_resources, :dependent => :destroy
  has_many :resources, :through => :partner_resources, :source => :marketing_resource
  has_many :widget_exclusions, :dependent => :destroy

  # homeowners
  has_many :partner_clients, :dependent => :destroy
  has_many :clients, :through => :partner_clients


  # warranty plans
  has_many :partner_warranty_plans, :dependent => :destroy
  has_many :warranty_plans, :through => :partner_warranty_plans

  # transactions
  has_many :partner_binder_transaction, :dependent => :destroy
  has_many :binder_transactions, :through => :partner_binder_transaction

  #has_many :organization_resource, :dependent => :destroy
  #has_many :resources, :through => :organization_resource

  has_one :account, :foreign_key => "manager_id", :dependent => :destroy
  has_one :api_key, :foreign_key => "partner_id", :dependent => :destroy

  accepts_nested_attributes_for :address
  accepts_nested_attributes_for :partner_configuration
  
  phony_normalize :phone, :default_country_code => 'US'

  validates_presence_of   :name, {:message => I18n.t(:err_value_required)}
  validates_presence_of   :email, {:message => I18n.t(:err_value_required)}
  validates_uniqueness_of :email, {:message => I18n.t(:err_value_unique)}
  validates_presence_of   :code, {:message => I18n.t(:err_value_required)}
  validates_uniqueness_of :code, {:message => I18n.t(:err_value_unique)}
  validates_uniqueness_of :partner_key, :allow_blank => true, :allow_nil => true
  validates_format_of     :partner_key, :with => /\A[a-z]+\z/, :message => "%{value} #{I18n.t(:err_is_invalid)}", :allow_blank => true, :allow_nil => true
  validates_length_of     :partner_key, :minimum => 4, :maximum => 30, :allow_blank => true, :allow_nil => true
  validates_presence_of   :partner_type, {:message => I18n.t(:err_value_required)}
  validate                :valid_partner_type

  # partner name is limited to 255 characters
  validates :name, length: { maximum: 255}
  # partner email_display_name is limited to 50 characters
  validates :email_display_name, length: { maximum: 50}
  # partner affiliate_code is limited to 50 characters
  validates :affiliate_code, length: { maximum: 50}

  validates :users, length: {minimum: 1, message: " - Minimum of 1 user per partner required"}, :on => :update, if: :old_account?
  
  before_validation :unique_partner_admin, :on => :update, if: :old_account?
  before_save { |p| p.contact = p.name if p.contact.blank?}
  
  def unique_partner_admin
    if PartnerUser.where(:partner_id => id, :role => "admin").count < 1
        errors.add(:partner_users, "Partner Admin is required")
    end
  end
  
  # make sure to downcase emails
  before_save { |p| p.email = p.email.downcase }
  
  # check if contractor object exists for partner and connect partner
  after_create        :find_exisiting_pro
  after_update        :update_roles_if_type_changed
  after_save          :update_default_locale
  
  # update marketing resources if name, email, website, phone, or logo are changed
  after_update_commit :update_marketing_resources,
    if: proc { |record| 
      (record.previous_changes.key?(:name) && record.previous_changes[:name].first != record.previous_changes[:name].last) ||
      (record.previous_changes.key?(:email) && record.previous_changes[:email].first != record.previous_changes[:email].last) ||
      (record.previous_changes.key?(:website) && record.previous_changes[:website].first != record.previous_changes[:website].last) ||
      (record.previous_changes.key?(:phone) && record.previous_changes[:phone].first != record.previous_changes[:phone].last) ||
      (record.previous_changes.key?(:logo_updated_at) && record.previous_changes[:logo_updated_at].first != record.previous_changes[:logo_updated_at].last)
    }

  has_attached_file :logo,
      :path => "/partner:id/logo/:style/:filename",
      :source_file_options => { all: '-auto-orient' },
      :convert_options => { :all => "-quality 100" },
      :styles => {
      :thumb => ["175x175>",:png],
      :original => ["350x350>",:png]
  }

  validates_attachment :logo, :content_type => {
      :content_type => [
          "image/jpeg",
          "image/jpg",
          "image/png",
          "image/gif",
          "image/bmp"
      ]
  }
  
  def update_default_locale
    Partners::UpdateDefaultLocaleJob.perform_async(id)
  end
  
  def find_exisiting_pro
    pro = Contractor.where(:email => email).first
    return if pro.nil?
    self.contractor_id = pro.id
    save!
  end

  def update_roles_if_type_changed
    Partners::UpdatePartnerTypeJob.perform_async(id, self.partner_type_was) if self.partner_type_changed?
  end
  
  def update_marketing_resources
    Partners::PdfGeneratorServiceJob.perform_async(self.id) if (!self.account.nil? && self.account.account_status === "active" && self.account.account_sub_type === "paid")
  end

  def old_account?
    created_at < Date.today
  end

  def valid_partner_type
    pt = self.partner_type.nil? ? nil : self.partner_type.downcase
    if ![INSPECTOR, BROKER, PROPERTY_MANAGER, BUILDER, RELOCATION, "lender", "homepro", "builder", "property_manager", "hoa", "insurance"].include? pt
      errors.add(:partner_type, I18n.t(:err_invalid_partner_type))
    end
  end

  def belongs_to_organization?
    return organizations.count > 0
  end

  def account
    return Account.find_by_id(self.account_id)
  end

  def get_admin
    partner_user = PartnerUser.where(:role => "admin", :partner_id => self.id).first
    return nil if partner_user.nil?
    return partner_user.user
  end

  def phone_formatted
    return nil if self.phone.nil?
    return nil if not Phony.plausible?(self.phone)
    return self.phone.phony_formatted(:format => :national, :spaces => '-')
  end

  def add_role(role, item, client_id)
    PartnerBinder.create!(:partner_id => self.id, :binder_id => item.id, :role => role.to_s, :client_id => client_id, :partner_user_id => partner_user_ids.first, :repair_pricer_enabled => self.partner_configuration.repair_pricer_pre_paid_reports)
  end
  
  settings do
    mappings dynamic: false do
      indexes :name,                type: :text
      indexes :phone,               type: :text
      indexes :email,               type: :text
      indexes :created_at,          type: :date
      indexes :contact,             type: :text
      indexes :partner_type,        type: :keyword
      indexes :website,             type: :text
      indexes :partner_key,         type: :text
      indexes :partner_binders do
        indexes :binder_id,       type: :integer
        indexes :dashboard_id,    type: :integer
        indexes :partner_id,      type: :integer
        indexes :created_at,      type: :date
      end
      indexes :partner_configuration do
        indexes :default_binder_action,                 type: :keyword
        indexes :automation_binder_action,              type: :boolean
        indexes :binder_action_delay_length,            type: :integer
        indexes :send_agents_transfer_notification,     type: :keyword
        indexes :show_isn_tab,                          type: :boolean
        indexes :default_user_branding_option,          type: :keyword
        indexes :allow_client_calls,                    type: :boolean
        indexes :active_warranty_account,               type: :boolean
        indexes :can_perform_apr,                       type: :boolean
        indexes :annual_property_review_cost_cents,     type: :integer
        indexes :annual_property_review_cost_currency,  type: :keyword
        indexes :default_transaction_type,              type: :keyword
        indexes :repair_pricer_enabled,                 type: :boolean
        indexes :repair_pricer_pre_paid_reports,        type: :boolean
        indexes :repair_pricer_messaging_enabled,       type: :boolean
      end
    end
  end
    
  def as_indexed_json(options={})
      self.as_json(
          {
              only: [:name, :phone, :email, :created_at, :contact, :partner_type, :website, :partner_key
              ],
              include: [:partner_binders, :partner_configuration]
          }
      )
  end

end
