class AbsFile < ApplicationRecord
    # relationships
    belongs_to :binder

    validates_presence_of :binder,  :message => I18n.t(:err_value_required)

    has_attached_file :payload,           :path => "/abs_file:id/payload/:filename"
    do_not_validate_attachment_file_type :payload

    enum status: {
        pending: 0,
        processing: 1,
        binder_created: 2
    }, _prefix: :status
end
