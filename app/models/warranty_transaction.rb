# == Schema Information
#
# Table name: warranty_transactions
#
#  id                    :integer          not null, primary key
#  binder_transaction_id :integer          not null
#  warranty_id           :integer          not null
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class WarrantyTransaction < ApplicationRecord
    
    # associations
    belongs_to :warranty
    belongs_to :binder_transaction, :class_name => "Binder::Transaction"
    
    # validations
    validates_presence_of   :warranty,               :message => I18n.t(:err_value_required)
    validates_presence_of   :binder_transaction_id,     :message => I18n.t(:err_value_required)
    
    validates_uniqueness_of :warranty_id, scope: :binder_transaction_id
    validates_uniqueness_of :binder_transaction_id, scope: :warranty_id
end
