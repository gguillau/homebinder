module SellerReport::SellerReportBuilder

    def self.included(klass)
        klass.extend ClassMethods
    end

    module ClassMethods
        
        def base_query
            "CAST(seller_reports.id as varchar(25)) LIKE :search OR
                      LOWER(seller_reports.code) LIKE :search OR
                      CAST(seller_reports.binder_id as varchar(25)) LIKE :search OR
                      LOWER(binders.name) LIKE :search OR
                      LOWER(CONCAT(properties.address1, ' ', properties.address2, ' ', properties.city, ', ', properties.state, ' ', properties.zip)) LIKE :search".freeze
        end
        
        def show(hb_request, params)
            # get the item
            item = find_by_code(params[:id])
            
            raise NotFoundException.new "#{self.name} with ID: #{params[:id]} not found" if item.nil?
            
            return item
        end

        def build(hb_request, params)
            binder = Binder.find(params[:seller_report][:binder_id])
            hb_request.ability.authorize! :write, binder, :message => "You are not authorized to access binder ID: #{binder.id}."
            super
        end

        def update(hb_request, params)
            seller_report = SellerReport.find(params[:id])

            # check if the user has permission to write in the binder
            binder = Binder.find(seller_report.binder_id)

            hb_request.ability.authorize! :write, binder, :message => "You are not authorized to access binder ID: #{binder.id}."

            # set the public flag and show_project_costs
            seller_report.public = params[:public]
            seller_report.show_project_costs = params[:show_project_costs]
            seller_report.save!

            binder.documents.each do |d|
                include = params[:documents] ? params[:documents].include?(d.id) : false
                if not d.seller_report_item
                    d.seller_report_item = SellerReportItem.new(include: include)
                else
                    d.seller_report_item.include = include
                end
                d.seller_report_item.save!
            end

            binder.images.each do |i|
                include = params[:images] ? params[:images].include?(i.id) : false
                if not i.seller_report_item
                    i.seller_report_item = SellerReportItem.new(include: include)
                else
                    i.seller_report_item.include = include
                end
                i.seller_report_item.save!
            end

            binder.appliances.each do |a|
                include = params[:appliances] ? params[:appliances].include?(a.id) : false
                if not a.seller_report_item
                    a.seller_report_item = SellerReportItem.new(include: include)
                else
                    a.seller_report_item.include = include
                end
                a.seller_report_item.save!
            end

            binder.maintenance_items.each do |m|
                include = params[:maintenance] ? params[:maintenance].include?(m.id) : false
                if not m.seller_report_item
                    m.seller_report_item = SellerReportItem.new(include: include)
                else
                    m.seller_report_item.include = include
                end
                m.seller_report_item.save!
            end

            binder.projects.each do |p|
                include = params[:improvements] ? params[:improvements].include?(p.id) : false
                if not p.seller_report_item
                    p.seller_report_item = SellerReportItem.new(include: include)
                else
                    p.seller_report_item.include = include
                end
                p.seller_report_item.save!
            end

            binder.binder_contractors.each do |c|
                include = params[:contractors] ? params[:contractors].include?(c.id) : false
                if not c.seller_report_item
                    c.seller_report_item = SellerReportItem.new(include: include)
                else
                    c.seller_report_item.include = include
                end
                c.seller_report_item.save!
            end

            binder.paints.each do |pa|
                include = params[:paints] ? params[:paints].include?(pa.id) : false
                if not pa.seller_report_item
                    pa.seller_report_item = SellerReportItem.new(include: include)
                else
                    pa.seller_report_item.include = include
                end
                pa.seller_report_item.save!
            end
            
            binder.permits.each do |pt|
                include = params[:permits] ? params[:permits].include?(pt.id) : false
                if not pt.seller_report_item
                    pt.seller_report_item = SellerReportItem.new(include: include)
                else
                    pt.seller_report_item.include = include
                end
                pt.seller_report_item.save!
            end

            binder.finishes.each do |f|
                include = params[:finishes] ? params[:finishes].include?(f.id) : false
                if not f.seller_report_item
                    f.seller_report_item = SellerReportItem.new(include: include)
                else
                    f.seller_report_item.include = include
                end
                f.seller_report_item.save!
            end
            
            EventService.perform_async({:event_type => "engagement", :event_name => "seller report updated", :user_id => hb_request.user.id, :user_role => hb_request.user.role, :user_created_at => hb_request.user.created_at, :user_create_method => hb_request.user.create_method})
            
            return seller_report
        end
    end
end
