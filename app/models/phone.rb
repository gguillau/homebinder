class Phone
    
    # we need the country, the number and two formatted numbers: international for display and
    # national for editing
    
    attr_reader :number
    attr_reader :country_code
    attr_reader :international
    attr_reader :national
  
    def initialize(phone, country_code)
        @number = phone
        @country_code = country_code
        
        normalize
    end
  
    def normalize
        @international = set_international
        @national = format_national
    end
  
    def format_national
        return "" if @number.nil?
        number = @number.phony_formatted(:format => :national, :spaces => ' ')
        case @country_code
            when "GB"
                number.slice!("0")
                return number
            else
                return number
        end
    end
    
    def set_international
        return "" if @number.nil?
        return @number.phony_formatted(:format => :international, :spaces => ' ')
    end

end