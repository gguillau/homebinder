class RepairPricerReportFinding < ApplicationRecord
    
    # relationships
    belongs_to :repair_pricer_report
    
    has_many :repair_pricer_report_photos,    :dependent => :destroy
    
    validates_presence_of :name, :message => I18n.t(:err_value_required)
    
    # one unique finding per report
    validates_uniqueness_of :name, scope: :repair_pricer_report
end
