class RepairPricerReport < ApplicationRecord
    include RepairPricerReport::Actions
    
    # relationships
    belongs_to :binder
    belongs_to :partner
    belongs_to :creator, :class_name => "User", :foreign_key => "creator_id"
    
    has_one :address,    :dependent => :destroy
    
    has_many :repair_pricer_report_findings,    :dependent => :destroy
    
    validates_presence_of :status, :message => I18n.t(:err_value_required)
    validates_presence_of :address, :message => I18n.t(:err_value_required), :on => :update
    
    has_attached_file :repair_report,           :path => "/repair_repair_pricer:id/repair_report/:filename"
    has_attached_file :inspection_report,       :path => "/repair_repair_pricer:id/inspection_report/:filename"
    has_attached_file :pool_report,             :path => "/repair_repair_pricer:id/pool_report/:filename"
    has_attached_file :home_history_report,     :path => "/repair_repair_pricer:id/home_history_report/:filename"
    
    enum status: {
        created: 0,
        submitted: 1,
        accepted: 2,
        in_progress: 3,
        on_hold: 4,
        ready: 5,
        downloaded: 6,
        rejected: 7,
        cancelled: 8,
    }, _prefix: :status

    enum payment_status: {
        not_applicable: 0,
        not_paid: 1,
        paid: 2,
        refunded: 3,
        payment_failed: 4
    }, _prefix: :payment_status
    
    validates_attachment :repair_report, :content_type => {
        :content_type => [
            "application/pdf"
            ]
    }
    
    do_not_validate_attachment_file_type :inspection_report
    
    # validates_attachment :inspection_report, :content_type => {
    #     :content_type => [
    #         "application/pdf",
    #         "application/mspowerpoint",
    #         "application/powerpoint",
    #         "application/vnd.ms-powerpoint",
    #         "application/x-mspowerpoint",
    #         "application/vnd.openxmlformats-officedocument.presentationml.presentation",
    #         "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
    #         "application/msword",
    #         "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
    #         "application/excel",
    #         "application/vnd.ms-excel",
    #         "application/x-excel",
    #         "application/x-msexcel",
    #         "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
    #         "text/plain"
    #     ]
    # }
    
    validates_attachment :pool_report, :content_type => {
        :content_type => [
            "application/pdf",
            "application/mspowerpoint",
            "application/powerpoint",
            "application/vnd.ms-powerpoint",
            "application/x-mspowerpoint",
            "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
            "application/msword",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "application/excel",
            "application/vnd.ms-excel",
            "application/x-excel",
            "application/x-msexcel",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "text/plain"
        ]
    }
    
    validates_attachment :home_history_report, :content_type => {
        :content_type => [
            "application/pdf",
            "application/mspowerpoint",
            "application/powerpoint",
            "application/vnd.ms-powerpoint",
            "application/x-mspowerpoint",
            "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
            "application/msword",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "application/excel",
            "application/vnd.ms-excel",
            "application/x-excel",
            "application/x-msexcel",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "text/plain"
        ]
    }
    
    def full_address
        return nil if address.nil?
        return address.full_address
    end
    
    def time_left
        left = (created_at + 24.hours) - Time.now
        if left.positive?
            return Time.at(left).utc.strftime("%H:%M:%S")
        else
            return "Expired"
        end
    end
    
    settings do
        mappings dynamic: false do
            indexes :report_id,                     type: :text
            indexes :status,                        type: :keyword
            indexes :binder_id,                     type: :text
            indexes :partner_id,                    type: :text

            indexes :client_first,                  type: :text
            indexes :client_last,                   type: :text
            indexes :client_email,                  type: :text
            indexes :client_phone,                  type: :text
            
            indexes :buyer_agent_first,             type: :text
            indexes :buyer_agent_last,              type: :text
            indexes :buyer_agent_email,             type: :text
            
            indexes :payment_status,                type: :keyword
            indexes :order_pool_report,             type: :boolean
            indexes :order_home_history_report,     type: :boolean
            indexes :rush_report,                   type: :boolean
            indexes :created_at,                    type: :date
            indexes :updated_at,                    type: :date
            indexes :delivery_date,                 type: :date
            indexes :full_address,                  type: :text
            
            indexes :partner do
                indexes :name,                type: :text
                indexes :phone,               type: :text
                indexes :email,               type: :text
                indexes :created_at,          type: :date
                indexes :contact,             type: :text
                indexes :partner_type,        type: :keyword
                indexes :website,             type: :text
                indexes :partner_key,         type: :text
                indexes :partner_configuration do
                    indexes :default_binder_action,                 type: :keyword
                    indexes :automation_binder_action,              type: :boolean
                    indexes :binder_action_delay_length,            type: :integer
                    indexes :send_agents_transfer_notification,     type: :keyword
                    indexes :show_isn_tab,                          type: :boolean
                    indexes :default_user_branding_option,          type: :keyword
                    indexes :allow_client_calls,                    type: :boolean
                    indexes :active_warranty_account,               type: :boolean
                    indexes :repair_pricer_enabled,                 type: :boolean
                    indexes :repair_pricer_pre_paid_reports,        type: :boolean
                    indexes :repair_pricer_messaging_enabled,       type: :boolean
                end
            end
            
            indexes :binder do
                indexes :name,              type: :text
                indexes :created_at,        type: :date
                indexes :create_method,     type: :keyword
                indexes :status,            type: :keyword
                indexes :details,           type: :text
            end
            
            indexes :creator do
                indexes :email,                 type: :text
                indexes :role,                  type: :keyword
                indexes :user_profile do
                    indexes :first_name,        type: :text
                    indexes :last_name,         type: :text
                    indexes :mobile_phone,      type: :text
                end
            end
        end
    end
    
    def as_indexed_json(options={})
        self.as_json(
            {
                only: [:report_id, :status, :binder_id, :partner_id, :client_first, :client_last, :client_email, :client_phone, :buyer_agent_first, :buyer_agent_last, :buyer_agent_email,
                :payment_status, :order_pool_report, :order_home_history_report, :rush_report, :created_at, :updated_at, :delivery_date],
                include: {
                    :binder => {
                        :only => [:name, :created_at, :create_method, :status, :details]
                    },
                    :partner => {
                        :only => [:name, :phone, :email, :created_at, :contact, :partner_type, :website, :partner_key], 
                        :include => [
                            :partner_configuration => {
                                :only => [:default_binder_action, :automation_binder_action, :binder_action_delay_length, :binder_action_delay_length,
                                :send_agents_transfer_notification, :show_isn_tab, :default_user_branding_option, :allow_client_calls,
                                :repair_pricer_enabled, :repair_pricer_pre_paid_reports, :repair_pricer_messaging_enabled]
                            }
                        ]
                    },
                    :creator => {
                        :only => [:email, :role], 
                        :include => [
                            :user_profile => {
                                :only => [:first_name, :last_name, :mobile_phone]
                            }
                        ]
                    }
                },
                methods: [:full_address]
            }
        )
    end
    
    
end
