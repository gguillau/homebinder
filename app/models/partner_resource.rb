# == Schema Information
#
# Table name: partner_resources
#
#  id                    :integer          not null, primary key
#  marketing_resource_id :integer
#  partner_id            :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class PartnerResource < ApplicationRecord
  
    # associations
    belongs_to :marketing_resource
    belongs_to :partner
    
    # validations
    validates_presence_of   :partner,    :message => I18n.t(:err_value_required)
    validates_presence_of   :marketing_resource,       :message => I18n.t(:err_value_required)

    # one unique marketing_resource per partner
    validates_uniqueness_of :marketing_resource_id, scope: :partner_id, :message => "already exists for partner"
end
