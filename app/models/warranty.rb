# == Schema Information
#
# Table name: warranties
#
#  id               :integer          not null, primary key
#  client_first     :string(50)
#  client_last      :string(50)
#  client_email     :string(100)
#  client_phone     :string(20)
#  status           :string(20)
#  expiration_date  :datetime
#  binder_id        :integer          not null
#  warranty_plan_id :integer          not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Warranty < ApplicationRecord
    include Warranty::Actions
    
    # associations
    belongs_to :warranty_plan
    belongs_to :warranty_company
    belongs_to :binder
    
    has_one :warranty_transaction, :dependent => :destroy
    has_one :binder_transaction, :through => :warranty_transaction
    has_one :partner_binder_transaction, :through => :binder_transaction
    has_one :user_binder_transaction, :through => :binder_transaction
    has_one :address, :dependent => :destroy
    
    # validations
    validates_presence_of   :status,  :message => I18n.t(:err_value_required)
    validates_length_of     :status,  :maximum => 20
    
    validates_presence_of   :warranty_plan_id,  :message => I18n.t(:err_value_required)

    validates_presence_of   :binder_id,  :message => I18n.t(:err_value_required)
    
    validates_presence_of   :client_first,          :message => I18n.t(:err_value_required)
    validates_length_of     :client_first,          :maximum => 50
    validates_presence_of   :client_last,           :message => I18n.t(:err_value_required)
    validates_length_of     :client_last,           :maximum => 50
    validates_presence_of   :client_email,          :message => I18n.t(:err_value_required)
    validates_length_of     :client_email,          :maximum => 100
    
    # only 1 plan per binder
    validates_uniqueness_of :binder_id, :scope => [:warranty_plan_id], :message => "Error - A warranty for this binder already exists"
    
    accepts_nested_attributes_for :address
    
    def warranty_company
        warranty_plan&.warranty_company
    end
    
    def partner_id
        binder_transaction&.partner_binder_transaction&.partner.id
    end
    
    def client_phone_formatted
        return "(222)-222-2222" if self.client_phone.nil?
        return "(222)-222-2222" if not Phony.plausible?(self.client_phone)
        return self.client_phone.phony_formatted(:format => :national, :spaces => '-')
    end
    
    settings do
        mappings dynamic: false do
            indexes :warranty_plan_id,              type: :text
            indexes :partner_id,                    type: :text
            indexes :created_at,                    type: :date
        end
    end
    
    def as_indexed_json(options={})
        self.as_json(
            {
                only: [:warranty_plan_id, :created_at],
                methods: [:partner_id]
            }
        )
    end
    
    
    
end
