require 'securerandom'

module User::UserActions

    TO = "to".freeze
    USER = "user".freeze
    SHARE = "share".freeze
    CLIENT = "client".freeze
    TRANSFER = "transfer".freeze
    PARTNER = "partner".freeze

    def self.included(klass)
        klass.extend ClassMethods
    end

    def init_binders
        Users::InitBindersJob.perform_async(id)
    end

    def waiting_binders
        # get any binder with a pending share or transfer
        waiting = []
        user_profile = nil

        if create_method.nil? || create_method === "transfer" || create_method === "sell_side_inspection"

            transferred_binders = binders.joins(:transfers).where(:transfers => {:receiver_id => id}).where.not(:transfers => {:status => "accepted"})

            transferred_binders.each do |b|
                # get the last transfer
                transfer = b.transfers.order(:id).last

                # make sure we have a sender available
                next if transfer.sender.nil?
                next if transfer.sender.user_profile.nil?

                # get the partner info
                user_profile = transfer.sender.user_profile.branding

                # get some statistics to display to the homeowner
                maintenance_count = Binder::MaintenanceItem.where(:binder_id => b.id).count
                home_pros_count = Binder::BinderContractor.where(:binder_id => b.id).count
                documents_count = Binder::Document.where(:binder_id => b.id).count
                
                # get repair pricer access
                access_repair_pricer = UserBinder.where(:user_id => id, :binder_id => b.id, :access_repair_pricer => true).exists?

                # add the binder
                waiting.push({:id => b.id, :property => b.property, :access_token => transfer.access_token, :maintenance_count => maintenance_count, :home_pros_count => home_pros_count, :documents_count => documents_count, :access_repair_pricer => access_repair_pricer})
            end

            # update any transfer copies
            Binder::Transfer.where(:receiver_id => id, :transfer_type => "copy").where.not(:status => "accepted").update_all(:status => "accepted")
        else
            shared_binders = binders.joins(:shares).where(:shares => {:shared_with_id => id}).where.not(:shares => {:status => "accepted"})

            shared_binders.each do |b|
                # get the last share
                share = b.shares.order(:id).last
                
                # make sure we have a sender available
                next if share.sender.nil?
                next if share.sender.user_profile.nil?
                
                # get the branding info
                user_profile = share.sender.user_profile.branding
                
                # get some statistics to display to the homeowner
                maintenance_count = Binder::MaintenanceItem.where(:binder_id => b.id).count
                home_pros_count = Binder::BinderContractor.where(:binder_id => b.id).count
                documents_count = Binder::Document.where(:binder_id => b.id).count
                
                # add the binder
                waiting.push({:id => b.id, :property => b.property, :access_token => share.access_token, :maintenance_count => maintenance_count, :home_pros_count => home_pros_count, :documents_count => documents_count})
            end
        end

        return {:binders => waiting, :branding => user_profile}
    end

    def send_reset_password_instructions
        raise BadRequestException.new "Your password has already been reset. Please check your email for a link to update your password." if self.reset_password_token.present?
        token = SecureRandom.urlsafe_base64(nil, false)
        self.reset_password_token = BCrypt::Password.create(token, :cost => 11)
        self.reset_password_sent_at = Time.now
        self.save!
        UserMailer.send_reset_password_instructions(id, token).deliver_later
        return token
    end

    def valid_reset_password_token?(token)
        return false if token.blank?
        return false if reset_password_sent_at.blank?
        return false if Time.now - reset_password_sent_at > Rails.configuration.password_reset_time_frame.hours
        return BCrypt::Password.new(reset_password_token) == token
    end

    def reset_password_by_token(user)
        self.password = user[:password]
        self.password_confirmation = user[:password_confirmation]
        self.reset_password_token = nil
        self.reset_password_sent_at = nil
        return self
    end

    def invite!(user_id = nil)
        regenerate_access_token
        UserMailer.send_invitation(id, user_id).deliver_later
    end

    def remove
        Users::RemoveJob.perform_async(id)
    end

    def send_failed_payment_notification
        AccountMailer.send_failed_payment_notification(id).deliver_later
    end

    module ClassMethods

        USER_SEARCH = "CAST(users.id as varchar(25)) LIKE :search OR
                   LOWER(users.email) LIKE :search OR
                   LOWER(user_profiles.first_name) LIKE :search OR
                   LOWER(user_profiles.last_name) LIKE :search OR
                   LOWER(CONCAT(user_profiles.first_name, ' ', user_profiles.last_name)) LIKE :search".freeze

        TO = "to".freeze
        SHARE = "share".freeze
        TRANSFER = "transfer".freeze
        REGISTER = "register".freeze
        USER_REQUIRED = "User is required".freeze
        EMAIL_REQUIRED = "Email is required".freeze
        PASSWORD_REQUIRED = "Password is required".freeze
        USER_REGISTERD = "User already registered".freeze
        USER_NOT_FOUND = "User not found".freeze
        EMAIL_AND_PASSWORD_REQUIRED = "Email and password are required".freeze
        INVALID_EMAIL = "Invalid email".freeze
        INVALID_PASSWORD = "Invalid password".freeze
        BLANK_PASSWORD = "New Password cannot be blank".freeze
        INVALID_CREDENTIALS = "Invalid credentials".freeze
        RESET_ERROR = "Password Reset Failed.".freeze

        def build(hb_request, params)
            item = nil
            # check if we are adding a user for a partner
            # ensure they don't already exist

            if params[:partnerId]
                # get the user
                item = User.find_by_email(params[:user][:email].downcase)
            end

            if item.nil?
                params[symbol][:password] = SecureRandom.hex(5)
                # create the new item
                item = self.new(params[symbol].permit!)

                # save the item
                if not item.save
                    raise UnprocessableException.new(item)
                end
            end

            if params[:partnerId]
                # add the user to the partner
                partner = Partner.find(params[:partnerId])
                if item.role != params[symbol][:role]
                    raise BadRequestException.new "Validation failed: Incorrect Partner User role"
                else
                    PartnerService.new(hb_request.user, partner).add_user(item, params[:role])
                end
            end

            # call analytics
            Users::UserAnalytics.delay(:queue => "analytics").created(item.id)

            return item
        end

        def update(hb_request, params)
            user = super

            # update the analytics
            Users::UserAnalytics.delay(:queue => "analytics").update_user(user.id)

            return user
        end

        def register(params)
            # verify a user hash was provided
            raise BadRequestException.new USER_REQUIRED if params[:user].nil?
            # verify email was provided
            raise BadRequestException.new EMAIL_REQUIRED if params[:user][:email].nil?

            # check if the user already exists
            existing = User.where(:email => params[:user][:email].downcase).first
            if existing
                return complete(params, existing)
            else
                # verify password was provided
                raise BadRequestException.new PASSWORD_REQUIRED if not params[:user][:password]

                # if create method isn't provided we assume it was a standard registration
                if not params[:user][:create_method]
                    params[:user][:create_method] = REGISTER
                end

                # make sure the email is lower case
                email = params[:user][:email].downcase
                pwd = params[:user][:password]
                create_method = params[:user][:create_method]
                role = params[:user][:role] || UserGlobalRoles::HOMEOWNER
                created_by = params[:user][:created_by]
                user_profile_attributes = params[:user][:user_profile_attributes].present? ? params[:user][:user_profile_attributes].permit! : {}
                user_binders_attributes = params[:user][:user_binders_attributes].present? ? params[:user][:user_binders_attributes] : []

                # set the parameters
                # we are always settings homeowner as the role here. when a partner is registered
                # the partner service is updating the role to reflect the partner
                #user = ActionController::Parameters.new({})

                # create the new user
                user = User.new(email: email, password: pwd, create_method: create_method, role: role, user_profile_attributes: user_profile_attributes, user_binders_attributes: user_binders_attributes, created_by: created_by)

                if params[:user][:create_method] == REGISTER
                    user.sign_in_count += 1
                    user.last_sign_in_at = DateTime.now
                end

                # save user
                if not user.save
                    raise UnprocessableException.new user
                end

                if user.create_method != REGISTER
                    # don't send monthly email to users who registered by transfer/share/inspector
                    user.user_profile.monthly_email = false
                    if not user.user_profile.save
                        ErrorService.perform_async(user.user_profile.errors.full_messages.first, {:task => "register"}, user&.id)
                    end
                end

                # call analytics
                Users::UserAnalytics.delay(:queue => "analytics").created(user.id)

                return user
            end
        end

        def register_for_transfer(params)
            params[:create_method] = TRANSFER
            params[:password] = SecureRandom.hex(5)
            return register(ActionController::Parameters.new({ :user => params }))
        end

        def register_for_share(params)
            params[:create_method] = SHARE
            params[:password] = SecureRandom.hex(5)
            return register(ActionController::Parameters.new({ :user => params }))
        end

        def opt_out(id)
            # find the user opting out
            user = User.joins(:transfers_received).where(:transfers => {:access_token => id}).first || User.joins(:shares_received).where(:shares => {:access_token => id}).first
            return if user.nil?

            # get all the user_binders the user has access to
            user.user_binders.each do |user_binder|
                binder = user_binder.binder
                next if binder.nil?
                if user_binder.role == UserBinderRoles::OWNER.to_s
                    # check for a pending transfer
                    if binder.transfers.where(:receiver_id => user.id)
                        .where.not(:status => "created")
                        .where.not(:status => "declined")
                        .where.not(:status => "accepted").count > 0
                        
                        # analytics
                        EventService.perform_async({:event_name => "declined", :event_type => "transfer", :user_id => user.id, :user_created_at => user.created_at, :user_role => user.role})

                        # update transfers where status is created, accepted or declined
                        binder.transfers.where(:receiver_id => user.id)
                            .where.not(:status => "created")
                            .where.not(:status => "declined")
                            .where.not(:status => "accepted")
                            .update_all(:status => "declined")

                        # delete the binder from the user
                        user_binder.destroy
                    end
                else
                    # check for a pending share
                    if binder.shares.where(:shared_with_id => user.id)
                        .where.not(:status => "created")
                        .where.not(:status => "declined")
                        .where.not(:status => "accepted").count > 0
                        
                        # analytics
                        EventService.perform_async({:event_name => "declined", :event_type => "share", :user_id => user.id, :user_created_at => user.created_at, :user_role => user.role})

                        # update shares where status is created, accepted or declined
                        binder.shares.where(:shared_with_id => user.id)
                            .where.not(:status => "created")
                            .where.not(:status => "declined")
                            .where.not(:status => "accepted")
                            .update_all(:status => "declined")

                        # delete the binder from the user
                        user_binder.destroy
                    end
                end

                # check for an orphaned binder
                if binder.owner.nil? and binder.user_binders.where(:role => "co_owner").count < 1
                    binder.orphan
                end
            end

            # if the user has no binders delete them
            if user.binders.count == 0
                user.remove
            end
        end

        def binders_waiting_by_email(email)
            # find the user
            user = User.where(:email => email).first
            return [] if user.nil?
            # get the waiting binders
            return user.waiting_binders
        end

        def binders_waiting_by_id(id)
            # find the user
            user = User.where(:id => id).first
            return [] if user.nil?
            # ge the waiting binders
            return user.waiting_binders
        end

        def signin(email, password)
            # check for an email and password
            raise BadRequestException.new EMAIL_AND_PASSWORD_REQUIRED unless email.present? and password.present?

            # find the user
            user = User.where(:email => email.downcase).first
            raise BadRequestException.new INVALID_CREDENTIALS if user.nil?

            # verify the password
            if not user.valid_password?(password)
                raise BadRequestException.new INVALID_CREDENTIALS
            end

            # check if first time logging in and user was created through transfer/share
            if user.sign_in_count === 0 and user.create_method != REGISTER
                user.user_profile.monthly_email = true
                if not user.user_profile.save
                    ErrorService.perform_async(user.user_profile.errors.full_messages.first, {:task => "signin"}, user&.id)
                end
            end

            # auditing
            user.sign_in_count += 1
            user.last_sign_in_at = DateTime.now
            user.reset_password_token = nil
            user.reset_password_sent_at = nil
            user.save!

            # update any pending transfers and shares
            if user.role === "homeowner"
                user.init_binders
            else
                user.init_binders
            end

            # call analytics
            Users::UserAnalytics.delay(:queue => "analytics").logged_in(user.id)

            return user
        end

        def change_email(current_user, params)
            # verify a user hash was provided
            raise BadRequestException.new USER_REQUIRED if params[:user].nil?

            # find the user
            user = User.find(params[:id])

            # check that user can change the email
            ability = Ability.new(current_user)
            ability.authorize! :write, user, :message => "You are not authorized to access user ID: #{user.id}."

            # verify the password
            if not user.valid_password?(params[:password])
                raise BadRequestException.new INVALID_PASSWORD
            end

            # verify the email
            if not user.email.downcase == params[:email].downcase
                raise BadRequestException.new INVALID_EMAIL
            end

            if not user.update_attributes(params[:user].permit!)
                raise UnprocessableException.new user
            end

            return user
        end

        def change_password(current_user, params)
            # verify a user hash was provided
            raise BadRequestException.new USER_REQUIRED if params[:user].nil?

            # find the user
            user = User.find_by_id(params[:id])
            raise BadRequestException.new USER_NOT_FOUND if user.nil?

            # check that user can change the password
            current_user.ability.authorize! :write, user, :message => "You are not authorized to access user ID: #{user.id}."

            # verify the password
            if not user.valid_password?(params[:password])
                raise BadRequestException.new INVALID_PASSWORD
            end
            
            if params[:user][:password].blank?
                raise BadRequestException.new BLANK_PASSWORD
            end

            if not user.update_attributes(params[:user].permit!)
                raise UnprocessableException.new user
            end

            return user
        end
        
        def confirm_password(current_user, params)

            # verify a user hash was provided
            raise BadRequestException.new "User ID required" if params[:id].nil?
            
            # find the user
            user = User.find_by_id(params[:id])
            raise BadRequestException.new USER_NOT_FOUND if user.nil?

            # confirm token is valid
            if not user.valid_reset_password_token?(params[:token])
                # raise an exception if invalid token
                raise BadRequestException.new "Invalid Password Reset Token"
            end

            return user
        end

        def reset_password(params)
            raise BadRequestException.new RESET_ERROR unless params[:user].present?
            raise BadRequestException.new RESET_ERROR unless params[:user][:email].present?
            # find the user
            user = User.find_by_email(params[:user][:email].downcase)
            # verify the user exists
            raise BadRequestException.new RESET_ERROR unless not user.nil?
            # reset the password
            user.send_reset_password_instructions
        end

        def update_password(params)
            raise BadRequestException.new RESET_ERROR unless params[:user].present?
            raise BadRequestException.new RESET_ERROR unless params[:user][:email].present?

            # verify user exists and that the token and email are for the same user
            user = User.find_by_email(params[:user][:email].downcase)
            # raise an exception if user doesn't exist or if incorrect token
            raise BadRequestException.new INVALID_EMAIL unless not user.nil?

            # confirm token is valid
            if user.valid_reset_password_token?(params[:user][:reset_password_token])
                # reset the password
                user = user.reset_password_by_token(params[:user])
                # check if there are errors
                if not user.save
                    # raise an exception
                    raise UnprocessableException.new(user)
                end
            else
                # raise an exception if IDs don't match
                raise BadRequestException.new "Invalid Password Reset Token"
            end
        end

        # find a user by email

        def find_user_by_email(current_user, params)
            return nil if params[:email].nil?

            # get the user
            user = User.find_by_email(params[:email].downcase)

            # check if user exists
            if user.nil?
                return
            end

            # return the user
            return user
        end

        # user accepts transfer/share

        def welcome(params)
            # verify a token was provided
            raise BadRequestException.new "Access token required" if params[:id].nil?

            # rails query User.where.or.where cannot use includes so I'm using this
            # for now
            user = User.includes(:transfers_received)
                .where(:transfers => {:access_token => params[:id]})
                .first ||
                   User.includes(:shares_received)
                       .where(:shares => {:access_token => params[:id]})
                       .first

            if not user.nil?
                Binder::Transfer.where(:access_token => params[:id]).update_all(:acceptance_method => params[:welcomeMethod])
                return user
            elsif User.where(:access_token => (params[:id])).count > 0
                return User.where(:access_token => (params[:id])).first
            else
                raise BadRequestException.new "User account does not exist"
            end
        end

        # completes onboarding for new homeowner

        def complete(params, existing)
            # if the user exists because they registered themselves raise an error
            raise BadRequestException.new USER_REGISTERD if existing.create_method == REGISTER

            # the user was auto generated. determine if they completed the process. If logon count is > 0 then
            # the user finished the process
            raise BadRequestException.new USER_REGISTERD if existing.sign_in_count > 0

            # update the user with the new password
            existing.sign_in_count += 1
            existing.accepted_transfer_at = DateTime.now
            existing.last_sign_in_at = DateTime.now

            user = params[:user].except(:first_name, :last_name)

            if not existing.update_attributes(user.permit!)
                raise UnprocessableException.new existing
            end

            # update any transfer/shares
            existing.init_binders

            # call analytics
            Users::UserAnalytics.delay(:queue => "analytics").logged_in(existing.id)

            return existing
        end

        def invite(hb_request, params)
            raise BadRequestException.new "User ID required" unless params[:id].present?
            # find the user
            user = User.find(params[:id])

            hb_request.ability.authorize! :read, user, :message => "You are not authorized to access user ID: #{user.id}."

            # send an invitation
            user.invite!(hb_request.user.id)
        end

        def destroy(hb_request, id)
            user = User.where(:id => id).first
            return unless user.present?

            hb_request.ability.authorize! :destroy, user, :message => "You are not authorized to delete user ID: #{user.id}."

            user.remove
        end

        def pager(hb_request, params = {}, search_method = :for_admin)
            # comment out for now
            #return elasticsearch(hb_request, params) if params[:search]
            
            set_defaults

            raise Exception.new unless default_order_by.present?
            raise Exception.new unless default_order.present?

            # set the parameters
            item_page = params[:page].present? ? params[:page].to_i : page
            item_count = params[:count].present? ? params[:count].to_i : count
            offset = (item_page - 1) * item_count
            orderBy = params[:orderBy].present? ? params[:orderBy] : default_order_by
            order = params[:order].present? ? params[:order] : default_order
            search_method = params[:searchMethod] ? params[:searchMethod] : :for_admin

            # get the search method
            search_method_obj = self.method(search_method)

            # search
            total = search_method_obj.call(params, hb_request).count
            items = search_method_obj.call(params, hb_request)
                              .order("#{orderBy} #{order}")
                              .limit(item_count)
                              .offset(offset)

            # return the page
            return Page.new(items, total)
        end

        def for_admin(params = {}, hb_request)
            hb_request.ability.authorize! :read, User, :message => "You are not authorized to access all users."

            # get the search value
            searchValue = params[:search]
            # set the includes/joins arrays
            includes = []
            joins = [:user_profile]

            # the custom args where/where_not clauses
            where = {}
            where_not = {}

            # check if any custom args were added
            add_custom_args(where, where_not, params, includes, joins)

            # search
            if searchValue.present?
                self.joins(joins)
                    .includes(includes)
                    .where(USER_SEARCH, search: "%#{searchValue}%".downcase)
                    .where(where)
                    .where.not(where_not)
                    .references(includes)
            else
                self.joins(joins)
                    .includes(includes)
                    .where(where)
                    .where.not(where_not)
                    .all
            end
        end

        def for_partner(params = {}, hb_request)
            parameters = params.clone

            raise Exception.new unless parameters[:partnerId].present?

            # get the partner
            partner = Partner.find(parameters[:partnerId])
            hb_request.ability.authorize! :read, partner, :message => "You are not authorized to access partner ID: #{partner.id}."

            # get the search value
            searchValue = parameters[:search]
            # set the includes/joins arrays
            includes = []
            joins = [:user_profile]

            # the custom args where/where_not clauses
            where = {}
            where_not = {}

            # inspectors now can have 2 users roles so we want to search by those roles
            if partner.partner_type === "inspector" && parameters[:role] === "inspector"
                parameters[:role] = nil
                second_where = "users.role = 'inspector' OR users.role = 'apr_reviewer'"
            end

            # check if any custom args were added
            add_custom_args(where, where_not, parameters, includes, joins)

            if searchValue.present?
                partner.users.joins(joins)
                    .includes(includes)
                    .where(USER_SEARCH, search: "%#{searchValue}%".downcase)
                    .where(where)
                    .where(second_where)
                    .references(includes)
            else
                partner.users.joins(joins)
                    .includes(includes)
                    .where(where)
                    .where(second_where)
                    .where.not(where_not)
                    .all
            end
        end

        def for_organization(params = {}, hb_request)
            raise Exception.new unless params[:organizationId].present?

            # get the org
            org = Organization.find(params[:organizationId])
            hb_request.ability.authorize! :read, org, :message => "You are not authorized to access organization ID: #{org.id}."

            # get the search value
            searchValue = params[:search]
            # set the includes/joins arrays
            includes = []
            joins = [:user_profile]

            # the custom args where/where_not clauses
            where = {}
            where_not = {}

            # check if any custom args were added
            add_custom_args(where, where_not, params, includes, joins)

            # search
            if searchValue.present?
                org.users.joins(joins).includes(includes).where(USER_SEARCH, search: "%#{searchValue}%".downcase).references(includes)
            else
                org.users.joins(joins).includes(includes).where(where)
            end
        end

        def add_custom_args(where, where_not, params, includes, joins)
            address_hash = {}
            if params[:role]
                where.merge!({:role => params[:role]})
            end

            if params[:partnerId]
                joins.push(:partners)
                where.merge!({:partners => {:id => params[:partnerId].to_i}})
            end

            if params[:create_method]
                where.merge!({:create_method => params[:create_method]})
            end

            if params[:state]
                joins.push(:user_profile => :address)
                address_hash.merge!({:state => params[:state]})
                where.merge!({:addresses => address_hash})
            end

            if params[:country]
                joins.push(:user_profile => :address)
                address_hash.merge!({:country => params[:country]})
                where.merge!({:addresses => address_hash})
            end
        end
    end

end
