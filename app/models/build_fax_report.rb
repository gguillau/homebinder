class BuildFaxReport < ApplicationRecord
    include BuildFaxReport::Actions
    
    # relationships
    belongs_to :partner
    belongs_to :binder
    
    has_one :address,    :dependent => :destroy
    
    validates_presence_of :status, :message => I18n.t(:err_value_required)
    validates_presence_of :report, :message => I18n.t(:err_value_required)
    validates_presence_of :address, :message => I18n.t(:err_value_required)
    
    accepts_nested_attributes_for :address
    
    has_attached_file :report, :path => "/partner:partner_id/build_fax_reports/:filename"
    
    validates_attachment :report, :content_type => {
        :content_type => [
            "application/pdf",
            "application/mspowerpoint",
            "application/powerpoint",
            "application/vnd.ms-powerpoint",
            "application/x-mspowerpoint",
            "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
            "application/msword",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "application/excel",
            "application/vnd.ms-excel",
            "application/x-excel",
            "application/x-msexcel",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "text/plain"
            ]
    }
end
