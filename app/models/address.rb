# == Schema Information
#
# Table name: addresses
#
#  id               :integer          not null, primary key
#  addressable_id   :integer
#  addressable_type :string(255)
#  name             :string(255)
#  country          :string(255)
#  address1         :string(255)
#  address2         :string(255)
#  city             :string(255)
#  state            :string(255)
#  zip              :string(255)
#  lat              :string(255)
#  long             :string(255)
#

#require 'carmen'
#include Carmen

class Address < ApplicationRecord

  validates_length_of       :country, :maximum => 10
  validates_length_of       :address1, :maximum => 50
  validates_length_of       :address2, :maximum => 50
  validates_length_of       :city, :maximum => 50
  validates_length_of       :state, :maximum => 10
  
  # associations
  belongs_to :user_profile
  belongs_to :build_fax_report
  belongs_to :partner
  belongs_to :warranty
  belongs_to :contractor
  belongs_to :partner_contractor,   :class_name => "PartnerContractor"
  belongs_to :contractor_template,  :class_name => "Template::Contractor"
  belongs_to :repair_pricer_report
  
  geocoded_by :full_address, :latitude  => :lat, :longitude => :long
  reverse_geocoded_by :lat, :long do |obj,results|
    if geo = results.first
          obj.zip = geo.postal_code
    end
  end
  before_validation :verify_state_and_country
  before_save { |a| a.country = a.country.upcase if a.country.present?}
  
  before_save :set_valid_address, :if => Proc.new { ENV["ENABLE_GEOCODING"] === "true" }
  
  def set_valid_address
    return if address1.nil?
    return if city.nil?
    return if state.nil?
    return if lat.present?
    return if long.present?

    geocode
    reverse_geocode
  end

  def verify_state_and_country
    # check if country exists in Carmen
    ctry = self.country
    if ctry.present?
      ctry = Country.find_by_value(ctry.upcase)
      if ctry
        self.country = ctry.alpha2
      end
      
      if state && ctry
        # check if state/province exists in Carmen
        ste = self.state
        self.state  = ctry.subdivision(ste)
      end
    elsif state && ctry.nil?
      # look up the countries we currently support
      # US, CA, AS, GB, IE
      countries = []
      countries.push(Country["US"], Country["CA"], Country["AS"], Country["GB"], Country["IE"])
      ste = state

      countries.each do |ctr|
        state = ctr.subdivision(ste)
        next unless state.present?
        # set the state
        self.state = state
        self.country = ctr.alpha2
        break
      end
    end
  end
  
  def full_address
    "#{address1.titleize}, #{city.titleize}, #{state.upcase}"
  end
  
  def city_state
    "#{city}, #{state}"
  end
  
  def contractor_type
    return nil if contractor.nil?
    return contractor.types.pluck(:name).first
  end
  
  settings do
    mappings dynamic: false do
      indexes :country,     type: :keyword
      indexes :address1,    type: :keyword
      indexes :city,        type: :keyword
      indexes :state,       type: :keyword
      indexes :zip,         type: :keyword
      indexes :lat,         type: :keyword
      indexes :long,        type: :keyword
      indexes :user_profile do
        indexes :first_name,      type: :keyword
        indexes :last_name,       type: :keyword
        indexes :sex,             type: :keyword
        indexes :monthly_email,   type: :boolean
        indexes :user do
          indexes :role,                  type: :keyword
          indexes :sign_in_count,         type: :integer
          indexes :last_sign_in_at,       type: :date
          indexes :created_at,            type: :date
          indexes :create_method,         type: :keyword
          indexes :accepted_transfer_at,  type: :date
          indexes :completed_onboarding,  type: :boolean
        end
      end
      indexes :partner do
          indexes :name,                type: :text
          indexes :phone,               type: :text
          indexes :email,               type: :text
          indexes :created_at,          type: :date
          indexes :contact,             type: :text
          indexes :partner_type,        type: :keyword
          indexes :partner_configuration do
              indexes :default_binder_action,                 type: :keyword
              indexes :automation_binder_action,              type: :boolean
              indexes :binder_action_delay_length,            type: :integer
              indexes :send_agents_transfer_notification,     type: :keyword
              indexes :show_isn_tab,                          type: :boolean
              indexes :default_user_branding_option,          type: :keyword
              indexes :allow_client_calls,                    type: :boolean
              indexes :active_warranty_account,               type: :boolean
              indexes :repair_pricer_enabled,                 type: :boolean
              indexes :repair_pricer_pre_paid_reports,        type: :boolean
              indexes :repair_pricer_messaging_enabled,       type: :boolean
          end
      end
      indexes :contractor do
          indexes :name,                type: :text
          indexes :phone,               type: :text
          indexes :email,               type: :text
          indexes :contact,             type: :text
      end
    end
  end
    
  def as_indexed_json(options={})
      self.as_json(
          {
              only: [:country, :address1, :city, :state, :zip, :lat, :long],
              include: {
                :user_profile => {
                  :only => [:first_name, :last_name, :sex, :monthly_email],
                  :include => [
                    :user => {
                      :only => [:role, :sign_in_count, :last_sign_in_at, :created_at, :create_method, :accepted_transfer_at, :completed_onboarding],
                    }
                  ]
                },
                :partner => {
                  :only => [:name, :phone, :email, :created_at, :contact, :partner_type, :website],
                  :include => [
                      :partner_configuration => {
                          :only => [:default_binder_action, :automation_binder_action, :binder_action_delay_length, :binder_action_delay_length,
                          :send_agents_transfer_notification, :show_isn_tab, :default_user_branding_option, :allow_client_calls,
                          :repair_pricer_enabled, :repair_pricer_pre_paid_reports, :repair_pricer_messaging_enabled]
                      }
                  ]
                },
                :contractor => {
                  :only => [:name, :phone, :email, :contact],
                  methods: [:contractor_type]
                }
              },
              methods: [:city_state]
          }
      )
  end

end
