# == Schema Information
#
# Table name: contractor_templates
#
#  id                 :integer          not null, primary key
#  binder_template_id :integer
#  library_template   :boolean          default(FALSE)
#  library_source_id  :integer
#  name               :string(100)      not null
#  contractor_type    :string(100)
#  phone              :string(20)
#  email              :string(200)
#  notes              :string(5000)
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

require 'phony_rails'

class Template::Contractor < ApplicationRecord
  include Template::TemplateActions
  include Template::ContractorActions
  
  self.table_name = "contractor_templates"
  
  belongs_to :partner_contractor, :class_name => "::PartnerContractor"
  belongs_to :binder_template, class_name: "BinderTemplate",
                               foreign_key: "binder_template_id"

  has_many    :binder_contractors, :class_name => "Binder::BinderContractor", :foreign_key => "library_source_id"

  has_attached_file :image,
                    :path => Proc.new{|a| a.instance.s3_path}

  accepts_nested_attributes_for :partner_contractor

  validates :binder_template, presence: true
  
  # one unique partner_contractor per template
  validates_uniqueness_of :partner_contractor_id, scope: :binder_template_id, :message => "is already a pro"
  
  after_destroy   :remove_binder_contractors
  after_create    :update_existing_binders

  def remove_binder_contractors
    Binder::BinderContractor.where(:library_source_id => id, :worked_on_property => false).destroy_all
  end

  def update_existing_binders
    Binders::UpdateExistingBindersFromTemplateJob.perform_async(id)
  end

  def s3_path
    library_template ? "/library/contractors/:filename" : "/binder_templates/:binder_template_id/contractors/:filename"
  end
end
