# == Schema Information
#
# Table name: appliance_templates
#
#  id                 :integer          not null, primary key
#  binder_template_id :integer
#  library_template   :boolean          default(FALSE)
#  library_source_id  :integer
#  name               :string(100)      not null
#  notes              :string(5000)
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Template::Appliance < ApplicationRecord
    include Template::TemplateActions
    
    self.table_name = "appliance_templates"
    
    belongs_to :binder_template,    class_name: "BinderTemplate",
                                    foreign_key: "binder_template_id"
    
    validates :name,    length: { maximum: 100 }, presence: true
    validates :notes,   length: { maximum: 5000 }
    
    has_attached_file :image,
                    :path => Proc.new{|a| a.instance.s3_path}
    
    validates_attachment :image, :content_type => {
        :content_type => [
            "image/jpeg",
            "image/jpg",
            "image/png",
            "image/gif",
            "image/bmp"
        ]
    }
    
    validates_uniqueness_of :name, scope: :binder_template_id
    validates_presence_of :binder_template, :message => I18n.t(:err_value_required)
    
    def s3_path
        library_template ? "/library/appliances/:filename" : "/binder_templates/:binder_template_id/appliances/:filename"
    end
    
    def self.base_query
        "CAST(appliance_templates.id as varchar(25)) LIKE :search OR
        LOWER(appliance_templates.name) LIKE :search OR
        LOWER(appliance_templates.description) LIKE :search OR
        LOWER(appliance_templates.notes) LIKE :search OR
        LOWER(binder_templates.name) LIKE :search".freeze
    end
end
