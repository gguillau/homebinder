# == Schema Information
#
# Table name: maintenance_templates
#
#  id                        :integer          not null, primary key
#  binder_template_id        :integer
#  library_template          :boolean          default(FALSE)
#  library_source_id         :integer
#  name                      :string(100)
#  frequency                 :string(50)
#  due_date                  :datetime
#  notes                     :string(5000)
#  image_file_name           :string(255)
#  image_content_type        :string(255)
#  image_file_size           :integer
#  image_updated_at          :datetime
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  initial_due_date_interval :integer
#

class Template::Maintenance < ApplicationRecord
  include Template::TemplateActions
  include Template::Maintenance::Actions
  
  self.table_name = "maintenance_templates"
  
  belongs_to :binder_template, class_name: "BinderTemplate",
                               foreign_key: "binder_template_id"
                               
  has_many  :maintenance_template_contractor_types, :class_name => "Template::MaintenanceTemplateContractorType", :foreign_key => "maintenance_template_id",  :dependent => :destroy
  has_many  :types, :through => :maintenance_template_contractor_types, :source => "contractor_category"
  
  validates :name, length: { maximum: 100 }, presence: true
  validates :notes, length: { maximum: 5000 }
  validates :frequency, length: { maximum: 100 }
  validates_presence_of :binder_template, :message => I18n.t(:err_value_required)
  
  accepts_nested_attributes_for :maintenance_template_contractor_types
  accepts_nested_attributes_for :types
  
  has_attached_file :image,
                    :path => Proc.new{|a| a.instance.s3_path}
                    
  validates_attachment :image, :content_type => {
    :content_type => [
      "image/jpeg",
      "image/jpg",
      "image/png",
      "image/gif",
      "image/bmp"]}
      
  validates_uniqueness_of :name, scope: :binder_template_id
  
  def s3_path
    library_template ? "/library/maintenance/:filename" : "/binder_templates/:binder_template_id/maintenance/:filename"
  end
  
  def self.base_query
      "CAST(maintenance_templates.id as varchar(25)) LIKE :search OR
      LOWER(maintenance_templates.name) LIKE :search OR
      LOWER(maintenance_templates.description) LIKE :search OR
      LOWER(maintenance_templates.notes) LIKE :search OR
      LOWER(maintenance_templates.frequency) LIKE :search OR
      LOWER(binder_templates.name) LIKE :search".freeze
  end

end
