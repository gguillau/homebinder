module Template::Maintenance::Actions
    def self.included(klass)
        klass.extend ClassMethods
    end

    module ClassMethods
        
        def build(hb_request, params)
            validate_types(params)
            super(hb_request, params)
        end

        def update(hb_request, params)
            validate_types(params)
            super(hb_request, params)
        end
        
        def validate_types(params)
            if params[symbol].dig(:types_attributes)
                params[symbol][:maintenance_template_contractor_types] = []
                params[symbol][:types_attributes].each do |type|
                    exisiting_type = ::Contractor::Category.where("LOWER(name) = ?", type[:name].downcase).first
                    
                    if exisiting_type.nil?
                        exisiting_type = ::Contractor::Category.create(:name => type[:name])
                    end
                    
                    params[symbol][:maintenance_template_contractor_types].push(Template::MaintenanceTemplateContractorType.new({"contractor_category_id" => exisiting_type.id}))
                    
                    if params[symbol].dig(:contractor_attributes) && params[symbol].dig(:contractor_attributes, :contractor_types_attributes).nil?
                        params[symbol][:contractor_attributes][:contractor_types_attributes] = []
                    end
                    
                    if params[symbol].dig(:contractor_attributes) && params[symbol].dig(:contractor_attributes, :contractor_types_attributes)
                        params[symbol][:contractor_attributes][:contractor_types_attributes].push({"contractor_category_id" => exisiting_type.id})
                    end
                end
                
                if params[symbol].dig(:maintenance_template_contractor_types_attributes)
                    params[symbol][:maintenance_template_contractor_types_attributes].each do |type|
                        if type[:id]
                            existing = Template::MaintenanceTemplateContractorType.find_by(:id => type[:id])
                            params[symbol][:maintenance_template_contractor_types].push(existing)
                        else
                            existing = Template::MaintenanceTemplateContractorType.new(:contractor_category_id => type[:contractor_category_id])
                            params[symbol][:maintenance_template_contractor_types].push(existing)
                        end
                    end
                end
                
                params[symbol][:types_attributes] = params[symbol][:maintenance_template_contractor_types_attributes] = []
                
                if params[symbol].dig(:contractor_attributes, :types_attributes)
                    params[symbol][:contractor_attributes][:types_attributes] = []
                end
            end
        end
    end
end