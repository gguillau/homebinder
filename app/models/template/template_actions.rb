module Template::TemplateActions

    def self.included(klass)
        klass.extend ClassMethods
    end

    module ClassMethods
        
        def base_includes
            [:binder_template]
        end

        def index(hb_request, params)
            if hb_request.role != "admin"
                authorize(hb_request, params)
            end
            super(hb_request, params)
        end

        def build(hb_request, params)
            authorize(hb_request, params)
            super(hb_request, params)
        end

        def send_test_notification_email(hb_request, id)
            item = show(hb_request, {id: id})
            binder = item.binder_template.partner_configuration.partner.binders.first
            return if binder.nil?
            do_date = Date.tomorrow

            item = OpenStruct.new({name: item.name, filter_easy_eligible?: false, details: item.notes})
            event = OpenStruct.new({id: -1, maintenance_item: item})
            
            # send the email
            MaintenanceNotifyMailer.notify_email(event[:id], binder.id, hb_request.user.id, do_date.to_s).deliver_later
        end
        
        def add_custom_args(where, where_not, params, includes, joins)
            if params[:system]
                joins.push(:binder_template)
                where.merge!({:binder_templates => {:system => true}})
            end
            
            if params[:default]
                joins.push(:binder_template)
                if where[:binder_templates]
                    where[:binder_templates].merge!({:default => true})
                else
                    where.merge!({:binder_templates => {:default => true}})
                end
            end
            super
        end
        
        def query_arguments
            ["library_template", "binder_template_id", "name"]
        end

        def query_arguments_hash(query, value)
            case query
            when "library_template", "binder_template_id", "name"
                {query.to_sym => value}
            else
                {}
            end
        end

        private

        def authorize(hb_request, params)
            if params[:binder_template_id]
                binder_template = BinderTemplate.find(params[:binder_template_id])
                hb_request.ability.authorize! :read, binder_template, :message => "You are not authorized to access binder template ID: #{binder_template.id}."
            end
        end
    end

end
