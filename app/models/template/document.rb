# == Schema Information
#
# Table name: document_templates
#
#  id                 :integer          not null, primary key
#  binder_template_id :integer
#  library_source_id  :integer
#  file_file_name     :string(255)
#  file_content_type  :string(255)
#  file_file_size     :integer
#  file_updated_at    :datetime
#  notes              :string(5000)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Template::Document < ApplicationRecord
  include Template::TemplateActions
  
  self.table_name = "document_templates"
  
  belongs_to :binder_template, class_name: "BinderTemplate",
                               foreign_key: "binder_template_id"
  belongs_to :document_type
  
  validates :notes, length: { maximum: 5000 }
  # validations
  validates_presence_of :document_type, :message => I18n.t(:err_value_required)
  validates_presence_of :binder_template, :message => I18n.t(:err_value_required)
  
  after_save :update_binder_documents
  
  has_attached_file :file,
                    :path => Proc.new{|a| a.instance.s3_path}
  
  validates_attachment :file, :content_type => {
    :content_type => [
      "application/pdf",
      "application/mspowerpoint",
      "application/powerpoint",
      "application/vnd.ms-powerpoint",
      "application/x-mspowerpoint",
      "application/vnd.openxmlformats-officedocument.presentationml.presentation",
      "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
      "application/msword",
      "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
      "application/excel",
      "application/vnd.ms-excel",
      "application/x-excel",
      "application/x-msexcel",
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
      "text/plain"]}

  # one unique file_file_name per binder_template
  validates_uniqueness_of :file_file_name, scope: :binder_template_id
  
  def s3_path
    "/binder_templates/:binder_template_id/documents/:filename"
  end
  
  def update_binder_documents
    if file_updated_at_changed?
      Templates::Document::UpdateBinderDocumentsJob.perform_async(id)
    end
  end
  
end
