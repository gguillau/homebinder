class Template::MaintenanceTemplateContractorType < ApplicationRecord
    self.table_name = "maintenance_template_contractor_types"
    
    belongs_to  :maintenance_template,  :class_name => "Template::Maintenance"
    belongs_to  :contractor_category,   :class_name => "Contractor::Category"
    
    # validations
    validates_uniqueness_of     :maintenance_template, scope: :contractor_category, :message => "already exists"
    validates_presence_of       :maintenance_template, :message => I18n.t(:err_value_required), :on => :update
    validates_presence_of       :contractor_category, :message => I18n.t(:err_value_required)
end
