module Template::ContractorActions

    def self.included(klass)
        klass.extend ClassMethods
    end

    module ClassMethods
        
        def build(hb_request, params)
            validate_types(params)
            validate_sub_types(params)
            super(hb_request, params)
        end

        def update(hb_request, params)
            validate_types(params)
            validate_sub_types(params)
            super(hb_request, params)
        end

        def validate_types(params)
            if params[symbol].dig(:partner_contractor_attributes, :types_attributes)
                params[symbol][:partner_contractor_attributes][:partner_contractor_types] = []
                params[symbol].dig(:partner_contractor_attributes, :types_attributes).each do |type|
                    exisiting_type = ::Contractor::Category.where("LOWER(name) = ?", type[:name].downcase).first
                    
                    if exisiting_type.nil?
                        exisiting_type = Contractor::Category.create(:name => type[:name])
                    end
                    
                    params[symbol][:partner_contractor_attributes][:partner_contractor_types].push(PartnerContractorType.new({"contractor_category_id" => exisiting_type.id}))

                    if params[symbol].dig(:partner_contractor_attributes, :contractor_attributes, :contractor_types_attributes)
                        params[symbol][:partner_contractor_attributes][:contractor_attributes][:contractor_types_attributes].push({"contractor_category_id" => exisiting_type.id})
                    end
                end
                
                partner_contractor_types = params[symbol].dig(:partner_contractor_attributes, :partner_contractor_types_attributes) || []
                partner_contractor_types.each do |type|
                    if type[:id]
                        existing = PartnerContractorType.find_by(:id => type[:id])
                        params[symbol][:partner_contractor_attributes][:partner_contractor_types].push(existing)
                    else
                        existing = PartnerContractorType.new(:contractor_category_id => type[:contractor_category_id])
                        params[symbol][:partner_contractor_attributes][:partner_contractor_types].push(existing)
                    end
                end
                
                params[symbol][:partner_contractor_attributes][:types_attributes] = params[symbol][:partner_contractor_attributes][:partner_contractor_types_attributes] = []
                
                if params[symbol].dig(:partner_contractor_attributes, :contractor_attributes, :types_attributes)
                    params[symbol][:partner_contractor_attributes][:contractor_attributes][:types_attributes] = []
                end
            end
        end
        
        def validate_sub_types(params)
            if params[symbol].dig(:partner_contractor_attributes, :sub_types_attributes)
                params[symbol][:partner_contractor_attributes][:partner_contractor_sub_types] = []
                params[symbol].dig(:partner_contractor_attributes, :sub_types_attributes).each do |type|
                    exisiting_type = ::Contractor::SubCategory.where("LOWER(name) = ?", type[:name].downcase).first
                    
                    if exisiting_type.nil?
                        exisiting_type = Contractor::SubCategory.create(:name => type[:name])
                    end
                    
                    params[symbol][:partner_contractor_attributes][:partner_contractor_sub_types_attributes].push(PartnerContractorSubType.new({"contractor_sub_category_id" => exisiting_type.id}))

                    if params[symbol].dig(:partner_contractor_attributes, :contractor_attributes, :contractor_sub_types_attributes)
                        params[symbol][:partner_contractor_attributes][:contractor_attributes][:contractor_sub_types_attributes].push({"contractor_sub_category_id" => exisiting_type.id})
                    end
                end
                
                params[symbol].dig(:partner_contractor_attributes, :partner_contractor_sub_types_attributes).each do |type|
                    if type[:id]
                        existing = PartnerContractorSubType.find_by(:id => type[:id])
                        params[symbol][:partner_contractor_attributes][:partner_contractor_sub_types].push(existing)
                    else
                        existing = PartnerContractorSubType.new(:contractor_sub_category_id => type[:contractor_sub_category_id])
                        params[symbol][:partner_contractor_attributes][:partner_contractor_sub_types].push(existing)
                    end
                end
                
                params[symbol][:partner_contractor_attributes][:sub_types_attributes] = params[symbol][:partner_contractor_attributes][:partner_contractor_sub_types_attributes] = []
                
                if params[symbol].dig(:partner_contractor_attributes, :contractor_attributes, :sub_types_attributes)
                    params[symbol][:partner_contractor_attributes][:contractor_attributes][:sub_types_attributes] = []
                end
            end
        end
    end
end
