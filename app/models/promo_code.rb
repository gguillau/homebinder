# == Schema Information
#
# Table name: promo_codes
#
#  id              :integer          not null, primary key
#  name            :string(100)      not null
#  percent_off     :integer          default(0), not null
#  amount_off      :integer          default(0), not null
#  expiration_date :datetime
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class PromoCode < ApplicationRecord
    include PromoCode::Actions
    
    # validations
    validates_presence_of   :name,              :message => I18n.t(:err_value_required)
    validates_presence_of   :amount_off,        :message => I18n.t(:err_value_required)
    validates_presence_of   :percent_off,       :message => I18n.t(:err_value_required)
end
