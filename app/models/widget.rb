# == Schema Information
#
# Table name: widgets
#
#  id                          :integer          not null, primary key
#  name                        :string(100)
#  key                         :string(100)
#  description                 :string(1000)
#  category                    :string(100)
#  created_at                  :datetime
#  updated_at                  :datetime
#  configuration               :text
#  organization_restriction_id :integer
#  partner_restriction_id      :integer
#  admin_restriction           :boolean          default(FALSE)
#

class Widget < ApplicationRecord
  # relationships
  has_many :dashboard_widgets,  :dependent => :destroy
  has_many :dashboards, through: :dashboard_widgets
  
  # validations
  validates :name, presence: true, length: { maximum: 255 }, unique: {}
  validates :key, presence: true, length: { maximum: 255 }
  validates :category, length: { maximum: 255 }
  validate :max_one_restriction
  
  def max_one_restriction
    if organization_restriction_id.present?
      if partner_restriction_id.present?
        errors.add(:partner_restriction_id, I18n.t(:err_one_restriction))
      end
      
      if admin_restriction === true
        errors.add(:admin_restriction, I18n.t(:err_one_restriction))
      end
    end
    
    if partner_restriction_id.present?
      if organization_restriction_id.present?
        errors.add(:organization_restriction_id, I18n.t(:err_one_restriction))
      end
      
      if admin_restriction === true
        errors.add(:admin_restriction, I18n.t(:err_one_restriction))
      end
    end
    
    if admin_restriction === true
      if organization_restriction_id.present?
        errors.add(:organization_restriction_id, I18n.t(:err_one_restriction))
      end
      
      if partner_restriction_id.present?
        errors.add(:partner_restriction_id, I18n.t(:err_one_restriction))
      end
    end
  end

end
