class HBRequest
  
  attr_accessor :controller
  attr_accessor :action
  attr_accessor :user
  attr_accessor :role
  attr_accessor :ability
  attr_accessor :data
  
  def create_request(user)
    hb_request = HBRequest.new
    hb_request.user = user
    hb_request.ability = Ability.new(user)
    hb_request.role = user.role
    return hb_request
  end
  
end