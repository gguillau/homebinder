# == Schema Information
#
# Table name: contractor_templates
#
#  id                 :integer          not null, primary key
#  binder_template_id :integer
#  library_template   :boolean          default(FALSE)
#  library_source_id  :integer
#  name               :string(100)      not null
#  contractor_type    :string(100)
#  phone              :string(20)
#  email              :string(200)
#  notes              :string(5000)
#  image_file_name    :string(255)
#  image_content_type :string(255)
#  image_file_size    :integer
#  image_updated_at   :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

require 'phony_rails'

class PartnerContractor < ApplicationRecord
    include ::PartnerContractor::Actions

    self.table_name = "partner_contractors"

    belongs_to :contractor, :class_name => "::Contractor"
    belongs_to :partner

    has_many    :partner_contractor_types, :class_name => "PartnerContractorType", :foreign_key => "partner_contractor_id",  :dependent => :destroy
    has_many    :types, :through => :partner_contractor_types, :source => "contractor_category"
    has_many    :partner_contractor_sub_types, :class_name => "PartnerContractorSubType", :foreign_key => "partner_contractor_id", :dependent => :destroy
    has_many    :sub_types, :through => :partner_contractor_sub_types, :source => "contractor_sub_category"

    has_many    :template_contractors, :class_name => "Template::Contractor", :foreign_key => "partner_contractor_id"
    has_one     :address, :dependent => :destroy, :foreign_key => "partner_contractor_id"

    accepts_nested_attributes_for :address
    accepts_nested_attributes_for :contractor
    accepts_nested_attributes_for :partner_contractor_types
    accepts_nested_attributes_for :types
    accepts_nested_attributes_for :sub_types
    accepts_nested_attributes_for :partner_contractor_sub_types

    validates :name, length: { maximum: 100 }, presence: true
    validates :phone, length: { maximum: 20 }
    validates :email, length: { maximum: 200 }
    validates :notes, length: { maximum: 5000 }
    validates :contractor, presence: true
    validates :partner, presence: true

    validates_uniqueness_of :contractor, scope: :partner, :message => "already exists"
    validates_uniqueness_of   :email, scope: :partner, allow_blank: true, if: :email_changed?

    phony_normalize :phone, :default_country_code => "US"

    has_attached_file :logo,
                      :path => "/partners/:partner_id/contractors/:filename"

    validates_attachment :logo, :content_type => {
                                    :content_type => [
                                        "image/jpeg",
                                        "image/jpg",
                                        "image/png",
                                        "image/gif",
                                        "image/bmp"
                                    ]
                                }

    # make sure to downcase emails
    before_validation :format_email
    before_validation :valid_types

    def valid_types
        if types.empty? && partner_contractor_types.empty?
            errors.add(:types, "- Minimum of 1 type per pro required")
        end
    end

    after_update    :update_contractor
    after_update    :update_binder_contractors
    after_destroy   :remove_template_contractors
    after_create    :send_invitation

    def send_invitation
        return if not send_email_invitation
        return if contractor.nil?
        return if contractor.email.blank?
        return if contractor.created_at.to_i < created_at.to_i
        Users::CreateHomeProJob.perform_async(contractor_id)
    end

    def format_email
        return if email.nil?
        self.email = Partner::Automation::EmailParser.new.format_value(["", "email"], email.downcase.gsub(/\s+/, ""))
        self.email = email =~ /\A[^@\s]+@[^@\s]+\z/ ? email : ""
    end

    def update_contractor
        return if contractor.user && contractor.user.sign_in_count > 0
        contractor.update(
            :name => name, 
            :email => email, 
            :phone => phone, 
            :url => website
        )
    
        return if contractor.address.nil?
        return if address.nil?
        
        contractor.address.update(
            :address1 => address.address1, 
            :address2 => address.address2, 
            :city => address.city, 
            :state => address.state,
            :zip => address.zip,
            :country => address.country
        )
    end
    
    def update_binder_contractors
        Partners::UpdateBinderContractorsJob.perform_async(id)
    end
    
    def remove_template_contractors
        template_contractors.destroy_all
    end
end
