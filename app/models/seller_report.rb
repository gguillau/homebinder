# == Schema Information
#
# Table name: seller_reports
#
#  id         :integer          not null, primary key
#  binder_id  :integer
#  code       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  public     :boolean
#

class SellerReport < ApplicationRecord
  include SellerReport::SellerReportBuilder
  # attr_accessible :title, :body
  belongs_to :binder
  
  before_create {|report| report.code = SecureRandom.hex(3).upcase}
  
  def appliances
    #binder.appliances
    @apps = Array.new
    binder.appliances.each do |a|
      if a.seller_report_item && a.seller_report_item.include
        @apps << a
      end
    end
    return @apps    
  end
  
  def projects
    #binder.projects.where(status: 'Completed')
    @projs = Array.new
    binder.projects.where(status: 'Completed').each do |p|
      if p.seller_report_item && p.seller_report_item.include
        @projs << p
      end
    end
    return @projs    
  end
  
  def maintenance_items
    #binder.maintenance_items
    @maints = Array.new
    binder.maintenance_items.each do |m|
      if m.seller_report_item && m.seller_report_item.include
        @maints << m
      end
    end
    return @maints    
  end
  
  def contractors
    #binder.binder_contractors
    @conts = Array.new
    binder.binder_contractors.each do |c|
      if c.seller_report_item && c.seller_report_item.include
        @conts << c
      end
    end
    return @conts    
  end
  
  def paints
  @paints = Array.new
    binder.paints.each do |pi|
      if pi.seller_report_item && pi.seller_report_item.include
        @paints << pi
      end
    end
    return @paints
  end
  
  def permits
    @permits = Array.new
    binder.permits.each do |pt|
      if pt.seller_report_item && pt.seller_report_item.include
        @permits << pt
      end
    end
    return @permits
  end
  
  def finishes
  @finish = Array.new
    binder.finishes.each do |f|
      if f.seller_report_item && f.seller_report_item.include
        @finish << f
      end
    end
    return @finish
  end
  
  def documents
    @docs = Array.new
    binder.documents.each do |d|
      if d.seller_report_item && d.seller_report_item.include
        @docs << d
      end
    end
    return @docs
  end
  
  def images
    @imgs = Array.new
    binder.images.each do |i|
      if i.seller_report_item && i.seller_report_item.include
        @imgs << i
      end
    end
    return @imgs
  end
  
  def binder
    @binder ||= Binder.find_by_id(self.binder_id)
  end
  
end
