# == Schema Information
#
# Table name: organization_users
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  user_id         :integer
#  role            :string(100)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class OrganizationUser < ApplicationRecord
    
    # associations
    belongs_to :organization
    belongs_to :user
    
    # validations
    validates_presence_of   :user,               :message => I18n.t(:err_value_required)
    validates_presence_of   :organization,       :message => I18n.t(:err_value_required)
    #validates_presence_of   :role,                  :message => I18n.t(:err_value_required)
    
    # one unique user per organization
    validates_uniqueness_of :user_id, scope: :organization_id
end
