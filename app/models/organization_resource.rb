# == Schema Information
#
# Table name: organization_resources
#
#  id                    :integer          not null, primary key
#  marketing_resource_id :integer
#  organization_id       :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class OrganizationResource < ApplicationRecord
    
    # associations
    belongs_to :organization
    belongs_to :marketing_resource
    
    # validations
    validates_presence_of   :marketing_resource,     :message => I18n.t(:err_value_required)
    validates_presence_of   :organization,           :message => I18n.t(:err_value_required)
    
    # one unique marketing_resource per organization
    validates_uniqueness_of :marketing_resource_id, scope: :organization_id, :message => "already exists for organization"
end
