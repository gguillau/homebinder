class Event
    include Elasticsearch::Persistence::Model
    include Elasticsearch::Model
    include BasePager
    include BaseSearcher
    include BaseElasticsearch
    
    attribute :event_name,          String
    attribute :event_type,          String
    attribute :user_id,             Integer
    attribute :user_role,           String
    attribute :user_created_at,     Date
    attribute :user_create_method,  String
    attribute :appliance_id,        Integer
    attribute :binder_id,           Integer
    attribute :homepro_id,          Integer
    attribute :apr_id,              Integer
    attribute :partner_key,         String
    attribute :warranty_plan,       String
    attribute :partner_id,          Integer
    attribute :partner_type,        String
    
    def self.index(hb_request, params, search_method = :search)
        self.pager(hb_request, params, search_method)
    end
end