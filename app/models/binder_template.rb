# == Schema Information
#
# Table name: binder_templates
#
#  id                       :integer          not null, primary key
#  partner_configuration_id :integer
#  name                     :string(100)      not null
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  transfer_note            :string(1000)
#

class BinderTemplate < ApplicationRecord
  include BinderTemplateActions
  
  belongs_to :partner_configuration, class_name: "Partner::Configuration",
                                     foreign_key: "partner_configuration_id"
                                     
  has_many :appliance_templates, :dependent => :destroy,
                                 class_name: "Template::Appliance",
                                 foreign_key: "binder_template_id"
  has_many :contractor_templates, :dependent => :destroy,
                                  class_name: "Template::Contractor",
                                  foreign_key: "binder_template_id"
  has_many :maintenance_templates, :dependent => :destroy,
                                   class_name: "Template::Maintenance",
                                   foreign_key: "binder_template_id"
  has_many :document_templates, :dependent => :destroy,
                                   class_name: "Template::Document",
                                   foreign_key: "binder_template_id"
                                   
  validates :name, length: { maximum: 100 }, presence: true
  
  before_destroy :ensure_default_valid_template, prepend: true
  
  # we need to make sure that a partner always has a default template
  def ensure_default_valid_template
    # check if the template belongs to a partner config and is the default template
    if partner_configuration && partner_configuration.default_binder_template_id === id
      errors.add(:id, "Cannot destroy default binder template!")
      throw :abort
    elsif Binder.where(:binder_template_id => id).exists?
      errors.add(:id, "Cannot destroy templates used to create existing binders")
      throw :abort
    end
  end
end
