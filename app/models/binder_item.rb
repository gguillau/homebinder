class BinderItem < ApplicationRecord
    include Binder::BinderItems

    # associations
    belongs_to  :binder
    belongs_to  :user
    belongs_to  :appliance,         :class_name => "Binder::Appliance"
    belongs_to  :area,              :class_name => "Binder::Area"
    belongs_to  :binder_contractor, :class_name => "Binder::BinderContractor"
    belongs_to  :finish,            :class_name => "Binder::Finish"
    belongs_to  :inventory_item,    :class_name => "Binder::InventoryItem"
    belongs_to  :maintenance_item,  :class_name => "Binder::MaintenanceItem"
    belongs_to  :paint,             :class_name => "Binder::Paint"
    belongs_to  :receipt,           :class_name => "Binder::Receipt"
    belongs_to  :structure,         :class_name => "Binder::Structure"
    belongs_to  :image,             :class_name => "Binder::Image"
    belongs_to  :document,          :class_name => "Binder::Document"
    belongs_to  :permit,            :class_name => "Binder::Permit"
    belongs_to  :project,           :class_name => "Binder::Project"


    # validations
    validates_presence_of :binder,  :message => I18n.t(:err_value_required)
    validates_presence_of :user,    :message => I18n.t(:err_value_required)
    
    # scopes
    scope :verified,    -> { where(:verified  => true) }
    scope :unverified,  -> { where(:verified  => false) }
    scope :dismissed,   -> { where(:dismissed => true) }
    
    def item
        if appliance
            return appliance
        elsif binder_contractor
            return binder_contractor
        elsif area
            return area
        elsif finish
            return finish
        elsif inventory_item
            return inventory_item
        elsif maintenance_item
            return maintenance_item
        elsif paint
            return paint
        elsif receipt
            return receipt
        elsif structure
            return structure
        elsif image
            return image
        elsif document
            return document
        elsif project
            return project
        elsif permit
            return permit
        end
    end
    
    def type
        item.class.name.constantize.table_name.singularize.titleize
    end
end
