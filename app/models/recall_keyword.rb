# == Schema Information
#
# Table name: recall_keywords
#
#  id         :integer          not null, primary key
#  keyword    :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class RecallKeyword < ApplicationRecord
  # attr_accessible :title, :body
  
  # validations
  validates :keyword, presence: true, length: { maximum: 250 }, unique: {}
end
