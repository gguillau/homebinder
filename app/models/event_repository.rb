class EventRepository
    include Elasticsearch::Persistence::Repository

    index_name "events"
    klass Event
    
    def all(options = {})
        search({ query: { match_all: { } } }, options)
    end
    
    def deserialize(document)
        hash = document['_source']
        hash["id"] = hash["_id"] = document["_id"]
        klass.new hash
    end

end