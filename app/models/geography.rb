class Geography < ApplicationRecord
  # validations
  validates :name, presence: true, length: { maximum: 250 }, unique: {}
end
