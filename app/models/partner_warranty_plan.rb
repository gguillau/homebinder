# == Schema Information
#
# Table name: partner_warranty_plans
#
#  id               :integer          not null, primary key
#  price            :money            not null
#  partner_id       :integer          not null
#  warranty_plan_id :integer          not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class PartnerWarrantyPlan < ApplicationRecord
    
    # associations
    belongs_to :warranty_plan
    belongs_to :partner
    
    belongs_to :warranty_configuration, :foreign_key => :partner_id
    
    # validations
    validates_presence_of   :price,             :message => I18n.t(:err_value_required)
    validates_presence_of   :partner_id,        :message => I18n.t(:err_value_required)
    validates_presence_of   :warranty_plan,     :message => I18n.t(:err_value_required)
    validates_uniqueness_of :warranty_plan_id,  scope: :partner_id

end
