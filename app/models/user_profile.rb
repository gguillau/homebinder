# == Schema Information
#
# Table name: user_profiles
#
#  id                     :integer          not null, primary key
#  user_id                :integer
#  first_name             :string(255)
#  last_name              :string(255)
#  home_phone             :string(255)
#  mobile_phone           :string(255)
#  dob                    :date
#  sex                    :string(255)
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  monthly_email          :boolean          default(TRUE)
#  company                :string(255)
#  website                :string(255)
#  bio                    :text
#  message                :string(1000)
#  head_shot_file_name    :string
#  head_shot_content_type :string
#  head_shot_file_size    :integer
#  head_shot_updated_at   :datetime
#  logo_file_name         :string
#  logo_content_type      :string
#  logo_file_size         :integer
#  logo_updated_at        :datetime
#

require 'phony_rails'
require 'uri'

class UserProfile < ApplicationRecord
  self.table_name = "user_profiles"
  
  # relationships
  belongs_to  :user

  has_one :address, :dependent => :destroy
  
  accepts_nested_attributes_for :address
  
  phony_normalize :home_phone, :default_country_code => 'US'
  phony_normalize :mobile_phone, :default_country_code => 'US'
  
  validates_length_of :first_name, :maximum => 50
  validates_length_of :last_name, :maximum => 50
  validates_length_of :sex, :maximum => 1
  validates_length_of         :website,   :maximum => 250
  validates_presence_of   :user,      :message => I18n.t(:err_value_required), :on => :update
  #validates_presence_of   :address,   :message => I18n.t(:err_value_required), :on => :update, if: :old_account?
  
  validates_uniqueness_of   :user, :on => :update
  
  # branding attributes is limited to 255 characters
  validates :company, length: { maximum: 255}
  validates :message, length: { maximum: 1000}
    
  has_attached_file :head_shot,
      :path => "/userprofile:id/headshot/:style/:filename",
      :source_file_options => { all: '-auto-orient' },
      #:processors => [:transparency],
      :convert_options => { :all => "-quality 100" },
      :styles => {
      :thumb => ["175x175>",:png],
      :original => ["350x350>",:png]
  }
  
  validates_attachment :head_shot, :content_type => {
      :content_type => [
          "image/jpeg",
          "image/jpg",
          "image/png",
          "image/gif",
          "image/bmp"
      ]
  }
  
  has_attached_file :logo,
      :path => "/userprofile:id/logo/:style/:filename",
      :source_file_options => { all: '-auto-orient' },
      #:processors => [:transparency],
      :convert_options => { :all => "-quality 100" },
      :styles => {
      :thumb => ["175x175>",:png],
      :original => ["350x350>",:png]
  }
  
  validates_attachment :logo, :content_type => {
      :content_type => [
          "image/jpeg",
          "image/jpg",
          "image/png",
          "image/gif",
          "image/bmp"
      ]
  }
  
  before_destroy  :can_destroy?, prepend: true
  
  def can_destroy?
      if Partner::Configuration.where(:default_user_branding_id => id).count > 0
          errors.add(:id, "Cannot delete profile for #{display_name} because he/she is the default branding user for a partner")
          throw :abort
      end
  end
  
  def old_account?
    created_at < Date.today
  end
  
  def display_name
    if !first_name.blank? && !last_name.blank?
      "#{first_name} #{last_name}"
    else
      if !first_name.blank?
        first_name
      else
        email
      end
    end
  end
  
  def email
    user.email
  end
  
  def mobile_phone_formatted
    return nil if mobile_phone.nil?
    return nil if not Phony.plausible?(mobile_phone)
    return mobile_phone.phony_formatted(:format => :national, :spaces => ' ')
  end
  
  def branding
    return {
      email: email,
      role: user.role,
      display_name: display_name,
      first_name: first_name,
      last_name: last_name,
      mobile_phone: mobile_phone_formatted,
      company: company.present? ? company.titleize : "",
      message: message.present? ? message.html_safe : "<p>Thank you for your business!</p>".html_safe,
      transfer_message_to_homeowner: transfer_message_to_homeowner.present? ? transfer_message_to_homeowner.html_safe : nil,
      transfer_message_to_agent: transfer_message_to_agent.present? ? transfer_message_to_agent.html_safe : nil,
      transfer_message_to_prepaid_agent: transfer_message_to_prepaid_agent.present? ? transfer_message_to_prepaid_agent.html_safe : nil,
      headshot: head_shot.present? ? head_shot.expiring_cloud_front_url(1.week, :thumb) : "https://s3.amazonaws.com/homebinderstatic/img/headshot.png",
      full_role: full_role,
      partner_id: user.partner_ids.first,
      website: website,
      logo: logo.present? ? logo.expiring_cloud_front_url(1.week, :thumb) : nil,
      types: types,
      sub_types: sub_types,
      binder_count: binder_count
    }
  end
  
  def binder_count
    return 0 if user.contractor.nil?
    return Binder::BinderContractor.where(:contractor_id => user.contractor.id).count
  end
  
  def types
    return "" if user.contractor.nil?
    return user.contractor.types.pluck(:name).join(", ")
  end
  
  def sub_types
    return "" if user.contractor.nil?
    return user.contractor.sub_types.pluck(:name).join(", ")
  end
  
  def full_role
    case self.user.role
    when "inspector"
      "Home Inspector"
    when "broker"
      "Real Estate Broker"
    when "agent"
      "Real Estate Agent"
    when "lender"
      "Home Lender"
    when "homepro"
      "Home Professional"
    when "builder"
      "Home Builder"
    else
      "Homeowner"
    end
  end
  
  def head_shot_url(url)
    self.head_shot = URI.parse(url)
  end
  
  def logo_url(url)
    self.logo = URI.parse(url)
  end
  
end
