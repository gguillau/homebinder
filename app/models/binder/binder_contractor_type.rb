class Binder::BinderContractorType < ApplicationRecord
    self.table_name = "binder_contractor_types"
    
    belongs_to  :binder_contractor
    belongs_to  :contractor_category,   :class_name => "Contractor::Category"
    
    # validations
    validates_uniqueness_of     :binder_contractor, scope: :contractor_category, :message => "already exists"
    validates_presence_of       :binder_contractor, :message => I18n.t(:err_value_required), :on => :update
    validates_presence_of       :contractor_category, :message => I18n.t(:err_value_required)
end
