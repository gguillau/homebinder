# == Schema Information
#
# Table name: shares
#
#  id                :integer          not null, primary key
#  shared_by_id      :integer
#  shared_with_id    :integer
#  shared_with_email :string(255)
#  sharable_id       :integer
#  sharable_type     :string(255)
#  role_name         :string(255)
#  status            :string(255)
#  expirable         :boolean
#  expires_on        :date
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  binder_id         :integer
#  last_notified_at  :date
#  completed_at      :date
#  access_token      :string
#

class Binder::Share < ApplicationRecord
    include Binder::ComponentActions
    include Binder::Share::Actions

    self.table_name = "shares"

    has_secure_token :access_token

    belongs_to :sender, :class_name => "User", :foreign_key => "shared_by_id"
    belongs_to :receiver, :class_name => "User", :foreign_key => "shared_with_id"
    belongs_to :binder

    # validations
    validates_presence_of :binder, :message => I18n.t(:err_value_required)
    validates_presence_of :role_name, :message => I18n.t(:err_value_required)
    validates_presence_of :status, :message => I18n.t(:err_value_required)
    validates_presence_of :shared_by_id, :message => I18n.t(:err_value_required)
    validates_presence_of :shared_with_id, :message => I18n.t(:err_value_required)
    
    # one unique share per shared_by and shared_with
    validates_uniqueness_of :shared_by_id, scope: [:binder_id, :shared_with_id]

    before_validation :different_users

    def different_users
        if self.shared_by_id === self.shared_with_id
            errors.add(:shared_by_id, "Error - Share Sender and Receiver cannot be same users")
        end
    end
end
