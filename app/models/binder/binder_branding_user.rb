class Binder::BinderBrandingUser
    include ActiveModel::Serialization
    
    def initialize(object)
        self.partner_id = object[:partner_id]
        self.role = object[:role]
    end
    
    attr_accessor :partner_id, :role
    
    def attributes
        {"partner_id" => partner_id, "role" => role}
    end
end
