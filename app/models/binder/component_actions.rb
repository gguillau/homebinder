module Binder::ComponentActions

    def self.included(klass)
        klass.extend ClassMethods
    end

    module ClassMethods

        FREE = "free".freeze
        PAID = "paid".freeze
        NOT_FOUND = "Item not found".freeze

        def index(hb_request, params)
            if hb_request.user.role != "admin" && hb_request.user.role != "limited_admin"
                if params[:created_by].blank?
                    raise BadRequestException.new "Binder ID required" if params[:binderId].blank? && params[:binder_id].blank?
                    # get the binder
                    binder = Binder.find(params[:binderId] || params[:binder_id])
                    # verify the user can read the binder
                    hb_request.ability.authorize! :read, binder, :message => "You are not authorized to access binder ID: #{binder.id}."
    
                    # verify the subscription
                    verify_subscription(binder)
                else
                    raise BadRequestException.new "Created By required" if params[:created_by].nil?
                    raise BadRequestException.new "#{hb_request.user.email} is not a partner" if !hb_request.user.belongs_to_partner?
                    
                    # retrieve all the partner users
                    partner = hb_request.user.partners.where(:partner_type => hb_request.user.role).first
                    user_ids = partner.users.where(:role => partner.partner_type).ids
                    
                    # retrieve ids for all partner_user owned binders
                    binder_ids = partner.binders.joins(:transfers).where({:transfers => {:status => "created"}}).ids
                    
                    params[:created_by] = user_ids
                    params[:binder_id] = binder_ids
                end
            end

            super(hb_request, params)
        end

        def build(hb_request, params)
            raise BadRequestException.new "#{self.name.demodulize.titleize} required" if params[symbol].nil?
            
            # get the tag list
            tag_list = params[symbol][:tags_attributes] if params[symbol].has_key?(:tags_attributes)
            params[symbol].delete(:tags_attributes) if params[symbol].has_key?(:tags_attributes)
            
            # create the new resource
            item = self.new(params[symbol].permit!)
            item.created_by = hb_request.user.id unless !item.respond_to?(:created_by)

            # get the binder being added to
            binder = Binder.find(item.binder_id)

            # verify the user can create items in the binder
            hb_request.ability.authorize! :write, binder, :message => "You are not authorized to write binder ID: #{binder.id}."
            
            # save the resource
            if item.save
                # update typeahead values
                Typeahead.add_typeahead_values(item) unless !item.respond_to?(:typeahead)

                # create binder item
                if  item.respond_to?(:verified) && !item.verified?
                    item.create_binder_item(:binder => item.binder, :user => hb_request.user)
                end
                
                if item.respond_to?(:tags)
                    if tag_list && tag_list.any?
                        tags = []
                        tag_list.each do |tag|
                            tags.push(tag.to_unsafe_h)
                        end
                        Tags::SetResourcesTagsJob.perform_async(item.class.name, item.id, tags)
                    end
                end
                
                EventService.perform_async({:event_type => "engagement", :event_name => "#{item.class.table_name.singularize} created", :user_id => hb_request.user.id, :user_role => hb_request.user.role, :user_created_at => hb_request.user.created_at, :user_create_method => hb_request.user.create_method})
            else
                raise UnprocessableException.new(item)
            end
            
            Binders::CompletenessJob.perform_async(item.binder_id)

            return item
        end

        def update(hb_request, params)
            raise BadRequestException.new "#{self.name.demodulize.titleize} ID required" if params[:id].nil?
            raise BadRequestException.new "#{self.name.demodulize.titleize} required" if params[symbol].nil?
            
            # get the tag list
            tag_list = params[symbol][:tags_attributes] if params[symbol].has_key?(:tags_attributes)
            params[symbol].delete(:tags_attributes) if params[symbol].has_key?(:tags_attributes)
            
            # get the item being updated
            item = show(hb_request, params)

            # check if the user can update the item
            hb_request.ability.authorize! :write, item, :message => "You are not authorized to access #{item.class.name} ID: #{item.id}."

            # update
            if item.update_attributes(params[symbol].permit!)
                # update the typeahead values
                Typeahead.add_typeahead_values(item)
                EventService.perform_async({:event_type => "engagement", :event_name => "#{item.class.table_name.singularize} created", :user_id => hb_request.user.id, :user_role => hb_request.user.role, :user_created_at => hb_request.user.created_at, :user_create_method => hb_request.user.create_method})
                
                if item.respond_to?(:tags)
                    if tag_list && tag_list.any?
                        tags = []
                        tag_list.each do |tag|
                            tags.push(tag.to_unsafe_h)
                        end
                        Tags::SetResourcesTagsJob.perform_async(item.class.name, item.id, tags)
                    end
                end
                
                return item
            else
                raise UnprocessableException.new(item)
            end
        end

        def destroy(hb_request, id)
            # check for required fields
            raise BadRequestException.new "#{self.name.demodulize.titleize} ID required" unless id.present?

            item = find_by_id(id)
            return if item.nil?

            hb_request.ability.authorize! :destroy, item, :message => "You are not authorized to delete #{item.class.name} ID: #{item.id}."

            item.remove

            tagName = "#{self.name.demodulize.downcase}_#{id}"
            Tags::DeleteTagReferencesJob.perform_async(tagName)
            EventService.perform_async({:event_type => "engagement", :event_name => "#{item.class.table_name.singularize} deleted", :user_id => hb_request.user.id, :user_role => hb_request.user.role, :user_created_at => hb_request.user.created_at, :user_create_method => hb_request.user.create_method})
            Binders::CompletenessJob.perform_async(item.binder_id)
        end

        def add_custom_args(where, where_not, params, includes, joins)

            if params[:binderId]
                includes.push(:binder)
                where.merge!({:binders => {:id => params[:binderId]}})
            end

            # check if user is searching by name
            if params[:name]
                where.merge!({:name => params[:name]})
            end

            # check if user is searching by name
            if params[:verified]
                where.merge!({:verified => params[:verified]})
            end

            if params[:tag_id]
                includes.push(:tags)
                where.merge!({:tags => {:tag => params[:tag_id]}})
            end

            if params[:images]
                joins.push(:images)
            end

            super(where, where_not, params, includes, joins)
        end

        def verify_subscription(binder, index = true)
            if index
                raise SubscriptionError if binder.subscription.nil?
                raise SubscriptionError unless binder.subscription.plan_id == FREE || binder.subscription.payment_status == PAID
            else
                raise SubscriptionError if binder.subscription.nil?
                raise BadRequestException.new "Please upgrade your subscription to upload more items" if binder.subscription.plan_id === FREE && binder.total_storage > Rails.configuration.uploads[:max_size]
            end
        end
    end
end
