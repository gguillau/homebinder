# == Schema Information
#
# Table name: properties
#
#  id               :integer          not null, primary key
#  binder_id        :integer
#  property_type_id :integer
#  apn              :string(255)
#  country          :string(255)
#  address1         :string(255)
#  address2         :string(255)
#  city             :string(255)
#  state            :string(255)
#  zip              :string(255)
#  county           :string(255)
#  lat              :string(255)
#  long             :string(255)
#  details          :text
#  acres            :decimal(, )
#  created_by       :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  property_type    :string(255)
#  sq_ft            :integer
#  year_built       :integer
#

#require 'carmen'
#include Carmen

class Binder::Property < ApplicationRecord
  self.table_name = "properties"
  
  #resourcify
  
  # relationships
  belongs_to  :binder

  has_many    :notes, :dependent => :destroy

  # validations
  validates_length_of     :apn,       :maximum => 100
  validates_presence_of   :country, :message => I18n.t(:err_value_required)
  validates_length_of     :country,   :is => 2
  validates_presence_of   :address1, :message => I18n.t(:err_value_required)
  validates_length_of     :address1,  :maximum => 50
  validates_length_of     :address2,  :maximum => 50
  validates_presence_of   :city, :message => I18n.t(:err_value_required)
  validates_length_of     :city,      :maximum => 50
  validates_presence_of   :state, :message => I18n.t(:err_value_required)
  validates_length_of     :state,     :maximum => 10
  #validates_presence_of  :zip, :message => I18n.t(:err_value_required)
  validates_length_of     :zip,       :maximum => 20
  #validates_format_of     :county,    :with => /A[a-zA-Z]{0,50}z/,  :allow_blank => true
  #validates_format_of     :acres,     :with => /A\d+\.?\d{0,2}z/,   :allow_blank => true
  validates_presence_of   :binder, :message => I18n.t(:err_value_required), :on => :update
  
  before_save { |p| p.acres = p.acres.to_s }
  before_save { |p| p.country = p.country.upcase if p.country.present?}
  
  geocoded_by :full_address, :latitude  => :lat, :longitude => :long
  reverse_geocoded_by :lat, :long do |obj, results|
    if geo = results.first
        obj.zip = geo.postal_code
    end
  end
      
  before_save :set_valid_address, :if => Proc.new { ENV["ENABLE_GEOCODING"] === "true" }
  after_commit :set_custom_email, :on => [:create, :update]

  def set_custom_email
      binder.custom_support_email = "submissions+#{address1.gsub(/\s+/, "").first(8)}_#{binder.id}@homebinder.com"
      binder.save(:validate => false)
  end
    
  def set_valid_address
    return if lat.present?
    return if long.present?
    
    geocode
    reverse_geocode
  end
  
  def full_address
    "#{address1.titleize}, #{city.titleize}, #{state.upcase}"
  end
  
  def verify_state_and_country
    # check if country exists in Carmen
    ctry = self.country
    if ctry.present?
      country = Country[ctry]
      if country.nil?
        country = Country.find_country_by_name(ctry)
        self.country = country.gec unless country.nil?
      end
      
      if state && country
        # check if state/province exists in Carmen
        ste = self.state
        self.state  = country.subdivision(ste)
      end
    elsif state && country.nil?
      # look up the countries we currently support
      # US, CA, AS, GB, IE
      countries = []
      countries.push(Country["US"], Country["CA"], Country["AS"], Country["GB"], Country["IE"])
      ste = state

      countries.each do |ctr|
        state = ctr.subdivision(ste)
        next unless state.present?
        # set the state
        self.state = state
        self.country = ctr.gec
        break
      end
    end
  end
  
  def city_state
    "#{city}, #{state}"
  end
  
  settings do
    mappings dynamic: false do
      indexes :acres,           type: :integer
      indexes :address1,        type: :keyword
      indexes :address2,        type: :keyword
      indexes :city,            type: :keyword
      indexes :state,           type: :keyword
      indexes :zip,             type: :keyword
      indexes :country,         type: :keyword
      indexes :lat,             type: :integer
      indexes :long,            type: :integer
      indexes :created_at,      type: :date
      indexes :updated_at,      type: :date
      indexes :year_built,      type: :integer
      indexes :property_type,   type: :keyword
    end
  end
    
  def as_indexed_json(options={})
      self.as_json(
          {
              only: [:acres, :address1, :address2, :city, :state, :country, :lat, :long, :created_at, :updated_at, :year_built, :property_type],
              methods: [:city_state]
          }
      )
  end

end
