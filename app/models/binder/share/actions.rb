module Binder::Share::Actions

    def self.included(klass)
        klass.extend ClassMethods
    end

    # resend a share email
    def resend
        Shares::CreateJob.perform_async(id, true)
    end

    module ClassMethods

        ROLES = "role must be 'co_owner', 'reader', or 'writer'".freeze
        SHARE_TAG = "share".freeze
        WRITER = "writer".freeze
        READER = "reader".freeze
        CO_OWNER = "co_owner".freeze
        CLIENT_REQUIRED = "Client information required".freeze
        WITH_REQUIRED = "with is required".freeze
        ROLE_REQUIRED = "role is required".freeze
        PRIOR_ACCESS = "User with email %s already has access to the binder".freeze

        def base_query
            "CAST(shares.id as varchar(25)) LIKE :search OR
            CAST(shares.binder_id as varchar(25)) LIKE :search OR
            CAST(shares.shared_by_id as varchar(25)) LIKE :search OR
            CAST(shares.shared_with_id as varchar(25)) LIKE :search OR
                  LOWER(shares.status) LIKE :search OR
                  LOWER(users.email) LIKE :search OR 
                  LOWER(binders.name) LIKE :search".freeze
        end
    
        def base_includes
            [:sender, :receiver, :binder]
        end
        
        # create the share

        def build(hb_request, params)
            # verify the parameters
            raise BadRequestException.new CLIENT_REQUIRED if params[:share].nil?
            raise BadRequestException.new CLIENT_REQUIRED if params[:share][:user].nil?
            raise BadRequestException.new WITH_REQUIRED if params[:share][:user][:email].nil?
            raise BadRequestException.new ROLE_REQUIRED if params[:share][:role_name].nil?
            raise BadRequestException.new "Binder ID Required" if params[:share][:binder_id].nil?

            # find the binder
            binder = Binder.find(params[:share][:binder_id])
            
            raise BadRequestException.new "Cannot share an orphaned binder" if binder.status === "orphan"

            # check if the user can create the share
            hb_request.ability.authorize! :share, binder, :message => "You are not authorized to share binder ID: #{binder.id}."

            # get the user
            with_user = params[:share][:user]

            begin
                role_name = UserBinderRoles.map(params[:share][:role_name])
            rescue BadRequestException
                raise BadRequestException.new ROLES
            end

            # get the user to share with
            with_user = User.where(:email => params[:share][:user][:email].downcase).first

            # add the phone
            user_profile_attributes = {mobile_phone: params[:share][:user][:phone] }

            # if the user being shared with doesn't exist create them
            if with_user.nil?
               
                # set the user's address to the address for the binder
                user_profile_attributes[:address_attributes] = {
                    address1: binder.property.address1,
                    address2: binder.property.address2,
                    city: binder.property.city,
                    state: binder.property.state,
                    zip: binder.property.zip,
                    country: binder.property.country
                }
                
                # create the new user
                with_user = User.register_for_share({ :email => params[:share][:user][:email], :first_name => params[:share][:user][:first], :last_name => params[:share][:user][:last], :user_profile_attributes => user_profile_attributes})
            end

            share = {
                :binder_id => binder.id,
                :role_name => role_name,
                :shared_by_id => hb_request.user.id,
                :shared_with_id => with_user.id,
                :status => "created"
            }

            UserBinder.create!(user_id: with_user.id, binder_id: binder.id, role: role_name)

            # create the share
            share = Binder::Share.new(share)

            if not share.save
                raise UnprocessableException.new(share)
            end

            # share and send an email
            Shares::CreateJob.perform_async(share.id)
            
            share.status = "pending"
            share.save!

            return share
        end

        # resends a share to the receiver

        def resend(hb_request, params)
            # check that the correct params were passed in
            raise BadRequestException.new "Share ID Required" if params[:id].nil?

            # get the share
            share = find(params[:id])

            # find the binder
            binder = Binder.find(share.binder_id)
            
            raise BadRequestException.new "Cannot re-share an orphaned binder" if binder.status === "orphan"

            # make sure we can share the binder
            hb_request.ability.authorize! :share, binder, :message => "You are not authorized to share binder ID: #{binder.id}."

            # resend the share
            share.resend

            share.status = "sent"
            share.save!
        end
        
        def query_arguments
            ["shared_by_id", "shared_with_id", "binder_id", "status"]
        end
    
        def query_arguments_hash(query, value)
            case query
            when "shared_by_id", "binder_id", "shared_with_id", "status"
                {query.to_sym => value }
            end
        end
    end

end