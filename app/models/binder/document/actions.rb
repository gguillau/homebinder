module Binder::Document::Actions
  def self.included(klass)
    klass.extend ClassMethods
  end

  module ClassMethods

    TRUE = "true".freeze
    RTAG = "rtag".freeze
    SEMICOLON = ";".freeze
    DOC_NOT_FOUND = "Document template not found".freeze

    def build(hb_request, params)

      # check permissions
      binder = Binder.find(params[:binder_id])

      hb_request.ability.authorize! :write, binder, :message => "You are not authorized to access binder ID: #{binder.id}."
      
      verify_subscription(binder, false)
      
      verified = params[:verified].present? ? params[:verified]: true

      # create the document object
      if params[:document] and params[:document][:document_template_id]
        dt = Template::Document.find(params[:document][:document_template_id])
        notes = dt.notes.present? ? dt.notes : ""
        document = Binder::Document.new(binder_id: params[:binder_id], file: dt.file, details: notes, document_type: dt.document_type)
      else
        document = Binder::Document.new(
          binder_id: params[:binder_id],
          file: params[:file],
          verified: verified,
          structure_id: params[:structure_id],
          area_id: params[:area_id],
          maintenance_item_id: params[:maintenance_item_id],
          project_id: params[:project_id],
          appliance_id: params[:appliance_id],
          finish_id: params[:finish_id],
          paint_id: params[:paint_id],
          inventory_item_id: params[:inventory_item_id],
          receipt_id: params[:receipt_id],
          permit_id: params[:permit_id],
          binder_contractor_id: params[:binder_contractor_id],
          document_type_id: params[:document_type_id]
        )

        #get the tag list
        params.each {|k,v|
          if k.start_with? RTAG
            parts = v.split(SEMICOLON)
            document.tags.new(tag: parts[0], auto_generated: parts[1] == TRUE)
          end
        }

      end
      begin
        # save
        if not document.save
          raise UnprocessableException.new(document)
        end
      rescue => e
        case e
        when UnprocessableException
          raise UnprocessableException.new(document)
        else
          raise BadRequestException.new(e.message)
        end
      end

      # create binder item
      if  document.respond_to?(:verified) && !document.verified?
          document.create_binder_item(:binder => document.binder, :user => hb_request.user)
      end
      
      EventService.perform_async({:event_type => "engagement", :event_name => "#{document.class.table_name.singularize} created", :user_id => hb_request.user.id, :user_role => hb_request.user.role, :user_created_at => hb_request.user.created_at, :user_create_method => hb_request.user.create_method})

      return document
    end

    def query_arguments
        ["structure_id", "area_id", "maintenance_item_id", "project_id", "appliance_id", "finish_id", "paint_id", "inventory_item_id", "receipt_id", "permit_id", "binder_contractor_id", "document_type_id", "binder_id"]
    end

    def query_arguments_hash(query, value)
        case query
        when "structure_id", "area_id", "maintenance_item_id", "project_id", "appliance_id", "finish_id", "paint_id", "inventory_item_id", "receipt_id", "permit_id", "binder_contractor_id", "document_type_id", "binder_id"
            { query.to_sym => value }
        else
            {}
        end
    end

  end
end