# == Schema Information
#
# Table name: finish_models
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_by :integer
#  verified   :boolean
#

class Binder::Finish::Model < ApplicationRecord
    self.table_name = "finish_models"
    
    validates_presence_of   :name,    :message => I18n.t(:err_value_required)
end
