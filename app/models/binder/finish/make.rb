# == Schema Information
#
# Table name: finish_makes
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_by :integer
#  verified   :boolean
#

class Binder::Finish::Make < ApplicationRecord
  self.table_name = "finish_makes"
  
  validates_presence_of   :name,    :message => I18n.t(:err_value_required)
end
