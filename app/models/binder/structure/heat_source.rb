# == Schema Information
#
# Table name: heat_sources
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_by :integer
#  verified   :boolean
#

class Binder::Structure::HeatSource < ApplicationRecord
    self.table_name = "heat_sources"
    
    validates_presence_of   :name,          :message => I18n.t(:err_value_required)
    validates_presence_of   :created_by,    :message => I18n.t(:err_value_required)
end
