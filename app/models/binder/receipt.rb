# == Schema Information
#
# Table name: receipts
#
#  id         :integer          not null, primary key
#  binder_id  :integer
#  name       :string(255)
#  details    :text
#  deductable :boolean
#  created_by :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Binder::Receipt < ApplicationRecord
  include Binder::ComponentActions

  self.table_name = "receipts"

  belongs_to :binder
  
  has_one :binder_item, :dependent => :destroy
  has_one :purchase, :dependent => :destroy
  
  has_many :notes, :dependent => :destroy
  has_many :tags, :as => :taggable, :dependent => :destroy
  has_many :images, :dependent => :destroy
  has_many :documents, :dependent => :destroy

  accepts_nested_attributes_for :purchase

  validates_presence_of :name, :message => I18n.t(:err_value_required)
  validates_presence_of   :binder,   :message => I18n.t(:err_value_required)
  validates_length_of :name, :maximum => 50
end
