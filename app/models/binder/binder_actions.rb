module Binder::BinderActions

    ORPHAN_USER_EMAIL = "orphans@homebinder.com"

    def self.included(klass)
        klass.extend ClassMethods
    end

    def enable_maintenance_reminders
        Binders::EnableMaintenanceRemindersJob.perform_async(id)
    end

    def suppress_maintenance_reminders
        Binders::SuppressMaintenanceRemindersJob.perform_async(id)
    end

    def orphan
        orphan_user = User.where(email: ORPHAN_USER_EMAIL).first
        # only orphan the binder if the owner is not present
        if orphan_user.present? and UserBinder.where(:binder_id => self.id, :role => UserBinderRoles::OWNER).count < 1
            # create the user binder
            UserBinder.create!(user_id: orphan_user.id, binder_id: self.id, role: UserBinderRoles::OWNER)
            self.status = "orphan"
            self.save!

            # analytics
            EventService.perform_async({:event_name => "orphan", :event_type => "binder", :binder_id => id, :user_id => created_by})
        end
    end

    def pending_transfer
        self.transfers.where(:status => "sent").count > 0
    end

    # returns an array of the branding users for a certain scope
    # scope can be maintenance_email, recall_email, transfer_email, share_email

    def generate_branding(scope)
        # get the brandings where the users are not homeowner
        binder_brandings = self.binder_brandings.joins(:user_profile, :user_profile => :user).where(:scope => scope).where.not(:users => {:role => "homeowner"})
        # generate the brandings - gets the userprofile for each branding and removes any nil brandings
        brandings = binder_brandings.map{ |branding| branding.generate_branding }.compact

        brandings = brandings.uniq
        # sort the brandings accordingly: partner_user and then agent
        sort_order = ["inspector", "broker", "agent"]

        # do the sort and make sure we only return uniq users and not duplicates
        return brandings.sort_by {|branding| sort_order.index(branding[:role])}
    end

    def recalls
        return Binder::Appliance.joins(:appliance_recalls).where(:binder_id => self.id).count
    end

    def last_recall
        last = Recall.joins(:appliances).where(:appliances => {:binder_id => self.id}).order("recall_date DESC").limit(1)
        if (last.length > 0)
            return last[0].recall_date
        else
            return nil
        end
    end

    # for partners
    def transfer_status(user)
        # find the partner users
        users = Partner.get_partner_users(user.partners.first.id)

        # check if the binder has been shared and a transfer has not been sent
        if shares.where(:shared_by_id => users).count > 0 and transfers.where(:status => "created", :sender_id => users).count > 0
          shares.where(:shared_by_id => users).order(:id).pluck(:status).last
        # return the transfer status
        else
          transfers.where(:sender_id => users).order(:id).pluck(:status).last
        end
    end
    
    def recalculate_storage
        documents_total = documents.sum(:file_file_size)
        images_total = images.sum(:file_file_size)
        self.total_storage = images_total + documents_total
        self.save!
    end

    def remove
        Binders::RemovalJob.perform_async(id)
    end

    module ClassMethods

        PLUS_USERS_QUERY = "CAST(binders.id as varchar(25)) LIKE :search OR
                    LOWER(binders.name) LIKE :search OR
                    LOWER(CONCAT(properties.address1, ' ', properties.address2, ' ', properties.city, ', ', properties.state, ' ', properties.zip)) LIKE :search OR
                    LOWER(users.email) LIKE :search".freeze

        BASE_QUERY = "CAST(binders.id as varchar(25)) LIKE :search OR
                      LOWER(binders.name) LIKE :search OR
                      LOWER(CONCAT(properties.address1, ' ', properties.address2, ' ', properties.city, ', ', properties.state, ' ', properties.zip)) LIKE :search".freeze

        ALL = "all".freeze

        def build(hb_request, params)
            raise BadRequestException.new "Binder Required" if params[:binder].blank?
            
            # the UI currently sends the request wrapped in binder so we receive
            # binder => binder: {}, user_binders: [], partner_binders: []
            params = params[:binder]

            # setup the new binder
            binder = Binder.new(params[:binder].permit!)
            binder.created_by = hb_request.user.id

            if binder.save
                # we add user binders and subscription here so we can pass permissions
                # link the users
                if params[:user_binders]
                    params[:user_binders].each do |user_binder|
                        UserBinder.create!(user_id: user_binder[:user_id], binder_id: binder.id, role: user_binder[:role])
                    end
                end

                # set the subscription level for the binder to free
                Subscription.new(binder_id: binder.id, plan_id: "free").save!
                
                # check if we need to link to a partner
                if (hb_request.user.belongs_to_partner? and hb_request.user.role != "agent") || params[:partner_id].present?
                
                    # if an admin then we get the binder owner AKA the user who's branded on/owns the binder
                    hb_request.user = binder.owner if hb_request.user.is_admin?
                
                    partner = hb_request.user.partners[0] || Partner.find(params[:partner_id])
                    
                    # if the creator is a partner upgrade
                    if partner and partner.coupons.count > 0
                         BinderService.new(hb_request.user, binder).upgrade(partner.coupons[0].code)
                    end
                end
                EventService.perform_async({:event_type => "engagement", :event_name => "#{binder.class.table_name.singularize} created", :user_id => hb_request.user.id, :user_role => hb_request.user.role, :user_created_at => hb_request.user.created_at, :user_create_method => hb_request.user.create_method})
                BinderService.new(hb_request.user, binder).after_create(params)
            else
                raise UnprocessableException.new(binder)
            end
            return binder
        end

        def update(hb_request, params)
            binder = super(hb_request, params)

            # ensure binder is the only primary
            BinderService.new(hb_request.user, binder).check_primary

            # analytics
            Binders::BinderAnalytics.delay(:queue => "analytics").updated(binder.id)
            
            EventService.perform_async({:event_type => "engagement", :event_name => "#{binder.class.table_name.singularize} updated", :user_id => hb_request.user.id, :user_role => hb_request.user.role, :user_created_at => hb_request.user.created_at, :user_create_method => hb_request.user.create_method})
            
            Binders::CompletenessJob.perform_async(binder.id)
            
            return binder
        end

        # delete_binder either deletes the binders or removes the user link AND/OR removes the partner link

        def destroy(hb_request, id)
            # check for required fields
            raise BadRequestException.new "#{self.name} ID required" unless id.present?

            # get the binder
            binder = find_by_id(id)
            return if binder.nil?

            # get the user binder object so we can check the role
            user_binder = UserBinder.where(binder_id: binder.id, user_id: hb_request.user.id).first

            if user_binder.present?
                # the user is the owner, delete from the system
                if user_binder.role == UserBinderRoles::OWNER.to_s and !hb_request.user.is_admin?
                    # check if the user can delete it
                    hb_request.ability.authorize! :destroy, binder, :message => "You are not authorized to delete binder ID: #{binder.id}."
                    binder.remove
                else
                    # remove the user link
                    user_binder.remove
                end
            end

            # if the user belongs to a partner remove the partner link
            if hb_request.user.belongs_to_partner?
                PartnerBinder.where(partner_id: hb_request.user.partners[0].id, binder_id: binder.id).delete_all
            end

            # if the user is an admin remove the binder from the database in the background
            if hb_request.user.is_admin?
                binder.remove
            end
            
            EventService.perform_async({:event_type => "engagement", :event_name => "#{binder.class.table_name.singularize} deleted", :user_id => hb_request.user.id, :user_role => hb_request.user.role, :user_created_at => hb_request.user.created_at, :user_create_method => hb_request.user.create_method})
        end

        def pager(hb_request, params = {}, search_method = :for_admin)
            # comment out for now
            #return elasticsearch(hb_request, params) if params[:search]
            
            set_defaults

            raise Exception.new unless default_order_by.present?
            raise Exception.new unless default_order.present?

            # set the parameters
            item_page = params[:page].present? ? params[:page].to_i : page
            item_count = params[:count].present? ? params[:count].to_i : count
            offset = (item_page - 1) * item_count
            orderBy = params[:orderBy].present? ? params[:orderBy] : default_order_by
            order = params[:order].present? ? params[:order] : default_order
            search_method = params[:searchMethod] ? params[:searchMethod] : :for_admin

            # get the search method
            search_method_obj = self.method(search_method)

            # search
            total = search_method_obj.call(params, hb_request).count
            items = search_method_obj.call(params, hb_request)
                              .order("#{orderBy} #{order}")
                              .limit(item_count)
                              .offset(offset)

            # return the page
            return Page.new(items, total)
        end

        def for_admin(params = {}, hb_request)
            hb_request.ability.authorize! :read, Binder, :message => "You are not authorized to access these binders."
            # get the search value
            searchValue = params[:search]
            # set the includes/joins arrays
            includes = [:property, :users]
            joins = []

            # the custom args where/where_not clauses
            where = {}
            where_not = {}

            # check if any custom args were added
            add_custom_args(where, where_not, params, includes, joins)

            # search all binders in the system
            if searchValue.present?
                self.joins(joins)
                      .includes(includes)
                      .where(PLUS_USERS_QUERY, search: "%#{searchValue}%".downcase)
                      .where(where)
                      .where.not(where_not)
                      .references(includes)
            else
                self.joins(joins)
                      .includes(includes)
                      .where(where)
                      .where.not(where_not)
                      .all
            end
        end

        def for_user(params = {}, hb_request)
            raise BadRequestException.new "User ID Required" if params[:userId].nil?
            user = User.find(params[:userId])

            hb_request.ability.authorize! :read, user, :message => "You are not authorized to access the user account for user ID: #{user.id}."
            # get the search value
            searchValue = params[:search]
            # set the includes/joins arrays
            includes = [:property, :users]
            joins = []

            # the custom args where/where_not clauses
            where = {}
            where_not = {}

            add_custom_args(where, where_not, params, includes, joins)

            # search binders belonging to the user
            if searchValue.present?
                user.binders.joins(joins).includes(includes)
                    .where(BASE_QUERY, search: "%#{searchValue}%".downcase)
                    .where(where)
                    .where.not(where_not)
                    .references(includes)
            else
                user.binders.joins(joins)
                    .includes(includes)
                    .where(where)
                    .where.not(where_not)
            end
        end

        def for_partner(params = {}, hb_request)
            raise BadRequestException.new "Partner ID Required" if params[:partnerId].nil?

            # check if user can read the partner
            partner = Partner.find(params[:partnerId])
            hb_request.ability.authorize! :read, partner, :message => "You are not authorized to access this account for partner ID: #{partner.id}."

            # get the search value
            searchValue = params[:search]
            # set the includes/joins arrays
            includes = [:property, :users]
            joins = []

            # the custom args where/where_not clauses
            where = {}
            where_not = {}

            add_custom_args(where, where_not, params, includes, joins)

            # search a partners binders
            if searchValue.present?
                partner.binders.joins(joins)
                    .includes(includes)
                    .where(PLUS_USERS_QUERY, search: "%#{searchValue}%".downcase)
                    .where(where)
                    .where.not(where_not)
                    .references(:property, :users)
            else
                partner.binders.joins(joins)
                    .includes(includes)
                    .where(where)
                    .where.not(where_not)
            end
        end

        def add_custom_args(where, where_not, params, includes, joins)
            properties_hash = {}

            if params[:filter] === "shared"
                joins.push(:shares)
                ids = Partner.get_partner_users(params[:partnerId])
                where.merge!({:shares => {:status => "shared", :shared_by_id => ids}})
            end

            if params[:filter] === "transferred"
                joins.push(:transfers)
                where_not.merge!({:transfers => {:status => "created"}})
            end

            if params[:filter] === "created"
                # only show binders not sent
                joins.push(:transfers)
                where.merge!({:transfers => {:status => "created"}})
                
                # and binders not shared
                includes.push(:shares)
                where.merge!({:shares => {:id => nil}})
            end

            if params.key?(:userId)
                params[:userId] = params[:userId] === "null" ? nil : params[:userId]
                where.merge!({:users => {:id => params[:userId]}})
            end

            if params[:create_method]
                where.merge!({:create_method => params[:create_method]})
            end

            if params[:state]
                properties_hash.merge!({:state => params[:state]})
                where.merge!({:properties => properties_hash})
            end

            if params[:country]
                properties_hash.merge!({:country => params[:country]})
                where.merge!({:properties => properties_hash})
            end

            if params[:partnerId]
                includes.push(:partners)
                where.merge!({:partners => {:id => params[:partnerId]}})
            end
        end
    end

end
