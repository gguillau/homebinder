module Binder::Image::Actions
  def self.included(klass)
    klass.extend ClassMethods
  end

  module ClassMethods

    TRUE = "true".freeze
    RTAG = "rtag".freeze
    SEMICOLON = ";".freeze
    IMAGE_ERROR = "Error saving image".freeze
    BINDER_ERROR = "Error saving binder".freeze

    def build(hb_request, params)
      # check permissions
      binder = Binder.find(params[:binder_id])
      hb_request.ability.authorize! :write, binder, :message => "You are not authorized to access binder ID: #{binder.id}."
      
      verify_subscription(binder, false)
      
      verified = params[:verified].present? ? params[:verified]: true

      if !params[:base64].blank?
        file = StringIO.new(Base64.decode64(params[:base64]))
        file.class.class_eval { attr_accessor :original_filename }
        file.original_filename = params[:name]
        params[:file] = file
      end
      
      # create the image object
      image = Binder::Image.new(
        binder_id: params[:binder_id],
        file: params[:file],
        verified: verified,
        structure_id: params[:structure_id],
        area_id: params[:area_id],
        maintenance_item_id: params[:maintenance_item_id],
        project_id: params[:project_id],
        appliance_id: params[:appliance_id],
        finish_id: params[:finish_id],
        paint_id: params[:paint_id],
        inventory_item_id: params[:inventory_item_id],
        receipt_id: params[:receipt_id],
        permit_id: params[:permit_id],
        binder_contractor_id: params[:binder_contractor_id]
      )
  
      #get the tag list
      params.each {|k,v|
        if k.start_with? RTAG
          parts = v.split(SEMICOLON)
          image.tags.new(tag: parts[0], auto_generated: parts[1] == TRUE)
        end
      }

      # we may experience an internal server error from paperclip
      # or AWS so we wrap the image.save call in a begin/rescue to
      # catch any errors

      begin
        # save
        if not image.save
            raise UnprocessableException.new(image)
        end
      rescue => e
        case e
        when UnprocessableException
          raise UnprocessableException.new(image)
        else
          raise BadRequestException.new(e.message)
        end
      end

      # set the hero image?
      if params[:hero_image_id]
        binder.hero_image_id = image.id

        if not binder.save
          raise UnprocessableException.new(binder)
        end
      elsif binder.hero_image_id.nil? && image.non_binder_item_image?

        # set the image as the hero image automatically
        binder.hero_image_id = image.id
        
        if not binder.save
          raise UnprocessableException.new(binder)
        end
      end

      # create binder item
      if  image.respond_to?(:verified) && !image.verified?
          image.create_binder_item(:binder => image.binder, :user => hb_request.user)
      end
      
      EventService.perform_async({:event_type => "engagement", :event_name => "#{image.class.table_name.singularize} created", :user_id => hb_request.user.id, :user_role => hb_request.user.role, :user_created_at => hb_request.user.created_at, :user_create_method => hb_request.user.create_method})

      return image
    end

    def update(hb_request, params)
      # check permissions
      binder = Binder.find(params[:image][:binder][:id])
      hb_request.ability.authorize! :write, binder, :message => "You are not authorized to access binder ID: #{binder.id}."

      # get the image to update
      image = Binder::Image.find(params[:image][:id])

      binder.hero_image_id = image.id

      # save
      if not binder.save
        raise UnprocessableException.new(binder), BINDER_ERROR
      end
      
      EventService.perform_async({:event_type => "engagement", :event_name => "#{image.class.table_name.singularize} created", :user_id => hb_request.user.id, :user_role => hb_request.user.role, :user_created_at => hb_request.user.created_at, :user_create_method => hb_request.user.create_method})

      return image
    end

    def query_arguments
        ["structure_id", "area_id", "maintenance_item_id", "project_id", "appliance_id", "finish_id", "paint_id", "inventory_item_id", "receipt_id", "permit_id", "binder_contractor_id"]
    end

    def query_arguments_hash(query, value)
        case query
        when "structure_id", "area_id", "maintenance_item_id", "project_id", "appliance_id", "finish_id", "paint_id", "inventory_item_id", "receipt_id", "permit_id", "binder_contractor_id"
            { query.to_sym => value }
        else
            {}
        end
    end

  end
end