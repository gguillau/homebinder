# == Schema Information
#
# Table name: structures
#
#  id                    :integer          not null, primary key
#  binder_id             :integer
#  name                  :string(255)
#  year_built            :integer
#  beds                  :integer
#  baths                 :decimal(, )
#  sq_ft                 :integer
#  building_type_id      :integer
#  construction_style_id :integer
#  construction_type_id  :integer
#  heat_type_id          :integer
#  heat_source_id        :integer
#  details               :text
#  created_by            :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  building_type         :string(255)
#  construction_type     :string(255)
#  construction_style    :string(255)
#  heat_type             :string(255)
#  heat_source           :string(255)
#

class Binder::Structure < ApplicationRecord
  include Binder::ComponentActions

  self.table_name = "structures"

  #relationships
  belongs_to :binder

  has_one :binder_item, :dependent => :destroy

  has_many :notes, :dependent => :destroy
  has_many :tags, :as => :taggable, :dependent => :destroy
  has_many :images, :dependent => :destroy
  has_many :documents, :dependent => :destroy

  # validations
  validates_presence_of       :name, :message => I18n.t(:err_value_required)
  validates_presence_of       :binder, :message => I18n.t(:err_value_required)

  validates_length_of         :name, :maximum => 50
  validates_numericality_of   :year_built, :greater_than_or_equal_to => 1700, :less_than_or_equal_to => Date.today.year, :only_integer => true, :allow_nil => true
  validates_numericality_of   :sq_ft, :greater_than_or_equal_to => 0, :less_than_or_equal_to => 1000000, :only_integer => true, :allow_nil => true
  validates_numericality_of   :beds, :greater_than_or_equal_to => 0, :less_than_or_equal_to => 50, :only_integer => true, :allow_nil => true
  validates_numericality_of   :baths, :greater_than_or_equal_to => 0, :less_than_or_equal_to => 50, :allow_nil => true

  # one unique structure name per binder
  validates_uniqueness_of :name, scope: :binder_id

  def typeahead
    return [
      { :name => "building_type", :to => "Binder::Structure::BuildingType" },
      { :name => "construction_style", :to => "Binder::Structure::ConstructionStyle" },
      { :name => "construction_type", :to => "Binder::Structure::ConstructionType" },
      { :name => "heat_type", :to => "Binder::Structure::HeatType" },
      { :name => "heat_source", :to => "Binder::Structure::HeatSource" }
    ]
  end

end
