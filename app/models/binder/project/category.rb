# == Schema Information
#
# Table name: project_types
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_by :integer
#  verified   :boolean
#

class Binder::Project::Category < ApplicationRecord
    self.table_name = "project_types"
    
    validates_presence_of   :name,  :message => I18n.t(:err_value_required)
end
