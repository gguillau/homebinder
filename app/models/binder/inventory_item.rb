# == Schema Information
#
# Table name: inventory_items
#
#  id                     :integer          not null, primary key
#  binder_id              :integer
#  inventory_item_type_id :integer
#  name                   :string(255)
#  value_cents            :integer          default(0), not null
#  value_currency         :string(255)      default("USD"), not null
#  details                :text
#  created_by             :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  inventory_item_type    :string(255)
#

class Binder::InventoryItem < ApplicationRecord
  include Binder::ComponentActions

  self.table_name = "inventory_items"
  
  belongs_to  :binder

  has_one :binder_item, :dependent => :destroy
  
  monetize :value_cents, :allow_nil => true

  validates_presence_of   :name, :message => I18n.t(:err_value_required)
  validates_length_of     :name, :maximum => 50
  validates_presence_of   :binder, :message => I18n.t(:err_value_required)

  has_many :notes, :dependent => :destroy
  has_many :tags, :as => :taggable, :dependent => :destroy
  has_many :images, :dependent => :destroy
  has_many :documents, :dependent => :destroy

  # one unique inventory item name per binder
  validates_uniqueness_of :name, scope: :binder_id

  def typeahead
    return [
      { :name => "inventory_item_type", :to => "Binder::InventoryItem::Category" }
    ]
  end

end
