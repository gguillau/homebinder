# == Schema Information
#
# Table name: binder_transactions
#
#  id               :integer          not null, primary key
#  tracking_number  :string(100)
#  transaction_cost :money
#  transaction_type :string(20)
#  transaction_date :datetime
#  description      :string(1000)
#  binder_id        :integer          not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Binder::Transaction < ApplicationRecord
    include Binder::ComponentActions

    self.table_name = "binder_transactions"

     # associations
    belongs_to :binder

    has_one :partner_binder_transaction,    :foreign_key => "binder_transaction_id"
    has_one :user_binder_transaction,       :foreign_key => "binder_transaction_id"

    # validations
    validates_presence_of   :binder, :message => I18n.t(:err_value_required)

    def transaction_date_to_date
        return if self.transaction_date.nil?
        return self.transaction_date.to_date
    end
    
    def self.query_arguments
        ["binder_id", "transaction_type"]
    end

    def self.query_arguments_hash(query, value)
        case query
        when "binder_id", "transaction_type"
          {query.to_sym => value }
        end
    end
    
    def add_custom_args(where, where_not, params, includes, joins)
        if params[:partner_id]
            joins.push(:partner_binder_transaction)
            where.merge!({:partner_binder_transactions => {:partner_id => params[:partner_id]}})
        end
        super
    end
end
