# == Schema Information
#
# Table name: projects
#
#  id                :integer          not null, primary key
#  binder_id         :integer
#  project_type_id   :integer
#  project_status_id :integer
#  name              :string(255)
#  details           :text
#  start_date        :date
#  end_date          :date
#  cost_cents        :integer          default(0), not null
#  cost_currency     :string(255)      default("USD"), not null
#  created_by        :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  project_type      :string(255)
#  status            :string(255)
#

class Binder::Project < ApplicationRecord
  include Binder::ComponentActions

  self.table_name = "projects"

  belongs_to  :binder
  
  has_one :binder_item, :dependent => :destroy
  has_one :seller_report_item, :dependent => :destroy
  
  has_many :notes, :dependent => :destroy
  has_many :tags, :as => :taggable, :dependent => :destroy
  has_many :images, :dependent => :destroy
  has_many :documents, :dependent => :destroy

  validates_presence_of   :name, :message => I18n.t(:err_value_required)
  validates_length_of     :name, :maximum => 50
  validates_presence_of   :binder,   :message => I18n.t(:err_value_required)

  accepts_nested_attributes_for :seller_report_item

  monetize  :cost_cents, :allow_nil => true

  def typeahead
    return [{ :name => "project_type", :to => "Binder::Project::Category" }]
  end

  def get_status
    ProjectStatus.find(self.project_status_id).name unless self.project_status_id.nil?
  end

end
