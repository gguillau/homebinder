# == Schema Information
#
# Table name: maintenance_items
#
#  id                   :integer          not null, primary key
#  binder_id            :integer
#  appliance_id         :integer
#  area_id              :integer
#  structure_id         :integer
#  maintenance_type_id  :integer
#  maintenance_cycle_id :integer
#  name                 :string(255)
#  details              :text
#  interval             :integer
#  created_by           :integer
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  maintenance_type     :string(255)
#  maintenance_cycle    :string(255)
#  email_notifications  :boolean          default(TRUE)
#  library_source_id    :integer
#

class Binder::MaintenanceItem < ApplicationRecord
  # attr_accessible :title, :body
  include Binder::ComponentActions
  include Binder::MaintenanceItem::Actions

  self.table_name = "maintenance_items"

  #resourcify

  belongs_to :binder

  has_many :maintenance_events, :class_name => "Binder::MaintenanceItem::MaintenanceEvent",
                                :dependent => :destroy
  has_many :notes, :dependent => :destroy
  has_many :tags, :as => :taggable, :dependent => :destroy
  has_many :images, :dependent => :destroy
  has_many :documents, :dependent => :destroy
  
  has_one :binder_item, :dependent => :destroy
  has_one :seller_report_item, :dependent => :destroy

  validates_presence_of :name, :message => I18n.t(:err_value_required)
  validates_presence_of :binder, :message => I18n.t(:err_value_required)

  validates_length_of :name, :maximum => 100
  validates_length_of :details, :maximum => 5000
  validates_numericality_of :interval, :greater_than_or_equal_to => 1, :allow_blank => true

  # one unique maintenance_item name per binder
  validates_uniqueness_of :name, scope: :binder_id

  accepts_nested_attributes_for :seller_report_item

  #before_create { |item| item.due_on = item.start_on }
  after_save :update_do_date

  scope :filters, -> { where("lower(name) LIKE '%filter%'") }
  scope :filter_easy_items, -> { where("(lower(name) LIKE '%hvac%' OR lower(name) LIKE '%central%' OR lower(name) LIKE '%air%') AND lower(name) LIKE '%filter%'") }

  def typeahead
    return [{ name: "maintenance_type", :to => "Binder::MaintenanceItem::Category" }]
  end

  def do_date
    if self.maintenance_events.count == 0
      dt = Date.current
      dt = dt.advance(:years => 1)
      return dt
    else
      self.maintenance_events.last.do_date
    end
  end

  def do_date=(date)
    @do_date = date
  end

  def update_do_date
    if @do_date.nil?
      return
    end
    if self.maintenance_events.count == 0
      evt = Binder::MaintenanceItem::MaintenanceEvent.new(maintenance_item_id: self.id, created_by: self.created_by, do_date: @do_date)
      evt.save
    else
      evt = self.maintenance_events.where(:completed_date => nil).first
      evt.do_date = @do_date
      evt.save
    end
  end

  def next_event_date(from_date)
    cycle = self.maintenance_cycle
    if cycle.nil?
      return
    end

    d = from_date
    if from_date.nil?
      d = Date.today
    end

    d = d.advance(:days => self.interval) if cycle == "Days"
    d = d.advance(:weeks => self.interval) if cycle == "Weeks"
    d = d.advance(:months => self.interval) if cycle == "Months"
    d = d.advance(:years => self.interval) if cycle == "Years"

    # make sure the next event date is greater than today
    if d <= Date.today
      d = Date.today.advance(:days => self.interval) if cycle == "Days"
      d = Date.today.advance(:weeks => self.interval) if cycle == "Weeks"
      d = Date.today.advance(:months => self.interval) if cycle == "Months"
      d = Date.today.advance(:years => self.interval) if cycle == "Years"
    end

    return d
  end

  def schedule_next_event
    return if self.interval.nil? or return if self.maintenance_cycle.nil?
    # do not create a new event if the item maintenance cycle is As Needed
    return if self.maintenance_cycle === "As Needed"
    # do not create a new event if the item maintenance cycle is Once
    return if self.maintenance_cycle === "Once"
    # do not create a new event if there is a future event scheduled and the completed date is nil
    return if self.maintenance_events.where("do_date > ?", Date.today.tomorrow).where(:completed_date => nil).count > 0

    # get the last notified event
    event = last_notified_event

    # set the next due_date
    next_due_date = next_event_date(Date.today) if event.nil?
    next_due_date = event.do_date if event.present?

    # create the event
    Binder::MaintenanceItem::MaintenanceEvent.new(maintenance_item_id: self.id, created_by: self.created_by, do_date: next_event_date(next_due_date)).save
  end

  def first_event
    self.maintenance_events.where(:completed_date => nil).first
  end

  def next_event
    self.maintenance_events.where(:completed_date => nil).last
  end

  def last_notified_event
    self.maintenance_events.where("last_notify_date IS NOT NULL").last
  end

  def last_completed_event
    self.maintenance_events.where("completed_date IS NOT NULL").order("completed_date DESC").first
  end

  def get_next_event_date
    evt = self.maintenance_events.where(:completed_date => nil).first
    return evt.nil? ? nil : evt.do_date
  end

  def get_last_completed_date
    evt = self.maintenance_events.where("completed_date IS NOT NULL").order("completed_date DESC").first
    return evt.nil? ? nil : evt.completed_date
  end

  def get_last_notify_date
    evt = self.maintenance_events.where(:last_notify_date => nil).first
    return evt.nil? ? nil : evt.last_notify_date
  end

  def filter_easy_eligible?
    return false if binder.property.country.downcase === "ca"
    return (name.downcase.include?("hvac") ||
        name.downcase.include?("central") ||
        name.downcase.include?("a/c") ||
        name.downcase.include?("ac") ||
        name.downcase.include?("furnace") ||
        name.downcase.include?("heating") ||
        name.downcase.include?("air")) &&
        name.downcase.include?("filter")
  end
  
  settings do
    mappings dynamic: false do
      indexes :binder_id, type: :integer
    end
  end
    
  def as_indexed_json(options={})
    self.as_json(
      {
        only: [:binder_id]
      }
    )
  end
end
