# == Schema Information
#
# Table name: images
#
#  id                :integer          not null, primary key
#  imageable_id      :integer
#  imageable_type    :string(255)
#  location          :string(255)
#  key               :string(255)
#  bucket            :string(255)
#  etag              :string(255)
#  details           :text
#  created_by        :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  binder_id         :integer
#  file_size         :integer
#  name              :string(255)
#  file_file_name    :string(255)
#  file_content_type :string(255)
#  file_file_size    :integer
#  file_updated_at   :datetime
#

require 'uri'

class Binder::Image < ApplicationRecord
  include Binder::ComponentActions
  include Binder::Image::Actions

  self.table_name = "images"

  #resourcify

  has_many :tags, :as => :taggable, :dependent => :destroy
  
  has_one :binder_item, :dependent => :destroy
  has_one :seller_report_item, :dependent => :destroy
  has_one :hero_image_id, :dependent => :nullify, class_name: "Binder", foreign_key: "hero_image_id"

  belongs_to :binder

  accepts_nested_attributes_for :seller_report_item, :binder

  has_attached_file :file,
    :path => "/binder:binder_id/images/:style/:filename",
    :source_file_options => { all: '-auto-orient' },
    :convert_options => { :all => "-quality 100" },
    :styles => {
      :thumb => ["75x75>",:jpg],
      :medium => ["350x350>",:jpg],
      :large => ["800x800>", :jpg]
    }

  validates_presence_of :binder, :message => I18n.t(:err_value_required)
  do_not_validate_attachment_file_type :file
  # validates_attachment :file, :content_type => {
  #   :content_type => [
  #     "image/jpeg",
  #     "image/jpg",
  #     "image/png",
  #     "image/gif",
  #     "image/bmp"]}

  # ".png", ".JPG", ".jpg", ".PNG", ".jpeg", ".bmp", ".JPEG"
  # Validate filename
  #validates_attachment_file_name :file, matches: [/png\z/i, /jpe?g\z/i, /jpg\z/i, /bmp\z/i, /gif\z/i], message: "%{value} is invalid"

  validates_uniqueness_of :file_file_name, scope: :binder_id
  
  after_create-> { increment_binder_storage_size }
  after_destroy-> { decrement_binder_storage_size }
  
  def increment_binder_storage_size
    return if file_file_size.blank?
    self.binder.total_storage += file_file_size
    self.binder.save!
  end
  
  def decrement_binder_storage_size
    return if file_file_size_was.blank?
    self.binder.total_storage -= file_file_size_was
    self.binder.save!
  end

  def file_url(url)
    encoded_url = URI.encode(url)
    self.file = URI.parse(encoded_url)
  end

  def non_binder_item_image?
    return structure_id.nil? && area_id.nil? && maintenance_item_id.nil? && 
            project_id.nil? && appliance_id.nil? && finish_id.nil? &&
            paint_id.nil? && inventory_item_id.nil? && receipt_id.nil? &&
            permit_id.nil? &&binder_contractor_id.nil?
  end

end
