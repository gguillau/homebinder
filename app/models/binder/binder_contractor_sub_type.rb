class Binder::BinderContractorSubType < ApplicationRecord
    self.table_name = "binder_contractor_sub_types"
    
    belongs_to  :binder_contractor
    belongs_to  :contractor_sub_category,   :class_name => "Contractor::SubCategory"
    
    # validations
    validates_uniqueness_of     :binder_contractor, scope: :contractor_sub_category, :message => "already exists"
    validates_presence_of       :binder_contractor, :message => I18n.t(:err_value_required), :on => :update
    validates_presence_of       :contractor_sub_category, :message => I18n.t(:err_value_required)
end
