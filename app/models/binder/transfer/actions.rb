module Binder::Transfer::Actions

    def self.included(klass)
        klass.extend ClassMethods
    end

    # resend a transfer email
    def resend
        TransferService.new(id).send_email(true, nil)
    end

    module ClassMethods

        include Binder::Transfer::Constants
        
        def build(hb_request, params)
            transfer = super
            transfer.status = "created"
            transfer.save!
            return transfer
        end

        # executes a transfer

        def execute(hb_request, params)
            # check that the correct params were passed in
            raise BadRequestException.new "Transfer ID Required" if params[:id].nil?
            raise BadRequestException.new I18n.t(:err_transfer_to_required) if params[:user].nil?
            raise BadRequestException.new I18n.t(:err_transfer_type_required) if params[:transfer_type].nil?
            raise BadRequestException.new I18n.t(:err_transfer_type_not_owner_copy) if params[:transfer_type] != TRANSFER_TYPE_COPY and params[:transfer_type] != TRANSFER_TYPE_OWNER

            # get the params
            to_user = params[:user]
            transfer_type = params[:transfer_type]
            cc_transfer_by = params[:cc_transfer_by]

            # get the transfer
            transfer = find(params[:id])

            # find the binder
            binder = transfer.binder
            
            raise BadRequestException.new "Cannot transfer an orphaned binder" if binder.status === "orphan"

            # make sure we can transfer the binder
            hb_request.ability.authorize! :transfer, binder, :message => "You are not authorized to transfer binder ID: #{binder.id}."

            # update the receiver information
            TransferService.new(transfer.id).update_receiver(to_user)

            # do the transfer
            if transfer_type == TRANSFER_TYPE_COPY
                TransferService.new(transfer.id).transfer_copy
            elsif transfer_type == TRANSFER_TYPE_OWNER
                TransferService.new(transfer.id).transfer_ownership
            end

            # supress maintenance reminders
            binder.suppress_maintenance_reminders

            # notify the user of a transfer
            TransferService.new(transfer.id).send_email(false, cc_transfer_by)

            # transfer analytics
            TransferService.new(transfer.id).analytics
            
            transfer.status = "pending"
            transfer.save!
            
            # return the transfer
            return transfer
        end

        # resends a transfer to the receiver

        def resend(hb_request, params)
            # check that the correct params were passed in
            raise BadRequestException.new "Transfer ID Required" if params[:id].nil?
            raise BadRequestException.new I18n.t(:err_transfer_to_required) if params[:user].nil?
            
            # get the params
            to_user = params[:user]
            
            # get the transfer
            transfer = find(params[:id])

            # find the binder
            binder = Binder.find(transfer.binder_id)
            
            raise BadRequestException.new "Cannot re-transfer an orphaned binder" if binder.status === "orphan"
            
            # make sure we can transfer the binder
            hb_request.ability.authorize! :transfer, binder, :message => "You are not authorized to transfer binder ID: #{binder.id}."

            # update the receiver information
            TransferService.new(transfer.id).update_receiver(to_user)

            # resend the transfer
            transfer.resend
            
            transfer.status = "pending"
            transfer.save!
        end
        
        def cancel(hb_request, params)
            # check that the correct params were passed in
            raise BadRequestException.new "Transfer ID Required" if params[:id].nil?

            # get the transfer
            transfer = find(params[:id])

            # find the binder
            binder = Binder.find(transfer.binder_id)
            
            raise BadRequestException.new "Cannot cancel a transfer for an orphaned binder" if binder.status === "orphan"

            # make sure we can write the binder
            hb_request.ability.authorize! :write, binder

            # cancel the transfer - transfer back ownership
            TransferService.new(transfer.id).cancel
            
            transfer.status = "created"
            transfer.save!
        end
        
        def base_query
            "CAST(transfers.id as varchar(25)) LIKE :search OR
            CAST(transfers.binder_id as varchar(25)) LIKE :search OR
            CAST(transfers.sender_id as varchar(25)) LIKE :search OR
            CAST(transfers.receiver_id as varchar(25)) LIKE :search OR
                  LOWER(transfers.status) LIKE :search OR
                  LOWER(users.email) LIKE :search OR 
                  LOWER(binders.name) LIKE :search".freeze
        end
    
        def base_includes
            [:sender, :receiver, :binder]
        end
        
        def query_arguments
            ["receiver_id", "sender_id", "binder_id", "status", "transfer_type"]
        end
    
        def query_arguments_hash(query, value)
            case query
            when "sender_id", "binder_id", "receiver_id", "status", "transfer_type"
                {query.to_sym => value }
            end
        end
    end

end