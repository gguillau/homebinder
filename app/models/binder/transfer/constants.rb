module Binder::Transfer::Constants
    TO = "to".freeze
    TRANSFER_TYPE_COPY = "copy".freeze
    TRANSFER_TYPE_OWNER = "ownership".freeze
    TRANSFER_TAG = "transfer".freeze
    CLIENT_TAG = "client".freeze
    KEY_LAST_NOTIFY = "last_notify".freeze
    KEY_DATE = "date".freeze
    KEY_NOTIFY_COUNT = "notify_count".freeze
    KEY_FROM = "from".freeze
    KEY_TO_ID = "to_id".freeze
    KEY_NEXT_REMINDER = "next_reminder".freeze
    QUERY_PARTNER_TAG = "LOWER(tag) = :partner".freeze
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
    INVALID_EMAIL = "Invalid Email".freeze
    TIME_ZONE = "Eastern Time (US & Canada)".freeze
    EMPTY = "".freeze
    TAG_ERROR = "Unable to find transfer tag".freeze
    SAME_USER_TRANSFER = "User cannot transfer binder to self".freeze
end