# == Schema Information
#
# Table name: inventory_item_types
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_by :integer
#  verified   :boolean
#

class Binder::InventoryItem::Category < ApplicationRecord
  self.table_name = "inventory_item_types"
  
  validates_presence_of   :name,    :message => I18n.t(:err_value_required)
end
