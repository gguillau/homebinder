# == Schema Information
#
# Table name: binder_contractors
#
#  id                :integer          not null, primary key
#  binder_id         :integer
#  contractor_id     :integer
#  created_by        :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  details           :text
#  account_number    :string(255)
#  contact           :string(255)
#  library_source_id :integer
#

class Binder::BinderContractor < ApplicationRecord
  include Binder::ComponentActions
  include Binder::BinderContractor::Actions

  self.table_name = "binder_contractors"

  belongs_to :binder
  belongs_to :contractor
  belongs_to :user_contractor
  belongs_to :contractor_template, :class_name => "Template::Contractor", :foreign_key => "library_source_id"
  belongs_to :creator, :class_name => "User", :foreign_key => "created_by"
  
  delegate :binder_template, to: :contractor_template
  delegate :partner_configuration, to: :binder_template
  delegate :partner, to: :partner_configuration
  
  has_many    :permits
  has_many    :shares, :as => :sharable
  has_many    :notes, :as => :notable
  has_many    :tags, :as => :taggable, :dependent => :destroy
  
  has_many :images, :dependent => :destroy
  has_many :documents, :dependent => :destroy
  
  has_many    :binder_contractor_types, :class_name => "Binder::BinderContractorType", :dependent => :destroy
  has_many    :types, :through => :binder_contractor_types, :source => "contractor_category"
  has_many    :binder_contractor_sub_types, :class_name => "Binder::BinderContractorSubType", :dependent => :destroy
  has_many    :sub_types, :through => :binder_contractor_sub_types, :source => "contractor_sub_category"
  
  has_one :binder_item, :dependent => :destroy
  has_one :seller_report_item, :dependent => :destroy

  validates_presence_of   :binder,      :message => I18n.t(:err_value_required)
  validates_presence_of   :contractor,  :message => I18n.t(:err_value_required)
  validates_uniqueness_of :contractor,  scope: :binder, :message => "already exists for this binder"
  validates_presence_of   :created_by,  :message => I18n.t(:err_value_required)
  
  accepts_nested_attributes_for :contractor
  accepts_nested_attributes_for :seller_report_item
  accepts_nested_attributes_for :binder_contractor_types
  accepts_nested_attributes_for :binder_contractor_sub_types
  accepts_nested_attributes_for :types
  accepts_nested_attributes_for :sub_types
  
  after_create    :send_invitation
  before_destroy  :remove_permits
  
  # make sure to downcase emails
  before_validation :format_email
  before_validation :valid_types
  
  def valid_types
    if types.empty? && binder_contractor_types.empty?
      errors.add(:types, "- Minimum of 1 type per pro required")
    end
  end
  
  def send_invitation
    return if not send_email_invitation
    return if contractor.nil?
    return if contractor.email.blank?
    return if contractor.created_at.to_i < created_at.to_i
    Users::CreateHomeProJob.perform_async(contractor_id)
  end
  
  def format_email
    return if secondary_email.nil?
    self.secondary_email = Partner::Automation::EmailParser.new.format_value(["", "email"], secondary_email.downcase.gsub(/\s+/, ""))
    self.secondary_email = secondary_email =~ /\A[^@\s]+@[^@\s]+\z/ ? secondary_email : ""
  end

  def name
    self.contractor.name
  end

  private

  def remove_permits
    Binder::Permit.where(:binder_contractor_id => self.id).update_all(:binder_contractor_id => nil)
  end

end
