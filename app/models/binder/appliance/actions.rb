module Binder::Appliance::Actions
  def self.included(klass)
    klass.extend ClassMethods
  end

  module ClassMethods

    def build(hb_request, params)
      appliance = super(hb_request, params)

      # do a recall check. The recall service determines if it's allowed
      Recalls::DownloadJob.new.check_for_appliance_recall(appliance)

      return appliance
    end

    def update(hb_request, params)
      appliance = super(hb_request, params)

      # do a recall check. The recall service determines if it's allowed
      Recalls::DownloadJob.new.check_for_appliance_recall(appliance)

      return appliance
    end

    def send_test_recall_email(current_user, id)
      # find the item
      item = Binder::Appliance.find(id)

      # send the email
      RecallMailer.notify_of_recalls(item.id, current_user.id, "", "confirmed_recall").deliver_later
    end

    def check_for_recalls(current_user, id)
      appliance = Binder::Appliance.find(id)
      Recalls::DownloadJob.new.check_for_appliance_recall(appliance)
      return appliance.appliance_recalls
    end

    def query_arguments
        ["no_model", "no_make", "no_serial"]
    end

    def query_arguments_hash(query, value)
        case query
        when "no_model"
          { :model => nil }
        when "no_make"
          { :manufacturer => nil }
        when "no_serial"
            { :serial_no => nil }
        else
            {}
        end
    end

  end
end