# == Schema Information
#
# Table name: appliance_manufacturers
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_by :integer
#  verified   :boolean
#

class Binder::Appliance::Manufacturer < ApplicationRecord
    self.table_name = "appliance_manufacturers"
    
    validates_presence_of   :name,    :message => I18n.t(:err_value_required)
end
