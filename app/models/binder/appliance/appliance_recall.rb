# == Schema Information
#
# Table name: appliance_recalls
#
#  id            :integer          not null, primary key
#  appliance_id  :integer
#  recall_id     :integer
#  strength      :integer
#  status        :string(25)
#  status_date   :datetime
#  notifications :integer
#

class Binder::Appliance::ApplianceRecall < ApplicationRecord
    self.table_name = "appliance_recalls"
    
    # associations
    belongs_to :appliance
    belongs_to :recall
    
    # validations
    validates_presence_of   :appliance,    :message => I18n.t(:err_value_required)
    validates_presence_of   :recall,       :message => I18n.t(:err_value_required)
    
    scope :approved, -> {where(:status => "verified", :sub_category => "pending")}
    
    def self.base_query
        "CAST(appliance_recalls.id as varchar(25)) LIKE :search OR LOWER(appliances.model_number) LIKE :search OR
        LOWER(appliances.name) LIKE :search OR LOWER(appliances.manufacturer) LIKE :search OR
        LOWER(appliances.model) LIKE :search OR LOWER(appliances.serial_no) LIKE :search OR
        LOWER(binders.name) LIKE :search".freeze
    end
    
    def self.base_includes
        [:appliance, :recall, :appliance => :binder]
    end
    
    def self.query_arguments
        ["status"]
    end

    def self.query_arguments_hash(query, value)
        case query
        when "status"
            {:status => value}
        else
            {}
        end
    end
    
    def notify
        if status === "verified" and notifications < 3
            user = User.with_role(:owner, appliance.binder).where(:role => "homeowner").first
            unless user.nil?
                RecallMailer.notify_of_recalls(appliance.id, user.id, recall.recall_url, category).deliver_later
                self.notifications += 1
                self.save!
    
                # analytics
                EventService.perform_async({:event_name => "recall_notification", :event_type => "email", :appliance_id => appliance.id, :user_id => user.id, :user_created_at => user.created_at, :user_role => user.role})
            end
        end
    end
    
    def self.add_custom_args(where, where_not, params, includes, joins)
        if params[:binderId]
            joins.push(:appliance => :binder)
            where.merge!({:binders => {:id => params[:binderId]}})
        end
        super
    end
end
