# == Schema Information
#
# Table name: area_types
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_by :integer
#  verified   :boolean
#

class Binder::Area::Category < ApplicationRecord
    self.table_name = "area_types"
    
    validates_presence_of   :name,    :message => I18n.t(:err_value_required)
end
