# == Schema Information
#
# Table name: binder_brandings
#
#  id               :integer          not null, primary key
#  scope            :string(255)      not null
#  binder_id        :integer          not null
#  user_branding_id :integer          not null
#  partner_id       :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Binder::BinderBranding < ApplicationRecord
    include Binder::ComponentActions
    self.table_name = "binder_brandings" 
    
    belongs_to :user_profile, :foreign_key => "user_branding_id"
    belongs_to :binder
    belongs_to :partner

    validates_presence_of       :scope,             {:message => I18n.t(:err_value_required)}
    validates_presence_of       :binder_id,             {:message => I18n.t(:err_value_required)}
    validates_presence_of       :user_profile,  {:message => I18n.t(:err_value_required)}
    
    validates_numericality_of   :user_branding_id,  :only_integer => true, :allow_nil => false
    validates_numericality_of   :partner_id,        :only_integer => true, :allow_nil => true
    
    # one unique area name per binder
    validates_uniqueness_of :user_profile, scope: [:binder, :scope]
    
    # branding name is limited to 255 characters
    validates :scope, length: { maximum: 255}
    
    before_validation :unique_scope
    before_validation :user_role
    before_validation :maximum_count, :on => :create
    
    after_create -> { add_business_card_widget }
    after_destroy -> { remove_business_card_widget }

    def unique_scope
        case self.scope
        when "binder","transfer_email","seller_report","share_email"
            if self.class.where.not(:id => self.id).where(:binder_id => self.binder_id, :scope => self.scope).count > 0
                errors.add(:scope, "#{self.scope} already exists")
            end
        when "recall_email", "maintenance_email"
            if self.class.where.not(:id => self.id).where(:binder_id => self.binder_id, :scope => self.scope).count > 1
                errors.add(:scope, "#{self.scope} maximum count reached")
            end
        end
    end
    
    def user_role
        if User.joins(:user_profile).where(:user_profiles => {:id => self.user_branding_id}, :role => "homeowner").count > 0
            errors.add(:user_branding_id, "ID belongs to a homeowner and homeowners cannot be added to a binder's branding")
        end
    end
    
    def maximum_count
        if Binder::BinderBranding.where(:binder_id => self.binder_id).count === 8
            errors.add(:binder_id, "#{self.binder_id} has reached maximum amount of binder brandings")
        end
    end
    
    # gets the branding user
    def generate_branding
        # check if the user exists
        if self.user_profile.present?
            self.user_profile.branding
        elsif self.partner_id.present?
            generate_partner_branding
        end
    end
    
    # gets the branding for the partner admin
    def generate_partner_branding
        if self.partner_id
            return nil unless self.partner.present?
            # get the partner admin
            admin = self.partner.get_admin
            # get the branding for the partner admin
            return admin.user_profile.branding
        end
    end
    
    # injects double business card widget after branding is added
    def add_business_card_widget
        if self.scope == "binder"
            doubleBusinessCardWidget = Widget.where(:key => "doubleBusinessCard").first
            return if doubleBusinessCardWidget.nil?
            
            user_binder = UserBinder.where(:role => "owner", :binder_id => self.binder_id).first
            return if user_binder.nil?
            
            dashboard_id = user_binder.dashboard_id
            return if dashboard_id.nil?
            
            return if WidgetExclusion.where(:partner_id => self.partner_id, :widget_id => doubleBusinessCardWidget.id).exists?
            
            if !DashboardWidget.where(:dashboard_id => dashboard_id, :widget_id => doubleBusinessCardWidget.id).exists?
                DashboardWidget.create!(:dashboard_id => dashboard_id, :widget_id => doubleBusinessCardWidget.id, :index => 1)
            end
        end
    end
    
    # removes double business card widget after branding is removed and no agent is present
    def remove_business_card_widget
        if self.scope == "binder" && !UserBinder.where(:binder_id => self.binder_id, :role => "buyer_agent").exists?
            doubleBusinessCardWidget = Widget.where(:key => "doubleBusinessCard").first
            return if doubleBusinessCardWidget.nil?
            
            user_binder = UserBinder.where(:role => "owner", :binder_id => self.binder_id).first
            return if user_binder.nil?
            
            dashboard_id = user_binder.dashboard_id
            return if dashboard_id.nil?
            
            DashboardWidget.where(:dashboard_id => dashboard_id, :widget_id => doubleBusinessCardWidget.id).destroy_all
        end
    end
end
