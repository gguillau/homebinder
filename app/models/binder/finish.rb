# == Schema Information
#
# Table name: finishes
#
#  id              :integer          not null, primary key
#  binder_id       :integer
#  name            :string(255)
#  finish_make_id  :integer
#  finish_model_id :integer
#  style_color     :string(255)
#  details         :text
#  created_by      :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  make            :string(255)
#  model           :string(255)
#  style           :string(100)
#  color           :string(50)
#

class Binder::Finish < ApplicationRecord
  include Binder::ComponentActions

  self.table_name = "finishes"

  belongs_to :binder

  has_one :binder_item, :dependent => :destroy
  has_one :purchase, :dependent => :destroy
  has_one :seller_report_item, :dependent => :destroy
  
  has_many :notes, :dependent => :destroy
  has_many :tags, :as => :taggable, :dependent => :destroy
  has_many :images, :dependent => :destroy
  has_many :documents, :dependent => :destroy

  validates_presence_of :name, :message => I18n.t(:err_value_required)
  validates_presence_of :binder, :message => I18n.t(:err_value_required)

  validates_length_of :name, :maximum => 50
  validates_length_of :make, :maximum => 50
  validates_length_of :model, :maximum => 50

  # one unique finish name per binder
  validates_uniqueness_of :name, scope: :binder_id

  accepts_nested_attributes_for :purchase
  accepts_nested_attributes_for :seller_report_item

  def typeahead
    return [
      { :name => "make", :to => "Binder::Finish::Make" },
      { :name => "model", :to => "Binder::Finish::Model" }
    ]
  end
end
