# == Schema Information
#
# Table name: paint_sheens
#
#  id         :integer          not null, primary key
#  name       :string(50)
#  created_by :integer
#  verified   :boolean
#

class Binder::Paint::Sheen < ApplicationRecord
    self.table_name = "paint_sheens"
    
    validates_presence_of   :name,          :message => I18n.t(:err_value_required)
    validates_presence_of   :created_by,    :message => I18n.t(:err_value_required)
end
