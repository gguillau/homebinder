# == Schema Information
#
# Table name: paint_types
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_by :integer
#  verified   :boolean
#

class Binder::Paint::Category < ApplicationRecord
    self.table_name = "paint_types"
    
    validates_presence_of   :name,          :message => I18n.t(:err_value_required)
    validates_presence_of   :created_by,    :message => I18n.t(:err_value_required)
end
