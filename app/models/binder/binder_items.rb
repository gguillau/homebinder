module Binder::BinderItems

    def self.included(klass)
        klass.extend ClassMethods
    end

    module ClassMethods

        def index(hb_request, params)
            if hb_request.user.role === "homeowner"
                raise BadRequestException.new "" unless params[:binderId].present?

                # get the binder
                binder = Binder.find(params[:binderId])

                # verify the user can read the binder
                hb_request.ability.authorize! :read, binder, :message => "You are not authorized to access binder ID: #{binder.id}."
            elsif hb_request.user.role != "admin"
                # to do - various use cases for contractors and partners
            end

            super(hb_request, params)
        end

        def update(hb_request, params)
            binder_item = super
            if binder_item.verified? && !binder_item.dismissed?
                item = binder_item.item
                item.verified = true
                item.save!
            end
        end

        def query_arguments
            ["binderId", "userId", "verified"]
        end

        def query_arguments_hash(query, value)
            case query
            when "binderId"
                {:binder_id => value}
            when "userId"
                {:user_id => value}
            when "verified"
                {:verified => value}
            end
        end
    end
end
