module Binder::BinderContractor::Actions
    def self.included(klass)
        klass.extend ClassMethods
    end

    module ClassMethods

        def build(hb_request, params)
            validate_types(params)
            validate_sub_types(params)
            super(hb_request, params)
        end

        def update(hb_request, params)
            validate_types(params)
            validate_sub_types(params)
            super(hb_request, params)
        end

        def contact(hb_request, params)
            raise BadRequestException.new "#{self.name.titleize} ID required" if params[:id].nil?

            # get the item
            item = find(params[:id])

            # check if the user can read it
            hb_request.ability.authorize! :read, item, :message => "You are not authorized to access #{self.name} ID: #{item.id}."

            BinderMailer.home_pro_work_request(hb_request.user.id, item.binder_id, item.contractor_id, item.binder.partner_ids.first, params[:comments]).deliver_later
            BinderMailer.home_pro_work_request_confirmation(hb_request.user.id, item.binder_id, item.contractor_id, item.binder.partner_ids.first, params[:comments]).deliver_later
            BinderMailer.home_pro_work_request_support(hb_request.user.id, item.binder_id, item.contractor_id, item.binder.partner_ids.first, params[:comments]).deliver_later

            EventService.perform_async({
                :event_type => "engagement",
                :event_name => "home_pro_contacted",
                :user_id => hb_request.user.id,
                :user_role => hb_request.user.role,
                :user_created_at => hb_request.user.created_at,
                :user_create_method => hb_request.user.create_method,
                :binder_contractor_id => item.id,
                :binder_id => item.binder_id
            })

            return item
        end
        
        def validate_types(params)
            if params[symbol].dig(:types_attributes)
                params[symbol][:binder_contractor_types] = []
                params[symbol][:types_attributes].each do |type|
                    exisiting_type = ::Contractor::Category.where("LOWER(name) = ?", type[:name].downcase).first
                    
                    if exisiting_type.nil?
                        exisiting_type = ::Contractor::Category.create(:name => type[:name])
                    end
                    
                    params[symbol][:binder_contractor_types].push(Binder::BinderContractorType.new({"contractor_category_id" => exisiting_type.id}))
                    
                    if params[symbol].dig(:contractor_attributes) && params[symbol].dig(:contractor_attributes, :contractor_types_attributes).nil?
                        params[symbol][:contractor_attributes][:contractor_types_attributes] = []
                    end
                    
                    if params[symbol].dig(:contractor_attributes) && params[symbol].dig(:contractor_attributes, :contractor_types_attributes)
                        params[symbol][:contractor_attributes][:contractor_types_attributes].push({"contractor_category_id" => exisiting_type.id})
                    end
                end
                
                if params[symbol].dig(:binder_contractor_types_attributes)
                    params[symbol][:binder_contractor_types_attributes].each do |type|
                        if type[:id]
                            existing = Binder::BinderContractorType.find_by(:id => type[:id])
                            params[symbol][:binder_contractor_types].push(existing)
                        else
                            existing = Binder::BinderContractorType.new(:contractor_category_id => type[:contractor_category_id])
                            params[symbol][:binder_contractor_types].push(existing)
                        end
                    end
                end
                
                params[symbol][:types_attributes] = params[symbol][:binder_contractor_types_attributes] = []
                
                if params[symbol].dig(:contractor_attributes, :types_attributes)
                    params[symbol][:contractor_attributes][:types_attributes] = []
                end
            end
        end

        def validate_sub_types(params)
            if params[symbol].dig(:sub_types_attributes)
                params[symbol][:binder_contractor_sub_types] = []
                params[symbol][:sub_types_attributes].each do |type|
                    exisiting_type = Binder::Contractor::SubCategory.where("LOWER(name) = ?", type[:name].downcase).first
                    
                    if exisiting_type.nil?
                        exisiting_type = Binder::Contractor::SubCategory.create(:name => type[:name])
                    end
                    
                    params[symbol][:binder_contractor_sub_types].push(Binder::BinderContractorSubType.new({"contractor_sub_category_id" => exisiting_type.id}))
                    
                    if params[symbol].dig(:contractor_attributes) && params[symbol].dig(:contractor_attributes, :contractor_types_attributes).nil?
                        params[symbol][:contractor_attributes][:contractor_types_attributes] = []
                    end
                    
                    if params[symbol].dig(:contractor_attributes) && params[symbol].dig(:contractor_attributes, :contractor_sub_types_attributes)
                        params[symbol][:contractor_attributes][:contractor_sub_types_attributes].push({"contractor_sub_category_id" => exisiting_type.id})
                    end
                end
                
                if params[symbol].dig(:binder_contractor_sub_types_attributes)
                    params[symbol][:binder_contractor_sub_types_attributes].each do |type|
                        if type[:id]
                            existing = Binder::BinderContractorSubType.find_by(:id => type[:id])
                            params[symbol][:binder_contractor_sub_types].push(existing)
                        else
                            existing = Binder::BinderContractorSubType.new(:contractor_sub_category_id => type[:contractor_sub_category_id])
                            params[symbol][:binder_contractor_sub_types].push(existing)
                        end
                    end
                end
                
                params[symbol][:sub_types_attributes] = params[symbol][:binder_contractor_sub_types_attributes] = []
                
                if params[symbol].dig(:contractor_attributes, :sub_types_attributes)
                    params[symbol][:contractor_attributes][:sub_types_attributes] = []
                end
            end
        end
    end
end
