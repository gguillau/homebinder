# == Schema Information
#
# Table name: appliances
#
#  id                        :integer          not null, primary key
#  binder_id                 :integer
#  name                      :string(255)
#  appliance_type_id         :integer
#  appliance_manufacturer_id :integer
#  appliance_model_id        :integer
#  model_number              :string(255)
#  serial_no                 :string(255)
#  warranty                  :string(255)
#  user_guide_url            :string(255)
#  details                   :text
#  created_by                :integer
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  last_recall_check         :date
#  upc                       :string(255)
#  appliance_type            :string(255)
#  manufacturer              :string(255)
#  model                     :string(255)
#  install_date              :date
#  library_source_id         :integer
#

class Binder::Appliance < ApplicationRecord
  include Binder::ComponentActions
  include Binder::Appliance::Actions

  self.table_name = "appliances"
  
  belongs_to :binder

  has_one :purchase, :dependent => :destroy
  has_one :seller_report_item, :dependent => :destroy
  has_one :binder_item, :dependent => :destroy
  
  has_many :appliance_recalls, :dependent => :destroy, :class_name => "Binder::Appliance::ApplianceRecall"
  has_many :recalls, through: :appliance_recalls
  has_many :notes,      :dependent => :destroy
  has_many :tags, :as => :taggable, :dependent => :destroy
  has_many :images,     :dependent => :destroy
  has_many :documents,  :dependent => :destroy

  validates_presence_of :name, :message => I18n.t(:err_value_required)
  validates_presence_of :binder, :message => I18n.t(:err_value_required)

  validates_length_of :name, :maximum => 50
  validates_length_of :serial_no, :maximum => 100
  validates_length_of :warranty, :maximum => 500
  validates_length_of :user_guide_url, :maximum => 250
  validates_length_of :details, :maximum => 1000

  # one unique appliance name per binder
  validates_uniqueness_of :name, scope: :binder_id

  accepts_nested_attributes_for :purchase
  accepts_nested_attributes_for :seller_report_item

  def typeahead
    return [
      { :name => "appliance_type", :to => "Binder::Appliance::Category" },
      { :name => "manufacturer", :to => "Binder::Appliance::Manufacturer" },
      { :name => "model", :to => "Binder::Appliance::Model" }
    ]
  end

  def has_upc
    not self.upc.nil? and not self.upc.empty?
  end

  def has_manufacturer_and_model
    not self.manufacturer.nil? and
    not self.manufacturer.empty? and
    not self.model.nil? and
    not self.model.empty?
  end

  def ready_for_recall
    self.has_upc or self.has_manufacturer_and_model
  end
end
