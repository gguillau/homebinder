# == Schema Information
#
# Table name: documents
#
#  id                :integer          not null, primary key
#  documentable_id   :integer
#  documentable_type :string(255)
#  name              :string(255)
#  location          :string(255)
#  key               :string(255)
#  bucket            :string(255)
#  etag              :string(255)
#  details           :text
#  created_by        :integer
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  binder_id         :integer
#  file_size         :integer
#  file_file_name    :string(255)
#  file_content_type :string(255)
#  file_file_size    :integer
#  file_updated_at   :datetime
#  library_source_id :integer
#

class Binder::Document < ApplicationRecord
  include Binder::ComponentActions
  include Binder::Document::Actions

  self.table_name = "documents"

  belongs_to :binder
  belongs_to :document_type
  
  has_many :tags, :as => :taggable, :dependent => :destroy
  
  has_one :binder_item, :dependent => :destroy
  has_one :seller_report_item, :dependent => :destroy

  accepts_nested_attributes_for :seller_report_item

  has_attached_file :file, :path => "/binder:binder_id/documents/:filename"

  # validations
  validates_presence_of :binder, :message => I18n.t(:err_value_required)
  do_not_validate_attachment_file_type :file
  # validates_attachment :file, :content_type => {
  #   :content_type => [
  #     "application/pdf",
  #     "application/mspowerpoint",
  #     "application/powerpoint",
  #     "application/vnd.ms-powerpoint",
  #     "application/x-mspowerpoint",
  #     "application/vnd.openxmlformats-officedocument.presentationml.presentation",
  #     "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
  #     "application/msword",
  #     "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
  #     "application/excel",
  #     "application/vnd.ms-excel",
  #     "application/x-excel",
  #     "application/x-msexcel",
  #     "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
  #     "text/plain"]}

  validates_uniqueness_of :file_file_name, scope: :binder_id
  
  after_create  -> { increment_binder_storage_size }
  after_destroy -> { decrement_binder_storage_size }
  
  def increment_binder_storage_size
    return if file_file_size.blank?
    return if !accessible?
    self.binder.total_storage += file_file_size
    self.binder.save!
  end
  
  def decrement_binder_storage_size
    return if file_file_size_was.blank?
    return if !accessible?
    self.binder.total_storage -= file_file_size_was
    self.binder.save!
  end

end
