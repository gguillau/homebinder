# == Schema Information
#
# Table name: transfers
#
#  id                 :integer          not null, primary key
#  user_id            :integer
#  binder_id          :integer
#  transfer_to        :string(255)
#  transfer_type      :string(255)
#  status             :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  last_notified      :datetime
#  sender_id          :integer
#  receiver_id        :integer
#  transferred_at     :date
#  delivery_failed_at :date
#  completed_at       :date
#  access_token       :string
#

class Binder::Transfer < ApplicationRecord
    include Binder::ComponentActions
    include Binder::Transfer::Actions

    self.table_name = "transfers"

    has_secure_token :access_token

    belongs_to :sender, :class_name => "User"
    belongs_to :receiver, :class_name => "User"
    belongs_to :binder

    # validations
    validates_presence_of :binder, :message => I18n.t(:err_value_required)
    validates_presence_of :receiver_id, :message => I18n.t(:err_value_required)
    validates_presence_of :sender_id, :message => I18n.t(:err_value_required)
    validates_presence_of :status, :message => I18n.t(:err_value_required)
    validates_presence_of :transfer_type, :message => I18n.t(:err_value_required)
    
    # one unique transfer per receiver and sender
    validates_uniqueness_of :receiver_id, scope: [:binder_id, :sender_id]

    before_validation :different_users

    def different_users
        if self.sender_id === self.receiver_id
            errors.add(:sender_id, "Error - Transfer Sender and Receiver cannot be same users")
        end
    end
end
