# == Schema Information
#
# Table name: areas
#
#  id           :integer          not null, primary key
#  binder_id    :integer
#  name         :string(255)
#  structure_id :integer
#  details      :text
#  dimensions   :string(255)
#  area_type_id :integer
#  created_by   :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  area_type    :string(255)
#

class Binder::Area < ApplicationRecord
  include Binder::ComponentActions

  self.table_name = "areas"
  
  belongs_to :binder
  
  has_one :binder_item, :dependent => :destroy
  
  has_many :notes, :dependent => :destroy
  has_many :tags, :as => :taggable, :dependent => :destroy
  has_many :images, :dependent => :destroy
  has_many :documents, :dependent => :destroy

  validates_presence_of :name, :message => I18n.t(:err_value_required)
  validates_length_of :name, :maximum => 255
  validates_presence_of :binder, :message => I18n.t(:err_value_required)

  # one unique area name per binder
  validates_uniqueness_of :name, scope: :binder_id

  def typeahead
    return [{ name: "area_type", to: "Binder::Area::Category" }]
  end
end
