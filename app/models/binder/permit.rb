# == Schema Information
#
# Table name: permits
#
#  id                   :integer          not null, primary key
#  status               :string(255)
#  proposed_use         :string(255)
#  work_class           :string(255)
#  permit_type          :string(255)
#  permit_number        :string(255)      not null
#  valuation_amount     :money
#  details              :string(1000)
#  permit_date          :datetime
#  created_by           :integer
#  binder_contractor_id :integer
#  binder_id            :integer          not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class Binder::Permit < ApplicationRecord
  include Binder::ComponentActions

  self.table_name = "permits"

  belongs_to :binder
  belongs_to :binder_contractor
  
  has_one :binder_item, :dependent => :destroy
  has_one :seller_report_item, :dependent => :destroy
  
  has_many :tags, :as => :taggable, :dependent => :destroy
  has_many :images, :dependent => :destroy
  has_many :documents, :dependent => :destroy

  validates_presence_of :permit_number, :message => I18n.t(:err_value_required)
  validates_presence_of :binder,     :message => I18n.t(:err_value_required)
  validates_length_of   :details,       :maximum => 1000

end
