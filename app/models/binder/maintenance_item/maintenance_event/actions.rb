module Binder::MaintenanceItem::MaintenanceEvent::Actions
  def self.included(klass)
    klass.extend ClassMethods 
  end
  
  module ClassMethods
    
    ORDER = "do_date DESC".freeze
    ITEM_NOT_FOUND = "Maintenance item not found".freeze
    EVENT_NOT_FOUND = "Maintenance event not found".freeze
    BINDER_NOT_FOUND = "Binder not found".freeze
    
    def index(hb_request, params)
      if params[:search]
        return super
      else
        # get the binder
        binder = load_binder(params[:maintenance_item_id])
        
        # verify the user has permission
        hb_request.ability.authorize! :read, binder, :message => "You are not authorized to access binder ID: #{binder.id}."
        
        # get the maintenance events
        return Binder::MaintenanceItem::MaintenanceEvent.where(:maintenance_item_id => params[:maintenance_item_id]).order(ORDER)
      end
    end
  
    def build(hb_request, params)
      # check if the user can create in the binder
      mi = Binder::MaintenanceItem.where(:id => params[:maintenance_event][:maintenance_item_id]).first
      raise NotFoundException.new ITEM_NOT_FOUND if mi.nil?
      
      binder = Binder.find(mi.binder_id)
      
      hb_request.ability.authorize! :write, binder, :message => "You are not authorized to access binder ID: #{binder.id}."
      
      super(hb_request, params)
    end
  
    def update(hb_request, params)
      event = super(hb_request, params)
        
      if (event.completed_date)
        mi = Binder::MaintenanceItem.find(event.maintenance_item_id)
        mi.schedule_next_event
      end
        
      return event
    end
  
    # helper methods
    def load_binder(maintenance_item_id)
      mi = Binder::MaintenanceItem.find(maintenance_item_id)
      
      binder = Binder.find(mi.binder_id)
      
      return binder
    end
  end
  
end