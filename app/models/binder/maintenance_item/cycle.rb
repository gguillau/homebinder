# == Schema Information
#
# Table name: maintenance_cycles
#
#  id   :integer          not null, primary key
#  name :string(255)
#

class Binder::MaintenanceItem::Cycle < ApplicationRecord
    self.table_name = "maintenance_cycles"
    
    CYCLE = "cycle".freeze
    AS_NEEDED = "as needed".freeze
    ONCE = "once".freeze
    MONTHS = "months".freeze
    YEARS = "years".freeze
    
    ANNUAL = "Annual".freeze
    SEMI_ANNUAL = "Semi-Annual".freeze
    QUARTERLY = "Quarterly".freeze
    MONTHLY = "Monthly".freeze
    EVERY_OTHER = "Every-Other-Year".freeze
    EVERY_THREE = "Every-3-Years".freeze
    EVERY_FOUR = "Every-4-Years".freeze
    EVERY_FIVE = "Every-5-Years".freeze
    EVERY_TEN = "Every-10-Years".freeze
    EVERY_FOURTY = "Every-40-Years".freeze
    
    validates_presence_of   :name,    :message => I18n.t(:err_value_required)
    
    def self.frequencies
        [
            ONCE,
            AS_NEEDED,
            ANNUAL,
            SEMI_ANNUAL,
            QUARTERLY,
            MONTHLY,
            EVERY_OTHER,
            EVERY_THREE,
            EVERY_FOUR,
            EVERY_FIVE,
            EVERY_TEN,
            EVERY_FOURTY
        ]
    end
  
end
