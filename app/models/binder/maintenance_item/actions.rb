module Binder::MaintenanceItem::Actions
    def self.included(klass)
        klass.extend ClassMethods
    end

    module ClassMethods

        def send_test_notification_email(hb_request, id)
            # find the item
            item = Binder::MaintenanceItem.find(id)
            # get the binder
            binder = Binder.find(item.binder_id)

            event = item.next_event

            # return unless we have an event
            return unless event.present?
            # return unless the item do date is tomorrow
            return unless event.do_date.present?

            # get the due date
            do_date = item.do_date

            # send the email
            MaintenanceNotifyMailer.notify_email(event.id, binder.id, hb_request.user.id, do_date.to_s).deliver_later
        end

        def base_query
            "CAST(maintenance_items.id as varchar(25)) LIKE :search OR LOWER(maintenance_items.name) LIKE :search OR LOWER(maintenance_items.maintenance_cycle) LIKE :search".freeze
        end

        def base_includes
            [:maintenance_events]
        end

        def query_arguments
            ["maintenance_cycle", "email_notifications", "interval", "do_date", "created_by", "binder_id"]
        end

        def query_arguments_hash(query, value)
            case query
            when "maintenance_cycle", "email_notifications", "interval", "do_date"
                {:maintenance_events => {query.to_sym => Date.parse(value)}}
            when "created_by", "binder_id"
                {query.to_sym => value }
            else
                {}
            end
        end

        # find all maintenance items where the due date for the next event has already passed
        # this is for auditing purposes to make sure our system is working correctly

        def past_due_items
            Binder::MaintenanceItem.joins(:maintenance_events).where(:maintenance_events => {:last_notify_date => nil}).where("maintenance_events.do_date < ?", Date.today)
        end
    end
end