# == Schema Information
#
# Table name: maintenance_types
#
#  id                   :integer          not null, primary key
#  name                 :string(255)
#  interval             :integer
#  maintenance_cycle_id :integer
#  created_by           :integer
#  verified             :boolean
#

class Binder::MaintenanceItem::Category < ApplicationRecord
    self.table_name = "maintenance_types"
    
    validates_presence_of   :name,    :message => I18n.t(:err_value_required)
end
