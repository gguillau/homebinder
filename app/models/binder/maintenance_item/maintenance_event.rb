# == Schema Information
#
# Table name: maintenance_events
#
#  id                  :integer          not null, primary key
#  maintenance_item_id :integer
#  contractor_id       :integer
#  do_date             :date
#  completed_date      :date
#  created_by          :integer
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  last_notify_date    :date
#

class Binder::MaintenanceItem::MaintenanceEvent < ApplicationRecord
  include Binder::MaintenanceItem::MaintenanceEvent::Actions
  
  self.table_name = "maintenance_events"
  index_name    "maintenance_events"
  document_type "maintenance_event"
  
  belongs_to :maintenance_item
  belongs_to :contractor
  belongs_to :creator, :class_name => "User", :foreign_key => "created_by"
  
  delegate :binder, to: :maintenance_item
  delegate :owner, to: :binder
  
  has_many :notes, :dependent => :destroy
  has_many :tags, :as => :taggable, :dependent => :destroy

  validates_presence_of :maintenance_item, :message => I18n.t(:err_value_required)
  validates_presence_of :created_by, :message => I18n.t(:err_value_required)
  validates_presence_of :do_date, :message => I18n.t(:err_value_required)
  
  settings do
      mappings dynamic: false do
          indexes :id,                    type: :integer
          indexes :do_date,               type: :date
          indexes :created_at,            type: :date
          indexes :updated_at,            type: :date
          indexes :completed_date,        type: :date
          indexes :last_notify_date,      type: :date
          indexes :email_status,          type: :keyword
          indexes :clicks,                type: :integer
          indexes :opens,                 type: :integer
          
          indexes :maintenance_item do
              indexes :id,                  type: :integer
              indexes :name,                type: :keyword
              indexes :maintenance_cycle,   type: :keyword
              indexes :email_notifications, type: :boolean
              indexes :library_source_id,   type: :integer
          end
          
          indexes :binder do
              indexes :id,                type: :integer
              indexes :name,              type: :text
              indexes :created_at,        type: :date
              indexes :create_method,     type: :keyword
              indexes :status,            type: :keyword
              indexes :details,           type: :text
          end
          
          indexes :partner do
            indexes :partner_id,          type: :integer
            indexes :role,                type: :keyword
          end
      end
  end
  
  def as_indexed_json(options={})
      self.as_json(
          {
              only: [:id, :do_date, :completed_date, :last_notify_date, :email_status, :clicks, :opens, :created_at, :updated_at],
              include: {
                  :maintenance_item => {
                      :only => [:id, :name, :maintenance_cycle, :email_notifications, :library_source_id]
                  },
                  :binder => {
                      :only => [:id, :name, :created_at, :create_method, :status, :details]
                  },
                  :partner => {
                    :only => [:partner_id, :role]
                  }
              },
              methods: [:agent, :owner]
          }
      )
  end

  def partner
    if user = binder.generate_branding("maintenance_email").select { |branding| ["lender", "apr_reviewer", "builder", "broker", "agent", "inspector", "homepro", "property_manager"].include? branding[:role]}.first
      Binder::BinderBrandingUser.new(user)
    end
  end
  
  def agent
    binder.generate_branding("maintenance_email").select { |branding| branding[:role] === "agent"}.first
  end
  
end
  
