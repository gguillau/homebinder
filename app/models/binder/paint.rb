# == Schema Information
#
# Table name: paints
#
#  id                    :integer          not null, primary key
#  created_by            :integer
#  binder_id             :integer
#  name                  :string(255)
#  paint_manufacturer_id :integer
#  code                  :string(255)
#  makeup                :string(255)
#  details               :text
#  paint_type_id         :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  paint_type            :string(255)
#  manufacturer          :string(255)
#  sheen                 :string(50)
#

class Binder::Paint < ApplicationRecord
  include Binder::ComponentActions

  self.table_name = "paints"

  belongs_to :binder

  has_one :binder_item, :dependent => :destroy
  has_one :purchase, :dependent => :destroy
  has_one :seller_report_item, :dependent => :destroy
  
  has_many :notes, :dependent => :destroy
  has_many :tags, :as => :taggable, :dependent => :destroy
  has_many :images, :dependent => :destroy
  has_many :documents, :dependent => :destroy

  validates_presence_of :name, :message => I18n.t(:err_value_required)
  validates_presence_of :binder, :message => I18n.t(:err_value_required)

  validates_length_of :name, :maximum => 50
  validates_length_of :manufacturer, :maximum => 50
  validates_length_of :code, :maximum => 50
  validates_length_of :makeup, :maximum => 50

  # one unique paint name per binder
  validates_uniqueness_of :name, scope: :binder_id

  accepts_nested_attributes_for :purchase
  accepts_nested_attributes_for :seller_report_item

  def typeahead
    return [
      { :name => "paint_type", :to => "Binder::Paint::Category" },
      { :name => "manufacturer", :to => "Binder::Paint::Manufacturer" },
      { :name => "sheen", :to => "Binder::Paint::Sheen" }
    ]
  end
end
