# == Schema Information
#
# Table name: annual_property_review_findings
#
#  id                        :integer          not null, primary key
#  annual_property_review_id :integer
#  name                      :string(255)      not null
#  priority                  :integer          default("tbd"), not null
#  estimated_cost            :integer          default("less_hun"), not null
#  details                   :text
#  recommended_homepro_id    :integer
#  selected_homepro_id       :integer
#  status                    :integer          default("identified"), not null
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#

class AnnualPropertyReviewFinding < ApplicationRecord
    belongs_to :annual_property_review
    has_many :annual_property_review_photos,            :dependent => :destroy

    validates :name, presence: true, length: { maximum: 255 }
    
    enum priority: {
             tbd: 0,
             cosmetic: 1,
             low: 2,
             high: 3,
             critical: 4
         }, _prefix: :priority
    enum status: {
             identified: 0,
             dismissed: 1,
             bid_requested: 2
         }, _prefix: :status
    enum estimated_cost: {
             less_hun: 0,
             one_hun_to_two_fity: 1,
             two_fifty_to_five_hun: 2,
             five_hun_to_one_thou: 3,
             one_thou_to_twenty_hun: 4,
             twenty_hun_to_five_thou: 5,
             five_thou_plus: 6,
             unknown: 7,
             tbd: 8,
         }, _prefix: :estimated_cost

    def recommended_homepro
        return nil if self.recommended_homepro_id.nil?
        homepro = Template::Contractor.find_by_id(self.recommended_homepro_id)
        return nil if homepro.nil?
        return nil if homepro.partner_contractor.nil?
        return {
                   id: homepro.id,
                   name: homepro.partner_contractor.name,
                   email: homepro.partner_contractor.email,
                   phone: homepro.partner_contractor.phone
               }
    end

    def cost
        case self.estimated_cost
        when "less_hun"
            return "Less than $100"
        when "one_hun_to_two_fity"
            return "$100 - $250"
        when "two_fifty_to_five_hun"
            return "$250 - $500"
        when "five_hun_to_one_thou"
            return "$500 - $1,000"
        when "one_thou_to_twenty_hun"
            return "$1,000 - $2,500"
        when "twenty_hun_to_five_thou"
            return "$2,000 to $5,000"
        when "five_thou_plus"
            return "$5,000+"
        when "unknown"
            return "Unknown"
        when "tbd"
            return "TBD by Home Professional"
        end
    end
    
    def default_photo
        return "https://www.homebinder.com/img/no-image-available.jpg" if annual_property_review_photos.count < 1
        photo = annual_property_review_photos[0]
        return photo.file.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :medium)
    end
end
