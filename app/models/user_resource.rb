# == Schema Information
#
# Table name: user_resources
#
#  id                    :integer          not null, primary key
#  marketing_resource_id :integer
#  user_id               :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class UserResource < ApplicationRecord
    
    # join table for users and resources
    belongs_to :user
    belongs_to :marketing_resource
    
    # validations
    validates_presence_of   :marketing_resource,     :message => I18n.t(:err_value_required)
    validates_presence_of   :user,                   :message => I18n.t(:err_value_required)

    # one unique marketing_resource per user
    validates_uniqueness_of :marketing_resource_id, scope: :user_id, :message => "already exists for user"
end
