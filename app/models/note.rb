# == Schema Information
#
# Table name: notes
#
#  id           :integer          not null, primary key
#  content      :text
#  notable_id   :integer
#  notable_type :string(255)
#  access_level :integer
#  created_by   :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#

class Note < ApplicationRecord
end
