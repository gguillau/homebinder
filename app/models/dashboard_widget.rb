# == Schema Information
#
# Table name: dashboard_widgets
#
#  id           :integer          not null, primary key
#  dashboard_id :integer
#  widget_id    :integer
#  index        :integer
#

class DashboardWidget < ApplicationRecord
    
    # join table for dashboards and widgets
    belongs_to :dashboard
    belongs_to :widget
    
    # validations
    validates_presence_of   :dashboard,  :message => I18n.t(:err_value_required)
    validates_presence_of   :widget,     :message => I18n.t(:err_value_required)

    # one unique widget per dashboard
    validates_uniqueness_of :widget_id, scope: :dashboard_id, :message => "already exists on the dashboard"
end
