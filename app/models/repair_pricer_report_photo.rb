class RepairPricerReportPhoto < ApplicationRecord
    
    # relationships
    belongs_to :repair_pricer_report_finding

    validates_presence_of :file, :message => I18n.t(:err_value_required)
end
