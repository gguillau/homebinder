module MarketingResource::Actions
    
    def self.included(klass)
        klass.extend ClassMethods 
    end
  
    module ClassMethods
    
        def build(hb_request, params)
            
            resource = super(hb_request, params)
            
            # link the partner
            if params[:partner_id]
                PartnerResource.create!(:partner_id => params[:partner_id], :marketing_resource_id => resource.id)
            end
            
            # link the organization
            if params[:organization_id]
                OrganizationResource.create!(:organization_id => params[:organization_id], :marketing_resource_id => resource.id)
                # link the association user types
                MarketingResources::LinkUserTypesJob.perform_async(params[:organization_id], resource.id)
            end
            
            # return the resource
            return resource
        end

        def add_custom_args(where, where_not, params, includes, joins)
            if params[:partnerId]
                joins.push(:partner_resources)
                where.merge!(:partner_resources => {:partner_id => params[:partnerId]})
            end
            
            if params[:organizationId]
                joins.push(:organization_resources)
                where.merge!(:organization_resources => {:organization_id => params[:organizationId]})
            end
            
            super
        end
    end
  
end