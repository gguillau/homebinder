module AnnualPropertyReview::AnnualPropertyReviewPhotoBuilder

    def self.included(klass)
        klass.extend ClassMethods
    end

    module ClassMethods
        
        def build(hb_request, params)
            raise BadRequestException.new I18n.t :err_apr_photo_parentid_required unless params[:annual_property_review_id].present?
            apr = AnnualPropertyReview.find(params[:annual_property_review_id])
            hb_request.ability.authorize! :write, apr, :message => "You are not authorized to write to APR ID: #{apr.id}."
    
            # annual_property_review_id is required. The other ids are optional
            photo = AnnualPropertyReviewPhoto.new(
                    name: params[:file].original_filename,
                    annual_property_review_id: params[:annual_property_review_id],
                    annual_property_review_finding_id: params[:annual_property_review_finding_id],
                    annual_property_review_capital_item_id: params[:annual_property_review_capital_item_id], 
                    file: params[:file], 
                    description: params[:description])
    
            if not photo.save
                raise UnprocessableException.new photo
            end
    
            return photo
        end
    end
end
