module AnnualPropertyReview::AnnualPropertyReviewBuilder

    def self.included(klass)
        klass.extend ClassMethods
    end

    module ClassMethods
        attr_writer :card_processor

        QUERY = "LOWER(uuid) LIKE :search OR LOWER(client_first_name) LIKE :search OR LOWER(client_last_name) LIKE :search
                OR LOWER(client_email) LIKE :search OR LOWER(CONCAT(address1, ' ', address2, ' ', city, ', ', state, ' ', zip)) LIKE :search".freeze
        
        def show(user, params = {})
            raise BadRequestException.new if not params[:id].present?
            
            aprInclude = AnnualPropertyReview.includes(:annual_property_review_findings, :annual_property_review_capital_items, :annual_property_review_photos)
            
            if params[:id].to_s =~ /\A\d+\Z/
                # todo only partners and homebinder admins can access by id
                # homeowners must use uuid.
                apr = aprInclude.where(id: params[:id]).first
                
                raise CanCan::AccessDenied.new("You are not authorized to access APR ID: #{apr.id}") if user.nil? || user.is_homeowner?
                
                # authorize the user can read the apr
                user.ability.authorize! :read, apr, :message => "You are not authorized to access APR ID: #{apr.id}."
            else
                apr = aprInclude.where(uuid: params[:id]).first
            end
            
            apr
        end
        
        def build(user, params)
            @user = user
            apr = AnnualPropertyReview.new(params[:annual_property_review])
            apr.uuid = SecureRandom.uuid.gsub("-", "")

            # status must be email or active
            raise BadRequestException.new I18n.t :err_apr_create_status_invalid unless apr.status_active? || apr.status_unassigned?

            # validate the apr
            if not apr.valid?
                raise UnprocessableException.new apr
            end

            # save the credit card if status is active
            if apr.status_active? || apr.status_unassigned?
                if params[:payment]
                    begin
                        charge = process_payment(apr.reviewer_company_id, params[:payment])
                        apr.stripe_charge_id = charge[:charge].id if charge[:charge].amount > 0
                        apr.amount_charged_cents = charge[:charge].amount
                        apr.promo_code_name = params[:payment][:promo_code]
                        apr.promo_code_amount_off = charge[:amount_off]
                        apr.payment_status = AnnualPropertyReview.payment_statuses[:paid]
                    rescue => e
                        raise BadRequestException.new e.message
                    end
                end
            end

            # save the apr
            if not apr.save
                raise UnprocessableException.new apr
            end

            # send an email
            AnnualPropertyReviewMailer.new_client_apr(apr.id).deliver_later
            AnnualPropertyReviewMailer.new_company_apr(apr.id).deliver_later
            AnnualPropertyReviewMailer.new_support_apr(apr.id).deliver_later

            # analytics
            EventService.perform_async({:event_name => "created", :event_type => "apr", :user_id => @user.id, :user_role => @user.role, :apr_id => apr.id})
            EventService.perform_async({:event_type => "engagement", :event_name => "#{apr.class.table_name.singularize} created", :user_id => @user.id, :user_role => @user.role, :user_created_at => @user.created_at, :user_create_method => @user.create_method})
            
            return apr
        end

        def update(user, params)
            @user = user
            # get the apr to update
            apr = AnnualPropertyReview.find(params[:id])

            # we are allowing users to redraft when the APR is complete
            #raise BadRequestException.new I18n.t :err_apr_complete if apr.status_completed?

            # authorize the user to modify the apr
            # homeowners will be accessing the APR without logging in.
            # we can't authorize the user
            # @user.authorize! :write, apr

            next_status = params[:annual_property_review][:status]
            next_status = AnnualPropertyReview.statuses[next_status]

            params[:send_email] = false
            case next_status
            when AnnualPropertyReview.statuses[:scheduled]
                update_to_scheduled(apr, params)
            when AnnualPropertyReview.statuses[:draft]
                # add default home planning
                add_default_home_planning_items(apr)
            when AnnualPropertyReview.statuses[:draft_completed]
                update_to_draft_completed(apr, params)
            when AnnualPropertyReview.statuses[:completed]
                update_to_completed(apr, params)
            when AnnualPropertyReview.statuses[:canceled]
                update_to_canceled(apr, params)
            when AnnualPropertyReview.statuses[:on_hold]
                update_to_on_hold(apr, params)
            end

            # update the apr
            if not apr.update_attributes(params[:annual_property_review])
                raise UnprocessableException.new apr
            end

            # send an email?
            if params[:send_email]
                if apr.status_scheduled?
                elsif apr.status_draft_completed?
                    AnnualPropertyReviewMailer.draft_completed(apr.id).deliver_later
                elsif apr.status_completed?
                    AnnualPropertyReviewMailer.completed(apr.id).deliver_later
                elsif apr.status_canceled?
                    send_canceled_message(apr)
                elsif apr.status_on_hold?
                    send_on_hold_message(apr)
                end
            end
            
            EventService.perform_async({:event_type => "engagement", :event_name => "#{apr.class.table_name.singularize} updated", :user_id => @user.id, :user_role => @user.role, :user_created_at => @user.created_at, :user_create_method => @user.create_method}) if @user.present?

            return apr
        end

        def update_to_scheduled(apr, params)
            if apr.status_scheduled?
                # apr is already scheduled. if a new date/time is being set update scheduled_on
                next_datetime = DateTime.parse(params[:annual_property_review][:scheduled_for])
                return if
                    apr.scheduled_for.day == next_datetime.day and
                    apr.scheduled_for.month == next_datetime.month and
                    apr.scheduled_for.year == next_datetime.year and
                    apr.scheduled_for.hour == next_datetime.hour and
                    apr.scheduled_for.min == next_datetime.min
            end
            params[:annual_property_review][:scheduled_on] = DateTime.now
            params[:send_email] = true

            # analytics
            EventService.perform_async({:event_name => "scheduled", :event_type => "apr", :user_id => @user.id, :user_role => @user.role, :apr_id => apr.id})
        end

        def update_to_draft_completed(apr, params)
            AnnualPropertyReviewMailer.send_full_report(apr.id, nil).deliver_later
            params[:send_email] = true
        end

        def update_to_completed(apr, params)
            return if apr.status_completed?
            params[:annual_property_review][:completed_on] = DateTime.now
            params[:send_email] = true

            # analytics
            EventService.perform_async({:event_name => "completed", :event_type => "apr", :user_id => @user.id, :user_role => @user.role, :apr_id => apr.id})
        end

        def update_to_canceled(apr, params)
            raise BadRequestException.new I18n.t :err_apr_no_cancel unless apr.status_active? or apr.status_scheduled? or apr.status_draft?

            if apr.status_scheduled?
                limit = Rails.configuration.apr[:max_cancel_hours]
                raise BadRequestException.new I18n.t :err_apr_review_too_close if (apr.scheduled_for >= DateTime.current) and (apr.scheduled_for - limit.hours <= DateTime.current)
            end

            if apr.amount_charged_cents > 0
                # issue a refund
                cc = @card_processor || Payments::CreditCardProcessor.new(@user)
                refund = cc.refund(apr.stripe_charge_id)
                params[:annual_property_review][:stripe_refund_id] = refund.id
                params[:annual_property_review][:payment_status] = "refunded"
            end
            params[:annual_property_review][:canceled_on] = DateTime.now
            params[:send_email] = true
        end

        def update_to_on_hold(apr, params)
            raise BadRequestException.new I18n.t :err_apr_no_hold if apr.status_draft? or apr.status_draft_completed? or apr.status_completed?
            return if apr.status_canceled? or apr.status_on_hold?

            limit = Rails.configuration.apr[:max_hold_hours]
            raise BadRequestException.new I18n.t :err_apr_review_too_close if apr.scheduled_for and (apr.scheduled_for - limit.hours <= DateTime.current)

            params[:annual_property_review][:on_hold_on] = DateTime.now
            params[:annual_property_review][:scheduled_for] = nil
            params[:annual_property_review][:scheduled_on] = nil
            params[:send_email] = true
        end

        def add_default_home_planning_items(apr)
            if apr.annual_property_review_capital_items.length < 1
                apr.annual_property_review_capital_items.new({:name => "Roof", :eul => "unknown", :cost_range => "unknown"}).save
                apr.annual_property_review_capital_items.new({:name => "HVAC", :eul => "unknown", :cost_range => "unknown"}).save
                apr.annual_property_review_capital_items.new({:name => "Siding", :eul => "unknown", :cost_range => "unknown"}).save
                apr.annual_property_review_capital_items.new({:name => "Windows", :eul => "unknown", :cost_range => "unknown"}).save
                apr.annual_property_review_capital_items.new({:name => "Chimney", :eul => "unknown", :cost_range => "unknown"}).save
                apr.annual_property_review_capital_items.new({:name => "Water Heater", :eul => "unknown", :cost_range => "unknown"}).save

                apr.property_reviewer_comments = "Thank you for the opportunity to perform a review of your home.  If you have any questions, please do not hesitate to contact us at any time.  We wish you the best in your home ownership!"
                apr.save!
            end
        end

        def message(user, params)
            @user = user
            apr = AnnualPropertyReview.find(params[:id])
            message = params[:message]

            if user.present? # we send message to homeowner
                AnnualPropertyReviewMailer.message_to_homeowner(apr.id, message).deliver_later
            else # we send a message to the reviewer
                AnnualPropertyReviewMailer.message_to_reviewer(apr.id, message).deliver_later
            end
        end

        def send_canceled_message(apr)
            if @user.present?
                sender = {
                    :name => @user.user_profile.display_name,
                    :email => @user.email
                }

                receiver = {
                    :name => apr.client_first_name + " " + apr.client_last_name,
                    :email => apr.client_email
                }

                AnnualPropertyReviewMailer.canceled(apr.id, sender, receiver).deliver_later
            else
                sender = {
                    :name => apr.client_first_name + " " + apr.client_last_name,
                    :email => apr.client_email
                }

                receiver = {
                    :name => apr.reviewer_company.name,
                    :email => apr.reviewer_company.email
                }

                AnnualPropertyReviewMailer.canceled(apr.id, sender, receiver).deliver_later
            end
        end

        def send_on_hold_message(apr)
            if @user.present?
                sender = {
                    :name => @user.user_profile.display_name,
                    :email => @user.email
                }

                receiver = {
                    :name => apr.client_first_name + " " + apr.client_last_name,
                    :email => apr.client_email
                }

                AnnualPropertyReviewMailer.on_hold(apr.id, sender, receiver).deliver_later
            else
                sender = {
                    :name => apr.client_first_name + " " + apr.client_last_name,
                    :email => apr.client_email
                }

                receiver = {
                    :name => apr.reviewer_company.name,
                    :email => apr.reviewer_company.email
                }

                AnnualPropertyReviewMailer.on_hold(apr.id, sender, receiver).deliver_later
            end
        end

        def request_quote(user, params)
            @user = user
            
            # make sure an id was passed in
            id = params[:id]
            raise BadRequestException.new unless id.present?

            # find the apr
            apr = AnnualPropertyReview.includes(:reviewer_company).find(id)

            comments = params[:comments]

            AnnualPropertyReviewMailer.request_quote(apr.id, params[:recommended_homepro_id], comments, params[:selected_ids]).deliver_later

            # analytics
            EventService.perform_async({:event_name => "quote_requested", :event_type => "apr", :homepro_id => params[:recommended_homepro_id], :apr_id => apr.id})
        end

        def process_payment(partner_id, payment)
            raise BadRequestException.new I18n.t :err_apr_credit_required unless payment[:card]

            if partner_id.present?
                # get the cost of an apr
                partner = Partner.includes(:partner_configuration).find(partner_id)
                amount = partner.partner_configuration.annual_property_review_cost_cents
            else
                amount = payment[:cost] * 100
            end

            amount_off = payment[:amount_off]

            # do not process the charge if the amount is $0
            if amount < 1
                return {:charge => OpenStruct.new({:amount => amount}), :amount_off => amount_off}
            end

            # create a card processor
            cc = Payments::CreditCardProcessor.new(@user)
            # attempt to charge the provided card
            begin
                charge = cc.charge_card({ amount: amount, card: payment[:card] })
                return {:charge => charge, :amount_off => amount_off}
            rescue => e
                # not able to charge the card
                ErrorService.perform_async(e.message, {user_id: @user&.id})
                raise BadRequestException.new e.message
            end
        end

        def pager(hb_request, params = {}, search_method = :for_admin)
            # comment out for now
            #return elasticsearch(hb_request, params) if params[:search]
            
            set_defaults

            raise Exception.new unless default_order_by.present?
            raise Exception.new unless default_order.present?

            # set the parameters
            item_page = params[:page].present? ? params[:page].to_i : page
            item_count = params[:count].present? ? params[:count].to_i : count
            offset = (item_page - 1) * item_count
            orderBy = params[:orderBy].present? ? params[:orderBy] : default_order_by
            order = params[:order].present? ? params[:order] : default_order
            search_method = params[:searchMethod] ? params[:searchMethod] : :for_admin

            # get the search method
            search_method_obj = self.method(search_method)

            # search
            total = search_method_obj.call(params, hb_request).count
            items = search_method_obj.call(params, hb_request)
                .order("#{orderBy} #{order}")
                .limit(item_count)
                .offset(offset)

            # return the page
            return Page.new(items, total)
        end

        def for_admin(params = {}, hb_request)
            hb_request.ability.authorize! :manage, :all, :message => "You are not authorized to view these users."

            # get the search value
            searchValue = params[:search]
            # the custom args where clause
            where = {}
            where_not = {}

            includes = []
            joins = []

            add_custom_args(where, where_not, params, includes, joins)

            if searchValue.present?
                return self.where(QUERY, search: "%#{searchValue}%".downcase)
                                           .where(where)
            else
                return self.where(where)
            end
        end

        def for_company(params = {}, hb_request)
            raise BadRequestException.new "Partner ID Required" if params[:companyId].nil?
            
            # get the search value
            searchValue = params[:search]
            company_id = params[:companyId]

            # check if user can read the partner
            partner = Partner.find(params[:companyId])
            hb_request.ability.authorize! :read, partner, :message => "You are not authorized to access partner ID: #{partner.id}."
            
            # the custom args where clause
            where = {}
            where_not = {}

            includes = []
            joins = []

            add_custom_args(where, where_not, params, includes, joins)

            if searchValue.present?
                return self
                           .where(reviewer_company_id: company_id)
                           .where(where)
                           .where(QUERY, search: "%#{searchValue}%".downcase)
            else
                return self
                           .where(reviewer_company_id: company_id)
                           .where(where)
            end
        end

        def add_custom_args(where, where_not, params, includes, joins)
            if params[:status]
                where.merge!({:status => params[:status]})
            end

            if params[:companyId]
                where.merge!({:reviewer_company_id => params[:companyId]})
            end
        end

    end
end
