# == Schema Information
#
# Table name: tags
#
#  id             :integer          not null, primary key
#  taggable_id    :integer
#  taggable_type  :string(255)
#  tag            :string(255)
#  auto_generated :boolean          default(TRUE)
#  created_by     :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  scope          :string(255)
#  metadata       :text
#

class Tag < ApplicationRecord
    # attr_accessible :title, :body
    belongs_to :taggable, :polymorphic => true

    validates_presence_of :tag, :message => I18n.t(:err_value_required)

    def get_taggable
        self.taggable_type.classify.constantize.find(self.taggable_id)
    end

    def get_tagged_resource
        parts = tag.split('_')
        parts[0][0] = parts[0][0].capitalize
        parts[0] = "binder::" + parts[0]
        begin
            cls = parts[0].classify.constantize
        rescue
            return nil
        end
        return cls.find_by_id(parts[1])
    end
end
