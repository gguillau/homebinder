# == Schema Information
#
# Table name: marketing_resources
#
#  id                    :integer          not null, primary key
#  name                  :string(100)
#  description           :text
#  document_file_name    :string
#  document_content_type :string
#  document_file_size    :integer
#  document_updated_at   :datetime
#  image_file_name       :string
#  image_content_type    :string
#  image_file_size       :integer
#  image_updated_at      :datetime
#  video_file_name       :string
#  video_content_type    :string
#  video_file_size       :integer
#  video_updated_at      :datetime
#  url                   :string
#  resource_type         :integer
#  user_type             :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class MarketingResource < ApplicationRecord
    include MarketingResource::Actions
    
    # relationships
    has_many :partner_resources,            :dependent => :destroy
    has_many :organization_resources,       :dependent => :destroy
    
    enum resource_type: [ :document, :image, :link, :video]
    enum user_type:     [ :homeowner, :inspector, :broker]
    
    validates_presence_of     :name, :message => I18n.t(:err_value_required)
    validates_length_of       :name, :maximum => 100

    has_attached_file :image,
        :path => "/marketing_resource:id/:filename",
        :source_file_options => { all: '-auto-orient' },
        :convert_options => { :all => "-quality 100" },
        :styles => {
        :thumb => ["175x175>",:png],
        :original => ["350x350>",:png]
    }
    
    validates_attachment :image, :content_type => {
        :content_type => [
            "image/jpeg",
            "image/jpg",
            "image/png",
            "image/gif",
            "image/bmp"
        ]
    }
    
    has_attached_file :document, :path => "/marketing_resource:id/:filename"
    
    validates_attachment :document, :content_type => {
        :content_type => [
            "application/pdf",
            "application/mspowerpoint",
            "application/powerpoint",
            "application/vnd.ms-powerpoint",
            "application/x-mspowerpoint",
            "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            "application/vnd.openxmlformats-officedocument.presentationml.slideshow",
            "application/msword",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "application/excel",
            "application/vnd.ms-excel",
            "application/x-excel",
            "application/x-msexcel",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            "text/plain"
            ]
    }

end
