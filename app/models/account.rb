# == Schema Information
#
# Table name: accounts
#
#  id                           :integer          not null, primary key
#  manager_id                   :integer
#  account_status               :string(50)       not null
#  account_sub_status           :string(50)
#  payment_type                 :string(50)
#  account_type                 :string(50)       not null
#  account_sub_type             :string(50)       not null
#  trial_expiration             :datetime
#  billing_activation           :datetime
#  subscription_amount_cents    :integer          default(0), not null
#  subscription_amount_currency :string(255)      default("USD"), not null
#  transaction_amount_cents     :integer          default(0), not null
#  transaction_amount_currency  :string(255)      default("USD"), not null
#  name                         :string(500)
#  notes                        :text
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  account_number               :string(12)
#  billing_frequency            :string(50)
#  stripe_customer_id           :string
#

require 'securerandom'

class Account < ApplicationRecord
    include ActiveModel::Dirty
    include Account::AccountBuilder

    QUERY_BY_SEARCH = "CAST(accounts.id as varchar(25)) LIKE :search OR
                    accounts.account_number LIKE :search OR
                    LOWER(accounts.name) LIKE :search OR
                    LOWER(accounts.account_status) LIKE :search OR
                    LOWER(accounts.account_sub_status) LIKE :search OR
                    LOWER(accounts.account_type) LIKE :search OR
                    LOWER(accounts.account_sub_type) LIKE :search".freeze

    # accounts belong to a partner. an account can belong
    # to multiple partners.
    belongs_to :partner

    before_update :prevent_update

    monetize :transaction_amount_cents, :allow_nil => true
    monetize :subscription_amount_cents, :allow_nil => true

    # account validation

    # a manager id is required for an account
    validates :manager_id, presence: {:message => I18n.t(:err_value_required)},
                           numericality: { greater_than: 0}
    # account status is required
    validates :account_status, presence: {:message => I18n.t(:err_value_required)}
    # account_sub_status is an informational field. no validation
    # payment_type is not always required. it is not there for free trials
    # account type is required
    validates :account_type, presence: {:message => I18n.t(:err_value_required)}
    # account sub type is required
    validates :account_sub_type, presence: {:message => I18n.t(:err_value_required)}
    # account name is limited to 500 characters
    validates :name, length: { maximum: 500}

    # custom validation
    # validate all the passed in enum values are valid options
    validate :enum_values_are_valid?,
             :trial_expiration_valid?,
             :payment_info_valid?

    # update marketing resources if account_status or account_sub_type have changed
    after_update_commit :update_marketing_resources,
                        if: proc { |record|
                            (record.previous_changes.key?(:account_status) && record.previous_changes[:account_status].first != record.previous_changes[:account_status].last) ||
                            (record.previous_changes.key?(:account_sub_type) && record.previous_changes[:account_sub_type].first != record.previous_changes[:account_sub_type].last)
                        }

    def enum_values_are_valid?
        checks = [
            { property: :account_status, cls: Account::AccountStatus },
            { property: :account_sub_status, cls: Account::AccountSubStatus },
            { property: :payment_type, cls: Account::PaymentType },
            { property: :account_type, cls: Account::AccountType },
            { property: :account_sub_type, cls: Account::AccountSubType }
        ]

        checks.each do |check|
            val = self.send(check[:property])
            if not val.nil? and not check[:cls].is_valid?(val)
                errors.add(check[:property], I18n.t(:err_is_invalid))
            end
        end
    end

    def trial_expiration_valid?
        if account_sub_type == Account::AccountSubType::FREE_TRIAL and
           trial_expiration.nil?
            errors.add(:trial_expiration, I18n.t(:err_value_required))
        end
    end

    def payment_info_valid?
        if account_sub_type == Account::AccountSubType::PAID
            if payment_type.nil?
                errors.add(:payment_type, I18n.t(:err_value_required))
            end

            if billing_frequency.nil?
                errors.add(:billing_frequency, I18n.t(:err_value_required))
            end
        end
    end

    # accessor for the manager of an account
    def manager
        Partner.where(:id => self.manager_id).first
    end

    # accessor to get all partners using the account
    def partners
        Partner.where(:account_id => self.id)
    end

    def generate_account_number
        self.account_number = 10.times.map{rand(10)}.join
        self.save!
    end

    def update_marketing_resources
        manager.update_marketing_resources
    end

    private

    def prevent_update
        return true if !self.account_number_changed? || self.account_number_was.nil?
        self.errors.add(:account_number, I18n.t(:err_value_cannot_be_updated))
        throw :abort
    end
end
