# == Schema Information
#
# Table name: warranty_configurations
#
#  id                  :integer          not null, primary key
#  automatic_purchases :boolean          default(FALSE)
#  account_number      :string(100)      not null
#  warranty_plan_id    :integer          not null
#  warranty_company_id :integer          not null
#  partner_id          :integer          not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class WarrantyConfiguration < ApplicationRecord
    include Warranty::Configuration::Actions

    # associations
    belongs_to :partner
    belongs_to :warranty_company
    belongs_to :warranty_plan
    
    has_many :warranty_plans, :primary_key => "partner_id", :foreign_key => "partner_id", :dependent => :destroy,  :class_name => PartnerWarrantyPlan


    # validations
    validates_presence_of   :account_number,  :message => I18n.t(:err_value_required)
    validates_length_of     :account_number,  :maximum => 100
    
    validates_presence_of   :partner_id,                :message => I18n.t(:err_value_required)
    validates_presence_of   :warranty_plan_id,          :message => I18n.t(:err_value_required)
    validates_presence_of   :warranty_company_id,       :message => I18n.t(:err_value_required)
    validates_uniqueness_of :partner_id
end
