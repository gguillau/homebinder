# == Schema Information
#
# Table name: organizations
#
#  id          :integer          not null, primary key
#  name        :string(250)
#  description :string(1000)
#

class Organization < ApplicationRecord
  # associations
  has_many :organization_partners, :dependent => :destroy
  has_many :partners, :through => :organization_partners
  has_many :organization_users, :dependent => :destroy
  has_many :users, :through => :organization_users
  has_many :dashboards, :dependent => :destroy
  has_many :organization_resources, :dependent => :destroy
  has_many :resources, :through => :organization_resources, :source => "marketing_resource"
  
  # validations
  validates :name, presence: true, length: { maximum: 250 }, unique: {}
end
