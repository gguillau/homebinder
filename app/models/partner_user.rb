# == Schema Information
#
# Table name: partner_users
#
#  id         :integer          not null, primary key
#  partner_id :integer
#  user_id    :integer
#  role       :string(100)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class PartnerUser < ApplicationRecord

    # associations
    belongs_to :partner
    belongs_to :user

    # validations
    validates_presence_of   :partner,   :message => I18n.t(:err_value_required)
    validates_presence_of   :user,      :message => I18n.t(:err_value_required)
    validates_presence_of   :role,      :message => I18n.t(:err_value_required)
    validates_length_of     :role,      :within  => 2..20
    validate :user_role,    :if => :user_id? && :partner_id?
    
    # one unique user per partner
    validates_uniqueness_of :user_id, scope: :partner_id, :message => "is already a partner user"

    before_validation :unique_admin
    before_destroy  :can_destroy?, prepend: true
    
    def unique_admin
        case role
        when "admin"
            if self.class.where.not(:id => id).where(:partner_id => partner_id, :role => "admin").count > 0
                errors.add(:role, "#{role} already exists for partner: #{partner_id}")
                throw :abort
            end
        end
    end

    def self.query_arguments
        ["partner_id", "user_id", "role", "partnerId"]
    end

    def self.query_arguments_hash(query, value)
        case query
        when "partner_id", "user_id", "role"
            {query.to_sym => value }
        when "partnerId"
            {:partner_id => value }
        end
    end
    
    def user_role
        if user.nil?
            errors.add(:user_id, I18n.t(:err_value_required))
        elsif user.is_admin?
            errors.add(:role, "User: #{user.email} is an admin and cannot be added to partner: #{partner_id}")
        elsif user.is_homeowner?
            errors.add(:role, "User: #{user.email} is a homeowner and cannot be added to partner: #{partner_id}")
        elsif user.is_agent? and role === "member" || role === "admin"
            errors.add(:role, "User: #{user.email} is an agent and cannot be added as an admin/member to partner: #{partner_id}")
        elsif user.role === "broker" and partner.partner_type != "broker" and role === "member" || role === "admin"
            errors.add(:role, "User: #{user.email} is a #{user.role} and cannot be added as a member to partner: #{partner_id}")
        elsif user.role === "inspector" || user.role === "apr_reviewer" and partner.partner_type != "inspector" and role === "member" || role === "admin"
            errors.add(:role, "User: #{user.email} is a #{user.role} and cannot be added as a member to partner: #{partner_id}")
        end
    end
    
    def can_destroy?
        if role === "admin"
            errors.add(:role, "User: #{user.email} is an admin for partner: #{partner_id} and cannot be deleted")
            throw :abort
        end
    end
end
