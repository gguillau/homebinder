class Host
  
  LOCAL_HOST = "http://localhost:5000".freeze
  STAGING_HOST = "http://staging.homebinder.com".freeze
  TEST_HOST = "http://test.homebinder.com".freeze
  PRODUCTION_HOST = "http://homebinder.com".freeze
  
  def self.path
    if Rails.env.development?
      return ENV["DOMAIN"] || LOCAL_HOST
    end
    if Rails.env.testing?
      return TEST_HOST
    end
    if Rails.env.staging?
      return STAGING_HOST
    end
    if Rails.env.production?
      return PRODUCTION_HOST
    end
  end
  
  def self.generate_host_path
    if Rails.env.production?
      return "www.homebinder.com"
    elsif Rails.env.testing?
      return "test.homebinder.com"
    elsif Rails.env.staging?
      return "staging.homebinder.com"
    else
      return Host.path
    end
  end
end