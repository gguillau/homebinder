# == Schema Information
#
# Table name: binders
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  primary       :boolean
#  active        :boolean          default(TRUE)
#  created_by    :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  details       :text
#  hero_image_id :integer
#  create_method :string(255)
#  status        :string           default("active")
#

class Binder < ApplicationRecord
    include Binder::BinderActions

    require 'stripe'
    require 'securerandom'

    # relationships
    has_one :subscription, :dependent => :destroy
    has_one :property, :dependent => :destroy
    has_one :seller_report, :dependent => :destroy

    has_many :abs_files,          :dependent => :destroy
    has_many :structures,         :dependent => :destroy
    has_many :areas,              :dependent => :destroy
    has_many :appliances,         :dependent => :destroy
    has_many :finishes,           :dependent => :destroy
    has_many :paints,             :dependent => :destroy
    has_many :binder_contractors, :dependent => :destroy
    has_many :projects,           :dependent => :destroy
    has_many :maintenance_items,  :dependent => :destroy
    has_many :inventory_items,    :dependent => :destroy
    has_many :receipts,           :dependent => :destroy
    has_many :documents,          :dependent => :destroy
    has_many :images,             :dependent => :destroy
    has_many :permits,            :dependent => :destroy
    has_many :binder_items,       :dependent => :destroy

    has_many  :binder_brandings,    :dependent => :destroy
    validates :binder_brandings, length: {maximum: 8, message: " - Maximum of 8 branding users per binder required"}, :on => :update

    has_many :user_binders,         :dependent => :destroy
    has_many :users,                :through => :user_binders
    belongs_to :creator, :class_name => "User", :foreign_key => "created_by"

    has_many :partner_binders
    has_many :partners, :through => :partner_binders

    has_many :notes, :dependent => :destroy
    has_many :tags,       :as => :taggable, :dependent => :destroy

    has_many :annual_property_reviews

    has_many :shares,             :dependent => :destroy
    has_many :transfers,          :dependent => :destroy
    has_many :warranties,         :dependent => :destroy
    has_many :transactions,       :dependent => :destroy,  :class_name => Binder::Transaction

    has_many :binder_items, :dependent => :destroy

    belongs_to :hero_image, class_name: "Binder::Image", foreign_key: "hero_image_id"

    # validations
    validates_presence_of :name, :message => I18n.t(:err_value_required)
    validates_length_of :name, :maximum => 50

    accepts_nested_attributes_for :property
    accepts_nested_attributes_for :subscription
    accepts_nested_attributes_for :binder_brandings

    def get_subscription_detail
        if self.subscription.customer_id.nil?
            return
        end

        customer = Stripe::Customer.retrieve(self.subscription.customer_id)
        @subscription = customer.subscription
    end

    def get_owner
        owner
    end

    def owner
        User.joins(:user_binders).where(:user_binders => {:role => "owner", :binder_id => self.id}).first
    end

    def partner
        Partner.joins(:partner_binders).where(:partner_binders => {:binder_id => self.id}).first
    end

    def full_address
        property.full_address
    end

    def self.with_role(role, user)
        return joins(:user_binders).where(:user_binders => {:user_id => user.id})
    end

    # settings do
    #     mappings dynamic: true do
    #         indexes :name,              type: :keyword
    #         indexes :active,            type: :boolean
    #         indexes :primary,           type: :boolean
    #         indexes :created_at,        type: :date
    #         indexes :create_method,     type: :keyword
    #         indexes :status,            type: :keyword
    #         indexes :details,           type: :text
    #         indexes :transfers do
    #             indexes :status,    type: :keyword
    #         end
    #         indexes :shares do
    #             indexes :status,    type: :keyword
    #         end
    #         indexes :property do
    #             indexes :address1,  type: :keyword
    #             indexes :address2,  type: :keyword
    #             indexes :zip,       type: :keyword
    #             indexes :city,      type: :keyword
    #             indexes :state,     type: :keyword
    #             indexes :country,   type: :keyword
    #         end
    #     end
    # end

    # def as_indexed_json(options={})
    #     self.as_json(
    #         {
    #             only: [:name, :active, :primary, :created_at, :create_method, :status, :details],
    #             include: [:transfers, :shares, :property]
    #         }
    #     )
    # end
end
