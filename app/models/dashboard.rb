# == Schema Information
#
# Table name: dashboards
#
#  id              :integer          not null, primary key
#  name            :string(100)
#  description     :string(1000)
#  organization_id :integer
#  partner_id      :integer
#  binder_id       :integer
#  default         :boolean          default(FALSE)
#  user_id         :integer
#  system          :boolean          default(FALSE)
#  scope           :string
#

class Dashboard < ApplicationRecord
  
  include Dashboard::DashboardBuilder
  
  belongs_to :user
  belongs_to :partner
  belongs_to :organization
  
  has_many :dashboard_widgets, -> { order(index: :asc) }, :dependent => :destroy
  has_many :widgets, through: :dashboard_widgets
  
  # validations
  validates :name, presence: true, length: { maximum: 100 } 
  validates :description, length: { maximum: 1000 }
  validate :validate_owner, :validate_scope
  
  scope :system, -> { where(system: true) }
  scope :system_inspector_binder, -> { system.where(scope: DashboardScope::INSPECTOR_BINDER) }
  scope :system_broker_binder, -> { sytem.where(scope: DashboardScope::BROKER_BINDER) }
  scope :system_homeowner_binder, -> { system.where(scope: DashboardScope::HOMEOWNER_BINDER) }
  
  def validate_owner
    if user_id.present?
      if organization_id.present?
        errors.add(:organization_id, I18n.t(:err_owner_already_set))
      end
      if partner_id.present?
        errors.add(:partner_id, I18n.t(:err_owner_already_set))
      end
      if system === true
        errors.add(:system, I18n.t(:err_owner_already_set))
      end
    elsif organization_id.present?
      if partner_id.present?
        errors.add(:partner_id, I18n.t(:err_owner_already_set))
      end
      if system === true
        errors.add(:system, I18n.t(:err_owner_already_set))
      end
    elsif partner_id.present?
      if system === true
        errors.add(:system, I18n.t(:err_owner_already_set))
      end
    else
      if system != true
        errors.add(:system, I18n.t(:err_owner_not_set))
      end
    end
  end
  
  def validate_scope
    if user_id.present? or organization_id.present? or partner_id.present?
      if scope != DashboardScope::BINDER.to_s and
         scope != DashboardScope::USER.to_s
        errors.add(:scope, I18n.t(:err_org_partner_scope))
      end
    else
      if system === true
        if scope != DashboardScope::BINDER.to_s and
           scope != DashboardScope::USER.to_s and
           scope != DashboardScope::INSPECTOR.to_s and
           scope != DashboardScope::BROKER.to_s and
           scope != DashboardScope::PARTNER.to_s and
           scope != DashboardScope::AGENT.to_s and
           scope != DashboardScope::ADMIN.to_s
          errors.add(:scope, I18n.t(:err_system_scope))
        end
      end
    end
  end
end
