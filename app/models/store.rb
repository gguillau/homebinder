# == Schema Information
#
# Table name: stores
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_by :integer
#  verified   :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Store < ApplicationRecord
  
  # validations
  validates :name, presence: true, length: { maximum: 250 }, unique: {}
    
  # attr_accessible :title, :body
  before_save { |s| s.name = s.name.downcase }
  
  def self.available(user_id)
    Store.where("verified = ? OR created_by = ?", true, user_id)
  end
end
