module Warranty::Company::Actions

    def self.included(klass)
        klass.extend ClassMethods 
    end
    
    module ClassMethods
        def base_query
            "CAST(warranty_companies.id as varchar(25)) LIKE :search OR
            LOWER(warranty_companies.name) LIKE :search OR LOWER(warranty_companies.contact) LIKE :search 
            OR LOWER(warranty_companies.email) LIKE :search".freeze
        end
        
        def add_custom_args(where, where_not, params, includes, joins)
            # check if user is searching by name
            if params[:name]
                where.merge!({:name => params[:name]})
            end
            super
        end
    end
end