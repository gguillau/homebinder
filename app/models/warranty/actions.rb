module Warranty::Actions

    def self.included(klass)
        klass.extend ClassMethods 
    end
    
    module ClassMethods
        
        def index(hb_request, params)
            if !hb_request.user.is_admin?
                raise BadRequestException.new "Binder ID is Required" if params[:binderId].nil?
                
                binder = Binder.find(params[:binderId])
                hb_request.ability.authorize! :read, binder, :message => "You are not authorized to access binder ID: #{binder.id}."
            end
            
            pager(hb_request, params)
        end
        
        def base_query
            "CAST(warranties.id as varchar(25)) LIKE :search OR
            CAST(warranties.binder_id as varchar(25)) LIKE :search OR
            LOWER(warranties.status) LIKE :search OR
            LOWER(binders.name) LIKE :search OR
            LOWER(users.email) LIKE :search OR
            LOWER(user_profiles.first_name) LIKE :search OR
            LOWER(user_profiles.last_name) LIKE :search OR
            LOWER(CONCAT(properties.address1, ' ', properties.address2, ' ', properties.city, ', ', properties.state, ' ', properties.zip)) LIKE :search".freeze
        end
        
        def base_includes
            [:binder, :binder => [:property, :users => :user_profile]]
        end
        
        def build(hb_request, params)
            # find the binder
            binder = Binder.find(params[:warranty][:binder_id])

            # check if the user can write to the binder
            hb_request.ability.authorize! :write, binder, :message => "You are not authorized to access binder ID: #{binder.id}."
            
            super
        end
        
        def update(user, params)
            w = super
            
            # update the warranty transaction price
            w.binder_transaction.transaction_cost = params[:price]
            w.binder_transaction.save!
            
            # return the updated warranty
            return w
        end
        
        def add_custom_args(where, where_not, params, includes, joins)
            # check if user is searching by binder_id
            if params[:binderId]
                where.merge!({:binder_id => params[:binderId]})
            end
            
            # check if user is searching by partner_id
            if params[:partnerId]
                includes.push(:binder_transaction => :partner_binder_transaction)
                where.merge!(:partner_binder_transactions => {:partner_id => params[:partnerId]})
            end
        end
    end
end