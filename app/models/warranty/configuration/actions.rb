module Warranty::Configuration::Actions

    def self.included(klass)
        klass.extend ClassMethods
    end

    module ClassMethods

        def build(hb_request, params)
            warranty_configuration = super

            # create the partner warranty plans
            partner_warranty_plans = params[:partner_warranty_plans]

            # create the plan price
            partner_warranty_plans.each do |plan|
                PartnerWarrantyPlan.create!(
                    :partner_id => params[:warranty_configuration][:partner_id],
                    :price => plan[:price],
                    :warranty_plan_id => plan[:id]
                )
            end

            # get the partner_id and update the config
            partner_id = params[:warranty_configuration][:partner_id]
            Partner::Configuration.where(:partner_id => partner_id).update_all(:active_warranty_account => true)

            # return the warranty_configuration
            return warranty_configuration
        end

        def update(hb_request, params)
            wc = super

            # create the partner warranty plans
            partner_warranty_plans = params[:partner_warranty_plans]

            # create the plan price
            partner_warranty_plans.each do |plan|
                # update the plan price
                if PartnerWarrantyPlan.where(:partner_id => wc.partner_id, :warranty_plan_id => plan[:id]).count > 0
                    PartnerWarrantyPlan.where(:partner_id => wc.partner_id, :warranty_plan_id => plan[:id]).update_all(:price => plan[:price])
                else
                    # create the plan with a price
                    PartnerWarrantyPlan.create!(:partner_id => wc.partner_id, :price => plan[:price], :warranty_plan_id => plan[:id])
                end
            end

            # return the updated warranty_configuration
            return wc
        end

        def add_custom_args(where, where_not, params, includes, joins)
            # check if user is searching by binder_id
            if params[:binderId]
                where.merge!({:binder_id => params[:binderId]})
            end

            # check if user is searching by name
            if params[:name]
                where.merge!({:name => params[:name]})
            end

            # check if the query_arguments method is defined
            return if not self.respond_to? :query_arguments

            # check if the argument is defined in the class as a query argument
            params.each do |arg|
                if self.query_arguments.include?(arg)
                    where.merge!(self.query_arguments_hash(arg, params[arg]))
                end
            end
        end
    end
end