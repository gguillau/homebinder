module Warranty::Plan::Actions

    def self.included(klass)
        klass.extend ClassMethods 
    end
    
    def remove
        # remove any partner warranty plans related to the plan
        PartnerWarrantyPlan.where(:warranty_plan_id => self.id).delete_all
        # still runs in background since we call super
        super
    end
    
    module ClassMethods
        
        def base_query
            "CAST(warranty_plans.id as varchar(25)) LIKE :search OR LOWER(warranty_plans.description) LIKE :search OR
            CAST(warranty_plans.duration as varchar(25)) LIKE :search OR
            LOWER(warranty_plans.name) LIKE :search OR
            LOWER(warranty_plans.cycle) LIKE :search".freeze
        end
        
        def build(hb_request, params)
            warranty_plan = super
            
            # find all warranty_configurations where the warranty company is the same as the warranty company on the plan
            warranty_configurations = WarrantyConfiguration.where(:warranty_company_id => warranty_plan.warranty_company_id)
            
            # add the partner_plan if the plan doesn't exist
            warranty_configurations.each do |config|
                PartnerWarrantyPlan.create!(:partner_id => config.partner_id, :price => warranty_plan.price, :warranty_plan_id => warranty_plan.id)
            end
            
            # return the warranty_plan
            return warranty_plan
        end
        
        def add_custom_args(where, where_not, params, includes, joins)
            # check if user is searching by binder_id
            if params[:warranty_company_id]
                where.merge!({:warranty_company_id => params[:warranty_company_id]})
            end
        end
    end
end