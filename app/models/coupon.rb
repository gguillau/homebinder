# == Schema Information
#
# Table name: coupons
#
#  id              :integer          not null, primary key
#  partner_id      :integer
#  code            :string(255)
#  duration        :string(255)
#  expires_on      :datetime
#  max_redemptions :integer
#  active          :boolean
#  percent_off     :integer
#  amount_off      :integer
#  duration_months :integer
#

class Coupon < ApplicationRecord
  # attr_accessible :title, :body
  #resourcify
  
  belongs_to :partner
end
