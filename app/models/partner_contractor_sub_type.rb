class PartnerContractorSubType < ApplicationRecord
    self.table_name = "partner_contractor_sub_types"
    
    belongs_to  :partner_contractor,   :class_name => "PartnerContractor"
    belongs_to  :contractor_sub_category,   :class_name => "Contractor::SubCategory"
    
    # validations
    validates_uniqueness_of :partner_contractor, scope: :contractor_sub_category, :message => "already exists"
    validates_presence_of       :partner_contractor, :message => I18n.t(:err_value_required), :on => :update
    validates_presence_of       :contractor_sub_category, :message => I18n.t(:err_value_required)
end
