# == Schema Information
#
# Table name: annual_property_reviews
#
#  id                         :integer          not null, primary key
#  binder_id                  :integer
#  uuid                       :string(32)       not null
#  client_first_name          :string(100)      not null
#  client_last_name           :string(100)      not null
#  client_email               :string(100)      not null
#  client_phone               :string(30)       not null
#  address1                   :string(100)      not null
#  address2                   :string(100)
#  city                       :string(100)      not null
#  state                      :string(100)      not null
#  zip                        :string(100)      not null
#  country                    :string(100)      not null
#  status                     :integer          default("active"), not null
#  reviewer_id                :integer
#  reviewer_company_id        :integer
#  scheduled_for              :datetime
#  scheduled_on               :datetime
#  completed_on               :datetime
#  on_hold_on                 :datetime
#  canceled_on                :datetime
#  client_comments            :text
#  score                      :integer          default("again")
#  cancel_reason              :text
#  amount_charged_cents       :integer          default(0), not null
#  amount_charged_currency    :string           default("USD"), not null
#  payment_status             :integer          default("not_applicable")
#  reviewed_on                :datetime
#  property_reviewer_comments :text
#  admin_reviewer_comments    :text
#  stripe_charge_id           :string(50)
#  stripe_refund_id           :string(50)
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  snapshot_file_name         :string
#  snapshot_content_type      :string
#  snapshot_file_size         :integer
#  snapshot_updated_at        :datetime
#  promo_code_name            :string
#  promo_code_amount_off      :integer
#

class AnnualPropertyReview < ApplicationRecord
  include AnnualPropertyReview::AnnualPropertyReviewBuilder
  
  belongs_to :binder
  
  has_many :annual_property_review_findings,          :dependent => :destroy
  has_many :annual_property_review_photos,            :dependent => :destroy
  has_many :annual_property_review_capital_items,     :dependent => :destroy
  
  belongs_to :reviewer, :class_name => "User", :foreign_key => "reviewer_id"
  belongs_to :reviewer_company, :class_name => "Partner", :foreign_key => "reviewer_company_id"
  
  validates :client_first_name, presence: true, length: { maximum: 100 }
  validates :client_last_name, presence: true, length: { maximum: 100 }
  validates :client_email, presence: true, length: { maximum: 100 }
  validates :client_phone, presence: true, length: { maximum: 30 }
  validates :address1, presence: true, length: { maximum: 100 }
  validates :address2, length: { maximum: 100 }
  validates :city, presence: true, length: { maximum: 100 }
  validates :state, presence: true, length: { maximum: 100 }
  validates :zip, presence: true, length: { maximum: 100 }
  validates :country, presence: true, length: { maximum: 100 }
  validates :cancel_reason, length: { maximum: 1000 }

  monetize :amount_charged_cents, allow_nil: true

  enum status: {
    active: 0,
    scheduled: 1,
    draft: 2,
    draft_completed: 3,
    completed: 4,
    canceled: 5,
    on_hold: 6,
    unassigned: 7
  }, _prefix: :status
  enum score: {
    again: 0,
    not_again: 1,
    no_value: 2
  }, _prefix: :score
  enum payment_status: {
    not_applicable: 0,
    paid: 1,
    refunded: 2
  }, _prefix: :payment_status
  
  def client_phone_formatted
    return nil if self.client_phone.nil?
    return nil if not Phony.plausible?(self.client_phone)
    return self.client_phone.phony_formatted(:format => :national, :spaces => '-')
  end
end
