# == Schema Information
#
# Table name: partner_binders
#
#  id           :integer          not null, primary key
#  partner_id   :integer
#  binder_id    :integer
#  created_at   :datetime
#  updated_at   :datetime
#  role         :string(100)
#  dashboard_id :integer
#  thanked      :boolean            default: false
#

class PartnerBinder < ApplicationRecord

    # associations
    belongs_to  :binder
    belongs_to  :partner
    belongs_to  :user,          :foreign_key => "client_id"
    belongs_to  :partner_user,  :foreign_key => "partner_user_id"
    
    # validations
    validates_presence_of   :partner,       :message => I18n.t(:err_value_required)
    validates_presence_of   :binder,        :message => I18n.t(:err_value_required)
    validates_presence_of   :user,          :message => I18n.t(:err_value_required)
    validates_presence_of   :partner_user,  :message => I18n.t(:err_value_required)
    
    # one unique partner per binder
    validates_uniqueness_of :partner_id,    scope: :binder_id, :message => "Binder already exists"
    
    def self.query_arguments
        ["binderId", "binder_id", "partnerId", "partner_id", "role", "repair_pricer_enabled"]
    end

    def self.query_arguments_hash(query, value)
        case query
        when "binderId", "binder_id"
            {:binder_id => value}
        when "partnerId", "partner_id"
            {:partner_id => value}
        when "role"
            {:role => value}
        when "repair_pricer_enabled"
            {:repair_pricer_enabled => value}
        else
            {}
        end
    end
    
    settings do
        mappings dynamic: false do
            indexes :role,                      type: :keyword
            indexes :binder_id,                 type: :integer
            indexes :client_id,                 type: :integer
            indexes :partner_id,                type: :integer
            indexes :partner_user_id,           type: :integer
            indexes :created_at,                type: :date
            indexes :repair_pricer_enabled,     type: :boolean
            indexes :accepted,                  type: :boolean
            indexes :partner do
                indexes :id,                        type: :integer
                indexes :partner_type,              type: :keyword
                indexes :partner_configuration do
                    indexes :default_binder_action,                 type: :keyword
                    indexes :automation_binder_action,              type: :boolean
                    indexes :binder_action_delay_length,            type: :integer
                    indexes :send_agents_transfer_notification,     type: :keyword
                    indexes :show_isn_tab,                          type: :boolean
                    indexes :default_user_branding_option,          type: :keyword
                    indexes :allow_client_calls,                    type: :boolean
                    indexes :active_warranty_account,               type: :boolean
                    indexes :can_perform_apr,                       type: :boolean
                    indexes :annual_property_review_cost_cents,     type: :integer
                    indexes :annual_property_review_cost_currency,  type: :keyword
                    indexes :default_transaction_type,              type: :keyword
                    indexes :repair_pricer_enabled,                 type: :boolean
                    indexes :repair_pricer_pre_paid_reports,        type: :boolean
                    indexes :repair_pricer_messaging_enabled,       type: :boolean
                end
            end
        end
    end
    
    def as_indexed_json(options={})
        self.as_json(
            {
                only: [:role, :binder_id, :partner_id, :created_at, :client_id, :partner_user_id, :repair_pricer_enabled, :accepted
                ],
                include: [:partner => {include: [:partner_configuration]}]
            }
        )
    end
end
