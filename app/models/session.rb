# == Schema Information
#
# Table name: sessions
#
#  id         :integer          not null, primary key
#  token      :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  expires_at :datetime
#

class Session < ApplicationRecord
    # associations
    belongs_to :user
    
    # validations
    validates_presence_of   :token,             :message => I18n.t(:err_value_required)
    validates_presence_of   :expires_at,        :message => I18n.t(:err_value_required)
    validates_presence_of   :user,              :message => I18n.t(:err_value_required)
    
    enum status: {
         active: 0,
         inactive: 1
     }, _prefix: :status
    
    def self.query_arguments
        ["user_id"]
    end

    def self.query_arguments_hash(query, value)
        case query
        when "user_id"
          { :user_id => value }
        end
    end

end
