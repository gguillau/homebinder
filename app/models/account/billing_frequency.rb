class Account
  class BillingFrequency
    PER_TRANSACTION = "per_transaction".freeze
    MONTHLY = "monthly".freeze
    QUARTERLY = "quarterly".freeze
    ANNUAL = "annual".freeze
    
    def self.is_valid?(value)
      return [PER_TRANSACTION, MONTHLY, QUARTERLY, ANNUAL].include?(value)
    end
  end
end