module Account::AccountBuilder

    def self.included(klass)
        klass.extend ClassMethods
    end

    module ClassMethods

        def base_query
            Account::QUERY_BY_SEARCH
        end

        def build(hb_request, params)
            partner_id = params[:account][:manager_id]
            partner = Partner.find(partner_id)

            # authorize the user to create accounts
            hb_request.ability.authorize! :write, partner, :message => "You are not authorized to write to partner ID: #{partner.id}."

            account = super(hb_request, params)
            account.generate_account_number

            # add the account to the managing partner
            partner.account_id = account.id
            if not partner.save
                ErrorService.perform_async(partner.errors.full_messages.first, {:task => "build"}, hb_request.user&.id)
            end

            # analytics
            Partners::UpdatedJob.perform_async(partner.id)
            
            # salesforce
            PartnerService.new(hb_request.user, partner).update_salesforce
            
            if hb_request.present? && hb_request.user
                EventService.perform_async({:event_type => "engagement", :event_name => "#{account.class.table_name.singularize} updated", :user_id => hb_request.user.id, :user_role => hb_request.user.role, :user_created_at => hb_request.user.created_at, :user_create_method => hb_request.user.create_method})
            end

            return account
        end

        # find account by account/partner ID
        # I kept it flexibile since we don't really have an account index view

        def show(hb_request, params)
            raise BadRequestException.new "#{self.name.titleize} ID required" if params[:id].nil?

            # get the item
            item = find_by_manager_id(params[:id])
            raise NotFoundException.new "Item not found" if item.nil?
            
            # check if the user can read it
            hb_request.ability.authorize! :read, item, :message => "You are not authorized to access account ID: #{item.id}."

            return item
        end

        def update(hb_request, params)
            account = super(hb_request, params)

            # get the partner
            partner = Partner.find(account.manager_id)

            # analytics
            Partners::UpdatedJob.perform_async(partner.id)

            # salesforce
            PartnerService.new(hb_request.user, partner).update_salesforce
            
            if hb_request.present? && hb_request.user
                EventService.perform_async({:event_type => "engagement", :event_name => "#{account.class.table_name.singularize} updated", :user_id => hb_request.user.id, :user_role => hb_request.user.role, :user_created_at => hb_request.user.created_at, :user_create_method => hb_request.user.create_method})
            end
            
            return account
        end
    end
end