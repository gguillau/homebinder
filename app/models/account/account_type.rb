class Account
  class AccountType
    SINGLE = "single".freeze
    CORPORATE = "corporate".freeze
    
    def self.is_valid?(value)
      return [SINGLE, CORPORATE].include?(value)
    end
  end
end