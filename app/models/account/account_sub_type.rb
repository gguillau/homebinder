class Account
  class AccountSubType
    FREE_TRIAL = "free_trial".freeze
    PAID = "paid".freeze
    
    def self.is_valid?(value)
      return [FREE_TRIAL, PAID].include?(value)
    end
  end
end