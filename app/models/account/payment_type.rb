class Account
  class PaymentType
    SUBSCRIPTION = "subscription".freeze
    TRANSACTIONAL = "transactional".freeze
    
    def self.is_valid?(value)
      return [SUBSCRIPTION, TRANSACTIONAL].include?(value)
    end
  end
end