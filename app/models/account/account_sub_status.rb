class Account
  class AccountSubStatus
    INVALID_CREDIT_CARD = "invalid_credit_card".freeze
    FREE_TRIAL_EXPIRED = "free_trial_expired".freeze
    NONE = "none".freeze
    
    def self.is_valid?(value)
      return [INVALID_CREDIT_CARD, FREE_TRIAL_EXPIRED, NONE].include?(value)
    end
  end
end
  