class Account
  class AccountStatus
    ACTIVE = "active".freeze
    EXPIRED = "expired".freeze
    SUSPENDED = "suspended".freeze
    CLOSED = "closed".freeze
    CANCELED = "canceled".freeze
    WHERE = "account_status = ? AND account_sub_type = ? AND trial_expiration < ?".freeze
    
    def self.is_valid?(value)
      return [ACTIVE, EXPIRED, SUSPENDED, CLOSED, CANCELED].include?(value)
    end
  end
end