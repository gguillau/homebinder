module Dashboard::DashboardBuilder

    def self.included(klass)
        klass.extend ClassMethods
    end

    module ClassMethods

        def index(hb_request, params, search_method = :search)
            if hb_request.user.belongs_to_partner?
                pager(hb_request, params, :owned_by_partner)
            else
                super
            end
        end

        def build(hb_request, params)
            # ensure widgets exist
            ensure_widgets_exist!(params)

            # create the dashboard
            board = Dashboard.new(params[:dashboard].permit!)

            # authorize
            if board.user_id.present?
                user = User.find(board.user_id)
                hb_request.ability.authorize! :write, user, :message => "You are not authorized to access user ID: #{user.id}."
            elsif board.partner_id.present?
                partner = Partner.find(board.partner_id)
                hb_request.ability.authorize! :write, partner, :message => "You are not authorized to access partner ID: #{partner.id}."
            elsif board.organization_id.present?
                org = Organization.find(board.organization_id)
                hb_request.ability.authorize! :write, org, :message => "You are not authorized to access organization ID: #{org.id}."
            else
                hb_request.ability.authorize! :create, Dashboard, :message => "You are not authorized to create Dashboards."
            end

            # save
            if not board.save
                raise UnprocessableException.new board
            end

            # add the widgets
            if params[:widgets].present?
                params[:widgets].each do |widget|
                    DashboardWidget.create!(dashboard_id: board.id, widget_id: widget[:widget_id], index: widget[:index])
                end
            end

            # ensure only 1 default
            if board.default === true
                enforce_single_default board
            end

            board
        end

        def update(hb_request, params)
            board = Dashboard.find(params[:id])

            hb_request.ability.authorize! :write, board, :message => "You are not authorized to access dashboard ID: #{board.id}."

            # ensure widgets exist
            ensure_widgets_exist!(params)

            # if this is a system dashboard make sure the result will still
            # leave a default dashboard for the scope
            if params[:dashboard][:system] === true
                if params[:dashboard][:default] === false
                    current_default = Dashboard.where(system: true, scope: params[:dashboard][:scope], default: true).first
                    if current_default.present? and current_default.id == board.id
                        raise BadRequestException.new I18n.t(:err_default_board_not_set)
                    end
                end
            end

            # update the dashboard
            if not board.update_attributes(params[:dashboard].permit!)
                raise UnprocessableException.new board
            end

            # update the widgets
            if params[:widgets]
                if params[:widgets].length == 0
                    DashboardWidget.where(dashboard_id: board.id).delete_all
                else
                    # remove any widgets on the dashboard but not params
                    board.dashboard_widgets.each do |dwidget|
                        if not params[:widgets].index { |widget| widget[:id].present? and (dwidget.id == widget[:id]) }
                            dwidget.destroy
                        end
                    end

                    # update and add widgets
                    params[:widgets].each do |dwidget|
                        # if the dashboard widget exists update otherwise add it
                        if dwidget[:id]
                            # only thing to update is index
                            dw = DashboardWidget.find(dwidget[:id])
                            dw.index = dwidget[:index]
                            dw.save!
                        else
                            DashboardWidget.create!(dashboard_id: board.id, widget_id: dwidget[:widget_id], index: dwidget[:index])
                        end
                    end
                end
            end

            # ensure only 1 default
            if board.default === true
                enforce_single_default board
            end

            board.reload
        end

        def base_query
            "CAST(dashboards.id as varchar(25)) LIKE :search OR
                   LOWER(dashboards.name) LIKE :search OR
                   LOWER(dashboards.description) LIKE :search OR
                   LOWER(dashboards.scope) LIKE :search".freeze
        end

        def owned_by_system(params = {}, hb_request)
            hb_request.ability.authorize! :list, Dashboard, :message => "You are not authorized to access system dashboards."

            query = get_base_query

            # search
            searchValue = params[:search]
            if searchValue.present?
                self.system.where(query, search: "%#{searchValue}%".downcase)
            else
                self.system
            end
        end

        def owned_by_user(params = {}, hb_request)
            searchValue = params[:search]

            query = get_base_query

            # search
            searchValue = params[:search]
            if searchValue.present?
                hb_request.user.dashboards.where(query, search: "%#{searchValue}%".downcase)
            else
                hb_request.user.dashboards
            end
        end

        def owned_by_organization(params = {}, hb_request)
            id = params[:id]
            raise BadRequestException.new unless id.present?

            org = Organization.find(id)

            hb_request.ability.authorize! :read, org, :message => "You are not authorized to access organization ID: #{org.id}."

            query = get_base_query

            # search
            searchValue = params[:search]
            if searchValue.present?
                org.dashboards.where(query, search: "%#{searchValue}%".downcase)
            else
                org.dashboards
            end
        end

        def owned_by_partner(params = {}, hb_request)
            id = params[:id] || params[:partnerId]
            raise BadRequestException.new unless id.present?

            partner = Partner.find(id)

            hb_request.ability.authorize! :read, partner, :message => "You are not authorized to access partner ID: #{partner.id}."

            query = get_base_query

            # search
            searchValue = params[:search]
            if searchValue.present?
                partner.dashboards.where(query, search: "%#{searchValue}%".downcase)
            else
                partner.dashboards
            end
        end

        def query_arguments
            ["partnerId"]
        end

        def query_arguments_hash(query, value)
            case query
            when "partnerId"
                {:partner_id => value}
            end
        end

        private

        def enforce_single_default(board)
            if board.user_id.present?
                Dashboard.where(user_id: board.user_id, default: true, scope: board.scope).each do |db|
                    if db.id != board.id
                        db.default = false
                        db.save!
                    end
                end
            elsif board.partner_id.present?
                Dashboard.where(partner_id: board.partner_id, default: true, scope: board.scope).each do |db|
                    if db.id != board.id
                        db.default = false
                        db.save!
                    end
                end
            elsif board.organization_id.present?
                Dashboard.where(organization_id: board.organization_id, default: true, scope: board.scope).each do |db|
                    if db.id != board.id
                        db.default = false
                        db.save!
                    end
                end
            else
                if board.system === true
                    Dashboard.where(default: true, system: true, scope: board.scope).each do |db|
                        if db.id != board.id
                            db.default = false
                            db.save!
                        end
                    end
                end
            end
        end

        def ensure_widgets_exist!(params)
            if params[:widgets].present?
                widget_ids = []
                params[:widgets].each do |widget|
                    widget_ids << widget[:widget_id]
                end
                Widget.find(widget_ids)
            end
        end
    end
end
