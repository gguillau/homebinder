# == Schema Information
#
# Table name: widget_exclusions
#
#  id              :integer          not null, primary key
#  user_id         :integer
#  partner_id      :integer
#  organization_id :integer
#  widget_id       :integer
#  widget_category :string(100)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class WidgetExclusion < ApplicationRecord
    
    # relationships
    belongs_to  :user
    belongs_to  :partner
    belongs_to  :organization
    belongs_to  :widget
    
    # exclude 1 widget/category per user
    validates :widget, uniqueness: {scope: [:partner_id, :user_id, :organization_id], allow_nil: true, allow_blank: true, message: "has already been excluded"}
    validates :widget_category, uniqueness: {scope: [:partner_id, :user_id, :organization_id], allow_nil: true, allow_blank: true, :case_sensitive => true, message: "has already been excluded"}

end
