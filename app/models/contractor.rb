# == Schema Information
#
# Table name: contractors
#
#  id                 :integer          not null, primary key
#  contractor_type_id :integer
#  name               :string(255)
#  phone              :string(255)
#  email              :string(255)
#  url                :string(255)
#  account_number     :string(255)
#  contact            :string(255)
#  details            :text
#  created_by         :integer
#  verified           :boolean
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  contractor_type    :string(255)
#

require 'phony_rails'

class Contractor < ApplicationRecord
  self.table_name = "contractors"

  include Contractor::Actions

  belongs_to :creator, :class_name => "User", :foreign_key => "created_by"
  belongs_to :user

  has_many    :binder_contractors, :class_name => "Binder::BinderContractor"
  has_many    :partner_contractors, :class_name => "PartnerContractor"
  has_many    :template_contractors, :class_name => "Template::Contractor", :through => :partner_contractors
  has_many    :binders, :through => :binder_contractors
  has_one     :address, :dependent => :destroy

  has_many    :contractor_types, :class_name => "ContractorCategory", :dependent => :destroy
  has_many    :types, :through => :contractor_types, :source => "contractor_category"
  has_many    :contractor_sub_types, :dependent => :destroy
  has_many    :sub_types, :through => :contractor_sub_types, :source => "contractor_sub_category"

  accepts_nested_attributes_for :address
  accepts_nested_attributes_for :types
  accepts_nested_attributes_for :contractor_types
  accepts_nested_attributes_for :sub_types
  accepts_nested_attributes_for :contractor_sub_types

  before_save { |c| c.name = c.name.downcase }

  phony_normalize :phone, :default_country_code => 'US'

  validates_presence_of     :name,        :message => I18n.t(:err_value_required)
  validates_presence_of     :created_by, :message => I18n.t(:err_value_required)
  validates_length_of       :name, :maximum => 100
  validates_uniqueness_of   :email, allow_blank: true, if: :email_changed?
  validates_length_of       :url, :maximum => 250

  # make sure to downcase emails
  before_validation :format_email
  before_validation :valid_types

  def valid_types
    if types.empty? && contractor_types.empty?
      errors.add(:types, "- Minimum of 1 type per pro required")
    end
  end

  def send_invitation_email
    return if email.nil?
    # check if user is present - most likely a non-homepro
    return if user.present?

    Users::Update.new.create_home_pro(id)
  end

  def format_email
      return if email.nil?
      self.email = Partner::Automation::EmailParser.new.format_value(["", "email"], email.downcase.gsub(/\s+/, ""))
      self.email = email =~ /\A[^@\s]+@[^@\s]+\z/ ? email : ""
  end

  def typeahead
    return [{ name: "contractor_type", to: "Contractor::Category" }]
  end

  def branding_name
    return name.titleize if name.present?
    return contact.titleize if contact.present?
    return email if email.present?
    return ""
  end
end

require_dependency 'user_contractor'
require_dependency 'user_contractor/actions'
require_dependency 'template/contractor'