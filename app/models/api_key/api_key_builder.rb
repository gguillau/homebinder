module ApiKey::ApiKeyBuilder
    
    def self.included(klass)
        klass.extend ClassMethods 
    end
    
    module ClassMethods
        
        def build(hb_request, params)
            # authorize the user to create an ApiKey
            check_permissions(hb_request, params[:partner][:partner_id])
    
            apiKey = ApiKey.find_by_partner_id(params[:partner][:partner_id])

            #check if partner api key already exists. if so we update the key
            if apiKey and (apiKey.company_name === params[:partner][:company_name])
                
                apiKey.regenerate_key
                
                partner = {
                    company_name: params[:partner][:company_name],
                    contact_email: params[:partner][:contact_email]
                }
                
                if not apiKey.update_attributes(partner)
                    raise UnprocessableException.new(apiKey) 
                end
            #else we create a new key        
            else
                apiKey = ApiKey.new(params[:partner].permit!)
                if not apiKey.save
                    raise UnprocessableException.new(apiKey)
                end
            end
            return apiKey
        end
        
        private
        
        def check_permissions(hb_request, id)
            # find partner
            Partner.find(id)
            raise CanCan::AccessDenied.new("You are not authorized to access API Keys") unless hb_request.user.role != "homeowner"
        end
    end
    
end