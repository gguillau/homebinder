module UserContractor::Actions
    def self.included(klass)
        klass.extend ClassMethods
    end

    module ClassMethods
        
        def base_query
            "CAST(#{self.table_name}.id as varchar(25)) LIKE :search OR
            LOWER(#{self.table_name}.name) LIKE :search OR LOWER(#{self.table_name}.email) LIKE :search
            OR LOWER(#{self.table_name}.notes) LIKE :search"
        end
        
        def pager(hb_request, params = {}, search_method = :search)
            #return elasticsearch(hb_request, params) if params[:search]
            
            set_defaults

            raise Exception.new unless default_order_by.present?
            raise Exception.new unless default_order.present?
            
            #hb_request.ability.authorize! :search, self.class, :message => "You are not authorized to search for #{self.name.pluralize}."
            
            # set the parameters
            item_page = params[:page].present? ? params[:page].to_i : page
            item_count = params[:count].present? ? params[:count].to_i : count
            offset = (item_page - 1) * item_count
            orderBy = params[:orderBy].present? ? params[:orderBy] : default_order_by
            order = params[:order].present? ? params[:order] : default_order
            
            # get the search method
            search_method_obj = self.method(search_method)

            # search
            total = search_method_obj.call(params, hb_request).count
            items = search_method_obj.call(params, hb_request)
                              .order("#{orderBy} #{order}")
                              .limit(item_count)
                              .offset(offset)
            
            # return the page
            return Page.new(items, total)
        end

        def build(hb_request, params)
            validate_types(params)
            validate_sub_types(params)
            super(hb_request, params)
        end

        def update(hb_request, params)
            validate_types(params)
            validate_sub_types(params)
            super(hb_request, params)
        end

        def validate_types(params)
            if params[symbol].dig(:types_attributes)
                params[symbol][:user_contractor_types] = []
                params[symbol][:types_attributes].each do |type|
                    exisiting_type = ::Contractor::Category.where("LOWER(name) = ?", type[:name].downcase).first
                    
                    if exisiting_type.nil?
                        exisiting_type = ::Contractor::Category.create(:name => type[:name])
                    end
                    
                    params[symbol][:user_contractor_types].push(UserContractorType.new({"contractor_category_id" => exisiting_type.id}))
                    
                    if params[symbol].dig(:contractor_attributes) && params[symbol].dig(:contractor_attributes, :contractor_types_attributes).nil?
                        params[symbol][:contractor_attributes][:contractor_types_attributes] = []
                    end
                    
                    if params[symbol].dig(:contractor_attributes) && params[symbol].dig(:contractor_attributes, :contractor_types_attributes)
                        params[symbol][:contractor_attributes][:contractor_types_attributes].push({"contractor_category_id" => exisiting_type.id})
                    end
                end
                
                if params[symbol].dig(:user_contractor_types_attributes)
                    params[symbol][:user_contractor_types_attributes].each do |type|
                        if type[:id]
                            existing = UserContractorType.find_by(:id => type[:id])
                            params[symbol][:user_contractor_types].push(existing)
                        else
                            existing = UserContractorType.new(:contractor_category_id => type[:contractor_category_id])
                            params[symbol][:user_contractor_types].push(existing)
                        end
                    end
                end
                
                params[symbol][:types_attributes] = params[symbol][:user_contractor_types_attributes] = []
                
                if params[symbol].dig(:contractor_attributes, :types_attributes)
                    params[symbol][:contractor_attributes][:types_attributes] = []
                end
            end
        end

        def validate_sub_types(params)
            if params[symbol].dig(:sub_types_attributes)
                params[symbol][:user_contractor_sub_types] = []
                params[symbol][:sub_types_attributes].each do |type|
                    exisiting_type = ::Contractor::SubCategory.where("LOWER(name) = ?", type[:name].downcase).first
                    
                    if exisiting_type.nil?
                        exisiting_type = ::Contractor::SubCategory.create(:name => type[:name])
                    end
                    
                    params[symbol][:user_contractor_sub_types].push(UserContractorSubType.new({"contractor_sub_category_id" => exisiting_type.id}))
                    
                    if params[symbol].dig(:contractor_attributes) && params[symbol].dig(:contractor_attributes, :contractor_types_attributes).nil?
                        params[symbol][:contractor_attributes][:contractor_types_attributes] = []
                    end
                    
                    if params[symbol].dig(:contractor_attributes) && params[symbol].dig(:contractor_attributes, :contractor_sub_types_attributes)
                        params[symbol][:contractor_attributes][:contractor_sub_types_attributes].push({"contractor_sub_category_id" => exisiting_type.id})
                    end
                end
                
                if params[symbol].dig(:user_contractor_sub_types_attributes)
                    params[symbol][:user_contractor_sub_types_attributes].each do |type|
                        if type[:id]
                            existing = UserContractorSubType.find_by(:id => type[:id])
                            params[symbol][:user_contractor_sub_types].push(existing)
                        else
                            existing = UserContractorSubType.new(:contractor_sub_category_id => type[:contractor_sub_category_id])
                            params[symbol][:user_contractor_sub_types].push(existing)
                        end
                    end
                end
                
                params[symbol][:sub_types_attributes] = params[symbol][:user_contractor_sub_types_attributes] = []
                
                if params[symbol].dig(:contractor_attributes, :sub_types_attributes)
                    params[symbol][:contractor_attributes][:sub_types_attributes] = []
                end
            end
        end

        def query_arguments
            ["user_id"]
        end

        def query_arguments_hash(query, value)
            case query
            when "user_id"
                {query.to_sym => value }
            else
                {}
            end
        end
    end
end
