# == Schema Information
#
# Table name: contractor_types
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_by :integer
#  verified   :boolean
#

class Contractor::Category < ApplicationRecord
  self.table_name = "contractor_categories"
  
  validates :name, presence: true, length: { maximum: 255 }, unique: {}
  validates_uniqueness_of :name
  
  has_many  :category_types, :class_name => "Contractor::CategoryType", :foreign_key => :contractor_type_id
  has_many  :sub_types, :through => :category_types, :source => "contractor_sub_category"
  
  def self.build(hb_request, params)
    sub_types = params[:contractor_type][:sub_types]
    params[:contractor_type] = params[:contractor_type][:contractor_type]
    contractor_type = super
    contractor_type.update_sub_types(sub_types)
    return contractor_type
  end
  
  def self.update(hb_request, params)
    sub_types = params[:contractor_type][:sub_types]
    params[:contractor_type] = params[:contractor_type][:contractor_type]
    contractor_type = super
    contractor_type.update_sub_types(sub_types)
    return contractor_type.reload
  end
  
  def self.base_query
      "CAST(contractor_categories.id as varchar(25)) LIKE :search OR
      LOWER(contractor_categories.name) LIKE :search OR
      LOWER(contractor_sub_categories.name) LIKE :search".freeze
  end
  
  def self.base_includes
      [:sub_types]
  end
  
  def self.query_arguments
    ["verified", "name", "created_by"]
  end

  def self.query_arguments_hash(query, value)
    case query
    when "verified", "name", "created_by"
        {query.to_sym => value }
    end
  end
  
  def self.pager(hb_request, params = {}, search_method = :search)
    set_defaults

    raise Exception.new unless default_order_by.present?
    raise Exception.new unless default_order.present?
    
    #hb_request.ability.authorize! :search, self.class, :message => "You are not authorized to search for #{self.name.pluralize}."
    
    # set the parameters
    item_page = params[:page].present? ? params[:page].to_i : page
    item_count = params[:count].present? ? params[:count].to_i : count
    offset = (item_page - 1) * item_count
    orderBy = params[:orderBy].present? ? params[:orderBy] : default_order_by
    order = params[:order].present? ? params[:order] : default_order
    
    # get the search method
    search_method_obj = self.method(search_method)

    # search
    total = search_method_obj.call(params, hb_request).count
    items = search_method_obj.call(params, hb_request)
                      .order("#{orderBy} #{order}")
                      .limit(item_count)
                      .offset(offset)
    
    # return the page
    return Page.new(items, total)
  end
  
  def update_sub_types(types)
    if types.nil? or types.length == 0
        sub_types.delete_all
        return
    end

    # get the current sub_types saved on the resource
    currentTypes = sub_types

    # get the types that should be removed
    toBeRemoved = currentTypes.select {|ct| not types.any? {|t| t[:name] === ct.name}}

    # get the types that need to be added
    toBeAdded = types.select {|ntype| not currentTypes.any? {|ct| ct.name === ntype[:name]}}

    # add the new types to the resource
    toBeAdded.each do |tba|
        sub_category = Contractor::SubCategory.find_by_name(tba[:name])
        
        if sub_category.present?
          Contractor::CategoryType.create!(:contractor_category => self, :contractor_sub_category => sub_category)
        else
          sub_category = Contractor::SubCategory.create!(:name => tba[:name])
          Contractor::CategoryType.create!(:contractor_category => self, :contractor_sub_category => sub_category)
        end
    end

    # delete the types to be removed
    toBeRemoved.each do |tbr|
        self.sub_types.delete(tbr)
    end
  end
  
  private
  
  def self.symbol
    :contractor_type
  end
end
