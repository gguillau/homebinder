# == Schema Information
#
# Table name: contractor_category_types
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_by :integer
#  verified   :boolean
#

class Contractor::CategoryType < ApplicationRecord
  self.table_name = "contractor_category_types"
  
  belongs_to  :contractor_category,      :class_name => "Contractor::Category",     :foreign_key => :contractor_type_id
  belongs_to  :contractor_sub_category,  :class_name => "Contractor::SubCategory",  :foreign_key => :contractor_sub_types_id
  
  # validations
  validates_uniqueness_of     :contractor_category, scope: :contractor_sub_category, :message => "already exists"
  validates_presence_of       :contractor_category, :message => I18n.t(:err_value_required), :on => :update
  validates_presence_of       :contractor_sub_category, :message => I18n.t(:err_value_required)
end
