class Contractor::SubCategory < ApplicationRecord
  self.table_name = "contractor_sub_categories"
  
  validates :name, presence: true, length: { maximum: 255 }, unique: {}
  validates_uniqueness_of :name

  before_save { |sc| sc.name = sc.name.downcase }
  
  scope :names,    -> { pluck(:name) }
  
  def self.base_query
    "LOWER(contractor_sub_categories.name) LIKE :search".freeze
  end
  
  def self.query_arguments
    ["name"]
  end

  def self.query_arguments_hash(query, value)
    case query
    when "name"
        {query.to_sym => value}
    else
        {}
    end
  end
end
