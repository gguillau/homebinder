require "elasticsearch/dsl"

module Contractor::Actions
  def self.included(klass)
    klass.extend ClassMethods 
  end
  
  module ClassMethods
      
    def base_query
        "CAST(contractors.id as varchar(25)) LIKE :search OR
                  LOWER(contractors.name) LIKE :search OR
                  LOWER(contractors.contact) LIKE :search OR
                  LOWER(contractors.email) LIKE :search OR
                  LOWER(contractors.url) LIKE :search".freeze
    end
    
    def index(hb_request, params)
        if params[:searchType] === "fuzzy"
            fuzzy_elasticsearch(hb_request, params)
        else
            super(hb_request, params)
        end
    end
    
    def search(params = {}, hb_request = nil)
        # get the search value
        searchValue = params[:search]

        # set the includes/joins arrays
        includes = []
        joins = []

        # the custom args where/where_not clauses
        where = {}
        where_not = {}

        # get the base includes/joins and base query
        includes = get_includes
        joins = get_joins
        query = get_base_query
        
        add_custom_args(where, where_not, params, includes, joins)

        # search the system
        if params[:binderId].present?
            binder = Binder.find(params[:binderId])
            owner_id = binder.owner.id
            
            self.joins(joins)
                .includes(includes)
                .where("contractors.created_by IN (?) OR contractors.created_by IN (?)", [owner_id, hb_request.user], [binder.created_by, hb_request.user])
                .where(:binder_contractors => {:binder_id => binder.id})
                .references(includes)
                .distinct
        elsif params[:searchType] === "fuzzy"
            phone = params[:phone].present? ? params[:phone] : ""
            self.joins(joins)
                .includes(includes)
                .where("contractors.phone LIKE :phone OR contractors.email LIKE :email OR contractors.name LIKE :name", {:email => "%#{params[:email]}%", :phone => "%+#{phone.lstrip}%", :name => "%#{params[:name]}%"})
                .all
                .distinct
        else
            self.joins(joins)
                .includes(includes)
                .where(where)
                .all
        end
    end
    
    def base_includes
        [:address, :binder_contractors, :types, :sub_types]
    end
    
    def query_arguments
        ["user_id", "name", "email", "contact", "phone"]
    end

    def query_arguments_hash(query, value)
      case query
      when "user_id", "name", "email", "contact"
          {query.to_sym => value }
      when "phone"
        {query.to_sym => "+" + value.gsub(/\s+/, "")}
      else
          {}
      end
    end
    
    def fuzzy_elasticsearch(hb_request, params)
        begin
            phone = params[:phone].present? ? params[:phone] : ""
            
            query = Elasticsearch::DSL::Search.search do
                query do
                    bool do
                        should do
                            match :name do
                                query params[:name]
                            end
                        end
                        
                        should do
                            match :email do
                                query params[:email]
                            end
                        end
                        
                        should do
                            match :phone do
                                query phone
                            end
                        end
                    end
                end
            end
    
            response = self.__elasticsearch__.search query, size: params[:count] || 10
            
            return Page.new(response.results.response.records.to_a, response.results.total)
        rescue => e
            raise BadRequestException.new(e.message)
        end
    end
  end
end