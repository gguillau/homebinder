module RepairPricerReport::Actions

    def self.included(klass)
        klass.extend ClassMethods
    end
    
    def set_address(address)
        return if address.blank?
        create_address(:address1 => address[:address1], :address2 => address[:address2], :city => address[:city], :state => address[:state], :zip => address[:zip], :country => address[:country])
    end
    
    def order_report
        RepairPricerReports::OrderReportJob.perform_async(id)
    end
    
    def set_inspection_report(document_id)
        if !document_id.blank?
            document = Binder::Document.find_by_id(document_id)
            if document.present?
                self.inspection_report = document.file
                self.save!
            end
        end
    end
    
    def set_charge(charge_id)
        RepairPricerReports::SetChargeJob.perform_async(id, charge_id)
    end
    
    module ClassMethods
        
        def build(hb_request, params)
            # pay for report
            charge = process_payment(params[symbol][:creator_id], params[:payment])
            
            # create report
            repair_pricer_report = super
            
            # create address
            repair_pricer_report.set_address(params[:address])
            
            if !charge.blank?
                repair_pricer_report.set_charge(charge.id)
            end
            
            # set inspection report
            repair_pricer_report.set_inspection_report(params[:document_id])
            
            # order actual report from RP
            repair_pricer_report.order_report
            
            return repair_pricer_report
        end
        
        def update(hb_request, params)
            if params[:action_type] === "refund"
                report = super
                
                raise BadRequestException.new "Payment required before refund (stripe_charge_id is blank)" if report.stripe_charge_id.blank?
                raise BadRequestException.new "Refund already processed!" if !report.stripe_refund_id.blank?
                
                
                if report.amount_charged_cents && report.amount_charged_cents > 0
                    # issue a refund
                    cc = Payments::CreditCardProcessor.new(hb_request.user)
                    refund = cc.refund(report.stripe_charge_id)
                    report.stripe_refund_id = refund.id
                    report.status = 8
                    report.payment_status = "refunded"
                    report.save!
                end
            else
                report = super
            end
            return report
        end
        
        def process_payment(user_id, payment)
            return if payment.blank?
            return if payment.dig(:total).blank?
            return if payment.dig(:card).blank?
            return if payment.dig(:total) < 1
            user = User.find_by_id(user_id)
            return if user.nil?
            
            # attempt to charge the provided card
            begin
                return Payments::CreditCardProcessor.new(user).charge_card({ amount: payment[:total] * 100, card: payment[:card] })
            rescue => e
                raise BadRequestException.new e.message
            end
        end
    end 
end