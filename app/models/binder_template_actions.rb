module BinderTemplateActions
    def self.included(klass)
        klass.extend ClassMethods
    end

    def add_default_items
        template = BinderTemplate.where(:partner => true).first
        return if template.nil?
        template.appliance_templates.each do |app|
            Template::Appliance.create!(:name => app.name, :binder_template_id => id, :library_source_id => app.id, :image => app.image, :notes => app.notes, :description => app.description)
        end

        template.maintenance_templates.each do |item|
            Template::Maintenance.create!(:name => item.name, :frequency => item.frequency, :due_date => item.due_date, :initial_due_date_interval => item.initial_due_date_interval, :binder_template_id => id, :library_source_id => item.id, :image => item.image, :notes => item.notes, :description => item.description)
        end

        template.document_templates.each do |item|
            Template::Document.create!(:file => item.file, :binder_template_id => id, :library_source_id => item.id, :notes => item.notes, :description => item.description, :document_type_id => item.document_type_id)
        end
    end

    module ClassMethods

        ID = "id".freeze
        CREATED_AT = "created_at".freeze
        UPDATED_AT = "updated_at".freeze
        COPY = " (Copy)".freeze
        STOVE = "Stove".freeze
        OVEN = "Oven/Range".freeze
        DISPOSAL = "Disposal".freeze
        WASHER = "Washer".freeze
        DRYER = "Dryer".freeze
        ANNUAL = "Annual".freeze
        FURNACE = "Furnace".freeze
        QUARTERLY = "Quarterly".freeze
        MICROWAVE = "Microwave".freeze
        DISHWASHER = "Dishwasher".freeze
        SEMI_ANNUAL = "Semi-Annual".freeze
        WATER_HEATER = "Water Heater".freeze
        EVERY_OTHER_YEAR = "Every-Other-Year".freeze
        EVERY_TEN_YEARS = "Every-10-Years".freeze
        TEMPLATE_NOT_FOUND = "Template not found".freeze
        PARTNER_NOT_FOUND = "Partner not found".freeze
        REFRIGERATOR = "Refrigerator".freeze
        SMOKE_DETECTOR = "Smoke Alarm Battery".freeze
        SWEEP_CHIMNEY = "Sweep Chimney".freeze
        PUMP_TANK = "Pump Septic Tank".freeze
        CHANGE_AC_FILTER = "Change A/C Filter".freeze
        CHANGE_WATER_FILTER = "Change Water Filter".freeze
        BLOW_OUT_LINES = "Blow Out Sprinkler Lines".freeze
        ADD_WATER_SOFTENER = "Add Water Softener".freeze
        CLEAN_GUTTERS = "Clean Gutters".freeze
        INSPECT_CRAWLSPACE = "Inspect Crawlspace".freeze
        CLEAN_DRYER_VENT = "Clean Dryer Vent".freeze
        SERVICE_FURNACE = "Service Furnace".freeze
        SCHEDULE_HOME_AUDIT = "Schedule Home Energy Audit".freeze

        def base_query
            "CAST(binder_templates.id as varchar(25)) LIKE :search OR
            LOWER(binder_templates.name) LIKE :search".freeze
        end

        def index(hb_request, params)
            if hb_request.user.role != "admin"
                raise BadRequestException.new "Partner ID Required" unless params[:partner_id].present?

                partner_id = params[:partner_id]

                partner = Partner.includes(partner_configuration: [:binder_templates]).where(:id => partner_id).first
                raise NotFoundException.new PARTNER_NOT_FOUND if partner.nil?

                hb_request.ability.authorize! :read, partner, :message => "You are not authorized to access partner ID: #{partner.id}."
            end

            super(hb_request, params)
        end

        def build(hb_request, params)
            partner_id = params[:partner_id]

            partner = Partner.includes(:partner_configuration).where(:id => partner_id).first
            raise NotFoundException.new PARTNER_NOT_FOUND if partner.nil?

            hb_request.ability.authorize! :write, partner, :message => "You are not authorized to access partner ID: #{partner.id}."

            template = ::BinderTemplate.new(params[:binder_template].permit!)
            template.partner_configuration_id = partner.partner_configuration.id
            if not template.save
                raise UnprocessableException.new template
            end

            Templates::AddDefaultItemsJob.perform_async(template.id)

            return template
        end

        def copy(hb_request, params)
            old_template = BinderTemplate.find(params[:id])
            new_template = old_template.dup
            new_template.name = new_template.name + COPY
            if not new_template.save
                raise UnprocessableException.new new_template
            end
            copy_associations(old_template, new_template)
            return new_template
        end

        def copy_associations(old_template, new_template)
            old_template.appliance_templates.each do |appT|
                hash = appT.attributes.except("id")
                hash[:binder_template_id] = new_template.id
                appliance = Template::Appliance.new(hash)
                if not appliance.save
                    raise UnprocessableException.new appliance
                end
            end

            old_template.contractor_templates.each do |conT|
                hash = conT.attributes.except("id")
                hash[:binder_template_id] = new_template.id
                contractor = Template::Contractor.new(hash)
                if not contractor.save
                    raise UnprocessableException.new contractor
                end
            end

            old_template.document_templates.each do |docT|
                hash = docT.attributes.except("id")
                hash[:binder_template_id] = new_template.id
                document = Template::Document.new(hash)
                if not document.save
                    raise UnprocessableException.new document
                end
            end

            old_template.maintenance_templates.each do |mainT|
                hash = mainT.attributes.except("id")
                hash[:binder_template_id] = new_template.id
                item = Template::Maintenance.new(hash)
                if not item.save
                    raise UnprocessableException.new item
                end
            end
        end

        def default_appliances
            return [REFRIGERATOR, DISHWASHER, MICROWAVE, STOVE, OVEN, DISPOSAL, WASHER, DRYER, WATER_HEATER, FURNACE]
        end

        def default_maintenance_items
            return [{
                       name: "Change Locks",
                       frequency: "Once",
                       initial_due_date_interval: 45
                   }, {
                       name: "Add Appliance Information To Your HomeBinder Account",
                       frequency: "Once",
                       initial_due_date_interval: 90
                   }, {
                       name: "Smoke Alarm Battery",
                       frequency: SEMI_ANNUAL,
                       initial_due_date_interval: 60
                   }, {
                       name: "Replace Water Heater",
                       frequency: "Every-5-Years",
                       initial_due_date_interval: 1825
                   },{
                       name: CLEAN_DRYER_VENT,
                       frequency: ANNUAL,
                       initial_due_date_interval: 120
                   },{
                       name: SERVICE_FURNACE,
                       frequency: EVERY_OTHER_YEAR,
                       initial_due_date_interval: 330
                   },{
                       name: SCHEDULE_HOME_AUDIT,
                       frequency: EVERY_TEN_YEARS,
                       initial_due_date_interval: 300
                   },{
                       name: "Change HVAC Filter",
                       frequency: SEMI_ANNUAL,
                       initial_due_date_interval: 180
                   }]
        end

        def set_date(month, day)
            date = Date.parse("#{day}-#{month}-#{Date.today.year}")
            return check_date(date)
        end

        def check_date(date)
            if date >= Date.today
                return date
            else
                return date + 1.year
            end
        end

        def add_custom_args(where, where_not, params, includes, joins)
            if params[:partner_id]
                joins.push(:partner_configuration)
                where.merge!({:partner_configurations => {:partner_id => params[:partner_id]}})
            end
            super
        end

        def query_arguments
            ["system", "default", "partner"]
        end

        def query_arguments_hash(query, value)
            case query
            when "system", "default", "partner"
                {query.to_sym => value}
            else
                {}
            end
        end
    end
end
