# == Schema Information
#
# Table name: organization_partners
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  partner_id      :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class OrganizationPartner < ApplicationRecord
    
    # associations
    belongs_to :organization
    belongs_to :partner
    
    # validations
    validates_presence_of   :partner,        :message => I18n.t(:err_value_required)
    validates_presence_of   :organization,   :message => I18n.t(:err_value_required)
    
    # one unique partner per organization
    validates_uniqueness_of :partner_id, scope: :organization_id, :message => "is already a member"
end
