module Partner::Automation::Error::Actions

    def self.included(klass)
        klass.extend ClassMethods
    end

    module ClassMethods
        
        def base_query
            "CAST(automation_errors.id as varchar(25)) LIKE :search OR
                      LOWER(automation_errors.client_first) LIKE :search OR
                      LOWER(automation_errors.client_email) LIKE :search OR
                      CAST(automation_errors.binder_template_id as varchar(25)) LIKE :search OR
                      LOWER(automation_errors.property_city) LIKE :search OR
                      LOWER(automation_errors.error_message) LIKE :search OR
                      LOWER(automation_errors.property_address) LIKE :search".freeze
        end
        
        # create the new error
        def build(params, error_message)
            return if error_message.start_with?("Binder with address")
            error = create_object(params, error_message)
            @error = Partner::Automation::Error.new(error)
            if @error.save
                upload_files(params)
            else
                ErrorService.perform_async(@error.errors.full_messages.first, {automation_error: error_message})
            end
            return @error
        end

        # update the error
        def update(hb_request, params)
            # find the error first
            @error = find(params[:id])
            
            # reformat the request
            request = create_request(params[:error])
            
            # resend the request with the updated params
            binder = Partner::Automation::API::Service.create_binder(request)

            # add any images or documents
            self.delay(queue: 'abs').create_files(binder[:id], @error.id)
            
            # destroy the error
            destroy(hb_request, @error.id)
        end

        private

        def create_files(binder_id, error_id)
            @binder = Binder.find(binder_id)
            
            create_aws

            documents = @bucket.objects(prefix: "automation/#{error_id}/documents/").collect(&:key)
            images = @bucket.objects(prefix: "automation/#{error_id}/images/").collect(&:key)

            download_images(images)
            download_documents(documents)
        end

        def download_documents(documents)
            # download and add new documents to array
            new_documents = documents.map { |document| {:file => download(document)}}
            # add documents to binder
            Partner::Automation::Service.create_documents(@binder.id, new_documents)
        end

        def download_images(images)
            images.each do |image|
                file = download(image)
                Partner::Automation::Service.create_image(@binder.id, {:file => file})
            end
        end

        def download(path)
            obj = @bucket.object(path)
            obj_url = obj.presigned_url(:get, expires_in: 60)

            filenamepath = File.basename(path)

            temp_dir_name = "#{Rails.root}/tmp"

            temp_dir = File.dirname("#{temp_dir_name}")

            file_name = temp_dir + "/tmp/images/" + filenamepath

            # Create Directory
            FileUtils.mkdir_p  File.dirname(file_name)

            begin
                download = open(obj_url)
            rescue => e
                ErrorService.perform_async(e.message, {:task => "download"})
            end
            IO.copy_stream(download, file_name)

            file = File.new file_name
            file.class.class_eval { attr_accessor :original_filename }
            file.original_filename = file_name

            return file
        end

        def create_aws
            @s3 = S3FileService.load_s3
            # get the bucket
            @bucket = @s3.bucket(Paperclip::Attachment.default_options[:bucket])
        end

        def upload_files(params)
            # create an instance of the s3 object
            create_aws

            # upload documents to AWS
            if params[:documents].present? and params[:documents].length > 0
                upload_documents(params[:documents])
            end

            # upload image to AWS
            if params[:property_photo]
                upload_image(params[:property_photo])
            end
        end

        def upload_documents(documents)
            documents.each_with_index do |doc, index|
                begin
                    if doc.class.name === "StringIO"
                        name = doc.original_filename
                        file = doc
                    else
                        name = doc[:name]
                        file = doc[:file]                    
                    end
                rescue => e
                    name = "doc#{index}.pdf"
                    file = doc
                end

                begin
                    @bucket.put_object({:key => "automation/#{@error.id}/documents/#{name}", :body => file})
                rescue => e
                    ErrorService.perform_async(e.message, {:task => "upload_documents"})
                end
            end
        end

        def upload_image(image)
            if image[:file].present?
                # get the image
                property_photo = image[:file]
                # add it to the bucket
                name = property_photo.respond_to?(:original_filename) ? property_photo.original_filename : image[:name]
                begin
                    @bucket.put_object({:key => "automation/#{@error.id}/images/#{name}", :body => property_photo})
                rescue => e
                    ErrorService.perform_async(e.message, {:task => "upload_image"})
                end
            end
        end

        def create_object(params, error_message)
            return {
                       :api_key => params[:api_key],
                       :error_message => error_message,
                       :partner_name => params[:key].present? ? params[:key]: nil,
                       :binder_template_id => params[:binder_template_id].present? ? params[:binder_template_id]: nil,
                       :client_first => params[:client].present? ? params[:client][:first] : nil,
                       :client_last => params[:client].present? ? params[:client][:last] : nil,
                       :client_email => params[:client].present? ? params[:client][:email] : nil,
                       :property_address => params[:property].present? ? params[:property][:address] : nil,
                       :property_address2 => params[:property].present? ? params[:property][:address2] : nil,
                       :property_city => params[:property].present? ? params[:property][:city] : nil,
                       :property_state => params[:property].present? ? params[:property][:state] : nil,
                       :property_postalcode => params[:property].present? ? params[:property][:postalcode] : nil,
                       :property_country => params[:property].present? ? params[:property][:country] : nil,
                       :agent_name => params[:agent].present? ? params[:agent][:name] : nil,
                       :agent_contact => params[:agent].present? ? params[:agent][:contact] : nil,
                       :agent_phone => params[:agent].present? ? params[:agent][:phone] : nil,
                       :agent_email => params[:agent].present? ? params[:agent][:email] : nil
                   }
        end

        def create_request(error)
            return {
                       :key => error[:partner_name],
                       :partner_name => error[:key],
                       :binder_template_id => error[:binder_template_id],
                       :client => {
                           :first => error[:client_first],
                           :last => error[:client_last],
                           :email => error[:client_email]
                       },
                       :property => {
                           :address => error[:property_address],
                           :address2 => error[:property_address2],
                           :city => error[:property_city],
                           :state => error[:property_state],
                           :country => error[:property_country],
                           :postalcode => error[:property_postalcode]
                       },
                       :agent => set_agent(error)
                   }
        end
        
        def set_agent(error)
            if error[:agent_email].present?
                return {
                    :name => error[:agent_name],
                    :contact => error[:agent_contact],
                    :phone => error[:agent_phone],
                    :email => error[:agent_email]
                }
            elsif error[:buyer_agent].present? and error[:buyer_agent][:email].present?
                return {
                    :name => error[:buyer_agent][:name],
                    :contact => error[:buyer_agent][:first] + " " + error[:buyer_agent][:last],
                    :phone => error[:buyer_agent][:phone],
                    :email => error[:buyer_agent][:email]
                }
            end
        end
    end
end