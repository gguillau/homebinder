# == Schema Information
#
# Table name: automation_errors
#
#  id                  :integer          not null, primary key
#  partner_name        :string(255)
#  binder_template_id  :string(255)
#  client_first        :string(255)
#  client_last         :string(255)
#  client_email        :string(255)
#  property_address    :string(255)
#  property_address2   :string(255)
#  property_city       :string(255)
#  property_state      :string(255)
#  property_postalcode :string(255)
#  property_country    :string(255)
#  agent_name          :string(255)
#  agent_contact       :string(255)
#  agent_phone         :string(255)
#  agent_email         :string(255)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  error_message       :string(255)
#  api_key             :string(255)
#

class Partner::Automation::Error < ApplicationRecord
    include Actions
    
    self.table_name = "automation_errors"
    
    
    validates_length_of       :partner_name, :maximum => 255
    validates_length_of       :binder_template_id, :maximum => 255
    validates_length_of       :client_first, :maximum => 255
    validates_length_of       :client_last, :maximum => 255
    validates_length_of       :client_email, :maximum => 255
    validates_length_of       :property_address, :maximum => 255
    validates_length_of       :property_address2, :maximum => 255
    validates_length_of       :property_city, :maximum => 255
    validates_length_of       :property_state, :maximum => 255
    validates_length_of       :property_postalcode, :maximum => 255
    validates_length_of       :property_country, :maximum => 255
    validates_length_of       :agent_name, :maximum => 255
    validates_length_of       :agent_contact, :maximum => 255
    validates_length_of       :agent_phone, :maximum => 255
    validates_length_of       :agent_email, :maximum => 255
    validates_length_of       :error_message, :maximum => 255
    validates_length_of       :api_key, :maximum => 255
    
    # one unique widget per dashboard
    validates_uniqueness_of :partner_name, scope: [:binder_template_id, :client_first, :client_last, :property_address, :property_city, :property_state, :error_message], :message => "already exists"
end
