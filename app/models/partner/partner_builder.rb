module Partner::PartnerBuilder

    def self.included(klass)
        klass.extend ClassMethods
    end

    def RemoveObjectJob
        RemovalJob.perform_async(self.class.name, id)
    end

    module ClassMethods

        QUERY = "LOWER(partner_key) = :partner_key".freeze

        def base_query
            Partner::QUERY_BY_SEARCH
        end

        def base_includes
            [:partner_configuration]
        end
        
        def query_arguments
            ["repair_pricer_pre_paid_reports", "repair_pricer_enabled", "active_warranty_account"]
        end

        def query_arguments_hash(query, value)
            case query
            when "repair_pricer_pre_paid_reports", "repair_pricer_enabled", "active_warranty_account"
                {:partner_configurations => {query.to_sym => value}}
            else
                {}
            end
        end

        def build(hb_request, params)
            # create the user object
            user = params[:user]
            user = User.register({ user: user })

            # create the partner
            partner = super(hb_request, params)

            # add the user to the partner
            PartnerUser.create!(partner_id: partner.id, user_id: user.id, role: UserPartnerRoles::ADMIN)

            # add additional objects
            PartnerService.new(user, partner).after_create
                
            return {token: UserTokenService.create_jwt(user, params)}
        end

        def update(hb_request, params)
            partner = super

            # update callbacks
            PartnerService.new(hb_request.user, partner).after_update

            return partner
        end

        def show_apr_config(key)
            partner = Partner.includes(:partner_configuration).where(QUERY, partner_key: key).first
            raise NotFoundException unless partner
            return partner
        end

        def get_automation_fields(key)
            raise BadRequestException.new "Partner Key Required" if key.nil?
            partner = Partner.includes(:partner_configuration).where(:partner_key => key).first
            raise BadRequestException.new "Partner not found" unless partner.present?
            raise BadRequestException.new "API Key not found" unless partner.api_key.present?

            logo = partner.logo.exists? ? partner.logo.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval]) : nil

            return {
                       logo: logo,
                       api_key: partner.api_key.key,
                       default_binder_template_id: partner.partner_configuration.default_binder_template_id,
                       name: partner.name,
                       contact: partner.contact,
                       address: partner.address
                   }
        end

        def get_logo_url(code)
            partner = Partner.where("LOWER(code) = :code", code: code.downcase).first
            raise NotFoundException unless partner
            return nil unless partner.logo.present?
            return partner.logo.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :original)
        end

        def for_organization(params = {}, hb_request)
            raise Exception.new unless params[:organizationId].present?

            # get the org
            org = Organization.find(params[:organizationId])
            hb_request.ability.authorize! :read, org, :message => "You are not authorized to access organization ID: #{org.id}."

            # set the includes/joins arrays
            includes = []
            joins = []

            # the custom args where/where_not clauses
            where = {}
            where_not = {}

            # get the base includes and base query
            includes = get_includes
            query = get_base_query

            add_custom_args(where, where_not, params, includes, joins)

            # search
            if searchValue.present?
                org.partners.includes(includes).where(query, search: "%#{searchValue}%".downcase).references(includes)
            else
                org.partners.includes(includes)
            end
        end

        def add_custom_args(where, where_not, params, includes, joins)
            configuration_hash = {}

            if params[:type].present?
                where.merge!({:partner_type => params[:type]})
            end

            if params[:default_action].present?
                configuration_hash.merge!({:default_binder_action => params[:default_action]})
                where.merge!({:partner_configurations => configuration_hash})
            end

            if params[:auto_action].present?
                configuration_hash.merge!({:automation_binder_action => params[:auto_action]})
                where.merge!({:partner_configurations => configuration_hash})
            end

            if params[:notify_agent].present?
                configuration_hash.merge!({:send_agents_transfer_notification => params[:notify_agent]})
                where.merge!({:partner_configurations => configuration_hash})
            end

            if params[:branding_option].present?
                configuration_hash.merge!({:default_user_branding_option => params[:branding_option]})
                where.merge!({:partner_configurations => configuration_hash})
            end

            if params[:warranty_option].present?
                configuration_hash.merge!({:active_warranty_account => params[:warranty_option]})
                where.merge!({:partner_configurations => configuration_hash})
            end

            if params[:apr_option].present?
                configuration_hash.merge!({:can_perform_apr => params[:apr_option]})
                where.merge!({:partner_configurations => configuration_hash})
            end
            
            if params[:repair_pricer_pre_paid_reports].present?
                configuration_hash.merge!({:repair_pricer_pre_paid_reports => params[:repair_pricer_pre_paid_reports]})
                where.merge!({:partner_configurations => configuration_hash})
            end
            
            if params[:active_warranty_account].present?
                configuration_hash.merge!({:active_warranty_account => params[:active_warranty_account]})
                where.merge!({:partner_configurations => configuration_hash})
            end
        end
    end

end
