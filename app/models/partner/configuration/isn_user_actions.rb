module Partner::Configuration::IsnUserActions
    def self.included(klass)
        klass.extend ClassMethods
    end

    module ClassMethods

        def index(hb_request, params)
            if hb_request.user.role != "admin"
                # get the partner
                partner = Partner.joins(:partner_configuration).where(:partner_configurations => {:id => params[:partner_configuration_id]}).first
                raise NotFoundException if partner.nil?

                # verify the user can read the partner
                hb_request.ability.authorize! :read, partner, :message => "You are not authorized to access partner ID: #{partner.id}."
            end

            super(hb_request, params)
        end

        def build(hb_request, params)
            partner = Partner.joins(:partner_configuration).where(:partner_configurations => {:id => params[:isn_user][:partner_configuration_id]}).first
            raise NotFoundException if partner.nil?

            hb_request.ability.authorize! :write, partner, :message => "You are not authorized to access partner ID: #{partner.id}."

            iu = super(hb_request, params)

            Isn::RakeJob.new.get_footprints(nil, iu)

            return iu
        end

        def query_arguments
            ["partner_configuration_id"]
        end

        def query_arguments_hash(query, value)
            case query
            when "partner_configuration_id"
                {:partner_configuration_id => value }
            else
                {}
            end
        end
    end

end
