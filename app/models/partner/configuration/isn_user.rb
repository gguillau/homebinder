# == Schema Information
#
# Table name: isn_users
#
#  id                       :integer          not null, primary key
#  partner_configuration_id :integer
#  company_key              :string(50)
#  api_endpoint             :string(500)
#  username                 :string(50)
#  encrypted_password       :string(200)
#  endpoint_refreshed_at    :datetime
#  datetime                 :datetime
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  name                     :string(255)
#

require 'attr_encrypted'

class Partner::Configuration::IsnUser < ApplicationRecord
  include Partner::Configuration::IsnUserActions
  
  self.table_name = "isn_users"
  
  belongs_to :partner_configuration, class_name: "Partner::Configuration",
                                     foreign_key: "partner_configuration_id"
  
  attr_encrypted :password, :key => ENV['KEY'], :insecure_mode => true
  
  validates :name,          :length => { :maximum => 100 }, :presence => true
  validates :company_key,   :length => { :maximum => 50 },  :presence => true
  validates :api_endpoint,  :length => { :maximum => 200 }
  validates :username,      :presence => true
  validates :password,      :presence => true
end
