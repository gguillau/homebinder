module Partner::ConfigurationActions
  
  NAME = "Binder Defaults".freeze
  
  def self.included(klass)
    klass.extend ClassMethods 
  end
  
  def generate_default_template
    template = self.binder_templates.new(name: NAME)
    template.transfer_note = "Thank you for trusting me with your business. I wish you the best of luck in your home ownership and would welcome the opportunity to work with any friends or family members that you know who might be in need of my services. I'm never too busy for your referrals!"
    template.save!
    
    Templates::AddDefaultItemsJob.perform_async(template.id)
    
    return template
  end
  
  module ClassMethods
    # updates the configuration of a partner
    def update(hb_request, params)
      config = super
      
      # analytics
      Partners::UpdatedJob.perform_async(config.partner.id)
      
      return config
    end
  end
end