# == Schema Information
#
# Table name: partner_configurations
#
#  id                                   :integer          not null, primary key
#  partner_id                           :integer
#  maintenance_note                     :string(1000)
#  default_binder_template_id           :integer
#  default_binder_action                :string(250)      default("transfer")
#  automation_binder_action             :boolean          default(FALSE)
#  binder_action_delay_length           :integer          default(5)
#  send_agents_transfer_notification    :boolean          integer
#  show_isn_tab                         :boolean          default(FALSE)
#  default_user_branding_id             :integer
#  default_user_branding_option         :string           default("co_brand")
#  allow_client_calls                   :boolean          default(TRUE)
#  active_warranty_account              :boolean          default(FALSE)
#  can_perform_apr                      :boolean          default(FALSE)
#  annual_property_review_cost_cents    :integer          default(19900), not null
#  annual_property_review_cost_currency :string           default("USD"), not null
#

class Partner::Configuration < ApplicationRecord
  include ConfigurationActions
  
  self.table_name = "partner_configurations"

  has_many :binder_templates, foreign_key: "partner_configuration_id"
  has_many :isn_users,        class_name: "Partner::Configuration::IsnUser",
                              foreign_key: "partner_configuration_id" 
  
  belongs_to :default_binder_template, class_name: "BinderTemplate", foreign_key: "default_binder_template_id"
  belongs_to :default_branded_profile, class_name: "UserProfile", foreign_key: "default_user_branding_id"
  belongs_to :partner
  
  enum send_agents_transfer_notification: {
    always: 0,
    once: 1,
    never: 2,
  }
  
  validates :maintenance_note, length: { maximum: 10000 }
  
  validates :default_binder_action, inclusion: ["transfer", "share"]
  
  validates :automation_binder_action, inclusion: [true, false]
  
  validates :default_binder_template, presence: {:message => I18n.t(:err_value_required)}, :on => :update, if: :old_account?
  
  validates :default_user_branding_id, presence: {:message => I18n.t(:err_value_required)}, :on => :update, if: :old_account?
  
  validates_numericality_of :binder_action_delay_length, :greater_than_or_equal_to => 0, :allow_blank => false
  
  before_destroy :prevent_deletion, prepend: true
  
  def old_account?
    partner.created_at < Date.today
  end
  
  # we need to make sure that a partner always has a configuration object
  def prevent_deletion
    errors.add(:id, "Cannot destroy configuration object")
    throw :abort
  end
end
