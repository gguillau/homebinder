# == Schema Information
#
# Table name: user_binders
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  binder_id    :integer
#  role         :string(100)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  dashboard_id :integer
#

class UserBinder < ApplicationRecord
    include UserBinder::Actions

    # associations
    belongs_to :user
    belongs_to :binder

    # validations
    validates_presence_of :user, :message => I18n.t(:err_value_required)
    validates_presence_of :binder, :message => I18n.t(:err_value_required)
    validates_presence_of :role, :message => I18n.t(:err_value_required)

    # one unique user per binder and role combined
    validates_uniqueness_of :user_id, scope: [:binder_id, :role], :message => "already has role on binder"
    
    # one unique user per binder and role combined
    validates_uniqueness_of :user_id, scope: [:binder_id], :message => "already has role on binder", :unless => :user_is_an_agent?
    
    validate :user_role,    :if => :user_id?
    validate :unique_users
    validate :orphan_check, :on => :update
    
    has_secure_token :repair_pricer_token
    
    after_create -> { add_business_card_widget }
    
    before_destroy  :orphan_check, prepend: true
    after_destroy   :remove_business_card_widget
    
    def self.query_arguments
        ["user_id", "binder_id", "role"]
    end

    def self.query_arguments_hash(query, value)
        case query
        when "user_id", "binder_id", "role"
          {query.to_sym => value }
        end
    end

    def user_role
        if !user.is_agent? and role === "buyer_agent" || role === "seller_agent"
            errors.add(:role, "User: #{user.email} is a #{user.role} and cannot be added to binder: #{binder_id} as a buyer agent")
        end
    end
    
    def unique_users
        case role
        when "owner"
            if self.class.where.not(:id => id).where(:role => "owner", :binder_id => binder_id).count > 0
                errors.add(:role, "#{role} already exists for binder: #{binder_id}")
            end
        when "buyer_agent"
            if self.class.where.not(:id => id).where(:role => "buyer_agent", :binder_id => binder_id).count > 0
                errors.add(:role, "#{role} already exists for binder: #{binder_id}")
            end
        when "seller_agent"
            if self.class.where.not(:id => id).where(:role => "seller_agent", :binder_id => binder_id).count > 0
                errors.add(:role, "#{role} already exists for binder: #{binder_id}")
            end
        end
    end
    
    def orphan_check
        if binder.status === "orphan"
            errors.add(:role, "Cannot change user roles for an orphaned binder")
            throw :abort
        end
    end
    
    def user_is_an_agent?
        return false if user.nil?
        return true if user.role === "agent"
    end
    
    # injects double business card widget after agent is added
    def add_business_card_widget
        if self.role == "buyer_agent"
            doubleBusinessCardWidget = Widget.where(:key => "doubleBusinessCard").first
            return if doubleBusinessCardWidget.nil?
            
            user_binder = UserBinder.where(:role => "owner", :binder_id => self.binder_id).first
            return if user_binder.nil?
            
            dashboard_id = user_binder.dashboard_id
            return if dashboard_id.nil?
            
            if !DashboardWidget.where(:dashboard_id => dashboard_id, :widget_id => doubleBusinessCardWidget.id).exists?
                DashboardWidget.create!(:dashboard_id => dashboard_id, :widget_id => doubleBusinessCardWidget.id, :index => 1)
            end
        end
    end

    # removes double business card widget after agent is removed and partner branding
    def remove_business_card_widget
        if !Binder::BinderBranding.where(:binder_id => self.binder_id, :scope => "binder").exists? && self.role == "buyer_agent"
            doubleBusinessCardWidget = Widget.where(:key => "doubleBusinessCard").first
            return if doubleBusinessCardWidget.nil?
            
            user_binder = UserBinder.where(:role => "owner", :binder_id => self.binder_id).first
            return if user_binder.nil?
            
            dashboard_id = user_binder.dashboard_id
            return if dashboard_id.nil?
            
            DashboardWidget.where(:dashboard_id => dashboard_id, :widget_id => doubleBusinessCardWidget.id).destroy_all
        end
    end
    
end
