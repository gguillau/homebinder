# == Schema Information
#
# Table name: recalls
#
#  id          :integer          not null, primary key
#  number      :string(255)
#  recall_date :date
#  models      :text
#  verified    :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  ignore      :boolean
#  details     :text
#

class Recall < ApplicationRecord
    # attr_accessible :title, :body
    has_many :appliance_recalls, :class_name => "Binder::Appliance::ApplianceRecall",
                               :dependent => :destroy
    has_many :appliances, through: :appliance_recalls,
                        :class_name => "Binder::Appliance"
  
    def recall_url
        details = JSON.parse(self.details)
        return details["URL"]
    end
  
    def self.base_query
        "CAST(recalls.id as varchar(25)) LIKE :search OR
        LOWER(recalls.number) LIKE :search OR
        LOWER(recalls.details) LIKE :search".freeze
    end
  
    def self.query_arguments
        ["verified", "applianceId"]
    end

    def self.query_arguments_hash(query, value)
        case query
        when "verified"
            {:verified => value}
        when "applianceId"
            {:appliance_id => value}
        else
            {}
        end
    end
end
