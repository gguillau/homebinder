class UserMerge < ApplicationRecord
    # relationships
    belongs_to :user

    validates_presence_of :user,                :message => I18n.t(:err_value_required)
    validates_presence_of :old_agent_email,     :message => I18n.t(:err_value_required)

    has_attached_file :footprint,           :path => "/user_merge:id/footprint/:filename"
    do_not_validate_attachment_file_type    :footprint

    enum status: {
        merged: 0,
        reverting: 1,
        reverted: 2,
    }, _prefix: :status
end
