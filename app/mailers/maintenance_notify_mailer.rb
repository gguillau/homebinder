class MaintenanceNotifyMailer < MandrillMailer

  default from: "HomeBinder.com <support@homebinder.com>"
  TRANSFEREMAIL = "transfers@homebinder.com".freeze

  def notify_email(event_id, binder_id, user_id, do_date)
    event = Binder::MaintenanceItem::MaintenanceEvent.find_by_id(event_id)
    binder = Binder.find_by_id(binder_id)
    user = User.find_by_id(user_id)
    return if user.nil?
    return if event.nil?
    return if binder.nil?
    
    # set variables
    @event        = event
    @maintItem    = event.maintenance_item
    @binder       = binder
    @user         = user
    @to_no_exist  = user.sign_in_count == 0
    @do_date      = do_date
    @subject      =  "#{@maintItem.name.titleize} Maintenance Due (#{binder.full_address})"
    @template     = "Maintenance Reminder - w/ Home Pro"
    @from_name    = "HomeBinder Support"
    @from_email   = "support@homebinder.com"

    @item = {
      id: @maintItem.id,
      name: @maintItem.name,
      do_date: do_date,
      notes: @maintItem.details
    }

    @to = [
        {:email => @user.email, :name => nil, :type => "to"}
    ]

    generate_decline_link

    generate_binder_branding

    @global_merge_vars = [
        {"content" => @user.user_profile.display_name, "name"=>"user"},
        {"content" => @binder.full_address, "name" => "full_address" },
        {"content" => OpenStruct.new(@item).to_h, "name" => "item" },
        {"content" => Host.generate_host_path, "name" => "url" },
        {"content" => @maintItem.filter_easy_eligible?, "name" => "show_filter_ad" },
        {"content" => "#{Host.generate_host_path}/binders/#{@binder.id}/maintenanceItems/#{@maintItem.id}", "name" => "item_link"},
        {"content" => "#{Host.generate_host_path}/binders/#{@binder.id}/binder_contractors?proClicked=true&maintenanceEventId=#{event.id}", "name" => "homepro_link"}
    ]
    @merge_language = "handlebars"

    if @brandings.length > 0
      @global_merge_vars.push({"content" => OpenStruct.new(@brandings[0]).to_h, "name" => "branding_user_1"})
    end

    if @brandings.length > 1
      @global_merge_vars.push({"content" => OpenStruct.new(@brandings[1]).to_h, "name" => "branding_user_2"})
    end

    # send the email
    return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "maintenance_reminder", :maintenance_event_id => event.id}, @attachments, @global_merge_vars, nil, @merge_language)
  end

  private

  def generate_decline_link
    @decline_link = "#{Host.generate_host_path}/homeowners/#{@user.id}/decline"
  end

  def generate_binder_branding
    # get the brandings
    @brandings = @binder.generate_branding("maintenance_email")
    @show_message = true
  end
end