class RecallMailer < MandrillMailer

  default from: "HomeBinder.com <support@homebinder.com>"
  TRANSFEREMAIL = "transfers@homebinder.com".freeze

  def notify_of_recalls(appliance_id, user_id, recall_url, category)
    appliance = Binder::Appliance.find_by_id(appliance_id)
    return if appliance.nil?
    
    binder = Binder.find_by_id(appliance.binder_id)
    user = User.find_by_id(user_id)
    
    return if user.nil?
    return if binder.nil?
    
    @user       = user
    @binder     = binder
    @appliance  = appliance
    @subject     = "Recall identified on appliance at #{binder.full_address}"
    @template     = category === "confirmed_recall" ? "Recall Email - Active Recall" : "Recall Email - Potential Recall"
    @from_name    = "HomeBinder Support"
    @from_email   = "support@homebinder.com"

    @to = [
        {:email => @user.email, :name => nil, :type => "to"},
        {:email => "transfers@homebinder.com", :name => nil, :type => "bcc"}
    ]

    generate_binder_branding

    @global_merge_vars = [
        {"content" => @user.user_profile.display_name, "name"=>"user"},
        {"content" => @appliance.attributes, "name" => "item" },
        {"content" => recall_url, "name" => "recall_url" },
        {"content" => "#{Host.generate_host_path}/binders/#{@binder.id}/appliances/#{@appliance.id}", "name" => "item_link"}
    ]
    @merge_language = "handlebars"

    if @brandings.length > 0
      @global_merge_vars.push({"content" => OpenStruct.new(@brandings[0]).to_h, "name" => "branding_user_1"})
    end

    if @brandings.length > 1
      @global_merge_vars.push({"content" => OpenStruct.new(@brandings[1]).to_h, "name" => "branding_user_2"})
    end

    # send the email
    return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "recall_email"}, @attachments, @global_merge_vars, nil, @merge_language)
  end

  def generate_binder_branding
    # get the brandings
    @brandings = @binder.generate_branding("recall_email")
  end
end
