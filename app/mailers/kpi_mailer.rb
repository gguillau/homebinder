class KpiMailer < MandrillMailer
  
  FROM = "HomeBinder.com <support@homebinder.com>".freeze
  DATES = "%s - %s".freeze
  DATE_FORMAT = "%A %B %d, %Y".freeze
  METRICS_SUBJECT = "HomeBinder Analytics Update For %s".freeze
  ACCOUNT_SUMMARY_SUBJECT = "Partner Accounts Summary".freeze
  KPI_SUMMARY_SUBJECT = "KPI Summary".freeze
  ACCOUNT_FILE_NAME = "account_summaries.csv".freeze
  KPI_FILE_NAME = "kpi_summaries.pdf".freeze
  EMPTY = "".freeze
  
  default from: FROM

  def send_weekly_metrics(partner_id, metrics)
    partner = Partner.find_by_id(partner_id)
    return if partner.nil?
    
    partner_name = partner.name
    @partner = partner.get_admin
    return if @partner.nil?
    
    @metrics = metrics
        
    # set variables
    @subject = METRICS_SUBJECT % partner_name
    @template = "New Expanded Partner Metrics Biweekly Email"
    @from_name = "HomeBinder Support"
    @from_email = "support@homebinder.com"
    
    @to = [
        {:email => @partner.email, :name => nil, :type => "to"}
    ]
  
    @global_merge_vars = [
        {"content" => partner_name, "name"=>"partner_name"},
        {"content" => @metrics[:homeowner_acceptance_rate], "name" => "acceptance_current"},
        {"content" => @metrics[:homeowner_acceptance_rate_difference], "name" => "acceptance_change"},
        {"content" => @metrics[:thank_yous_past_90_days], "name" => "thanks_90days"},
        {"content" => @metrics[:reminders_sent], "name" => "reminders_alltime"},
        {"content" => @metrics[:total_binders_past_30_days], "name" => "binders_30days"},
        {"content" => @metrics[:total_binders], "name" => "binders_alltime"},
        {"content" => "#{Host.generate_host_path}/partners/#{partner.id}/analytics", "name"=> "analytics_link"}
    ]
    @merge_language = "handlebars"

    # send the email
    return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "weekly_partner_metrics"}, @attachments, @global_merge_vars, nil, @merge_language)
  end
  
  def send_monthly_agent_metrics(agent_id)
    agent = User.find_by_id(agent_id)
    return if agent.nil?

    @metrics = Kpi::Agents.new.get_monthly_metrics(agent.id)
    referring_partners = format_referring_partners(@metrics[:referring_partners])
        
    # set variables
    @subject = "Monthly Metrics Update for HomeBinder Agents"
    @template = "Monthly Agent Metrics Email V1.0"
    @from_name = "HomeBinder Support"
    @from_email = "support@homebinder.com"
    
    @to = [
        {:email => agent.email, :name => nil, :type => "to"}
    ]
  
    @global_merge_vars = [
        {"content" => agent.user_profile.display_name, "name"=>"agent_name"},
        {"content" => referring_partners, "name" => "referring_partners"},
        {"content" => @metrics[:total_binders], "name" => "total_binders"},
        {"content" => @metrics[:maintenance_emails_sent], "name" => "reminders_sent"},
        {"content" => @metrics[:annual_maintenance_emails], "name" => "reminders_annual"},
        {"content" => "#{Host.generate_host_path}/agents/#{agent.id}/binders", "name"=> "dashboard_link"}
    ]
    @merge_language = "handlebars"

    # send the email
    return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "monthly_agent_metrics"}, @attachments, @global_merge_vars, nil, @merge_language)
  end
  
  def send_account_summary(user_id, file)
    user = User.find_by_id(user_id)
    return if user.nil?
    
    # add the file
    @attachments = [:type => "csv", :content => file, :name => "account_summaries.csv"]
    
    # set variables
    @subject = ACCOUNT_SUMMARY_SUBJECT
    @template = nil
    @from_name = "HomeBinder Support"
    @from_email = "support@homebinder.com"
    @to = [{:email => user.email, :name => nil, :type => "to"}]
    
    # send the email
    return send_mandrill_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "account_summaries"}, @attachments)
  end
  
  def send_kpi_metrics(user_id)
    user = User.find_by_id(user_id)
    return if user.nil?
    
    file = Kpi::Report.new(user.id).get_report
    
    # add the file
    @attachments = [:type => "pdf", :content => Base64.encode64(file), :name => "kpi_summaries.pdf"]
    
    # set variables
    @subject = KPI_SUMMARY_SUBJECT
    @template = nil
    @from_name = "HomeBinder Support"
    @from_email = "support@homebinder.com"
    @to = [{:email => user.email, :name => nil, :type => "to"}]
    
    # send the email
    return send_mandrill_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "kpi_summaries"}, @attachments)
  end

  private
  
  def format_referring_partners(partners)
    array = []
    partners.each do |partner|
      array.push(partner.contact)
    end
    return array
  end
end
