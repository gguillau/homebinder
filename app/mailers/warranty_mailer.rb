class WarrantyMailer < MandrillMailer

    def send_warranty_report(user_id, csv_file)
        user = User.find_by_id(user_id)
        return if user.nil?
        
        # add the file
        @attachments = [:type => "csv", :content => csv_file, :name => "warranty_report.csv"]
        
        # set variables
        @subject = "45 Day Warranty Report"
        @template = nil
        @email = user.email
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]
        
        # send the email
        return send_mandrill_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "warranty_report"}, @attachments)
    end
    
    def send_warranty_request(partner_id)
        partner = Partner.find_by_id(partner_id)
        return if partner.nil?
        
        @partner = partner
        @attachments = []
        
        # set variables
        @subject = "Partner Warranty Account Request"
        @template = "Partner Warranty Request"
        @email = "support@homebinder.com"
        @from_name = @partner.name
        @from_email = @partner.email
        @to = [{:email => @email, :name => nil, :type => "to"}]
        
        @global_merge_vars = [
            {"content" => @partner.attributes, "name"=>"partner"}
        ]
        @merge_language = "handlebars"
        
        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "warranty_request"}, @attachments, @global_merge_vars, nil, @merge_language)
    end
end
