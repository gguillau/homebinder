class FreeTrialRequestMailer < MandrillMailer
  
  FROM = "HomeBinder.com <support@homebinder.com>".freeze
  SUBJECT = "Mobile Free Trial Request".freeze
  
  default from: FROM
  
  def notify_free_trial_request_mail(name, email, phone)
    # set variables
    @user = OpenStruct.new({name: name, email: email, phone: phone})
    @subject = SUBJECT
    @template = "Free Trial Mailer"
    @from_name = "HomeBinder Support"
    @from_email = "support@homebinder.com"
    
    @to = [
        {:email => @from_email, :name => nil, :type => "to"}
    ]

    @global_merge_vars = [
        {"content" => @user.name, "name" => "name"},
        {"content" => @user.email, "name" => "email"},
        {"content" => @user.phone.phony_formatted(:format => :international, :spaces => ' '), "name" => "phone"}
    ]
    @merge_language = "handlebars"
    
    # send the email
    return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "notify_free_trial_request_mail"}, @attachments, @global_merge_vars, nil, @merge_language)
  end
  
end
