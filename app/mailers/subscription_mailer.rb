class SubscriptionMailer < MandrillMailer

  FROM = "HomeBinder.com <support@homebinder.com>".freeze
  FAILED = "Homebinder.com Payment Failed Notification".freeze
  PAYMENT = "Homebinder.com Payment Notification".freeze
  NOTIFICATION = "Homebinder.com Subscription Notification".freeze
  CANCELLATION = "Homebinder.com Subscription Canceled Notification".freeze
  
  def notify_customer_subscription(customer)
    # set variables
    @subject = "New Customer Subscription"
    @template = "New Customer Subscription"
    @email =  "support@homebinder.com"
    @from_name = "HomeBinder Support"
    @from_email = "support@homebinder.com"

    @to = [{:email => @email, :name => nil, :type => "to"}]

    @global_merge_vars = [
        {"content"=> "#{customer[:name]}", "name"=>"customer_name"},
        {"content"=> "#{customer[:id]}", "name"=>"customer_id"},
        {"content"=> "#{customer[:email]}", "name"=>"customer_email"},
        {"content"=> "#{customer.dig(:phone, :national)}", "name"=>"customer_phone"}
    ]
    
    # send the email
    return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "notify_customer_subscription"}, @attachments, @global_merge_vars)
  end
  
  def notify_homeowner_subscription(subscription_id)
    subscription = Subscription.find_by_id(subscription_id)
    return if subscription.nil?
    return if subscription.binder.nil?
    return if subscription.binder.owner.nil?
    return unless subscription.binder.owner.is_homeowner?
    
    customer = subscription.binder.owner
    binder = subscription.binder

    # set variables
    @subject = "Thank you for upgrading to Homeowner Edition!"
    @template = "New Homeowner Subscription"
    @email =  customer.email
    @from_name = "HomeBinder Support"
    @from_email = "support@homebinder.com"
    @to = [{:email => @email, :name => nil, :type => "to"}]
    @global_merge_vars = [
        {"content"=> customer.attributes, "name"=>"customer"},
        {"content"=> binder.attributes, "name"=>"binder"},
        {"content"=> subscription.attributes, "name"=>"subscription"}
    ]
    
    @merge_language = "handlebars"
    
    # send the email
    return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "notify_homeowner_subscription"}, @attachments, @global_merge_vars, nil, @merge_language)
  end
end