class AccountMailer < MandrillMailer

    ACCOUNT = "Homebinder.com Account Creation Notification".freeze
    BROKER = "broker".freeze
    FROM = "HomeBinder.com <support@homebinder.com>".freeze
    INSPECTOR = "inspector".freeze
    REGISTRATION = "New Partner Registration: Partner %s".freeze
    SUPPORT = "support@homebinder.com".freeze
    THANKYOU = "Thank you for signing up for HomeBinder!".freeze
    BOUNCE_EMAILS_SUBJECT = "Weekly Bounce Emails Notification".freeze

    default from: FROM

    def auto_generated_support_email(partner_id, data)
        partner = Partner.find_by_id(partner_id)
        return if partner.nil?
        
        @partner = partner
        @data = data

        @logo_location = @partner.logo.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :original) if @partner.logo.present?

        # set variables
        @subject = REGISTRATION % @partner.id
        @template = "New Partner Registration"
        @email = "support@homebinder.com"
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]

        @global_merge_vars = [
            {"content" => @partner.attributes, "name" => "partner"},
            {"content" => @logo_location, "name" => "logo"},
            {"content" => "#{Host.generate_host_path}/admin/partners/#{@partner.id}/edit", "name" => "account_link"}
        ]
        @merge_language = "handlebars"

        if @partner.partner_type == INSPECTOR
            @global_merge_vars.push({"content" => data[:employees][:range], "name" => "employees"})
            @global_merge_vars.push({"content" => data[:inspections][:range], "name" => "inspections"})

            if data[:comments]
                @global_merge_vars.push({"content" => data[:comments], "name" => "comments"})
            end
        end

        if @partner.partner_type == BROKER
            @global_merge_vars.push({"content" => data[:transactions][:range], "name" => "transactions"})
            @global_merge_vars.push({"content" => data[:years][:range], "name" => "years"})
        end

        if data[:software]
            @global_merge_vars.push({"content" => data[:software].keys.join(", "), "name" => "software"})
        end

        if data[:source]
            @global_merge_vars.push({"content" => data[:source].keys.join(", "), "name" => "source"})
        end

        if @data[:otherSoftware]
            @global_merge_vars.push({"content" => data[:otherSoftware], "name" => "otherSoftware"})
        end

        if @data[:other]
            @global_merge_vars.push({"content" => data[:other], "name" => "other"})
        end

        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "new_partner_registration"}, @attachments, @global_merge_vars, nil, @merge_language)
    end

    def send_partner_feedback(homeowner_id, partner_email)
        homeowner = User.find_by_id(homeowner_id)
        return if homeowner.nil?
        
        @homeowner = homeowner
        @partner_email = partner_email

        # set variables
        @subject = "Client feedback alert from HomeBinder"
        @template = "Client to Partner Feedback"
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"

        @to = [
            {:email => @partner_email, :name => nil, :type => "to"},
            {:email => "transfers@homebinder.com", :name => nil, :type => "cc"}
        ]

        @global_merge_vars = [
            {"content"=> @homeowner.user_profile.display_name, "name"=>"homeowner"},
        ]
        @merge_language = "handlebars"

        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "send_partner_feedback"}, @attachments, @global_merge_vars, nil, @merge_language)
    end

    def send_services_requested(homeowner_id, binder_id, services, email, subject)
        homeowner = User.find_by_id(homeowner_id)
        binder = Binder.find_by_id(binder_id)
        return if homeowner.nil?
        return if binder.nil?
        
        @homeowner = homeowner
        @binder = binder
        @services = services

        # set variables
        @subject = "#{subject} Requested for #{@binder.full_address}"
        @template = "Third Party Services"
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"

        @to = [
            {:email => email, :name => nil, :type => "to"},
            {:email => "transfers@homebinder.com", :name => nil, :type => "cc"}
        ]

        @global_merge_vars = [
            {"content" => @homeowner.user_profile.display_name, "name"=>"homeowner"},
            {"content" => @homeowner.email, "name"=>"email"},
            {"content" => @homeowner.user_profile.mobile_phone, "name"=>"phone"},
            {"content" => @binder.full_address, "name"=>"address"},
            {"content" => @services, "name" => "services" },
        ]
        @merge_language = "handlebars"

        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "send_services_requested"}, @attachments, @global_merge_vars, nil, @merge_language)
    end

    def apr_widget(homeowner_id, binder_id, services)
        homeowner = User.find_by_id(homeowner_id)
        binder = Binder.find_by_id(binder_id)
        return if homeowner.nil?
        return if binder.nil?
        
        @homeowner = homeowner
        @binder = binder
        @services = services

        # set variables
        @subject = "More Info about APRs Requested for #{@binder.full_address}"
        @template = "Third Party Services"
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"

        @to = [
            {:email => "support@homebinder.com", :name => nil, :type => "to"},
            {:email => "transfers@homebinder.com", :name => nil, :type => "cc"}
        ]

        @global_merge_vars = [
            {"content" => @homeowner.user_profile.display_name, "name"=>"homeowner"},
            {"content" => @homeowner.email, "name"=>"email"},
            {"content" => @homeowner.user_profile.mobile_phone, "name"=>"phone"},
            {"content" => @binder.full_address, "name"=>"address"},
            {"content" => @services, "name" => "services" },
        ]
        @merge_language = "handlebars"

        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "apr_widget"}, @attachments, @global_merge_vars, nil, @merge_language)
    end
    
    def secure24(user_id, partner_id)
        user = User.find_by_id(user_id)
        partner = Partner.find_by_id(partner_id)
        return if user.nil?
        return if partner.nil?
        
        # set variables
        @subject = "Secure24 Request from HomeBinder"
        @template = "Secure24"
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"

        @to = [
            {:email => ENV["SECURE_24_EMAIL"], :name => nil, :type => "to"},
            {:email => partner.email, :name => nil, :type => "bcc"}
        ]
        
        @customer = {
            :name => user.user_profile.display_name,
            :email => user.email,
            :address => user.user_profile.address.full_address,
            :phone => user.user_profile.mobile_phone,
        }
        
        @partner = {
            :name => partner.name,
            :phone => partner.phone,
            :email => partner.email,
        }

        @global_merge_vars = [
            {"content"=> @customer, "name"=>"customer"},
            {"content"=> @partner, "name"=>"partner"},
        ]
        @merge_language = "handlebars"

        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "secure24"}, @attachments, @global_merge_vars, nil, @merge_language)
    end

    def send_failed_payment_notification(user_id)
        user = User.find_by_id(user_id)
        return if user.nil?
        
        # set variables
        @subject = "[Action Required] Please Update Your Billing Information"
        @template = "Payment Failed"
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"

        @to = [
            {:email => user.email, :name => nil, :type => "to"},
            {:email => @from_email, :name => nil, :type => "cc"}
        ]

        @global_merge_vars = [
            {"content" => user.user_profile.display_name, "name"=>"display_name"}
        ]
        @merge_language = "handlebars"

        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "payment_failed"}, @attachments, @global_merge_vars, nil, @merge_language)
    end
    
    def batch_binder_upload_confirmation(user_id, number_of_binders, number_of_duplicates)
        user = User.find_by_id(user_id)
        return if user.nil?
        
        # set variables
        @subject = "Batch Binder Upload Confirmation - #{number_of_binders} successfully uploaded"
        @template = "Batch Binder Upload Confirmation 1.0"
        @email = user.email
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]
        @global_merge_vars = [
            {"content"=> user.user_profile.display_name, "name"=>"display_name"},
            {"content"=> "#{number_of_binders}", "name"=>"number_of_binders"},
            {"content"=> "#{number_of_duplicates}", "name"=>"number_of_duplicates"}
        ]

        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "batch_binder_upload_confirmation"}, @attachments, @global_merge_vars)
    end
    
    def send_marketing_materials_notification(partner)
        # set variables
        @subject = "Custom Marketing Materials Available"
        @template = "Marketing Materials Available Notification"
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"

        @to = [{:email => partner.email, :name => nil, :type => "to"}]

        @global_merge_vars = [
            {"content" => partner.email_display_name, "name"=>"user"},
            {"content" => "#{Host.generate_host_path}/partners/#{partner.id}/resources", "name"=> "resources_link"}
        ]
        @merge_language = "handlebars"

        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "send_marketing_materials_notification"}, @attachments, @global_merge_vars, nil, @merge_language)
    end
end
