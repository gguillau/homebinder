require 'mandrill'
require "erb"

class MandrillMailer < ActionMailer::Base
    
    attr_accessor :client
    
    def initialize
        self.client = Mandrill::API.new Mandrill::ParseWebhookJob.new.password
    end
    
    # sends an email using the mandrill api
    # need to pass in a hash with subject, to, template and metadata
    # opts = {
    #     :subject => "Test Subject",
    #     :template => File.read("#{Rails.root}/app/views/transfer_mailer/test.html.erb"),
    #     :from_name => "John Smith",
    #     :from_email => "johnsmith@gmail.com",
    #     :to => [{:email => "janesmith@gmail.com", :to => "to", :name =>"Jane Smith"}],
    #     :metadata => {},
    #     :bcc_address => nil
    # }
    
    def send_email(opts)
        subject = opts[:subject]
        tpl = opts[:template]
        from_name = opts[:from_name]
        from_email = opts[:from_email]
        to = opts[:to]
        metadata = opts[:metadata]
        attachments = opts[:attachments]
        html = tpl.present? ? ERB.new(tpl).result(binding) : tpl
        return "email not sent" if to.any? {|receiver| receiver[:email] === "orphans@homebinder.com"}
        return "emails are disabled" if ENV["DISABLE_EMAILS"] === 'true'
        
        begin
            message = {
                :subject => subject,
                :from_name => from_name,
                :from_email => from_email,
                :to => to,
                :html => html,
                :metadata => metadata,
                :track_opens => true,
                :track_clicks => true,
                :attachments => attachments
            }
            client.messages.send message
            return message
        rescue => e
            ErrorService.perform_async(e.message, {:from_email => from_email, :subject => subject, :to => to.first[:email]})
        end
    end
    
    def send_template_email(opts)
        subject = opts[:subject]
        tpl = opts[:template]
        from_name = opts[:from_name]
        from_email = opts[:from_email]
        to = opts[:to]
        metadata = opts[:metadata]
        attachments = opts[:attachments]
        global_merge_vars = opts[:global_merge_vars]
        headers = opts[:headers]
        merge_language = opts[:merge_language]
        return "email not sent" if to.any? {|receiver| receiver[:email] === "orphans@homebinder.com"}
        return "emails are disabled" if ENV["DISABLE_EMAILS"] === 'true'
        
        begin
            message = {
                :subject => subject,
                :from_name => from_name,
                :from_email => from_email,
                :to => to,
                :metadata => metadata,
                :track_opens => true,
                :track_clicks => true,
                :attachments => attachments,
                :global_merge_vars => global_merge_vars,
                :headers => headers,
                :merge => true,
                :merge_language => merge_language
            }
      
            client.messages.send_template tpl, [], message

            return message
        rescue => e
            ErrorService.perform_async(e.message, {:from_email => from_email, :subject => subject, :to => to.first[:email]})
        end
    end
    
    def send_mandrill_email(subject, tpl, from_name, from_email, to, metadata = {}, attachments = nil)
        
        template = tpl.present? ? File.read(tpl) : tpl
        
        opts = {
            :subject => subject,
            :template => template,
            :from_name => from_name,
            :from_email => from_email,
            :to => to,
            :metadata => metadata,
            :attachments => attachments
        }
        return send_email(opts)
    end
    
    def send_mandrill_template_email(subject, tpl, from_name, from_email, to, metadata = {}, attachments = nil, global_merge_vars = nil, headers = nil, merge_language = "mailchimp")
        
        opts = {
            :subject => subject,
            :template => tpl,
            :from_name => from_name,
            :from_email => from_email,
            :to => to,
            :metadata => metadata,
            :attachments => attachments,
            :global_merge_vars => global_merge_vars,
            :headers => headers,
            :merge_language => merge_language
        }
        return send_template_email(opts)
    end
end