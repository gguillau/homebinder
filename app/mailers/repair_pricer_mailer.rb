class RepairPricerMailer < MandrillMailer

    require 'date'
    require 'active_support'
    require 'active_support/core_ext'
    
    default from: "HomeBinder.com <support@homebinder.com>"

    PRODUCTION = "http://www.homebinder.com".freeze
    TEST = "http://test.homebinder.com".freeze
    STAGING = "http://staging.homebinder.com".freeze
    FROM = "HomeBinder.com <support@homebinder.com>".freeze
    SUBJECT = "Your Monthly HomeBinder Summary for %s".freeze
    
    def send_repair_pricer_confirmation(user_id, binder_id)
        binder = Binder.find_by_id(binder_id)
        user = User.find_by_id(user_id)
        return if binder.nil?
        return if user.nil?
    
        # set variables
        @subject = "Cost Estimate Report Ordered"
        @template = "RP Order Notification"
        @user = user
        @email = @user.email
        @binder = binder
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]
        
        # send date is 45 days later
        now = Date.today
        @send_date = (now + 45)
        
        @merge_language = "handlebars"
        
        @global_merge_vars = [
            {"content" => @user.user_profile.first_name, "name"=> "user"},
            {"content" => @binder.full_address, "name" => "full_address" }
        ]

        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "confirmation_email"}, @attachments, @global_merge_vars, nil, @merge_language)
    end
    
    def send_inspector_notification(user_id, partner_id, binder_id)
        binder = Binder.find_by_id(binder_id)
        creator = User.find_by_id(user_id)
        partner = Partner.find_by_id(partner_id)
        return if binder.nil?
        return if creator.nil?
        return if partner.nil?
    
        # set variables
        @subject = "Prepaid Repair Pricer Report Ordered"
        @template = "RP Order - Inspector Notification"
        @creator = creator
        @partner = partner
        @email = @partner.email
        @binder = binder
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]
        
        person_ordered = @creator.user_profile.full_role + " " + @creator.user_profile.display_name
        
        @merge_language = "handlebars"
        
        @global_merge_vars = [
            {"content" => @partner.email_display_name, "name"=> "partner_name"},
            {"content" => person_ordered, "name"=> "person_ordered"},
            {"content" => @binder.full_address, "name" => "full_address" }
        ]

        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "repair_pricer_inspector_notification"}, @attachments, @global_merge_vars, nil, @merge_language)
    end
    
    def find_a_pro(repair_pricer_report_id)
        repair_pricer_report = RepairPricerReport.find_by_id(repair_pricer_report_id)
        return if repair_pricer_report.nil?
        
        # set variables
        @report         = repair_pricer_report
        @findings       = format_findings(@report)
        @binder_id      = @report.binder_id
        @partner        = @report.partner
        @subject        = "Take Action on Home Improvements Identified During Your Inspection"
        @template       = "RP Items - Find Pros"
        @from_name      = "HomeBinder Support"
        @from_email     = "support@homebinder.com"
        partner_name    = @partner ? @partner.name : ""
        
        # get homeowner name to display in email
        @homeowner_name = get_homeowner_name(@report.client_first, @report.client_email)
        
        @to = [
            {:email => @report.client_email, :name => nil, :type => "to"}
        ]
        
        @global_merge_vars = [
            {"content" => @homeowner_name, "name"=>"homeowner_name"},
            {"content" => partner_name, "name" => "referring_partner" },
            {"content" => "#{Host.generate_host_path}/binders/#{@binder_id}/binder_contractors?proClicked=true&email=repairPricer", "name" => "homepro_link" },
            {"content" => @findings, "name" => "findings" }
        ]
      
        @merge_language = "handlebars"
        
        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "repair_pricer_find_pro"}, @attachments, @global_merge_vars, nil, @merge_language)
    end
    
    private
    
    def get_homeowner_name(first, email)
        if !first.nil?
            first
        else
            email
        end
    end
    
    def format_findings(report)
        costs = []
        types = report.repair_pricer_report_findings.distinct.pluck(:contractor_type).uniq
        types.each do |category|
            finding = {
                contractor_type: category, 
                defective_price: report.repair_pricer_report_findings.where(:contractor_type => category).sum(:defective_price)
            }
            costs.push finding
        end
        costs
    end
end