class AgentMergeMailer < MandrillMailer

    PRODUCTION = "http://www.homebinder.com".freeze
    TEST = "http://test.homebinder.com".freeze
    STAGING = "http://staging.homebinder.com".freeze
    FROM = "HomeBinder.com <support@homebinder.com>".freeze
    
    # takes hashes for old_agent_data and new_agent_data
    # send_to chooses who to send the email to: 0 = old_agent, anything else else = new_agent
    def notify_agents(old_agent_data, new_agent_data, merger_id)
        # set variables
        @template = "Agent_Merge Binder Notify_1.0"
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @subject = "Your duplicate accounts have been merged"
        
        # get agent and merger data
        @old_agent = old_agent_data
        @new_agent = new_agent_data
        @merger = get_merger_data(merger_id)
        
        # set to
        @to_old = [{:email => @old_agent[:email], :name => nil, :type => "to"}]
        @to_new = [{:email => @new_agent[:email], :name => nil, :type => "to"}]
        
        @global_merge_vars = [
            {"content" => @old_agent[:first_name], "name" => "first_name"},
            {"content"=> OpenStruct.new(@old_agent).to_h, "name"=>"old_agent"},
            {"content"=> OpenStruct.new(@new_agent).to_h, "name"=>"new_agent"},
            {"content"=> OpenStruct.new(@merger).to_h, "name"=>"merger"},
        ]
        @merge_language = "handlebars"

        # send the emails
        send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to_old, {:email_type => "agent_merge_email"}, @attachments, @global_merge_vars, nil, @merge_language)
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to_new, {:email_type => "agent_merge_email"}, @attachments, @global_merge_vars, nil, @merge_language)
    end
    
    # takes hashes for old_agent_data and new_agent_data
    def support_notification(old_agent_data, new_agent_data, merger_id, binder_ids)
        # set variables
        @template = "Agent Merge Support Notification 1.0"
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => "support@homebinder.com", :name => nil, :type => "to"}]
        
        # set agent and merger data
        @old_agent = old_agent_data
        @new_agent = new_agent_data
        @merger = get_merger_data(merger_id)
        
        # set subject
        @subject = "Agent Merge Alert: #{@new_agent[:first_name]} #{@new_agent[:last_name]} by #{@merger[:name]}".freeze
        
        @global_merge_vars = [
            {"content"=> OpenStruct.new(@old_agent).to_h, "name"=>"old_agent"},
            {"content"=> OpenStruct.new(@new_agent).to_h, "name"=>"new_agent"},
            {"content"=> OpenStruct.new(@merger).to_h, "name"=>"merger"},
            {"content"=> binder_ids, "name"=>"binder_ids"},
            {"content"=> binder_ids.length, "name"=>"num_binders"}
        ]
        @merge_language = "handlebars"

        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "agent_merge_support_email"}, @attachments, @global_merge_vars, nil, @merge_language)
    end
    
    def get_merger_data(merger_id)
        merger = User.find_by_id(merger_id)
        return if merger.nil?
        
        if merger.role == "admin"
            return {
                user_id: merger.id,
                partner_id: nil,
                name: "HomeBinder admin",
                company: "https://www.homebinder.com",
                email: merger.email
            }
        else
            partner = PartnerUser.where(:user_id => merger_id).first.partner
            return if partner.nil?
            
            return {
                user_id: merger.id,
                partner_id: partner.id,
                name: partner.contact,
                company: partner.name,
                email: merger.email
            }
        end
    end
end