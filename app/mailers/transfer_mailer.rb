class TransferMailer < MandrillMailer
  include ActionView::Helpers::NumberHelper
  
  FROM_EMAIL = "support@homebinder.com".freeze
  FROM = "HomeBinder.com <#{FROM_EMAIL}>".freeze
  TRANSFEREMAIL = "transfers@homebinder.com".freeze
  PRODUCTION = "www.homebinder.com".freeze
  TEST = "http://test.homebinder.com".freeze
  STAGING = "http://staging.homebinder.com".freeze
  
  default from: FROM
  
  def notify_email(is_resend = false, cc_transfer_by, transfer_id)
    transfer = Binder::Transfer.find_by_id(transfer_id)
    return if transfer.nil?
    
    transfer_by = transfer.sender
    transfer_to = transfer.receiver
    binder = transfer.binder
    return if transfer_by.nil?
    return if transfer_to.nil?
    return if binder.nil?
    
    @transfer_to   = transfer_to
    @transfer_by   = transfer_by
    @binder        = binder
    @transfer      = transfer
    
    @template     = "Transfer from User"
    @from_name    = @transfer_by.user_profile.display_name
    @from_email   = @transfer_by.email
    
    generate_subject(is_resend)
    generate_transfer_link
    generate_decline_link
    
    if is_resend
      @to = [{:email => @transfer_to.email, :name => @transfer_to.user_profile.display_name, :type => "to"}]
    elsif cc_transfer_by
      @to = [
        {:email => @transfer_to.email, :name => @transfer_to.user_profile.display_name, :type => "to"},
        {:email => @transfer_by.email, :name => @transfer_by.user_profile.display_name, :type => "cc"},
        {:email => TRANSFEREMAIL, :type => "bcc"}
      ]
    else
      @to = [
        {:email => @transfer_to.email, :name => @transfer_to.user_profile.display_name, :type => "to"},
        {:email => TRANSFEREMAIL, :type => "bcc"}
      ]
    end
    
    @global_merge_vars = [
        {"content" => @transfer_to.user_profile.display_name, "name"=>"to"},
        {"content" => @transfer_by.user_profile.display_name, "name"=>"from"},
        {"content" => @binder.full_address, "name" => "full_address" },
        {"content" => @transfer_link, "name" => "welcome_link" },
        {"content" => @decline_link, "name" => "decline_link" },
        {"content" => @hero_location, "name" => "hero_photo"}
    ]
    @merge_language = "handlebars"
    # send the email
    return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "transfer_from_user", :transfer_id => @transfer.id}, @attachments, @global_merge_vars, nil, @merge_language)
  end
  
  def notify_email_from_inspector(is_resend = false, cc_transfer_by, transfer_id, partner_id)
    transfer = Binder::Transfer.find_by_id(transfer_id)
    partner = Partner.find_by_id(partner_id)
    return if transfer.nil?
    return if partner.nil?
    
    transfer_by = transfer.sender
    transfer_to = transfer.receiver
    binder = transfer.binder
    return if transfer_by.nil?
    return if transfer_to.nil?
    return if binder.nil?
    
    @transfer_to  = transfer_to
    @transfer_by  = transfer_by
    @binder       = binder
    @partner      = partner
    @transfer     = transfer
    
    @template     = set_inspector_template(binder)
    @from_name    = @transfer_by.user_profile.display_name
    @from_email   = @partner.email
    
    generate_binder_details 
    generate_inspector_subject(is_resend)
    generate_transfer_link
    generate_decline_link
    generate_binder_branding
    
    if is_resend
      @to = [{:email => @transfer_to.email, :name => @transfer_to.user_profile.display_name, :type => "to"}]
    elsif cc_transfer_by
      @to = [
        {:email => @transfer_to.email, :name => @transfer_to.user_profile.display_name, :type => "to"},
        {:email => @transfer_by.email, :name => @transfer_by.user_profile.display_name, :type => "cc"},
        {:email => TRANSFEREMAIL, :type => "bcc"}
      ]
    else
      @to = [
        {:email => @transfer_to.email, :name => @transfer_to.user_profile.display_name, :type => "to"},
        {:email => TRANSFEREMAIL, :type => "bcc"}
      ]
    end
    
    client_first_name = @transfer_to.user_profile.first_name.present? ? @transfer_to.user_profile.first_name : ""
    
    @global_merge_vars = [
        {"content" => @partner.attributes, "name"=>"partner"},
        {"content" => @binder.full_address, "name" => "full_address" },
        {"content" => @transfer_link, "name" => "welcome_link" },
        {"content" => @decline_link, "name" => "decline_link" },
        {"content" => @hero_location, "name" => "hero_photo"},
        {"content" => @binder.attributes, "name" => "binder" },
        {"content" => client_first_name, "name" => "client_first_name"}
    ]
    @merge_language = "handlebars"
    
    if @brandings.length > 0
      @global_merge_vars.push({"content" => OpenStruct.new(@brandings[0]).to_h, "name" => "branding_user_1"})
    end
    
    # send the email
    return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "transfer_from_inspector", :transfer_id => @transfer.id}, @attachments, @global_merge_vars, nil, @merge_language)
  end
  
  def notify_email_from_broker(is_resend = false, cc_transfer_by, transfer_id, partner_id)
    transfer = Binder::Transfer.find_by_id(transfer_id)
    partner = Partner.find_by_id(partner_id)
    return if transfer.nil?
    return if partner.nil?
    
    transfer_by = transfer.sender
    transfer_to = transfer.receiver
    binder = transfer.binder
    return if transfer_by.nil?
    return if transfer_to.nil?
    return if binder.nil?
    
    @transfer_to  = transfer_to
    @transfer_by  = transfer_by
    @binder       = binder
    @partner      = partner
    @to_no_exist  = transfer_to.sign_in_count == 0
    @transfer     = transfer
    
    @template     = "Custom Transfer from Broker 1.0"
    @from_name    = @transfer_by.user_profile.display_name
    @from_email   = @partner.email
    
    generate_broker_subject(is_resend)
    generate_transfer_link
    generate_decline_link
    generate_binder_details
    generate_binder_branding
    
    if is_resend
      @to = [{:email => @transfer_to.email, :name => @transfer_to.user_profile.display_name, :type => "to"}]
    elsif cc_transfer_by
      @to = [
        {:email => @transfer_to.email, :name => @transfer_to.user_profile.display_name, :type => "to"},
        {:email => @transfer_by.email, :name => @transfer_by.user_profile.display_name, :type => "cc"},
        {:email => TRANSFEREMAIL, :type => "bcc"}
      ]
    else
      @to = [
        {:email => @transfer_to.email, :name => @transfer_to.user_profile.display_name, :type => "to"},
        {:email => TRANSFEREMAIL, :type => "bcc"}
      ]
    end
    
    client_first_name = @transfer_to.user_profile.first_name.present? ? @transfer_to.user_profile.first_name : ""
    
    @global_merge_vars = [
        {"content" => @partner.attributes, "name"=>"partner"},
        {"content" => @binder.full_address, "name" => "full_address" },
        {"content" => @transfer_link, "name" => "welcome_link" },
        {"content" => @decline_link, "name" => "decline_link" },
        {"content" => @hero_location, "name" => "hero_photo"},
        {"content" => @binder.attributes, "name" => "binder" },
        {"content" => client_first_name, "name" => "client_first_name" },
    ]
    @merge_language = "handlebars"
    
    if @brandings.length > 0
      @global_merge_vars.push({"content" => OpenStruct.new(@brandings[0]).to_h, "name" => "branding_user_1"})
    end
    
    # send the email
    return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "transfer_from_broker", :transfer_id => @transfer.id}, @attachments, @global_merge_vars, nil, @merge_language)
  end
  
  def notify_email_from_hoa(is_resend = false, cc_transfer_by, transfer_id, partner_id)
    transfer = Binder::Transfer.find_by_id(transfer_id)
    partner = Partner.find_by_id(partner_id)
    return if transfer.nil?
    return if partner.nil?
    
    transfer_by = transfer.sender
    transfer_to = transfer.receiver
    binder = transfer.binder
    return if transfer_by.nil?
    return if transfer_to.nil?
    return if binder.nil?
    
    @transfer_to  = transfer_to
    @transfer_by  = transfer_by
    @binder       = binder
    @partner      = partner
    @to_no_exist  = transfer_to.sign_in_count == 0
    @transfer     = transfer
    
    @template     = "Custom transfer from HOA 1.0"
    @from_name    = @transfer_by.user_profile.display_name
    @from_email   = @partner.email
    
    generate_broker_subject(is_resend)
    generate_transfer_link
    generate_decline_link
    generate_binder_details
    generate_binder_branding
    
    if is_resend
      @to = [{:email => @transfer_to.email, :name => @transfer_to.user_profile.display_name, :type => "to"}]
    elsif cc_transfer_by
      @to = [
        {:email => @transfer_to.email, :name => @transfer_to.user_profile.display_name, :type => "to"},
        {:email => @transfer_by.email, :name => @transfer_by.user_profile.display_name, :type => "cc"},
        {:email => TRANSFEREMAIL, :type => "bcc"}
      ]
    else
      @to = [
        {:email => @transfer_to.email, :name => @transfer_to.user_profile.display_name, :type => "to"},
        {:email => TRANSFEREMAIL, :type => "bcc"}
      ]
    end
    
    client_first_name = @transfer_to.user_profile.first_name.present? ? @transfer_to.user_profile.first_name : ""
    
    @global_merge_vars = [
        {"content" => @partner.attributes, "name"=>"partner"},
        {"content" => @binder.full_address, "name" => "full_address" },
        {"content" => @transfer_link, "name" => "welcome_link" },
        {"content" => @decline_link, "name" => "decline_link" },
        {"content" => @hero_location, "name" => "hero_photo"},
        {"content" => @binder.attributes, "name" => "binder" },
        {"content" => client_first_name, "name" => "client_first_name" },
    ]
    @merge_language = "handlebars"
    
    if @brandings.length > 0
      @global_merge_vars.push({"content" => OpenStruct.new(@brandings[0]).to_h, "name" => "branding_user_1"})
    end
    
    # send the email
    return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "transfer_from_hoa", :transfer_id => @transfer.id}, @attachments, @global_merge_vars, nil, @merge_language)
  end
  
  def notify_email_from_partner(is_resend = false, cc_transfer_by, transfer_id, partner_id)
    transfer = Binder::Transfer.find_by_id(transfer_id)
    partner = Partner.find_by_id(partner_id)
    return if transfer.nil?
    return if partner.nil?
    
    transfer_by = transfer.sender
    transfer_to = transfer.receiver
    binder = transfer.binder
    return if transfer_by.nil?
    return if transfer_to.nil?
    return if binder.nil?
    
    @transfer_to  = transfer_to
    @transfer_by  = transfer_by
    @binder       = binder
    @partner      = partner
    @to_no_exist  = transfer_to.sign_in_count == 0
    @transfer     = transfer
    
    @template     = "Transfer from Partner"
    @from_name    = @transfer_by.user_profile.display_name
    @from_email   = @partner.email
    
    generate_broker_subject(is_resend)
    generate_transfer_link
    generate_decline_link
    generate_binder_details
    generate_binder_branding
    
    if is_resend
      @to = [{:email => @transfer_to.email, :name => @transfer_to.user_profile.display_name, :type => "to"}]
    elsif cc_transfer_by
      @to = [
        {:email => @transfer_to.email, :name => @transfer_to.user_profile.display_name, :type => "to"},
        {:email => @transfer_by.email, :name => @transfer_by.user_profile.display_name, :type => "cc"},
        {:email => TRANSFEREMAIL, :type => "bcc"}
      ]
    else
      @to = [
        {:email => @transfer_to.email, :name => @transfer_to.user_profile.display_name, :type => "to"},
        {:email => TRANSFEREMAIL, :type => "bcc"}
      ]
    end
    
    client_first_name = @transfer_to.user_profile.first_name.present? ? @transfer_to.user_profile.first_name : ""
    
    @global_merge_vars = [
        {"content" => @partner.attributes, "name"=>"partner"},
        {"content" => @binder.full_address, "name" => "full_address" },
        {"content" => @transfer_link, "name" => "welcome_link" },
        {"content" => @decline_link, "name" => "decline_link" },
        {"content" => @hero_location, "name" => "hero_photo"},
        {"content" => @binder.attributes, "name" => "binder" },
        {"content" => client_first_name, "name" => "client_first_name" },
    ]
    @merge_language = "handlebars"
    
    if @brandings.length > 0
      @global_merge_vars.push({"content" => OpenStruct.new(@brandings[0]).to_h, "name" => "branding_user_1"})
    end
    
    # send the email
    return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "transfer_from_partner", :transfer_id => @transfer.id}, @attachments, @global_merge_vars, nil, @merge_language)
  end
  
  def notify_email_to_agent(agent_id, transfer_id, partner_id)
    transfer = Binder::Transfer.find_by_id(transfer_id)
    partner = Partner.find_by_id(partner_id)
    agent = User.find_by_id(agent_id)
    return if transfer.nil?
    return if partner.nil?
    return if agent.nil?
    
    transfer_by = transfer.sender
    transfer_to = transfer.receiver
    binder = transfer.binder
    return if transfer_by.nil?
    return if transfer_to.nil?
    return if binder.nil?
    
    @agent        = agent
    @binder       = binder
    @transfer_by  = transfer_by
    @partner      = partner
    @subject      = "HomeBinder for #{@binder.full_address}"
    
    # Get homeowner and client name, and return if they do not exist
    return if @binder.status === "orphan"
    return if @binder.partner_binders.empty?
    homeowner = @binder.partner_binders.first.user
    return if homeowner.nil?
    client_name = homeowner.user_profile.display_name
    
    @agent_display_name = @agent.user_profile.display_name
    @transfer_by_display_name = @transfer_by.user_profile.display_name.present? ? @transfer_by.user_profile.display_name : @transfer_by.user_profile.company
    
    generate_partner_logo_link

    @to = [{:email => @agent.email, :name => @agent.user_profile.display_name, :type => "to"}]
    
    @template     = set_agent_template_and_subject(binder, partner, client_name)
    @from_name    = @transfer_by.user_profile.display_name
    @from_email   = @transfer_by.email
    
    @user = {
      name: @transfer_by.user_profile.display_name,
      company: @transfer_by.user_profile.company,
      email: @transfer_by.email,
      phone: @transfer_by.user_profile.mobile_phone_formatted,
      website: @transfer_by.user_profile.website,
      logo: @logo_location,
      transfer_message_to_agent: @transfer_by.user_profile.branding[:transfer_message_to_agent],
      transfer_message_to_prepaid_agent: @transfer_by.user_profile.branding[:transfer_message_to_prepaid_agent]
    }
    
    # Generate access token for agent to order repair pricer 
    user_binder = UserBinder.where(:user_id => @agent.id, :binder_id => @binder.id).first
    user_binder.regenerate_repair_pricer_token
    
    @global_merge_vars = [
      {"content" => @binder.full_address, "name" => "full_address" },
      {"content" => @agent_display_name, "name" => "agent_display_name" },
      {"content" => OpenStruct.new(@user).to_h, "name" => "inspector" },
      {"content" => @hero_location, "name" => "hero_photo"},
      {"content" => @binder.attributes, "name" => "binder" },
      {"content" => @agent.user_profile.branding, "name" => "agent" },
      {"content" => client_name, "name" => "client_name"},
      {"content" => "#{Host.generate_host_path}/#{@agent.welcome_path}/#{@agent.access_token}/welcome", "name"=> "welcome_link"},
      {"content" => "#{Host.generate_host_path}/order_repair_pricer?repair_pricer_token=#{user_binder.repair_pricer_token}", "name"=> "repair_pricer_link"}
    ]
    
    @merge_language = "handlebars"
    
    # send the email
    return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "agent_notification"}, @attachments, @global_merge_vars, nil, @merge_language)
  end

  private
  
  def generate_binder_details
    @details = @binder.details.present? ? @binder.details.html_safe : nil
  end
  
  def generate_subject(is_resend)
    if is_resend
      @subject = "Reminder! HomeBinder for #{@binder.property.address1.titleize}, #{@binder.property.city.titleize}, #{@binder.property.state.upcase}"
    else
      @subject = "HomeBinder for #{@binder.property.address1.titleize}, #{@binder.property.city.titleize}, #{@binder.property.state.upcase}"
    end
  end
  
  def generate_broker_subject(is_resend)
    if is_resend
      @subject = "Reminder! HomeBinder for #{@binder.property.address1.titleize}, #{@binder.property.city.titleize}, #{@binder.property.state.upcase}"
    else
      @subject = "HomeBinder for #{@binder.property.address1.titleize}, #{@binder.property.city.titleize}, #{@binder.property.state.upcase}"
    end
  end
  
  def generate_inspector_subject(is_resend)
    if is_resend
      @subject = "Reminder! Last step in your home inspection for #{@binder.property.address1.titleize}, #{@binder.property.city.titleize}, #{@binder.property.state.upcase}"
    else
      @subject = "Last step in your home inspection for #{@binder.property.address1.titleize}, #{@binder.property.city.titleize}, #{@binder.property.state.upcase}"
    end
  end
  
  def generate_transfer_link
    @transfer_link = "#{Host.generate_host_path}/homeowners/#{@transfer.access_token}/welcome?notify=email"
  end
  
  def generate_decline_link
    @decline_link = "#{Host.generate_host_path}/homeowners/#{@transfer.access_token}/decline"
  end
  
  def generate_partner_logo_link
    if @transfer_by.user_profile.logo.present?
      @logo_location = @transfer_by.user_profile.logo.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :original)
    end
  end
  
  def generate_binder_branding
    # get the brandings
    @brandings = @binder.generate_branding("transfer_email")
  end
  
  def set_inspector_template(binder)
    if binder.transactions.where(:transaction_type => "sell_side_inspection").count > 0
      return "Transfer from Inspector Sell Side"
    else
      return "Custom Transfer from Inspector 1.1"
    end
  end
  
  def set_agent_template_and_subject(binder, partner, client_name)
    if binder.transactions.where(:transaction_type => "sell_side_inspection").count > 0
      return "Agent Notification Email Pre Listing Inspection"
      
    # If the partner has repair pricer pre-paid and the agent has access to it
    elsif partner.partner_configuration.repair_pricer_pre_paid_reports && UserBinder.where(:binder_id => binder.id, :role => "buyer_agent", :access_repair_pricer => true).exists?
      @subject = "#{@transfer_by.user_profile.company} set up #{client_name} with a HomeBinder"
      return "Custom Agent Notification Pre-RP 1.1"
    # Default template
    else
      return "Custom Agent Notification 1.1"
    end
  end
end
