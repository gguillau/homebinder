class ShareMailer < MandrillMailer
  
  FROM = "HomeBinder.com <support@homebinder.com>".freeze
  SUBJECT = "Homebinder.com Share Notification".freeze
  TRANSFEREMAIL = "transfers@homebinder.com".freeze
  
  default from: FROM

  def notify_email(share_id)
    share = Binder::Share.find_by_id(share_id)
    return if share.nil?
    
    binder = share.binder
    from = share.sender
    with = share.receiver
    return if binder.nil?
    return if with.nil?
    return if from.nil?
    
    # set variables
    @with = with
    @from = from
    @binder = binder
    @share = share
    
    generate_share_link
    generate_decline_link

    @subject     = SUBJECT
    @template     = "Share from User"
    @from_name    = @from.user_profile.display_name
    @from_email   = @from.email
    
    @to = [
      {:email => @with.email, :name => @with.user_profile.display_name, :type => "to"},
      {:email => TRANSFEREMAIL, :type => "bcc"}
    ]

    @global_merge_vars = [
        {"content" => @from.user_profile.display_name, "name"=>"from"},
        {"content" => @binder.full_address, "name" => "full_address" },
        {"content" => @welcome_link, "name" => "welcome_link" },
        {"content" => @decline_link, "name" => "decline_link" },
        {"content" => @hero_location, "name" => "hero_photo"}
    ]
    
    @merge_language = "handlebars"
    
    # send the email
    return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "share_from_user", :share_id => @share.id}, @attachments, @global_merge_vars, nil, @merge_language)
  end
  
  def notify_email_from_inspector(share_id, partner_id)
    share = Binder::Share.find_by_id(share_id)
    partner = Partner.find_by_id(partner_id)
    return if share.nil?
    return if partner.nil?
    
    binder = share.binder
    from = share.sender
    with = share.receiver
    return if binder.nil?
    return if with.nil?
    return if from.nil?
    
    # set variables
    @with = with
    @from = from
    @binder = binder
    @partner = partner
    @share = share
    
    generate_binder_details
    generate_binder_branding
    generate_inspector_subject
    generate_share_link
    generate_decline_link

    @template     = "Custom Transfer from Inspector 1.1"
    @from_name    = @partner.name
    @from_email   = @partner.email
    
    @to = [
      {:email => with.email, :name => with.user_profile.display_name, :type => "to"},
      {:email => TRANSFEREMAIL, :type => "bcc"}
    ]

    @global_merge_vars = [
        {"content" => @from.user_profile.first_name, "name"=>"client_first_name"},
        {"content" => @binder.full_address, "name" => "full_address" },
        {"content" => @binder.attributes, "name" => "binder" },
        {"content" => @partner.attributes, "name" => "partner" },
        {"content" => @welcome_link, "name" => "welcome_link" },
        {"content" => @decline_link, "name" => "decline_link" },
        {"content" => @hero_location, "name" => "hero_photo"}
    ]
    @merge_language = "handlebars"
    
    if @brandings.length > 0
      @global_merge_vars.push({"content" => OpenStruct.new(@brandings[0]).to_h, "name" => "branding_user_1"})
    end

    # send the email
    return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "share_from_inspector", :share_id => @share.id}, @attachments, @global_merge_vars, nil, @merge_language)
  end
  
  def notify_email_from_broker(share_id, partner_id)
    share = Binder::Share.find_by_id(share_id)
    partner = Partner.find_by_id(partner_id)
    return if share.nil?
    return if partner.nil?
    
    binder = share.binder
    from = share.sender
    with = share.receiver
    return if binder.nil?
    return if with.nil?
    return if from.nil?
    
    # set variables
    @with = with
    @from = from
    @binder = binder
    @partner = partner
    @share = share
    
    generate_share_link
    generate_decline_link
    generate_binder_branding
    generate_broker_subject

    @template     = "Share from Broker"
    @from_name    = @partner.name
    @from_email   = @partner.email
    
    @to = [
      {:email => with.email, :name => with.user_profile.display_name, :type => "to"},
      {:email => TRANSFEREMAIL, :type => "bcc"}
    ]

    @global_merge_vars = [
        {"content" => @from.user_profile.display_name, "name"=>"from"},
        {"content" => @binder.full_address, "name" => "full_address" },
        {"content" => @binder.attributes, "name" => "binder" },
        {"content" => @partner.attributes, "name" => "partner" },
        {"content" => @welcome_link, "name" => "welcome_link" },
        {"content" => @decline_link, "name" => "decline_link" },
        {"content" => @hero_location, "name" => "hero_photo"}
    ]
    @merge_language = "handlebars"
    
    if @brandings.length > 0
      @global_merge_vars.push({"content" => OpenStruct.new(@brandings[0]).to_h, "name" => "branding_user_1"})
    end

    # send the email
    return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "share_from_broker", :share_id => @share.id}, @attachments, @global_merge_vars, nil, @merge_language)
  end
  
  def notify_email_from_partner(share_id, partner_id)
    share = Binder::Share.find_by_id(share_id)
    partner = Partner.find_by_id(partner_id)
    return if share.nil?
    return if partner.nil?
    
    binder = share.binder
    from = share.sender
    with = share.receiver
    return if binder.nil?
    return if with.nil?
    return if from.nil?
    
    # set variables
    @with = with
    @from = from
    @binder = binder
    @partner = partner
    @share = share
    
    generate_share_link
    generate_decline_link
    generate_binder_branding
    generate_broker_subject

    @template     = "Share from Partner"
    @from_name    = @partner.name
    @from_email   = @partner.email
    
    @to = [
      {:email => with.email, :name => with.user_profile.display_name, :type => "to"},
      {:email => TRANSFEREMAIL, :type => "bcc"}
    ]

    @global_merge_vars = [
        {"content" => @from.user_profile.display_name, "name"=>"from"},
        {"content" => @binder.full_address, "name" => "full_address" },
        {"content" => @binder.attributes, "name" => "binder" },
        {"content" => @partner.attributes, "name" => "partner" },
        {"content" => @welcome_link, "name" => "welcome_link" },
        {"content" => @decline_link, "name" => "decline_link" },
        {"content" => @hero_location, "name" => "hero_photo"}
    ]
    @merge_language = "handlebars"
    
    if @brandings.length > 0
      @global_merge_vars.push({"content" => OpenStruct.new(@brandings[0]).to_h, "name" => "branding_user_1"})
    end

    # send the email
    return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "share_from_partner", :share_id => @share.id}, @attachments, @global_merge_vars, nil, @merge_language)
  end
  
  private
  
  def generate_broker_subject
    @subject = "HomeBinder for #{@binder.property.address1.titleize}, #{@binder.property.city.titleize}, #{@binder.property.state.upcase}"
  end
  
  def generate_inspector_subject
    @subject = "Last step in your home inspection for #{@binder.property.address1.titleize}, #{@binder.property.city.titleize}, #{@binder.property.state.upcase}"
  end
  
  def generate_binder_details
    @details = @binder.details.present? ? @binder.details.html_safe : nil
  end
  
  def generate_share_link
    @welcome_link = "#{Host.generate_host_path}/homeowners/#{@share.access_token}/welcome"
  end
  
  def generate_decline_link
    @decline_link = "#{Host.generate_host_path}/homeowners/#{@share.access_token}/decline"
  end
  
  def generate_binder_branding
    # get the brandings
    @brandings = @binder.generate_branding("share_email")
  end
  
end
