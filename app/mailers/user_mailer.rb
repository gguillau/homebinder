class UserMailer < MandrillMailer

    require 'date'
    require 'active_support'
    require 'active_support/core_ext'

    PRODUCTION = "http://www.homebinder.com".freeze
    TEST = "http://test.homebinder.com".freeze
    STAGING = "http://staging.homebinder.com".freeze
    FROM = "HomeBinder.com <support@homebinder.com>".freeze
    SUBJECT = "Your Monthly HomeBinder Summary for %s".freeze

    def send_reset_password_instructions(user_id, token)
        user = User.find_by_id(user_id)
        return if user.nil?
        
        # set variables
        @subject = "HomeBinder Password Reset"
        @template = "Reset Password"
        @email = user.email
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]
        @global_merge_vars = [
            {"content"=> "#{user.user_profile.display_name}", "name"=>"user_name"},
            {"content"=> "#{Host.generate_host_path}/passwords/#{token}/update?userId=#{user.id}", "name"=>"reset_link"},
            {"content" => "#{Rails.configuration.password_reset_time_frame.hours/3600} hours", "name" => "expire_length"}
        ]

        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "password_reset"}, @attachments, @global_merge_vars)
    end
    
    def custom_support_email(user_id, binder_id)
        user = User.find_by_id(user_id)
        binder = Binder.find_by_id(binder_id)
        return if user.nil?
        return if binder.nil?
        
        # set variables
        @subject = "A new easy way to add to your HomeBinder"
        @template = "Homeowner Custom Email Notification"
        @email = user.email
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]
        
        @merge_language = "handlebars"
        
        @global_merge_vars = [
            {"content" => binder.attributes, "name"=> "binder"}
        ]

        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "custom_support_email"}, @attachments, @global_merge_vars, nil, @merge_language)
    end

    def send_invitation(user_id, invitee_id = nil)
        user = User.find_by_id(user_id)
        return if user.nil?
        return if user.is_homeowner?
        
        return notify_agent(user_id, invitee_id) if user.role === "agent"
        return notify_homepro(user_id, user&.contractor&.creator&.id) if user.role === "homepro"
        
        # set variables
        @subject = "Welcome to HomeBinder!"
        @template = "User Invitation"
        @email = user.email
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]
        welcome_path = user.welcome_path
        
        @merge_language = "handlebars"
        
        if user.is_admin?
            invited_by = "HomeBinder, INC"
        else
            if user.partners.exists?
                invited_by = user.partners.order(:id).first.name
            else
                invited_by = "HomeBinder, INC"
            end
        end
        
        @global_merge_vars = [
            {"content" => "#{Host.generate_host_path}/#{welcome_path}/#{user.access_token}/welcome", "name"=> "welcome_link"},
            {"content" => user.user_profile.branding, "name"=> "user"},
            {"content" => invited_by, "name"=> "invited_by"}
        ]

        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "invitation_email"}, @attachments, @global_merge_vars, nil, @merge_language)
    end
    
    def notify_homepro(user_id, creator_id, template = "Home Pro Profile Invitation from Homeowner")
        homepro = User.find_by_id(user_id)
        return if homepro.nil?
        
        creator = User.find_by_id(creator_id)
        return if creator.nil?
        
        # set variables
        @subject = "Access your HomeBinder Profile"
        @template = template
        @email = homepro.email
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]
        welcome_path = homepro.welcome_path
        
        @merge_language = "handlebars"

        invited_by = creator
        
        @global_merge_vars = [
            {"content" => "#{Host.generate_host_path}/#{welcome_path}/#{homepro.access_token}/welcome", "name"=> "welcome_link"},
            {"content" => homepro.user_profile.branding, "name"=> "homepro"},
            {"content" => invited_by, "name"=> "invited_by"}
        ]

        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "homepro_profile_invitation"}, @attachments, @global_merge_vars, nil, @merge_language)
    end
    
    def notify_agent(user_id, invitee_id = nil, template = "Agent Profile Invitation", partner_id = nil)
        agent = User.find_by_id(user_id)
        return if agent.nil?
        
        # set variables
        @subject = "Access your HomeBinder Profile"
        @template = template
        @email = agent.email
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]
        welcome_path = agent.welcome_path
        @binders = agent.binders.distinct.map{ |binder| binder.full_address}
        
        @merge_language = "handlebars"

        if partner_id.present?
            invited_by = Partner.find_by_id(partner_id).name
        elsif invitee_id.present?
            invitee = User.find(invitee_id)
            invited_by = invitee.user_profile.company
        else
            partner = agent.partners.order(:id).first
            if partner.present?
                invited_by = partner.name
            else
                invited_by = "The HomeBinder Team"
            end
        end
        
        @global_merge_vars = [
            {"content" => "#{Host.generate_host_path}/#{welcome_path}/#{agent.access_token}/welcome", "name"=> "welcome_link"},
            {"content" => agent.user_profile.branding, "name"=> "agent"},
            {"content" => invited_by, "name"=> "invited_by"},
            {"content" => @binders, "name"=> "binders"}
        ]

        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "agent_profile_invitation"}, @attachments, @global_merge_vars, nil, @merge_language)
    end

    def monthly_email(user_id, binder_id)
        user = User.find_by_id(user_id)
        binder = Binder.find_by_id(binder_id)
        return if user.nil?
        return if binder.nil?
        
        @user = user
        @binder = binder

        # dates
        @first_day = 1.month.ago.beginning_of_month
        @last_day = 1.month.ago.end_of_month

        # methods
        sort_items
        sort_appliances
        sort_projects
        sort_home_pros

        # set variables
        @subject = SUBJECT % [@binder.property.address1]
        @template = "Monthly Homeowner Mailer "
        @email = @user.email
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]
        @global_merge_vars = [
            {"content"=> "Your HomeBinder Update for #{@first_day.strftime("%B")} #{@first_day.strftime("%Y")}", "name"=>"title"},
            {"content"=> @first_day.strftime("%B %d"), "name"=>"first_day"},
            {"content"=> @last_day.strftime("%B %d"), "name"=>"last_day"},
            {"content"=> @items_count, "name"=>"items_count"},
            {"content"=> @home_pros_count, "name"=>"home_pros_count"},
            {"content"=> @projects_count, "name"=>"projects_count"},
            {"content"=> @img, "name"=>"img"},
            {"content"=> @maintenance_items, "name"=>"maintenance_items"},
            {"content"=> @appliances, "name"=>"appliances"}
        ]

        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "monthly_mailer"}, @attachments, @global_merge_vars)
    end

    def sort_items
        @maintenance_items = @binder.maintenance_items.joins(:maintenance_events).where(:maintenance_events => {:do_date => Date.today..Date.today + 30.days})
        @items_count = @binder.maintenance_items.joins(:maintenance_events).where(:maintenance_events => {:completed_date => Date.today..Date.today + 30.days}).count
    end

    def sort_appliances
        @appliances = @binder.appliances.joins(:appliance_recalls).where(:appliance_recalls => {:status => "new"})
    end

    def sort_home_pros
        @home_pros_count = @binder.binder_contractors.where(:created_at => @first_day..@last_day).count
    end

    def sort_projects
        @projects_count = @binder.projects.where(:end_date => @first_day..@last_day, :status => "Completed").count
    end
end
