class BinderMailer < MandrillMailer

  default from: "HomeBinder.com <support@homebinder.com>"

  def notify_of_unverified_items(binder_id)
    binder = Binder.find_by_id(binder_id)
    return if binder.nil?
    
    @subject     = "Please verify - #{binder.binder_items.unverified.count} item(s) added to your HomeBinder!"
    @template     = "Binder Items"
    @from_name    = "HomeBinder Support"
    @from_email   = "support@homebinder.com"
    @owner        = binder.owner
    
    # we don't want to regenerate the token if it already exists since
    # the owner might receive multiple emails and the approve/dismiss links
    # need to work each time
    
    @owner.regenerate_access_token if @owner.access_token.nil?

    @items = binder.binder_items.unverified.map {|item|
      {
        name: item.item.name,
        type: item.type,
        company_name: item.user.user_profile.company || item.user.user_profile.display_name
      }
    }

    @to = [
        {:email => binder.owner.email, :name => nil, :type => "to"},
        {:email => "transfers@homebinder.com", :name => nil, :type => "bcc"}
    ]

    @global_merge_vars = [
        {"content" => binder.owner.user_profile.display_name, "name"=> "display_name"},
        {"content" => @items, "name" => "items" },
        {"content" => binder.full_address, "name" => "binder_address" },
        {"content" => "#{Host.generate_host_path}/binders/#{binder.id}/overview", "name" => "overview_page"},
        {"content" => "#{Host.generate_host_path}/binders/#{binder.id}/verify/#{@owner.access_token}",   "name" => "accept_link"},
        {"content" => "#{Host.generate_host_path}/binders/#{binder.id}/decline/#{@owner.access_token}",  "name" => "dimiss_link"}
    ]

    @merge_language = "handlebars"

    # send the email
    return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "binder_items"}, @attachments, @global_merge_vars, nil, @merge_language)
  end
  
  def inventory_report(binder_id, user_id)
    binder = Binder.find_by_id(binder_id)
    user = User.find_by_id(user_id)
    return if binder.nil?
    return if user.nil?
    
    pdf = InventoryReportPdfService.new(binder, user).create
    
    # add the file
    @attachments = [:type => "pdf", :content => Base64.encode64(pdf), :name => "inventory_report.pdf"]
    
    # set variables
    @subject = "Inventory PDF Report for #{binder.full_address}"
    @template = nil
    @from_name = "HomeBinder Support"
    @from_email = "support@homebinder.com"
    @to = [{:email => user.email, :name => nil, :type => "to"}]

    # send the email
    return send_mandrill_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "inventory_report"}, @attachments)
  end
  
  def capital_expense_report(binder_id, user_id)
    binder = Binder.find_by_id(binder_id)
    user = User.find_by_id(user_id)
    return if binder.nil?
    return if user.nil?
    
    pdf = CapitalExpenseReportPdfService.new(binder, user).create
    
    # add the file
    @attachments = [:type => "pdf", :content => Base64.encode64(pdf), :name => "capital_expense_report.pdf"]
    
    # set variables
    @subject = "Capital Expense PDF Report for #{binder.full_address}"
    @template = nil
    @from_name = "HomeBinder Support"
    @from_email = "support@homebinder.com"
    @to = [{:email => user.email, :name => nil, :type => "to"}]

    # send the email
    return send_mandrill_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "capital_expense_report"}, @attachments)
  end
  
  def home_pro_work_request(user_id, binder_id, pro_id, partner_id, homeowner_note)
    binder = Binder.find_by_id(binder_id)
    homeowner = User.find_by_id(user_id)
    pro = Contractor.find_by_id(pro_id)
    partner = Partner.find_by_id(partner_id)
    return if binder.nil?
    return if homeowner.nil?
    return if pro.nil?
    
    @user           = homeowner
    @binder         = binder
    @partner        = partner
    @pro            = pro
    @subject        =  "#{@user.user_profile.display_name} requesting your services"
    @template       = "Home Pro Work Request 1.0"
    @from_name      = @user.user_profile.display_name
    @from_email     = @user.email
    
    @to = [
        {:email => @pro.email, :name => nil, :type => "to"}
    ]
    
    @global_merge_vars = [
        {"content" => @user.user_profile.display_name, "name"=>"homeowner_name"},
        {"content" => homeowner_note, "name" => "homeowner_note" },
        {"content" => @user.user_profile.mobile_phone, "name" => "homeowner_phone" },
        {"content" => @user.email, "name" => "homeowner_email" },
        {"content" => @binder.full_address, "name" => "property_state" },
        {"content" => @pro.types.pluck(:name).first, "name" => "pro_type" },
        {"content" => @pro.branding_name, "name" => "home_pro" }
    ]
    
    if @partner
      @global_merge_vars.push({"content" => @partner.name, "name" => "referring_partner" })
      
      @from_name      = @partner.name
      @from_email     = @partner.email
    end
  
    @merge_language = "handlebars"
    
    # send the email
    return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "home_pro_work_request"}, @attachments, @global_merge_vars, nil, @merge_language)
  end
  
  def home_pro_work_request_confirmation(user_id, binder_id, pro_id, partner_id, homeowner_note)
    binder = Binder.find_by_id(binder_id)
    homeowner = User.find_by_id(user_id)
    pro = Contractor.find_by_id(pro_id)
    partner = Partner.find_by_id(partner_id)
    return if binder.nil?
    return if homeowner.nil?
    return if pro.nil?
    
    @user           = homeowner
    @binder         = binder
    @partner        = partner
    @pro            = pro
    @subject        =  "#{@pro.types.pluck(:name).first} Contacted On Your Behalf"
    @template       = "Homeowner Work Request Notification"
    @from_name      = "HomeBinder Support"
    @from_email     = "support@homebinder.com"
    
    @to = [
        {:email => @user.email, :name => nil, :type => "to"}
    ]
    
    @global_merge_vars = [
        {"content" => @user.user_profile.display_name, "name"=>"homeowner_name"},
        {"content" => homeowner_note, "name" => "homeowner_note" },
        {"content" => @user.user_profile.mobile_phone, "name" => "homeowner_phone" },
        {"content" => @user.email, "name" => "homeowner_email" },
        {"content" => @binder.full_address, "name" => "property_state" },
        {"content" => @pro.types.pluck(:name).first, "name" => "homepro_type" },
        {"content" => @pro.branding_name, "name" => "home_pro" },
        {"content" => @pro.phone, "name" => "home_pro_phone" },
        {"content" => @pro.email, "name" => "home_pro_email" }
    ]
    
    if @partner
      @global_merge_vars.push({"content" => @partner.name, "name" => "referring_partner" })
    end
  
    @merge_language = "handlebars"
    
    # send the email
    return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "home_pro_work_request_confirmation"}, @attachments, @global_merge_vars, nil, @merge_language)
  end
  
  def home_pro_work_request_support(user_id, binder_id, pro_id, partner_id, homeowner_note)
    binder = Binder.find_by_id(binder_id)
    homeowner = User.find_by_id(user_id)
    pro = Contractor.find_by_id(pro_id)
    partner = Partner.find_by_id(partner_id)
    return if binder.nil?
    return if homeowner.nil?
    return if pro.nil?
    
    @user           = homeowner
    @binder         = binder
    @partner        = partner
    @pro            = pro
    @subject        =  "Action Required: HO Pro Request for #{@binder.full_address}"
    @template       = "HB Notification of Requested Home Pro Service"
    @from_name      = "HomeBinder Support"
    @from_email     = "support@homebinder.com"
    
    @to = [
        {:email => "support@homebinder.com", :name => nil, :type => "to"}
    ]
    
    @global_merge_vars = [
        {"content" => @user.user_profile.display_name, "name"=>"homeowner_name"},
        {"content" => homeowner_note, "name" => "homeowner_note" },
        {"content" => @user.user_profile.mobile_phone, "name" => "homeowner_phone" },
        {"content" => @user.email, "name" => "homeowner_email" },
        {"content" => @binder.full_address, "name" => "property_state" },
        {"content" => @pro.types.pluck(:name).first, "name" => "homepro_type" },
        {"content" => @pro.branding_name, "name" => "home_pro" },
        {"content" => @pro.phone, "name" => "home_pro_phone" },
        {"content" => @pro.email, "name" => "home_pro_email" }
    ]
    
    if @partner
      @global_merge_vars.push({"content" => @partner.name, "name" => "referring_partner" })
    end
  
    @merge_language = "handlebars"
    
    # send the email
    return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "home_pro_work_request_support"}, @attachments, @global_merge_vars, nil, @merge_language)
  end
end
