class AnnualPropertyReviewMailer < MandrillMailer
    include ActionView::Helpers::NumberHelper
    
    PRODUCTION = "http://www.homebinder.com".freeze
    TEST = "http://test.homebinder.com".freeze
    STAGING = "http://staging.homebinder.com".freeze
  
    def new_client_apr(apr_id)
        apr = AnnualPropertyReview.find_by_id(apr_id)
        return if apr.nil?
        
        # set variables
        @subject = "Annual Property Review Request for #{apr.address1} #{apr.city} #{apr.state}"
        @template = "New APR Homeowner"
        @email = apr.client_email
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]
        @global_merge_vars = [
            {"content"=> "#{apr.client_first_name} #{apr.client_last_name}", "name"=>"client_name"},
            {"content"=> "#{apr.address1}", "name"=>"client_address"},
            {"content"=> "#{apr.city}", "name"=>"client_city"},
            {"content"=> "#{apr.state}", "name"=>"client_state"},
            {"content"=> "#{apr.zip}", "name"=>"client_zip"},
            {"content"=> "#{apr.country}", "name"=>"client_country"},
            {"content"=> "#{number_to_currency(apr.amount_charged_cents/100, :precision => 2)}", "name"=>"apr_cost"},
            {"content"=> apr.uuid, "name"=> "apr_id"},
            {"content"=> "#{Host.generate_host_path}/apr/#{apr.uuid}", "name"=> "apr_link"}
        ]
        
        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "apr_created"}, @attachments, @global_merge_vars)
    end
    
    def new_company_apr(apr_id)
        apr = AnnualPropertyReview.find_by_id(apr_id)
        return if apr.nil?
        return if apr.reviewer_company_id.nil?
        
        # set variables
        @subject = "Annual Property Review Request for #{apr.address1} #{apr.city} #{apr.state}"
        @template = "New APR Partner "
        @email = apr.reviewer_company.email
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]
        @global_merge_vars = [
            {"content"=> "#{apr.reviewer_company.name}", "name"=>"company_name"},
            {"content"=> "#{apr.client_first_name} #{apr.client_last_name}", "name"=>"client_name"},
            {"content"=> "#{apr.address1}", "name"=>"client_address"},
            {"content"=> "#{apr.city}", "name"=>"client_city"},
            {"content"=> "#{apr.state}", "name"=>"client_state"},
            {"content"=> "#{apr.zip}", "name"=>"client_zip"},
            {"content"=> "#{apr.country}", "name"=>"client_country"},
            {"content"=> "#{apr.client_comments}", "name"=>"client_comments"}
        ]
        
        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "apr_created"}, @attachments, @global_merge_vars)
    end
    
    def new_support_apr(apr_id)
        apr = AnnualPropertyReview.find_by_id(apr_id)
        return if apr.nil?
        
        if apr.reviewer_company_id.nil?
            company_name = "Unassigned"
            company_id = "N/A"
        else
            company_name = apr.reviewer_company.name
            company_id = apr.reviewer_company.id
        end
        
        # set variables
        @subject = "Annual Property Review Request for #{apr.address1} #{apr.city} #{apr.state}"
        @template = "New APR Support "
        @email = "support@homebinder.com"
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]
        @global_merge_vars = [
            {"content"=> "#{company_name}", "name"=>"company_name"},
            {"content"=> "#{company_id}", "name"=>"company_id"},
            {"content"=> "#{apr.client_first_name} #{apr.client_last_name}", "name"=>"client_name"},
            {"content"=> "#{apr.address1}", "name"=>"client_address"},
            {"content"=> "#{apr.city}", "name"=>"client_city"},
            {"content"=> "#{apr.state}", "name"=>"client_state"},
            {"content"=> "#{apr.zip}", "name"=>"client_zip"},
            {"content"=> "#{apr.country}", "name"=>"client_country"},
            {"content"=> "#{apr.client_comments}", "name"=>"client_comments"},
            {"content"=> "#{number_to_currency(apr.amount_charged_cents/100, :precision => 2)}", "name"=>"apr_cost"},
        ]
        
        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "apr_created"}, @attachments, @global_merge_vars)
    end

    def scheduled(apr_id)
        apr = AnnualPropertyReview.find_by_id(apr_id)
        return if apr.nil?
        
        # set variables
        @subject = "APR Scheduled for #{apr.address1} #{apr.city} #{apr.state}"
        @template = "APR Scheduled "
        @email = apr.client_email
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]
        @global_merge_vars = [
            {"content"=> "#{apr.reviewer_company.name}", "name"=>"company_name"},
            {"content"=> "#{apr.client_first_name} #{apr.client_last_name}", "name"=>"client_name"},
            {"content"=> "#{apr.scheduled_for.strftime("%A, %B %d, %Y")}", "name"=>"date"},
            {"content"=> "#{apr.scheduled_for.strftime("%I:%M %p %Z")}", "name"=>"time"},
            {"content"=> "#{apr.created_at.year}", "name"=>"apr_year"},
            {"content"=> "#{Host.generate_host_path}/apr/#{apr.uuid}", "name"=> "apr_link"}
        ]
        
        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "apr_scheduled"}, @attachments, @global_merge_vars)
    end

    def draft_completed(apr)
    end

    def completed(apr_id)
        apr = AnnualPropertyReview.find_by_id(apr_id)
        return if apr.nil?
        
        # set variables
        @subject = "APR Report Completed for #{apr.address1} #{apr.city} #{apr.state}"
        @template = "APR Completed "
        @email = apr.client_email
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]
        @global_merge_vars = [
            {"content"=> "#{apr.reviewer_company.name}", "name"=>"company_name"},
            {"content"=> "#{apr.client_first_name} #{apr.client_last_name}", "name"=>"client_name"},
            {"content"=> "#{apr.created_at.year}", "name"=>"apr_year"},
            {"content"=> "#{Host.generate_host_path}/apr/#{apr.uuid}", "name"=> "apr_link"}
        ]
        
        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "apr_scheduled"}, @attachments, @global_merge_vars)
    end

    def canceled(apr_id, sender, receiver)
        apr = AnnualPropertyReview.find_by_id(apr_id)
        return if apr.nil?
        
        # set variables
        @subject = "APR Report Canceled for #{apr.address1} #{apr.city} #{apr.state}"
        @template = "APR Canceled "
        @email = receiver[:email]
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]
        @global_merge_vars = [
            {"content"=> "#{sender[:name]}", "name"=>"sender_name"},
            {"content"=> "#{receiver[:name]}", "name"=>"receiver_name"},
            {"content"=> "#{apr.address1} #{apr.city} #{apr.state}", "name"=>"address"},
            {"content"=> "#{apr.created_at.year}", "name"=>"apr_year"},
            {"content"=> "#{Host.generate_host_path}/apr/#{apr.uuid}", "name"=> "apr_link"}
        ]
        
        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "apr_canceled"}, @attachments, @global_merge_vars)
    end

    def on_hold(apr_id, sender, receiver)
        apr = AnnualPropertyReview.find_by_id(apr_id)
        return if apr.nil?
        
        # set variables
        @subject = "APR Report On Hold for #{apr.address1} #{apr.city} #{apr.state}"
        @template = "APR On Hold "
        @email = receiver[:email]
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]
        @global_merge_vars = [
            {"content"=> "#{sender[:name]}", "name"=>"sender_name"},
            {"content"=> "#{receiver[:name]}", "name"=>"receiver_name"},
            {"content"=> "#{apr.address1} #{apr.city} #{apr.state}", "name"=>"address"},
            {"content"=> "#{apr.created_at.year}", "name"=>"apr_year"},
            {"content"=> "#{Host.generate_host_path}/apr/#{apr.uuid}", "name"=> "apr_link"}
        ]
        
        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "apr_on_hold"}, @attachments, @global_merge_vars)
    end
    
    def reminder(apr_id)
        apr = AnnualPropertyReview.find_by_id(apr_id)
        return if apr.nil?
        
        # set variables
        @subject = "APR Review Reminder for #{apr.address1} #{apr.city} #{apr.state}"
        @template = "APR Reminder "
        @email = apr.client_email
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]
        @global_merge_vars = [
            {"content"=> "#{apr.reviewer_company.name}", "name"=>"company_name"},
            {"content"=> "#{apr.client_first_name} #{apr.client_last_name}", "name"=>"client_name"},
            {"content"=> "#{apr.scheduled_for.strftime("%A, %B %d, %Y")}", "name"=>"date"},
            {"content"=> "#{apr.scheduled_for.strftime("%I:%M %p %Z")}", "name"=>"time"},
            {"content"=> "#{apr.created_at.year}", "name"=>"apr_year"},
            {"content"=> "#{Host.generate_host_path}/apr/#{apr.uuid}", "name"=> "apr_link"}
        ]
        
        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "apr_reminder"}, @attachments, @global_merge_vars)
    end
    
    def message_to_homeowner(apr_id, message)
        apr = AnnualPropertyReview.find_by_id(apr_id)
        return if apr.nil?
        
        # set variables
        @subject = "New Message from #{apr.reviewer_company.name}"
        @template = "APR Message "
        @email = apr.client_email
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]
        @global_merge_vars = [
            {"content"=> "#{apr.reviewer_company.name}", "name"=>"company_name"},
            {"content"=> "#{apr.client_first_name} #{apr.client_last_name}", "name"=>"client_name"},
            {"content"=> "#{apr.created_at.year}", "name"=>"apr_year"},
            {"content"=> "#{Host.generate_host_path}/apr/#{apr.uuid}", "name"=> "apr_link"},
            {"content"=>  message, "name"=> "message"}
        ]

        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "apr_message"}, @attachments, @global_merge_vars)
    end
    
    def message_to_reviewer(apr_id, message)
        apr = AnnualPropertyReview.find_by_id(apr_id)
        return if apr.nil?
        
        # set variables
        @subject = "New Message from #{apr.client_first_name}"
        @template = "APR Message "
        @email = apr.reviewer_company.email
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]
        @global_merge_vars = [
            {"content"=> "#{apr.reviewer_company.name}", "name"=>"company_name"},
            {"content"=> "#{apr.client_first_name} #{apr.client_last_name}", "name"=>"client_name"},
            {"content"=> "#{apr.created_at.year}", "name"=>"apr_year"},
            {"content"=> "#{Host.generate_host_path}/apr/#{apr.uuid}", "name"=> "apr_link"},
            {"content"=>  message, "name"=> "message"}
        ]

        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "apr_message"}, @attachments, @global_merge_vars)
    end
    
    def request_quote(apr_id, recommended_homepro_id, comments, ids)
        apr = AnnualPropertyReview.find_by_id(apr_id)
        recommended_homepro = Template::Contractor.find_by_id(recommended_homepro_id)
        return if apr.nil?
        return if recommended_homepro.nil?
        
        # get the selected findings/home pro/homeowner comments
        findings = apr.annual_property_review_findings.where(:id => ids).map {|finding|
            {
                photo: finding.default_photo,
                name: finding.name,
                priority: finding.priority,
                estimated_cost: finding.cost,
                details: finding.details
            }
        }
        
        # set variables
        @subject = "Quote Request for #{apr.address1} #{apr.city} #{apr.state}"
        @template = "Home Pro Request Email"
        @email = recommended_homepro.partner_contractor.email
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => @email, :name => nil, :type => "to"}]
        
        @global_merge_vars = [
            {"content"=> "#{recommended_homepro.partner_contractor.name}", "name"=>"home_pro_company"},
            {"content"=> "#{apr.reviewer_company.name}", "name"=>"reviewer_company_name"},
            {"content"=> "#{apr.reviewer_company.email}", "name"=>"reviewer_company_email"},
            {"content"=> "#{apr.reviewer_company.phone_formatted}", "name"=>"reviewer_company_phone"},
            {"content"=> "#{apr.client_first_name} #{apr.client_last_name}", "name"=>"homeowner_name"},
            {"content"=> "#{apr.client_phone_formatted}", "name"=>"homeowner_phone"},
            {"content"=> "#{apr.address1} #{apr.city} #{apr.state}", "name"=>"property_address"},
            {"content"=>  comments, "name"=> "comments"},
            {"content"=>  findings, "name"=> "findings"}
        ]
        @headers = {"Reply-To" => apr.client_email}
        @merge_language = "handlebars"
        
        # send the email
        return send_mandrill_template_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "apr_quote"}, @attachments, @global_merge_vars, @headers, @merge_language)
    end
    
    def send_full_report(apr_id, user_id)
        apr = AnnualPropertyReview.find_by_id(apr_id)
        user = User.find_by_id(user_id)
        return if apr.nil?
        
        pdf = AnnualPropertyReviews::AprPdfGenerator.new(apr_id).generate

        if user.present?
            email = user.email
        else
            email =  apr.client_email
        end
        # add the file
        @attachments = [:type => "pdf", :content => Base64.encode64(pdf), :name => "annual_property_review.pdf"]
        
        # set variables
        @subject = "Annual Property Review Full PDF Report"
        @template = nil
        @from_name = "HomeBinder Support"
        @from_email = "support@homebinder.com"
        @to = [{:email => email, :name => nil, :type => "to"}]

        # send the email
        return send_mandrill_email(@subject, @template, @from_name, @from_email, @to, {:email_type => "annual_property_review_full_report"}, @attachments)
    end
end
