class Api::V1::FinishesController < HomebinderController
  def klass
    "Binder::Finish".classify.constantize
  end
end
