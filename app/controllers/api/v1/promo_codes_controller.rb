class Api::V1::PromoCodesController < HomebinderController
    skip_before_action :verify_jwt_token, :only => [:verify]
    
    def verify
        render :json => PromoCode.verify_promo_code(params[:amount], params[:promoCode])
    end
end
