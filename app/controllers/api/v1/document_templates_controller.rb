class Api::V1::DocumentTemplatesController < HomebinderController
    def klass
    "Template::Document".classify.constantize
  end
end
