class Api::V1::FreeTrialsController < HomebinderController
  
  EMAIL_SENT = "Email sent.".freeze
  
  skip_before_action :verify_api_key
  skip_before_action :verify_jwt_token
  
  def create
    FreeTrialRequestMailer.notify_free_trial_request_mail(params[:name], params[:email], params[:phone]).deliver_later
    render :status => 200, :json => {:message=> EMAIL_SENT}
  end
  
end
