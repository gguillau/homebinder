class Api::V1::ContractorTemplatesController < HomebinderController
    # we need to specify the serializer because the controller confuses contractors with
    # contractor templates and thus uses the wrong serializer
    
    def index
        render :json => klass.index(self.hb_request, params), each_serializer: Template::ContractorSerializer
    rescue => e
        raise BadRequestException.new(e.message)
    end
  
    def create
        render :json => klass.build(self.hb_request, params), serializer: Template::ContractorSerializer
    rescue => e
        case e
        when UnprocessableException
            raise UnprocessableException.new(e.resource)
        else
            raise BadRequestException.new(e.message)
        end
    end
    
    def show
        render :json => klass.show(self.hb_request, params), serializer: Template::ContractorSerializer
    rescue => e
        raise BadRequestException.new(e.message)
    end
    
    def update
        render :json => klass.update(self.hb_request, params), serializer: Template::ContractorSerializer
    rescue => e
        case e
        when UnprocessableException
            raise UnprocessableException.new(e.resource)
        else
            raise BadRequestException.new(e.message)
        end
    end
    
    def klass
      "Template::Contractor".classify.constantize
    end
end
