class Api::V1::PartnerConfigurationsController < HomebinderController
    def klass
        "Partner::Configuration".classify.constantize
    end
end
