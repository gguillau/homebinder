class Api::V1::AutomationErrorsController < HomebinderController
    def klass
        "Partner::Automation::Error".classify.constantize
    end
end
