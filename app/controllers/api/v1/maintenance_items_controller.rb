class Api::V1::MaintenanceItemsController < HomebinderController
  def klass
    "Binder::MaintenanceItem".classify.constantize
  end

  def send_test_email
    klass.send_test_notification_email(self.hb_request, params[:id])
    head :no_content
  end
end
