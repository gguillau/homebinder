class Api::V1::CouponsController < HomebinderController
  require 'stripe'
  
  INVALID_COUPON = "Invalid coupon. Verify the coupon is spelled correctly using the correct case.".freeze
  PROCESSING_ERROR = "We're having trouble processing your request. Try again later.".freeze
  
  def show
    begin
      render json: Stripe::Coupon.retrieve(params[:id]) 
      rescue Stripe::CardError => e
        # Since it's a decline, Stripe::CardError will be caught
        body = e.json_body
        err  = body[:error]
        
        render json: {message: "#{err[:message].titleize}"}, status: :bad_request 
      rescue Stripe::InvalidRequestError => e
        # Since it's a decline, Stripe::CardError will be caught
        body = e.json_body
        err  = body[:error]
        
        #render json: {message: "#{err[:message].titleize}"}, status: :unprocessable_entity
        render json: {message: INVALID_COUPON}, status: :bad_request
      rescue Stripe::AuthenticationError => e
        # Authentication with Stripe's API failed
        # (maybe you changed API keys recently)
        render json: {message: PROCESSING_ERROR}, status: :bad_request
      rescue Stripe::APIConnectionError => e
        # Network communication with Stripe failed
        render json: {message: PROCESSING_ERROR}, status: :bad_request
      rescue Stripe::StripeError => e
        # Display a very generic error to the user, and maybe send
        # yourself an email
        render json: {message: PROCESSING_ERROR}, status: :bad_request
    end
  end
end
