class Api::V1::UserTokensController < HomebinderController
  
    skip_before_action :verify_jwt_token, :only => [:create]
    
    def create
        user = User.signin(params[:email], params[:password])
        render :status => 200, :json=> {token: UserTokenService.create_jwt(user, params)}
    rescue => e
        case e
        when UnprocessableException
            raise UnprocessableException.new(e.resource)
        else
            raise BadRequestException.new(e.message)
        end
    end
        
    def destroy
        session = Session.find_by_token(params[:id])
        if session.nil?
            render :status => 404, :json => {:message=> I18n.t(:err_invalid_user_token)}
        else
            session.update(:status => 1)
            render :status => 200, :json => {:token => params[:id]}
        end
    rescue => e
        case e
        when UnprocessableException
            raise UnprocessableException.new(e.resource)
        else
            raise BadRequestException.new(e.message)
        end
    end
end
