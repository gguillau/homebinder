class Api::V1::EventsController < HomebinderController

    def create
        EventService.perform_async(params[:event].to_unsafe_h)
        head :no_content
    end

end
