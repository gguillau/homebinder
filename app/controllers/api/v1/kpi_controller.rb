class Api::V1::KpiController < HomebinderController

    FILE_NAME = "HomeBinder_KPI_Report_%s.pdf".freeze
    FILE_TYPE = "application/pdf".freeze
    FILE_SIZE = "A4".freeze

    def get_summary
        render :json => Kpi::Summary.new(self.hb_request).get_summary
    end

    def get_binders
        render :json => Kpi::Summary.new(self.hb_request).get_binder_totals
    end

    def get_users
        render :json => Kpi::Summary.new(self.hb_request).get_user_totals
    end

    def get_engagement
        render :json => Kpi::Summary.new(self.hb_request).get_engagement(params)
    end

    def download
        Kpi::SummaryReportJob.perform_async(self.hb_request.user.id)
        head :no_content
    end
    
    def account_summaries
        Partners::AccountSummaryJob.perform_async(self.hb_request.user.id)
        head :no_content
    end
end
