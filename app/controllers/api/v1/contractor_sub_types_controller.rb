class Api::V1::ContractorSubTypesController < HomebinderController
    def klass
        "Contractor::SubCategory".classify.constantize
    end
end
