class Api::V1::OrganizationsController < HomebinderController
  include Organizations
  include Partners
  include Users
  
  def add_partners
    Organizations::OrganizationBuilder.new(current_user).add_partner(params)
    head :no_content
  end
  
  def remove_partner
    Organizations::OrganizationBuilder.new(current_user).remove_partner(params)
    head :no_content
  end
  
  def add_users
    Organizations::OrganizationBuilder.new(current_user).add_user(params)
    head :no_content
  end
  
  def remove_user
    Organizations::OrganizationBuilder.new(current_user).remove_user(params)
    head :no_content
  end
end