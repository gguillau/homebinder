class Api::V1::RepairPricerReportsController < HomebinderController
    skip_before_action :verify_jwt_token, :only => [:create]
end
