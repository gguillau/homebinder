class Api::V1::BinderItemsController < HomebinderController
    before_action :set_token, :only => [:index, :update]
    skip_before_action :verify_jwt_token, :only => [:index, :update]

    def set_token
        if params[:token].present? && params[:token] != "undefined"
            self.hb_request.user = User.find_by_access_token(params[:token])
            self.hb_request.ability = hb_request.user.ability
            self.hb_request.role = hb_request.user.role
        else
            verify_jwt_token
        end
    end
end
