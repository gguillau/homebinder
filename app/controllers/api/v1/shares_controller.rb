class Api::V1::SharesController < HomebinderController

    def resend
        render :json => klass.resend(self.hb_request, params)
    end

    def klass
        "Binder::Share".classify.constantize
    end
end
