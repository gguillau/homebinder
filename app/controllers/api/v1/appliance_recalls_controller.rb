class Api::V1::ApplianceRecallsController < HomebinderController
    
    def send_email
        Binder::Appliance::ApplianceRecall.find(params[:id]).notify
        head :no_content
    end
    
    def klass
        "Binder::Appliance::ApplianceRecall".classify.constantize
    end
end