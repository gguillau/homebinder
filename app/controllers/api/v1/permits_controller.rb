class Api::V1::PermitsController < HomebinderController
    def klass
        "Binder::Permit".classify.constantize
    end
end
