class Api::V1::CustomersController < HomebinderController

    def show
        render :json => Payments::Customer.new(current_user).show(params)
    end
    
    def create
        render :json => Payments::Customer.new(current_user).create(params)
    end
    
    def update
        render :json => Payments::Customer.new(current_user).update(params)
    end
    
    def destroy
        Payments::Customer.new(current_user).delete(params)
        head :no_content
    end
end
