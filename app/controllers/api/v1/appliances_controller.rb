class Api::V1::AppliancesController < HomebinderController
  def send_test_email
    Binder::Appliance.send_test_recall_email(current_user, params[:id])
    head :no_content
  end
  
  def check_for_recalls
    render :json => Binder::Appliance.check_for_recalls(current_user, params[:id])
  end
  
  def klass
    "Binder::Appliance".classify.constantize
  end
end
