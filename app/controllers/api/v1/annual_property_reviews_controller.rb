class Api::V1::AnnualPropertyReviewsController < HomebinderController
    USER_TOKEN_HEADER_KEY = "HB-UserToken".freeze
    skip_before_action :verify_jwt_token, :only => [:show, :create, :update, :download, :request_quote]
    
    def show
        render :json => klass.show(get_user, params), serializer: AnnualPropertyReviewFullSerializer
    end

    def create
        render json: klass.build(get_user, { annual_property_review: apr_params, payment: params[:payment] })
    end
    
    def update
        render json: klass.update(get_user, { id: params[:id], annual_property_review: apr_params, payment: params[:payment] }), serializer: AnnualPropertyReviewFullSerializer
    end
    
    def download
        user = get_user
        AnnualPropertyReviewMailer.send_full_report(params[:id], user&.id).deliver_later
        head :no_content
    end
      
    def messages
        klass.message(get_user, params)
        head :no_content
    end
      
    def request_quote
        klass.request_quote(get_user, params)
        head :no_content
    end
    
    private
    
    def apr_params
        params.require(:annual_property_review).permit!
    end
      
    def get_user
        token = request.headers[USER_TOKEN_HEADER_KEY]
        if not token
          token = params[USER_TOKEN_HEADER_KEY]
        end
        
        return nil if token.nil?
        return UserTokenService.verify_jwt_token token
    end
end
