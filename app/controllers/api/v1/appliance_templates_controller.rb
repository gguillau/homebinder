class Api::V1::ApplianceTemplatesController < HomebinderController
  def klass
    "Template::Appliance".classify.constantize
  end
end
