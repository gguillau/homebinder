module Api
  module V1
    class ServiceKeysController < HomebinderController
      API_HEADER_KEY = "HB-APIKey".freeze
      
      skip_before_action  :verify_api_key
      skip_before_action  :init_response
      skip_before_action  :verify_jwt_token
    
      def index
        keys = nil
        api_key = request.headers[API_HEADER_KEY]
        api_key = params[:api_key] unless api_key.present?
        keys = ServiceKeys::ServiceKeysMapper.get_keys api_key
          
        render json: keys
      end
    end
  end
end