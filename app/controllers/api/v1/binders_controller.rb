class Api::V1::BindersController < HomebinderController
  before_action :create_service, :except => [:index, :show, :create, :destroy, :find_binder_by_property, :batch]
  
  def batch
    Binders::BatchUploadJob.perform_async(hb_request.user.id, params.to_unsafe_h)
    head :no_content
  rescue => e
    raise BadRequestException.new(e.message)
  end
  
  def orphan
    render :json => @service.orphan
  end

  def sendFeedback
    @service.send_partner_feedback
    head :no_content
  end
  
  def secure24
    @service.secure24
    head :no_content
  end
  
  def requestServices
    Binders::RequestServicesJob.perform_async(self.hb_request.user.id, params[:id], params.to_unsafe_h)
    head :no_content
  end
  
  def requestSolarServices
    render :json => @service.requestSolarServices(params).to_json
  end
  
  def declineServices
    Binders::DeclineServicesJob.perform_async(self.hb_request.user.id, params[:id], params.to_unsafe_h)
    head :no_content
  end
  
  def learn_more_apr
    @service.learn_more_apr
    head :no_content
  end
  
  def add_user
    render :json => @service.add_user(params)
  end

  def get_acl
    render :json => @service.get_acl
  end

  def delete_acl
    @service.delete_acl(params)
    head :no_content
  end

  def add_partner
    render :json => @service.add_partner(params)
  end
  
  def get_partner
    render :json => @service.get_partner
  end

  def delete_partner
    @service.delete_partner(params)
    head :no_content
  end
  
  def inventory_report
    @service.inventory_report
    head :no_content
  end
  
  def capital_expense_report
    @service.capital_expense_report
    head :no_content
  end
  
  def find_binder_by_property
    render :json => Partner::Automation::Service.verify_duplicate(params)
  end
  
  private
  
  def create_service
    binder = Binder.find(params[:id])
    @service = BinderService.new(self.hb_request.user, binder)
  end

end
