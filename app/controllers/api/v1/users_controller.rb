class Api::V1::UsersController < HomebinderController
  
  skip_before_action :verify_jwt_token, :only => [:reset_password, :update_password, :welcome, :complete, :confirm_password]

  def welcome
    render :json => User.welcome(params)
  end
  
  def invite
    User.invite(self.hb_request, params)
    head :no_content
  rescue => e
    raise BadRequestException.new(e.message)
  end
  
  def change_email
    render :json => User.change_email(self.hb_request.user, params)
  end

  def change_password
    render :json => User.change_password(self.hb_request.user, params)
  end

  def reset_password
    User.reset_password(params)
    head :no_content
  end

  def update_password
    User.update_password(params)
    head :no_content
  end
  
  def find_user_by_email
    render :json => User.find_user_by_email(self.hb_request.user, params)
  end
  
  def confirm_password
    render :json => User.confirm_password(self.hb_request.user, params)
  end
  
  def merge_agents
    Users::MergeAgentsJob.perform_async(params[:old_agent_id], params[:new_agent_id], params[:merger_id])
    head :no_content
  end
end
