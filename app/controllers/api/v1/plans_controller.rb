class Api::V1::PlansController < HomebinderController
  require 'stripe'
  
  PROCESSING_ERROR = "We're having trouble processing your request. Try again later.".freeze
  
  def show
    begin
      plan_id = SubscriptionService.get_plan_id(params[:id])
      render json: Stripe::Plan.retrieve(plan_id) 
 
      rescue Stripe::CardError => e
        # Since it's a decline, Stripe::CardError will be caught
        body = e.json_body
        err  = body[:error]
        
        render json: {message: "#{err[:message].titleize}"}, status: :bad_request 
      rescue Stripe::InvalidRequestError => e
        # Since it's a decline, Stripe::CardError will be caught
        body = e.json_body
        err  = body[:error]
        
        render json: {message: "#{err[:message].titleize}"}, status: :bad_request
      rescue Stripe::AuthenticationError => e
        # Authentication with Stripe's API failed
        # (maybe you changed API keys recently)
        render json: PROCESSING_ERROR, status: :bad_request
      rescue Stripe::APIConnectionError => e
        # Network communication with Stripe failed
        render json: PROCESSING_ERROR, status: :bad_request
      rescue Stripe::StripeError => e
        # Display a very generic error to the user, and maybe send
        # yourself an email
        render json: PROCESSING_ERROR, status: :bad_request
    end
  end
  
end
