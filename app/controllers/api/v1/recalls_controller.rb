class Api::V1::RecallsController < HomebinderController
    
    def download
        self.hb_request.ability.authorize! :read, Recall, :message => "You are not authorized to download recalls."
        RecallService.download_recalls
        head :no_content
    end
    
    def run_service
        Recalls::DownloadJob.perform_async
        head :no_content
    end
end
