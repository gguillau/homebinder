class Api::V1::Partners::PartnersController < HomebinderController
    
    skip_before_action  :verify_jwt_token
    
    rescue_from BadRequestException do |e|
        Partner::Automation::Error.delay(queue: 'abs').build(params, e.message)
        ErrorService.perform_async(e.message, {:task => "Api::V1::Partners::PartnersController.create"})
        render :status => :bad_request, :json => { :message => e.message }
    end
    
    # create a binder through the API
    
    def create
        params[:api_key] = request.env["HTTP_HB_APIKEY"]
        render :status => 201, :json => Partner::Automation::Factory.lookup(params).create_binder(params)
    end
    
    
    # verify a partners credentials work
    
    def verify
        params[:api_key] = request.env["HTTP_HB_APIKEY"]
        Partner::Automation::Factory.lookup(params)
        render :status => 200, :json => {:message => "Test succeeded"}
    end
end