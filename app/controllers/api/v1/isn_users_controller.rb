class Api::V1::IsnUsersController < HomebinderController
  def klass
    "Partner::Configuration::IsnUser".classify.constantize
  end
end
