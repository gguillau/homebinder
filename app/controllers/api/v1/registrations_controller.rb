class Api::V1::RegistrationsController < HomebinderController
    skip_before_action :verify_jwt_token

    def create
        user = User.register(params)
        render status: :ok, json: { token: UserTokenService.create_jwt(user, params) }
    end
end
