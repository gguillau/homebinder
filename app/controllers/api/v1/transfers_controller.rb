class Api::V1::TransfersController < HomebinderController

    def execute
        render :json => klass.execute(self.hb_request, params)
    end
    
    def resend
        render :json => klass.resend(self.hb_request, params)
    end
    
    def cancel
        render :json => klass.cancel(self.hb_request, params)
    end
    
    def klass
        "Binder::Transfer".classify.constantize
    end

end
