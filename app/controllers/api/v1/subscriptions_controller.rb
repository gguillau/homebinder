class Api::V1::SubscriptionsController < HomebinderController
  require 'stripe'
  
  INVALID_CARD = "Invalid credit card".freeze
  PROCESSING_ERROR = "We're having trouble processing your request. Try again later.".freeze

  before_action :set_ability
  
  def set_ability
    @ability = self.hb_request.ability
  end
  
  def index
    if params[:user]
        @subscription_wrappers = Array.new
        binders = @current_user.binders.joins(:subscription)
        binders.each do |b|
          @subscription_wrappers << SubscriptionWrapper.new(b.subscription)
        end
        render :json => @subscription_wrappers
    else
      super
    end
  end

  def update
    @subscription = Subscription.find(params[:id])
    @binder = Binder.find(@subscription.binder_id)
    @action = params[:subscription_action]
    @card = params[:card].nil? ? nil : params[:card]
    @coupon = params[:coupon].nil? ? nil : params[:coupon]

    @ability.authorize! :write, @binder, :message => "You are not authorized to update binder ID: #{@binder.id}."

    begin
        SubscriptionService.update_subscription(@action, @subscription, @card, @coupon)
        render :json => @subscription
      rescue Stripe::CardError => e
        # Since it's a decline, Stripe::CardError will be caught
        body = e.json_body
        err  = body[:error]
          
        #render :json => "#{err[:message].titleize}", :status => :unprocessable_entity
        render :json => [INVALID_CARD], :status => :unprocessable_entity
      rescue Stripe::InvalidRequestError => e
        # Since it's a decline, Stripe::CardError will be caught
        body = e.json_body
        err  = body[:error]
          
        render :json => ["#{err[:message].titleize}"], :status => :unprocessable_entity
      rescue Stripe::AuthenticationError => e
        # Authentication with Stripe's API failed
        # (maybe you changed API keys recently)
        render :json => [PROCESSING_ERROR], :status => :unprocessable_entity 
      rescue Stripe::APIConnectionError => e
        # Network communication with Stripe failed
        render :json => [PROCESSING_ERROR], status => :unprocessable_entity 
      rescue Stripe::StripeError => e
        # Display a very generic error to the user, and maybe send
        # yourself an email
        render :json => [PROCESSING_ERROR], status => :unprocessable_entity
      rescue CardRequiredException => e
        render :json => [e.message], :status => :unprocessable_entity
      rescue UnprocessableException => e
        render :json => [@subscription.errors.first], :status => :unprocessable_entity
    end
  end
end
