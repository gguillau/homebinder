class Api::V1::PaintsController < HomebinderController
  def klass
    "Binder::Paint".classify.constantize
  end
end
