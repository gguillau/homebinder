class Api::V1::PartnerContractorsController < HomebinderController
    # we need to specify the serializer because the controller confuses contractors with
    # contractor templates and thus uses the wrong serializer
    
    def index
        begin
            render :json => klass.index(self.hb_request, params), each_serializer: ::PartnerContractorSerializer
        rescue => e
            raise BadRequestException.new(e.message)
        end
    end
  
    def create
        begin
            render :json => klass.build(self.hb_request, params), serializer: ::PartnerContractorSerializer
        rescue => e
            case e
            when UnprocessableException
                raise UnprocessableException.new(e.resource)
            else
                raise BadRequestException.new(e.message)
            end
        end
    end
    
    def show
        begin
            render :json => klass.show(self.hb_request, params), serializer: ::PartnerContractorSerializer
        rescue => e
            raise BadRequestException.new(e.message)
        end
    end
    
    def update
        begin
            render :json => klass.update(self.hb_request, params), serializer: ::PartnerContractorSerializer
        rescue => e
            case e
            when UnprocessableException
                raise UnprocessableException.new(e.resource)
            else
                raise BadRequestException.new(e.message)
            end
        end
    end
    
    def klass
        "PartnerContractor".classify.constantize
    end
end
