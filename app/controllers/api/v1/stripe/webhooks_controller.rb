class Api::V1::Stripe::WebhooksController < HomebinderController

    skip_before_action  :verify_api_key
    skip_before_action  :init_response
    skip_before_action  :verify_jwt_token
    
    rescue_from JSON::ParserError do |e|
        ErrorService.perform_async(e.message, {:task => "parse_webhook"})
        render :status => :bad_request, :json => { :message => e.message }
    end
    
    rescue_from Stripe::SignatureVerificationError do |e|
        ErrorService.perform_async(e.message, {:task => "parse_webhook"})
        render :status => :bad_request, :json => { :message => e.message }
    end
    
    def update
        Stripe::ParseWebhookJob.perform_async(request)
        head :no_content
    end

end