class Api::V1::UserBindersController < HomebinderController
    skip_before_action :verify_jwt_token, :only => [:find_by_access_token, :find_by_property_address]
    
    def find_by_access_token
        render :json => klass.find_by_access_token(self.hb_request.user, params)
    end
    
    def find_by_property_address
        render :json => klass.find_by_property_address(self.hb_request.user, params)
    end
end
