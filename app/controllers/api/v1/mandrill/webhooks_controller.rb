class Api::V1::Mandrill::WebhooksController < HomebinderController
    
    skip_before_action  :verify_api_key
    skip_before_action  :init_response
    skip_before_action  :verify_jwt_token
    
    def update
        ::Mandrill::ParseWebhookJob.perform_async(params)
        head :no_content
    end

end