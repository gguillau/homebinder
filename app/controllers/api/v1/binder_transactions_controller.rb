class Api::V1::BinderTransactionsController < HomebinderController
    def klass
        "Binder::Transaction".classify.constantize
    end
end
