class Api::V1::ProjectsController < HomebinderController
    def klass
        "Binder::Project".classify.constantize
    end
end
