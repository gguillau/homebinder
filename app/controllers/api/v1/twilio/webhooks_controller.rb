class Api::V1::Twilio::WebhooksController < HomebinderController

    skip_before_action  :verify_api_key
    skip_before_action  :init_response
    skip_before_action  :verify_jwt_token
    
    def create
        TextService::Tool.new.parse(params)
        head :no_content
    end

end