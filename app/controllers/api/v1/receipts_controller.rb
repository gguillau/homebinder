class Api::V1::ReceiptsController < HomebinderController
    def klass
        "Binder::Receipt".classify.constantize
    end
end
