class Api::V1::BinderBrandingsController < HomebinderController
    def klass
        "Binder::BinderBranding".classify.constantize
    end
end