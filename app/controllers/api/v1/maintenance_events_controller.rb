class Api::V1::MaintenanceEventsController < HomebinderController
  def klass
    "Binder::MaintenanceItem::MaintenanceEvent".classify.constantize
  end
end
