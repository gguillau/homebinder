class Api::V1::RecallReportPdfController < HomebinderController
  
  FILE_NAME = "HomeBinder_Recall_Report_%s_%s.pdf".freeze
  FILE_TYPE = "application/pdf".freeze
  FILE_SIZE = "A4".freeze
  
  # TODO - why are we skipping verification here
  skip_before_action :verify_api_key
  skip_before_action :verify_jwt_token
 
  def index
     # check if the user can read the binder
    binder = Binder.find(params[:binder_id])
    
    pdf   = RecallReportPdfService.new(binder).create
    
    date      = Date.today
    pdfname   = FILE_NAME % [binder.name, date]

    send_data pdf.render, filename: pdfname, type: FILE_TYPE, page_size: FILE_SIZE 
    
  end
  
end
