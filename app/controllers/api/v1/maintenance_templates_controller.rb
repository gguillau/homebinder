class Api::V1::MaintenanceTemplatesController < HomebinderController
  def klass
    "Template::Maintenance".classify.constantize
  end

  def send_test_email
    klass.send_test_notification_email(self.hb_request, params[:id])
    head :no_content
  end
end
