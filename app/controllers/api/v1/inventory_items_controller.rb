class Api::V1::InventoryItemsController < HomebinderController
    def klass
    "Binder::InventoryItem".classify.constantize
  end
end
