class Api::V1::PropertyTypesController < HomebinderController
  def index
    render json: PropertyType.all.map { |type| type.name }
  end
end