class Api::V1::ContractorTypesController < HomebinderController
    def klass
        "Contractor::Category".classify.constantize
    end
end
