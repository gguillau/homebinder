class Api::V1::TypeaheadsController < HomebinderController
  
  UPDATED_MESSAGE = "Typeahead updated".freeze
  
  def get_typeaheads
    render :json => Typeahead.get_typeahead_values(current_user, params[:klass], params[:search])
  end
  
  def get_unverified_typeaheads
    render :json => Typeahead.get_unverified_typeahead_values(params[:klass])
  end
  
  def save_unverified_typeahead
    Typeahead.save_unverified_typeahead_value(params[:klass], params[:id], params[:typeahead])
    render :json => {message: UPDATED_MESSAGE}
  end
  
  def remove_unverified_typeahead
    Typeahead.remove_unverified_typeahead_value(params[:klass], params[:id])
    head :no_content
  end
  
end