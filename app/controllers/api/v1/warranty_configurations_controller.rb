class Api::V1::WarrantyConfigurationsController < HomebinderController
    def orders
        render :json => Warranties::Configurations::Service.new(params[:id]).order(current_user, params)
    end
end
