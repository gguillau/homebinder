class Api::V1::BinderTemplatesController < HomebinderController

  def copy
    render json: klass.copy(self.hb_request, params)
  end
end
