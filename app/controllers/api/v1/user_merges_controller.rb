class Api::V1::UserMergesController < HomebinderController
    def revert
        Users::UndoUserMergeJob.perform_async(params[:id])
        head :no_content
    end
end