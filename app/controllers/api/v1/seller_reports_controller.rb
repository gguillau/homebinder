class Api::V1::SellerReportsController < HomebinderController
  skip_before_action :verify_api_key,   :only => [:show]
  skip_before_action :verify_jwt_token, :only => [:show]
end