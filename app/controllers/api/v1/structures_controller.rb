class Api::V1::StructuresController < HomebinderController
    def klass
        "Binder::Structure".classify.constantize
    end
end
