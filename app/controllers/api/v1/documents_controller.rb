class Api::V1::DocumentsController < HomebinderController
  def klass
    "Binder::Document".classify.constantize
  end
end
