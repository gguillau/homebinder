class Api::V1::PendingUsersController < HomebinderController
  skip_before_action :verify_jwt_token

  def get_waiting_binders_by_email
    render :json => User.binders_waiting_by_email(params[:email])
  end

  def get_waiting_binders_by_id
    render :json => User.binders_waiting_by_id(params[:id])
  end

  def optout
    User.opt_out(params[:id])
    head :no_content
  end
end
