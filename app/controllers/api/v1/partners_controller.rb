class Api::V1::PartnersController < HomebinderController

  before_action :create_service, :only => [:update_role, :complete_onboarding, :enable_warranty_account, :widget_exclusions, :warranty_plans, :remove_user, :import_users]
  skip_before_action :verify_jwt_token, :only => [:create, :show_apr_config, :automation]

  def create_service
    partner = Partner.find(params[:id])
    @service = PartnerService.new(self.hb_request.user, partner)
  end

  def complete_onboarding
    render json: @service.complete_onboarding(params)
  end

  def automation
    render json: klass.get_automation_fields(params[:id])
  end
  
  def show_apr_config
    render :json => klass.show_apr_config(params[:id]), serializer: PartnerAprSerializer
  end
  
  def enable_warranty_account
    @service.enable_warranty_account
    head :no_content
  end
  
  def footprints
    Isn::RakeJob.new.refresh(params)
    head :no_content
  end
  
  # analytics operations

  def analytics_overview
    render :json => Partner.get_metrics(params[:partner_id])
  end

  def analytics_logins
    render :json => Partner.get_logins(params[:partner_id])
  end

  def analytics_reminders
    render :json => Partner.get_maintenance_reminders(params[:partner_id]), serializer: nil
  end

  def analytics_agents
    render :json => Partner.get_agent_analytics(params[:partner_id]), serializer: nil
  end

  def download_reminders
    send_data Partner.download_reminders(params[:partner_id]), :type => 'text/csv;'
  end
  
  def download_logins
    send_data Partner.download_logins(params[:partner_id]), :type => 'text/csv;'
  end

  def warranty_plans
    render :json => @service.find_warranty_plans
  end
  
  def widget_exclusions
    render :json => @service.widget_exclusions
  end

  def remove_user
    render :json => @service.remove_user(params)
  end

  def import_users
    render :json => @service.import_users(params)
  end
  
  def update_role
    render :json => @service.update_user_role(params)
  end

end
