class Api::V1::AreasController < HomebinderController
  def klass
    "Binder::Area".classify.constantize
  end
end
