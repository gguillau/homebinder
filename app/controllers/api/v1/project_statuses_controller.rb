class Api::V1::ProjectStatusesController < HomebinderController
  def index
    render json: ProjectStatus.all
  end
end