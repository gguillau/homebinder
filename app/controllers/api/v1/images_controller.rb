class Api::V1::ImagesController < HomebinderController
  def klass
    "Binder::Image".classify.constantize
  end
end
