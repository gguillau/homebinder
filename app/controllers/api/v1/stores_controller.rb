class Api::V1::StoresController < HomebinderController
  def index
    render :json => Store.available(@current_user.id)
  end
end
