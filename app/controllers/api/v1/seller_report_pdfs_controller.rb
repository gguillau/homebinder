class Api::V1::SellerReportPdfsController < HomebinderController
  
  NOT_FOUND = "Seller Report not found.".freeze
  FILE_TYPE = "application/pdf".freeze
  FILE_SIZE = "A4".freeze
  
  skip_before_action :verify_api_key
  skip_before_action :verify_jwt_token
  
  def show    
    # find the report by code
    seller_report = SellerReport.where(:code => params[:id]).first
    
    # check some stuff
    return render :status => 404, :json => {:message => NOT_FOUND} if seller_report.nil?

    if not params[:full]
      pdf = SellerReportPdfService.new(seller_report).create
    else
      pdf = FullSellerReportPdfService.new(seller_report).create
    end

    send_data pdf.render, type: FILE_TYPE, page_size: FILE_SIZE

  end
end