class Api::V1::BuildfaxController < HomebinderController

    def get_permits
        render :json => Buildfax::Client.new(self.hb_request, params[:binderId]).get_permits(params[:binderId])
    end

    def create_permits
        render :json => Buildfax::Client.new(self.hb_request, params[:binderId]).create_permits(params)
    end

end
