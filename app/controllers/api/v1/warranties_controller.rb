class Api::V1::WarrantiesController < HomebinderController
    def download
        Warranties::DownloadJob.perform_async(hb_request.user&.id)
        head :no_content
    end
end
