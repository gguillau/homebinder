class Api::V1::BinderContractorsController < HomebinderController
    # we need to specify the serializer because the controller confuses binder_contractors with
    # contractor templates and thus uses the wrong serializer
    
    def index
        render :json => klass.index(self.hb_request, params), each_serializer: Binder::BinderContractorSerializer
    rescue => e
        raise BadRequestException.new(e.message)
    end
  
    def create
        render :json => klass.build(self.hb_request, params), serializer: Binder::BinderContractorSerializer
    rescue => e
        case e
        when UnprocessableException
            raise UnprocessableException.new(e.resource)
        else
            raise BadRequestException.new(e.message)
        end
    end
    
    def show
        render :json => klass.show(self.hb_request, params), serializer: Binder::BinderContractorSerializer
    rescue => e
        raise BadRequestException.new(e.message)
    end
    
    def update
        render :json => klass.update(self.hb_request, params), serializer: Binder::BinderContractorSerializer
    rescue => e
        case e
        when UnprocessableException
            raise UnprocessableException.new(e.resource)
        else
            raise BadRequestException.new(e.message)
        end
    end
    
    def contact
        render :json => klass.contact(self.hb_request, params), serializer: Binder::BinderContractorSerializer
    rescue => e
        raise BadRequestException.new(e.message)
    end
    
    def klass
      "Binder::BinderContractor".classify.constantize
    end
end
