class Api::V1::DashboardsController < HomebinderController
  
  def owned_by_system
    render :json => klass.index(self.hb_request, params, :owned_by_system)
  end
  
  def owned_by_user
    render :json => klass.index(self.hb_request, params, :owned_by_user)
  end
  
  def owned_by_partner
    render :json => klass.index(self.hb_request, params, :owned_by_partner)
  end
  
  def owned_by_organization
    render :json => klass.index(self.hb_request, params, :owned_by_organization)
  end
  
  def for_binder
    render :json => Dashboards::DashboardLookup.new(self.hb_request.user).for_binder(params)
  end
end