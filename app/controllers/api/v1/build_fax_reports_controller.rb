class Api::V1::BuildFaxReportsController < HomebinderController
    def search
        render :json => BuildFaxReport.search_buildfax(self.hb_request, params)
    end
end
