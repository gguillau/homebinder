require "cancancan"
require "rack-timeout"

class HomebinderController < ApplicationController

    include ActionController::Serialization
    include CanCan::ControllerAdditions
    include BaseController

    serialization_scope :get_serialization_scope

    VERSION = "version".freeze
    MANIFEST = "#{Rails.root}/public/manifest.json".freeze
    API_HEADER_KEY = "HB-APIKey".freeze
    USER_TOKEN_HEADER_KEY = "HB-UserToken".freeze
    VERSION_HEADER_KEY = "HB-Version".freeze

    # verification filters
    before_action :init_response
    before_action :verify_api_key
    before_action :verify_jwt_token
    
    # skip filter for routing errors
    skip_before_action :verify_jwt_token, :only => [:routing_error]

    # accessors
    attr_accessor :current_user
    attr_accessor :hb_request

    # rescuers
    rescue_from CanCan::AccessDenied do |e|
        ErrorService.perform_async(e.message, {:task => "CanCan::AccessDenied"}, self.current_user&.id)
        render :status => :forbidden, :json => {:message => I18n.t(:err_access_denied)}
    end

    rescue_from PermissionDeniedException do |e|
        ErrorService.perform_async(e.message, {:task => "PermissionDeniedException"}, self.current_user&.id)
        render :status => :forbidden, :json => {:message => I18n.t(:err_access_denied)}
    end

    rescue_from Paperclip::Error do |e|
        ErrorService.perform_async(e.message, {:task => "Paperclip::Error"}, self.current_user&.id)
        render :status => :bad_request, :json => { :message => e.message }
    end

    rescue_from ActiveRecord::RecordInvalid do |e|
        ErrorService.perform_async(e.message, {:task => "ActiveRecord::RecordInvalid"}, self.current_user&.id)
        render :status => :bad_request, :json => { :message => e.message }
    end

    rescue_from ActiveRecord::RecordNotFound do |e|
        ErrorService.perform_async(e.message, {:task => "ActiveRecord::RecordNotFound"}, self.current_user&.id)
        render :status => :not_found, :json => { :message => e.message }
    end

    rescue_from SubscriptionError do |e|
        ErrorService.perform_async(e.message, {:task => "SubscriptionError"}, self.current_user&.id)
        render :status => :payment_required, :json => {:message => I18n.t(:err_subscription_expired)}
    end

    rescue_from NotFoundException do |e|
        ErrorService.perform_async(e.message, {:task => "NotFoundException"}, self.current_user&.id)
        render :status => :not_found, :json => { :message => e.message }
    end

    rescue_from BadRequestException do |e|
        ErrorService.perform_async(e.message, {:task => "BadRequestException"}, self.current_user&.id)
        render :status => :bad_request, :json => { :message => e.message }
    end

    rescue_from UnprocessableException do |e|
        # returning the first error from the array so we know what is the root cause of the error
        ErrorService.perform_async(e.resource.errors.full_messages.first, {:task => "UnprocessableException"}, self.current_user&.id)
        render :status => :unprocessable_entity, :json => [e.resource.errors.full_messages.first]
    end

    rescue_from ActionController::ParameterMissing do |e|
        ErrorService.perform_async(e.message, {:task => "ActionController::ParameterMissing"}, self.current_user&.id)
        render :status => :unprocessable_entity, :json => [e.message]
    end

    rescue_from ActionController::UnpermittedParameters do |e|
        ErrorService.perform_async(e.message, {:task => "ActionController::UnpermittedParameters"}, self.current_user&.id)
        render :status => :unprocessable_entity, :json => [e.message]
    end

    rescue_from ActiveModel::UnknownAttributeError do |e|
        ErrorService.perform_async(e.message, {:task => "ActiveModel::UnknownAttributeError"}, self.current_user&.id)
        render :status => :bad_request, :json => { :message => e.message }
    end

    rescue_from Rack::Timeout::RequestTimeoutException do |e|
        ErrorService.perform_async(e.message, {:task => "Rack::Timeout::RequestTimeoutException"}, self.current_user&.id)
        render :status => :request_timeout, :json => { :message => "Your request timed out. Please try again later." }
    end

    rescue_from Rack::Timeout::RequestExpiryError do |e|
        ErrorService.perform_async(e.message, {:task => "Rack::Timeout::RequestExpiryError"}, self.current_user&.id)
        render :status => :request_timeout, :json => { :message => "Your request timed out. Please try again later." }
    end

    rescue_from Rack::Timeout::RequestTimeoutError do |e|
        ErrorService.perform_async(e.message, {:task => "Rack::Timeout::RequestTimeoutError"}, self.current_user&.id)
        render :status => :request_timeout, :json => { :message => "Your request timed out. Please try again later." }
    end

    rescue_from ActiveRecord::StatementInvalid do |e|
        ErrorService.perform_async(e.message, {:task => "ActiveRecord::StatementInvalid"}, self.current_user&.id)
        render :status => :bad_request, :json => { :message => e.message }
    end
    
    rescue_from ActiveRecord::ValueTooLong do |e|
        ErrorService.perform_async(e.message, {:task => "ActiveRecord::ValueTooLong"}, self.current_user&.id)
        render :status => :bad_request, :json => { :message => e.message }
    end

    def routing_error
        render :status => :forbidden, :json => {:message => I18n.t(:err_invalid_route)}
    end

    # for active model serializers
    def default_serializer_options
        {root: false}
    end

    def get_serialization_scope
        if self.current_user.nil?
            return {
                       controller: self.hb_request.controller,
                       action: self.hb_request.action
                   }
        end

        return {
                   current_user: self.hb_request.user,
                   ability: self.hb_request.ability,
                   role: self.hb_request.role,
                   controller: self.hb_request.controller,
                   action: self.hb_request.action
               }
    end

    # verify the JWT token, load the user and abilities
    def verify_jwt_token
        token = request.headers[USER_TOKEN_HEADER_KEY]

        if not token
            token = params[USER_TOKEN_HEADER_KEY]
        end

        if not token
            render :status => 401, :json => {:message => I18n.t(:err_invalid_user_token)}
            return
        end

        begin
            # get the user from the token
            self.current_user = UserTokenService.verify_jwt_token token
            # add the remaining data to the response
            self.hb_request.user = current_user
            self.hb_request.ability = Ability.new(current_user)
            self.hb_request.role = current_user.role
            
            Raven.user_context(current_user.attributes)
        rescue InvalidUserTokenException
            render :status => :unauthorized, :json => {:message => I18n.t(:err_invalid_user_token)}
        end
    end

    # verify there is an api key in the request
    def verify_api_key
        api_key = request.headers[API_HEADER_KEY]
        if (api_key.nil?)
            api_key = params[:api_key]
            if api_key.nil?
                render :status => 403, :json => {:message => I18n.t(:err_invalid_api_key)}
                return
            end
        end

        api_key_object = ApiKey.find_by_key(api_key)
        if api_key_object.nil?
            render :status => 403, :json => {:message => I18n.t(:err_invalid_api_key)}
            return
        end
    end

    def init_response
        self.hb_request = HBRequest.new
        self.hb_request.controller = controller_name,
        self.hb_request.action = action_name
        Raven.extra_context(params: params.to_unsafe_h, controller: controller_name, action: action_name)
    end
end
