class UniqueValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    return if value.nil?
    # check for an existing record with the same name
    cls = record.class
    prop = attribute.to_s
    existing = cls.where("LOWER(#{prop}) = :value".freeze, prop: prop, value: value.downcase).first
    return if existing.nil?
    
    # check if the record is the same one
    if record.id != existing.id
      record.errors[attribute] << I18n.t(:err_not_unique)
    end
  end
  
end