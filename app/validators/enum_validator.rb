class EnumValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)
    # cls (class) is required to lookup the values
    if not options.key?(:cls)
      raise ArgumentError
    end
    
    # get the array of constants
    cls = options[:cls]
    const = cls.constants
    vals = []
    const.each do |con|
      vals << cls.const_get(con)
    end
    
    # check if the provided value is in the list
    if not vals.include?(value)
      record.errors[attribute] << I18n.t(:err_enum_value)
    end
  end
  
end