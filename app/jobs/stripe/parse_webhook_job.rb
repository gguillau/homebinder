class Stripe::ParseWebhookJob
    include Sidekiq::Worker
    sidekiq_options queue: 'default'
    
    def perform(request)
        payload = request.body.read
        sig_header = request.env['HTTP_STRIPE_SIGNATURE']
        endpoint_secret = ENV["STRIPE_WEBHOOK_SECRET_KEY"]

        event = Stripe::Webhook.construct_event(payload, sig_header, endpoint_secret)
    
        return if event.nil?
        # check if event type is charged.failed
        return if event["type"] && event["type"] != "charge.failed"
        # check if we have the customer_id
        return if event["data"].nil? || event["data"]["object"].nil? || event["data"]["object"]["customer"].nil?
        subscription = Subscription.find_by_customer_id(event["data"]["object"]["customer"])
        
        if subscription.present?
            return if subscription.binder.nil? || subscription.binder.owner.nil?
            
            # send email notifying partner of failed payment
            subscription.binder.owner.send_failed_payment_notification
            
            # Downgrade binder if payment fails
            subscription.plan_id = "free"
            subscription.payment_status = "failed"
            subscription.save!
        else
            account = Account.find_by_stripe_customer_id(event["data"]["object"]["customer"])
            return if account.nil? || account.manager.nil?
            # send email notifying partner of failed payment
            account.manager.get_admin.send_failed_payment_notification
        end
    end
end