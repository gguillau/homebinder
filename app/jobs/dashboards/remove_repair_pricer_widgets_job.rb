class Dashboards::RemoveRepairPricerWidgetsJob
    include Sidekiq::Worker
    sidekiq_options queue: 'default'
    
    def perform
        # Get Repair Pricer Widget
        repairPricerWidget = Widget.find_by_name("Repair Pricer Widget")
        
        # Go through non-system and non-default binders
        Dashboard.where(:system => false, :default => false).find_each do |dashboard|
            DashboardWidget.where(:dashboard_id => dashboard.id, :widget_id => repairPricerWidget.id, :created_at => 90.days.ago..Time.now).destroy_all
        end
    end
end