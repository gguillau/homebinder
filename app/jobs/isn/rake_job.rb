# rake task that looks up partners who have ISN users and show_isn_tab set to true

class Isn::RakeJob
    include Sidekiq::Worker
    sidekiq_options queue: 'abs'
    
    def perform
        isn_users = Partner::Configuration::IsnUser.joins(:partner_configuration, :partner_configuration => :partner).where(:partner_configurations => {:show_isn_tab => true})
        isn_users.each do |isn_user|
            hb_request = create_request(isn_user)
            next if hb_request.nil?
            get_footprints(hb_request, isn_user)
        end
    end
    
    def refresh(params)
        # check for required fields
        raise BadRequestException.new "Partner ID required" unless params.present?
        raise BadRequestException.new "Partner ID required" unless params[:id].present?

        partner = Partner.find(params[:id])
        
        isn_users = partner.partner_configuration.isn_users
        
        isn_users.each do |isn_user|
            hb_request = create_request(isn_user)
            hash = Isn::Client.new(hb_request, isn_user.id).get_footprints
            process_footprints(hb_request, isn_user, hash[:footprints])
        end
    end
    
    def create_request(isn_user)
        admin = isn_user.partner_configuration.partner.get_admin
        return nil if admin.nil?
        return HBRequest.new.create_request(admin)
    end
    
    def get_footprints(request = nil, isn_user)
        if request.nil?
            hb_request = create_request(isn_user)
            return if hb_request.nil?
        else
            hb_request = request
        end
        # returns {:footprints => [], :total => 0}
        hash = Isn::Client.new(hb_request, isn_user.id).get_footprints
        process_footprints(hb_request, isn_user, hash[:footprints])
    end
    
    def process_footprints(hb_request, isn_user, footprints)
        footprints.each do |footprint|
            
            # {"id"=>"f6094562-abaa-41f6-b3a5-ff0acd4defff", "name"=>"484-central-st.footprint", 
            # "order"=>"5ccae32e-27cb-49b3-a44f-236664495aec", "oid"=>2277, "address1"=>"484 Central St", 
            # "address2"=>"", "city"=>"Saugus", "state"=>"MA", "zip"=>"01906", "datetime"=>"2016-02-27T08:00:00-05:00", 
            # "client"=>"a41f1923-a75f-4f90-9c0e-0bbafcfdd1ed", "buyersagent"=>"32a34e01-70d9-4c4d-a555-82a145dbdc1f", 
            # "sellersagent"=>"ea511e2f-438b-4a76-81ff-05dee82b3245"}
            
            # {:client=>{:name=>"Harry Potter", :email=>"scott@scottsinspections.com", :home_phone=>"", 
            # :mobile_phone=>""}, :property=>{:address1=>"484 Central St", :address2=>"", :city=>"Saugus", 
            # :state=>"MA", :zip=>"01906"}, :buyersagent=>{:id=>33699, :email=>"susan.mccarthy83782@gmail.com", 
            # :role=>"agent", :sign_in_count=>0, :last_sign_in_at=>nil, 
            # :created_at=>Sun, 26 Mar 2017 17:44:14 EDT -04:00, :accepted_transfer_at=>nil, 
            # :user_profile_attributes=>{:id=>33622, :user_id=>33699, :first_name=>"SusanMcCarthy", :last_name=>nil, 
            # :home_phone=>nil, :mobile_phone=>"+16212440044", :dob=>nil, :sex=>nil, :address_attributes=>nil, 
            # :company=>"The Real Estate Experts LLC", :website=>nil, :bio=>nil, :message=>nil, :head_shot_file=>nil, 
            # :logo_file=>nil, :email=>"susan.mccarthy83782@gmail.com", :monthly_email=>false}, :salesforce_id=>nil, 
            # :completed_onboarding=>false}}
            
            binder_info = Isn::Client.new(hb_request, isn_user.id).get_binder_info(footprint[:id])
            create_binder(binder_info, footprint, isn_user)
        end
    end
    
    def set_homeowner(info)
        full_name = info[:client][:name].split(" ")
        return {
            :first => full_name.first,
            :last => full_name.last,
            :email => info[:client][:email],
            :phone => info[:client][:mobile_phone]
        }
    end
    
    def set_agent(info, key)
        
        return nil if info[key].nil?
        return nil if info[key].empty?
        
        agent = info[key]
        if agent[:user_profile_attributes][:address_attributes].present?
            property = {
                :address1 => agent[:user_profile_attributes][:address_attributes][:address1],
                :city => agent[:user_profile_attributes][:address_attributes][:city],
                :state => agent[:user_profile_attributes][:address_attributes][:state],
                :country => agent[:user_profile_attributes][:address_attributes][:country]
            }
        else
            property = {
                :address1 => nil,
                :city => nil,
                :state => nil,
                :country => nil
            }
        end
        return {
            :name => agent[:user_profile_attributes][:company],
            :first => agent[:user_profile_attributes][:first_name],
            :last => agent[:user_profile_attributes][:last_name],
            :email => agent[:email],
            :phone => agent[:user_profile_attributes][:mobile_phone],
            :address1 => property[:address1],
            :city => property[:city],
            :state => property[:state],
            :country => property[:country]
        }
    end
    
    def set_property(info)
        return {
            :address => info[:property][:address1],
            :address2 => info[:property][:address2],
            :city => info[:property][:city],
            :state => info[:property][:state],
            :country => "US",
            :zip => info[:property][:zip]
        }
    end
    
    def create_binder(info, footprint, isn_user)
        # get the partner info
        partner = isn_user.partner_configuration.partner
        configuration = isn_user.partner_configuration
        
        # setup homeowner, property and buyer agent
        client = set_homeowner(info)
        buyer_agent = set_agent(info, :buyersagent)
        seller_agent = set_agent(info, :sellersagent)
        property = set_property(info)
        
        # set up the params
        parameters = {
            :key => partner.partner_key, 
            :client => client, 
            :property => property, 
            :binder_template_id => configuration.default_binder_template_id, 
            :buyer_agent => buyer_agent,
            :seller_agent => seller_agent,
            :property_photo => nil,
            :documents => [],
            :inspection_date => footprint[:datetime]
        }
        
        begin
            Partner::Automation::API::Service.create_binder(parameters)
        rescue => e
            Partner::Automation::Error.build(parameters, e.message)
            ErrorService.perform_async(e.message, {:task => "create_binder"})
        end
    end
end