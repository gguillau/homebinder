module Tags
    class SetResourcesTagsJob
        include Sidekiq::Worker
        sidekiq_options queue: 'default'
        
        def perform(model_name, id, tags = nil)
            model = model_name.constantize
            resource = model.find_by_id(id)
            return if resource.nil?
            
            if tags.nil? or tags.length == 0
                resource.tags.delete_all
                return
            end
    
            # get the current tags saved on the resource
            currentTags = resource.tags
    
            # get the tags that should be removed
            toBeRemoved = currentTags.select {|ct| not tags.any? {|t| t["tag"] == ct.tag}}
    
            # get the tags that need to be added
            toBeAdded = tags.select {|ntag| not currentTags.any? {|ct| ct.tag == ntag["tag"]}}
            
            # get the tags that need to be updated
            toBeUpdated = tags.select {|ntag| currentTags.any? {|ct| ct.tag == ntag["tag"]}}
    
            # add the new tags to the resource
            toBeAdded.each do |tba|
                tba.delete("tag_scope")
                tba = ActionController::Parameters.new(tba)
                resource.tags.new(tba.permit!).save!
            end
    
            # delete the tags to be removed
            toBeRemoved.each do |tbr|
                resource.tags.delete(tbr)
            end
    
            #update the tags with new attributes if any
            if toBeUpdated.length > 0
                tags.each do |tbu|
                    if tbu["id"]
                        tag = Tag.find(tbu["id"])
                        tbu.delete("tag_scope")
                        tbu = ActionController::Parameters.new(tbu)
                        tag.update_attributes!(tbu.permit!)
                    end
                end
            end
        end
    end
end