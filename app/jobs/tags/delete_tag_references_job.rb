module Tags
    class DeleteTagReferencesJob
        include Sidekiq::Worker
        sidekiq_options queue: 'default'
        
        def perform(tagName)
            Tag.where(:tag => tagName).destroy_all
        end
    end
end