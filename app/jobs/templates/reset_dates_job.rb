class Templates::ResetDatesJob
  include Sidekiq::Worker
  sidekiq_options queue: 'non_critical'
  
  def perform
    maintenance_templates = Template::Maintenance.all
    maintenance_templates.each do |template|
      if template.due_date and template.due_date < Date.today
        if check_frequency(template.frequency)
          frequency = get_frequency(template)
          template.due_date = set_date(template.due_date, frequency)
          if not template.save
            ErrorService.perform_async(template.errors.full_messages.first, {:task => "reset_dates"})
          end
        end
      end
    end
  end
  
  def get_frequency(template)
    frequency = {}
    case template.frequency
    when Binder::MaintenanceItem::Cycle::ANNUAL
        frequency[:interval] = 1
        frequency[:cycle] = Binder::MaintenanceItem::Cycle::YEARS
    when Binder::MaintenanceItem::Cycle::SEMI_ANNUAL
        frequency[:interval] = 6
        frequency[:cycle] = Binder::MaintenanceItem::Cycle::MONTHS
    when Binder::MaintenanceItem::Cycle::QUARTERLY
        frequency[:interval] = 3
        frequency[:cycle] = Binder::MaintenanceItem::Cycle::MONTHS
    when Binder::MaintenanceItem::Cycle::MONTHLY
        frequency[:interval] = 1
        frequency[:cycle] = Binder::MaintenanceItem::Cycle::MONTHS
    when Binder::MaintenanceItem::Cycle::EVERY_OTHER
        frequency[:interval] = 2
        frequency[:cycle] = Binder::MaintenanceItem::Cycle::YEARS
    when Binder::MaintenanceItem::Cycle::EVERY_THREE
        frequency[:interval] = 3
        frequency[:cycle] = Binder::MaintenanceItem::Cycle::YEARS
    when Binder::MaintenanceItem::Cycle::EVERY_FOUR
        frequency[:interval] = 4
        frequency[:cycle] = Binder::MaintenanceItem::Cycle::YEARS
    when Binder::MaintenanceItem::Cycle::EVERY_FIVE
        frequency[:interval] = 5
        frequency[:cycle] = Binder::MaintenanceItem::Cycle::YEARS
    when Binder::MaintenanceItem::Cycle::EVERY_TEN
        frequency[:interval] = 10
        frequency[:cycle] = Binder::MaintenanceItem::Cycle::YEARS
    when Binder::MaintenanceItem::Cycle::EVERY_FOURTY
        frequency[:interval] = 40
        frequency[:cycle] = Binder::MaintenanceItem::Cycle::YEARS
    end
    return frequency
  end
  
  def check_frequency(frequency)
    if frequency.downcase === Binder::MaintenanceItem::Cycle::AS_NEEDED.downcase 
      return false
    elsif frequency.downcase === Binder::MaintenanceItem::Cycle::ONCE.downcase
      return false
    end
    return true
  end
  
  def set_date(date, frequency)
    if frequency[:cycle] === Binder::MaintenanceItem::Cycle::MONTHS
      return date + frequency[:interval].months
    else
      return date + frequency[:interval].years
    end
  end
  
end
