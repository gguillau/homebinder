class Templates::Document::UpdateBinderDocumentsJob
    include Sidekiq::Worker
    sidekiq_options queue: 'default'
    
    def perform(id)
        template_document = Template::Document.find_by_id(id)
        return if template_document.nil?
        Binder::Document.where(:library_source_id => template_document.id).find_each do |binder_document|
            binder_document.file = template_document.file
            if not binder_document.save
                ErrorService.perform_async(binder_document.errors.full_messages.first, {:task => "update_binder_documents"})
            end
        end
    end
end
