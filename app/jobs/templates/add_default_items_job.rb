class Templates::AddDefaultItemsJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    def perform(id)
        template = BinderTemplate.find_by_id(id)
        return if template.nil?
        
        template.add_default_items
    end
end