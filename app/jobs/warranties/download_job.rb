require 'csv'

class Warranties::DownloadJob
    include Sidekiq::Worker
    sidekiq_options queue: 'abs'
    
    #   Warranty ID, Request Date, Inspection Date, Binder ID, Client Name
    #   Address, Phone, Email, Inspector Company, Partner ID, Inspector Acct 
    #   Warranty Option, Warranty Price, Warranty Company, Status
    
    def perform(user_id)
        # check for required fields
        raise BadRequestException.new "User ID Required" unless user_id.present?
        user = User.find_by_id(user_id)
        return if user.nil?
        
        # check if the user can download
        raise BadRequestException.new "User must be an admin" unless user.is_admin?
        
        # get all the warranties from past 45 days
        warranties = Warranty.where(:created_at => 45.days.ago..Time.now)
        
        # set the CSV headers
        headers = [
            "warranty_id", "warranty_request_date", "warranty_plan", "inspection_date", "binder_id", "homeowner_first_name", "homeowner_last_name",
            "homeowner_phone", "homeowner_email", "property_address1", "property_city", "property_state", "property_zip", 
            "partner_company_name", "partner_id", "warranty_account_number", "warranty_price", "warranty_company", "warranty_status"
        ]
        
        # generate the CSV file
        csv_file = CSV.generate({}) do |csv|
            
            csv << headers
            
            warranties.each do |warranty|
                
                # get the values that we need
                binder = warranty.binder
                next if binder.nil?
                inspection_transaction = binder.transactions.where(:transaction_type => "buy_side_inspection").first
                next if inspection_transaction.nil?
                transaction = warranty.binder_transaction
                next if transaction.nil?
                partner = transaction.partner_binder_transaction.partner
                next if partner.nil?
                next if partner.warranty_configuration.nil?
                
                # set the attributes we need
                attributes = [
                    warranty.id, warranty.created_at.to_date, warranty.warranty_plan.name, inspection_transaction.transaction_date.to_date, binder.id, warranty.client_first,
                    warranty.client_last, warranty.client_phone, warranty.client_email, warranty.address.address1, warranty.address.city, 
                    warranty.address.state, warranty.address.zip, partner.name, partner.id, partner.warranty_configuration.account_number, 
                    transaction.transaction_cost, partner.warranty_configuration.warranty_company.name, warranty.status
                ]
                
                # add the attributes to the csv file
                csv << attributes
            end
        end
        
        # encode the file
        encoded_file = Base64.encode64(csv_file)
    
        # send the report via email
        WarrantyMailer.send_warranty_report(user.id, encoded_file).deliver_later
    end
end