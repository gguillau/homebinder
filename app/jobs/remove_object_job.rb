class RemoveObjectJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    def perform(model_name = "", record_id = -1)
        return if model_name.blank?
        return if record_id < 0
        
        model = model_name.constantize
        record = model.find_by_id(record_id)
        return if record.nil?
        record.destroy
    end
end