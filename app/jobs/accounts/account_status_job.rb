module Accounts
  class AccountStatusJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    def perform
      # find all active free trial accounts
      Account.where(Account::AccountStatus::WHERE, Account::AccountStatus::ACTIVE, Account::AccountSubType::FREE_TRIAL, Date.today)
          .update_all(:account_status => Account::AccountStatus::EXPIRED)
    end
  end
end