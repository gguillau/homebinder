require "soapforce"

class Partners::UpdateSalesforceAccountsJob
    include Sidekiq::Worker
    sidekiq_options queue: 'analytics'
    
    # rake task that will run nightly. It updates all the partner accounts in Salesforce.
    # Data is going from HomeBinder to Salesforce.
    
    def perform
        ::Partner.find_each do |partner|
            begin
                Partners::UpdateSalesforceAccountJob.perform_async(partner.id)
            rescue => e
                # notify new relic if the save was unsuccessful
                ErrorService.perform_async(e.message, {:partner_id => partner.id})
            end
        end
    end
end