class Partners::UpdateBinderContractorsJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    def perform(partner_contractor_id)
        partner_contractor = ::PartnerContractor.find_by_id(partner_contractor_id)
        return if partner_contractor.nil?
        
        Binder::BinderContractor.joins(:contractor => :user).where("users.sign_in_count < 1").where(:library_source_id => partner_contractor.template_contractor_ids).update_all(:contact => partner_contractor.name, :details => partner_contractor.notes)
        Binder::BinderContractor.joins(:contractor).where(:contractors => {:user_id => nil}).where(:library_source_id => partner_contractor.template_contractor_ids).update_all(:contact => partner_contractor.name, :details => partner_contractor.notes)
    end
end