class Partners::AccountSummaryJob 
  include Sidekiq::Worker
  sidekiq_options queue: 'critical'
  
  def perform(user_id)
    account_keys = Account.attribute_names - ["id", "name", "created_at", "updated_at"]
    # get all the users who have accounts
    partners = Partner.joins(:account)
    # generate the CSV file
    csv_file = CSV.generate({}) do |csv|
      csv << Partner.attribute_names + account_keys + Partners::EmailsJob::METRIC_KEYS
      partners.each do |partner|
        # check if partner has an account
        account = partner.account
        if account.present?
          summary = Hash.new
          begin
            metrics = Partner.get_weekly_metrics(partner.id)
            summary = summary.merge(get_fields(Partner.attribute_names, partner))
            summary = summary.merge(get_fields(account_keys, account))
            summary = summary.merge(metrics.except(:user, :reminders_sent, :reminders_sent_past_month, :reminders_sent_past_ninety))
            csv << summary.values
          rescue
            # just move on to the next partner if an error occurs 
            next
          end
        end
      end
    end
    
    # encode the file
    encoded_file = Base64.encode64(csv_file)
    
    KpiMailer.send_account_summary(user_id, encoded_file).deliver_later
  end
  
  def get_fields(fields, object)
    hash = {}
    fields.each do |field| hash[field.to_sym] = object[field.to_sym] end
    return hash
  end
end
