require "open-uri"

class Partners::RemovalJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    def perform(partner_id)
        partner = Partner.find_by_id(partner_id)
        return if partner.nil?
        
        client = Intercom::HomeBinderClient.new
        intercom = client.intercom
        
        # post an event so we are aware the partner was deleted
        # from our system
        metadata = client.metadata_for_partner(partner)

        begin
            intercom.events.create(
                event_name: "partner-deleted",
                email: partner.email,
                created_at: Time.now,
                metadata: metadata
            )
        rescue => e
            ErrorService.perform_async(e.message, {partner_id: partner.id, event: "partner-deleted"})
        end

        # destroy all templates the default binder template contains, if it exists
        default_template = partner.partner_configuration.default_binder_template
        
        if !default_template.nil?
            default_template.maintenance_templates.destroy_all
            default_template.appliance_templates.destroy_all
            default_template.document_templates.destroy_all
            default_template.contractor_templates.destroy_all
            default_template.delete
        end

        # destroy all binder templates
        partner.partner_configuration.binder_templates.destroy_all

        # destroy isn_users and config
        partner.partner_configuration.isn_users.destroy_all
        partner.partner_configuration.delete

        # destroy each partner_user
        partner.partner_users.each do |partner_user|
            partner_user.delete
        end

        # destroy the partner
        partner.reload
        partner.destroy
    end
end
