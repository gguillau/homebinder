require "soapforce"

class Partners::SwitchBinderJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    def perform(old_partner_id, new_partner_id, binder_id)
        old_partner = Partner.find_by_id(old_partner_id)
        return if old_partner.nil?
        
        new_partner = Partner.find_by_id(new_partner_id)
        return if new_partner.nil?
        
        binder = Binder.find_by_id(binder_id)
        return if binder.nil?
        
        new_admin = PartnerUser.where(:role => "admin", :partner_id => new_partner_id).order(:id).first
        return if new_admin.nil?
        
        new_admin_id = new_admin.user_id
        old_partner_user_ids = PartnerUser.joins(:user).where(:users => {:role => old_partner.partner_type}, :partner_id => old_partner_id).pluck(:user_id)
        
        # update owner
        UserBinder.where(:user_id => old_partner_user_ids, :binder_id => binder_id).update_all(:user_id => new_admin_id)
        
        # update partner binders
        PartnerBinder.where(:binder_id => binder_id, :partner_id => old_partner_id).update_all(:partner_id => new_partner_id)
        
        # update partner transactions
        PartnerBinderTransaction.joins(:binder_transaction => :binder).where(:binders => {:id => binder_id}, :partner_id => old_partner_id).update_all(:partner_id => new_partner_id)
        
        # update transfer/share
        Binder::Transfer.where(:binder_id => binder_id, :sender_id => old_partner_user_ids).update_all(:sender_id => new_admin_id)
        Binder::Share.where(:binder_id => binder_id, :shared_by_id => old_partner_user_ids).update_all(:shared_by_id => new_admin_id)
        
        # update branding
        Binder::BinderBranding.where(:binder_id => binder_id, :partner_id => old_partner_id).update_all(:user_branding_id => new_partner.partner_configuration.default_user_branding_id)
        
        template = new_partner.partner_configuration.default_binder_template
        return if template.nil?
        
        binder.details = new_partner.partner_configuration.default_binder_template.transfer_note
        binder.save!
        
        admin = new_partner.get_admin
        template = new_partner.partner_configuration.default_binder_template
        
        # replace binder items from old template to template
        Binder::Appliance.where(:binder_id => binder_id).destroy_all
        Binder::BinderContractor.where(:binder_id => binder_id).destroy_all
        Binder::MaintenanceItem.where(:binder_id => binder_id).destroy_all
        
        # add the transfer note to the binder details
        Partner::Automation::Service.delay(queue: 'abs').add_transfer_note(binder.id, template.id)

        # add appliances to binder
        Partner::Automation::Service.delay(queue: 'abs').add_appliances([], binder.id, template.id, admin.id)

        # add maintenance items to binder
        Partner::Automation::Service.delay(queue: 'abs').add_maintenance_items([], binder.id, template.id, admin.id)
        
        # add contractors to binder
        Partner::Automation::Service.delay(queue: 'abs').add_contractors([], binder.id, template.id, admin.id)

        # add projects to binder
        Partner::Automation::Service.delay(queue: 'abs').add_projects([], binder.id)

        # add documents to binder
        Partner::Automation::Service.delay(queue: 'abs').add_template_docs(binder.id, template.id)
        
        Partner::Automation::Service.delay(queue: 'abs').add_partner_user_as_home_pro(admin.id, new_partner.id, binder.id)
    end
end