require 'recurrence'

class Partners::EmailsJob 
  include Sidekiq::Worker
  sidekiq_options queue: 'analytics'
  
  METRIC_KEYS = ["total_binders".freeze, "total_binders_past_week".freeze, "total_binders_past_month".freeze, "total_binders_not_transferred".freeze]
  
  def perform
    every_other_monday = Recurrence.new(:every => :week, :on => :monday, :interval => 2, :starts => '2019-06-20')
    # check if today's date is included in the recurrence
    date = Date.today
    if every_other_monday.include?(date)
      Partner.all.each do |partner|
        # check if partner has an account and it is active
        account = partner.account
        if verify_account(account)
          # get metrics
          begin
            metrics = Partner.get_biweekly_metrics(partner.id)
          rescue => e
            ErrorService.perform_async(e.message, {partner_id: partner.id})
            next
          end
          # send email with metrics
          KpiMailer.send_weekly_metrics(partner.id, metrics).deliver_later
        end
      end
    end
  end
  
  def verify_account(account)
    if account and account.account_status == Account::AccountStatus::ACTIVE and
      account.subscription_amount_cents > 0 || account.transaction_amount_cents > 0
      return true
    else
      return false
    end
  end
end
