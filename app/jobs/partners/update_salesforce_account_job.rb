require "soapforce"

class Partners::UpdateSalesforceAccountJob
    include Sidekiq::Worker
    sidekiq_options queue: 'default'
    
    def perform(partner_id)
        partner = Partner.find(partner_id)

        # update account in salesforce
        Salesforce::Partner.new.update_partner(partner)
    end
end