require "soapforce"

class Partners::CreateSalesforceAccountJob
    include Sidekiq::Worker
    sidekiq_options queue: 'default'
    
    def perform(partner_id, survey_data)
        partner = Partner.find(partner_id)
        
        # create a new account in salesforce
        Salesforce::Partner.new.new_partner(partner, OpenStruct.new(survey_data))
    end
end