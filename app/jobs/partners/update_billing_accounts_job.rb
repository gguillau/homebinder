require "soapforce"

class Partners::UpdateBillingAccountsJob
    include Sidekiq::Worker
    sidekiq_options queue: 'analytics'
    
    ACCOUNT = "Account".freeze
    JOINS = "INNER JOIN accounts on accounts.manager_id = partners.id".freeze
    BILLING_SAVE_ERROR = "Unable to save billing account: %s".freeze
    
    attr_accessor :client
    
    # rake task that will run nightly. It updates all the partner billing accounts in HomeBinder.
    # Data is going from Salesforce to HomeBinder.
    
    def perform
        # we initialize the salesforce class
        initialize_salesforce
        
        # cycle through all partners
        partners = ::Partner.joins(JOINS)
        partners.each do |partner|
            # find account by partner_id and we're only updating billing information at the moment
            salesforce_accounts = client.find_where(ACCOUNT, Partner_ID__c: partner.id)
            # get the first account in the array
            salesforce_account = salesforce_accounts.first
            # check if there is an object
            if not salesforce_account.nil?
                # update the HomeBinder billing account
                update_partner(partner, salesforce_account)
            end
        end
    end
    
    def initialize_salesforce
        self.client = Salesforce::Base.new.client
    end
    
    # upates an individual partners billing account information
    
    def update_partner(partner, salesforce_account)
        # get the billing account
        billing_account = partner.account
        # check if billing account exists
        return unless billing_account.present?

        # fields we can update
        billing_account.name = salesforce_account[:name]
        billing_account.account_status = format_value(billing_account.account_status, salesforce_account[:account_status__c])
        billing_account.account_sub_status = format_value(billing_account.account_sub_status, salesforce_account[:account_sub_status__c])
        billing_account.account_sub_type = format_value(billing_account.account_sub_type, salesforce_account[:account_sub_type__c])
        billing_account.account_type = format_value(billing_account.account_type, salesforce_account[:account_type__c])
        
        billing_account.billing_frequency = format_value(billing_account.billing_frequency, salesforce_account[:account_billing_frequency__c])
        billing_account.trial_expiration = format_value(billing_account.trial_expiration, salesforce_account[:account_trial_expiration__c])
        billing_account.billing_activation = format_value(billing_account.billing_activation, salesforce_account[:account_billing_activation__c])
        billing_account.subscription_amount = format_value(billing_account.subscription_amount, salesforce_account[:account_subscription_amount__c])
        billing_account.transaction_amount = format_value(billing_account.transaction_amount, salesforce_account[:account_transaction_amount__c])
        billing_account.payment_type = format_value(billing_account.payment_type, salesforce_account[:account_payment_type__c])
        
        # get the address or else create a new one
        address = partner.address.present? ? partner.address : partner.build_address
        
        # address
        address.address1 = format_value(address.address1, salesforce_account[:billing_street])
        address.city = format_value(address.city, salesforce_account[:billing_city])
        address.state = format_value(address.state, salesforce_account[:billing_state])
        address.country = format_value(address.country, salesforce_account[:billing_country])
        address.zip = format_value(address.zip, salesforce_account[:billing_postal_code])
        
        # save the billing account
        if not billing_account.save
            ErrorService.perform_async(BILLING_SAVE_ERROR % billing_account.errors.full_messages.first, {partner_id: partner.id})
        end
        
        # save the address
        if not address.save
            ErrorService.perform_async(BILLING_SAVE_ERROR % address.errors.full_messages.first, {partner_id: partner.id})
        end
    end
    
    # check to make sure salesforce fields are not nil
    
    def format_value(hb_field, sf_field)
        # check if nil
        if sf_field.to_s.empty?
            # if nil then we return the current value in HomeBinder
            return hb_field
        else
            # check if salesforce field is a date
            if not sf_field.is_a?(Date)
                sf_field = sf_field.to_s
                # replace any white spaces with an underscore
                sf_field.gsub! " ", "_"
                # return the value in lower case characters
                return sf_field.downcase
            else
                # if value is a date field we just return the value without any modifications
                return sf_field
            end
        end
    end
end