require "open-uri"

class Partners::RepairPricerStatusJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    def perform
        ::RepairPricerReport.where("status = 1 OR status = 2 OR status = 3 OR status = 4 OR status = 5").find_each do |repair_pricer_report|
            begin
                response = ::RepairPricer::Client.new.get_report(repair_pricer_report.report_id) 
                next if response.nil?
                next if response.dig("status").nil?
                
                case response["status"]
                when "10"
                    repair_pricer_report.status = 1
                    repair_pricer_report.save!
                when "20"
                    repair_pricer_report.status = 2
                    repair_pricer_report.save!
                when "30"
                    repair_pricer_report.status = 3
                    repair_pricer_report.save!
                when "40"
                    repair_pricer_report.status = 4
                    repair_pricer_report.save!
                when "50"
                    repair_pricer_report.status = 5
                    repair_pricer_report.save!
                    
                    download_report(repair_pricer_report.report_id, response)
                when "99"
                    repair_pricer_report.status = 7
                    repair_pricer_report.save!
                end
            rescue => e
                ErrorService.perform_async(e.message, {task: "Partners::RepairPricer.download_report"})
                next
            end
        end
    end
    
    def download_report(id, response)
        repair_pricer_report = ::RepairPricerReport.find_by_report_id(id)
        return if repair_pricer_report.nil?
        return if response["reporturl"].blank?
        
        begin
            report_url = URI.parse(response["reporturl"])
            repair_pricer_report.repair_report = report_url
            repair_pricer_report.delivery_date = Date.today
            repair_pricer_report.status = 6
            repair_pricer_report.save!
            
            download_home_history_report(repair_pricer_report.report_id, response)
            download_pool_report(repair_pricer_report.report_id, response)
            download_report_items(repair_pricer_report.report_id)
            add_document_to_binder(repair_pricer_report.binder_id, response["reporturl"])
        rescue => e
            ErrorService.perform_async(e.message, {repair_pricer_id: id, url: response["reporturl"], task: "Partners::RepairPricer.download_report"})
            return
        end
    end
    
    def download_pool_report(id, response)
        repair_pricer_report = ::RepairPricerReport.find_by_report_id(id)
        return if repair_pricer_report.nil?
        return if response["reporturlpool"].blank?
        
        begin
            repair_pricer_report.pool_report = URI.parse(response["reporturlpool"])
            repair_pricer_report.save!
        rescue => e
            ErrorService.perform_async(e.message, {repair_pricer_id: id, url: response["reporturlpool"], task: "Partners::RepairPricer.download_pool_report"})
            return
        end
        
        add_document_to_binder(repair_pricer_report.binder_id, response["reporturlpool"])
    end
    
    def download_home_history_report(id, response)
        repair_pricer_report = ::RepairPricerReport.find_by_report_id(id)
        return if repair_pricer_report.nil?
        return if response["reporturlhh"].blank?
        
        begin
            repair_pricer_report.home_history_report = URI.parse(response["reporturlhh"])
            repair_pricer_report.save!
        rescue => e
            ErrorService.perform_async(e.message, {repair_pricer_id: id, url: response["reporturlhh"], task: "Partners::RepairPricer.download_home_history_report"})
            return
        end
        
        add_document_to_binder(repair_pricer_report.binder_id, response["reporturlhh"])
    end
    
    def download_report_items(id)
        repair_pricer_report = ::RepairPricerReport.find_by_report_id(id)
        return if repair_pricer_report.nil?
        return if repair_pricer_report.repair_pricer_report_findings.exists?
        begin
            response = ::RepairPricer::Client.new.download_report(repair_pricer_report.report_id) 
            return if response.nil?
            return if response.dig("data").nil?
            
            response["data"].each do |finding|
    
                item = finding["Items"]
                repair_pricer_report.repair_pricer_report_findings.create(
                    :name => item["Item"],
                    :pg => item["Page"],
                    :code => item["Code"],
                    :contractor_type => item["Contractor"] || "Handyman",
                    :action => item["Action"],
                    :defective_price => item["DPrice"],
                    :cosmetic_price => item["CPrice"],
                    :complete_price => item["BPrice"],
                    :potential_price => item["PPrice"],
                )
            end
            
            RepairPricerMailer.find_a_pro(repair_pricer_report.id).deliver_later(wait: 45.days)
        rescue => e
            ErrorService.perform_async(e.message, {task: "Partners::RepairPricer.download_report_items"})
        end
    end
    
    def add_document_to_binder(binder_id, url)
        return if binder_id.nil?
        return if url.nil?
        binder = Binder.find_by_id(binder_id)
        return if binder.nil?
        
        begin
            document = binder.documents.new
            document.file = URI.parse(url)
            document.save!
        rescue => e
            ErrorService.perform_async(e.message, {url: url, binder_id: binder_id, task: "Partners::RepairPricer.add_document_to_binder"})
        end
    end
end