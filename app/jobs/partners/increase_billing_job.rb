require "soapforce"

class Partners::IncreaseBillingJob
    include Sidekiq::Worker
    sidekiq_options queue: 'default'
    
    def perform
        # find accounts where subscription is 1600
        Account.where.not(:stripe_customer_id => nil).where(:account_status => "active").where("subscription_amount_cents < 1700").find_each do |account|
            partner = account.manager
            next if partner.nil?
            if partner.binders.where(:created_at => 1.year.ago..Time.now).count > 49
                begin
                    customer = Stripe::Customer.retrieve(account.stripe_customer_id)
                    next if customer.nil?
                    next if customer.subscription.nil?
                    next if customer.subscription.plan.nil?
                    
                    account.subscription_amount_cents = 4900
                    account.save!
                    
                    Partners::UpdateSalesforceAccountJob.perform_async(partner.id)
                    
                    # cancel subscription
                    customer.subscription.delete
                    # add new plan
                    plan = Stripe::Plan.retrieve("standard49inspectorsub")
                    # create new subscription
                    Stripe::Subscription.create(
                        :customer => customer.id,
                        :items => [
                            {
                                :plan => plan.id,
                            }
                        ]
                    )
                
                rescue => e
                    # notify new relic if the save was unsuccessful
                    ErrorService.perform_async(e.message, {:partner_id => partner.id})
                end
            end
        end
    end
end