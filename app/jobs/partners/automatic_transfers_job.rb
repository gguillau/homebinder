require "soapforce"

class Partners::AutomaticTransfersJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    def perform
        Partner::Automation::Service::Access.new.transfer_binders
    end
end