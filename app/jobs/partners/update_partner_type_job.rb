require "soapforce"

class Partners::UpdatePartnerTypeJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    def perform(id, type)
        # get the partner
        partner = Partner.find(id)
        
        # update all partner users with the same type
        users = partner.users.where(:role => type)
        
        # expire the users token
        Session.where(:user_id => users.ids).destroy_all
        
        # set the new role
        users.update_all(:role => partner.partner_type)
    end
end