require "open-uri"

class Partners::BrokerMintJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    def perform
        Partner.joins(:partner_configuration).where.not(:partner_configurations => {:broker_mint_api_key => nil}).find_each do |partner|
            begin
                api_key = partner.partner_configuration.broker_mint_api_key
                transactions = Brokermint::Client.new.get_transactions(api_key)
                transactions.each do |transaction|
                    retrieve_transaction(api_key, transaction["id"])
                end
            rescue => e
                ErrorService.perform_async(e.message, {task: "Partners::BrokerMint.check_transaction_status"})
                next
            end
        end
    end
    
    def retrieve_transaction(api_key, transaction_id)
        begin
            return if api_key.nil?
            return if transaction_id.nil?
            
            partner = Partner.joins(:partner_configuration).where(:partner_configurations => {:broker_mint_api_key => api_key}).first
            return if partner.nil?
            
            transaction = Brokermint::Client.new.get_transaction(api_key, transaction_id)
            return if transaction.empty?
            return if transaction["status"] != "closed"

            participants = Brokermint::Client.new.get_transaction_participants(api_key, transaction_id)
            return if participants.empty?
            
            buyer = participants.select { |parti| parti["role"] === "Buyer" }.first
            return if buyer.nil?
            return if buyer.dig("type").nil?
            return if buyer.dig("type") != "Contact"
            
            homeowner = Brokermint::Client.new.get_contact(api_key, buyer["id"])
            
            create_binder(partner, homeowner, transaction)
        rescue => e
            ErrorService.perform_async(e.message, {task: "Partners::BrokerMint.retrieve_transaction"})
            return
        end
    end
    
    def create_binder(partner, buyer, transaction)
        return if partner.nil?
        return if buyer.nil?
        return if transaction.nil?
        
        property = {
            :address => transaction["address"],
            :address2 => "",
            :city => transaction["city"],
            :state => transaction["state"],
            :zip => transaction["zip"],
            :country => "US",
        }
        homeowner = {
            :first => buyer["first_name"],
            :last => buyer["last_name"],
            :email => buyer["email"],
            :phone => buyer["phone"] || buyer["mobile_phone"]
        }
        
        # get the partner info
        configuration = partner.partner_configuration
        
        # set up the params
        parameters = {
            :key => partner.partner_key, 
            :client => homeowner, 
            :property => property, 
            :binder_template_id => configuration.default_binder_template_id, 
            :buyer_agent => {},
            :seller_agent => {},
            :property_photo => nil,
            :documents => []
        }
        
        begin
            Partner::Automation::API::Service.create_binder(parameters)
        rescue => e
            Partner::Automation::Error.build(parameters, e.message)
            ErrorService.perform_async(e.message, {:task => "create_binder"})
        end
    end
end