require "open-uri"

class Partners::WorkaroundJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    def perform
        InspectionReportJob.where.not(:status => 4).where.not(:status => 5).find_each do |job|
            begin
                check_job_status(job.id)
            rescue => e
                ErrorService.perform_async(e.message, {task: "Partners::Workaround.check_jobs_status"})
                next
            end
        end
    end

    def check_job_status(id)
        begin
            job = InspectionReportJob.find(id)
            inspection_job = WorkaroundService.new.get_call(job.id)

            if inspection_job.dig("homeowner_first_name")
                job.status = 4
                job.save!
            else
                return
            end

            if job.status_complete?
                data = inspection_job

                # set up the params
                parameters = {
                    :key => job.partner.partner_key,
                    :client => {
                        :first => data["homeowner_first_name"],
                        :last => data["homeowner_last_name"],
                        :email => data["homeowner_email"],
                        :phone => data["homeowner_phone"]
                    },
                    :property => {
                        :address => data["address1"],
                        :address2 => data["address2"],
                        :city => data["city"],
                        :state => data["state"],
                        :country => data["country"],
                        :zip => data["zip"]
                    },
                    :binder_template_id => job.partner.partner_configuration.default_binder_template_id,
                    :buyer_agent => {},
                    :documents => [{:url => job.inspection_report.expiring_cloud_front_url(1.hour), :name => "Home Inspection Report.pdf", :document_type => "Home Inspection Report"}],
                    :inspection_date => data["inspection_date"]
                }
                params = ActionController::Parameters.new(parameters)
                binder = Partner::Automation::API::Service.create_binder(params)
                job.binder_id = binder[:id]
                job.status = "binder_created"
                job.save!
            end
        rescue => e
            ErrorService.perform_async(e.message, {task: "Partners::Workaround.check_job_status"})
        end
    end
end
