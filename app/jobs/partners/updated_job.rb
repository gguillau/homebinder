require "open-uri"

class Partners::UpdatedJob
    include Sidekiq::Worker
    sidekiq_options queue: 'analytics'
    
    def perform(partner_id)
        partner = Partner.find_by_id(partner_id)
        return if partner.nil?

        client = Intercom::HomeBinderClient.new
        intercom = client.intercom

        # create the additional custom attributes for the partner
        metadata = client.metadata_for_partner(partner)
        event = "partner-created"
        begin
            # create an intercom company
            intercom.companies.create(
                company_id: partner.id.to_s,
                name: partner.name,
                custom_attributes: metadata
            )
        rescue => e
            ErrorService.perform_async(e.message, {partner_id: partner.id, event: event})
        end

        return unless User.where(email: partner.email).exists?

        begin
            # post event about the partner being created
            intercom.events.create(
                event_name: event,
                email: partner.email,
                created_at: Time.now
            )
        rescue => e
            ErrorService.perform_async(e.message, {partner_id: partner.id, event: event})
        end
    end
end
