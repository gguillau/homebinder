require "soapforce"

class Partners::UpdateDefaultLocaleJob
    include Sidekiq::Worker
    sidekiq_options queue: 'non_critical'
    
    def perform(id)
        partner = Partner.find_by_id(id)
        return if partner.nil?
        return if partner.partner_configuration.nil?
        
        UserProfile.where(:user_id => partner.user_ids).update_all(:default_locale => partner.partner_configuration.default_locale)
    end
end