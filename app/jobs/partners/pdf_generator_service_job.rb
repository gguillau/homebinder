require 'httparty'

class Partners::PdfGeneratorServiceJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    def perform(partner_id)
        # Get partner
        partner = Partner.find_by_id(partner_id)
        return if partner.nil?
        
        if partner.partner_type === "inspector"
            generate_pdf("Homeowner Flyer", "Flyer for homeowners", partner, 54315)
            generate_pdf("Homeowner Flyer RP", "Flyer for homeowners about Repair Pricer", partner, 54318)
            generate_pdf("Buyer Agent Flyer", "Flyer for buyer agents", partner, 54316)
            generate_pdf("Buyer Agent Flyer RP", "Flyer for buyer agents about Repair Pricer", partner, 54314)
        end
        
        # only send email to partners we have flyers for
        if partner.partner_type === "inspector"
            AccountMailer.send_marketing_materials_notification(partner).deliver_later
        end
    end
    
    def generate_pdf(name, description, partner, template_id)
        pdf = pdf_url(partner, template_id)
        
        # Check if a document with this template already exists and update it if it does
        MarketingResource.where(:document_template_id => template_id).each do |resource|
            if PartnerResource.where(:marketing_resource_id => resource.id, :partner_id => partner.id).exists?
                resource.update(:document => URI.parse(pdf))
                return
            end
        end
        
        marketing_resource = MarketingResource.create!(:name => name, :description => description, :document => URI.parse(pdf), :document_template_id => template_id, :resource_type => "document", :user_type => "inspector")
        PartnerResource.create!(:marketing_resource_id => marketing_resource.id, :partner_id => partner.id)
    end
    
    def pdf_url(partner, template_id)
        template = "templates/" + template_id.to_s + "/output"
        
        # Set up the data and convert to JSON
        data = [{
            'company' => partner.name,
            'email' => partner.email,
            'website' => partner.website,
            'phone' => partner.phone_formatted,
            'logo' => partner.logo.expiring_cloud_front_url
        }].to_json
        
        authentication = key + template + workspace
        signature = OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha256'), secret, authentication)
        
        url = base_url + template

        response = HTTParty.get(URI.escape(url), {
            body: data,
            query: {
                'format' => 'pdf',
                'output' => 'url'
            },
            headers: {      
                'X-Auth-Key' => key,
                'X-Auth-Workspace' => workspace,
                'X-Auth-Signature' => signature,
                'Accept' => 'application/json',
                'Content-Type' => 'application/json; charset=utf-8'
            }
        })

        if response.code === 200
            return response["response"]
        else
            raise BadRequestException.new response.body
        end
    end

    def workspace
        if Host.path === Host::PRODUCTION_HOST
            raise BadRequestException.new "PDF Generator Production Workspace Required" unless ENV["PDF_GENERATOR_PRODUCTION_WORKSPACE"].present?
            ENV["PDF_GENERATOR_PRODUCTION_WORKSPACE"].to_s
        else
            raise BadRequestException.new "PDF Generator Test Workspace Required" unless ENV["PDF_GENERATOR_TEST_WORKSPACE"].present?
            ENV["PDF_GENERATOR_TEST_WORKSPACE"].to_s
        end
    end
    
    def base_url
        if Host.path === Host::PRODUCTION_HOST
            raise BadRequestException.new "PDF Generator Production base URL Required" unless ENV["PDF_GENERATOR_PRODUCTION_BASE_URL"].present?
            ENV["PDF_GENERATOR_PRODUCTION_BASE_URL"].to_s
        else
            raise BadRequestException.new "PDF Generator Test base URL Required" unless ENV["PDF_GENERATOR_TEST_BASE_URL"].present?
            ENV["PDF_GENERATOR_TEST_BASE_URL"].to_s
        end
    end
    
    def secret
        if Host.path === Host::PRODUCTION_HOST
            raise BadRequestException.new "PDF Generator Production Secret Required" unless ENV["PDF_GENERATOR_PRODUCTION_SECRET"].present?
            ENV["PDF_GENERATOR_PRODUCTION_SECRET"].to_s
        else
            raise BadRequestException.new "PDF Generator Test Secret Required" unless ENV["PDF_GENERATOR_TEST_SECRET"].present?
            ENV["PDF_GENERATOR_TEST_SECRET"].to_s
        end
    end
    
    def key
        if Host.path === Host::PRODUCTION_HOST
            raise BadRequestException.new "PDF Generator Production Key Required" unless ENV["PDF_GENERATOR_PRODUCTION_KEY"].present?
            ENV["PDF_GENERATOR_PRODUCTION_KEY"].to_s
        else
            raise BadRequestException.new "PDF Generator Test Key Required" unless ENV["PDF_GENERATOR_TEST_KEY"].present?
            ENV["PDF_GENERATOR_TEST_KEY"].to_s
        end
    end
end