require 'csv'

class AnnualPropertyReviews::ScheduleJob
    include Sidekiq::Worker
    sidekiq_options queue: 'non_critical'
    
    # find all APRs scheduled for tomororw
    # and send an email to the homeowner
    
    def perform
        aprs = AnnualPropertyReview.where(:scheduled_for => Date.tomorrow)
        aprs.find_each do |apr|
            AnnualPropertyReviewMailer.reminder(apr&.id).deliver_later
        end
    end
end