class Recalls::DownloadJob
    include Sidekiq::Worker
    sidekiq_options queue: 'low'
    
    # checks all binders in the system for any new recalls
    def perform
        if Date.today.friday?
            # query for the list of recalls
            preload_recalls

            # check each binder
            Binder.find_each do |binder|
                # only do recall checks on paid binders
                unless binder.subscription.nil?
                    if binder.subscription.plan_id != "free"
                        binder.appliances.each do |appliance|
                            check_for_appliance_recall appliance
                        end
                    end
                end
            end
        end
    end
    
    # determines which version of the lookup to use and loads
    # the recalls into @recalls
    def preload_recalls
        if Rails.configuration.recall_check[:source] == "homebinder"
            @recalls = Recall.where(:verified => true, :ignore => false)
        elsif Rails.configuration.recall_check[:source] == "cpsc"
            # try and get the list of recalls from the database.
            # if it doesn't exist or wasn't refreshed today we will
            # get a new list
            cr = ConsumerRecall.find_by_id(1)

            if cr.nil? or (cr.refreshed_at + Time.new.utc_offset).to_date != Date.today
                # get the new list of recalls from the CPSC
                @recalls = CPSCService.get_all_recalls
                # if the ConsumerRecall record doesn't exist create one
                if cr.nil?
                    cr = ConsumerRecall.new
                end
                # store the list of recalls and time we refreshed the list at
                cr.list = JSON.generate(@recalls)
                cr.refreshed_at = DateTime.now
                cr.save!
            else
                @recalls = JSON.parse(cr.list)
            end
        end
    end

    # check all the appliances in a binder for a recall
    def check_binder_for_recall(binder)
        # only upgraded binders support recall checks
        if binder.subscription.plan_id != "free"
            # if @recalls isn't set preload it
            if @recalls.nil?
                preload_recalls
            end

            # check each appliance in the binder
            binder.appliances.each do |appliance|
                check_for_appliance_recall appliance
            end
        end
    end

    # calls the appropriate search method and handles the matches
    def check_for_appliance_recall(appliance)
        if @recalls.nil?
            preload_recalls
        end

        if Rails.configuration.recall_check[:source] == "homebinder"
            # search our local database for any recalls
            matches = search_for_recalls({
                :manufacturer => appliance.manufacturer,
                :model => appliance.model,
                :appliance_type => appliance.appliance_type
            })

            # link each recall to the appliance
            matches.each do |match|
                if Binder::Appliance::ApplianceRecall.where(appliance_id: appliance.id, recall_id: match[:recall].id).count == 0
                    ar = Binder::Appliance::ApplianceRecall.new(
                        appliance_id: appliance.id,
                        recall_id: match[:recall].id,
                        strength: match[:strength],
                        status: "unverified",
                        notifications: 0
                    )
                    ar.save!
                end
            end
        else
            # search the cpsc directly for any recalls
            matches = search_all_recalls({
                :manufacturer => appliance.manufacturer,
                :model => appliance.model,
                :appliance_type => appliance.appliance_type
            })

            matches.each do |match|
                # if the recall isn't in our database add it
                recall = Recall.find_by_number(match[:recall]["RecallNumber"])
                if recall.nil?
                    recall = Recall.new(number: match[:recall]["RecallNumber"],
                                        recall_date: match[:recall]["RecallDate"],
                                        details: JSON.generate(match[:recall]),
                                        verified: false,
                                        ignore: false)
                    recall.save!
                end
                # link the recall to the appliance
                if Binder::Appliance::ApplianceRecall.where(appliance_id: appliance.id, recall_id: recall.id).count == 0
                    ar = Binder::Appliance::ApplianceRecall.new(
                        appliance_id: appliance.id,
                        recall_id: recall.id,
                        strength: match[:strength],
                        status: "unverified",
                        notifications: 0
                    )
                    ar.save!
                end
            end
        end

        # flag date of this check
        appliance.last_recall_check = DateTime.now
        appliance.save!

        # send back the new matches
        return matches
    end

    ################################################
    # Original code to check for recalls
    def search_for_recalls(opts)
        min_strength = Rails.configuration.recall_check[:minimum_strength]
        matches = []
        strength = 0

        # set the search values
        manufacturer = opts[:manufacturer].nil? ? nil : opts[:manufacturer].downcase
        model = opts[:model].nil? ? nil : opts[:model].downcase
        appliance_type = opts[:appliance_type].nil? ? nil : opts[:appliance_type].downcase

        # search through all verified non ignore recalls
        @recalls.each do |recall|
            strength = 0

            unless recall.details.nil?
                recall_details = JSON.parse(recall.details)

                # check for a manufacturer match either with manufacturer or within the product name
                # this could lead to lots of false positives as manufacturer could be the full name
                # as opposed to abbreviated. General Electric opposed to GE.
                if manufacturer_match?(recall_details, manufacturer)
                    strength += 2
                end

                if appliance_type_match?(recall_details, appliance_type)
                    strength += 1
                end

                if model_match?(recall, model)
                    strength += 4
                end

                # if the strenth of the match is at least 4 include the recall
                if strength >= min_strength
                    matches.push({ :strength => strength, :recall => recall })
                end
            end
        end

        # send back the matches
        return matches
    end

    def search_all_recalls(opts)
        min_strength = Rails.configuration.recall_check[:minimum_strength]
        matches = []
        strength = 0

        # set the search values
        manufacturer = opts[:manufacturer].nil? ? nil : opts[:manufacturer].downcase
        model = opts[:model].nil? ? nil : opts[:model].downcase
        appliance_type = opts[:appliance_type].nil? ? nil : opts[:appliance_type].downcase

        @recalls.each do |recall|
            strength = 0

            # check for a manufacturer match either with manufacturer or within the product name
            # this could lead to lots of false positives as manufacturer could be the full name
            # as opposed to abbreviated. General Electric opposed to GE.
            if manufacturer_match?(recall, manufacturer)
                strength += 2
            end

            if appliance_type_match?(recall, appliance_type)
                strength += 1
            end

            if find_model?(recall, model)
                strength += 4
            end

            # if the strenth of the match is at least 4 include the recall
            if strength >= min_strength
                matches.push({ :strength => strength, :recall => recall })
            end
        end

        # send back the matches
        return matches
    end

    def recall_check(opts)
        # load the recalls prior to the search
        preload_recalls

        # deal with the response
        if Rails.configuration.recall_check[:source] == "homebinder"
            recalls = []
            matches = search_for_recalls(opts)
            matches.each do |match|
                recalls << { :strength => match[:strength], :recall => JSON.parse(match[:recall].details) }
            end
            return recalls
        else
            return search_all_recalls(opts)
        end
    end

    def manufacturer_match?(recall_details, manufacturer)
        return false if manufacturer.nil? or manufacturer.empty?

        manufacturer = manufacturer.downcase

        recall_details["Manufacturers"].each do |m|
            return true if find_in_paragraph(m["Name"], manufacturer)
        end

        return true if find_in_paragraph(recall_details["Title"], manufacturer)
        return true if find_in_paragraph(recall_details["Description"], manufacturer)

        recall_details["Products"].each do |product|
            return true if find_in_paragraph(product["Name"], manufacturer)
            return true if find_in_paragraph(product["Description"], manufacturer)
        end

        return false
    end

    def appliance_type_match?(recall_details, appliance_type)
        return false if appliance_type.nil? or appliance_type.empty?

        appliance_type = appliance_type.downcase

        return true if find_in_paragraph(recall_details["Title"], appliance_type)
        return true if find_in_paragraph(recall_details["Description"], appliance_type)

        recall_details["Products"].each do |product|
            return true if find_in_paragraph(product["Name"], appliance_type)
            return true if find_in_paragraph(product["Description"], appliance_type)
            return true if find_in_paragraph(product["Type"], appliance_type)
        end

        return false
    end

    def find_model?(recall, model)
        return false if model.nil? or model.empty?

        min_word_length = Rails.configuration.recall_check[:word_length]
        model = model.downcase

        words = recall["Description"].split unless recall["Description"].nil?
        words = [] if recall["Description"].nil?
        
        words.each do |word|
            word = word.strip.downcase
            if (word.start_with?(model) or model.start_with?(word)) and word.length >= min_word_length
                return true
            end
        end
        return false
    end

    ##################################################
    # old model match function
    def model_match?(recall, model)
        return false if model.nil? or model.empty?
        return false if recall.models.nil? or recall.models.empty?

        model = model.downcase
        rmodels = recall.models.downcase.split(',')

        rmodels.each do |rmodel|
            rmodel = rmodel.strip
            if rmodel.start_with?(model) or model.start_with?(rmodel)
                return true
            end
        end
        return false
    end

    private

    def find_in_paragraph(paragraph, value)
        return false if paragraph.nil? or paragraph.empty?
        return false if value.nil? or value.empty?

        paragraph = paragraph.downcase
        value = value.downcase.strip
        index = paragraph.index(value)

        if index
            before = nil
            after = nil

            if index > 0
                before = paragraph[index - 1]
            end

            if index + value.length < paragraph.length
                after = paragraph[index + value.length]
            end

            if (before.nil? or before.match(/\s/) or before.match(/\(/)) and
               (after.nil? or after.match(/\s/) or after.match(/\)/))
                return true
            end
        end
        return false
    end
end