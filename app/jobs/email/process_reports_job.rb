require 'gmail'

class Email::ProcessReportsJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    SUBJECT = "new inspection report request".freeze
    TEST_SUBJECT = "new test inspection report request".freeze
    GOOGLE_INSPECTION_REPORT_CREDENTIALS = "GOOGLE_INSPECTION_REPORT_CREDENTIALS".freeze
    GOOGLE_INSPECTION_REPORT_PASSWORD = "GOOGLE_INSPECTION_REPORT_PASSWORD".freeze
    PROCESSED = "Processed Emails".freeze

    def perform(inbox = "Inbox")
        # log in to gmail
        gmail = Gmail.new(username, password)
        gmail.mailbox(inbox).emails.each do |email|
            next if email.from.nil? || email.from.empty?
            process_email(email)
            email.move_to(PROCESSED)
        end
        # log out
        gmail.logout
    end

    def process_email(email)
        return if email.from.nil? || email.from.empty?
        return if !Partner.where(:email => email.from[0]).exists?
        partner = Partner.where(:email => email.from[0]).first

        begin
            files = Array.new
            email.attachments.each do |attachment|
                if (attachment.content_type.start_with?("application"))
                    file = StringIO.new(attachment.decoded)
                    file.class.class_eval { attr_accessor :original_filename, :content_type }
                    file.original_filename = attachment.filename
                    file.content_type = attachment.mime_type
                    files.push(file)
                end
            end

            if !files.empty?
                file = files[0]
                values = file.instance_values
                job = InspectionReportJob.create({
                    :inspection_report => file,
                    :inspection_report_file_name => values["original_filename"],
                    :partner_id => partner.id
                })
                response = WorkaroundService.new.post_call(job.id, job.inspection_report.expiring_cloud_front_url(1.month))
                job.job_id = response["extractid"]
                job.status = 1
                job.save!
            end
        rescue => e
            ErrorService.perform_async(e.message, {email: email.from[0]})
        end
    end

    private

    def username
        raise BadRequestException.new "Google Username Required" unless ENV[GOOGLE_INSPECTION_REPORT_CREDENTIALS].present?
        ENV[GOOGLE_INSPECTION_REPORT_CREDENTIALS]
    end

    def password
        raise BadRequestException.new "Google Password Required" unless ENV[GOOGLE_INSPECTION_REPORT_PASSWORD].present?
        ENV[GOOGLE_INSPECTION_REPORT_PASSWORD]
    end
end
