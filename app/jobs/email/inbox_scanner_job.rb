require 'gmail'

class Email::InboxScannerJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    SUBJECT = "new binder request".freeze
    TEST_SUBJECT = "new test binder request".freeze
    GOOGLE_BINDER_CREDENTIALS = "GOOGLE_BINDER_CREDENTIALS".freeze
    GOOGLE_BINDER_PASSWORD = "GOOGLE_BINDER_PASSWORD".freeze
    PROCESSED = "Processed Emails".freeze

    def perform(inbox = "Inbox")
        # log in to gmail
        gmail = Gmail.new(username, password)
        gmail.mailbox(inbox).emails.each do |email|
            next unless email.subject.present?

            # check email subject line
            if email.subject.downcase === get_subject
                begin
                    params = nil
                    # sometimes the parsing may cause an error so we wrap the call in a BEGIN/RESCUE
                    params = Partner::Automation::EmailParser.new.parse(email)
                rescue => e
                    # temporary fix until we move files to AWS
                    ErrorService.perform_async(e.message, {email: email.from[0]})
                    next
                end

                if !params.nil?
                    process_email(params, email)
                    email.move_to(PROCESSED)
                end
            end
        end
        # log out
        gmail.logout
    end

    def process_email(params, email)
        begin
            # begin processing the request
            params[:method] = "email"
            Partner::Automation::Factory.email_lookup(params).create_binder(params)

            # analytics
            EventService.perform_async({:event_name => "email_request", :event_type => "abs", :partner_key => params[:key]})
        rescue => e
            # create new error object
            Partner::Automation::Error.build(params, e.message)
            ErrorService.perform_async(e.message, {email: email.from[0]})
        end
    end

    def re_run(inbox = "Emails with Errors")
        # log in to gmail
        gmail = Gmail.new(username, password)
        gmail.mailbox(inbox).emails.each do |email|
            next unless email.subject.present?
            # check email subject line
            if email.subject.downcase === get_subject
                begin
                    params = nil
                    # sometimes the parsing may cause an error so we wrap the call in a BEGIN/RESCUE
                    params = Partner::Automation::EmailParser.new.parse(email)
                rescue => e
                    # temporary fix until we move files to AWS
                    ErrorService.perform_async(e.message, {email: email.from[0]})
                    next
                end

                if !params.nil?
                    re_process_email(params, email)
                end
            end
        end
        # log out
        gmail.logout
    end

    def re_process_email(params, email)
        begin
            Partner::Automation::Service.reprocess_emails(params)
        rescue => e
            ErrorService.perform_async(e.message, {email: email.from[0]})
        end
    end

    private

    def get_subject
        if Host.path === Host::PRODUCTION_HOST
            return SUBJECT
        else
            return TEST_SUBJECT
        end
    end

    def username
        raise BadRequestException.new "Google Username Required" unless ENV[GOOGLE_BINDER_CREDENTIALS].present?
        ENV[GOOGLE_BINDER_CREDENTIALS]
    end

    def password
        raise BadRequestException.new "Google Password Required" unless ENV[GOOGLE_BINDER_PASSWORD].present?
        ENV[GOOGLE_BINDER_PASSWORD]
    end

end
