module Shares
    class CreateJob
        include Sidekiq::Worker
        sidekiq_options queue: 'default'
        
        def perform(share_id, resend = false)
            share = Binder::Share.find_by_id(share_id)
            return if share.nil?
            
            # check if user is a partner
            partner = find_partner(share)
    
            if partner.present?
                share_from_partner(partner, share)
                Binders::BinderAnalytics.delay(:queue => "analytics").share(share.id, partner.id)
            else
                ShareMailer.notify_email(share.id).deliver_later
            end
    
            # call analytics
            Users::UserAnalytics.delay(:queue => "analytics").share_received(share.id)
        end
        
        def share_from_partner(partner, share)
            binder = share.binder
            case partner.partner_type
            when "inspector"
                ShareMailer.notify_email_from_inspector(share.id, partner.id).deliver_later
                TransferService.new(binder.transfers.first.id).send_agent_email
            when "broker"
                ShareMailer.notify_email_from_broker(share.id, partner.id).deliver_later
            else
                ShareMailer.notify_email_from_partner(share.id, partner.id).deliver_later
            end
        end
    
        def find_partner(share)
            from_user = share.sender
            binder = share.binder
            if from_user.role != "homeowner" and from_user.role != "admin"
                partner_binder = PartnerBinder.where(:binder_id => binder.id).first
                return partner_binder.partner unless partner_binder.nil?
            end
          return nil
        end
    end
end