require 'date'

class Agents::EmailJob
    include Sidekiq::Worker
    sidekiq_options queue: 'analytics'
    
    def perform
        today       = Date.today
        email_date  = Date.today.beginning_of_month

        # only send the email on the first day of the month
        if today === email_date
            # only send a monthly email to agents who have signed in and have been added to at least one binder as a buyer agent
            User.where("role = 'agent' AND sign_in_count > 0").find_each do |agent|
                if UserBinder.where(:user_id => agent.id, :role => "buyer_agent").exists?
                    KpiMailer.send_monthly_agent_metrics(agent.id).deliver_later
                    
                    # analytics
                    EventService.perform_async({:event_name => "monthly_agent_mailer", :event_type => "email", :user_id => agent.id, :user_created_at => agent.created_at, :user_role => agent.role})
                end
            end
        end
    end
end