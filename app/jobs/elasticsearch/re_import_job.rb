class Elasticsearch::ReImportJob
    include Sidekiq::Worker
    sidekiq_options queue: 'elasticsearch'
    
    def perform(model)
        begin
            klass = model.classify.constantize
            klass.__elasticsearch__.delete_index!
            klass.__elasticsearch__.create_index!
            klass.__elasticsearch__.import batch_size: 1000
        rescue => e
            ErrorService.perform_async(e.message, {:task => "Elasticsearch::Build.re_import", :klass => model})
        end
    end
end