class Elasticsearch::BuildJob
    include Sidekiq::Worker
    sidekiq_options queue: 'elasticsearch'
    
    def perform
        begin
            client = Elasticsearch::IndexJob.new.client
            indices = client.cat.indices h: ["index"], format: "json"
            indices = indices.map {|index| index["index"]}
            
            # we want to make sure to NEVER delete the events index
            indices.delete("events")
            
            # we want to make sure to NEVER delete the events index
            abort if indices.include?("events")
            
            indices.each do |index|
                # we want to make sure to NEVER delete the events index
                next if index === "events"
                client.indices.delete index: index
            end
            Elasticsearch::ImportJob.perform_async
        rescue => e
            ErrorService.perform_async(e.message, {:task => "rebuild"})
        end
    end
end