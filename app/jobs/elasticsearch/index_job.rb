class Elasticsearch::IndexJob
    include Sidekiq::Worker
    sidekiq_options queue: 'elasticsearch'
    
    def perform(model_name, record_id, operation)
        return if Rails.env.test?
        model = model_name.constantize
        begin
            case operation
            when 'index'
                record = model.find_by_id(record_id)
                return if record.nil?
                record.__elasticsearch__.index_document
            when 'update'
                record = model.find_by_id(record_id)
                return if record.nil?
                record.__elasticsearch__.update_document
            when 'delete'
                client.delete index: model.index_name, id: record_id, type: model.__elasticsearch__.document_type, ignore: 404
            else raise ArgumentError, "Unknown operation '#{operation}'"
            end
        rescue => e
            ErrorService.perform_async(e.message, {:operation => operation, :id => record_id, type: model.__elasticsearch__.document_type})
        end
    end
    
    def client
        if ENV["AWS_ENABLED"] === 'true'
            Elasticsearch::Client.new host: host, logger: Logger.new("#{Rails.root}/log/elasticsearch.log")
        else
            Elasticsearch::Client.new host: host, user: user, password: password, logger: Logger.new("#{Rails.root}/log/elasticsearch.log")
        end
    end    
    
    def host
        ENV["ELASTICSEARCH_HOST_URL"] || '0.0.0.0:9200'
    end

    def user
        ENV["ELASTICSEARCH_USER"] || ""
    end
    
    def password
        ENV["ELASTICSEARCH_PASSWORD"] || ""
    end
end