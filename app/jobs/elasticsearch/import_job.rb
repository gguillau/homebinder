class Elasticsearch::ImportJob
    include Sidekiq::Worker
    sidekiq_options queue: 'elasticsearch'
    
    def perform
        begin
            # needed for development environment
            Rails.application.eager_load!
            
            # get all the objects in our DB and import them
            models = ActiveRecord::Base.descendants
            
            models.each do |model|
                next if not model.respond_to? :__elasticsearch__
                next if model.__elasticsearch__.client.indices.exists? index: model.index_name
            
                # create the index
                model.__elasticsearch__.create_index!
            
                # import
                model.import
            end
        rescue => e
            ErrorService.perform_async(e.message, {:task => "import"})
        end
    end
end