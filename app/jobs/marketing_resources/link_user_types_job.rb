module MarketingResources
  class LinkUserTypesJob
    include Sidekiq::Worker
    sidekiq_options queue: 'default'
    
    def perform(organization_id, resource_id)
      # get the org
      organization = Organization.find_by_id(organization_id)
      resource = MarketingResource.find_by_id(resource_id)
      return if organization.nil?
      return if resource.nil?
      
      # get the org partners
      partners = organization.partners.where(:partner_type => resource.user_type)
      # create the resource for the partners
      partners.each do |partner|
          PartnerResource.create!(:partner_id => partner.id, :marketing_resource_id => resource.id)
      end
    end
  end
end