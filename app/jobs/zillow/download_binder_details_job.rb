class Zillow::DownloadBinderDetailsJob
    include Sidekiq::Worker
    include Numbers
    sidekiq_options queue: 'abs'
    
    URL = "url".freeze
    ZPID = "zpid".freeze
    ZERO = "0".freeze
    CODE = "code".freeze
    COUNT = "count".freeze
    IMAGE = "image".freeze
    IMAGES = "images".freeze
    ROOMS = "rooms".freeze
    MESSAGE = "message".freeze
    RESULT = "result".freeze
    RESULTS = "results".freeze
    RESPONSE = "response".freeze
    BEDROOMS = "bedrooms".freeze
    BATHROOMS = "bathrooms".freeze
    YEAR_BUILT = "yearBuilt".freeze
    BINDER_INFO = "binder info".freeze
    EDITED_FACTS = "editedFacts".freeze
    SEARCH_RESULTS = "searchresults".freeze
    UPDATED_DETAILS = "updatedPropertyDetails".freeze
    QUERY_STRING = "address=%s&citystatezip=%s+%s".freeze
    RESULT_URL = "http://www.zillow.com/webservice/GetDeepSearchResults.htm?zws-id=%s&".freeze
    ZILLOW_URL = "http://www.zillow.com/webservice/GetUpdatedPropertyDetails.htm?zws-id=%s&zpid=%s".freeze
    ZILLOW_ID = "ZILLOW_ID".freeze
    
    attr_accessor :binder
    
    def perform(binder_id)
        self.binder = Binder.find(binder_id)
        
        # check if there are any images
        images = get_property_photo
        create_images(images)
        
        # create default rooms/structures
        binder.structures.create(:name => "House")
        binder.areas.create(:name => "Kitchen")
        binder.areas.create(:name => "Living Room")
    end
    
    def get_property_photo
        obj = Zillow::Request.new.call(build_url)
        if obj.nil?
            return []
        end

        code = obj[SEARCH_RESULTS][MESSAGE][CODE]
        if code === ZERO
            return get_binder_info(obj[SEARCH_RESULTS][RESPONSE][RESULTS][RESULT])
        else
            return []
        end
    end
    
    def create_images(images)
        images.each do |image|
            # create image
            begin
                image = Binder::Image.new(binder_id: binder.id, file: URI.parse(image))
                if not image.save
                    ErrorService.perform_async(image.errors.full_messages.first, {:binder_id => binder.id}, nil)
                    next
                else
                    # check if we need to add image as hero image
                    if binder.hero_image_id.blank?
                        binder.hero_image_id = image.id
                        if not binder.save
                            ErrorService.perform_async(binder.errors.full_messages.first, {:binder_id => binder.id}, nil)
                            next
                        end
                    end
                end
            rescue => e
                ErrorService.perform_async(e.message, {:binder_id => binder.id}, nil)
                next
            end
        end
    end
    
    def build_url
        address =  binder.property.address1.gsub(/\s+/, '+')
        city =  binder.property.city
        state = binder.property.state
        request_url = RESULT_URL % zillow_id
        return request_url + QUERY_STRING % [address, city, state]
    end

    def get_binder_info(result)
        if result.length < 1
            return []
        elsif result.is_a?(Array)
            result = result.first
        end
        
        set_additional_binder_property_fields(result)
        
        obj = get_updated_property_info(result[ZPID])
        code = obj[UPDATED_DETAILS][MESSAGE][CODE]

        if code === ZERO
            get_images(obj[UPDATED_DETAILS][RESPONSE])
        else
            return []
        end
    end

    def get_updated_property_info(zpid)
        zillow_url = ZILLOW_URL % [zillow_id, zpid]
        Zillow::Request.new.call(zillow_url)
    end

    def get_images(result)
        return [] if result[IMAGES].nil?
        
        images = result[IMAGES]
        if images[COUNT].to_i === 1
            return [images[IMAGE][URL]]
        elsif images[COUNT].to_i > 0
            return images[IMAGE][URL]
        else
            return []
        end
    end

    def zillow_id
        raise BadRequestException.new "Zillow ID Required" unless ENV[ZILLOW_ID].present?
        ENV[ZILLOW_ID]
    end
    
    def set_additional_binder_property_fields(result)
        price = result.dig("lastSoldPrice", "__content__")
        self.binder.property.purchase_date = Date.strptime(result.dig("lastSoldDate"),"%m/%d/%Y") if result.dig("lastSoldDate").present?
        self.binder.property.purchase_price = !price.blank? ? price.to_i : 0
        self.binder.property.year_built = result.dig("yearBuilt")
        self.binder.property.sq_ft = result.dig("finishedSqFt")
        self.binder.property.save!
        
        if bathrooms = result.dig("bathrooms")
            half = bathrooms.to_f - bathrooms.to_i
            
            if half > 0
                binder.areas.create(:name => "Half Bathroom")
            end
            
            bathrooms.to_i.times do |i|
                number = i + 1
                case number
                when 1
                    binder.areas.create(:name => "Master Bathroom")
                else
                    binder.areas.create(:name => "#{ordinal(number)} Bathroom")
                end
            end
        end
        
        if bedrooms = result.dig("bedrooms")
            bedrooms.to_i.times do |i|
                number = i + 1
                case number
                when 1
                    binder.areas.create(:name => "Master Bedroom")
                else
                    binder.areas.create(:name => "#{ordinal(number)} Bedroom")
                end
            end
        end
        
        if total_rooms = result.dig("totalRooms")
            if total_rooms.to_i > 6
                binder.areas.create(:name => "Family Room")
            end
        end
    end
end