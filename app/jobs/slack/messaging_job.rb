require 'slack-ruby-client'

class Slack::MessagingJob
    include Sidekiq::Worker
    sidekiq_options queue: 'low'
    
    def perform(channel, text, as_user = true)
        # create new client
        client = Slack::Web::Client.new
        # send the message
        client.chat_postMessage(channel: channel, text: text, as_user: as_user)
    end
end