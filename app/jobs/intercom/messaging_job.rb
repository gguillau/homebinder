# https://github.com/intercom/intercom-ruby
require 'intercom'
require 'intercom/client'

class Intercom::MessagingJob
    include Sidekiq::Worker
    sidekiq_options queue: 'non_critical'
    
    USER = "user".freeze
    INTERCOM_TEST_TOKEN = "INTERCOM_TEST_TOKEN".freeze
    INTERCOM_PRODUCTION_TOKEN = "INTERCOM_PRODUCTION_TOKEN".freeze
    FIRST_ERROR_MESSAGE_TO_USER = "Hey%s, looks like you were experiencing an issue. Let me know what you were trying to do so we can get you back on track. :-)".freeze
    SECOND_ERROR_MESSAGE_TO_USER  = "Still experiencing issues? Let me know if I can help. :-)".freeze
    TIME_ZONE = "Eastern Time (US & Canada)".freeze
    
    attr_accessor :intercom, :hb_user, :error

    # sends an in app message to the current user to help diagnose any issues
    # they're experiencing
    
    def perform(user_id, error)
        self.error = error
        self.hb_user = User.find_by_id(user_id)
        self.intercom = Intercom::Client.new(token: intercom_token)
        
        return if ENV["INTERCOM_ERROR_MESSAGING"] === "false"
        # check that the user object is not nil
        return unless hb_user.present?

        # make sure the user isn't an admin
        return unless !hb_user.has_role? :admin
        
        # make sure the user doesn't have any active non-web sessions
        return if hb_user.sessions.where(:status => 0).where.not(:device_type => "web").exists?
        
        # find the user in intercom
        intercom_user = find_intercom_user

        return unless intercom_user.present?
        return unless intercom_user.user_id.present?
        
        # always post event even if hb_user.last_intercom_error we don't send a message to track all user errors
        post_event("user-error")
        
        unless hb_user.last_intercom_error.nil?
            # check the time to see when the last time a message was sent
            time = Time.now.in_time_zone(TIME_ZONE)
            created_at = hb_user.last_intercom_error.to_datetime.in_time_zone(TIME_ZONE)
            hours = (time - created_at)/3600
            
            return if hours < 24
        end
        
        # save the date/time 
        hb_user.last_intercom_error = Time.now
        hb_user.save!
        
        if intercom_user.name.present?
            name = " #{intercom_user.name.split[0]}"
        else
            name = nil
        end
        
        send_message("inapp", FIRST_ERROR_MESSAGE_TO_USER % name, intercom_user.id)
    end

    # sends a message to a user using intercom
    
    def send_message(message_type, message, to_id)
        # create the message and send it
        intercom.messages.create({
            message_type: message_type,
            body: message,
            from: {
                type: "admin",
                id: find_admin
            },
            to: {
                type: "user",
                id: to_id
            }
        })
    rescue => e
        ErrorService.perform_async(e.message, {:user_id => hb_user.id})
    end
    
    def post_event(event)
        begin
            intercom.events.create(
                event_name: event,
                email: hb_user.email,
                created_at: Time.now.to_i,
                metadata: {:error => error}
            )
        rescue => e
            ErrorService.perform_async(e.message, {:user_id => hb_user.id})
        end
    end
    
    def find_admin
        raise BadRequestException.new "Intercom Admin ID Required" unless ENV["INTERCOM_ADMIN_ID"].present?
        ENV["INTERCOM_ADMIN_ID"]
    end
    
    def find_intercom_user
        return find_user_by_email
    end
    
    def find_user_by_email
        # Find user by email
        begin
            user = intercom.users.find(email: hb_user.email)
            return user
        rescue => e
            ErrorService.perform_async(e.message, {:user_id => hb_user.id})
            return nil
        end
    end
    
    # returns the intercom token and raises an error if the environment variable is not set
    
    def intercom_token
        raise BadRequestException.new "Intercom Token Required" unless ENV[get_token_name].present?
        return ENV[get_token_name]
    end
    
    def get_token_name
        if Host.path === Host::PRODUCTION_HOST
            return INTERCOM_PRODUCTION_TOKEN
        else
            return INTERCOM_TEST_TOKEN
        end
    end
end