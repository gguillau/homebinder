module Rake
    class SeedJob
        include Sidekiq::Worker
        sidekiq_options queue: 'critical'
        require "json"
        
        def perform
            create_widgets
            create_dashboards
            create_api_keys
            create_binder_library
            create_contractor_types
            create_property
            create_users_and_partners
            import
        rescue
        end

        def create_widgets
            Widget.new(name: "Binder Header Block", key: "binderHeaderBlock", description: "Displays the address and image or location of the property on a map.", category: "Binder").save!
            Widget.new(name: "Maintenance Reminders", key: "maintenanceReminders", description: "Dislays the next 5 maintenance reminders.", category: "Binder").save!
            Widget.new(name: "Recent Projects", key: "recentProjects", description: "Displays the 5 most recent projects.", category: "Binder").save!
            Widget.new(name: "Top Home Pros", key: "topHomePros", description: "Displays your favorite home pros.", category: "Binder").save!
            Widget.new(:name => "Third Party Services", :key => "thirdPartyServices", :description => "Allow homeowners to request services from third party companies", :category => "Binder").save!
            Widget.new(:name => "HomeSelfe Form", :key => "homeSelfe", :description => "Allow homeowners to request services from third party companies", :category => "Binder").save!
            Widget.new(:name => "APR Widget", :key => "apr", :description => "Displays information about an APR", :category => "Binder").save!
            Widget.new(:name => "Sell Side Inspection Widget", :key => "sellSideInspection", :description => "Nudge Homeowners to create seller's report", :category => "Binder").save!
            Widget.new(:name => "Walter Clews Widget", :key => "walterClewsWidget", :description => "Allow homeowners to request services from third party services from Walter Clews", :category => "Binder").save!
            Widget.new(:name => "Repair Pricer Widget", :key => "repairPricerWidget", :description => "Allow homeowners to request repair pricer report", :category => "Binder").save!
            Widget.new(:name => "Secure 24", :key => "secure24Widget", :description => "Allow homeowners to send requests to Secure 24 Alarm Systems, the nation’s leading ADT Authorized Dealer", :category => "Binder").save!
            Widget.new(:name => "Binder Completeness Widget", :key => "binderCompletenessWidget", :description => "Give homeowners a gauge to see how complete their binder is and motivate completion through links to binder sections", :category => "Binder").save!
            Widget.new(:name => "Inspection Report Widget", :key => "inspectionReportWidget", :description => "Allow user to view inspection report", :category => "Binder").save!
            Widget.new(:name => "Double Business Card", :key => "doubleBusinessCard", :description => "Displays a user's business card with the partner and agent visible", :category => "Binder").save!
            Widget.new(:name => "Custom", :key => "customWidget", :description => "A custom widget from partners for their clients\' dashboards", :category => "Partner.custom").save!
        rescue
        end

        def create_dashboards
            # create the default dashboard
            dashboard = Dashboard.create!(:name => "Default Dashboard", :default => true, :scope => "binder", :system => true)

            # add the widgets
            dashboard.widgets << Widget.first(4)
        rescue
        end
        
        def create_binder_library
          create_aws
          
          maintenance_templates = [
            {:name => "Smoke Alarm Batteries", :image => download("img/maintenance/SmokeDetector_MaintenanceLibrary.png"), :library_template => true, :initial_due_date_interval => 100, :frequency => "Annual", :notes => "Consider using the new 10 year models.", :description => "If you find yourself falling behind with changing batteries, they do make 10 year models that are increasing in popularity."},
            {:name => "Sweep Chimney", :image => download('img/maintenance/ChimneySweep_MaintenanceLibrary.png'), :library_template => true, :initial_due_date_interval => 100, :frequency => "Annual", :notes => "If you have a natural gas fireplace then you only need to inspect chimney for obstructions that may have fallen into the flue.", :description => "Not needed if fireplace is natural gas however chimney should be checked periodically both interior and exterior by a professional."},
            {:name => "Pump Septic Tank", :image => download('img/maintenance/PumpSeptic_MaintenanceLibrary.png'), :library_template => true, :initial_due_date_interval => 100, :frequency => "Every 3 Years", :notes => "You may be able to adjust the frequency based on usage and size of the system.", :description => "Can be done up to every 4 years if low usage - depends on size and # in household"},
            {:name => "Change HVAC Filter", :image => download('img/maintenance/HVACfilter_MaintenanceLibrary.png'), :library_template => true, :initial_due_date_interval => 100, :frequency => "Every 6 months", :notes => "If your system is used for both heating and cooling you may need to replace filters more frequently.", :description => "Depending on use and the climate, this could be as often as every 6-8 weeks in sub-tropical/tropical zones"},
            {:name => "Change Oil Furnace Filter", :image => download('img/maintenance/OilFilter_MaintenanceLibrary.png'), :library_template => true, :initial_due_date_interval => 100, :frequency => "Annual", :notes => "This is an inexpensive part and can be completed with minimal tools.", :description => "Often overlooked, this task should be done annually and may be done at time of service."},
            {:name => "Add Water Softener", :image => download('img/maintenance/WaterSoftener_MaintenanceLibrary.png'), :library_template => true, :initial_due_date_interval => 100, :frequency => "Every 3 months", :notes => "Typically recommend minimum of two bags per quarter", :description => "If 3 bags are added each time it should last at least 4 months.  For some areas the 'yellow' bag is fine but the 'green' bag that prevents iron build up is often recommended.  Check with your professional."},
            {:name => "Blow out Sprinkler Lines", :image => download('img/maintenance/BlowOutSprinkler_MaintenanceLibrary.png'), :library_template => true, :initial_due_date_interval => 100, :frequency => "Annual", :notes => "If you don't have an air compressor, hiring a professional is recommended.", :description => "Needs to be late in season when all leaves are down but not after freeze"},
            {:name => "Clean Gutters", :image => download('img/maintenance/CleanOutGutters_MaintenanceLibrary.png'), :library_template => true, :initial_due_date_interval => 100, :frequency => "Every 6 months", :notes => "Consider a guard system to reduce what gets into your gutters.", :description => "Depending on proximity and type of trees, you may have to do this more than 1x per year.  Also make sure downspouts and diversion devices keep water from foundation."},
            {:name => "Change Refrigerator Water Filter", :image => download('img/maintenance/RefrigeratorFilter_MaintenanceLibrary.png'), :library_template => true, :initial_due_date_interval => 100, :frequency => "Every 6 months", :notes => "Generic filters are often available but be sure to closely check for compatibility.", :description => "Refrigerators with in unit filters typically need every 6 months"},
            {:name => "Change House Water Filter", :image => download('img/maintenance/HouseWaterFilter_MaintenanceLibrary.png'), :library_template => true, :initial_due_date_interval => 100, :frequency => "Annual", :notes => "A special wrench is typically used - be careful not to over-tighten.", :description => "Unless you have excessive use or are prone to sediment build up, this can typically be done annually."},
            {:name => "Service Generator", :image => download('img/maintenance/ServiceGenerator_MaintenanceLibrary.png'), :library_template => true, :initial_due_date_interval => 100, :frequency => "Annual", :notes => "Remember to periodically run your generator during the year as well.", :description => "If you have a pad mounted or portable generator be sure to have it serviced at least annually.  Make sure you have plenty of fuel on hand at all times."},
            {:name => "Service Oil Furnace", :image => download('img/maintenance/ServiceFurnace_MaintenanceLibrary.png'), :library_template => true, :initial_due_date_interval => 100, :frequency => "Annual", :notes => "When serviced, make sure they replace the filter.", :description => "Oil furnances need annual service. Do early in the seasonbefore companies get busy in the fall."},
            {:name => "Dryer Vent Line", :image => download('img/maintenance/DryerVentHose_MaintenanceLibrary.png'), :library_template => true, :initial_due_date_interval => 100, :frequency => "Annual", :notes => "You can do this yourself or there are a number of companies that provide this service in most every market.", :description => "If dryer vent hose is more than 1 foot long it should be done each year.  Be sure to minimize the length and 'turns' as both increase the amount of lint buildup."},
            {:name => "Replace Washer Water Line", :image => download('img/maintenance/ReplaceWasherHose_MaintenanceLibrary.png'), :library_template => true, :initial_due_date_interval => 100, :frequency => "Every 5 years", :notes => "Consider using metal reinforced ones to reduce the chance of the hose failing.  Also, be sure rubber washers are in good condition.", :description => "These break and can cause significant damage. Replace with reinforced ones (metal) and they will last much longer."},
            {:name => "Well Test", :image => download('img/maintenance/WaterWellTest_MaintenanceLibrary.png'), :library_template => true, :initial_due_date_interval => 100, :frequency => "Every 3 years", :notes => "It is a good idea to test for any potential contaminants including Metals, Volatiles and Organics.", :description => "Do early in year in case something needs to be done. Well chemistry changes periodically so important to do every 3-5 years"},
            {:name => "Central Air Service", :image => download('img/maintenance/ServiceCentralAir_MaintenanceLibrary.png'), :library_template => true, :initial_due_date_interval => 100, :frequency => "Every other year", :notes => "Also be sure to make sure no vegetation is surrounding your central air unit.", :description => "Units can become inefficient if not checked, cleaned and maintained"},
            {:name => "Service Water System", :image => download('img/maintenance/ServiceHouseWaterSystem_MaintenanceLibrary.png'), :library_template => true, :initial_due_date_interval => 100, :frequency => "Annual", :notes => "Typically this is best done by a professional.  Seek a pro in your area.", :description => "This is typically best done by a company if you have a complex system.  They should also perform a basic water test at the time of service but it should not be considered a comprehensive test for all contaminants."}
          ]
          
          appliance_templates = [
            {:name => "Refrigerator", :library_template => true, :image => download('img/appliances/Refrigerator_ApplianceLibrary.jpg')},
            {:name => "Stove", :library_template => true, :image => download('img/appliances/Stove_ApplianceLibrary.jpg')},
            {:name => "Furnace", :library_template => true, :image => download('img/appliances/Furnace_ApplianceLibrary.jpg')},
            {:name => "Water Heater - Direct", :library_template => true, :image => download('img/appliances/WaterHeater_Direct_ApplianceLibrary.jpg')},
            {:name => "Water Heater - Indirect", :library_template => true, :image => download('img/appliances/WaterHeater_Indirect_ApplianceLibrary.jpg')},
            {:name => "Oven", :library_template => true, :image => download('img/appliances/Oven_ApplianceLibrary.jpg')},
            {:name => "Microwave", :library_template => true, :image => download('img/appliances/Microwave_ApplianceLibrary.jpg')},
            {:name => "Dishwasher", :library_template => true, :image => download('img/appliances/Dishwasher_ApplianceLibrary.jpg')},
            {:name => "Air Conditioner - Window Unit", :library_template => true, :image => download('img/appliances/AirConditioner_Window_ApplianceLibrary.jpg')},
            {:name => "Washer", :library_template => true, :image => download('img/appliances/Washer_ApplianceLibrary.jpg')},
            {:name => "Dryer", :library_template => true, :image => download('img/appliances/Dryer_ApplianceLibrary.jpg')},
            {:name => "Condenser", :library_template => true, :image => download('img/appliances/Condenser_ApplianceLibrary.jpg')},
            {:name => "Air Conditioner - Central Air", :library_template => true, :image => download('img/appliances/AirConditioner_CentralAir_ApplianceLibrary.png')},
            {:name => "Well Pump", :library_template => true, :image => download('img/appliances/WellPump_ApplianceLibrary.jpg')},
            {:name => "Disposal", :library_template => true, :image => download('img/appliances/Disposal_ApplianceLibrary.jpg')}
          ]
          
          system_template = BinderTemplate.create!(:system => true, :default => true, :name =>"System Default")
          partner_template = BinderTemplate.create!(:system => true, :default => false, :name =>"Partner Default", :partner => true)
          
          system_template.appliance_templates.create!(appliance_templates)
          
          partner_template.appliance_templates.create!(appliance_templates)
          
          system_template.maintenance_templates.create!(maintenance_templates)
          
          partner_template.maintenance_templates.create!(maintenance_templates)
          
          document_types.each do |type|
            DocumentType.find_or_create_by(:name => type)
          end
          
          partner_template.document_templates.create!(
            [
              {:file =>  download('pdf/New Homeowner Guide Final.pdf'), :file_file_name => "New Homeowner Guide.pdf", :document_type_id => DocumentType.find_by(:name => "Other").id, :library_template => true, :notes => "Congratulations on becoming a new homeowner! Whether or not this is your first home, taking ownership and moving in can be an exciting and stressful time.", :description => "This book is your key to HomeBinder’s tool chest, as well as a summary of the many aspects of home maintenance that preserve and enhance the value of your property."}
            ]
          )
          
          Contractor::SubCategory.create!([
            {:name => "Additions & Renovations" },
            {:name => "Aerial" },
            {:name => "Agent" },
            {:name => "Air Sealing" },
            {:name => "ASHI" },
            {:name => "Basic Electrical" },
            {:name => "Basic Plumbing" },
            {:name => "Blower Testing" },
            {:name => "Boarding" },
            {:name => "Broker" },
            {:name => "Business" },
            {:name => "Cabinets" },
            {:name => "Car and Auto" },
            {:name => "Carpentry" },
            {:name => "Cleaning" },
            {:name => "Cleaning & Service" },
            {:name => "Commercial" },
            {:name => "Construction" },
            {:name => "Countertops" },
            {:name => "CPA" },
            {:name => "Crack Repair" },
            {:name => "Custom" },
            {:name => "Design" },
            {:name => "Detail" },
            {:name => "Dog Walking" },
            {:name => "Drilling and Installation" },
            {:name => "Duct Cleaning" },
            {:name => "Electrical" },
            {:name => "Exterior" },
            {:name => "Fire & Flood" },
            {:name => "Foundation" },
            {:name => "Gardening" },
            {:name => "General" },
            {:name => "General Removal" },
            {:name => "Hardwood Refinishing" },
            {:name => "Home" },
            {:name => "Home Financing & Refinancing" },
            {:name => "House Sitting" },
            {:name => "InterNACHI" },
            {:name => "Injury" },
            {:name => "Insects" },
            {:name => "Installation" },
            {:name => "Insulation & Airsealing" },
            {:name => "Interior" },
            {:name => "Large Items & Dumpsters" },
            {:name => "Large Machinery" },
            {:name => "Long Haul" },
            {:name => "Mechanical" },
            {:name => "Miscellaneous" },
            {:name => "Mold" },
            {:name => "Monitoring" },
            {:name => "Mosquitos" },
            {:name => "Mowing" },
            {:name => "New home construction" },
            {:name => "Paint Sales" },
            {:name => "Painting" },
            {:name => "Patios & Walkways" },
            {:name => "Personal" },
            {:name => "Planting" },
            {:name => "Pumping & Service" },
            {:name => "Realtor" },
            {:name => "Removal" },
            {:name => "Renovation" },
            {:name => "Renovations" },
            {:name => "Repair" },
            {:name => "Replacement" },
            {:name => "Residential" },
            {:name => "Rodents" },
            {:name => "Sales" },
            {:name => "Sealing & Patching" },
            {:name => "Service & Repair" },
            {:name => "Service & Support" },
            {:name => "Short Haul" },
            {:name => "Small Engine" },
            {:name => "Small Projects" },
            {:name => "Spa Only" },
            {:name => "Special Items" },
            {:name => "Staging" },
            {:name => "Stonewalls" },
            {:name => "Structural" },
            {:name => "Stump Grinding" },
            {:name => "Sweeping" },
            {:name => "Taxes" },
            {:name => "Testing" },
            {:name => "Testing & Auditing" },
            {:name => "Ticks" },
            {:name => "Tree Removal" },
            {:name => "Water Proofing" },
            {:name => "Whole House" },
            {:name => "Wills & Estates" },
            {:name => "Windows" }
          ])
        rescue
        end
        
        def create_contractor_types
          Contractor::Category.create!([
            {:name => "Accounting", :sub_type_ids => find_sub_categories(["CPA", "Taxes"])},
            {:name => "Alarm Security Systems", :sub_type_ids => find_sub_categories(["Installation", "Monitoring"])},
            {:name => "Appliance", :sub_type_ids => find_sub_categories(["Sales", "Service & Repair"])},
            {:name => "Appraisal", :sub_type_ids => find_sub_categories(["Residential", "Commercial"])},
            {:name => "Asbestos", :sub_type_ids => find_sub_categories(["Testing", "Removal"])},
            {:name => "Bathroom and Tile", :sub_type_ids => find_sub_categories(["Renovation"])},
            {:name => "Cabinetmaking, Millwork, & Woodworking", :sub_type_ids => find_sub_categories(["Construction", "Painting"])},
            {:name => "Chimney", :sub_type_ids => find_sub_categories(["Construction", "Sweeping", "Repair"])},
            {:name => "Cleaning", :sub_type_ids => find_sub_categories(["Whole House", "Windows"])},
            {:name => "Dock", :sub_type_ids => find_sub_categories(["Installation", "Service & Repair"])},
            {:name => "Driveway", :sub_type_ids => find_sub_categories(["Construction", "Sealing & Patching", "Small Projects"])},
            {:name => "Dryer Vent Cleaning", :sub_type_ids => find_sub_categories([""])},
            {:name => "Electrical", :sub_type_ids => find_sub_categories(["General", "Whole House", "Small Projects"])},
            {:name => "Energy Efficiency", :sub_type_ids => find_sub_categories(["Insulation & Airsealing", "Testing & Auditing"])},
            {:name => "Engineering", :sub_type_ids => find_sub_categories(["Structural", "Mechanical", "Foundation", "Electrical"])},
            {:name => "Fencing", :sub_type_ids => find_sub_categories(["Installation"])},
            {:name => "Financial Advisor", :sub_type_ids => find_sub_categories([""])},
            {:name => "Fireplace", :sub_type_ids => find_sub_categories(["Installation", "Service & Repair"])},
            {:name => "Firewood", :sub_type_ids => find_sub_categories([""])},
            {:name => "Flooring", :sub_type_ids => find_sub_categories(["Installation", "Cleaning", "Hardwood Refinishing"])},
            {:name => "Foundation", :sub_type_ids => find_sub_categories(["Construction", "Crack Repair", "Water Proofing"])},
            {:name => "Garage Door", :sub_type_ids => find_sub_categories(["Installation", "Service & Repair"])},
            {:name => "General Contracting", :sub_type_ids => find_sub_categories(["New home construction", "Additions & Renovations"])},
            {:name => "Glass", :sub_type_ids => find_sub_categories(["Replacement", "Repair"])},
            {:name => "Gutters", :sub_type_ids => find_sub_categories(["Installation", "Cleaning", "Repair"])},
            {:name => "Handyman", :sub_type_ids => find_sub_categories(["Carpentry", "Basic Electrical", "Basic Plumbing", "Miscellaneous"])},
            {:name => "Heating and Air Conditioning (HVAC)", :sub_type_ids => find_sub_categories(["Installation", "Service & Repair", "Duct Cleaning"])},
            {:name => "Home Inspector", :sub_type_ids => find_sub_categories(["ASHI", "InterNACHI"])},
            {:name => "Home Owner Association (HOA)", :sub_type_ids => find_sub_categories([""])},
            {:name => "Home Sales", :sub_type_ids => find_sub_categories(["Staging"])},
            {:name => "Insulation", :sub_type_ids => find_sub_categories(["Installation", "Air Sealing", "Blower Testing"])},
            {:name => "IT & Computer", :sub_type_ids => find_sub_categories(["Sales", "Service & Support"])},
            {:name => "Kitchen", :sub_type_ids => find_sub_categories(["Renovations", "Countertops", "Cabinets", "Construction"])},
            {:name => "Landscaping", :sub_type_ids => find_sub_categories(["Design", "Installation", "Planting", "Mowing", "Gardening"])},
            {:name => "Legal", :sub_type_ids => find_sub_categories(["Business", "Wills & Estates", "Injury"])},
            {:name => "Lighting", :sub_type_ids => find_sub_categories(["Interior", "Exterior"])},
            {:name => "Locksmith", :sub_type_ids => find_sub_categories([""])},
            {:name => "Masonry", :sub_type_ids => find_sub_categories(["Stonewalls", "Patios & Walkways"])},
            {:name => "Mechanic", :sub_type_ids => find_sub_categories(["Car and Auto", "Small Engine", "Large Machinery"])},
            {:name => "Mold", :sub_type_ids => find_sub_categories(["Testing", "Removal"])},
            {:name => "Mortgage", :sub_type_ids => find_sub_categories(["Home Financing & Refinancing"])},
            {:name => "Moving", :sub_type_ids => find_sub_categories(["Short Haul", "Long Haul", "Special Items"])},
            {:name => "Not Defined"},
            {:name => "Painting", :sub_type_ids => find_sub_categories(["Interior", "Exterior", "Detail", "Custom", "Paint Sales"])},
            {:name => "Personal Assistant", :sub_type_ids => find_sub_categories([""])},
            {:name => "Pest Control", :sub_type_ids => find_sub_categories(["Rodents", "Mosquitos", "Insects", "Ticks"])},
            {:name => "Pet", :sub_type_ids => find_sub_categories(["Dog Walking", "House Sitting", "Boarding"])},
            {:name => "Photography", :sub_type_ids => find_sub_categories(["Aerial", "Personal", "Home"])},
            {:name => "Plumbing", :sub_type_ids => find_sub_categories(["General", "Whole House", "Small Projects"])},
            {:name => "Pool & Spa", :sub_type_ids => find_sub_categories(["Installation", "Cleaning & Service", "Spa Only"])},
            {:name => "Real Estate", :sub_type_ids => find_sub_categories(["Agent"])},
            {:name => "Restoration", :sub_type_ids => find_sub_categories(["Fire & Flood", "Mold"])},
            {:name => "Roofing", :sub_type_ids => find_sub_categories(["Installation"])},
            {:name => "Septic/Cesspool", :sub_type_ids => find_sub_categories(["Design", "Installation", "Pumping & Service"])},
            {:name => "Solar", :sub_type_ids => find_sub_categories([""])},
            {:name => "Trash and Waste Removal", :sub_type_ids => find_sub_categories(["General Removal", "Large Items & Dumpsters"])},
            {:name => "Tree Service", :sub_type_ids => find_sub_categories(["Tree Removal", "Stump Grinding", "Planting"])},
            {:name => "Water Well", :sub_type_ids => find_sub_categories(["Drilling and Installation", "Repair"])},
            {:name => "Windows", :sub_type_ids => find_sub_categories(["Installation", "Cleaning"])}
          ])
          
          Contractor::Category.update_all(:verified => true)
        rescue
        end
        
        def find_sub_categories(names)
          names.map! {|name| name.downcase}
          count = 0
          where = ""
          names.each_with_index do |name, index|
            where += "LOWER(name) = ?"
            count += 1
            where += " OR " if count > 0 && index != names.length - 1
          end
          Contractor::SubCategory.where(where, *names).pluck(:id)
        rescue
        end

        def create_property
            PropertyType.create!([
              { name: "Single Family", created_by: 0, verified: true },
              { name: "Multi Family", created_by: 0, verified: true },
              { name: "Condo", created_by: 0, verified: true }
            ])
        rescue
        end

        def create_api_keys
            ApiKey.create!([
              { company_name: "HomeBinder.com", application_name: "HomeBinder.com", key: ENV["HB_APIKEY"],
                contact_email: "dev@homebinder.com" },
            ])
        rescue
        end

        def create_users_and_partners
          # users
          User.create!([
            { email: "orphans@homebinder.com", password: SecureRandom.hex(5), role: "homeowner"},
            { email: "johnsmith@sampleinspector.com", password: SecureRandom.hex(5), role: "inspector"},
            { email: "dev@homebinder.com", password: SecureRandom.hex(5), role: "admin"}
          ])

          UserProfile.joins(:user).where(:users => {:email => "johnsmith@sampleinspector.com"}).update_all({:company => "John Smith Inspections", :mobile_phone => "+13414365214", :website => "www.homebinder.com"})

          orphans = User.first
          user = User.find_by_email("johnsmith@sampleinspector.com")

          Partner.create!([
            { name: "Sample Home Inspector", phone: "+18002939293", email: user.email, contact: "John Smith",
              partner_type: "inspector", email_display_name: "Sample Home Inspector", partner_key: "samplehomeinspector",
              completed_onboarding: true, code: "SAMPLE"}
          ])

          partner = Partner.last
          partner.create_address(:country => "US")
          PartnerUser.create!(:user => user, :partner => partner, :role => "admin")
          PartnerService.new(user, partner).after_create

          Binder.create!([
            { name: "Jennifer Billings Home", primary: true, active: true, created_by: user.id}
          ])

          binder = Binder.last
          binder.partner_binders.create(:partner => partner, :role => "binder", :user => orphans)
          Binder::Property.create!(binder_id: binder.id, address1: "123 Main Street", city: "Boston", state: "MA", country: "US")
          Binder::Transfer.create!(binder_id: binder.id, transfer_type: "ownership", status: "created", sender_id: user.id, receiver_id: orphans.id)
          
          template = BinderTemplate.find(partner.partner_configuration.default_binder_template_id)
          
          # add the transfer note to the binder details
          Partner::Automation::Service.delay(queue: 'abs').add_transfer_note(binder.id, template.id)
  
          # add appliances to binder
          Partner::Automation::Service.delay(queue: 'abs').add_appliances([], binder.id, template.id, user.id)
  
          # add maintenance items to binder
          Partner::Automation::Service.delay(queue: 'abs').add_maintenance_items([], binder.id, template.id, user.id)
          
          # add contractors to binder
          Partner::Automation::Service.delay(queue: 'abs').add_contractors([], binder.id, template.id, user.id)
  
          # add projects to binder
          Partner::Automation::Service.delay(queue: 'abs').add_projects([], binder.id)
  
          # add documents to binder
          Partner::Automation::Service.delay(queue: 'abs').add_template_docs(binder.id, template.id)
          
          UserBinder.create!([
            { user_id: user.id, binder_id: binder.id, role: "owner"}
          ])

          warranty_company = WarrantyCompany.create!(:name => "Complete Home Warranties", :contact => "Mike Cameron", :email => "support@homebinder.com")

          plans = warranty_company.warranty_plans.create!(
            [
              {:name => "90 Day Warranty + Water/Sewer, Mold, Roof, Washer, Dryer and Refrigerator Protection", :duration => "90", :cycle => "days", :description => "Valid for 90 days from the date of the inspection or 30 days after closing.", :price => 33},
              {:name => "90 Day Warranty + Washer, Dryer and Refrigerator Protection", :duration => "90", :cycle => "days", :description => "Valid for 90 days from the date of the inspection or 30 days after closing.", :price => 33},
              {:name => "90 Day Warranty", :duration => "90", :cycle => "days", :description => "Valid for 90 days from the date of the inspection or 30 days after closing.", :price => 33},
              {:name => "90 Day Warranty + Water/Sewer, Mold and Roof Protection", :duration => "90", :cycle => "days", :description => "Valid for 90 days from the date of the inspection or 30 days after closing.", :price => 33}
            ]
          )

          warranty_configuration = WarrantyConfiguration.create!(:account_number => SecureRandom.hex(8), :warranty_plan_id => plans[0].id, :warranty_company_id => warranty_company.id, :partner_id => partner.id)
          warranty_configuration.warranty_plans.create!(
            [
              {:price => plans[0].price, :warranty_plan_id => plans[0].id},
              {:price => plans[1].price, :warranty_plan_id => plans[1].id},
              {:price => plans[2].price, :warranty_plan_id => plans[2].id},
              {:price => plans[3].price, :warranty_plan_id => plans[3].id}
            ]
          )

          binder.warranties.create!(:client_first => "Jay", :client_last => "Nathan", :client_email => "support@homebinder.com", :client_phone => "222-222-2222", :status => "submitted", :expiration_date => Date.today + 30.days, :binder_id => binder.id, :warranty_plan_id => plans[0].id, )
          binder.transactions.create!(:transaction_type => "buy_side_inspection", :transaction_date => Date.today)

          AnnualPropertyReview.create!(
            :status => AnnualPropertyReview.statuses[:active],
            :client_first_name => "Chris",
            :client_last_name => "Green",
            :client_email => "support@homebinder.com",
            :client_phone => "555-123-4567",
            :address1 => "123 Main Street",
            :city => "Boston",
            :state => "MA",
            :zip => "02210",
            :country => "US",
            :reviewer_company_id => partner.id,
            :uuid => SecureRandom.hex(8)
          )

          # recalls
          # seller report
          # binder items
          # marketing resources
          # isn users
        rescue
        end
        
        def document_types
          [ "Home Inspection Report", "Appraisal", "Home Reference Manual", "Radon Report", 
          "Well Test Report", "Checklist", "Plot Map", "Invoice", "Inspection Agreement", "Pest/Insect Report", "Other"]
        rescue
        end
        
        def import
          Elasticsearch::BuildJob.perform_async
        rescue
        end
        
        def create_aws
          @s3 = S3FileService.load_s3
          
          # get the bucket
          @bucket = @s3.bucket("homebinderstatic")
        rescue
        end
        
        def download(path)
          URI.parse(@bucket.object(path).presigned_url(:get, expires_in: 60))
        rescue
        end
    end
end