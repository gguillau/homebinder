# this file contains a list of methods that will run after a sprint is pushed
# to the production server
module Rake
    class SprintJob
        include Sidekiq::Worker
        sidekiq_options queue: 'default'
        
        def perform
            fix_documents
        end
        
        def fix_documents
            errors = []
            library_source_ids = Binder::Document.where(:file_file_size => 0).where.not(:library_source_id => nil).distinct.pluck(:library_source_id)
            library_source_ids.each do |library_source_id|
                begin
                    first_document = Binder::Document.where(library_source_id: library_source_id).where.not(file_file_size: 0).first
                    if first_document.nil?
                        errors.push library_source_id
                        next
                    else
                        Binder::Document.where(library_source_id: library_source_id).where(file_file_size: 0).find_each do |document|
                            document.file = first_document.file
                            document.save
                        end
                    end
                rescue
                    errors.push library_source_id
                    next
                end
            end
        end
    end
end