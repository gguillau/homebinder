module Users
    class UpdateContractorJob
        include Sidekiq::Worker
        sidekiq_options queue: 'abs'
        
        def perform(user_id)
            user = User.find_by_id(user_id)
            return if user.nil?

            contractor = Contractor.find_by(:user_id => user_id)

            if contractor
                contractor.name = user.user_profile.company
                return if contractor.name.blank?
                
                contractor.contact = user.user_profile.display_name
                contractor.phone = user.user_profile.mobile_phone
                contractor.url = user.user_profile.website
                contractor.email = user.email
                contractor.save!
                
                address_1 = contractor.address.presence ? contractor.address : contractor.build_address
                address_2 = user.user_profile.address.presence ? user.user_profile.address : user.user_profile.build_address
                
                address_1.address1 = address_2.address1
                address_1.address2 = address_2.address2
                address_1.city = address_2.city
                address_1.state = address_2.state
                address_1.zip = address_2.zip
                address_1.country = address_2.country
                address_1.save!
                
                Binder::BinderContractor.where(:contractor_id => contractor.id).update_all(:contact => user.user_profile.display_name)
                PartnerContractor.where(:contractor_id => contractor.id).update_all(:email => user.email, :name => user.user_profile.company, :phone => user.user_profile.mobile_phone)
            end
        end
    end
end