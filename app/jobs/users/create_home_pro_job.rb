require 'csv'

class Users::CreateHomeProJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    def perform(contractor_id)
        contractor = Contractor.find_by_id(contractor_id)
        return if contractor.nil?
        return if contractor.email.nil?
        return if contractor.user.present?
        return if contractor.creator.nil?
        
        address = contractor.address.present? ? contractor.address : contractor.create_address
            
        user = {
            :email => contractor.email, 
            :password => SecureRandom.hex(5), 
            :role => "homepro",
            :user_profile_attributes => {
                :first_name => contractor.contact,
                :mobile_phone => contractor.phone,
                :company => contractor.name,
                :website => contractor.url,
                :address_attributes => {
                    :country => address.country, 
                    :address1 => address.address1, 
                    :address2 => address.address2, 
                    :city => address.city, 
                    :state => address.state, 
                    :zip => address.zip
                }
            }
        }
        
        code = contractor.email + contractor.name.gsub(/\s+/, '') + "homepro"
        
        partner = {
            :name => contractor.name,
            :partner_type => "homepro",
            :code => code,
            :email => contractor.email,
            :contact => contractor.contact,
            :phone => contractor.phone,
            :website => contractor.url,
            :email_display_name  => contractor.name.length > 50 ? contractor.contact : contractor.name,
            :address_attributes => {
                :country => address.country, 
                :address1 => address.address1, 
                :address2 => address.address2, 
                :city => address.city, 
                :state => address.state, 
                :zip => address.zip
            }
        }
        
        begin
            Partner.build({}, ActionController::Parameters.new({user: user, partner: partner}))
            
            user = User.find_by_email(contractor.email)
            
            if user
                if contractor.creator.is_homeowner? || !user.belongs_to_partner?
                    creator = contractor.creator.user_profile.display_name
                    template = "Home Pro Profile Invitation from Homeowner"
                else
                    creator = contractor.creator&.partners&.first&.name
                    template = "Home Pro Profile Invitation from Partner"
                end
            
                # Home Pro Previously Added Invitation
                UserMailer.notify_homepro(user, creator, template).deliver_later

                contractor.user_id = user.id
                contractor.save!
                
                user.sign_in_count = 0
                user.create_method = contractor.creator.role
                user.save!
            end
        rescue => e
            task = "Users::CreateHomeProJob"
            case e
            when UnprocessableException
                message = e.resource.errors.full_messages.first
            else
                message = e.message
            end
            ErrorService.perform_async(message, {:task => task})
        end
    end
end