require 'csv'

class Users::MonthlyEmailJob
    include Sidekiq::Worker
    sidekiq_options queue: 'non_critical'
    
    def perform
        require 'date'

        today       = Date.today
        email_date  = Date.today.beginning_of_month

        # only send the email on the first day of the month
        if today === email_date
            # only send a monthly email to homeowners who have signed in
            User.joins(:user_profile).where(:role => "homeowner", :user_profiles => {:monthly_email => true}).where("sign_in_count > 0").find_each do |email_user|
                email_user_binders  = email_user.binders
                email_user_binders.each do |binder|
                    UserMailer.monthly_email(email_user.id, binder.id).deliver_later
                    
                    # analytics
                    EventService.perform_async({:event_name => "monthly_mailer", :event_type => "email", :binder_id => binder.id, :user_id => email_user.id, :user_created_at => email_user.created_at, :user_role => email_user.role})
                end
            end
        end
    end
end