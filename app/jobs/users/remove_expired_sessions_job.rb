module Users
    class RemoveExpiredSessionsJob
        include Sidekiq::Worker
        sidekiq_options queue: 'non_critical'
        
        def perform
            # remove sessions where the expires at is less than today
            Session.where("expires_at < ?", Date.today).destroy_all
        end
    end
end