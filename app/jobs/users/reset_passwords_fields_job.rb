module Users
    class ResetPasswordsFieldsJob
        include Sidekiq::Worker
        sidekiq_options queue: 'non_critical'
        
        # sometimes users won't reset their passwords within the required timeframe
        # this rake task runs hourly and resets the reset_password_token and the reset_password_sent_at
        # so users can request a reset of their password again

        def perform
            user_ids = User.where.not(:reset_password_token => nil, :reset_password_sent_at => nil).ids
            user_ids.each do |id|
                user = User.find_by_id(id)
                next if user.nil?
                user.update(:reset_password_token => nil, :reset_password_sent_at => nil) if Time.now - user.reset_password_sent_at > Rails.configuration.password_reset_time_frame.hours
            end
        end
    end
end