class Users::UndoUserMergeJob
    include Sidekiq::Worker
    
    def perform(user_merge_id)
        user_merge = UserMerge.find(user_merge_id)
        return if user_merge.status === 1 || user_merge.status === 2
        user_merge.status = 1
        user_merge.save!
        
        content = Paperclip.io_adapters.for(user_merge.footprint).read
        params = JSON.parse(content, symbolize_names: true)
        
        reverted_user = User.new(params[:old_agent])
        reverted_user.password = SecureRandom.hex(8)
        reverted_user.save!
        
        merged_agent = user_merge.user
        
        # update the old agent's associations to point to the new agent
        UserBinder.where(:id => params[:old_agent_user_binder_ids], :user_id => merged_agent.id).update_all(:user_id => reverted_user.id)
        PartnerUser.where(:id => params[:old_agent_partner_user_ids], :user_id => merged_agent.id, :role => "agent").update_all(:user_id => reverted_user.id)
        PartnerClient.where(:id => params[:old_agent_partner_client_ids], :client_id => merged_agent.id).update_all(:client_id => reverted_user.id)
        Dashboard.where(:id => params[:old_agent_dashboard_ids], :user_id => merged_agent.id).update_all(:user_id => reverted_user.id)
        BinderItem.where(:id => params[:old_agent_binder_item_ids], :user_id => merged_agent.id).update_all(:user_id => reverted_user.id)
        Binder::Transfer.where(:id => params[:old_agent_transfers_received], :receiver_id => merged_agent.id).update_all(:receiver_id => reverted_user.id)
        Binder::Transfer.where(:id => params[:old_agent_transfers_sent], :sender_id => merged_agent.id).update_all(:sender_id => reverted_user.id)
        Binder::Share.where(:id => params[:old_agent_shares_received], :shared_with_id => merged_agent.id).update_all(:shared_with_id => reverted_user.id)
        Binder::Share.where(:id => params[:old_agent_shares_sent], :shared_by_id => merged_agent.id).update_all(:shared_by_id => reverted_user.id)
        
        UserContractor.where(:id => params[:old_agent_user_contractors], :user_id => merged_agent.id).update_all(:user_id => reverted_user.id)
        OrganizationUser.where(:id => params[:old_agent_organization_users], :user_id => merged_agent.id).update_all(:user_id => reverted_user.id)
        PartnerBinder.where(:id => params[:old_agent_partner_binders], :client_id => merged_agent.id).update_all(:client_id => reverted_user.id)
        UserBinderTransaction.where(:id => params[:old_agent_user_binder_transactions], :user_id => merged_agent.id).update_all(:user_id => reverted_user.id)
        UserResource.where(:id => params[:old_agent_user_resources], :user_id => merged_agent.id).update_all(:user_id => reverted_user.id)
        
        params[:shared_partners].each do |partner_id|
            PartnerUser.create(:partner_id => partner_id, :user_id => reverted_user.id, :role => "agent")
            PartnerClient.create(:partner_id => partner_id, :client_id => reverted_user.id)
        end
        
        # update contactors
        Contractor.where(:id => params[:old_agent_contractor_id]).update_all(:user_id => reverted_user.id)

        Binder::BinderContractor.where(:id => params[:old_agent_binder_contractor_ids], :contractor_id => merged_agent.contractor&.id).update_all(:contractor_id => reverted_user.contractor&.id)
        PartnerContractor.where(:id => params[:old_agent_partner_contractor_ids], :contractor_id => merged_agent.contractor&.id).update_all(:contractor_id => reverted_user.contractor&.id)

        merged_agent.update(params[:new_agent])
        
        user_merge.status = 2
        user_merge.save!
    end
end