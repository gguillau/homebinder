class Users::RemoveJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    def perform(id)
        user = User.find_by_id(id)
        return if user.nil?
        
        # delete the users binders
        user.binders.each do |b|
            if user.ability.can? :destroy, b
                # delete the binder
                b.remove
            else
                # update transfers where status is created, accepted or declined
                b.transfers.where(:receiver_id => user.id)
                    .where.not(:status => "created")
                    .where.not(:status => "declined")
                    .where.not(:status => "accepted")
                    .update_all(:status => "declined")

                # update shares where status is created, accepted or declined
                b.shares.where(:shared_with_id => user.id)
                    .where.not(:status => "created")
                    .where.not(:status => "declined")
                    .where.not(:status => "accepted")
                    .update_all(:status => "declined")
            end
        end

        # doing this in delayed jobs doesn't work for some reason
        Users::UserAnalytics.delay(queue: 'critical').destroyed(user.id)
    end
    
end