class Users::InitBindersJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    def perform(id)
        user = User.find_by_id(id)
        return if user.nil?
        
        user.binders.each do |b|
            # enable maintenance reminders
            b.enable_maintenance_reminders

            if b.transfers.where(:receiver_id => user.id)
                .where.not(:status => "created")
                .where.not(:status => "declined")
                .where.not(:status => "accepted").count > 0
                if user.accepted_transfer_at.nil? then user.accepted_transfer_at = Date.today and user.save end

                # get the transfer

                transfer = b.transfers.where(:receiver_id => user.id)
                    .where.not(:status => "created")
                    .where.not(:status => "declined")
                    .where.not(:status => "accepted").first

                # update transfers where status is created, accepted or declined
                b.transfers.where(:receiver_id => user.id)
                    .where.not(:status => "created")
                    .where.not(:status => "declined")
                    .where.not(:status => "accepted")
                    .update_all(:status => "accepted")

                # add wigets dependent on acceptance of transfer
                TransferService.new(transfer.id).add_widgets

                # analytics
                EventService.perform_async({:event_name => "accepted", :event_type => "transfer", :user_id => id, :user_created_at => user.created_at, :user_role => user.role})
            end
            
            # update shares
            b.shares.where(:shared_with_id => user.id)
                .where.not(:status => "created")
                .where.not(:status => "declined")
                .where.not(:status => "shared")
                .update_all(:status => "shared")
                
            # update partner_binders
            PartnerBinder.where(:binder_id => b.id).update(:accepted => true)
        end

        # update the analytics
        Users::UserAnalytics.delay(:queue => "analytics").update_user(user.id)
    end
    
end