class Users::MergeAgentsJob
    include Sidekiq::Worker
    
    def perform(old_agent_id, new_agent_id, merger_id)
        # retrieve the agents
        old_agent = User.find_by_id(old_agent_id)
        return if old_agent.nil?
        new_agent = User.find_by_id(new_agent_id)
        return if new_agent.nil?
        
        # make sure both users are agents
        return if (old_agent.role != "agent" || new_agent.role != "agent")
        
        footprint = create_footprint(old_agent, new_agent)

        # delete unique shared partner account data to avoid duplicates
        delete_shared_partner_data(footprint[:shared_partners], old_agent_id)

        # get the agent info before anything is changed to have data to send in emails
        new_agent_data = get_agent_data(new_agent)
        old_agent_data = get_agent_data(old_agent)
        
        update_associations(old_agent, new_agent)
        
        # get user profiles
        old_profile = old_agent.user_profile
        new_profile = new_agent.user_profile
        
        # setup attributes for update
        email = new_agent.email.present? ? new_agent.email : old_agent.email
        address_attributes = new_profile.address.address1.present? ? new_profile.address.attributes : old_profile.address.attributes
        address_attributes.delete("id")
        address_attributes["user_profile_id"] = new_profile.id
        user_profile = {
            :first_name => new_profile.first_name.present? ? new_profile.first_name : old_profile.first_name,
            :last_name => new_profile.last_name.present? ? new_profile.last_name : old_profile.last_name,
            :home_phone => new_profile.home_phone.present? ? new_profile.home_phone : old_profile.home_phone,
            :mobile_phone => new_profile.mobile_phone_formatted.present? ? new_profile.mobile_phone : old_profile.mobile_phone,
            :dob => new_profile.dob.present? ? new_profile.dob : old_profile.dob,
            :sex => new_profile.sex.present? ? new_profile.sex : old_profile.sex,
            :company => new_profile.company.present? ? new_profile.company : old_profile.company,
            :website => new_profile.website.present? ? new_profile.website : old_profile.website,
            :bio => new_profile.bio.present? ? new_profile.bio : old_profile.bio,
            :message => new_profile.message.present? ? new_profile.message : old_profile.message,
        }
        
        # if new agent doesn't have head shot or logo then we merge logo and headshot from old agent into new agent
        if !new_agent.user_profile.logo.present?
            user_profile[:logo] = old_profile.logo
        end
        
        if !new_agent.user_profile.head_shot.present?
            user_profile[:head_shot] = old_profile.head_shot
        end
        
        # make the updates
        new_agent.update(:email => email)
        address = new_profile.address
        address.update(address_attributes)
        address.save!
        new_profile.update(user_profile)
        
        # delete the old_agent
        old_agent.destroy
        
        # send the emails to notify the two agents and HomeBinder support of the merge
        AgentMergeMailer.notify_agents(old_agent_data, new_agent_data, merger_id).deliver_later
        AgentMergeMailer.support_notification(old_agent_data, new_agent_data, merger_id, footprint[:old_agent_binder_ids]).deliver_later
        
        user_merge = UserMerge.new
        user_merge.user_id = new_agent_id
        user_merge.old_agent_email = old_agent.email
        user_merge.old_agent_first_name = old_agent.user_profile.first_name
        user_merge.old_agent_last_name = old_agent.user_profile.last_name

        file = StringIO.new(footprint.to_json) # mimic a real upload file
        file.class.class_eval { attr_accessor :original_filename, :content_type } # add attr's that paperclip needs
        file.original_filename = "footprint.json" # assign filename in way that paperclip likes
        file.content_type = "application/json" # you could set this manually aswell if needed e.g 'application/pdf'
        
        user_merge.footprint = file
        user_merge.save!
    end
    
    def update_associations(old_agent, new_agent)
        # update the old agent's associations to point to the new agent
        UserBinder.where(:user_id => old_agent.id).update_all(:user_id => new_agent.id)
        PartnerUser.where(:user_id => old_agent.id, :role => "agent").update_all(:user_id => new_agent.id)
        PartnerClient.where(:client_id => old_agent.id).update_all(:client_id => new_agent.id)
        Dashboard.where(:user_id => old_agent.id).update_all(:user_id => new_agent.id)
        BinderItem.where(:user_id => old_agent.id).update_all(:user_id => new_agent.id)
        Binder::Transfer.where(:receiver_id => old_agent.id).update_all(:receiver_id => new_agent.id)
        Binder::Transfer.where(:sender_id => old_agent.id).update_all(:sender_id => new_agent.id)
        Binder::Share.where(:shared_with_id => old_agent.id).update_all(:shared_with_id => new_agent.id)
        Binder::Share.where(:shared_by_id => old_agent.id).update_all(:shared_by_id => new_agent.id)
        UserContractor.where(:user_id => old_agent.id).update_all(:user_id => new_agent.id)
        OrganizationUser.where(:user_id => old_agent.id).update_all(:user_id => new_agent.id)
        PartnerBinder.where(:client_id => old_agent.id).update_all(:client_id => new_agent.id)
        UserBinderTransaction.where(:user_id => old_agent.id).update_all(:user_id => new_agent.id)
        UserResource.where(:user_id => old_agent.id).update_all(:user_id => new_agent.id)
        
        # make sure contractors exist before destroying
        if old_agent.contractor
            if new_agent.contractor
                Binder::BinderContractor.where(:contractor_id => old_agent.contractor.id).update_all(:contractor_id => new_agent.contractor.id)
                PartnerContractor.where(:contractor_id => old_agent.contractor.id).update_all(:contractor_id => new_agent.contractor.id)
            else
                contractor = old_agent.contractor
                contractor.user_id = new_agent.id
                contractor.save!
            end
        end
    end
    
    def create_footprint(old_agent, new_agent)
        {
            old_agent_id: old_agent.id,
            old_agent_user_profile: old_agent.user_profile.id,
            old_agent_address_id: old_agent.user_profile.address.id,
            old_agent: {
                email: old_agent.email,
                role: old_agent.role,
                sign_in_count: old_agent.sign_in_count,
                completed_onboarding: old_agent.completed_onboarding,
                salesforce_id: old_agent.salesforce_id,
                created_by: old_agent.created_by,
                user_profile_attributes: {
                    :first_name => old_agent.user_profile.first_name,
                    :last_name => old_agent.user_profile.last_name,
                    :home_phone => old_agent.user_profile.home_phone,
                    :mobile_phone => old_agent.user_profile.mobile_phone,
                    :dob => old_agent.user_profile.dob,
                    :sex => old_agent.user_profile.sex,
                    :company => old_agent.user_profile.company,
                    :website => old_agent.user_profile.website,
                    :bio => old_agent.user_profile.bio,
                    :message => old_agent.user_profile.message,
                    :head_shot => get_base_64(old_agent.user_profile.head_shot),
                    :logo => get_base_64(old_agent.user_profile.logo),
                    :address_attributes => {
                        :country => old_agent.user_profile.address.country,
                        :address1 => old_agent.user_profile.address.address1,
                        :address2 => old_agent.user_profile.address.address2,
                        :city => old_agent.user_profile.address.city,
                        :state => old_agent.user_profile.address.state,
                        :zip => old_agent.user_profile.address.zip,
                        :lat => old_agent.user_profile.address.lat,
                        :long => old_agent.user_profile.address.long
                    }
                }
            },
            new_agent_id: new_agent.id,
            new_agent_user_profile_id: new_agent.user_profile.id,
            new_agent_address_id: new_agent.user_profile.address.id,
            new_agent: {
                email: new_agent.email,
                role: new_agent.role,
                sign_in_count: new_agent.sign_in_count,
                completed_onboarding: new_agent.completed_onboarding,
                salesforce_id: new_agent.salesforce_id,
                created_by: new_agent.created_by,
                user_profile_attributes: {
                    :first_name => new_agent.user_profile.first_name,
                    :last_name => new_agent.user_profile.last_name,
                    :home_phone => new_agent.user_profile.home_phone,
                    :mobile_phone => new_agent.user_profile.mobile_phone,
                    :dob => new_agent.user_profile.dob,
                    :sex => new_agent.user_profile.sex,
                    :company => new_agent.user_profile.company,
                    :website => new_agent.user_profile.website,
                    :bio => new_agent.user_profile.bio,
                    :message => new_agent.user_profile.message,
                    :head_shot => get_base_64(new_agent.user_profile.head_shot),
                    :logo => get_base_64(new_agent.user_profile.logo),
                    :address_attributes => {
                        :country => new_agent.user_profile.address.country,
                        :address1 => new_agent.user_profile.address.address1,
                        :address2 => new_agent.user_profile.address.address2,
                        :city => new_agent.user_profile.address.city,
                        :state => new_agent.user_profile.address.state,
                        :zip => new_agent.user_profile.address.zip,
                        :lat => new_agent.user_profile.address.lat,
                        :long => new_agent.user_profile.address.long
                    }
                }
            },
            new_agent_contractor_id: new_agent.contractor&.id,
            shared_partners: PartnerUser.where(:partner_id => new_agent.partner_ids, :user_id => old_agent.id).distinct.pluck(:partner_id), # ids of partners shared between the two agents
            old_agent_binder_ids: UserBinder.where(:user_id => old_agent.id).distinct.pluck(:binder_id),
            old_agent_user_binder_ids: UserBinder.where(:user_id => old_agent.id).pluck(:id),
            old_agent_partner_user_ids: PartnerUser.where(:user_id => old_agent.id, :role => "agent").pluck(:id),
            old_agent_partner_client_ids: PartnerClient.where(:client_id => old_agent.id).pluck(:id),
            old_agent_dashboard_ids: Dashboard.where(:user_id => old_agent.id).pluck(:id),
            old_agent_binder_item_ids: BinderItem.where(:user_id => old_agent.id).pluck(:id),
            old_agent_transfers_received: Binder::Transfer.where(:receiver_id => old_agent.id).pluck(:id),
            old_agent_transfers_sent: Binder::Transfer.where(:sender_id => old_agent.id).pluck(:id),
            old_agent_shares_received: Binder::Share.where(:shared_with_id => old_agent.id).pluck(:id),
            old_agent_shares_sent: Binder::Share.where(:shared_by_id => old_agent.id).pluck(:id),
            old_agent_user_contractors: UserContractor.where(:user_id => old_agent.id).pluck(:id),
            old_agent_organization_users: OrganizationUser.where(:user_id => old_agent.id).pluck(:id),
            old_agent_partner_binders: PartnerBinder.where(:client_id => old_agent.id).pluck(:id),
            old_agent_dashboards: Dashboard.where(:user_id => old_agent.id).pluck(:id),
            old_agent_binder_items: BinderItem.where(:user_id => old_agent.id).pluck(:id),
            old_agent_user_binder_transactions: UserBinderTransaction.where(:user_id => old_agent.id).pluck(:id),
            old_agent_user_resources: UserResource.where(:user_id => old_agent.id).pluck(:id),
            old_agent_contractor_id: old_agent.contractor&.id,
            old_agent_binder_contractor_ids: Binder::BinderContractor.where(:contractor_id => old_agent.contractor&.id).pluck(:id),
            old_agent_partner_contractor_ids: PartnerContractor.where(:contractor_id => old_agent.contractor&.id).pluck(:id)
        }
    end
    
    # delete the old agent's PartnerUsers and PartnerClients for shared partners so there aren't duplicates after the merge
    def delete_shared_partner_data(shared_partners, old_agent_id)
        shared_partners.uniq.each do |partner_id|
            PartnerUser.where(:partner_id => partner_id, :user_id => old_agent_id, :role => "agent").destroy_all
            PartnerClient.where(:partner_id => partner_id, :client_id => old_agent_id).destroy_all
        end
    end
    
    def get_agent_data(agent)
        branding = agent.user_profile.branding
        branding[:id] = agent.id
        return branding
    end
    
    def get_base_64(image)
        return nil if !image.present?
        file = Paperclip.io_adapters.for(image.expiring_cloud_front_url)
        downloaded = open(image.expiring_cloud_front_url).read
        "data:#{file.content_type};base64," + Base64.encode64(downloaded)
    rescue
    end
end