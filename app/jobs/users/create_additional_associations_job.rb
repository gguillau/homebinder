module Users
    class CreateAdditionalAssociationsJob
        include Sidekiq::Worker
        sidekiq_options queue: 'critical'
        
        def perform(id)
            user = User.find(id)

            if user.user_profile.nil?
                user.create_user_profile
            end

            if user.contractor.nil? && (
                user.role === "inspector" ||
                user.role === "broker" ||
                user.role === "agent" ||
                user.role === "lender" ||
                user.role === "apr_reviewer" ||
                user.role === "builder" ||
                user.role === "property_manager" ||
                user.role === "homepro"
                )
                contractor_name = user.user_profile.company.blank? ? user.user_profile.display_name : user.user_profile.company
                cont = Contractor.new(
                    {
                        :user_id => user.id,
                        :name => contractor_name,
                        :phone => user.user_profile.mobile_phone,
                        :email => user.email,
                        :contact => user.user_profile.display_name,
                        :url => user.user_profile.website,
                        :created_by => user.created_by || user.id
                    }
                )

                case user.role
                when "inspector", "apr_reviewer"
                    type = Contractor::Category.find_or_create_by(:name => "Home Inspector")
                    cont.types << type
                    cont.sub_types << type.sub_types
                when "lender"
                    type = Contractor::Category.find_or_create_by(:name => "Lender")
                    cont.types << type
                    cont.sub_types << type.sub_types
                when "builder"
                    type = Contractor::Category.find_or_create_by(:name => "Builder")
                    cont.types << type
                    cont.sub_types << type.sub_types
                when "property_manager"
                    type = Contractor::Category.find_or_create_by(:name => "Property Manager")
                    cont.types << type
                    cont.sub_types << type.sub_types
                when "homepro"
                    type = Contractor::Category.find_or_create_by(:name => "Not Defined")
                    cont.types << type
                    cont.sub_types << type.sub_types
                else
                    type = Contractor::Category.find_or_create_by(:name => "Real Estate")
                    cont.types << type
                    cont.sub_types << type.sub_types
                end

                if not cont.save
                    ErrorService.perform_async(cont.errors.full_messages.first, {user_id: id})
                end
            end
        end
    end
end