class RepairPricerReports::SetChargeJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    def perform(id, charge_id)
        repair_pricer_report = RepairPricerReport.find_by_id(id)
        return if repair_pricer_report.nil?
        
        charge = Stripe::Charge.retrieve(charge_id)
        repair_pricer_report.stripe_charge_id = charge.id
        repair_pricer_report.amount_charged_cents = charge.amount
        repair_pricer_report.payment_status = "paid"
        repair_pricer_report.save!
    end
end