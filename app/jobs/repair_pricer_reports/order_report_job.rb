class RepairPricerReports::OrderReportJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    def perform(id)
        repair_pricer_report = RepairPricerReport.find_by_id(id)
        return if repair_pricer_report.nil?
        
        raise BadRequestException.new "Inspection Report Required" if !repair_pricer_report.inspection_report?
        raise BadRequestException.new "Creator Required" if !repair_pricer_report.creator.present?
        
        inspid = nil
        
        if repair_pricer_report.partner.present?
            inspid = repair_pricer_report.partner.partner_configuration.repair_pricer_id
        end
        
        # if the creator was an agent, set the repair_pricer_token to nil
        if repair_pricer_report.creator.role === "agent"
            UserBinder.where(:binder_id => repair_pricer_report.binder_id, :user_id => repair_pricer_report.creator.id).update(:repair_pricer_token => nil)
        end
        
        # call repair pricer
        payload = {
            id: id,
            fileurl: repair_pricer_report.inspection_report.expiring_cloud_front_url(7.days),
            fname: repair_pricer_report.creator.user_profile.first_name,
            lname: repair_pricer_report.creator.user_profile.last_name,
            email: repair_pricer_report.creator.email,
            phone: repair_pricer_report.creator.user_profile.mobile_phone,
            address1: repair_pricer_report.address.address1,
            address2: repair_pricer_report.address.address2,
            city: repair_pricer_report.address.city,
            state: repair_pricer_report.address.state,
            zip: repair_pricer_report.address.zip,
            inspid: inspid,
            pool_report: repair_pricer_report.order_pool_report,
            rush_report: repair_pricer_report.rush_report,
            home_history_report: repair_pricer_report.order_home_history_report,
            pool_url: repair_pricer_report.pool_report.expiring_cloud_front_url(7.days),
            report_type: repair_pricer_report.report_type
        }

        # create report
        RepairPricer::Client.new.create_report(payload)
        
        # send notification to creator
        RepairPricerMailer.send_repair_pricer_confirmation(repair_pricer_report.creator&.id, repair_pricer_report.binder_id).deliver_later
        
        # send partner email notification if the partner has prepaid enabled and they are not the creator
        is_non_partner = ["agent", "homeowner", "broker"].include? repair_pricer_report.creator.role
        if repair_pricer_report.partner.present? && repair_pricer_report.partner.partner_configuration.repair_pricer_pre_paid_reports && is_non_partner
            RepairPricerMailer.send_inspector_notification(repair_pricer_report.creator&.id, repair_pricer_report.partner.id, repair_pricer_report.binder_id).deliver_later
        end
    end
end