module Kpi
    class SummaryReportJob
        include Sidekiq::Worker
        sidekiq_options queue: 'analytics'
        
        def perform(user_id)
            KpiMailer.send_kpi_metrics(user_id).deliver_later
        end
    end
end