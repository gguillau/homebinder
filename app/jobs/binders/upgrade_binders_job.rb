class Binders::UpgradeBindersJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    def perform
        binder_ids = Binder.joins(:partners).includes(:subscription).where(:subscriptions => {:id => nil}).distinct.pluck(:id)
        binder_ids.each do |binder_id|
            binder = Binder.find_by_id(binder_id)

            next if binder.nil?
            next if binder.status === "orphan"

            partner = binder.partners.first
            next if partner.get_admin.nil?
            
            hb_request = HBRequest.new.create_request(partner.get_admin)

            Subscription.new(binder_id: binder.id, plan_id: "free").save!
            BinderService.new(hb_request.user, binder).upgrade(partner.coupons[0].code)
        end
    end
end