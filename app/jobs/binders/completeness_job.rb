class Binders::CompletenessJob
    include Sidekiq::Worker
    sidekiq_options queue: 'non_critical'
    
    def perform(binder_id)
        binder = Binder.find_by_id(binder_id)
        return if binder.nil?
        
        score = 0
        
        # maintenance items
        if binder.maintenance_items.count >= 10
            score += 10
        else
            score += binder.maintenance_items.count 
        end
        
        # binder_contractors created by non homeowner
        if binder.binder_contractors.joins(:creator).where.not(:users => {:role => "homeowner"}).count >= 10
            score += 10
        else
            score += binder.binder_contractors.joins(:creator).where.not(:users => {:role => "homeowner"}).count
        end
        
        # binder_contractors created by homeowner
        if binder.binder_contractors.joins(:creator).where(:users => {:role => "homeowner"}).count >= 5
            score += 10
        else
            score += binder.binder_contractors.joins(:creator).where(:users => {:role => "homeowner"}).count * 2
        end
        
        # appliances with serial no, make or model
        if binder.appliances.where("manufacturer IS NOT NULL AND model_number IS NOT NULL").count >= 5
            score += 10
        else
            score += binder.appliances.where("manufacturer IS NOT NULL AND model_number IS NOT NULL").count * 2
        end
        
        # projects
        if binder.projects.count >= 5
            score += 10
        else
            score += binder.projects.count * 2
        end
        
        # permits
        if binder.permits.exists?
            score += 10
        end
        
        # documents
        if binder.documents.count >= 10
            score += 10
        else
            score += binder.documents.count 
        end
        
        # inspection_report
        if binder.documents.joins(:document_type).where(:document_types => {:name => "Home Inspection Report"}).exists?
            score += 10
        end
        
        # areas
        if binder.areas.count >= 5
            score += 10
        else
            score += binder.areas.count * 2
        end
        
        # paints
        if binder.paints.count >= 3
            score += 9
        else
            score += binder.paints.count * 3
        end
        
        # inventory_items
        if binder.inventory_items.count >= 10
            score += 10
        else
            score += binder.inventory_items.count
        end
        
        # purchase_price
        if binder.property.purchase_price.present?
            score += 5
        end
        
        # purchase_date
        if binder.property.purchase_date.present?
            score += 5
        end

        binder.completeness_score = (score.to_f/119.to_f).round(2)
        binder.save!
    end
end