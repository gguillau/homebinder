class Binders::BatchUploadJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    def perform(user_id, params)
        partner = Partner.find(params["partnerId"])
        template = partner.partner_configuration.default_binder_template
        number_of_binders = 0
        number_of_duplicates = 0
        existing_properties = []
        params["binders"].each do |binder|
            client =  {first: binder["first_name"], last: binder["last_name"], email: binder["email"], phone: binder["phone"]}
            property = {address: binder["address1"], address2: binder["address2"], city: binder["city"], state: binder["state"], postalcode: binder["zip"], country: binder["country"]}
            parameters = {:key => partner.partner_key, :client => client, :property => property, :binder_template_id => template.id}
            begin 
                # check for existing duplicate
                Partner::Automation::Service.verify_duplicate(parameters)
                raise BadRequestException if existing_property?(existing_properties, property)
                Partner::Automation::Service.delay(queue: 'abs').create_binder(parameters)
                number_of_binders += 1
                existing_properties.push(property)
            rescue
                number_of_duplicates += 1
                next
            end
        end
        AccountMailer.batch_binder_upload_confirmation(user_id, number_of_binders, number_of_duplicates).deliver_later
    end
    
    def existing_property?(existing_properties, property)
        exists = false
        existing_properties.each do |existing_property|
            existing = "#{existing_property[:address]} #{existing_property[:city]} #{existing_property[:state]}"
            pending = "#{property[:address]} #{property[:city]} #{property[:state]}"
            exists = true if existing === pending
        end
        exists
    end
end