class Binders::EnableMaintenanceRemindersJob
    include Sidekiq::Worker
    sidekiq_options queue: 'non_critical'
    
    def perform(binder_id)
        binder = Binder.find_by_id(binder_id)
        return if binder.nil?
        
        binder.maintenance_items.each do |item|
            item.email_notifications = true
            if not item.save
                ErrorService.perform_async(item.errors.full_messages.first, {:task => "enable_maintenance_reminders"})
            end
        end
    end
end