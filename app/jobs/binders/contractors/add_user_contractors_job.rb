class Binders::Contractors::AddUserContractorsJob
    include Sidekiq::Worker
    sidekiq_options queue: 'abs'
    
    def perform(user_contractor_id)
        user_contractor = UserContractor.find_by_id(user_contractor_id)
        return if user_contractor.nil?
        
        user = user_contractor.user
        return if user.nil?
        
        binder_ids = Binder.where(:created_by => user.id).ids
        all_ids = user.binder_ids + binder_ids
        all_ids.uniq.each do |binder_id|
            next if Binder::BinderContractor.where(:binder_id => binder_id, :user_contractor_id => user_contractor_id).exists?
            add_user_contractor_to_binder(user_contractor_id, binder_id)
        end
    end
    
    def add_user_contractor_to_binder(user_contractor_id, binder_id)
        user_contractor = UserContractor.find_by_id(user_contractor_id)
        return if user_contractor.nil?
        
        binder = Binder.find_by_id(binder_id)
        return if binder.nil?
        
        owner = binder.owner
        
        binder_contractor = OpenStruct.new({
            :user_contractor_id => user_contractor.id,
            :secondary_details => user_contractor.secondary_notes,
            :name => user_contractor.name,
            :secondary_name => user_contractor.secondary_name,
            :secondary_email => user_contractor.secondary_email,
            :secondary_phone => user_contractor.secondary_phone,
            :notes => user_contractor.notes,
            :contractor_id => user_contractor.contractor_id
        })

        types = user_contractor.types.map { |type| {:contractor_category_id => type.id} }
        sub_types = user_contractor.sub_types.map { |sub_type| {:contractor_sub_category_id => sub_type.id} }
        
        Partner::Automation::Service.delay(queue: 'abs').add_as_contractor(binder_contractor, owner.id, binder.id, types, sub_types)
    end
end