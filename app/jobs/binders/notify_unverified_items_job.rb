class Binders::NotifyUnverifiedItemsJob
    include Sidekiq::Worker
    sidekiq_options queue: 'low'
    
    def perform
        binders = Binder.joins(:binder_items)
                        .where(:binder_items => {:verified => false})
                        .where("binder_items.last_notify_date < ? OR binder_items.last_notify_date IS NULL", 7.days.ago)
                        .distinct
                        
        binders.find_each do |binder|
            return if binder.owner.nil?

            # send the email
            BinderMailer.notify_of_unverified_items(binder.id).deliver_later

            # update the items' last_notify_date
            binder.binder_items
                  .where(:binder_items => {:verified => false})
                  .where("binder_items.last_notify_date < ? OR binder_items.last_notify_date IS NULL", 7.days.ago)
                  .update_all(:last_notify_date => Date.today)
        end
    end
end