class Binders::RemovalJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    def perform(binder_id)
        binder = Binder.find_by_id(binder_id)
        return if binder.nil?
        
        # remove all roles on the binder
        User.with_role(:owner, binder).each do |o|
            o.remove_role(:owner, binder)
        end

        User.with_role(:co_owner, binder).each do |co|
            co.remove_role(:co_owner, binder)
        end

        User.with_role(:reader, binder).each do |r|
            r.remove_role(:reader, binder)
        end

        User.with_role(:reader_writer, binder).each do |rw|
            rw.remove_role(:reader_writer, binder)
        end

        binder_id = binder.id
        binder.destroy

        # remove from salesforce
        Salesforce::Binder.new.delete_salesforce_object(binder_id)
    end
end