class Binders::RequestServicesJob
    include Sidekiq::Worker
    sidekiq_options queue: 'default'
    
    def perform(user_id, binder_id, params)
        user = User.find_by_id(user_id)
        binder = Binder.find_by_id(binder_id)
        return if user.nil?
        return if binder.nil?
        
        services = params["services"]

        if params["email"] === "support@homebinder.com"
            # one source
            if services.include?("Setting up utilities (gas, electric, water)") || services.include?("Installing home security systems") || services.include?("Choosing cable, internet or satellite TV providers")
                OneSourceService.new.request_services(binder)
            end
            # porch
            if services.include?("Planning projects or getting quotes from local home pros")
                AccountMailer.send_services_requested(user.id, binder.id, ["Planning projects or getting quotes from qualified professionals in your area"], ENV["PORCH_EMAIL"], "Help with Projects").deliver_later
            end

            params["services"].each do |service|
                Salesforce::Binder::ServiceRequest.new.new_service_request(service, binder, "requested")
            end
        else
            AccountMailer.send_services_requested(user.id, binder.id, services, params["email"], "Third Party Services ").deliver_later
        end
        
        services.each do |service|
            # analytics
            EventService.perform_async({:event_name => service, :event_type => "services_requested", :binder_id => binder.id, :user_id => user.id, :user_role => user.role, :user_created_at => user.created_at})
        end
    end
end