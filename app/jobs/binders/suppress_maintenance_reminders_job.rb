class Binders::SuppressMaintenanceRemindersJob
    include Sidekiq::Worker
    sidekiq_options queue: 'non_critical'
    
    def perform(binder_id)
        binder = Binder.find_by_id(binder_id)
        return if binder.nil?
        
        binder.maintenance_items.each do |item|
            item.email_notifications = false
            if not item.save
                ErrorService.perform_async(item.errors.full_messages.first, {:task => "suppress_maintenance_reminders"})
            end
        end
    end
end