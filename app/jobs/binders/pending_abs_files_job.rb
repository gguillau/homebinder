class Binders::PendingAbsFilesJob
    include Sidekiq::Worker
    sidekiq_options queue: 'critical'
    
    def perform(abs_file_id)
        abs_file = AbsFile.find(abs_file_id)
        abs_file.status = 1
        abs_file.save!
        
        content = Paperclip.io_adapters.for(abs_file.payload).read
        params = JSON.parse(content, symbolize_names: true)

        binder = abs_file.binder
        transfer = binder.transfers.first
        homeowner = binder.transfers.first.receiver
        partner = binder.creator.partners.first

        # find the admin
        user = partner.get_admin

        # check if template can be used
        template = Partner::Automation::Service.verify_template(params, user)

        # make the user and partner the owner of the binder
        owner = Partner::Automation::Service.set_binder_owner(binder, partner, params[:partner_email])

        # automatically transfer the binder if the client email is orphans
        Partner::Automation::Service.transfer_to_orphans(binder, owner, transfer, homeowner)

        # add the transfer note to the binder details
        Partner::Automation::Service.delay(queue: 'abs').add_transfer_note(binder.id, template.id)

        # add appliances to binder
        Partner::Automation::Service.delay(queue: 'abs').add_appliances(params[:appliances], binder.id, template.id, user.id)

        # add maintenance items to binder
        Partner::Automation::Service.delay(queue: 'abs').add_maintenance_items(params[:maintenance_items], binder.id, template.id, user.id)

        # add contractors to binder
        Partner::Automation::Service.delay(queue: 'abs').add_contractors(params[:home_professionals], binder.id, template.id, user.id)

        # add projects to binder
        Partner::Automation::Service.delay(queue: 'abs').add_projects(params[:projects], binder.id)

        # add documents to binder
        Partner::Automation::Service.delay(queue: 'abs').add_template_docs(binder.id, template.id)

        component_options = {
            :buyer_agent => params[:buyer_agent],
            :seller_agent => params[:seller_agent],
            :agent => params[:agent],
            :partner_email => params[:partner_email],
            :inspection_report_url => params[:inspection_report_url],
            :transaction_type => params[:transaction_type],
            :project_number => params[:project_number]
        }

        # add binder components
        Partner::Automation::Service.delay(queue: 'abs').add_components(binder.id, user.id, partner.id, component_options, homeowner.id)

        Partner::Automation::Service.create_documents(binder.id, params.dig(:documents))
        Partner::Automation::Service.create_images(binder.id, params)

        abs_file.status = 2
        abs_file.save!
    end
end
