class Binders::MaintenanceRemindersJob
  include Sidekiq::Worker
  sidekiq_options queue: 'critical'
  
  USER = "homeowner".freeze
  MAINTENANCE_EMAIL = "maintenance_email".freeze
  TIME_ZONE = "Eastern Time (US & Canada)".freeze
  
  def perform
    # get the due date
    tomorrow = DateTime.now.tomorrow.to_date
    # find all binders with maintenance items that have enabled email notifications and a due date set to tomorrow
    binder_ids = Binder.joins(:maintenance_items, :maintenance_items => :maintenance_events).where(:maintenance_events => {:do_date => tomorrow}, :maintenance_items => {:email_notifications => true}).distinct.pluck(:id)
    
    # iterate through each binder
    Binder.where(:id => binder_ids).find_each do |binder|
      owner = User.with_role(:owner, binder).where(:role => "homeowner").where("sign_in_count > 0").first
      coOwners = User.with_role(:co_owner, binder).where(:role => "homeowner").where("sign_in_count > 0")
      next if owner.nil?
      
      # get the items
      items = binder.maintenance_items.joins(:maintenance_events).where(:maintenance_events => {:do_date => tomorrow})
      
      # iterate through the items
      items.each do |m|
        # get the next event
        event = m.next_event

        # move to the next item unless we have an event
        next unless event.present?

        # move to the next item unless the item do date is tomorrow
        next unless event.do_date.present? and event.do_date.to_date === tomorrow
        
        # make sure we didn't already send an email for this event
        next if event.last_notify_date
        
        # get the due date
        do_date = event.do_date
        
        # send email to the owner
        MaintenanceNotifyMailer.notify_email(event.id, binder.id, owner.id, do_date.strftime("%m-%d-%Y")).deliver_later
        PartnerClient.where(:client_id => owner.id).update(:last_maintenance_notification_date => Date.today)
        
        # analytics
        EventService.perform_async({:event_name => m.name, :event_type => "maintenance_reminder", :binder_id => binder.id, :user_id => owner.id, :user_role => owner.role, :user_created_at => owner.created_at})
        EventService.perform_async({:event_name => "maintenance_reminder", :event_type => "email", :binder_id => binder.id, :user_id => owner.id, :user_role => owner.role, :user_created_at => owner.created_at})
        
        #send email to the co owners
        coOwners.each do |coOwner|
          MaintenanceNotifyMailer.notify_email(event.id, binder.id, coOwner.id, do_date.strftime("%m-%d-%Y")).deliver_later
          PartnerClient.where(:client_id => coOwner.id).update(:last_maintenance_notification_date => Date.today)
          EventService.perform_async({:event_name => "maintenance_reminder", :event_type => "email", :binder_id => binder.id, :user_id => coOwner.id, :user_role => coOwner.role, :user_created_at => coOwner.created_at})
        end
        
        schedule_next_event(event.id, m.id)
      end
    end
  end
  
  def schedule_next_event(event_id, item_id)
    m = Binder::MaintenanceItem.find(item_id)
    event = Binder::MaintenanceItem::MaintenanceEvent.find(event_id)
    
    # save the event
    event.last_notify_date = Date.current
    event.email_status = "pending"
    event.save!  
    
    # schedule the next event
    m.schedule_next_event
  end
  
end