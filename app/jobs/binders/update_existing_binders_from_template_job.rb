class Binders::UpdateExistingBindersFromTemplateJob
    include Sidekiq::Worker
    sidekiq_options queue: 'abs'
    
    def perform(template_contractor_id)
        template_contractor = Template::Contractor.find_by_id(template_contractor_id)
        return if template_contractor.nil?
        
        binder_ids = Binder.where(:binder_template_id => template_contractor.binder_template_id).distinct.pluck(:id)
        binder_ids.each do |binder_id|
            update_existing_binder_from_template(template_contractor_id, binder_id)
        end
    end
    
    def update_existing_binder_from_template(template_contractor_id, binder_id)
        template_contractor = Template::Contractor.find_by_id(template_contractor_id)
        return if template_contractor.nil?

        template = BinderTemplate.find_by_id(template_contractor.binder_template_id)
        return if template.nil?

        binder = Binder.find_by_id(binder_id)
        return if binder.nil?

        owner = binder.owner
        return if owner.nil?

        binder_contractor = OpenStruct.new({
            :binder_template_id => template_contractor.binder_template_id,
            :library_source_id => template_contractor.id,
            :secondary_details => template_contractor.partner_contractor.secondary_notes,
            :name => template_contractor.partner_contractor.name,
            :secondary_name => template_contractor.partner_contractor.secondary_name,
            :secondary_email => template_contractor.partner_contractor.secondary_email,
            :secondary_phone => template_contractor.partner_contractor.secondary_phone,
            :notes => template_contractor.partner_contractor.notes,
            :contractor_id => template_contractor.partner_contractor.contractor_id
        })
        
        types = template_contractor.partner_contractor.types.map { |type| {:contractor_category_id => type.id} }
        sub_types = template_contractor.partner_contractor.sub_types.map { |sub_type| {:contractor_sub_category_id => sub_type.id} }
        
        Partner::Automation::Service.delay_for(2.minutes, queue: 'abs').add_as_contractor(binder_contractor, owner.id, binder.id, types, sub_types)
    end
end