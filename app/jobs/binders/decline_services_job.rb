class Binders::DeclineServicesJob
    include Sidekiq::Worker
    sidekiq_options queue: 'non_critical'
    
    def perform(user_id, binder_id, params)
        user = User.find_by_id(user_id)
        binder = Binder.find_by_id(binder_id)
        return if user.nil?
        return if binder.nil?
        
        services = ["Choosing satellite TV, cable or internet providers", "Installing home security systems", "Setting up utilities (gas, electric, water)", "Planning projects or getting quotes from qualified professionals in your area"]

        if params[:email] === "support@homebinder.com"
            # create the requests in salesforce
            services.each do |service|
                Salesforce::Binder::ServiceRequest.new.new_service_request(service, binder, "skipped")
            end
        end

        services.each do |service|
            # analytics
            EventService.perform_async({:event_name => service, :event_type => "services_declined", :binder_id => binder.id, :user_id => user.id, :user_role => user.role, :user_created_at => user.created_at})
        end
    end
end