class InspectionReportJobSerializer < ActiveModel::Serializer
  attributes :id, :job_id, :status, :binder, :binder_id, :created_at, :updated_at, :partner, :inspection_report_location
  
  def inspection_report_location
    return nil if !object.inspection_report?
    object.inspection_report.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval])
  end
end
