class AddressSerializer < ActiveModel::Serializer
  attributes :country, :name, :address1, :address2, :city, :state, :zip
end