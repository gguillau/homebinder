class OrganizationSerializer < ObjectPermissionSerializer
  attributes :id, :name, :description
end