class BinderTemplateSerializer < ActiveModel::Serializer
  attributes :id, :name, :transfer_note, :contractor_templates, :system, :default, :partner
  
  has_many :appliance_templates
  has_many :maintenance_templates
  has_many :document_templates
  
  def contractor_templates
    object.contractor_templates.map{|temp| Template::ContractorSerializer.new(temp).attributes}
  end

end