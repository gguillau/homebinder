class PartnerContractorTypeSerializer < ActiveModel::Serializer
    attributes :id, :partner_contractor, :type
    
    def type
        object.contractor_category
    end
end