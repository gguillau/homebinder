class AnnualPropertyReviewPhotoSerializer < ActiveModel::Serializer

  attributes :id, :name, :file_size, :location, :annual_property_review_id, :permissions, 
  :annual_property_review_finding_id, :annual_property_review_capital_item_id, :description

  def permissions
    @permissions = PermissionSerializer.new(object, {scope: scope}).attributes
    return @permissions
  end
  
  def name
    return object.file_file_name
  end
  
  def file_size
    return object.file_file_size
  end
  
  def location
    if scope
      if scope[:action] == "index"
          object.file.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :large)
      else
          object.file.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :medium)
      end
    end
  end

end