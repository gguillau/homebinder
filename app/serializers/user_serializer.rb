class UserSerializer < ActiveModel::Serializer

  SHOW = "show".freeze
  INDEX = "index".freeze

  attributes :id, :email, :role, :sign_in_count, :last_sign_in_at, :created_at, :accepted_transfer_at, :user_profile_attributes,
  :salesforce_id, :completed_onboarding, :create_method, :partner_users, :creator
  
  has_many :partner_users

  def user_profile_attributes
    UserProfileSerializer.new(object.user_profile).attributes
  end

  def partner_role
    user = PartnerUser.where(:user_id => object.id).where("role = ? OR role = ?", "admin", "member").first
    return user.role unless user.nil?
    return nil
  end

  def partner_id
    # get the first partner user where the user is either an admin or member or apr_reviewer
    partner_user = PartnerUser.where(:user_id => object.id).where("role = ? OR role = ?", "admin", "member").first
    return partner_user.partner_id unless partner_user.nil?
    return object.partners[0].id
  end

  def attributes
    hash = super

    if object.partners.count > 0
      # partner_id is important only for partners admins or members
      hash[:partner_id] = partner_id
      hash[:partner_role] = partner_role
      partner = Partner.find(hash[:partner_id])
      hash[:partner_name] = partner.name
    end

    return hash
  end
  
  def creator
    creator = User.find_by_id(object.created_by)
    return creator if !creator.nil?
    return nil
  end

end
