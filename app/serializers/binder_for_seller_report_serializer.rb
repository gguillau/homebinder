class BinderForSellerReportSerializer < ActiveModel::Serializer
  attributes :id, :name, :primary
  
  has_one :property
end
