require 'stripe'

class SubscriptionSerializer < ActiveModel::Serializer
  attributes :id, :binder_id
  
  attr_accessor :customer
  attr_accessor :card
  
  def attributes
    hash = super
    wrapper = SubscriptionWrapper.new(object)
    hash[:binder] = wrapper.binder
    hash[:plan] = wrapper.plan
    hash[:payment_status] = wrapper.payment_status
    hash[:card_type] = wrapper.card_type
    hash[:last4] = wrapper.last4
    hash[:subtotal] = wrapper.subtotal
    hash[:discount] = wrapper.discount
    hash[:end_date] = wrapper.end_date
    hash[:property] = wrapper.property
    return hash
  end
  
end
