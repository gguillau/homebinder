class AnnualPropertyReviewSerializer < ActiveModel::Serializer
    attributes  :id, 
                :client_first_name, 
                :client_last_name, 
                :client_email, 
                :client_phone,
                :address1,
                :address2,
                :city,
                :state,
                :zip,
                :country,
                :client_comments,
                :status,
                :reviewer,
                :reviewer_company,
                :scheduled_for,
                :scheduled_on,
                :completed_on,
                :on_hold_on,
                :canceled_on,
                :cancel_reason,
                :reviewed_on,
                :payment_status,
                :property_reviewer_comments,
                :admin_reviewer_comments,
                :created_at,
                :reviewer_company_id,
                :uuid,
                :reviewer_company,
                :binder
                
                
    def client_phone
        Phone.new(object.client_phone, object.country)
    end

    def reviewer_company
        return nil if object.reviewer_company.nil?
        partner = object.reviewer_company
        attrs = {
            id: partner.id,
            name: partner.name,
            partner_key: partner.partner_key,
            partner_type: partner.partner_type,
            phone: Phone.new(partner.phone, "US"),
            logo: partner.logo.present? ? partner.logo.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :original) : nil
        }
        attrs
    end
    
    def partner_country
        return nil if object.reviewer_company.address.nil?
        return object.reviewer_company.address.country
    end
end