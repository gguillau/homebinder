class UserContractorSubTypeSerializer < ActiveModel::Serializer
    attributes :id, :user_contractor, :sub_type

    def sub_type
        object.contractor_sub_category
    end
end