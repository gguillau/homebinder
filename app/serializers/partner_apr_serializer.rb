class PartnerAprSerializer < ActiveModel::Serializer
  attributes :id, :name, :partner_type, :code, :contact, :phone, :email,
  :affiliate_code, :website, :email_display_name, :partner_key,
  :logo_file, :can_perform_apr, :annual_property_review_cost
  
  def logo_file
      return nil unless object.logo.present?
      return object.logo.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :thumb)
  end
  
  def phone
    object.phone.phony_formatted(:format => :international, :spaces => ' ')
  end
  
  def can_perform_apr
    object.partner_configuration.can_perform_apr
  end
  
  def annual_property_review_cost
    object.partner_configuration.annual_property_review_cost_cents
  end
end