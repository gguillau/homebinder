class WidgetExclusionSerializer < ActiveModel::Serializer
    attributes :id, :name, :widget_category, :widget, :user, :partner, :organization, :partner_id, :widget_id, :user_id
    
    def name
        return if object.widget.nil?
        object.widget.name
    end
    
    def widget
        object.widget
    end
    
    def user
        object.user
    end
    
    def partner
        object.partner
    end
    
    def organization
        object.organization
    end
end