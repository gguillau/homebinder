class PurchaseSerializer < ActiveModel::Serializer
  attributes :id, :date, :price, :store, :number
  
  def price
      return '%.2f' % object.price.fractional
  end 

  def number
      return object.price.to_i
  end
end
