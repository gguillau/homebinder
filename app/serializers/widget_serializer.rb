class WidgetSerializer < ObjectPermissionSerializer
  attributes :id,
             :key,
             :name,
             :description,
             :category,
             :configuration,
             :organization_restriction_id,
             :partner_restriction_id,
             :admin_restriction,
             :permissions
end