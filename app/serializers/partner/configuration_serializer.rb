class Partner::ConfigurationSerializer < ActiveModel::Serializer
  attributes :id, :maintenance_note, :partner_id, :default_binder_template_id, :default_binder_action, :automation_binder_action,
  :binder_action_delay_length, :send_agents_transfer_notification, :show_isn_tab, :default_user_branding_id, :default_user_branding_option,
  :can_perform_apr, :annual_property_review_cost, :allow_client_calls, :active_warranty_account, :default_transaction_type, :binder_templates,
  :repair_pricer_id, :repair_pricer_enabled, :repair_pricer_pre_paid_reports, :repair_pricer_messaging_enabled, :default_locale, :client_text_messaging_enabled,
  :broker_mint_api_key, :default_repair_pricer_report_type


  has_many :isn_users
  has_many :binder_templates

  def binder_templates
    object.binder_templates.sort_by(&:created_at) + object.binder_templates.reject(&:created_at)
  end

  def annual_property_review_cost
      object.annual_property_review_cost_cents / 100
  end

end
