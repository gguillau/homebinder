class Partner::Configuration::IsnUserSerializer < ActiveModel::Serializer
  attributes :id, :partner_configuration_id, :name, :company_key, :api_endpoint, :username, :password
end