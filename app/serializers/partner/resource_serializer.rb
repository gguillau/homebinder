class Partner::ResourceSerializer < ActiveModel::Serializer
  attributes :id, :partner_id
  
  has_many :documents
  has_many :images
  has_many :organization_documents

end