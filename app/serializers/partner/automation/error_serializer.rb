class Partner::Automation::ErrorSerializer < ActiveModel::Serializer
    attributes
    
    def attributes
        object.attributes
        object[:agent_phone] = agent_phone
        return object.attributes
    end
    
    def agent_phone
        Phone.new(object.agent_phone, object.property_country)
    end
end