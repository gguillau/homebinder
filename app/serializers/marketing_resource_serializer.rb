class MarketingResourceSerializer < ActiveModel::Serializer
  
    attributes :id, :document_file, :url, :image_file, :description, :name, :user_type, :resource_type, :document_content_type, :image_content_type, :document_file_name, :image_file_name
    
    def image_file
        object.image.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval])
    end
    
    def document_file
        object.document.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval])
    end
end
