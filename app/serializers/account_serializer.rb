class AccountSerializer < ActiveModel::Serializer
  attributes  :id,
              :account_number,
              :name,
              :manager_id,
              :account_type,
              :account_sub_type,
              :account_status,
              :account_sub_status,
              :payment_type,
              :billing_frequency,
              :trial_expiration,
              :billing_activation,
              :subscription_amount_cents,
              :transaction_amount_cents,
              :notes,
              :partners,
              :stripe_customer_id
              
  def partners
    object.partners.map { |p| p.id }
  end
  
end