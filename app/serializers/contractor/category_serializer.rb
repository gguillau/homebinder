class Contractor::CategorySerializer < ActiveModel::Serializer
    attributes :id, :name, :sub_types, :verified, :created_by
    
    def sub_types
        return object.sub_types
    end
end