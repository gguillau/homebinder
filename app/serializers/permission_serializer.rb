class PermissionSerializer < ActiveModel::Serializer
  def ability
    return scope ? scope[:ability] : nil
  end
  
  def attributes
    hash = super
    if scope
      return if ability.nil?
      hash[:can_read] = ability.can? :read, object
      hash[:can_write] = ability.can? :write, object      
      hash[:can_create] = ability.can? :create, object
      hash[:can_destroy] = ability.can? :destroy, object
      if object.is_a?(Binder)
        hash[:can_subscribe] = ability.can? :read, object
        hash[:can_share] = ability.can? :share, object
        hash[:can_transfer] = ability.can? :transfer, object
        hash[:can_view_master_report] = ability.can? :view_master_report, object
        hash[:can_create_seller_report] = ability.can? :read, object
        hash[:can_edit_seller_report] = ability.can? :write, object
        hash[:can_orphan] = ability.can? :orphan, object
        hash[:access_repair_pricer] = ability.can? :access_repair_pricer, object
      end
    end
    return hash
  end
  
end