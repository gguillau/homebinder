class WarrantySerializer < ActiveModel::Serializer
  attributes :id, :status, :expiration_date, :binder_id, :warranty_plan_id, :created_at, :updated_at, :binder, :warranty_plan,
            :warranty_company, :price, :partner, :client_first, :client_last, :client_email, :client_phone,  :address,
            :last_inspection, :name
            
  def name
    object.warranty_plan.name
  end
  
  def binder
    object.binder
  end
  
  def address
    object.address
  end
  
  def warranty_plan
    object.warranty_plan
  end
  
  def warranty_company
    object.warranty_company
  end
  
  def partner
    return nil if object.binder_transaction.nil?
    return nil if object.binder_transaction.partner_binder_transaction.nil?
    object.binder_transaction.partner_binder_transaction.partner
  end
  
  def price
    return nil if object.binder_transaction.nil?
    object.binder_transaction.transaction_cost.to_i
  end
  
  def last_inspection
    Binder::Transaction.where(:transaction_type => "buy_side_inspection", :binder_id => object.binder.id).last
  end
  
  def client_phone
    #Phone.new(object.client_phone, address.country)
  end
end
