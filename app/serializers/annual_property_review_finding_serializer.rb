class AnnualPropertyReviewFindingSerializer < ActiveModel::Serializer

    attributes :id,
        :annual_property_review_id,
        :name,
        :priority,
        :estimated_cost,
        :details,
        :recommended_homepro,
        :status,
        :homeowner_item
        
    has_many :annual_property_review_photos
    
    def recommended_homepro
        pro = Template::Contractor.where(:id => object.recommended_homepro_id).first
        
        return nil if pro.nil?
        return {
            id: pro.id,
            email: pro.partner_contractor.email,
            name: pro.partner_contractor.name,
            phone: Phone.new(pro.partner_contractor.phone, country(pro)),
            notes: pro.partner_contractor.notes,
            website: pro.partner_contractor.website
        }
    end
    
    def country(object)
        return object&.binder_template&.partner_configuration&.partner&.address&.country
    end
end
