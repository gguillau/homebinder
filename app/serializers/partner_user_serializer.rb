class PartnerUserSerializer < ActiveModel::Serializer
    attributes :id, :partner, :user, :role, :created_at, :updated_at, :access_repair_pricer
end
