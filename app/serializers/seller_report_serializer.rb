class SellerReportSerializer < ActiveModel::Serializer
  
  SHOW = "show".freeze
  
  attributes :id, :code, :public, :created_at, :binder, :binder_id, :show_project_costs

  def appliances
    object.appliances
  end
  
  def projects
	  object.projects
  end
  
  def maintenance_items
    object.maintenance_items
  end
  
  def paints
	  object.paints
  end
  
  def permits
	  object.permits
	  
	  binder_permits = Array.new
  	object.permits.each do |permit|
  	  permit = Binder::PermitSerializer.new(permit, {scope: scope}).attributes
  	  binder_permits << permit
  	end
  	return binder_permits
  end
  
  def finishes
	  object.finishes
  end
  
  def binder_contractors
    contractors = Array.new
  	object.contractors.each do |contractor|
  	  contractor = Binder::BinderContractorSerializer.new(contractor, {scope: scope}).attributes
  	  contractors << contractor
  	end
  	return contractors
  end
  
  def documents
    object.documents.each do |doc|
      doc.name = doc.file_file_name
      doc.location = S3FileService.url_for(doc)
    end
    return object.documents
  end
  
  def images
    object.images.each do |image|
      image.location = S3FileService.url_for(image)
    end
    return object.images
  end
  
  def binder
    BinderSerializer.new(object.binder, {except: [:seller_report, :partner], root: false, scope: scope})
  end
  
  def seller_report_logo
    branding = Binder::BinderBranding.where(:binder_id => object.binder.id, :scope => "seller_report").first
    return Binder::BinderBrandingSerializer.new(branding).attributes unless branding.nil?
    return nil
  end
  
  def attributes
    hash = super
    if scope
      if scope[:action] == SHOW
        hash[:appliances] = appliances
        hash[:projects] = projects
        hash[:maintenance_items] = maintenance_items
        hash[:paints] = paints
        hash[:finishes] = finishes
        hash[:binder_contractors] = binder_contractors
        hash[:documents] = documents
        hash[:images] = images
        hash[:permits] = permits
        hash[:seller_report_logo] = seller_report_logo
      end
    end
    return hash
  end
end