class AnnualPropertyReviewCapitalItemSerializer < ActiveModel::Serializer

    attributes :id,
        :annual_property_review_id,
        :name,
        :eul,
        :cost_range,
        :comments
        
    has_many :annual_property_review_photos
end
