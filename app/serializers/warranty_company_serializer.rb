class WarrantyCompanySerializer < ActiveModel::Serializer
  attributes :id, :name, :contact, :email, :secondary_email, :created_at, :updated_at
end
