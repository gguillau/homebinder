class Template::MaintenanceSerializer < ActiveModel::Serializer
  attributes :id, :binder_template_id, :name, :image_url, :frequency, :due_date, :notes, :initial_due_date_interval,
  :library_template, :description, :binder_template, :estimated_cost, :estimated_time, :maintenance_template_contractor_types, 
  :types, :video_url
  
  has_many :maintenance_template_contractor_types, each_serializer: Template::MaintenanceTemplateContractorTypeSerializer
  
  def image_url
    return nil unless object.image.present?
    object.image.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval])
  end
end