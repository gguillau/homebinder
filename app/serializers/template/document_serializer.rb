class Template::DocumentSerializer < ActiveModel::Serializer
  attributes :id, :binder_template_id, :name, :notes, :file_url, :size, :file_extension, :binder_template,
  :description, :library_template, :document_type_id

  def name
    File.basename(object.file_file_name, file_extension)
  end

  def size
    object.file_file_size
  end

  def file_extension
    File.extname(object.file_file_name)
  end

  def file_url
    object.file.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval])
  end
end