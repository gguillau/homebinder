class Template::MaintenanceTemplateContractorTypeSerializer < ActiveModel::Serializer
    attributes :id, :maintenance_template, :type
    
    def type
        object.contractor_category
    end
end