class Template::ContractorTypeSerializer < ActiveModel::Serializer
    attributes :id, :template_contractor, :type
    
    def type
        object.contractor_category
    end
end