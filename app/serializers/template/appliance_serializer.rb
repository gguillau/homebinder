class Template::ApplianceSerializer < ActiveModel::Serializer
  attributes :id, :binder_template_id, :name, :image_url, :notes, :description, :library_template, :binder_template, :image_file_name
  
  def image_url
    return nil unless object.image.present?
    object.image.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval])
  end
end