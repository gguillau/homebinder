class Template::ContractorSubTypeSerializer < ActiveModel::Serializer
    attributes :id, :template_contractor, :sub_type
    
    def sub_type
        object.contractor_sub_category
    end
end