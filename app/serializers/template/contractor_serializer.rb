class Template::ContractorSerializer < ActiveModel::Serializer
  attributes :id, :binder_template_id, :created_at, :updated_at, :partner_contractor_id, :partner_contractor
  
  def partner_contractor
    return nil if object.partner_contractor.nil?
    ::PartnerContractorSerializer.new(object.partner_contractor, {scope: scope}).attributes
  end
end