class UserContractorSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :name, :image_url, :phone, :email, :notes,
            :types, :sub_types, :address, :contractor_id, :secondary_name, :secondary_email,
            :secondary_phone, :secondary_notes, :user, :send_email_invitation, :contractor, :website,
            :user_contractor_types, :user_contractor_sub_types

  def user_contractor_types
    object.user_contractor_types.map{|type| UserContractorTypeSerializer.new(type).attributes}
  end

  def user_contractor_sub_types
    object.user_contractor_sub_types.map{|type| UserContractorSubTypeSerializer.new(type).attributes}
  end

  def image_url
    object.logo.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval])
  end

  def types
    return object.types
  end

  def sub_types
    return object.sub_types
  end

  def phone
    country = object.address.present? ? object.address.country : nil
    Phone.new(object.phone, country)
  end

  def secondary_phone
    country = object.address.present? ? object.address.country : nil
    Phone.new(object.secondary_phone, country)
  end

  def user
    return nil if object.contractor.nil?
    return nil if object.contractor.user.nil?
    return nil if object.contractor.user
  end
end