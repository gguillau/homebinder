class TagSerializer < ActiveModel::Serializer
  attributes :id, :tag, :tag_scope, :metadata
  
  def tag_scope
    object.scope
  end
end
