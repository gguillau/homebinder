class PartnerWarrantyPlanSerializer < ActiveModel::Serializer
    attributes :id, :price, :plan, :partner_id
    
    def price
        object.price.to_i
    end
    
    def plan
        WarrantyPlan.find_by_id(object.warranty_plan_id)
    end
end