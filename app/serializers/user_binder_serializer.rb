class UserBinderSerializer < ActiveModel::Serializer
    attributes :id, :user, :role, :binder, :access_repair_pricer, :repair_pricer_token
    
    def user
        UserSerializer.new(object.user, {:scope => scope}).attributes
    end
    
    def binder
        BinderSerializer.new(object.binder, {:scope => scope}).attributes
    end
end
