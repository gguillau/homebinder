class WarrantyPlanSerializer < ActiveModel::Serializer
  attributes :id, :name, :duration, :description, :cycle, :warranty_company_id, :price, :created_at, :updated_at, :warranty_company
  
  def warranty_company
      WarrantyCompany.find_by_id(object.warranty_company_id)
  end
  
  def price
      object.price.to_i
  end
end
