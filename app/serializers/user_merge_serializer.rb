class UserMergeSerializer < ActiveModel::Serializer
    attributes :id, :user, :old_agent_email, :old_agent_first_name, :old_agent_last_name, :status, :created_at, :updated_at
end
