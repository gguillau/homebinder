class UserContractorTypeSerializer < ActiveModel::Serializer
    attributes :id, :user_contractor, :type

    def type
        object.contractor_category
    end
end