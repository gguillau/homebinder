class PartnerBinderSerializer < ActiveModel::Serializer

    attributes :id, :partner, :role, :homeowner, :repair_pricer_enabled, :binder, :thanked
    
    def binder
        return nil if object.binder.nil?
        {
            id: object.binder.id,
            name: object.binder.name,
            property: object.binder.property,
            completeness_score: object.binder.completeness_score,
            create_method: object.binder.create_method,
            created_at: object.binder.created_at,
            custom_support_email: object.binder.custom_support_email,
            details: object.binder.details,
            subscription: object.binder.subscription,
            total_storage: object.binder.total_storage
        }
    end

    def homeowner
        return nil if object.user.nil?
        UserSerializer.new(object.user, {:scope => scope}).attributes
    end
    
    def partner
        return nil if object.partner.nil?
        PartnerSerializer.new(object.partner, {:scope => scope}).attributes
    end
end
