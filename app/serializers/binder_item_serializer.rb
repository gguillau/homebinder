class BinderItemSerializer < ActiveModel::Serializer
    attributes :id, :verified, :last_notify_date, :binder, :user, :item, :type
end
