class UserProfileSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :first_name, :last_name, :home_phone, :mobile_phone, :dob, :sex, :address_attributes,
  :company, :website, :bio, :message, :transfer_message_to_homeowner, :transfer_message_to_agent, 
  :transfer_message_to_prepaid_agent, :head_shot_file, :logo_file, :email, :monthly_email, :display_name, :role, 
  :full_role, :default_locale

  def email
    object.user.email
  end

  def address_attributes
    return AddressSerializer.new(object.address).attributes unless object.address.nil?
    return nil
  end

  def head_shot_file
      return nil unless object.head_shot.present?
      return object.head_shot.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :thumb)
  end

  def logo_file
      return nil unless object.logo.present?
      return object.logo.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :thumb)
  end

  def attributes
    hash = super

    if object.user.role === "agent"
      hash[:branding] = object.branding
    end

    return hash
  end
  
  def role
    object.user.role
  end
  
  def mobile_phone
    country = object.address.present? ? object.address.country : nil
    Phone.new(object.mobile_phone, country)
  end

end
