class PartnerSlimSerializer < ActiveModel::Serializer
  attributes :id, :name, :email
end