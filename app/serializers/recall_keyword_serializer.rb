class RecallKeywordSerializer < ActiveModel::Serializer
  attributes :id, :keyword
end