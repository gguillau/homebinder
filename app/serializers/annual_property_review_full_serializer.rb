class AnnualPropertyReviewFullSerializer < ActiveModel::Serializer
    attributes  :id, 
                :client_first_name, 
                :client_last_name, 
                :client_email, 
                :client_phone,
                :address1,
                :address2,
                :city,
                :state,
                :zip,
                :country,
                :client_comments,
                :status,
                :reviewer,
                :reviewer_company,
                :scheduled_for,
                :scheduled_on,
                :completed_on,
                :on_hold_on,
                :canceled_on,
                :cancel_reason,
                :reviewed_on,
                :property_reviewer_comments,
                :admin_reviewer_comments,
                :created_at,
                :payment_status,
                :reviewer_company_id,
                :uuid,
                :binder

    has_many :annual_property_review_findings
    has_many :annual_property_review_capital_items
    has_many :annual_property_review_photos, key: :photos, serializer: AnnualPropertyReviewPhotoSerializer
    
    def annual_property_review_photos
        object.annual_property_review_photos.where(:annual_property_review_finding_id => nil, :annual_property_review_capital_item_id => nil)
    end
    
    def client_phone
        Phone.new(object.client_phone, object.country)
    end

    def reviewer
        rev = User.includes(:user_profile).where(id: object.reviewer_id).first
        return nil unless rev.present?
        attrs = {
            id: rev.id,
            first_name: rev.user_profile.first_name,
            last_name: rev.user_profile.last_name
        }
        attrs
    end

    def reviewer_company
        return nil if object.reviewer_company.nil?
        partner = object.reviewer_company
        attrs = {
            id: partner.id,
            name: partner.name,
            partner_key: partner.partner_key,
            phone: Phone.new(partner.phone, "US"),
            logo: partner.logo.present? ? partner.logo.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :original) : nil
        }
        attrs
    end
    
    def partner_country
        return nil if object.reviewer_company.address.nil?
        return object.reviewer_company.address.country
    end
end