class ContractorSerializer < ActiveModel::Serializer
  include ActionView::Helpers::NumberHelper
  
  attributes :id, :name, :phone, :email, :url, :types, :sub_types, :address,
  :user_id, :contact, :details, :created_by, :user
  
  BINDER_CONTRACTOR = "binder_contractors".freeze
  SHOW = "show".freeze
  
  def phone
    country = object.address.present? ? object.address.country : nil
    Phone.new(object.phone, country)
  end
  
  def name
    object.name.titleize
  end
  
end

require_dependency 'template/contractor'