class BinderSerializer < ActiveModel::Serializer

  SHOW = "show".freeze
  ADMIN = "admin".freeze
  INDEX = "index".freeze
  SHARED = "Shared".freeze
  UNKNOWN = "Unknown".freeze
  TRANSFERRED = "Transferred".freeze
  SELLER_REPORTS = "seller_reports".freeze

  attributes :id, :name, :primary, :details, :created_at, :hero_image_id, :create_method, :binder_items_count, :total_storage,
  :custom_support_email, :binder_status, :completeness_score, :property
  
  def completeness_score
    object.completeness_score * 100
  end
  
  def binder_status
    return object.status
  end

  def permissions
    return PermissionSerializer.new(object, {scope: scope}).attributes
  end

  def branding_users
    object.binder_brandings.map {|branding| Binder::BinderBrandingSerializer.new(branding).attributes}
  end

  def partner_binders
    object.partner_binders.map {|partner_binder| PartnerBinderSerializer.new(partner_binder).attributes}
  end

  def user_binders
    object.user_binders.map {|user_binder| UserBinderSerializer.new(user_binder).attributes}
  end

  def hero_photo
    return nil if object.hero_image_id.nil?
    image = object.images.where(:id => object.hero_image_id).first
    return nil if image.nil?
    if scope
      if scope[:action] == SHOW
        image.file.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :large)
      else
        image.file.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :thumb)
      end
    end
  end

  def access_list
    begin
      return BinderService.new(scope[:current_user], object).get_acl
    rescue
      return []
    end
  end

  def binder_items_count
    object.binder_items.unverified.count
  end

  def creator
      return nil if object.creator.nil?
      return object.creator.user_profile.company
  end

  # status is only for partners
  # assumes that there is always a transfer on the binder

  def status
    # get the current user
    user = scope[:current_user]
    return object.transfer_status(user)
  end

  def recalls
    Binder::Appliance::ApplianceRecall.joins(:appliance).where(:appliances => {:binder_id => object.id}).count
  end

  def owner
    owner = User.joins(:user_binders).where(user_binders: { binder_id: object.id, role: UserBinderRoles::OWNER.to_s }).first
    return owner.attributes.except(:authentication_token) unless owner.nil?
    return nil
  end

  def subscription
    return nil if object.subscription.nil?
    SubscriptionSerializer.new(object.subscription).attributes
  end

  # returns pending transfer information

  def transfer
    # get the current user
    user = scope[:current_user]
    return if user.nil?

    case user.role
    when "admin"
        if object.transfers.where(:status => "created").count > 0
            transfer = object.transfers.where(:status => "created").order(:id).last
            return nil if transfer.nil?
            return nil if transfer.receiver.nil?
            return {
              id: transfer.id,
              sender_id: transfer.sender_id,
              receiver_id: transfer.receiver_id,
              receiver_email: transfer.receiver.email.present? ? transfer.receiver.email : nil
            }
        elsif object.pending_transfer
            transfer = object.transfers.where(:status => "sent").order(:id).last
            return nil if transfer.nil?
            return nil if transfer.receiver.nil?
            return {
              id: transfer.id,
              sender_id: transfer.sender_id,
              receiver_id: transfer.receiver_id,
              receiver_email: transfer.receiver.email.present? ? transfer.receiver.email : nil
            }
        end
    when "inspector", "broker", "lender", "homepro", "builder", "property_manager", "hoa", "insurance"
        # find the partner users
        users = Partner.get_partner_users(user.partners.first.id)

        if object.transfers.where(:sender_id => users, :status => "created").count > 0
            transfer = object.transfers.where(:status => "created").order(:id).last
            return nil if transfer.nil?
            return nil if transfer.receiver.nil?
            return {
              id: transfer.id,
              sender_id: transfer.sender_id,
              receiver_id: transfer.receiver_id,
              receiver_email: transfer.receiver.email.present? ? transfer.receiver.email : nil
            }
        elsif object.pending_transfer
            transfer = object.transfers.where(:sender_id => users, :status => "sent").order(:id).last
            return nil if transfer.nil?
            return nil if transfer.receiver.nil?
            return {
              id: transfer.id,
              sender_id: transfer.sender_id,
              receiver_id: transfer.receiver_id,
              receiver_email: transfer.receiver.email.present? ? transfer.receiver.email : nil
            }
        end
    when "agent"
        users = [user]

        if object.transfers.where(:sender_id => users, :status => "created").count > 0
            transfer = object.transfers.where(:status => "created").order(:id).last
            return nil if transfer.nil?
            return nil if transfer.receiver.nil?
            return {
              id: transfer.id,
              sender_id: transfer.sender_id,
              receiver_id: transfer.receiver_id,
              receiver_email: transfer.receiver.email.present? ? transfer.receiver.email : nil
            }
        elsif object.pending_transfer
            transfer = object.transfers.where(:sender_id => users, :status => "sent").order(:id).last
            return nil if transfer.nil?
            return nil if transfer.receiver.nil?
            return {
              id: transfer.id,
              sender_id: transfer.sender_id,
              receiver_id: transfer.receiver_id,
              receiver_email: transfer.receiver.email.present? ? transfer.receiver.email : nil
            }
        end
    end
  end

  def attributes
    hash = super
    if scope
      current_user = scope[:current_user]
      if current_user.present?
        user_binder = UserBinder.where(binder_id: object.id, user_id: current_user.id).first
        if user_binder.present?
          hash[:role] = user_binder.role
        end
      end

      if scope[:action] == INDEX
        # admin binders dashboard displays the owner of the binder
        if current_user.is_admin?
          hash[:owner] = owner
          hash[:hero_photo] = hero_photo
          hash[:permissions] = permissions
          hash[:pending_transfer] = object.pending_transfer
          hash[:transfer] = transfer
          hash[:partner_binders] = partner_binders
        end
        
        if current_user.is_homeowner?
          hash[:permissions] = permissions
          hash[:subscription] = subscription
        end

        # partner dashboard values
        if current_user.belongs_to_partner?
          hash[:status] = status
          hash[:recalls] = recalls
          hash[:hero_photo] = hero_photo
          hash[:permissions] = permissions
          hash[:pending_transfer] = object.pending_transfer
          hash[:transfer] = transfer
          hash[:creator] = creator
          hash[:seller_report] = object.seller_report
          hash[:partner_binders] = partner_binders
        end
        
        if current_user.is_agent?
          hash[:permissions] = permissions
          hash[:partner_binders] = partner_binders
        end
      elsif scope[:action] == SHOW and scope[:controller][0] === SELLER_REPORTS
          hash[:hero_photo] = hero_photo
      else

        hash[:owner] = owner
        hash[:permissions] = permissions
        hash[:hero_photo] = hero_photo

        hash[:partner_binders] = partner_binders
        hash[:branding_users] = branding_users
        hash[:user_binders] = user_binders
        hash[:transfer] = transfer

        hash[:access_list] = access_list
        hash[:seller_report] = object.seller_report
        hash[:subscription] = subscription

        # partner dashboard values
        if current_user && current_user.belongs_to_partner?
          hash[:status] = status
          hash[:recalls] = recalls
          hash[:pending_transfer] = object.pending_transfer
          hash[:transfer] = transfer
        end

      end
    end

    return hash
  end

end
