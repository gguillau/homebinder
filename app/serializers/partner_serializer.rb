class PartnerSerializer < ActiveModel::Serializer

  SHOW = "show".freeze
  INDEX = "index".freeze

  attributes :id, :name, :partner_type, :code, :contact, :phone, :email,
  :created_at, :affiliate_code, :account, :website, :email_display_name, :partner_key,
  :api_key, :completed_onboarding, :logo_file, :can_perform_apr,
  :annual_property_review_cost, :address
  
  def phone
    country = object.address.present? ? object.address.country : nil
    Phone.new(object.phone, country)
  end

  def admin_email
    admin = object.get_admin
    return admin ? admin.email : nil
  end

  def logo_file
      return nil unless object.logo.present?
      return object.logo.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :thumb)
  end

  def api_key
    key = ApiKey.find_by_partner_id(object.id)
    return key ? key.key : nil
  end

  def configuration
    configuration = object.partner_configuration
    if configuration.present?
      return {
        id: configuration.id,
        default_binder_action: configuration.default_binder_action,
        binder_action_delay_length: configuration.binder_action_delay_length.to_i,
        automation_binder_action: configuration.automation_binder_action,
        maintenance_note: configuration.maintenance_note,
        send_agents_transfer_notification: configuration.send_agents_transfer_notification,
        default_user_branding_id: configuration.default_user_branding_id,
        default_user_branding_option: configuration.default_user_branding_option,
        show_isn_tab: configuration.show_isn_tab,
        allow_client_calls: configuration.allow_client_calls,
        active_warranty_account: configuration.active_warranty_account,
        repair_pricer_enabled: configuration.repair_pricer_enabled,
        repair_pricer_id: configuration.repair_pricer_id,
        repair_pricer_pre_paid_reports: configuration.repair_pricer_pre_paid_reports,
        repair_pricer_messaging_enabled: configuration.repair_pricer_messaging_enabled,
        default_locale: configuration.default_locale,
        client_text_messaging_enabled: configuration.client_text_messaging_enabled,
        default_repair_pricer_report_type: configuration.default_repair_pricer_report_type
      }
    else
      return nil
    end
  end

  def warranties_configuration
    configuration = object.warranty_configuration
    return nil if configuration.nil?
    return {
      id: configuration.id
    }
  end

  def binder_count
    object.binders.count
  end

  def account
    account = Account.find_by_id(object.account_id)
    if account.present?
      return {
        id: account.id,
        account_number: account.account_number,
        name: account.name,
        account_status: account.account_status,
        account_sub_status: account.account_sub_status,
        account_sub_type: account.account_sub_type,
        stripe_customer_id: account.stripe_customer_id
      }
    else
      return {}
    end
  end

  def can_perform_apr
    object.partner_configuration.can_perform_apr
  end

  def annual_property_review_cost
    object.partner_configuration.annual_property_review_cost_cents
  end

  def attributes
    hash = super
    if scope
      if scope[:action] === INDEX
        hash[:configuration] = configuration
        hash[:admin_email] = admin_email
      else
        hash[:warranties_configuration] = warranties_configuration
        hash[:binder_count] = binder_count
        hash[:account] = account
        hash[:configuration] = Partner::ConfigurationSerializer.new(object.partner_configuration, {scope: scope}).attributes
      end
    else
      hash[:warranties_configuration] = warranties_configuration
      hash[:binder_count] = binder_count
      hash[:account] = account
      hash[:configuration] = Partner::ConfigurationSerializer.new(object.partner_configuration, {scope: scope}).attributes
    end

    return hash
  end

end
