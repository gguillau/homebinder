class PartnerContractorSubTypeSerializer < ActiveModel::Serializer
    attributes :id, :partner_contractor, :sub_type
    
    def sub_type
        object.contractor_sub_category
    end
end