class PartnerResourceSerializer < ActiveModel::Serializer
  
    attributes :id, :resource
    
    # get the partner
    def resource
        resource = MarketingResource.find_by_id(object.marketing_resource_id)
        return MarketingResourceSerializer.new(resource).attributes unless resource.nil?
        return nil
    end

end
