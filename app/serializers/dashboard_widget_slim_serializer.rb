class DashboardWidgetSlimSerializer < ActiveModel::Serializer
  attributes :id, :dashboard_id, :widget_id, :widget_key, :index, :configuration
  
  def widget_key
    object.widget.key
  end
  
  def configuration
    object.widget.configuration
  end
end