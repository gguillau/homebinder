class DashboardWidgetSerializer < ActiveModel::Serializer
  attributes :id, :dashboard_id, :widget_id, :index
  
  has_one :widget
end