class BuildFaxReportSerializer < ActiveModel::Serializer
    attributes :id, :name, :status, :binder_id, :partner_id, :created_at, :updated_at, :report, :address
    
    def name
        object.report_file_name
    end
    
    def report
        object.report.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval])
    end
end
