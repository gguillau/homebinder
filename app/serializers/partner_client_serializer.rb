class PartnerClientSerializer < ActiveModel::Serializer
    attributes :id, :partner, :client, :created_at
end
