class DashboardSerializer < ObjectPermissionSerializer
  attributes :id, :user_id, :partner_id, :partner, :organization_id, :organization, :name, :description, :system, :default, :permissions, :user
  
  has_many :dashboard_widgets, serializer: DashboardWidgetSlimSerializer
  
  def partner
    if object.partner_id.present?
      p = Partner.find_by_id(object.partner_id)
      return nil unless p.present?
      return PartnerSlimSerializer.new(p).attributes
    end
    return nil
  end
  
  def organization
    if object.organization_id.present?
      o = Organization.find_by_id(object.organization_id)
      return o
    end
    return nil
  end
  
  def attributes
    hash = super
    # scope is a method on ActiveModel::Serializer
    hash[:scope] = object.scope
    hash
  end
  
end