class ObjectPermissionSerializer < ActiveModel::Serializer
  def ability
    return scope ? scope[:ability] : nil
  end
  
  def permissions
    perm = {
      can_read: ability.can?(:read, object),
      can_write: ability.can?(:write, object),
      can_destroy: ability.can?(:destroy, object)
    }
    perm
  end
  
end