class Binder::PermitSerializer < ActiveModel::Serializer
  attributes :id, :status, :proposed_use, :work_class, :permit_type, :permit_number, :valuation_amount,
  :permit_date, :details, :binder_id, :binder_contractor
  
  has_many :tags
  has_one :seller_report_item
  
  def valuation_amount
    object.valuation_amount.to_i
  end
  
  def binder_contractor
    cont = Binder::BinderContractor.find_by_id(object.binder_contractor_id)
    if cont.present?
      return {
        id: cont.id,
        contact: cont.contractor.name
      }
    else
      return nil
    end
  end
end
