class Binder::BinderContractorSerializer < ActiveModel::Serializer

  attributes :id, :binder_id, :account_number, :contact, :details, :seller_report_item, :contractor, :permits,
              :types, :sub_types, :secondary_name, :secondary_email, :secondary_phone, :secondary_details,
              :worked_on_property, :send_email_invitation, :contractor_id, :binder_contractor_types,
              :binder_contractor_sub_types, :preferred_partner, :preferred_agent
  
  has_many :tags
  has_one :seller_report_item
  has_one :contractor
  has_many :binder_contractor_types,      each_serializer: Binder::BinderContractorTypeSerializer
  has_many :binder_contractor_sub_types,  each_serializer: Binder::BinderContractorSubTypeSerializer
  
  SHOW = "show".freeze
  SELLER_REPORTS = "seller_reports".freeze
  
  def preferred_partner
    return nil if object.library_source_id.nil?
    return nil if object.partner_configuration.nil?
    object.partner
  end
  
  def preferred_agent
    return nil if object.user_contractor_id.nil?
    object.user_contractor.user.user_profile
  end
  
  def contact
    if object.contact.nil? &&  object.contractor && object.contractor.name.present?
      return object.contractor.name.titleize
    elsif object.contact.present?
      return object.contact.titleize
    else
      return ""
    end
  end
  
  def permissions
    return PermissionSerializer.new(object, {scope: scope}).attributes
  end
  
  def attributes
    hash = super
    if scope && scope[:action] == SHOW and scope[:controller][0] === SELLER_REPORTS
      hash[:contractor] = ContractorSerializer.new(object.contractor, {root: false})
    else
      hash[:permissions] = permissions
      hash[:contractor] = ContractorSerializer.new(object.contractor)
    end
    return hash
  end
  
  def secondary_phone
    country = object.contractor.present? ? object.contractor.address.present? ? object.contractor.address.country : nil : nil
    Phone.new(object.secondary_phone, country)
  end
end
