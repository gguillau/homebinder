class Binder::BinderContractorTypeSerializer < ActiveModel::Serializer
    attributes :id, :binder_contractor, :type
    
    def type
        object.contractor_category
    end
end