class Binder::AreaSerializer < ActiveModel::Serializer
  attributes :id, :binder_id, :name, :area_type, :dimensions, :details
  
  has_one :permissions
  has_many :tags
  
  def permissions
    return PermissionSerializer.new(object, {scope: scope}).attributes
  end
end
