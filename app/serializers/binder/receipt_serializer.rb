class Binder::ReceiptSerializer < ActiveModel::Serializer
  attributes :id, :name, :details, :deductable, :binder_id

  has_many :tags
  has_one :purchase
  has_one :permissions

  def permissions
    return PermissionSerializer.new(object, {scope: scope}).attributes
  end

end
