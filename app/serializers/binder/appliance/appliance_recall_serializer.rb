class Binder::Appliance::ApplianceRecallSerializer < ActiveModel::Serializer
  attributes :id, :status, :recall, :appliance, :binder_owner, :category, :sub_category, :recall_url
  
  def binder_owner
    return nil if object.appliance.binder.nil?
    return nil if object.appliance.binder.owner.nil?
    object.appliance.binder.owner
  end
  
  def recall_url
    object.recall.recall_url
  end
end