class Binder::BinderContractorSubTypeSerializer < ActiveModel::Serializer
    attributes :id, :binder_contractor, :sub_type
    
    def sub_type
        object.contractor_sub_category
    end
end