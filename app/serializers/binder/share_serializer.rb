class Binder::ShareSerializer < ActiveModel::Serializer
  attributes :id, :shared_by_id, :role_name, :status, :shared_with_id, :sender, :receiver, :binder, :access_token, :binder_id
end
