class Binder::ImageSerializer < ActiveModel::Serializer

  attributes :id, :permissions, :name, :file_size, :location, :tags, :type, :binder, :is_hero
  
  has_one :seller_report_item
  has_many :tags
  
  def permissions
    @permissions = PermissionSerializer.new(object, {scope: scope}).attributes
    return @permissions
  end
  
  def name
    return object.name if object.name.present?
    return "" if object.file_file_name.nil?
    return object.file_file_name
  end
  
  def file_size
    return object.file_file_size
  end
  
  def location
    if scope
      if scope[:action] == "index"
          object.file.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :large)
      else
          object.file.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :medium)
      end
    end
  end
  
  def type
    return "image"
  end
  
  def is_hero
    return Binder.where(:hero_image_id => object.id).count > 0
  end

end
