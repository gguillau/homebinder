class Binder::MaintenanceItemSerializer < ActiveModel::Serializer
  attributes :id, :binder_id, :maintenance_cycle, :name, :details, :interval, :do_date, :last_event_date, :last_notify_date, :email_notifications, :binder_address,
  :binder, :library_source_id
  
  has_one :seller_report_item
  has_one :permissions
  has_many :tags
  
  def permissions
    PermissionSerializer.new(object, {scope: scope}).attributes
  end
  
  def do_date
	  object.do_date
  end
  
  def last_event_date
    object.get_last_completed_date
  end
  
  def last_notify_date
    object.get_last_notify_date
  end
  
  def binder_address
    object.binder.full_address
  end
  
  def binder
    BinderSerializer.new(object.binder, {scope: scope}).attributes
  end
end
