class Binder::FinishSerializer < ActiveModel::Serializer
  attributes :id, :binder_id, :name, :make, :model, :style_color, :details
  
  has_one :purchase
  has_one :permissions
  has_one :seller_report_item
  has_many :tags
  
  def permissions
    return PermissionSerializer.new(object, {scope: scope}).attributes
  end
end
