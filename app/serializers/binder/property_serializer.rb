class Binder::PropertySerializer < ActiveModel::Serializer
    attributes :id, :property_type, :country, :address1, :address2, :city, :state, :zip, :acres, :details,
                :apn, :lat, :long, :sq_ft, :year_built, :county, :purchase_price, :purchase_date
                
    def acres
        return object.acres.to_f unless object.acres.nil?
        return object.acres
    end
end