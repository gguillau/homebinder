class Binder::PaintSerializer < ActiveModel::Serializer
  attributes :id, :binder_id, :name, :paint_type, :manufacturer, :code, :makeup, :details, :sheen
  
  has_one :purchase
  has_one :seller_report_item
  has_one :permissions
  has_many :tags
  
  def permissions
    return PermissionSerializer.new(object, {scope: scope}).attributes
  end
end
