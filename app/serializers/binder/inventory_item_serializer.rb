class Binder::InventoryItemSerializer < ActiveModel::Serializer
  attributes :id, :binder_id, :name, :inventory_item_type, :details, :value, :number
  
  has_many :tags
  has_one :permissions
    
  def permissions
    return PermissionSerializer.new(object, {scope: scope}).attributes
  end
  
  def value
    return object.value.cents
  end
  
  def number
    return object.value.to_i
  end
  
end
