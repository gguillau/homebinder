class Binder::ProjectSerializer < ActiveModel::Serializer
  attributes :id, :name, :status, :project_type, :start_date, :end_date, :details, :cost, :number, :binder_id

  has_one :seller_report_item
  has_one :permissions
  has_many :tags

  def permissions
    return PermissionSerializer.new(object, {scope: scope}).attributes
  end

  def cost
    return object.cost.cents.to_i
  end

  def number
    return object.cost.to_i
  end
end
