class Binder::MaintenanceItem::MaintenanceEventSerializer < ActiveModel::Serializer
  attributes :id, :maintenance_item_id, :contractor_id, :do_date, :completed_date, :contractor
  
  has_many :tags
  
  def contractor
    return Contractor.find_by_id(object.contractor_id)
  end
  
end
