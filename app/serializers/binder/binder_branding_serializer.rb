class Binder::BinderBrandingSerializer < ActiveModel::Serializer
  
    attributes :id, :scope, :user, :partner_id
    
    def scope
        object.scope
    end
    
    # get the user branding
    def user
        user_profile = UserProfile.find_by_id(object.user_branding_id)
        return UserProfileSerializer.new(user_profile).attributes unless user_profile.nil?
        # find the partner admin
        partner_user = PartnerUser.where(:role => "admin", :partner_id => object.partner_id).first
        return nil unless partner_user.present?
        user = User.find_by_id(partner_user.user_id)
        user_profile = UserProfile.find_by_id(user.user_profile.id)
        return UserProfileSerializer.new(user_profile).attributes unless user_profile.nil?
        return nil
    end

end
