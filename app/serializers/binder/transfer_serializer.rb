class Binder::TransferSerializer < ActiveModel::Serializer
  attributes :id, :binder_id, :status, :sender, :receiver, :transfer_type, :created_at, :access_token, :binder
end