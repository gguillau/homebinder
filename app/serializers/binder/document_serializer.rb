class Binder::DocumentSerializer < ActiveModel::Serializer
  attributes :id, :name, :file_size, :location, :details, :file_file_name, :binder_id, :accessible, :library_source_id, :url

  has_one :seller_report_item
  has_many :tags

  def name
    return object.name if object.name.present?
    return "" if object.file_file_name.nil?
    return object.file_file_name
  end

  def file_size
    return object.file_file_size
  end

  def location
    object.file.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval])
  end

end
