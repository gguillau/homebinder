class Binder::ApplianceSerializer < ActiveModel::Serializer
  attributes :id, :name, :binder_id, :manufacturer, :model, :serial_no, :warranty, :user_guide_url, :details, :last_recall_check, :upc, :install_date,
  :library_source_id
  
  has_many :appliance_recalls
  has_one :permissions
  has_one :purchase
  has_one :seller_report_item
  has_many :tags

  def appliance_recalls
    object.appliance_recalls
  end

  def permissions
    return PermissionSerializer.new(object, {scope: scope}).attributes
  end
end
