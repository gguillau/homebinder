class Binder::StructureSerializer < ActiveModel::Serializer
  attributes :id, :name, :construction_style, :construction_type, :year_built, :binder_id, :beds, :baths, :sq_ft, :heat_type, :heat_source, :details, :permissions
  has_many :tags

  def permissions
    return PermissionSerializer.new(object, {scope: scope}).attributes
  end
end
