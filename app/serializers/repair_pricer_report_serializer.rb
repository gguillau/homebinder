class RepairPricerReportSerializer < ActiveModel::Serializer
  attributes :id, :report_id, :status, :repair_report, :binder_id, :created_at, :updated_at, :client_first, :client_last, :client_email,
  :client_phone, :buyer_agent_first, :buyer_agent_last, :buyer_agent_email, :amount_charged_cents, :amount_charged_currency, :payment_status,
  :stripe_charge_id, :stripe_refund_id, :closing_date, :creator_id, :partner_id, :delivery_date, :order_home_history_report, :rush_report, :home_history_report,
  :inspection_report, :order_pool_report, :pool_report, :full_address, :address, :creator, :partner, :amount_charged, :inspection_report_location,
  :repair_report_location, :home_history_report_location, :pool_report_location, :time_left, :report_type
  
  def full_address
      return "N/A" if object.address.nil?
      object.address.full_address
  end
  
  def amount_charged
    object.amount_charged_cents.to_f/100.to_f
  end
  
  def partner
    return nil if object.partner.nil?
    return {
      id: object.partner.id,
      name: object.partner.name,
      prepaid: object.partner.partner_configuration.repair_pricer_pre_paid_reports
    }
  end
  
  def client_phone
    country = object.address.present? ? object.address.country : nil
    Phone.new(object.client_phone, country)
  end
  
  def inspection_report_location
    return nil if !object.inspection_report?
    object.inspection_report.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval])
  end
  
  def home_history_report_location
    return nil if !object.home_history_report?
    object.home_history_report.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval])
  end
  
  def pool_report_location
    return nil if !object.pool_report?
    object.pool_report.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval])
  end
  
  def repair_report_location
    return nil if !object.repair_report?
    object.repair_report.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval])
  end
  
end
