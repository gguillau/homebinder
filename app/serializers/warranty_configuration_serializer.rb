class WarrantyConfigurationSerializer < ActiveModel::Serializer
  attributes :id, :automatic_purchases, :account_number, :warranty_plan_id, :warranty_company_id, :partner_id,
                :created_at, :updated_at, :partner, :warranty_company, :warranty_plan, :name, :price, :warranty_plans
            
    def name
        object.partner.name
    end
                
    def partner
        object.partner
    end
    
    def warranty_company
        object.warranty_company
    end
    
    def warranty_plan
        object.warranty_plan
    end
    
    def warranty_plans
        object.partner.partner_warranty_plans.map {|plan| {:id => plan.id, :name => plan.warranty_plan.name, :price => plan.price.to_i, :warranty_plan_id => plan.warranty_plan_id}}
    end
    
    def price
        plan = PartnerWarrantyPlan.where(:partner_id => object.partner_id, :warranty_plan_id => object.warranty_plan_id).first
        return if plan.nil?
        return plan.price.to_i
    end
end