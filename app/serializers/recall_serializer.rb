class RecallSerializer < ActiveModel::Serializer
  attributes :id, :number, :recall_date, :details, :verified, :ignore, :models
  
  def appliance_recalls
    object.appliance_recalls.map { | appliance_recall| Binder::Appliance::ApplianceRecallSerializer.new(appliance_recall).attributes}
  end
  
  def attributes
    hash = super
    if scope
      if scope[:action] == "show"
        hash[:appliance_recalls] = appliance_recalls
      end
    end
    return hash
  end
end
