require "stripe"

module Payments
    class Customer
        
        def initialize(user)
            @user = user
        end
        
        def show(params)
            id = params[:id]
            begin
                return Stripe::Customer.retrieve(id)
            rescue => e
                raise BadRequestException.new e.message
            end
        end
        
        def create(params)
            begin
                # save the customer
                customer = Stripe::Customer.create(
                    :description => @user.user_profile.display_name,
                    :email => params[:customer][:email],
                    :source => params[:payment][:card]
                )
            rescue => e
                raise BadRequestException.new e.message
            end
            
            # thanks for signing up - rep will reach out with more info - feel free to reach out if you have questions
            # by clicking chat bubble
            
            # create a subscription
            
            plan = find_plan(params)
            
            begin
                Stripe::Subscription.create(
                    :customer => customer.id,
                    :items => [
                        {
                            :plan => plan
                        },
                    ]
                )
            rescue => e
                raise BadRequestException.new e.message
            end
            
            if params[:account_id].present?
                account = Account.find(params[:account_id])
                account.account_status = "active"
                account.account_sub_type = "paid"
                account.account_sub_status = "none"
                account.billing_activation = Date.today
                account.billing_frequency = "monthly"
                account.payment_type = "subscription"
                account.subscription_amount_cents = plan.amount
                account.stripe_customer_id = customer.id
                account.save!
                
                # save the address
                partner = account.manager
                partner.address_attributes = {
                    address1: params[:customer][:address][:address1], 
                    address2: params[:customer][:address][:address2],
                    city: params[:customer][:address][:city], 
                    state: params[:customer][:address][:state], 
                    country: params[:customer][:address][:country],
                    zip: params[:customer][:address][:zip]
                }
                partner.save!
                
                # call PDF generator to generate custom marketing materials
                Partners::PdfGeneratorServiceJob.perform_async(partner.id)
            end
            
            # send an email to support
            # CC alert
            # has just added CC info to their account
            # id, name, email, phone, company name
            
            SubscriptionMailer.notify_customer_subscription(params[:customer].to_unsafe_h).deliver_later
            
            return customer
        end
        
        def update(params)
            customer = show(params)
            customer.email = params[:customer][:email]
            customer.source = params[:payment][:card]
            customer.save
            
            if params[:account_id].present?
                account = Account.find(params[:account_id])
                # save the address
                partner = account.manager
                partner.address_attributes = {
                    address1: params[:customer][:address][:address1], 
                    address2: params[:customer][:address][:address2],
                    city: params[:customer][:address][:city], 
                    state: params[:customer][:address][:state], 
                    country: params[:customer][:address][:country],
                    zip: params[:customer][:address][:zip]
                }
                partner.save!
            end
            return customer
        end
        
        def delete(params)
            customer = show(params)
            customer.delete
        end
        
        
        ##### HELPER METHODS ######
        
        def find_plan(params)
            # find the plan by name and if we can't find it then we 
            # create the plan to keep the process moving
            plan = params[:plan]
            cost = params[:cost]
            currency = find_country_code(params)
            
            begin 
                return Stripe::Plan.retrieve(plan)
            rescue => e
                case e.message
                when "No such Plan: #{plan}", "No such plan: #{plan}", "No such plan: #{plan}; a similar object exists in live mode, but a test mode key was used to make this request."
                    return create_plan(plan, cost, currency)
                else
                    raise BadRequestException.new e.message
                end
            end
        end
        
        def create_plan(plan, cost, currency)
            begin
                # create the plan
                return Stripe::Plan.create(
                    :amount => get_cost(cost),
                    :interval => "month",
                    :name => plan,
                    :currency => currency,
                    :id => plan
                )
            rescue => e
                raise BadRequestException.new e.message
            end
        end
        
        def find_country_code(params)
            return "usd" if params[:customer][:country].nil?
            return "usd" if Country[params[:customer][:country]].nil?
            return Country[params[:customer][:country]].currency.iso_code.downcase
        end
        
        def get_cost(cost)
            return (cost * 100).floor
        end
    end
end