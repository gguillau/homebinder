require 'stripe'

module Payments
  class CreditCardProcessor
    def initialize(user)
      @user = user
    end
    
    def charge_card(args)
      begin
        charge = Stripe::Charge.create(
          amount: args[:amount],
          currency: "usd",
          description: args[:description],
          source: args[:card]
        )
        return charge
      rescue => e
        raise BadRequestException.new e.message
      end
    end
    
    def refund(charge_id)
      begin
        return Stripe::Refund.create(charge: charge_id)
      rescue => e
        raise BadRequestException.new e.message
      end
    end
    
  end
end