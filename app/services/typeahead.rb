class Typeahead
  
  def self.add_typeahead_values resource
    if resource.class.method_defined? :typeahead
      fields = resource.send(:typeahead)
      fields.each do |field|
        # get the property name
        typeaheadName = field[:name]
        # get the typeahead value
        typeaheadVal = resource.send(typeaheadName)
        # if there was no value move on
        if typeaheadVal.nil? or typeaheadVal.blank?
          next
        end
        # get the class of the field storing the typeahead value
        typeaheadClass = field[:to] ? field[:to].classify.constantize : typeaheadName.classify.constantize
        # add the typeahead value if it doesn't exist
        existing = typeaheadClass.where("(LOWER(name) = :value AND verified = true) or
                                         (LOWER(name) = :value AND verified = false AND created_by = :created_by)",
                                          value: typeaheadVal.downcase, created_by: resource.created_by)
        if existing.count > 0
          # check if there is an option available to the user already
          existing.each do |e|
            # if there is already a verified version ignore it. NOTE: we should always be returning
            # verified values but just in case do all of them
            if e.verified
              next
            end
            # non verified entry. update it with the latest value submitted
            if e.name != typeaheadVal
              e.name = typeaheadVal
              e.save!
              return
            end
          end
        else
          # save a new option
          newVal = typeaheadClass.new(name: typeaheadVal, verified: false, created_by: resource.created_by)
          newVal.save!
        end
      end
    end
  end
  
  def self.get_typeahead_values(user, klass, search = nil)
    cls = klass.classify.constantize
    if search
      return cls.where("(verified = true OR created_by = :created_by) AND LOWER(name) LIKE :search", created_by: user.id, search: "#{search}%")
                .order("name")
                .pluck("name")  
    else
      return cls.where("verified = true OR created_by = :created_by", created_by: user.id)
                .order("name")
                .pluck("name")
    end
  end
  
  def self.get_unverified_typeahead_values(klass)
    cls = klass.classify.constantize
    return cls.where("verified = false").order("name")
  end
  
  def self.save_unverified_typeahead_value(klass, id, typeahead)
    cls = klass.classify.constantize
    
    @typeahead = cls.find(id)
    # save the typeahead
    raise UnprocessableException.new(@typeahead) unless @typeahead.update_attributes(typeahead.permit!)
  end
  
  def self.remove_unverified_typeahead_value(klass, id)
    cls = klass.classify.constantize
    cls.find(id).destroy
  end
  
end