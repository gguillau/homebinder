require 'date'
require 'open-uri'
require 'uri'
require 'prawn'
require 'prawn/table'

class InventoryReportPdfService < PdfService
    include ActionView::Helpers::NumberHelper
    
    attr_accessor :binder, :user
    
    def initialize(binder, user)
        self.binder = binder
        self.user = user
        super(binder)
        self.text = "Inventory Report for"
    end

    def create
        init
        add_logos
        add_title
        add_total_cost
        add_items
        add_footer
        return pdf.render
    end
    
    def add_total_cost
        total_cost = number_to_currency(binder.inventory_items.sum(:value_cents)/100, :precision => 2)
        pdf.move_down(35)
        pdf.formatted_text [{:text => "Total value of inventory items: ", :styles => [:bold], :at => [300, pdf.cursor - 10], :size => 14}, {:text => total_cost, :styles => [:bold], :at => [300, pdf.cursor - 10], :size => 14, :color => "ff6633"}]
    end
    
    def add_items
        pdf.move_down(50)
        if binder.inventory_items.count > 0
            binder.inventory_items.find_each do |item|
                add_item(item)
            end
        else
            f_color = "dff0d8"
            s_color = "d6e9c6"
            
            pdf.move_down(100)
    
            pdf.line_width = 2
            pdf.fill_color "#{f_color}"
            pdf.stroke_color "#{s_color}"
    
            pdf.fill_and_stroke_rectangle [(pdf.bounds.left), (pdf.cursor + 15)], (pdf.bounds.right), 55
    
            pdf.fill_color "000000"
            pdf.stroke_color "000000"
    
            pdf.move_down(5)
    
            pdf.text "No inventory items found", :size => 20, :color => "000000", :align => :center
        end
    end
    
    def add_item(item)
        pdf.move_down(25)
        pdf.formatted_text [{:text => "#{item.name}", :styles => [:bold], :at => [0, pdf.cursor], :size => 12, :color => "ff6633"}]
        pdf.move_down(10)
        
        pdf.stroke do
            pdf.stroke_color 'ff6633'
            pdf.stroke_horizontal_rule
        end
        
        # item info
        pdf.move_down(15)
        type = item.inventory_item_type.present? ? item.inventory_item_type : "Item type not available"
        details = item.details.present? ? item.details : "Items details not available"
        pdf.formatted_text [{:text => "Type: ", :styles => [:bold], :at => [0, pdf.cursor], :size => 12}, {:text => "#{type}", :at => [0, pdf.cursor], :size => 12}]
        pdf.formatted_text [{:text => "Value: ", :styles => [:bold], :at => [300, pdf.cursor], :size => 12}, {:text => number_to_currency(item.value_cents/100, :precision => 2), :at => [350, pdf.cursor], :size => 12}]
        pdf.formatted_text [{:text => "Details: ", :styles => [:bold], :at => [0, pdf.cursor], :size => 12}, {:text => "#{details}", :at => [0, pdf.cursor], :size => 12}]
        
        # images
        pdf.move_down(15)
        pdf.formatted_text [{:text => "Images", :styles => [:bold], :at => [0, pdf.cursor], :size => 12}]
        pdf.move_down(10)
        
        images_count = item.images.count
        if images_count > 0
            row = []
            width = pdf.bounds.width/4
            column_widths = [width, width, width, width]
            
            images = item.images
            images.each_slice(4) do |sliced_images|
                array = []
                sliced_images.each do |sliced_image|
                    begin
                        photo = open(sliced_image.file.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :large), {ssl_verify_mode:OpenSSL::SSL::VERIFY_NONE})
                        array.push({:image => photo, :fit => [150, width - 50], :border_widths => 0})
                    rescue => e
                        ErrorService.perform_async(e.message, {:task => "add_item"}, user&.id)
                        next
                    end
                end
                
                row.push(array)

                pdf.table(row, :header => false, :column_widths => column_widths, :cell_style => { :inline_format => true}) do
                    cells.padding       = 4
                    cells.size          = 9
                end
                row = []
                pdf.move_down(10)
                
            end
        else
            pdf.text "No images found for this item", :size => 14, :color => "000000", :align => :center
        end
    end
    
    def add_footer
        # footer
        pdf.repeat(:all, :dynamic => true) do
            pdf.formatted_text_box [
                { :text => "Website: www.homebinder.com\n", :size => 8, :color => "686868" },
                { :text => "Call support: 800.377.6915", :size => 8, :color => "686868" }
            ], :at => [0, -45], :width => 400, :height => 50
            
            pdf.formatted_text_box [
                { :text => "Report Date: #{Date.today}", :size => 8, :color => "686868" }
            ], :at => [pdf.bounds.right-90, -45], :width => 100, :height => 50
        end
    end

end
