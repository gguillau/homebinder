require 'stripe'

class PartnerService < BaseService

    ADD = "add".freeze
    QUERY = "LOWER(partner_key) = :partner_key".freeze
    REMOVE = "remove".freeze
    COUPON = "%sFREELIFE".freeze
    FOREVER = "forever".freeze
    PARTNER_TAG = "partner%s".freeze
    DEFAULT_ORDER_BY = "partners.id".freeze
    DEFAULT_ORDER = "desc".freeze
    EMAILREQUIRED = "email is required".freeze
    SAVE_ERROR = "Error saving partner".freeze
    PARTNER_TYPE_REQUIRED = "Partner Type required".freeze
    STRIPE_ERROR = "STRIPE ERROR::FAILED TO CREATE COUPON -> ".freeze

    def after_create
        # save the partner again with the partner code
        item.code = PARTNER_TAG % item.id
        item.save!

        # create coupon
        coupon = COUPON % item.code.upcase
        create_coupon(coupon)

        # create the configuration object
        create_configuration

        # create the billing account
        create_billing_account

        # generate the API key
        generate_api_key

        # generate the API route name
        generate_route_name

        # set the user's global role
        user.role = UserGlobalRoles.map_partner_type(item.partner_type)
        user.save!
        
        # call analytics
        Partners::CreatedJob.perform_async(item.id)
        EventService.perform_async({:event_name => "partner_registered", :event_type => "partner", :user_id => user.id, :user_created_at => user.created_at, :user_role => user.role, :partner_id => item.id, :partner_type => item.partner_type})
    end

    # create the partner configuration object and set the default user branding ID
    def create_configuration
        # create the configuration object
        Partner::Configuration.create!(:partner_id => item.id, :default_user_branding_id => item.users.where(:role => item.partner_type).order(:id).first.user_profile.id)

        # generate the default template - for some reason this doesn't work in build mode
        template = item.partner_configuration.generate_default_template
        item.partner_configuration.default_binder_template_id = template.id
        item.partner_configuration.repair_pricer_enabled = true if item.address && item.address.country === "US" && item.partner_type != "lender"
        item.partner_configuration.save!
    end

    # creates the billing account for the partner
    def create_billing_account
        account = Account.new(
            manager_id: item.id,
            name: "#{item.name} - #{item.id}",
            account_type: Account::AccountType::SINGLE,
            account_sub_type: Account::AccountSubType::FREE_TRIAL,
            account_status: Account::AccountStatus::ACTIVE,
            trial_expiration: Date.today + 14.days
        )
        account.generate_account_number
        item.account_id = account.id
        item.save!
    end

    # automatically generate API keys for partners during registration
    def generate_api_key
        key = {
            partner_id: item.id,
            company_name: item.name,
            application_name: "Website",
            contact_email: item.email,
            key: SecureRandom.uuid.gsub('-', '')
        }
        ApiKey.create!(key)
    end

    # automatically generate API route name for partners during registration
    def generate_route_name
        # get the partner name and remove all spaces
        name = item.name.gsub(/[^A-Za-z]/, "")

        # check if the name is too long
        if name.length > 20 && item.contact.present?
            # get the partner contact person name
            names = item.contact.split(" ")
            contact = names[0][0] + names.last
            # set the name to the partner contact
            name = contact.gsub(/[^A-Za-z]/, "")
        end

        # check if the name is too short
        if name.length < 4

            # get the partner contact person name
            names = item.contact.split(" ")
            contact = names[0][0] + names.last
            # set the name to the partner contact
            name = contact.gsub(/[^A-Za-z]/, "")
        end

        # set the key to the name in lowercase letters
        item.partner_key = name.downcase

        # check if route name exists
        if Partner.where(:partner_key => name.downcase).count > 0
            # get the partner contact person name
            names = item.contact.split(" ")
            contact = names[0][0] + names.last
            # set the name to the partner contact
            name = contact.gsub(/[^A-Za-z]/, "")
            item.partner_key = name.downcase
        end

        # save the partner
        if not item.save
            message = "Unable to create API Route Name: %s" % item.errors.full_messages.first
            ErrorService.perform_async(message, {:partner_id => item.id})
            return
        end
        return item
    end

    def update_coupon
        item.code = PARTNER_TAG % item.id
        item.save!

        # check if a coupon already exists
        coupon = COUPON % item.code.upcase
        begin
            Stripe::Coupon.retrieve(coupon)
        rescue
            create_coupon(coupon)
        end
    end

    def create_coupon(code)
        # coupon may already exist in test system so we add date time to allow creation
        code =+ "_#{DateTime.now.to_i}" if !Rails.env.production?
        begin
            Stripe::Coupon.create(
                :percent_off => 100,
                :duration => FOREVER,
                :id => code
            )

            Coupon.new(partner_id: item.id, code: code, percent_off: 100, duration: FOREVER).save!
        rescue => e
            ErrorService.perform_async(e.message, {partner_id: item.id, code: code})
        end
    end

    # update the partner in salesforce, intercom and update all users
    def after_update

        if item.partner_type != "broker"
            update_salesforce
        end

        update_coupon

        update_users

        Partners::UpdatedJob.perform_async(item.id)
    end

    # updates the partner account after completion of onboarding and creates
    # salesforce and billing accounts
    def complete_onboarding(params)
        partner = params[:partner][:partner]
        partner[:completed_onboarding] = true

        item.update_attributes!(partner.permit!)

        # get the survey data
        data = params[:partner][:data]

        # check if partner is an inspector <-- temporary fix
        if item.partner_type === "inspector"
            add_to_org("HomeBinder Inspectors", "All HomeBinder inspectors")
        end

        create_salesforce_account(data)

        # add the partner logo to the user account
        user.user_profile.logo = item.logo
        user.user_profile.save!

        # send an email to support
        AccountMailer.auto_generated_support_email(item.id, data.to_unsafe_h).deliver_later

        #share binder sample binder with partner
        share_binder

        return item
    end

    def add_to_org(org_name, description)
        # add partner to the homebinder organization for inspectors
        org = Organization.where(:name => org_name, :description => description).first
        # check if the org exists
        if org.present?
            OrganizationPartner.create!(organization_id: org.id, partner_id: item.id)
            # get the resources for the organization
            resources = MarketingResource.joins(:organization_resources).where(:organization_resources => {:organization_id => org.id}, :user_type => item.partner_type)
            # add the resources
            resources.each do |resource|
                PartnerResource.create!(:partner_id => item.id, :marketing_resource_id => resource.id)
            end
        end
        return item
    end

    # shares a binder with new partners so they can see what a HomeBinder looks like upon
    # registering as a new user of the site
    def share_binder
        # find user by email
        sample = User.find_by_email("johnsmith@sampleinspector.com")
        # return if the user doesn't exist or if the user doesn't have any binders
        return ErrorService.perform_async("Sample user johnsmith@sampleinspector.com not found", {:task => "share_binder"}) unless sample.present?
        return ErrorService.perform_async("Sample binder for johnsmith@sampleinspector.com not found", {:task => "share_binder"}) unless sample.binders.count > 0

        # get the first binder
        binder = sample.binders.first
        # share the binder
        UserBinder.create!(:binder_id => binder.id, :user_id => user.id, :role => "reader")
    end

    def enable_warranty_account
        # enable the warranty account
        item.partner_configuration.active_warranty_account = true
        item.partner_configuration.save!

        # analytics
        EventService.perform_async({:event_name => "new_partner_warranty_request", :event_type => "warranty", :partner_id => item.id, :partner_type => item.partner_type})

        # send the email
        WarrantyMailer.send_warranty_request(item.id).deliver_later
    end

    # checks if the user isn't already a partner user and adds them
    # partner user members receive the partner logo as their user profile logo
    def add_user(new_user, role)

        if new_user.role === "homeowner"
            new_user.role = role
            new_user.save!
        end

        if new_user.role === "agent" and !new_user.has_role? :agent, item
            PartnerUser.create!(partner_id: item.id, user_id: new_user.id, role: :agent)
        elsif new_user.role != "agent" and !new_user.has_role? :member, item
            # add the user to the partner
            PartnerUser.create!(partner_id: item.id, user_id: new_user.id, role: :member)

            new_user.user_profile.logo = item.logo
            new_user.save!

            # send an email to the user
            new_user.invite! unless new_user.role === "agent"
        end

        # analytics
        EventService.perform_async({:event_name => "user_created", :event_type => "partner", :user_id => new_user.id, :user_role => new_user.role, :user_created_at => new_user.created_at, :partner_id => item.id, :partner_type => item.partner_type})
    end

    # update the partner users sometimes partner may update company name/logo
    # and so we need to update all the partner users
    def update_users
        # iterate through each user that has similar partner type <- prevents updating agents and other non similar partner types
        item.users.includes(:user_profile).where(:role => item.partner_type).each do |user|
            # set the attributes
            user.user_profile.logo = item.logo
            user.user_profile.company = item.name
            user.user_profile.website = item.website
            # save the profile
            user.user_profile.save!
        end
    end

    def remove_user(params)
        user_id = params[:user_id]

        # find the user
        remove = User.find_by_id(user_id)
        return if remove.nil?

        # check if we can delete the user
        user.ability.authorize! :destroy, remove, :message => "You are not authorized to delete user ID: #{user.id}."

        # find the partner admin
        admin = PartnerUser.where(partner_id: item.id, role: "admin").first

        # make sure we're not removing the admin
        raise BadRequestException.new "Cannot remove partner admin" if admin.user.id === user_id
        
        # get the role and make sure if the user is an agent to not actually delete the user from the system
        role = PartnerUser.where(partner_id: item.id, user_id: user_id).first.role
        
        # remove the user from the partner
        PartnerUser.where(partner_id: item.id, user_id: user_id).delete_all

        # we return if the user is an agent
        return if role === "agent"
        
        # check if user is default branding user
        change_default_branding_user(remove, admin.user)

        # add the partner admin as the owner for all the users binders
        UserBinder.where(:user_id => user_id, :role => "owner").update_all(:user_id => admin.user_id)

        # delete the user
        remove.remove
    end

    def change_default_branding_user(user, admin)
        return unless user.user_profile.id === item.partner_configuration.default_user_branding_id
        # set the default branding user to the admin
        item.partner_configuration.default_user_branding_id = admin.user_profile.id
        # save the partner
        if not item.save
            ErrorService.perform_async("Unable to set new default branding user", {partner_id: item.id})
        end
    end

    def find_warranty_plans
        return item.partner_warranty_plans
    end

    def widget_exclusions
        return item.widget_exclusions
    end

    # creates the partner acccount, contact and opportunity object in salesforce
    def create_salesforce_account(data)
        if item.partner_type === "inspector"
            # check for required fields
            raise BadRequestException.new "# of Employees required when creating salesforce account" if data[:employees].nil?
            raise BadRequestException.new "# of Employees required when creating salesforce account" if data[:employees][:range].nil?
            raise BadRequestException.new "# of Inspections required when creating salesforce account" if data[:inspections].nil?
            raise BadRequestException.new "# of Inspections required when creating salesforce account" if data[:inspections][:range].nil?
            raise BadRequestException.new "Software required when creating salesforce account" if data[:software].nil?
            raise BadRequestException.new "Source required when creating salesforce account" if data[:source].nil?
    
            employees = data[:employees][:range] === "Just me" ?  "1" : data[:employees][:range].split("-")[0].gsub("+", "")
            volume = data[:inspections][:range] === "1100+" ? "1100" : data[:inspections][:range].split("-")[1].gsub("+", "")
            admin_software = data[:software].include?("ISN") ? "ISN" : nil
    
            data[:software].delete("ISN")
    
            if data[:software].include?("Other")
                software = "Other"
            else
                software = data[:software].keys.first
            end
    
            source = data[:source].keys[0]

            Partners::CreateSalesforceAccountJob.perform_async(item.id, {employees: employees, source: source, volume: volume, software: software, admin_software: admin_software})
        else
            volume = data[:newClients][:range] === "10,000+" ? "10000" : data[:newClients][:range].split("-")[1].gsub("+", "")
            # create a new account in salesforce
            Partners::CreateSalesforceAccountJob.perform_async(item.id, {employees: nil, source: nil, volume: volume, software: nil, admin_software: nil})
        end
    end

    def update_salesforce
        # update the partner account in salesforce
        Partners::UpdateSalesforceAccountJob.perform_async(item.id)
    end

    def import_users(params)
        raise BadRequestException.new "Role required"  unless params[:role].present?
        raise BadRequestException.new "Users required" unless params[:users].present?

        # get the role each user will have
        role = params[:role]
        # get the users
        users = params[:users]
        # check if the user is creating more than 50 users
        # if so, we will create the users in the background

        if users.length > 50

            delayed_users = users.last(users.length - 50)
            non_delayed_users = users.first(50)

            # this method doesn't work in the background when calling from PartnerService
            # so added a method to Partners::PartnerUsers class

            # perform in background
            create_imported_users(delayed_users, role)

            # perform in foreground
            create_imported_users(non_delayed_users, role)
        else
            # perform in foreground
            create_imported_users(users, role)
        end

        return []
    end

    # creates users from an array
    # users is an array, role is the type of role each user will have
    # all users are added to the partner
    
    def create_imported_users(users, role)
        request = HBRequest.new.create_request(user)
        # remove any invalid users
        users.reject! { |u| verify_user(u, role)}

        #modify the array in place
        users.map! { |u| create_users(u)}

        # create the users
        users.each { |u|
            begin
                User.build(request, {:user => u, :partnerId => item.id})
            rescue UnprocessableException => e
                ErrorService.perform_async(e.resource.errors.full_messages.first, {user: u[:email]})
                next
            rescue => e
                ErrorService.perform_async(e.message, {user: u[:email]})
                next
            end
        }
    end

    # verify required fields when importing users
    # return true if user is missing required attributes
    # check for nil and values where value is ""

    def verify_user(add, role)
        # add the role
        add["role"] = role
        if add["first_name"].nil? || add["last_name"].nil? || add["company"].nil? || add["email"].nil?
            return true
        end
        if add["first_name"].empty? || add["last_name"].empty? || add["company"].empty? || add["email"].empty?
            return true
        end
        return false
    end

    def create_users(u)
        # create the user
        add = {
            :email => u["email"],
            :role => u["role"],
            :user_profile_attributes => {
                :first_name => u["first_name"],
                :last_name => u["last_name"],
                :company => u["company"],
                :mobile_phone => u["mobile_phone"],
                :website => u["website"],
                :address_attributes => {
                    :address1 => u["address1"],
                    :address2 => u["address2"],
                    :city => u["city"],
                    :state => u["state"],
                    :zip => u["zip"],
                    :country => u["country"]
                }
            }
        }
        # add headshot/logo
        add[:user_profile_attributes][:head_shot] = add_user_image(u["headshot"])
        add[:user_profile_attributes][:logo] = add_user_image(u["logo"])

        # return action controller parameters object
        ActionController::Parameters.new(add)
    end

    def add_user_image(url)
        # make sure we have an actual url
        return nil if url.nil? || url.empty? || url.blank?
        return nil unless url =~ URI::regexp
        # begin downloading the image
        begin
            encoded_url = URI.encode(url)
            return URI.parse(encoded_url)
        rescue => e
            ErrorService.perform_async(e.message, {url: url})
            return nil
        end
    end
    
    def update_user_role(params)
        # check for required fields
        raise BadRequestException.new "User required" unless params[:user]
        raise BadRequestException.new "User ID required" unless params[:user][:id]
        raise BadRequestException.new "User Role required" unless params[:user][:role]

        # find the user
        pUser = User.find(params[:user][:id])

        # check ability
        user.ability.authorize! :write, pUser

        # find the partner_user
        partner_user = PartnerUser.where(:partner_id => item.id, :user_id => pUser.id).first
        raise BadRequestException.new "Partner User not found" unless partner_user.present?

        if not partner_user.update_attributes(:role => params[:user][:role])
            raise UnprocessableException.new(partner_user)
        end

        return pUser
    end
end
