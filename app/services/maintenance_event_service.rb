class MaintenanceEventService
    
    # initalizes the service with a maintenance_event ID
    
    def initialize(id)
        @maintenance_event = Binder::MaintenanceItem::MaintenanceEvent.find(id)
    end
    
    def handle_webhook(status)
        @maintenance_event.email_status = status
        @maintenance_event.save!
    end
    
    def handle_webhook_click
        @maintenance_event.clicks += 1
        @maintenance_event.save!
    end
    
    def handle_webhook_open
        @maintenance_event.opens += 1
        @maintenance_event.save!
    end
    
end