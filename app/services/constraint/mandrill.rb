class Constraint::Mandrill
    
    SHA1 = "sha1".freeze
    MANDRILL_KEY = "MANDRILL_KEY".freeze
    HTTP_X_MANDRILL_SIGNATURE = "HTTP_X_MANDRILL_SIGNATURE".freeze

    def self.matches?(request)
        signature = generate_signature(request)
        return request.env[HTTP_X_MANDRILL_SIGNATURE] === signature
    end
    
    def self.generate_signature(request)
        string = webhook_url
        mandrill_key = mandrill_api_key

        request.POST.sort_by { |key, value| key.to_i }.each do |key,value|
            string += key.to_s.strip
            string += value.to_s.strip
        end
        
        digest = OpenSSL::Digest.new(SHA1)
        encode = OpenSSL::HMAC.digest(digest,mandrill_key, string)
        
        return Base64.encode64(encode).strip
    end
    
    def self.webhook_url
        "https://#{ENV["DOMAIN"]}/api/v1/mandrill/webhooks"
    end
    
    def self.mandrill_api_key
        raise BadRequestException.new "Mandrill API Key Required" unless ENV[MANDRILL_KEY].present?
        ENV[MANDRILL_KEY]
    end

end