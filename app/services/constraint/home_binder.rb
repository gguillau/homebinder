class Constraint::HomeBinder
    
    ENV_HB_APIKEY = "HB_APIKEY".freeze
    HTTP_HB_APIKEY = "HTTP_HB_APIKEY".freeze
  
    def self.matches?(request)
        # make sure the HB-APIKEY is set as environment file
        raise BadRequestException.new "API Key required" unless ENV[ENV_HB_APIKEY].present?
        verify_key(request.env[HTTP_HB_APIKEY])
    end
    
    def self.verify_key(key)
        # return if key is nil
        return false unless key.present?
        # compare key to env variable
        return false unless key == ENV[ENV_HB_APIKEY]
        # find the key
        key = ApiKey.find_by_key(key)
        # check if a key exists in database
        return false unless key.present?
        # key is good
        return true
    end

end