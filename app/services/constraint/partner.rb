module Constraint::Partner
    
    HTTP_HB_APIKEY = "HTTP_HB_APIKEY".freeze
  
    def self.matches?(request)
        # check if ApiKey is a key from us given to a partner
        find_partner(request.env[HTTP_HB_APIKEY])
    end
    
    def self.find_partner(key)
        # find the key
        key = ApiKey.find_by_key(key)

        # check if a partner_id is associated with this api key
        return false unless key.present?
        return false unless key.partner_id.present?
        # begin the check
        begin
            # check if a partner
            return ::Partner.where(:id => key.partner_id).exists?
        rescue => e
            ErrorService.perform_async(e.message, {:task => "Constraint::Partner", :api_key => key})
            return false
        end
    end
end