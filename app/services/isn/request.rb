class Isn::Request
  
  def call(service_url, auth = nil)
    response = auth.nil? ? HTTParty.get(URI.escape(service_url)) : HTTParty.get(URI.escape(service_url), {basic_auth: auth})
    obj = JSON.parse(response.body)
    
    if response.code == 200 and obj["status"] != "error"
      return obj
    else
      raise BadRequestException.new "Isn::Request Error - #{obj["message"]} - #{service_url}"
    end
  end
  
end