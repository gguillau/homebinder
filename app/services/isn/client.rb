#require 'ruby_odata'
require 'base64'
require 'httparty'
require 'uri'

class Isn::Client

    attr_accessor :user, :ability, :isn_user

    def initialize(request, id)
        init_strings

        self.user = request.user
        self.isn_user = Partner::Configuration::IsnUser.find(id)
        self.ability = request.ability
    end

    def init_strings
        # environment keys
        @env_username = "ISN_USERNAME"
        @env_password = "ISN_PASSWORD"

        # URLs for ISN
        @service_url = "https://isnadmin.com/rest/isn/url?companykey=%s"
        @admin_url = "https://isnadmin.com/rest/isn/url?companykey=%s"
        @rest_url = "https://%s/rest/%s%s"

        # routes for ISN
        @route_agent = "agent/%s"
        @route_agency =  "agency/%s"
        @route_footprints = "orders/footprints"
        @route_orders_footprint = "orders/footprint/"
        @route_client = "client/%s"
        @route_order = "order/%s"

        # Keys used in the various JSON responses and hashes
        @key_footprints = "footprints"
        @key_datetime = "datetime"
        @key_id = "id"
        @key_client = "client"
        @key_property = "property"
        @key_buyers_agent = "buyersagent"
        @key_sellers_agent = "sellersagent"
        @key_first = "first"
        @key_last = "last"
        @key_email = "email"
        @key_address1 = "address1"
        @key_address2 = "address2"
        @key_city = "city"
        @key_state = "state"
        @key_zip = "zip"
        @key_order = "order"
        @key_url = "url"
        @key_agent = "agent"
        @key_display = "display"
        @key_home = "homephone"
        @key_mobile = "mobilephone"
        @key_agency = "agency"
        @key_name = "name"
        @key_office = "office"

        # other strings
        @empty_string = ""
        @client_name = "%s %s"
        @start_query_string = "?%s=%s"
        @append_query_string = "%s&%s=%s"
    end

    def execute_request(params)
        raise BadRequestException I18n.t(:err_isn_request_required) if params[:request].nil?
        raise BadRequestException I18n.t(:err_isn_request_route_required) if params[:request][:route].nil?

        resp = call_isn(params[:request][:route], params[:request][:queryParams])
        return resp
    end

    def get_footprints
        resp = call_isn(@route_footprints)
        resp[@key_footprints].sort! { |a,b| b[@key_datetime] <=> a[@key_datetime] }
        footprints = create_footprints(resp[@key_footprints])
        return {footprints: footprints, total: resp[@key_footprints].count}
    end

    def create_footprints(footprints)
        new_footprints = Array.new
        footprints.each do |footprint|
            client = get_client_info(footprint)
            new_footprint = {
                id: footprint[@key_id],
                datetime: footprint[@key_datetime],
                client: client[:client],
                property: client[:property]
            }
            new_footprints.push(new_footprint)
        end
        return new_footprints
    end

    def get_client(client_id)
        resp = call_isn(@route_client % [client_id])
        return resp[@key_client]
    end

    def get_client_info(fp)
        # use the footprint to get client info
        if fp[@key_client] and not fp[@key_client].empty?
            client = get_client(fp[@key_client])
        else
            client = {
                first: @empty_string,
                last: @empty_string,
                email: @empty_string
            }
        end

        # use the footprint to get buyer agent info
        if fp[@key_buyers_agent] and not fp[@key_buyers_agent].empty?
            buyersagent = get_agent(fp[@key_buyers_agent])
        else
            buyersagent = {}
        end
        
        # use the footprint to get seller agent info
        if fp[@key_sellers_agent] and not fp[@key_sellers_agent].empty?
            sellersagent = get_agent(fp[@key_sellers_agent])
        else
            sellersagent = {}
        end

        # create a info for client
        return {
                   client: {
                       name: @client_name % [client[@key_first], client[@key_last]],
                       email: client[@key_email],
                       home_phone: client[@key_home],
                       mobile_phone: client[@key_mobile]
                   },
                   property: {
                       address1: fp[@key_address1],
                       address2: fp[@key_address2],
                       city: fp[@key_city],
                       state: fp[@key_state],
                       zip: fp[@key_zip]
                   },
                   buyersagent: buyersagent,
                   sellersagent: sellersagent
               }
    end
    
    def get_agent(agent)
        ba_response = execute_request({:request => { :route => @route_agent % [agent] }})
        buyersagent = User.includes(:user_profile).find_by_email(ba_response[@key_agent][@key_email])
        if buyersagent.nil?
            buyersagent = {
                user_profile_attributes: {
                    first_name: ba_response[@key_agent][@key_first],
                    last_name: ba_response[@key_agent][@key_last],
                    website: ba_response[@key_agent][@key_url],
                    home_phone: PhonyRails.normalize_number(ba_response[@key_agent][@key_home], country_code: 'US'),
                    mobile_phone: PhonyRails.normalize_number(ba_response[@key_agent][@key_mobile], country_code: 'US'),
                    company: @empty_string,
                    address_attributes: {
                        address1: ba_response[@key_agent][@key_address1],
                        address2: ba_response[@key_agent][@key_address2],
                        city: ba_response[@key_agent][@key_city],
                        state: ba_response[@key_agent][@key_state],
                        zip: ba_response[@key_agent][@key_zip],
                        country: "US"
                    }
                },
                email: ba_response[@key_agent][@key_email]
            }
            if ba_response[@key_agent][@key_agency].present?
                agency = execute_request({:request => {:route => @route_agency % [ba_response[@key_agent][@key_agency]] }})
                buyersagent[:user_profile_attributes][:company] = agency[@key_agency][@key_name]
            end
        else
            buyersagent = UserSerializer.new(buyersagent).attributes
        end
        return buyersagent
    end
    
    def get_binder_info(footprint_id)
        # get the footprints
        resp = call_isn(@route_footprints)
        resp[@key_footprints].sort! { |a,b| b[@key_datetime] <=> a[@key_datetime] }
        footprints = resp[@key_footprints]
        res = footprints.select{ |f| f[@key_id] == footprint_id }
        raise NotFoundException.new I18n.t(:err_isn_footprint_not_found) if res.count == 0

        fp = res[0]
        client = get_client_info(fp)

        return client
    end
    
    def refresh_endpoint!
        # check the last time the endpoint was refreshed
        refresh = false
        if self.isn_user.api_endpoint.nil?
            refresh = true
        else
            endpoint_refreshed_at = self.isn_user.endpoint_refreshed_at
            refresh = endpoint_refreshed_at.nil? ? true : Time.now - endpoint_refreshed_at > 7.days
        end

        return if not refresh

        # make sure there is a company key
        company_key = self.isn_user.company_key
        raise BadRequestException.new I18n.t(:err_isn_company_key_missing) if company_key.nil? or company_key.empty?

        # create the service url
        service_url = @service_url % [company_key]

        # get the authentication for the admin api
        auth = {
            username: username,
            password: password
        }

        # get the endpoint
        resp = Isn::Request.new.call(service_url, auth)
        self.isn_user.api_endpoint = resp[@key_url]
        self.isn_user.endpoint_refreshed_at = Time.now

        # save the changes
        self.isn_user.save
    end

    def call_isn(route, queryParams = nil)
        # refresh the API endpoint if it's stale
        refresh_endpoint!

        # verify the user can read the isn user
        self.ability.authorize! :read, self.isn_user, :message => "You are not authorized to access ISN User ID: #{isn_user.id}."

        # make sure there is an api endpoint
        api_endpoint = self.isn_user.nil? ? nil : self.isn_user.api_endpoint
        raise BadRequestException.new I18n.t(:err_isn_no_endpoint) if api_endpoint.nil? or api_endpoint.empty?

        # build the query string
        queryString = nil
        if not queryParams.nil?
            queryParams.each_pair { |key, value|
                if queryString.nil?
                    queryString = @start_query_string % [key, value]
                else
                    queryString = @append_query_string % [queryString, key, value]
                end
            }
            queryString = URI.escape(queryString)
        end

        # build the REST enpoint url
        service_url = @rest_url % [api_endpoint, route, queryString]

        # get the authentication for the endpoint
        auth = {
            username: URI.escape(isn_user.username),
            password: URI.escape(isn_user.password)
        }

        # get the footprints
        return Isn::Request.new.call(service_url, auth)
    end

    def username
        raise BadRequestException.new "ISN API Username Required" unless ENV[@env_username].present?
        ENV[@env_username]
    end

    def password
        raise BadRequestException.new "ISN API Password Required" unless ENV[@env_password].present?
        ENV[@env_password]
    end

end
