# allow HB to send errors to error tracking service
# allow devs to switch out error tracking services without changing
# the calls made to error service class

class ErrorService
    include Sidekiq::Worker
    sidekiq_options queue: 'low'
    
    def perform(message, custom_params = {}, user_id = nil)
        # find the user
        user = User.find_by_id(user_id)
        
        # send a message to the user
        Intercom::MessagingJob.perform_async(user_id, message)
        
        # merge the caller into the custom_params
        add_to_custom_params(custom_params, get_app_caller(caller), user)
        
        if user.present?
            Raven.user_context(user.attributes)
        end
        
        # notify Sentry that an error occurred
        Raven.capture_message(message, { :tags => custom_params })
        
        analytics(message, custom_params, user)
    end
        
    def add_to_custom_params(custom_params, call, user)
        if !custom_params.nil?
            custom_params.merge!({task: call})
        else
            custom_params = {task: call}
        end
        
        if user.present?
            custom_params.merge!({user_id: user.id, user_role: user.role})
        end
    end
    
    def get_app_caller(caller)
        list = caller.select{|item| item.include?("/app/app/")}
        return list.first
    end
    
    def analytics(message, custom_params, user)
        if custom_params
            custom_params.merge!({:event_name => message, :event_type => "error", :user_id => user.id}) if user.present?
        else
            custom_params = {:event_name => message, :event_type => "error", :user_id => user.id} if user.present?
        end
        
        # send error to keen
        EventService.perform_async(custom_params)
    end
end