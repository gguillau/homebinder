require 'base64'
require 'uri'

class Buildfax::Client
    include ActionView::Helpers::TextHelper

    DATES = "%s - %s".freeze
    NO_COVERAGE = "We apologize, but we do not have electronic coverage for your area at this time.".freeze
    NO_DATE_COVERAGE = "Electronic coverage exists from %s but no permits are on record for your property.".freeze
    COVERAGE_EXIST = "We have coverage from %s. The following permits are on record. Please check the boxes for the permits that you would like to import into your binder.".freeze

    # initialize the client with the HBRequest object and binder_id

    def initialize(request, binder_id)
        @user = request.user
        @ability = request.ability
        @binder_id = binder_id
    end

    # find the binder and return the API request from BuildFax

    def get_permits(binder_id)
        binder = check_permissions(binder_id)
        # get the binder address
        address = "#{binder.property.address1}, #{binder.property.city}, #{binder.property.state} #{binder.property.zip}"
        # send request
        return send_request(address)
    end

    def check_permissions(binder_id)
        # load the binder
        binder = Binder.find_by_id(binder_id)
        # check if the binder exists
        raise NotFoundException unless binder.present?
        # check if the user can read it
        @ability.authorize! :read, binder, :message => "You are not authorized to access binder ID: #{binder.id}."
        # return the binder
        return binder
    end

    # sends the request to BuildFax and returns a response with a message and an array of permits

    def send_request(address)
        # instantiate permits array
        permits = []
        # send the request
        report = Buildfax::Request.new.send_request(address)
        # we need to check that the report has jurisdiction and then if there are any permits

        if report["buildFaxReport"]["reportBody"]["jurisdiction"].nil?
            return {message: NO_COVERAGE, permits: permits, code: 0}
        end

        if report.dig("buildFaxReport", "reportBody", "jurisdiction").is_a?(Array)
            report["buildFaxReport"]["reportBody"]["jurisdiction"] = report.dig("buildFaxReport", "reportBody", "jurisdiction")[0]
        end
        
        # if there is jurisdiction then we check if there are any permits
        if report.dig("buildFaxReport", "reportBody", "jurisdiction", "property").is_a?(Array)
            permits = parse_array(report)
        else
            permits = parse_hash(report)
        end

        # get the jurisdiction
        jurisdiction = report["buildFaxReport"]["reportBody"]["jurisdiction"]
        # get dates of coverage
        dates = get_coverage_dates(jurisdiction)

        # parse each permit so we can send back a refined verison of the data
        parse_proc = Proc.new{|permit| parse_permit(permit)}
        permits.reject!{|permit| permit["permitNum"].nil? }
        permits = permits.map(&parse_proc)

        if permits.length < 1
            # return the permits
            return {message: NO_DATE_COVERAGE % dates, permits: permits, code: 1}
        else
            # return the permits
            return {message: COVERAGE_EXIST % dates, permits: permits, code: 2}
        end
    end

    # parses the jurisdiction json object to get the coverage dates for the property

    def get_coverage_dates(jurisdiction)
        # get the exclusionMinDate
        min = jurisdiction["exclusionMinDate"]["__content__"].strftime("%A %B %d, %Y")
        max = jurisdiction["exclusionMaxDate"]["__content__"].strftime("%A %B %d, %Y")
        # return string with dates
        return DATES % [min, max]
    end

    # parses an array object to get the permits for a property

    def parse_array(report)
        permit_proc = Proc.new{|i| find_permits(i)}
        permits = report["buildFaxReport"]["reportBody"]["jurisdiction"]["property"].map(&permit_proc).flatten
        return permits
    end

    # parses a hash object to get the permits for a property

    def parse_hash(report)
        # check if we have permits and return an empty array if we don't have any
        if report["buildFaxReport"]["reportBody"]["jurisdiction"]["property"]["permit"].length < 1
            return []
        else
            # if report is a hash then we have only 1 permit
            if report["buildFaxReport"]["reportBody"]["jurisdiction"]["property"]["permit"].is_a?(Hash)
                permit = report["buildFaxReport"]["reportBody"]["jurisdiction"]["property"]["permit"]
                return [permit]
            else
                # return the array of permits
                report["buildFaxReport"]["reportBody"]["jurisdiction"]["property"]["permit"]
            end
        end
    end

    # proc method to iterate through a nested array of permits

    def find_permits(object)
        return object["permit"]
    end

    # creates a list of permits

    def create_permits(params)
        # check permissions
        check_permissions(params[:binderId])
        new_permits = []
        # create permits
        params[:permits].each do |permit|
            # check if a permit exits already to avoid duplicates
            if can_create(permit)
                # create the permit
                new = create_permit(permit)
                # add the new permit to the list of new permits
                new_permits.push(new)
            end
        end
        # return the newly created permits
        return new_permits
    end

    # creates an individual permit

    def create_permit(permit)
        permit.delete("import")
        permit[:binder_contractor_id] = create_contractors(permit[:contractors])
        permit.delete("contractors")
        permit = ActionController::Parameters.new(permit.to_unsafe_h)
        #permit(:status, :proposed_use, :work_class, :permit_type, :permit_number, :valuation_amount, :details, :permit_date, :created_by, :binder_id)
        permit = Binder::Permit.new(permit.permit!)
        if not permit.save
            ErrorService.perform_async(permit.errors.full_messages.first, {:task => "create_permit"})
        end
        return permit
    end

    # takes an array of contractor names and creates a contractor for each

    def create_contractors(contractors)
        id = nil
        contractors.each do |contractor|
            id = create_contractor(contractor)
        end
        return id
    end

    # creates a contractor for the permit

    def create_contractor(name)
        # check if the contractor already exists in the binder
        binder_contractor = Binder::BinderContractor.where(:binder_id => @binder_id).where("LOWER(contact) = ?", name.downcase).first

        # if so return the binder contractor id
        return binder_contractor.id unless binder_contractor.nil?
        
        type = Contractor::Category.find_or_create_by(:name => "Handyman")
        
        # if not we create a new binder contractor
        contractor = Binder::BinderContractor.new({:contact => name, :binder_id => @binder_id, :created_by => @user.id, :binder_contractor_types_attributes => [{contractor_category_id: type.id}], :contractor_attributes => {:name => name, :created_by => @user.id, :contractor_types_attributes => [contractor_category_id: type.id]}})
        
        # save the new object
        if contractor.save
            return contractor.id
        else
            # send an error to New Relic and return nil
            ErrorService.perform_async(contractor.errors.full_messages.first, {:task => "create_contractor"})
            return nil
        end
    end

    # prevents creation of duplicate permits for a binder

    def can_create(permit)
        # find the permit by permit number and binder id
        binder_permit = Binder::Permit.where(:permit_number => permit[:permit_number], :binder_id => @binder_id).first
        # return false if the permit already exists
        return false unless not binder_permit.present?
        # else return true
        return true
    end

    # parses a permit and returns a formatted hash of values

    def parse_permit(permit)
        {
            :status => validate_field(permit, "permitStatus"),
            :proposed_use => validate_field(permit, "proposedUse"),
            :work_class => validate_field(permit, "workClass"),
            :permit_type => validate_field(permit, "permitTypePreferred"),
            :permit_number => validate_field(permit, "permitNum"),
            :valuation_amount => validate_field(permit, "valuationAmount"),
            :details => truncate(validate_field(permit, "description"), :length => 1000), # <- make sure we truncate any descriptions greater than 1000 characters
            :permit_date => validate_field(permit, "preferredDate"),
            :contractors => validate_field(permit, "contractor"),
            :import => true,
            :created_by => @user.id,
            :binder_id => @binder_id
        }
    end

    # checks the permit to see if the passed in field exists in the hash objects

    def validate_field(permit, field)
        # check if the field exists and return the value
        if permit[field].present?
            return get_contractor(permit, field) unless field != "contractor"
            return permit[field]["__content__"]
        else
            return [] unless field != "contractor"
            # return nil
            return "Not Available"
        end
    end

    # parses the permit hash to pull the contractor name
    # sometimes the hash contains a nested array of contractors or nested hash for one contractor

    def get_contractor(permit, field)
        # check if the hash contains an array
        if permit[field].is_a?(Array)
            # return an array with the contractors
            return permit[field].map{|contractor| 
                if contractor.key?("fullName")
                    contractor["fullName"]["__content__"]
                else
                    contractor["companyName"]["__content__"]
                end
            }
        else
            if permit[field]["companyName"] && permit[field]["companyName"]["__content__"]
                return [permit[field]["companyName"]["__content__"]]
            else
                # return an array with one contractor
                return [permit[field]["fullName"]["__content__"]]
            end
        end
    end

end
