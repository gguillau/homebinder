require 'multi_xml'
require 'httparty'

class Buildfax::Request
  
  # sends the request to the buildfax api and returns the response
  
  def send_request(address)
    # base url to pull permit records for a property
    url = "https://delivery.buildfax.com/api/reports/addresses/%s/?output=xml&template=#{template}" % address

    # make the call to the api
    response = HTTParty.get URI.encode(url), :basic_auth => Buildfax::Request.new.auth
    # check if a request was successful and if so return the response
    if response.code == 200
      obj = MultiXml.parse(response.body)
      return obj
    else
      # raise an error if the response was unsuccessful
      raise BadRequestException.new "Buildfax::Request Error - #{response.body} - #{address}"
    end
  end
  
  def download_report(address)
    url = "https://delivery.buildfax.com/api/reports/addresses/#{URI.encode(address)}/?output=PDF&template=BuildFax%20Permit%20Timeline%20Report"
    # make the call to the api
    response = HTTParty.get url, :basic_auth => Buildfax::Request.new.auth
    # check if a request was successful and if so return the response
    if response.code == 200
      return response.body
    else
      # raise an error if the response was unsuccessful
      raise BadRequestException.new raise BadRequestException.new "Buildfax::Request Error - #{response.body} - #{address}"
    end
  end
  
  # authentication needed to log in to buildfax API
  
  def auth
    raise BadRequestException.new "BuildFax Username Required" unless ENV["BUILD_FAX_USERNAME"].present?
    raise BadRequestException.new "BuildFax Password Required" unless ENV["BUILD_FAX_PASSWORD"].present?
    { 
      username: ENV["BUILD_FAX_USERNAME"], 
      password: ENV["BUILD_FAX_PASSWORD"]
    }
  end
  
  def template
    if Host.path === Host::PRODUCTION_HOST
      raise BadRequestException.new "BuildFax Template Required" unless ENV["BUILD_FAX_PRODUCTION_TEMPLATE"].present?
      ENV["BUILD_FAX_PRODUCTION_TEMPLATE"].to_s
    else
      raise BadRequestException.new "BuildFax Template Required" unless ENV["BUILD_FAX_TEST_TEMPLATE"].present?
      ENV["BUILD_FAX_TEST_TEMPLATE"].to_s
    end
  end
  
end