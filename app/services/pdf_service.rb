class PdfService
    
    attr_accessor :text, :pdf, :binder
    
    def initialize(object)
        self.pdf = Prawn::Document.new(:bottom_margin => 100)
        self.text = "PDF Report for"
    end
    
    def init
        pdf.font_families.update("Arvo" => {
                                     :normal => { :file => "#{Rails.root}/public/assets/Arvo-Regular.ttf" },
                                     :italic => { :file => "#{Rails.root}/public/assets/Arvo-Italic.ttf" },
                                     :bold => { :file => "#{Rails.root}/public/assets/Arvo-Bold.ttf" },
                                     :bold_italic => { :file => "#{Rails.root}/public/assets/Arvo-BoldItalic.ttf" }
                                 })
    end

    def add_logos
        # logos
        pdf.bounding_box([0, 666], :width => pdf.bounds.width, :height => 50) do
            # hb logo, top left
            pdf.image "#{Rails.root}/public/img/flatlogonotext.png", :height => 50, :position => :left, :vposition => :top

            # broker logo, top right
            pdf.move_up(50)
            branding = binder.binder_brandings.where(:scope => "binder").first
            if branding.present?
                profile = UserProfile.find_by_id(branding.user_branding_id)
                return unless profile.present?
                if profile.logo.present?
                    pdf.image open(profile.logo.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :original), {ssl_verify_mode:OpenSSL::SSL::VERIFY_NONE}), :height => 40, :position => :right, :vposition => :top
                end
            end
        end
    end
    
    def add_title
        raise BadRequestException.new "Binder required" if binder.nil?
        # report title
        pdf.formatted_text_box [
                                   { :text => "#{text}\n", :color => "999999", :size => 8 },
                                   { :text => "#{binder.property.address1}\n", :size => 22, :font => "Arvo" },
                                   { :text => "#{binder.property.city}, #{binder.property.state} #{binder.property.zip}", :color => '999999', :size => 12, :font => "Arvo" }
                               ], :at => [50, 665], :width => 400, :height => 80
    end
end