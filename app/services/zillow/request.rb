require 'multi_xml'

class Zillow::Request
  
    def call(service_url)
        response = HTTParty.get(URI.escape(service_url))
        if response.code == 200
            begin
                obj = MultiXml.parse(response.body)
            rescue => e
                ErrorService.perform_async(e.message, {task: "Zillow::Request.call"})
                return nil
            end
            return obj
        else
            raise BadRequestException.new "Zillow::Request Error - #{response.body}"
        end
    end
  
end