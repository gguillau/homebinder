class Warranties::Service
    
    def initialize(id)
        @warranty = Warranty.find(id)
    end
    
    # create/update inspection transaction
    
    def create_inspection_transaction(inspection_date)
        # check if there is an existing buy_side_inspection transaction for this binder
        if Binder::Transaction.where(:binder_id => @warranty.binder_id, :transaction_type => "buy_side_inspection").count > 0
            
            # get the transaction and set the inspection_date
            transaction = Binder::Transaction.where(:binder_id => @warranty.binder_id, :transaction_type => "buy_side_inspection").last
            transaction.transaction_date = inspection_date
            
            # save
            if not transaction.save
                raise UnprocessableException.new(transaction)
            end
        else
            # create the transaction and set the inspection_date
            transaction = Binder::Transaction.new(:binder_id => @warranty.binder_id, :transaction_type => "buy_side_inspection")
            transaction.transaction_date = inspection_date
            
            # save
            if not transaction.save
                raise UnprocessableException.new(transaction)
            end
        end
    end
    
    # create warranty transaction
    
    def create_warranty_transaction(price)
        # create the transaction and set the inspection_date
        transaction = Binder::Transaction.new(:binder_id => @warranty.binder_id, :transaction_type => "warranty")
        transaction.transaction_cost = price
        transaction.transaction_date = Date.today
        
        # save
        if not transaction.save
            raise UnprocessableException.new(transaction)
        else
            # make sure we connect the transaction to a warranty
            ::WarrantyTransaction.create!(:binder_transaction_id => transaction.id, :warranty_id => @warranty.id)
        end
    end

end