require 'httparty'

class Warranties::CP::Request
    
    def send_request(warranty_id, inspection_date)
        warranty = Warranty.find(warranty_id)
        transaction = warranty.binder_transaction
        return if transaction.nil?
        partner = transaction.partner_binder_transaction.partner
        return if partner.nil?
        
        # set up the data
        data = {
            apiKey: api_key,
            data: {
                policy_type: "homebinder",    
                #plan_id: "WordPress Plan Id",    
                plan_price: warranty.warranty_plan.price.to_i,    
                plan_title: warranty.warranty_plan.name,    
                inspection_date: inspection_date.to_date,    
                pa_first_name: warranty.client_first.sub('&', 'and'),    
                pa_last_name: warranty.client_last,    
                pa_phone: warranty.client_phone_formatted,
                pa_address: warranty.address.address1,    
                pa_city: warranty.address.city,    
                pa_state: warranty.address.state,    
                pa_zip: warranty.address.zip,    
                pa_email: warranty.client_email, 
                homebinder_warranty_id: warranty.id, 
                homebinder_id: warranty.binder_id, 
                partner_account_number: partner.warranty_configuration.account_number, 
                partner_company_name: partner.name,
                # pa_order_notes: "Order Notes",    
                # pa_referred_by: "Referred By",    
                # pa_referred_by_notes: "Referral Notes",    
                # ma_first_name: "Mailing Address First Name",    
                # ma_last_name: "Mailing Address Last Name",    
                # ma_address: "Mailing Street Address",    
                # ma_city: "Mailing Address City",    
                # ma_state: "Mailing Address State or Province (abbreviation)",    
                # ma_zip: "Mailing Address Zip or Postal Code",    
                # ma_email: "Mailing Address Email",    
                # ma_phone: "Mailing Address Phone",    
                # ba_first_name: "Billing Address First Name",    
                # ba_last_name: "Billing Address Last Name",    
                # ba_address: "Billing Street Address",    
                # ba_city: "Billing Address City",    
                # ba_state: "Billing Address State (abbreviation)",    
                # ba_zip: "Billing Address Zip",    
                # ba_email: "Billing Address Email",    
                # ba_phone: "Billing Address Phone",    
                # property_manager_company: "Property Manager Company Name",    
                # property_manager_contact: "Property Manager Contact Name",
            }
        }
        # headers
        headers = {"Content-Type" => "application/json"}

        # make the call
        response = HTTParty.post(URI.escape(url), :body => JSON.generate(data), :headers => headers)
        
        begin
            if response.code === 201
                result = JSON.parse(response.body)
                return result["orderId"]
            else
                result = JSON.parse(response.body)
                raise BadRequestException.new "Warranties::CP::Request Error - #{result["errors"].first}"
            end
        rescue => e
            raise BadRequestException.new "Warranties::CP::Request Error - #{e.message} - WarrantyID - #{warranty_id}"
        end
    end
    
    def url
        if Host.path === Host::PRODUCTION_HOST
            raise BadRequestException.new "CP Production URL Required" unless ENV["CP_PRODUCTION_URL"].present?
            ENV["CP_PRODUCTION_URL"].to_s
        else
            raise BadRequestException.new "CP Test URL Required" unless ENV["CP_TEST_URL"].present?
            ENV["CP_TEST_URL"].to_s
        end
    end
    
    def api_key
        raise BadRequestException.new "CP API KEY Required" unless ENV["CP_API_KEY"].present?
        ENV["CP_API_KEY"].to_s
    end
end