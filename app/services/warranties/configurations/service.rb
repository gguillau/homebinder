class Warranties::Configurations::Service

    # initalizes the service with a warranty config ID
    
    def initialize(id)
        @warranty_configuration = WarrantyConfiguration.find(id)
    end
    
    # send warranty information to the warranty_company
    # creates the warranty and retrieves the warranty config, warranty plan, etc
    
    def order(user, params)
        # check for required fields
        raise BadRequestException.new "Order Required" unless params[:order].present?
        raise BadRequestException.new "Homeowner Required" unless params[:order][:homeowner].present?
        raise BadRequestException.new "Property Required" unless params[:order][:property].present?
        raise BadRequestException.new "Inspection Date Required" unless params[:order][:inspection_date].present?
        raise BadRequestException.new "Binder ID Required" unless params[:order][:binder_id].present?
        raise BadRequestException.new "Warranty Plan Required" unless params[:order][:warranty_plan_id].present?
        raise BadRequestException.new "Warranty Price Required" unless params[:order][:price].present?
        
        # update the homeowner information
        homeowner = update_homeowner(params[:order][:homeowner])

        # create the warranty
        warranty = create_warranty(user, params)

        # create partner binder transaction
        PartnerBinderTransaction.create!(:partner_id => @warranty_configuration.partner.id, :binder_transaction_id => warranty.binder_transaction.id)
        
        # analytics
        EventService.perform_async({:event_name => "created", :event_type => "warranty", :user_id => user.id, :user_role => user.role, :user_created_at => user.created_at, :warranty_plan => warranty.warranty_plan.name, :binder_id => warranty.binder.id})
        
        # send warranty information through api
        
        warranty.order_id = Warranties::CP::Request.new.send_request(warranty.id, params[:order][:inspection_date])
        #warranty.status = "submitted"
        warranty.save!
        
        return warranty if homeowner.nil?
        
        # connect the transaction to the homeowner
        UserBinderTransaction.create!(:user_id => homeowner.id, :binder_transaction_id => warranty.binder_transaction.id)
        
        return warranty
    end
    
    # updates the homeowner connected to a binder
    
    def update_homeowner(homeowner)
        raise BadRequestException.new "Homeowner User ID Required" unless homeowner[:id].present?
        raise BadRequestException.new "Homeowner First Name Required" unless homeowner[:firstName].present?
        raise BadRequestException.new "Homeowner Last Name Required" unless homeowner[:lastName].present?
        raise BadRequestException.new "Homeowner Email Required" unless homeowner[:email].present?
        raise BadRequestException.new "Homeowner Phone Required" unless homeowner[:phone].present?
        
        # find the homeowner by email
        user = User.find_by_id(homeowner[:id])
        return nil if user.nil?
        # set up the homeowner hash
        update = {
            :email => homeowner[:email],
            :user_profile_attributes => {
                :first_name => homeowner[:firstName],
                :last_name => homeowner[:lastName],
                :mobile_phone => homeowner[:phone]
            }
        }
        
        update = ActionController::Parameters.new(update)
        
        # update the user
        if not user.update_attributes(update.permit!)
            raise UnprocessableException.new(user)
        end
        
        return user
    end
    
    # creates a warranty
    
    def create_warranty(user, params)
        raise BadRequestException.new "Property Address1 Required" unless params[:order][:property][:address1].present?
        raise BadRequestException.new "Property City Required" unless params[:order][:property][:city].present?
        raise BadRequestException.new "Property State Required" unless params[:order][:property][:state].present?
        raise BadRequestException.new "Property Country Required" unless params[:order][:property][:country].present?
        
        # set the expiration date
        warranty_plan = WarrantyPlan.find(params[:order][:warranty_plan_id])
        expiration_date = 5.days
        
        # get the homeowner and property
        homeowner = params[:order][:homeowner]
        property = {
            address1: params[:order][:property][:address1],
            address2: params[:order][:property][:address2],
            city: params[:order][:property][:city],
            state: params[:order][:property][:state],
            country: params[:order][:property][:country],
            zip: params[:order][:property][:zip].blank? ? "02210" : params[:order][:property][:zip]
        }
        
        case warranty_plan.cycle
            when "days"
                expiration_date = warranty_plan.duration.days
            when "weeks"
                expiration_date = warranty_plan.duration.weeks
            when "months"
                expiration_date = warranty_plan.duration.months
            when "years"
                expiration_date = warranty_plan.duration.years
        end

        # set the warranty hash
        data = {
            warranty: {
                :binder_id => params[:order][:binder_id],
                :status => "pending",
                :warranty_plan_id => params[:order][:warranty_plan_id],
                # set warranty expiration date to warranty_plan duration + inspection date
                :expiration_date => params[:order][:inspection_date].to_date + expiration_date,
                :client_first => homeowner[:firstName],
                :client_last => homeowner[:lastName],
                :client_email => homeowner[:email],
                :client_phone => homeowner[:phone],
                :address_attributes => property
            },
            price: params[:order][:price]
        }
         
        data = ActionController::Parameters.new(data)
        request = HBRequest.new.create_request(user)
        # create the warranty
        warranty = Warranty.build(request, data)
        
        # create the inspection transaction
        Warranties::Service.new(warranty.id).create_inspection_transaction(params[:order][:inspection_date])
            
        # create the warranty transaction
        Warranties::Service.new(warranty.id).create_warranty_transaction(params[:order][:price])
        
        return warranty
    end
    
end