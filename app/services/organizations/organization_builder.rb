module Organizations
  class OrganizationBuilder
    def initialize(user)
      @user = user
    end
    
    def add_partner(params = {})
      load_and_authorize_write params
      
      partner_ids = params[:partners]
      if partner_ids.present?
        
        # make sure ther are no duplicates
        partner_ids = partner_ids.uniq
        
        # verify all the partners exist and aren't already added
        partner_ids.each do |pid|
          Partner.find(pid)
          
          if OrganizationPartner.where(organization_id: @org.id, partner_id: pid).count > 0
            raise BadRequestException.new I18n.t(:err_organization_partner_exists)
          end
        end
        
        # add the partners
        org_partners = [];
        partner_ids.each do |pid|
          org_partners << { organization_id: @org.id, partner_id: pid }
        end
        
        OrganizationPartner.create!(org_partners)
      end
    end
    
    def remove_partner(params = {})
      load_and_authorize_write params
      
      org_id = params[:id]
      partner_id = params[:partner_id]
      OrganizationPartner.where(organization_id: org_id, partner_id: partner_id).destroy_all
    end
    
    def add_user(params = {})
      load_and_authorize_write params
      
      user_ids = params[:users]
      if user_ids.present?
        # ensure there are no duplicate ids
        user_ids = user_ids.uniq
        
        # verify all users exist
        user_ids.each do |uid|
          User.find(uid)
          
          if OrganizationUser.where(organization_id: @org.id, user_id: uid).count > 0
            raise BadRequestException.new I18n.t(:err_organization_user_exists)
          end
        end
        
        # add the users
        org_users = []
        user_ids.each do |uid|
          org_users << { organization_id: @org.id, user_id: uid }
        end
        
        OrganizationUser.create!(org_users)
      end
    end
    
    def remove_user(params = {})
      load_and_authorize_write params
      
      org_id = params[:id]
      user_id = params[:user_id]
      
      OrganizationUser.where(organization_id: org_id, user_id: user_id).destroy_all
    end
    
    private
    
    def load_and_authorize_write(params)
      org_id = params[:id]
      raise BadRequestException.new unless org_id.present?
      
      # find the org
      @org = Organization.find(org_id)
      
      # verify the user can update the org
      @ability ||= Ability.new(@user)
      @ability.authorize! :write, @org, :message => "You are not authorized to access organization ID: #{@org.id}."
    end
    
  end
end