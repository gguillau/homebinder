require "aws-sdk"

class S3FileService
  
  TEST_S3_BUCKET = "homebinder_unittest".freeze
  S3_ACCESS_KEY = "S3_ACCESS_KEY".freeze
  S3_SECRET_KEY = "S3_SECRET_KEY".freeze
  S3_REGION = "AWS_REGION".freeze
  S3_BUCKET = "S3_BUCKET".freeze
  S3_ERROR = "Error copying %s to %s: %s".freeze
  S3_URL = "S3_URL".freeze
  
  def self.load_object(resource)
    # create an instance of the s3 object
    s3 = load_s3
    bucket = Rails.env.test? ? TEST_S3_BUCKET : ENV[S3_BUCKET]
    # get the bucket
    bucket = s3.bucket(bucket)
    
    # get the object
    if resource.file.nil?
      return nil
    end
    
    key = resource.file.path
    if key.nil?
      return nil
    end
      
    key = key[1..key.length]
    s3_file = bucket.object(key)
      
    return s3_file
  end
  
  def self.url_for(resource, opts = {})
    defaults = { :expire => true }
    opts = defaults.merge(opts)
    
    expire_at = opts[:expire] ?
      Time.now + Rails.application.config.access_tokens[:token_refresh_interval] :
      Time.now + Rails.application.config.access_tokens[:long_token_refresh_interval]

    if resource
      s3_obj = load_object(resource)
      if s3_obj and s3_obj.exists?
        time = expire_at.to_i - Time.now.to_i
        s3_obj.presigned_url(:get, expires_in: time)
      else
        return nil
      end
    else
      return nil
    end
  end

  def self.load_s3
    Aws::S3::Resource.new(:client => load_client)
  end
  
  def self.load_client
    Aws::S3::Client.new(
        :credentials => Aws::Credentials.new(ENV[S3FileService::S3_ACCESS_KEY], ENV[S3FileService::S3_SECRET_KEY]),
        :region => ENV[S3FileService::S3_REGION]
    )
  end
  
  # downloads the paperclip attachment to the tmp folder and returns the file path
  # takes a model object such as a logo, image or headshot
  
  def self.download(object)
    # create an instance of the s3 client
    s3 = load_client
    
    # get the object file path in AWS
    path = object.file.path
    # remove the leading forward slash
    path.slice!(0)
    
    # Create Directory
    filename = "#{Rails.root}/tmp/logos/#{object.file.path}"
    FileUtils.mkdir_p  File.dirname(filename)
    
    # download the file to the tmp folder using the filename
    s3.get_object({bucket: ENV['S3_BUCKET'], key: path}, target: filename)

    return filename
  end
  
end