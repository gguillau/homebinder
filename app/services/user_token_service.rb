require 'jwt'

class UserTokenService

    USER_TOKEN = "user_token".freeze

    def self.create_jwt_token(payload)
        JWT.encode(payload, JWT_SECRET)
    end

    def self.create_jwt(user, params = {})
        token = user.generate_authentication_token
        payload = {
            id: user.id,
            email: user.email,
            user_token: token,
            role: user.role,
            created_at: user.created_at,
            create_method: user.create_method,
            default_locale: user.user_profile.default_locale,
            time: (Time.now.to_f * 1000).to_i + 5000,
            intercom_key: OpenSSL::HMAC.hexdigest('sha256', get_identity_key, user.email)
        }
        puser = PartnerUser.where(:user_id => user.id).where("role = ? OR role = ?", "admin", "member").first
        if puser.present?
            payload[:partner_id] = puser.partner_id
            payload[:partner_role] = puser.role
        end
        if user.organizations.count > 0
            payload[:organization_id] = user.organizations[0].id
        end
        
        Session.create!(
            :token => token, 
            :expires_at => Date.today + 30.days, 
            :user_id => user.id, 
            :device_type => params.dig(:device_type), 
            :device_name => params.dig(:device_name),
            :device_version => params.dig(:device_version),
            :operating_system => params.dig(:operating_system)
        )
        
        return create_jwt_token(payload)
    end

    def self.verify_jwt_token(token)
        begin
            jwt = JWT.decode(token, JWT_SECRET)
        rescue JWT::DecodeError
            raise InvalidUserTokenException
        end
        user = User.joins(:sessions).where(:sessions => {:token => jwt[0][USER_TOKEN], :status => 0}).first
        if not user
            raise InvalidUserTokenException
        else
            user
        end
    end

    def self.get_identity_key
        if Host.path === Host::PRODUCTION_HOST
            return ENV["INTERCOM_PRODUCTION_IDENTITY_SECRET"]
        else
            return ENV["INTERCOM_TEST_IDENTITY_SECRET"]
        end
    end

    private

    JWT_SECRET = "b!nd3rsRg00D".freeze

end
