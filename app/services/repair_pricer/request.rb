require "httparty"

class RepairPricer::Request
  
    def post_call(url, body, headers)
        response = ::HTTParty.post(URI.escape(url), :body => body, :headers => headers)
        begin
            parse_response(response)
        rescue => e
            raise BadRequestException.new "RepairPricer::Request Error - #{e.message}"
        end
    end
    
    def parse_response(response)
        if response.code === 201 || response.code === 200
            return JSON.parse(response.body)
        else
            error = JSON.parse(response.body)
            raise BadRequestException.new error["error"]
        end
    end
  
end