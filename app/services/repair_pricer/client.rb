require 'base64'
require 'uri'

# https://api.repairpriceronline.com/docs/

# 0 : report does not exist
# 10 : report received
# 20 : report accepted (all information present)
# 30 : report estimate process started
# 40 : report on hold pending pricing information
# 50 : report ready for download

class RepairPricer::Client
    
    def create_report(params)
        raise BadRequestException.new "Parameters Required" if params.blank?
        raise BadRequestException.new "Repair Pricer Report ID Required" if params[:id].blank?
        raise BadRequestException.new "Inspection Report Required" if params[:fileurl].blank?
        raise BadRequestException.new "First Name Required" if params[:fname].blank?
        raise BadRequestException.new "Email Required" if params[:email].blank?
        raise BadRequestException.new "Last Name Required" if params[:lname].blank?
        raise BadRequestException.new "Address Required" if params[:address1].blank?
        raise BadRequestException.new "City Required" if params[:city].blank?
        raise BadRequestException.new "State Required" if params[:state].blank?
        raise BadRequestException.new "Postal Code Required" if params[:zip].blank?
        
        fileurl = params[:fileurl]
        
        data = {
            fileurl: fileurl,
            token: api_token,
            fname: params[:fname],
            lname: params[:lname],
            email: params[:email],
            phone: params[:phone],
            address1: params[:address1],
            address2: params[:address2],
            city: params[:city],
            state: params[:state],
            zip: params[:zip],
            inspid: params[:inspid],
            poolurl: params[:pool_url],
            ref: params[:id],
            type: params[:report_type],
            hh: params[:home_history_report],
            rush: params[:rush_report],
            pool: params[:pool_report]
        }

        if !Rails.env.production?
            data[:ref] = "ID:#{params[:id]}-TIME:#{Time.now.to_i}"
        end
        
        json = JSON.generate(data)
        
        # headers
        headers = {"Content-Length" => "#{json.length}", "Content-Type" => "application/json"}
        
        # make the call
        result = RepairPricer::Request.new.post_call(api_url, json, headers)
        
        repair_pricer_report = RepairPricerReport.find(params[:id])
        repair_pricer_report.report_id = result["id"]
        case result["status"]
        when "10"
            repair_pricer_report.status = 1
        when "20"
            repair_pricer_report.status = 2
        when "30"
            repair_pricer_report.status = 3
        when "40"
            repair_pricer_report.status = 4
        when "50"
            repair_pricer_report.status = 5
        end
        repair_pricer_report.save!
    end
    
    def get_report(id)
        raise BadRequestException.new "Report ID Required" if id.blank?
        
        url = "https://api.repairpriceronline.com/report/"
        
        data = {
            token: api_token,
            id: id
        }
        
        json = JSON.generate(data)
        
        # headers
        headers = {"Content-Length" => "#{json.length}", "Content-Type" => "application/json"}
        
        # make the call
        return RepairPricer::Request.new.post_call(url, json, headers)
    end
    
    def download_report(id)
        raise BadRequestException.new "Report ID Required" if id.blank?
        
        url = "https://api.repairpriceronline.com/report/repairs/"
        
        data = {
            token: api_token,
            id: id
        }
        
        json = JSON.generate(data)
        
        # headers
        headers = {"Content-Length" => "#{json.length}", "Content-Type" => "application/json"}
        
        # make the call
        return RepairPricer::Request.new.post_call(url, json, headers)
    end
    
    def api_token
        raise BadRequestException.new "Repair Pricer API Token Required" if ENV["REPAIR_PRICER_API_TOKEN"].blank?
        ENV["REPAIR_PRICER_API_TOKEN"]
    end
    
    def api_url
        raise BadRequestException.new "Repair Pricer API Token Required" if ENV["REPAIR_PRICER_API_TOKEN"].blank?
        ENV["REPAIR_PRICER_API_URL"]
    end
end
