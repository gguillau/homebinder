class BinderService < BaseService

    FREE = "free".freeze

    def check_primary
        if item.primary?
            user.binders.where(:primary => true).find_each do |primary|
                if primary.id != item.id
                    primary.primary = false
                    primary.save!
                end
            end
        end
    end

    def after_create(params)
        create_partner_binders(params)
        
        create_binder_brandings(params)
        
        # check if we need to link to a partner
        if (user.belongs_to_partner? and user.role != "agent") || params[:partner_id].present?
            
            # if an admin then we get the binder owner AKA the user who's branded on/owns the binder
            self.user = item.owner if user.is_admin?
            
            partner = user.partners[0] || Partner.find(params[:partner_id])

            Partner::Automation::Service.delay(queue: 'abs').add_additional_services(item.id, params[:inspection_date], user.id, partner.id, params[:homeowner_id])
            
            branded_user = partner.partner_configuration.default_branded_profile.user
            
            Partner::Automation::Service.delay(queue: 'abs').add_partner_user_as_home_pro(branded_user.id, partner.id, item.id)
            
            # add template items
            if params[:binder_template_id]
                template = BinderTemplate.find(params[:binder_template_id])
                Partner::Automation::Service.delay(queue: 'abs').add_transfer_note(item.id, template.id)
                
                # add appliances to binder
                Partner::Automation::Service.delay(queue: 'abs').add_appliances([], item.id, template.id, user.id)
        
                # add maintenance items to binder
                Partner::Automation::Service.delay(queue: 'abs').add_maintenance_items([], item.id, template.id, user.id)
                
                # add contractors to binder
                Partner::Automation::Service.delay(queue: 'abs').add_contractors([], item.id, template.id, user.id)
        
                # add projects to binder
                Partner::Automation::Service.delay(queue: 'abs').add_projects([], item.id)
        
                # add documents to binder
                Partner::Automation::Service.delay(queue: 'abs').add_template_docs(item.id, template.id)
                
            end

            options = {:transaction_type => params[:transaction_type], :project_number => params[:project_number], :inspection_date => params[:inspection_date]}
            Partner::Automation::Service.delay(queue: 'abs').create_transaction(item.id, options, partner.id)
        elsif user.is_agent?
            # add user_contractors to binder
            user.user_contractor_ids.each do |user_contractor_id|
                Binders::Contractors::AddUserContractorsJob.perform_async(user_contractor_id)
            end
        elsif user.is_homeowner?
            inject_homeowner_guide
            
            # Widgets - hard coding for now
            Widgets::WidgetService.new(user).add_widget("Third Party Services", item)
        end
        
        # if this is set to primary make sure there are no other primaries
        check_primary
        
        Zillow::DownloadBinderDetailsJob.perform_async(item.id)

        # analytics
        Binders::BinderAnalytics.delay(run_at: Time.now, queue: "analytics").created(item.id)

        # Widgets - hard coding for now
        Widgets::WidgetService.new(user).add_widget("APR Widget", item)
    end
    
    def create_partner_binders(params)
        partner_user_id = nil
        
        # get the branded partner user
        if params[:binder_brandings]
            binder_branding = params[:binder_brandings].select {|branding| branding[:scope] === "transfer_email"}.first
            if binder_branding.present?
                partner_user_id = PartnerUser.joins(:user => :user_profile).where(:user_profiles => {:id => binder_branding[:user_branding_id]}).first.try(:id)
            end
        end
        
        # link the partner
        if params[:partner_binders]
            params[:partner_binders].each do |partner_binder|
                # get the default dashboard for the partner
                dashboard_id = Dashboard.where(:partner_id => partner_binder[:partner_id], :default => true, :scope => "binder").pluck(:id).first
                partner = Partner.find_by_id(partner_binder[:partner_id])
                next if partner.nil?
                if !partner_user_id
                    partner_user_id = PartnerUser.joins(:user => :user_profile).where(:user_profiles => {:id => partner.partner_configuration.default_user_branding_id}).first.try(:id)
                end
                PartnerBinder.create(partner_id: partner_binder[:partner_id], binder_id: item.id, role: partner_binder[:role], dashboard_id: dashboard_id, client_id: params[:homeowner_id], :repair_pricer_enabled => partner.partner_configuration.repair_pricer_pre_paid_reports, :partner_user_id => partner_user_id)
            end
        end
    end
    
    def create_binder_brandings(params)
        # link the brandings
        if params[:binder_brandings]
            params[:binder_brandings].each do |binder_branding|
                Binder::BinderBranding.new(partner_id: binder_branding[:partner_id], binder_id: item.id, scope: binder_branding[:scope], user_branding_id: binder_branding[:user_branding_id]).save
            end
        end
    end

    def orphan
        # remove any owners/co-owners
        UserBinder.where(:binder_id => item.id, :role => "owner").destroy_all
        UserBinder.where(:binder_id => item.id, :role => "co_owner").destroy_all

        # orphan the binder
        item.orphan

        return item
    end

    def send_partner_feedback
        # get the user branded on the binder
        branding = item.binder_brandings.where(:scope => "binder").first
        return if branding.nil?
        return if branding.user_profile.nil?

        # get the email
        email = branding.user_profile.email
        return if email.nil?

        # send the feedback email
        AccountMailer.send_partner_feedback(user.id, email).deliver_later

        return if branding.user_profile.user.partners.count < 1
        partner = branding.user_profile.user.partners.first

        # analytics
        EventService.perform_async({:event_name => "feedback_given", :event_type => "widget", :user_id => user.id, :user_role => user.role, :user_created_at => user.created_at, :partner_id => partner.id, :partner_type => partner.partner_type, :branded_email => branding.user_profile.user.email})
    end
    
    def secure24
        # get the user branded on the binder
        branding = item.binder_brandings.where(:scope => "binder").first
        return if branding.nil?
        return if branding.user_profile.nil?

        # get the email
        email = branding.user_profile.email
        return if email.nil?
        return if branding.user_profile.user.partners.count < 1
        partner = branding.user_profile.user.partners.first

        # send the secure24 email
        AccountMailer.secure24(user.id, partner.id).deliver_later
        
        # analytics
        EventService.perform_async({:event_name => "secure24_request", :event_type => "widget", :user_id => user.id, :user_created_at => user.created_at, :user_role => user.role, :partner_id => partner.id, :partner_type => partner.partner_type, :branded_email => branding.user_profile.user.email})
    end

    def requestSolarServices(params)
        service = "Receive info about switching to solar power"

        url = EnergysageService.new.request_services(item.id)
            
        EventService.perform_async({:event_name => service, :event_type => "services_requested", :binder_id => item.id, :user_id => user.id, :user_role => user.role, :user_created_at => user.created_at})
            
        return url
    end

    def learn_more_apr
        AccountMailer.apr_widget(user.id, item.id, ["Learn more about the Annual Property Review"]).deliver_later

        # analytics
        EventService.perform_async({:event_name => "learn_more_apr", :event_type => "widget", :user_id => user.id, :user_created_at => user.created_at})
    end

    def add_user(params)
        # ensure correct params were passed in
        raise BadRequestException.new "User ID required" unless params[:user_id]
        raise BadRequestException.new "Role required" unless params[:role]

        begin
            role = UserBinderRoles.map(params[:role])
        rescue BadRequestException
            raise BadRequestException.new "role must be 'co_owner', 'reader', or 'writer'"
        end

        # verify the user can add
        user.ability.authorize! :write, item, :message => "You are not authorized to access binder ID: #{item.id}."

        # find the user
        added_user = User.find(params[:user_id])

        # get the role
        role = params[:role]

        # add the user
        user_binder = UserBinder.new(:user_id => added_user.id, :binder_id => item.id, :role => role, :access_repair_pricer => params[:access_repair_pricer])
        user_binder.save!

        # if added user is a buyer_agent we add user as binder contractor
        if role === "buyer_agent"
            Partner::Automation::Service.delay(queue: 'abs').add_agent_as_home_pro(added_user.id, item.id, user.id)
        end

        return user_binder
    end

    def get_acl
        results = []
        # get all the users with access to the binder and their roles
        item.user_binders.each do |u|
            user_binder = UserBinder.find(u.id)
            results << { user: UserSerializer.new(user_binder.user).attributes, role: user_binder.role, id: user_binder.id, access_repair_pricer: user_binder.access_repair_pricer }
        end

        return results
    end

    def delete_acl(params)
        # ensure correct params were passed in
        raise BadRequestException.new "User ID required" unless params[:user_id]

        # verify the user can write
        user.ability.authorize! :write, item, :message => "You are not authorized to access binder ID: #{item.id}."

        from_user = User.where(:id => params[:user_id]).first
        return if from_user.nil?

        binder = from_user.binders.where(:id => item.id).first
        return if binder.nil?

        UserBinder.where(user_id: from_user.id, binder_id: binder.id).delete_all
    end

    def add_partner(params)
        # check if required fields are present
        raise BadRequestException.new "PartnerID and Role required" if params[:data].nil?
        raise BadRequestException.new "Role required" if params[:data][:role].nil?
        raise BadRequestException.new "Partner ID required" if params[:data][:partner_id].nil?
        raise BadRequestException.new "Homeowner ID required" if params[:data][:client_id].nil?

        # check if the user can add the partner
        user.ability.authorize! :write, item, :message => "You are not authorized to access binder ID: #{item.id}."

        role = params[:data][:role]
        partner_id = params[:data][:partner_id]
        client_id = params[:data][:client_id]
        # check if partner binder already exists
        partner_binder = PartnerBinder.where(:binder_id => item.id, :partner_id => partner_id, :role => role, :client_id => client_id).first
        return partner_binder unless partner_binder.nil?
        
        partner = Partner.find(partner_id)
        partner_user_id = PartnerUser.joins(:user => :user_profile).where(:user_profiles => {:id => partner.partner_configuration.default_user_branding_id}).first.try(:id) || partner.partner_user_ids.first

        # create the partner binder
        PartnerBinder.new(:binder_id => item.id, :partner_id => partner_id, :role => role, :client_id => client_id, :partner_user_id => partner_user_id).save!

        return partner_binder
    end

    def get_partner
        # get the partner binder object
        partner_binder = PartnerBinder.where(binder_id: item.id).first
        raise NotFoundException unless partner_binder.present?

        # get the partner
        partner = partner_binder.partner

        #return partner
        hash = partner.attributes
        hash[:warranties_configuration] = partner.warranty_configuration

        return hash
    end

    def delete_partner(params)
        # check if required fields are present
        raise BadRequestException.new "Partner Binder ID required" if params[:partner_binder_id].nil?

        # get the partner binder object so we can check the role
        partner_binder = PartnerBinder.where(binder_id: item.id, id: params[:partner_binder_id]).first
        raise NotFoundException unless partner_binder.present?

        # check if the user can delete it
        user.ability.authorize! :write, item, :message => "You are not authorized to access binder ID: #{item.id}."

        # delete the partner binder
        partner_binder.remove
    end

    # this is a duplicate of the method in SubscriptionService but I added this method
    # so we could create the subscription in the background
    def upgrade(coupon)
        subscription = item.subscription
        return if subscription.nil?

        id = SubscriptionService.get_plan_id(SubscriptionService::STANDARD)

        # upgrade the plan
        subscription.plan_id = id

        # create the customer in stripe
        begin
            retries ||= 0
            cust = Stripe::Customer.create(:description => SubscriptionService::DESCRIPTION % [subscription.binder_id, subscription.binder.owner.email])
        rescue => e
            retry if (retries += 1) < 3
            raise BadRequestException.new(e.message)
        end

        cust.plan = subscription.plan_id
        cust.coupon = coupon unless coupon.nil? or coupon.empty?
        cust.save

        # store the stripe customer id and mark the subscription as paid
        subscription.customer_id = cust.id
        subscription.payment_status = SubscriptionService::PAID

        if not subscription.save
            raise UnprocessableException.new(subscription)
        end
        return subscription
    end
    
    def inventory_report
        BinderMailer.inventory_report(item.id, user.id).deliver_later
        EventService.perform_async({:event_type => "engagement", :event_name => "inventory_report_created", :user_id => user.id, :user_role => user.role, :user_created_at => user.created_at, :user_create_method => user.create_method, :binder_id => item.id})
    end
    
    def capital_expense_report
        BinderMailer.capital_expense_report(item.id, user.id).deliver_later
        EventService.perform_async({:event_type => "engagement", :event_name => "capital_expense_report_created", :user_id => user.id, :user_role => user.role, :user_created_at => user.created_at, :user_create_method => user.create_method, :binder_id => item.id})
    end
    
    def inject_homeowner_guide
        template_document = Template::Document.joins(:binder_template).where(:binder_templates => {:system => true}).first
        return if template_document.nil?

        create_aws
        
        item.documents.create!({
            :file =>  download('pdf/New Homeowner Guide Final.pdf'), 
            :file_file_name => "New Homeowner Guide.pdf", 
            :document_type_id => DocumentType.find_by(:name => "Other").id, 
            :library_source_id => template_document.id, 
            :accessible => false,
            :details => "This book is your key to HomeBinder’s tool chest, as well as a summary of the many aspects of home maintenance that preserve and enhance the value of your property."})
    end
        
    def create_aws
        @s3 = S3FileService.load_s3
        
        # get the bucket
        @bucket = @s3.bucket("homebinderstatic")
    end
    
    def download(path)
        URI.parse(@bucket.object(path).presigned_url(:get, expires_in: 60))
    end
end