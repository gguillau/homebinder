require 'date'
require 'open-uri'
require 'uri'
require 'prawn'
require 'prawn/table'

class CapitalExpenseReportPdfService < PdfService
    include ActionView::Helpers::NumberHelper
    
    attr_accessor :binder, :user
    
    def initialize(binder, user)
        self.binder = binder
        self.user = user
        super(binder)
        self.text = "Capital Expense Report for"
    end

    def create
        init
        add_logos
        add_title
        calculate_cost
        add_header
        add_receipts
        add_total_cost
        add_footer
        pdf.render
    end
    
    def calculate_cost
        costs = 0
        binder.receipts.each do |receipt|
            next if receipt.purchase.nil?
            costs += receipt.purchase.price.to_i
        end
        
        if binder.property.purchase_price.present?
            @cost_basis = number_to_currency(costs + binder.property.purchase_price, :precision => 2)
        end
        
        @total_cost = number_to_currency(costs, :precision => 2)
    end
    
    def add_header
        pdf.move_down(35)
        
        pdf.formatted_text [
            {:text => "Reporting Time Frame: ", :styles => [:bold], :at => [300, pdf.cursor - 10], :size => 14}, 
            {:text => "#{purchase_date} - #{Date.today.strftime("%A, %B %d, %Y")}", :styles => [:bold], :at => [300, pdf.cursor - 10], :size => 14, :color => "ff6633"}]
        
        pdf.move_down(50)
        
        pdf.formatted_text [
            {:text => "Purchase Price: ", :styles => [:bold], :at => [300, pdf.cursor - 10], :size => 14}, 
            {:text => "#{purchase_price}", :styles => [:bold], :at => [300, pdf.cursor - 10], :size => 14, :color => "ff6633"}]
        
        pdf.move_down(10)
        
        pdf.formatted_text [
            {:text => "Total Capital Expenditures: ", :styles => [:bold], :at => [300, pdf.cursor - 10], :size => 14}, 
            {:text => @total_cost, :styles => [:bold], :at => [300, pdf.cursor - 10], :size => 14, :color => "ff6633"}]
        
        pdf.stroke do
            pdf.stroke_color 'ff6633'
            pdf.stroke_horizontal_line(0, 300)
        end
        
        pdf.move_down(10)
            
        pdf.formatted_text [
            {:text => "New Cost Basis: ".upcase, :styles => [:bold], :at => [300, pdf.cursor - 10], :size => 14}, 
            {:text => "#{cost_basis}", :styles => [:bold], :at => [300, pdf.cursor - 10], :size => 14, :color => "ff6633"}]
    end
    
    def purchase_date
        return binder.property.purchase_date.strftime("%A, %B %d, %Y") if binder.property.purchase_date.present?
        return binder.receipts.order(:created_at).first.created_at.strftime("%A, %B %d, %Y") if binder.receipts.count > 0
        return Date.today.strftime("%A, %B %d, %Y")
    end
    
    def purchase_price
        return number_to_currency(binder.property.purchase_price, :precision => 2) if binder.property.purchase_price.present?
        return "Unknown"
    end
    
    def cost_basis
        if @cost_basis
            return @cost_basis
        else
            return "Unknown"
        end
    end
    
    def add_receipts
        pdf.move_down(35)
        receipts = binder.receipts.includes(:purchase).order("purchases.date")

        if (receipts.length > 0)

            receiptdata = [[ "RECEIPT NAME", "DATE", "COST" ]]
            receipts.each do |receipt|
                date = receipt.purchase.present? ? receipt.purchase.date : "None"
                price = receipt.purchase.present? ? number_to_currency(receipt.purchase.price, :precision => 2) : 0
                receiptdata += [[ receipt.name, date, "#{price}" ]]
            end
        else
            receiptdata = [[{:content => "No Receipts found", :colspan => 6}]]
        end
        
        pdf.table(receiptdata, :header => true, :width => pdf.bounds.width, :cell_style => { :inline_format => true }) do
            cells.padding       = 6
            cells.borders       = [:bottom]
            cells.size          = 9
            cells.border_color  = "686868"

            row(0).borders      = [:bottom]
            row(0).border_width = 1
            row(0).font_style   = :bold
            row(0).size         = 8

            column(2).align     = :right
        end
    end
    
    def add_total_cost
        pdf.move_down(35)
        
        pdf.move_down(35)
        
        pdf.text "Total Capital Expenditures: ", :align => :right, :style => :bold
        
        pdf.text "#{@total_cost}", :align => :right, :style => :bold, :color => "ff6633"
    end
    
    def add_footer
        # footer
        pdf.repeat(:all, :dynamic => true) do
            pdf.formatted_text_box [
                { :text => "Website: www.homebinder.com\n", :size => 8, :color => "686868" },
                { :text => "Call support: 800.377.6915", :size => 8, :color => "686868" }
            ], :at => [0, -45], :width => 400, :height => 50
            
            pdf.formatted_text_box [
                { :text => "Report Date: #{Date.today}", :size => 8, :color => "686868" }
            ], :at => [pdf.bounds.right-90, -45], :width => 100, :height => 50
        end
    end

end
