require 'intercom'

module Users
  class UserAnalytics
    USER_REGISTERED = "user-registered".freeze
    USER_LOGGED_ON = "user-loggedon".freeze
    
    def self.update_user(user_id)
      user = User.find_by_id(user_id)
      return if user.nil?
      
      # create the user metadata
      custom_attributes = create_user_attributes(user)
      companies = []
      
      # get the user's company
      role = user.role
      if role == "inspector" or role == "broker" and user.partners.length > 0
        companies = [{company_id: user.partners[0].id}]
      end
        
      begin
        intercom = Intercom::HomeBinderClient.new.intercom
        # create the user in intercom
        intercom_user = intercom.users.create(
          user_id: user.id.to_s,
          email: user.email,
          name: "#{user.user_profile.first_name} #{user.user_profile.last_name}",
          custom_attributes: custom_attributes,
          last_request_at: Time.now.utc,
          companies: companies)
        
        # save the intercom id
        user.intercom_id = intercom_user.id
        user.save!
      rescue => e
        ErrorService.perform_async(e.message, {user_id: user.id})
      end
      
      update_salesforce_user(user)
    end
    
    def self.created(user_id)
      user = User.find_by_id(user_id)
      return if user.nil?
      
      # analytics
      EventService.perform_async({:event_name => "created", :event_type => "user", :user_id => user.id, :user_role => user.role, :user_created_at => user.created_at})
      
      create_salesforce_user(user)
      
      create_partner_client(user)
      
      update_user(user)
      
      begin  
        intercom = Intercom::HomeBinderClient.new.intercom
        # create an event the user logged on
        intercom.events.create(
          event_name: USER_REGISTERED,
          email: user.email,
          created_at: Time.now)
      rescue
      end
    end
    
    def self.logged_in(user_id)
      user = User.find_by_id(user_id)
      return if user.nil?
      
      begin
        intercom = Intercom::HomeBinderClient.new.intercom
        # update the user last request time
        intercom.users.create(
          email: user.email,
          last_request_at: Time.now.utc,
          new_session: true
        )
        
        # post an event
        intercom.events.create(
          event_name: USER_LOGGED_ON,
          email: user.email,
          created_at: Time.now)
      rescue
      end
      
      # analytics
      EventService.perform_async({:event_name => "logged_in", :event_type => "user", :user_id => user.id, :user_role => user.role, :user_created_at => user.created_at})
      
      update_salesforce_user(user)
    end
    
    def self.transfer_received(transfer_id)
      transfer = Binder::Transfer.find_by_id(transfer_id)
      return if transfer.nil?
      user = transfer.receiver
      return if user.nil?

      begin
        intercom = Intercom::HomeBinderClient.new.intercom
        # Find user by user_id
        intercom_user = intercom.users.find(user_id: user.id)    
        return if intercom_user.nil?
        
        intercom_user.custom_attributes = create_user_attributes(user)
        intercom_user.custom_attributes[:transfer_link] = "#{Host.path}/homeowners/#{transfer.access_token}/welcome"
        intercom_user.custom_attributes[:decline_link] = "#{Host.path}/homeowners/#{transfer.access_token}/decline"
      
        intercom.users.save(intercom_user)
      rescue => e
        ErrorService.perform_async(e.message, {user_id: user.id, task: "transfer_received"})
      end
    end
    
    def self.share_received(share_id)
      share = Binder::Share.find_by_id(share_id)
      return if share.nil?
      
      user = User.find_by_id(share.shared_with_id)
      retur if user.nil?
      
      begin
        # Find user by user_id
        intercom = Intercom::HomeBinderClient.new.intercom
        intercom_user = intercom.users.find(user_id: user.id)    
        return if intercom_user.nil?
        
        intercom_user.custom_attributes = create_user_attributes(user)
        intercom_user.custom_attributes[:share_link] = "#{Host.path}/homeowners/#{share.access_token}/welcome"
        intercom_user.custom_attributes[:decline_link] = "#{Host.path}/homeowners/#{share.access_token}/decline"
        intercom.users.save(intercom_user)
      rescue => e
        ErrorService.perform_async(e.message, {user_id: user.id, task: "share_received"})
      end
    end
    
    def self.destroyed(id)
      user = User.find_by_id(id)
      return if user.nil?
      
      begin
        # remove the user from intercom
        intercom = Intercom::HomeBinderClient.new.intercom
        intercom_user = intercom.users.find(:user_id => user.id)
        intercom.users.delete(intercom_user)
      rescue => e
        ErrorService.perform_async(e.message, {user_id: user.id})
      end

      # # delete the HomeBinder user
      user_id = user.id
      user_role = user.role
      user.destroy
      
      # analytics
      EventService.perform_async({:event_name => "destroyed", :event_type => "user", :user_id => user_id, :user_role => user_role, :user_created_at => user.created_at})
      
      Salesforce::Homeowner.new.delete_salesforce_object(user_id)
    end
    
    def self.create_salesforce_user(user)
      Salesforce::Homeowner.new.new_homeowner(user)
    end
    
    def self.update_salesforce_user(user)
      Salesforce::Homeowner.new.update_homeowner(user)
    end
    
    def self.create_partner_client(user)
      # make sure the creator is a partner
      return if user.creator.nil?
      return if !user.creator.belongs_to_partner?
      return if user.creator.partners.count < 1
      
      # create the partner user
      creator = user.creator
      partner = creator.partners[0]
      
      if PartnerClient.where(:partner_id => partner.id, :client_id => user.id).count < 1
        PartnerClient.create!(:partner_id => partner.id, :client_id => user.id)
      end
      
      user.user_profile.text_messaging_enabled = partner.partner_configuration.client_text_messaging_enabled
      user.user_profile.save
    end
    
    def self.create_user_attributes(user)
      
      custom_attributes = {
        role: user.role,
        create_method: user.create_method
      }

      # get the address of the user and the partner that created them if applicable
      if user.binders.count > 0
        # if the user was just created because of a transfer or share they
        # will only have 1 binder
        binder = user.binders[0]
        
        # check if homeowner has any upgraded binders
        if user.is_homeowner?
          custom_attributes[:upgraded_binders] = user.binders.joins(:subscription).where(:subscriptions => {:payment_status => "paid"}).count > 0
        end
        
        # address
        custom_attributes[:property_address] = "#{binder.property.address1} #{binder.property.city}, #{binder.property.state}" unless binder.property.nil?
        
        
        partner_binder = PartnerBinder.where(:binder_id => binder.id).first
        
        if partner_binder.present?
          partner = Partner.where(:id => partner_binder.partner_id).first
          if partner.present?
            custom_attributes[:register_partner_id] = partner_binder.partner_id
            custom_attributes[:register_partner_name] = partner.name
            custom_attributes[:register_partner_type] = partner.partner_type
            custom_attributes[:register_partner_repair_pricer_enabled] = partner.partner_configuration.repair_pricer_enabled
            custom_attributes[:register_partner_repair_pricer_id] = partner.partner_configuration.repair_pricer_id
            custom_attributes[:register_partner_repair_pricer_messaging_enabled] = partner.partner_configuration.repair_pricer_messaging_enabled
            custom_attributes[:register_partner_repair_pricer_pre_paid_reports] = partner.partner_configuration.repair_pricer_pre_paid_reports
          end
        end
      end
      
      if user.belongs_to_partner? and user.partners.count > 0
        custom_attributes[:partner_id] = user.partners[0].id
        custom_attributes[:partner_type] = user.partners[0].partner_type
        custom_attributes[:partner_name] = Partner.where(:id => user.partners[0].id).first.name
      end
      
      return custom_attributes
    end

    # unsubscribe user from all intercom emails
    def self.unsubscribe(user_id)
      user = User.find_by_id(user_id)
      return if user.nil?
      
      # find the user
      begin 
        intercom = Intercom::HomeBinderClient.new.intercom
        intercom_user = intercom.users.find(:user_id => user.id)
        # set unsubscribe to true
        intercom_user.unsubscribed_from_emails = true
        # save the user
        intercom.users.save(intercom_user)
      rescue => e
        ErrorService.perform_async(e.message, {user_id: user.id})
      end
      
      # analytics
      EventService.perform_async({:event_name => "unsubscribe", :event_type => "email", :user_id => user.id, :user_role => user.role, :user_created_at => user.created_at})
    end
  end
end