module Widgets
    class WidgetService
        
        def initialize(user)
            @user = user
        end
        
        def add_widget(widget_name, binder)
            return if @user.role != "homeowner"
            
            # find the dashboard
            dashboard = Dashboards::DashboardLookup.new(@user).for_binder({:id => binder.id})
            return if dashboard.nil?
            return if dashboard.default?  && dashboard.system?
            
            widget = Widget.find_by_name(widget_name)
            return if widget.nil?
            
            # add the widget
            DashboardWidget.create!(:dashboard_id => dashboard.id, :widget_id => widget.id, :index => 1)
        end
        
        # takes a widget name and binder and removes the widget from the
        # dashboard
        
        def remove_widget(widget_name, binder)
            # find the dashboard
            dashboard = Dashboards::DashboardLookup.new(@user).for_binder({:id => binder.id})
            return if dashboard.nil?
            
            widget = Widget.find_by_name(widget_name)
            return if widget.nil?
            
            DashboardWidget.where(:dashboard_id => dashboard.id, :widget_id => widget.id).delete_all
        end
    end
end