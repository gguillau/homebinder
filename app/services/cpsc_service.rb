#require 'ruby_odata'
require 'base64'
require 'httparty'
require 'uri'

# Class to call the CPSC API to check for product recalls
class CPSCService
  
  @@cpsc_url = "http://www.saferproducts.gov/RestWebServices/Recall"
  
  # query for all recalls that match out keywords. NOTE: this could be more efficient
  # by only going back to a certain date but since it's not a user facing operation
  # I'm not worryng about it.
  def self.get_new_recalls
    RecallKeyword.all.each do |kw|
      parameters = ["RecallTitle", "RecallDescription"]
      parameters.each do |p|
        get_recalls_by_parameter(p, kw.keyword)      
      end
    end
  end
  
  def self.get_recalls_by_parameter(param, value)
    service_url = "#{@@cpsc_url}?#{param}=#{value}&format=json"
    response = HTTParty.get(URI.escape(service_url))
    if response.code == 200
      begin
        recalls = JSON.parse(response.body)
      rescue => e
        ErrorService.perform_async(e.message, {url: service_url, message: response.body})
        return
      end
      recalls.each do |r|
        existing = Recall.find_by_number(r["RecallNumber"])
        if existing.nil?
          recall = Recall.new(number: r["RecallNumber"],
                              recall_date: r["RecallDate"],
                              details: JSON.generate(r),
                              verified: false,
                              ignore: false)
          recall.save!
        else
          # ensure we have the full details for recalls found with the old CPSC API
          if existing.details.nil?
            existing.details = JSON.generate(r)
            existing.save!
          end
        end
      end
    else
      ErrorService.perform_async(nil, {url: service_url, message: response.body, code: response.code})
    end
  end
  
  def self.get_all_recalls
    service_url = "#{@@cpsc_url}?format=json"
    response = HTTParty.get(URI.escape(service_url))
    if response.code == 200
      begin
        return JSON.parse(response.body)
      rescue => e
        ErrorService.perform_async(e.message, {url: service_url, message: response.body})
        return []
      end
    else
      ErrorService.perform_async("Error received from CPSC service", {url: service_url, message: response.body})
      return []
    end
  end
  
end