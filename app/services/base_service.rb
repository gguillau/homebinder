class BaseService
  
  attr_accessor :user, :item
  
  def initialize(user, item)
    raise BadRequestException.new "User required" if user.nil?
    raise BadRequestException.new "Item required" if item.nil?
    
    self.user = user
    self.item = item
    
    # check if the user can read the item
    user.ability.authorize! :read, item, :message => "You are not authorized to access #{item.class.name} ID: #{item.id}."
  end

end