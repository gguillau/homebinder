require 'mandrill'
require "erb"

class MandrillService
    
    BOUNCE_DESCRIPTION = "bounce_description".freeze
    MANDRILL_PASSWORD = "MANDRILL_PASSWORD".freeze
    MANDRILL_ERROR = "A mandrill error occurred: %s - %s".freeze
    HARD_BOUNCE = "hard%sbounce".freeze
    SOFT_BOUNCE = "soft%sbounce".freeze
    MANDRILL_EVENTS = "mandrill_events".freeze
    UNDERSCORE = "_".freeze
    LAST_STEP = "last step".freeze
    TRANSFER = "transfer".freeze
    SUBJECT = "subject".freeze
    REASON = "reason".freeze
    REJECT = "reject".freeze
    HYPHEN = "-".freeze
    SENDER = "sender".freeze
    EVENT = "event".freeze
    SHARE = "share".freeze
    EMPTY = "".freeze
    EMAIL = "email".freeze
    SEND = "send".freeze
    MSG = "msg".freeze

    # parse the webhook call from Mandrill
    
    def parse_webhook(params)
        # types of events ["hard_bounce", "unsub", "soft_bounce", "reject", "spam", "open", "click"]
        events = JSON.parse(params["mandrill_events"])
        
        # iterate through each event
        events.each do |event|
            case event[EVENT]
            when HARD_BOUNCE % UNDERSCORE, SOFT_BOUNCE % UNDERSCORE, REJECT
                handle_webhook_bounce(event[MSG])
            when SEND
                handle_webhook_send(event[MSG])
            when "click"
                handle_webhook_click(event[MSG])
            when "open"
                handle_webhook_open(event[MSG])
            when "unsub", "spam" 
                handle_unsub(event[MSG])
            end
        end
    end
    
    # handle unsubscriptions
    def handle_unsub(event)
        # get the email and find the user
        email = event[EMAIL]
        user = User.find_by_email(email)
        
        # if user exists then process
        if user.present?
            # if the user unsubscribes from a transfer/share
            handle_webhook_bounce(event)
            
            # unsubscribe user from intercom
            Users::UserAnalytics.delay(:queue => "analytics").unsubscribe(user.id)
        end
    end
        
    def handle_webhook_bounce(event)
        # make sure email metadata is present
        return if event["metadata"].nil?
        return if event["metadata"]["email_type"].nil?
        
        # get the email type
        email_type = event["metadata"]["email_type"]
        
        if email_type.include?("transfer")
            return if event["metadata"]["transfer_id"].nil?
            return if Binder::Transfer.joins(:receiver).where(:id => event["metadata"]["transfer_id"], :users => {:email => event["email"]}).count < 1
            
            TransferService.new(event["metadata"]["transfer_id"]).handle_webhook("delivery_failed")
            EventService.perform_async({:event_name => event[EVENT], :event_type => "email"})
            delete_user(event)
        elsif email_type.include?("share")
            return if event["metadata"]["share_id"].nil?
            return if Binder::Share.joins(:receiver).where(:id => event["metadata"]["share_id"], :users => {:email => event["email"]}).count < 1
            ShareService.new(event["metadata"]["share_id"]).handle_webhook("delivery_failed")
            EventService.perform_async({:event_name => event[EVENT], :event_type => "email"})
            delete_user(event)
        elsif email_type.include?("maintenance_reminder")
            return if event["metadata"]["maintenance_event_id"].nil?
            return if Binder::MaintenanceItem::MaintenanceEvent.where(:id => event["metadata"]["maintenance_event_id"]).count < 1
            MaintenanceEventService.new(event["metadata"]["maintenance_event_id"]).handle_webhook("delivery_failed")
        end
    end
    
    def handle_webhook_send(event)
        # make sure email metadata is present
        return if event["metadata"].nil?
        return if event["metadata"]["email_type"].nil?
        
        # get the email type
        email_type = event["metadata"]["email_type"]
        
        if email_type.include?("transfer")
            return if event["metadata"]["transfer_id"].nil?
            return if Binder::Transfer.where(:id => event["metadata"]["transfer_id"]).count < 1
            TransferService.new(event["metadata"]["transfer_id"]).handle_webhook("sent")
        elsif email_type.include?("share")
            return if event["metadata"]["share_id"].nil?
            return if Binder::Share.where(:id => event["metadata"]["share_id"]).count < 1
            ShareService.new(event["metadata"]["share_id"]).handle_webhook("shared")
        elsif email_type.include?("maintenance_reminder")
            return if event["metadata"]["maintenance_event_id"].nil?
            return if Binder::MaintenanceItem::MaintenanceEvent.where(:id => event["metadata"]["maintenance_event_id"]).count < 1
            MaintenanceEventService.new(event["metadata"]["maintenance_event_id"]).handle_webhook("sent")
        end
    end
    
    def handle_webhook_click(event)
        # make sure email metadata is present
        return if event["metadata"].nil?
        return if event["metadata"]["email_type"].nil?
        
        # get the email type
        email_type = event["metadata"]["email_type"]
        
        if email_type.include?("maintenance_reminder")
            return if event["metadata"]["maintenance_event_id"].nil?
            return if Binder::MaintenanceItem::MaintenanceEvent.where(:id => event["metadata"]["maintenance_event_id"]).count < 1
            MaintenanceEventService.new(event["metadata"]["maintenance_event_id"]).handle_webhook_click
        end
    end
    
    def handle_webhook_open(event)
        # make sure email metadata is present
        return if event["metadata"].nil?
        return if event["metadata"]["email_type"].nil?
        
        # get the email type
        email_type = event["metadata"]["email_type"]
        
        if email_type.include?("maintenance_reminder")
            return if event["metadata"]["maintenance_event_id"].nil?
            return if Binder::MaintenanceItem::MaintenanceEvent.where(:id => event["metadata"]["maintenance_event_id"]).count < 1
            MaintenanceEventService.new(event["metadata"]["maintenance_event_id"]).handle_webhook_open
        end
    end
    
    def delete_user(event)
        # get the email and find the user
        email = event[EMAIL]
        user = User.find_by_email(email)
        
        # if user exists then process
        if user.present? and user.sign_in_count < 1
            Users::UserAnalytics.delay(:queue => "analytics").destroyed(user.id)
        end
    end
    
    def password
        raise BadRequestException.new "Mandrill API Key Required" unless ENV[MANDRILL_PASSWORD].present?
        ENV[MANDRILL_PASSWORD]
    end
end