require 'stripe'

class SubscriptionService

    FREE = "free".freeze
    PAID = "paid".freeze
    CANCEL = "cancel".freeze
    UPDATE = "update".freeze
    UPGRADE = "upgrade".freeze
    FAILED = "failed".freeze
    FOREVER = "forever".freeze
    STANDARD = "standard".freeze
    DESCRIPTION = "Binder - %s - %s".freeze
    CARD_REQUIRED = "A credit card is required".freeze
    THIRTY_SIX_PER_YEAR = "homeownerplan299".freeze

    def self.verify(binder)
        subscription = binder.subscription
        if subscription.nil?
            subscription = create_subscription(binder, nil)
        end
        raise SubscriptionError unless subscription.plan_id == FREE || subscription.payment_status == PAID
    end

    def self.get_plan_id(name)
        case name
        when STANDARD
            return THIRTY_SIX_PER_YEAR
        when FREE
            return FREE
        end
    end

    def self.get_plan_name(id)
        case id
        when THIRTY_SIX_PER_YEAR
            return STANDARD
        when FREE
            return FREE
        end
    end

    def self.update_subscription(action, subscription, card, coupon)
        case action
        when UPGRADE
            upgrade(subscription, card, coupon)
        when UPDATE
            update_card(subscription, card)
        when CANCEL
            cancel(subscription)
        end
    end

    def self.upgrade(subscription, card, coupon)
        return if subscription.nil?
        
        id = get_plan_id(STANDARD)
        
        # upgrade the plan
        subscription.plan_id = id

        # check if a credit card is required for the request
        card_required = is_card_required?(coupon, id)

        # make sure we have a card if we need it
        if card_required and (card.nil? or card.empty?)
            raise CardRequiredException, CARD_REQUIRED
        end

        # if credit card is required create a token
        if card_required
            card_token = card
        end

        # create the customer in stripe
        begin
            retries ||= 0
            email = subscription.binder.owner.present? ? subscription.binder.owner.email : ""
            cust = Stripe::Customer.create(:description => DESCRIPTION % [subscription.binder_id, email])
        rescue => e
            retry if (retries += 1) < 3
            raise BadRequestException.new(e.message)
        end

        cust.plan = subscription.plan_id
        cust.source = card_token unless card_token.nil?
        cust.coupon = coupon unless coupon.nil? or coupon.empty?
        cust.save

        # store the stripe customer id and mark the subscription as paid
        subscription.customer_id = cust.id
        subscription.payment_status = PAID
        subscription.code = coupon

        if not subscription.save
            raise UnprocessableException.new(subscription)
        else
            # send email
            SubscriptionMailer.notify_homeowner_subscription(subscription.id).deliver_later
            
            # update all documents
            Binder::Document.where(:binder_id => subscription.binder_id).update_all(:accessible => true)
        end
        return subscription
    end

    def self.update_card(subscription, card)
        if card.nil? or card.empty?
            raise CardRequiredException, CARD_REQUIRED
        end

        # get the token for the card
        card_token = create_card_token(card)

        raise BadRequestException, "Customer ID Required" if subscription.customer_id.nil?
        
        # get the customer
        customer = Stripe::Customer.retrieve(subscription.customer_id)
        # delete the current default card
        if not customer.default_card.nil?
            delete_card = customer.cards.retrieve(customer.default_card)
            delete_card.delete
        end
        # create the new card for the customer
        customer.cards.create(:card => card_token.id)
        customer.save
        
        # if the user has a failed payment we will try this new card
        if subscription.payment_status == FAILED
            Stripe::Invoice.all(:customer => subscription.customer_id).each do |invoice|
                if invoice.paid == false
                    invoice.pay
                    break
                end
            end
        end
    end

    def self.cancel(subscription)
        # return if the subscription is nil
        return if subscription.nil?
        # if there is no customer id nothing to cancel
        return unless not subscription.customer_id.nil?
        # get the customer from stripe and delete it
        cust = Stripe::Customer.retrieve(subscription.customer_id)
        if not cust.nil?
            cust.cancel_subscription
            cust.delete
        end
        # update our subscription
        subscription.plan_id = get_plan_id(FREE)
        subscription.customer_id = nil
        subscription.payment_status = nil

        if not subscription.save
            raise UnprocessableException.new(subscription)
        end
        return subscription
    end

    def self.is_free_for_life?(subscription)
        return false if subscription.nil? or subscription.customer_id.nil? or subscription.plan_id == get_plan_id(FREE)
        # get the customer
        customer = Stripe::Customer.retrieve(subscription.customer_id)

        return false if customer.subscriptions.nil? or customer.subscriptions.total_count == 0
        # get the subscription for the binder
        sub = customer.subscriptions.data[0]
        # get the plan for the binder
        pln = sub.plan
        
        # if there is no discount not free for life
        disc = customer.discount
        return false if disc.nil?
        return false if pln.nil?
        
        # get the coupon
        cpn = disc.coupon

        # Check if the amount off is the same as the plan amount
        if cpn.amount_off == pln.amount and cpn.duration == FOREVER
            return true
        end

        # check if the percent off is 100
        if cpn.percent_off == 100 and cpn.duration == FOREVER
            return true
        end

        return false
    end

    def self.copy_subscription(source, dest)
        # get the source customer, plan and coupon
        source_customer = Stripe::Customer.retrieve(source.customer_id)
        sub = source_customer.subscriptions["data"][0]
        cpn = sub.discount.present? ? sub.discount.coupon : nil
        
        email = dest.binder.owner.present? ? dest.binder.owner.email : ""

        # create the dest customer in stripe
        cust = Stripe::Customer.create(:description => DESCRIPTION % [dest.binder_id, email])
        cust.plan = source.plan_id
        cust.coupon = cpn.present? ? cpn.id : nil

        begin
            cust.save
        rescue => e
            ErrorService.perform_async("Subscription Service Error: #{e.message}", {:binder_id => dest.binder_id})
            return
        end

        dest.customer_id = cust.id
        dest.payment_status = PAID

        
        if not dest.save
            ErrorService.perform_async("Subscription Service Error: #{dest.errors.full_messages.first}", {:binder_id => dest.binder_id})
            return
        end
    end

    def self.create_subscription(binder_id, partner_id)
        binder = Binder.find(binder_id)
        partner = Partner.find_by_id(partner_id)
        
        # set the subscription level for the binder to free
        subscription = Subscription.new(binder_id: binder.id, plan_id: Partner::Automation::Service::FREE)

        if not subscription.save
            ErrorService.perform_async(subscription.errors.full_messages.first, {:task => "create_subscription"})
            return
        else
            # if the creator is a partner upgrade
            if partner.present? and partner.coupons.count > 0
                SubscriptionService.upgrade(subscription, nil, partner.coupons[0].code)
            end
            return subscription
        end
    end

    def self.create_card_token(card)
        card_token = Stripe::Token.create(
            :card => {
                :number => card[:number],
                :exp_month => card[:exp_month],
                :exp_year => card[:exp_year],
                :cvc => card[:cvc]
            }
        )

        return card_token
    end

    def self.is_card_required?(coupon, plan)
        # if there is no coupon we need a card
        return true if coupon.nil? or coupon.empty?

        pln = Stripe::Plan.retrieve(plan)
        cpn = Stripe::Coupon.retrieve(coupon)

        # Check if the amount off is the same as the plan amount
        if cpn.amount_off == pln.amount
            return false
        end
        
        if cpn.duration != "forever"
            return true
        end

        # check if the percent off is 100
        if cpn.percent_off == 100
            return false
        end

        return true
    end

end
