module ServiceKeys
  class ServiceKeysMapper
    HOMEBINDER = "HomeBinder.com"
    STRIPE = "STRIPE_PUBLISHABLE_KEY"
    
    def self.get_keys(api_key)
      return nil if api_key.nil?
      api_key_object = ApiKey.find_by_key(api_key)
      return nil if api_key_object.nil?
      return nil if api_key_object.company_name.downcase != HOMEBINDER.downcase
      return {
        stripe: ENV[STRIPE].to_s
      }
    end
  end
end