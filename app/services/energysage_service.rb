require 'httparty'
require 'jwt'

class EnergysageService
    
    def request_services(binder_id)
        # Get binder
        binder = Binder.find_by_id(binder_id)
        return if binder.nil?
        
        # Return from sending a request if property is not covered by EnergySage
        return if !valid_location(binder.property.country, binder.property.state)

        # Attempt to populate zip, latitude, and longitude, and return if still not present
        if !valid_address(binder)
            binder.property.set_valid_address
            if !valid_address(binder)
                return
            end
        end
        
        # Set up the data
        data = {
            'ph' => binder.owner.user_profile.mobile_phone,                         # Phone
            'fa' => "#{binder.property.full_address}, #{binder.property.country}",  # Address
            'ci' => binder.property.city,                                           # City
            'st' => binder.property.state,                                          # State
            'pc' => binder.property.zip,                                            # Zip code
            'lat' => binder.property.lat.to_s,                                      # Latitude
            'lon' => binder.property.long.to_s,                                     # Longitude
            'fn' => binder.owner.user_profile.first_name.gsub("&", "and"),          # First name
            'ln' => binder.owner.user_profile.last_name.gsub("&", "and"),           # Last name
            'em' => binder.owner.email,                                             # Email
            'hor' => 'o',                                                           # Own or Rent?
            'hsz' => binder.property.sq_ft.to_s,                                    # House size in square feet
            'tou' => 'yes'                                                          # Accepted terms of use?
        }
        
        # Set home type
        set_home_type(binder, data)
        
        # Convert data to JWT object and get full URL
        token = JWT.encode data, secret_key, 'HS256'
        url = base_url + token

        # Make the handoff call
        response = HTTParty.get(URI.escape(url))

        if response.code === 200
            return url
        else
            raise BadRequestException.new response.body
        end
    end
    
    def base_url
        if Host.path === Host::PRODUCTION_HOST
            raise BadRequestException.new "EnergySage Production base URL Required" unless ENV["ENERGYSAGE_PRODUCTION_BASE_URL"].present?
            ENV["ENERGYSAGE_PRODUCTION_BASE_URL"].to_s
        else
            raise BadRequestException.new "EnergySage Test base URL Required" unless ENV["ENERGYSAGE_TEST_BASE_URL"].present?
            ENV["ENERGYSAGE_TEST_BASE_URL"].to_s
        end
    end
    
    def secret_key
        if Host.path === Host::PRODUCTION_HOST
            raise BadRequestException.new "EnergySage Production Secret Required" unless ENV["ENERGYSAGE_PRODUCTION_SECRET"].present?
            ENV["ENERGYSAGE_PRODUCTION_SECRET"].to_s
        else
            raise BadRequestException.new "EnergySage Test Secret Required" unless ENV["ENERGYSAGE_TEST_SECRET"].present?
            ENV["ENERGYSAGE_TEST_SECRET"].to_s
        end
    end
    
    # Returns true if country and state are covered by EnergySage
    def valid_location(country, state)
        if country != "US"
            return false
        end
        
        valid_states = ["AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "IL", "IN", 
                        "IA", "LA", "MA", "MD", "ME", "MI", "MN", "MO", "NC", "NH", 
                        "NJ", "NM", "NV", "NY", "OH", "OR", "PA", "RI", "SC", "TX", 
                        "UT", "VA", "VT", "WA", "WI"]
                        
        return valid_states.include?(state)
    end
    
    # Returns true if address latitude, longitude, and zip exist
    def valid_address(binder)
        return binder.property.zip.present? && binder.property.lat.present? && binder.property.long.present?
    end
    
    # Sets data hometype to single family or multi family when applicable
    def set_home_type(binder, data)
        if binder.property.property_type === 'Single Family'
            data[:ht] = 'sf'
        elsif binder.property.property_type === 'Multi Family'
            data[:ht] = 'mf'
        end
    end
end