require "httparty"

class WorkaroundService
    def post_call(job_id, link)
        url = create_call_url
        data = {
            "token": api_key,
            "extractref": "#{Rails.env}_#{job_id}",
            "extracturl": link
        }

        json = JSON.generate(data)

        # headers
        headers = {"Content-Length" => "#{json.length}", "Content-Type" => "application/json"}
        
        response = ::HTTParty.post(URI.escape(url), :body => json, :headers => headers)
        begin
            parse_response(response)
        rescue => e
            raise BadRequestException.new "WorkaroundService Error - #{e.message}"
        end
    end

    def get_call(job_id)
        url = get_call_url
        
        data = {
            "token": api_key,
            "extractref": "#{Rails.env}_#{job_id}"
        }
        
        json = JSON.generate(data)

        # headers
        headers = {"Content-Length" => "#{json.length}", "Content-Type" => "application/json"}

        response = ::HTTParty.post(URI.escape(url), :body => json, :headers => headers)

        begin
            parse_response(response)
        rescue => e
            raise BadRequestException.new "WorkaroundService Error - #{e.message}"
        end
    end
    
    def parse_response(response)
        if response.code != 201 && response.code != 200
            raise BadRequestException.new JSON.parse(response.body)
        elsif response.code === 200
            return JSON.parse(response.body)
        end
    end
    
    def create_call_url
        if Host.path === Host::PRODUCTION_HOST
            raise BadRequestException.new "WorkaroundService Production Create Call URL Required" unless ENV["WORKAROUND_PRODUCTION_CREATE_CALL_URL"].present?
            ENV["WORKAROUND_PRODUCTION_CREATE_CALL_URL"].to_s
        else
            raise BadRequestException.new "WorkaroundService Test Create Call URL Required" unless ENV["WORKAROUND_TEST_CREATE_CALL_URL"].present?
            ENV["WORKAROUND_TEST_CREATE_CALL_URL"].to_s
        end
    end
    
    def get_call_url
        if Host.path === Host::PRODUCTION_HOST
            raise BadRequestException.new "WorkaroundService Production Get Call URL Required" unless ENV["WORKAROUND_PRODUCTION_GET_CALL_URL"].present?
            ENV["WORKAROUND_PRODUCTION_GET_CALL_URL"].to_s
        else
            raise BadRequestException.new "WorkaroundService Test Get Call URL Required" unless ENV["WORKAROUND_TEST_GET_CALL_URL"].present?
            ENV["WORKAROUND_TEST_GET_CALL_URL"].to_s
        end
    end
    
    def api_key
        if Host.path === Host::PRODUCTION_HOST
            raise BadRequestException.new "WorkaroundService Production API Key Required" unless ENV["WORKAROUND_PRODUCTION_KEY"].present?
            ENV["WORKAROUND_PRODUCTION_KEY"].to_s
        else
            raise BadRequestException.new "WorkaroundService Test API Key Required" unless ENV["WORKAROUND_TEST_KEY"].present?
            ENV["WORKAROUND_TEST_KEY"].to_s
        end
    end
end
