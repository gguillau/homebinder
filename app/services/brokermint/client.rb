# https://brokermint.com/documentation/developer-api/
class Brokermint::Client

    # [
    #     {
    #         id: 12345,
    #         address: '11332 Demo St',
    #         city: 'San Diego',
    #         state: 'CA',
    #         zip: '92123',
    #         status: 'listing',
    #     },
    # ]
    
    def get_transactions(api_key)
        return [] if api_key.nil?
        url = "https://my.brokermint.com/api/v1/transactions?statuses=listing"
        return Brokermint::Request.new.call(url, api_key)
    end

    # {
    #     id: 12345,
    #     external_id: 'RT123-45',
    #     address: '11332 Demo St',
    #     city: 'San Diego',
    #     state: 'CA',
    #     zip: '92123',
    #     status: 'listed',
    #     transaction_type: 'traditional sale',
    #     price: 415000,
    #     representing: 'buyer',
    #     acceptance_date: 1428278400000,
    #     expiration_date: '',
    #     custom_attributes: [
    #         {
    #             name: 'description',
    #             label: 'Description',
    #             type: 'text',
    #             required: false,
    #             required_if_status: ''
    #         },
    #         {
    #             name: 'property_type',
    #             label: 'Property type',
    #             type: 'dropdown',
    #             required: false,
    #             required_if_status: '',
    #             options: [
    #                 'Not specified',
    #                 'Single family',
    #                 'Duplex',
    #                 'Multi unit',
    #                 'Condo',
    #                 'Townhouse',
    #                 'Mobile',
    #                 'Manufactured',
    #                 'Apartment',
    #                 'Land',
    #                 'Farm',
    #                 'Commercial',
    #                 'Other'
    #             ]
    #         },
    #         {
    #             name: 'file_date',
    #             label: 'File date',
    #             type: 'date',
    #             required: false,
    #             required_if_status: 'closed'
    #         }
    #     ],
    #     total_gross_commission: 6,
    #     created_at: 1428278400000,
    #     updated_at: 1429148200000,
    #     closed_at: 1440575080000,
    #     closing_date: 1443571200000,
    #     listing_date: '',
    #     commissions_finalized_at: 1443571200000,
    #     listing_side_representer: {
    #         id: 341,
    #         type: 'Account'
    #     },
    #     buying_side_representer: {
    #         id: 12,
    #         type: 'Contact'
    #     },
    #     timezone: -7
    # }

    def get_transaction(api_key, transaction_id)
        return [] if api_key.nil? || transaction_id.nil?
        url = "https://my.brokermint.com/api/v1/transactions/#{transaction_id}"
        return Brokermint::Request.new.call(url, api_key)
    end

    # [
    #     {
    #         id: 342,
    #         first_name: 'Alex',
    #         last_name: 'Glen',
    #         email: 'alex@glen.com',
    #         contact_type: 'lead'
    #     }
    # ]

    def get_contacts(api_key)
        return [] if api_key.nil?
        url = "https://my.brokermint.com/api/v1/contacts"
        return Brokermint::Request.new.call(url, api_key)
    end

    # {
    #     id: 870,
    #     contact_type: 'client',
    #     first_name: 'Andrew',
    #     last_name: 'McGreen',
    #     company: '',
    #     address: '',
    #     city: '',
    #     state: '',
    #     zip: '',
    #     email: '',
    #     phone: '',
    #     mobile_phone: '',
    #     fax: '',
    #     comments: [],
    #     created_at: 1429147920000,
    #     updated_at: 1429147920000,
    #     lead_source: '',
    #     custom_attributes: [],
    #     created_by: 226,
    #     private: true,
    #     external_id:'F4501'
    # }

    def get_contact(api_key, contact_id)
        return {} if api_key.nil? || contact_id.nil?
        url = "https://my.brokermint.com/api/v1/contacts/#{contact_id}"
        return Brokermint::Request.new.call(url, api_key)
    end

    # [
    #     {
    #         id: 342,
    #         type: 'User',
    #         role: 'Agent',
    #         owner: true
    #     },
    #     {
    #         id: 342,
    #         type: 'Contact',
    #         role: 'Buyer',
    #         owner: false
    #     }
    # ]

    def get_transaction_participants(api_key, transaction_id)
        return [] if api_key.nil? || transaction_id.nil?
        url = "https://my.brokermint.com/api/v1/transactions/#{transaction_id}/participants"
        return Brokermint::Request.new.call(url, api_key)
    end
end
