require "httparty"

class Brokermint::Request
  
    def call(url, api_key)
        body = {
            api_key: api_key
        }
        
        json = JSON.generate(body)
        
        # headers
        headers = {"Content-Length" => "#{json.length}", "Content-Type" => "application/json"}
        
        response = ::HTTParty.get(URI.escape(url), :body => json, :headers => headers)
        begin
            parse_response(response)
        rescue => e
            raise BadRequestException.new "Brokermint::Request Error - #{e.message} - API Key #{api_key}"
        end
    end
    
    def parse_response(response)
        if response.code === 201 || response.code === 200
            return JSON.parse(response.body)
        else
            raise BadRequestException.new response.body
        end
    end
  
end