require "soapforce"

# https://github.com/TinderBox/soapforce

class Salesforce::Base
    
    SALESFORCE_PRODUCTION_USERNAME = "SALESFORCE_PRODUCTION_USERNAME".freeze
    SALESFORCE_PRODUCTION_PASSWORD = "SALESFORCE_PRODUCTION_PASSWORD".freeze
    SALESFORCE_TESTING_USERNAME = "SALESFORCE_TESTING_USERNAME".freeze
    SALESFORCE_TESTING_PASSWORD = "SALESFORCE_TESTING_PASSWORD".freeze
    TESTING_HOST = "test.salesforce.com".freeze
    
    attr_accessor :client
    
    def initialize
        begin
            # check if we are on production and log in to salesforce using production credentials
            if Host.path === Host::PRODUCTION_HOST
                self.client = Soapforce::Client.new
                self.client.authenticate(:username => production_username, :password => production_password)
            # use testing credentials
            else
                self.client = Soapforce::Client.new(:host => TESTING_HOST)
                self.client.authenticate(:username => testing_username, :password => testing_password)
            end
        rescue => e
            # rescue from any errors and notify new relic
            ErrorService.perform_async(e.message, {:task => "salesforce_initialize"})
            # raise an exception so we don't go on to create the account/contact/opportunity
            raise BadRequestException.new e.message
        end
    end
    
    # format the HomeBinder dates - Salesforce needs dates in the following format: 2017-01-25
    
    def format_date(date)
        if date.present?
            return date.strftime("%F")
        end
        return date
    end
    
    # check for nil fields and titlize any non-date fields
    
    def format_value(field)
        # check if nil
        if field.nil?
            # if nil then we return the current value
            return field
        else
            # check if salesforce field is a date
            if not field.is_a?(Date)
                # return the value in lower case characters
                return field.titleize
            else
                # if value is a date field we just return the value formatted as YYYY-MM-DD
                return field.strftime("%F")
            end
        end
    end
    
    private
    
    def production_username
        raise BadRequestException.new "Salesforce Username Required" unless ENV[SALESFORCE_PRODUCTION_USERNAME].present?
        ENV[SALESFORCE_PRODUCTION_USERNAME]
    end
    
    def production_password
        raise BadRequestException.new "Salesforce Password Required" unless ENV[SALESFORCE_PRODUCTION_PASSWORD].present?
        ENV[SALESFORCE_PRODUCTION_PASSWORD]
    end
    
    def testing_username
        raise BadRequestException.new "Salesforce Username Required" unless ENV[SALESFORCE_TESTING_USERNAME].present?
        ENV[SALESFORCE_TESTING_USERNAME]
    end
    
    def testing_password
        raise BadRequestException.new "Salesforce Password Required" unless ENV[SALESFORCE_TESTING_PASSWORD].present?
        ENV[SALESFORCE_TESTING_PASSWORD]
    end
    
end