class Salesforce::Homeowner < Salesforce::Base
    
    ACCOUNT_ERROR = "%s - Unable to create account".freeze
    ACCOUNT_UPDATE_ERROR = "Unable to update account information".freeze

    
    # creates a new homeowner object within Salesforce. only creates a homeowner
    
    def new_homeowner(user)
        return if user.nil?
        return if not user.is_homeowner?
        
        # create the homeowner
        result = create_homeowner(user)
        
        # check if the salesforce_homeowner id was returned
        # result will include the lead ID and the result of the action: true or false
        
        if result.nil?
            # logout
            return #self.client.logout
        elsif result === false
            # notify new relic if the save was unsuccessful
            ErrorService.perform_async(ACCOUNT_ERROR, {:user_id => user.id})
        elsif not result[:success] === true
            # notify new relic if the save was unsuccessful
            ErrorService.perform_async(ACCOUNT_ERROR, {:user_id => user.id})
        elsif result[:success] === true
            user.salesforce_id = result[:id]
            if not user.save
                ErrorService.perform_async(user.errors.full_messages.first, {:user_id => user.id})
            end
        end
        # logout
        #self.client.logout
    end
    
    # create homeowner within Salesforce
    
    def create_homeowner(user)
        return if user.nil?
        return if not user.is_homeowner?

        # call the Salesforce API to create a homeowner <- use the hash bang to get an accurate error message
        begin
            result = self.client.create!(
                "Homeowner__c", 
                User_ID__c: user.id,
                Email__c: user.email,
                Sign_In_Count__c: user.sign_in_count,
                Create_Method__c: user.create_method,
                Accepted_Transfer_Date__c: format_date(user.accepted_transfer_at),
                Role__c: user.role,
                DOB__c: format_date(user.user_profile.dob),
                First_Name__c: user.user_profile.first_name,
                Last_Name__c: user.user_profile.last_name,
                Gender__c: user.user_profile.sex,
                HB_Account_Created_Date__c: format_date(user.created_at),
                Monthly_Email__c: user.user_profile.monthly_email,
                Phone__c: user.user_profile.mobile_phone,
                Do_Not_Contact__c: user.allow_calls
            )
        rescue Savon::Error
            return nil
        end

        return result
    end
    
    # updates the homeowner object within Salesforce
    
    def update_homeowner(user)
        return if user.nil?
        return if not user.is_homeowner?
        
        # find homeowner by user_id
        homeowners = self.client.find_where("Homeowner__c", User_ID__c: user.id)
        # get the the first homeowner in the array
        homeowner = homeowners.first
        
        # return if the homeowner was not found
        return unless homeowner.present?
        
        # update the homeowner account object in Salesforce
        update_salesforce_account(user, homeowner)
    end
    
    # updates the homeowner account object within Salesforce
    
    def update_salesforce_account(user, homeowner)
        return if user.nil?
        return if not user.is_homeowner?
        
        # update homeowner
        result = client.update!("Homeowner__c", 
            ID: homeowner[:id],
            User_ID__c: user.id,
            Email__c: user.email,
            Sign_In_Count__c: user.sign_in_count,
            Create_Method__c: user.create_method,
            Accepted_Transfer_Date__c: format_date(user.accepted_transfer_at),
            Role__c: user.role,
            DOB__c: user.user_profile.dob,
            First_Name__c: user.user_profile.first_name,
            Last_Name__c: user.user_profile.last_name,
            Gender__c: user.user_profile.sex,
            HB_Account_Created_Date__c: format_date(user.created_at),
            Monthly_Email__c: user.user_profile.monthly_email,
            Phone__c: user.user_profile.mobile_phone,
            Do_Not_Contact__c: user.allow_calls
        )

        # review the result
        review_result(result, user, ACCOUNT_UPDATE_ERROR)
    end
    
    # review the results of a soapforce action and notify new relic if an error occurred
    
    def review_result(result, user, error)
        # result will include the object ID and the result of the action: true or false
        if result === false
            # notify new relic if the save was unsuccessful
            ErrorService.perform_async(error, {:user_id => user.id})
        elsif not result[:success] === true
            # notify new relic if the save was unsuccessful
            ErrorService.perform_async(error, {:user_id => user.id})
        end
        
        #self.client.logout
    end
    
    # deletes the user in salesforce
    
    def delete_salesforce_object(id)
        return if id.nil?
        # find homeowner by user_id
        homeowners = self.client.find_where("Homeowner__c", User_ID__c: id)
        # get the the first homeowner in the array
        homeowner = homeowners.first
        
        # return if the homeowner was not found
        return unless homeowner.present?
        
        client.destroy!(homeowner[:id])
        
        #self.client.logout
    end

end