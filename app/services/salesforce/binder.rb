class Salesforce::Binder < Salesforce::Base
    
    ACCOUNT = "Account".freeze
    ACCOUNT_ERROR = "%s - Unable to create account".freeze
    ACCOUNT_UPDATE_ERROR = "Unable to update account information".freeze

    
    # creates a new binder object within Salesforce. only creates a binder
    
    def new_binder(binder_id)
        binder = Binder.find_by_id(binder_id)
        return if binder.nil?
        return if binder.owner.nil?
        
        # create the binder
        result = create_binder(binder.id)

        # check if the salesforce_binder id was returned
        # result will include the lead ID and the result of the action: true or false
        if result.nil?
            # logout
            #self.client.logout
            return
        elsif result === false
            # notify new relic if the save was unsuccessful
            ErrorService.perform_async(ACCOUNT_ERROR, {:binder_id => binder.id})
        elsif not result[:success] === true
            # notify new relic if the save was unsuccessful
            ErrorService.perform_async(ACCOUNT_ERROR, {:binder_id => binder.id})
        end
        # logout
        #self.client.logout
    end
    
    # create binder within Salesforce
    
    def create_binder(binder_id)
        binder = Binder.find_by_id(binder_id)
        
        return if binder.nil?
        return if binder.owner.nil?
        
        # find owner by user_id
        homeowners = self.client.find_where("Homeowner__c", User_ID__c: binder.owner.id)
        # get the the first homeowner in the array
        homeowner = homeowners.first
        
        # check if the homeowner was not found
        owner = homeowner.present? ? homeowner[:id] : nil
        
        begin
            # call the Salesforce API to create a binder <- use the hash bang to get an accurate error message
            result = self.client.create!(
                "Binder__c", 
                Name: binder.name,
                Binder_ID__c: binder.id,
                Primary__c: binder.primary,
                Active__c: binder.active,
                Create_Method__c: binder.create_method,
                Created_Date__c: Salesforce::Binder.new.format_date(binder.created_at),
                Updated_Date__c: Salesforce::Binder.new.format_date(binder.updated_at),
                Details__c: Salesforce::Binder.new.format_value(binder.details),
                Address__c: binder.property.address1,
                Address_Line_2__c: binder.property.address1,
                City__c: binder.property.city,
                State__c: binder.property.state,
                Zip_Code__c: binder.property.zip,
                Country__c: binder.property.country,
                Homeowner_Name__c: owner
            )
        rescue Savon::Error
            return nil
        end

        return result
    end
    
    # updates the binder object within Salesforce
    
    def update_binder(homebinder_id)
        homebinder = Binder.find_by_id(homebinder_id)
        
        return if homebinder.nil?
        return if homebinder.owner.nil?
        
        # find binder by binder_id
        binders = self.client.find_where("Binder__c", Binder_ID__c: homebinder.id)
        # get the the first binder in the array
        binder = binders.first
        
        # return if the binder was not found
        return unless binder.present?
        
        # update the binder account object in Salesforce
        update_salesforce_account(homebinder, binder)
    end
    
    # updates the binder account object within Salesforce
    
    def update_salesforce_account(homebinder_id, binder)
        homebinder = Binder.find_by_id(homebinder_id)
        
        return if homebinder.nil?
        return if homebinder.owner.nil?
        
        # find owner by user_id
        homeowners = self.client.find_where("Homeowner__c", User_ID__c: homebinder.owner.id)
        # get the the first homeowner in the array
        homeowner = homeowners.first
        
        # return if the homeowner was not found
        owner = homeowner.present? ? homeowner[:id] : nil
        
        # update binder
        result = client.update!("Binder__c", 
            ID: binder[:id],
            Name: homebinder.name,
            Binder_ID__c: homebinder.id,
            Primary__c: homebinder.primary,
            Active__c: homebinder.active,
            Create_Method__c: homebinder.create_method,
            Created_Date__c: format_date(homebinder.created_at),
            Updated_Date__c: format_date(homebinder.updated_at),
            Details__c: format_value(homebinder.details),
            Address__c: homebinder.property.address1,
            Address_Line_2__c: homebinder.property.address1,
            City__c: homebinder.property.city,
            State__c: homebinder.property.state,
            Zip_Code__c: homebinder.property.zip,
            Country__c: homebinder.property.country,
            Homeowner_Name__c: owner
        )

        # review the result
        review_result(result, homebinder, ACCOUNT_UPDATE_ERROR)
    end
    
    # review the results of a soapforce action and notify new relic if an error occurred
    
    def review_result(result, homebinder, error)
        # result will include the object ID and the result of the action: true or false
        if result === false
            # notify new relic if the save was unsuccessful
            ErrorService.perform_async(error, {:binder_id => homebinder.id})
        elsif not result[:success] === true
            # notify new relic if the save was unsuccessful
            ErrorService.perform_async(error, {:binder_id => homebinder.id})
        end
        
        #self.client.logout
    end
    
    # deletes the binder in salesforce
    
    def delete_salesforce_object(id)
        return if id.nil?
        # find binder by binder_id
        binders = self.client.find_where("Binder__c", Binder_ID__c: id)
        # get the the first binder in the array
        binder = binders.first
        
        # return if the binder was not found
        return unless binder.present?
        
        client.destroy!(binder[:id])
        
        #self.client.logout
    end
end