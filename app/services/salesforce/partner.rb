class Salesforce::Partner < Salesforce::Base
    
    LEAD = "Lead".freeze
    ACCOUNT = "Account".freeze
    CONTACT = "Contact".freeze
    OPPORTUNITY = "Opportunity".freeze
    ACCOUNT_ERROR = "%s - Unable to create account".freeze
    OPPORTUNITY_ERROR = "%s - Unable to create opportunity".freeze
    CONTACT_ERROR = "%s - Unable to create contact".freeze
    LEAD_ERROR = "%s - Unable to create lead".freeze
    CONTACT_UPDATE_ERROR = "Unable to update contact information".freeze
    ACCOUNT_UPDATE_ERROR = "Unable to update account information".freeze

    
    # creates a new partner object within Salesforce. creates a partner, opportunity and contact
    
    def new_partner(partner, survey)
        
        if partner.contact.present?
            # get the partner first and last name
            split = partner.contact.split(Partner::Automation::EmailParser::SPACE)
        else
            split = ["", ""]
        end
        
        # create the account
        result = create_account(partner, survey, split)
        # check if the account id was returned
        # result will include the lead ID and the result of the action: true or false
        
        if result === false
            # notify new relic if the save was unsuccessful
            ErrorService.perform_async(ACCOUNT_ERROR, {:partner_id => partner.id})
        elsif not result[:success] === true
            # notify new relic if the save was unsuccessful
            ErrorService.perform_async(ACCOUNT_ERROR, {:partner_id => partner.id})
        elsif result[:id].present?
            # get the account id
            account_id = result[:id]
            # create the contact
            create_contact(partner, account_id, split)
            # create the opportunity
            create_opportunity(partner, survey, account_id, split)
        end
        # logout
        #self.client.logout
    end
    
    # create account within Salesforce
    
    def create_account(partner, survey, split)
        begin
            if partner.partner_type === "inspector"
                # call the Salesforce API to create an account <- use the hash bang to get an accurate error message
                result = self.client.create!(
                    ACCOUNT, 
                    Name: partner.contact + " - " + partner.name, 
                    Phone: partner.phone,
                    NumberOfEmployees: survey.employees, 
                    # create custom fields
                    Partner_ID__c: partner.id,
                    Affiliate_Code__c: partner.affiliate_code,
                    Category__c: survey.employees === "1" ? "Single Inspector Firm": "Multi-Inspector Firm",
                    Volume__c: survey.volume,
                    Software_Administrative__c: survey.admin_software,
                    Software_Report_Writing__c: survey.software
                )
            else
                result = self.client.create!(
                    ACCOUNT, 
                    Name: partner.contact + " - " + partner.name, 
                    Phone: partner.phone,
                    # create custom fields
                    Partner_ID__c: partner.id,
                    Affiliate_Code__c: partner.affiliate_code
                )
            end
        rescue Savon::Error
            return false
        end

        return result
    end
    
    # create contact within Salesforce
    
    def create_contact(partner, account_id, split)
        # call the Salesforce API to create a contact <- use the hash bang to get an accurate error message
        result = self.client.create!(
            CONTACT,
            AccountId: account_id,
            FirstName: split[0],
            Email: partner.email,
            Phone: partner.phone,
            LastName: split[1]
        )
        review_result(result, partner, CONTACT_ERROR)
    end
    
    # create contact within Salesforce
    
    def create_opportunity(partner, survey, account_id, split)
        # call the Salesforce API to create an opportunity <- use the hash bang to get an accurate error message
        if partner.partner_type === "inspector"
            result = self.client.create!(
                OPPORTUNITY,
                AccountId: account_id,
                Name: partner.contact,
                CloseDate: Date.today + 30.days,
                StageName: "Qualifying",
                LeadSource: survey.source,
                Amount: get_opportunity_amount(survey.volume),
                # Custom Fields
                Category__c: survey.employees === "1" ? "Single Inspector Firm": "Multi-Inspector Firm",
                Affiliate_Code__c: partner.affiliate_code,
                Volume__c: survey.volume,
                Software_Administrative__c: survey.admin_software,
                Software_Report_Writing__c: survey.software
            )
        else
            result = self.client.create!(
                OPPORTUNITY,
                AccountId: account_id,
                Name: partner.contact,
                CloseDate: Date.today + 30.days,
                StageName: "Qualifying",
                Volume__c: survey.volume,
                Amount: get_generic_opportunity_amount(partner.partner_type, survey.volume),
            )
        end

        # parse the result
        review_result(result, partner, OPPORTUNITY_ERROR)
    end
    
    # returns the correct opportunity amount based on inspector volume/year
    
    def get_opportunity_amount(volume)
        case volume
        when "50"
            192
        when "250"
            648
        when "500"
            648
        when "1000"
            1175
        when "1100"
            2350
        else
            192
        end
    end
    
    def get_generic_opportunity_amount(partner_type, volume)
        case partner_type
        when "broker"
            volume.to_i * 8
        when "lender"
            volume.to_i * 8
        when "homepro"
            49
        when "property_manager"
            volume.to_i * 8
        when "hoa"
            volume.to_i * 12
        when "insurance"
            volume.to_i * 8
        end
    end
    
    # updates the partner object within Salesforce
    
    def update_partner(partner)
        # find account and contact by partner_id and account_id respectively
        accounts = self.client.find_where(ACCOUNT, Partner_ID__c: partner.id)
        # get the the first account in the array
        account = accounts.first
        
        # return if the account was not found
        return unless account.present?
        
        # update the partner account object in Salesforce
        update_salesforce_account(partner, account)
        
        # find the contact
        contacts = self.client.find_where(CONTACT, AccountId: account[:id])
        
        # get the first contact in the array
        contact = contacts.first
        
        # check if the contact object was not found
        return unless contact.present?
        
        # update the contact object <-- not doing this anymore
        # update_salesforce_contact(partner, contact)
    end
    
    # updates the partner account object within Salesforce
    
    def update_salesforce_account(partner, account)
        # get the billing account
        billing_account = partner.account
        # check if billing account exists
        return unless billing_account.present?
        
        partner_users = Partner.get_partner_users(partner.id)
        template_ids = partner.partner_configuration.binder_templates.pluck(:id)
        contractors = Template::Contractor.where(:binder_template_id => template_ids).distinct(:name).count
        # get the address or else create a new one
        address = partner.address.present? ? partner.address : partner.build_address
        
        # update account
        result = client.update!(ACCOUNT, 
            ID: account[:id],
            # not updating the name anymore
            #Name: partner.contact + " - " + partner.name, 
            Phone: partner.phone,
            # Address Fields
            BillingStreet: address.address1,
            BillingCity: address.city,
            BillingState: address.state,
            BillingCountry: address.country,
            BillingPostalCode: address.zip,
            # custom fields in Salesforce
            Affiliate_Code__c: partner.affiliate_code,
            Account_Number__c: billing_account.account_number,
            Account_Status__c: format_value(billing_account.account_status),
            Account_Sub_Status__c: format_value(billing_account.account_sub_status),
            Account_Sub_Type__c: format_value(billing_account.account_sub_type),
            Account_Type__c: billing_account.account_type,
            Account_Billing_Frequency__c: format_value(billing_account.billing_frequency),
            Account_Trial_Expiration__c: format_date(billing_account.trial_expiration),
            Account_Billing_Activation__c: format_date(billing_account.billing_activation),
            Account_Subscription_Amount__c: billing_account.subscription_amount,
            Account_Transaction_Amount__c: billing_account.transaction_amount,
            Account_Payment_Type__c: billing_account.payment_type,
            # additional custom fields
            Binders_Last_7_Days__c: Partner.total_binders(partner_users, {:created_at => 7.days.ago..Time.now}),
            Binders_Last_30_Days__c: Partner.total_binders(partner_users, {:created_at => 30.days.ago..Time.now}),
            Total_Binders__c: Partner.total_binders(partner_users),
            Binders_Not_Transferred__c: Partner.total_binders_not_transferred(partner_users),
            Automatic_Transfer__c: partner.partner_configuration.automation_binder_action,
            Transfer_Delay__c: partner.partner_configuration.binder_action_delay_length,
            Default_User_Branding_Option__c: partner.partner_configuration.default_user_branding_option,
            Agent_Notification__c: partner.partner_configuration.send_agents_transfer_notification,
            Self_Serve_Link__c: "#{Host.path}/partners/#{partner.partner_key}/homeowners",
            Show_ISN_Tab__c: partner.partner_configuration.show_isn_tab,
            Binder_Action_Option__c: partner.partner_configuration.default_binder_action,
            Recommended_Home_Pros__c: contractors,
            Stripe_Customer_ID__c: partner.account.stripe_customer_id,
            Enable_Repair_Pricer__c: partner.partner_configuration.repair_pricer_enabled,
            Repair_Pricer_Pre_Paid_Reports__c: partner.partner_configuration.repair_pricer_pre_paid_reports,
            Report_Link__c: partner.partner_configuration.isn_report_url
        )

        # review the result
        review_result(result, partner, ACCOUNT_UPDATE_ERROR)
    end
    
    # updates the partner contact object within Salesforce
    
    def update_salesforce_contact(partner, contact)
        # get the partner first and last name
        split = partner.contact.split(Partner::Automation::EmailParser::SPACE)
        # update contact
        result = client.update!(CONTACT, 
            ID: contact[:id],
            FirstName: split[0],
            Email: partner.email,
            Phone: partner.phone,
            LastName: split[1]
        )
        
        review_result(result, partner, CONTACT_UPDATE_ERROR)
    end
    
    # review the results of a soapforce action and notify new relic if an error occurred
    
    def review_result(result, partner, error)
        # result will include the object ID and the result of the action: true or false
        if result === false
            # notify new relic if the save was unsuccessful
            ErrorService.perform_async(error, {:partner_id => partner.id})
        elsif not result[:success] === true
            # notify new relic if the save was unsuccessful
            ErrorService.perform_async(error, {:partner_id => partner.id})
        end
    end
end