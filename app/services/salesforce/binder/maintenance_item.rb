class Salesforce::Binder::MaintenanceItem < Salesforce::Base
    
    ACCOUNT_ERROR = "%s - Unable to create maintenance_item".freeze
    ACCOUNT_UPDATE_ERROR = "Unable to update maintenance_item information".freeze

    
    # creates a new maintenance_item object within Salesforce. only creates a maintenance_item
    
    def new_item(item)
        return if item.nil?
        
        # create the item
        result = create_maintenance_item(item)

        # check if the salesforce_item id was returned
        # result will include the lead ID and the result of the action: true or false
        
        if result.nil?
            # logout
            #self.client.logout
            return
        elsif result === false

            # notify new relic if the save was unsuccessful
            ErrorService.perform_async(ACCOUNT_ERROR, {:maintenance_item_id => item.id})
        elsif not result[:success] === true

            # notify new relic if the save was unsuccessful
            ErrorService.perform_async(ACCOUNT_ERROR, {:maintenance_item_id => item.id})
        end
        # logout
        #self.client.logout
    end
    
    # create maintenance_item within Salesforce
    
    def create_maintenance_item(item)
        return if item.nil?
        
        # find binder by Binder_ID__c
        binders = self.client.find_where("Binder__c", Binder_ID__c: item.binder_id)
        # get the the first binder in the array
        binder = binders.first
        
        # return if the binder was not found
        return if binder.nil?
        
        begin
            # call the Salesforce API to create a maintenance_item <- use the hash bang to get an accurate error message
            result = self.client.create!(
                "Maintenance_Item__c", 
                Name: item.name,
                Maintenance_ID__c: item.id,
                Binder__c: binder[:id],
                Additional_details__c: Salesforce::Binder::MaintenanceItem.new.format_value(item.details),
                Email_Notifications__c: item.email_notifications,
                Last_Completed_On__c: Salesforce::Binder::MaintenanceItem.new.format_date(item.get_last_completed_date),
                Due_Next_On__c: Salesforce::Binder::MaintenanceItem.new.format_date(item.do_date),
                Occurs_Every__c: item.interval,
                Time__c: item.maintenance_cycle
            )
        rescue Savon::Error => e
            ErrorService.perform_async(e.message, {:maintenance_item_id => item.id})
            return nil
        end

        return result
    end
    
    # updates the maintenance_item object within Salesforce
    
    def update_item(item)
        return if item.nil?
        
        # find maintenance_item by Maintenance_ID__c
        maintenance_items = self.client.find_where("Maintenance_Item__c", Maintenance_ID__c: item.id)
        # get the the first maintenance_item in the array
        maintenance_item = maintenance_items.first
        
        # return if the maintenance_item was not found
        return unless maintenance_item.present?
        
        # update the maintenance_item account object in Salesforce
        update_salesforce_account(item, maintenance_item)
    end
    
    # updates the maintenance_item account object within Salesforce
    
    def update_salesforce_account(item, maintenance_item)
        return if item.nil?
        
        # find binder by Binder_ID__c
        binders = self.client.find_where("Binder__c", Binder_ID__c: item.binder_id)
        # get the the first binder in the array
        binder = binders.first
        
        # return if the binder was not found
        return if binder.nil?
        
        # update maintenance_item
        result = client.update!("Maintenance_Item__c", 
            ID: maintenance_item[:id],
            Name: item.name,
            Maintenance_ID__c: item.id,
            Binder__c: binder[:id],
            Additional_details__c: format_value(item.details),
            Email_Notifications__c: item.email_notifications,
            Last_Completed_On__c: format_date(item.get_last_completed_date),
            Due_Next_On__c: format_date(item.do_date),
            Occurs_Every__c: item.interval,
            Time__c: item.maintenance_cycle
        )

        # review the result
        review_result(result, item, ACCOUNT_UPDATE_ERROR)
    end
    
    # deletes the item in salesforce
    
    def delete_salesforce_object(id)
        return if id.nil?
        # find maintenance_item by Maintenance_ID__c
        maintenance_items = self.client.find_where("Maintenance_Item__c", Maintenance_ID__c: id)
        # get the the first maintenance_item in the array
        maintenance_item = maintenance_items.first
        
        # return if the maintenance_item was not found
        return unless maintenance_item.present?
        
        client.destroy!(maintenance_item[:id])
        
        #self.client.logout
    end
    
    # review the results of a soapforce action and notify new relic if an error occurred
    
    def review_result(result, item, error)
        # result will include the object ID and the result of the action: true or false
        if result === false
            # notify new relic if the save was unsuccessful
            ErrorService.perform_async(error, {:maintenance_item_id => item.id})
        elsif not result[:success] === true
            # notify new relic if the save was unsuccessful
            ErrorService.perform_async(error, {:maintenance_item_id => item.id})
        end
        
        #self.client.logout
    end

end