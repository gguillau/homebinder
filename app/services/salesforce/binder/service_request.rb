class Salesforce::Binder::ServiceRequest < Salesforce::Base
    
    ACCOUNT_ERROR = "%s - Unable to create service_request".freeze
    
    # creates a new service_request object within Salesforce. only creates a service_request
    
    def new_service_request(service_request, homebinder, status)
        return if service_request.nil?
        return if homebinder.nil?
        return if status.nil?

        # create the service_request
        result = create_service_request(service_request, homebinder, status)
        
        # check if the salesforce_service_request id was returned
        # result will include the lead ID and the result of the action: true or false
        
        if result.nil?
            # logout
            #self.client.logout
            return
        elsif result === false
            # notify new relic if the save was unsuccessful
            ErrorService.perform_async(ACCOUNT_ERROR, {:service_request => service_request, :binder_id => homebinder.id})
        elsif not result[:success] === true
            # notify new relic if the save was unsuccessful
            ErrorService.perform_async(ACCOUNT_ERROR, {:service_request => service_request, :binder_id => homebinder.id})
        end
        # logout
        #self.client.logout
    end
    
    # create service_request within Salesforce
    
    def create_service_request(service_request, homebinder, status)
        return if service_request.nil?
        return if homebinder.nil?
        return if status.nil?
        
        # find binder by Binder_ID__c
        binders = self.client.find_where("Binder__c", Binder_ID__c: homebinder.id)
        # get the the first binder in the array
        binder = binders.first
        
        # return if the binder was not found
        return if binder.nil?
        
        begin
            # call the Salesforce API to create a service_request <- use the hash bang to get an accurate error message
            result = self.client.create!(
                "Service_Request__c", 
                Name: service_request,
                Status__c: status,
                Request_Date__c: format_date(Date.today),
                Binder__c: binder[:id]
            )
        rescue Savon::Error
            return nil
        end

        return result
    end
end