require 'date'
require 'open-uri'
require 'uri'
require 'prawn'
require 'prawn/table'
require 'nokogiri'

class SellerReportPdfService < PdfService
    include ActionView::Helpers::NumberHelper

    attr_accessor :seller_report, :full_report, :aprintcount, :mprintcount, :cprintcount, :pprintcount,
    :paprintcount, :fprintcount, :dprintcount, :ptprintcount

    def initialize(seller_report)
        super
        self.seller_report = seller_report
        self.binder = seller_report.binder
        self.text = "Seller's Report for"
        self.full_report = false
        self.aprintcount = 0
        self.mprintcount = 0
        self.pprintcount = 0
        self.cprintcount = 0
        self.paprintcount = 0
        self.fprintcount = 0
        self.dprintcount = 0
        self.ptprintcount = 0
    end

    def create
        # figure out the number of items to print for each group
        det_item_ct

        init
        add_logos
        add_title
        add_hero_photo
        add_details
        add_appliances
        add_maintenance
        add_projects
        add_contractors
        add_paints
        add_finishes
        add_permits
        add_documents
        add_footer
        pdf
    end

    # determine how many of each category to print
    def det_item_ct
        acount    = seller_report.appliances.length         # appliances
        mcount    = seller_report.maintenance_items.length  # maintenance
        pcount    = seller_report.projects.length           # projects
        ccount    = seller_report.contractors.length        # contractors
        pacount    = seller_report.paints.length            # paints
        fcount    = seller_report.finishes.length           # finishes
        dcount    = seller_report.documents.length          # documents
        ptcount    = seller_report.permits.length          # permits
        tprinted  = 12                                      # total remaining to be printed (12)
        addgrpct  = 0                                       # num of groups to add to (0)

        # appliances
        if (acount >= 3) and full_report != true
            self.aprintcount = 3
        else
            self.aprintcount = acount
        end

        tprinted = (tprinted - aprintcount)

        # maintenances
        if (mcount >= 3) and full_report != true
            self.mprintcount = 3
        else
            self.mprintcount = mcount
        end

        tprinted = (tprinted - mprintcount)

        # projects
        if (pcount >= 3) and full_report != true
            self.pprintcount = 3
        else
            self.pprintcount = pcount
        end

        tprinted = (tprinted - pprintcount)

        # contractors
        if (ccount >= 3) and full_report != true
            self.cprintcount = 3
        else
            self.cprintcount = ccount
        end

        tprinted = (tprinted - cprintcount)

        # paints
        if (pacount >= 3) and full_report != true
            self.paprintcount = 3
        else
            self.paprintcount = pacount
        end

        tprinted = (tprinted - paprintcount)

        # finishes
        if (fcount >= 3) and full_report != true
            self.fprintcount = 3
        else
            self.fprintcount = fcount
        end

        tprinted = (tprinted - fprintcount)

        # documents
        if (dcount >= 3) and full_report != true
            self.dprintcount = 3
        else
            self.dprintcount = dcount
        end
        
        tprinted = (tprinted - dprintcount)
        
        # permits
        if (ptcount >= 3) and full_report != true
            self.ptprintcount = 3
        else
            self.ptprintcount = ptcount
        end

        tprinted = (tprinted - ptprintcount)

        # now see what we've got left
        # after our first round of printing/counting
        if (tprinted > 0)

            # if any of the item counts are greater than 3
            # set them as able to be added to
            # and keep count of how many item groups
            # can take extra items
            if (acount > 3)
                acountadd = true
                addgrpct += 1
            end
            if (mcount > 3)
                mcountadd = true
                addgrpct += 1
            end
            if (pcount > 3)
                pcountadd = true
                addgrpct += 1
            end
            if (ccount > 3)
                ccountadd = true
                addgrpct += 1
            end

            # now determine how many items to add to each group
            # by dividing the number of remaining items
            # with the number of groups that can take extra items
            itemcttoadd = addgrpct == 0 ? 0 : (tprinted / addgrpct).round

            # groups that can take must already have 3,
            # that's just how this works.

            # TODO: for example if itemcttoadd is 2 and one grp only has 4
            # figure out what to do with the remainder.
            if (acountadd == true)
                self.aprintcount = 3 + itemcttoadd
            else
                self.aprintcount = acount
            end

            if (mcountadd == true)
                self.mprintcount = 3 + itemcttoadd
            else
                self.mprintcount = mcount
            end

            if (pcountadd == true)
                self.pprintcount = 3 + itemcttoadd
            else
                self.pprintcount = pcount
            end

            if (ccountadd == true)
                self.cprintcount = 3 + itemcttoadd
            else
                self.cprintcount = ccount
            end
        end
    end

    def add_hero_photo
        return if binder.hero_image.nil?
        image = binder.hero_image.file.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :large)
        # report hero photo
        pdf.repeat([1], :dynamic => true) do
            pdf.bounding_box([0, 600], :width => pdf.bounds.width/3, :height => 100) do
                pdf.image open(image, {ssl_verify_mode:OpenSSL::SSL::VERIFY_NONE}), :height => 100, :position => :left, :vposition => :top
            end
        end
        pdf.move_down(100)
    end

    def add_details
        if not binder.details.nil?
            pdf.move_down(20)
            pdf.formatted_text_box [
                                       { :text => "Home Details", :size => 12, :color => "ff6633" },
                                   ], :at => [0, pdf.cursor], :width => 400

            pdf.move_down(20)
            details = Nokogiri::HTML(binder.details)
            pdf.text details.text, :size => 9, :color => "000000", :align => :justify
        end
    end

    def add_appliances
        # appliances list
        # appliances = seller_report.appliances.order('install_date DESC').first(aprintcount)
        # order doesn't work anymore since appliances array isn't direct from DB.
        appliances = seller_report.appliances.first(aprintcount)

        # appliances = appliances.sort_by(&:"install_date").reverse
        appliances = appliances.sort_by{ |install_date| [install_date ? 0 : 1, install_date]}
        appliances.reverse

        if (appliances.length > 0)
            pdf.move_down(12)
            pdf.formatted_text_box [
                                       { :text => "Appliance", :size => 12, :color => "ff6633" },
                                   ], :at => [0, pdf.cursor], :width => 400

            pdf.move_down(10)

            appliancedata = [[ "DETAILS", "MAKE & MODEL", "YEAR INSTALLED" ]]
            appliances.each do |appliance|
                apname = "<b>#{appliance.name}</b>" unless appliance.name.nil?
                if not appliance.details.nil?
                    if full_report === true
                        apname += "\n#{appliance.details.squish}"
                    else
                        apname += "\n#{appliance.details.truncate(50, :separator => ' ').squish}"
                    end
                end
                apmanu = "<b>#{appliance.manufacturer}</b>" unless appliance.manufacturer.nil?
                if not appliance.model.nil?
                    if (apmanu)
                        apmanu += "\n#{appliance.model.upcase}"
                    else
                        apmanu = "#{appliance.model}"
                    end
                end
                apdate = appliance.install_date.strftime("%Y") unless appliance.install_date.nil?

                appliancedata += [[ apname, apmanu, apdate ]]
            end

            pdf.table(appliancedata, :header => true, :width => pdf.bounds.width, :cell_style => { :inline_format => true }) do
                cells.padding       = 6
                cells.borders       = [:bottom]
                cells.size          = 9
                cells.border_color  = "686868"

                row(0).borders      = [:bottom]
                row(0).border_width = 1
                row(0).font_style   = :bold
                row(0).size         = 8

                column(2).align     = :right
            end

            aplen = seller_report.appliances.length
            if (aplen - aprintcount) > 0
                pdf.move_down(5)
                pdf.text "+#{aplen - aprintcount} more at homebinder.com", :size => 8, :align => :right
                pdf.move_up(15)
            end
        end
    end

    def add_maintenance
        # maintenance list
        # maintenance_items = seller_report.maintenance_items.order('name').first(mprintcount)
        # order doesn't work anymore since maintanance array isn't direct from DB.
        maintenance_items = seller_report.maintenance_items.first(mprintcount)
        maintenance_items = maintenance_items.sort_by(&:"name")

        if (maintenance_items.length > 0)
            pdf.move_down(20)
            pdf.formatted_text_box [
                                       { :text => "Maintenance", :size => 12, :color => "ff6633" },
                                   ], :at => [0, pdf.cursor], :width => 400

            pdf.move_down(10)

            #@maint_items = binder.maintenance_items.sort_by {|i| i.next_event.do_date}
            maintdata = [[ "DESCRIPTION", "FREQUENCY" ]]
            maintenance_items.each do |item|
                iname = "<b>#{item.name}</b>" unless item.name.nil?
                if not item.details.nil?
                    if full_report == true
                        iname += "\n#{item.details.squish}"
                    else
                        iname += "\n#{item.details.truncate(75, :separator => ' ').squish}"
                    end
                end

                sinterval = ""
                if not item.interval.nil? and not item.maintenance_cycle.nil?
                    sinterval = "Every "
                    sinterval << item.interval.to_s unless item.interval.nil?
                    sinterval << ' ' + item.maintenance_cycle unless item.maintenance_cycle.nil?
                end

                maintdata += [[ iname, sinterval ]]
            end

            pdf.table(maintdata, :header => true, :width => pdf.bounds.width, :cell_style => { :inline_format => true }) do
                cells.padding       = 6
                cells.borders       = [:bottom]
                cells.size          = 9
                cells.border_color  = "686868"

                row(0).borders      = [:bottom]
                row(0).border_width = 1
                row(0).font_style   = :bold
                row(0).size         = 8

                column(1).align     = :right
            end

            milen = seller_report.maintenance_items.length
            if (milen - mprintcount) > 0
                pdf.move_down(5)
                pdf.text "+#{milen - mprintcount} more at homebinder.com", :size => 8, :align => :right
                pdf.move_up(15)
            end
        end
    end

    def add_projects
        # improvements list
        # projects = seller_report.projects.order('end_date DESC').first(pprintcount)
        # projects = projects.sort_by(&:"end_date").reverse
        projects = seller_report.projects.select(&:end_date).sort_by(&:end_date).reverse + seller_report.projects.reject(&:end_date)
        projects = projects.first(pprintcount)

        if (projects.length > 0)
            pdf.move_down(20)
            pdf.formatted_text_box [
                                       { :text => "Home Improvements", :size => 12, :color => "ff6633" },
                                   ], :at => [0, pdf.cursor], :width => 400

            pdf.move_down(10)

            projectdata = [[ "DESCRIPTION", "COMPLETED" ]] if !seller_report.show_project_costs
            projectdata = [["DESCRIPTION", "COSTS", "COMPLETED"]] if seller_report.show_project_costs
            projects.each do |project|
                
                pname = "<b>#{project.name}</b>" unless project.name.nil?
                pcost = number_to_currency(project.cost_cents/100, :precision => 2)
                
                if not project.details.nil?
                    if full_report == true
                        pname += "\n#{project.details.squish}"
                    else
                        pname += "\n#{project.details.truncate(75, :separator => ' ').squish}"
                    end
                end
                if project.status === "Completed"
                    pendd = project.status
                else
                    pendd = "In Progress"
                end

                projectdata += [[ pname, pendd ]] if !seller_report.show_project_costs
                projectdata += [[ pname, pcost, pendd ]] if seller_report.show_project_costs
            end

            pdf.table(projectdata, :header => true, :width => pdf.bounds.width, :cell_style => { :inline_format => true }) do
                cells.padding       = 6
                cells.borders       = [:bottom]
                cells.size          = 9
                cells.border_color  = "686868"

                row(0).borders      = [:bottom]
                row(0).border_width = 1
                row(0).font_style   = :bold
                row(0).size         = 8

                column(1).align     = :right
            end

            plen = seller_report.projects.length
            if (plen - pprintcount) > 0
                pdf.move_down(5)
                pdf.text "+#{plen - pprintcount} more at homebinder.com", :size => 8, :align => :right
                pdf.move_up(15)
            end
        end
    end

    def add_contractors
        # contractors list
        contractors = seller_report.contractors.first(cprintcount)

        if (contractors.length > 0)
            pdf.move_down(20)
            pdf.formatted_text_box [
                                       { :text => "Home Pros", :size => 12, :color => "ff6633" },
                                   ], :at => [0, pdf.cursor], :width => 400

            pdf.move_down(10)

            contrdata = [[ "NAME", "TYPE", "CONTACT" ]]
            contractors.each do |binder_contractor|
                contractor = binder_contractor.contractor
                next if contractor.nil?
                cname = "<b>#{contractor.name.titleize}</b>" unless contractor.name.nil?
                ctype = contractor.types.join(", ")
                if not contractor.phone.nil?
                    ccont = contractor.phone.phony_formatted(:normalize => :US, :format => :national, :spaces => '-')
                else
                    if not contractor.email.nil?
                        ccont = contractor.email
                    end
                end

                contrdata += [[ cname, ctype, ccont ]]
            end

            pdf.table(contrdata, :header => true, :width => pdf.bounds.width, :cell_style => { :inline_format => true }) do
                cells.padding       = 6
                cells.borders       = [:bottom]
                cells.size          = 9
                cells.border_color  = "686868"

                row(0).borders      = [:bottom]
                row(0).border_width = 1
                row(0).font_style   = :bold
                row(0).size         = 8

                column(2).align     = :right
            end

            clen = seller_report.contractors.length
            if (clen - cprintcount) > 0
                pdf.move_down(5)
                pdf.text "+#{clen - cprintcount} more at homebinder.com", :size => 8, :align => :right
                pdf.move_up(15)
            end
        end
    end

    def add_paints
        # paints list
        paints = seller_report.paints.first(paprintcount)

        if (paints.length > 0)
            pdf.move_down(25)
            pdf.formatted_text_box [
                                       { :text => "Paints", :size => 12, :color => "ff6633" },
                                   ], :at => [0, pdf.cursor], :width => 400

            pdf.move_down(15)

            paintdata = [[ "NAME", "MANUFACTURER", "CODE" ]]
            paints.each do |paint|
                pname = "<b>#{paint.name.titleize}</b>" unless paint.name.nil?
                pmanuf = paint.manufacturer.titleize unless paint.manufacturer.nil?
                pcode = paint.code.titleize unless paint.code.nil?

                paintdata += [[ pname, pmanuf, pcode ]]
            end

            pdf.table(paintdata, :header => true, :width => pdf.bounds.width, :cell_style => { :inline_format => true }) do
                cells.padding       = 6
                cells.borders       = [:bottom]
                cells.size          = 9
                cells.border_color  = "686868"

                row(0).borders      = [:bottom]
                row(0).border_width = 1
                row(0).font_style   = :bold
                row(0).size         = 8

                column(2).align     = :right
            end

            plen = seller_report.paints.length
            if (plen - paprintcount) > 0
                pdf.move_down(5)
                pdf.text "+#{plen - pprintcount} more at homebinder.com", :size => 8, :align => :right
                pdf.move_up(15)
            end
        end
    end

    def add_finishes
        # finishes list
        finishes = seller_report.finishes.first(fprintcount)

        if (finishes.length > 0)
            pdf.move_down(25)
            pdf.formatted_text_box [
                                       { :text => "Finishes", :size => 12, :color => "ff6633" },
                                   ], :at => [0, pdf.cursor], :width => 400

            pdf.move_down(15)

            findata = [[ "NAME", "MAKE", "MODEL" ]]
            finishes.each do |fin|
                fname = "<b>#{fin.name.titleize}</b>" unless fin.name.nil?
                fmake = fin.make.titleize unless fin.make.nil?
                fmodel = fin.model.titleize unless fin.model.nil?

                findata += [[ fname, fmake, fmodel ]]
            end

            pdf.table(findata, :header => true, :width => pdf.bounds.width, :cell_style => { :inline_format => true }) do
                cells.padding       = 6
                cells.borders       = [:bottom]
                cells.size          = 9
                cells.border_color  = "686868"

                row(0).borders      = [:bottom]
                row(0).border_width = 1
                row(0).font_style   = :bold
                row(0).size         = 8

                column(2).align     = :right
            end

            plen = seller_report.finishes.length
            if (plen - fprintcount) > 0
                pdf.move_down(5)
                pdf.text "+#{plen - pprintcount} more at homebinder.com", :size => 8, :align => :right
                pdf.move_up(15)
            end
        end
    end
    
    def add_permits
        # permits list
        permits = seller_report.permits.first(ptprintcount)

        if (permits.length > 0)
            pdf.move_down(25)
            pdf.formatted_text_box [
                                       { :text => "Permits", :size => 12, :color => "ff6633" },
                                   ], :at => [0, pdf.cursor], :width => 400

            pdf.move_down(15)

            permdata = [[ "NUMBER", "STATUS", "TYPE", "DATE" ]]
            permits.each do |permit|
                ptname = "<b>#{permit.permit_number}</b>"
                ptstatus = permit.status
                pttype = permit.permit_type
                ptdate = permit.permit_date ? permit.permit_date.strftime("%B %Y") : "No Permit Date Available"
                permdata += [[ ptname, ptstatus, pttype, ptdate ]]
            end

            pdf.table(permdata, :header => true, :width => pdf.bounds.width, :cell_style => { :inline_format => true }) do
                cells.padding       = 6
                cells.borders       = [:bottom]
                cells.size          = 9
                cells.border_color  = "686868"

                row(0).borders      = [:bottom]
                row(0).border_width = 1
                row(0).font_style   = :bold
                row(0).size         = 8

                column(2).align     = :right
            end

            plen = seller_report.permits.length
            if (plen - ptprintcount) > 0
                pdf.move_down(5)
                pdf.text "+#{plen - pprintcount} more at homebinder.com", :size => 8, :align => :right
                pdf.move_up(15)
            end
        end
    end

    def add_documents
        # documents list
        documents = seller_report.documents.first(dprintcount)

        if (documents.length > 0)
            pdf.move_down(25)
            pdf.formatted_text_box [
                                       { :text => "Documents", :size => 12, :color => "ff6633" },
                                   ], :at => [0, pdf.cursor], :width => 400

            pdf.move_down(15)

            docdata = [[ "NAME" ]]
            documents.each do |doc|
                unless doc.file_file_name.nil?
                    dname = "<link href='#{doc.location}'><b>#{doc.file_file_name.titleize}</b></link>"
                    docdata += [[ dname ]]
                end
            end

            pdf.table(docdata, :header => true, :width => pdf.bounds.width, :cell_style => { :inline_format => true }) do
                cells.padding       = 6
                cells.borders       = [:bottom]
                cells.size          = 9
                cells.border_color  = "686868"

                row(0).borders      = [:bottom]
                row(0).border_width = 1
                row(0).font_style   = :bold
                row(0).size         = 8

                column(2).align     = :right
            end

            plen = seller_report.documents.length
            if (plen - dprintcount) > 0
                pdf.move_down(5)
                pdf.text "+#{plen - pprintcount} more at homebinder.com", :size => 8, :align => :right
                pdf.move_up(15)
            end
        end
    end

    def add_footer
        # footer
        pdf.repeat(:all) do
            pdf.bounding_box([0,-30], :width => pdf.bounds.width, :height => 30) do
                pdf.text "Get the full report at http://www.homebinder.com/SellerReport/" << seller_report.code, :size => 10, :align => :center if full_report != true
                pdf.move_down(5) if full_report != true
                pdf.text "Information contained in this report is provided by the Seller.", :size => 8, :align => :center, :color => "686868"
            end

            pdf.formatted_text_box [
                                       { :text => "Website: www.homebinder.com\n", :size => 8, :color => "686868" },
                                       { :text => "Call support: 800.377.6915", :size => 8, :color => "686868" }
                                   ], :at => [0, -65], :width => 400, :height => 50

            pdf.formatted_text_box [
                                       { :text => "Report Date: #{Date.today}", :size => 8, :color => "686868" }
                                   ], :at => [pdf.bounds.right-90, -65], :width => 100, :height => 50
        end
    end
end
