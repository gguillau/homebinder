require 'date'
require 'open-uri'
require 'uri'
require 'prawn'
require 'prawn/table'
require 'nokogiri'

module AnnualPropertyReviews
    class AprPdfGenerator < PdfService
        
        attr_accessor :apr, :grey_color
                            
        def initialize(id)
            raise BadRequestException.new "APR ID Required" if id.nil?
            self.apr = AnnualPropertyReview.find(id)
            self.grey_color = "767676"
            self.text = "Annual Property Review \u2122 Report #{apr.created_at.year}"
            convert_bullet_points
        end
      
        def generate
            init
            add_title
            add_hero_photo
            add_details
            add_homeowner_comments
            add_findings
            add_capital_items
            add_reviewer_comments
            add_liability
            add_footer
            pdf.render
        end
        
        def init
            self.pdf = Prawn::Document.new( :bottom_margin => 75, :top_margin => 85 )
            super
        end
        
        def add_title
            # logos and title
            pdf.repeat(:all, :dynamic => true) do
                pdf.bounding_box([0, 686], :width => pdf.bounds.width, :height => 50) do
                    # hb logo, top left
                    pdf.image "#{Rails.root}/public/img/flatlogonotext.png", :height => 50, :width => 50, :position => :left, :vposition => :top
                    
                    pdf.move_up(35)
                    pdf.draw_text "Annual Property Review \u2122 Report #{apr.created_at.year}\n", :at => [100, pdf.cursor - 10], :color => grey_color, :size => 18
                    pdf.move_up(50)

                    if apr.reviewer_company.logo.exists?
                        logo = apr.reviewer_company.logo.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :thumb)
                        pdf.image open(logo, {ssl_verify_mode:OpenSSL::SSL::VERIFY_NONE}), :height => 50, :position => :right, :vposition => :top
                    end
                end
            end
            pdf.move_down(75)
        end
        
        def add_hero_photo
            return if !apr.annual_property_review_photos.where(:annual_property_review_finding_id => nil, :annual_property_review_capital_item_id => nil).exists?
            image = apr.annual_property_review_photos.first.file.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :large)
            # report hero photo
            pdf.repeat([1], :dynamic => true) do
                pdf.bounding_box([0, 600], :width => pdf.bounds.width/3, :height => 150) do
                    pdf.image open(image, {ssl_verify_mode:OpenSSL::SSL::VERIFY_NONE}), :height => 150, :position => :left, :vposition => :top
                end
            end
        end
        
        def add_details

            pdf.move_up(50)

            review_date = apr.scheduled_for.present? ? apr.scheduled_for.strftime("%A, %B %d, %Y") : "No Review Date Set"
            phone = apr.reviewer_company.phone.phony_formatted(:format => :national, :spaces => '-')
            
            details = [[{:content => "APR Details", :colspan => 2}]]
            details += [["Review Date", review_date]]
            details += [["Prepared for" ,apr.client_first_name + " " + apr.client_last_name]]
            details += [["Address", apr.address1 + (apr.address2 ? " " + apr.address2 : "") + ", " + apr.city + ", " + apr.state]]
            details += [["Reviewer Company", apr.reviewer_company.name]]
            details += [["Reviewer Phone", phone]]
            details += [["Reviewer", apr.reviewer ? apr.reviewer.user_profile.display_name : "Not assigned"]]
            
            pdf.table(details, :header => false, :width => 250, :cell_style => { :inline_format => true, :border_lines => [:solid]}, :position => :right) do
                cells.padding       = 6
                cells.borders       = [:top, :bottom, :left, :right]
                cells.size          = 9
                cells.border_color  = "686868"
                 
                row(0).borders      = [:top, :bottom, :left, :right]
                row(0).border_width = 1
                row(0).font_style   = :bold
                row(0).size         = 8
                row(0).text_color   = "ff6633"
                row(0).background_color = "888888"
            end
        end
        
        def add_homeowner_comments
            pdf.move_down(25)
            
            comments = apr.client_comments.present? ? apr.client_comments : "No Homeowner Comments"
            
            details = [["Homeowner Comments"]]
            details += [[comments]]
            
            pdf.table(details, :header => false, :width => pdf.bounds.width, :cell_style => { :inline_format => true, :border_lines => [:solid]}) do
                cells.padding       = 6
                cells.borders       = [:top, :bottom, :left, :right]
                cells.size          = 9
                cells.border_color  = "686868"
                 
                row(0).borders      = [:top, :bottom, :left, :right]
                row(0).border_width = 1
                row(0).font_style   = :bold
                row(0).size         = 8
                row(0).text_color   = "ff6633"
                row(0).background_color = "888888"
            end
        end
        
        def add_findings
            pdf.move_down(25)

            details = [[{:content => "Findings", :colspan => 6}]]
            details += [["Photo", "Finding", "Priority", "Cost", "Details", "Home Pro"]]
            
            width = pdf.bounds.width/6
            column_widths = [width, width, width, width, width, width]
            
            if apr.annual_property_review_findings.length > 0
                
                apr.annual_property_review_findings.order("priority DESC").each do |finding|
                    recommended_homepro = finding.recommended_homepro.present? ? "#{finding.recommended_homepro[:name]} #{finding.recommended_homepro[:phone]}" : "None"
                    recommended_homepro = finding.homeowner_item === true ? "To Be Completed by Homeowner" : recommended_homepro

                    photo = finding.annual_property_review_photos.length > 0 ? open(finding.annual_property_review_photos[0].file.expiring_cloud_front_url(Rails.application.config.access_tokens[:token_refresh_interval], :large), {ssl_verify_mode:OpenSSL::SSL::VERIFY_NONE}) : "#{Rails.root}/public/img/no-image-available.jpg"

                    details += [[{:image => photo, :fit => [50,50]}, finding.name, finding.priority.upcase, finding.cost, finding.details, recommended_homepro]]
                end
                
            else
                details += [[{:content => "No Findings Added", :colspan => 6}]]
            end
            
            pdf.table(details, :header => false, :column_widths => column_widths, :cell_style => { :inline_format => true, :border_lines => [:solid]}) do
                cells.padding       = 4
                cells.borders       = [:top, :bottom, :left, :right]
                cells.size          = 9
                cells.border_color  = "686868"
                 
                row(0).borders      = [:top, :bottom, :left, :right]
                row(0).border_width = 1
                row(0).font_style   = :bold
                row(0).size         = 8
                row(0).text_color   = "ff6633"
                row(0).background_color = "888888"
                
                row(1).borders      = [:top, :bottom, :left, :right]
                row(1).border_width = 1
                row(1).font_style   = :bold
                row(1).size         = 8
                row(1).text_color   = "ff6633"
            end
        end
        
        def add_capital_items
            pdf.move_down(25)
            
            details = [[{:content => "Home Planning", :colspan => 4}]]
            details += [["Name", "Remaining Useful Life", "Cost", "Comments"]]
            
            if apr.annual_property_review_capital_items.length > 0
                
                apr.annual_property_review_capital_items.each do |capital_item|
                    comments = capital_item.comments.present? ? capital_item.comments : "No Comments Added"
                    details += [[capital_item.name, capital_item.rul, capital_item.cost, comments]]
                end
                
            else
                details += [[{:content => "No Home Planning Items Added", :colspan => 4}]]
            end
            
            width = pdf.bounds.width/3.75
            rest = (pdf.bounds.width - width)/3
            
            pdf.table(details, :header => false, :column_widths => [width, rest, rest, rest], :cell_style => { :inline_format => true, :border_lines => [:solid]}) do
                cells.padding       = 6
                cells.borders       = [:top, :bottom, :left, :right]
                cells.size          = 9
                cells.border_color  = "686868"
                 
                row(0).borders      = [:top, :bottom, :left, :right]
                row(0).border_width = 1
                row(0).font_style   = :bold
                row(0).size         = 8
                row(0).text_color   = "ff6633"
                row(0).background_color = "888888"
                
                row(1).borders      = [:top, :bottom, :left, :right]
                row(1).border_width = 1
                row(1).font_style   = :bold
                row(1).size         = 8
                row(1).text_color   = "ff6633"
            end
        end
        
        def add_reviewer_comments
            pdf.move_down(25)
            
            comments = apr.property_reviewer_comments.present? ? Nokogiri::HTML(apr.property_reviewer_comments).text : "No Reviewer Comments"

            details = [["Reviewer Comments"]]
            details += [[comments]]
            
            pdf.table(details, :header => false, :width => pdf.bounds.width, :cell_style => { :inline_format => true, :border_lines => [:solid]}) do
                cells.padding       = 6
                cells.borders       = [:top, :bottom, :left, :right]
                cells.size          = 9
                cells.border_color  = "686868"
                 
                row(0).borders      = [:top, :bottom, :left, :right]
                row(0).border_width = 1
                row(0).font_style   = :bold
                row(0).size         = 8
                row(0).text_color   = "ff6633"
                row(0).background_color = "888888"
            end
        end
        
        def add_liability
            pdf.move_down(20)
            pdf.formatted_text_box [
            { :text => "Limitation of Liability", :size => 12, :color => grey_color },
            ], :at => [0, pdf.cursor], :width => 400
            
            pdf.move_down(20)
            text = "Although the Reviewer will attempt to review numerous aspects of the home, this is not an inclusive or investigative report.Certain aspects due to conditions may not be practically reviewable at the time of the site visit. The maximum liability for which HomeBinder and the Reviewer will be accountable for is theprice paid for the Annual Property Review. Please see the full Terms and Conditions and Limitations on the APR Ordering Page."
            pdf.text text, :size => 9, :color => "000000", :align => :justify
        end
        
        def add_footer
            # footer
            pdf.repeat(:all, :dynamic => true) do
                pdf.formatted_text_box [
                    { :text => "Website: www.homebinder.com\n", :size => 8, :color => "686868" },
                    { :text => "Call support: 800.377.6915", :size => 8, :color => "686868" }
                ], :at => [0, -45], :width => 400, :height => 50
                
                pdf.formatted_text_box [
                    { :text => "Report Date: #{Date.today}", :size => 8, :color => "686868" }
                ], :at => [pdf.bounds.right-90, -45], :width => 100, :height => 50
            end
        end

        def convert_bullet_points
            return if apr.property_reviewer_comments.nil?
            if apr.property_reviewer_comments.include?('<ol>')
                self.apr.property_reviewer_comments.gsub!("<ol>","\n\n")
            end
            if apr.property_reviewer_comments.include?('</ol>')
                self.apr.property_reviewer_comments.gsub!("</ol>","\n")
            end
            if apr.property_reviewer_comments.include?('<li>')
                self.apr.property_reviewer_comments.gsub!("<li>","\u2022&nbsp;")
            end
            if apr.property_reviewer_comments.include?('</li>')
                self.apr.property_reviewer_comments.gsub!("</li>","\n")
            end
        end
        
    end
end