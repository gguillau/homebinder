require 'base64'
require 'uri'

class RecallService

    #############################################
    # the 2 functions below are used by the admin.
    # they are part of the original method of checking recalls

    def self.download_recalls
        CPSCService.delay(:queue => "non_critical").get_new_recalls
    end

    def self.get_new_recalls
        return Recall.where(verified: false)
    end
    ##############################################
end
