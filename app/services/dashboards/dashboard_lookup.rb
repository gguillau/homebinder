module Dashboards
  class DashboardLookup
    def initialize(user)
      @user = user
      @ability = Ability.new(@user)
    end
    
    def for_binder(params = {})

      binder_id = params[:id]
      raise BadRequestException.new unless binder_id.present?
      
      binder = Binder.find(binder_id)
      
      # check for a user binder for the user
      user_binder = binder.user_binders.where(binder_id: binder_id, user_id: @user.id).first
      if user_binder.present?
        # check for a dashboard id on the user binder
        dashboard_id = user_binder.dashboard_id
        if dashboard_id.present?
          # get the dashboard
          dashboard = Dashboard.where(id: dashboard_id).first
          add_double_business_card_widget(dashboard.id, binder_id)
          return dashboard if dashboard.present?
        end
      end
      
      # no user level dashboard. check for a partner level dashboard
      partner_binder = binder.partner_binders.where(role: PartnerBinderRole::BINDER).first
      if partner_binder.present?
        dashboard_id = partner_binder.dashboard_id
        if dashboard_id.present?
          # get the dashboard
          dashboard = Dashboard.where(id: dashboard_id).first
        else
          # get the default dashboard for the partner
          dashboard = Dashboard.where(partner_id: partner_binder.partner_id, default: true, scope: DashboardScope::BINDER).first
        end
        
        # if we still don't have a dashboard check for an org default dashboard
        if dashboard.nil?
          partner = Partner.find_by_id(partner_binder.partner_id)
          if partner.present? and partner.belongs_to_organization?
            partner.organizations.each do |org|
              # get the default dashboard for the org
              dashboard = Dashboard.where(organization_id: org.id, default: true).first
              if dashboard.present?
                break
              end
            end
          end
        end
        
        # save and return?
        if dashboard.present?
          if @user.is_homeowner?
            add_double_business_card_widget(dashboard.id, binder_id)
            return copy_dashboard_to_user(user_binder, dashboard, binder)
          else
            add_double_business_card_widget(dashboard.id, binder_id)
            return dashboard
          end
        end
      end
    
      # nothing found. get a system binder
      dashboard = Dashboard.where(system: true, default: true, scope: DashboardScope::BINDER).first
      return nil unless dashboard.present?
      if @user.is_homeowner?
        add_double_business_card_widget(dashboard.id, binder_id)
        return copy_dashboard_to_user(user_binder, dashboard, binder)
      else
        return dashboard
      end
    end
    
    def for_user(params = {})
      # check for a dashboard the user owns
      board = Dashboard.where(user_id: @user.id, default: true, scope: DashboardScope::USER).first
      return board if board.present?
      
      # check for a dashboard owned by the partner
      if @user.belongs_to_partner?
        board = Dashboard.where(partner_id: @user.partners[0].id, default: true, scope: DashboardScope::USER).first
        return board if board.present?
      end
      
      # check for a dashboard owned by the org
      if @user.belongs_to_organization?
        board = Dashboard.where(organization_id: @user.organizations[0].id, default: true, scope: DashboardScope::USER).first
        return board if board.present?
      end
      
      # get one of the system dashboards
      if @user.role == UserGlobalRoles::ADMIN.to_s
        Dashboard.where(system: true, default: true, scope: DashboardScope::ADMIN).first
      elsif @user.role == UserGlobalRoles::HOMEOWNER.to_s
        Dashboard.where(system: true, default: true, scope: DashboardScope::USER).first
      elsif @user.role == UserGlobalRoles::INSPECTOR.to_s
        Dashboard.where(system: true, default: true, scope: DashboardScope::INSPECTOR).first
      elsif @user.role == UserGlobalRoles::BROKER.to_s
        Dashboard.where(system: true, default: true, scope: DashboardScope::BROKER).first
      elsif @user.role == UserGlobalRoles::PARTNER.to_s
        Dashboard.where(system: true, default: true, scope: DashboardScope::PARTNER).first
      else
        Dashboard.where(system: true, default: true, scope: DashboardScope::USER).first
      end
    end
    
    private
    
    def add_double_business_card_widget(dashboard_id, binder_id)

      dashboard = Dashboard.find_by_id(dashboard_id)
      return if dashboard.nil?
      return if dashboard.default?  && dashboard.system?
      
      binder = Binder.find_by_id(binder_id)
      return if binder.nil?

      agent_user_binder = UserBinder.where(:role => "buyer_agent", :binder_id => binder_id).first
      binder_branding = Binder::BinderBranding.where(:scope => "binder", :binder_id => binder_id).first
      return if (agent_user_binder.nil? && binder_branding.nil?)

      double_business_card_widget = Widget.find_by_name("Double Business Card")
      return if double_business_card_widget.nil?

      agent_dashboard_widget = DashboardWidget.where(:dashboard_id => dashboard_id, :widget_id => double_business_card_widget.id).first
      return if agent_dashboard_widget.present?

      return if WidgetExclusion.where(:partner_id => binder.partners.pluck(:id), :widget_id => double_business_card_widget.id).exists?
      # add the agent business card
      DashboardWidget.create!(:dashboard_id => dashboard_id, :widget_id => double_business_card_widget.id, :index => 4)
    end
    
    def copy_dashboard_to_user(user_binder, dashboard, binder)
      # return the original dashboard without copying if no user binder
      return dashboard if user_binder.nil?
      
      # copy the dashboard
      copy = Dashboard.create!(name: binder.name, description: dashboard.description, user_id: user_binder.user_id, scope: dashboard.scope)
      dashboard.dashboard_widgets.each do |dw|
        
        if dw.widget.key === "Agent Business Card"
          next if (!binder.user_binders.where(:role => "buyer_agent").exists? && !binder.binder_brandings.where(:scope => "binder").exists?)
        end
        
        DashboardWidget.create!(dashboard_id: copy.id, widget_id: dw.widget_id, index: dw.index)
      end
      user_binder.dashboard_id = copy.id
      user_binder.save!
      return copy
    end
  end
end