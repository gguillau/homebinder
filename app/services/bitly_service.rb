require "bitly"

class BitlyService
    
    attr_accessor :bitly
    
    def initialize
        self.bitly = ::Bitly.client
    end
    
    def create(link)
        return bitly.shorten(link)
    end
end