class ShareService
    
    # initalizes the service with a share ID and retrieves
    # the to_user, from_user, and the binder

    def initialize(id)
        @share = Binder::Share.find(id)
        @from_user = @share.sender
        @to_user = @share.receiver
        @binder = @share.binder
    end
    
    def handle_webhook(status)
        # update the share
        @share.status = status
        @share.save!
    end
end