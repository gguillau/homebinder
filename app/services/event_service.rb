class EventService
    include Sidekiq::Worker
    sidekiq_options queue: 'low'
    
    def perform(event)
        return if event.nil?
        Event.create(event)
    end

end