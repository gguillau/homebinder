require 'date'
require 'open-uri'
require 'uri'
require 'prawn'
require 'prawn/table'
require 'nokogiri'

class FullSellerReportPdfService < SellerReportPdfService
    
    def initialize(seller_report)
        super
        self.full_report = true
    end
end
