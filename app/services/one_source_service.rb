require 'httparty'

class OneSourceService
    
    def request_services(binder)
        # set up the data
        data = {
            'Phone' => binder.owner.user_profile.mobile_phone,
            'Address' => binder.property.address1,
            'City' => binder.property.city,
            'State' => binder.property.state,
            'Zip' => binder.property.zip.present? ? binder.property.zip : "55555",
            'FirstName' => binder.owner.user_profile.first_name.gsub("&", "and"),
            'LastName' => binder.owner.user_profile.last_name.gsub("&", "and"),
            'Email' => binder.owner.email,
            "APIKey" => api_key, 
            "Method" => "lead-create",
            "OwnRent" => "Own",
            "ClosingDate" => Date.today + 21.days
        }
        
        # signature
        signature = Digest::MD5.hexdigest(Time.now.in_time_zone("UTC").strftime('%Y-%m-%d %H:%M') + data.to_json + secret_key)
        
        # headers
        headers = {"Signature" => "#{signature}\r\n", "Content-Type" => "application/json"}

        # make the call
        response = HTTParty.post(URI.escape(url), :body => JSON.generate(data), :headers => headers)

        if response.code === 200
            result = JSON.parse(response.body)
            if result["ResultCode"] === 200
                return result
            else
                raise BadRequestException.new "OneSourceService Error - #{result["ResultMessage"]} - Binder ID: #{binder.id}"
            end
        else
            raise BadRequestException.new response.body
        end
    end
    
    def url
        if Host.path === Host::PRODUCTION_HOST
            raise BadRequestException.new "One Source Production URL Required" unless ENV["ONE_SOURCE_PRODUCTION_URL"].present?
            ENV["ONE_SOURCE_PRODUCTION_URL"].to_s
        else
            raise BadRequestException.new "One Source Test URL Required" unless ENV["ONE_SOURCE_TEST_URL"].present?
            ENV["ONE_SOURCE_TEST_URL"].to_s
        end
    end
    
    def api_key
        raise BadRequestException.new "One Source API KEY Required" unless ENV["ONE_SOURCE_API_KEY"].present?
        ENV["ONE_SOURCE_API_KEY"].to_s
    end
    
    def secret_key
        if Host.path === Host::PRODUCTION_HOST
            raise BadRequestException.new "One Source Production Secret Required" unless ENV["ONE_SOURCE_PRODUCTION_SECRET"].present?
            ENV["ONE_SOURCE_PRODUCTION_SECRET"].to_s
        else
            raise BadRequestException.new "One Source Test Secret Required" unless ENV["ONE_SOURCE_TEST_SECRET"].present?
            ENV["ONE_SOURCE_TEST_SECRET"].to_s
        end
    end
end