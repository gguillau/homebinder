class TransferService
    include Binder::Transfer::Constants
    
    # initalizes the service with a transfer ID and retrieves 
    # the to_user, from_user, and the binder
    
    def initialize(id)
        @transfer = Binder::Transfer.find(id)
        @from_user = @transfer.sender
        @to_user = @transfer.receiver
        @binder = @transfer.binder
        @partner = get_partner
    end
    
    def send_email(is_resend = false, cc_transfer_by)
        if @partner
            case @partner.partner_type
            when "inspector"
                TransferMailer.notify_email_from_inspector(is_resend, cc_transfer_by, @transfer&.id, @partner.id).deliver_later
            when "broker"
                TransferMailer.notify_email_from_broker(is_resend, cc_transfer_by, @transfer&.id, @partner.id).deliver_later
            when "hoa"
                TransferMailer.notify_email_from_hoa(is_resend, cc_transfer_by, @transfer&.id, @partner.id).deliver_later
            else
                TransferMailer.notify_email_from_partner(is_resend, cc_transfer_by, @transfer&.id, @partner.id).deliver_later
            end
        else
            TransferMailer.notify_email(is_resend, cc_transfer_by, @transfer&.id).deliver_later
        end
        
        if not is_resend
            send_agent_email
        end
        
        send_text_message
    end
    
    def transfer_ownership
        # remove the @binder from the current owner
        remove_from_current_owner
        
        add_user
    end
    
    def remove_from_current_owner
        # get the owner
        owner = UserBinder.where(binder_id: @binder.id, role: "owner").first
        
        # if owner is a partner
        if owner && owner.user.role != "homeowner"
            # remove access to the binder from all users except the buyer/seller agent
            UserBinder.where(binder_id: @binder.id).where.not("role = ? OR role = ?", "buyer_agent", "seller_agent").delete_all
        else
            # remove access to the binder from all users
            UserBinder.where(binder_id: @binder.id).delete_all
        end
        
        # cancel the subscription if it's not free for life. Otherwise let it stick
        # around. No card is attached

        subscription = @binder.subscription
        # check if binder subscription exists first - some binders were created without subscriptions due to past bugs
        if subscription.nil?
            SubscriptionService.create_subscription(@binder.id, @partner&.id)
        end
        if not SubscriptionService.is_free_for_life?(subscription) 
            SubscriptionService.cancel subscription
        end
    end
    
    def cancel
        remove_from_current_owner
        
        # add the binder to the new owner
        UserBinder.create!(user_id: @from_user.id, binder_id: @binder.id, role: UserBinderRoles::OWNER)
    end
    
    def update_receiver(to_user)
        return if @to_user.nil?
        
        # get the user's first and last name and phone number
        user = {
            :email => to_user[:email],
            :user_profile_attributes => {
                :id => @to_user.user_profile.id,
                :address_attributes => @to_user.user_profile.address ? @to_user.user_profile.address.attributes : {},
                :first_name => to_user[:first],
                :last_name => to_user[:last],
                :mobile_phone => to_user[:phone]
            }
        }
        # update the user
        if not @to_user.update_attributes(user)
            raise UnprocessableException.new(@to_user)
        end
    end
    
    # all bounced emails will transfer ownership to orphans
    # need to change/update once transfer object is back
    
    def transfer_back_ownership
        # set the to user
        @to_user = User.find_by_email("orphans@homebinder.com")
        # check if orphans exist
        if @to_user.nil?
            # send an error to new relic
            ErrorService.perform_async("orphans@homebinder.com does not exist", {:binder_id => @binder.id}, nil)
            return
        end
        
        @binder.status = "orphan"
        @binder.save!
        
        # transfer ownership
        transfer_ownership
    end
    
    def transfer_copy
        # make a copy of the @binder
        new_binder = copy_item(@binder)
        new_binder.property = copy_item(@binder.property)
        new_binder.save!
        
        # copy over the structures
        @binder.structures.each do |s|
            new_s = copy_item(s)
            new_binder.structures << new_s
        end
        
        # copy over the areas
        @binder.areas.each do |a|
            new_a = copy_item(a)
            new_binder.areas << new_a
        end
        
        # copy over the appliances
        @binder.appliances.each do |a|
            if a.seller_report_item and a.seller_report_item.include
                new_a = copy_item(a)
                new_binder.appliances << new_a
            end
        end
        
        # copy over the contractors
        @binder.binder_contractors.each do |bc|
            if bc.seller_report_item and bc.seller_report_item.include?
                new_bc = copy_item(bc)
                new_bc.account_number = nil
                new_binder.binder_contractors << new_bc
            end
        end
        
        # copy over the maintenance
        @binder.maintenance_items.each do |mi|
            if mi.seller_report_item and mi.seller_report_item.include
                new_mi = copy_item(mi)
                me = mi.next_event
            if not me.nil?
                new_mi.do_date = me.do_date
            end
                new_binder.maintenance_items << new_mi
            end
        end
        
        # copy over the finishes
        @binder.finishes.each do |f|
            if f.seller_report_item and f.seller_report_item.include
                new_f = copy_item(f)
                new_binder.finishes << new_f
            end
        end
        
        # copy over the paints
        @binder.paints.each do |p|
            if p.seller_report_item and p.seller_report_item.include
                new_p = copy_item(p)
                new_binder.paints << new_p
            end
        end
        
        # copy over the projects
        @binder.projects.each do |p|
            if p.seller_report_item and p.seller_report_item.include?
                new_p = copy_item(p)
                new_p.cost_cents = 0
                new_binder.projects << new_p
            end
        end
        
        # set the subscription level for the binder to free
        subscription = Subscription.new(binder_id: new_binder.id, plan_id: 'free')
        subscription.save!
        
        # copy the subscription over if it's free for life
        if SubscriptionService.is_free_for_life?(@binder.subscription)
            new_binder.subscription = Subscription.new({})
            new_binder.subscription.binder_id = @binder.id
            SubscriptionService.copy_subscription(@binder.subscription, new_binder.subscription)
        end
        
        # we have to do the documents and images after saving the @binder
        # because the id is part of the s3 key
        
        # copy the documents
        @binder.documents.each do |d|
            if d.seller_report_item and d.seller_report_item.include
                new_d = copy_item(d)
                new_binder.documents << new_d
            end
        end
        
        # copy the images
        @binder.images.each do |i|
            if i.seller_report_item and i.seller_report_item.include
                new_i = copy_item(i)
                new_binder.images << new_i
            end
        end
        
        #copy the current transfer
        transfer = copy_item(@transfer)
        transfer.access_token = nil
        transfer.binder_id = new_binder.id
        transfer.save!
        
        # make the transfer_to user the new owner
        @to_user.add_role :owner, new_binder
    end
        
    def copy_item(item)
        if item.nil?
            return
        end
        
        klass = Object.const_get item.class.name
        new_item = klass.new(item.attributes)
        new_item.id = nil
        new_item.binder_id = nil unless not item.respond_to?(:binder_id)
        new_item.created_at = nil
        new_item.updated_at = nil
        
        # copy the tags
        if item.respond_to?(:tags)
            tags = Array.new
            tags = item.tags
            new_item.tags = [];
            tags.each do |t|
                new_item.tags << Tag.new(tag: t.tag, auto_generated: t.auto_generated)
            end
        end
        
        return new_item
    end
    
    def analytics
        # call analytics
        Binders::BinderAnalytics.delay(:queue => "analytics").transfer(@transfer.id, @partner&.id)
        Users::UserAnalytics.delay(:queue => "analytics").transfer_received(@transfer.id)
    end
    
    def get_partner
        return if @from_user.nil?
        if @from_user.belongs_to_partner? && !@from_user.is_agent?
            return Partner.joins(:partner_users).where(:partner_users => {:user_id => @from_user.id}).first
        end
    end
    
    def handle_webhook(status)
        # transfer ownership if delivery failed
        if status === "delivery_failed"
            # update the transfer
            @transfer.status = status
            @transfer.delivery_failed_at = Time.now
            @transfer.save!
            # transfer
            transfer_back_ownership
        else
            # update the transfer
            @transfer.last_notified = Time.now
            @transfer.status = status
            @transfer.save!
        end
    end
    
    def send_agent_email
        return if @partner.nil?
        
        # find the agent user_binder
        if @binder.transactions.where(:transaction_type => "sell_side_inspection").count > 0
            agent_binder = UserBinder.where(:binder_id => @binder.id, :role => "seller_agent").first
            agent_binder = UserBinder.where(:binder_id => @binder.id, :role => "buyer_agent").first if agent_binder.nil?
        else
            agent_binder = UserBinder.where(:binder_id => @binder.id, :role => "buyer_agent").first
        end
        
        return if agent_binder.nil?
        
        agent = User.where(:role => "agent", :id => agent_binder.user_id).first
        return if agent.nil?
        
        broker = Partner.find_by_email(agent.email)
        notify_agent = false
        
        if broker.nil?
            case @partner.partner_configuration.send_agents_transfer_notification
            when "always"
                notify_agent = true
            when "once"
                notify_agent = !UserBinder.where(:binder_id => PartnerBinder.where(:partner_id => @partner.id).distinct.pluck(:id), :user_id => agent.id).exists?
            end
            
            if notify_agent
                TransferMailer.notify_email_to_agent(agent.id, @transfer.id, @partner.id).deliver_later
                
                # analytics
                EventService.perform_async({
                        :event_name => "agent_notification_sent",
                        :event_type => "email",
                        :binder_id => @binder.id, 
                        :user_id => agent.id,
                        :partner_id => @partner.id
                    }
                )
            end
        end
    end
    
    def add_user
        return if @to_user.nil?
        
        if UserBinder.where(:role => "buyer_agent").count > 0
            access_repair_pricer = UserBinder.where(:role => "buyer_agent", :access_repair_pricer => true).exists?
            
            # add the binder to the new owner
            UserBinder.create!(user_id: @to_user.id, binder_id: @binder.id, role: UserBinderRoles::OWNER, :access_repair_pricer => access_repair_pricer)
        else
            UserBinder.create!(user_id: @to_user.id, binder_id: @binder.id, role: UserBinderRoles::OWNER)
        end
    end
    
    # TO DO - move to widget service
    
    def add_widgets
        return if @from_user.nil?
        # add any widgets dependent on accepting a transfer
        dashboard = Dashboards::DashboardLookup.new(@to_user).for_binder({:id => @binder.id})
        return if dashboard.nil?
        return if dashboard.system? && dashboard.default?
        
        # add the widgets
        doubleBusinessCard = Widget.find_by_name("Double Business Card")
        thirdPartyWidget = Widget.find_by_name("Third Party Services")
        homeSelfe = Widget.find_by_name("HomeSelfe Form")
        sellSideInspectionWidget = Widget.find_by_name("Sell Side Inspection Widget")
        repairPricerWidget = Widget.find_by_name("Repair Pricer Widget")
        secure24 = Widget.find_by_name("Secure 24")
        inspectionReportWidget = Widget.find_by_name("Inspection Report Widget")
        customWidget = Widget.find_by_name("Custom")
        document_type = DocumentType.find_or_create_by(:name => "Home Inspection Report")
        
        if @binder.transactions.where(:transaction_type => "sell_side_inspection").count > 0
            # check if doubleBusinessCard is present
            if doubleBusinessCard.present? && WidgetExclusion.where(:partner_id => @binder.partners.pluck(:id), :widget_id => doubleBusinessCard.id).count < 1
                # check if sender belongs to partner
                if @from_user.belongs_to_partner? && DashboardWidget.where(:dashboard_id => dashboard.id, :widget_id => doubleBusinessCard.id).count < 1
                    DashboardWidget.create!(:dashboard_id => dashboard.id, :widget_id => doubleBusinessCard.id, :index => 1)
                end
            end
            
            # check if sellSideInspectionWidget is present
            if sellSideInspectionWidget.present? && WidgetExclusion.where(:partner_id => @binder.partners.pluck(:id), :widget_id => sellSideInspectionWidget.id).count < 1
                # check if sender belongs to partner
                if @from_user.belongs_to_partner? && DashboardWidget.where(:dashboard_id => dashboard.id, :widget_id => sellSideInspectionWidget.id).count < 1
                    DashboardWidget.create!(:dashboard_id => dashboard.id, :widget_id => sellSideInspectionWidget.id, :index => 1)
                end
            end
        else
            # check if doubleBusinessCard is present
            if doubleBusinessCard.present? && WidgetExclusion.where(:partner_id => @binder.partners.pluck(:id), :widget_id => doubleBusinessCard.id).count < 1
                # check if sender belongs to partner
                if @from_user.belongs_to_partner? and DashboardWidget.where(:dashboard_id => dashboard.id, :widget_id => doubleBusinessCard.id).count < 1
                    DashboardWidget.create!(:dashboard_id => dashboard.id, :widget_id => doubleBusinessCard.id, :index => 1)
                end
            end
            
            # check if thirdPartyWidget is present and if binder property is not in CA
            if thirdPartyWidget.present? && @binder.property.country != "CA" && WidgetExclusion.where(:partner_id => @binder.partners.pluck(:id), :widget_id => thirdPartyWidget.id).count < 1
                # check if sender belongs to partner
                if @from_user.belongs_to_partner? and DashboardWidget.where(:dashboard_id => dashboard.id, :widget_id => thirdPartyWidget.id).count < 1
                    DashboardWidget.create!(:dashboard_id => dashboard.id, :widget_id => thirdPartyWidget.id, :index => 1)
                end
            end
            
            # check if selfe is present
            if homeSelfe.present? && WidgetExclusion.where(:partner_id => @binder.partners.pluck(:id), :widget_id => homeSelfe.id).count < 1
                # check if sender belongs to partner
                if @from_user.belongs_to_partner? and @binder.partners.joins(:organizations).where(:organizations => {name: "Amerispec"}).count > 0 and DashboardWidget.where(:dashboard_id => dashboard.id, :widget_id => homeSelfe.id).count < 1
                    DashboardWidget.create!(:dashboard_id => dashboard.id, :widget_id => homeSelfe.id, :index => 1)
                end
            end
            
            # check if secure24 is present
            if secure24.present? && WidgetExclusion.where(:partner_id => @binder.partners.pluck(:id), :widget_id => secure24.id).count < 1
                # check if sender belongs to partner
                if @from_user.belongs_to_partner? and @binder.partners.joins(:organizations).where(:organizations => {name: "Amerispec"}).count > 0 and DashboardWidget.where(:dashboard_id => dashboard.id, :widget_id => secure24.id).count < 1
                    DashboardWidget.create!(:dashboard_id => dashboard.id, :widget_id => secure24.id, :index => 1) unless @binder.property.country != "US"
                end
            end
            
            # check if repairPricerWidget is present
            if UserBinder.where(:user_id => @to_user.id, :binder_id => @binder.id, :access_repair_pricer => true).exists? && repairPricerWidget.present? && WidgetExclusion.where(:partner_id => @binder.partners.pluck(:id), :widget_id => repairPricerWidget.id).count < 1 && @from_user.belongs_to_partner?
                # check if sender belongs to partner
                partner = @from_user.partners[0]
                if partner.partner_configuration.repair_pricer_enabled && partner.partner_configuration.repair_pricer_messaging_enabled && DashboardWidget.where(:dashboard_id => dashboard.id, :widget_id => repairPricerWidget.id).count < 1
                    DashboardWidget.create!(:dashboard_id => dashboard.id, :widget_id => repairPricerWidget.id, :index => 1)
                end
            end

            # check if inspectionReportWidget is present and if the homeowner has a document identified as an inspection report
            if inspectionReportWidget.present? && @binder.documents.where(:document_type_id => document_type.id).exists? && (WidgetExclusion.where(:partner_id => @binder.partners.pluck(:id), :widget_id => inspectionReportWidget.id).count < 1)
                # check if sender belongs to partner

                if @from_user.belongs_to_partner? && (DashboardWidget.where(:dashboard_id => dashboard.id, :widget_id => inspectionReportWidget.id).count < 1)
                    DashboardWidget.create!(:dashboard_id => dashboard.id, :widget_id => inspectionReportWidget.id, :index => 1)
                end
            end
            
            # check if customWidget and PartnerWidget are present
            if customWidget.present? && !@partner.nil? && PartnerWidget.where(:partner_id => @partner.id, :widget_id => customWidget.id).exists? && WidgetExclusion.where(:partner_id => @binder.partners.pluck(:id), :widget_id => customWidget.id).count < 1
                # check if sender belongs to partner
                if @from_user.belongs_to_partner? and DashboardWidget.where(:dashboard_id => dashboard.id, :widget_id => customWidget.id).count < 1
                    DashboardWidget.create!(:dashboard_id => dashboard.id, :widget_id => customWidget.id, :index => 1)
                end
            end
        end
    end
    
    def send_text_message
        return if @to_user.nil?
        return if @from_user.nil?
        return if @binder.nil?
        return if ENV["TEXT_MESSAGING_AB_TESTING"] === "true" && rand(2) < 1
        return if !@to_user.user_profile.text_messaging_enabled?
        return if !@transfer.text_message_sid.nil?
        
        bitly = BitlyService.new.create("https://#{Host.generate_host_path}/homeowners/#{@transfer.access_token}/welcome?notify=text")
        return if bitly.nil?
        message = "Hi #{@to_user.user_profile.display_name}, your #{@from_user.user_profile.full_role} #{@from_user.user_profile.display_name} set you up with a \xF0\x9F\x8F\xA1 Homebinder account for your recent inspection at #{@binder.full_address}\n\nHomeBinder is a digital home management that likely already has the \xF0\x9F\x93\x91 inspection report, \xF0\x9F\x93\x85 custom maintenance reminders, \xf0\x9f\x9b\xa0 recommended home pros and more.\n\nComplete the last step in your inspection by accessing your HomeBinder at: #{bitly.short_url}"
        text_message = TextService::Sms.new.send_message(@to_user.user_profile.mobile_phone, message)
        return if text_message.nil?

        @transfer.text_message_sid = text_message.sid
        @transfer.save
    rescue => e
        ErrorService.perform_async(e.message, {transfer_id: @transfer.id})
    end
end