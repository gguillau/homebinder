class TextService::Twilio
    
    def client
        Twilio::REST::Client.new account_sid, auth_token
    end
    
    def account_sid
        raise BadRequestException.new "Twilio Account SID Required" if ENV["TWILIO_ACCOUNT_SID"].blank?
        ENV["TWILIO_ACCOUNT_SID"]
    end
    
    def auth_token
        raise BadRequestException.new "Twilio Auth Token Required" if ENV["TWILIO_AUTH_TOKEN"].blank?
        ENV["TWILIO_AUTH_TOKEN"]
    end
end