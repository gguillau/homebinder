require 'twilio-ruby'

class TextService::Sms
    attr_accessor :client
    
    def initialize
        self.client = TextService::Twilio.new.client
    end
    
    def send_message(number, message)
        return if ENV["ENABLE_TEXT_MESSAGING"] != "true"
        return if number.blank?
        return if message.blank?
        return if not Phony.plausible?(number)

        return client.messages.create(
            from: ENV["TWILIO_DEFAULT_PHONE_NUMBER"],
            to: number,
            status_callback: "https://#{Host.generate_host_path}/api/v1/twilio/webhooks",
            body: message
        )
    end
    
    def run_at
        # need to check if time is between 10AM and 2PM
        # if time is between 10AM and 2PM then we run the job
        # if not then we schedule the job to run at 10AM the following day
        
        starting_time = Time.zone.parse "9:59 AM"
        ending_time = Time.zone.parse "2:01 PM"
        current_time = Time.zone.now 

        if current_time > starting_time && 
            current_time  < ending_time
            return Time.now
        else
            if current_time < starting_time
                return Time.zone.today.at_beginning_of_day.advance(hours: 10) 
            else
                return Time.zone.tomorrow.at_beginning_of_day.advance(hours: 10) 
            end
        end
    end
end