class TextService::Tool
    
    # expecting params to contains similar:
    # {
    #   "SmsSid"=>"SM7484a798931d424d960d2f03a58c3121", "SmsStatus"=>"delivered", "MessageStatus"=>"delivered", 
    #   "To"=>"+17714968453", "MessageSid"=>"SM7484a798931d424d960d2f03a58c3121", "AccountSid"=>"ACd25cb21f3590e614e3a1b801f3ace52d", 
    #   "From"=>"+16176525661", "ApiVersion"=>"2010-04-01"
    # }
    
    def parse(params)
        return if params.blank?
        return if params.dig("To").blank?
        return if params.dig("MessageSid").blank?
        return if params.dig("SmsStatus").blank?
        return if !Binder::Transfer.where(:text_message_sid => params.dig("MessageSid")).exists?
        
        transfer = Binder::Transfer.where(:text_message_sid => params.dig("MessageSid")).first
        transfer.text_message_status_date = Time.now
        transfer.text_message_status = params.dig("SmsStatus")
        transfer.save!
    end
end