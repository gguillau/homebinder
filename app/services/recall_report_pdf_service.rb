require 'date'
require 'open-uri'
require 'uri'
require 'prawn'
require 'prawn/table'

class RecallReportPdfService < PdfService
    
    attr_accessor :binder
    
    def initialize(binder)
        super
        self.binder = binder
        self.text = "Recall Report for"
    end

    def create
        init
        add_logos
        add_title
        add_recall_title
        add_recall_details
        add_appliances
        add_footer
        pdf
    end

    def add_recall_title
        #recall title
        if binder.recalls > 0
            text = "POTENTIAL RECALLS IDENTIFIED"
            f_color = "fcf8e3"
            s_color = "faebcc"
        else
            text = "NO POTENTIAL RECALLS IDENTIFIED"
            f_color = "dff0d8"
            s_color = "d6e9c6"
        end

        pdf.move_down(40)

        pdf.line_width = 2
        pdf.fill_color "#{f_color}"
        pdf.stroke_color "#{s_color}"

        pdf.fill_and_stroke_rectangle [(pdf.bounds.left), (pdf.cursor + 15)], (pdf.bounds.right), 55

        pdf.fill_color "000000"
        pdf.stroke_color "000000"

        pdf.move_down(5)

        pdf.text "#{text}", :size => 20, :color => "000000", :align => :center
    end

    def add_recall_details
        pdf.move_down(50)

        if not binder.last_recall.nil?
            lastrecall = binder.last_recall.strftime("%B %d, %Y")
        else
            lastrecall = "N/A"
        end

        pdf.formatted_text_box [
                                   { :text => "Total Appliances: #{binder.appliances.length}", :size => 14, :color => "000000", :font_style => "bold" },
                               ], :at => [0, pdf.cursor], :width => 400

        pdf.move_down(30)

        pdf.formatted_text_box [
                                   { :text => "Number of Potential Recalls Identified: #{binder.recalls}", :size => 14, :color => "000000", :font_style => "bold" },
                               ], :at => [0, pdf.cursor], :width => 400

        pdf.move_down(30)

        pdf.formatted_text_box [
                                   { :text => "Last Recall: #{lastrecall}", :size => 14, :color => "000000", :font_style => "bold" },
                               ], :at => [0, pdf.cursor], :width => 400
    end

    def add_appliances
        # appliances list
        # appliances = seller_report.appliances.order('install_date DESC').first(@aprintcount)
        # order doesn't work anymore since appliances array isn't direct from DB.
        appliances = binder.appliances
        # appliances = appliances.sort_by(&:"install_date").reverse
        appliances = appliances.sort_by{ |install_date| [install_date ? 0 : 1, install_date]}
        appliances.reverse

        if (appliances.length > 0)
            pdf.move_down(50)
            pdf.formatted_text_box [
                                       { :text => "Appliance List", :size => 12, :color => "ff6633" },
                                   ], :at => [0, pdf.cursor], :width => 400

            pdf.move_down(10)

            appliancedata = [[ "NAME", "MAKE & MODEL", "SERIAL #", "YEAR INSTALLED", "RECALLS"]]
            appliances.each do |appliance|
                apname = "<b>#{appliance.name}</b>" unless appliance.name.nil?

                apmanu = "<b>#{appliance.manufacturer}</b>" unless appliance.manufacturer.nil?
                if not appliance.model.nil?
                    if (apmanu)
                        apmanu += "\n#{appliance.model.upcase}"
                    else
                        apmanu = "#{appliance.model}"
                    end
                end

                apserl = appliance.serial_no unless appliance.serial_no.nil?
                apdate = appliance.install_date.strftime("%Y") unless appliance.install_date.nil?

                if appliance.recalls.length > 0
                    apjson    = JSON.parse appliance.recalls.first.details
                    aprecl = "<u><link href='#{apjson["URL"]}'>POTENTIAL RECALL</link></u>"
                else
                    aprecl = "NONE"
                end

                appliancedata += [[ apname, apmanu, apserl, apdate, aprecl ]]
            end

            pdf.table(appliancedata, :header => true, :width => pdf.bounds.width, :cell_style => { :inline_format => true }) do
                cells.padding       = 6
                cells.borders       = [:bottom]
                cells.size          = 9
                cells.border_color  = "686868"
                cells.height        = 30

                row(0).borders      = [:bottom]
                row(0).border_width = 1
                row(0).font_style   = :bold
                row(0).size         = 8
            end
        end
    end

    def add_footer
        # footer
        pdf.repeat(:all) do
            pdf.bounding_box([0,-30], :width => pdf.bounds.width, :height => 30) do
                pdf.text "For any potential recalls listed, visit the CPSC website URL and review if the serial number listed is identified in the recall.", :size => 8, :align => :center, :color => "686868"
                pdf.move_down(5)
                pdf.text "For questions or support, call 800-377-6915 or email support@homebinder.com.", :size => 8, :align => :center, :color => "686868"
            end

            pdf.formatted_text_box [
                                       { :text => "Website: www.homebinder.com\n", :size => 8, :color => "686868" },
                                       { :text => "Call support: 800.377.6915", :size => 8, :color => "686868" }
                                   ], :at => [0, -65], :width => 400, :height => 50

            pdf.formatted_text_box [
                                       { :text => "Report Date: #{Date.today.strftime("%B %d, %Y")}", :size => 8, :color => "686868" }
                                   ], :at => [pdf.bounds.right-115, -65], :width => 150, :height => 50
        end
    end

end
