require 'csv'

class Partner::Automation::EmailParser

    API = "API".freeze
    COLON = ":".freeze
    IMAGE = "image/".freeze
    PARTNER = "Partner".freeze
    CONTACT = "contact".freeze
    CLIENT = "Client".freeze
    PROPERTY = "Property".freeze
    APPLICATION = "application".freeze
    BINDER_TEMPLATE_ID = "bindertemplateid".freeze
    BINDER_TEMPLATE = "BinderTemplate".freeze
    AGENT = "Agent".freeze
    BUYER_AGENT = "BuyerAgent".freeze
    SELLER_AGENT = "SellerAgent".freeze
    NBSP = "&nbsp;".freeze
    COMMA = ",".freeze
    SEMI_COLON = ";".freeze
    EQUAL = "=".freeze
    EMPTY = "".freeze
    SPACE = " ".freeze
    DELETE = "^a-zA-Z0-9".freeze
    CONTACT_DELETE = "^a-zA-Z0-9@. ".freeze
    ELSE_DELETE = "^a-zA-Z0-9-& ".freeze
    NOT_PROVIDED = "Not Provided".freeze

    def parse(email)
        email_format = nil
        # convert body of email into plain text
        plain_part = email.multipart? ? (email.text_part ? email.text_part.body.decoded : nil) : email.body.decoded
        html_part = email.html_part ? email.html_part.body.decoded : nil
        array = []
        if plain_part.nil?
            email_format = 1
            html_part = email.html_part ? email.html_part.body.decoded : nil

            string = html_part.gsub(/<\/?span[^>]*>/, EMPTY)
            # split html text into objects by <br/> characters
            array = string.split(/<br\/>/)
        elsif plain_part.present? && !html_part.present?
            # split plain text into objects by newline characters
            if plain_part.include?("\n") and !plain_part.include?("<br/>")
                email_format = 2
                string = plain_part.gsub(/<\/?p[^>]*>/, EMPTY)
                array = string.lines
            else
                email_format = 3
                string = plain_part.gsub(/<\/?span[^>]*>/, EMPTY)
                # split html text into objects by <br/> characters
                array = string.split(/<br\/>/)
            end
        elsif html_part.present?
            email_format = 4
            html_part = html_part.gsub(/<br \/>/, "\n")
            string = html_part.gsub(/<\/?p[^>]*>/, "")
            string = ActionView::Base.full_sanitizer.sanitize(string)
            formatted = string.gsub(/amp;/, "")
            array = formatted.split(/\n/)
            
            html_doc = Nokogiri::HTML html_part
            html_doc.css("p").each do |element|
                if element.children.text.include? "BuyerAgent Photo"
                    array.unshift "BuyerAgent Photo: #{element.css("img").first["src"]}"
                end
            end
        end

        # :nocov:
        # we forgo this check when running tests to tests emails with missing params
        if array.length < 3 and !Rails.env.test?
            email_format = 5
            html_part = email.html_part ? email.html_part.body.decoded : nil

            string = html_part.gsub(/<\/?p[^>]*>/, EMPTY)
            # split html text into objects by <br/> characters
            array = string.split(/<br \/>/)
        end
        # :nocov:

        correct_inspection_report_url(array)

        parameters = getParams(array)
        request = formatParams(parameters)

        request = attachReport(request, email)
        request = attachImage(request, email)
        return request
    end

    def correct_inspection_report_url(array)
        string = array.select { |str| str.include?("ReportURL") || str.include?("Report URL") }.first
        return if string.nil?
        index = array.find_index(string)
        return if index.nil? || index < 0
        return if array[index + 1].nil?
        array[index] = array[index] + array[index + 1]
    end

    def getParams(array)
        parameters = Array.new

        array.each do |text|
            # check if text contains key words
            variables.each do |word|
                if text.include? word
                    parameters.append(text)
                end
            end
        end

        return parameters
    end

    def formatParams(parameters)
        hash = Hash.new
        parameters.each do |word|
            param = word.split(COLON, 2)

            keys = param[0]

            key = formatKeys(keys)

            value = format_value(key, param[1])

            # switch case depending on key value
            case key[0]
            when API.downcase
                new = { :api_key => value.rstrip}
            when PARTNER.downcase
                if key[1].downcase === "name"
                    new = { :key => value.rstrip}
                elsif key[1].downcase === "email"
                    new = { :partner_email => value.rstrip}
                end
            when BINDER_TEMPLATE_ID
                new = { :binder_template_id => value.rstrip}
            when "buyeragent"
                if key[1] === "headshot" || key[1] === "logo"
                    value = value.present? ? value.rstrip : EMPTY
                    new = { :buyer_agent => { key[1].to_sym => { key[2].to_sym => value }}}
                elsif key[1] === "photo"
                    if !value.blank?
                        new = { :buyer_agent => { :headshot => { :url => value }}}
                    else
                        new = {}
                    end
                else
                    value = value.present? ? value.rstrip : EMPTY
                    new = { :buyer_agent => { key[1].to_sym => value}}
                end
            when "selleragent"
                if key[1] === "headshot" || key[1] === "logo"
                    value = value.present? ? value.rstrip : EMPTY
                    new = { :seller_agent => { key[1].to_sym => { key[2].to_sym => value }}}
                else
                    value = value.present? ? value.rstrip : EMPTY
                    new = { :seller_agent => { key[1].to_sym => value}}
                end
            when "reporturl", "report"
                value = value.present? ? value.rstrip : EMPTY
                value = value.present? ? value.lstrip : EMPTY
                new = { :inspection_report_url => value}
            when "inspection"
                new = { :inspection_date => value }
            when "project"
                new = { :project_number => value }
            when "transaction"
                new = { :transaction_type => value }
            else
                case key[1]
                when "photourl"
                    value = value.present? ? value.rstrip : EMPTY
                    new = { :property_photo => { :url => value}}
                else
                    if key[1].downcase === "yearbuilt"
                        key[1] = "year_built"
                    elsif key[1].downcase === "squarefootage"
                        key[1] = "sq_ft"
                    end
                    value = value.present? ? value.rstrip : EMPTY
                    new = { key[0].to_sym => { key[1].to_sym => value}}
                end
            end
            #merge keys and values into hash
            hash = hash.deep_merge(new)
        end

        return hash
    end

    def format_value(key, value)
        value = value.nil? ? "" : value
        value = value.to_s.force_encoding("UTF-8")
        value.gsub!(" ", " ")
        value = value.lstrip

        case key[0]
        when "reporturl"
            return value.lstrip
        end
        case key[1]
        when "photo", "photourl","headshot","logo", "reporturl", "url", "report"
            return value.lstrip
        when "email"
            # remove leading white space and non alpha numberic characters then return value
            value = value.gsub!(NBSP, EMPTY) unless !value.include? NBSP
            value = value.lstrip.strip
            # axium requests have not provided as a value when value is not available
            if value.downcase === NOT_PROVIDED.downcase
                return EMPTY
            else
                # check if the email includes a comma, space or semi-colon
                stripped = value.split(SPACE) unless !value.include? SPACE
                stripped = value.split(COMMA) unless !value.include? COMMA
                stripped = value.split(SEMI_COLON) unless !value.include? SEMI_COLON
                # parse the final value
                email = stripped.present? ? stripped[0] : value
                email = email.gsub!(COMMA, EMPTY) unless !email.include? COMMA
                email = email.gsub!(SEMI_COLON, EMPTY) unless !email.include? SEMI_COLON
                email = email.gsub!(EQUAL, EMPTY) unless !email.include? EQUAL
                email = email.delete('\\"')
                # return the final value
                return email
            end
        when "date"
            value = value.gsub!(NBSP, SPACE) unless !value.include? NBSP
            return value.lstrip
        when CONTACT
            # remove leading white space and non alpha numberic characters then return value
            value = value.gsub!(NBSP, SPACE) unless !value.include? NBSP
            stripped = value.delete(CONTACT_DELETE).lstrip.chomp.split(SPACE)
            return stripped.join(SPACE)
        else
            # remove leading white space and non alpha numberic characters then return value
            value = value.gsub!(NBSP, EMPTY) unless !value.include? NBSP
            value = value.gsub!(" &amp", EMPTY) unless !value.include? "&amp"
            return "" if value.nil?
            return value.delete(ELSE_DELETE).lstrip.chomp
        end
    end

    def formatKeys(keys)
        keys.gsub!(" ", " ")
        array = Array.new
        keys = keys.gsub!(NBSP, SPACE) unless !keys.include? NBSP
        object = keys.split(SPACE).first
        key = keys.split(SPACE).second

        array.append(object.downcase.delete(DELETE).lstrip.chomp) unless object.nil?
        array.append(key.downcase.delete(DELETE).lstrip.chomp) unless key.nil?

        # if agent headshot/logo
        if key and key.downcase === "headshot" || key.downcase === "logo"
            third_key = keys.split(SPACE).third
            array.append(third_key.downcase.delete(DELETE).lstrip.chomp) unless third_key.nil?
        end
        return array
    end

    def attachReport(request, email)
        files = Array.new
        email.attachments.each do |attachment|
            if (attachment.content_type.start_with?(APPLICATION))
                file = "data:#{attachment.mime_type};base64," + Base64.encode64(attachment.decoded)
                files.push({:file => file, :name => attachment.filename})
            end
        end
        new = { :documents => files}
        #merge keys and values into hash
        request = request.deep_merge(new)
        return request
    end

    def attachImage(request, email)
        files = Array.new
        email.attachments.each_with_index do |attachment, index|
            if (attachment.content_type.start_with?(IMAGE))
                file = "data:#{attachment.mime_type};base64," + Base64.encode64(attachment.decoded)
                if index == 0
                    new = { :property_photo => {:file => file, :name => attachment.filename}}
                    #merge keys and values into hash
                    request = request.deep_merge(new)
                else
                    files.push({:file => file, :name => attachment.filename})
                end
            end
            new = { :additional_images => files}
            #merge keys and values into hash
            request = request.deep_merge(new)
        end
        return request
    end

    def send_event(email_format)
        EventService.perform_async({:event_name => email_format, :event_type => "email_parser"})
    end

    def variables
        [API, PARTNER, CLIENT, PROPERTY, BINDER_TEMPLATE, AGENT, BUYER_AGENT, SELLER_AGENT, "Inspection", "Transaction", "Project", "ReportURL", "Report URL", "ReportURL"]
    end
end
