class Partner::Automation::Service::Access
    
    TIME_ZONE = "Eastern Time (US & Canada)".freeze
    
    def transfer_binders
        # get all the partners who have the automation binder action set to true
        partners = Partner.joins(:partner_configuration).where(:partner_configurations => {:automation_binder_action => true})
        # iterate through each
        partners.each do |partner|
            # get the configuration object
            configuration = partner.partner_configuration
            
            # find the admin
            user = partner.get_admin
            
            # get the users and binders
            users = User.joins(:partner_users).where(:partner_users => {:partner_id => partner.id}, :role => partner.partner_type).pluck(:id)
            all = Binder.joins(:transfers, :user_binders).where(:user_binders => {:user_id => users, :role => "owner"}).where(:transfers => {:status => "created"}).distinct
            
            # iterate through the binder
            all.each do |binder|
                # check if we can transfer/share the binder
                if can_transfer_access(binder, configuration)
                    # get the action
                    if check_binder_action(configuration) === User::TRANSFER
                        # transfer the binder
                        transfer(binder, user)
                    elsif can_share(binder)
                        # share the binder
                        share(binder, user)
                    end
                end
            end
        end
    end

    def can_transfer_access(binder, configuration)
        time = Time.now.in_time_zone(TIME_ZONE)
        created_at = binder.created_at
        hours = (time - created_at.to_datetime)/3600
        return hours > configuration.binder_action_delay_length
    end
    
    # determines whether binder can be shared with user
    # checks if the user already has access to the binder
    
    def can_share(binder)
        transfer = binder.transfers.last
        # check if the receiver exists
        if transfer.nil? or transfer.receiver.nil?
            return false
        end
        user = transfer.receiver
        return UserBinder.where(:user_id => user.id, :binder_id => binder.id).count < 1
    end
    
    # returns whether default binder action is transfer/share
    
    def check_binder_action(configuration)
        return configuration.default_binder_action
    end
    
    # checks whether we can transfer/share the binder
    # according to the setting set by the partner
    
    def can_perform_action(configuration)
        return configuration.automation_binder_action == true
    end
    
    # transfers the binder to the pending user
    
    def transfer(binder, user)
        # get the transfer
        transfer = binder.transfers.first
        
        # check if the receiver exists
        return if transfer.receiver.nil?
        
        # set up the transfer hash
        transfer = {
            id: transfer.id,
            user: {
                first: transfer.receiver.user_profile.first_name,
                last: transfer.receiver.user_profile.last_name,
                email: transfer.receiver.email,
                phone: transfer.receiver.user_profile.mobile_phone
            },
            transfer_type: "ownership",
            cc_transfer_by: false
        }
        # execute transfer
        Binder::Transfer.execute(user, transfer)
    end
    
    # shares the binder to the pending user
    
    def share(binder, user)
        # get the transfer
        transfer = binder.transfers.first
        # get the user
        homeowner = transfer.receiver
        
        # check if the receiver exists
        return if transfer.receiver.nil?
        
        # set up the share hash
        share = {
            user: {
                first: homeowner.user_profile.first_name,
                last: homeowner.user_profile.last_name,
                email: homeowner.email,
                phone: homeowner.user_profile.mobile_phone
            },
            binder_id: binder.id,
            role_name: "co_owner"
        }
        request = HBRequest.new.create_request(user)
        
        # execute share
        Binder::Share.build(request, {:share => share})
    end
end