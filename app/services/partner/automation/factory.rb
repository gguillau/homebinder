class Partner::Automation::Factory
    
    INVALID_KEY = "Invalid API Key".freeze
    INVALID_PARTNER_KEY = "Invalid Partner Key".freeze
    EMAILS = "emails".freeze
    
    # each of the lines below could be their own method so we can test each method. Raise errors from here to be handled in the controller
    def self.lookup(params)
        # verify params and key were passed in
        raise BadRequestException.new(INVALID_PARTNER_KEY) unless params.present?
        raise BadRequestException.new(INVALID_PARTNER_KEY) unless params[:key].present?
        
        # verify partner exists
        verify_partner(params[:key])
        
        # verify api_key belongs to partner
        verify_partner_key(params[:key], params[:api_key])
        
        # try and create service class
        return Partner::Automation::API::Service
    end
    
    def self.email_lookup(params)
        # verify params and key were passed in
        raise BadRequestException.new(INVALID_PARTNER_KEY) unless params.present?
        raise BadRequestException.new(INVALID_PARTNER_KEY) unless params[:key].present?
        # verify api key
        verify_api_key(params[:api_key])
        
        # verify partner exists
        verify_partner(params[:key])
        
        # verify key and api_key belong to same partner
        verify_partner_key(params[:key], params[:api_key])
        
        # try and create service class
        return Partner::Automation::Service
    end
    
    def self.verify_api_key(key)
        #find the key
        apikey = ApiKey.find_by_key(key)
        #check if a partner_id is associated with this api key
        raise BadRequestException.new(INVALID_KEY) unless apikey.present?
        raise BadRequestException.new(INVALID_KEY) unless apikey.partner_id.present?

        #check if a partner
        partner = Partner.find_by_id(apikey.partner_id)
        raise BadRequestException.new(INVALID_KEY) unless partner.present?
    end
    
    def self.verify_partner(key)
        raise BadRequestException.new(INVALID_PARTNER_KEY) if key.nil? || key.empty?
        partner = Partner.find_by_partner_key(key)
        raise BadRequestException.new(INVALID_PARTNER_KEY) unless partner.present?
    end
    
    def self.verify_partner_key(key, api_key)
        apikey = ApiKey.find_by_key(api_key)
        partner_with_api_key = Partner.find_by_id(apikey.partner_id)
        partner_with_partner_key = Partner.find_by_partner_key(key)
        
        raise BadRequestException.new("INVALID CREDENTIALS - Partner API Key and Partner Route do not match") unless partner_with_api_key === partner_with_partner_key
    end

end