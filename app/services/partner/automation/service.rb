#require 'carmen'
require 'uri'
#include Carmen

class Partner::Automation::Service

    include Partner::Automation::Global::Constants

    def self.create_binder(params)
        # make sure all required fields are present
        verify_params(params)

        # make sure email is valid
        verify_email(params)

        # verify state and country before moving on to verifying duplicate
        params = verify_state_and_country(params)

        # verify that this binder has not been created before
        verify_duplicate(params)

        # find the partner
        partner = Partner.find_by_partner_key(params[:key])

        # find the admin
        user = partner.get_admin

        # check if template can be used
        verify_template(params, user)

        address2 = params[:property][:address2].present? ? params[:property][:address2] : "".freeze
        postalcode = params[:property][:postalcode].present? ? params[:property][:postalcode]: "".freeze
        binder_name = set_binder_name(params)

        # set the binder
        binder = {
            name: binder_name,
            create_method: params[:method],
            binder_template_id: params[:binder_template_id],
            property_attributes: {
                property_type: SINGLE_FAMILY,
                address1: params[:property][:address],
                address2: address2,
                city: params[:property][:city],
                state: params[:property][:state],
                zip: postalcode,
                country: params[:property][:country],
                county: params[:property][:county],
                apn: params[:property][:apn],
                year_built: params[:property][:year_built],
                sq_ft: params[:property][:sq_ft],
                acres: params[:property][:acres]
            }
        }

        binder = self.set_parameters(binder)

        # create the new binder
        binder = Binder.new(binder)
        binder.created_by = user.id

        if binder.save
            # create the homeowner
            homeowner = create_homeowner(params, binder)

            # create the transfer
            transfer = create_transfer(binder, homeowner, user)

            file = StringIO.new(params.to_json) #mimic a real upload file
            file.class.class_eval { attr_accessor :original_filename, :content_type } #add attr's that paperclip needs
            file.original_filename = "payload.json" #assign filename in way that paperclip likes
            file.content_type = "application/json" # you could set this manually aswell if needed e.g 'application/pdf'

            abs_file = binder.abs_files.new
            abs_file.payload = file
            abs_file.save!

            Binders::PendingAbsFilesJob.perform_async(abs_file.id)

            # return binder id, name and created at
            return {id: binder.id, name: binder.name, created_at: binder.created_at, acceptance_link: "#{Host.generate_host_path}/homeowners/#{transfer.access_token}/welcome?notify=email"}
        else
            raise UnprocessableException.new(binder)
        end
    end

    def self.verify_duplicate(params)
        address1 = params[:property][:address]
        address2 = params[:property][:address2].present? ? params[:property][:address2] : "".freeze
        city = params[:property][:city]
        state = params[:property][:state]
        postalcode = params[:property][:postalcode].present? ? params[:property][:postalcode]: "".freeze
        zip = params[:property][:zip].present? ? params[:property][:zip]: "".freeze
        country = params[:property][:country]
        binder_property = "#{address1} #{address2}, #{city}, #{state} #{postalcode} #{country}"

        # find the property
        properties = Binder::Property.where(:address1 => address1, :city => city, :zip => postalcode, :country => country, :state => state) + properties = Binder::Property.where(:address1 => address1, :city => city, :zip => zip, :country => country, :state => state)

        binder_ids = properties.pluck(:binder_id)

        params[:client][:email] = "" if params[:client][:email].nil?

        # find where the client could be an impending owner of a binder
        users = User.joins(:transfers_received)
            .where(:email => params[:client][:email].downcase)
            .where("transfers.receiver_id = users.id AND transfers.binder_id IN (?)", binder_ids)

        # raise an exception if property exists
        raise BadRequestException.new DUPLICATE_BINDER_PROPERTY % binder_property if properties.length > 0 and users.count > 0

        # find where the client could be an owner of a binder
        users = User.joins(:user_binders)
            .where(:email => params[:client][:email].downcase, :user_binders => {:binder_id => binder_ids})

        # raise an exception if property exists
        raise BadRequestException.new DUPLICATE_BINDER_PROPERTY % binder_property if properties.length > 0 and users.count > 0

        partner = Partner.find_by_partner_key(params[:key])
        return if partner.nil?

        # raise an exception if property exists and property is orphaned and property created in last 72 hours
        raise BadRequestException.new DUPLICATE_BINDER_PROPERTY % binder_property if partner.binders.where(:id => binder_ids, :created_at => 72.hours.ago..Time.now).exists?
    end

    def self.set_binder_name(params)
        name = NAME % [params[:client][:first], params[:client][:last]]
        if name.length > 50
            return "My Home"
        end
        return name
    end

    def self.verify_template(params, user)
        # check if template can be used
        template = BinderTemplate.find_by_id(params[:binder_template_id])
        raise BadRequestException.new TEMPLATE_ID_NOT_FOUND % params[:binder_template_id] if template.nil?

        # check if partner can use this template
        ability = Ability.new(user)
        raise BadRequestException.new TEMPLATE_NOT_AUTHORIZED % template.id unless ability.can?(:read, template)

        return template
    end

    def self.verify_params(params)
        # check if param is present
        raise BadRequestException.new CLIENT_REQUIRED unless params[:client].present?
        raise BadRequestException.new PROPERTY_REQUIRED unless params[:property].present?
        # check if all params are present
        raise BadRequestException.new FIRST_NAME_REQUIRED unless params[:client][:first].present?
        raise BadRequestException.new ADDRESS_REQUIRED unless params[:property][:address].present?
        raise BadRequestException.new CITY_REQUIRED unless params[:property][:city].present?
        raise BadRequestException.new STATE_REQUIRED unless params[:property][:state].present?
        raise BadRequestException.new COUNTRY_REQUIRED unless params[:property][:country].present?
        raise BadRequestException.new TEMPLATE_ID_REQUIRED unless params[:binder_template_id].present?

        # convert the date
        if params[:inspection_date].present?
            date = params[:inspection_date].gsub("/", "-")

            begin
                params[:inspection_date] = Date.strptime(date, "%m-%d-%Y")
            rescue
                params[:inspection_date] = nil
            end
        end
    end

    def self.verify_email(params)
        # raise an exception if client email is not valid
        params[:client][:email] = "orphans@homebinder.com" unless (params[:client][:email] =~ VALID_EMAIL_REGEX) == 0

        if params[:agent].present? and params[:agent][:email].present?
            # raise an exception if agent email is not valid
            params[:agent][:email] = nil unless (params[:agent][:email] =~ VALID_EMAIL_REGEX) == 0
        end
    end

    def self.verify_state_and_country(params)
        # check if country exists in Carmen
        ctry = params[:property][:country]
        country = Country.find_by_value(ctry)

        if country.nil?
            raise BadRequestException.new INCORRECT_COUNTRY % params[:property][:country]
        end

        # set the country
        params[:property][:country] = country.gec

        # check if state/province exists in Carmen
        ste = params[:property][:state]
        state = country.subdivision(ste)
        raise BadRequestException.new INCORRECT_STATE % params[:property][:state] unless state.present?

        # set the state
        params[:property][:state] = state
        # return params object
        return params
    end

    def self.add_components(binder_id, user_id, partner_id, params, homeowner_id)
        # create the buyer AND seller agent if present
        agent_options = {:buyer_agent => params[:buyer_agent], :seller_agent => params[:seller_agent], :agent => params[:agent]}
        self.delay(queue: 'abs').create_agents(binder_id, agent_options, partner_id, user_id)

        self.delay(queue: 'abs').create_partner_binder(partner_id, binder_id, homeowner_id, params[:partner_email])

        # add the binder branding
        self.delay(queue: 'abs').add_binder_branding(binder_id, partner_id, params[:partner_email])

        if !params[:inspection_report_url].blank?
            self.delay(queue: 'abs').download_inspection_report(binder_id, params[:inspection_report_url], partner_id)
        end

        self.delay(queue: 'abs').add_partner_user_as_home_pro(user_id, partner_id, binder_id)

        SubscriptionService.delay(queue: 'abs').create_subscription(binder_id, partner_id)

        # check zillow for property photos and add them
        Zillow::DownloadBinderDetailsJob.perform_async(binder_id)

        # create transactions
        transaction_options = {:inspection_date => params[:inspection_date], :transaction_type => params[:transaction_type], :project_number => params[:project_number]}
        self.delay(queue: 'abs').create_transaction(binder_id, transaction_options, partner_id)

        # add additional services
        self.delay(queue: 'abs').add_additional_services(binder_id, params[:inspection_date], user_id, partner_id, homeowner_id)

        # analytics
        Binders::BinderAnalytics.delay(queue: "analytics").created(binder_id)

        # call analytics
        Users::UserAnalytics.delay(:queue => "analytics").created(homeowner_id)
    end

    def self.create_partner_binder(partner_id, binder_id, homeowner_id, partner_user_email = nil)
        partner = Partner.find(partner_id)
        binder = Binder.find(binder_id)
        homeowner = User.find(homeowner_id)

        user = partner.users.where(:email => partner_user_email.downcase, :role => partner.partner_type).first unless partner_user_email.nil?
        if user.nil?
            # get the branding settings
            default_user_branding_id = partner.partner_configuration.default_user_branding_id
        else
            default_user_branding_id = user.user_profile.id
        end

        partner_user_id = PartnerUser.joins(:user => :user_profile).where(:user_profiles => {:id => default_user_branding_id}).ids.first || partner.partner_user_ids.first
        PartnerBinder.create(:partner_id => partner.id, :binder_id => binder.id, :client_id => homeowner.id, :role => PartnerBinderRole::BINDER, :partner_user_id => partner_user_id, :repair_pricer_enabled => partner.partner_configuration.repair_pricer_pre_paid_reports)
    end

    def self.add_partner_user_as_home_pro(user_id, partner_id, binder_id)
        user = User.find(user_id)
        partner = Partner.find(partner_id)
        binder = Binder.find(binder_id)

        cont = OpenStruct.new({
            name: partner.name,
            phone: partner.phone,
            email: partner.email.present? ? partner.email.downcase : nil,
            contractor_id: user.contractor.id,
            contact: partner.contact
        })

        if partner.partner_type === INSPECTOR
            type = Contractor::Category.find_or_create_by(:name => "Home Inspector")
            sub_types = type.sub_types.map { |sub_type| {:contractor_sub_category_id => sub_type.id} }

            self.delay_for(2.minutes, queue: 'abs').add_as_contractor(cont, user.id, binder.id, [{:contractor_category_id => type.id}], sub_types)
        else
            type = Contractor::Category.find_or_create_by(:name => "Real Estate")
            sub_types = type.sub_types.map { |sub_type| {:contractor_sub_category_id => sub_type.id} }

            self.delay_for(2.minutes, queue: 'abs').add_as_contractor(cont, user.id, binder.id, [{:contractor_category_id => type.id}], sub_types)
        end
    end

    def self.transfer_to_orphans(binder, owner, transfer, homeowner)
        if homeowner.email === "orphans@homebinder.com"
            # user
            parameters = {
                :id => transfer.id,
                :user => {
                    :first => homeowner.user_profile.first_name,
                    :last => homeowner.user_profile.last_name,
                    :email => homeowner.email,
                    :phone => homeowner.user_profile.mobile_phone
                },
                :transfer_type => "ownership"
            }

            begin
                Binder::Transfer.execute(owner, parameters)

                binder.status = "orphan"
                binder.save!
            rescue => e
                case e
                when UnprocessableException
                    ErrorService.perform_async(e.resource.errors.full_messages.first, {binder_id: binder.id})
                else
                    ErrorService.perform_async(e.message, {binder_id: binder.id})
                end
            end
        end
    end

    def self.create_homeowner(params, binder)
        if params[:transaction_type].present?
            case params[:transaction_type]
            when "BUY", "buy_side"
                create_method = "transfer"
            when "SELL", "sell_side"
                create_method = "sell_side_inspection"
            end
        else
            create_method = "transfer"
        end

        if params[:client][:email] != "orphans@homebinder.com"
            # get the binder property address
            address = {
                address1: binder.property.address1,
                address2: binder.property.address2,
                city: binder.property.city,
                state: binder.property.state,
                zip: binder.property.zip,
                country: binder.property.country
            }

            # create the homeowner
            homeowner = {
                :email => params[:client][:email].downcase,
                :password => SecureRandom.hex(4),
                :user_profile_attributes => {
                    :address_attributes => address,
                    :first_name => params[:client][:first],
                    :last_name => params[:client][:last],
                    :mobile_phone => PhonyRails.normalize_number(params[:client][:phone], country_code: 'US')
                },
                :role => "homeowner",
                :create_method => create_method,
                :created_by => binder.created_by
            }

            # check if homeowner already exists
            user = User.where(:email => homeowner[:email].downcase).first

            if user.nil?
                homeowner = User.new(homeowner)
                # need to create method to check for orphans and create if orphans does not exist
                if not homeowner.save
                    user = User.find_by_email("orphans@homebinder.com")

                    if user.nil?
                        # create orphans
                        homeowner = {
                            :email => "orphans@homebinder.com",
                            :password => SecureRandom.hex(4),
                            :user_profile_attributes => {
                                :first_name => "Orphans"
                            },
                            :role => "inspector",
                            :create_method => "register",
                        }

                        homeowner = User.new(homeowner)
                        homeowner.save

                        return homeowner
                    end

                    return user
                end
            else
                homeowner = user
            end

            return homeowner
        else
            user = User.find_by_email("orphans@homebinder.com")

            if user.nil?
                # create orphans
                homeowner = {
                    :email => "orphans@homebinder.com",
                    :password => SecureRandom.hex(4),
                    :user_profile_attributes => {
                        :first_name => "Orphans"
                    },
                    :role => "inspector",
                    :create_method => "register",
                }

                homeowner = User.new(homeowner)
                homeowner.save
                return homeowner
            end
            return user
        end
    end

    def self.create_transfer(binder, homeowner, user)
        # set up the hash
        transfer = {
            :sender_id => user.id,
            :receiver_id => homeowner.id,
            :binder_id => binder.id,
            :transfer_type => "ownership",
            :status => "created",
        }
        # create the transfer
        transfer = Binder::Transfer.new(transfer)
        transfer.save
        return transfer
    end

    # sets the binder owner
    # checks if the partner user email has a user, if not we check for the default user
    # if not then we use the partner admin

    def self.set_binder_owner(binder, partner, partner_user_email)
        # find the partner user by email
        user = partner.users.where(:email => partner_user_email.downcase, :role => partner.partner_type).first unless partner_user_email.nil?
        if user.nil?
            # get the default user branding ID
            profile = UserProfile.find_by_id(partner.partner_configuration.default_user_branding_id)
            if profile.nil?
                user = partner.get_admin
            else
                user = profile.user
            end
        end
        UserBinder.create(:user_id => user.id, :binder_id => binder.id, :role => "owner")
        return user
    end

    def self.add_transfer_note(binder_id, template_id)
        binder = Binder.find(binder_id)
        template = BinderTemplate.find(template_id)

        # add default transfer note to binder
        if binder.details.nil?
            binder.details = template.transfer_note
            binder.save
        end
    end

    def self.add_appliances(appliances, binder_id, template_id, user_id)
        if appliances and appliances.length > 0
            appliances.each do |app|
                app[:binder_id] = binder_id
                app = set_parameters(app)
                app = Binder::Appliance.new(app)

                if app.save
                    # do a recall check. The recall service determines if it's allowed
                    Recalls::DownloadJob.new.check_for_appliance_recall(app)
                else
                    ErrorService.perform_async(app.errors.full_messages.first, {binder_id: binder_id})
                end
            end
        else
            template = BinderTemplate.find(template_id)
            template.appliance_templates.each do |app|
                app = Binder::Appliance.new(:name => app.name, :created_by => user_id, :details => app.notes, :binder_id => binder_id, :library_source_id => app.id)

                if not app.save
                    ErrorService.perform_async(app.errors.full_messages.first, {binder_id: binder_id})
                else
                    # do a recall check. The recall service determines if it's allowed
                    Recalls::DownloadJob.new.check_for_appliance_recall(app)
                end
            end
        end
    end

    def self.add_maintenance_items(maintenance_items, binder_id, template_id, user_id)
        if maintenance_items and maintenance_items.length > 0
            maintenance_items.each do |item|
                item[:binder_id] = binder_id
                item = set_parameters(item)
                item = Binder::MaintenanceItem.new(item)

                if not item.save
                    ErrorService.perform_async(item.errors.full_messages.first, {binder_id: binder_id})
                end
            end
        else
            template = BinderTemplate.find(template_id)
            template.maintenance_templates.each do |item|
                if item.initial_due_date_interval.to_i > 0
                    item.due_date = Date.today + item.initial_due_date_interval
                end

                case item.frequency
                when Binder::MaintenanceItem::Cycle::AS_NEEDED.titleize
                    interval = 1
                    cycle = Binder::MaintenanceItem::Cycle::AS_NEEDED.titleize
                when Binder::MaintenanceItem::Cycle::ONCE
                    interval = 1
                    cycle = Binder::MaintenanceItem::Cycle::ONCE.capitalize
                when Binder::MaintenanceItem::Cycle::ANNUAL
                    interval = 1
                    cycle = Binder::MaintenanceItem::Cycle::YEARS.capitalize
                when Binder::MaintenanceItem::Cycle::SEMI_ANNUAL
                    interval = 6
                    cycle = Binder::MaintenanceItem::Cycle::MONTHS.capitalize
                when Binder::MaintenanceItem::Cycle::QUARTERLY
                    interval = 3
                    cycle = Binder::MaintenanceItem::Cycle::MONTHS.capitalize
                when Binder::MaintenanceItem::Cycle::MONTHLY
                    interval = 1
                    cycle = Binder::MaintenanceItem::Cycle::MONTHS.capitalize
                when Binder::MaintenanceItem::Cycle::EVERY_OTHER
                    interval = 2
                    cycle = Binder::MaintenanceItem::Cycle::YEARS.capitalize
                when Binder::MaintenanceItem::Cycle::EVERY_THREE
                    interval = 3
                    cycle = Binder::MaintenanceItem::Cycle::YEARS.capitalize
                when Binder::MaintenanceItem::Cycle::EVERY_FOUR
                    interval = 4
                    cycle = Binder::MaintenanceItem::Cycle::YEARS.capitalize
                when Binder::MaintenanceItem::Cycle::EVERY_FIVE
                    interval = 5
                    cycle = Binder::MaintenanceItem::Cycle::YEARS.capitalize
                when Binder::MaintenanceItem::Cycle::EVERY_TEN
                    interval = 10
                    cycle = Binder::MaintenanceItem::Cycle::YEARS.capitalize
                when Binder::MaintenanceItem::Cycle::EVERY_FOURTY
                    interval = 40
                    cycle = Binder::MaintenanceItem::Cycle::YEARS.capitalize
                end

                # create item
                maint = Binder::MaintenanceItem.new(:name => item.name, :created_by => user_id, :details => item.notes,
                                                    :binder_id => binder_id, :do_date => item.due_date, :maintenance_cycle => cycle, :interval => interval, :library_source_id => item.id)

                # save the maintenance item
                if maint.save
                    # update typeahead values
                    Typeahead.add_typeahead_values(maint)
                else
                    ErrorService.perform_async(maint.errors.full_messages.first, {binder_id: binder_id})
                end
            end
        end
    end

    def self.add_contractors(home_professionals, binder_id, template_id, user_id)
        if home_professionals and home_professionals.length > 0
            home_professionals.each do |pro|
                pro[:binder_id] = binder_id
                pro = set_parameters(pro)
                pro = Binder::BinderContractor.new(pro)

                if not pro.save
                    ErrorService.perform_async(pro.errors.full_messages.first, {binder_id: binder_id})
                end
            end
        else
            template = BinderTemplate.find(template_id)
            template.contractor_templates.each do |cont|
                binder_contractor = OpenStruct.new({
                    :binder_template_id => cont.binder_template_id,
                    :library_source_id => cont.id,
                    :secondary_details => cont.partner_contractor.secondary_notes,
                    :name => cont.partner_contractor.name,
                    :secondary_name => cont.partner_contractor.secondary_name,
                    :secondary_email => cont.partner_contractor.secondary_email,
                    :secondary_phone => cont.partner_contractor.secondary_phone,
                    :notes => cont.partner_contractor.notes,
                    :contractor_id => cont.partner_contractor.contractor_id
                })

                types = cont.partner_contractor.types.map { |type| {:contractor_category_id => type.id} }
                sub_types = cont.partner_contractor.sub_types.map { |sub_type| {:contractor_sub_category_id => sub_type.id} }

                self.delay_for(2.minutes, queue: 'abs').add_as_contractor(binder_contractor, user_id, binder_id, types, sub_types)
            end
        end
    end

    def self.add_projects(projects, binder_id)
        if projects
            projects.each do |project|
                project[:binder_id] = binder_id
                project = set_parameters(project)
                project = Binder::Project.new(project)

                if not project.save
                    ErrorService.perform_async(project.errors.full_messages.first, {binder_id: binder_id})
                end
            end
        end
    end

    def self.add_template_docs(binder_id, template_id)
        template = BinderTemplate.find(template_id)

        template.document_templates.each do |docu|
            notes = docu.notes.present? ? docu.notes : ""
            document = Binder::Document.new(:binder_id => binder_id, :file => docu.file, :details => notes, :library_source_id => docu.id)
            # save the document
            if not document.save
                ErrorService.perform_async(document.errors.full_messages.first, {binder_id: binder_id})
            end
        end
    end

    # verify that the buyer AND/OR seller agent are present
    # then add them to the binder
    def self.create_agents(binder_id, agent_options, partner_id, user_id)
        # first we check if there is a buyer_agent
        if agent_options[:buyer_agent].present? && agent_options[:buyer_agent][:email].present?
            agent_options[:buyer_agent][:role] = "buyer_agent"
            self.delay_for(2.minutes, queue: 'abs').create_agent(binder_id, agent_options[:buyer_agent], partner_id, user_id)
        end
        # then we check if there is a sellerc_agent
        if agent_options[:seller_agent].present? and agent_options[:seller_agent][:email].present?
            agent_options[:seller_agent][:role] = "seller_agent"
            self.delay_for(2.minutes, queue: 'abs').create_agent(binder_id, agent_options[:seller_agent], partner_id, user_id)
        end

        if agent_options[:agent]
            agent = agent_options[:agent]
            # check if agent name is available and set name/contact accordingly

            if agent[:name].present? || agent[:contact].present?
                if agent[:name] === false
                    agent[:name] = ""
                end

                if agent[:contact] === false
                    agent[:contact] = ""
                end

                if agent[:phone].present? and agent[:phone].downcase === NOT_PROVIDED.downcase
                    agent[:phone] = nil
                end

                if agent[:contact].downcase === NOT_PROVIDED.downcase
                    agent[:contact] = agent[:name]
                end

                if agent[:contact].downcase === NOT_PROVIDED.downcase and agent[:name].downcase === NOT_PROVIDED.downcase
                    return
                end

                if agent[:name].empty? and agent[:contact].present?
                    agent[:name] = agent[:contact]
                end

                if agent[:contact].empty? and agent[:name].present?
                    agent[:contact] = agent[:name]
                end

                full_name = agent[:contact].split
                first_name = full_name.first
                last_name = full_name.last

                agent[:first] = first_name
                agent[:last] = last_name

                return if agent[:email].nil?
                return if agent[:email].empty?

                self.delay_for(2.minutes, queue: 'abs').create_agent(binder_id, agent, partner_id, user_id)
            end
        end
    end

    def self.create_agent(binder_id, agent, partner_id, user_id)
        user = User.find(user_id)

        agent[:phone] = PhonyRails.normalize_number(agent[:phone], country_code: 'US')
        agent[:phone] = Phony.plausible?(agent[:phone]) ? agent[:phone] : nil
        address = {
            :address1 => agent[:address1],
            :city => agent[:city],
            :state => agent[:state],
            :zip => agent[:postalcode],
            :country => agent[:country]
        }
        
        user_agent = {
            :email => Partner::Automation::EmailParser.new.format_value(["", "email"], agent[:email].downcase.gsub(/\s+/, "")),
            :user_profile_attributes => {
                :address_attributes => address,
                :first_name => agent[:first],
                :last_name => agent[:last],
                :mobile_phone => agent[:phone],
                :company => agent[:name]
            },
            :role => "agent",
            :create_method => user.role
        }

        user_agent = ActionController::Parameters.new(user_agent)
        user_agent = User.build(HBRequest.new.create_request(user),{:user => user_agent, :partnerId => partner_id, :role => "agent"})
        role = agent[:role].present? ? agent[:role] : "buyer_agent"

        if role === "buyer_agent"
            UserBinder.create(user_id: user_agent.id, binder_id: binder_id, role: role, access_repair_pricer: PartnerUser.where(:user_id => user_agent.id, :partner_id => partner_id, :access_repair_pricer => true).exists?)
        else
            UserBinder.create(user_id: user_agent.id, binder_id: binder_id, role: role)
        end

        add_agent_headshot_logo(agent, user_agent)

        self.delay(queue: 'abs').add_agent_as_home_pro(user_agent.id, binder_id, user_id)
    end
    
    def self.add_agent_as_home_pro(agent_id, binder_id, user_id)
        agent = User.find(agent_id)
        cont = OpenStruct.new({
            name: agent.user_profile.display_name,
            phone: agent.user_profile.mobile_phone,
            email: agent.email,
            contractor_id: agent.contractor.id,
            contact: agent.user_profile.display_name
        })

        type = Contractor::Category.find_or_create_by(:name => "Real Estate")
        sub_types = type.sub_types.map { |sub_type| {:contractor_sub_category_id => sub_type.id} }

        self.delay_for(2.minutes, queue: 'abs').add_as_contractor(cont, user_id, binder_id, [{:contractor_category_id => type.id}], sub_types)
    end

    # adds the headshot/logo to any agents user profile
    # agent is the parameter object from the API/email, user_agent
    # is the user account that was created

    def self.add_agent_headshot_logo(agent, user_agent)
        # add headshot
        if agent[:headshot].present? and agent[:headshot][:file].present?
            user_agent.user_profile.head_shot = Paperclip.io_adapters.for(agent[:headshot][:file])
            user_agent.user_profile.save
        elsif agent[:headshot].present? and agent[:headshot][:url].present? and !agent[:headshot][:url].empty?
            user_agent.user_profile.head_shot = image_url(agent[:headshot][:url])
            user_agent.user_profile.save
        end

        # add logo
        if agent[:logo].present? and agent[:logo][:file].present?
            user_agent.user_profile.logo = Paperclip.io_adapters.for(agent[:logo][:file])
            user_agent.user_profile.save
        elsif agent[:logo].present? and agent[:logo][:url].present? and !agent[:logo][:url].empty?
            user_agent.user_profile.logo = image_url(agent[:logo][:url])
            user_agent.user_profile.save
        end
    end

    # takes a URL to an image and either downloads the file
    # or returns nil if the image cannot be downloaded

    def self.image_url(url)
        return nil if url === "https://dlil96nns7nd5.cloudfront.net/images/agency-no-logo.gif"
        begin
            return Paperclip.io_adapters.for(url)
        rescue => e
            ErrorService.perform_async(e.message, {url: url})
            return nil
        end
    end

    # may be used several times in this class
    def self.add_as_contractor(cont, user_id, binder_id, types, sub_types)
        user = User.find(user_id)
        binder = Binder.find(binder_id)

        contractor = {
            :contact => cont.dig(:name),
            :contractor_id => cont.dig(:contractor_id),
            :binder_id => binder.id,
            :details => cont[:notes],
            :binder_contractor_types_attributes => types,
            :binder_contractor_sub_types_attributes => sub_types,
            :library_source_id => cont.dig(:library_source_id),
            :secondary_name => cont.dig(:secondary_name),
            :secondary_email => cont.dig(:secondary_email),
            :secondary_phone => cont.dig(:secondary_phone),
            :secondary_details => cont.dig(:secondary_notes),
            :user_contractor_id => cont.dig(:user_contractor_id)
        }

        contractor = ActionController::Parameters.new(contractor)
        Binder::BinderContractor.build(HBRequest.new.create_request(user),{:binder_contractor => contractor})
    end

    #we override the create_documents method because documents come through differently in the API
    def self.create_documents(binder_id, documents)
        return if documents.nil? || documents.empty?

        # get existing documents to prevent creation of duplicate documents
        current_documents = Binder::Document.where(:binder_id => binder_id)

        # iterate through the attached documents
        documents.each do |doc|
            if doc.present?
                # make sure we don't create a new document with the same filename as an existing document
                if !current_documents.where(:file_file_name => doc[:name]).exists?
                    document_type = DocumentType.find_or_create_by(:name => doc[:document_type])
                    document = Binder::Document.new(binder_id: binder_id, document_type: document_type)

                    if doc[:file]
                        file = Paperclip.io_adapters.for(doc[:file])
                        extension = Rack::Mime::MIME_TYPES.invert[file.content_type]
                        
                        if !doc[:name]
                            doc[:name] = Time.now.to_i.to_s + extension
                        else
                            doc[:name] = doc[:name].split(".").first + extension
                        end
                        
                        file.original_filename = doc[:name]
                        document.file = file
                    elsif doc[:url]
                        encoded_url = URI.encode(doc[:url])
                        document.file = URI.parse(encoded_url)
                        document.file_file_name = doc[:name]
                        document.url = doc[:url]
                        document.save!
                    end

                    if not document.save
                        ErrorService.perform_async(document.errors.full_messages.first, {binder_id: binder_id, file_file_name: document.file_file_name})
                    end
                end
            end
        end
    end

    def self.download_inspection_report(binder_id, url, partner_id)
        binder = Binder.find(binder_id)
        partner = Partner.find(partner_id)
        partner.partner_configuration.isn_report_url = true
        partner.partner_configuration.save!
        
        encoded = URI.encode(url)
        page = Nokogiri::HTML(open(encoded))
        page.css('a.btn.btn-default.btn-sm').each do |button|
            document = binder.documents.new
            encoded_url = URI.encode(button['href'])

            if !encoded_url.include?("http://inspectionsupport.com") &&
               !encoded_url.include?("http://goisn.net") &&
               !encoded_url.include?("https://4isn.com")
                encoded_url = "https://inspectionsupport.com" + encoded_url
            end

            document.file = URI.parse(encoded_url)
            document.url = url
            document.document_type = DocumentType.find_or_create_by(:name => "Home Inspection Report")
            document.save!
        end
    rescue => e
        ErrorService.perform_async(e.message, {url: url, binder_id: binder_id})
    end

    def self.create_images(binder_id, params)
        # create the property/hero photo
        if params[:property_photo].present?
            create_image(binder_id, params[:property_photo])
        end

        # create any additional images
        if params[:additional_images] and params[:additional_images].length > 0
            params[:additional_images].each do |image|
                create_image(binder_id, image)
            end
        end
    end

    def self.create_image(binder_id, image)
        if image.present? and image[:file].present?
            # create the image
            binder_image = Binder::Image.new(binder_id: binder_id)

            file = Paperclip.io_adapters.for(image[:file])
            extension = Rack::Mime::MIME_TYPES.invert[file.content_type]
            
            if !image[:name]
                image[:name] = Time.now.to_i.to_s + extension
            else
                image[:name] = image[:name].split(".").first + extension
            end
            
            file.original_filename = image[:name]
            binder_image.file = file
            # save
            if not binder_image.save
                ErrorService.perform_async(binder_image.errors.full_messages.first, {binder_id: binder_id})
                # return since the image wasn't saved
                return
            end
            Binder.where(:id => binder_id, :hero_image_id => nil).update_all(:hero_image_id => binder_image.id)
        elsif image.present? and image[:url].present?
            return if image[:url] === "https://s3.amazonaws.com/isn-cdn/images/no-propertyfound.png"

            image = Binder::Image.new(binder_id: binder_id, file: image_url(image[:url]))

            if not image.save
                ErrorService.perform_async(image.errors.full_messages.first, {binder_id: binder_id})
                return
            end

            Binder.where(:id => binder_id, :hero_image_id => nil).update_all(:hero_image_id => image.id)
        end
    end

    # add the binder brandings
    # make sure to add the partner_id as a fallback option in case user is not found
    def self.add_binder_branding(binder_id, partner_id, partner_user_email)
        partner = Partner.find(partner_id)
        binder = Binder.find(binder_id)

        user = partner.users.where(:email => partner_user_email.downcase, :role => partner.partner_type).first unless partner_user_email.nil?
        if user.nil?
            # get the branding settings
            default_user_branding_id = partner.partner_configuration.default_user_branding_id
            return unless default_user_branding_id.present?
        else
            default_user_branding_id = user.user_profile.id
        end

        # get the default option
        default_user_branding_option = partner.partner_configuration.default_user_branding_option

        case default_user_branding_option
        when "user"
            # add the user
            binder.binder_brandings.create(:user_branding_id => default_user_branding_id, :partner_id => partner.id, :scope => "binder")
            binder.binder_brandings.create(:user_branding_id => default_user_branding_id, :partner_id => partner.id, :scope => "recall_email")
            binder.binder_brandings.create(:user_branding_id => default_user_branding_id, :partner_id => partner.id, :scope => "maintenance_email")
            binder.binder_brandings.create(:user_branding_id => default_user_branding_id, :scope => "seller_report") unless partner.partner_type === "inspector"
        when "agent"
            # find the agent
            agent_binder = UserBinder.where(:binder_id => binder.id, :role => "buyer_agent").first
            if (agent_binder && agent_binder.user && agent_binder.user.user_profile)
                # set the ID
                agent_user_branding_id = agent_binder.user.user_profile.id
                # create the brandings
                binder.binder_brandings.create(:user_branding_id => agent_user_branding_id, :scope => "binder")
                binder.binder_brandings.create(:user_branding_id => agent_user_branding_id, :scope => "recall_email")
                binder.binder_brandings.create(:user_branding_id => agent_user_branding_id, :scope => "maintenance_email")
                binder.binder_brandings.create(:user_branding_id => agent_user_branding_id, :scope => "seller_report")
            end
        when "co_brand"
            # add the user
            binder.binder_brandings.create(:user_branding_id => default_user_branding_id, :partner_id => partner.id, :scope => "binder")
            binder.binder_brandings.create(:user_branding_id => default_user_branding_id, :partner_id => partner.id, :scope => "recall_email")
            binder.binder_brandings.create(:user_branding_id => default_user_branding_id, :partner_id => partner.id, :scope => "maintenance_email")
            # find the agent
            agent_binder = UserBinder.where(:binder_id => binder.id, :role => "buyer_agent").first
            if (agent_binder && agent_binder.user && agent_binder.user.user_profile)
                # set the ID
                agent_user_branding_id = agent_binder.user.user_profile.id
                # create the brandings
                binder.binder_brandings.create(:user_branding_id => agent_user_branding_id, :scope => "recall_email")
                binder.binder_brandings.create(:user_branding_id => agent_user_branding_id, :scope => "maintenance_email")
                binder.binder_brandings.create(:user_branding_id => agent_user_branding_id, :scope => "seller_report")
            end
        when "none"
            # do nothing
        else
            # do nothing - catch all in case
        end

        # add transfer and share email scopes <- they are not dependent on the default_user_branding_option
        binder.binder_brandings.create(:user_branding_id => default_user_branding_id, :partner_id => partner.id, :scope => "share_email")
        binder.binder_brandings.create(:user_branding_id => default_user_branding_id, :partner_id => partner.id, :scope => "transfer_email")
    end

    def self.set_parameters(object)
        parameters = ActionController::Parameters.new(object.to_h)
        parameters.permit!
        return parameters
    end

    def self.create_transaction(binder_id, transaction_options = {}, partner_id)
        binder = Binder.find(binder_id)
        partner = Partner.find(partner_id)

        if transaction_options[:transaction_type].present?
            case transaction_options[:transaction_type].downcase
            when "buy", "buy_side"
                transaction_type = "buy_side_inspection"
            when "sell", "sell_side"
                transaction_type = "sell_side_inspection"
            end
        else
            transaction_type = "buy_side_inspection"
        end

        transaction = Binder::Transaction.new(:binder_id => binder.id, :transaction_type => transaction_type, :tracking_number => transaction_options[:project_number], :transaction_date => transaction_options[:inspection_date])
        transaction.save

        # connect transaction to partner
        PartnerBinderTransaction.create(:partner_id => partner.id, :binder_transaction_id => transaction.id)
    end

    def self.add_additional_services(binder_id, inspection_date, user_id, partner_id, homeowner_id)
        user = User.find(user_id)
        partner = Partner.find(partner_id)
        homeowner = User.find(homeowner_id)
        binder = Binder.find(binder_id)

        return if partner.warranty_configuration.nil?
        return if partner.warranty_configuration.automatic_purchases === false

        if inspection_date.nil?
            inspection_date = Date.today
        end

        homeowner = {
            id: homeowner.id,
            firstName: homeowner.user_profile.first_name || homeowner.user_profile.display_name,
            lastName: homeowner.user_profile.last_name || homeowner.user_profile.display_name,
            email: homeowner.email,
            phone: homeowner.user_profile.mobile_phone.present? ? homeowner.user_profile.mobile_phone : "222-222-2222"
        }

        parameters = {
            order: {
                :binder_id => binder.id,
                :warranty_company_id => partner.warranty_configuration.warranty_company_id,
                :warranty_plan_id => partner.warranty_configuration.warranty_plan_id,
                :account_number => partner.warranty_configuration.account_number,
                :price => partner.partner_warranty_plans.where(:warranty_plan_id => partner.warranty_configuration.warranty_plan_id).first.price,
                :homeowner => homeowner,
                :property => binder.property,
                :inspection_date => inspection_date
            }
        }

        order = set_parameters(parameters)

        # create the warranty
        Warranties::Configurations::Service.new(partner.warranty_configuration.id).order(user, order)
    end

    def self.reprocess_emails(params)
        address1 = params[:property][:address]
        city = params[:property][:city]
        state = params[:property][:state]
        postalcode = params[:property][:postalcode].present? ? params[:property][:postalcode]: "".freeze
        country = params[:property][:country]

        # find the property
        property = Binder::Property.where(:address1 => address1, :city => city, :zip => postalcode, :country => country, :state => state).first

        if property.present?
            binder = property.binder

            if params[:documents].present? and params[:documents].length > 0
                create_documents(binder.id, params[:documents])
            end

            create_images(binder.id, params)
        end
    end
end