class Partner::Automation::API::Service < Partner::Automation::Service

    def self.create_binder(params)
        params[:method] = params[:method] || "api"
        # analytics
        EventService.perform_async({:event_name => "abs_api_request", :event_type => "abs", :partner_key => params[:key]})
        super(params)
    end

    def self.set_parameters(object)
        if object.is_a?(ActionController::Parameters)
            return object.permit!
        end
        parameters = ActionController::Parameters.new(object)
        parameters.permit!
        return parameters.to_unsafe_h
    end
end
