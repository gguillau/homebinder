module Binders
    class BinderAnalytics
        BINDER_TRANSFER = "binder-transfer".freeze
        BINDER_SHARE = "binder-share".freeze

        def self.created(binder_id)
            Salesforce::Binder.new.new_binder(binder_id)
        end

        def self.updated(binder_id)
            Salesforce::Binder.new.update_binder(binder_id)
        end

        def self.transfer(transfer_id, partner_id = nil)
            transfer = Binder::Transfer.find_by_id(transfer_id)
            partner = Partner.find_by_id(partner_id)
            
            return if transfer.nil?
            return if transfer.binder.nil?
            return if transfer.binder.property.nil?
            return if transfer.sender.nil?
            return if transfer.receiver.nil?
            
            binder = transfer.binder
            metadata = create_binder_metadata(binder, partner)
            metadata[:from] = transfer.sender.email
            metadata[:to] = transfer.receiver.email
            metadata[:subject] = "HomeBinder for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}"
            metadata[:transfer_link] = "#{Host.path}/homeowners/#{transfer.access_token}/welcome"
            intercom = Intercom::HomeBinderClient.new.intercom
            
            begin
                intercom.events.create(
                    event_name: BINDER_TRANSFER,
                    email: transfer.receiver.email,
                    created_at: Time.now,
                    metadata: metadata
                )
            rescue
            end
            
            updated(binder.id)
        end

        def self.share(share_id, partner_id = nil)
            share = Binder::Share.find_by_id(share_id)
            return if share.nil?
            
            partner = Partner.find_by_id(partner_id)
            
            binder = share.binder
            metadata = create_binder_metadata(binder, partner)
            metadata[:by_user] = share.sender.email
            metadata[:with_user] = share.receiver.email
            metadata[:share_link] = "#{Host.path}/homeowners/#{share.access_token}/welcome"
            
            intercom = Intercom::HomeBinderClient.new.intercom
            
            begin
                intercom.events.create(
                    event_name: BINDER_SHARE,
                    email: share.sender.email,
                    created_at: Time.now,
                    metadata: metadata
                )
            rescue
            end
            
            updated(binder.id)
        end
        
        def self.create_binder_metadata(binder, partner)
            metadata = {
                binder_name: binder.name,
                binder_address: "#{binder.property.address1} #{binder.property.address2} #{binder.property.city}, #{binder.property.state} #{binder.property.zip}",
                created_at_date: Time.now
            }

            hero = binder.images.where(:id => binder.hero_image_id).first
            if hero
                metadata[:hero_link] = S3FileService.url_for(hero)
            end

            if partner
                metadata[:partner_name] = partner.name
                metadata[:partner_type] = partner.partner_type
                metadata[:partner_email] = partner.email
                metadata[:parnter_contact] = partner.contact
                if partner.logo.present?
                    metadata[:logo_link] = partner.logo.expiring_cloud_front_url(Rails.application.config.access_tokens[:week_long_token_refresh_interval], :original)
                end
            end

            metadata
        end
    end
end
