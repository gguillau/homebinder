require 'intercom'

class Intercom::HomeBinderClient

    INTERCOM_APPID = "INTERCOM_APPID".freeze
    INTERCOM_KEY = "INTERCOM_KEY".freeze
    INTERCOM_TEST_TOKEN = "INTERCOM_TEST_TOKEN".freeze
    INTERCOM_PRODUCTION_TOKEN = "INTERCOM_PRODUCTION_TOKEN".freeze

    attr_reader :intercom

    def initialize
        @intercom = Intercom::Client.new(token: token)
    end

    def metadata_for_partner(partner)
        # add the metadata from the partner object
        metadata = {
            type: partner.partner_type,
            contact: partner.contact,
            contact_email: partner.email,
            code: partner.code,
            route: partner.partner_key
        }

        # if there is an API key add it
        api_key = ApiKey.where(partner_id: partner.id).first
        if api_key
            metadata[:api_key] = api_key.key
        end

        # if the account has been created add the account information
        if partner.account
            metadata[:account_name] = partner.account.name
            metadata[:account_number] = partner.account.account_number
            metadata[:account_status] = partner.account.account_status
            metadata[:account_sub_status] = partner.account.account_sub_status
            metadata[:account_type] = partner.account.account_type
            metadata[:account_sub_type] = partner.account.account_sub_type
            metadata[:account_payment_type] = partner.account.payment_type
            metadata[:account_billing_frequency] = partner.account.billing_frequency
            metadata[:account_trial_expiration] = partner.account.trial_expiration
            metadata[:account_billing_activation] = partner.account.billing_activation
            metadata[:account_subscription_amount] = partner.account.subscription_amount.to_s
            metadata[:account_transaction_amount] = partner.account.transaction_amount.to_s
        end

        # add configuration info
        if partner.partner_configuration
            metadata[:default_binder_action] = partner.partner_configuration.default_binder_action
            metadata[:automation_binder_action] = partner.partner_configuration.automation_binder_action
            metadata[:binder_creation_delay] = partner.partner_configuration.binder_action_delay_length
        end

        # add binder data
        begin
            metrics = Partner.get_weekly_metrics(partner.id)
            metadata[:binders_last_7_days] = metrics[:total_binders_past_week]
            metadata[:binders_last_month] = metrics[:total_binders_past_month]
            metadata[:total_binders] = metrics[:total_binders]
        rescue
        end

        metadata
    end

    # returns the intercom token and raises an error if the environment variable is not set

    def token
        raise BadRequestException.new "Intercom Token Required" unless ENV[get_token_name].present?
        return ENV[get_token_name]
    end

    def get_token_name
        if Host.path === Host::PRODUCTION_HOST
            return INTERCOM_PRODUCTION_TOKEN
        else
            return INTERCOM_TEST_TOKEN
        end
    end
end
