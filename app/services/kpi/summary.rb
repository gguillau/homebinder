class Kpi::Summary
    
    include Kpi::Users
    include Kpi::Binders
    include Kpi::Partners
    include Kpi::Homeowners
    include Kpi::Global::Constants
    
    def initialize(response)
        @current_user = response.user
        @ability = response.ability
        @role = response.role
    end
    
    def get_summary
        # only admins can do this
        raise CanCan::AccessDenied.new("You are not authorized to access KPI Reports") unless @current_user.role === "admin"
        return {
            total_acv: total_acv,
            homeowners: {
                interaction: get_homeowner_interaction,
                acceptance: homeowner_acceptance_rate_by_partner_type(INSPECTOR)
            },
            inspectors: {
                total_binders: partner_binders_created_in_last_seven(INSPECTOR),
                total_inspectors_with_binders: total_with_binders_by_paid_customers(INSPECTOR),
                retention_rate: retention_rate(INSPECTOR)
            }
        }
    end
    
    def get_binder_totals
        # only admins can do this
        raise CanCan::AccessDenied.new("You are not authorized to access KPI Reports") unless @current_user.role === "admin"
        return get_binder_numbers
    end
    
    def get_user_totals
        # only admins can do this
        raise CanCan::AccessDenied.new("You are not authorized to access KPI Reports") unless @current_user.role === "admin"
        return get_user_numbers
    end
    
    def get_engagement(params)
        # only admins can do this
        raise CanCan::AccessDenied.new("You are not authorized to access KPI Reports") unless @current_user.role === "admin"
        raise BadRequestException unless params[:partner_type]
        return {
                binders: binders_created_by_partner_type_owned_by_homeowners(params[:partner_type]),
                logins: users_with_binders_created_by_partner_type(params[:partner_type]),
        }
    end
    
    def report
        KpiMailer.send_kpi_metrics(@current_user.id).deliver_later
    end

end