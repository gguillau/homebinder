module Kpi::Binders
    
    include Kpi::Global::Constants
    include Kpi::Global::Methods
    
    # get all the numbers
    def get_binder_numbers
        # global variables
        @total = Binder.all.count
        @float = @total
        # return the data
        return {
            all: get_all_binders,
            partners: get_partner_binders,
            homeowner: get_homeowner_binders,
            inspectors: get_binders_by_partner_type(INSPECTOR),
            agents: get_binders_by_partner_type(BROKER),
            admin: get_admin_binders,
            orphan: get_orphaned_binders
        }
    end
    
    # total binders
    def get_all_binders
        @seven = Binder.where(created_at: 7.days.ago..Time.now).count
        @thirty = Binder.where(created_at: 30.days.ago..Time.now).count
        @ninety = Binder.where(created_at: 3.months.ago..Time.now).count
        @half_year = Binder.where(created_at: 6.months.ago..Time.now).count
        @year = Binder.where(created_at: 1.year.ago..Time.now).count
        
        return {
            last_seven: {count: @seven, percentage: 1},
            last_thirty: {count: @thirty, percentage: 1},
            last_ninety: {count:(@ninety/3), percentage: 1},
            half_year: {count:(@half_year/6), percentage: 1},
            year: {count: (@year/12), percentage: 1},
            total: {count: @total, percentage: 1}
        }
    end

    # total binders created by homeowners
    def get_homeowner_binders
        # get the homeowner IDs
        ids = homeowners
        # queries
        total = Binder.where(:created_by => ids).count
        seven = Binder.where(created_at: 7.days.ago..Time.now).where(:created_by => ids).count
        thirty = Binder.where(created_at: 30.days.ago..Time.now).where(:created_by => ids).count
        ninety = (Binder.where(created_at: 3.months.ago..Time.now).where(:created_by => ids).count/3)
        half_year = (Binder.where(created_at: 6.months.ago..Time.now).where(:created_by => ids).count/6)
        year = (Binder.where(created_at: 1.year.ago..Time.now).where(:created_by => ids).count/12)
        
        
        return {
            last_seven: {count: seven, percentage: percent(seven.to_f/@seven.to_f)},
            last_thirty: {count: thirty, percentage: percent(thirty.to_f/@thirty.to_f)},
            last_ninety: {count: ninety, percentage: percent(ninety.to_f/(@ninety.to_f/3))},
            half_year: {count:  half_year, percentage: percent(half_year.to_f/(@half_year.to_f/6))},
            year: {count:  year, percentage: percent(year.to_f/(@year.to_f/12))},
            total: {count: total, percentage: percent(total.to_f/@float)}
        }
    end

    # total binders created by partners
    def get_partner_binders
        # get the partner IDS
        ids = partner_clause
        # global variables are used in other methods
        @partner_total = Binder.where(ids).count
        @partner_seven = Binder.where(created_at: 7.days.ago..Time.now).where(ids).count
        @partner_thirty = Binder.where(created_at: 30.days.ago..Time.now).where(ids).count
        @partner_ninety = (Binder.where(created_at: 3.months.ago..Time.now).where(ids).count/3)
        @partner_half_year = (Binder.where(created_at: 6.months.ago..Time.now).where(ids).count/6)
        @partner_year = (Binder.where(created_at: 1.year.ago..Time.now).where(ids).count/12)
        
        return {
            last_seven: {count: @partner_seven, percentage: percent(@partner_seven.to_f/@seven.to_f)},
            last_thirty: {count: @partner_thirty, percentage: percent(@partner_thirty.to_f/@thirty.to_f)},
            last_ninety: {count: @partner_ninety, percentage: percent(@partner_ninety.to_f/(@ninety.to_f/3))},
            half_year: {count:  @partner_half_year, percentage: percent(@partner_half_year.to_f/(@half_year.to_f/6))},
            year: {count:  @partner_year, percentage: percent(@partner_year.to_f/(@year.to_f/12))},
            total: {count: @partner_total, percentage: percent(@partner_total.to_f/@float)}
        }
    end
    
    # total binders created by a specific partner type
    def get_binders_by_partner_type(partner_type)
        clause = get_clause(partner_type)
        get_partner_binders
        
        total = Binder.where(clause).count
        seven = Binder.where(created_at: 7.days.ago..Time.now).where(clause).count
        thirty = Binder.where(created_at: 30.days.ago..Time.now).where(clause).count
        ninety = (Binder.where(created_at: 3.months.ago..Time.now).where(clause).count/3)
        half_year = (Binder.where(created_at: 6.months.ago..Time.now).where(clause).count/6)
        year = (Binder.where(created_at: 1.year.ago..Time.now).where(clause).count/12)
        
        return {
            last_seven: {count: seven, percentage: percent(seven.to_f/@partner_seven.to_f)},
            last_thirty: {count: thirty, percentage: percent(thirty.to_f/@partner_thirty.to_f)},
            last_ninety: {count: ninety, percentage: percent(ninety.to_f/@partner_ninety.to_f)},
            half_year: {count:  half_year, percentage: percent(half_year.to_f/@partner_half_year.to_f)},
            year: {count:  year, percentage: percent(year.to_f/@partner_year.to_f)},
            total: {count: total, percentage: percent(total.to_f/@partner_total.to_f)}#,
        }
    end

    # total binders created by admins
    def get_admin_binders
        # get the admin IDS
        where = admin_clause
        # queries
        total = Binder.where(where).count
        seven = Binder.where(created_at: 7.days.ago..Time.now).where(where).count
        thirty = Binder.where(created_at: 30.days.ago..Time.now).where(where).count
        ninety = (Binder.where(created_at: 3.months.ago..Time.now).where(where).count/3)
        half_year = (Binder.where(created_at: 6.months.ago..Time.now).where(where).count/6)
        year = (Binder.where(created_at: 1.year.ago..Time.now).where(where).count/12)
        
        
        return {
            last_seven: {count: seven, percentage: percent(seven.to_f/@seven.to_f)},
            last_thirty: {count: thirty, percentage: percent(thirty.to_f/@thirty.to_f)},
            last_ninety: {count: ninety, percentage: percent(ninety.to_f/(@ninety.to_f/3))},
            half_year: {count:  half_year, percentage: percent(half_year.to_f/(@half_year.to_f/6))},
            year: {count:  year, percentage: percent(year.to_f/(@year.to_f/12))},
            total: {count: total, percentage: percent(total.to_f/@float)}
        }
    end
    
    # total binders that have no owners
    def get_orphaned_binders
        total = Binder.includes(:users).where(orphan_clause).count
        seven = Binder.includes(:users).where(created_at: 7.days.ago..Time.now).where(orphan_clause).count
        thirty = Binder.includes(:users).where(created_at: 30.days.ago..Time.now).where(orphan_clause).count
        ninety = (Binder.includes(:users).where(created_at: 3.months.ago..Time.now).where(orphan_clause).count/3)
        half_year = (Binder.includes(:users).where(created_at: 6.months.ago..Time.now).where(orphan_clause).count/6)
        year = (Binder.includes(:users).where(created_at: 1.year.ago..Time.now).where(orphan_clause).count/12)
        
        
        return {
            last_seven: {count: seven, percentage: percent(seven.to_f/@seven.to_f)},
            last_thirty: {count: thirty, percentage: percent(thirty.to_f/@thirty.to_f)},
            last_ninety: {count: ninety, percentage: percent(ninety.to_f/(@ninety.to_f/3))},
            half_year: {count:  half_year, percentage: percent(half_year.to_f/(@half_year.to_f/6))},
            year: {count:  year, percentage: percent(year.to_f/(@year.to_f/12))},
            total: {count: total, percentage: percent(total.to_f/@float)}
        }
    end
    
end