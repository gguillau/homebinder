module Kpi::Global::Constants
        
    TRUE = "true".freeze    
    FALSE = "false".freeze
    PARTNER = "Partner".freeze
    BROKER = "broker".freeze  
    INSPECTOR = "inspector".freeze  
    ID = "id".freeze  
    CREATED_METHOD = "users.create_method != ?".freeze, "share".freeze  
    ID_IN = "id IN (?)".freeze  
    ANNUAL = "annual".freeze
    MONTHLY = "monthly".freeze
    CREATED_BY = "created_by".freeze  
    CREATED_BY_LESS = "created_at <= ?".freeze
    CREATED_NOT_IN = "created_by NOT IN (?)".freeze  
    COUNT_ID_DESC = "count_id desc".freeze  
    USERS_ID = "users.id".freeze  
    USERS_ID_IN = "users.id IN (?)".freeze  
    USERS_EMAIL = "users.email IN (?)".freeze  
    USERS_EMAIL_NOT_IN = "users.email NOT IN (?)".freeze  
    PARTNER_EMAIL_QUERY = "partners.email <> ''".freeze  
    PARTNER_EMAILS = "partners.email <> ''".freeze  
    SIGN_IN_COUNT = "users.sign_in_count > 0".freeze  
    LAST_SIGN_IN_IS_NULL = "last_sign_in_at IS NULL".freeze
    LAST_SIGN_IN_IS_NOT_NULL = "last_sign_in_at IS NOT NULL".freeze
    INSPECTOR_TAG_INTERPOLATION = ["tags.metadata LIKE :meta".freeze, meta: "%inspector%".freeze]
    BROKER_TAG_INTERPOLATION = ["tags.metadata LIKE :meta".freeze, meta: "%broker%".freeze]
    PARTNER_TAG_INTERPOLATION = ["tags.tag LIKE :tag".freeze,  tag: "%partner%".freeze]
    ACCOUNT_SUM = "SUM(accounts.subscription_amount_cents) AS subscription, SUM(accounts.transaction_amount_cents) AS transaction".freeze
    ACCOUNTS_JOIN = "INNER JOIN accounts ON accounts.manager_id = partners.id".freeze
    TRANSACTION_QUERY = "transaction_amount_cents > 0 OR subscription_amount_cents > 0".freeze
    MONTHLY_QUERY = "(transaction_amount_cents/100 + subscription_amount_cents/100) * 12".freeze
    ANNUAL_QUERY = "transaction_amount_cents/100 + subscription_amount_cents/100".freeze
    REMINDER_JOINS = "INNER JOIN maintenance_items on maintenance_items.id = maintenance_events.maintenance_item_id INNER JOIN binders on maintenance_items.binder_id = binders.id INNER JOIN properties on properties.binder_id = binders.id INNER JOIN user_binders on binders.id = user_binders.binder_id INNER JOIN users on user_binders.user_id = users.id".freeze
    REMINDER_SELECT = "maintenance_items.name AS maintenance_item_name, maintenance_events.last_notify_date, binders.name AS binder_name, binders.id, properties.address1, properties.zip, users.email"
    MAINTENANCE_EVENT_KEYS = ["id", "last_notify_date", "maintenance_item_name", "binder_name", "address1", "zip_code", "email"]
end