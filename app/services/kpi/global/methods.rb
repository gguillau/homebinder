# KPI helper methods used in all modules
module Kpi::Global::Methods
    
    include Kpi::Global::Constants
    
    # get the ids of all homeowner users
    def homeowners
        User.where(:role => "homeowner").pluck(:id)
    end
    
    # get the IDs fo all inspector users
    def inspector_user_ids
        User.where(:role => "inspector").pluck(:id)
    end
    
    # get the user ids for all inspectors
    def inspector_clause
        {:created_by => inspector_user_ids.flatten(1)}
    end
    
    # check if a number is nan and either return 0 or the number as a percentage
    def percent(percentage)
        return percentage.nan? ? 0 : percentage
    end
    
    def partner_binders_created_in_last_seven(partner_type)
        # use appropriate clause for partner
        clause = get_clause(partner_type)
        # get all binders created in last seven by all partners
        @partner_seven = Binder.where(created_at: 7.days.ago..Time.now).where(partner_clause).count
        # get all binders created in last seven by all inspectors
        seven = Binder.where(created_at: 7.days.ago..Time.now).where(clause).count
        return {count: seven, percentage: percent(seven.to_f/@partner_seven.to_f)}
    end
    
    # get the user ids for all partners
    def partner_clause
        ids = User.where("role NOT IN (?)", ["admin", "homeowner", "agent"]).pluck(:id)
        {:created_by => ids}
    end
    
    # get the user ids for all brokers/agents
    def agent_clause
        ids = User.where(:role => "broker").pluck(:id)
        {:created_by => ids}
    end
    
    # get the user ids for all admins
    def admin_clause
        ids = User.where(:role => "admin").pluck(:id)
        {:created_by => ids}
    end
    
    # association where there are no users
    def orphan_clause
        {:users => {:id => nil}}
    end
    
    # return appropriate where clause for specific partner type
    def get_clause(partner_type)
        return partner_type === INSPECTOR ? inspector_clause : agent_clause
    end
    
    # returns percentage of numerator/denominator
    def percentage(numerator, denominator)
        return "n/a" if denominator === 0
        return ((numerator.to_f / denominator.to_f) * 100).round
    end
        
    # returns YoY percentage
    def year_over_year(this_year, last_year)
        return "n/a" if (!this_year.is_a? Numeric) || (!last_year.is_a? Numeric)
        return percentage((this_year - last_year), last_year)
    end
        
    # converts number to string for displaying
    def num_to_string(number)
        return number if !number.is_a? Numeric
        return "+" + number.to_s if number > 0
        return number = number.to_s
    end
    
end