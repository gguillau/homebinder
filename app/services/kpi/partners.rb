require 'csv'

module Kpi::Partners
    
    include Kpi::Global::Constants
    include Kpi::Global::Methods
    
    
    def self.included(klass)
        klass.extend ClassMethods 
    end
    
    # total account contract value
    def total_acv
        monthly_sum = Account.where(:account_status => Account::AccountStatus::ACTIVE, :billing_frequency => MONTHLY).sum(MONTHLY_QUERY)
        annual_sum = Account.where(:account_status => Account::AccountStatus::ACTIVE, :billing_frequency => ANNUAL).sum(ANNUAL_QUERY)
        return monthly_sum.to_i + annual_sum.to_i
    end
    
    # will be used to get partner metrics for partners in their dashboard
    def get_partner_weekly_metrics(params)
        # do check to see if partner can read
        
        # return metrics
        return get_weekly_metrics(params[:id])
    end
    
    # finds the retention rate by partner type
    def retention_rate(partner_type)
        # get the number that are active and paying
        paying = Partner.joins(:account).where(:partner_type => partner_type).where(:accounts =>  {:account_status => Account::AccountStatus::ACTIVE}).where(TRANSACTION_QUERY).count
        # get the total that have paid whether active or not
        total = Partner.joins(:account).where(:partner_type => partner_type).where(TRANSACTION_QUERY).count
        return percent(paying.to_f/total.to_f)
    end
    
    def binders_created_by_partner_type_owned_by_homeowners(partner_type)
        clause = get_clause(partner_type)
        week = 12
        weeks = []
        
        while week > -1 do
            
            week_start = week.weeks.ago.beginning_of_week(start_day = :sunday)
            week_end = week.weeks.ago.end_of_week(start_day = :sunday)
            dates = [(week_start - 60.days)..week_end]
            
            count = Binder.where(clause).where(created_at: dates).count
            ids = Binder.where(clause).where(created_at: dates).pluck(:id)
            users = homeowners_with_binders_created_by_partner_type(ids, partner_type)
            weeks.append({ week_start: (week_start - 60.days), week_end: week_end, accepted: users, created: count, percentage: percent(users.to_f/count.to_f)})
            week -= 1
        end

        return weeks
    end
    
    # get the number of logins for homeowner created by a specific partner from 12 weeks out
    def users_with_binders_created_by_partner_type(partner_type)
        week = 12
        weeks = []
        
        while week > -1 do
            start_day = :sunday
            week_start = week.weeks.ago.beginning_of_week(start_day)
            week_end = week.weeks.ago.end_of_week(start_day)
            logins = homeowners_with_binders_created_by_partner_type_dates(week_end, partner_type)
            weeks.append({ week_start: week_start, week_end: week_end, logins: logins})
            week -= 1
        end

        return weeks
    end
    
    # homeowners who have binders created by a specific partner type
    def homeowners_with_binders_created_by_partner_type_dates(week_end, partner_type)
        # get the where clause
        clause = get_clause(partner_type)
        # get the users
        User.joins(:binders).where(:role => "homeowner").where(last_sign_in_at: 60.days.ago..week_end).where(:binders => clause).where(SIGN_IN_COUNT).count
    end
    
    # homeowners who have binders created by a specific partner type
    def homeowners_with_binders_created_by_partner_type(ids, partner_type)
        # get the where clause
        clause = get_clause(partner_type)
        User.joins(:binders).where(:role => "homeowner").where(:binders => clause).where(:binders => {:id => ids}).where(SIGN_IN_COUNT).count
    end
    
    # total binders created by a partner type in past 7 days
    def total_with_binders_by_paid_customers(partner_type)
        # get all the partner ids for partners with active accounts and who are paying
        ids = Partner.joins(:account).where(:partner_type => partner_type).where(:accounts =>  {:account_status => Account::AccountStatus::ACTIVE}).where(TRANSACTION_QUERY).pluck(PartnerService::DEFAULT_ORDER_BY)
        # get all the user accounts related to partners and check if any binders were created in the past 7 days
        created = User.joins(:partner_users, :binders).where(:partner_users => {:role => "admin"}).where(:binders => {:created_at => 7.days.ago..Time.now}).distinct.count
        # return the percentage
        return percent(created.to_f/ids.length.to_f)
    end
    
    module ClassMethods
        
        include Kpi::Global::Constants
        include Kpi::Global::Methods

        # get the current metrics
        def get_metrics(partner_id)
            # set variables
            partner_users = get_partner_users(partner_id)
            week_start = 1.weeks.ago.beginning_of_week(start_day = :sunday)
            week_end = 1.weeks.ago.end_of_week(start_day = :sunday)

            # raise an exception if the partner is not associated with an user
            raise BadRequestException unless partner_users.length > 0
            return {
                total_binders: total_binders(partner_users),
                total_binders_past_week: total_binders(partner_users, {:created_at => 7.days.ago..Time.now}),
                total_binders_past_month: total_binders(partner_users, {:created_at => 1.month.ago..Time.now}),
                total_binders_not_transferred: total_binders_not_transferred(partner_users),
                reminders_sent: maintenance_reminders_sent(partner_users, week_start, week_end),
                reminders_sent_past_month: maintenance_reminders_sent(partner_users, week_start - 30.days, week_end),
                reminders_sent_past_ninety: maintenance_reminders_sent(partner_users, week_start - 90.days, week_end),
                login_rate: homeowner_login_rate(partner_users),
                user: find_user(partner_id)
            }
        end

        # get the weekly metrics
        def get_weekly_metrics(partner_id)
            # set variables
            partner_users = get_partner_users(partner_id)
            week_start = 1.weeks.ago.beginning_of_week(start_day = :sunday)
            week_end = 1.weeks.ago.end_of_week(start_day = :sunday)
            # raise an exception if the partner is not associated with an user
            raise BadRequestException unless partner_users.length > 0
            return {
                total_binders: total_binders(partner_users),
                total_binders_past_week: total_binders(partner_users, {:created_at => 7.days.ago..Time.now}),
                total_binders_past_month: total_binders(partner_users, {:created_at => 1.month.ago..Time.now}),
                total_binders_not_transferred: total_binders_not_transferred(partner_users),
                reminders_sent: maintenance_reminders_sent(partner_users, week_start, week_end),
                reminders_sent_past_month: maintenance_reminders_sent(partner_users, week_start - 30.days, week_end),
                reminders_sent_past_ninety: maintenance_reminders_sent(partner_users, week_start - 90.days, week_end),
                user: find_user(partner_id)
            }
        end
        
        # get the updated biweekly metrics
        def get_biweekly_metrics(partner_id)
            # get partner
            partner = Partner.find_by_id(partner_id)
            # raise an exception if the partner does not exist
            raise BadRequestException if partner.nil?
            
            # get homeowner acceptance rates for all time and 30 days ago
            homeowner_acceptance_rate = get_homeowner_acceptance_rate(partner_id, Time.now)
            homeowner_acceptance_rate_30_days_ago = get_homeowner_acceptance_rate(partner_id, 30.days.ago)
            homeowner_acceptance_rate_difference = homeowner_acceptance_rate - homeowner_acceptance_rate_30_days_ago
            
            # convert homeowner_acceptance_rate_difference to a string to properly display positive numbers
            homeowner_acceptance_rate_difference = num_to_string(homeowner_acceptance_rate_difference)
            
            return {
                homeowner_acceptance_rate: homeowner_acceptance_rate,
                homeowner_acceptance_rate_difference: homeowner_acceptance_rate_difference,
                thank_yous_past_90_days: thank_yous(partner_id, 90.days.ago, Time.now),
                reminders_sent: maintenance_reminders_sent_all_time(partner),
                total_binders_past_30_days: total_binders_by_partner_binders(partner_id, {:created_at => 30.days.ago..Time.now}),
                total_binders: total_binders_by_partner_binders(partner_id)
            }
        end
        
        # homeowners who have binders created by a specific partner and have signed in at least once
        def get_logins(partner_id)
            partner_users = get_partner_users(partner_id)
            User.joins(:binders).where(:role => "homeowner").where(:binders => {:created_by => partner_users}).where(SIGN_IN_COUNT).last(100).reverse
        end
        
        # maintenance_reminders
        def get_maintenance_reminders(partner_id)
            partner_users = get_partner_users(partner_id)
            # dates to use
            week_start = 4.weeks.ago.beginning_of_week(start_day = :sunday)
            week_end = 1.weeks.ago.end_of_week(start_day = :sunday)
            
            event_where = {:created_by => partner_users, :last_notify_date => week_start..week_end}
            
            Binder::MaintenanceItem::MaintenanceEvent.joins(REMINDER_JOINS).select(REMINDER_SELECT).where(event_where).where("users.role = ?", "homeowner")
        end
        
        def download_reminders(partner_id)
            items = get_maintenance_reminders(partner_id)
            csv_file = ::CSV.generate({}) do |csv|
            
                csv << MAINTENANCE_EVENT_KEYS
                
                items.all.each do |item|
                    csv << item.attributes.values
                end
            end
            csv_file
        end
        
        def download_logins(partner_id)
            items = get_logins(partner_id)
            csv_file = ::CSV.generate({}) do |csv|
                csv << ["id", "email", "binder_transfer_date", "date_accepted_transfer"]
                
                items.each do |item|
                    csv << [item.id, item.email, item.created_at.to_date, item.accepted_transfer_at.to_date]
                end
            end
            csv_file
        end
        
        # total binders created by a specific partner
        # second where clause is to specify any additional parameters
        # ex: {:created_at => 7.days.ago..Time.now}
        def total_binders(partner_users, where = {})
            Binder.where(:created_by => partner_users).where(where).count
        end
        
        # total binders based on PartnerBinders
        def total_binders_by_partner_binders(partner_id, where = {})
            PartnerBinder.where(:partner_id => partner_id).where(where).count
        end
        
        # total binders created by a specific partner that have not been transferred or shared
        def total_binders_not_transferred(partner_users)
            Binder.joins(:users).includes(:shares).where("users.id" => partner_users, :shares => {:id => nil}).where(:created_by => partner_users).count
        end
        
        # total number of thank-yous sent to a given partner within the timeframe passed as arguments
        def thank_yous(partner_id, gte, lte)
            Event.index({}, {searchType: 'aggregation', query: 'event_name: feedback_given AND partner_id: ' + partner_id.to_s, gte: gte.strftime("%m/%d/%Y"), lte: lte.strftime("%m/%d/%Y"), range: 'created_at', search: true})
        end
        
        # total all-time maintenance reminders sent for binders created by a partner
        def maintenance_reminders_sent_all_time(partner)
            Binder::MaintenanceItem::MaintenanceEvent.index({}, {searchType: 'aggregation', query: "partner.role: " + partner.partner_type.to_s + " AND partner.partner_id:" + partner.id.to_s, lte: Time.now.strftime("%m/%d/%Y"), range: 'do_date', search: true})
        end
        
        # total maintenance reminders sent for binders created by a partner within a given time-frame
        def maintenance_reminders_sent(partner_users, week_start, week_end)
            Binder::MaintenanceItem::MaintenanceEvent.where(:created_by => partner_users, :last_notify_date => week_start..week_end).count
        end
        
        # total maintenance reminders sent for binders created by a partner in the past week
        def maintenance_reminders_sent_past_week(partner_users, week_start, week_end)
            Binder::MaintenanceItem::MaintenanceEvent.where(:created_by => partner_users, :last_notify_date => week_start..week_end).last(20)
        end
        
        # finds the actual user account related to a partner
        def find_user(partner_id)
            # get the first user with the partner admin role
            User.joins(:partner_users).where(:partner_users => {:partner_id => partner_id, :role => "admin"}).order(:created_at).first
        end
        
        def homeowner_login_rates_by_date(partner_user, week_start, week_end)
            User.joins(:binders).where(:role => "homeowner").where(:binders => {:created_by => partner_user.id}, :last_sign_in_at => week_start..week_end).count
        end
        
        def get_login_rate(partner_id)
            partner_users = get_partner_users(partner_id)
            homeowner_login_rate(partner_users)
        end
        
        # homeowner login rates per week
        def homeowner_login_rate(partner_users)
            # get all unique homeowners with binders created by the partner
            count = User.joins(:binders).where(:role => "homeowner").where(:binders => {:created_by => partner_users}).where("binders.created_at < ?", 45.days.ago).where("users.sign_in_count > 0").distinct.count
            total = User.joins(:binders).where(:role => "homeowner").where(:binders => {:created_by => partner_users}).where("binders.created_at < ?", 45.days.ago).distinct.count

            # minus 20%
            total = total - (total * 0.2)

            percentage = percent(count.to_f/total.to_f) * 100
            return percentage.round(0)
        end
        
        # homeowner acceptance rate for all binders created before the created_before argument
        def get_homeowner_acceptance_rate(partner_id, created_before)
            all_clients = PartnerClient.index({}, {searchType: 'aggregation', query: 'partner_id: ' + partner_id.to_s, lte: created_before.strftime("%m/%d/%Y"), range: 'created_at', search: true}).to_f
            accepted_clients = PartnerClient.index({}, {searchType: 'aggregation', query: 'client.sign_in_count:>0 AND partner_id: ' + partner_id.to_s, lte: created_before.strftime("%m/%d/%Y"), range: 'created_at', search: true}).to_f
            return 0 if all_clients === 0
            return ((accepted_clients / all_clients) * 100).round
        end
        
        # returns agent analytics for partner
        def get_agent_analytics(partner_id)
            partner = Partner.find(partner_id)
            partner_binders = partner.binders
            partner_binder_ids = partner_binders.pluck(:id)

            top_agents = get_top_agents(partner) 
            
            agents = partner.users.where(:role => "agent")
            agent_logins = agents.sum(:sign_in_count)
            agent_count = agents.size
            
            agents_this_year = agents.where(:created_at => 1.year.ago..Time.now).size
            agents_last_year = agents.where(:created_at => 2.years.ago..1.year.ago).size
            agents_yoy = num_to_string(year_over_year(agents_this_year, agents_last_year))

            num_binders_this_year = partner_binders.where(:created_at => 1.year.ago..Time.now).size
            
            # get agents who have been added to at least 2 binders
            agent_binders_this_year, agent_binders_last_year = get_agent_binders(partner_id)

            percent_agent_binders_this_year = percentage(agent_binders_this_year, num_binders_this_year)
            agent_binders_yoy = num_to_string(year_over_year(agent_binders_this_year, agent_binders_last_year))
            
            return {:agents => top_agents, :agentLogins => agent_logins, :agentCount => agent_count, :agentsThisYear => agents_this_year, 
                    :agentsLastYear => agents_last_year, :agentsYoy => agents_yoy, :percentAgentBindersThisYear => percent_agent_binders_this_year, 
                    :agentBindersYoy => agent_binders_yoy}
            
        end
        
        # returns partner's top 50 agents sorted by this year's binder count
        def get_top_agents(partner)
            partner_binder_ids = partner.binders.pluck(:id)
            binders_this_year = "CAST(COUNT(CASE WHEN user_binders.created_at BETWEEN '" + 1.year.ago.to_s + "' AND '" + Time.now.to_s + "' THEN 1 ELSE null END) AS float)"
            binders_last_year = "CAST(COUNT(CASE WHEN user_binders.created_at BETWEEN '" + 2.years.ago.to_s + "' AND '" + 1.year.ago.to_s + "' THEN 1 ELSE null END) AS float)"
            top_agents = partner.users.joins(:user_binders, :user_profile)
                            .select("users.*, user_profiles.first_name AS first_name, user_profiles.last_name AS last_name, " + binders_this_year + " AS binders_this_year, " + binders_last_year + " AS binders_last_year, (((" + binders_this_year + "-" + binders_last_year + ")/NULLIF(" + binders_last_year + ", 0))*100.00) as year_over_year")
                            .where(:role => "agent", :user_binders => {:role => ["buyer_agent", "seller_agent"], :binder_id => partner_binder_ids})
                            .group("users.id, user_profiles.first_name, user_profiles.last_name")
                            .limit(50)
                            .order("binders_this_year DESC")
            return top_agents
        end
        
        # returns number of binders created this year and last year with existing agents added
        def get_agent_binders(partner_id)
            partner = Partner.find partner_id
            agent_ids = partner.users.where(:role => "agent").ids
            partner_binder_ids = partner.binder_ids
            ids_for_agents_with_at_least_2_binders = UserBinder.where(:user_id => agent_ids, :binder_id => partner_binder_ids).group(:user_id).having("COUNT(user_id) > 1").pluck(:user_id)
            agent_binders_this_year = UserBinder.where(:binder_id => partner_binder_ids, :user_id => ids_for_agents_with_at_least_2_binders, :created_at => 1.year.ago..Time.now).count
            agent_binders_last_year = UserBinder.where(:binder_id => partner_binder_ids, :user_id => ids_for_agents_with_at_least_2_binders, :created_at => 2.years.ago..1.year.ago).count
            return agent_binders_this_year, agent_binders_last_year
        end
        
        # get the users via role for partner
        def get_partner_users(partner_id)
            partner = Partner.find(partner_id)
            User.joins(:partner_users).where(:partner_users => {:partner_id => partner_id}, :role => partner.partner_type).order(:created_at).pluck(:id)
        end
        
        # transfers <- for future use - work in progress
        
        def transferred_binders_by_date(partner_id)
            partner_users = get_partner_users(partner_id)
            
            return {
                total_binders_created_in_past_month: total_binders(partner_users, {:created_at => 1.month.ago..Time.now}),
                total_binders_transferred_in_past_month: total_binders_transferred_by_date(partner_users),
                total_binders_transferred_and_delivered_in_past_month: total_transferred_and_delivered_by_date(partner_users)
            }
        end
        
        def total_binders_transferred_by_date(partner_users)
            Binder.joins(:tags, :users).where(:created_by => partner_users, :created_at => 30.days.ago..Time.now, :tags => {:tag => "transfer"}).count
        end
        
        # find transfers that were delivered
        def total_transferred_and_delivered_by_date(partner_users)
            count = 0
            binders = Binder.joins(:tags, :users).where(:created_by => partner_users, :created_at => 30.days.ago..Time.now, :tags => {:tag => "transfer"})
            binders.each do |binder|
                tag = binder.tags.where(:tag => "transfer").first
                transfer = JSON.parse(tag.metadata)
                if transfer["email_delivered"] === true
                    count += 1
                end
            end
            return count
        end
        
        # def total_binders_without_transfer_tag
        #     #no_tags = []
        #     clause = get_clause("inspector")
        #     Binder.joins(:users).where(:users => {:id => homeowners, :accepted_transfer_at => 7.days.ago..Time.now}).includes(:tags).references(:tags).where(clause).where(:created_at => 7.days.ago..Time.now).distinct
        # end
        
        # def accepted_transfers
        # end
        
        
        # def total_binders_transferred_by_date
        #     clause = get_clause("inspector")
        #     Binder.joins(:tags, :users).where(clause).where(:created_at => 7.days.ago..Time.now, :tags => {:tag => "transfer"}).distinct.count
        # end
        
        # # find transfers that were delivered
        
        # def total_transferred_and_delivered_by_date
        #     count = 0
        #     clause = get_clause("inspector")
        #     binders = Binder.joins(:tags, :users).where(clause).where(:created_at => 7.days.ago..Time.now, :tags => {:tag => "transfer"}).distinct
        #     binders.each do |binder|
        #         tag = binder.tags.where(:tag => "transfer").first
        #         transfer = JSON.parse(tag.metadata)
        #         if transfer["email_delivered"] === true
        #             count += 1
        #         end
        #     end
        #     return count
        # end
        
        
    end
    
end