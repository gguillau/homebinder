class Kpi::Agents
    def get_monthly_metrics(agent_id)
        binders = Binder.joins(:user_binders).where(:user_binders => {:role => "buyer_agent", :user_id => agent_id})

        return {
            referring_partners: get_referring_partners(agent_id),
            total_binders: binders.size,
            maintenance_emails_sent: get_maintenance_emails_sent(binders),
            annual_maintenance_emails: get_annual_maintenance_emails(binders)
        }
    end

    def get_referring_partners(agent_id)
        Partner.joins(:partner_users).where(:partner_users => {:user_id => agent_id, :role => "agent"})
    end

    def get_maintenance_emails_sent(binders)
        total_emails = 0

        binders.each do |binder|
            total_emails = total_emails + Event.index({}, {
                searchType: 'aggregation',
                query: 'event_name: maintenance_reminder AND event_type: email AND binder_id: ' + binder.id.to_s,
                gte: "01/01/2010",
                lte: Date.today.strftime("%m/%d/%Y"),
                range: 'created_at',
                search: true
            })
        end

        return total_emails
    end

    def get_annual_maintenance_emails(binders)
        total_emails = 0.0

        binders.each do |binder|
            maintenance_items = Binder::MaintenanceItem.where(:binder => binder)
            maintenance_items.each do |maintenance_item|
                frequency = maintenance_item.maintenance_cycle
                interval = maintenance_item.interval
                next if interval.nil? || interval == 0

                if frequency == "Years"
                    total_emails = total_emails + (1.0/interval.to_f)
                elsif frequency == "Months"
                    total_emails = total_emails + (12.0/interval.to_f)
                elsif frequency == "Weeks"
                    total_emails = total_emails + (52.0/interval.to_f)
                elsif frequency == "Days"
                    total_emails = total_emails + (365.0/interval.to_f)
                end
            end
        end

        return total_emails.round
    end
end
