require 'date'
require 'open-uri'
require 'uri'
require 'prawn'
require 'prawn/table'

class Kpi::Report
    
    include Kpi::Users
    include Kpi::Binders
    include Kpi::Partners
    include Kpi::Homeowners
    include Kpi::Global::Constants
    include ActionView::Helpers::NumberHelper
    
    attr_accessor :pdf, :user
    
    def initialize(user_id)
        self.user = User.find(user_id)
        self.pdf = Prawn::Document.new( :bottom_margin => 100 )
    end

    def get_report
        # only admins can do this
        raise CanCan::AccessDenied.new("You are not authorized to view KPI Reports") unless user.has_role? :admin
        # inspectors hash
        @inspectors = {}
        @homeowner = {}
        
        get_data
        
        init
        add_logos
        add_title
        add_summary
        add_aggregate_totals
        add_user_logins_totals
        add_binders_setup_totals
        add_no_logins_totals
        add_binders_created_by_inspectors
        add_logins_for_users_created_by_inspectors
        add_footer
        pdf.render
    end
    
    def get_data
        #get data
        @total_acv = total_acv
        @homeowner[:interaction] = get_homeowner_interaction
        @homeowner[:acceptance] = homeowner_acceptance_rate_by_partner_type(INSPECTOR)
        @binders = get_binder_numbers
        @users = get_user_numbers
        @inspectors[:retention_rate] = retention_rate(INSPECTOR)
        @inspectors[:total_inspectors_with_binders] = total_with_binders_by_paid_customers(INSPECTOR)
        @inspectors[:total_binders] = partner_binders_created_in_last_seven(INSPECTOR)
        @inspectors[:binders] = binders_created_by_partner_type_owned_by_homeowners(INSPECTOR)
        @inspectors[:logins] = users_with_binders_created_by_partner_type(INSPECTOR)
    end
    
    def init
        pdf.font_families.update("Arvo" => {
          :normal => { :file => "#{Rails.root}/public/assets/Arvo-Regular.ttf" },
          :italic => { :file => "#{Rails.root}/public/assets/Arvo-Italic.ttf" },
          :bold => { :file => "#{Rails.root}/public/assets/Arvo-Bold.ttf" },
          :bold_italic => { :file => "#{Rails.root}/public/assets/Arvo-BoldItalic.ttf" }
        })
    end
    
    def add_logos
        # logos
        pdf.bounding_box([0, 666], :width => pdf.bounds.width, :height => 50) do
            # hb logo, top left
            pdf.image "#{Rails.root}/public/img/flatlogonotext.png", :height => 50, :position => :left, :vposition => :top
        end
    end
    
    def add_title
        # report title
        pdf.formatted_text_box [
            { :text => "HomeBinder KPI Report", :color => "999999", :size => 22 }
        ], :at => [75, 655], :width => 400, :height => 80
    end
    
    def add_summary
        
        pdf.move_down(25)
        
        pdf.formatted_text_box [
          { :text => "Total Acv", :size => 14, :color => "000000", :font_style => "bold" },
        ], :at => [0, pdf.cursor], :width => 400
        
        pdf.move_down(25)
        
        pdf.formatted_text_box [
          { :text => number_to_currency(@total_acv, :unit => "$"), :size => 14, :color => "ff6633", :font_style => "bold" },
        ], :at => [0, pdf.cursor], :width => 400
        
        pdf.move_down(25)
        
        pdf.formatted_text_box [
          { :text => "# of Binders", :size => 14, :color => "000000", :font_style => "bold" },
        ], :at => [0, pdf.cursor], :width => 200
        
        pdf.formatted_text_box [
          { :text => "Creating Binders", :size => 14, :color => "000000", :font_style => "bold" },
        ], :at => [200, pdf.cursor], :width => 200
        
        pdf.formatted_text_box [
          { :text => "Retention Rate", :size => 14, :color => "000000", :font_style => "bold" },
        ], :at => [400, pdf.cursor], :width => 200
        
        pdf.move_down(25)
        
        pdf.formatted_text_box [
          { :text => "#{@inspectors[:total_binders][:count]} | #{format_percent(@inspectors[:total_binders][:percentage])}%", :size => 14, :color => "ff6633", :font_style => "bold" },
        ], :at => [50, pdf.cursor], :width => 200
        
        pdf.formatted_text_box [
          { :text => "#{format_percent(@inspectors[:total_inspectors_with_binders])}%", :size => 14, :color => "ff6633", :font_style => "bold" },
        ], :at => [250, pdf.cursor], :width => 200
        
        pdf.formatted_text_box [
          { :text => "#{format_percent(@inspectors[:retention_rate])}%", :size => 14, :color => "ff6633", :font_style => "bold" },
        ], :at => [450, pdf.cursor], :width => 200
        
        pdf.move_down(25)
        
        pdf.formatted_text_box [
          { :text => "Homeowner Interaction", :size => 14, :color => "000000", :font_style => "bold" },
        ], :at => [0, pdf.cursor], :width => 200
        
        pdf.formatted_text_box [
          { :text => "Homeowner Acceptance", :size => 14, :color => "000000", :font_style => "bold" },
        ], :at => [200, pdf.cursor], :width => 200
        
        pdf.move_down(25)
        
        pdf.formatted_text_box [
          { :text => "#{format_percent(@homeowner[:interaction])}%", :size => 14, :color => "ff6633", :font_style => "bold" },
        ], :at => [50, pdf.cursor], :width => 200
        
        pdf.formatted_text_box [
          { :text => "#{format_percent(@homeowner[:acceptance])}%", :size => 14, :color => "ff6633", :font_style => "bold" },
        ], :at => [250, pdf.cursor], :width => 200
        
        pdf.move_down(25)
    end
    
    def add_aggregate_totals

        pdf.move_down(25)
        
        pdf.formatted_text_box [
          { :text => "Aggregate Totals", :size => 14, :color => "000000", :font_style => "bold" },
        ], :at => [0, pdf.cursor], :width => 400
        
        pdf.move_down(25)
        
        data =  [
            ["Aggregate Totals", "New in last 7 days", "New in last 30 days", "3 Month Average", "6 Month Average", "12 Month Average", "Total on site"],
            [
                "Binders", 
                "#{@binders[:all][:last_seven][:count]}",
                "#{@binders[:all][:last_thirty][:count]}",
                "#{@binders[:all][:last_ninety][:count]}",
                "#{@binders[:all][:half_year][:count]}",
                "#{@binders[:all][:year][:count]}",
                "#{@binders[:all][:total][:count]}"
            ],
            [
                "Users", 
                "#{@users[:count][:all][:last_seven]}",
                "#{@users[:count][:all][:last_thirty]}",
                "#{@users[:count][:all][:last_ninety]}",
                "#{@users[:count][:all][:half_year]}",
                "#{@users[:count][:all][:year]}",
                "#{@users[:count][:all][:total]}"
            ],
            [
                "Agents", 
                "#{@users[:count][:agents][:last_seven]}",
                "#{@users[:count][:agents][:last_thirty]}",
                "#{@users[:count][:agents][:last_ninety]}",
                "#{@users[:count][:agents][:half_year]}",
                "#{@users[:count][:agents][:year]}",
                "#{@users[:count][:agents][:total]}"
            ],
            [
                "Inspectors", 
                "#{@users[:count][:inspectors][:last_seven]}",
                "#{@users[:count][:inspectors][:last_thirty]}",
                "#{@users[:count][:inspectors][:last_ninety]}",
                "#{@users[:count][:inspectors][:half_year]}",
                "#{@users[:count][:inspectors][:year]}",
                "#{@users[:count][:inspectors][:total]}"
            ],
            [
                "Partners", 
                "#{@users[:count][:partners][:last_seven]}",
                "#{@users[:count][:partners][:last_thirty]}",
                "#{@users[:count][:partners][:last_ninety]}",
                "#{@users[:count][:partners][:half_year]}",
                "#{@users[:count][:partners][:year]}",
                "#{@users[:count][:partners][:total]}"
            ]
        ]
        
        pdf.table data, :position => :center, :cell_style => { :size => 8}
    end
    
    def add_binders_setup_totals
        
        pdf.move_down(25)
        
        pdf.formatted_text_box [
          { :text => "Binders Setup", :size => 14, :color => "000000", :font_style => "bold" },
        ], :at => [0, pdf.cursor], :width => 400
        
        pdf.move_down(25)
        
        data =  [
            ["Binders Setup", "Last 7 days", "Last 30 days", "3 Month Average", "6 Month Average", "12 Month Average", "Total on site"],
            [
                "Total Binders",
                "#{@binders[:all][:last_seven][:count]} | #{format_percent(@binders[:all][:last_seven][:percentage])}%",
                "#{@binders[:all][:last_thirty][:count]} | #{format_percent(@binders[:all][:last_thirty][:percentage])}%",
                "#{@binders[:all][:last_ninety][:count]} | #{format_percent(@binders[:all][:last_ninety][:percentage])}%",
                "#{@binders[:all][:half_year][:count]} | #{format_percent(@binders[:all][:half_year][:percentage])}%",
                "#{@binders[:all][:year][:count]} | #{format_percent(@binders[:all][:year][:percentage])}%",
                "#{@binders[:all][:total][:count]} | #{format_percent(@binders[:all][:total][:percentage])}%"
            ],
            [
                "Admin Binders", 
                "#{@binders[:admin][:last_seven][:count]} | #{format_percent(@binders[:admin][:last_seven][:percentage])}%",
                "#{@binders[:admin][:last_thirty][:count]} | #{format_percent(@binders[:admin][:last_thirty][:percentage])}%",
                "#{@binders[:admin][:last_ninety][:count]} | #{format_percent(@binders[:admin][:last_ninety][:percentage])}%",
                "#{@binders[:admin][:half_year][:count]} | #{format_percent(@binders[:admin][:half_year][:percentage])}%",
                "#{@binders[:admin][:year][:count]} | #{format_percent(@binders[:admin][:year][:percentage])}%",
                "#{@binders[:admin][:total][:count]} | #{format_percent(@binders[:admin][:total][:percentage])}%"
            ],
            [
                "Homeowner Binders", 
                "#{@binders[:homeowner][:last_seven][:count]} | #{format_percent(@binders[:homeowner][:last_seven][:percentage])}%",
                "#{@binders[:homeowner][:last_thirty][:count]} | #{format_percent(@binders[:homeowner][:last_thirty][:percentage])}%",
                "#{@binders[:homeowner][:last_ninety][:count]} | #{format_percent(@binders[:homeowner][:last_ninety][:percentage])}%",
                "#{@binders[:homeowner][:half_year][:count]} | #{format_percent(@binders[:homeowner][:half_year][:percentage])}%",
                "#{@binders[:homeowner][:year][:count]} | #{format_percent(@binders[:homeowner][:year][:percentage])}%",
                "#{@binders[:homeowner][:total][:count]} | #{format_percent(@binders[:homeowner][:total][:percentage])}%"
            ],
            [
                "Inspector Binders", 
                "#{@binders[:inspectors][:last_seven][:count]} | #{format_percent(@binders[:inspectors][:last_seven][:percentage])}%",
                "#{@binders[:inspectors][:last_thirty][:count]} | #{format_percent(@binders[:inspectors][:last_thirty][:percentage])}%",
                "#{@binders[:inspectors][:last_ninety][:count]} | #{format_percent(@binders[:inspectors][:last_ninety][:percentage])}%",
                "#{@binders[:inspectors][:half_year][:count]} | #{format_percent(@binders[:inspectors][:half_year][:percentage])}%",
                "#{@binders[:inspectors][:year][:count]} | #{format_percent(@binders[:inspectors][:year][:percentage])}%",
                "#{@binders[:inspectors][:total][:count]} | #{format_percent(@binders[:inspectors][:total][:percentage])}%"
            ],
            [
                "Agent Binders", 
                "#{@binders[:agents][:last_seven][:count]} | #{format_percent(@binders[:agents][:last_seven][:percentage])}%",
                "#{@binders[:agents][:last_thirty][:count]} | #{format_percent(@binders[:agents][:last_thirty][:percentage])}%",
                "#{@binders[:agents][:last_ninety][:count]} | #{format_percent(@binders[:agents][:last_ninety][:percentage])}%",
                "#{@binders[:agents][:half_year][:count]} | #{format_percent(@binders[:agents][:half_year][:percentage])}%",
                "#{@binders[:agents][:year][:count]} | #{format_percent(@binders[:agents][:year][:percentage])}%",
                "#{@binders[:agents][:total][:count]} | #{format_percent(@binders[:agents][:total][:percentage])}%"
            ],
            [
                "Partners Binders", 
                "#{@binders[:partners][:last_seven][:count]} | #{format_percent(@binders[:partners][:last_seven][:percentage])}%",
                "#{@binders[:partners][:last_thirty][:count]} | #{format_percent(@binders[:partners][:last_thirty][:percentage])}%",
                "#{@binders[:partners][:last_ninety][:count]} | #{format_percent(@binders[:partners][:last_ninety][:percentage])}%",
                "#{@binders[:partners][:half_year][:count]} | #{format_percent(@binders[:partners][:half_year][:percentage])}%",
                "#{@binders[:partners][:year][:count]} | #{format_percent(@binders[:partners][:year][:percentage])}%",
                "#{@binders[:partners][:total][:count]} | #{format_percent(@binders[:partners][:total][:percentage])}%"
            ],
            [
                "Orphaned Binders", 
                "#{@binders[:orphan][:last_seven][:count]} | #{format_percent(@binders[:orphan][:last_seven][:percentage])}%",
                "#{@binders[:orphan][:last_thirty][:count]} | #{format_percent(@binders[:orphan][:last_thirty][:percentage])}%",
                "#{@binders[:orphan][:last_ninety][:count]} | #{format_percent(@binders[:orphan][:last_ninety][:percentage])}%",
                "#{@binders[:orphan][:half_year][:count]} | #{format_percent(@binders[:orphan][:half_year][:percentage])}%",
                "#{@binders[:orphan][:year][:count]} | #{format_percent(@binders[:orphan][:year][:percentage])}%",
                "#{@binders[:orphan][:total][:count]} | #{format_percent(@binders[:orphan][:total][:percentage])}%"
            ]
        ]
        
        pdf.table data, :position => :center, :cell_style => { :size => 8}
    end
    
    def add_user_logins_totals
        
        pdf.move_down(25)
        
        pdf.formatted_text_box [
          { :text => "User Logins", :size => 14, :color => "000000", :font_style => "bold" },
        ], :at => [0, pdf.cursor], :width => 400
        
        pdf.move_down(25)
        
        data =  [
            ["User Logins", "Last 30 days", "Last 60 Days", "3 Month Average", "6 Month Average", "12 Month Average", "Total on site"],
            [
                "All Users", 
                "#{@users[:logins][:all][:last_thirty][:count]} | #{format_percent(@users[:logins][:all][:last_thirty][:percentage])}%", 
                "#{@users[:logins][:all][:last_thirty][:count]} | #{format_percent(@users[:logins][:all][:last_thirty][:percentage])}%", 
                "#{@users[:logins][:all][:last_thirty][:count]} | #{format_percent(@users[:logins][:all][:last_thirty][:percentage])}%",
                "#{@users[:logins][:all][:last_thirty][:count]} | #{format_percent(@users[:logins][:all][:last_thirty][:percentage])}%",
                "#{@users[:logins][:all][:last_thirty][:count]} | #{format_percent(@users[:logins][:all][:last_thirty][:percentage])}%",
                "#{@users[:logins][:all][:last_thirty][:count]} | #{format_percent(@users[:logins][:all][:last_thirty][:percentage])}%",
            ],
            [
                "Organic Users",
                "#{@users[:logins][:organic][:last_thirty][:count]} | #{format_percent(@users[:logins][:organic][:last_thirty][:percentage])}%", 
                "#{@users[:logins][:organic][:last_sixty][:count]} | #{format_percent(@users[:logins][:organic][:last_sixty][:percentage])}%", 
                "#{@users[:logins][:organic][:last_ninety][:count]} | #{format_percent(@users[:logins][:organic][:last_ninety][:percentage])}%", 
                "#{@users[:logins][:organic][:half_year][:count]} | #{format_percent(@users[:logins][:organic][:half_year][:percentage])}%", 
                "#{@users[:logins][:organic][:year][:count]} | #{format_percent(@users[:logins][:organic][:year][:percentage])}%", 
                "#{@users[:logins][:organic][:total][:count]} | #{format_percent(@users[:logins][:organic][:total][:percentage])}%"]
        ]
        
        pdf.table data, :position => :center, :cell_style => { :size => 8}
    end
    
    def add_no_logins_totals
        
        pdf.move_down(25)
        
        pdf.formatted_text_box [
          { :text => "Users with No Logins", :size => 14, :color => "000000", :font_style => "bold" },
        ], :at => [0, pdf.cursor], :width => 400
        
        pdf.move_down(25)
        
        data =  [
            ["Users with No Logins", "Total"],
            ["All Users", @users[:nologins][:all][:count]],
            ["Organic Users", @users[:nologins][:organic][:count]],
            ["Partners", @users[:nologins][:partners][:count]],
            ["Agents", @users[:nologins][:agents][:count]],
            ["Inspectors", @users[:nologins][:inspectors][:count]]
        ]
        
        pdf.table data, :position => :center, :cell_style => { :size => 8}
    end
    
    def add_binders_created_by_inspectors
        
        pdf.move_down(25)
        
        pdf.formatted_text_box [
          { :text => "Binders created by Home Inspectors", :size => 14, :color => "000000", :font_style => "bold" },
        ], :at => [0, pdf.cursor], :width => 400
        
        pdf.move_down(25)
        
        data =  [
            ["Week", "Total Accepted by Homeowners", "Total Created", "Percentage"]
        ]
        
        @inspectors[:binders].each do |week|
            data += [
                [
                    "#{week[:week_start].strftime("%B %d, %Y")} - #{week[:week_end].strftime("%B %d, %Y")}", 
                    week[:accepted].to_s, 
                    week[:created].to_s, 
                    "#{format_percent(week[:percentage]).to_s}%"
                ]
            ]
        end
        
        pdf.table data, :position => :center, :cell_style => { :size => 8}
    end
    
    def add_logins_for_users_created_by_inspectors
        
        pdf.move_down(25)
        
        pdf.formatted_text_box [
          { :text => "Homeowners created by Inspectors Logins - Week to Week", :size => 14, :color => "000000", :font_style => "bold" },
        ], :at => [0, pdf.cursor], :width => 400
        
        pdf.move_down(25)
        
        data =  [
            ["Week", "Total Logins"]
        ]

        @inspectors[:logins].each do |week|
            data += [
                [
                    "#{week[:week_start].strftime("%B %d, %Y")} - #{week[:week_end].strftime("%B %d, %Y")}", 
                    week[:logins].to_s
                ]
            ]
        end
        
        pdf.table data, :position => :center, :cell_style => { :size => 8}
    end
    
    def add_footer
        # footer
        pdf.repeat(:all, :dynamic => true) do
          pdf.formatted_text_box [
            { :text => "Report Date: #{Date.today.strftime("%B %d, %Y")}", :size => 8, :color => "686868" }
          ], :at => [pdf.bounds.right-115, -65], :width => 150, :height => 50
        end
    end
    
    def format_percent(percentage)
        (percentage * 100).round(2)
    end
end