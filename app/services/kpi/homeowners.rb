module Kpi::Homeowners
    
    include Kpi::Global::Constants
    include Kpi::Global::Methods
    
    # homeowner interaction kpi
    def get_homeowner_interaction
        # get count of all homeowner users who have signed in the past 60 days
        count = User.where(:role => "homeowner").where(CREATED_BY_LESS, 60.days.ago).where(last_sign_in_at: 60.days.ago..Date.today).count
        # get count of all users who have signed in the past 60 days
        total = User.where(:role => "homeowner").where(CREATED_BY_LESS, 60.days.ago).count
        return count.to_f/total.to_f
    end
    
    # homeowners who have accepted binders created by a specific partner type
    def homeowner_acceptance_rate_by_partner_type(partner_type)
        # gets rates from 6 weeks ago
        week_start = 6.weeks.ago.beginning_of_week(start_day = :sunday)
        week_end = 6.weeks.ago.end_of_week(start_day = :sunday)
        # set the dates
        dates = [(week_start - 60.days)..week_end]
        # get the correct clause
        clause = get_clause(partner_type)
        # get the number of binders    
        count = Binder.where(clause).where(created_at: dates).count
        ids = Binder.where(clause).where(created_at: dates).pluck(:id)
        users = homeowners_with_binders_created_by_partner_type(ids, partner_type)
        return percent(users.to_f/count.to_f)
    end
    
    # homeowners with binders created by a specific partner type
    def homeowners_with_binders_created_by_partner_type(ids, partner_type)
        clause = get_clause(partner_type)
        return User.joins(:binders).where(:role => "homeowner").where(:binders => clause).where(:binders => {:id => ids}).where(SIGN_IN_COUNT).count
    end
    
end