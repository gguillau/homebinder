module Kpi::Users
    
    include Kpi::Global::Constants
    
    def get_user_numbers
        return {
            count: get_user_count, 
            logins: get_logins,
            nologins: get_no_logins
        }
    end
    
    def get_user_count
        return {
            all: get_all_users_count,
            agents: get_agents_count,
            inspectors: get_inspectors_count,
            partners: get_partner_count
        }
    end
    
    def get_all_users_count
        return {
            last_seven: User.where(created_at: 7.days.ago..Time.now).count,
            last_thirty: User.where(created_at: 30.days.ago..Time.now).count,
            last_ninety: (User.where(created_at: 90.days.ago..Time.now).count/3),
            half_year: (User.where(created_at: 6.months.ago..Time.now).count/6),
            year: (User.where(created_at: 1.year.ago..Time.now).count/12),
            total: User.all.count
        }
    end
    
    def get_partner_count
        return {
            last_seven: Partner.where(created_at: 7.days.ago..Time.now).count,
            last_thirty: Partner.where(created_at: 30.days.ago..Time.now).count,
            last_ninety: (Partner.where(created_at: 90.days.ago..Time.now).count/3),
            half_year: (Partner.where(created_at: 6.months.ago..Time.now).count/6),
            year: (Partner.where(created_at: 1.year.ago..Time.now).count/12),
            total: Partner.all.count
        }
    end 
    
    def get_agents_count
        return {
            last_seven: Partner.where(created_at: 7.days.ago..Time.now).where(:partner_type => BROKER).count,
            last_thirty: Partner.where(created_at: 30.days.ago..Time.now).where(:partner_type => BROKER).count,
            last_ninety: (Partner.where(created_at: 90.days.ago..Time.now).where(:partner_type => BROKER).count/3),
            half_year: (Partner.where(created_at: 6.months.ago..Time.now).where(:partner_type => BROKER).count/6),
            year: (Partner.where(created_at: 1.year.ago..Time.now).where(:partner_type => BROKER).count/12),
            total: Partner.where(:partner_type => BROKER).count
        }
    end
    
    def get_inspectors_count
        return {
            last_seven: Partner.where(created_at: 7.days.ago..Time.now).where(:partner_type => INSPECTOR).count,
            last_thirty: Partner.where(created_at: 30.days.ago..Time.now).where(:partner_type => INSPECTOR).count,
            last_ninety: (Partner.where(created_at: 90.days.ago..Time.now).where(:partner_type => INSPECTOR).count/3),
            half_year: (Partner.where(created_at: 6.months.ago..Time.now).where(:partner_type => INSPECTOR).count/6),
            year: (Partner.where(created_at: 1.year.ago..Time.now).where(:partner_type => INSPECTOR).count/12),
            total: Partner.where(:partner_type => INSPECTOR).count
        }
    end
    
    def get_logins
        return {
            all: get_all_logins,
            organic: get_organic_logins,
            inspectors: get_inspectors_logins,
            agents: get_agents_logins,
            partners: get_partners_logins
        }
    end
    
    def get_no_logins
        return {
            all: {count: User.where(LAST_SIGN_IN_IS_NULL).count},
            organic: get_organic_nologin,
            partners: {count: User.where(LAST_SIGN_IN_IS_NULL).where("role NOT IN (?)", ["admin", "homeowner", "agent"]).count},
            inspectors: {count: User.where(LAST_SIGN_IN_IS_NULL).where(:role => "inspector").count},
            agents: {count: User.where(LAST_SIGN_IN_IS_NULL).where(:role => "broker").count},
        }
    end
    
    def get_all_logins
        @totalusers = User.all.count
        @total = User.where(LAST_SIGN_IN_IS_NOT_NULL).count
        @thirty = User.where(LAST_SIGN_IN_IS_NOT_NULL).where(last_sign_in_at: 30.days.ago..Time.now).count
        @sixty = User.where(LAST_SIGN_IN_IS_NOT_NULL).where(last_sign_in_at: 60.days.ago..Time.now).count
        @ninety = User.where(LAST_SIGN_IN_IS_NOT_NULL).where(last_sign_in_at: 90.days.ago..Time.now).count
        @half_year = User.where(LAST_SIGN_IN_IS_NOT_NULL).where(last_sign_in_at: 360.days.ago..Time.now).count
        @year = User.where(LAST_SIGN_IN_IS_NOT_NULL).where(last_sign_in_at: 1.year.ago..Time.now).count
        
        return {
            last_thirty: {count: @thirty, percentage: 1},
            last_sixty: {count: @sixty, percentage: 1},
            last_ninety: {count: @ninety, percentage: 1},
            half_year: {count: @half_year, percentage: 1},
            year: {count: @year, percentage: 1},
            total: {count: @total, percentage: 1}
        }
    end
    
    def get_organic_logins
        last_30 = {last_sign_in_at: 30.days.ago..Time.now}
        last_60 = {last_sign_in_at: 60.days.ago..Time.now}
        last_90 = {last_sign_in_at: 90.days.ago..Time.now}
        last_6_months = {last_sign_in_at: 6.months.ago..Time.now}
        last_year = {last_sign_in_at: 1.year.ago..Time.now}
        
        
        total = User.where(:role => "homeowner", :create_method => "register").where(LAST_SIGN_IN_IS_NOT_NULL).count
        thirty = User.where(:role => "homeowner", :create_method => "register").where(LAST_SIGN_IN_IS_NOT_NULL).where(last_30).count
        sixty = User.where(:role => "homeowner", :create_method => "register").where(LAST_SIGN_IN_IS_NOT_NULL).where(last_60).count
        ninety = User.where(:role => "homeowner", :create_method => "register").where(LAST_SIGN_IN_IS_NOT_NULL).where(last_90).count
        half_year = User.where(:role => "homeowner", :create_method => "register").where(LAST_SIGN_IN_IS_NOT_NULL).where(last_6_months).count
        year = User.where(:role => "homeowner", :create_method => "register").where(LAST_SIGN_IN_IS_NOT_NULL).where(last_year).count
        
        return {
            last_thirty: {count: thirty, percentage: thirty.to_f/@thirty.to_f},
            last_sixty: {count: sixty, percentage: sixty.to_f/@sixty.to_f},
            last_ninety: {count: ninety, percentage: ninety.to_f/@ninety.to_f},
            half_year: {count: half_year, percentage: half_year.to_f/@half_year.to_f},
            year: {count: year, percentage: year.to_f/@year.to_f},
            total: {count: total, percentage: total.to_f/@total.to_f}
        }
    end
    
    def get_organic_nologin
        return {count: User.where(:role => "homeowner", :create_method => "register").where(LAST_SIGN_IN_IS_NOT_NULL).count}
    end
    
    def get_inspectors_logins
        @total = User.where(:role => "inspector").where(LAST_SIGN_IN_IS_NOT_NULL).count
        @thirty = User.where(:role => "inspector").where(LAST_SIGN_IN_IS_NOT_NULL).where(last_sign_in_at: 30.days.ago..Time.now).distinct.count
        @sixty = User.where(:role => "inspector").where(LAST_SIGN_IN_IS_NOT_NULL).where(last_sign_in_at: 60.days.ago..Time.now).distinct.count
        @ninety = User.where(:role => "inspector").where(LAST_SIGN_IN_IS_NOT_NULL).where(last_sign_in_at: 90.days.ago..Time.now).distinct.count
        @half_year = User.where(:role => "inspector").where(LAST_SIGN_IN_IS_NOT_NULL).where(last_sign_in_at: 6.months.ago..Time.now).distinct.count
        @year = User.where(:role => "inspector").where(LAST_SIGN_IN_IS_NOT_NULL).where(last_sign_in_at: 1.year.ago..Time.now).distinct.count
        return {
            last_thirty: @thirty.to_f/@total.to_f,
            last_sixty: @sixty.to_f/@total.to_f,
            last_ninety: @ninety.to_f/@total.to_f,
            half_year: @half_year.to_f/@total.to_f,
            year: @year.to_f/@total.to_f,
        }
    end
    
    def get_agents_logins
        @total = User.where(:role => "broker").where(LAST_SIGN_IN_IS_NOT_NULL).count
        @thirty = User.where(:role => "broker").where(LAST_SIGN_IN_IS_NOT_NULL).where(last_sign_in_at: 30.days.ago..Time.now).distinct.count
        @sixty = User.where(:role => "broker").where(LAST_SIGN_IN_IS_NOT_NULL).where(last_sign_in_at: 60.days.ago..Time.now).distinct.count
        @ninety = User.where(:role => "broker").where(LAST_SIGN_IN_IS_NOT_NULL).where(last_sign_in_at: 90.days.ago..Time.now).distinct.count
        @half_year = User.where(:role => "broker").where(LAST_SIGN_IN_IS_NOT_NULL).where(last_sign_in_at: 6.months.ago..Time.now).distinct.count
        @year = User.where(:role => "broker").where(LAST_SIGN_IN_IS_NOT_NULL).where(last_sign_in_at: 1.year.ago..Time.now).distinct.count
        return {
            last_thirty: @thirty.to_f/@total.to_f,
            last_sixty: @sixty.to_f/@total.to_f,
            last_ninety: @ninety.to_f/@total.to_f,
            half_year: @half_year.to_f/@total.to_f,
            year: @year.to_f/@total.to_f,
        }
    end

    def get_partners_logins
        @total = User.where("role NOT IN (?)", ["admin", "homeowner", "broker"]).where(LAST_SIGN_IN_IS_NOT_NULL).count
        @thirty = User.where("role NOT IN (?)", ["admin", "homeowner", "broker"]).where(LAST_SIGN_IN_IS_NOT_NULL).where(last_sign_in_at: 30.days.ago..Time.now).distinct.count
        @sixty = User.where("role NOT IN (?)", ["admin", "homeowner", "broker"]).where(LAST_SIGN_IN_IS_NOT_NULL).where(last_sign_in_at: 60.days.ago..Time.now).distinct.count
        @ninety = User.where("role NOT IN (?)", ["admin", "homeowner", "broker"]).where(LAST_SIGN_IN_IS_NOT_NULL).where(last_sign_in_at: 90.days.ago..Time.now).distinct.count
        @half_year = User.where("role NOT IN (?)", ["admin", "homeowner", "broker"]).where(LAST_SIGN_IN_IS_NOT_NULL).where(last_sign_in_at: 6.months.ago..Time.now).distinct.count
        @year = User.where("role NOT IN (?)", ["admin", "homeowner", "broker"]).where(LAST_SIGN_IN_IS_NOT_NULL).where(last_sign_in_at: 1.year.ago..Time.now).distinct.count
        return {
            last_thirty: @thirty.to_f/@total.to_f,
            last_sixty: @sixty.to_f/@total.to_f,
            last_ninety: @ninety.to_f/@total.to_f,
            half_year: @half_year.to_f/@total.to_f,
            year: @year.to_f/@total.to_f,
        }
    end
end