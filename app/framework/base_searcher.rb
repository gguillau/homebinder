module BaseSearcher

    def self.included(klass)
        klass.extend ClassMethods
    end

    module ClassMethods

        def base_query
            "CAST(#{self.table_name}.id as varchar(25)) LIKE :search"
        end

        def search(params = {}, hb_request = nil)
            # get the search value
            searchValue = params[:search]

            # set the includes/joins arrays
            includes = []
            joins = []

            # the custom args where/where_not clauses
            where = {}
            where_not = {}

            # get the base includes/joins and base query
            includes = get_includes
            joins = get_joins
            query = get_base_query

            add_custom_args(where, where_not, params, includes, joins)

            # search the system
            if searchValue.present?
                self.joins(joins)
                    .includes(includes)
                    .where(query, search: "%#{searchValue}%".downcase)
                    .where(where)
                    .where.not(where_not)
                    .references(includes)
                    .distinct
            else
                self.joins(joins)
                    .includes(includes)
                    .where(where)
                    .where.not(where_not)
                    .all
                    .distinct
            end
        end

        def get_base_query
            return {} if not self.respond_to? :base_query
            self.base_query
        end

        def get_includes
            return [] if not self.respond_to? :base_includes
            self.base_includes
        end

        def get_joins
            return [] if not self.respond_to? :base_joins
            self.base_joins
        end

        def add_custom_args(where, where_not, params, includes, joins)
            # check if the query_arguments method is defined
            return if not self.respond_to? :query_arguments

            # check if the argument is defined in the class as a query argument
            params.each do |arg|
                if query_arguments.include?(arg.first.to_s)
                    where.merge!(query_arguments_hash(arg.first.to_s, params[arg.first.to_sym]))
                elsif query_arguments.include?(arg)
                    where.merge!(query_arguments_hash(arg, params[arg]))
                end
            end
        end

        def elasticsearch(hb_request, params)
            hash = {
                "size" => 0,
                "query" => {
                    "bool" => {
                        "must" =>[]
                    }
                }
            }

            if params[:searchType] === "aggregation_field"
                hash[:aggs] = {
                    "2": {
                        "terms": {
                            "field": params[:field],
                            "size": params[:count],
                            "order": {
                                    "_count":"desc"
                            }
                        }
                    }
                }
            end
            
            if !params[:query].nil?
                hash["query"]["bool"]["must"].push({
                    "query_string" => {
                        "query" => params[:query]
                    }
                })
            end
            
            if !params[:range].nil?
                hash["query"]["bool"]["must"].push({
                    "range" => {
                        "#{params[:range]}" => {
                            "gte" => params[:gte],
                            "lte" => params[:lte],
                            "format" => "MM/dd/yyyy"
                        }
                    }
                })
            end
            
            begin
                if params[:searchType] === "aggregation"
                    response = self.__elasticsearch__.search hash
                    return response.results.response.response.hits.total
                elsif params[:searchType] === "aggregation_field"
                    response = self.__elasticsearch__.search hash
                    return response.results.response.response.aggregations["2"].buckets
                else
                    response = self.__elasticsearch__.search params[:search], size: params[:count].to_i || 10, from: (params[:page].to_i - 1) * (params[:count].to_i || 10)
                    return Page.new(response.results.response.records.to_a, response.results.total)
                end
            rescue => e
                raise BadRequestException.new(e.message)
            end
        end
    end
end
