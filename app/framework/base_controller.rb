module BaseController

    def index
        begin
            render :json => klass.index(self.hb_request, params.permit!)
        rescue => e
            raise BadRequestException.new(e.message)
        end
    end

    def create
        begin
            render :json => klass.build(self.hb_request, params)
        rescue => e
            case e
            when UnprocessableException
                raise UnprocessableException.new(e.resource)
            else
                raise BadRequestException.new(e.message)
            end
        end
    end

    def show
        begin
            render :json => klass.show(self.hb_request, params)
        rescue => e
            case
            when CanCan::AccessDenied
                raise
            else
                raise BadRequestException.new(e.message)
            end
        end
    end

    def update
        begin
            render :json => klass.update(self.hb_request, params)
        rescue => e
            case e
            when UnprocessableException
                raise UnprocessableException.new(e.resource)
            else
                raise BadRequestException.new(e.message)
            end
        end
    end

    def destroy
        begin
            klass.destroy(self.hb_request, params[:id])
            head :no_content
        rescue => e
            raise BadRequestException.new(e.message)
        end
    end

    private

    # returns model name for controller
    # ex: partners_controller will return Partner
    # custom controllers will need to define klass
    # or override controller methods

    def klass
        controller_name.classify.constantize
    end

end
