module BaseBuilder
    
    def self.included(klass)
        klass.extend ClassMethods 
    end
    
    # destroy item in the background
    
    def remove
        RemoveObjectJob.perform_async(self.class, id)
    end
  
    module ClassMethods
        
        def index(hb_request, params, search_method = :search)
            pager(hb_request, params, search_method)
        end
        
        def build(hb_request, params)
            raise BadRequestException.new "#{self.name.titleize} required" if params[symbol].nil?
            
            # create the new item
            item = self.new(params[symbol].permit!)
            
            # save the item
            if not item.save
                raise UnprocessableException.new(item)
            end
            
            if hb_request.present? && hb_request.user
                EventService.perform_async({:event_type => "engagement", :event_name => "#{item.class.table_name.singularize} created", :user_id => hb_request.user.id, :user_role => hb_request.user.role, :user_created_at => hb_request.user.created_at, :user_create_method => hb_request.user.create_method})
            end
            
            return item
        end
        
        def show(hb_request, params)
            raise BadRequestException.new "#{self.name.titleize} ID required" if params[:id].nil?
            
            # get the item
            item = find(params[:id])
            
            # check if the user can read it
            hb_request.ability.authorize! :read, item, :message => "You are not authorized to access #{self.name} ID: #{item.id}."
            
            if hb_request.present? && hb_request.user
                EventService.perform_async({:event_type => "engagement", :event_name => "#{item.class.table_name.singularize} viewed", :user_id => hb_request.user.id, :user_role => hb_request.user.role, :user_created_at => hb_request.user.created_at, :user_create_method => hb_request.user.create_method})
            end
            
            return item
        end
        
        def update(hb_request, params)
            raise BadRequestException.new "#{self.name.titleize} ID required" if params[:id].nil?
            raise BadRequestException.new "#{self.name.titleize} required" if params[symbol].nil?
            
            # get the item being updated
            item = find(params[:id])
            
            # check if the user can update the item
            hb_request.ability.authorize! :write, item, :message => "You are not authorized to update #{self.name} ID: #{item.id}."
        
            # update
            if item.update_attributes(params[symbol].permit!)
                if hb_request.present? && hb_request.user
                    EventService.perform_async({:event_type => "engagement", :event_name => "#{item.class.table_name.singularize} updated", :user_id => hb_request.user.id, :user_role => hb_request.user.role, :user_created_at => hb_request.user.created_at, :user_create_method => hb_request.user.create_method})
                end
                
                return item
            else
                raise UnprocessableException.new(item)
            end
        end
    
        def destroy(hb_request, id)
            # check for required fields
            raise BadRequestException.new "#{self.name.titleize} ID required" unless id.present?
            
            item = find_by_id(id)
            return if item.nil?
    
            hb_request.ability.authorize! :destroy, item, :message => "You are not authorized to delete #{self.name} ID: #{item.id}."

            item.remove
            
            if hb_request.present? && hb_request.user
                EventService.perform_async({:event_type => "engagement", :event_name => "#{item.class.table_name.singularize} deleted", :user_id => hb_request.user.id, :user_role => hb_request.user.role, :user_created_at => hb_request.user.created_at, :user_create_method => hb_request.user.create_method})
            end
        end
        
        private
        
        def symbol
            self.table_name.singularize.to_sym
        end
    end
end