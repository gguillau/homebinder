require "slack"

desc "This task is called by the Heroku scheduler add-on"
task :homebinder_recall_check => :environment do |task, args|
  begin
    Recalls::DownloadJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_daily_maintenance_check => :environment do |task, args|
  begin
    Binders::MaintenanceRemindersJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_inbox_scanner => :environment do |task, args|
  begin
    Email::InboxScannerJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_billing_account_check => :environment do |task, args|
  begin
    Accounts::AccountStatusJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_metric_emails => :environment do |task, args|
  begin
    Partners::EmailsJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_reset_maintenance_template_dates => :environment do |task, args|
  begin
    Templates::ResetDatesJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_automatic_transfers => :environment do |task, args|
  begin
    Partners::AutomaticTransfersJob.perform_async
  rescue => e
    case e
    when UnprocessableException
      handle_error(task, e.resource.errors.full_messages.first)
    else
      handle_error(task, e.message)
    end
  end
end

task :homebinder_update_partner_billing_accounts => :environment do |task, args|
  begin
    Partners::UpdateBillingAccountsJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_update_partner_accounts => :environment do |task, args|
  begin
    Partners::UpdateSalesforceAccountsJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_remove_expired_sessions => :environment do |task, args|
  begin
    Users::RemoveExpiredSessionsJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_monthly_email, [:task_name] => :environment do |task, args|
  begin
    Users::MonthlyEmailJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_monthly_agent_email, [:task_name] => :environment do |task, args|
  begin
    Agents::EmailJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_isn_rake => :environment do |task, args|
  begin
    Isn::RakeJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_notify_unverified_items => :environment do |task, args|
  begin
    Binders::NotifyUnverifiedItemsJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_sprint => :environment do |task, args|
  begin
    Rake::SprintJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_check_repair_pricer_status => :environment do |task, args|
  begin
    Partners::RepairPricerStatusJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_increase_billing => :environment do |task, args|
  begin
    Partners::IncreaseBillingJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_upgrade_binders => :environment do |task, args|
  begin
    Binders::UpgradeBindersJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_reset_passwords_fields => :environment do |task, args|
  begin
    Users::ResetPasswordsFieldsJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_call_broker_mint => :environment do |task, args|
  begin
    Partners::BrokerMintJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_process_inspection_reports => :environment do |task, args|
  begin
    Email::ProcessReportsJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_check_inspection_report_jobs => :environment do |task, args|
  begin
    Partners::WorkaroundJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_remove_repair_pricer_widgets => :environment do |task, args|
  begin
    Dashboards::RemoveRepairPricerWidgetsJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_maintenance_reminder_follow_up => :environment do |task, args|
  begin
    Binders::MaintenanceRemindersFollowUpJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

task :homebinder_maintenance_reminder_advance_notice => :environment do |task, args|
  begin
    Binders::MaintenanceRemindersAdvanceNoticeJob.perform_async
  rescue => e
    handle_error(task, e.message)
  end
end

def handle_error(task, message)
    Slack::MessagingJob.perform_async("#bugs", "#{Rails.env}: #{task.name.titleize} Error: #{message}")
    ErrorService.perform_async(message, {task: task.name.titleize})
end

Rake::Task.tasks.each do |t|
  if t.name != "before" || t.name != "after" and t.name.include? ("homebinder_")
    t.enhance do 
      Logger.new("#{Rails.root}/log/cron.log").info "#{t.inspect} was called on #{Time.now.in_time_zone("Eastern Time (US & Canada)").strftime("%A, %B %d, %Y %I:%M %p")}"
    end
  end
end