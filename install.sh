##!/bin/bash --login
# A sample Bash script to set up the HomeBinder repo, by Gerlin
cd client &&
echo "install node"
nvm install 6.17.1 &&
echo "install grunt, karma"
npm -g install grunt grunt-cli karma &&
echo "install yarn"
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add - &&
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list &&
sudo apt-get update && 
sudo apt-get install yarn && 
echo "install yarn dependencies"
nvm use 6.17.1 && yarn &&
echo "build public folder"
grunt deploy &&
echo "exit client folder"
cd .. &&
echo "install postgreqsl file"
sudo apt-get install libpq-dev &&
echo "start postgresql"
sudo service postgresql start &&
echo "alter user password"
sudo -u postgres psql -c "ALTER USER postgres WITH PASSWORD 'password';" &&
echo "Create database"
sudo -u postgres psql -c "CREATE DATABASE app_development;" &&
echo "Create database"
sudo -u postgres psql -c "CREATE DATABASE app_production;" &&
echo "Create database"
sudo -u postgres psql -c "CREATE DATABASE app_test;" &&
echo "postgresql"
sudo -u postgres psql -c "update pg_database set datallowconn = TRUE where datname = 'template0';" &&
echo "postgresql"
sudo -u postgres psql -c "\c template0" &&
echo "postgresql"
sudo -u postgres psql -c "update pg_database set datistemplate = FALSE where datname = 'template1';" &&
echo "postgresql"
sudo -u postgres psql -c "drop database template1;" &&
echo "postgresql"
sudo -u postgres psql -c "create database template1 with template = template0 encoding = 'UTF8';" &&
echo "postgresql"
sudo -u postgres psql -c "update pg_database set datistemplate = TRUE where datname = 'template1';" &&
echo "postgresql"
sudo -u postgres psql -c "\c template1" &&
echo "postgresql"
sudo -u postgres psql -c "update pg_database set datallowconn = FALSE where datname = 'template0';" &&
echo "updating environment"
sudo apt-get update &&
echo "install jre"
sudo apt-get install default-jre -y &&
echo "download java"
echo -ne '\n' | sudo add-apt-repository ppa:webupd8team/java &&
echo "install java"
echo -ne '\n' | sudo apt-get install oracle-java8-installer -y &&
echo "set up java config"
sudo update-alternatives --config java &&
echo "download elasticsearch"
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-5.6.0.deb &&
echo "install elasticsearch"
sudo dpkg -i elasticsearch-5.6.0.deb &&
echo "remove elasticsearch"
rm elasticsearch-5.6.0.deb &&
echo "set up symlink"
sudo ln -s /etc/elasticsearch/elasticsearch /usr/share/elasticsearch/config &&
echo "start elasticsearch"
sudo service elasticsearch start &&
echo "download kibana"
wget https://artifacts.elastic.co/downloads/kibana/kibana-5.6.0-amd64.deb &&
echo "install kibana"
sudo dpkg -i kibana-5.6.0-amd64.deb &&
echo "remove kibana package"
rm kibana-5.6.0-amd64.deb &&
echo "start kibana"
sudo service kibana start &&
echo "installing postgresql client"
sudo apt-get install -y postgresql postgresql-contrib &&
echo "install imagemagick"
echo 'y' | sudo apt-get install imagemagick &&
echo "install nodejs"
echo 'y' | sudo apt-get install nodejs &&
echo "downloading rvm"
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 &&
echo "updating rvm"
rvm requirements &&
echo "install ruby"
rvm install 2.3.8 &&
echo "rvm reload"
rvm reload &&
echo "source"
source /usr/local/rvm/scripts/rvm &&
source ~/.profile &&
echo "setting default ruby"
rvm use 2.3.8 --default &&
echo "install bundler"
gem install bundler &&
echo "install library for puma"
sudo apt-get install libssl1.0-de &&
echo "setting default ruby"
rvm use 2.3.8 --default &&
echo "bundle install"
bundle install &&
echo "setting up the database"
rake db:setup &&
echo "starting foreman"
foreman start -e .env