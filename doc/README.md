## Welcome to Rails API

Rails::API is a subset of a normal Rails application, created for applications 
that don't require all functionality that a complete Rails application provides. 
It is a bit more lightweight, and consequently a bit faster than a normal Rails 
application. At HomeBinder, we use Rails for our backend and Rails API is 
perfect since we don't need the entire Rails middleware stack nor template 
generation.

## Basics

Inside our /app folder, we have our controllers, mailers, models, serializers, 
services and validators. Since Rails is used only as an API, a basic request
from the client will hit our controllers and get routed to either model for 
CRUD actions or a service for non-CRUD actions. The data is then serialized by
one of our serializers in the serializers folder, returned to the controller
and then sent back as JSON to the client.

In Rails, the model is handled by what's called an object-relational mapping
layer entitled Active Record. This layer allows you to present the data from
database rows as objects and embellish these data objects with business logic
methods. You can read more about Active Record in
link:files/vendor/rails/activerecord/README.html.


## Description of Contents

```
HomeBinder
└───app
│   └───controllers
│   └───mailers
│   └───models
│   └───serializers
│   └───services
│   └───validators
|
└───config
│   └───environments
│   └───initializers
│   └───locales
|
└───db
│   └───migrate
|
└───doc
└───lib
└───log
└───public
└───script
└───spec
│   └───assets
│   └───controllers
│   └───factories
│   └───mailers
│   └───models
│   └───performance
│   └───rake
│   └───requests
│   └───serializers
│   └───services
│   └───support
│   └───validators
|
```

app
    
    Holds all the code that's specific to our HomeBinder application.

app/controllers

    Holds controllers that should be named like binders_controller.rb for
    automated URL mapping. All controllers should descend from our 
    HomeBinderController which descends from ApplicationController which itself 
    descends from ActionController::Base.

app/models

    Holds models that should be named like post.rb. Models descend from
    ActiveRecord::Base by default.
  
app/serializers

    We use using ActiveModel::Serializers to serialize our ActiveRecord 
    objects into JSON format

config

    Configuration files for the Rails environment, the routing map, the database,
    and other dependencies.

db

    Contains the database schema in schema.rb. db/migrate contains all the
    sequence of Migrations for our schema.

doc

    This directory contains all our documentation for the server side code.

lib

    Application specific libraries. Basically, any kind of custom code that
    doesn't belong under controllers, models, or helpers. This directory is in
    the load path.

public

    The directory available for the web server. Also contains the resulting
    build files from or client side.

script

    Helper scripts for automation and generation.

spec

    Contains all our server side unit tests


## Debugging Rails

Sometimes our application goes wrong. Fortunately there are a lot of tools that
will help you debug it and get it back on the rails.

First area to check is the development.log file in the /log directory. Rails 
will automatically display debugging and runtime information to this file. 

You can also log your own messages directly into the log file from your code
using the Ruby logger class from inside our controllers. Example:

    class Api::V1::ContractorTemplatesController < ActionController::Base
        def destroy
            Template::Contractor.destroy(@current_user, params)
            logger.info("#{Time.now} Destroyed Contractor Template ID #{params[:id]}!")
            head :no_content
        end
    end

The result will be a message in your log file along the lines of:

  Mon Oct 08 14:22:29 +1000 2007 Destroyed Contractor Template ID #1!

More information on how to use the logger is at http://www.ruby-doc.org/core/

Also, Ruby documentation can be found at http://www.ruby-lang.org/. There are
several books available online as well:

* Programming Ruby: http://www.ruby-doc.org/docs/ProgrammingRuby/ (Pickaxe)
* Learn to Program: http://pine.fm/LearnToProgram/ (a beginners guide)

These two books will bring you up to speed on the Ruby language and also on
programming in general.


## Console

The console is a Ruby shell, which allows you to interact with our 
application's domain model. Here you'll have all parts of the application
configured, just like it is when the application is running. You can inspect
domain models, change values, and save to the database. Starting the script
without arguments will launch it in the development environment.

To start the console, run `rails console` or `rails c`
from the root directory.

Options:

* Passing the `-s`, `--sandbox` argument will rollback any modifications
  made to the database.
* Passing an environment name as an argument will load the corresponding
  environment. Example: `rails console production`.

To reload your controllers and models after launching the console run
`reload!`

More information about irb can be found at:
link:http://www.rubycentral.org/pickaxe/irb.html