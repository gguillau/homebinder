swagger: '2.0'
info:
  version: 1.0.0
  title: HomeBinder API
  description: 'this document describes the HomeBinder REST API. The current version is v1. All requests should be prefixed with "/api/v1/"'
basePath: /api/v1
tags:
  - name: User registration
    description: User registration API
  - name: Log on
    description: User logon API
  - name: Maintenance
    description: Maintenance APIs
  - name: Partners
    description: Partner APIs
paths:
  /registrations:
    post:
      tags:
        - User registration
      description: Registers a new user in HomeBinder
      parameters:
        - name: credentials
          description: The user credentials
          in: body
          required: true
          schema:
            $ref: '#/definitions/credentials'
      responses:
        '200':
          description: success
          schema:
            $ref: '#/definitions/user_token'
        '422':
          description: unprocessable entity.
          schema:
            $ref: '#/definitions/error'
  /user_tokens:
    post:
      tags:
        - Log on
      description: logs a user in
      parameters:
        - name: credentials
          description: The user credentials
          in: body
          required: true
          schema:
            $ref: '#/definitions/credentials'
      responses:
        '200':
          description: success
          schema:
            $ref: '#/definitions/user_token'
  /maintenance_items:
    get:
      tags:
        - Maintenance
      description: Gets all maintenance items for a binder
      parameters:
        - name: binder_id
          in: query
          description: The id of the binder owning the maintenance items
          type: integer
          required: true
      responses:
        '200':
          description: success
  '/binders/:binder_id/mainenance_items':
    get:
      tags:
        - Maintenance
      description: Gets all maintenance items for a binder
      responses:
        '200':
          description: success
  /partners:
    get:
      tags:
        - Partners
      description: TODO
      responses:
        '200':
          description: success
  '/partners/:id/configuration':
    get:
      tags:
        - Partners
      description: Gets the configuration object for a partner
      responses:
        '200':
          description: success
          schema:
            $ref: '#/definitions/partner_configuration'
        '403':
          description: the user does not have permission
        '404':
          description: the partner was not found
    put:
      tags:
        - Partners
      description: Updates a partners configuration. If the configuration does not exist a new one is created
      responses:
        '200':
          description: success
          schema:
            $ref: '#/definitions/partner_configuration'
        '403':
          description: the user does not have permission
        '404':
          description: the partner was not found
        '422':
          description: validation failed on the submitted configuration
  '/partners/:id/isn/footprints':
    get:
      tags:
        - Partners
      description: Gets all the footprints for a partner from ISN
      parameters:
      - name: id
        in: query
        description: The id of the partner
        type: integer
        required: true
      responses:
        200:
          description: success
        400:
          description: bad request. Occurs when the company key is not set, the api endpoint is not set or an error from ISN.
        403: 
          description: forbidden. The user doesn't have premission to read the partner
        404: 
          description: not found. The partner was not found
  '/partners/:id/isn/clients/:client_id':
    get:
      tags:
        - Partners
      description: Gets ISN client details.
      parameters:
      - name: id
        in: query
        description: The id of the partner
        type: integer
        required: true
      - name: client_id
        in: query
        description: The id of the client to retrieve. This is found in an ISN footprint
        type: integer
        required: true
      responses:
        200:
          description: success
        400:
          description: bad request. Occurs when the company key is not set, the api endpoint is not set or an error from ISN.
        403:
          description: forbidden. The user doesn't have permission to read the partner
        404:
          description: not found. The partner was not found
  '/partners/:id/isn/orders/:order_id':
    get:
      tags:
        - Partners
      description: Gets ISN order details.
      parameters:
      - name: id
        in: query
        description: The id of the partner
        type: integer
        required: true
      - name: order_id
        in: query
        description: The id of the order to retrieve. This is found in an ISN footprint
        type: integer
        required: true
      responses:
        200:
          description: success
        400:
          description: bad request. Occurs when the company key is not set, the api endpoint is not set or an error from ISN.
        403:
          description: forbidden. The user doesn't have permission to read the partner
        404:
          description: not found. The partner was not found
  '/partners/:id/isn/binder_info/:footprint_id':
    get:
      tags:
        - Partners
      description: Gets the binder information based on an ISN footprint.
      parameters:
      - name: id
        in: query
        description: The id of the partner
        type: integer
        required: true
      - name: footprint_id
        in: query
        description: The id of the footprint used to get the binder information.
        type: integer
        required: true
      responses:
        200:
          description: success
          schema:
            $ref: '#/definitions/isn_binder_info'
        400:
          description: bad request. Occurs when the company key is not set, the api endpoint is not set or an error from ISN.
        403:
          description: forbidden. The user doesn't have permission to read the partner
        404:
          description: not found. The partner was not found
definitions:
  error:
    type: object
    title: error
    properties:
      message:
        type: string
  credentials:
    type: object
    title: credentials
    properties:
      email:
        type: string
      password:
        type: string
  user_token:
    type: object
    title: user_token
    properties:
      jwt:
        type: string
  binder:
    type: object
    properties:
      name:
        type: string
      primary:
        type: boolean
      property:
        $ref: '#/definitions/property'
  property:
    type: object
    properties:
      address1:
        type: string
  partner_configuration:
    type: object
    title: configuration
    properties:
      id:
        type: integer
        description: the database id of the object
      partner_id:
        type: integer
        description: the id of the partner that owns the configuration
      maintenance_note:
        type: string
        description: a note to appear on maintenance emails sent for tasks entered by the partner.
  isn_binder_info:
    type: object
    title: binder_info
    properties:
      client:
        type: object
        properties:
          name:
            type: string
          email:
            type: string
      property:
        type: object
        properties:
          address1:
            type: string
          address2:
            type: string
          city:
            type: string
          state:
            type: string
          zip:
            type: string
