require 'rails_helper'

RSpec.describe DashboardWidgetSlimSerializer, :type => :serializer do
    
    it "returns fields" do
        dashboard_widget = create(:dashboard_widget)
        serializer = DashboardWidgetSlimSerializer.new(dashboard_widget)
        expect(serializer.serializable_hash[:widget_id]).to_not be(nil)
    end

end