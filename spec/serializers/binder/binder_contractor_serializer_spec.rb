require 'rails_helper'

RSpec.describe Binder::BinderContractorSerializer, :type => :serializer do
    
    context "basic test" do
        it "returns preferred partner field" do
            partner = create(:partner)
            binder_template = create(:binder_template, :partner_configuration => partner.partner_configuration)
            contractor_template = create(:contractor_template, :binder_template => binder_template)
            binder_contractor = create(:binder_contractor, :library_source_id => contractor_template.id)
            
            serializer = Binder::BinderContractorSerializer.new(binder_contractor, {:scope => nil})
            expect(serializer.serializable_hash[:preferred_partner]).to_not be_nil
        end
        
        it "returns preferred agent field" do
            agent = create(:user, :role => 'agent')
            user_contractor = create(:user_contractor, :user => agent)
            binder_contractor = create(:binder_contractor, :user_contractor => user_contractor)
            
            serializer = Binder::BinderContractorSerializer.new(binder_contractor, {:scope => nil})
            expect(serializer.serializable_hash[:preferred_agent]).to_not be_nil
        end
    end
end