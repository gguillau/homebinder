require 'rails_helper'

RSpec.describe SellerReportSerializer, :type => :serializer do
    
    it "returns documents" do
        binder = create(:binder)
        seller_report = create(:seller_report, :binder => binder)
        create_list(:appliance, 2, :binder => binder)
        create_list(:maintenance_item, 2, :binder => binder)
        create_list(:binder_contractor, 2, :binder => binder)
        create_list(:project, 2, :binder => binder, :status => "Completed")
        create_list(:paint, 2, :binder => binder)
        create_list(:finish, 2, :binder => binder)
        create_list(:document, 2, :binder => binder)
        create_list(:image, 2, :binder => binder)
        create_list(:permit, 2, :binder => binder)
        binder.appliances.each do |app|
            create(:seller_report_item, :appliance_id => app.id, :include => true)
        end
        binder.maintenance_items.each do |item|
            create(:seller_report_item, :maintenance_item_id => item.id, :include => true)
        end
        binder.projects.each do |project|
            create(:seller_report_item, :project_id => project.id, :include => true)
        end
        binder.binder_contractors.each do |binder_contractor|
            create(:seller_report_item, :binder_contractor_id => binder_contractor.id, :include => true)
        end
        binder.paints.each do |paint|
            create(:seller_report_item, :paint_id => paint.id, :include => true)
        end
        binder.finishes.each do |finish|
            create(:seller_report_item, :finish_id => finish.id, :include => true)
        end
        binder.documents.each do |doc|
            create(:seller_report_item, :document_id => doc.id, :include => true)
        end
        binder.images.each do |img|
            create(:seller_report_item, :image_id => img.id, :include => true)
        end
        binder.permits.each do |permit|
            create(:seller_report_item, :permit_id => permit.id, :include => true)
        end
  
        serializer = SellerReportSerializer.new(seller_report, {:scope => {:action => "show", :controller => ["seller_reports"]}})
        expect(serializer.serializable_hash[:documents].length).to eq(2)
    end
end