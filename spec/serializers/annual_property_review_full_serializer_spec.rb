require 'rails_helper'

RSpec.describe AnnualPropertyReviewFullSerializer, :type => :serializer do
    
    it "returns fields" do
        inspector = create(:user, :role => "inspector")
        apr = create(:annual_property_review, :reviewer_id => inspector.id)
        serializer = AnnualPropertyReviewFullSerializer.new(apr)
        expect(serializer.serializable_hash[:reviewer]).to_not be(nil)
    end

end