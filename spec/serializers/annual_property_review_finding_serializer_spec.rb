require 'rails_helper'

RSpec.describe AnnualPropertyReviewFindingSerializer, :type => :serializer do
    
    it "returns fields" do
        partner = create(:partner)
        create(:address, :partner => partner)
        template = partner.partner_configuration.generate_default_template
        contractor = create(:contractor_template, :binder_template => template)
        annual_property_review_finding = create(:annual_property_review_finding, :recommended_homepro_id => contractor.id)
        serializer = AnnualPropertyReviewFindingSerializer.new(annual_property_review_finding)
        expect(serializer.serializable_hash[:recommended_homepro]).to_not be(nil)
    end

end