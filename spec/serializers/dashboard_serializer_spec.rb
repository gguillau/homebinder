require 'rails_helper'

RSpec.describe DashboardSerializer, :type => :serializer do
    
    it "returns fields" do
        admin = create(:user, :role => "admin")
        partner = create(:partner)
        dashboard = create(:dashboard, :partner => partner, :system => false)
        serializer = DashboardSerializer.new(dashboard, {:scope => {:current_user => admin, :ability => admin.ability}})
        expect(serializer.serializable_hash[:partner]).to_not be(nil)
    end
    
    it "returns fields" do
        admin = create(:user, :role => "admin")
        organization = create(:organization)
        dashboard = create(:dashboard, :organization => organization, :system => false)
        serializer = DashboardSerializer.new(dashboard, {:scope => {:current_user => admin, :ability => admin.ability}})
        expect(serializer.serializable_hash[:organization]).to_not be(nil)
    end

end