require 'rails_helper'

RSpec.describe BinderSerializer, :type => :serializer do
    
    context "basic test" do
        it "returns name field" do
            binder = create(:binder)
            serializer = BinderSerializer.new(binder)
            expect(serializer.serializable_hash[:name]).to eq binder.name
        end
        it "returns large image for show action" do
            homeowner = create(:user, :role => "homeowner")
            binder = create(:binder)
            image = create(:image, :binder_id => binder.id)
            image.file = File.new("spec/assets/SampleImage.png")
            image.save!
            binder.hero_image_id = image.id
            binder.save!
                serializer = BinderSerializer.new(binder, {:scope => {:current_user => homeowner, :action => "show", :controller => ["binders"]}})
            expect(serializer.serializable_hash[:hero_photo]).to_not be(nil)
        end
        it "returns large image for seller report show action" do
            homeowner = create(:user, :role => "homeowner")
            binder = create(:binder)
            image = create(:image, :binder_id => binder.id)
            image.file = File.new("spec/assets/SampleImage.png")
            image.save!
            binder.hero_image_id = image.id
            binder.save!
                serializer = BinderSerializer.new(binder, {:scope => {:current_user => homeowner, :action => "show", :controller => ["seller_reports"]}})
            expect(serializer.serializable_hash[:hero_photo]).to_not be(nil)
        end
    end
    
    context "when a partner" do
        it "returns transfer, status for show" do
            inspector = create(:user, :role => "inspector")
            homeowner = create(:user, :role => "homeowner")
            partner = create(:partner, :partner_type => "inspector")
            binder = create(:binder)
            create(:transfer, :status => "created", :sender => inspector, :receiver => homeowner, :binder => binder)
            create(:partner_user, :user => inspector, :partner => partner, :role => "admin")
            create(:user_binder, :user => inspector, :binder => binder, :role => "owner")
                serializer = BinderSerializer.new(binder, {:scope => {:current_user => inspector}})
            expect(serializer.serializable_hash[:name]).to eq binder.name
        end
        it "returns transfer, status for index" do
            inspector = create(:user, :role => "inspector", :user_profile_attributes => {:company => "Test Company"})
            homeowner = create(:user, :role => "homeowner")
            partner = create(:partner, :partner_type => "inspector")
            binder = create(:binder, :created_by => inspector.id)
            create(:transfer, :status => "sent", :sender => inspector, :receiver => homeowner, :binder => binder)
            create(:partner_user, :user => inspector, :partner => partner, :role => "admin")
            create(:user_binder, :user => homeowner, :binder => binder, :role => "owner")
                serializer = BinderSerializer.new(binder, {:scope => {:current_user => inspector, :action => "index"}})
            expect(serializer.serializable_hash[:name]).to eq binder.name
            expect(serializer.serializable_hash[:creator]).to eq "Test Company"
        end
        it "returns transfer, status for index" do
            inspector = create(:user, :role => "inspector", :user_profile_attributes => {:company => "Test Company"})
            homeowner = create(:user, :role => "homeowner")
            partner = create(:partner, :partner_type => "inspector")
            binder = create(:binder, :created_by => inspector.id)
            create(:share, :status => "shared", :sender => inspector, :receiver => homeowner, :binder => binder)
            create(:transfer, :status => "created", :sender => inspector, :receiver => homeowner, :binder => binder)
            create(:partner_user, :user => inspector, :partner => partner, :role => "admin")
            create(:user_binder, :user => homeowner, :binder => binder, :role => "owner")
            serializer = BinderSerializer.new(binder, {:scope => {:current_user => inspector, :action => "index"}})
            expect(serializer.serializable_hash[:name]).to eq binder.name
            expect(serializer.serializable_hash[:creator]).to eq "Test Company"
            expect(serializer.serializable_hash[:status]).to eq "shared"
        end
    end
    
    context "when an agent" do
        it "returns transfer, status for show" do
            agent = create(:user, :role => "agent")
            homeowner = create(:user, :role => "homeowner")
            partner = create(:partner, :partner_type => "inspector")
            create(:partner_user, :user => agent, :partner => partner, :role => "agent")
            binder = create(:binder)
            create(:transfer, :status => "created", :sender => agent, :receiver => homeowner, :binder => binder)
            create(:user_binder, :user => agent, :binder => binder, :role => "owner")
                serializer = BinderSerializer.new(binder, {:scope => {:current_user => agent}})
            expect(serializer.serializable_hash[:name]).to eq binder.name
        end
        it "returns transfer, status for index" do
            agent = create(:user, :role => "agent")
            homeowner = create(:user, :role => "homeowner")
            partner = create(:partner, :partner_type => "inspector")
            create(:partner_user, :user => agent, :partner => partner, :role => "agent")
            binder = create(:binder)
            create(:transfer, :status => "sent", :sender => agent, :receiver => homeowner, :binder => binder)
            create(:user_binder, :user => homeowner, :binder => binder, :role => "owner")
                serializer = BinderSerializer.new(binder, {:scope => {:current_user => agent, :action => "index"}})
            expect(serializer.serializable_hash[:name]).to eq binder.name
        end
    end
    
    context "when an admin" do
        it "returns owner for show" do
            admin = create(:user, :role => "admin")
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            friend = create(:user, :role => "homeowner")
            create(:user_binder, :user => homeowner, :binder => binder, :role => "owner")
            create(:transfer, :status => "created", :sender => homeowner, :receiver => friend, :binder => binder)
                serializer = BinderSerializer.new(binder, {:scope => {:current_user => admin}})
            expect(serializer.serializable_hash[:name]).to eq binder.name
            expect(serializer.serializable_hash[:owner]["id"]).to eq homeowner.id
        end
        it "returns owner for index" do
            admin = create(:user, :role => "admin")
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            friend = create(:user, :role => "homeowner")
            create(:user_binder, :user => friend, :binder => binder, :role => "owner")
            create(:transfer, :status => "sent", :sender => homeowner, :receiver => friend, :binder => binder)
                serializer = BinderSerializer.new(binder, {:scope => {:current_user => admin, :action => "index"}})
            expect(serializer.serializable_hash[:name]).to eq binder.name
            expect(serializer.serializable_hash[:owner]["id"]).to eq friend.id
        end
    end
    
    context "when a homeowner" do
        it "returns subscription and access_list for show" do
            homeowner = create(:user, :role => "homeowner")
            binder = create(:binder)
            image = create(:image, :binder => binder)
            create(:subscription, :binder => binder)
                binder.hero_image_id = image.id
            binder.save
                serializer = BinderSerializer.new(binder, {:scope => {:current_user => homeowner}})
            expect(serializer.serializable_hash[:name]).to eq binder.name
        end
    end
end