require 'rails_helper'

RSpec.describe RecallSerializer, :type => :serializer do
    

    it "returns appliance_recalls field" do
        recall = create(:recall)
        serializer = RecallSerializer.new(recall, {:scope => {:action => "show"}})
        expect(serializer.serializable_hash[:appliance_recalls]).to eq []
    end
end