require 'rails_helper'

RSpec.describe PartnerAprSerializer, :type => :serializer do
    
    it "returns logo" do
        user = create(:user, :role => "inspector")
        partner = create(:partner)
        create(:partner_user, :user => user, :partner => partner, :role => "admin")
        allow(partner.logo).to receive(:present?).and_return(true)
        allow(partner.logo).to receive(:expiring_cloud_front_url).and_return("")
        serializer = PartnerAprSerializer.new(partner, {:scope => {:current_user => user, :action => "index"}})
        expect(serializer.serializable_hash[:logo_file]).to eq("")
    end
end