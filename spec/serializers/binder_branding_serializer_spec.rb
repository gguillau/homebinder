require 'rails_helper'

RSpec.describe Binder::BinderBrandingSerializer, :type => :serializer do
    
    it "returns user field" do
        inspector = create(:user, :role => "inspector")
        partner = create(:partner)
        create(:partner_user, :partner => partner, :user => inspector, :role => "admin")
        binder_branding = create(:binder_branding, :user_branding_id => inspector.user_profile.id, :partner_id => partner.id)
        serializer = Binder::BinderBrandingSerializer.new(binder_branding)
        expect(serializer.serializable_hash[:user][:email]).to eq(inspector.email)
    end
    
    it "returns admin user field" do
        inspector = create(:user, :role => "inspector")
        partner = create(:partner)
        inspector2 = create(:user, :role => "inspector")
        create(:partner_user, :partner => partner, :user => inspector2, :role => "admin")
        binder_branding = create(:binder_branding, :user_branding_id => inspector.user_profile.id, :partner_id => partner.id)
        inspector.destroy
                serializer = Binder::BinderBrandingSerializer.new(binder_branding)
        expect(serializer.serializable_hash[:user][:email]).to eq(inspector2.email)
    end
    
    it "returns nil when both user and admin are destroyed" do
        inspector = create(:user, :role => "inspector")
        partner = create(:partner)
        inspector2 = create(:user, :role => "inspector")
        partner_user = create(:partner_user, :partner => partner, :user => inspector2, :role => "admin")
        binder_branding = create(:binder_branding, :user_branding_id => inspector.user_profile.id, :partner_id => partner.id)
        inspector.destroy
        inspector2.destroy
        binder_branding.reload
        serializer = Binder::BinderBrandingSerializer.new(binder_branding)
        expect(serializer.serializable_hash[:user][:email]).to eq(inspector2.email)
    end
end