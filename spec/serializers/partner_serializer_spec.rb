require 'rails_helper'

RSpec.describe PartnerSerializer, :type => :serializer do
    
    it "returns warranty fields even when scope is nil" do
        user = create(:user, :role => "inspector")
        partner = create(:partner)
        create(:partner_user, :user => user, :partner => partner, :role => "admin")
        warranty_configuration = create(:warranty_configuration, :partner => partner)
        serializer = PartnerSerializer.new(partner, {:scope => nil})
        expect(serializer.serializable_hash[:warranties_configuration][:id]).to eq(warranty_configuration.id)
    end
    
    it "returns warranty fields" do
        user = create(:user, :role => "inspector")
        partner = create(:partner)
        create(:partner_user, :user => user, :partner => partner, :role => "admin")
        warranty_configuration = create(:warranty_configuration, :partner => partner)
        serializer = PartnerSerializer.new(partner, {:scope => {:current_user => user}})
        expect(serializer.serializable_hash[:warranties_configuration][:id]).to eq(warranty_configuration.id)
    end
    
    it "returns account fields" do
        user = create(:user, :role => "inspector")
        account = create(:account)
        partner = create(:partner, :account_id => account.id)
        create(:partner_user, :user => user, :partner => partner, :role => "admin")
        serializer = PartnerSerializer.new(partner, {:scope => {:current_user => user}})
        expect(serializer.serializable_hash[:account][:id]).to eq(account.id)
    end
    
    it "returns admin email and logo" do
        user = create(:user, :role => "inspector")
        partner = create(:partner)
        create(:partner_user, :user => user, :partner => partner, :role => "admin")
        allow(partner.logo).to receive(:present?).and_return(true)
        allow(partner.logo).to receive(:expiring_cloud_front_url).and_return("")
        serializer = PartnerSerializer.new(partner, {:scope => {:current_user => user, :action => "index"}})
        expect(serializer.serializable_hash[:admin_email]).to eq(user.email)
        expect(serializer.serializable_hash[:logo_file]).to eq("")
    end
end