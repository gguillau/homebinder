require 'rails_helper'

RSpec.describe Partner::Automation::ErrorSerializer, :type => :serializer do
    
    it "returns phone object" do
        automation_error = create(:automation_error)
        serializer = Partner::Automation::ErrorSerializer.new(automation_error)
        expect(serializer.serializable_hash["agent_phone"]).to_not be(nil)
    end

end