require 'rails_helper'

RSpec.describe UserSerializer, :type => :serializer do
    

    it "returns email field" do
        user = create(:user)
        serializer = UserSerializer.new(user)
        expect(serializer.serializable_hash[:email]).to eq user.email
    end
    
    it "returns partner fields for inspector" do
        user = create(:user, :role => "inspector")
        partner = create(:partner)
        create(:partner_user, :partner => partner, :user => user, :role => "admin")
        serializer = UserSerializer.new(user)
        expect(serializer.serializable_hash[:partner_id]).to eq partner.id
    end
    
    it "returns partner fields for agent" do
        user = create(:user, :role => "agent")
        partner = create(:partner)
        create(:partner_user, :partner => partner, :user => user, :role => "agent")
        serializer = UserSerializer.new(user)
        expect(serializer.serializable_hash[:partner_id]).to eq partner.id
    end
end