require 'rails_helper'

RSpec.describe UserProfileSerializer, :type => :serializer do
    
    it "returns logo/headshot fields" do
        agent = create(:user, :role => "agent")
        user_profile = create(:user_profile, :user => agent)
        allow(user_profile.logo).to receive(:present?).and_return(true)
        allow(user_profile.logo).to receive(:expiring_cloud_front_url).and_return("")
        allow(user_profile.head_shot).to receive(:present?).and_return(true)
        allow(user_profile.head_shot).to receive(:expiring_cloud_front_url).and_return("")
        serializer = UserProfileSerializer.new(user_profile)
        expect(serializer.serializable_hash[:head_shot_file]).to eq("")
        expect(serializer.serializable_hash[:logo_file]).to eq("")
    end

end