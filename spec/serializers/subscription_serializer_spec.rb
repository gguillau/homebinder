require 'rails_helper'

RSpec.describe SubscriptionSerializer, :type => :serializer do
    
    it "returns plan field" do
        retrieved_card = OpenStruct.new({:delete => true, :type => "Visa", :last4 => "4242"})
        cards = OpenStruct.new({:retrieve => true, :create => true})
        plan = OpenStruct.new({id: 1, amount: 4900})
        stripe_subscription = OpenStruct.new({ plan: plan, current_period_end: Date.today})
        coupon = OpenStruct.new({id: 1, amount_off: 4900})
        discount = OpenStruct.new({ coupon: coupon })
        stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, discount: discount, delete: true, default_card: {}, cards: cards, subscription: stripe_subscription})
        allow(Stripe::Customer).to receive(:retrieve).and_return(stripe_customer)
        allow(stripe_customer.cards).to receive(:retrieve).and_return(retrieved_card)
            subscription = create(:subscription, :customer_id => 1)
        serializer = SubscriptionSerializer.new(subscription)
        expect(serializer.serializable_hash[:plan]).to eq SubscriptionService.get_plan_name(subscription.plan_id)
    end
    
    it "returns plan field" do
        retrieved_card = OpenStruct.new({:delete => true, :type => "Visa", :last4 => "4242"})
        cards = OpenStruct.new({:retrieve => true, :create => true})
        plan = OpenStruct.new({id: 1, amount: 4900})
        stripe_subscription = OpenStruct.new({ plan: plan, current_period_end: Date.today})
        coupon = OpenStruct.new({id: 1, percent_off: 50})
        discount = OpenStruct.new({ coupon: coupon })
        stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, discount: discount, delete: true, default_card: {}, cards: cards, subscription: stripe_subscription})
        allow(Stripe::Customer).to receive(:retrieve).and_return(stripe_customer)
        allow(stripe_customer.cards).to receive(:retrieve).and_return(retrieved_card)
            subscription = create(:subscription, :customer_id => 1)
        serializer = SubscriptionSerializer.new(subscription)
        expect(serializer.serializable_hash[:plan]).to eq SubscriptionService.get_plan_name(subscription.plan_id)
    end
end