FactoryBot.define do
  factory :user_resource do
    user_id {FactoryBot.create(:user).id}
    marketing_resource_id {FactoryBot.create(:marketing_resource).id}
  end
end