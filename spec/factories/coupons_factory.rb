FactoryBot.define do
  factory :coupon do
    code {Faker::Lorem.words(8)}
  end
end