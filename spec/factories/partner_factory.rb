FactoryBot.define do
  factory :partner do
    name { Faker::Company.name }
    contact { Faker::Name.name }
    code { Faker::Internet.password }
    partner_type { "inspector" }
    phone {"610-600-4000"}
    partner_key {""}
    email { Faker::Internet.email }
    after(:create) do |partner, evaluator|
      create(:partner_configuration, :partner_id => partner.id) if partner.partner_configuration.nil?
      partner.reload
    end
  end
end
