FactoryBot.define do
    factory :repair_pricer_report do
        report_id "301"
        association :binder, factory: :binder
        association :partner, factory: :partner
        association :creator, factory: :user
        association :address, factory: :address
        status 1
    end
end
