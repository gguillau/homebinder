FactoryBot.define do
  factory :annual_property_review_finding do
    name { Faker::Lorem.characters(255) }
    priority { 1 }
    status { 1 }
  end
end