FactoryBot.define do
  factory :api_key do
    company_name {"Test"}
    application_name {"Website"}
    contact_email {"test@gmail.com"}
    key { ENV["HB_APIKEY"] }
    #partner_id {FactoryBot.create(:partner).id}
  end
end
