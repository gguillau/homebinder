FactoryBot.define do
  factory :contractor_category, :class => ContractorCategory do
    association :contractor,     factory: :contractor
    association :contractor_category, factory: :contractor_contractor_category
  end
end