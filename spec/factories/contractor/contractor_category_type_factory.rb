FactoryBot.define do
  factory :contractor_category_type, :class => Contractor::CategoryType do |f|
    association :contractor_category,     factory: :contractor_contractor_category
    association :contractor_sub_category, factory: :contractor_sub_category
  end
end