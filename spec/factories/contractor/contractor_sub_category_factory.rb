FactoryBot.define do
  factory :contractor_sub_category, :class => Contractor::SubCategory do |f|
    f.name {Faker::Lorem.characters(25)}
  end
end