FactoryBot.define do
  factory :contractor_contractor_category, :class => Contractor::Category do |f|
    f.name {Faker::Lorem.characters(25)}
    f.verified true
    f.created_by 0
  end
end