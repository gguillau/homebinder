FactoryBot.define do
  factory :user_binder_transaction do
    binder_transaction_id {FactoryBot.create(:binder_transaction).id}
    user_id {FactoryBot.create(:user).id}
  end
end
