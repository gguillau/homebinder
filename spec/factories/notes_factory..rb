FactoryBot.define do
    factory :note do
        content {Faker::Lorem.characters(100)}
        binder_contractor_id {FactoryBot.create(:binder_contractor).id}
    end
end
