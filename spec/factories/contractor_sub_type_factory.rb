FactoryBot.define do
  factory :contractor_sub_type, :class => ContractorSubType do
    association :contractor,     factory: :contractor
    association :contractor_sub_category, factory: :contractor_sub_category
  end
end