FactoryBot.define do
  factory :account do
    account_number {10.times.map{rand(10)}.join}
    name { Faker::Lorem.characters(500) }
    manager_id { create(:partner).id }
    account_status { Account::AccountStatus::ACTIVE }
    account_type { Account::AccountType::SINGLE }
    account_sub_type { Account::AccountSubType::FREE_TRIAL }
    trial_expiration { Date.today + 14.days }
  end
end