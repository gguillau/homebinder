FactoryBot.define do
  factory :warranty do
    status { "pending" }
    warranty_plan_id {FactoryBot.create(:warranty_plan).id}
    binder_id {FactoryBot.create(:binder).id}
    client_first {"First"}
    client_last {"Last"}
    client_email {"tet@gmail.com"}
    client_phone {"434-340-2030"}
    address_attributes {FactoryBot.attributes_for(:address)}
  end
end
