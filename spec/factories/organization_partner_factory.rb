FactoryBot.define do
  factory :organization_partner do
    organization_id {FactoryBot.create(:organization).id}
    partner_id {FactoryBot.create(:partner).id}
  end
end