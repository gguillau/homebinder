FactoryBot.define do
  factory :user_contractor, :class => UserContractor do
    association :user, factory: :user
    name { Faker::Lorem.characters(50) }
    association :contractor, factory: :contractor
    types_attributes {[{name: Faker::Lorem.characters(5)}]}
  end
end