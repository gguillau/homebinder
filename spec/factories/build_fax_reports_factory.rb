FactoryBot.define do
    factory :build_fax_report do
        status "not_paid"
        association :address, factory: :address
        report { Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleDoc.pdf'), 'application/pdf') }
    end
end
