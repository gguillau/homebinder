FactoryBot.define do
  factory :partner_user do
    association :partner, factory: :partner
    association :user, factory: :user, :role => "inspector"
    role {"member"}
  end
end