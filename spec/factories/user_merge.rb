FactoryBot.define do
  factory :user_merge do
    user
    footprint {File.new("spec/assets/footprint.json")}
    old_agent_email { Faker::Internet.email }
    old_agent_first_name {Faker::Name.first_name}
    old_agent_last_name {Faker::Name.last_name}
  end
end
