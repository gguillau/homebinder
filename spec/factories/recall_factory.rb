FactoryBot.define do
  factory :recall, :class => Recall do
    number { Faker::Number.number(7) }
    models ""
    verified false
    recall_date Date.yesterday
    details JSON.generate(:url => "TEST")
    trait :ignore_recall do
      after(:create) do |r|
        r.ignore = true
      end
    end
    trait :no_ignore_recall do
      after(:create) do |r|
        r.ignore = false
      end
    end
  end
end