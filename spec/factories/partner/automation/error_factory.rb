FactoryBot.define do
  factory :automation_error, :class => Partner::Automation::Error do
    partner_name {}
    binder_template_id {}
    client_first {"Bob"}
    client_last {"Smith"}
    client_email {"bobsmith@gmail.com"}
    property_address {"321 Test Street"}
    property_address2 {""}
    property_city {"Boston"}
    property_state {"MA"}
    property_postalcode {"02210"}
    property_country {"US"}
    agent_name {"Keller Williams"}
    agent_contact {"Carmen Smith"}
    agent_phone {"516-064-0219"}
    agent_email {"carmensmith@gmail.com"}
  end
end