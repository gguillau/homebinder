FactoryBot.define do
  factory :partner_configuration, :class => Partner::Configuration do
    association :partner, factory: :partner
  end
end