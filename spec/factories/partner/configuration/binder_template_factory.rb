FactoryBot.define do
  factory :binder_template, :class => Partner::Configuration::BinderTemplate do
    name { Faker::Lorem.characters(50) }
  end
end