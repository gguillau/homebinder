FactoryBot.define do
  factory :isn_user, :class => Partner::Configuration::IsnUser do
    name { Faker::Lorem.characters(50) }
    company_key { Faker::Lorem.characters(50) }
    api_endpoint { Faker::Lorem.characters(200) }
    username { Faker::Lorem.characters(12) }
    password { Faker::Lorem.characters(12) }
  end
end