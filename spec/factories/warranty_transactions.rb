FactoryBot.define do
  factory :warranty_transaction do
    association :warranty, factory: :warranty
    association :binder_transaction, factory: :binder_transaction
  end
end
