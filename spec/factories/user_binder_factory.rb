FactoryBot.define do
  factory :user_binder do
    user_id {FactoryBot.create(:user).id}
    binder_id {FactoryBot.create(:binder).id}
    role {"owner"}
  end
end