FactoryBot.define do
  factory :user_profile, :class => UserProfile do
    association :user, factory: :user, :role => "inspector"
    first_name {Faker::Name.first_name}
    last_name {Faker::Name.last_name}
    mobile_phone {"610-600-4000"}
    address_attributes {FactoryBot.attributes_for(:address)}
    company {Faker::Company.name}
  end
end