FactoryBot.define do
  factory :contractor do
    name { Faker::Lorem.characters(100) }
    email { Faker::Internet.unique.email }
    phone {"234-534-1004"}
    association :creator,    factory: :user
    types_attributes {[{name: Faker::Lorem.characters(5)}]}
    
  end
end