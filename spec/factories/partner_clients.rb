FactoryBot.define do
    factory :partner_client do
        partner_id {FactoryBot.create(:partner).id}
        client_id {FactoryBot.create(:user, :role => "homeowner").id}
    end
end
