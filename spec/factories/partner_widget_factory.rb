FactoryBot.define do
  factory :partner_widget do
    association :partner, factory: :partner
    association :widget, factory: :widget
  end
end