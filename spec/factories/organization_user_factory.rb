FactoryBot.define do
  factory :organization_user do
    organization_id {FactoryBot.create(:organization).id}
    user_id {FactoryBot.create(:user).id}
    role {"ceo"}
  end
end