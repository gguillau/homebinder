FactoryBot.define do
  factory :store do
    name { Faker::Lorem.characters(100) }
  end
end