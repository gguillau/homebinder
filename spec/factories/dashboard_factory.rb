FactoryBot.define do
  factory :dashboard do
    name { Faker::Lorem.characters(100) }
    system true
    scope DashboardScope::BINDER
  end
end