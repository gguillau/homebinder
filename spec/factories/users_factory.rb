FactoryBot.define do
  factory :user do
    # :email, :password, :password_confirmation, :remember_me
    password = Faker::Internet.password(10)
    email { Faker::Internet.email }
    password password
    password_confirmation password
    role "homeowner"
    user_profile_attributes {FactoryBot.attributes_for(:user_profile)}
  end
end
