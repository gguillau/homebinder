FactoryBot.define do
  factory :partner_binder do
    association :binder,  factory: :binder
    association :partner, factory: :partner
    association :user,    factory: :user
    association :partner_user,    factory: :partner_user
  end
end