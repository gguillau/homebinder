FactoryBot.define do
  factory :widget_exclusion do
    association :widget, factory: :widget
  end
end
