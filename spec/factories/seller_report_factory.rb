FactoryBot.define do
  factory :seller_report do
    code {SecureRandom.hex(3).upcase}
  end
end