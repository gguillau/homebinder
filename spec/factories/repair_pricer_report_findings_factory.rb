FactoryBot.define do
    factory :repair_pricer_report_finding do
        name "Dishwashers (1 EA)"
        code "18"
        action "Remove & reset dishwasher"
        pg "1"
        contractor_type "Electrician"
        association :repair_pricer_report, factory: :repair_pricer_report
    end
end
