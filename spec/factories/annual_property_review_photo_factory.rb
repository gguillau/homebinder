FactoryBot.define do
  factory :annual_property_review_photo do
    name { Faker::Lorem.characters(254) }
    file { Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleImage.png'), 'image/png') }
    association :annual_property_review, factory: :annual_property_review
  end
end