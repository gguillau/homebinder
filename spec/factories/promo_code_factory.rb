FactoryBot.define do
  factory :promo_code do
    name "30% off"
    percent_off 5
  end
end