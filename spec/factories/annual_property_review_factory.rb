require 'securerandom'

FactoryBot.define do
  factory :annual_property_review do
    uuid { SecureRandom.uuid.gsub("-", "") }
    client_first_name { Faker::Lorem.characters(100) }
    client_last_name { Faker::Lorem.characters(100) }
    client_email { Faker::Internet.email }
    client_phone { "1-555-555-5555" }
    address1 { Faker::Address.street_address }
    city { Faker::Address.city }
    state { Faker::Address.state_abbr }
    zip { Faker::Address.zip }
    country { "US" }
    status { "active" }
    association :reviewer_company, factory: :partner
  end
end
