FactoryBot.define do
  factory :partner_resource do
    partner_id {FactoryBot.create(:partner).id}
    marketing_resource_id {FactoryBot.create(:marketing_resource).id}
  end
end