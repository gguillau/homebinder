FactoryBot.define do
  factory :binder do
    name { Faker::Lorem.characters(50) }
    property_attributes {FactoryBot.attributes_for(:property)}
    details {Faker::Lorem.characters(50)}
  end
end
