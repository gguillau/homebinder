FactoryBot.define do
  factory :partner_warranty_plan do
    price {4}
    partner_id {FactoryBot.create(:warranty_configuration).id}
    warranty_plan_id {FactoryBot.create(:warranty_plan).id}
  end
end
