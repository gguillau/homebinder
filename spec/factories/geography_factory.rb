FactoryBot.define do
  factory :geography do
    name { Faker::Lorem.characters(250) }
  end
end