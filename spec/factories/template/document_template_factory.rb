FactoryBot.define do
  factory :document_template, :class => Template::Document do
    file {File.new("spec/assets/SampleDoc.pdf")}
    file_file_name {Faker::Lorem.characters(10)}
    document_type
    association :binder_template, factory: :binder_template
  end
end