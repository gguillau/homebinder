FactoryBot.define do
  factory :contractor_template, :class => Template::Contractor do
    association :partner_contractor, factory: :partner_contractor
    association :binder_template, factory: :binder_template
  end
end