FactoryBot.define do
  factory :maintenance_template, :class => Template::Maintenance do
    name { Faker::Lorem.characters(50) }
    association :binder_template, factory: :binder_template
  end
end