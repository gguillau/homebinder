FactoryBot.define do
  factory :appliance_template, :class => Template::Appliance do
    name { Faker::Lorem.characters(50) }
    association :binder_template, factory: :binder_template
  end
end