FactoryBot.define do
  factory :widget do
    name { Faker::Lorem.characters(100) }
    key { Faker::Lorem.characters(100) }
  end
end