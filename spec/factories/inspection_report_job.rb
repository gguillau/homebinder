FactoryBot.define do
  factory :inspection_report_job do
      association :binder, factory: :binder
      association :partner, factory: :partner
      inspection_report { Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleDoc.pdf'), 'application/pdf') }
  end
end
