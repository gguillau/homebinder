FactoryBot.define do
  factory :partner_contractor, :class => PartnerContractor do
    association :partner, factory: :partner
    name { Faker::Lorem.characters(50) }
    association :contractor, factory: :contractor
    types_attributes {[{name: Faker::Lorem.characters(5)}]}
  end
end