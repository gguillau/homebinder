FactoryBot.define do
  factory :warranty_company do
    name { Faker::Company.name }
    contact { Faker::Name.name }
    email { Faker::Internet.email }
  end
end
