FactoryBot.define do
  factory :subscription do
    association :binder, factory: :binder
  end
end
