FactoryBot.define do
  factory :organization do
    name { Faker::Lorem.characters(250) }
  end
end