FactoryBot.define do
  factory :warranty_configuration, :class => WarrantyConfiguration do
    account_number { Faker::Lorem.characters(50) }
    partner_id {FactoryBot.create(:partner).id}
    warranty_plan_id {FactoryBot.create(:warranty_plan).id}
    warranty_company_id {FactoryBot.create(:warranty_company).id}
  end
end
