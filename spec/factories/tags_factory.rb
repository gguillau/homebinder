FactoryBot.define do
  factory :tag do
    association :taggable, factory: :area
    tag "tag"
  end
end
