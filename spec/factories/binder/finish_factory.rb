FactoryBot.define do
  factory :finish, :class => Binder::Finish do |f|
    f.name { Faker::Lorem.characters(50) }
    f.binder_id {FactoryBot.create(:binder).id}
  end
end