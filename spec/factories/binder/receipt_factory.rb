FactoryBot.define do
  factory :receipt, :class => Binder::Receipt do |f|
    f.name { Faker::Lorem.characters(50) }
    f.binder_id {FactoryBot.create(:binder).id}
  end
end