FactoryBot.define do
  factory :project, :class => Binder::Project do |f|
    f.binder_id {FactoryBot.create(:binder).id}
    f.name { Faker::Lorem.characters(50) }
    f.end_date {Date.today + 30.days}
    f.details { Faker::Lorem.characters(50) }
  end
end