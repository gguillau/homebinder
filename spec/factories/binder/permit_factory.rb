FactoryBot.define do
  factory :permit, :class => Binder::Permit do |f|
    f.binder_id {FactoryBot.create(:binder).id}
    f.permit_number { Faker::Lorem.characters(50) }
  end
end