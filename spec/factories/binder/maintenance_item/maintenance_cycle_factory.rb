FactoryBot.define do
  factory :maintenance_cycle, :class => Binder::MaintenanceItem::Cycle do |f|
    f.name {Faker::Lorem.characters(25)}
  end
end