FactoryBot.define do
  factory :maintenance_event, :class => Binder::MaintenanceItem::MaintenanceEvent do |f|
    f.maintenance_item_id {FactoryBot.create(:maintenance_item).id}
    f.do_date {Faker::Time.forward(20)}
    f.created_by {FactoryBot.create(:user).id}
    clicks 0
    opens 0
  end
end