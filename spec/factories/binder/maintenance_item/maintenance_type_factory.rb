FactoryBot.define do
  factory :maintenance_type, :class => Binder::MaintenanceItem::Category do |f|
    f.name {Faker::Lorem.characters(25)}
    f.verified true
    f.created_by 0
  end
end