FactoryBot.define do
  factory :paint_manufacturer, :class => Binder::Paint::Manufacturer do |f|
    f.name {Faker::Lorem.characters(25)}
    f.verified true
    f.created_by 0
  end
end