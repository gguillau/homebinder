FactoryBot.define do
  factory :paint, :class => Binder::Paint do |f|
    f.name { Faker::Lorem.characters(50) }
    f.binder_id {FactoryBot.create(:binder).id}
  end
end