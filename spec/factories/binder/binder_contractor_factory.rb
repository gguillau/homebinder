FactoryBot.define do
  factory :binder_contractor, :class => Binder::BinderContractor do |f|
    association :binder, factory: :binder
    association :creator, factory: :user
    contact { Faker::Name.unique.name }
    association :contractor, factory: :contractor
    secondary_name { Faker::Name.unique.name }
    secondary_email { Faker::Internet.unique.email }
    secondary_phone { Faker::PhoneNumber.unique.phone_number }
    types_attributes {[{name: Faker::Lorem.characters(5)}]}
  end
end