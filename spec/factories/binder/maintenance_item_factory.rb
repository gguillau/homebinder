FactoryBot.define do
  factory :maintenance_item, :class => Binder::MaintenanceItem do |f|
    f.name { Faker::Lorem.characters(50) }
    f.binder_id {FactoryBot.create(:binder).id}
    f.do_date {Faker::Time.forward(10)}
    f.maintenance_cycle {"months"}
    f.interval {6}
    f.created_by {FactoryBot.create(:user).id}
    f.details { Faker::Lorem.characters(50) }
  end
end