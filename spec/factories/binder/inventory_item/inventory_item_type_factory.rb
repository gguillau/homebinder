FactoryBot.define do
  factory :inventory_item_type, :class => Binder::InventoryItem::Category do |f|
    f.name {Faker::Lorem.characters(25)}
    f.verified true
    f.created_by 0
  end
end