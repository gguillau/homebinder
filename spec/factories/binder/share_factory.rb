FactoryBot.define do
  factory :share, :class => Binder::Share do |f|
    association :binder,    factory: :binder
    association :sender,    factory: :user
    association :receiver,  factory: :user
    
    f.status {"created"}
    f.role_name {"co_owner"}
  end
end