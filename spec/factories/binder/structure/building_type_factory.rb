FactoryBot.define do
  factory :building_type, :class => Binder::Structure::BuildingType do |f|
    f.name {Faker::Lorem.characters(25)}
    f.verified true
    f.created_by 0
  end
end