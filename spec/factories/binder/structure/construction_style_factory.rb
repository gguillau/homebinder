FactoryBot.define do
  factory :construction_style, :class => Binder::Structure::ConstructionStyle do |f|
    f.name {Faker::Lorem.characters(25)}
    f.verified true
    f.created_by 0
  end
end