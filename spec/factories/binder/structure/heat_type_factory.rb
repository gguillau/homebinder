FactoryBot.define do
  factory :heat_type, :class => Binder::Structure::HeatType do |f|
    f.name {Faker::Lorem.characters(25)}
    f.verified true
    f.created_by 0
  end
end