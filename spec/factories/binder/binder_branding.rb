FactoryBot.define do
  factory :binder_branding, :class => Binder::BinderBranding do |f|
    f.binder_id {FactoryBot.create(:binder).id}
    f.scope { "binder" }
    f.user_branding_id {create(:user_profile).id} 
  end
end