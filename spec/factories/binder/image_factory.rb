FactoryBot.define do
  factory :image, :class => Binder::Image do |f|
    f.binder_id {FactoryBot.create(:binder).id}
    f.file_file_name {Faker::File.unique.file_name("")}
    f.file_file_size {0}
  end
end