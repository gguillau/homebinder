FactoryBot.define do
  factory :appliance, :class => Binder::Appliance do |f|
    f.name { Faker::Lorem.characters(50) }
    f.binder_id {FactoryBot.create(:binder).id}
    f.details { Faker::Lorem.characters(50) }
    f.model { Faker::Lorem.characters(50) }
    f.manufacturer { Faker::Lorem.characters(50) }
  end
end