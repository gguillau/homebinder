FactoryBot.define do
  factory :inventory_item, :class => Binder::InventoryItem do |f|
    f.name { Faker::Lorem.characters(50) }
    f.binder_id {FactoryBot.create(:binder).id}
  end
end