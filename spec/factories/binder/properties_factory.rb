FactoryBot.define do
  factory :property, :class => Binder::Property do
    association :binder,    factory: :binder
    address1 { Faker::Address.street_address }
    address2 { Faker::Address.secondary_address }
    city { Faker::Address.city }
    state { Faker::Address.state_abbr }
    zip { Faker::Address.zip }
    country 'US'
    lat { Faker::Address.latitude }
    long { Faker::Address.longitude }
  end
end
