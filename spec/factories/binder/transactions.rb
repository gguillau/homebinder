FactoryBot.define do
  factory :binder_transaction, :class => Binder::Transaction do
    binder_id {FactoryBot.create(:binder).id}
  end
end
