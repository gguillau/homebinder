FactoryBot.define do
  factory :binder_contractor_sub_type, :class => Binder::BinderContractorSubType do |f|
    association :binder_contractor,     factory: :binder_contractor
    association :contractor_sub_category, factory: :contractor_sub_category
  end
end