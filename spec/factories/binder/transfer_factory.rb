FactoryBot.define do
  factory :transfer, :class => Binder::Transfer do |f|
    association :binder,    factory: :binder
    association :sender,    factory: :user
    association :receiver,  factory: :user
    status {"created"}
    transfer_type {"ownership"}
  end
end