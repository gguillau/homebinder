FactoryBot.define do
  factory :binder_contractor_type, :class => Binder::BinderContractorType do |f|
    association :binder_contractor,     factory: :binder_contractor
    association :contractor_category, factory: :contractor_contractor_category
  end
end