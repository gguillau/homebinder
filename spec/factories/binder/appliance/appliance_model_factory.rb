FactoryBot.define do
  factory :appliance_model, :class => Binder::Appliance::Model do |f|
    f.name {Faker::Lorem.characters(25)}
    f.verified true
    f.created_by 0
  end
end