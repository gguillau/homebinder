FactoryBot.define do
  factory :appliance_recalls, :class => Binder::Appliance::ApplianceRecall do
    association :appliance, factory: :appliance
    association :recall, factory: :recall
  end
end