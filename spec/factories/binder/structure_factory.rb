FactoryBot.define do
  factory :structure, :class => Binder::Structure do |f|
    f.name { Faker::Lorem.characters(50) }
    f.binder_id {FactoryBot.create(:binder).id}
  end
end