FactoryBot.define do
  factory :area, :class => Binder::Area do |f|
    f.name { Faker::Lorem.characters(50) }
    f.binder_id {FactoryBot.create(:binder).id}
  end
end