FactoryBot.define do
  factory :document, :class => Binder::Document do |f|
    binder
    f.file_file_name {Faker::File.unique.file_name("")}
    f.file_file_size {0}
  end
end