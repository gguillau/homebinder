FactoryBot.define do
  factory :finish_make, :class => Binder::Finish::Make do |f|
    f.name {Faker::Lorem.characters(25)}
    f.verified true
    f.created_by 0
  end
end