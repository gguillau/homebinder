FactoryBot.define do
  factory :annual_property_review_capital_item do
    name { Faker::Lorem.characters(255) }
    eul { 1 }
    cost_range { 1 }
  end
end