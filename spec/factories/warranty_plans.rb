FactoryBot.define do
  factory :warranty_plan do
    name { Faker::Lorem.characters(50) }
    duration { 90 }
    cycle { "months" }
    description { Faker::Lorem.characters(50) }
    price {5}
    warranty_company_id {FactoryBot.create(:warranty_company).id}
  end
end
