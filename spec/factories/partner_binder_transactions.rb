FactoryBot.define do
  factory :partner_binder_transaction do
    binder_transaction_id {FactoryBot.create(:binder_transaction).id}
    partner_id {FactoryBot.create(:partner).id}
  end
end
