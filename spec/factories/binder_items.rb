FactoryBot.define do
  factory :binder_item do
    association :binder,    factory: :binder
    association :user,      factory: :user
  end
end
