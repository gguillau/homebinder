FactoryBot.define do
  factory :organization_resource do
    organization_id {FactoryBot.create(:organization).id}
    marketing_resource_id {FactoryBot.create(:marketing_resource).id}
  end
end