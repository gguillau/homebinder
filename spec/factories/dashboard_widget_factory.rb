FactoryBot.define do
  factory :dashboard_widget do
    dashboard_id {FactoryBot.create(:dashboard).id}
    widget_id {FactoryBot.create(:widget).id}
  end
end