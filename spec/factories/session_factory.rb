FactoryBot.define do
  factory :session do
    association :user, factory: :user
    token "token"
    expires_at Date.today + 30.days
  end
end