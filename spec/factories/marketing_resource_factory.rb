FactoryBot.define do
  factory :marketing_resource do
    name { Faker::Lorem.characters(50) }
  end
end
