require 'rails_helper'

RSpec.describe "HomeBinder::Application", :type => :config do
    
    describe "uploads" do
        it "has a set max_size for uploads" do
            expect(Rails.configuration.uploads[:max_size]).to eq(10000000) 
        end
    end
    
    describe "password_reset_time_frame" do
        it "the time frame is present" do
            expect(Rails.configuration.password_reset_time_frame).to eq(6) 
        end
    end
end