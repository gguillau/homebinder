require 'rails_helper'

RSpec.describe Host, :type => :model do

    describe "#path" do
        it "returns LOCAL_HOST" do
            allow(Rails.env).to receive(:development?).and_return(true)
            host = ENV["DOMAIN"] || Host::LOCAL_HOST
            expect(Host.path).to eq(host)
        end
        it "returns TEST_HOST" do
            allow(Rails.env).to receive(:testing?).and_return(true)
            expect(Host.path).to eq(Host::TEST_HOST)
        end
        it "returns STAGING_HOST" do
            allow(Rails.env).to receive(:staging?).and_return(true)
            expect(Host.path).to eq(Host::STAGING_HOST)
        end
        it "returns PRODUCTION_HOST" do
            allow(Rails.env).to receive(:production?).and_return(true)
            expect(Host.path).to eq(Host::PRODUCTION_HOST)
        end
    end
    
    describe "#generate_host_path" do
        it "returns www.homebinder.com" do
            allow(Rails.env).to receive(:production?).and_return(true)
            expect(Host.generate_host_path).to eq("www.homebinder.com")
        end
        it "returns test.homebinder.com" do
            allow(Rails.env).to receive(:testing?).and_return(true)
            expect(Host.generate_host_path).to eq("test.homebinder.com")
        end
        it "returns staging.homebinder.com" do
            allow(Rails.env).to receive(:staging?).and_return(true)
            expect(Host.generate_host_path).to eq("staging.homebinder.com")
        end
        it "returns Host.path" do
            allow(Rails.env).to receive(:development?).and_return(true)
            expect(Host.generate_host_path).to eq(Host.path)
        end
    end
end