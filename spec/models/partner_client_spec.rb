require 'rails_helper'

RSpec.describe PartnerClient, type: :model do
    
    it "has a valid factory" do
        partner_client = build :partner_client
        expect(partner_client).to be_valid
    end
    
    it "is invalid because partner_id is required" do
        partner_client = build(:partner_client, partner_id: nil)
        expect(partner_client).not_to be_valid
        expect(partner_client.errors.full_messages.first).to eq("Partner is required")
    end
    
    it "is invalid because client_id is required" do
        partner_client = build(:partner_client, client_id: nil)
        expect(partner_client).not_to be_valid
        expect(partner_client.errors.full_messages.first).to eq("Client is required")
    end
    
    it "is invalid because client already a client for partner" do
        partner_client = create(:partner_client)
        expect(partner_client).to be_valid
        partner_client_2 = build(:partner_client, partner_id: partner_client.partner_id, client_id: partner_client.client_id)
        expect(partner_client_2).not_to be_valid
        expect(partner_client_2.errors.full_messages.first).to eq("Client is already a partner client")
    end
    
    describe "#pager" do
        it "searches" do
            admin = create(:user, :role => "admin")
            request = HBRequest.new.create_request(admin)
            partner = create(:partner)
            create(:partner_client, :partner => partner)
                page = PartnerClient.pager(request, {:partner_id => partner.id})
            expect(page.total).to eq(1)
        end
    end
end
