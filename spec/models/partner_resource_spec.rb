require 'rails_helper'

RSpec.describe PartnerResource, :type => :model do
    
    it "has a valid factory" do
        partner_resource = build :partner_resource
        expect(partner_resource).to be_valid
    end
    
    it "is invalid because partner_id is required" do
        partner_resource = build(:partner_resource, partner_id: nil)
        expect(partner_resource).not_to be_valid
        expect(partner_resource.errors.full_messages.first).to eq("Partner is required")
    end
    
    it "is invalid because marketing_resource_id is required" do
        partner_resource = build(:partner_resource, marketing_resource_id: nil)
        expect(partner_resource).not_to be_valid
        expect(partner_resource.errors.full_messages.first).to eq("Marketing resource is required")
    end
    
    it "is invalid because marketing_resource already exists for partner" do
        partner_resource = create(:partner_resource)
        expect(partner_resource).to be_valid
        partner_resource_2 = build(:partner_resource, partner_id: partner_resource.partner_id, marketing_resource_id: partner_resource.marketing_resource_id)
        expect(partner_resource_2).not_to be_valid
        expect(partner_resource_2.errors.full_messages.first).to eq("Marketing resource already exists for partner")
    end
end