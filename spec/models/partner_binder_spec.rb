require 'rails_helper'

RSpec.describe PartnerBinder, :type => :model do

    it "has a valid factory" do
        partner_binder = build :partner_binder
        expect(partner_binder).to be_valid
    end

    it "is invalid because binder_id is required" do
        partner_binder = build(:partner_binder, binder_id: nil)
        expect(partner_binder).not_to be_valid
        expect(partner_binder.errors.full_messages.first).to eq("Binder is required")
    end

    it "is invalid because partner_id is required" do
        partner_binder = build(:partner_binder, partner_id: nil)
        expect(partner_binder).not_to be_valid
        expect(partner_binder.errors.full_messages.first).to eq("Partner is required")
    end

    it "is invalid because marketing_resource already exists in organization" do
        partner_binder = create(:partner_binder)
        expect(partner_binder).to be_valid

        partner_binder_2 = build(:partner_binder, binder_id: partner_binder.binder_id, partner_id: partner_binder.partner_id)
        expect(partner_binder_2).not_to be_valid
        expect(partner_binder_2.errors.full_messages.first).to eq("Partner Binder already exists")
    end

    it "returns the default query arguments" do
        expect(PartnerBinder.query_arguments).to eq(["binderId", "binder_id", "partnerId", "partner_id", "role", "repair_pricer_enabled"])
    end

    it "returns a hash with binder_id and a value" do
        expect(PartnerBinder.query_arguments_hash("binderId", 1)).to eq({:binder_id => 1})
        expect(PartnerBinder.query_arguments_hash("binder_id", 1)).to eq({:binder_id => 1})
    end

    it "returns a hash with partner_id and a value" do
        expect(PartnerBinder.query_arguments_hash("partnerId", 1)).to eq({:partner_id => 1})
        expect(PartnerBinder.query_arguments_hash("partner_id", 1)).to eq({:partner_id => 1})
    end
    
    it "returns a hash with role and a value" do
        expect(PartnerBinder.query_arguments_hash("role", "inspector")).to eq({:role => "inspector"})
    end
    
    it "returns a hash with repair_pricer_enabled and a value" do
        expect(PartnerBinder.query_arguments_hash("repair_pricer_enabled", true)).to eq({:repair_pricer_enabled => true})
    end
    
    it "returns object as json" do
        partner_binder = create(:partner_binder)
        expect(partner_binder.as_indexed_json["partner_id"]).to eq(partner_binder.partner_id)
    end

end
