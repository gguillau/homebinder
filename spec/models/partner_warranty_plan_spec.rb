require 'rails_helper'

RSpec.describe PartnerWarrantyPlan, type: :model do
    
    it "has a valid factory" do
        partner_warranty_plan = create :partner_warranty_plan
        expect(partner_warranty_plan).to be_valid
    end
    
    it "requires a partner_id" do
        partner_warranty_plan = build(:partner_warranty_plan, partner_id: nil)
        expect(partner_warranty_plan).to_not be_valid
        expect(partner_warranty_plan.errors.full_messages.first).to eq("Partner is required")
    end
    
    it "requires a warranty_plan_id" do
        partner_warranty_plan = build(:partner_warranty_plan, warranty_plan_id: nil)
        expect(partner_warranty_plan).to_not be_valid
        expect(partner_warranty_plan.errors.full_messages.first).to eq("Warranty plan is required")
    end
    
    it "requires a price" do
        partner_warranty_plan = build(:partner_warranty_plan, price: nil)
        expect(partner_warranty_plan).to_not be_valid
        expect(partner_warranty_plan.errors.full_messages.first).to eq("Price is required")
    end
    
    it "requires a unique warranty_plan_id per partner" do
        partner_warranty_plan = create :partner_warranty_plan
        expect(partner_warranty_plan).to be_valid
        partner_warranty_plan_2 = build :partner_warranty_plan, :warranty_plan_id => partner_warranty_plan.warranty_plan_id, :partner_id => partner_warranty_plan.partner_id
        expect(partner_warranty_plan_2).to_not be_valid
        expect(partner_warranty_plan_2.errors.full_messages.first).to eq("Warranty plan has already been taken")
    end
end
