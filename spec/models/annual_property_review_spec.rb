require 'rails_helper'

RSpec.describe AnnualPropertyReview, :type => :model do

  it "has a valid factory" do
    apr = build(:annual_property_review)
    expect(apr).to be_valid
  end

  describe "validation" do
    it "requires client_first_name" do
      apr = build(:annual_property_review, client_first_name: nil)
      expect(apr).not_to be_valid
    end

    it "client_first_name length <= 100" do
      apr = build(:annual_property_review, client_first_name: Faker::Lorem.characters(101))
      expect(apr).not_to be_valid
    end

    it "requires client_last_name" do
      apr = build(:annual_property_review, client_last_name: nil)
      expect(apr).not_to be_valid
    end

    it "client_last_name length <= 100" do
      apr = build(:annual_property_review, client_first_name: Faker::Lorem.characters(101))
      expect(apr).not_to be_valid
    end

    it "requires client_email" do
      apr = build(:annual_property_review, client_email: nil)
      expect(apr).not_to be_valid
    end

    it "client_email length <= 100" do
      apr = build(:annual_property_review, client_email: Faker::Lorem.characters(101))
      expect(apr).not_to be_valid
    end

    it "requires client_phone" do
      apr = build(:annual_property_review, client_phone: nil)
      expect(apr).not_to be_valid
    end

    it "client_phone <= 30" do
        apr = build(:annual_property_review, client_phone: Faker::Lorem.characters(101))
        expect(apr).not_to be_valid
    end

    it "requires address1" do
        apr = build(:annual_property_review, address1: nil)
        expect(apr).not_to be_valid
    end

    it "address1 <= 100" do
        apr = build(:annual_property_review, address1: Faker::Lorem.characters(101))
        expect(apr).not_to be_valid
    end

    it "address2 <= 100" do
        apr = build(:annual_property_review, address2: Faker::Lorem.characters(101))
        expect(apr).not_to be_valid
    end

    it "requires city" do
        apr = build(:annual_property_review, city: nil)
        expect(apr).not_to be_valid
    end

    it "city <= 100" do
        apr = build(:annual_property_review, city: Faker::Lorem.characters(101))
        expect(apr).not_to be_valid
    end

    it "requires state" do
        apr = build(:annual_property_review, state: nil)
        expect(apr).not_to be_valid
    end

    it "state <= 100" do
        apr = build(:annual_property_review, state: Faker::Lorem.characters(101))
        expect(apr).not_to be_valid
    end

    it "requires zip" do
        apr = build(:annual_property_review, zip: nil)
        expect(apr).not_to be_valid
    end

    it "zip <= 100" do
        apr = build(:annual_property_review, zip: Faker::Lorem.characters(101))
        expect(apr).not_to be_valid
    end

    it "requires country" do
        apr = build(:annual_property_review, country: nil)
        expect(apr).not_to be_valid
    end

    it "country <= 100" do
        apr = build(:annual_property_review, country: Faker::Lorem.characters(101))
        expect(apr).not_to be_valid
    end

    it "cancel_reason <= 1000" do
      apr = build(:annual_property_review, cancel_reason: Faker::Lorem.characters(1001))
      expect(apr).not_to be_valid
    end
  end
end
