require 'rails_helper'

RSpec.describe WarrantyConfiguration, type: :model do
    
    it "has a valid factory" do
        warranty_configuration = create :warranty_configuration
        expect(warranty_configuration).to be_valid
    end
    
    it "requires an account_number" do
        warranty_configuration = build(:warranty_configuration, account_number: nil)
        expect(warranty_configuration).to_not be_valid
        expect(warranty_configuration.errors.full_messages.first).to eq("Account number is required")
    end

    it "requires a partner_id" do
        warranty_configuration = build(:warranty_configuration, partner_id: nil)
        expect(warranty_configuration).to_not be_valid
        expect(warranty_configuration.errors.full_messages.first).to eq("Partner is required")
    end

    it "requires a warranty_plan_id" do
        warranty_configuration = build(:warranty_configuration, warranty_plan_id: nil)
        expect(warranty_configuration).to_not be_valid
        expect(warranty_configuration.errors.full_messages.first).to eq("Warranty plan is required")
    end
    
    it "requires a binder_id" do
        warranty_configuration = build(:warranty_configuration, warranty_company_id: nil)
        expect(warranty_configuration).to_not be_valid
        expect(warranty_configuration.errors.full_messages.first).to eq("Warranty company is required")
    end
    
    it "requires 1 unique object per partner" do
        warranty_configuration = create :warranty_configuration
        expect(warranty_configuration).to be_valid
        warranty_configuration_2 = build :warranty_configuration, :partner_id => warranty_configuration.partner_id
        expect(warranty_configuration_2).to_not be_valid
        expect(warranty_configuration_2.errors.full_messages.first).to eq("Partner has already been taken")
    end
end
