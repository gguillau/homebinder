require 'rails_helper'

RSpec.describe PartnerBinderTransaction, type: :model do
    
    it "has a valid factory" do
        partner_binder_transaction = create(:partner_binder_transaction)
        expect(partner_binder_transaction).to be_valid
    end
    
    it "requires a binder_transaction" do
        partner_binder_transaction = build(:partner_binder_transaction, binder_transaction_id: nil)
        expect(partner_binder_transaction).to_not be_valid
        expect(partner_binder_transaction.errors.full_messages.first).to eq("Binder transaction is required")
    end
    
    it "requires a partner" do
        partner_binder_transaction = build(:partner_binder_transaction, partner_id: nil)
        expect(partner_binder_transaction).to_not be_valid
        expect(partner_binder_transaction.errors.full_messages.first).to eq("Partner is required")
    end
    
    it "does not allow duplicate transaction" do
        partner_binder_transaction = create(:partner_binder_transaction)
        expect(partner_binder_transaction).to be_valid
        partner_binder_transaction_2 = build(:partner_binder_transaction, partner_id: partner_binder_transaction.partner_id, :binder_transaction_id => partner_binder_transaction.binder_transaction_id)
        expect(partner_binder_transaction_2).to_not be_valid
        expect(partner_binder_transaction_2.errors.full_messages.first).to eq("Partner has already been taken")
    end
end
