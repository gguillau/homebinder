require 'rails_helper'

RSpec.describe Purchase, :type => :model do
  it "has a valid factory" do
    purchase = create :purchase
    expect(purchase).to be_valid
  end
  
  it "has a valid store" do
    store = create(:store)
    purchase = create :purchase, :store_id => store.id
    expect(purchase.store).to eq(store.name.titleize)
  end
  
  it "sets the store" do
    store = create(:store)
    purchase = create :purchase
    purchase.store = store.name
    
    expect(purchase.store).to eq(store.name.titleize)
  end
end
