require 'rails_helper'

RSpec.describe InspectionReportJob, type: :model do
    describe "#status" do
        it "has the correct status" do
            job = create(:inspection_report_job, :status => "complete")
            expect(job.status_complete?).to be(true)
        end
    end
end
