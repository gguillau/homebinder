require 'rails_helper'

RSpec.describe AnnualPropertyReviewCapitalItem, :type => :model do
  
  it "has a valid factory" do
    apr = build(:annual_property_review_capital_item)
    expect(apr).to be_valid
  end
  
  describe "validation" do
    it "requires name" do
      aprci = build(:annual_property_review_capital_item, name: nil)
      expect(aprci).not_to be_valid
    end
  
    it "name length <= 255" do
      aprci = build(:annual_property_review_capital_item, name: Faker::Lorem.characters(256))
      expect(aprci).not_to be_valid
    end
  end
  
  describe "#cost" do
    it "returns Less than $500" do
      apr_capital_item = create(:annual_property_review_capital_item, :cost_range => "less_five_hun")
      expect(apr_capital_item.cost).to eq("Less than $500")
    end
    it "returns $500 - $1,000" do
      apr_capital_item = create(:annual_property_review_capital_item, :cost_range => "five_hun_to_one_thou")
      expect(apr_capital_item.cost).to eq("$500 - $1,000")
    end
    it "returns $1,000 - $2,500" do
      apr_capital_item = create(:annual_property_review_capital_item, :cost_range => "one_thou_to_twentyfive_hun")
      expect(apr_capital_item.cost).to eq("$1,000 - $2,500")
    end
    it "returns $2,500 to $5,000" do
      apr_capital_item = create(:annual_property_review_capital_item, :cost_range => "twentyfive_hun_to_five_thou")
      expect(apr_capital_item.cost).to eq("$2,500 to $5,000")
    end
    it "returns $5,000 - $10,000" do
      apr_capital_item = create(:annual_property_review_capital_item, :cost_range => "five_thou_to_ten_thou")
      expect(apr_capital_item.cost).to eq("$5,000 - $10,000")
    end
    it "returns $10,000 - $15,000" do
      apr_capital_item = create(:annual_property_review_capital_item, :cost_range => "ten_thou_to_fifteen_thou")
      expect(apr_capital_item.cost).to eq("$10,000 - $15,000")
    end
    it "returns $15,000 - $20,000" do
      apr_capital_item = create(:annual_property_review_capital_item, :cost_range => "fifteen_thou_to_twenty_thou")
      expect(apr_capital_item.cost).to eq("$15,000 - $20,000")
    end
    it "returns $20,000+" do
      apr_capital_item = create(:annual_property_review_capital_item, :cost_range => "twenty_thou_plus")
      expect(apr_capital_item.cost).to eq("$20,000+")
    end
    it "returns unknown" do
      apr_capital_item = create(:annual_property_review_capital_item, :cost_range => "unknown")
      expect(apr_capital_item.cost).to eq("Unknown")
    end
  end
  
  describe "#rul" do
    it "returns Less than 1 year" do
      apr_capital_item = create(:annual_property_review_capital_item, :eul => "less_one")
      expect(apr_capital_item.rul).to eq("Less than 1 year")
    end
    it "returns 1 to 2 years" do
      apr_capital_item = create(:annual_property_review_capital_item, :eul => "one_to_two")
      expect(apr_capital_item.rul).to eq("1 to 2 years")
    end
    it "returns 2 to 3 years" do
      apr_capital_item = create(:annual_property_review_capital_item, :eul => "two_to_three")
      expect(apr_capital_item.rul).to eq("2 to 3 years")
    end
    it "returns 3 to 5 years" do
      apr_capital_item = create(:annual_property_review_capital_item, :eul => "three_to_five")
      expect(apr_capital_item.rul).to eq("3 to 5 years")
    end
    it "returns 5 to 10 years" do
      apr_capital_item = create(:annual_property_review_capital_item, :eul => "five_to_ten")
      expect(apr_capital_item.rul).to eq("5 to 10 years")
    end
    it "returns 10 to 15 years" do
      apr_capital_item = create(:annual_property_review_capital_item, :eul => "ten_to_fifteen")
      expect(apr_capital_item.rul).to eq("10 to 15 years")
    end
    it "returns 15 to 20 years" do
      apr_capital_item = create(:annual_property_review_capital_item, :eul => "fifteen_to_twenty")
      expect(apr_capital_item.rul).to eq("15 to 20 years")
    end
    it "returns 20+ years" do
      apr_capital_item = create(:annual_property_review_capital_item, :eul => "twenty_plus")
      expect(apr_capital_item.rul).to eq("20+ years")
    end
    it "returns None" do
      apr_capital_item = create(:annual_property_review_capital_item, :eul => "none")
      expect(apr_capital_item.rul).to eq("None")
    end
    it "returns N/A" do
      apr_capital_item = create(:annual_property_review_capital_item, :eul => "not_applicable")
      expect(apr_capital_item.rul).to eq("N/A")
    end
    it "returns unknown" do
      apr_capital_item = create(:annual_property_review_capital_item, :eul => "unknown")
      expect(apr_capital_item.rul).to eq("Unknown")
    end
  end
  
end