require 'rails_helper'

RSpec.describe Binder::Transaction, type: :model do
    
    it "has a valid factory" do
        binder = create(:binder)
        binder_transaction = create :binder_transaction, :binder_id => binder.id
        expect(binder_transaction).to be_valid
    end
    
    it "requires a binder" do
        binder_transaction = build(:binder_transaction, binder_id: nil)
        expect(binder_transaction).to_not be_valid
        expect(binder_transaction.errors.full_messages.first).to eq("Binder is required")
    end
end
