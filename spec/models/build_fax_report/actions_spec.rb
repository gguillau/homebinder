require 'rails_helper'
require "cancan/matchers"

RSpec.describe BuildFaxReport::Actions, :type => :model do

    let(:admin) { FactoryBot.create(:user, :role => "admin") }
    let(:inspector) { FactoryBot.create(:user, :role => "inspector") }
    let(:partner) { FactoryBot.create(:partner, :partner_type => "inspector") }
    let!(:partner_admin) { FactoryBot.create(:partner_user, :user => inspector, :partner => partner, :role => "admin") }
    let(:build_fax_report) {FactoryBot.create(:build_fax_report)}
    let(:hb_request) { HBRequest.new.create_request(admin)}
    let(:hb_request_inspector) { HBRequest.new.create_request(inspector)}

    describe "index" do
        it "gets the build_fax_reports" do
            page = BuildFaxReport.index(hb_request_inspector, {:partnerId => partner.id, :status => "paid", :state => "MA"})
            expect(page.total).to eq(0)
        end
    end
    
    describe "show" do
        it "raise an error because of missing field" do
            expect{BuildFaxReport.show(hb_request, {})}.to raise_error BadRequestException, "Build Fax Report ID required"
        end
        it "gets the build_fax_report" do
            report = BuildFaxReport.show(hb_request, {:id => build_fax_report.id})
            expect(report).to_not be(nil)
        end
    end
    
    describe "build" do
        it "raise an error because of missing field" do
            expect{BuildFaxReport.build(hb_request, {})}.to raise_error BadRequestException, "Report Address required"
        end
        it "raise an error because of invalid file content" do
            hash = ActionController::Parameters.new({
                :address => "12 Main Street",
                :city => "Boston",
                :state => "MA",
                :zip => "02210"
                }
            )
            buildfax = Buildfax::Request.new
            allow(Buildfax::Request).to receive(:new).and_return(buildfax)
            file = Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleDoc.pdf'), 'application/pdf')
            allow(buildfax).to receive(:download_report).and_return(file)
                expect{BuildFaxReport.build(hb_request, {:build_fax_report => hash})}.to raise_error UnprocessableException
        end
        it "creates the resource" do
            buildfax = Buildfax::Request.new
            allow(Buildfax::Request).to receive(:new).and_return(buildfax)

            file = File.open("#{Rails.root}/spec/assets/SampleDoc.pdf", "rb")
            contents = file.read
                allow(buildfax).to receive(:download_report).and_return(contents)
            build_fax_report = {
                :address => "12 Main Street",
                :city => "Boston",
                :state => "MA",
                :zip => "02210"
            }
                    hash = ActionController::Parameters.new({
                :address => "12 Main Street",
                :city => "Boston",
                :state => "MA",
                :zip => "02210"
                }
            )
                    build_fax_report = BuildFaxReport.build(hb_request, {:build_fax_report => hash})
                expect(build_fax_report).to_not be(nil)
            expect(build_fax_report.id).to_not be(nil)
            expect(build_fax_report.status).to eq("not_paid")
        end
    end
    
    describe "update" do
        it "raise an error because of missing field" do
            expect{BuildFaxReport.update(hb_request, {})}.to raise_error BadRequestException, "Build Fax Report ID required"
        end
        it "raise an error because of missing build_fax_report" do
            expect{BuildFaxReport.update(hb_request, {:id => -1, :build_fax_report => {:status => "paid"}})}.to raise_error ActiveRecord::RecordNotFound
        end
        it "raise an error because of invalid report" do
            hash = ActionController::Parameters.new({:report => Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleImage.png'), 'image/png')})
            expect{BuildFaxReport.update(hb_request, {:id => build_fax_report.id, :build_fax_report => hash})}.to raise_error UnprocessableException
        end
        it "updates the report" do
            hash = ActionController::Parameters.new({:status => "paid"})
            report = BuildFaxReport.update(hb_request, {:id => build_fax_report.id, :build_fax_report => hash})
            expect(report.status).to eq("paid")
        end
    end
    
    describe "destroy" do
        it "raise an error because of missing field" do
            expect{BuildFaxReport.destroy(hb_request, nil)}.to raise_error BadRequestException, "Build Fax Report ID required"
        end
        it "deletes the build_fax_report" do
            BuildFaxReport.destroy(hb_request, build_fax_report.id)
                expect{BuildFaxReport.find(build_fax_report.id)}.to raise_error ActiveRecord::RecordNotFound
        end
    end
    
    describe "search_buildfax" do
        it "raise an error because of missing field" do
            expect{BuildFaxReport.search_buildfax({}, {})}.to raise_error BadRequestException, "Search Address required"
        end
        it "raise an error because of missing field" do
            expect{BuildFaxReport.search_buildfax({}, {:search => {:address => "123 Main Street"}})}.to raise_error BadRequestException, "User required"
        end
        it "calls the API" do
            hb_request = HBRequest.new.create_request(admin)
            client = Buildfax::Client.new(hb_request, nil)
            allow(Buildfax::Client).to receive(:new).and_return(client)
            allow(client).to receive(:send_request)
                search = {
                    :address => "12 Main Street",
                    :city => "Boston",
                    :state => "MA",
                    :zip => "02210"
                }
                BuildFaxReport.search_buildfax(hb_request, {:search => search})
                expect(client).to have_received(:send_request)
        end
    end
    
end
