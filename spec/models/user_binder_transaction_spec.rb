require 'rails_helper'

RSpec.describe UserBinderTransaction, type: :model do
    
    it "has a valid factory" do
        user_binder_transaction = create(:user_binder_transaction)
        expect(user_binder_transaction).to be_valid
    end
    
    it "requires a binder_transaction" do
        user_binder_transaction = build(:user_binder_transaction, binder_transaction_id: nil)
        expect(user_binder_transaction).to_not be_valid
        expect(user_binder_transaction.errors.full_messages.first).to eq("Binder transaction is required")
    end
    
    it "requires a user" do
        user_binder_transaction = build(:user_binder_transaction, user_id: nil)
        expect(user_binder_transaction).to_not be_valid
        expect(user_binder_transaction.errors.full_messages.first).to eq("User is required")
    end
    
    it "does not allow duplicate transaction" do
        user_binder_transaction = create(:user_binder_transaction)
        expect(user_binder_transaction).to be_valid
        user_binder_transaction_2 = build(:user_binder_transaction, user_id: user_binder_transaction.user_id, :binder_transaction_id => user_binder_transaction.binder_transaction_id)
        expect(user_binder_transaction_2).to_not be_valid
        expect(user_binder_transaction_2.errors.full_messages.first).to eq("User has already been taken")
    end
end
