require 'rails_helper'
require "cancan/matchers"

RSpec.describe User::UserActions, :type => :model do
    let(:admin){create(:user, :role => "admin")}
    let(:request) {HBRequest.new.create_request(admin)}

    before :each do
        search = OpenStruct.new(
            results: OpenStruct.new({
                total: 1,
                response: OpenStruct.new({
                    records: [{id: 1}]
                })
            })
        )
        allow(User.__elasticsearch__).to receive(:search).and_return(search)
    end

    describe "build" do
        it "raises UnprocessableException" do
            user = ActionController::Parameters.new({:email => "test@gmail.com", :password => "password"})
            expect{User.build(request, {:user => user})}.to raise_error UnprocessableException
        end
        it "creates the partner user" do
            inspector = create(:user, :role => "inspector")
            partner = create(:partner, :partner_type => "inspector")
            create(:partner_user, :partner => partner, :user => inspector, :role => "admin")
            user = ActionController::Parameters.new({:email => "test@gmail.com", :password => "password", :role => "inspector"})
            new_user = User.build(request, {:user => user, :partnerId => partner.id, :role => "member"})

            expect(new_user.partners.count).to eq(1)
        end
    end

    describe "update" do
        it "updates the user" do
            inspector = create(:user, :role => "inspector")
            user = ActionController::Parameters.new({:user_profile_attributes => {:first_name => "updated", :company => "Test Company"}})
            new_user = User.update(request, {:id => inspector.id, :user => user})
            expect(new_user.user_profile.first_name).to eq("updated")
        end
    end

    describe "register" do
        it "raises an error because hash is empty" do
            expect{ User.register({})}.to raise_error BadRequestException, "User is required"
        end

        it "raises an error because hash is empty" do
            expect{ User.register({user: {password: ""}})}.to raise_error BadRequestException, "Email is required"
        end

        it "raises an error because user already registered" do
            create(:user, email: "test@gmail.com", create_method: "register")
            expect{ User.register({user: {email: "test@gmail.com"}})}.to raise_error BadRequestException, "User already registered"
        end

        it "raises an error because user already signed in" do
            create(:user, email: "test@gmail.com", sign_in_count: 1)
            expect{ User.register({user: {email: "test@gmail.com"}})}.to raise_error BadRequestException, "User already registered"
        end

        it "updates the user" do
            create(:user, email: "test@gmail.com")
            user = ActionController::Parameters.new({user: {email: "test@gmail.com"}})
            user = User.register(user)

            expect(user.sign_in_count).to eq(1)
        end

        it "raises an error because password not supplied" do
            expect {User.register({user: {email: "test@gmail.com"}})}.to raise_error BadRequestException, "Password is required"
        end

        it "creates the user" do
            user = ActionController::Parameters.new({user: {email: "test@gmail.com", password: "password"}})
            user = User.register(user)
            expect(user.sign_in_count).to eq(1)
        end

        it "raises an UnprocessableException" do
            user = ActionController::Parameters.new({user: {email: "test@gmail.com", password: "password"}})
            allow_any_instance_of(User).to receive(:save).and_return false

            expect{User.register(user)}.to raise_error UnprocessableException
        end

        it "updates the accepted_transfer_at because of a transfer register" do
            @partner = create(:partner, name: "Test Inspections", contact: "Test Inspector", partner_key: "test")
            @partner_user = create(:user, email: @partner.email, :role => "inspector")
            @partner_user.add_role :partner_admin, @partner
            @partner.partner_configuration.automation_binder_action = true
            @partner.save
            @config = @partner.partner_configuration
            @template = create(:binder_template, partner_configuration_id: @config.id, transfer_note: "This is a transfer note")
            create(:appliance_template, binder_template_id: @template.id)
            create(:contractor_template, binder_template_id: @template.id)
            create(:document_template, binder_template_id: @template.id)
            create(:maintenance_template, binder_template_id: @template.id, due_date: nil, frequency: "As Needed", initial_due_date_interval: 200)

            # create the binder
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}

            parameters = {
                :key => "test",
                :client => client,
                :property => property,
                :binder_template_id => @template.id,
                :agent => agent,
                :property_photo => nil,
                :documents => nil
            }

            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

            # create a binder
            response = Partner::Automation::Service.create_binder(parameters)

            binder = Binder.find(response[:id])

            # change the creation time so we can transfer the binder
            binder.created_at = 12.hours.ago
            binder.save

            # transfer the binder
            Partner::Automation::Service::Access.new.transfer_binders

            # get the attribues and make sure they're correct
            binders = Binder.with_role(:owner, @partner_user)
            homeowner = User.with_role(:owner, binder).first

            expect(binders.count).to eq(0)
            expect(homeowner.email).to eq("transfers@homebinder.com")

            # register the user
            user = ActionController::Parameters.new({user: {email: "transfers@homebinder.com", password: "password"}})
            user = User.register(user)
            expect(user.sign_in_count).to eq(1)
            expect(user.accepted_transfer_at).to eq(Date.today)
        end

        it "raises an error" do
            @partner = create(:partner, name: "Test Inspections", contact: "Test Inspector", partner_key: "test")
            @partner_user = create(:user, email: @partner.email, :role => "inspector")
            @partner_user.add_role :partner_admin, @partner
            @partner.partner_configuration.automation_binder_action = true
            @partner.save
            @config = @partner.partner_configuration
            @template = create(:binder_template, partner_configuration_id: @config.id, transfer_note: "This is a transfer note")
            create(:appliance_template, binder_template_id: @template.id)
            create(:contractor_template, binder_template_id: @template.id)
            create(:document_template, binder_template_id: @template.id)
            create(:maintenance_template, binder_template_id: @template.id, due_date: nil, frequency: "As Needed", initial_due_date_interval: 200)

            # create the binder
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}

            parameters = {
                :key => "test",
                :client => client,
                :property => property,
                :binder_template_id => @template.id,
                :agent => agent,
                :property_photo => nil,
                :documents => nil
            }

            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

            # create a binder
            response = Partner::Automation::Service.create_binder(parameters)

            binder = Binder.find(response[:id])

            # change the creation time so we can transfer the binder
            binder.created_at = 12.hours.ago
            binder.save

            # transfer the binder
            Partner::Automation::Service::Access.new.transfer_binders

            # get the attribues and make sure they're correct
            binders = Binder.with_role(:owner, @partner_user)
            homeowner = User.with_role(:owner, binder).first

            expect(binders.count).to eq(0)
            expect(homeowner.email).to eq("transfers@homebinder.com")

            # register the user
            user = ActionController::Parameters.new({user: {email: "transfers@homebinder.com", password: "password"}})
            allow_any_instance_of(User).to receive(:update_attributes).and_return false

            expect{User.register(user)}.to raise_error UnprocessableException
        end
    end

    describe "register_for_transfer" do
        it "creates the user" do
            user = User.register_for_transfer({email: "test@gmail.com"})
            expect(user.sign_in_count).to eq(0)
            expect(user.create_method).to eq("transfer")
            #expect(user.organization_id).to eq(Organization.where(key: "homeowner").first.id)
        end
    end

    describe "register_for_share" do
        it "creates the user" do
            user = User.register_for_share({email: "test@gmail.com"})
            expect(user.sign_in_count).to eq(0)
            expect(user.create_method).to eq("share")
            #expect(user.organization_id).to eq(Organization.where(key: "homeowner").first.id)
        end
    end

    describe "opt_out" do
        before :each do
            @orphan_user = create(:user, email: "orphans@homebinder.com")
            @user = create(:user)
            @partner_user = create(:user)
            @binder = create(:binder)
        end

        it "removes from share" do
            user_id = @user.id
            user_email = @user.email
            create(:user_binder, user_id: @user.id, binder_id: @binder.id, role: UserBinderRoles::CO_OWNER)
            create(:user_binder, user_id: @partner_user.id, binder_id: @binder.id, role: UserBinderRoles::OWNER)

            share = @binder.shares.create(:shared_by_id => @partner_user.id, :shared_with_id => @user.id, :status => "sent", :role_name => "co_owner")

            User.opt_out(share.access_token)

            expect(UserBinder.where(user_id: user_id).count).to eq(0)
            expect(User.where(email: user_email).first).to be_nil
            expect(@partner_user.binders.count).to eq(1)
        end

        it "removes from transfer" do
            user_id = @user.id
            user_email = @user.email
            create(:user_binder, user_id: @user.id, binder_id: @binder.id, role: UserBinderRoles::OWNER)

            transfer = @binder.transfers.create(:sender_id => @partner_user.id, :receiver_id => @user.id, :status => "sent", :transfer_type => "ownership")

            User.opt_out(transfer.access_token)

            expect(UserBinder.where(user_id: user_id).count).to eq(0)
            expect(User.where(email: user_email).first).to be_nil
            expect(UserBinder.where(user_id: @orphan_user.id, binder_id: @binder.id).count).to eq(1)
        end

        it "does not delete user when multiple binders" do
            binder2 = create(:binder)
            sender = create(:user)
            create(:user_binder, user_id: @user.id, binder_id: binder2.id, role: UserBinderRoles::OWNER)
            create(:user_binder, user_id: @user.id, binder_id: @binder.id, role: UserBinderRoles::OWNER)
            transfer = @binder.transfers.create(:receiver_id => @user.id, :sender_id => sender.id, :transfer_type => "ownership", :status => "sent")

            User.opt_out(transfer.access_token)

            expect(User.where(email: @user.email).first).to_not be_nil
            expect(@user.binders.count).to eq(1)
            expect(@user.binders[0].id).to eq(binder2.id)
        end
    end

    describe "binders_waiting_by_email" do
        it "return empty array" do
            user = create(:user)
            array = User.binders_waiting_by_email(user.email)
            expect(array).to eq({:binders => [], :branding => nil})
        end
    end

    describe "binders_waiting_by_id" do
        it "return empty array" do
            user = create(:user)
            array = User.binders_waiting_by_id(user.id)
            expect(array).to eq({:binders => [], :branding => nil})
        end
    end

    describe "signin" do
        it "raises an error because email is empty" do
            expect {User.signin("", "password")}.to raise_error BadRequestException, "Email and password are required"
        end

        it "raises an error because password is nil" do
            expect {User.signin("test@gmail.com", "")}.to raise_error BadRequestException, "Email and password are required"
        end

        it "raises an error because user doesn't exist" do
            expect {User.signin("test@gmail.com", "password")}.to raise_error BadRequestException, "Invalid credentials"
        end

        it "raises an error because password is incorrect" do
            create(:user, email: "test@gmail.com")
            expect {User.signin("test@gmail.com", "password")}.to raise_error BadRequestException, "Invalid credentials"
        end

        it "signs the homeowner in and calls init_binders" do
            user = create(:user, email: "test@gmail.com", :role => "homeowner")
            expect_any_instance_of(User).to receive(:init_binders)
            signed_in = User.signin("test@gmail.com", user.password)

            expect(signed_in.sign_in_count).to eq(1)
        end

        it "signs the inspector in and calls init_binders" do
            inspector = create(:user, email: "test@gmail.com", :role => "inspector")
            expect_any_instance_of(User).to receive(:init_binders)
            signed_in = User.signin("test@gmail.com", inspector.password)

            expect(signed_in.sign_in_count).to eq(1)
        end
    end

    describe "change_email" do
        it "raise an error because of invalid user" do
            user = create(:user)
            expect{User.change_email(user, {user: nil})}.to raise_error BadRequestException, "User is required"
        end

        it "raise an error because user was not found" do
            user = create(:user)
            expect{User.change_email(user, {id: 99191, user: {}})}.to raise_error ActiveRecord::RecordNotFound
        end

        it "raise a CanCan error because user was not found" do
            user = create(:user)
            no_access = create(:user)
            expect{User.change_email(no_access, {id: user.id, user: {}})}.to raise_error CanCan::AccessDenied
        end

        it "raise an error because of an invalid password" do
            user = create(:user)
            expect{User.change_email(user, {id: user.id, user: {}, password: "password"})}.to raise_error BadRequestException, "Invalid password"
        end

        it "raise an error because of an invalid email" do
            user = create(:user)
            expect{User.change_email(user, {id: user.id, user: {}, email: "email@test.com", password: user.password})}.to raise_error BadRequestException, "Invalid email"
        end

        it "raise an error because of an invalid email" do
            user = create(:user)
            param = ActionController::Parameters.new({email: ""})
            expect{User.change_email(user, {id: user.id, user: param, email: user.email, password: user.password})}.to raise_error UnprocessableException
        end

        it "changes the users email" do
            user = create(:user, email: "test@gmail.com")
            expect(user.email).to eq("test@gmail.com")

            param = ActionController::Parameters.new({email: "test@test.com"})
            User.change_email(user, {id: user.id, user: param, email: user.email, password: user.password})
            user.reload

            expect(user.email).to eq("test@test.com")
        end
    end

    describe "change_password" do
        it "raise an error because of invalid user" do
            user = create(:user)
            expect{User.change_password(user, {user: nil})}.to raise_error BadRequestException, "User is required"
        end

        it "raise an error because user was not found" do
            user = create(:user)
            expect{User.change_password(user, {id: 99191, user: {}})}.to raise_error BadRequestException, "User not found"
        end

        it "raise a CanCan error because user was not found" do
            user = create(:user)
            no_access = create(:user)
            expect{User.change_password(no_access, {id: user.id, user: {}})}.to raise_error ::CanCan::AccessDenied
        end

        it "raise an error because of an invalid password" do
            user = create(:user)
            expect{User.change_password(user, {id: user.id, user: {}, password: "password"})}.to raise_error BadRequestException, "Invalid password"
        end

        it "raise an error because of an invalid password" do
            user = create(:user)
            param = ActionController::Parameters.new({password: Faker::Lorem.characters(129)})
            expect{User.change_password(user, {id: user.id, user: param, password: user.password})}.to raise_error UnprocessableException
        end

        it "saves the password" do
            user = create(:user, password: "password", password_confirmation: "password")
            expect(user.password).to eq("password")

            param = ActionController::Parameters.new({password: "newpassword"})
            changed = User.change_password(user, {id: user.id, user: param, password: user.password})
            user = User.find(user.id)

            expect(user.id).to eq(changed.id)
            expect(user.valid_password? "newpassword").to_not eq(false)
        end
    end

    describe "reset_password" do
        it "raise an error because of missing params" do
            expect{User.reset_password({})}.to raise_error BadRequestException
        end

        it "raise an error because of missing params" do
            expect{User.reset_password({:user => {}})}.to raise_error BadRequestException
        end

        it "raise an error because of missing user" do
            expect{User.reset_password({:user => {:email => "test@gmail.com"}})}.to raise_error BadRequestException
        end

        it "resets password" do
            reset = create(:user, :email => "TeSt@gMaIl.CoM")
            double = double("UserMailer", :deliver_later => true)
            allow(UserMailer).to receive(:send_reset_password_instructions).and_return(double)

            User.reset_password({:user => {:email => "TeSt@GMAIL.CoM"}})
            reset.reload

            expect(reset.reset_password_token).to_not be(nil)
            expect(reset.reset_password_sent_at).to_not be(nil)
            double = double("UserMailer", :deliver_later => true)
            expect(UserMailer).to have_received(:send_reset_password_instructions)
        end
    end

    describe "update_password" do
        it "raise an error because of missing params" do
            expect{User.update_password({})}.to raise_error BadRequestException
        end

        it "raise an error because of missing params" do
            expect{User.update_password({:user => {}})}.to raise_error BadRequestException
        end

        it "raise an error because of invalid email" do
            reset = create(:user)
            token = reset.send_reset_password_instructions
            password = "password"
            user = {
                :email => "test@gmail.com",
                :reset_password_token =>token,
                :password => password,
                :password_confirmation => password
            }
            expect{User.update_password({user: user})}.to raise_error BadRequestException
        end

        it "raise an error because of invalid token" do
            reset = create(:user)
            password = "password"
            user = {
                :email => reset.email,
                :reset_password_token =>"faketoken",
                :password => password,
                :password_confirmation => password
            }
            expect{User.update_password({user: user})}.to raise_error BadRequestException
        end

        it "raise an error because user email and token are not for same object" do
            reset = create(:user)
            invalid = create(:user)
            token = reset.send_reset_password_instructions
            password = "password"
            user = {
                :email => invalid.email,
                :reset_password_token =>token,
                :password => password,
                :password_confirmation => password
            }
            expect{User.update_password({user: user})}.to raise_error BadRequestException
        end

        it "raises an UnprocessableException" do
            reset = create(:user, :email => "CAPITALletters@gmail.COM")
            token = reset.send_reset_password_instructions
            password = "password"
            user = {
                :email => "capitalLETTERS@gmail.COM",
                :reset_password_token =>token,
                :password => password,
                :password_confirmation => password
            }

            allow_any_instance_of(User).to receive(:save).and_return false

            expect{User.update_password({user: user})}.to raise_error UnprocessableException
        end

        it "resets the password" do
            reset = create(:user, :email => "CAPITALletters@gmail.COM")
            token = reset.send_reset_password_instructions
            password = "password"
            user = {
                :email => "capitalLETTERS@gmail.COM",
                :reset_password_token =>token,
                :password => password,
                :password_confirmation => password
            }

            User.update_password({user: user})
            reset.reload

            expect(reset.valid_password?("password")).to_not eq(false)
        end
    end

    describe "confirm_password" do
        it "raise an error because of missing params" do
            expect{User.confirm_password(nil, {})}.to raise_error BadRequestException
        end

        it "returns the user" do
            reset = create(:user, :email => "CAPITALletters@gmail.COM")
            token = reset.send_reset_password_instructions

            params = {
                :id => reset.id,
                :token => token
            }

            user = User.confirm_password(nil, params)

            expect(user.id).to eq(reset.id)
        end
    end

    describe "find_user_by_email" do
        it "returns nil" do
            homeowner = create(:user)
            user = User.find_user_by_email(homeowner, {:email => nil})
            expect(user).to be(nil)
        end

        it "returns nil" do
            homeowner = create(:user)
            user = User.find_user_by_email(homeowner, {:email => "test@gmail.com"})
            expect(user).to be(nil)
        end

        it "returns a user" do
            homeowner = create(:user)
            user = User.find_user_by_email(homeowner, {:email => homeowner.email})

            expect(user).to_not be(nil)
            expect(homeowner.email).to eq(user.email)
        end
    end

    describe "welcome" do
        it "raises an error because params is empty" do
            expect {User.welcome({})}.to raise_error BadRequestException, "Access token required"
        end

        it "returns homeowner and sets transfer acceptance_method" do
            homeowner = create(:user)
            transfer = create(:transfer, :receiver => homeowner)
            user = User.welcome({:id => transfer.access_token, :welcomeMethod => "text"})
            transfer.reload

            expect(user).to_not be(nil)
            expect(user.id).to eq(homeowner.id)
            expect(transfer.acceptance_method).to eq("text")
        end

        it "returns homeowner" do
            homeowner = create(:user)
            transfer = create(:share, :receiver => homeowner)
            user = User.welcome({:id => transfer.access_token})

            expect(user).to_not be(nil)
            expect(user.id).to eq(homeowner.id)
        end

        it "returns agent" do
            agent = create(:user, :role => "agent")
            partner = create(:partner)
            PartnerUser.create!(:user => agent, :partner => partner, :role => "agent")

            agent.invite!
            user = User.welcome({:id => agent.access_token})

            expect(user).to_not be(nil)
            expect(user.id).to eq(agent.id)
        end

        it "raises an error" do
            expect{User.welcome({:id => Faker::Lorem.characters(51)})}.to raise_error BadRequestException, "User account does not exist"
        end
    end

    describe "waiting_binders" do
        it "returns a pending binder transfer when create method is nil" do
            inspector = create(:user, :role => "inspector", :user_profile_attributes => {:first_name => "test", :company => "Test Company"})
            homeowner = create(:user, :role => "homeowner", :create_method => nil)
            binder = create(:binder)
            create(:user_binder, :binder => binder, :user => homeowner, :role => "owner")
            create(:transfer, :binder => binder, :sender => inspector, :receiver => homeowner, :status => "sent")

            waiting = homeowner.waiting_binders
            expect(waiting[:binders].count).to eq(1)
        end
        
        it "returns a pending binder transfer when create method is transfer" do
            inspector = create(:user, :role => "inspector", :user_profile_attributes => {:first_name => "test", :company => "Test Company"})
            homeowner = create(:user, :role => "homeowner", :create_method => "transfer")
            binder = create(:binder)
            create(:user_binder, :binder => binder, :user => homeowner, :role => "owner")
            create(:transfer, :binder => binder, :sender => inspector, :receiver => homeowner, :status => "sent")

            waiting = homeowner.waiting_binders
            expect(waiting[:binders].count).to eq(1)
        end

        it "returns a pending binder transfer when create method is sell_side" do
            inspector = create(:user, :role => "inspector", :user_profile_attributes => {:first_name => "test", :company => "Test Company"})
            homeowner = create(:user, :role => "homeowner", :create_method => "sell_side_inspection")
            binder = create(:binder)
            create(:user_binder, :binder => binder, :user => homeowner, :role => "owner")
            create(:transfer, :binder => binder, :sender => inspector, :receiver => homeowner, :status => "sent")

            waiting = homeowner.waiting_binders
            expect(waiting[:binders].count).to eq(1)
        end

        it "returns a pending binder share" do
            inspector = create(:user, :role => "inspector", :user_profile_attributes => {:first_name => "test", :company => "Test Company"})
            homeowner = create(:user, :role => "homeowner", :create_method => "share")
            binder = create(:binder)
            create(:user_binder, :binder => binder, :user => homeowner, :role => "owner")
            create(:share, :binder => binder, :sender => inspector, :receiver => homeowner, :status => "sent")

            waiting = homeowner.waiting_binders
            expect(waiting[:binders].count).to eq(1)
        end
    end

    describe "#destroy" do
        it "removes the user and binder" do
            user = create(:user, email: "test@gmail.com", role: "homeowner")
            binder = create(:binder)
            create(:user_binder, :user => user, :binder => binder, :role => "owner")

            User.destroy(request, user.id)

            expect{User.find(user.id)}.to raise_error ActiveRecord::RecordNotFound
            expect{Binder.find(binder.id)}.to raise_error ActiveRecord::RecordNotFound
        end

        it "updates the transfer and share" do
            user = create(:user, email: "test@gmail.com", role: "homeowner")
            binder_1 = create(:binder)
            binder_2 = create(:binder)
            create(:user_binder, :user => user, :binder => binder_1, :role => "reader")
            create(:user_binder, :user => user, :binder => binder_2, :role => "reader")
            transfer = create(:transfer, :binder => binder_1, :sender => admin, :receiver => user, :status => "sent")
            share = create(:share, :binder => binder_2, :sender => admin, :receiver => user, :status => "sent")

            User.destroy(request, user.id)
            transfer.reload
            share.reload

            expect{User.find(user.id)}.to raise_error ActiveRecord::RecordNotFound
            expect{Binder.find(binder_1.id)}.to_not raise_error
            expect(transfer.status).to eq("declined")
            expect(share.status).to eq("declined")
        end
    end

    describe "#invite" do
        it "creates a new user and adds the user" do
            double = double("UserMailer", :deliver_later => true)
            allow(UserMailer).to receive(:send_invitation).and_return(double)
            user = create(:user, email: "test@gmail.com", role: "inspector")
            partner = create(:partner)
            user.add_role(:partner_admin, partner)
            User.invite(request, {:id => user.id})

            expect(UserMailer).to have_received(:send_invitation)
        end
    end

    describe "#send_failed_payment_notification" do
        it "calls send_failed_payment_notification" do
            double = double("AccountMailer", :deliver_later => true)
            allow(AccountMailer).to receive(:send_failed_payment_notification).and_return(double)
            user = create(:user, email: "test@gmail.com", role: "inspector")
            user.send_failed_payment_notification

            expect(AccountMailer).to have_received(:send_failed_payment_notification)
        end
    end

    describe "#pager" do
        context "for_admin" do
            it "gets all users for admin" do
                create_list(:user, 9)
                page = User.pager(request)
                expect(page.total).to eq(10)
            end

            it "gets all users with admin role" do
                create_list(:user, 9)
                page = User.pager(request, {:role => "admin"})
                expect(page.total).to eq(1)
            end

            it "gets all partner users" do
                inspector = create(:user, :role => "inspector")
                partner = create(:partner, :partner_type => "inspector")
                create(:partner_user, :partner => partner, :user => inspector, :role => "admin")

                create_list(:user, 8)
                page = User.pager(request, {:partnerId => partner.id})
                expect(page.total).to eq(1)
            end

            it "gets all registered users" do
                create(:user, :create_method => "register")
                create_list(:user, 8, :create_method => "transfer")
                page = User.pager(request, {:create_method => "register"})
                expect(page.total).to eq(1)
            end

            it "gets all users in Florida" do
                address_1 = {address1: "123 main street", city: "Miami", state: "FL", country: "US"}
                address_2 = {address1: "123 main street", city: "Toronto", state: "ON", country: "CA"}
                create(:user, :create_method => "register", :user_profile_attributes => {:first_name => "test", :company => "Test Company", :address_attributes => address_1})
                create_list(:user, 8, :create_method => "transfer", :user_profile_attributes => {:first_name => "test", :company => "Test Company", :address_attributes => address_2})
                page = User.pager(request, {:state => "FL"})
                expect(page.total).to be > 0
            end

            it "gets all users in CA" do
                address = {address1: "123 main street", city: "Toronto", state: "ON", country: "CA"}
                create(:user, :create_method => "register", :user_profile_attributes => {:first_name => "test", :company => "Test Company", :address_attributes => address})
                create_list(:user, 8, :create_method => "transfer")
                page = User.pager(request, {:country => "CA"})
                expect(page.total).to eq(1)
            end

            it "searches" do
                address = {address1: "123 main street", city: "Toronto", state: "ON", country: "CA"}
                create(:user, :create_method => "register", :user_profile_attributes => {:first_name => "test", :company => "Test Company", :address_attributes => address})
                create_list(:user, 8, :create_method => "transfer")
                page = User.pager(request, {:country => "CA", :search => "test"})
                expect(page.total).to eq(1)
            end
        end

        context "for_partner" do
            it "gets all users for partner" do
                inspector = create(:user, :role => "inspector")
                apr_reviewer = create(:user, :role => "apr_reviewer")
                agent = create(:user, :role => "agent")
                partner = create(:partner, :partner_type => "inspector")
                create(:partner_user, :partner => partner, :user => inspector, :role => "admin")
                create(:partner_user, :partner => partner, :user => apr_reviewer, :role => "member")
                create(:partner_user, :partner => partner, :user => agent, :role => "agent")

                request = HBRequest.new.create_request(inspector)

                create_list(:user, 8)
                page = User.pager(request, {searchMethod: "for_partner", partnerId: partner.id, role: "inspector"})
                expect(page.total).to eq(2)
            end

            it "gets all agents for partner" do
                inspector = create(:user, :role => "inspector")
                partner = create(:partner, :partner_type => "inspector")
                create(:partner_user, :partner => partner, :user => inspector, :role => "admin")

                agents = create_list(:user, 8, :role => "agent")
                agents.each do |agent|
                    PartnerUser.create!(:partner => partner, :user => agent, :role => "agent")
                end

                request = HBRequest.new.create_request(inspector)

                page = User.pager(request, {searchMethod: "for_partner", partnerId: partner.id, :role => "agent"})
                expect(page.total).to eq(8)
            end

            it "searches" do
                inspector = create(:user, :role => "inspector")
                partner = create(:partner, :partner_type => "inspector")
                create(:partner_user, :partner => partner, :user => inspector, :role => "admin")

                request = HBRequest.new.create_request(inspector)

                create_list(:user, 8)
                page = User.pager(request, {searchMethod: "for_partner", partnerId: partner.id, search: inspector.email})
                expect(page.total).to eq(1)
            end
        end

        context "for_organization" do
            it "gets all users for organization" do
                inspector = create(:user, :role => "inspector")
                organization = create(:organization)
                create(:organization_user, :organization => organization, :user => inspector)

                create_list(:user, 8)
                page = User.pager(request, {searchMethod: "for_organization", organizationId: organization.id})
                expect(page.total).to eq(1)
            end

            it "searches" do
                inspector = create(:user, :role => "inspector")
                organization = create(:organization)
                create(:organization_user, :organization => organization, :user => inspector)

                create_list(:user, 8)
                page = User.pager(request, {searchMethod: "for_organization", organizationId: organization.id, search: inspector.email})
                expect(page.total).to eq(1)
            end
        end
    end
end
