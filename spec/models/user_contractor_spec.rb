require 'rails_helper'

RSpec.describe UserContractor, :type => :model do
    describe "#add_to_binders" do
        it "adds the pro to all binders connected to the agent and then removes binder_contractors" do
            Sidekiq::Testing.inline! do
                agent = create(:user, :role => "agent")
                
                create_list(:user_binder, 2, :role => "owner", :user => agent)
                buyer_agent_user_binders = create_list(:user_binder, 2, :role => "buyer_agent", :user => agent)
                seller_agent_user_binders = create_list(:user_binder, 1, :role => "seller_agent", :user => agent)
                
                combined_user_binders = buyer_agent_user_binders + seller_agent_user_binders
                combined_user_binders.each do |user_binder|
                    create(:user_binder, :binder_id => user_binder.binder_id, :role => "owner")
                end
                
                agent.binder_ids.each do |binder_id|
                    create(:binder_contractor, :binder_id => binder_id)
                end
                
                
                user_contractor = create(:user_contractor, :user => agent)
                expect(user_contractor.user_id).to eq(agent.id)
                expect(user_contractor.binder_contractor_ids.count).to eq(5)
                
                binder_contractor_ids = user_contractor.binder_contractor_ids
                user_contractor.destroy
                
                expect(Binder::BinderContractor.where(:id => binder_contractor_ids).exists?).to eq(false)
            end
        end
    end
end
