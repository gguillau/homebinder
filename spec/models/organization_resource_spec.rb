require 'rails_helper'

RSpec.describe OrganizationResource, :type => :model do
    
    it "has a valid factory" do
        organization_resource = build :organization_resource
        expect(organization_resource).to be_valid
    end
    
    it "is invalid because organization_id is required" do
        organization_resource = build(:organization_resource, organization_id: nil)
        expect(organization_resource).not_to be_valid
        expect(organization_resource.errors.full_messages.first).to eq("Organization is required")
    end
    
    it "is invalid because marketing_resource_id is required" do
        organization_resource = build(:organization_resource, marketing_resource_id: nil)
        expect(organization_resource).not_to be_valid
        expect(organization_resource.errors.full_messages.first).to eq("Marketing resource is required")
    end
    
    it "is invalid because marketing_resource already exists in organization" do
        organization_resource = create(:organization_resource)
        expect(organization_resource).to be_valid
        organization_resource_2 = build(:organization_resource, organization_id: organization_resource.organization_id, marketing_resource_id: organization_resource.marketing_resource_id)
        expect(organization_resource_2).not_to be_valid
        expect(organization_resource_2.errors.full_messages.first).to eq("Marketing resource already exists for organization")
    end
end