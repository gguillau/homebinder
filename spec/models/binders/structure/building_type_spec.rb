require 'rails_helper'
require 'mandrill'
require 'cancan'

RSpec.describe Binder::Structure::BuildingType do
    
    it "has a valid factory" do
        building_type = build(:building_type)
        expect(building_type).to be_valid
    end
    
    it "requires a name" do
        building_type = build(:building_type, name: nil)
        expect(building_type).to_not be_valid
        expect(building_type.errors.full_messages.first).to eq("Name is required")
    end
    
    it "requires a created_by" do
        building_type = build(:building_type, created_by: nil)
        expect(building_type).to_not be_valid
        expect(building_type.errors.full_messages.first).to eq("Created by is required")
    end

end