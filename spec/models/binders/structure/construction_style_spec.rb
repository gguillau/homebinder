require 'rails_helper'
require 'mandrill'
require 'cancan'

RSpec.describe Binder::Structure::ConstructionStyle do
    
    it "has a valid factory" do
        construction_style = build(:construction_style)
        expect(construction_style).to be_valid
    end
    
    it "requires a name" do
        construction_style = build(:construction_style, name: nil)
        expect(construction_style).to_not be_valid
        expect(construction_style.errors.full_messages.first).to eq("Name is required")
    end
    
    it "requires a created_by" do
        construction_style = build(:construction_style, created_by: nil)
        expect(construction_style).to_not be_valid
        expect(construction_style.errors.full_messages.first).to eq("Created by is required")
    end

end