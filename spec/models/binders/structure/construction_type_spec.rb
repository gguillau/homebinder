require 'rails_helper'
require 'mandrill'
require 'cancan'

RSpec.describe Binder::Structure::ConstructionType do
    
    it "has a valid factory" do
        construction_type = build(:construction_type)
        expect(construction_type).to be_valid
    end
    
    it "requires a name" do
        construction_type = build(:construction_type, name: nil)
        expect(construction_type).to_not be_valid
        expect(construction_type.errors.full_messages.first).to eq("Name is required")
    end
    
    it "requires a created_by" do
        construction_type = build(:construction_type, created_by: nil)
        expect(construction_type).to_not be_valid
        expect(construction_type.errors.full_messages.first).to eq("Created by is required")
    end

end