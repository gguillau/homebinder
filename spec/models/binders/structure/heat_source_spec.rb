require 'rails_helper'
require 'mandrill'
require 'cancan'

RSpec.describe Binder::Structure::HeatSource do
    
    it "has a valid factory" do
        heat_source = build(:heat_source)
        expect(heat_source).to be_valid
    end
    
    it "requires a name" do
        heat_source = build(:heat_source, name: nil)
        expect(heat_source).to_not be_valid
        expect(heat_source.errors.full_messages.first).to eq("Name is required")
    end
    
    it "requires a created_by" do
        heat_source = build(:heat_source, created_by: nil)
        expect(heat_source).to_not be_valid
        expect(heat_source.errors.full_messages.first).to eq("Created by is required")
    end

end