require 'rails_helper'
require 'mandrill'
require 'cancan'

RSpec.describe Binder::Structure::HeatType do
    
    it "has a valid factory" do
        heat_type = build(:heat_type)
        expect(heat_type).to be_valid
    end
    
    it "requires a name" do
        heat_type = build(:heat_type, name: nil)
        expect(heat_type).to_not be_valid
        expect(heat_type.errors.full_messages.first).to eq("Name is required")
    end
    
    it "requires a created_by" do
        heat_type = build(:heat_type, created_by: nil)
        expect(heat_type).to_not be_valid
        expect(heat_type.errors.full_messages.first).to eq("Created by is required")
    end

end