require 'rails_helper'

RSpec.describe Binder::BinderContractor, :type => :model do
  
  before :each do
    @user = create(:user)
    @no_access = create(:user)
    @binder = create(:binder)
    create(:subscription, binder_id: @binder.id, plan_id: "free")
    @user.add_role(:owner, @binder)
    @request = HBRequest.new.create_request(@user)
    @no_access = create(:user)
    @no_request = HBRequest.new.create_request(@no_access)
  end
  
  it "requires a binder_id" do
    binder_contractor = build(:binder_contractor, binder_id: nil)
    expect(binder_contractor).to_not be_valid
    expect(binder_contractor.errors.full_messages.first).to eq("Binder is required")
  end
  
  it "gets all binder_contractors for a binder" do
    create(:binder_contractor, binder_id: @binder.id)
    create(:binder_contractor, binder_id: @binder.id)
    create(:binder_contractor, binder_id: create(:binder).id)
    page = Binder::BinderContractor.index(@request, {:binderId => @binder.id})
    expect(page.total).to be > 0
  end
  
  it "gets a binder_contractor" do
    binder_contractor = create(:binder_contractor, binder_id: @binder.id)
    expect(Binder::BinderContractor.show(@request, {:id => binder_contractor.id})).to_not be_nil
  end
  
  it "creates a binder_contractor with types" do
    type = create(:contractor_contractor_category, :name => "Real Estate")
    cont =  {
        :binder_contractor => {
          :binder_id => @binder.id,
          :contact => "BinderContractor 1",
          :types_attributes => [{name: "Home Inspector"}],
          :binder_contractor_types_attributes => [{:contractor_category_id => type.id}],
          :contractor_attributes => {
              :name => "BinderContractor 1",
              :created_by => @request.user.id,
              :types_attributes => [{name: SecureRandom.hex(4)}]
          }
        }
      }
    cont = ActionController::Parameters.new(cont)
    expect(@binder.binder_contractors.count).to eq(0)
    Binder::BinderContractor.build(@request, cont)
    expect(@binder.binder_contractors.count).to eq(1)
    expect(@binder.binder_contractors.first.types.count).to eq(2)
  end
  
  it "updates a binder_contractor with types" do
    partner = create(:partner)
    binder_contractor = create(:binder_contractor, contact: "BinderContractor1", binder_id: @binder.id)
    expect(binder_contractor.types.count).to eq(1)

    type = create(:contractor_contractor_category, :name => "Real Estate")
    cont = binder_contractor.attributes
    cont[:contact] = "BinderContractor 2"
    cont[:tags_attributes] = [{ tag: "partner_#{partner.id}"}]
    cont[:types_attributes] = [{name: "Home Inspector"}, {name: "Test"}]
    cont[:binder_contractor_types_attributes] = [{contractor_category_id: type.id}]
    
    update = ActionController::Parameters.new({:id => binder_contractor[:id],:binder_contractor => cont})
    
    binder_contractor = Binder::BinderContractor.update(@request, update)
    expect(binder_contractor.contact).to eq("BinderContractor 2")
    expect(binder_contractor.types.count).to eq(3)
  end
  
  it "deletes a binder_contractor" do
    binder_contractor = create(:binder_contractor, contact: "BinderContractor1", binder_id: @binder.id)
    Binder::BinderContractor.destroy(@request, binder_contractor.id)
    expect(Binder::BinderContractor.where(:id => binder_contractor.id).count).to eq(0)
  end
  
  it "contact a binder_contractor" do
    cont = create(:binder_contractor, :binder_id => @binder.id)
    double = double("BinderMailer", :deliver_later => true)
    allow(BinderMailer).to receive(:home_pro_work_request).and_return(double)
    allow(BinderMailer).to receive(:home_pro_work_request_confirmation).and_return(double)
    allow(BinderMailer).to receive(:home_pro_work_request_support).and_return(double)
    
    Binder::BinderContractor.contact(@request, {:id => cont.id, :message => "hey"})
    
    expect(BinderMailer).to have_received(:home_pro_work_request)
    expect(BinderMailer).to have_received(:home_pro_work_request_confirmation)
    expect(BinderMailer).to have_received(:home_pro_work_request_support)
  end
  
end