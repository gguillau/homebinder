require 'rails_helper'
require 'mandrill'
require 'cancan'

RSpec.describe Binder::Appliance::Manufacturer do
    
    it "has a valid factory" do
        appliance_manufacturer = build(:appliance_manufacturer)
        expect(appliance_manufacturer).to be_valid
    end
    
    it "requires a name" do
        appliance_manufacturer = build(:appliance_manufacturer, name: nil)
        expect(appliance_manufacturer).to_not be_valid
        expect(appliance_manufacturer.errors.full_messages.first).to eq("Name is required")
    end

end