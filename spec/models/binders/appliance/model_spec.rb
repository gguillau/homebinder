require 'rails_helper'
require 'mandrill'
require 'cancan'

RSpec.describe Binder::Appliance::Model do
    
    it "has a valid factory" do
        appliance_model = build(:appliance_model)
        expect(appliance_model).to be_valid
    end
    
    it "requires a name" do
        appliance_model = build(:appliance_model, name: nil)
        expect(appliance_model).to_not be_valid
        expect(appliance_model.errors.full_messages.first).to eq("Name is required")
    end

end