require 'rails_helper'
require 'mandrill'
require 'cancan'

RSpec.describe Binder::Appliance::Category do
    
    it "has a valid factory" do
        appliance_category = build(:appliance_category)
        expect(appliance_category).to be_valid
    end
    
    it "requires a name" do
        appliance_category = build(:appliance_category, name: nil)
        expect(appliance_category).to_not be_valid
        expect(appliance_category.errors.full_messages.first).to eq("Name is required")
    end

end