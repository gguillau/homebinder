require 'rails_helper'
require 'mandrill'
require 'cancan'

RSpec.describe Binder::Appliance::ApplianceRecall do
    
    it "has a valid factory" do
        appliance_recall = build(:appliance_recalls)
        expect(appliance_recall).to be_valid
    end
    
    it "requires a recall_id" do
        appliance_recall = build(:appliance_recalls, recall_id: nil)
        expect(appliance_recall).to_not be_valid
        expect(appliance_recall.errors.full_messages.first).to eq("Recall is required")
    end
    
    it "requires a appliance_id" do
        appliance_recall = build(:appliance_recalls, appliance_id: nil)
        expect(appliance_recall).to_not be_valid
        expect(appliance_recall.errors.full_messages.first).to eq("Appliance is required")
    end
end