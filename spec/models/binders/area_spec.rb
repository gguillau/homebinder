require 'rails_helper'

RSpec.describe Binder::Area, :type => :model do
  
  before :each do
    @user = create(:user)
    @no_access = create(:user)
    @binder = create(:binder)
    create(:subscription, binder_id: @binder.id, plan_id: "free")
    @user.add_role(:owner, @binder)
    @request = HBRequest.new.create_request(@user)
    @no_access = create(:user)
    @no_request = HBRequest.new.create_request(@no_access)
  end
  
  it "requires a binder_id" do
    area = build(:area, binder_id: nil)
    expect(area).to_not be_valid
    expect(area.errors.full_messages.first).to eq("Binder is required")
  end
  
  it "requires a name" do
    area = build(:area, name: nil)
    expect(area).to_not be_valid
    expect(area.errors.full_messages.first).to eq("Name is required")
  end
  
  it "prevents duplicate area name" do
      area = create(:area)
      area_2 = build(:area, binder_id: area.binder_id, name: area.name)
      expect(area_2).to_not be_valid
      expect(area_2.errors.full_messages.first).to eq("Name has already been taken")
  end
  
  it "gets all areas for a binder" do
    create(:area, binder_id: @binder.id)
    create(:area, binder_id: @binder.id)
    create(:area, binder_id: create(:binder).id)
    page = Binder::Area.index(@request, {:binderId => @binder.id})
    expect(page.total).to eq(2)
  end
  
  it "gets an area" do
    area = create(:area, binder_id: @binder.id)
    expect(Binder::Area.show(@request, {:id => area.id})).to_not be_nil
  end
  
  it "creates an area" do
    area = {
      :area => {
        :binder_id => @binder.id,
        :name => "Area 1"
      }
    }
    area = ActionController::Parameters.new(area)
    Binder::Area.build(@request, area)
    expect(@binder.areas.count).to eq(1)
  end
  
  it "updates an area" do
    area = create(:area, name: "Area 1", binder_id: @binder.id)
    update = {
      :id => area.id,
      :area => {
        :id => area.id,
        :binder_id => @binder.id,
        :name => "Area 2"
      }
    }
    update = ActionController::Parameters.new(update)
    area = Binder::Area.update(@request, update)
    expect(area.name).to eq("Area 2")
  end
  
  it "deletes an area" do
    structure = create(:structure)
    area = create(:area, name: "Area 1", binder_id: @binder.id)
    
    area.tags.new(:tag => "structure_#{structure.id}").save
    structure.tags.new(:tag => "area_#{area.id}").save
    area_id = area.id

    expect(Binder::Area.where(:id => area.id).count).to eq(1)
    expect(Tag.where(:taggable_id => area.id, :taggable_type => "Binder::Area").count).to eq(1)
    expect(Tag.where(:taggable_id => structure.id, :taggable_type => "Binder::Structure").count).to eq(1)
    
    Binder::Area.destroy(@request, area.id)
      
    expect(Binder::Area.where(:id => area.id).count).to eq(0)
    expect(Tag.where(:taggable_id => area_id, :taggable_type => "Binder::Area").count).to eq(0)
    expect(Tag.where(:taggable_id => structure.id, :taggable_type => "Binder::Structure").count).to eq(0)
  end
  
end