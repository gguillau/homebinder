require 'rails_helper'
require 'mandrill'
require 'cancan'

RSpec.describe Binder::Area::Category do
    
    it "has a valid factory" do
        area_type = build(:area_type)
        expect(area_type).to be_valid
    end
    
    it "requires a name" do
        area_type = build(:area_type, name: nil)
        expect(area_type).to_not be_valid
        expect(area_type.errors.full_messages.first).to eq("Name is required")
    end

end