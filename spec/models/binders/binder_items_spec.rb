require 'rails_helper'

RSpec.describe Binder::BinderItems, :type => :model do

    describe "#index" do
        it "gets item for binder when homeowner searches" do
            homeowner = create(:user, :role => "homeowner")
            binder = create(:binder)
            create(:user_binder, :user => homeowner, :binder => binder, :role => "owner")
            create(:binder_item, :binder => binder)
            request = HBRequest.new.create_request(homeowner)

            page = BinderItem.index(request, {:binderId => binder.id})
            expect(page.total).to eq(1)
        end

        it "gets item for binder when homeowner searches via verified" do
            homeowner = create(:user, :role => "homeowner")
            binder = create(:binder)
            create(:user_binder, :user => homeowner, :binder => binder, :role => "owner")
            create(:binder_item, :binder => binder)
            request = HBRequest.new.create_request(homeowner)

            page = BinderItem.index(request, {:binderId => binder.id, :verified => false})
            expect(page.total).to eq(1)
        end


        it "gets item for binder when contractor searches" do
            contractor = create(:user, :role => "contractor")
            homeowner = create(:user, :role => "homeowner")
            binder = create(:binder)
            create(:user_binder, :user => homeowner, :binder => binder, :role => "owner")
            create(:binder_item, :binder => binder, :user => contractor)
            request = HBRequest.new.create_request(contractor)

            page = BinderItem.index(request, {:userId => contractor.id})
            expect(page.total).to eq(1)
        end
    end

end