require 'rails_helper'
require 'mandrill'
require 'cancan'

RSpec.describe Binder::MaintenanceItem::Category do
    
    it "has a valid factory" do
        maintenance_type = build(:maintenance_type)
        expect(maintenance_type).to be_valid
    end
    
    it "requires a name" do
        maintenance_type = build(:maintenance_type, name: nil)
        expect(maintenance_type).to_not be_valid
        expect(maintenance_type.errors.full_messages.first).to eq("Name is required")
    end

end