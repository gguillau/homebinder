require 'rails_helper'
require 'mandrill'
require 'cancan'

RSpec.describe Binder::MaintenanceItem::MaintenanceEvent do
    
    it "has a valid factory" do
        maintenance_event = build(:maintenance_event)
        expect(maintenance_event).to be_valid
    end
    
    it "requires a maintenance_item_id" do
        maintenance_event = build(:maintenance_event, maintenance_item_id: nil)
        expect(maintenance_event).to_not be_valid
        expect(maintenance_event.errors.full_messages.first).to eq("Maintenance item is required")
    end
    
    it "requires a do_date" do
        maintenance_event = build(:maintenance_event, do_date: nil)
        expect(maintenance_event).to_not be_valid
        expect(maintenance_event.errors.full_messages.first).to eq("Do date is required")
    end
    
    it "requires a created_by" do
        maintenance_event = build(:maintenance_event, created_by: nil)
        expect(maintenance_event).to_not be_valid
        expect(maintenance_event.errors.full_messages.first).to eq("Created by is required")
    end
end