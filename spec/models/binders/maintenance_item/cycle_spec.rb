require 'rails_helper'
require 'mandrill'
require 'cancan'

RSpec.describe Binder::MaintenanceItem::Cycle do
    
    it "has a valid factory" do
        maintenance_cycle = build(:maintenance_cycle)
        expect(maintenance_cycle).to be_valid
    end
    
    it "requires a name" do
        maintenance_cycle = build(:maintenance_cycle, name: nil)
        expect(maintenance_cycle).to_not be_valid
        expect(maintenance_cycle.errors.full_messages.first).to eq("Name is required")
    end

end