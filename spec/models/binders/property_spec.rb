require 'rails_helper'

RSpec.describe Binder::Property, :type => :model do
  before :each do
    @property = create(:property)
  end

  it "has a valid factory" do
    expect(@property).to be_valid
  end

  it "is invalid without an address1" do
    property = build(:property, address1: nil)
    expect(property).not_to be_valid
    expect(property.errors.full_messages.first).to eq("Address1 is required")
  end

  it "is invalid with an address1 length greater than 50" do
    property = build(:property, address1: Faker::Lorem.characters(51))
    expect(property).not_to be_valid
    expect(property.errors.full_messages.first).to eq("Address1 is too long (maximum is 50 characters)")
  end
  
  it "is invalid with a apn length greater than 100" do
    property = build(:property, apn: Faker::Lorem.characters(101))
    expect(property).not_to be_valid
    expect(property.errors.full_messages.first).to eq("Apn is too long (maximum is 100 characters)")
  end
  
  it "is invalid without a country" do
    property = build(:property, state: nil, country: nil)
    expect(property).not_to be_valid
    expect(property.errors.full_messages.first).to eq("Country is required")
  end
  
  it "is invalid with a country length greater than 2" do
    property = build(:property, country: "TEST")
    expect(property).not_to be_valid
    expect(property.errors.full_messages.first).to eq("Country is the wrong length (should be 2 characters)")
  end
  
    it "is invalid with an address2 length greater than 50" do
    property = build(:property, address2: Faker::Lorem.characters(51))
    expect(property).not_to be_valid
    expect(property.errors.full_messages.first).to eq("Address2 is too long (maximum is 50 characters)")
  end
  
  it "is invalid without a city" do
    property = build(:property, city: nil)
    expect(property).not_to be_valid
    expect(property.errors.full_messages.first).to eq("City is required")
  end

  it "is invalid with a city length greater than 50" do
    property = build(:property, city: Faker::Lorem.characters(51))
    expect(property).not_to be_valid
    expect(property.errors.full_messages.first).to eq("City is too long (maximum is 50 characters)")
  end
  
  it "is invalid without a state" do
    property = build(:property, state: nil)
    expect(property).not_to be_valid
    expect(property.errors.full_messages.first).to eq("State is required")
  end
  
  it "is invalid with a zip length greater than 50" do
    property = build(:property, zip: Faker::Lorem.characters(21))
    expect(property).not_to be_valid
    expect(property.errors.full_messages.first).to eq("Zip is too long (maximum is 20 characters)")
  end
  
  it "sets the acres correctly" do
    property = create(:property, :acres => 2.9)
    expect(property.acres.to_f).to eq(2.9)
  end

end
