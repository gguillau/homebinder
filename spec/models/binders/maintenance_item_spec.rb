require 'rails_helper'

RSpec.describe Binder::MaintenanceItem, :type => :model do
  
  before :each do
    @user = create(:user)
    @no_access = create(:user)
    @binder = create(:binder)
    create(:subscription, binder_id: @binder.id, plan_id: "free")
    @user.add_role(:owner, @binder)
    double = double("MaintenanceNotifyMailer", :deliver_later => true)
    allow(MaintenanceNotifyMailer).to receive(:notify_email).and_return(double)
    allow(File).to receive(:read).and_call_original
    @request = HBRequest.new.create_request(@user)
  end
  
  it "requires a binder_id" do
    maintenance_item = build(:maintenance_item, binder_id: nil)
    expect(maintenance_item).to_not be_valid
    expect(maintenance_item.errors.full_messages.first).to eq("Binder is required")
  end
  
  it "requires a name" do
    maintenance_item = build(:maintenance_item, name: nil)
    expect(maintenance_item).to_not be_valid
    expect(maintenance_item.errors.full_messages.first).to eq("Name is required")
  end
  
  it "prevents duplicate maintenance_item name" do
      maintenance_item = create(:maintenance_item)
      maintenance_item_2 = build(:maintenance_item, binder_id: maintenance_item.binder_id, name: maintenance_item.name)
      expect(maintenance_item_2).to_not be_valid
      expect(maintenance_item_2.errors.full_messages.first).to eq("Name has already been taken")
  end
  
  it "gets all maintenance items for a binder" do
    create(:maintenance_item, binder_id: @binder.id)
    create(:maintenance_item, binder_id: @binder.id)
    create(:maintenance_item, binder_id: create(:binder).id)
    page = Binder::MaintenanceItem.index(@request, {:binderId => @binder.id})
    expect(page.total).to eq(2)
  end
  
  it "gets a maintenance item" do
    maintenance_item = create(:maintenance_item, binder_id: @binder.id)
    expect(Binder::MaintenanceItem.show(@request, {:id => maintenance_item.id})).to_not be_nil
  end
  
  it "creates a maintenance item" do
    item = {
        maintenance_item: {
          binder_id: @binder.id,
          name: "maintenance 1",
          interval: 10,
          maintenance_cycle: "months",
          do_date: Time.now
        }
      }
    item = ActionController::Parameters.new(item)
    item = Binder::MaintenanceItem.build(@request, item)
    
    expect(item).to_not be_nil
    expect(@binder.maintenance_items.count).to eq(1)
    expect(item.maintenance_events.count).to eq(1)
  end
  
  it "updates a maintenance item" do
    item = create(:maintenance_item, binder_id: @binder.id)

    update = {
        id: item.id,
        maintenance_item: {
          id: item.id,
          name: "maintenance",
          interval: 5,
          maintenance_cycle: "monthly"
        }
      }
    update = ActionController::Parameters.new(update)
    Binder::MaintenanceItem.update(@request, update)
    
    updated = Binder::MaintenanceItem.where(:id => item.id).first
    expect(updated).to_not be_nil
    expect(updated.name).to eq("maintenance")
    expect(updated.interval).to eq(5)
    expect(updated.maintenance_cycle).to eq("monthly")
  end

  it "deletes a maintenance item" do
    item = create(:maintenance_item, name: "Maintenance 1", binder_id: @binder.id)
    
    Binder::MaintenanceItem.destroy(@request, item.id)
    expect(Binder::MaintenanceItem.where(id: item.id).first).to be_nil
  end
  
  it "sends a test email" do
    item = create(:maintenance_item, name: "Maintenance 1", binder_id: @binder.id, do_date: Date.today)
    item.schedule_next_event
    Binder::MaintenanceItem.send_test_notification_email(@request, item.id)
    expect(MaintenanceNotifyMailer).to have_received(:notify_email).exactly(1).times
  end
  
  it "it sets the correct class base query" do
    expect(Binder::MaintenanceItem.base_query).to_not be(nil)
  end
  
  it "it sets the correct class base includes" do
    expect(Binder::MaintenanceItem.base_includes).to eq([:maintenance_events])
  end
  
  it "it returns the correct hash" do
    hash = Binder::MaintenanceItem.query_arguments_hash("do_date", "Thu Jun 29 2017 00:00:00 GMT-0400 (EDT)")
    date = Date.parse("Thu Jun 29 2017 00:00:00 GMT-0400 (EDT)")
    expect(hash).to eq({:maintenance_events => {:do_date => date}})
  end
  
  describe "#filter_easy_eligible?" do
    it "returns false" do
      item = create(:maintenance_item, :name => "test")
      expect(item.filter_easy_eligible?).to be(false)
    end
    
    it "returns false" do
      item = create(:maintenance_item, :name => "test filter")
      expect(item.filter_easy_eligible?).to be(false)
    end
    
    it "returns false" do
      item = create(:maintenance_item, :name => "water filter")
      expect(item.filter_easy_eligible?).to be(false)
    end
    
    it "returns true" do
      item = create(:maintenance_item, :name => "HvAc FiLteR")
      expect(item.filter_easy_eligible?).to be(true)
    end
    
    it "returns true" do
      item = create(:maintenance_item, :name => "CENTRAL FiLteR")
      expect(item.filter_easy_eligible?).to be(true)
    end
    
    it "returns true" do
      item = create(:maintenance_item, :name => "A/C FiLteR")
      expect(item.filter_easy_eligible?).to be(true)
    end
    
    it "returns true" do
      item = create(:maintenance_item, :name => "AC FiLteR")
      expect(item.filter_easy_eligible?).to be(true)
    end
    
    it "returns true" do
      item = create(:maintenance_item, :name => "furnace FiLteR")
      expect(item.filter_easy_eligible?).to be(true)
    end
    
    it "returns true" do
      item = create(:maintenance_item, :name => "heating FiLteR")
      expect(item.filter_easy_eligible?).to be(true)
    end
    
    it "returns true" do
      item = create(:maintenance_item, :name => "air FiLteR")
      expect(item.filter_easy_eligible?).to be(true)
    end
  end
  
end