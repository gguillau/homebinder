require 'rails_helper'

RSpec.describe Binder::Paint, :type => :model do
  
  before :each do
    @user = create(:user)
    @no_access = create(:user)
    @binder = create(:binder)
    create(:subscription, binder_id: @binder.id, plan_id: "free")
    @user.add_role(:owner, @binder)
    @request = HBRequest.new.create_request(@user)
  end
  
  it "requires a binder_id" do
    paint = build(:paint, binder_id: nil)
    expect(paint).to_not be_valid
    expect(paint.errors.full_messages.first).to eq("Binder is required")
  end
  
  it "requires a name" do
    paint = build(:paint, name: nil)
    expect(paint).to_not be_valid
    expect(paint.errors.full_messages.first).to eq("Name is required")
  end
  
  it "prevents duplicate paint name" do
      paint = create(:paint)
      paint_2 = build(:paint, binder_id: paint.binder_id, name: paint.name)
      expect(paint_2).to_not be_valid
      expect(paint_2.errors.full_messages.first).to eq("Name has already been taken")
  end
  
  it "gets all paints for a binder" do
    create(:paint, binder_id: @binder.id)
    create(:paint, binder_id: @binder.id)
    create(:paint, binder_id: create(:binder).id)
    page = Binder::Paint.index(@request, {:binderId => @binder.id})
    expect(page.total).to eq(2)
  end
  
  it "gets a paint" do
    paint = create(:paint, binder_id: @binder.id)
    expect(Binder::Paint.show(@request, {:id => paint.id})).to_not be_nil
  end
  
  it "deletes a paint" do
    paint = create(:paint, binder_id: @binder.id)
    
    Binder::Paint.destroy(@request, paint.id)
    expect(Binder::Paint.where(:id => paint.id).count).to eq(0)
  end
  
end