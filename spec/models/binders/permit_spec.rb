require 'rails_helper'

RSpec.describe Binder::Permit, :type => :model do
  
  before :each do
    @user = create(:user)
    @no_access = create(:user)
    @binder = create(:binder)
    create(:subscription, binder_id: @binder.id, plan_id: "free")
    @user.add_role(:owner, @binder)
    @request = HBRequest.new.create_request(@user)
  end
  
  it "requires a permit_number" do
    permit = build(:permit, permit_number: nil)
    expect(permit).to_not be_valid
    expect(permit.errors.full_messages.first).to eq("Permit number is required")
  end
  
  it "requires a binder_id" do
    permit = build(:permit, permit_number: "NASDA-92", binder_id: nil)
    expect(permit).to_not be_valid
    expect(permit.errors.full_messages.first).to eq("Binder is required")
  end
  
  it "requires a description less than 1001 characters" do
    permit = build(:permit, permit_number: "NASDA-92", binder_id: create(:binder).id, details: Faker::Lorem.characters(1001))
    expect(permit).to_not be_valid
    expect(permit.errors.full_messages.first).to eq("Details is too long (maximum is 1000 characters)")
  end
  
  
  it "gets all permits for a binder" do
    create(:permit, binder_id: @binder.id)
    create(:permit, binder_id: @binder.id)
    create(:permit, binder_id: create(:binder).id)
    page = Binder::Permit.index(@request, {:binderId => @binder.id})
    expect(page.total).to eq(2)
  end
  
  it "gets a permit" do
    permit = create(:permit, binder_id: @binder.id)
    expect(Binder::Permit.show(@request, {:id => permit.id})).to_not be_nil
  end
  
  it "creates a permit" do
    permit = {
      :permit => {
        :binder_id => @binder.id,
        :permit_number => "Permit 1"
      }
    }
    permit = ActionController::Parameters.new(permit)
    Binder::Permit.build(@request, permit)
    expect(@binder.permits.count).to eq(1)
  end
  
  it "updates a permit" do
    permit = create(:permit, binder_id: @binder.id)
    update = {
      :id => permit.id,
      :permit => {
        :id => permit.id,
        :binder_id => @binder.id,
        :permit_number => "permit 2"
      }
    }
    update = ActionController::Parameters.new(update)
    permit = Binder::Permit.update(@request, update)
    expect(permit.permit_number).to eq("permit 2")
  end
  
  it "deletes a permit" do
    permit = create(:permit, binder_id: @binder.id)
    
    Binder::Permit.destroy(@request, permit.id)
    expect(Binder::Permit.where(:id => permit.id).count).to eq(0)
  end
  
end