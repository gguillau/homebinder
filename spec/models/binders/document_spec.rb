require 'rails_helper'

RSpec.describe Binder::Document, :type => :model do
  
  before :each do
    @user = create(:user)
    @no_access = create(:user)
    @binder = create(:binder)
    create(:subscription, binder_id: @binder.id, plan_id: "free")
    @user.add_role(:owner, @binder)
    @request = HBRequest.new.create_request(@user)
  end
  
  it "requires a binder_id" do
    document = build(:document, binder_id: nil)
    expect(document).to_not be_valid
    expect(document.errors.full_messages.first).to eq("Binder is required")
  end
  
  it "gets all documents for a binder" do
    create(:document, binder_id: @binder.id)
    create(:document, binder_id: @binder.id)
    create(:document, binder_id: create(:binder).id)
    page = Binder::Document.index(@request, {:binderId => @binder.id})
    expect(page.total).to be > 0
  end
  
  it "gets a document" do
    document = create(:document, binder_id: @binder.id)
    expect(Binder::Document.show(@request, {:id => document.id})).to_not be_nil
  end

  it "deletes a document" do
    document = create(:document, name: "Document 1", binder_id: @binder.id)
    Binder::Document.destroy(@request, document.id)
    expect(Binder::Document.where(:id => document.id).count).to eq(0)
  end
  
end