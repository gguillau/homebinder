require 'rails_helper'

RSpec.describe Binder::Receipt, :type => :model do
  
  before :each do
    @user = create(:user)
    @no_access = create(:user)
    @binder = create(:binder)
    create(:subscription, binder_id: @binder.id, plan_id: "free")
    @user.add_role(:owner, @binder)
    @request = HBRequest.new.create_request(@user)
  end
  
  it "requires a binder_id" do
    receipt = build(:receipt, binder_id: nil)
    expect(receipt).to_not be_valid
    expect(receipt.errors.full_messages.first).to eq("Binder is required")
  end
  
  it "requires a name" do
    receipt = build(:receipt, name: nil)
    expect(receipt).to_not be_valid
    expect(receipt.errors.full_messages.first).to eq("Name is required")
  end
  
  it "gets all receipts for a binder" do
    create(:receipt, binder_id: @binder.id)
    create(:receipt, binder_id: @binder.id)
    create(:receipt, binder_id: create(:binder).id)
    page = Binder::Receipt.index(@request, {:binderId => @binder.id})
    expect(page.total).to eq(2)
  end
  
  it "gets a receipt" do
    receipt = create(:receipt, binder_id: @binder.id)
    expect(Binder::Receipt.show(@request, {:id => receipt.id})).to_not be_nil
  end
  
  it "deletes a receipt" do
    receipt = create(:receipt, binder_id: @binder.id)
    
    Binder::Receipt.destroy(@request, receipt.id)
    expect(Binder::Receipt.where(:id => receipt.id).count).to eq(0)
  end
  
end