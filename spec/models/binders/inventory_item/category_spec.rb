require 'rails_helper'
require 'mandrill'
require 'cancan'

RSpec.describe Binder::InventoryItem::Category do
    
    it "has a valid factory" do
        inventory_item_type = build(:inventory_item_type)
        expect(inventory_item_type).to be_valid
    end
    
    it "requires a name" do
        inventory_item_type = build(:inventory_item_type, name: nil)
        expect(inventory_item_type).to_not be_valid
        expect(inventory_item_type.errors.full_messages.first).to eq("Name is required")
    end

end