require 'rails_helper'
require 'mandrill'
require 'cancan'

RSpec.describe Binder::Paint::Sheen do
    
    it "has a valid factory" do
        paint_sheen = build(:paint_sheen)
        expect(paint_sheen).to be_valid
    end
    
    it "requires a name" do
        paint_sheen = build(:paint_sheen, name: nil)
        expect(paint_sheen).to_not be_valid
        expect(paint_sheen.errors.full_messages.first).to eq("Name is required")
    end
    
    it "requires a created_by" do
        paint_sheen = build(:paint_sheen, created_by: nil)
        expect(paint_sheen).to_not be_valid
        expect(paint_sheen.errors.full_messages.first).to eq("Created by is required")
    end

end