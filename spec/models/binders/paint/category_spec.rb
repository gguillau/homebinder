require 'rails_helper'
require 'mandrill'
require 'cancan'

RSpec.describe Binder::Paint::Category do
    
    it "has a valid factory" do
        paint_type = build(:paint_type)
        expect(paint_type).to be_valid
    end
    
    it "requires a name" do
        paint_type = build(:paint_type, name: nil)
        expect(paint_type).to_not be_valid
        expect(paint_type.errors.full_messages.first).to eq("Name is required")
    end
    
    it "requires a created_by" do
        paint_type = build(:paint_type, created_by: nil)
        expect(paint_type).to_not be_valid
        expect(paint_type.errors.full_messages.first).to eq("Created by is required")
    end

end