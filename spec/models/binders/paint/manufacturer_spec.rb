require 'rails_helper'
require 'mandrill'
require 'cancan'

RSpec.describe Binder::Paint::Manufacturer do
    
    it "has a valid factory" do
        paint_manufacturer = build(:paint_manufacturer)
        expect(paint_manufacturer).to be_valid
    end
    
    it "requires a name" do
        paint_manufacturer = build(:paint_manufacturer, name: nil)
        expect(paint_manufacturer).to_not be_valid
        expect(paint_manufacturer.errors.full_messages.first).to eq("Name is required")
    end
    
    it "requires a created_by" do
        paint_manufacturer = build(:paint_manufacturer, created_by: nil)
        expect(paint_manufacturer).to_not be_valid
        expect(paint_manufacturer.errors.full_messages.first).to eq("Created by is required")
    end

end