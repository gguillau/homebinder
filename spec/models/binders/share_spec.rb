require 'rails_helper'

RSpec.describe Binder::Share, :type => :model do
    before :each do
        @user = create(:user)
        @with = create(:user)
        @no_access = create(:user)
        @binder = create(:binder)
        create(:subscription, binder_id: @binder.id, plan_id: "free")
        @user.add_role(:owner, @binder)
        @request = HBRequest.new.create_request(@user)
        @no_access = create(:user)
        @no_request = HBRequest.new.create_request(@no_access)
    end

    it "requires a binder_id" do
        share = build(:share, binder_id: nil)
        expect(share).to_not be_valid
        expect(share.errors.full_messages.first).to eq("Binder is required")
    end

    it "requires a role_name" do
        share = build(:share, binder_id: create(:binder).id, role_name: nil)
        expect(share).to_not be_valid
        expect(share.errors.full_messages.first).to eq("Role name is required")
    end

    it "requires a status" do
        share = build(:share, binder_id: create(:binder).id, role_name: "co_owner", status: nil)
        expect(share).to_not be_valid
        expect(share.errors.full_messages.first).to eq("Status is required")
    end

    it "requires a shared_by_id" do
        share = build(:share, binder_id: create(:binder).id, role_name: "co_owner", status: "created", shared_by_id: nil)
        expect(share).to_not be_valid
        expect(share.errors.full_messages.first).to eq("Shared by is required")
    end

    it "requires a shared_with_id" do
        share = build(:share, binder_id: create(:binder).id, role_name: "co_owner", status: "created", shared_by_id: 1, shared_with_id: nil)
        expect(share).to_not be_valid
        expect(share.errors.full_messages.first).to eq("Shared with is required")
    end

    it "gets a share" do
        share = create(:share, status: "sent", shared_by_id: @user.id, shared_with_id: @with.id, role_name: "co_owner", binder_id: @binder.id)
        expect(Binder::Share.show(@request, {:id => share.id})).to_not be_nil
    end

    it "prevents share from sender to same sender as receiver" do
        share = build(:share, binder_id: @binder.id, shared_by_id: @user.id, shared_with_id: @user.id, status: "shared", role_name: "co_owner")
        expect(share).to_not be_valid
        expect(share.errors.full_messages.first).to eq("Shared by Error - Share Sender and Receiver cannot be same users")
    end

    describe "#build" do
        it "raises an exception" do
            expect{Binder::Share.build(@request, {})}.to raise_error BadRequestException, "Client information required"
        end
        it "raises an exception" do
            expect{Binder::Share.build(@request, {:share => {}})}.to raise_error BadRequestException, "Client information required"
        end
        it "raises an exception" do
            expect{Binder::Share.build(@request, {:share => {:user => {}}})}.to raise_error BadRequestException, "with is required"
        end
        it "raises an exception" do
            expect{Binder::Share.build(@request, {:share => {:user => {:email => ""}}})}.to raise_error BadRequestException, "role is required"
        end
        it "raises an exception" do
            expect{Binder::Share.build(@request, {:share => {:user => {:email => ""}, :role_name => ""}})}.to raise_error BadRequestException, "Binder ID Required"
        end
        it "raises an exception" do
            expect{Binder::Share.build(@request, {:share => {:user => {:email => ""}, :role_name => "", :binder_id => 999999}})}.to raise_error ActiveRecord::RecordNotFound
        end
        it "raises an exception" do
            expect{Binder::Share.build(@no_request, {:share => {:user => {:email => ""}, :role_name => "", :binder_id => @binder.id}})}.to raise_error CanCan::AccessDenied
        end
        it "raises an exception" do
            expect{Binder::Share.build(@request, {:share => {:user => {:email => ""}, :role_name => "fake", :binder_id => @binder.id}})}.to raise_error BadRequestException, "role must be 'co_owner', 'reader', or 'writer'"
        end
        it "raises an exception" do
            create(:share, :binder => @binder, :sender => @user, :receiver => @with)
            expect{Binder::Share.build(@request, {:share => {:user => {:email => @with.email, :phone => ""}, :role_name => "co_owner", :binder_id => @binder.id}})}.to raise_error UnprocessableException
        end
        it "shares the binder" do
            allow(Users::UserAnalytics).to receive(:share_received)
            Binder::Share.build(@request, {:share => {:user => {:email => @with.email, :phone => ""}, :role_name => "co_owner", :binder_id => @binder.id}})
            # find the binder co_owner
            co_owner = User.with_role(:co_owner, @binder).first
            # make sure the co_owner is the with user
            expect(co_owner.id).to eq(@with.id)
            expect(Users::UserAnalytics).to have_received(:share_received)
        end
    end

    describe "#update" do
        it "raises an exception" do
            expect{Binder::Share.update(@request, {})}.to raise_error BadRequestException, "Share ID required"
        end
        it "raises an exception" do
            expect{Binder::Share.update(@request, {:id => -1})}.to raise_error BadRequestException, "Share required"
        end
        it "raises an exception" do
            expect{Binder::Share.update(@request, {:id => -1, :share => {}})}.to raise_error ActiveRecord::RecordNotFound
        end
        it "raises an exception" do
            share = create(:share, status: "sent", shared_by_id: @user.id, shared_with_id: @with.id, role_name: "co_owner", binder_id: @binder.id)
            update = {
                :id => share.id,
                :share => {
                    :id => share.id,
                    :binder_id => @binder.id,
                    :status => "declined"
                }
            }
            update = ActionController::Parameters.new(update)
            share = Binder::Share.update(@request, update)
            expect{Binder::Share.update(@no_request, update)}.to raise_error CanCan::AccessDenied
        end
        it "updates a share" do
            share = create(:share, status: "sent", shared_by_id: @user.id, shared_with_id: @with.id, role_name: "co_owner", binder_id: @binder.id)
            update = {
                :id => share.id,
                :share => {
                    :id => share.id,
                    :binder_id => @binder.id,
                    :status => "declined"
                }
            }
            update = ActionController::Parameters.new(update)
            share = Binder::Share.update(@request, update)
            expect(share.status).to eq("declined")
        end
    end

    describe "#destroy" do
        it "raises an exception" do
            expect{Binder::Share.destroy(@request, nil)}.to raise_error BadRequestException, "Share ID required"
        end
        it "raises an exception" do
            no_access = create(:user)
            share = create(:share, status: "sent", shared_by_id: @user.id, shared_with_id: @with.id, role_name: "co_owner", binder_id: @binder.id)
            expect{Binder::Share.destroy(@no_request, share.id)}.to raise_error CanCan::AccessDenied
        end
        it "deletes a share" do
            share = create(:share, status: "sent", shared_by_id: @user.id, shared_with_id: @with.id, role_name: "co_owner", binder_id: @binder.id)
            Binder::Share.destroy(@request, share.id)
            expect(Binder::Share.where(:id => share.id).count).to eq(0)
        end
    end
end
