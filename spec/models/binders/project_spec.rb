require 'rails_helper'

RSpec.describe Binder::Project, :type => :model do
  
  before :each do
    @user = create(:user)
    @no_access = create(:user)
    @binder = create(:binder)
    create(:subscription, binder_id: @binder.id, plan_id: "free")
    @user.add_role(:owner, @binder)
    @request = HBRequest.new.create_request(@user)
  end
  
  it "requires a binder_id" do
    project = build(:project, binder_id: nil)
    expect(project).to_not be_valid
    expect(project.errors.full_messages.first).to eq("Binder is required")
  end
  
  it "gets all projects for a binder" do
    create(:project, binder_id: @binder.id)
    create(:project, binder_id: @binder.id)
    create(:project, binder_id: create(:binder).id)
    page = Binder::Project.index(@request, {:binderId => @binder.id})
    expect(page.total).to eq(2)
  end
  
  it "gets a project" do
    project = create(:project, binder_id: @binder.id)
    expect(Binder::Project.show(@request, {:id => project.id})).to_not be_nil
  end
  
  it "deletes a project" do
    project = create(:project, binder_id: @binder.id)
    
    Binder::Project.destroy(@request, project.id)
    expect(Binder::Project.where(:id => project.id).count).to eq(0)
  end
  
end