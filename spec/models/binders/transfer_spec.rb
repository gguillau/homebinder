require 'rails_helper'
require 'mandrill'
require 'cancan'

RSpec.describe Binder::Transfer, :type => :model do
    before :each do
        @user = create(:user)
        @with = create(:user)
        @no_access = create(:user)
        @binder = create(:binder)
        create(:subscription, binder_id: @binder.id, plan_id: "free")
        @user.add_role(:owner, @binder)
    end

    it "requires a binder_id" do
        transfer = build(:transfer, binder_id: nil)
        expect(transfer).to_not be_valid
        expect(transfer.errors.full_messages.first).to eq("Binder is required")
    end

    it "requires a receiver_id" do
        transfer = build(:transfer, binder_id: create(:binder).id, receiver_id: nil)
        expect(transfer).to_not be_valid
        expect(transfer.errors.full_messages.first).to eq("Receiver is required")
    end

    it "requires a sender_id" do
        transfer = build(:transfer, binder_id: create(:binder).id, receiver_id: 2, sender_id: nil)
        expect(transfer).to_not be_valid
        expect(transfer.errors.full_messages.first).to eq("Sender is required")
    end

    it "requires a status" do
        transfer = build(:transfer, binder_id: create(:binder).id, receiver_id: 1, sender_id: 2, status: nil)
        expect(transfer).to_not be_valid
        expect(transfer.errors.full_messages.first).to eq("Status is required")
    end

    it "requires a transfer_type" do
        transfer = build(:transfer, binder_id: create(:binder).id, receiver_id: 1, sender_id: 2, status: "created", transfer_type: nil)
        expect(transfer).to_not be_valid
        expect(transfer.errors.full_messages.first).to eq("Transfer type is required")
    end

    it "prevents duplicate pending transfer" do
        transfer = create(:transfer)
        transfer = build(:transfer, binder_id: transfer.binder_id, receiver_id: transfer.receiver_id, sender_id: transfer.sender_id, status: "created", transfer_type: "ownership")
        expect(transfer).to_not be_valid
        expect(transfer.errors.full_messages.first).to eq("Receiver has already been taken")
    end

    it "prevents transfer from sender to same sender as receiver" do
        transfer = build(:transfer, binder_id: @binder.id, receiver_id: @user.id, sender_id: @user.id, status: "created", transfer_type: "ownership")
        expect(transfer).to_not be_valid
        expect(transfer.errors.full_messages.first).to eq("Sender Error - Transfer Sender and Receiver cannot be same users")
    end

    describe "#build" do
        it "changes status of transfer to created" do
            transfer = {:binder_id => @binder.id, :status => "not_sent", :receiver_id => @no_access.id, :sender_id => @user.id, :transfer_type => "ownership"}
            transfer = ActionController::Parameters.new(transfer)
            transfer = Binder::Transfer.build(HBRequest.new.create_request(@user), {:transfer => transfer})

            expect(transfer.status).to eq("created")
        end
    end

    describe "#execute" do
        it "raises an exception" do
            expect{Binder::Transfer.execute(@user, {})}.to raise_error BadRequestException, "Transfer ID Required"
        end
        it "raises an exception" do
            expect{Binder::Transfer.execute(@user, {:id => 9999})}.to raise_error BadRequestException, "to is required"
        end
        it "raises an exception" do
            expect{Binder::Transfer.execute(@user, {:id => 9999, :user => {}})}.to raise_error BadRequestException, "transfer_type is required"
        end
        it "raises an exception" do
            expect{Binder::Transfer.execute(@user, {:id => 9999, :user => {}, :transfer_type => "fake"})}.to raise_error BadRequestException, "transfer_type must be 'copy' or 'ownership'"
        end
        it "raises an exception" do
            expect{Binder::Transfer.execute(@user, {:id => 9999, :user => {}, :transfer_type => "ownership"})}.to raise_error ActiveRecord::RecordNotFound
        end
        it "raises an exception" do
            no_access = create(:user)
            transfer = create(:transfer, status: "delivery_failed", sender_id: @user.id, receiver_id: @with.id, transfer_type: "ownership", binder_id: @binder.id)
            expect{Binder::Transfer.execute(no_access, {:id => transfer.id, :user => {}, :transfer_type => "ownership"})}.to raise_error CanCan::AccessDenied
        end
        it "transfers the binder" do
            allow(Users::UserAnalytics).to receive(:transfer_received)
            transfer = create(:transfer, status: "delivery_failed", sender_id: @user.id, receiver_id: @with.id, transfer_type: "ownership", binder_id: @binder.id)
            user = {
                first: @with.user_profile.first_name,
                last: @with.user_profile.last_name,
                email: @with.email,
                phone: @with.user_profile.mobile_phone,
            }
            Binder::Transfer.execute(@user, {:id => transfer.id, :user => user, :transfer_type => "ownership"})
            owner = @binder.owner
            # make sure the transfer went through
            expect(owner.id).to eq(@with.id)
            expect(Users::UserAnalytics).to have_received(:transfer_received)
        end
    end

    describe "#show" do
        it "gets a transfer" do
            transfer = create(:transfer, status: "sent", sender_id: @user.id, receiver_id: @with.id, transfer_type: "ownership", binder_id: @binder.id)
            @request = HBRequest.new.create_request(@user)
            expect(Binder::Transfer.show(@request, {:id => transfer.id})).to_not be_nil
        end
    end

    describe "#resend_email" do
        it "raises an exception" do
            expect{Binder::Transfer.resend(@user, {})}.to raise_error BadRequestException, "Transfer ID Required"
        end
        it "raises an exception" do
            expect{Binder::Transfer.resend(@user, {:id => 99999999})}.to raise_error BadRequestException, "to is required"
        end
        it "raises an exception" do
            expect{Binder::Transfer.resend(@user, {:id => 99999999, user: {first_name: "test", last_name: "name", email: "test@gmail.com", phone: "16175002600"}})}.to raise_error ActiveRecord::RecordNotFound
        end
        it "resends a transfer" do
            transfer = create(:transfer, status: "delivery_failed", sender_id: @user.id, receiver_id: @with.id, transfer_type: "ownership", binder_id: @binder.id)
            expect_any_instance_of(Binder::Transfer).to receive(:resend)
            Binder::Transfer.resend(@user, {:id => transfer.id, user: {first_name: "test", last_name: "name", email: "test@gmail.com", phone: "16175002600"}})
        end
    end

    describe "#update" do
        it "updates a transfer" do
            transfer = create(:transfer, status: "sent", sender_id: @user.id, receiver_id: @with.id, transfer_type: "ownership", binder_id: @binder.id)
            update = {
                :id => transfer.id,
                :transfer => {
                    :id => transfer.id,
                    :binder_id => @binder.id,
                    :status => "declined"
                }
            }
            update = ActionController::Parameters.new(update)
            hb_request = HBRequest.new.create_request(@user)
            transfer = Binder::Transfer.update(hb_request, update)
            expect(transfer.status).to eq("declined")
        end
    end

    describe "#destroy" do
        it "deletes a transfer" do
            transfer = create(:transfer, status: "sent", sender_id: @user.id, receiver_id: @with.id, transfer_type: "ownership", binder_id: @binder.id)
            hb_request = HBRequest.new.create_request(@user)
            Binder::Transfer.destroy(hb_request, transfer.id)
            expect(Binder::Transfer.where(:id => transfer.id).count).to eq(0)
        end
    end
end
