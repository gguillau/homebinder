require 'rails_helper'

RSpec.describe Binder::Image, :type => :model do
  
  before :each do
    @user = create(:user)
    @no_access = create(:user)
    @binder = create(:binder)
    create(:subscription, binder_id: @binder.id, plan_id: "free")
    @user.add_role(:owner, @binder)
    @request = HBRequest.new.create_request(@user)
    @no_access = create(:user)
    @no_request = HBRequest.new.create_request(@no_access)
  end
  
  it "requires a binder_id" do
    image = build(:image, binder_id: nil)
    expect(image).to_not be_valid
    expect(image.errors.full_messages.first).to eq("Binder is required")
  end
  
  it "gets all images for a binder" do
    create(:image, binder_id: @binder.id)
    create(:image, binder_id: @binder.id)
    create(:image, binder_id: create(:binder).id)
    page = Binder::Image.index(@request, {:binderId => @binder.id})
    expect(page.total).to eq(2)
  end
  
  it "gets an image" do
    image = create(:image, binder_id: @binder.id)
    expect(Binder::Image.show(@request, {:id => image.id})).to_not be_nil
  end
  
  it "deletes an image" do
    image = create(:image, binder_id: @binder.id)
    
    Binder::Image.destroy(@request, image.id)
    expect(Binder::Finish.where(:id => image.id).count).to eq(0)
  end
  
  it "saves the file" do
    image = create(:image, binder_id: @binder.id, file: nil)
    expect(Binder::Image.show(@request, {:id => image.id})).to_not be_nil
    
    image.file_url("https://dummyimage.com/300/09f/fff.png")
    image.save
    
    expect(image.file).to_not be(nil)
    expect(image.file_file_name).to eq("fff.png")
  end
  
  it "sets the image as the hero photo" do
    binder = create(:binder)
    create(:subscription, :binder => binder)
    admin = create(:user, :role => "admin")
    hb_request = HBRequest.new.create_request(admin)
    image = {
      :binder_id => binder.id,
      :image => {
        :binder_id => binder.id
      }
    }
    
    expect(binder.images.count).to eq(0)
    
    image = ActionController::Parameters.new(image)
    image = Binder::Image.build(hb_request, image)
    binder.reload
    
    expect(binder.images.count).to eq(1)
    expect(binder.hero_image_id).to eq(image.id)
  end
  
end