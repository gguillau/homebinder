require 'rails_helper'

RSpec.describe Binder::Structure, :type => :model do
  
  before :each do
    @user = create(:user)
    @no_access = create(:user)
    @binder = create(:binder)
    create(:subscription, binder_id: @binder.id, plan_id: "free")
    @user.add_role(:owner, @binder)
    @request = HBRequest.new.create_request(@user)
    @no_access = create(:user)
    @no_request = HBRequest.new.create_request(@no_access)
  end
  
  it "requires a binder_id" do
    structure = build(:structure, binder_id: nil)
    expect(structure).to_not be_valid
    expect(structure.errors.full_messages.first).to eq("Binder is required")
  end
  
  it "requires a name" do
    structure = build(:structure, name: nil)
    expect(structure).to_not be_valid
    expect(structure.errors.full_messages.first).to eq("Name is required")
  end
  
  it "prevents duplicate structure name" do
      structure = create(:structure)
      structure_2 = build(:structure, binder_id: structure.binder_id, name: structure.name)
      expect(structure_2).to_not be_valid
      expect(structure_2.errors.full_messages.first).to eq("Name has already been taken")
  end
  
  it "gets all structures for a binder" do
    create(:structure, binder_id: @binder.id)
    create(:structure, binder_id: @binder.id)
    create(:structure, binder_id: create(:binder).id)
    page = Binder::Structure.index(@request, {:binderId => @binder.id})
    expect(page.total).to eq(2)
  end
  
  it "gets a structure" do
    structure = create(:structure, binder_id: @binder.id)
    expect(Binder::Structure.show(@request, {:id => structure.id})).to_not be_nil
  end
  
  it "deletes a structure" do
    structure = create(:structure, binder_id: @binder.id)
    
    Binder::Structure.destroy(@request, structure.id)
    expect(Binder::Structure.where(:id => structure.id).count).to eq(0)
  end
  
end