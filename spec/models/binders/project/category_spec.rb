require 'rails_helper'
require 'mandrill'
require 'cancan'

RSpec.describe Binder::Project::Category do
    
    it "has a valid factory" do
        project_type = build(:project_type)
        expect(project_type).to be_valid
    end
    
    it "requires a name" do
        project_type = build(:project_type, name: nil)
        expect(project_type).to_not be_valid
        expect(project_type.errors.full_messages.first).to eq("Name is required")
    end

end