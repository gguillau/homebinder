require 'rails_helper'

RSpec.describe Binder::Appliance, :type => :model do

  before :each do
    @user = create(:user)
    @no_access = create(:user)
    @binder = create(:binder)
    create(:subscription, binder_id: @binder.id, plan_id: "free")
    @user.add_role(:owner, @binder)
    @request = HBRequest.new.create_request(@user)
    @no_access = create(:user)
    @no_request = HBRequest.new.create_request(@no_access)
  end

  it "requires a binder_id" do
    appliance = build(:appliance, binder_id: nil)
    expect(appliance).to_not be_valid
    expect(appliance.errors.full_messages.first).to eq("Binder is required")
  end

  it "requires a name" do
    appliance = build(:appliance, name: nil)
    expect(appliance).to_not be_valid
    expect(appliance.errors.full_messages.first).to eq("Name is required")
  end

  it "prevents duplicate appliance name" do
      appliance = create(:appliance)
      appliance_2 = build(:appliance, binder_id: appliance.binder_id, name: appliance.name)
      expect(appliance_2).to_not be_valid
      expect(appliance_2.errors.full_messages.first).to eq("Name has already been taken")
  end

  it "gets all appliances for a binder" do
    create(:appliance, binder_id: @binder.id)
    create(:appliance, binder_id: @binder.id)
    create(:appliance, binder_id: create(:binder).id)
    page = Binder::Appliance.index(@request, {:binderId => @binder.id})
    expect(page.total).to eq(2)
  end

  it "gets an appliance" do
    appliance = create(:appliance, binder_id: @binder.id)
    expect(Binder::Appliance.show(@request, {:id => appliance.id})).to_not be_nil
  end

  it "creates an appliance but not a binder item" do
    app = {
      :appliance => {
        :binder_id => @binder.id,
        :name => "Appliance 1"
      }
    }
    app = ActionController::Parameters.new(app)
    appliance = Binder::Appliance.build(@request,app)
    expect(@binder.appliances.count).to eq(1)
    expect(appliance.verified).to eq(true)
    expect(appliance.binder_item).to be(nil)
  end

  it "creates an appliance and binder item" do
    app = {
      :appliance => {
        :binder_id => @binder.id,
        :name => "Appliance 1",
        :verified => false
      }
    }
    app = ActionController::Parameters.new(app)
    appliance = Binder::Appliance.build(@request,app)
    binder_item = appliance.binder_item

    expect(@binder.appliances.count).to eq(1)
    expect(appliance.verified).to eq(false)
    expect(binder_item.verified).to eq(false)
    expect(binder_item.binder).to eq(@binder)
    expect(binder_item.user).to eq(@request.user)
  end

  it "updates an appliance" do
    appliance = create(:appliance, name: "Appliance1", binder_id: @binder.id)
    update = {
      :id => appliance.id,
      :appliance => {
        :id => appliance.id,
        :binder_id => @binder.id,
        :name => "Appliance 2"
      }
    }
    update = ActionController::Parameters.new(update)
    appliance = Binder::Appliance.update(@request,update)
    expect(appliance.name).to eq("Appliance 2")
  end

  it "deletes an appliance" do
    appliance = create(:appliance, name: "Appliance1", binder_id: @binder.id)

    Binder::Appliance.destroy(@request, appliance.id)
    expect(Binder::Appliance.where(:id => appliance.id).count).to eq(0)
  end

end