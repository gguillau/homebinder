require 'rails_helper'
require 'mandrill'
require 'cancan'

RSpec.describe Binder::Finish::Make do
    
    it "has a valid factory" do
        finish_make = build(:finish_make)
        expect(finish_make).to be_valid
    end
    
    it "requires a name" do
        finish_make = build(:finish_make, name: nil)
        expect(finish_make).to_not be_valid
        expect(finish_make.errors.full_messages.first).to eq("Name is required")
    end

end