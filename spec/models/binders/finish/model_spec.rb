require 'rails_helper'
require 'mandrill'
require 'cancan'

RSpec.describe Binder::Finish::Model do
    
    it "has a valid factory" do
        finish_model = build(:finish_model)
        expect(finish_model).to be_valid
    end
    
    it "requires a name" do
        finish_model = build(:finish_model, name: nil)
        expect(finish_model).to_not be_valid
        expect(finish_model.errors.full_messages.first).to eq("Name is required")
    end

end