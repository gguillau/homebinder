require 'rails_helper'

RSpec.describe Binder::InventoryItem, :type => :model do
  
  before :each do
    @user = create(:user)
    @no_access = create(:user)
    @binder = create(:binder)
    create(:subscription, binder_id: @binder.id, plan_id: "free")
    @user.add_role(:owner, @binder)
    @request = HBRequest.new.create_request(@user)
    @no_access = create(:user)
    @no_request = HBRequest.new.create_request(@no_access)
  end
  
  it "requires a binder_id" do
    inventory_item = build(:inventory_item, binder_id: nil)
    expect(inventory_item).to_not be_valid
    expect(inventory_item.errors.full_messages.first).to eq("Binder is required")
  end
  
  it "requires a name" do
    inventory_item = build(:inventory_item, name: nil)
    expect(inventory_item).to_not be_valid
    expect(inventory_item.errors.full_messages.first).to eq("Name is required")
  end
  
  it "prevents duplicate inventory_item name" do
      inventory_item = create(:inventory_item)
      inventory_item_2 = build(:inventory_item, binder_id: inventory_item.binder_id, name: inventory_item.name)
      expect(inventory_item_2).to_not be_valid
      expect(inventory_item_2.errors.full_messages.first).to eq("Name has already been taken")
  end
  
  it "gets all inventory_items for a binder" do
    create(:inventory_item, binder_id: @binder.id)
    create(:inventory_item, binder_id: @binder.id)
    create(:inventory_item, binder_id: create(:binder).id)
    page = Binder::InventoryItem.index(@request, {:binderId => @binder.id})
    expect(page.total).to be > 0
  end
  
  it "gets an inventory_item" do
    inventory_item = create(:inventory_item, binder_id: @binder.id)
    expect(Binder::InventoryItem.show(@request, {:id => inventory_item.id})).to_not be_nil
  end
  
  it "deletes an inventory_item" do
    inventory_item = create(:inventory_item, binder_id: @binder.id)
    Binder::InventoryItem.destroy(@request, inventory_item.id)
    expect(Binder::InventoryItem.where(:id => inventory_item.id).count).to eq(0)
  end
  
end