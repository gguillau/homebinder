require 'rails_helper'

RSpec.describe Binder::Finish, :type => :model do
  
  before :each do
    @user = create(:user)
    @no_access = create(:user)
    @binder = create(:binder)
    create(:subscription, binder_id: @binder.id, plan_id: "free")
    @user.add_role(:owner, @binder)
    @request = HBRequest.new.create_request(@user)
    @no_access = create(:user)
    @no_request = HBRequest.new.create_request(@no_access)
  end
  
  it "requires a binder_id" do
    finish = build(:finish, binder_id: nil)
    expect(finish).to_not be_valid
    expect(finish.errors.full_messages.first).to eq("Binder is required")
  end
  
  it "requires a name" do
    finish = build(:finish, name: nil)
    expect(finish).to_not be_valid
    expect(finish.errors.full_messages.first).to eq("Name is required")
  end
  
  it "prevents duplicate finish name" do
      finish = create(:finish)
      finish_2 = build(:finish, binder_id: finish.binder_id, name: finish.name)
      expect(finish_2).to_not be_valid
      expect(finish_2.errors.full_messages.first).to eq("Name has already been taken")
  end
  
  it "gets all finishes for a binder" do
    @binder.finishes.destroy_all
    create(:finish, binder_id: @binder.id)
    create(:finish, binder_id: @binder.id)
    create(:finish, binder_id: create(:binder).id)
    page = Binder::Finish.index(@request, {:binderId => @binder.id})
    expect(page.total).to eq(2)
  end
  
  it "gets a finish" do
    finish = create(:finish, binder_id: @binder.id)
    expect(Binder::Finish.show(@request, {:id => finish.id})).to_not be_nil
  end
  
  it "updates a finish" do
    finish = create(:finish, name: "Finish 1", binder_id: @binder.id)
    item = {
        :id => finish.id,
        :finish => {
          :id => finish.id,
          :binder_id => @binder.id,
          :name => "Finish 2"
        }
    }
    update = ActionController::Parameters.new(item)
    finish = Binder::Finish.update(
      @request, update)
    expect(finish.name).to eq("Finish 2")
  end
  
  it "deletes a finish" do
    finish = create(:finish, name: "Finish 1", binder_id: @binder.id)
    
    Binder::Finish.destroy(@request, finish.id)
    expect(Binder::Finish.where(:id => finish.id).count).to eq(0)
  end
  
end