require 'rails_helper'

RSpec.describe Binder::BinderBranding, :type => :model do
    
    let(:user) { FactoryBot.create(:user, :role => "inspector") }
    let(:member) { FactoryBot.create(:user, :role => "inspector") }
    let(:partner){FactoryBot.create(:partner)}
    let(:binder) {FactoryBot.create(:binder)}
    
    before :each do
        user.add_role :partner_admin, partner
        member.add_role :member, partner
    end
    
    it "requires a scope" do
        binder_branding = build(:binder_branding, scope: nil, :binder_id => 1)
        expect(binder_branding).to_not be_valid
        expect(binder_branding.errors.full_messages.first).to eq("Scope is required")
    end
    
    it "requires a binder_id" do
        binder_branding = build(:binder_branding, :scope => "binder", :binder_id => nil, :user_branding_id => user.user_profile.id)
        expect(binder_branding).to_not be_valid
        expect(binder_branding.errors.full_messages.first).to eq("Binder is required")
    end

    it "requires a user_branding_id" do
        binder_branding = build(:binder_branding, :user_branding_id => nil, :binder_id => 1)
        expect(binder_branding).to_not be_valid
        expect(binder_branding.errors.full_messages.first).to eq("User profile is required")
    end
    
    it "prevents duplicate binder scope on create" do
        binder = create(:binder)
        binder_branding = create(:binder_branding, :scope => "binder", :user_branding_id => user.user_profile.id, :binder_id => binder.id)
        expect(binder_branding).to be_valid
        binder_branding_2 = build(:binder_branding, :scope => "binder", :user_branding_id => user.user_profile.id, :binder_id => binder.id)
        binder_branding_2.save
        expect(binder_branding_2).to_not be_valid
        expect(binder_branding_2.errors.full_messages.first).to eq("Scope binder already exists")
    end
    
    it "prevents duplicate binder scope on update" do
        binder = create(:binder)
        binder_branding = create(:binder_branding, :scope => "binder", :user_branding_id => user.user_profile.id, :binder_id => binder.id)
        expect(binder_branding).to be_valid
        binder_branding_2 = create(:binder_branding, :scope => "transfer_email", :user_branding_id => user.user_profile.id, :binder_id => binder.id)
        expect(binder_branding_2).to be_valid
        binder_branding_2.scope = "binder"
        binder_branding_2.save
        expect(binder_branding_2).to_not be_valid
        expect(binder_branding_2.errors.full_messages.first).to eq("Scope binder already exists")
    end
    
    it "prevents duplicate transfer_email scope on create" do
        binder = create(:binder)
        binder_branding = create(:binder_branding, :scope => "transfer_email", :user_branding_id => user.user_profile.id, :binder_id => binder.id)
        expect(binder_branding).to be_valid
        binder_branding_2 = build(:binder_branding, :scope => "transfer_email", :user_branding_id => user.user_profile.id, :binder_id => binder.id)
        expect(binder_branding_2).to_not be_valid
        expect(binder_branding_2.errors.full_messages.first).to eq("Scope transfer_email already exists")
    end
    
    it "prevents duplicate transfer_email scope on update" do
        binder = create(:binder)
        binder_branding = create(:binder_branding, :scope => "transfer_email", :user_branding_id => user.user_profile.id, :binder_id => binder.id)
        expect(binder_branding).to be_valid
        binder_branding_2 = create(:binder_branding, :scope => "binder", :user_branding_id => user.user_profile.id, :binder_id => binder.id)
        expect(binder_branding_2).to be_valid
        binder_branding_2.scope = "transfer_email"
        binder_branding_2.save
        expect(binder_branding_2).to_not be_valid
        expect(binder_branding_2.errors.full_messages.first).to eq("Scope transfer_email already exists")
    end
    
    it "prevents duplicate share_email scope on create" do
        binder = create(:binder)
        binder_branding = create(:binder_branding, :scope => "share_email", :user_branding_id => user.user_profile.id, :binder_id => binder.id)
        expect(binder_branding).to be_valid
        binder_branding_2 = build(:binder_branding, :scope => "share_email", :user_branding_id => user.user_profile.id, :binder_id => binder.id)
        expect(binder_branding_2).to_not be_valid
        expect(binder_branding_2.errors.full_messages.first).to eq("Scope share_email already exists")
    end
    
    it "prevents duplicate share_email scope on update" do
        binder = create(:binder)
        binder_branding = create(:binder_branding, :scope => "share_email", :user_branding_id => user.user_profile.id, :binder_id => binder.id)
        expect(binder_branding).to be_valid
        binder_branding_2 = create(:binder_branding, :scope => "binder", :user_branding_id => user.user_profile.id, :binder_id => binder.id)
        expect(binder_branding_2).to be_valid
        binder_branding_2.scope = "share_email"
        binder_branding_2.save
        expect(binder_branding_2).to_not be_valid
        expect(binder_branding_2.errors.full_messages.first).to eq("Scope share_email already exists")
    end
    
    it "prevents duplicate seller_report scope on create" do
        binder = create(:binder)
        binder_branding = create(:binder_branding, :scope => "seller_report", :user_branding_id => user.user_profile.id, :binder_id => binder.id)
        expect(binder_branding).to be_valid
        binder_branding_2 = build(:binder_branding, :scope => "seller_report", :user_branding_id => user.user_profile.id, :binder_id => binder.id)
        expect(binder_branding_2).to_not be_valid
        expect(binder_branding_2.errors.full_messages.first).to eq("Scope seller_report already exists")
    end
    
    it "prevents duplicate seller_report scope on update" do
        binder = create(:binder)
        binder_branding = create(:binder_branding, :scope => "seller_report", :user_branding_id => user.user_profile.id, :binder_id => binder.id)
        expect(binder_branding).to be_valid
        binder_branding_2 = create(:binder_branding, :scope => "binder", :user_branding_id => user.user_profile.id, :binder_id => binder.id)
        expect(binder_branding_2).to be_valid
        binder_branding_2.scope = "seller_report"
        binder_branding_2.save
        expect(binder_branding_2).to_not be_valid
        expect(binder_branding_2.errors.full_messages.first).to eq("Scope seller_report already exists")
    end
    
    it "prevents triple recall_email scope" do
        binder = create(:binder)
        binder_branding = create(:binder_branding, :scope => "recall_email", :user_branding_id => user.user_profile.id, :binder_id => binder.id)
        expect(binder_branding).to be_valid 
        binder_branding_2 = create(:binder_branding, :scope => "recall_email", :user_branding_id => create(:user_profile).id, :binder_id => binder.id)
        expect(binder_branding_2).to be_valid
        binder_branding_3 = build(:binder_branding, :scope => "recall_email", :user_branding_id => create(:user_profile).id, :binder_id => binder.id)
        binder_branding_3.save
        expect(binder_branding_3).to_not be_valid
        expect(binder_branding_3.errors.full_messages.first).to eq("Scope recall_email maximum count reached")
    end
    
    it "prevents triple maintenance_email scope on create" do
        binder = create(:binder)
        binder_branding = create(:binder_branding, :scope => "maintenance_email", :user_branding_id => user.user_profile.id, :binder_id => binder.id)
        expect(binder_branding).to be_valid 
        binder_branding_2 = create(:binder_branding, :scope => "maintenance_email", :user_branding_id => create(:user_profile).id, :binder_id => binder.id)
        expect(binder_branding_2).to be_valid
        binder_branding_3 = build(:binder_branding, :scope => "maintenance_email", :user_branding_id => create(:user_profile).id, :binder_id => binder.id)
        binder_branding_3.save
        expect(binder_branding_3).to_not be_valid
        expect(binder_branding_3.errors.full_messages.first).to eq("Scope maintenance_email maximum count reached")
    end
    
    it "prevents triple maintenance_email scope on update" do
        binder = create(:binder)
        binder_branding = create(:binder_branding, :scope => "maintenance_email", :user_branding_id => user.user_profile.id, :binder_id => binder.id)
        expect(binder_branding).to be_valid 
        binder_branding_2 = create(:binder_branding, :scope => "maintenance_email", :user_branding_id => create(:user_profile).id, :binder_id => binder.id)
        expect(binder_branding_2).to be_valid
        binder_branding_3 = create(:binder_branding, :scope => "transfer_email", :user_branding_id => create(:user_profile).id, :binder_id => binder.id)
        expect(binder_branding_3).to be_valid
        binder_branding_3.scope = "maintenance_email"
        binder_branding_3.save
        expect(binder_branding_3.errors.full_messages.first).to eq("Scope maintenance_email maximum count reached")
    end
    
    it "prevents homeowner from being added on create" do
        binder = create(:binder)
        user = create(:user)
        binder_branding = build(:binder_branding, :scope => "maintenance_email", :user_branding_id => user.user_profile.id, :binder_id => binder.id)
        expect(binder_branding).to_not be_valid 
        expect(binder_branding.errors.full_messages.first).to eq("User branding ID belongs to a homeowner and homeowners cannot be added to a binder's branding")
    end
    
    it "prevents homeowner from being added on update" do
        binder = create(:binder)
        inspector = create(:user, :role => "inspector")
        homeowner = create(:user, :role => "homeowner")
        binder_branding = create(:binder_branding, :scope => "maintenance_email", :user_branding_id => inspector.user_profile.id, :binder_id => binder.id)
        expect(binder_branding).to be_valid 
        binder_branding.user_branding_id = homeowner.user_profile.id
        binder_branding.save
        expect(binder_branding.errors.full_messages.first).to eq("User branding ID belongs to a homeowner and homeowners cannot be added to a binder's branding")
    end
    
    it "prevents 9 binder brandings" do
        binder = create(:binder)
        count = 0
        scopes = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
        while count < 8
            scope = scopes[count]
            create(:binder_branding, :scope => scope, :user_branding_id => create(:user_profile).id, :binder_id => binder.id)
            count += 1
        end
        expect(binder.binder_brandings.count).to eq(8)
        binder_branding = build(:binder_branding, :scope => "final", :user_branding_id => count, :binder_id => binder.id)
        expect(binder_branding).to_not be_valid 
        expect(binder_branding.errors.full_messages.first).to eq("Binder #{binder.id} has reached maximum amount of binder brandings")
    end
    
    describe "#create" do
        it "requires branding scope" do
            branding = Binder::BinderBranding.new({
                :scope => nil,
            })
            branding.save
                expect(branding.errors.full_messages.first).to eq("Scope is required")
            expect(branding.class).to be(Binder::BinderBranding)
            expect(branding.id).to be(nil)
        end
        it "requires branding user branding id" do
            branding = Binder::BinderBranding.new({
                :scope => "recall_email",
                :binder_id => binder.id,
                :user_branding_id => nil
            })
            branding.save
                expect(branding.errors.full_messages.first).to eq("User profile is required")
            expect(branding.class).to be(Binder::BinderBranding)
            expect(branding.id).to be(nil)
        end
        it "creates the branding" do
            branding = Binder::BinderBranding.new({
                :scope => "recall_email",
                :binder_id => binder.id,
                :user_branding_id => user.user_profile.id
            })
            branding.save
                expect(branding.class).to be(Binder::BinderBranding)
            expect(branding.id).to_not be(nil)
        end
    end
    
    describe "#generate_branding" do
        it "gets the branding for the user" do
            branding = Binder::BinderBranding.new({
                :scope => "recall_email",
                :binder_id => binder.id,
                :user_branding_id => user.user_profile.id
            })
            branding.save
                # get the branding user
            branding_user = branding.generate_branding
            # verify branding information
            expect(branding_user[:email]).to eq(user.email)
            expect(branding_user[:role]).to eq(user.role)
            expect(branding_user[:message]).to eq("<p>Thank you for your business!</p>")
            expect(branding_user[:transfer_message_to_homeowner]).to eq(nil)
            expect(branding_user[:transfer_message_to_agent]).to eq(nil)
            expect(branding_user[:transfer_message_to_prepaid_agent]).to eq(nil)
            expect(branding_user[:full_role]).to eq("Home Inspector")
        end
        it "gets the branding for the partner when user is removed" do
            branding = Binder::BinderBranding.new({
                :scope => "recall_email",
                :binder_id => binder.id,
                :user_branding_id => member.user_profile.id,
                :partner_id => partner.id
            })
            branding.save
                # remove the user
            member.destroy
            branding.reload
                # get the branding user
            branding_user = branding.generate_branding
                # verify branding information
            expect(branding_user[:email]).to eq(user.email)
            expect(branding_user[:role]).to eq(user.role)
            expect(branding_user[:message]).to eq("<p>Thank you for your business!</p>")
            expect(branding_user[:transfer_message_to_homeowner]).to eq(nil)
            expect(branding_user[:transfer_message_to_agent]).to eq(nil)
            expect(branding_user[:transfer_message_to_prepaid_agent]).to eq(nil)
            expect(branding_user[:full_role]).to eq("Home Inspector")
        end
    end
    
    describe "#add_business_card_widget" do
        it "adds widget" do
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            user_binder_1 = create(:user_binder, :binder => binder, :role => "owner", :user => homeowner)
            expect(user_binder_1).to be_valid
            
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
            
            dashboard = Dashboards::DashboardLookup.new(homeowner).for_binder({:id => binder.id})
            
            expect(dashboard.widgets.count).to eq(4)
            
            branding = create(:binder_branding, :binder => binder, :scope => "binder")
            expect(branding).to be_valid
            
            dashboard.reload
            expect(dashboard.widgets.count).to eq(5)
        end
        
        it "does not add widget when scope is not binder" do
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            user_binder_1 = create(:user_binder, :binder => binder, :role => "owner", :user => homeowner)
            expect(user_binder_1).to be_valid
            
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
            
            dashboard = Dashboards::DashboardLookup.new(homeowner).for_binder({:id => binder.id})
            
            expect(dashboard.widgets.count).to eq(4)
            
            branding = create(:binder_branding, :binder => binder, :scope => "not_binder")
            expect(branding).to be_valid
            
            dashboard.reload
            expect(dashboard.widgets.count).to eq(4)
        end
        
        it "does not add widget when widget has already been added" do
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            user_binder_1 = create(:user_binder, :binder => binder, :role => "owner", :user => homeowner)
            expect(user_binder_1).to be_valid
            
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
            
            widget = Widget.find_by_name("Double Business Card")
            
            dashboard = Dashboards::DashboardLookup.new(homeowner).for_binder({:id => binder.id})
            dashboard.widgets << widget
            
            expect(dashboard.widgets.count).to eq(5)
            
            branding = create(:binder_branding, :binder => binder, :scope => "binder")
            expect(branding).to be_valid
            
            dashboard.reload
            expect(dashboard.widgets.count).to eq(5)
        end
    end

    describe "#remove_business_card_widget" do
        it "removes widget" do
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            user_binder_1 = create(:user_binder, :binder => binder, :role => "owner", :user => homeowner)
            expect(user_binder_1).to be_valid
            branding = create(:binder_branding, :binder => binder, :scope => "binder")
            expect(branding).to be_valid
            
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
            
            widget = Widget.find_by_name("Double Business Card")
            
            dashboard = Dashboards::DashboardLookup.new(homeowner).for_binder({:id => binder.id})
            dashboard.widgets << widget
            
            expect(dashboard.widgets.count).to eq(5)
            
            branding.destroy
            dashboard.reload
            
            expect(dashboard.widgets.count).to eq(4)
        end
        
        it "removes widget if present agent is seller_agent" do
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            agent = create(:user, :role => "agent")
            user_binder_1 = create(:user_binder, :binder => binder, :role => "owner", :user => homeowner)
            expect(user_binder_1).to be_valid
            branding = create(:binder_branding, :binder => binder, :scope => "binder")
            expect(branding).to be_valid
            user_binder_2 = build(:user_binder, :binder => binder, :role => "seller_agent", :user => agent)
            expect(user_binder_2).to be_valid
            
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
            
            widget = Widget.find_by_name("Double Business Card")
            
            dashboard = Dashboards::DashboardLookup.new(homeowner).for_binder({:id => binder.id})
            dashboard.widgets << widget
            
            expect(dashboard.widgets.count).to eq(5)
            
            branding.destroy
            dashboard.reload
            
            expect(dashboard.widgets.count).to eq(4)
        end
        
        it "does not remove widget if the scope is not binder" do
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            user_binder_1 = create(:user_binder, :binder => binder, :role => "owner", :user => homeowner)
            expect(user_binder_1).to be_valid
            branding = create(:binder_branding, :binder => binder, :scope => "not_binder")
            expect(branding).to be_valid
            
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
            
            widget = Widget.find_by_name("Double Business Card")
            
            dashboard = Dashboards::DashboardLookup.new(homeowner).for_binder({:id => binder.id})
            dashboard.widgets << widget
            
            expect(dashboard.widgets.count).to eq(5)
            
            branding.destroy
            dashboard.reload
            
            expect(dashboard.widgets.count).to eq(5)
        end
        
        it "does not remove widget when buyer_agent is present" do
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            agent = create(:user, :role => "agent")
            user_binder_1 = create(:user_binder, :binder => binder, :role => "owner", :user => homeowner)
            expect(user_binder_1).to be_valid
            branding = create(:binder_branding, :binder => binder, :scope => "binder")
            expect(branding).to be_valid
            user_binder_2 = create(:user_binder, :binder => binder, :role => "buyer_agent", :user => agent)
            expect(user_binder_2).to be_valid
            
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
            
            widget = Widget.find_by_name("Double Business Card")
            
            dashboard = Dashboards::DashboardLookup.new(homeowner).for_binder({:id => binder.id})
            dashboard.widgets << widget
            
            expect(dashboard.widgets.count).to eq(5)
            
            branding.destroy
            dashboard.reload
            
            expect(dashboard.widgets.count).to eq(5)
        end
    end
end