require 'rails_helper'

RSpec.describe SubscriptionWrapper, :type => :model do
    describe "#initialize" do
        it "raises an error" do
            allow(ErrorService).to receive(:perform_async)
            
            allow(Stripe::Customer).to receive(:retrieve).and_raise(BadRequestException)
            binder = create(:binder)
            subscription = create(:subscription, :binder => binder, :customer_id => 1)
            binder.subscription = subscription
            binder.save!
            SubscriptionWrapper.new(subscription)
            
            expect(ErrorService).to have_received(:perform_async)
        end
        it "returns" do
            allow(Stripe::Customer).to receive(:retrieve).and_return(nil)
            binder = create(:binder)
            subscription = create(:subscription, :binder => binder, :customer_id => 1)
            binder.subscription = subscription
            binder.save!
            subscription_wrapper = SubscriptionWrapper.new(subscription)
            expect(subscription_wrapper.card_type).to be(nil)
        end
        it "returns a customer" do
            retrieved_card = OpenStruct.new({:delete => true, :type => "Visa", :last4 => "4242"})
            cards = OpenStruct.new({:retrieve => true, :create => true})
            plan = OpenStruct.new({id: 1, amount: 4900})
            stripe_subscription = OpenStruct.new({ plan: plan, current_period_end: Date.today})
            coupon = OpenStruct.new({id: 1, amount_off: 4900})
            discount = OpenStruct.new({ coupon: coupon })
            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, discount: discount, delete: true, default_card: {}, cards: cards, subscription: stripe_subscription})
            allow(Stripe::Customer).to receive(:retrieve).and_return(stripe_customer)
            allow(stripe_customer.cards).to receive(:retrieve).and_return(retrieved_card)
            binder = create(:binder)
            subscription = create(:subscription, :binder => binder, :customer_id => 1)
            binder.subscription = subscription
            binder.save!
            subscription_wrapper = SubscriptionWrapper.new(subscription)
            expect(subscription_wrapper.card_type).to eq("Visa")
        end
    end
end
