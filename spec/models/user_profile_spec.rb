require 'rails_helper'

RSpec.describe UserProfile, :type => :model do
    
    before :each do
        @user = create(:user, :role => "inspector")
        @user_profile = @user.user_profile
    end
    
    it "has a valid factory" do
        expect(@user_profile).to be_valid
    end
    
    it "is invalid because first name is too long" do
        user_profile = build(:user_profile, first_name: Faker::Lorem.characters(51))
        expect(user_profile).not_to be_valid
        expect(user_profile.errors.full_messages.first).to eq("First name is too long (maximum is 50 characters)")
    end
    
    it "is invalid because last name is too long" do
        user_profile = build(:user_profile, last_name: Faker::Lorem.characters(51))
        expect(user_profile).not_to be_valid
        expect(user_profile.errors.full_messages.first).to eq("Last name is too long (maximum is 50 characters)")
    end
    
    it "is invalid because sex is too long" do
        user_profile = build(:user_profile, sex: Faker::Lorem.characters(2))
        expect(user_profile).not_to be_valid
        expect(user_profile.errors.full_messages.first).to eq("Sex is too long (maximum is 1 character)")
    end

    it "shows the display name with first and last name" do
        profile = create(:user_profile, :user_id => @user.id, :first_name => "Jack", :last_name => "Frost")
        expect(profile.display_name).to eq("Jack Frost")
    end
    
    it "shows the display name with only first" do
        profile = create(:user_profile, :user_id => @user.id, :first_name => "Jack", :last_name => nil)
        expect(profile.display_name).to eq("Jack")
    end
    
    it "shows the display name with only the email" do
        profile = create(:user_profile, :user_id => @user.id, :first_name => nil, :last_name => nil)
        expect(profile.display_name).to eq(@user.email)
    end
    
    it "shows the user email" do
        profile = create(:user_profile, :user_id => @user.id,)
        expect(profile.email).to eq(@user.email)
    end
    
    it "saves the headshot" do
        profile = create(:user_profile, :user_id => @user.id,)
        profile.head_shot_url("https://dummyimage.com/300/09f/fff.png")
        profile.save
    
        expect(profile.head_shot).to_not be(nil)
        expect(profile.head_shot_file_name).to eq("fff.png")
    end
    
    it "saves the company logo" do
        profile = create(:user_profile, :user_id => @user.id,)
        profile.logo_url("https://dummyimage.com/300/09f/fff.png")
        profile.save
        expect(profile.logo).to_not be(nil)
        expect(profile.logo_file_name).to eq("fff.png")
    end
    
    it "shows the user's branding information" do
        profile = create(:user_profile, :user_id => @user.id, :first_name => nil, :last_name => nil, :mobile_phone => nil)
        branding = {
            :email => @user.email, 
            :role => "inspector", 
            :first_name => nil, 
            :last_name => nil, 
            :mobile_phone => nil,
            :partner_id => nil,
            :company => profile.company.titleize, 
            :display_name => profile.display_name,
            :message => "<p>Thank you for your business!</p>",
            :transfer_message_to_homeowner => nil,
            :transfer_message_to_agent => nil,
            :transfer_message_to_prepaid_agent => nil,
            :headshot => "https://s3.amazonaws.com/homebinderstatic/img/headshot.png", 
            :full_role => "Home Inspector", 
            :logo => nil,
            :website => nil,
            :types => "Home Inspector",
            :sub_types => "",
            :binder_count => 0
             }
        expect(profile.branding).to eq(branding)
    end
    
    it "shows the full role for an inspector" do
        user = create(:user, :role => "inspector")
        profile = create(:user_profile, :user_id => user.id,)
        expect(profile.full_role).to eq("Home Inspector")
    end
    
    it "shows the full role for a broker" do
        user = create(:user, :role => "broker")
        profile = create(:user_profile, :user_id => user.id,)
        expect(profile.full_role).to eq("Real Estate Broker")
    end
    
    it "shows the full role for an agent" do
        user = create(:user, :role => "agent")
        profile = create(:user_profile, :user_id => user.id,)
        expect(profile.full_role).to eq("Real Estate Agent")
    end
    
    it "does not delete the account" do
        inspector = create(:user, :role => "inspector")
        partner = create(:partner)
        partner.partner_configuration.default_user_branding_id = inspector.user_profile.id
        partner.save
        inspector.destroy
        expect{User.find(inspector.id)}.to_not raise_error
        inspector.user_profile.reload
        expect(inspector.user_profile.errors.full_messages.first).to eq("Id Cannot delete profile for #{inspector.user_profile.display_name} because he/she is the default branding user for a partner")
    end

end
