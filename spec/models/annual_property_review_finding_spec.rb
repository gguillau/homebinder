require 'rails_helper'

RSpec.describe AnnualPropertyReviewFinding, :type => :model do
  
  it "has a valid factory" do
    apr = build(:annual_property_review_finding)
    expect(apr).to be_valid
  end
  
  describe "validation" do
    it "requires name" do
      aprf = build(:annual_property_review_finding, name: nil)
      expect(aprf).not_to be_valid
    end
  
    it "name length <= 255" do
      aprf = build(:annual_property_review_finding, name: Faker::Lorem.characters(256))
      expect(aprf).not_to be_valid
    end
  end
  
  describe "#recommended_homepro" do
    it "returns nil" do
      apr_finding = create(:annual_property_review_finding)
      homepro = apr_finding.recommended_homepro
      expect(homepro).to be(nil)
    end
    
    it "returns nil" do
      apr_finding = create(:annual_property_review_finding, :recommended_homepro_id => 1)
      homepro = apr_finding.recommended_homepro
      expect(homepro).to be(nil)
    end
    
    it "returns contractor" do
      contractor_template = create(:contractor_template)
      apr_finding = create(:annual_property_review_finding, :recommended_homepro_id => contractor_template.id)
      homepro = apr_finding.recommended_homepro
      expect(homepro).to_not be(nil)
      expect(homepro[:id]).to eq(contractor_template.id)
    end
  end
  
  describe "#cost" do
    it "returns Less than $100" do
      apr_finding = create(:annual_property_review_finding, :estimated_cost => "less_hun")
      expect(apr_finding.cost).to eq("Less than $100")
    end
    it "returns $100 - $250" do
      apr_finding = create(:annual_property_review_finding, :estimated_cost => "one_hun_to_two_fity")
      expect(apr_finding.cost).to eq("$100 - $250")
    end
    it "returns $250 - $500" do
      apr_finding = create(:annual_property_review_finding, :estimated_cost => "two_fifty_to_five_hun")
      expect(apr_finding.cost).to eq("$250 - $500")
    end
    it "returns $500 - $1,000" do
      apr_finding = create(:annual_property_review_finding, :estimated_cost => "five_hun_to_one_thou")
      expect(apr_finding.cost).to eq("$500 - $1,000")
    end
    it "returns $1,000 - $2,500" do
      apr_finding = create(:annual_property_review_finding, :estimated_cost => "one_thou_to_twenty_hun")
      expect(apr_finding.cost).to eq("$1,000 - $2,500")
    end
    it "returns $2,000 to $5,000" do
      apr_finding = create(:annual_property_review_finding, :estimated_cost => "twenty_hun_to_five_thou")
      expect(apr_finding.cost).to eq("$2,000 to $5,000")
    end
    it "returns $5,000+" do
      apr_finding = create(:annual_property_review_finding, :estimated_cost => "five_thou_plus")
      expect(apr_finding.cost).to eq("$5,000+")
    end
    it "returns unknown" do
      apr_finding = create(:annual_property_review_finding, :estimated_cost => "unknown")
      expect(apr_finding.cost).to eq("Unknown")
    end
    it "returns TBD by Home Professional" do
      apr_finding = create(:annual_property_review_finding, :estimated_cost => "tbd")
      expect(apr_finding.cost).to eq("TBD by Home Professional")
    end
  end
  
  describe "#default_photo" do
    it "returns default photo" do
      apr_finding = create(:annual_property_review_finding)
      expect(apr_finding.default_photo).to eq("https://www.homebinder.com/img/no-image-available.jpg")
    end
    
    it "returns apr photo" do
      apr_finding = create(:annual_property_review_finding)
      create(:annual_property_review_photo, :annual_property_review_finding_id => apr_finding.id, file: File.new("spec/assets/SampleImage.png"))
      expect(apr_finding.default_photo).to_not eq("https://www.homebinder.com/img/no-image-available.jpg")
    end
  end
end