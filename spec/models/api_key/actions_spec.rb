require 'rails_helper'

RSpec.describe ApiKey::ApiKeyBuilder, :type => :model do
  
  it "has a valid factory" do
    api_key = create :api_key
    expect(api_key).to be_valid
  end
  
  describe "update" do
    it "raises a ActiveRecord::RecordNotFound" do
      partner = create :partner
      user = create :user, :role => "inspector"
      
      parameters = {
      		partner_id: 99999,
					company_name: partner.name,
					application_name: "Website",
					contact_email: partner.email
      }
      
      request = HBRequest.new
      request.user = user
      request.ability = Ability.new(user)
      
      expect {ApiKey.build(request, {partner: parameters}) }.to raise_error ActiveRecord::RecordNotFound
    end
    
    it "raises a CanCan error" do
      partner = create :partner
      user = create :user, :role => "homeowner"
      
      parameters = {
      		partner_id: partner.id,
					company_name: partner.name,
					application_name: "Website",
					contact_email: partner.email
      }
      
      request = HBRequest.new
      request.user = user
      request.ability = Ability.new(user)
      
      parameters = ActionController::Parameters.new(parameters)
      expect {ApiKey.build(request, {partner: parameters}) }.to raise_error CanCan::AccessDenied
    end
    
    it "raises an UnprocessableException" do
      
      partner = create :partner
      user = create :user, :role => "inspector"
      user.add_role :partner_admin, partner
      create :api_key, :partner_id => partner.id, :company_name => partner.name
      
      parameters = {
      		partner_id: partner.id,
					company_name: partner.name,
					contact_email: partner.email
      }
      
      request = HBRequest.new
      request.user = user
      request.ability = Ability.new(user)
      parameters = ActionController::Parameters.new(parameters)
      allow_any_instance_of(ApiKey).to receive(:update_attributes).and_return false
      
      expect{ApiKey.build(request, {partner: parameters})}.to raise_error UnprocessableException
    end
    
    it "raises an UnprocessableException" do
      
      partner = create :partner
      user = create :user, :role => "inspector"
      user.add_role :partner_admin, partner
      
      parameters = {
      		partner_id: partner.id,
					company_name: partner.name,
					application_name: "Website",
					contact_email: partner.email
      }
      
      request = HBRequest.new
      request.user = user
      request.ability = Ability.new(user)
      parameters = ActionController::Parameters.new(parameters)
      allow_any_instance_of(ApiKey).to receive(:save).and_return false
      
      expect{ApiKey.build(request, {partner: parameters})}.to raise_error UnprocessableException
    end

    it "creates a new api key" do
      
      partner = create :partner
      user = create :user, :role => "inspector"
      user.add_role :partner_admin, partner
      
      parameters = {
      		partner_id: partner.id,
					company_name: partner.name,
					application_name: "Website",
					contact_email: partner.email
      }
      
      request = HBRequest.new
      request.user = user
      request.ability = Ability.new(user)
      parameters = ActionController::Parameters.new(parameters)
      key = ApiKey.build(request, {partner: parameters})
      
      expect(key.company_name).to eq(partner.name)
      expect(key.partner_id).to be(partner.id)
      expect(key.contact_email).to eq(partner.email)
      expect(key.key).to_not be(nil)
    end
  end
  
end
