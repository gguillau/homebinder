require 'rails_helper'

RSpec.describe WarrantyCompany, type: :model do
    
    it "has a valid factory" do
        warranty_company = create :warranty_company
        expect(warranty_company).to be_valid
    end
    
    it "requires a name" do
        warranty_company = build(:warranty_company, name: nil)
        expect(warranty_company).to_not be_valid
        expect(warranty_company.errors.full_messages.first).to eq("Name is required")
    end
    
    it "requires a valid name with less than 100 characters" do
        warranty_company = build(:warranty_company, name: Faker::Lorem.characters(101))
        expect(warranty_company).to_not be_valid
        expect(warranty_company.errors.full_messages.first).to eq("Name is too long (maximum is 100 characters)")
    end
    
    it "requires a contact" do
        warranty_company = build(:warranty_company, contact: nil)
        expect(warranty_company).to_not be_valid
        expect(warranty_company.errors.full_messages.first).to eq("Contact is required")
    end
    
    it "requires a valid contact with less than 100 characters" do
        warranty_company = build(:warranty_company, contact: Faker::Lorem.characters(101))
        expect(warranty_company).to_not be_valid
        expect(warranty_company.errors.full_messages.first).to eq("Contact is too long (maximum is 100 characters)")
    end
    
    it "requires an email" do
        warranty_company = build(:warranty_company, email: nil)
        expect(warranty_company).to_not be_valid
        expect(warranty_company.errors.full_messages.first).to eq("Email is required")
    end
    
    it "requires a valid email with less than 100 characters" do
        warranty_company = build(:warranty_company, email: Faker::Lorem.characters(101))
        expect(warranty_company).to_not be_valid
        expect(warranty_company.errors.full_messages.first).to eq("Email is too long (maximum is 100 characters)")
    end
    
    it "requires an unique email" do
        warranty_company = create :warranty_company
        expect(warranty_company).to be_valid
        warranty_company_2 = build :warranty_company, :email => warranty_company.email
        expect(warranty_company_2).to_not be_valid
        expect(warranty_company_2.errors.full_messages.first).to eq("Email has already been taken")
    end
end
