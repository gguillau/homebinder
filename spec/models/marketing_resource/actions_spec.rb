require 'rails_helper'
require "cancan/matchers"

RSpec.describe MarketingResource::Actions, :type => :model do

    let(:admin) { FactoryBot.create(:user, :role => "admin") }
    let(:resource) {FactoryBot.create(:marketing_resource, :user_type => "inspector")}
    let(:organization) {FactoryBot.create(:organization)}
    let(:inspector) {FactoryBot.create(:partner)}
    let(:broker) {FactoryBot.create(:partner, :partner_type => "broker")}
    let!(:request) {HBRequest.new.create_request(admin)}

    describe "build" do
        it "raise an error because of missing field" do
            expect{MarketingResource.build(request, {})}.to raise_error BadRequestException, "Marketing Resource required"
        end
        it "raise an error because of name is too long" do
            hash = ActionController::Parameters.new({:name => Faker::Lorem.characters(101)})
            expect{MarketingResource.build(request, {:marketing_resource => hash})}.to raise_error UnprocessableException
        end
        it "creates the resource" do
            hash = ActionController::Parameters.new({:name => "TET"})
            resource = MarketingResource.build(request, {:marketing_resource => hash})
                expect(resource).to_not be(nil)
            expect(resource.id).to_not be(nil)
            expect(resource.name).to eq("TET")
        end
        it "creates the resource and a partner/organization resource" do
            hash = ActionController::Parameters.new({:name => "TET", :user_type => "inspector"})
            OrganizationPartner.create!(:partner_id => inspector.id, :organization_id => organization.id)
            resource = MarketingResource.build(request, {:marketing_resource => hash, :partner_id => broker.id, :organization_id => organization.id})
                expect(resource).to_not be(nil)
            expect(resource.id).to_not be(nil)
            expect(resource.name).to eq("TET")
            expect(PartnerResource.all.count).to eq(2)
            expect(OrganizationResource.all.count).to eq(1)
            expect(inspector.resources.count).to eq(1)
            expect(broker.resources.count).to eq(1)
        end
    end
    
    describe "update" do
        it "raise an error because of missing field" do
            expect{MarketingResource.update(request, {})}.to raise_error BadRequestException, "Marketing Resource ID required"
        end
        it "raise an error because of user permissions" do
            homeowner = create(:user, :role => "homeowner")
            request = HBRequest.new.create_request(homeowner)
            expect{MarketingResource.update(request, {:id => resource.id, :marketing_resource => {:name => "TEST"}})}.to raise_error CanCan::AccessDenied
        end
        it "raise an error because of missing resource" do
            expect{MarketingResource.update(request, {:id => 99999, :marketing_resource => {:name => "TEST"}})}.to raise_error ActiveRecord::RecordNotFound
        end
        it "raise an error because of long name" do
            hash = ActionController::Parameters.new({:name => Faker::Lorem.characters(101)})
            expect{MarketingResource.update(request, {:id => resource.id, :marketing_resource => hash})}.to raise_error UnprocessableException
        end
        it "updates the resource" do
            hash = ActionController::Parameters.new({:name => "MARKETING"})
            resrce = MarketingResource.update(request, {:id => resource.id, :marketing_resource => hash})
                expect(resrce.name).to eq("MARKETING")
        end
    end
    
    describe "delete" do
        it "raise an error because of missing field" do
            expect{MarketingResource.destroy(request, nil)}.to raise_error BadRequestException, "Marketing Resource ID required"
        end
        it "raise an error because of user permissions" do
            homeowner = create(:user, :role => "homeowner")
            request = HBRequest.new.create_request(homeowner)
            expect{MarketingResource.destroy(request, resource.id)}.to raise_error CanCan::AccessDenied
        end
        it "deletes the resource" do
            MarketingResource.destroy(request, resource.id)
            expect{MarketingResource.find(resource.id)}.to raise_error ActiveRecord::RecordNotFound
        end
    end

    describe "#pager" do
        it "searches" do
            partner = create(:partner)
            marketing_resource = create(:marketing_resource)
            create(:partner_resource, :partner => partner, :marketing_resource => marketing_resource)
                page = MarketingResource.pager(@request, {:partnerId => partner.id})
            expect(page.total).to eq(1)
        end
        it "searches" do
            organization = create(:organization)
            marketing_resource = create(:marketing_resource)
            create(:organization_resource, :organization => organization, :marketing_resource => marketing_resource)
                page = MarketingResource.pager(@request, {:organizationId => organization.id})
            expect(page.total).to eq(1)
        end
    end
    
end
