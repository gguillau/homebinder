require 'rails_helper'

RSpec.describe UserBinder, :type => :model do
    
    it "has a valid factory" do
        user_binder = build :user_binder
        expect(user_binder).to be_valid
    end
    
    it "is invalid because binder_id is required" do
        user_binder = build(:user_binder, binder_id: nil)
        expect(user_binder).not_to be_valid
        expect(user_binder.errors.full_messages.first).to eq("Binder is required")
    end
    
    it "is invalid because user_id is required" do
        user_binder = build(:user_binder, user_id: nil)
        expect(user_binder).not_to be_valid
        expect(user_binder.errors.full_messages.first).to eq("User is required")
    end
    
    it "is invalid because role is required" do
        user_binder = build(:user_binder, role: nil)
        expect(user_binder).not_to be_valid
        expect(user_binder.errors.full_messages.first).to eq("Role is required")
    end
    
    it "is invalid because user already has a role on the binder" do
        user_binder = create(:user_binder)
        expect(user_binder).to be_valid
        user_binder_2 = build(:user_binder, binder_id: user_binder.binder_id, user_id: user_binder.user_id, role: user_binder.role)
        expect(user_binder_2).not_to be_valid
        expect(user_binder_2.errors.full_messages.first).to eq("User already has role on binder")
    end
    
    it "is invalid because inspector is not an agent" do
        inspector = create(:user, :role => "inspector")
        user_binder_1 = build(:user_binder, :user => inspector, :role => "buyer_agent")
        expect(user_binder_1).to_not be_valid
        user_binder_2 = build(:user_binder, :user => inspector, :role => "seller_agent")
        expect(user_binder_2).to_not be_valid
    end
    
    it "is invalid because broker is not an agent" do
        broker = create(:user, :role => "broker")
        user_binder_1 = build(:user_binder, :user => broker, :role => "buyer_agent")
        expect(user_binder_1).to_not be_valid
        user_binder_2 = build(:user_binder, :user => broker, :role => "seller_agent")
        expect(user_binder_2).to_not be_valid
    end
    
    it "is invalid because apr_reviewer is not an agent" do
        apr_reviewer = create(:user, :role => "apr_reviewer")
        user_binder_1 = build(:user_binder, :user => apr_reviewer, :role => "buyer_agent")
        expect(user_binder_1).to_not be_valid
        user_binder_2 = build(:user_binder, :user => apr_reviewer, :role => "seller_agent")
        expect(user_binder_2).to_not be_valid
    end
    
    it "is invalid because homeowner is not an agent" do
        homeowner = create(:user, :role => "homeowner")
        user_binder_1 = build(:user_binder, :user => homeowner, :role => "buyer_agent")
        expect(user_binder_1).to_not be_valid
        user_binder_2 = build(:user_binder, :user => homeowner, :role => "seller_agent")
        expect(user_binder_2).to_not be_valid
    end
    
    it "is invalid because admin is not an agent" do
        admin = create(:user, :role => "admin")
        user_binder_1 = build(:user_binder, :user => admin, :role => "buyer_agent")
        expect(user_binder_1).to_not be_valid
        user_binder_2 = build(:user_binder, :user => admin, :role => "seller_agent")
        expect(user_binder_2).to_not be_valid
    end
    
    it "is invalid because binder already has an owner" do
        binder = create(:binder)
        user_binder_1 = create(:user_binder, :binder => binder, :role => "owner")
        expect(user_binder_1).to be_valid
        user_binder_2 = build(:user_binder, :binder => binder, :role => "owner")
        expect(user_binder_2).to_not be_valid
    end
    
    it "is invalid because binder already has a buyer_agent" do
        binder = create(:binder)
        agent = create(:user, :role => "agent")
        user_binder_1 = create(:user_binder, :binder => binder, :role => "buyer_agent", :user => agent)
        expect(user_binder_1).to be_valid
        agent2 = create(:user, :role => "agent")
        user_binder_2 = build(:user_binder, :binder => binder, :role => "buyer_agent", :user => agent2)
        expect(user_binder_2).to_not be_valid
    end

    it "is invalid because binder already has a seller_agent" do
        binder = create(:binder)
        agent = create(:user, :role => "agent")
        user_binder_1 = create(:user_binder, :binder => binder, :role => "seller_agent", :user => agent)
        expect(user_binder_1).to be_valid
        agent2 = create(:user, :role => "agent")
        user_binder_2 = build(:user_binder, :binder => binder, :role => "seller_agent", :user => agent2)
        expect(user_binder_2).to_not be_valid
    end
    
    it "is invalid because binder already has a seller_agent" do
        binder = create(:binder)
        agent = create(:user, :role => "agent")
        user_binder_1 = create(:user_binder, :binder => binder, :role => "seller_agent", :user => agent)
        expect(user_binder_1).to be_valid
        agent2 = create(:user, :role => "agent")
        user_binder_2 = build(:user_binder, :binder => binder, :role => "seller_agent", :user => agent2)
        expect(user_binder_2).to_not be_valid
    end
    
    it "allows agent to have multiple roles on the binder" do
        binder = create(:binder)
        agent = create(:user, :role => "agent")
        user_binder_1 = create(:user_binder, :binder => binder, :role => "seller_agent", :user => agent)
        expect(user_binder_1).to be_valid
        user_binder_2 = create(:user_binder, :binder => binder, :role => "buyer_agent", :user => agent)
        expect(user_binder_2).to be_valid
        user_binder_3 = create(:user_binder, :binder => binder, :role => "owner", :user => agent)
        expect(user_binder_3).to be_valid
    end
    
    it "does not allow homeowner to have multiple roles on the binder" do
        binder = create(:binder)
        homeowner = create(:user, :role => "homeowner")
        user_binder_1 = create(:user_binder, :binder => binder, :role => "owner", :user => homeowner)
        expect(user_binder_1).to be_valid
        user_binder_2 = build(:user_binder, :binder => binder, :role => "co_owner", :user => homeowner)
        expect(user_binder_2).to_not be_valid
    end
    
    describe "#pager" do
        it "searches" do
            admin = create(:user, :role => "admin")
            request = HBRequest.new.create_request(admin)
            create(:user_binder, :user => admin)
                page = UserBinder.pager(request, {:user_id => admin.id})
            expect(page.total).to eq(1)
        end
    end
    
    describe "#add_business_card_widget" do
        it "adds widget" do
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            agent = create(:user, :role => "agent")
            user_binder_1 = create(:user_binder, :binder => binder, :role => "owner", :user => homeowner)
            expect(user_binder_1).to be_valid
            
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
            
            dashboard = Dashboards::DashboardLookup.new(homeowner).for_binder({:id => binder.id})
            
            expect(dashboard.widgets.count).to eq(4)
            
            user_binder_2 = create(:user_binder, :binder => binder, :role => "buyer_agent", :user => agent)
            expect(user_binder_2).to be_valid
            
            dashboard.reload
            expect(dashboard.widgets.count).to eq(5)
        end
        
        it "does not add widget if role is not buyer_agent" do
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            agent = create(:user, :role => "agent")
            user_binder_1 = create(:user_binder, :binder => binder, :role => "owner", :user => homeowner)
            expect(user_binder_1).to be_valid
            
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
            
            dashboard = Dashboards::DashboardLookup.new(homeowner).for_binder({:id => binder.id})
            
            expect(dashboard.widgets.count).to eq(4)
            
            user_binder_2 = create(:user_binder, :binder => binder, :role => "seller_agent", :user => agent)
            expect(user_binder_2).to be_valid
            
            dashboard.reload
            expect(dashboard.widgets.count).to eq(4)
        end
        
        it "does not widget if widget has already been injected" do
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            agent = create(:user, :role => "agent")
            user_binder_1 = create(:user_binder, :binder => binder, :role => "owner", :user => homeowner)
            expect(user_binder_1).to be_valid
            
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
            
            widget = Widget.find_by_name("Double Business Card")
            
            dashboard = Dashboards::DashboardLookup.new(homeowner).for_binder({:id => binder.id})
            dashboard.widgets << widget
            
            expect(dashboard.widgets.count).to eq(5)
            
            user_binder_2 = create(:user_binder, :binder => binder, :role => "buyer_agent", :user => agent)
            expect(user_binder_2).to be_valid
            
            dashboard.reload
            expect(dashboard.widgets.count).to eq(5)

        end
    end
    
    describe "#remove_business_card_widget" do
        it "removes widget" do
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            agent = create(:user, :role => "agent")
            user_binder_1 = create(:user_binder, :binder => binder, :role => "owner", :user => homeowner)
            expect(user_binder_1).to be_valid
            user_binder_2 = build(:user_binder, :binder => binder, :role => "buyer_agent", :user => agent)
            expect(user_binder_2).to be_valid
            
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
            
            widget = Widget.find_by_name("Double Business Card")
            
            dashboard = Dashboards::DashboardLookup.new(homeowner).for_binder({:id => binder.id})
            dashboard.widgets << widget
            
            expect(dashboard.widgets.count).to eq(5)
            
            user_binder_2.destroy
            dashboard.reload
            
            expect(dashboard.widgets.count).to eq(4)
        end
        
        it "removes widget when binder_branding scope is not binder" do
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            agent = create(:user, :role => "agent")
            user_binder_1 = create(:user_binder, :binder => binder, :role => "owner", :user => homeowner)
            expect(user_binder_1).to be_valid
            user_binder_2 = build(:user_binder, :binder => binder, :role => "buyer_agent", :user => agent)
            expect(user_binder_2).to be_valid
            branding = create(:binder_branding, :binder => binder, :scope => "not_binder")
            expect(branding).to be_valid
            
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
            
            widget = Widget.find_by_name("Double Business Card")
            
            dashboard = Dashboards::DashboardLookup.new(homeowner).for_binder({:id => binder.id})
            dashboard.widgets << widget
            
            expect(dashboard.widgets.count).to eq(5)
            
            user_binder_2.destroy
            dashboard.reload
            
            expect(dashboard.widgets.count).to eq(4)
        end
        
        it "does not remove widget if the role is not buyer_agent" do
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            agent = create(:user, :role => "agent")
            user_binder_1 = create(:user_binder, :binder => binder, :role => "owner", :user => homeowner)
            expect(user_binder_1).to be_valid
            user_binder_2 = build(:user_binder, :binder => binder, :role => "seller_agent", :user => agent)
            expect(user_binder_2).to be_valid
            
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
            
            widget = Widget.find_by_name("Double Business Card")
            
            dashboard = Dashboards::DashboardLookup.new(homeowner).for_binder({:id => binder.id})
            dashboard.widgets << widget
            
            expect(dashboard.widgets.count).to eq(5)
            
            user_binder_2.destroy
            dashboard.reload
            
            expect(dashboard.widgets.count).to eq(5)
        end
        
        it "does not remove widget if the binder has a branding with a partner" do
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            agent = create(:user, :role => "agent")
            user_binder_1 = create(:user_binder, :binder => binder, :role => "owner", :user => homeowner)
            expect(user_binder_1).to be_valid
            user_binder_2 = build(:user_binder, :binder => binder, :role => "buyer_agent", :user => agent)
            expect(user_binder_2).to be_valid
            branding = create(:binder_branding, :binder => binder, :scope => "binder")
            expect(branding).to be_valid
            
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
            
            widget = Widget.find_by_name("Double Business Card")
            
            dashboard = Dashboards::DashboardLookup.new(homeowner).for_binder({:id => binder.id})
            dashboard.widgets << widget
            
            expect(dashboard.widgets.count).to eq(5)
            
            user_binder_2.destroy
            dashboard.reload
            
            expect(dashboard.widgets.count).to eq(5)
        end
    end

end
