require 'rails_helper'
require "cancan/matchers"

RSpec.describe RepairPricerReport::Actions, :type => :model do

    describe "update" do
        it "updates the report and processes a refund" do
            card_processor = double("Payments::CreditCardProcessor")
            allow(card_processor).to receive(:refund).and_return(Stripe::Refund.new(id: "refund_id"))
            
            user = create(:user, :role => "admin")
            request = HBRequest.new.create_request(user)
            repair_pricer_report = create(:repair_pricer_report, :payment_status => "paid", :stripe_charge_id => "charged", :amount_charged_cents => 49)
            params = ActionController::Parameters.new({})
            
            report = RepairPricerReport.update(request, {:action_type => "refund", :id => repair_pricer_report.id, :repair_pricer_report => params})
            
            expect(report.status).to eq("cancelled")
            expect(report.payment_status).to eq("refunded")
        end
    end
    
    describe "build" do
        it "creates the report and calls set_charge" do
            Sidekiq::Testing.disable!
            card_processor = double("Payments::CreditCardProcessor")
            allow(card_processor).to receive(:charge_card).and_return(OpenStruct.new({:id => 1, :amount => 4900}))
            expect_any_instance_of(RepairPricerReport).to receive(:set_charge)
            
            user = create(:user, :role => "admin")
            document = create(:document)
            request = HBRequest.new.create_request(user)
            repair_pricer_report = build(:repair_pricer_report)
            address = build(:address)
            params = ActionController::Parameters.new(repair_pricer_report.attributes)
            
            RepairPricerReport.build(request, {:repair_pricer_report => params, :payment => {:total => 4900, :card => "test"}, :document_id => document.id, :address => address})
        end
    end
    
    describe "set_charge" do
        it "retrieves the stripe charge and sets the payment status" do
            allow(Stripe::Charge).to receive(:retrieve).and_return(OpenStruct.new({id: "1", :amount => 4900}))
            repair_pricer_report = create(:repair_pricer_report, :payment_status => "not_paid", :stripe_charge_id => nil, :amount_charged_cents => 0)
            
            repair_pricer_report.set_charge("test")
            repair_pricer_report.reload
            
            expect(repair_pricer_report.stripe_charge_id).to eq("1")
            expect(repair_pricer_report.amount_charged_cents).to eq(4900)
            expect(repair_pricer_report.payment_status).to eq("paid")
        end
    end
    
    describe "process_payment" do
        it "raises an error" do
            card_processor = double("Payments::CreditCardProcessor", :charge_card => true)
            
            allow(Payments::CreditCardProcessor).to receive(:new).and_return(card_processor)
            allow(card_processor).to receive(:charge_card).and_raise(BadRequestException.new)
            
            user = create(:user, :role => "admin")
            expect{RepairPricerReport.process_payment(user, {:total => 49, :card => "card"})}.to raise_error(BadRequestException)
        end
    end
end
