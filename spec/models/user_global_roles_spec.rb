require 'rails_helper'

RSpec.describe UserGlobalRoles, :type => :model do
    it "returns admin" do
        expect(UserGlobalRoles::ADMIN.to_s).to eq("admin")
    end

    it "returns homeowner" do
        expect(UserGlobalRoles::HOMEOWNER.to_s).to eq("homeowner")
    end

    it "returns inspector" do
        expect(UserGlobalRoles::INSPECTOR.to_s).to eq("inspector")
    end

    it "returns broker" do
        expect(UserGlobalRoles::BROKER.to_s).to eq("broker")
    end

    it "returns agent" do
        expect(UserGlobalRoles::AGENT.to_s).to eq("agent")
    end

    it "returns partner" do
        expect(UserGlobalRoles::PARTNER.to_s).to eq("partner")
    end

    describe "map_partner_type" do
        it "returns inspector" do
            expect(UserGlobalRoles.map_partner_type("inspector").to_s).to eq("inspector")
        end
        it "returns broker" do
            expect(UserGlobalRoles.map_partner_type("broker").to_s).to eq("broker")
        end
        it "returns homepro" do
            expect(UserGlobalRoles.map_partner_type("homepro").to_s).to eq("homepro")
        end
        it "returns lender" do
            expect(UserGlobalRoles.map_partner_type("lender").to_s).to eq("lender")
        end
        it "returns builder" do
            expect(UserGlobalRoles.map_partner_type("builder").to_s).to eq("builder")
        end
        it "returns property_manager" do
            expect(UserGlobalRoles.map_partner_type("property_manager").to_s).to eq("property_manager")
        end
        it "returns hoa" do
            expect(UserGlobalRoles.map_partner_type("hoa").to_s).to eq("hoa")
        end
        it "returns insurance" do
            expect(UserGlobalRoles.map_partner_type("insurance").to_s).to eq("insurance")
        end
        it "returns partner" do
            expect(UserGlobalRoles.map_partner_type("partner").to_s).to eq("partner")
        end
    end
end
