require 'rails_helper'

RSpec.describe Binder, :type => :model do
    
    before :each do
        @binder = create(:binder)
    end

    it "has a valid factory" do
        expect(@binder).to be_valid
    end

    it "requires a maximum of 8 brandings" do
        binder = create :binder
        expect(binder).to be_valid

        count = 0
        scopes = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]

        while count < 8
            scope = scopes[count]
            create(:binder_branding, :scope => scope, :user_branding_id => create(:user_profile).id, :binder_id => binder.id)
            count += 1
        end

        expect(binder.binder_brandings.count).to eq(8)

        binder.binder_brandings.new(:scope => "final", :user_branding_id => count, :binder_id => binder.id).save
        expect(binder).to_not be_valid
        expect(binder.errors.full_messages.first).to eq("Binder brandings binder #{binder.id} has reached maximum amount of binder brandings")
    end

    it "is invalid without a name" do
        binder = build(:binder, name: nil)
        expect(binder).not_to be_valid
    end

    it "is invalid with a name length greater than 50" do
        binder = build(:binder, name: Faker::Lorem.characters(51))
        expect(binder).not_to be_valid
    end

    describe "getting subscription detail" do
        before :each do
            @subscription = create(:subscription, binder_id: @binder.id,
                                                  customer_id: "1")
            stripe_customer_response = OpenStruct.new(:subscription => @subscription)
            allow(Stripe::Customer).to receive(:retrieve).
                                           with(@subscription.customer_id).and_return(stripe_customer_response)
        end

        it "has an accessible subscription" do
            expect(@binder.subscription).to be_valid
        end

        context "when the subscription has a customer_id" do
            it "can access the subscription" do
                subscription = @binder.get_subscription_detail
                expect(subscription).to be_valid
            end
        end

        context "when the subscription does not have a customer_id" do
            it "cannot access the subscription" do
                @subscription.customer_id = nil
                @subscription.save
                subscription = @binder.get_subscription_detail
                expect(subscription).to be_nil
            end
        end
    end

    describe "getting owner" do
        before :each do
            @user = create :user
        end

        context "when a user is added as binder owner" do
            it "returns the user object of the owner" do
                @user.add_role :owner, @binder
                expect(@binder.get_owner).to eq(@user)
            end
        end

        context "when a user is not added as binder owner" do
            it "returns nil" do
                expect(@binder.get_owner).not_to eq @user
                expect(@binder.get_owner).to eq nil
            end
        end
    end

    describe "enabling/disabling email notifications for maintenance items" do
        it "turns off email notifications for all binder maintenance items" do
            binder = create(:binder)
            create_list(:maintenance_item, 5, binder_id: binder.id, do_date: Date.today + 30.days)
            
            binder.maintenance_items.each do |item|
                expect(item.do_date).to eq(Date.today + 30.days)
                expect(item.email_notifications).to be(true)
            end

            binder.suppress_maintenance_reminders
            binder.reload
            
            binder.maintenance_items.each do |item|
                expect(item.do_date).to eq(Date.today + 30.days)
                expect(item.email_notifications).to be(false)
            end
        end

        it "turns on email notifications for all binder maintenance items" do
            binder = create(:binder)
            create_list(:maintenance_item, 5, binder_id: binder.id, do_date: Date.today + 30.days)

            binder.suppress_maintenance_reminders
            binder.reload
            
            binder.maintenance_items.each do |item|
                expect(item.do_date).to eq(Date.today + 30.days)
                expect(item.email_notifications).to be(false)
            end
            binder.enable_maintenance_reminders
            binder.reload
            
            binder.maintenance_items.each do |item|
                expect(item.do_date).to eq(Date.today + 30.days)
                expect(item.email_notifications).to be(true)
            end
        end
    end

    describe "orphan" do
        it "does not set the binder owner to orphans@homebinder.com" do
            # create the binder/user
            binder = create(:binder)
            user = create(:user)
            create(:user_binder, :binder_id => binder.id, :user_id => user.id, :role => "owner")

            # verify that the binder owner is the user we created
            owner = User.with_role(:owner, binder).first
            expect(owner.id).to eq(user.id)

            # orphan the binder
            binder.orphan

            # verify that the binder owner is still the user we created
            owner = User.with_role(:owner, binder).first
            expect(owner.id).to eq(user.id)
        end

        it "sets the binder owner to orphans@homebinder.com" do
            allow(EventService).to receive(:perform_async)
            # create the binder/user
            binder = create(:binder)
            user = create(:user)
            orphan = create(:user, :email => "orphans@homebinder.com")
            user_binder = create(:user_binder, :binder_id => binder.id, :user_id => user.id, :role => "owner")

            # verify that the binder owner is the user we created
            owner = User.with_role(:owner, binder).first
            expect(owner.id).to eq(user.id)

            # remove the owner
            user_binder.destroy

            # orphan the binder
            binder.orphan

            # verify that the binder owner is still the user we created
            owner = User.with_role(:owner, binder).first
            expect(owner.id).to eq(orphan.id)
                # analytics
            expect(EventService).to have_received(:perform_async)
        end
    end

    describe "pending_transfer" do
        it "returns false" do
            # create the binder
            binder = create(:binder)
            # check if pending transfer
            expect(binder.pending_transfer).to eq(false)
        end

        it "returns true" do
            sender = create(:user)
            receiver = create(:user)
            # create the binder and tag
            binder = create(:binder)
            binder.transfers.new(:status => "sent", :receiver_id => receiver.id, :sender_id => sender.id, :transfer_type => "ownership").save

            # check if pending transfer
            expect(binder.pending_transfer).to eq(true)
        end
    end

    describe "generate_branding" do
        it "returns an empty array" do
            # create the binder
            binder = create(:binder)
            # generate branding
            users = binder.generate_branding("test")

            expect(users.length).to eq(0)
        end

        it "returns 1 user" do
            # create the binder
            binder = create(:binder)
            user = create(:user, :user_profile_attributes => {:company => "ERA"}, :role => "agent")
            binder.binder_brandings.new(:scope => "maintenance_email", :user_branding_id => user.user_profile.id).save
            # check if pending transfer
            users = binder.generate_branding("maintenance_email")

            expect(users.length).to eq(1)
        end

        it "returns 2 user" do
            # create the binder
            binder = create(:binder)
            agent = create(:user, :user_profile_attributes => {:company => "ERA"}, :role => "agent")
            inspector = create(:user, :user_profile_attributes => {:company => "Home Inspection Company"}, :role => "inspector")
            binder.binder_brandings.new(:scope => "recall_email", :user_branding_id => agent.user_profile.id).save
            binder.binder_brandings.new(:scope => "recall_email", :user_branding_id => inspector.user_profile.id).save

            # generate branding
            users = binder.generate_branding("recall_email")
            expect(users.length).to eq(2)
            expect(users.first[:role]).to eq("inspector")
            expect(users.second[:role]).to eq("agent")
        end

        it "returns 1 user despite duplicate scopes" do
            # create the binder
            binder = create(:binder)
            inspector = create(:user, :user_profile_attributes => {:company => "Home Inspection Company"}, :role => "inspector")
            binder.binder_brandings.new(:scope => "recall_email", :user_branding_id => inspector.user_profile.id).save
            binder.binder_brandings.new(:scope => "recall_email", :user_branding_id => inspector.user_profile.id).save

            # generate branding
            users = binder.generate_branding("recall_email")

            expect(users.length).to eq(1)
            expect(users.first[:role]).to eq("inspector")
        end
    end
    
    describe "last_recall" do
        it "returns nil" do
            expect(@binder.last_recall).to eq nil
        end
        it "returns date" do
            appliance = create(:appliance, :binder_id => @binder.id)
            recall = create(:recall, :recall_date => Date.today)
            create(:appliance_recalls, :appliance => appliance, :recall => recall)
                expect(@binder.last_recall).to eq Date.today
        end
    end
    
    describe "recalls" do
        it "returns 0" do
            expect(@binder.recalls).to eq 0
        end
        it "returns 1" do
            appliance = create(:appliance, :binder_id => @binder.id)
            recall = create(:recall, :recall_date => Date.today)
            create(:appliance_recalls, :appliance => appliance, :recall => recall)
                expect(@binder.recalls).to eq 1
        end
    end
    
    describe "partner_binders" do
        it "does not the partner binder upon binder deletion" do
            binder = create(:binder)
            partner_binder = create(:partner_binder, :binder => binder)
                binder.destroy
                expect{PartnerBinder.find(partner_binder.id)}.to_not raise_error
        end
    end
    
    describe "update_binder_storage_size" do
        it "increments/decrements the binder storage size" do
            binder = create(:binder)
            create_list(:image, 5, :file_file_size => 1024, :binder_id => binder.id)
            create_list(:document, 5, :file_file_size => 1024, :binder_id => binder.id)
                binder.reload
                expect(binder.total_storage).to eq(10240)
        end
    end
end
