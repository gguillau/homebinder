require 'rails_helper'

RSpec.describe OrganizationPartner, :type => :model do
    
    it "has a valid factory" do
        organization_partner = build :organization_partner
        expect(organization_partner).to be_valid
    end
    
    it "is invalid because organization_id is required" do
        organization_partner = build(:organization_partner, organization_id: nil)
        expect(organization_partner).not_to be_valid
        expect(organization_partner.errors.full_messages.first).to eq("Organization is required")
    end
    
    it "is invalid because partner_id is required" do
        organization_partner = build(:organization_partner, partner_id: nil)
        expect(organization_partner).not_to be_valid
        expect(organization_partner.errors.full_messages.first).to eq("Partner is required")
    end
    
    it "is invalid because partner already exists in organization" do
        organization_partner = create(:organization_partner)
        expect(organization_partner).to be_valid
        organization_partner_2 = build(:organization_partner, organization_id: organization_partner.organization_id, partner_id: organization_partner.partner_id)
        expect(organization_partner_2).not_to be_valid
        expect(organization_partner_2.errors.full_messages.first).to eq("Partner is already a member")
    end
end