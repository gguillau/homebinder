require 'rails_helper'

RSpec.describe WarrantyPlan, type: :model do
    
    it "has a valid factory" do
        warranty_plan = create :warranty_plan
        expect(warranty_plan).to be_valid
    end
    
    it "requires a name" do
        warranty_plan = build(:warranty_plan, name: nil)
        expect(warranty_plan).to_not be_valid
        expect(warranty_plan.errors.full_messages.first).to eq("Name is required")
    end
    
    it "requires a valid name with less than 100 characters" do
        warranty_plan = build(:warranty_plan, name: Faker::Lorem.characters(101))
        expect(warranty_plan).to_not be_valid
        expect(warranty_plan.errors.full_messages.first).to eq("Name is too long (maximum is 100 characters)")
    end
    
    it "requires a duration" do
        warranty_plan = build(:warranty_plan, duration: nil)
        expect(warranty_plan).to_not be_valid
        expect(warranty_plan.errors.full_messages.first).to eq("Duration is required")
    end
    
    it "requires a duration" do
        warranty_plan = build(:warranty_plan, cycle: nil)
        expect(warranty_plan).to_not be_valid
        expect(warranty_plan.errors.full_messages.first).to eq("Cycle is required")
    end
    
    it "requires a valid duration with less than 10 characters" do
        warranty_plan = build(:warranty_plan, cycle: Faker::Lorem.characters(11))
        expect(warranty_plan).to_not be_valid
        expect(warranty_plan.errors.full_messages.first).to eq("Cycle is too long (maximum is 10 characters)")
    end
    
    it "requires a description" do
        warranty_plan = build(:warranty_plan, description: nil)
        expect(warranty_plan).to_not be_valid
        expect(warranty_plan.errors.full_messages.first).to eq("Description is required")
    end
    
    it "requires a valid email with less than 1000 characters" do
        warranty_plan = build(:warranty_plan, description: Faker::Lorem.characters(1001))
        expect(warranty_plan).to_not be_valid
        expect(warranty_plan.errors.full_messages.first).to eq("Description is too long (maximum is 1000 characters)")
    end
    
    it "requires a price" do
        warranty_plan = build(:warranty_plan, price: nil)
        expect(warranty_plan).to_not be_valid
        expect(warranty_plan.errors.full_messages.first).to eq("Price is required")
    end
    
    it "requires a warranty_company_id" do
        warranty_plan = build(:warranty_plan, warranty_company_id: nil)
        expect(warranty_plan).to_not be_valid
        expect(warranty_plan.errors.full_messages.first).to eq("Warranty company is required")
    end
    
    it "requires unique plan name per company" do
        warranty_plan = create :warranty_plan
        expect(warranty_plan).to be_valid
        warranty_plan_2 = build :warranty_plan, :name => warranty_plan.name, :warranty_company_id => warranty_plan.warranty_company_id
        expect(warranty_plan_2).to_not be_valid
        expect(warranty_plan_2.errors.full_messages.first).to eq("Name has already been taken")
    end
end
