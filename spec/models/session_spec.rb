require 'rails_helper'

RSpec.describe Session, :type => :model do

    it "is invalid because user_id is required" do
        session = build(:session, user_id: nil)
        expect(session).not_to be_valid
        expect(session.errors.full_messages.first).to eq("User is required")
    end
    
    it "is invalid because token is required" do
        session = build(:session, token: nil)
        expect(session).not_to be_valid
        expect(session.errors.full_messages.first).to eq("Token is required")
    end
    
    it "is invalid because expires_at is required" do
        session = build(:session, expires_at: nil)
        expect(session).not_to be_valid
        expect(session.errors.full_messages.first).to eq("Expires at is required")
    end
    
    describe "#pager" do
        it "searches" do
            admin = create(:user, :role => "admin")
            request = HBRequest.new.create_request(admin)
            create(:session, :user => admin)
                page = Session.pager(request, {:user_id => admin.id})
            expect(page.total).to eq(1)
        end
    end
end