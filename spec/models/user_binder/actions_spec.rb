require 'rails_helper'

RSpec.describe UserBinder::Actions, :type => :model do

    before :each do
        @user = create(:user)
    end

    describe "find_by_access_token" do
        it "raises a BadRequestException when params[:id] is nil" do
            params = {}
            expect{UserBinder.find_by_access_token(@user, params)}.to raise_error BadRequestException
        end
        
        it "raises a BadRequestException when user_binder is nil" do
            params = {:id => "access_token"}
            expect{UserBinder.find_by_access_token(@user, params)}.to raise_error BadRequestException
        end
        
        it "finds the user_binder by access_token" do
            user_binder = create(:user_binder, :repair_pricer_token => "access_token")
            params = {:id => "access_token"}
            result = UserBinder.find_by_access_token(@user, params)
            expect(result).to eq(user_binder)
        end
    end
    
    describe "find_by_property_address" do
        it "raises a BadRequestException when params[:country] is nil" do
            params = {
                :address1 => "address",
                :city => "city",
                :state => "state"
            }
            expect{UserBinder.find_by_property_address(@user, params)}.to raise_error BadRequestException
        end

        it "raises a BadRequestException when params[:address1] is nil" do
            params = {
                :country => "country",
                :city => "city",
                :state => "state"
            }
            expect{UserBinder.find_by_property_address(@user, params)}.to raise_error BadRequestException
        end
        
        it "raises a BadRequestException when params[:city] is nil" do
            params = {
                :country => "country",
                :address1 => "address",
                :state => "state"
            }
            expect{UserBinder.find_by_property_address(@user, params)}.to raise_error BadRequestException
        end
        
        it "raises a BadRequestException when params[:state] is nil" do
            params = {
                :country => "country",
                :address1 => "address",
                :city => "city"
            }
            expect{UserBinder.find_by_property_address(@user, params)}.to raise_error BadRequestException
        end
        
        it "returns the correct results when zip is present and address is exact" do
            params = {
                :country => "US",
                :address1 => "1234 Main Street",
                :city => "city",
                :state => "state",
                :zip => "zip"
            }
            
            binder = create(:binder)
            transfer = create(:transfer, :binder => binder, :access_token => "access_token", :status => "pending")
            property = create(:property, :binder => binder, :country => "US", :address1 => "1234 Main Street", :city => "city", :state => "state", :zip => "zip")
            user_binder = create(:user_binder, :user => @user, :binder => binder, :role => "owner")
            
            results = UserBinder.find_by_property_address(@user, params)
            owner = {:email => @user.email, :phone => @user.user_profile.mobile_phone}
            expect(results).to eq([{:owner => owner, :access_token => "access_token", :transfer_status => "pending"}])
        end
        
        it "returns the correct results when zip is not present and address is exact" do
            params = {
                :country => "US",
                :address1 => "1234 Main Street",
                :city => "city",
                :state => "state",
                :zip => "zip"
            }
            
            binder = create(:binder)
            transfer = create(:transfer, :binder => binder, :access_token => "access_token", :status => "pending")
            property = create(:property, :binder => binder, :country => "US", :address1 => "1234 Main Street", :city => "city", :state => "state")
            user_binder = create(:user_binder, :user => @user, :binder => binder, :role => "owner")
            
            results = UserBinder.find_by_property_address(@user, params)
            owner = {:email => @user.email, :phone => @user.user_profile.mobile_phone}
            expect(results).to eq([{:owner => owner, :access_token => "access_token", :transfer_status => "pending"}])
        end
        
        it "returns the correct results when zip is present and address is not exact" do
            params = {
                :country => "US",
                :address1 => "1234 Main",
                :city => "city",
                :state => "state",
                :zip => "zip"
            }
            
            binder = create(:binder)
            transfer = create(:transfer, :binder => binder, :access_token => "access_token", :status => "pending")
            property = create(:property, :binder => binder, :country => "US", :address1 => "1234 Main Street", :city => "city", :state => "state", :zip => "zip")
            user_binder = create(:user_binder, :user => @user, :binder => binder, :role => "owner")
            
            results = UserBinder.find_by_property_address(@user, params)
            owner = {:email => @user.email, :phone => @user.user_profile.mobile_phone}
            expect(results).to eq([{:owner => owner, :access_token => "access_token", :transfer_status => "pending"}])
        end
        
        it "returns the correct results when zip is not present and address is not exact" do
            params = {
                :country => "US",
                :address1 => "1234 Main",
                :city => "city",
                :state => "state",
                :zip => "zip"
            }
            
            binder = create(:binder)
            transfer = create(:transfer, :binder => binder, :access_token => "access_token", :status => "pending")
            property = create(:property, :binder => binder, :country => "US", :address1 => "1234 Main Street", :city => "city", :state => "state")
            user_binder = create(:user_binder, :user => @user, :binder => binder, :role => "owner")
            
            results = UserBinder.find_by_property_address(@user, params)
            owner = {:email => @user.email, :phone => @user.user_profile.mobile_phone}
            expect(results).to eq([{:owner => owner, :access_token => "access_token", :transfer_status => "pending"}])
        end

    end

end
