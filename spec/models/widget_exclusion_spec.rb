require 'rails_helper'

RSpec.describe WidgetExclusion, type: :model do
    
    context "user" do
        it "is invalid because widget already excluded for user" do
            widget = create(:widget)
            widget_exclusion = create(:widget_exclusion, :widget_id => widget.id, :user_id => 1)
            expect(widget_exclusion).to be_valid
                widget_exclusion2 = build(:widget_exclusion, :widget_id => widget.id, :user_id => 1)
            expect(widget_exclusion2).not_to be_valid
            expect(widget_exclusion2.errors.full_messages.first).to eq("Widget has already been excluded")
        end
        it "is invalid because widget_category already excluded for user" do
            widget_exclusion = create(:widget_exclusion, :widget_category => "Ads", :user_id => 1)
            expect(widget_exclusion).to be_valid
                widget_exclusion2 = build(:widget_exclusion, :widget_category => "Ads", :user_id => 1)
            expect(widget_exclusion2).not_to be_valid
            expect(widget_exclusion2.errors.full_messages.first).to eq("Widget category has already been excluded")
        end
    end
    
    context "partner" do
        it "is invalid because widget already excluded for partner" do
            widget = create(:widget)
            widget_exclusion = create(:widget_exclusion, :widget_id => widget.id, :partner_id => 1)
            expect(widget_exclusion).to be_valid
                widget_exclusion2 = build(:widget_exclusion, :widget_id => widget.id, :partner_id => 1)
            expect(widget_exclusion2).not_to be_valid
            expect(widget_exclusion2.errors.full_messages.first).to eq("Widget has already been excluded")
        end
        it "is invalid because widget_category already excluded for partner" do
            widget_exclusion = create(:widget_exclusion, :widget_category => "Ads", :partner_id => 1, :user_id => nil)
            expect(widget_exclusion).to be_valid
                widget_exclusion2 = build(:widget_exclusion, :widget_category => "Ads", :partner_id => 1, :user_id => nil)
            expect(widget_exclusion2).not_to be_valid
            expect(widget_exclusion2.errors.full_messages.first).to eq("Widget category has already been excluded")
        end
    end
    
    context "organization" do
        it "is invalid because widget already excluded for organization" do
            widget = create(:widget)
            widget_exclusion = create(:widget_exclusion, :widget_id => widget.id, :organization_id => 1)
            expect(widget_exclusion).to be_valid
                widget_exclusion2 = build(:widget_exclusion, :widget_id => widget.id, :organization_id => 1)
            expect(widget_exclusion2).not_to be_valid
            expect(widget_exclusion2.errors.full_messages.first).to eq("Widget has already been excluded")
        end
        it "is invalid because widget_category already excluded for organization" do
            widget_exclusion = create(:widget_exclusion, :widget_category => "Ads", :organization_id => 1)
            expect(widget_exclusion).to be_valid
                widget_exclusion2 = build(:widget_exclusion, :widget_category => "Ads", :organization_id => 1)
            expect(widget_exclusion2).not_to be_valid
            expect(widget_exclusion2.errors.full_messages.first).to eq("Widget category has already been excluded")
        end
    end
end