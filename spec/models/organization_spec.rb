require 'rails_helper'

RSpec.describe Organization, :type => :model do
  it "has a valid factory" do
    org = create :organization
    expect(org).to be_valid
  end
  
  describe "has_many partners" do
    it "gets the partners" do
      org = create(:organization)
      partner = create(:partner)
      create(:organization_partner, organization_id: org.id, partner_id: partner.id)
      
      expect(org.organization_partners.count).to eq(1)
      expect(org.partners.count).to eq(1)
    end
  end
  
  describe "has_many users" do
    it "gets the users" do
      org = create(:organization)
      user = create(:user)
      create(:organization_user, organization_id: org.id, user_id: user.id)
      
      expect(org.organization_users.count).to eq(1)
      expect(org.users.count).to eq(1)
    end
  end
  
  describe "has_many dashboards" do
    it "gets the dashboards" do
      org = create :organization
      create(:dashboard, organization_id: org.id, system: false)
      
      expect(org.dashboards.count).to eq(1)
    end
    
    it "destroys associated dashboards on destroy" do
      org = create :organization
      create(:dashboard, organization_id: org.id, system: false)
      org.destroy
      
      expect(Dashboard.where(:organization_id => org.id).count).to eq(0)
    end
  end
  
  describe "name" do
    it "is required" do
      org = build(:organization, name: nil)
      expect(org).to_not be_valid
    end
    
    it "must be unique" do
      create(:organization, name: "org")
      org2 = build(:organization, name: "org")
      
      expect(org2).to_not be_valid
    end
    
    it "must be 250 characters or less" do
      org = build(:organization, name: Faker::Lorem.characters(251))
      
      expect(org).to_not be_valid
    end
  end
end