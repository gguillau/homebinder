require 'rails_helper'

RSpec.describe Subscription, :type => :model do
    it "has a valid factory" do
        subscription = create :subscription
        expect(subscription).to be_valid
    end
    
    describe "#is_free?" do
        it "returns false" do
            subscription = create :subscription, plan_id: "paid"
            expect(subscription.is_free?).to eq(false)
        end
        it "returns true" do
            subscription = create :subscription, plan_id: "free"
            expect(subscription.is_free?).to eq(true)
        end
    end
    
    describe "#is_paid?" do
        it "returns false" do
            subscription = create :subscription, payment_status: "failed"
            expect(subscription.is_paid?).to eq(false)
        end
        it "returns true" do
            subscription = create :subscription, payment_status: "paid"
            expect(subscription.is_paid?).to eq(true)
        end
    end
    
    describe "#is_payment_failed?" do
        it "returns false" do
            subscription = create :subscription, payment_status: "paid"
            expect(subscription.is_payment_failed?).to eq(false)
        end
        it "returns true" do
            subscription = create :subscription, payment_status: "failed"
            expect(subscription.is_payment_failed?).to eq(true)
        end
    end
    
    describe "#destroy" do
        it "calls stripe" do
            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", source: nil, delete: true})
                # mock the stripe api 
            allow(stripe_customer).to receive(:delete).and_call_original
            allow(Stripe::Customer).to receive(:retrieve).and_return(stripe_customer)
            subscription = create :subscription, payment_status: "paid", customer_id: 1
            subscription.destroy
            
            expect(stripe_customer).to have_received(:delete)
        end
        
        it "calls stripe and raises an error" do
            allow(ErrorService).to receive(:perform_async)
            
            allow(Stripe::Customer).to receive(:retrieve).and_raise(BadRequestException)
            subscription = create :subscription, payment_status: "paid", customer_id: 1
            subscription.destroy
            
            expect(ErrorService).to have_received(:perform_async)
        end
    end
end
