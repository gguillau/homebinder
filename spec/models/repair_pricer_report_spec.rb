require 'rails_helper'

RSpec.describe RepairPricerReport, type: :model do
    
    describe 'when validating' do
        let(:repair_pricer_report) { build_stubbed(:repair_pricer_report) }

        it { expect(:repair_pricer_report).to validate_length_of(:status) }
    end 
    
    describe "#full_address" do
        it "returns the correct address" do
            repair_pricer_report = create(:repair_pricer_report)
            repair_pricer_report.address.destroy
            repair_pricer_report.reload
            
            address = create(:address, :repair_pricer_report_id => repair_pricer_report.id)
            expect(repair_pricer_report.full_address).to eq(address.full_address)
        end
    end
    
    describe "#set_address" do
        it "returns the correct address" do
            repair_pricer_report = create(:repair_pricer_report)
            repair_pricer_report.address.destroy
            repair_pricer_report.reload
            address = {:address1 => "321 Summer Street", :address2 => "", :city => "Boston", :country => "US", :zip => "02210"}
            repair_pricer_report.set_address(address)
            repair_pricer_report.reload
            
            expect(repair_pricer_report.address.address1).to eq("321 Summer Street")
        end
    end
    
    describe "#process_payment" do
        it "processes the payment and sets the payment status" do
            card_processor = double("Payments::CreditCardProcessor")
            allow(card_processor).to receive(:charge_card).and_return({:charge => {:id => 1}})
        
            user = create(:user)
            create(:repair_pricer_report, :payment_status => "not_paid")

            charge = RepairPricerReport.process_payment(user.id, {:total => 100, :card => "card"})
            
            expect(charge.id).to eq(1)
        end
    end
    
    describe "#order_report" do
        before {
            mailer = double("RepairPricerMailer", :deliver_later => true)
            allow(RepairPricerMailer).to receive(:send_repair_pricer_confirmation).and_return(mailer)
            allow(RepairPricerMailer).to receive(:send_inspector_notification).and_return(mailer)
        }
        it "calls create report" do
            repair_pricer_client = RepairPricer::Client.new
            allow(RepairPricer::Client).to receive(:new).and_return(repair_pricer_client)
            allow(repair_pricer_client).to receive(:create_report)
            
            repair_pricer_report = create(:repair_pricer_report, :inspection_report => File.new("spec/assets/SampleDoc.pdf"))

            RepairPricerReports::OrderReportJob.new.perform(repair_pricer_report.id)
            
            expect(repair_pricer_client).to have_received(:create_report)
            expect(RepairPricerMailer).to have_received(:send_repair_pricer_confirmation)
            expect(RepairPricerMailer).to_not have_received(:send_inspector_notification)
        end
        
        it "calls create report and send_inspector_notification when creator is an agent" do
        
            repair_pricer_client = RepairPricer::Client.new
            allow(RepairPricer::Client).to receive(:new).and_return(repair_pricer_client)
            allow(repair_pricer_client).to receive(:create_report)
            
            agent = create(:user, :role => "agent")
            create(:partner_user, :user => agent, :role => "agent")
            partner = create(:partner)
            partner.partner_configuration.repair_pricer_pre_paid_reports = true
            partner.partner_configuration.save!
            
            repair_pricer_report = create(:repair_pricer_report, :inspection_report => File.new("spec/assets/SampleDoc.pdf"), :partner => partner, :creator => agent)

            RepairPricerReports::OrderReportJob.new.perform(repair_pricer_report.id)
            
            expect(repair_pricer_client).to have_received(:create_report)
            expect(RepairPricerMailer).to have_received(:send_repair_pricer_confirmation)
            expect(RepairPricerMailer).to have_received(:send_inspector_notification)
        end
        
        it "calls create report and send_inspector_notification when creator is a homeowner" do
            repair_pricer_client = RepairPricer::Client.new
            allow(RepairPricer::Client).to receive(:new).and_return(repair_pricer_client)
            allow(repair_pricer_client).to receive(:create_report)
            
            homeowner = create(:user, :role => "homeowner")
            partner = create(:partner)
            partner.partner_configuration.repair_pricer_pre_paid_reports = true
            partner.partner_configuration.save!
            
            repair_pricer_report = create(:repair_pricer_report, :inspection_report => File.new("spec/assets/SampleDoc.pdf"), :partner => partner, :creator => homeowner)

            RepairPricerReports::OrderReportJob.new.perform(repair_pricer_report.id)
            
            expect(repair_pricer_client).to have_received(:create_report)
            expect(RepairPricerMailer).to have_received(:send_repair_pricer_confirmation)
            expect(RepairPricerMailer).to have_received(:send_inspector_notification)
        end
        
        it "calls create report but not send_inspector_notification when creator is an admin" do
            repair_pricer_client = RepairPricer::Client.new
            allow(RepairPricer::Client).to receive(:new).and_return(repair_pricer_client)
            allow(repair_pricer_client).to receive(:create_report)
            
            admin = create(:user, :role => "admin")
            partner = create(:partner)
            partner.partner_configuration.repair_pricer_pre_paid_reports = true
            partner.partner_configuration.save!
            
            repair_pricer_report = create(:repair_pricer_report, :inspection_report => File.new("spec/assets/SampleDoc.pdf"), :partner => partner, :creator => admin)

            RepairPricerReports::OrderReportJob.new.perform(repair_pricer_report.id)
            
            expect(repair_pricer_client).to have_received(:create_report)
            expect(RepairPricerMailer).to have_received(:send_repair_pricer_confirmation)
            expect(RepairPricerMailer).to_not have_received(:send_inspector_notification)
        end
        
        it "calls create report but not send_inspector_notification when creator is an inspector" do
            repair_pricer_client = RepairPricer::Client.new
            allow(RepairPricer::Client).to receive(:new).and_return(repair_pricer_client)
            allow(repair_pricer_client).to receive(:create_report)
            
            inspector = create(:user, :role => "inspector")
            partner = create(:partner)
            partner.partner_configuration.repair_pricer_pre_paid_reports = true
            partner.partner_configuration.save!
            
            repair_pricer_report = create(:repair_pricer_report, :inspection_report => File.new("spec/assets/SampleDoc.pdf"), :partner => partner, :creator => inspector)

            RepairPricerReports::OrderReportJob.new.perform(repair_pricer_report.id)
            
            expect(repair_pricer_client).to have_received(:create_report)
            expect(RepairPricerMailer).to have_received(:send_repair_pricer_confirmation)
            expect(RepairPricerMailer).to_not have_received(:send_inspector_notification)
        end
    end
    
    describe "#set_inspection_report" do
        it "sets the inspection report to an existing document" do
            document = create(:document, :file => File.open("#{Rails.root}/spec/assets/SampleDoc.pdf", "rb"))
            repair_pricer_report = create(:repair_pricer_report)
            
            expect(repair_pricer_report.inspection_report.present?).to be(false)
            repair_pricer_report.set_inspection_report(document.id)
            
            expect(repair_pricer_report.inspection_report.present?).to be(true)
        end
    end
    
    describe "#time_left" do
        it "returns 24 hours left" do
            repair_pricer_report = create(:repair_pricer_report)
            expect(repair_pricer_report.time_left).to eq("23:59:59")
        end
        
        it "returns expired" do
            repair_pricer_report = create(:repair_pricer_report, :created_at => 25.hours.ago)
            expect(repair_pricer_report.time_left).to eq("Expired")
        end
    end
    
    describe "#as_indexed_json" do
        it "returns object as json" do
            repair_pricer_report = create(:repair_pricer_report)
            expect(repair_pricer_report.as_indexed_json["status"]).to eq(repair_pricer_report.status)
        end
    end
end
