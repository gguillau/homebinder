require 'rails_helper'

RSpec.describe Geography, :type => :model do
  
  describe "name" do
    it "is required" do
      org = build(:geography, name: nil)
      expect(org).to_not be_valid
    end
    
    it "must be unique" do
      create(:geography, name: "org")
      org2 = build(:geography, name: "org")
      
      expect(org2).to_not be_valid
    end
    
    it "must be 250 characters or less" do
      org = build(:geography, name: Faker::Lorem.characters(251))
      
      expect(org).to_not be_valid
    end
  end
end