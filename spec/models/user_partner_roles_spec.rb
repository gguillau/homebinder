require 'rails_helper'

RSpec.describe UserPartnerRoles, :type => :model do

    
    it "returns admin" do
        expect(UserPartnerRoles::ADMIN.to_s).to eq("admin")
    end
    
    it "returns member" do
        expect(UserPartnerRoles::MEMBER.to_s).to eq("member")
    end

end
