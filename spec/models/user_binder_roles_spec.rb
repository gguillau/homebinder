require 'rails_helper'

RSpec.describe UserBinderRoles, :type => :model do

    
    it "returns owner" do
        expect(UserBinderRoles::OWNER.to_s).to eq("owner")
    end
    
    it "returns co_owner" do
        expect(UserBinderRoles::CO_OWNER.to_s).to eq("co_owner")
    end
    
    it "returns reader" do
        expect(UserBinderRoles::READER.to_s).to eq("reader")
    end
    
    it "returns reader_writer" do
        expect(UserBinderRoles::READER_WRITER.to_s).to eq("reader_writer")
    end
    
    it "returns buyer_agent" do
        expect(UserBinderRoles::BUYER_AGENT.to_s).to eq("buyer_agent")
    end
    
    it "returns seller_agent" do
        expect(UserBinderRoles::SELLER_AGENT.to_s).to eq("seller_agent")
    end
    
    describe "map" do
        it "returns owner" do
            expect(UserBinderRoles.map("owner").to_s).to eq("owner")
        end
        it "returns co_owner" do
            expect(UserBinderRoles.map("co_owner").to_s).to eq("co_owner")
        end
        it "returns reader" do
            expect(UserBinderRoles.map("reader").to_s).to eq("reader")
        end
        it "returns writer" do
            expect(UserBinderRoles.map("writer").to_s).to eq("writer")
        end
        it "returns reader_writer" do
            expect(UserBinderRoles.map("reader_writer").to_s).to eq("reader_writer")
        end
        it "raises an error" do
            expect{UserBinderRoles.map("partner").to_s}.to raise_exception BadRequestException
        end
        it "returns buyer_agent" do
            expect(UserBinderRoles.map("buyer_agent").to_s).to eq("buyer_agent")
        end
        it "returns seller_agent" do
            expect(UserBinderRoles.map("seller_agent").to_s).to eq("seller_agent")
        end
    end


end
