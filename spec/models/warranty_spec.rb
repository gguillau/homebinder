require 'rails_helper'

RSpec.describe Warranty, type: :model do
    
    it "has a valid factory" do
        warranty = create :warranty
        expect(warranty).to be_valid
    end
    
    it "requires a status" do
        warranty = build(:warranty, status: nil)
        expect(warranty).to_not be_valid
        expect(warranty.errors.full_messages.first).to eq("Status is required")
    end
    
    it "requires a valid status with less than 20 characters" do
        warranty = build(:warranty, status: Faker::Lorem.characters(21))
        expect(warranty).to_not be_valid
        expect(warranty.errors.full_messages.first).to eq("Status is too long (maximum is 20 characters)")
    end
    
    it "requires a warranty_plan_id" do
        warranty = build(:warranty, warranty_plan_id: nil)
        expect(warranty).to_not be_valid
        expect(warranty.errors.full_messages.first).to eq("Warranty plan is required")
    end
    
    it "requires a binder_id" do
        warranty = build(:warranty, binder_id: nil)
        expect(warranty).to_not be_valid
        expect(warranty.errors.full_messages.first).to eq("Binder is required")
    end
    
    it "requires unique plan per binder" do
        warranty = create :warranty
        expect(warranty).to be_valid
        warranty_2 = build :warranty, :warranty_plan_id => warranty.warranty_plan_id, :binder_id => warranty.binder_id
        expect(warranty_2).to_not be_valid
        expect(warranty_2.errors.full_messages.first).to eq("Binder Error - A warranty for this binder already exists")
    end
end
