require 'rails_helper'

RSpec.describe ApiKey, :type => :model do
   
    before :each do
        @api_key = create(:api_key)
    end
    
    it "has a valid factory" do
        expect(@api_key).to be_valid
    end
    
    it "is invalid because of duplicate api key" do
        api_key = build(:api_key, :key => @api_key.key)
        expect(api_key).not_to be_valid
        expect(api_key.errors.full_messages.first).to eq("API Key has already been taken")
    end

    it "is invalid because of duplicate partner" do
        api_key = build(:api_key, :key => SecureRandom.uuid.gsub('-', ''), :partner_id => @api_key.partner_id)
        expect(api_key).not_to be_valid
        expect(api_key.errors.full_messages.first).to eq("Partner already has an API key")
    end

end
