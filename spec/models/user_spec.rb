require 'rails_helper'

RSpec.describe User, :type => :model do
    it "has a valid factory" do
        user = create :user
        expect(user).to be_valid
    end
    
    it "downcases email and strips unneccessary characters" do
        email = "  \"TEST@gmAil.com  "
        user = create(:user, :email => email)
        expect(user.email).to eq("test@gmail.com")
    end
    
    describe "#add_role" do
        it "adds agent as partner user" do
            agent = create(:user, :role => "agent")
            partner = create(:partner)
            agent.add_role(:agent, partner)
                expect(agent.partners.count).to eq(1)
        end
    end
    
    describe "#has_role?" do
        it "returns true" do
            inspector = create(:user, :role => "inspector")
            partner = create(:partner)
            inspector.add_role(:partner_admin, partner)
            value = inspector.has_role?(:partner_admin, partner)
                expect(value).to be(true)
        end
    end
    
    describe "#with_role" do
        it "returns 1" do
            inspector = create(:user, :role => "inspector")
            partner = create(:partner)
            inspector.add_role(:partner_admin, partner)
                expect(User.with_role(:partner_admin, partner).count).to eq(1)
        end
        it "returns 1" do
            inspector = create(:user, :role => "inspector")
            partner = create(:partner)
            inspector.add_role(:member, partner)
                expect(User.with_role(:member, partner).count).to eq(1)
        end
        it "returns empty" do
            expect(User.with_role(:member).count).to eq(0)
        end
    end
    
    describe "#remove_role" do
        it "returns homeowner" do
            admin = create(:user, :role => "admin")
            admin.remove_role(:admin)
                expect(admin.role).to eq("homeowner")
        end
        it "removes partner admin role" do
            inspector = create(:user, :role => "inspector")
            partner = create(:partner)
            create(:partner_user, :user => inspector, :partner => partner, :role => "admin")
            expect(PartnerUser.where(:user => inspector, :partner => partner).count).to eq(1)
            inspector.remove_role(:partner_admin, partner)
                expect(PartnerUser.where(:user => inspector, :partner => partner).count).to eq(0)
        end
        it "removes partner member role" do
            inspector = create(:user, :role => "inspector")
            partner = create(:partner)
            create(:partner_user, :user => inspector, :partner => partner, :role => "member")
            expect(PartnerUser.where(:user => inspector, :partner => partner).count).to eq(1)
            inspector.remove_role(:member, partner)
                expect(PartnerUser.where(:user => inspector, :partner => partner).count).to eq(0)
        end
    end
    
    describe "#welcome_path" do
        it "returns partners" do
            inspector = create(:user, :role => "inspector")
            expect(inspector.welcome_path).to eq("partners")
        end
    end

    describe "registration" do
        it "user" do
            user = {
                :user => {
                    :email => "test@homebinder.com",
                    :password => "password",
                    :user_profile_attributes => {
                        :first_name => "First",
                        :last_name => "Last",
                    }
                }
            }
            user = ActionController::Parameters.new(user)
            user = User.register(user)

            expect(user).to_not be_nil
            expect(user.create_method).to eq("register")
            expect(user.sign_in_count).to eq(1)
            expect(user.valid_password?("password")).to_not eq(false)
        end

        it "for transfer" do
            user = User.register_for_transfer({
                :email => "test@homebinder.com",
                :user_profile_attributes => {
                    :first_name => "First",
                    :last_name => "Last",
                },
            })

            expect(user).to_not be_nil
            expect(user.create_method).to eq("transfer")
            expect(user.sign_in_count).to eq(0)
            expect(user.user_profile.monthly_email).to be(false)
        end

        it "for share" do
            user = ActionController::Parameters.new({
                :email => "test@homebinder.com",
                :user_profile_attributes => {
                    :first_name => "First",
                    :last_name => "Last",
                },
            })
            user = User.register_for_share(user)

            expect(user).to_not be_nil
            expect(user.create_method).to eq("share")
            expect(user.sign_in_count).to eq(0)
            expect(user.user_profile.monthly_email).to be(false)
        end

        it "user that has already been created for a transfer" do
            create(:user, email: "transfer@homebinder.com", create_method: "transfer")
            user = ActionController::Parameters.new({
                :user => {
                    :email => "transfer@homebinder.com",
                    :password => "password",
                    :user_profile_attributes => {
                        :first_name => "First",
                        :last_name => "Last",
                    },
                }
            })
            user = User.register(user)

            expect(user).to_not be_nil
            expect(user.sign_in_count).to eq(1)
            expect(user.valid_password?("password")).to_not eq(false)
        end

        it "user that has already been created for a share" do
            create(:user, email: "transfer@homebinder.com", create_method: "share")
            user = ActionController::Parameters.new({
                :user => {
                    :email => "transfer@homebinder.com",
                    :password => "password",
                    :user_profile_attributes => {
                        :first_name => "First",
                        :last_name => "Last",
                    },
                }
            })
            user = User.register(user)

            expect(user).to_not be_nil
            expect(user.sign_in_count).to eq(1)
            expect(user.valid_password?("password")).to_not eq(false)
        end

        it "does not register an already registered user" do
            create(:user, email: "transfer@homebinder.com", create_method: "register")

            expect{
                User.register({
                    :user => {
                        :email => "transfer@homebinder.com",
                        :password => "password",
                        :user_profile_attributes => {
                            :first_name => "First",
                            :last_name => "Last",
                        }
                    }
                })
            }.to raise_error(BadRequestException)
        end

        it "registers a user from a transfer once" do
            create(:user, email: "transfer@homebinder.com", create_method: "transfer", sign_in_count: 1)

            expect{
                User.register({
                    :user => {
                        :email => "transfer@homebinder.com",
                        :password => "password",
                        :user_profile_attributes => {
                            :first_name => "First",
                            :last_name => "Last",
                        }
                    }
                })
            }.to raise_error(BadRequestException)
        end

        it "registers a user from a share once" do
            create(:user, email: "transfer@homebinder.com", create_method: "share", sign_in_count: 1)

            expect{
                User.register({
                    :user => {
                        :email => "transfer@homebinder.com",
                        :password => "password"
                    }
                })
            }.to raise_error(BadRequestException)
        end

        it "updates pending transfers" do
            binder = create(:binder)
            sender = create(:user)

            u = create(:user, email: "cleartransfer@homebinder.com", create_method: "transfer")
            create(:user_binder, :user_id => u.id, :binder_id => binder.id, :role => "owner")
            transfer = create(:transfer, :binder_id => binder.id, :sender_id => sender.id, :receiver_id => u.id, :transfer_type => "ownership", :status => "sent")

            user = ActionController::Parameters.new({
                :user => {
                    :email => "cleartransfer@homebinder.com",
                    :password => "password",
                    :user_profile_attributes => {
                        :first_name => "First",
                        :last_name => "Last",
                    }
                }
            })
            User.register(user)

            transfer.reload

            expect(transfer.status).to eq("accepted")
        end

        it "updates pending shares" do
            binder = create(:binder)
            shared_by = create(:user)

            u = create(:user, email: "clearshare@homebinder.com", create_method: "transfer")
            create(:user_binder, :user_id => u.id, :binder_id => binder.id, :role => "owner")
            share = create(:share, :binder_id => binder.id, :shared_by_id => shared_by.id, :shared_with_id => u.id, :role_name => "co_owner", :status => "sent")

            user = ActionController::Parameters.new({
                :user => {
                    :email => "clearshare@homebinder.com",
                    :password => "password",
                    :user_profile_attributes => {
                        :first_name => "First",
                        :last_name => "Last",
                    }
                }
            })

            User.register(user)

            share.reload

            expect(share.status).to eq("shared")
        end
    end

    describe "signin" do
        it "requires an email" do
            expect{User.signin(nil, "password")}.to raise_error(BadRequestException)
        end

        it "checks if the user exists" do
            expect{User.signin("user@homebinder.com", "password")}.to raise_error(BadRequestException)
        end

        it "validates the password" do
            create(:user, email: "user@homebinder.com", password: "password", password_confirmation: "password")
            expect{User.signin("user@homebinder.com", "pwd")}.to raise_error(BadRequestException)
        end

        it "signs the user in and enables email_notifications" do
            b = create(:binder)
            create_list(:maintenance_item, 10, binder_id: b.id, email_notifications: false)

            u = create(:user, email: "user@homebinder.com", password: "password", password_confirmation: "password")
            create(:user_binder, :user_id => u.id, :binder_id => b.id, :role => "owner")

            user = User.signin("user@homebinder.com", "password")
            item = Binder::MaintenanceItem.where(:binder_id => b.id).last

            expect(user.sign_in_count).to eq(1)
            expect(item.email_notifications).to be(true)
        end

        it "signs the user in and sets monthly_email to true" do
            u = create(:user, email: "user@homebinder.com", password: "password", password_confirmation: "password", create_method: "transfer")
            u.user_profile.monthly_email = false
            u.save
            # monthly email is true by default now so we must set explicity set it to false
            expect(u.user_profile.monthly_email).to be(false)
            user = User.signin("user@homebinder.com", "password")
            expect(user.user_profile.monthly_email).to be(true)
        end
    end

    describe "opting out" do
        it "when user does not exist" do
            User.opt_out(99999)
        end

        it "of a shared binder" do
            b = create(:binder)
            sender = create(:user)
            u = create(:user, email: "user@homebinder.com", password: "password", password_confirmation: "password")
            share = create(:share, :binder_id => b.id, :shared_by_id => sender.id, :shared_with_id => u.id, :role_name => "co_owner", :status => "sent")
            create(:user_binder, :user_id => u.id, :binder_id => b.id, :role => "co_owner")

            User.opt_out(share.access_token)

            expect(u.binders.count).to eq(0)
            expect(u.has_role?(:co_owner, b)).to eq(false)
        end

        it "of a transferred binder" do
            b = create(:binder)
            sender = create(:user)

            u = create(:user, email: "user@homebinder.com", password: "password", password_confirmation: "password")
            create(:user_binder, :user_id => u.id, :binder_id => b.id, :role => "owner")
            transfer = create(:transfer, :binder_id => b.id, :sender_id => sender.id, :receiver_id => u.id, :transfer_type => "ownership", :status => "sent")

            User.opt_out(transfer.access_token)
            transfer.reload

            expect(u.binders.count).to eq(0)
            expect(u.has_role?(:owner, b)).to eq(false)
            expect(transfer.status).to eq("declined")
        end
    end

    describe "delete" do
        it "owns a binder" do
            b = create(:binder)
            u = create(:user)
            create(:user_binder, :user_id => u.id, :binder_id => b.id, :role => "owner")
            hb_request = HBRequest.new.create_request(u)
            User.destroy(hb_request, u.id)

            expect(Binder.where(:id => b.id).first).to be_nil
            expect(User.where(:id => u.id).first).to be_nil
        end

        it "co-owns a binder" do
            b = create(:binder)

            u = create(:user, email: "user@homebinder.com")
            create(:user_binder, :user_id => u.id, :binder_id => b.id, :role => "co_owner")
            hb_request = HBRequest.new.create_request(u)
            User.destroy(hb_request, u.id)

            expect(Binder.where(:id => b.id).first).to_not be_nil
            expect(User.where(:id => u.id).first).to be_nil
        end
    end
end
