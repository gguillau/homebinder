require 'rails_helper'

RSpec.describe Template::Document, :type => :model do
  before do
    @user = create(:user, :role => "inspector")
    @partner = create(:partner)
    @document_type = create(:document_type)
    @config = @partner.partner_configuration
    @template = create(:binder_template, partner_configuration_id: @config.id)
    @user.add_role :partner_admin, @partner
    @hb_request = HBRequest.new.create_request(@user)
    @no_access = create(:user)
    @no_request = HBRequest.new.create_request(@no_access)
  end
  
  describe "validation" do
    it "requires notes be less than 5001 characters" do
      doc = Template::Document.new(notes: Faker::Lorem.characters(5001))
      expect(doc).to_not be_valid
    end
    
    it "accepts notes of 5000 characters or less" do
      doc = Template::Document.new(notes: Faker::Lorem.characters(5000), document_type_id: @document_type.id, binder_template_id: @template.id)
      expect(doc).to be_valid
    end
  end
  
  describe "index" do
    it "gets document templates for a binder template" do
      file = Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleDoc.pdf'), 'application/pdf')
      Template::Document.new(binder_template_id: @template.id, file: file, :document_type_id => @document_type.id).save

      page = Template::Document.index(@hb_request, {binder_template_id: @template.id})
      
      expect(page.total).to eq(1)
    end
    
    it "raises CanCan:AccessDenied" do
      expect{Template::Document.index(@no_request, {binder_template_id: @template.id})}.to raise_error(CanCan::AccessDenied)
    end
    
    it "raises NotFound" do
      expect{Template::Document.index(@hb_request, {binder_template_id: 9999999})}.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
  
  describe "show" do
    it "gets a binder template document template" do
      file = Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleDoc.pdf'), 'application/pdf')
      doc = create(:document_template, binder_template_id: @template.id)
      
      template = Template::Document.show(@hb_request, {:id => doc.id})
      
      expect(template.id).to eq(doc.id)
    end
    
    it "raises CanCan::AccessDenied" do
      file = Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleDoc.pdf'), 'application/pdf')
      doc = create(:document_template, binder_template_id: @template.id)
      
      expect{Template::Document.show(@no_request, {:id => doc.id})}.to raise_error(CanCan::AccessDenied)
    end
    
    it "raises not found" do
      expect{Template::Document.show(@hb_request, {:id => 9999999})}.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
  
  describe "create" do
    it "creates an document template for a binder template" do
      file = Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleDoc.pdf'), 'application/pdf')
      dt = ActionController::Parameters.new({:file => file, :document_type_id => @document_type.id, binder_template_id: @template.id })
      doc = Template::Document.build(@hb_request, {:binder_template_id => @template.id, :document_template => dt})
      
      expect(doc.id).to_not be_nil
    end
    
    it "raises not found when binder template does not exist" do
      file = Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleDoc.pdf'), 'application/pdf')
      
      expect{Template::Document.build(@hb_request, {:binder_template_id => 9999999, :document_template => {:file => file}})}.to raise_error(ActiveRecord::RecordNotFound)
    end
    
    it "raises unprocessable" do
      @user.add_role :admin
      file = Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleDoc.pdf'), 'application/pdf')
      doc = ActionController::Parameters.new({:notes => Faker::Lorem.characters(5001), :file => file})
      expect{Template::Document.build(@hb_request, {:document_template => doc})}.to raise_error(UnprocessableException)
    end
  end
  
  describe "update" do
    it "updates a binder template document template" do
      file = Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleDoc.pdf'), 'application/pdf')
      doc = create(:document_template, binder_template_id: @template.id)
      docT = ActionController::Parameters.new({:file => file})
      u = Template::Document.update(@hb_request, {:id => doc.id, :document_template => docT})
      
      expect(u.id).to eq(doc.id)
    end

    it "raises not found when document template does not exist" do
      expect{Template::Document.update(@hb_request, {:id => 9999999, :document_template => { :notes => "new name" }})}.to raise_error(ActiveRecord::RecordNotFound)
    end
    
    it "raises CanCan::AccessDenied when updating a binder template document template" do
      doc = create(:document_template, binder_template_id: @template.id)
      
      expect{Template::Document.update(@no_request, {:id => doc.id, :document_template => { :notes => "new name" }})}.to raise_error(CanCan::AccessDenied)
    end
    
    it "raises unprocessable" do
      doc = create(:document_template, binder_template_id: @template.id)
      docT = ActionController::Parameters.new({ :notes => Faker::Lorem.characters(5001) })
      expect{Template::Document.update(@hb_request, {:id => doc.id, :document_template => docT})}.to raise_error(UnprocessableException)
    end
  end
  
  describe "destroy" do
    it "deletes an document template" do
      doc = create(:document_template, binder_template_id: @template.id)
      
      Template::Document.destroy(@hb_request, doc.id)
      
      expect(Template::Document.where(:id => doc.id).first).to be_nil
    end
    
    it "delete a non existing document template" do
      Template::Document.destroy(@hb_request, 9999999)
    end
    
    it "raises CanCan::AccessDenied when deleting a binder template document template" do
      doc = create(:document_template, binder_template_id: @template.id)
      
      expect{Template::Document.destroy(@no_request, doc.id)}.to raise_error(CanCan::AccessDenied)
    end
  end

end