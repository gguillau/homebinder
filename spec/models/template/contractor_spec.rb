require 'rails_helper'
require 'cancan'

RSpec.describe Template::Contractor, :type => :model do
    before do
        @admin = create(:user, :role => "admin")
        @user = create(:user, :role => "inspector")
        @partner = create(:partner)
        @config = @partner.partner_configuration
        @template = create(:binder_template, partner_configuration_id: @config.id)
        @request = HBRequest.new.create_request(@user)
    end

    describe "index" do
        it "gets contractor templates for a binder template" do
            @user.add_role :partner_admin, @partner

            create(:contractor_template, binder_template_id: @template.id)
            create(:contractor_template, binder_template_id: @template.id)

            page = Template::Contractor.index(@request, {binder_template_id: @template.id})

            expect(page.total).to eq(2)
        end

        it "raises ::CanCan:AccessDenied" do
            expect{Template::Contractor.index(@request, {binder_template_id: @template.id})}.to raise_error(CanCan::AccessDenied)
        end

        it "raises NotFound" do
            expect{Template::Contractor.index(@request, {binder_template_id: 9999999})}.to raise_error(ActiveRecord::RecordNotFound)
        end
    end

    describe "show" do
        it "gets a binder template contractor template" do
            @user.add_role :partner_admin, @partner

            a = create(:contractor_template, binder_template_id: @template.id)

            template = Template::Contractor.show(@request, {:id => a.id})

            expect(template.id).to eq(a.id)
        end

        it "raises ::CanCan::AccessDenied" do
            a = create(:contractor_template, binder_template_id: @template.id)

            expect{Template::Contractor.show(@request, {:id => a.id})}.to raise_error(::CanCan::AccessDenied)
        end

        it "raises not found" do
            expect{Template::Contractor.show(@request, {:id => 9999999})}.to raise_error(ActiveRecord::RecordNotFound)
        end
    end

    describe "create" do
        it "creates an contractor template for a binder template" do
            @user.add_role :partner_admin, @partner
            contractor = create(:contractor)
            cont = ActionController::Parameters.new({:binder_template_id => @template.id, :partner_contractor_attributes => {:partner_id => @partner.id, :name => "contractor", :contractor_id => contractor.id, :types_attributes => [{:name => SecureRandom.hex(4)}]}})
            a = Template::Contractor.build(@request, {:binder_template_id => @template.id, :contractor_template => cont })
            expect(a.id).to_not be_nil
        end

        it "creates an contractor template for the library" do
            @user.add_role :admin

            image = Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleImage.png'), 'image/png')
            contractor = create(:contractor)
            cont = ActionController::Parameters.new({:binder_template_id => @template.id, :partner_contractor_attributes => {:partner_id => @partner.id, :name => "contractor", :logo => image, :contractor_id => contractor.id, :types_attributes => [{:name => SecureRandom.hex(4)}] }})
            a = Template::Contractor.build(@request, {:contractor_template => cont})

            expect(a.id).to_not be_nil
        end

        it "prevents standard users from adding an contractor template to a binder template" do
            other = create(:user)
            request = HBRequest.new.create_request(other)
            expect{Template::Contractor.build(request, {:binder_template_id => @template.id, :contractor_template => {:name => "contractor"}})}.to raise_error(::CanCan::AccessDenied)
        end

        it "raises not found when binder template does not exist" do
            @user.add_role :partner_admin, @partner

            expect{Template::Contractor.build(@request, {:binder_template_id => 9999999, :contractor_template => {:partner_contractor_attributes => {:name => "contractor" }}})}.to raise_error(ActiveRecord::RecordNotFound)
        end

        it "raises unprocessable" do
            @user.add_role :admin

            cont = ActionController::Parameters.new({:partner_contractor_attributes => {:partner_id => @partner.id, :name => nil}})
            expect{Template::Contractor.build(@request, {:contractor_template => cont})}.to raise_error(UnprocessableException)
        end
    end

    describe "update" do
        it "updates a binder template contractor template" do
            request = HBRequest.new.create_request(@admin)
            a = create(:contractor_template, library_template: true)
            template = create(:binder_template, partner_configuration_id: @config.id)
            cont = ActionController::Parameters.new({:binder_template_id => template.id})
            u = Template::Contractor.update(request, {:id => a.id, :contractor_template => cont})

            expect(u.id).to eq(a.id)
            expect(u.binder_template_id).to eq(template.id)
        end

        it "updates a library contractor template" do
            @user.add_role :partner_admin, @partner

            a = create(:contractor_template, binder_template_id: @template.id)
            template = create(:binder_template, partner_configuration_id: @config.id)
            cont = ActionController::Parameters.new({:binder_template_id => template.id})
            u = Template::Contractor.update(@request, {:id => a.id, :contractor_template => cont})

            expect(u.id).to eq(a.id)
            expect(u.binder_template_id).to eq(template.id)
        end

        it "raises not found when contractor template does not exist" do
            @user.add_role :admin
            template = create(:binder_template, partner_configuration_id: @config.id)
            expect{Template::Contractor.update(@request, {:id => 9999999, :contractor_template => { :binder_template_id => template.id }})}.to raise_error(ActiveRecord::RecordNotFound)
        end

        it "raises ::CanCan::AccessDenied when updating a binder template contractor template" do
            a = create(:contractor_template, binder_template_id: @template.id)

            expect{Template::Contractor.update(@request, {:id => a.id, :contractor_template => { :name => "new name" }})}.to raise_error(::CanCan::AccessDenied)
        end

        it "raises ::CanCan:AccessDenied when updating a library contractor template" do
            a = create(:contractor_template, library_template: true)

            expect{Template::Contractor.update(@request, {:id => a.id, :contractor_template => { :name => "new name" }})}.to raise_error(CanCan::AccessDenied)
        end

        it "raises unprocessable" do
            @user.add_role :partner_admin, @partner
            a = create(:contractor_template, binder_template_id: @template.id)
            cont = ActionController::Parameters.new({:binder_template_id => nil})
            expect{Template::Contractor.update(@request, {:id => a.id, :contractor_template => cont})}.to raise_error(UnprocessableException)
        end
    end

    describe "destroy" do
        it "admin deletes an contractor template" do
            request = HBRequest.new.create_request(@admin)
            a = create(:contractor_template, library_template: true)

            Template::Contractor.destroy(request, a.id)

            expect(Template::Contractor.where(:id => a.id).first).to be_nil
        end

        it "delete a non existing contractor template" do
            Template::Contractor.destroy(@request, 9999999)
        end

        it "raises ::CanCan::AccessDenied when deleting a binder template contractor template" do
            a = create(:contractor_template, library_template: true)

            expect{Template::Contractor.destroy(@request, a.id)}.to raise_error(CanCan::AccessDenied)
        end

        it "raises ::CanCan::AccessDenied when deleting a library contractor template" do
            a = create(:contractor_template, binder_template_id: @template.id)

            expect{Template::Contractor.destroy(@request, a.id)}.to raise_error(CanCan::AccessDenied)
        end
    end
end
