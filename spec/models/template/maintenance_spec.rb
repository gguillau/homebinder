require 'rails_helper'

RSpec.describe Template::Maintenance, :type => :model do
    before do
        @admin = create(:user, :role => "admin")
        @user = create(:user, :role => "inspector")
        @homeowner = create(:user, :role => "homeowner")
        @partner = create(:partner)
        @config = @partner.partner_configuration
        @template = create(:binder_template, partner_configuration_id: @config.id)
        @admin_request = HBRequest.new.create_request(@admin)
        @request = HBRequest.new.create_request(@user)
    end

    describe "validation" do
        it "requires a name" do
            m = build(:maintenance_template, name: nil)
            expect(m).to_not be_valid
        end

        it "requires name not be blank" do
            m = build(:maintenance_template, name: "")
            expect(m).to_not be_valid
        end

        it "requires name be less than 101 characters longs" do
            m = build(:maintenance_template, name: Faker::Lorem.characters(101))
            expect(m).to_not be_valid
        end

        it "accepts name be between 1 and 100 characters long" do
            m = build(:maintenance_template, name: Faker::Lorem.characters(100))
            expect(m).to be_valid
        end

        it "requires notes be less than 5001 characters" do
            m = build(:maintenance_template, notes: Faker::Lorem.characters(5001))
            expect(m).to_not be_valid
        end

        it "accepts notes of 5000 characters or less" do
            m = build(:maintenance_template, notes: Faker::Lorem.characters(5000))
            expect(m).to be_valid
        end

        it "requires frequency be less than 101 characters" do
            m = build(:maintenance_template, frequency: Faker::Lorem.characters(101))
            expect(m).to_not be_valid
        end

        it "accepts frequence of 100 characters or less" do
            m = build(:maintenance_template, frequency: Faker::Lorem.characters(100))
            expect(m).to be_valid
        end
    end

    describe "index" do
        it "gets maintenance templates for a binder template" do
            @user.add_role :partner_admin, @partner

            create(:maintenance_template, binder_template_id: @template.id)
            create(:maintenance_template, binder_template_id: @template.id)

            page = Template::Maintenance.index(@user, {binder_template_id: @template.id})

            expect(page.total).to eq(2)
        end

        it "raises CanCan:AccessDenied" do
            expect{Template::Maintenance.index(@request, {binder_template_id: @template.id})}.to raise_error(CanCan::AccessDenied)
        end

        it "raises NotFound" do
            expect{Template::Maintenance.index(@request, {binder_template_id: 9999999})}.to raise_error(ActiveRecord::RecordNotFound)
        end

        it "gets maintenance templates from the library" do
            create(:maintenance_template, library_template: true)
            create(:maintenance_template, library_template: true)

            page = Template::Maintenance.index(@admin_request, {})

            expect(page.total).to eq(2)
        end
    end

    describe "show" do
        it "gets a binder template maintenance template" do
            @user.add_role :partner_admin, @partner

            a = create(:maintenance_template, binder_template_id: @template.id)

            template = Template::Maintenance.show(@request, {:id => a.id})

            expect(template.id).to eq(a.id)
        end

        it "gets a library maintenance template" do
            a = create(:maintenance_template, library_template: true)

            template = Template::Maintenance.show(@admin_request, {:id => a.id})

            expect(template.id).to eq(a.id)
        end

        xit "raises CanCan::AccessDenied" do
            other = create(:user, :role => "agent")
            a = create(:maintenance_template, binder_template_id: @template.id)
            request = HBRequest.new.create_request(other)
            expect{Template::Maintenance.show(request, {:id => a.id})}.to raise_error(CanCan::AccessDenied)
        end

        xit "raises CanCan::AccessDenied when the user is a standard user" do
            other = create(:user, :role => "agent")
            request = HBRequest.new.create_request(other)
            a = create(:maintenance_template, library_template: true)

            expect{Template::Maintenance.show(request, {:id => a.id})}.to raise_error(CanCan::AccessDenied)
        end

        it "raises not found" do
            expect{Template::Maintenance.show(@request, {:id => 9999999})}.to raise_error(ActiveRecord::RecordNotFound)
        end
    end

    describe "build" do
        it "creates an maintenance template for a binder template" do
            @user.add_role :partner_admin, @partner
            maint = ActionController::Parameters.new({:name => "maintenance", :binder_template_id => @template.id})
            a = Template::Maintenance.build(@request, {:binder_template_id => @template.id, :maintenance_template => maint})

            expect(a.id).to_not be_nil
        end

        it "creates an maintenance template for the library" do
            image = Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleImage.png'), 'image/png')
            maint = ActionController::Parameters.new({:name => "maintenance", :image => image, :binder_template_id => @template.id})
            a = Template::Maintenance.build(@admin_request, {:maintenance_template => maint})

            expect(a.id).to_not be_nil
        end

        it "raises not found when binder template does not exist" do
            @user.add_role :partner_admin, @partner
            expect{Template::Maintenance.build(@request, {:binder_template_id => 9999999, :maintenance_template => {:name => "maintenance"}})}.to raise_error(ActiveRecord::RecordNotFound)
        end

        it "raises unprocessable" do
            maint = ActionController::Parameters.new({:name => nil})
            expect{Template::Maintenance.build(@request, {:maintenance_template => maint})}.to raise_error(UnprocessableException)
        end
    end

    describe "update" do
        it "updates a binder template maintenance template" do
            a = create(:maintenance_template, library_template: true)
            maint = ActionController::Parameters.new({:name => "new name"})
            u = Template::Maintenance.update(@admin_request, {:id => a.id, :maintenance_template => maint})

            expect(u.id).to eq(a.id)
            expect(u.name).to eq("new name")
        end

        it "updates a library maintenance template" do
            a = create(:maintenance_template, binder_template_id: @template.id)
            maint = ActionController::Parameters.new({:name => "new name"})
            u = Template::Maintenance.update(@admin_request, {:id => a.id, :maintenance_template => maint})

            expect(u.id).to eq(a.id)
            expect(u.name).to eq("new name")
        end

        it "raises not found when maintenance template does not exist" do
            expect{Template::Maintenance.update(@request, {:id => 9999999, :maintenance_template => { :name => "new name" }})}.to raise_error(ActiveRecord::RecordNotFound)
        end

        it "raises CanCan::AccessDenied when updating a binder template maintenance template" do
            a = create(:maintenance_template, binder_template_id: @template.id)
            expect{Template::Maintenance.update(@request, {:id => a.id, :maintenance_template => { :name => "new name" }})}.to raise_error(CanCan::AccessDenied)
        end

        it "raises unprocessable" do
            @user.add_role :partner_admin, @partner
            a = create(:maintenance_template, binder_template_id: @template.id)
            maint = ActionController::Parameters.new({:name => nil})
            expect{Template::Maintenance.update(@request, {:id => a.id, :maintenance_template => maint})}.to raise_error(UnprocessableException)
        end
    end

    describe "destroy" do
        it "deletes an maintenance template" do
            a = create(:maintenance_template, library_template: true)
            Template::Maintenance.destroy(@admin_request, a.id)
            expect(Template::Maintenance.where(:id => a.id).first).to be_nil
        end

        it "delete a non existing maintenance template" do
            Template::Maintenance.destroy(@request, 9999999)
        end

        it "raises CanCan::AccessDenied when deleting a binder template maintenance template" do
            a = create(:maintenance_template, library_template: true)
            expect{Template::Maintenance.destroy(@request, a.id)}.to raise_error(CanCan::AccessDenied)
        end

        it "raises CanCan::AccessDenied when deleting a library maintenance template" do
            a = create(:maintenance_template, binder_template_id: @template.id)
            expect{Template::Maintenance.destroy(@request, a.id)}.to raise_error(CanCan::AccessDenied)
        end
    end

    describe "send_test_notification_email" do
        it "sends a test email" do
            double = double("MaintenanceNotifyMailer", :deliver_later => true)
            allow(MaintenanceNotifyMailer).to receive(:notify_email).and_return(double)

            @user.add_role :partner_admin, @partner
            binder = create(:binder)
            @partner.add_role :binder, binder, @homeowner.id
            a = create(:maintenance_template, binder_template_id: @template.id)

            Template::Maintenance.send_test_notification_email(@request, a.id)

            expect(MaintenanceNotifyMailer).to have_received(:notify_email).exactly(1).times
        end
    end
end
