require 'rails_helper'

RSpec.describe Template::Appliance, :type => :model do
  before do
    @user = create(:user, :role => "inspector")
    @partner = create(:partner)
    @config = @partner.partner_configuration
    @template = create(:binder_template, partner_configuration_id: @config.id)
    @hb_request = HBRequest.new.create_request(@user)
    @no_access = create(:user, :role => "homeowner")
    @no_request = HBRequest.new.create_request(@no_access)
  end
  
  describe "validation" do
    it "requires a name" do
      a = build(:appliance_template, name: nil)
      expect(a).to_not be_valid
    end
    
    it "requires name not be blank" do
      a = build(:appliance_template, name: "")
      expect(a).to_not be_valid
    end
    
    it "requires name be less than 100 characters longs" do
      a = build(:appliance_template, name: Faker::Lorem.characters(101))
      expect(a).to_not be_valid
    end
    
    it "accepts name be between 1 and 100 characters long" do
      a = build(:appliance_template, name: Faker::Lorem.characters(100))
      expect(a).to be_valid
    end
    
    it "requires notes be less than 5001 characters" do
      a = build(:appliance_template, notes: Faker::Lorem.characters(5001))
      expect(a).to_not be_valid
    end
    
    it "accepts notes of 5000 characters or less" do
      a = build(:appliance_template, notes: Faker::Lorem.characters(5000))
      expect(a).to be_valid
    end
  end
  
  describe "index" do
    it "gets appliance templates for a binder template" do
      @user.add_role :partner_admin, @partner
      
      create(:appliance_template, binder_template_id: @template.id)
      create(:appliance_template, binder_template_id: @template.id)
      
      page = Template::Appliance.index(@hb_request, {binder_template_id: @template.id})
      
      expect(page.total).to eq(2)
    end
    
    it "raises CanCan:AccessDenied" do
      expect{Template::Appliance.index(@no_request, {binder_template_id: @template.id})}.to raise_error(::CanCan::AccessDenied)
    end
    
    it "raises NotFound" do
      expect{Template::Appliance.index(@hb_request, {binder_template_id: 9999999})}.to raise_error(ActiveRecord::RecordNotFound)
    end
    
    it "gets appliance templates from the library" do
      create(:appliance_template, library_template: true)
      create(:appliance_template, library_template: true)
      
      page = Template::Appliance.index(@hb_request, {})
      
      expect(page.total).to eq(2)
    end
  end
  
  describe "show"do
    it "gets a binder template appliance template" do
      @user.add_role :partner_admin, @partner
      
      a = create(:appliance_template, binder_template_id: @template.id)
      
      template = Template::Appliance.show(@hb_request, {:id => a.id})
      
      expect(template.id).to eq(a.id)
    end
    
    xit "raises CanCan::AccessDenied" do
      other = create(:user)
      a = create(:appliance_template, binder_template_id: @template.id)
      
      expect{Template::Appliance.show(@no_request, {:id => a.id})}.to raise_error(CanCan::AccessDenied)
    end
    
    xit "raises CanCan::AccessDenied when the user is a standard user" do
      other = create(:user)
      
      a = create(:appliance_template, library_template: true)
      
      expect{Template::Appliance.show(@no_request, {:id => a.id})}.to raise_error(CanCan::AccessDenied)
    end
    
    it "raises not found" do
      expect{Template::Appliance.show(@hb_request, {:id => 9999999})}.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
  
  describe "build" do
    it "creates an appliance template for a binder template" do
      @user.add_role :partner_admin, @partner
      template = ActionController::Parameters.new({:name => "appliance", :binder_template_id => @template.id})
      a = Template::Appliance.build(@hb_request, {:binder_template_id => @template.id, :appliance_template => template})
      
      expect(a.id).to_not be_nil
    end
    
    it "creates an appliance template for the library" do
      @user.add_role :admin
      
      image = Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleImage.png'), 'image/png')
      template = ActionController::Parameters.new({:name => "appliance", :image => image, :binder_template_id => @template.id})
      a = Template::Appliance.build(@hb_request, {:appliance_template => template})
      
      expect(a.id).to_not be_nil
    end

    it "raises not found when binder template does not exist" do
      @user.add_role :partner_admin, @partner
      
      expect{Template::Appliance.build(@hb_request, {:binder_template_id => 9999999, :appliance_template => {:name => "appliance"}})}.to raise_error(ActiveRecord::RecordNotFound)
    end
    
    it "raises unprocessable" do
      @user.add_role :admin
      template = ActionController::Parameters.new({:name => nil})
      expect{Template::Appliance.build(@hb_request, {:appliance_template => template})}.to raise_error(UnprocessableException)
    end
  end
  
  describe "update" do
    it "updates a library appliance template" do
      @user.add_role :partner_admin, @partner
      
      a = create(:appliance_template, binder_template_id: @template.id)
      template = ActionController::Parameters.new({:name => "new name"})
      u = Template::Appliance.update(@hb_request, {:id => a.id, :appliance_template => template})
      
      expect(u.id).to eq(a.id)
      expect(u.name).to eq("new name")
    end
    
    it "raises not found when appliance template does not exist" do
      @user.add_role :admin
      
      expect{Template::Appliance.update(@hb_request, {:id => 9999999, :appliance_template => { :name => "new name" }})}.to raise_error(ActiveRecord::RecordNotFound)
    end
    
    it "raises CanCan::AccessDenied when updating a binder template appliance template" do
      a = create(:appliance_template, binder_template_id: @template.id)
      
      expect{Template::Appliance.update(@no_request, {:id => a.id, :appliance_template => { :name => "new name" }})}.to raise_error(CanCan::AccessDenied)
    end
    
    it "raises CanCan:AccessDenied when updating a library appliance template" do
      a = create(:appliance_template, library_template: true)
      
      expect{Template::Appliance.update(@no_request, {:id => a.id, :appliance_template => { :name => "new name" }})}.to raise_error(CanCan::AccessDenied)
    end
    
    it "raises unprocessable" do
      @user.add_role :partner_admin, @partner
      
      a = create(:appliance_template, binder_template_id: @template.id)
      template = ActionController::Parameters.new({:name => nil})
      expect{Template::Appliance.update(@hb_request, {:id => a.id, :appliance_template => template})}.to raise_error(UnprocessableException)
    end
  end
  
  describe "destroy" do
    it "deletes an appliance template" do
      a = create(:appliance_template, binder_template_id: @template.id)
      @user.add_role :partner_admin, @partner
      Template::Appliance.destroy(@hb_request, a.id)
      expect(Template::Appliance.where(:id => a.id).first).to be_nil
    end
    
    it "delete a non existing appliance template" do
      Template::Appliance.destroy(@hb_request, 9999999)
    end
    
    it "raises CanCan::AccessDenied when deleting a binder template appliance template" do
      a = create(:appliance_template, library_template: true)
      
      expect{Template::Appliance.destroy(@no_request, a.id)}.to raise_error(CanCan::AccessDenied)
    end
    
    it "raises CanCan::AccessDenied when deleting a library appliance template" do
      a = create(:appliance_template, binder_template_id: @template.id)
      
      expect{Template::Appliance.destroy(@no_request, a.id)}.to raise_error(CanCan::AccessDenied)
    end
  end

end