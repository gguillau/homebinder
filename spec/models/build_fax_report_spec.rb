require 'rails_helper'

RSpec.describe BuildFaxReport, type: :model do
    
    describe 'when validating' do
        let(:build_fax_report) { build_stubbed(:build_fax_report) }

        it { expect(:build_fax_report).to validate_length_of(:status) }
        it { expect(:build_fax_report).to validate_length_of(:report) }
        it { expect(:build_fax_report).to validate_length_of(:address) }
    end 
end
