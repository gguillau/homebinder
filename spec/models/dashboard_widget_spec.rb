require 'rails_helper'

RSpec.describe DashboardWidget, :type => :model do
  
    it "has a valid factory" do
        dashboard_widget = build :dashboard_widget
        expect(dashboard_widget).to be_valid
    end
    
    it "is invalid because dashboard_id is required" do
        dashboard_widget = build(:dashboard_widget, dashboard_id: nil)
        expect(dashboard_widget).not_to be_valid
        expect(dashboard_widget.errors.full_messages.first).to eq("Dashboard is required")
    end
    
    it "is invalid because widget_id is required" do
        dashboard_widget = build(:dashboard_widget, widget_id: nil)
        expect(dashboard_widget).not_to be_valid
        expect(dashboard_widget.errors.full_messages.first).to eq("Widget is required")
    end
    
    it "is invalid because widget already exists in dashboard" do
        dashboard_widget = create(:dashboard_widget)
        expect(dashboard_widget).to be_valid
        dashboard_widget2 = build(:dashboard_widget, dashboard_id: dashboard_widget.dashboard_id, widget_id: dashboard_widget.widget_id)
        expect(dashboard_widget2).not_to be_valid
        expect(dashboard_widget2.errors.full_messages.first).to eq("Widget already exists on the dashboard")
    end

end