require 'rails_helper'

RSpec.describe PartnerTypes, :type => :model do

    
    it "returns partner" do
        expect(PartnerTypes::PARTNER.to_s).to eq("partner")
    end
    
    it "returns inspector" do
        expect(PartnerTypes::INSPECTOR.to_s).to eq("inspector")
    end
    
    it "returns broker" do
        expect(PartnerTypes::BROKER.to_s).to eq("broker")
    end

end
