require 'rails_helper'

RSpec.describe PartnerWidget, :type => :model do
    it "has a valid factory" do
        partner_widget = build :partner_widget
        expect(partner_widget).to be_valid
    end
    
    describe "#index" do
        before {
            user = create(:user, :role => "admin")
            @request = HBRequest.new.create_request(user)
            @partner_widget = create(:partner_widget)
            create_list(:partner_widget, 9)
        }
        it "searches by partner_id" do
            page = PartnerWidget.index(@request, {:partner_id => @partner_widget.partner_id})
            expect(page.total).to eq(1)
        end
        
        it "searches by partnerId" do
            page = PartnerWidget.index(@request, {:partnerId => @partner_widget.partner_id})
            expect(page.total).to eq(1)
        end
    end
    
    describe "#add_custom_widget" do
        it "adds widget" do
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            user_binder_1 = create(:user_binder, :binder => binder, :role => "owner", :user => homeowner)
            partner = create(:partner)
            
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
            
            widget = Widget.find_by_name("Custom")
            dashboard = Dashboards::DashboardLookup.new(homeowner).for_binder({:id => binder.id})
            
            expect(dashboard.widgets.count).to eq(4)
            
            branding = create(:binder_branding, :partner => partner, :binder => binder, :scope => "binder")
            partner_widget = create(:partner_widget, :partner => partner, :widget => widget)
            
            dashboard.reload
            expect(dashboard.widgets.count).to eq(6)
        end
        
        it "does not add the widget when the scope isn't binder" do
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            user_binder_1 = create(:user_binder, :binder => binder, :role => "owner", :user => homeowner)
            partner = create(:partner)
            
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
            
            widget = Widget.find_by_name("Custom")
            dashboard = Dashboards::DashboardLookup.new(homeowner).for_binder({:id => binder.id})
            
            expect(dashboard.widgets.count).to eq(4)
            
            branding = create(:binder_branding, :partner => partner, :binder => binder, :scope => "maintenance_email")
            partner_widget = create(:partner_widget, :partner => partner, :widget => widget)
            
            dashboard.reload
            expect(dashboard.widgets.count).to eq(4)
        end
        
        it "does not add the widget when the user role isn't owner" do
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            user_binder_1 = create(:user_binder, :binder => binder, :role => "shared", :user => homeowner)
            partner = create(:partner)
            
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
            
            widget = Widget.find_by_name("Custom")
            dashboard = Dashboards::DashboardLookup.new(homeowner).for_binder({:id => binder.id})
            
            expect(dashboard.widgets.count).to eq(4)
            
            branding = create(:binder_branding, :partner => partner, :binder => binder, :scope => "maintenance_email")
            partner_widget = create(:partner_widget, :partner => partner, :widget => widget)
            
            dashboard.reload
            expect(dashboard.widgets.count).to eq(4)
        end
        
        it "does not add the widget when the widget already exists" do
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            user_binder_1 = create(:user_binder, :binder => binder, :role => "shared", :user => homeowner)
            partner = create(:partner)
            
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
            
            widget = Widget.find_by_name("Custom")
            dashboard = Dashboards::DashboardLookup.new(homeowner).for_binder({:id => binder.id})
            dashboard.widgets << widget
            
            expect(dashboard.widgets.count).to eq(5)
            
            branding = create(:binder_branding, :partner => partner, :binder => binder, :scope => "maintenance_email")
            partner_widget = create(:partner_widget, :partner => partner, :widget => widget)
            
            dashboard.reload
            expect(dashboard.widgets.count).to eq(5)
        end
    end
    
    describe "#destroy_custom_widget" do
        it "destroys the widget" do
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            user_binder_1 = create(:user_binder, :binder => binder, :role => "owner", :user => homeowner)
            partner = create(:partner)
            branding = create(:binder_branding, :partner => partner, :binder => binder, :scope => "binder")
            
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
            
            widget = Widget.find_by_name("Custom")
            partner_widget = create(:partner_widget, :partner => partner, :widget => widget)
            dashboard = Dashboards::DashboardLookup.new(homeowner).for_binder({:id => binder.id})
            
            expect(dashboard.widgets.count).to eq(6)
            partner_widget.destroy
            
            dashboard.reload
            expect(dashboard.widgets.count).to eq(5)
        end
        
        it "does not destroy the widget when the scope isn't binder" do
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            user_binder_1 = create(:user_binder, :binder => binder, :role => "owner", :user => homeowner)
            partner = create(:partner)
            branding = create(:binder_branding, :partner => partner, :binder => binder, :scope => "maintenance_email")
            
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
            
            widget = Widget.find_by_name("Custom")
            partner_widget = create(:partner_widget, :partner => partner, :widget => widget)
            dashboard = Dashboards::DashboardLookup.new(homeowner).for_binder({:id => binder.id})
            dashboard.widgets << widget
            
            expect(dashboard.widgets.count).to eq(5)
            partner_widget.destroy
            
            dashboard.reload
            expect(dashboard.widgets.count).to eq(5)
        end
        
        it "does not destroy the widget when the user role isn't owner" do
            binder = create(:binder)
            homeowner = create(:user, :role => "homeowner")
            user_binder_1 = create(:user_binder, :binder => binder, :role => "shared", :user => homeowner)
            partner = create(:partner)
            branding = create(:binder_branding, :partner => partner, :binder => binder, :scope => "binder")
            
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
            
            widget = Widget.find_by_name("Custom")
            partner_widget = create(:partner_widget, :partner => partner, :widget => widget)
            dashboard = Dashboards::DashboardLookup.new(homeowner).for_binder({:id => binder.id})
            dashboard.widgets << widget
            
            expect(dashboard.widgets.count).to eq(5)
            partner_widget.destroy
            
            dashboard.reload
            expect(dashboard.widgets.count).to eq(5)
        end
    end
end