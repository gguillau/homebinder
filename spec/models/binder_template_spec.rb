require 'rails_helper'

RSpec.describe BinderTemplate, :type => :model do
    describe "validation" do
        describe "name" do
            it "can not nil" do
                config = build(:binder_template, partner_configuration_id: 1, name: nil)
                expect(config).to_not be_valid
            end

            it "can not be an empty string" do
                config = build(:binder_template, partner_configuration_id: 1, name: "")
                expect(config).to_not be_valid
            end

            it "cannot be longer than 100 characters" do
                config = build(:binder_template, partner_configuration_id: 1, name: Faker::Lorem.characters(101))
                expect(config).to_not be_valid
            end

            it "can be 100 characters" do
                config = build(:binder_template, partner_configuration_id: 1, name: Faker::Lorem.characters(100))
                expect(config).to be_valid
            end

            it "can be 1 character" do
                config = build(:binder_template, partner_configuration_id: 1, name: Faker::Lorem.characters(1))
                expect(config).to be_valid
            end

            it "can be between 1 and 100 character" do
                config = build(:binder_template, partner_configuration_id: 1, name: Faker::Lorem.characters(50))
                expect(config).to be_valid
            end
        end
        describe "#destroy" do
            it "doesn't destroy the template" do
                partner_configuration = create(:partner_configuration)
                binder_template = create(:binder_template, partner_configuration_id: partner_configuration.id, name: Faker::Lorem.characters(1))
                partner_configuration.default_binder_template_id = binder_template.id
                partner_configuration.save!
                expect(binder_template).to be_valid
                expect(partner_configuration.default_binder_template_id).to eq(binder_template.id)

                binder_template.destroy
                expect{BinderTemplate.find(binder_template.id)}.to_not raise_error
            end
            
            it "doesn't destroy the template when template is used to create binder" do
                binder_template = create(:binder_template)
                create(:binder, :binder_template_id => binder_template.id)
                
                binder_template.destroy
                expect{BinderTemplate.find(binder_template.id)}.to_not raise_error
            end
        end
    end

    describe "actions" do
        before do
            @user = create(:user, :role => "inspector")
            @partner = create(:partner)
            @config = @partner.partner_configuration
            @template = create(:binder_template, partner_configuration_id: @config.id)
            @user.add_role :partner_admin, @partner
            @request = HBRequest.new.create_request(@user)
            @no_access = create(:user, :role => "homeowner")
            @no_request = HBRequest.new.create_request(@no_access)
        end

        describe "index" do
            it "gets the binder templates for a partner" do
                create(:binder_template, partner_configuration_id: @config.id)
                page = BinderTemplate.index(@request, {:partner_id => @partner.id})
                expect(page.total).to eq(2)
            end

            it "raises CanCan::AccessDenied" do
                expect{BinderTemplate.index(@no_request, {:partner_id => @partner.id})}.to raise_error(CanCan::AccessDenied)
            end

            it "raises NotFound" do
                expect{BinderTemplate.index(@request, {:partner_id => 9999999})}.to raise_error(NotFoundException)
            end
        end

        describe "show" do
            it "gets a template" do
                template = BinderTemplate.show(@request, {:id => @template.id})
                expect(template).to_not be_nil
            end

            it "raises CanCan::AccessDenied" do
                expect{BinderTemplate.show(@no_request, {:id => @template.id})}.to raise_error(CanCan::AccessDenied)
            end

            it "raises NotFound" do
                expect{BinderTemplate.show(@request, {:id => 9999999})}.to raise_error(ActiveRecord::RecordNotFound)
            end
        end

        describe "build" do
            it "creates a template" do
                template = ActionController::Parameters.new({ :name => "default" })
                template = BinderTemplate.build(@request, {:partner_id => @partner.id, :binder_template => template})
                expect(template).to_not be_nil
            end

            it "raises CanCan::AccessDenied" do
                expect{BinderTemplate.build(@no_request, {:partner_id => @partner.id, :binder_template => { :name => "default" }})}.to raise_error(CanCan::AccessDenied)
            end

            it "raises NotFound" do
                expect{BinderTemplate.build(@request, {:partner_id => 9999999, :binder_template => { :name => "default" }})}.to raise_error(NotFoundException)
            end
        end

        describe "update" do
            it "updates a template" do
                template = ActionController::Parameters.new({:id => @template.id, :name => "default" })
                template = BinderTemplate.update(@request, {:id => @template.id, :binder_template => template})
                expect(template).to_not be_nil
            end

            it "raises CanCan::AccessDenied" do
                expect{BinderTemplate.update(@no_request, {:id => @template.id, :binder_template => { :id => @template.id, :name => "default" }})}.to raise_error(CanCan::AccessDenied)
            end

            it "raises NotFound" do
                expect{BinderTemplate.update(@request, {:id => 9999999, :binder_template => { :id => 9999999, :name => "default" }})}.to raise_error(ActiveRecord::RecordNotFound)
            end
        end

        describe "copy" do
            it "copies a template and adds all items" do
                create_list(:appliance_template, 5, :binder_template_id => @template.id)
                create_list(:contractor_template, 5, :binder_template_id => @template.id)
                create_list(:document_template, 5, :binder_template_id => @template.id)
                create_list(:maintenance_template, 5, :binder_template_id => @template.id)

                template = BinderTemplate.copy(@request, {:id => @template.id})
                expect(template).to_not be_nil
            end
        end

        describe "destroy" do
            it "deletes a template" do
                BinderTemplate.destroy(@request, @template.id)
                expect(BinderTemplate.where(:id => @template.id).first).to be_nil
            end

            it "raises CanCan::AccessDenied" do
                expect{BinderTemplate.destroy(@no_request, @template.id)}.to raise_error(CanCan::AccessDenied)
            end

            it "ok when not found" do
                BinderTemplate.destroy(@request, 9999999)
            end
        end
    end
end
