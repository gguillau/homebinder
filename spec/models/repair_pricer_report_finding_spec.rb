require 'rails_helper'

RSpec.describe RepairPricerReportFinding, type: :model do
    
    describe 'when validating' do
        let(:repair_pricer_report_finding) { build_stubbed(:repair_pricer_report_finding) }

        it { expect(:repair_pricer_report_finding).to validate_length_of(:name) }
    end 
end
