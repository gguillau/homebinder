require 'rails_helper'

RSpec.describe PartnerUser, :type => :model do
    
    it "has a valid factory" do
        partner_user = build :partner_user
        expect(partner_user).to be_valid
    end
    
    it "is invalid because partner_id is required" do
        partner_user = build(:partner_user, partner_id: nil)
        expect(partner_user).not_to be_valid
        expect(partner_user.errors.full_messages.first).to eq("Partner is required")
    end
    
    it "is invalid because user_id is required" do
        partner_user = build(:partner_user, user_id: nil)
        expect(partner_user).not_to be_valid
        expect(partner_user.errors.full_messages.first).to eq("User is required")
    end
    
    it "is invalid because role is required" do
        partner_user = build(:partner_user, role: nil)
        expect(partner_user).not_to be_valid
        expect(partner_user.errors.full_messages.first).to eq("Role is required")
    end
    
    it "is invalid because user already a user for partner" do
        partner_user = create(:partner_user)
        expect(partner_user).to be_valid
        partner_user_2 = build(:partner_user, partner_id: partner_user.partner_id, user_id: partner_user.user_id)
        expect(partner_user_2).not_to be_valid
        expect(partner_user_2.errors.full_messages.first).to eq("User is already a partner user")
    end
    
    it "is invalid because user already a user admin for partner on create" do
        partner_user = create(:partner_user, :role => "admin")
        expect(partner_user).to be_valid
        partner_user_2 = build(:partner_user, partner_id: partner_user.partner_id, :role => "admin")
        partner_user_2.save
        expect(partner_user_2).not_to be_valid
        expect(partner_user_2.errors.full_messages.first).to eq("Role admin already exists for partner: #{partner_user.partner_id}")
    end
    
    it "is invalid because user already a user admin for partner on update" do
        partner_user = create(:partner_user, :role => "admin")
        expect(partner_user).to be_valid
        partner_user_2 = create(:partner_user, partner_id: partner_user.partner_id, :role => "member")
        expect(partner_user_2).to be_valid
        partner_user_2.role = partner_user.role
        partner_user_2.save
        expect(partner_user_2.errors.full_messages.first).to eq("Role admin already exists for partner: #{partner_user.partner_id}")
    end
    
    it "is invalid because user is a homeowner" do
        homeowner = create(:user, :role => "homeowner")
        partner_user = build(:partner_user, :user => homeowner, :role => "admin")
        expect(partner_user).to_not be_valid
    end
    
    it "is invalid because user is an agent and cannot be added as a partner user" do
        agent = create(:user, :role => "agent")
        partner_user_1 = build(:partner_user, :user => agent, :role => "member")
        expect(partner_user_1).to_not be_valid
        partner_user_2 = build(:partner_user, :user => agent, :role => "admin")
        expect(partner_user_2).to_not be_valid
    end
    
    it "is invalid because user is an admin and cannot be added as a partner user" do
        admin = create(:user, :role => "admin")
        partner_user_1 = build(:partner_user, :user => admin, :role => "member")
        expect(partner_user_1).to_not be_valid
        partner_user_2 = build(:partner_user, :user => admin, :role => "admin")
        expect(partner_user_2).to_not be_valid
    end
    
    it "is invalid because user is a broker and cannot be added as an admin to non-broker partner" do
        broker = create(:user, :role => "broker")
        partner = create(:partner, :partner_type => "inspector")
        partner_user = build(:partner_user, :user => broker, :role => "admin", :partner => partner)
        expect(partner_user).to_not be_valid
    end
    
    it "is invalid because user is a broker and cannot be added as a member to non-broker partner" do
        homeowner = create(:user, :role => "broker")
        partner = create(:partner, :partner_type => "inspector")
        partner_user = build(:partner_user, :user => homeowner, :role => "member", :partner => partner)
        expect(partner_user).to_not be_valid
    end
    
    it "is invalid because user is an inspector and cannot be added as an admin to non-inspector partner" do
        homeowner = create(:user, :role => "inspector")
        partner = create(:partner, :partner_type => "broker")
        partner_user = build(:partner_user, :user => homeowner, :role => "admin", :partner => partner)
        expect(partner_user).to_not be_valid
    end
    
    it "is invalid because user is an inspector and cannot be added as a member to non-inspector partner" do
        homeowner = create(:user, :role => "inspector")
        partner = create(:partner, :partner_type => "broker")
        partner_user = build(:partner_user, :user => homeowner, :role => "member", :partner => partner)
        expect(partner_user).to_not be_valid
    end
    
    it "does not delete the account" do
        inspector = create(:user, :role => "inspector")
        inspector2 = create(:user, :role => "inspector")
        partner = create(:partner)
        partner.partner_configuration.default_user_branding_id = inspector2.user_profile.id
        partner.save
        create(:partner_user, :partner => partner, :user => inspector, :role => "admin")
        inspector.destroy
        expect{User.find(inspector.id)}.to_not raise_error
    end
    
    describe "#pager" do
        it "searches" do
            admin = create(:user, :role => "admin")
            request = HBRequest.new.create_request(admin)
            partner = create(:partner)
            create(:partner_user, :partner => partner)
                page = PartnerUser.pager(request, {:partner_id => partner.id})
            expect(page.total).to eq(1)
        end
        
        it "searches with different case for partnerId" do
            admin = create(:user, :role => "admin")
            request = HBRequest.new.create_request(admin)
            partner = create(:partner)
            create(:partner_user, :partner => partner)
                page = PartnerUser.pager(request, {:partnerId => partner.id})
            expect(page.total).to eq(1)
        end
    end
end