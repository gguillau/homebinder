require 'rails_helper'

RSpec.describe Event, :type => :model do
    describe "#all" do
        it "calls search" do
            item = {"_source" => { "_id" => 1, "event_name" => "feedback_given" } }
            repo = EventRepository.new
            allow(repo).to receive(:search).and_return([item])
            events = repo.all({size: 10, from: 10})
            expect(events).to eq([item])
            
            event = repo.deserialize(events.first)
            expect(event.event_name).to eq "feedback_given"
        end
    end
end
