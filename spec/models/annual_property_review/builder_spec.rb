require 'rails_helper'
require 'stripe'
require "cancancan"

RSpec.describe AnnualPropertyReview::Builder, :type => :model do
    before :each do
        search = OpenStruct.new(
            results: OpenStruct.new({
                total: 1,
                response: OpenStruct.new({
                    records: []
                })
            })
        )
        allow(AnnualPropertyReview.__elasticsearch__).to receive(:search).and_return(search)
        @admin = create(:user, role: UserGlobalRoles::ADMIN)
        @partner = create(:partner, partner_type: "inspector", partner_configuration_attributes: { can_perform_apr: true, annual_property_review_cost_cents: 15000 })
        @partner_user = create(:user, role: UserGlobalRoles::INSPECTOR)
        @user = create(:user, email: "testapr@homebinder.com", role: UserGlobalRoles::HOMEOWNER)
        @request = HBRequest.new.create_request(@admin)
        card_processor = double("Payments::CreditCardProcessor")
        allow(card_processor).to receive(:charge_card).and_return({:charge => {:id => 1}})
    end

    describe "build" do
        it "creates an APR with status active" do
            token = Stripe::Token.create(
                card: {
                    number: "4242424242424242",
                    exp_month: 06,
                    exp_year: DateTime.now.year + 1,
                    cvc: 123
                }
            )

            apr = AnnualPropertyReview.build(@user, {
                annual_property_review: {
                    status: AnnualPropertyReview.statuses[:active],
                    client_first_name: "test",
                    client_last_name: "apr",
                    client_email: "testapr@homebinder.com",
                    client_phone: "555-123-4567",
                    address1: Faker::Address.street_address,
                    city: Faker::Address.city,
                    state: Faker::Address.state_abbr,
                    zip: Faker::Address.zip,
                    country: "US",
                    reviewer_company_id: @partner.id
                },
                payment: {
                    card: token,
                    amount_off: 0
                }
            })
            expect(apr).to_not be_nil
            expect(apr.amount_charged_cents).to eq(15000)
            expect(apr.stripe_charge_id).not_to be_nil
            expect(apr.payment_status_paid?).to eq(true)
        end
        it "raises an error" do
            allow(ErrorService).to receive(:perform_async)
            allow_any_instance_of(Payments::CreditCardProcessor).to receive(:charge_card).and_raise BadRequestException
            token = Stripe::Token.create(
                card: {
                    number: "4242424242424242",
                    exp_month: 06,
                    exp_year: DateTime.now.year + 1,
                    cvc: 123
                }
            )

            expect{
                AnnualPropertyReview.build(@user, {
                    annual_property_review: {
                        status: AnnualPropertyReview.statuses[:active],
                        client_first_name: "test",
                        client_last_name: "apr",
                        client_email: "testapr@homebinder.com",
                        client_phone: "555-123-4567",
                        address1: Faker::Address.street_address,
                        city: Faker::Address.city,
                        state: Faker::Address.state_abbr,
                        zip: Faker::Address.zip,
                        country: "US",
                        reviewer_company_id: @partner.id
                    },
                    payment: {
                        card: token,
                        amount_off: 0
                    }
                })
            }.to raise_error BadRequestException
            expect(ErrorService).to have_received(:perform_async)
        end
        it "raises an error" do
            allow_any_instance_of(AnnualPropertyReview).to receive(:valid?).and_return(false)
            token = Stripe::Token.create(
                card: {
                    number: "4242424242424242",
                    exp_month: 06,
                    exp_year: DateTime.now.year + 1,
                    cvc: 123
                }
            )

            expect{
                AnnualPropertyReview.build(@user, {
                    annual_property_review: {
                        status: AnnualPropertyReview.statuses[:active],
                        client_first_name: "test",
                        client_last_name: "apr",
                        client_email: "testapr@homebinder.com",
                        client_phone: "555-123-4567",
                        address1: Faker::Address.street_address,
                        city: Faker::Address.city,
                        state: Faker::Address.state_abbr,
                        zip: Faker::Address.zip,
                        country: "US",
                        reviewer_company_id: @partner.id
                    },
                    payment: {
                        card: token,
                        amount_off: 0
                    }
                })
            }.to raise_error UnprocessableException
        end
        it "raises an error" do
            allow_any_instance_of(AnnualPropertyReview).to receive(:save).and_return(false)
            token = Stripe::Token.create(
                card: {
                    number: "4242424242424242",
                    exp_month: 06,
                    exp_year: DateTime.now.year + 1,
                    cvc: 123
                }
            )

            expect{
                AnnualPropertyReview.build(@user, {
                    annual_property_review: {
                        status: AnnualPropertyReview.statuses[:active],
                        client_first_name: "test",
                        client_last_name: "apr",
                        client_email: "testapr@homebinder.com",
                        client_phone: "555-123-4567",
                        address1: Faker::Address.street_address,
                        city: Faker::Address.city,
                        state: Faker::Address.state_abbr,
                        zip: Faker::Address.zip,
                        country: "US",
                        reviewer_company_id: @partner.id
                    },
                    payment: {
                        card: token,
                        amount_off: 0
                    }
                })
            }.to raise_error UnprocessableException
        end

        it "requires credit card token" do
            expect{
                AnnualPropertyReview.build(@user, {
                    annual_property_review: {
                        status: AnnualPropertyReview.statuses[:active],
                        client_first_name: "test",
                        client_last_name: "apr",
                        client_email: "testapr@homebinder.com",
                        client_phone: "555-123-4567",
                        address1: Faker::Address.street_address,
                        city: Faker::Address.city,
                        state: Faker::Address.state_abbr,
                        zip: Faker::Address.zip,
                        country: "US",
                        reviewer_company_id: @partner.id
                    },
                    payment: {}
                })
            }.to raise_exception(BadRequestException)
        end

        it "validates initial status" do
            expect{
                AnnualPropertyReview.build(@user, {
                    annual_property_review: {
                        status: AnnualPropertyReview.statuses[:scheduled],
                        client_first_name: "test",
                        client_last_name: "apr",
                        client_email: "testapr@homebinder.com",
                        client_phone: "555-123-4567",
                        address1: Faker::Address.street_address,
                        city: Faker::Address.city,
                        state: Faker::Address.state_abbr,
                        zip: Faker::Address.zip,
                        country: "US",
                        reviewer_company_id: @partner.id
                    }
                })
            }.to raise_exception(BadRequestException)
        end
    end

    describe "update" do
        describe "active" do
            it "sets apr to active" do
                apr = create(:annual_property_review,
                             client_first_name: "first",
                             client_last_name: "last",
                             client_email: "apr@homebinder.com",
                             client_phone: "555-555-5555",
                             status: AnnualPropertyReview.statuses[:on_hold],
                             stripe_charge_id: "stripe_id",
                             reviewer_id: @partner_user.id,
                             reviewer_company_id: @partner.id)
                updated = AnnualPropertyReview.update(@partner_user, {
                    id: apr.id,
                    annual_property_review: {
                        status: "active"
                    }
                })
                expect(updated.status_active?).to eq(true)
            end
        end

        describe "scheduled" do
            it "sets scheduled for and scheduled on" do
                scheduled_for = DateTime.now
                apr = create(:annual_property_review,
                             client_first_name: "first",
                             client_last_name: "last",
                             client_email: "apr@homebinder.com",
                             client_phone: "555-555-5555",
                             status: AnnualPropertyReview.statuses[:active],
                             stripe_charge_id: "stripe_id",
                             reviewer_id: @partner_user.id,
                             reviewer_company_id: @partner.id)
                updated = AnnualPropertyReview.update(@partner_user, {
                    id: apr.id,
                    annual_property_review: {
                        status: "scheduled",
                        scheduled_for: scheduled_for.to_s
                    }
                })

                scheduled_for = scheduled_for.in_time_zone("Eastern Time (US & Canada)")

                expect(updated.status_scheduled?).to eq(true)
                expect(updated.scheduled_for.day).to eq(scheduled_for.day)
                expect(updated.scheduled_for.month).to eq(scheduled_for.month)
                expect(updated.scheduled_for.year).to eq(scheduled_for.year)
                expect(updated.scheduled_for.hour).to eq(scheduled_for.hour)
                expect(updated.scheduled_for.min).to eq(scheduled_for.min)
                expect(updated.scheduled_on.today?).to eq(true)
            end

            it "does not updated scheduled on unless scheduled for changed" do
                scheduled_for = DateTime.now
                apr = create(:annual_property_review,
                             client_first_name: "first",
                             client_last_name: "last",
                             client_email: "apr@homebinder.com",
                             client_phone: "555-555-5555",
                             status: AnnualPropertyReview.statuses[:scheduled],
                             stripe_charge_id: "stripe_id",
                             reviewer_id: @partner_user.id,
                             reviewer_company_id: @partner.id,
                             scheduled_for: scheduled_for.to_s,
                             scheduled_on: scheduled_for.prev_day)

                updated = AnnualPropertyReview.update(@partner_user, {
                    id: apr.id,
                    annual_property_review: {
                        status: "scheduled",
                        scheduled_for: scheduled_for.to_s
                    }
                })
            end

            it "updates scheduled on when scheduled for changed" do
                scheduled_for = DateTime.now

                apr = create(:annual_property_review,
                             client_first_name: "first",
                             client_last_name: "last",
                             client_email: "apr@homebinder.com",
                             client_phone: "555-555-5555",
                             status: AnnualPropertyReview.statuses[:scheduled],
                             stripe_charge_id: "stripe_id",
                             reviewer_id: @partner_user.id,
                             reviewer_company_id: @partner.id,
                             scheduled_for: DateTime.now.to_s,
                             scheduled_on: DateTime.now.prev_day.to_s)

                updated = AnnualPropertyReview.update(@partner_user, {
                    id: apr.id,
                    annual_property_review: {
                        status: "scheduled",
                        scheduled_for: scheduled_for.next_day.to_s
                    }
                })
                expect(updated.status_scheduled?).to eq(true)
                expect(updated.scheduled_on.today?).to eq(true)
            end
        end

        describe "draft" do
            it "sets apr to in draft" do
                apr = create(:annual_property_review,
                             client_first_name: "first",
                             client_last_name: "last",
                             client_email: "apr@homebinder.com",
                             client_phone: "555-555-5555",
                             status: AnnualPropertyReview.statuses[:active],
                             stripe_charge_id: "stripe_id",
                             reviewer_id: @partner_user.id,
                             reviewer_company_id: @partner.id)
                updated = AnnualPropertyReview.update(@partner_user, {
                    id: apr.id,
                    annual_property_review: {
                        status: "draft"
                    }
                })
                expect(updated.status_draft?).to eq(true)
            end
        end

        describe "UnprocessableException" do
            it "raises an error" do
                allow_any_instance_of(AnnualPropertyReview).to receive(:save).and_return(false)
                apr = create(:annual_property_review,
                             client_first_name: "first",
                             client_last_name: "last",
                             client_email: "apr@homebinder.com",
                             client_phone: "555-555-5555",
                             status: AnnualPropertyReview.statuses[:active],
                             stripe_charge_id: "stripe_id",
                             reviewer_id: @partner_user.id,
                             reviewer_company_id: @partner.id)
                expect{
                    AnnualPropertyReview.update(@partner_user, {
                        id: apr.id,
                        annual_property_review: {
                            status: "draft"
                        }
                    })
                }.to raise_error UnprocessableException
            end
        end

        describe "draft_completed" do
            it "updates to draft_completed" do
                apr = create(:annual_property_review,
                             client_first_name: "first",
                             client_last_name: "last",
                             client_email: "apr@homebinder.com",
                             client_phone: "555-555-5555",
                             status: AnnualPropertyReview.statuses[:draft],
                             stripe_charge_id: "stripe_id",
                             reviewer_id: @partner_user.id,
                             reviewer_company_id: @partner.id,
                             scheduled_for: DateTime.now,
                             scheduled_on: DateTime.now)
                service = AnnualPropertyReviews::AprPdfGenerator.new(apr.id)
                allow(AnnualPropertyReviews::AprPdfGenerator).to receive(:new).and_return(service)
                allow(service).to receive(:generate).and_return("")

                updated = AnnualPropertyReview.update(@partner_user, {
                    id: apr.id,
                    annual_property_review: {
                        status: "draft_completed"
                    }
                })

                expect(updated.status_draft_completed?).to eq(true)
                expect(service).to have_received(:generate).exactly(1).times
            end
        end

        describe "completed" do
            it "updates to completed" do
                apr = create(:annual_property_review,
                             client_first_name: "first",
                             client_last_name: "last",
                             client_email: "apr@homebinder.com",
                             client_phone: "555-555-5555",
                             status: AnnualPropertyReview.statuses[:draft_completed],
                             stripe_charge_id: "stripe_id",
                             reviewer_id: @partner_user.id,
                             reviewer_company_id: @partner.id,
                             scheduled_for: DateTime.now,
                             scheduled_on: DateTime.now)

                updated = AnnualPropertyReview.update(@partner_user, {
                    id: apr.id,
                    annual_property_review: {
                        status: "completed"
                    }
                })

                expect(updated.status_completed?).to eq(true)
                expect(updated.completed_on).to_not be_nil
            end

            it "does not allow updates once complete" do
                apr = create(:annual_property_review,
                             client_first_name: "first",
                             client_last_name: "last",
                             client_email: "apr@homebinder.com",
                             client_phone: "555-555-5555",
                             status: AnnualPropertyReview.statuses[:completed],
                             stripe_charge_id: "stripe_id",
                             reviewer_id: @partner_user.id,
                             reviewer_company_id: @partner.id,
                             scheduled_for: DateTime.now,
                             scheduled_on: DateTime.now)

                expect{
                    AnnualPropertyReview.update(@partner_user, {
                        id: apr.id,
                        annual_property_review: {
                            status: "canceled"
                        }
                    })
                }.to raise_exception(BadRequestException)
            end
        end

        describe "canceled" do
            before :each do
                allow(AnnualPropertyReviewMailer).to receive(:canceled).and_return(double("AnnualPropertyReviewMailer", :deliver_later => true))
            end

            it "updates to canceled when user is not logged in" do
                card_processor = double("Payments::CreditCardProcessor")
                allow(card_processor).to receive(:refund).and_return(Stripe::Refund.new(id: "refund_id"))

                apr = create(:annual_property_review,
                             client_first_name: "first",
                             client_last_name: "last",
                             client_email: "apr@homebinder.com",
                             client_phone: "555-555-5555",
                             amount_charged_cents: 19900,
                             status: AnnualPropertyReview.statuses[:scheduled],
                             #stripe_charge_id: "stripe_id",
                             reviewer_id: @partner_user.id,
                             reviewer_company_id: @partner.id,
                             scheduled_for: DateTime.now + 3.days,
                             scheduled_on: DateTime.now,
                             stripe_charge_id: "charge_id")

                AnnualPropertyReview.card_processor = card_processor
                updated = AnnualPropertyReview.update(nil, {
                    id: apr.id,
                    annual_property_review: {
                        status: "canceled"
                    }
                })

                expect(updated.status_canceled?).to eq(true)
                expect(card_processor).to have_received(:refund).exactly(1).times
                expect(updated.canceled_on).to_not be_nil
                expect(updated.stripe_refund_id).to eq("refund_id")
                expect(AnnualPropertyReviewMailer).to have_received(:canceled)
            end

            it "updates to canceled when user is logged in" do
                card_processor = double("Payments::CreditCardProcessor")
                allow(card_processor).to receive(:refund).and_return(Stripe::Refund.new(id: "refund_id"))

                apr = create(:annual_property_review,
                             client_first_name: "first",
                             client_last_name: "last",
                             client_email: "apr@homebinder.com",
                             client_phone: "555-555-5555",
                             amount_charged_cents: 19900,
                             status: AnnualPropertyReview.statuses[:scheduled],
                             #stripe_charge_id: "stripe_id",
                             reviewer_id: @partner_user.id,
                             reviewer_company_id: @partner.id,
                             scheduled_for: DateTime.now + 3.days,
                             scheduled_on: DateTime.now,
                             stripe_charge_id: "charge_id")

                AnnualPropertyReview.card_processor = card_processor
                updated = AnnualPropertyReview.update(@partner_user, {
                    id: apr.id,
                    annual_property_review: {
                        status: "canceled"
                    }
                })

                expect(updated.status_canceled?).to eq(true)
                expect(card_processor).to have_received(:refund).exactly(1).times
                expect(updated.canceled_on).to_not be_nil
                expect(updated.stripe_refund_id).to eq("refund_id")
                expect(AnnualPropertyReviewMailer).to have_received(:canceled)
            end

            it "updates to canceled but does not issue refund" do
                card_processor = double("Payments::CreditCardProcessor")
                allow(card_processor).to receive(:refund).and_return(Stripe::Refund.new(id: "refund_id"))

                apr = create(:annual_property_review,
                             client_first_name: "first",
                             client_last_name: "last",
                             client_email: "apr@homebinder.com",
                             client_phone: "555-555-5555",
                             amount_charged_cents: 0,
                             status: AnnualPropertyReview.statuses[:scheduled],
                             #stripe_charge_id: "stripe_id",
                             reviewer_id: @partner_user.id,
                             reviewer_company_id: @partner.id,
                             scheduled_for: DateTime.now + 3.days,
                             scheduled_on: DateTime.now,
                             stripe_charge_id: "charge_id")

                AnnualPropertyReview.card_processor = card_processor
                updated = AnnualPropertyReview.update(@user, {
                    id: apr.id,
                    annual_property_review: {
                        status: "canceled"
                    }
                })

                expect(updated.status_canceled?).to eq(true)
                expect(card_processor).to have_received(:refund).exactly(0).times
                expect(updated.canceled_on).to_not be_nil
                expect(updated.stripe_refund_id).to be_nil
            end

            it "does not allow cancel once in draft completed" do
                apr = create(:annual_property_review,
                             client_first_name: "first",
                             client_last_name: "last",
                             client_email: "apr@homebinder.com",
                             client_phone: "555-555-5555",
                             status: AnnualPropertyReview.statuses[:draft_completed],
                             #stripe_charge_id: "stripe_id",
                             reviewer_id: @partner_user.id,
                             reviewer_company_id: @partner.id,
                             scheduled_for: DateTime.now + 3.days,
                             scheduled_on: DateTime.now,
                             stripe_charge_id: "charge_id")

                expect{
                    AnnualPropertyReview.update(@user, {
                        id: apr.id,
                        annual_property_review: {
                            status: "canceled"
                        }
                    })
                }.to raise_exception(BadRequestException)
            end

            it "does not allow cancel once in complete" do
                apr = create(:annual_property_review,
                             client_first_name: "first",
                             client_last_name: "last",
                             client_email: "apr@homebinder.com",
                             client_phone: "555-555-5555",
                             status: AnnualPropertyReview.statuses[:completed],
                             #stripe_charge_id: "stripe_id",
                             reviewer_id: @partner_user.id,
                             reviewer_company_id: @partner.id,
                             scheduled_for: DateTime.now,
                             scheduled_on: DateTime.now,
                             stripe_charge_id: "charge_id")

                expect{
                    AnnualPropertyReview.update(@user, {
                        id: apr.id,
                        annual_property_review: {
                            status: "canceled"
                        }
                    })
                }.to raise_exception(BadRequestException)
            end
        end

        describe "on_hold" do
            before :each do
                allow(AnnualPropertyReviewMailer).to receive(:on_hold).and_return(double("AnnualPropertyReviewMailer", :deliver_later => true))
            end
            it "updates to on_hold via user" do
                apr = create(:annual_property_review,
                             client_first_name: "first",
                             client_last_name: "last",
                             client_email: "apr@homebinder.com",
                             client_phone: "555-555-5555",
                             status: AnnualPropertyReview.statuses[:active],
                             stripe_charge_id: "stripe_id",
                             reviewer_id: @partner_user.id,
                             reviewer_company_id: @partner.id,
                             scheduled_for: DateTime.now + 3.days,
                             scheduled_on: DateTime.now)

                updated = AnnualPropertyReview.update(@user, {
                    id: apr.id,
                    annual_property_review: {
                        status: "on_hold"
                    }
                })

                expect(updated.status_on_hold?).to eq(true)
                expect(updated.on_hold_on).to_not be_nil
                expect(updated.scheduled_for).to be_nil
                expect(updated.scheduled_on).to be_nil
                expect(AnnualPropertyReviewMailer).to have_received(:on_hold)
            end

            it "updates to on_hold via non logged in homeowner" do
                apr = create(:annual_property_review,
                             client_first_name: "first",
                             client_last_name: "last",
                             client_email: "apr@homebinder.com",
                             client_phone: "555-555-5555",
                             status: AnnualPropertyReview.statuses[:active],
                             stripe_charge_id: "stripe_id",
                             reviewer_id: @partner_user.id,
                             reviewer_company_id: @partner.id,
                             scheduled_for: DateTime.now + 3.days,
                             scheduled_on: DateTime.now)

                updated = AnnualPropertyReview.update(nil, {
                    id: apr.id,
                    annual_property_review: {
                        status: "on_hold"
                    }
                })

                expect(updated.status_on_hold?).to eq(true)
                expect(updated.on_hold_on).to_not be_nil
                expect(updated.scheduled_for).to be_nil
                expect(updated.scheduled_on).to be_nil
                expect(AnnualPropertyReviewMailer).to have_received(:on_hold)
            end
        end
    end

    describe "destroy" do
        it "raises BadRequestException with nil ID" do
            expect{AnnualPropertyReview.destroy(@request, nil)}.to raise_error BadRequestException
        end
        it "removes the apr" do
            apr = create(:annual_property_review)
            AnnualPropertyReview.destroy(@request, apr.id)
            expect{AnnualPropertyReview.find(apr.id)}.to raise_error ActiveRecord::RecordNotFound
        end
    end

    describe "message" do
        it "sends an email to homeowner" do
            allow(AnnualPropertyReviewMailer).to receive(:message_to_homeowner).and_return(double("AnnualPropertyReviewMailer", :deliver_later => true))

            apr = create(:annual_property_review,
                         client_first_name: "first",
                         client_last_name: "last",
                         client_email: "support@homebinder.com",
                         client_phone: "555-555-5555",
                         status: AnnualPropertyReview.statuses[:active],
                         stripe_charge_id: "stripe_id",
                         reviewer_id: @partner_user.id,
                         reviewer_company_id: @partner.id)

            AnnualPropertyReview.message(@user, {:id => apr.id})
            expect(AnnualPropertyReviewMailer).to have_received(:message_to_homeowner)
        end
        it "sends an email to reviewer_company" do
            allow(AnnualPropertyReviewMailer).to receive(:message_to_reviewer).and_return(double("AnnualPropertyReviewMailer", :deliver_later => true))

            apr = create(:annual_property_review,
                         client_first_name: "first",
                         client_last_name: "last",
                         client_email: "support@homebinder.com",
                         client_phone: "555-555-5555",
                         status: AnnualPropertyReview.statuses[:active],
                         stripe_charge_id: "stripe_id",
                         reviewer_id: @partner_user.id,
                         reviewer_company_id: @partner.id)

            AnnualPropertyReview.message(nil, {:id => apr.id})
            expect(AnnualPropertyReviewMailer).to have_received(:message_to_reviewer)
        end
    end

    describe "request_quote" do
        it "sends an email to home pro" do
            allow(AnnualPropertyReviewMailer).to receive(:request_quote).and_return(double("AnnualPropertyReviewMailer", :deliver_later => true))
            apr = create(:annual_property_review)
            finding = create(:annual_property_review_finding, :annual_property_review => apr)
            contractor = create(:contractor_template)

            AnnualPropertyReview.request_quote(nil, {:id => apr.id, :selected_ids => [finding.id], :recommended_homepro_id => contractor.id})
            expect(AnnualPropertyReviewMailer).to have_received(:request_quote)
        end
    end

    describe "process_payment" do
        it "returns 0 for amout charged" do
            charge = AnnualPropertyReview.process_payment(nil, {:cost => 0, :amount_off => 18900, :card => "fake"})
            expect(charge[:amount_off]).to eq(18900)
        end
    end

    describe "add_default_home_planning_items" do
        it "adds default items" do
            apr = create(:annual_property_review)
            AnnualPropertyReview.add_default_home_planning_items(apr)
            expect(apr.annual_property_review_capital_items.count).to eq(6)
        end
    end

    describe "#pager" do
        context "for_admin" do
            it "gets all aprs for admin" do
                create_list(:annual_property_review, 10)
                page = AnnualPropertyReview.pager(@request)
                expect(page.total).to eq(10)
            end
            it "gets all aprs by status for admin" do
                create_list(:annual_property_review, 10)
                create_list(:annual_property_review, 10, :status => "scheduled")
                page = AnnualPropertyReview.pager(@request, {:status => "scheduled"})
                expect(page.total).to eq(10)
            end
            it "searches" do
                create(:annual_property_review, :client_first_name => "test")
                page = AnnualPropertyReview.pager(@request, {:search => "test"})
                expect(page.total).to eq(1)
            end
        end
        context "for_company" do
            it "gets all aprs for partner" do
                partner = create(:partner)
                create_list(:annual_property_review, 10, :reviewer_company_id => partner.id)
                page = AnnualPropertyReview.pager(@request, {searchMethod: "for_company", companyId: partner.id})
                expect(page.total).to eq(10)
            end
            it "searches" do
                partner = create(:partner)
                create(:annual_property_review, :client_first_name => "test", :reviewer_company_id => partner.id)
                create_list(:annual_property_review, 10, :reviewer_company_id => partner.id)
                page = AnnualPropertyReview.pager(@request, {searchMethod: "for_company", companyId: partner.id, search: "test"})
                expect(page.total).to be > 0
            end
        end
    end
end
