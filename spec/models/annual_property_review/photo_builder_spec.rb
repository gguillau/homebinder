require 'rails_helper'
require 'stripe'
require "cancancan"

RSpec.describe AnnualPropertyReview::AnnualPropertyReviewPhotoBuilder, :type => :model do

    describe "build" do
        it "raises an error" do
            admin = create(:user, email: "testapr@homebinder.com", role: UserGlobalRoles::ADMIN)
            request = HBRequest.new.create_request(admin)
            allow_any_instance_of(AnnualPropertyReviewPhoto).to receive(:save).and_return(false)
            apr = create(:annual_property_review)
            params = {file: Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleImage.png')), annual_property_review_id: apr.id}
            expect{AnnualPropertyReviewPhoto.build(request, params)}.to raise_error UnprocessableException
        end
    end
end
