require 'rails_helper'

RSpec.describe Partner, :type => :model do
  context "when jobs are delayed" do
    
    it "has a valid factory" do
      partner = create :partner
      expect(partner).to be_valid
    end
    
    it "requires a user" do
      partner = create :partner
      expect(partner).to be_valid
      
      partner.created_at = 5.days.ago
      partner.save
      
      expect(partner).to_not be_valid
      expect(partner.errors.full_messages.first).to eq("Partner users Partner Admin is required")
      
      user = create(:user, :role => "inspector")
      create(:partner_user, :user_id => user.id, :partner_id => partner.id, :role => "admin")
      
      partner.reload
      partner.save
      expect(partner).to be_valid
    end
    
    
    it "requires a name" do
      partner = build(:partner, name: nil)
      expect(partner).to_not be_valid
      expect(partner.errors.full_messages.first).to eq("Name is required")
    end
    
    it "requires a valid name with less than 256 characters" do
      partner = build(:partner, name: Faker::Lorem.characters(256))
      expect(partner).to_not be_valid
      expect(partner.errors.full_messages.first).to eq("Name is too long (maximum is 255 characters)")
    end
    
    it "requires a code" do
      partner = build(:partner, code: nil)
      expect(partner).to_not be_valid
      expect(partner.errors.full_messages.first).to eq("Code is required")
    end
    
    it "requires a unique code" do
      partner_one = create(:partner, code: "TEST")
      partner_two = build(:partner, code: "TEST")
      expect(partner_one).to be_valid
      partner_two.save
      expect(partner_two).to_not be_valid
      expect(partner_two.errors.full_messages.first).to eq("Code is not unique")
    end
    
    it "requires a unique partner_key" do
      partner_one = create(:partner, partner_key: "test")
      partner_two = build(:partner, partner_key: "test")
      expect(partner_one).to be_valid
      partner_two.save
      expect(partner_two).to_not be_valid
      expect(partner_two.errors.full_messages.first).to eq("API Route Name has already been taken")
    end
    
    it "requires a lower case partner_key" do
      partner_one = build(:partner, partner_key: "TEST")
      partner_one.save
      expect(partner_one).to_not be_valid
      expect(partner_one.errors.full_messages.first).to eq("API Route Name TEST is invalid")
    end
    
    it "requires a valid partner type" do
      partner = build(:partner, partner_type: "test")
      expect(partner).to_not be_valid
      expect(partner.errors.full_messages.first).to eq("Partner type must be broker, inspector, property manager, builder or relocation company")
    end
    
    describe "get_admin" do
      it "returns the admin user for the partner accout" do
        partner = create(:partner)
        user = create(:user, :role => "inspector")
        create(:partner_user, partner_id: partner.id, user_id: user.id, role: "admin")
        admin = partner.get_admin
        expect(admin.email).to eq(user.email)
      end
    end
    
    describe "it has_many users" do
      it "gets the users" do
        partner = create(:partner)
        user = create(:user, :role => "inspector")
        create(:partner_user, partner_id: partner.id, user_id: user.id)
        expect(partner.partner_users.count).to eq(1)
        expect(partner.users.count).to eq(1)
      end
    end
    
    describe "it has_many dashboards" do
      it "has_many dashboards" do
        partner = create(:partner)
        create(:dashboard, partner_id: partner.id, system: false)
        expect(partner.dashboards.count).to eq(1)
      end
      
      it "destroys associated dashboards on destroy" do
        partner = create(:partner)
        create(:dashboard, partner_id: partner.id, system: false)
        partner.partner_configuration.delete
        partner.reload
        partner.destroy
        expect(Dashboard.where(:partner_id => partner.id).count).to eq(0)
      end
    end
    
    describe "it has_many binders through partner_binders" do
      it "gets partner_binders" do
        partner = create(:partner)
        binder = create(:binder)
        create(:partner_binder, partner_id: partner.id, binder_id: binder.id)
        expect(partner.partner_binders.count).to eq(1)
        expect(partner.binders.count).to eq(1)
      end
      
      it "destroys associated partner_binders when destroyed" do
        partner = create(:partner)
        binder = create(:binder)
        create(:partner_binder, partner_id: partner.id, binder_id: binder.id)
        partner.partner_configuration.delete
        partner.reload
        partner.destroy
        expect(PartnerBinder.where(:partner_id => partner.id).count).to eq(0)
      end
    end
    
    it "requires a valid email_display_name with less than 51 characters" do
      partner = build(:partner, email_display_name: Faker::Lorem.characters(51))
      expect(partner).to_not be_valid
      expect(partner.errors.full_messages.first).to eq("Email display name is too long (maximum is 50 characters)")
    end
    
    it "requires a valid code with less than 51 characters" do
      partner = build(:partner, affiliate_code: Faker::Lorem.characters(51))
      expect(partner).to_not be_valid
      expect(partner.errors.full_messages.first).to eq("Affiliate code is too long (maximum is 50 characters)")
    end
  end
  
  describe "partner_type" do
    it "updates all user types and expires tokens when partner type is changed" do
      partner = create(:partner, :partner_type => "inspector")
      partner2 = create(:partner, :partner_type => "inspector")
      inspector = create(:user, :role => "inspector")
      inspector2 = create(:user, :role => "inspector")
      agent = create(:user, :role => "agent")
      agent2 = create(:user, :role => "agent")
      
      # add the users to the partner
      create(:partner_user, :role => "admin", :user => inspector, :partner => partner)
      create(:partner_user, :role => "agent", :user => agent, :partner => partner)
      
      create(:partner_user, :role => "admin", :user => inspector2, :partner => partner2)
      create(:partner_user, :role => "agent", :user => agent2, :partner => partner2)
      
      inspector_session = create(:session, :user_id => inspector.id)
      agent_session = create(:session, :user_id => agent.id)
      inspector2_session = create(:session, :user_id => inspector2.id)
      agent2_session = create(:session, :user_id => agent2.id)
      
      partner.partner_type = "lender"
      partner.save!
      
      inspector.reload
      agent.reload
      
      # ensure the role have changed and session expired
      expect(agent.role).to eq("agent")
      expect(inspector.role).to eq("lender")
      
      expect{Session.find(inspector_session.id)}.to raise_error ActiveRecord::RecordNotFound
      expect{Session.find(agent_session.id)}.to_not raise_error
      
      partner2.reload
      inspector2.reload
      agent2.reload
      
      # ensure the update didn't affect anyone else
      expect(partner2.partner_type).to eq("inspector")
      expect(agent2.role).to eq("agent")
      expect(inspector2.role).to eq("inspector")
      
      expect{Session.find(inspector2_session.id)}.to_not raise_error
      expect{Session.find(agent2_session.id)}.to_not raise_error
    end
  end
end
