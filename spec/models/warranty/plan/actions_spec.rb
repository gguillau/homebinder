require 'rails_helper'
require "cancan/matchers"

RSpec.describe Warranty::Plan::Actions, :type => :model do

    let(:admin) { FactoryBot.create(:user, :role => "admin") }
    let(:warranty_company) {FactoryBot.create(:warranty_company)}
    let(:warranty_plan) {FactoryBot.create(:warranty_plan, :warranty_company => warranty_company)}
    let!(:warranty_configuration) {FactoryBot.create(:warranty_configuration, :warranty_company_id => warranty_company.id)}
    let(:warranty_plan) {FactoryBot.create(:warranty_plan)}
    let!(:request) {HBRequest.new.create_request(admin)}
    
    describe "build" do
        it "creates the warranty_plan" do
            hash = ActionController::Parameters.new({:name => "plan", :warranty_company_id => warranty_company.id, :duration => 10, :cycle => "days", :description => "test", :price => 10000})
            plan = WarrantyPlan.build(request, {:warranty_plan => hash})
                expect(plan).to_not be(nil)
        end
    end
    
    describe "#pager" do
        it "searches" do
            page = WarrantyPlan.pager(request, {:warranty_company_id => warranty_plan.warranty_company.id})
            expect(page.total).to eq(1)
        end
    end
    
end
