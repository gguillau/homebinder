require 'rails_helper'
require "cancan/matchers"

RSpec.describe Warranty::Company::Actions, :type => :model do

    let(:admin) { FactoryBot.create(:user, :role => "admin") }
    let!(:warranty_company) {FactoryBot.create(:warranty_company, :name => "fake")}
    let!(:request) {HBRequest.new.create_request(admin)}
    
    describe "#pager" do
        it "searches" do
            page = WarrantyCompany.pager(request, {:name => "fake"})
            expect(page.total).to eq(1)
        end
    end
    
end
