require 'rails_helper'
require 'stripe'
require "cancancan"

RSpec.describe PromoCode::Actions, :type => :model do
    
    describe "verify_promo_code" do
        it "returns 0 because promo code is nil" do
            amount_off = PromoCode.verify_promo_code(15000, nil)
            expect(amount_off).to eq(0)
        end

        it "returns 0 because promo code doesn't exist" do
            amount_off = PromoCode.verify_promo_code(15000, "Free")
            expect(amount_off).to eq(0)
        end

        it "returns 0 because prmo code has expired" do
            PromoCode.create(:name => "Free", :expiration_date => Date.yesterday)
            amount_off = PromoCode.verify_promo_code(15000, "Free")
            expect(amount_off).to eq(0)
        end

        it "returns 15000 because promo is applied" do
            PromoCode.create(:name => "Free", :percent_off => 100)
            amount_off = PromoCode.verify_promo_code(15000, "Free")
            expect(amount_off).to eq(15000)
        end

        it "returns 1500 because promo is applied" do
            PromoCode.create(:name => "10% off", :percent_off => 10)
            amount_off = PromoCode.verify_promo_code(15000, "10% off")
            expect(amount_off).to eq(1500)
        end

        it "returns 7500 because promo is applied" do
            PromoCode.create(:name => "50% off", :percent_off => 50)
            amount_off = PromoCode.verify_promo_code(15000, "50% off")
            expect(amount_off).to eq(7500)
        end

        it "returns 15000 because promo is applied" do
            PromoCode.create(:name => "Free", :amount_off => 150)
            amount_off = PromoCode.verify_promo_code(15000, "Free")
            expect(amount_off).to eq(15000)
        end

        it "returns 1500 because promo is applied" do
            PromoCode.create(:name => "$15 off", :amount_off => 15)
            amount_off = PromoCode.verify_promo_code(15000, "$15 off")
            expect(amount_off).to eq(1500)
        end

        it "returns 7500 because promo is applied" do
            PromoCode.create(:name => "$75 off", :amount_off => 75)
            amount_off = PromoCode.verify_promo_code(15000, "$75 off")
            expect(amount_off).to eq(7500)
        end
    end
end