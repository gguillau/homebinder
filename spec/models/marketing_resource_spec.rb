require 'rails_helper'

RSpec.describe MarketingResource, :type => :model do
    
    before :each do
        @marketing_resource = create(:marketing_resource)
    end
    
    it "has a valid factory" do
        expect(@marketing_resource).to be_valid
    end
    
    it "is invalid because of marketing_resource name is required" do
        marketing_resource = build(:marketing_resource, name: nil)
        expect(marketing_resource).not_to be_valid
        expect(marketing_resource.errors.full_messages.first).to eq("Name is required")
    end
    
    it "is invalid because of marketing_resource name is too long" do
        marketing_resource = build(:marketing_resource, name: Faker::Lorem.characters(101))
        expect(marketing_resource).not_to be_valid
        expect(marketing_resource.errors.full_messages.first).to eq("Name is too long (maximum is 100 characters)")
    end
    
end
