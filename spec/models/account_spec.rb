require 'rails_helper'

RSpec.describe Account, :type => :model do
  
  it "has a valid factory" do
    account = build(:account)
    expect(account).to be_valid
  end
  
  describe "account_number" do
    it "is not mass assignable" do
      account = create(:account)
      bool = account.update_attributes({ account_number: "1234567" })
      expect(bool).to eq(false)
      expect(account.errors.count).to be(1)
      expect(account.errors.full_messages.first).to eq("Account number cannot be updated")
    end
  end
  
  describe "validation" do
    it "requires manager_id" do
      account = build(:account, manager_id: nil)
      expect(account).to_not be_valid
      expect(account.errors.full_messages.first).to eq("Manager ID is required")
    end
    
    it "requires manager_id to be greater than 0" do
      account = build(:account, manager_id: 0)
      expect(account).to_not be_valid
      expect(account.errors.full_messages.first).to eq("Manager ID must be greater than 0")
    end
    
    it "requires account_status" do
      account = build(:account, account_status: nil)
      expect(account).to_not be_valid
      expect(account.errors.full_messages.first).to eq("Account status is required")
    end
    
    it "requires account_type" do
      account = build(:account, account_type: nil)
      expect(account).to_not be_valid
      expect(account.errors.full_messages.first).to eq("Account type is required")
    end
    
    it "requires account_sub_type" do
      account = build(:account, account_sub_type: nil)
      expect(account).to_not be_valid
      expect(account.errors.full_messages.first).to eq("Account sub type is required")
    end
    
    it "requires name less than 500 characters" do
      account = build(:account, name: Faker::Lorem.characters(501))
      expect(account).to_not be_valid
      expect(account.errors.full_messages.first).to eq("Account name is too long (maximum is 500 characters)")
    end
    
    it "verifies account_status value" do
      account = build(:account, account_status: "invalid")
      expect(account).to_not be_valid
      expect(account.errors.full_messages.first).to eq("Account status is invalid")
    end
    
    it "verifies account_sub_status value" do
      account = build(:account, account_sub_status: "invalid")
      expect(account).to_not be_valid
      expect(account.errors.full_messages.first).to eq("Account sub status is invalid")
    end
    
    it "verifies payment_type value" do
      account = build(:account, payment_type: "invalid")
      expect(account).to_not be_valid
      expect(account.errors.full_messages.first).to eq("Payment type is invalid")
    end
    
    it "verifies account_type" do
      account = build(:account, account_type: "invalid")
      expect(account).to_not be_valid
      expect(account.errors.full_messages.first).to eq("Account type is invalid")
    end
    
    it "verifies account_sub_type" do
      account = build(:account, account_sub_type: "invalid")
      expect(account).to_not be_valid
      expect(account.errors.full_messages.first).to eq("Account sub type is invalid")
    end
    
    it "requires trial_expiration when account is free trial" do
      account = build(:account, trial_expiration: nil)
      expect(account).to_not be_valid
      expect(account.errors.full_messages.first).to eq("Trial expiration is required")
    end
    
    it "requires payment_type for paid accounts" do
      account = build(:account,
        account_type: Account::AccountType::SINGLE,
        account_sub_type: Account::AccountSubType::PAID,
        billing_frequency: Account::BillingFrequency::ANNUAL)
      expect(account).to_not be_valid
      expect(account.errors.full_messages.first).to eq("Payment type is required")
    end
    
    it "requires billing_frequency for paid accounts" do
      account = build(:account,
        account_type: Account::AccountType::SINGLE,
        account_sub_type: Account::AccountSubType::PAID,
        payment_type: Account::PaymentType::SUBSCRIPTION)
      expect(account).to_not be_valid
      expect(account.errors.full_messages.first).to eq("Billing frequency is required")
    end
    
    it "requires billing_frequency and payment_type for paid accounts" do
      account = build(:account,
        account_type: Account::AccountType::SINGLE,
        account_sub_type: Account::AccountSubType::PAID,
        payment_type: Account::PaymentType::SUBSCRIPTION,
        billing_frequency: Account::BillingFrequency::ANNUAL)
      expect(account).to be_valid
    end
  end
  
  describe "manager" do
    it "gets the managing partner" do
      partner = create(:partner)
      account = create(:account, manager_id: partner.id)
      expect(account.manager.id).to equal(partner.id)
    end
  end
end