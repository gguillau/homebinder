require 'rails_helper'

RSpec.describe Dashboard::DashboardBuilder, :type => :model do
    before :each do
        @user = create(:user, :role => "homeowner")
        @admin = create(:user, role: UserGlobalRoles::ADMIN)
        @org = create(:organization)
        @partner = create(:partner)
        @widget1 = create(:widget)
        @widget2 = create(:widget)
        @request = HBRequest.new.create_request(@admin)
        @no_request = HBRequest.new.create_request(@user)
    end

    describe "build" do
        it "searches by partnerId" do
            partner = create(:partner)
            create(:dashboard, :partner_id => partner.id, :system => false)

            page = Dashboard.index(@request, {:partnerId => partner.id})
            expect(page.total).to eq(1)
        end
    end

    describe "build" do
        it "ensures widgets exist" do
            expect{
                Dashboard.build(@request, {
                    dashboard: {
                        user_id: @user.id,
                        name: "dashboard",
                        description: "desc"
                    },
                    widgets: [
                        {
                            widget_id: @widget1.id,
                            index: 1
                        },
                        {
                            widget_id: 999999999,
                            index: 0
                        }
                    ]
                })
            }.to raise_exception(ActiveRecord::RecordNotFound)
        end

        describe "user dashboard" do
            it "authorizes" do
                other = create(:user)
                dashboard = {
                    user_id: other.id,
                    name: "dashboard",
                    description: "desc",
                    default: true,
                    scope: DashboardScope::BINDER
                }
                dashboard = ActionController::Parameters.new(dashboard)
                expect{
                    Dashboard.build(@no_request,{
                        dashboard: dashboard,
                        widgets: [
                            {
                                widget_id: @widget1.id,
                                index: 1
                            },
                            {
                                widget_id: @widget2.id,
                                index: 0
                            }
                        ]
                    })
                }.to raise_exception(CanCan::AccessDenied)
            end

            it "success" do
                create(:dashboard, user_id: @user.id, default: true, scope: DashboardScope::BINDER, system: false)

                dashboard = {
                    user_id: @user.id,
                    name: "dashboard",
                    description: "desc",
                    default: true,
                    scope: DashboardScope::BINDER
                }
                dashboard = ActionController::Parameters.new(dashboard)

                board = Dashboard.build(@no_request, {
                    dashboard: dashboard,
                    widgets: [
                        {
                            widget_id: @widget1.id,
                            index: 1
                        },
                        {
                            widget_id: @widget2.id,
                            index: 0
                        }
                    ]
                })

                expect(board).to_not be_nil
                expect(board.user).to_not be_nil
                expect(board.name).to eq("dashboard")
                expect(board.description).to eq("desc")
                expect(board.default).to eq(true)
                expect(board.system).to eq(false)
                expect(board.widgets.count).to eq(2)
                expect(board.dashboard_widgets[0].widget_id).to eq(@widget2.id)
                expect(board.dashboard_widgets[0].index).to eq(0)
                expect(board.dashboard_widgets[1].widget_id).to eq(@widget1.id)
                expect(board.dashboard_widgets[1].index).to eq(1)
            end

            it "raises an error" do
                create(:dashboard, user_id: @user.id, default: true, scope: DashboardScope::BINDER, system: false)

                dashboard = {
                    user_id: @user.id,
                    name: "dashboard",
                    description: "desc",
                    default: true,
                    scope: DashboardScope::BINDER
                }
                dashboard = ActionController::Parameters.new(dashboard)
                allow_any_instance_of(Dashboard).to receive(:save).and_return(false)

                expect{Dashboard.build(@no_request, {
                    dashboard: dashboard,
                    widgets: [
                        {
                            widget_id: @widget1.id,
                            index: 1
                        },
                        {
                            widget_id: @widget2.id,
                            index: 0
                        }
                    ]
                })}.to raise_error UnprocessableException
            end
        end

        describe "partner dashboard" do
            it "authorizes" do
                dashboard = {
                    partner_id: @partner.id,
                    name: "dashboard",
                    description: "desc",
                    default: true,
                    scope: DashboardScope::BINDER
                }
                dashboard = ActionController::Parameters.new(dashboard)
                expect{
                    Dashboard.build(@no_request, {
                        dashboard: dashboard,
                        widgets: [
                            {
                                widget_id: @widget1.id,
                                index: 1
                            },
                            {
                                widget_id: @widget2.id,
                                index: 0
                            }
                        ]
                    })
                }.to raise_exception(CanCan::AccessDenied)
            end

            it "success" do
                @user.role = "inspector"
                @user.save
                create(:partner_user, :partner => @partner, :user => @user)
                @no_request = HBRequest.new.create_request(@user)
                create(:dashboard, partner_id: @partner.id, default: true, scope: DashboardScope::BINDER, system: false)
                dashboard = {
                    partner_id: @partner.id,
                    name: "dashboard",
                    description: "desc",
                    default: true,
                    scope: DashboardScope::BINDER
                }
                dashboard = ActionController::Parameters.new(dashboard)
                board = Dashboard.build(@no_request, {
                    dashboard: dashboard,
                    widgets: [
                        {
                            widget_id: @widget1.id,
                            index: 1
                        },
                        {
                            widget_id: @widget2.id,
                            index: 0
                        }
                    ]
                })

                expect(board).to_not be_nil
                expect(board.partner).to_not be_nil
            end
        end

        describe "organization dashboard" do
            it "authorizes" do
                dashboard = {
                    organization_id: @org.id,
                    name: "dashboard",
                    description: "desc",
                    default: true,
                    scope: DashboardScope::BINDER
                }
                dashboard = ActionController::Parameters.new(dashboard)

                expect{
                    Dashboard.build(@no_request, {
                        dashboard: dashboard,
                        widgets: [
                            {
                                widget_id: @widget1.id,
                                index: 1
                            },
                            {
                                widget_id: @widget2.id,
                                index: 0
                            }
                        ]
                    })
                }.to raise_exception(CanCan::AccessDenied)
            end

            it "success" do
                create(:organization_user, organization_id: @org.id, user_id: @user.id)
                @no_request = HBRequest.new.create_request(@user)
                create(:dashboard, organization_id: @org.id, default: true, scope: DashboardScope::BINDER, system: false)
                dashboard = {
                    organization_id: @org.id,
                    name: "dashboard",
                    description: "desc",
                    default: true,
                    scope: DashboardScope::BINDER
                }
                dashboard = ActionController::Parameters.new(dashboard)

                board = Dashboard.build(@no_request, {
                    dashboard: dashboard,
                    widgets: [
                        {
                            widget_id: @widget1.id,
                            index: 1
                        },
                        {
                            widget_id: @widget2.id,
                            index: 0
                        }
                    ]
                })

                expect(board).to_not be_nil
                expect(board.organization).to_not be_nil
            end
        end

        describe "system dashboard" do
            it "authorizes" do
                dashboard = {
                    name: "dashboard",
                    description: "desc",
                    default: true,
                    system: true,
                    scope: DashboardScope::ADMIN
                }
                dashboard = ActionController::Parameters.new(dashboard)
                expect{
                    Dashboard.build(@no_request, {
                        dashboard: dashboard,
                        widgets: [
                            {
                                widget_id: @widget1.id,
                                index: 1
                            },
                            {
                                widget_id: @widget2.id,
                                index: 0
                            }
                        ]
                    })
                }.to raise_exception(CanCan::AccessDenied)
            end

            it "success" do
                create(:dashboard, system: true, default: true, scope: DashboardScope::ADMIN)
                dashboard = {
                    name: "dashboard",
                    description: "desc",
                    default: true,
                    system: true,
                    scope: DashboardScope::ADMIN
                }
                dashboard = ActionController::Parameters.new(dashboard)

                board = Dashboard.build(@request, {
                    dashboard: dashboard,
                    widgets: [
                        {
                            widget_id: @widget1.id,
                            index: 1
                        },
                        {
                            widget_id: @widget2.id,
                            index: 0
                        }
                    ]
                })

                expect(board).to_not be_nil
            end
        end
    end

    describe "update" do
        describe "user dashboard" do
            it "updates" do
                board = create(:dashboard, user_id: @admin.id, system: false, scope: DashboardScope::BINDER)
                dashboard = {
                    id: board.id,
                    dashboard: {
                        name: "new name",
                        default: true
                    }
                }
                dashboard = ActionController::Parameters.new(dashboard)

                update = Dashboard.update(@request, dashboard)

                expect(update.name).to eq("new name")
            end

            it "raises an error" do
                board = create(:dashboard, user_id: @admin.id, system: false, scope: DashboardScope::BINDER)
                dashboard = {
                    id: board.id,
                    dashboard: {
                        name: "new name"
                    }
                }
                dashboard = ActionController::Parameters.new(dashboard)
                allow_any_instance_of(Dashboard).to receive(:update_attributes).and_return(false)

                expect{Dashboard.update(@request, dashboard)}.to raise_error UnprocessableException
            end

            it "authorizes" do
                other = create(:user)
                board = create(:dashboard, user_id: other.id, system: false, scope: DashboardScope::BINDER)
                dashboard = {
                    id: board.id,
                    dashboard: {
                        name: "new name"
                    }
                }
                dashboard = ActionController::Parameters.new(dashboard)

                expect{
                    Dashboard.update(@no_request, dashboard)
                }.to raise_exception(CanCan::AccessDenied)
            end
        end

        describe "organization dashboard" do
            it "updates" do
                create(:dashboard, organization_id: @org.id, default: true, scope: DashboardScope::BINDER, system: false)
                board = create(:dashboard, organization_id: @org.id, system: false, scope: DashboardScope::BINDER)
                create(:organization_user, organization_id: @org.id, user_id: @admin.id)
                dashboard = {
                    id: board.id,
                    dashboard: {
                        name: "new name"
                    }
                }
                dashboard = ActionController::Parameters.new(dashboard)

                update = Dashboard.update(@request, dashboard)

                expect(update.name).to eq("new name")
            end

            it "authorizes" do
                board = create(:dashboard, organization_id: @org.id, system: false, scope: DashboardScope::BINDER)
                dashboard = {
                    id: board.id,
                    dashboard: {
                        name: "new name"
                    }
                }
                dashboard = ActionController::Parameters.new(dashboard)
                expect{
                    Dashboard.update(@no_request, dashboard)
                }.to raise_exception(CanCan::AccessDenied)
            end
        end

        describe "partner dashboard" do
            it "updates" do
                @user.role = "inspector"
                @user.save
                create(:partner_user, :partner => @partner, :user => @user)
                board = create(:dashboard, partner_id: @partner.id, system: false, scope: DashboardScope::BINDER)
                dashboard = {
                    id: board.id,
                    dashboard: {
                        name: "new name"
                    }
                }
                dashboard = ActionController::Parameters.new(dashboard)

                update = Dashboard.update(@request, dashboard)

                expect(update.name).to eq("new name")
            end

            it "authorizes" do
                board = create(:dashboard, partner_id: @partner.id, system: false, scope: DashboardScope::BINDER)
                dashboard = {
                    id: board.id,
                    dashboard: {
                        name: "new name"
                    }
                }
                dashboard = ActionController::Parameters.new(dashboard)
                expect{
                    Dashboard.update(@no_request, dashboard)
                }.to raise_exception(CanCan::AccessDenied)
            end
        end

        describe "system dashboard" do
            it "updates" do
                board = create(:dashboard, system: true, scope: DashboardScope::BINDER)
                dashboard = {
                    id: board.id,
                    dashboard: {
                        name: "new name"
                    }
                }
                dashboard = ActionController::Parameters.new(dashboard)
                update = Dashboard.update(@request, dashboard)

                expect(update.name).to eq("new name")
            end

            it "authorizes" do
                board = create(:dashboard, system: true, default: true, scope: DashboardScope::BINDER)
                dashboard = {
                    id: board.id,
                    dashboard: {
                        name: "new name"
                    }
                }
                dashboard = ActionController::Parameters.new(dashboard)
                expect{
                    Dashboard.update(@no_request, dashboard)
                }.to raise_exception(CanCan::AccessDenied)
            end

            it "requires a default" do
                board = create(:dashboard, system: true, default: true, scope: DashboardScope::BINDER)
                dashboard = {
                    id: board.id,
                    dashboard: {
                        name: "new name",
                        system: true,
                        default: false,
                        scope: DashboardScope::BINDER
                    }
                }
                dashboard = ActionController::Parameters.new(dashboard)
                expect{
                    Dashboard.update(@request, dashboard)
                }.to raise_exception(BadRequestException)
            end
        end

        it "clears widgets" do
            board = create(:dashboard, user_id: @admin.id, system: false, scope: DashboardScope::BINDER)
            create(:dashboard_widget, dashboard_id: board.id, widget_id: @widget1.id)
            dashboard = {
                id: board.id,
                dashboard: {},
                widgets: []
            }
            dashboard = ActionController::Parameters.new(dashboard)
            update = Dashboard.update(@request, dashboard)

            expect(update.widgets.count).to eq(0)
        end

        it "updates, adds and removes widgets" do
            board = create(:dashboard, user_id: @admin.id, system: false, scope: DashboardScope::BINDER)
            create(:dashboard_widget, dashboard_id: board.id, widget_id: @widget1.id, index: 0)
            create(:dashboard_widget, dashboard_id: board.id, widget_id: @widget2.id, index: 1)
            dashboard = {
                id: board.id,
                dashboard: {},
                widgets: []
            }
            dashboard = ActionController::Parameters.new(dashboard)

            update = Dashboard.update(@request, dashboard)

            expect(update.widgets.count).to eq(0)
        end

        it "updates, adds and removes widgets" do
            board = create(:dashboard, user_id: @admin.id, system: false, scope: DashboardScope::BINDER)
            widget3 = create(:widget)
            dw1 = create(:dashboard_widget, dashboard_id: board.id, widget_id: @widget1.id, index: 0)
            create(:dashboard_widget, dashboard_id: board.id, widget_id: @widget2.id, index: 1)
            dashboard = {
                id: board.id,
                dashboard: {},
                widgets: [{:id => dw1.id, :index => 2 }, {:widget_id => widget3.id, :index => 0}]
            }
            dashboard = ActionController::Parameters.new(dashboard)

            update = Dashboard.update(@request, dashboard)

            expect(update.widgets.count).to eq(2)
        end
    end

    describe "destroy" do
        it "authorizes" do
            board = create(:dashboard, user_id: -1, system: false, scope: DashboardScope::BINDER)
            expect{Dashboard.destroy(@no_request, board.id)}.to raise_exception(CanCan::AccessDenied)
        end

        it "user dashboard" do
            board = create(:dashboard, user_id: @user.id, system: false, scope: DashboardScope::BINDER)
            Dashboard.destroy(@request, board.id)
            expect(Dashboard.where(id: board.id).count).to eq(0)
        end

        it "organization dasboard" do
            board = create(:dashboard, organization_id: @org.id, system: false, scope: DashboardScope::BINDER)
            create(:organization_user, organization_id: @org.id, user_id: @user.id)
            Dashboard.destroy(@request, board.id)
            expect(Dashboard.where(id: board.id).count).to eq(0)
        end

        it "partner dashboard" do
            @user.role = "inspector"
            @user.save
            create(:partner_user, :partner => @partner, :user => @user)
            board = create(:dashboard, partner_id: @partner.id, system: false, scope: DashboardScope::BINDER)
            Dashboard.destroy(@request, board.id)
            expect(Dashboard.where(id: board.id).count).to eq(0)
        end
    end

    describe "#owned_by_system" do
        context "when searching" do
            it "it gets the dashboards" do
                create_list(:dashboard, 10, :system => true, :name => "test")
                query = Dashboard.owned_by_system({search: "test"}, @request)
                expect(query.count).to eq(10)
            end
        end

        context "when not searching" do
            it "it gets the dashboards" do
                create_list(:dashboard, 10, :system => true)
                query = Dashboard.owned_by_system({}, @request)
                expect(query.count).to eq(10)
            end
        end
    end

    describe "#owned_by_user" do
        context "when searching" do
            it "it gets the dashboards" do
                create(:dashboard, :name => "test", :system => false, :user_id => @admin.id)
                query = Dashboard.owned_by_user({search: "test"}, @request)
                expect(query.count).to eq(1)
            end
        end

        context "when not searching" do
            it "it gets the dashboards" do
                create(:dashboard, :name => "test", :system => false, :user_id => @admin.id)
                query = Dashboard.owned_by_user({}, @request)
                expect(query.count).to eq(1)
            end
        end
    end

    describe "#owned_by_organization" do
        context "when searching" do
            it "it gets the dashboards" do
                create_list(:dashboard, 10, :system => false, :name => "test", :organization_id => @org.id)
                query = Dashboard.owned_by_organization({search: "test", id: @org.id}, @request)
                expect(query.count).to eq(10)
            end
        end

        context "when not searching" do
            it "it gets the dashboards" do
                create_list(:dashboard, 10, :system => false, :organization_id => @org.id)
                query = Dashboard.owned_by_organization({id: @org.id}, @request)
                expect(query.count).to eq(10)
            end
        end
    end

    describe "#owned_by_partner" do
        context "when searching" do
            it "it gets the dashboards" do
                create_list(:dashboard, 10, :system => false, :name => "test", :partner_id => @partner.id)
                query = Dashboard.owned_by_partner({search: "test", id: @partner.id}, @request)
                expect(query.count).to eq(10)
            end
        end

        context "when not searching" do
            it "it gets the dashboards" do
                create_list(:dashboard, 10, :system => false, :partner_id => @partner.id)
                query = Dashboard.owned_by_partner({id: @partner.id}, @request)
                expect(query.count).to eq(10)
            end
        end
    end
end
