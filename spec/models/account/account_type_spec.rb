require 'rails_helper'

RSpec.describe Account::AccountType, :type => :model do

  describe "is_valid?" do
    it "checks value and returns false" do
      value = Account::AccountType.is_valid?("test")
      expect(value).to eq(false)
    end
    
    it "checks value and returns true" do
      value = Account::AccountType.is_valid?("corporate")
      expect(value).to eq(true)
    end
    
    it "checks value and returns true" do
      value = Account::AccountType.is_valid?("single")
      expect(value).to eq(true)
    end
  end
  
end