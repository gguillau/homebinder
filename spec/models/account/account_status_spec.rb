require 'rails_helper'

RSpec.describe Account::AccountStatus, :type => :model do

  describe "is_valid?" do
    it "checks value and returns false" do
      value = Account::AccountStatus.is_valid?("test")
      expect(value).to eq(false)
    end
    
    it "checks value and returns true" do
      value = Account::AccountStatus.is_valid?("active")
      expect(value).to eq(true)
    end
    
    it "checks value and returns true" do
      value = Account::AccountStatus.is_valid?("expired")
      expect(value).to eq(true)
    end
    
    it "checks value and returns true" do
      value = Account::AccountStatus.is_valid?("suspended")
      expect(value).to eq(true)
    end
    
    it "checks value and returns true" do
      value = Account::AccountStatus.is_valid?("closed")
      expect(value).to eq(true)
    end
    
    it "checks value and returns true" do
      value = Account::AccountStatus.is_valid?("canceled")
      expect(value).to eq(true)
    end
  end
end