require 'rails_helper'

RSpec.describe Account::AccountSubStatus, :type => :model do

  describe "is_valid?" do
    it "checks value and returns false" do
      value = Account::AccountSubStatus.is_valid?("test")
      expect(value).to eq(false)
    end
    
    it "checks value and returns true" do
      value = Account::AccountSubStatus.is_valid?("free_trial_expired")
      expect(value).to eq(true)
    end
    
    it "checks value and returns true" do
      value = Account::AccountSubStatus.is_valid?("none")
      expect(value).to eq(true)
    end
    
    it "checks value and returns true" do
      value = Account::AccountSubStatus.is_valid?("invalid_credit_card")
      expect(value).to eq(true)
    end
  end
  
end