require 'rails_helper'

RSpec.describe Account::AccountBuilder, :type => :model do
    before :each do
        search = OpenStruct.new(
            results: OpenStruct.new({
                total: 1,
                response: OpenStruct.new({
                    records: [OpenStruct.new({:id => 1})]
                })
            })
        )

        allow(Account.__elasticsearch__).to receive(:search).and_return(search)
    end

    describe "index" do
        it "gets all accounts" do
            user = create(:user, :role => "admin")
            account = create(:account)
            request = HBRequest.new.create_request(user)
            page = Account.index(request, {})
            expect(page.total).to eq(1)
            expect(page.items[0].id).to eq(account.id)
        end

        it "gets accounts by id" do
            user = create(:user, :role => "admin")
            account = create(:account)
            search = OpenStruct.new(
                results: OpenStruct.new({
                    total: 1,
                    response: OpenStruct.new({
                        records: [OpenStruct.new({:id => account.id})]
                    })
                })
            )

            allow(Account.__elasticsearch__).to receive(:search).and_return(search)
            request = HBRequest.new.create_request(user)
            page = Account.index(request, { search: account.id.to_s })
            expect(page.total).to eq(1)
            expect(page.items[0].id).to eq(account.id)
        end

        it "gets accounts by number" do
            user = create(:user, :role => "admin")
            account = create(:account, account_number: "12345678")
            search = OpenStruct.new(
                results: OpenStruct.new({
                    total: 1,
                    response: OpenStruct.new({
                        records: [OpenStruct.new({:id => account.id})]
                    })
                })
            )

            allow(Account.__elasticsearch__).to receive(:search).and_return(search)
            request = HBRequest.new.create_request(user)
            page = Account.index(request, { search: account.account_number })
            expect(page.total).to eq(1)
            expect(page.items[0].id).to eq(account.id)
        end

        it "gets accounts by status" do
            user = create(:user, :role => "admin")
            account = create(:account, account_status: Account::AccountStatus::EXPIRED)
            search = OpenStruct.new(
                results: OpenStruct.new({
                    total: 1,
                    response: OpenStruct.new({
                        records: [OpenStruct.new({:id => account.id})]
                    })
                })
            )

            allow(Account.__elasticsearch__).to receive(:search).and_return(search)
            request = HBRequest.new.create_request(user)
            page = Account.index(request, { search: Account::AccountStatus::EXPIRED })
            expect(page.total).to eq(1)
            expect(page.items[0].id).to eq(account.id)
        end

        it "gets accounts by sub status" do
            user = create(:user, :role => "admin")
            account = create(:account, account_sub_status: Account::AccountSubStatus::FREE_TRIAL_EXPIRED)
            search = OpenStruct.new(
                results: OpenStruct.new({
                    total: 1,
                    response: OpenStruct.new({
                        records: [OpenStruct.new({:id => account.id})]
                    })
                })
            )

            allow(Account.__elasticsearch__).to receive(:search).and_return(search)

            request = HBRequest.new.create_request(user)
            page = Account.index(request, { search: Account::AccountSubStatus::FREE_TRIAL_EXPIRED })
            expect(page.total).to eq(1)
            expect(page.items[0].id).to eq(account.id)
        end

        it "gets accounts by type" do
            user = create(:user, :role => "admin")
            account = create(:account, account_type: Account::AccountType::CORPORATE)
            search = OpenStruct.new(
                results: OpenStruct.new({
                    total: 1,
                    response: OpenStruct.new({
                        records: [OpenStruct.new({:id => account.id})]
                    })
                })
            )

            allow(Account.__elasticsearch__).to receive(:search).and_return(search)
            request = HBRequest.new.create_request(user)
            page = Account.index(request, { search: Account::AccountType::CORPORATE })
            expect(page.total).to eq(1)
            expect(page.items[0].id).to eq(account.id)
        end

        it "gets accounts by sub type" do
            user = create(:user, :role => "admin")
            account = create(:account, account_sub_type: Account::AccountSubType::FREE_TRIAL)
            user = create(:user, :role => "admin")
            account = create(:account)

            search = OpenStruct.new(
                results: OpenStruct.new({
                    total: 1,
                    response: OpenStruct.new({
                        records: [OpenStruct.new({:id => account.id})]
                    })
                })
            )

            allow(Account.__elasticsearch__).to receive(:search).and_return(search)
            request = HBRequest.new.create_request(user)
            page = Account.index(request, { search: Account::AccountSubType::FREE_TRIAL })
            expect(page.total).to eq(1)
            expect(page.items[0].id).to eq(account.id)
        end

        it "gets accounts by name" do
            user = create(:user, :role => "admin")
            account = create(:account)

            search = OpenStruct.new(
                results: OpenStruct.new({
                    total: 1,
                    response: OpenStruct.new({
                        records: [OpenStruct.new({:id => account.id})]
                    })
                })
            )

            allow(Account.__elasticsearch__).to receive(:search).and_return(search)

            request = HBRequest.new.create_request(user)
            page = Account.index(request, { search: account.name })
            expect(page.total).to eq(1)
            expect(page.items[0].id).to eq(account.id)
        end

        it "finds nothing" do
            search = OpenStruct.new(
                results: OpenStruct.new({
                    total: 0,
                    response: OpenStruct.new({
                        records: [OpenStruct.new({:id => 1})]
                    })
                })
            )

            allow(Account.__elasticsearch__).to receive(:search).and_return(search)
            user = create(:user, :role => "admin")
            request = HBRequest.new.create_request(user)
            page = Account.index(request, { search: "nothing" })
            expect(page.total).to eq(0)
        end
    end

    describe "show" do
        it "requires rights" do
            user = create(:user)
            account = create(:account)
            request = HBRequest.new
            request.user = user
            request.ability = Ability.new(user)
            expect{Account.show(request, {id: account.manager.id })}.to raise_exception(CanCan::AccessDenied)
        end

        it "gets an individual account" do
            user = create(:user, :role => "admin")
            account = create(:account)
            request = HBRequest.new
            request.user = user
            request.ability = Ability.new(user)
            get = Account.show(request, {id: account.manager.id })
            expect(get.id).to eq(account.id)
        end
    end

    describe "build" do
        it "requires rights" do
            user = create(:user)
            request = HBRequest.new.create_request(user)
            partner = create(:partner)
            expect{Account.build(request, {account: {manager_id: partner.id}})}.to raise_exception(CanCan::AccessDenied)
        end

        it "raises unprocessable" do
            user = create(:user, :role => "admin")
            partner = create(:partner)
            request = HBRequest.new.create_request(user)
            empty = ActionController::Parameters.new({account: {manager_id: partner.id}})
            expect{Account.build(request, empty)}.to raise_exception(UnprocessableException)
        end

        it "creates an account" do
            user = create(:user, :role => "admin")
            partner = create(:partner)
            request = HBRequest.new.create_request(user)
            account = {
                account: {
                    manager_id: partner.id,
                    name: "test account",
                    account_status: Account::AccountStatus::ACTIVE,
                    account_type: Account::AccountType::CORPORATE,
                    account_sub_type: Account::AccountSubType::FREE_TRIAL,
                    trial_expiration: Time.now + 30.days
                }
            }
            account = ActionController::Parameters.new(account)
            account = Account.build(request, account)

            partner.reload
            expect(account).to_not be_nil
            expect(account.account_number).to_not be_nil
            expect(partner.account_id).to eq(account.id)
            expect(account.partners.count).to eq(1)
        end

        it "creates an account but calls new relic" do
            allow(ErrorService).to receive(:perform_async)
            
            allow_any_instance_of(Partner).to receive(:save).and_return(false)
            
            user = create(:user, :role => "admin")
            partner = create(:partner)
            request = HBRequest.new.create_request(user)
            account = {
                account: {
                    manager_id: partner.id,
                    name: "test account",
                    account_status: Account::AccountStatus::ACTIVE,
                    account_type: Account::AccountType::CORPORATE,
                    account_sub_type: Account::AccountSubType::FREE_TRIAL,
                    trial_expiration: Time.now + 30.days
                }
            }
            account = ActionController::Parameters.new(account)
            account = Account.build(request, account)

            partner.reload
            expect(account).to_not be_nil
            expect(account.account_number).to_not be_nil
            expect(partner.account_id).to be(nil)
            expect(account.partners.count).to eq(0)
            
            expect(ErrorService).to have_received(:perform_async)
        end
    end

    describe "update" do
        it "updates an account" do
            user = create(:user, :role => "admin")
            account = create(:account)
            request = HBRequest.new.create_request(user)

            update = {
                id: account.id,
                account: {
                    id: account.id,
                    name: "update",
                    trial_expiration: Time.now + 30.days
                }
            }
            update = ActionController::Parameters.new(update)
            account = Account.update(request, update)
            expect(account.name).to eq("update")
        end
    end

    describe "destroy" do
        it "deletes an account" do
            user = create(:user, :role => "admin")
            account = create(:account)
            request = HBRequest.new.create_request(user)
            Account.destroy(request, account.id)

            expect(Account.where(:id => account.id).count).to eq(0)
        end
    end
end
