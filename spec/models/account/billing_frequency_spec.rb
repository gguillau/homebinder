require 'rails_helper'

RSpec.describe Account::BillingFrequency, :type => :model do

  describe "is_valid?" do
    it "checks value and returns false" do
      value = Account::BillingFrequency.is_valid?("test")
      expect(value).to eq(false)
    end
    
    it "checks value and returns true" do
      value = Account::BillingFrequency.is_valid?("per_transaction")
      expect(value).to eq(true)
    end
    
    it "checks value and returns true" do
      value = Account::BillingFrequency.is_valid?("monthly")
      expect(value).to eq(true)
    end
    
    it "checks value and returns true" do
      value = Account::BillingFrequency.is_valid?("quarterly")
      expect(value).to eq(true)
    end
    
    it "checks value and returns true" do
      value = Account::BillingFrequency.is_valid?("annual")
      expect(value).to eq(true)
    end
  end
  
end