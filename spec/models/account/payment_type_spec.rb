require 'rails_helper'

RSpec.describe Account::PaymentType, :type => :model do

  describe "is_valid?" do
    it "checks value and returns false" do
      value = Account::PaymentType.is_valid?("test")
      expect(value).to eq(false)
    end
    
    it "checks value and returns true" do
      value = Account::PaymentType.is_valid?("subscription")
      expect(value).to eq(true)
    end
    
    it "checks value and returns true" do
      value = Account::PaymentType.is_valid?("transactional")
      expect(value).to eq(true)
    end
  end
  
end