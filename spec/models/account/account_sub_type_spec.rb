require 'rails_helper'

RSpec.describe Account::AccountSubType, :type => :model do

  describe "is_valid?" do
    it "checks value and returns false" do
      value = Account::AccountSubType.is_valid?("test")
      expect(value).to eq(false)
    end
    
    it "checks value and returns true" do
      value = Account::AccountSubType.is_valid?("free_trial")
      expect(value).to eq(true)
    end
    
    it "checks value and returns true" do
      value = Account::AccountSubType.is_valid?("paid")
      expect(value).to eq(true)
    end
  end
  
end