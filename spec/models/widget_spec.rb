require 'rails_helper'

RSpec.describe Widget, :type => :model do
  
  it "has a valid factory" do
    widget = create :widget
    expect(widget).to be_valid
  end
  
  describe "name" do
    it "is required" do
      widget = build(:widget, name: nil)
      expect(widget).to_not be_valid
    end
    
    it "is max length of 255" do
      widget = build(:widget, name: Faker::Lorem.characters(256))
      expect(widget).to_not be_valid
    end
    
    it "must be unique" do
      create(:widget, name: "widget")
      widget = build(:widget, name: "widget")
      expect(widget).to_not be_valid
    end
  end
  
  describe "key" do
    it "is required" do
      widget = build(:widget, key: nil)
      expect(widget).to_not be_valid
    end
    
    it "is max length of 255" do
      widget = build(:widget, key: Faker::Lorem.characters(256))
      expect(widget).to_not be_valid
    end
  end
  
  describe "category" do
    it "has max length of 255" do
      widget = build(:widget, category: Faker::Lorem.characters(256))
      expect(widget).to_not be_valid
    end
  end
  
  describe "restrictions" do
    it "only allows organization" do
      widget = build(:widget, organization_restriction_id: 1, partner_restriction_id: 1, admin_restriction: false)
      expect(widget).to_not be_valid
      
      widget = build(:widget, organization_restriction_id: 1, partner_restriction_id: nil, admin_restriction: true)
      expect(widget).to_not be_valid
      
      widget = build(:widget, organization_restriction_id: 1, partner_restriction_id: nil, admin_restriction: false)
      expect(widget).to be_valid
    end
    
    it "only allows partner" do
      widget = build(:widget, organization_restriction_id: 1, partner_restriction_id: 1, admin_restriction: false)
      expect(widget).to_not be_valid
      
      widget = build(:widget, organization_restriction_id: nil, partner_restriction_id: 1, admin_restriction: true)
      expect(widget).to_not be_valid
      
      widget = build(:widget, organization_restriction_id: nil, partner_restriction_id: 1, admin_restriction: false)
      expect(widget).to be_valid
    end
    
    it "only allows admin" do
      widget = build(:widget, organization_restriction_id: 1, partner_restriction_id: nil, admin_restriction: true)
      expect(widget).to_not be_valid
      
      widget = build(:widget, organization_restriction_id: nil, partner_restriction_id: 1, admin_restriction: true)
      expect(widget).to_not be_valid
      
      widget = build(:widget, organization_restriction_id: nil, partner_restriction_id: nil, admin_restriction: true)
      expect(widget).to be_valid
    end
  end
  
  describe "dashboards" do
    before :each do
      @widget = create(:widget)
      @board = create(:dashboard)
      create(:dashboard_widget, dashboard_id: @board.id, widget_id: @widget.id)
    end
    
    it "has many dashboard_widgets" do
      expect(@widget.dashboard_widgets.count).to eq(1)
    end
    
    it "has many dashboards through dashboard_widgets" do
      expect(@widget.dashboards.count).to eq(1)
    end
    
    it "destroys dependent dashboards" do
      @widget.destroy
      expect(DashboardWidget.where(dashboard_id: @board.id, widget_id: @widget.id).count).to eq(0)
    end
  end
  
end