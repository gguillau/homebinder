require 'rails_helper'
require 'cancan'

RSpec.describe PartnerContractor, :type => :model do
  before do
    @admin = create(:user, :role => "admin")
    @user = create(:user, :role => "inspector")
    @partner = create(:partner)
    @request = HBRequest.new.create_request(@user)
  end
  
  describe "validation" do
    it "requires a name" do
      c = build(:partner_contractor, name: nil)
      expect(c).to_not be_valid
    end
    
    it "requires name not be blank" do
      c = build(:partner_contractor, name: "")
      expect(c).to_not be_valid
    end
    
    it "requires name be less than 101 characters longs" do
      c = build(:partner_contractor, name: Faker::Lorem.characters(101))
      expect(c).to_not be_valid
    end
    
    it "accepts name be between 1 and 100 characters long" do
      c = build(:partner_contractor, name: Faker::Lorem.characters(100))
      expect(c).to be_valid
    end
    
    it "requires notes be less than 5001 characters" do
      c = build(:partner_contractor, notes: Faker::Lorem.characters(5001))
      expect(c).to_not be_valid
    end
    
    it "accepts notes of 5000 characters or less" do
      c = build(:partner_contractor, notes: Faker::Lorem.characters(5000))
      expect(c).to be_valid
    end
    
    it "requires phone to be valid even when less than 21 characters" do
      c = build(:partner_contractor, phone: "230-340-0310")
      expect(c).to be_valid
    end
    
    it "requires email be less than 201 characters" do
      list = Faker::Lorem.characters(201)
      email = "#{list}@gmail.com"
      c = build(:partner_contractor, email: email)
      expect(c).to_not be_valid
    end
    
    it "accepts email of 200 characters or less" do
      c = build(:partner_contractor, email: Faker::Lorem.characters(200))
      expect(c).to be_valid
    end
  end
  
  describe "index"  do
    it "gets contractors for partner" do
      @user.add_role :partner_admin, @partner
      
      create(:partner_contractor, partner_id: @partner.id)
      create(:partner_contractor, partner_id: @partner.id)
      
      page = PartnerContractor.index(@request, {partner_id: @partner.id})
      
      expect(page.total).to eq(2)
    end
  end
  
  describe "#update_contractor" do
    it "updates the parent contractor" do
      user = create(:user, :sign_in_count => 0)
      contractor = create(:contractor, :user => user)
      partner_contractor = create(:partner_contractor, :contractor => contractor)
      contractor_template = create(:contractor_template, :partner_contractor => partner_contractor)
      binder_contractor = create(:binder_contractor, :library_source_id => contractor_template.id)
      address = create(:address, :address1 => "123 Main Street", :city => "Boston", :state => "MA", :zip => "02210", :country => "US", :partner_contractor_id => partner_contractor.id)
      address2 = create(:address, :contractor_id => contractor.id)
      
      partner_contractor.name = "updated"
      partner_contractor.email = "updated@gmail.com"
      partner_contractor.phone = "+13034041211"
      partner_contractor.website = "www.homebinder.com"
      partner_contractor.save!
      
      contractor.reload
      address2.reload
      binder_contractor.reload
      
      expect(contractor.name).to eq("updated")
      expect(contractor.email).to eq("updated@gmail.com")
      expect(contractor.phone).to eq("+13034041211")
      expect(contractor.url).to eq("www.homebinder.com")
      expect(address2.address1).to eq("123 Main Street")
      expect(address2.city).to eq("Boston")
      expect(address2.state).to eq("MA")
      expect(address2.zip).to eq("02210")
      expect(address2.country).to eq("US")
      
      expect(binder_contractor.contact).to eq(partner_contractor.name)
      expect(binder_contractor.details).to eq(partner_contractor.notes)
    end
  end

end