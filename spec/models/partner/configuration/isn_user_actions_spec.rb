require 'rails_helper'

RSpec.describe Partner::Configuration::IsnUser, :type => :model do

    describe "actions" do
        before do
            @user = create(:user, :role => "inspector")
            @partner = create(:partner)
            @config = @partner.partner_configuration
            @isn_user = create(:isn_user, partner_configuration_id: @config.id)
            @user.add_role :partner_admin, @partner
            @request = HBRequest.new.create_request(@user)
            @no_access = create(:user, :role => "homeowner")
            @no_request = HBRequest.new.create_request(@no_access)
        end

        describe "index" do
            it "gets the isn_users for a partner" do
                page = Partner::Configuration::IsnUser.index(@request, {:partner_configuration_id => @config.id})
                expect(page.total).to eq(1)
            end

            it "raises CanCan::AccessDenied" do
                expect{Partner::Configuration::IsnUser.index(@no_request, {:partner_configuration_id => @config.id})}.to raise_error(CanCan::AccessDenied)
            end

            it "raises NotFound" do
                expect{Partner::Configuration::IsnUser.index(@request, {:partner_configuration_id => 9999999})}.to raise_error(NotFoundException)
            end
        end

        describe "show" do
            it "gets an isn_user" do
                @request = HBRequest.new.create_request(@user)
                isn_user = Partner::Configuration::IsnUser.show(@request, {:id => @isn_user.id})
                expect(isn_user).to_not be_nil
            end

            it "raises CanCan::AccessDenied" do
                expect{Partner::Configuration::IsnUser.show(@no_request, {:id => @isn_user.id})}.to raise_error(CanCan::AccessDenied)
            end

            it "raises ActiveRecord::RecordNotFound" do
                expect{Partner::Configuration::IsnUser.show(@request, {:id => 9999999})}.to raise_error(ActiveRecord::RecordNotFound)
            end
        end

        describe "create" do
            it "gets the footprints" do
                rake = Isn::RakeJob.new
                allow(Isn::RakeJob).to receive(:new).and_return(rake)
                allow(rake).to receive(:get_footprints)

                isn_user = ActionController::Parameters.new({ :name => "test", :company_key => "test", :password => "password", :username => "bsmith", :partner_configuration_id => @config.id })
                isn_user =  Partner::Configuration::IsnUser.build(@request, {:id => @partner.id, :isn_user => isn_user})

                expect(isn_user).to_not be_nil
                expect(rake).to have_received(:get_footprints)
            end

            it "raises CanCan::AccessDenied" do
                expect{Partner::Configuration::IsnUser.build(@no_request, {:isn_user => { :name => "default", :partner_configuration_id => @config.id }})}.to raise_error(CanCan::AccessDenied)
            end

            it "raises ActiveRecord::RecordNotFound" do
                expect{Partner::Configuration::IsnUser.build(@request, {:isn_user => { :name => "default", :partner_configuration_id => 9999999}})}.to raise_error(NotFoundException)
            end
        end

        describe "update" do
            it "updates an isn_user" do
                isn_user = ActionController::Parameters.new({:id => @isn_user.id, :name => "updated" })
                isn_user = Partner::Configuration::IsnUser.update(@request, {:id => @isn_user.id, :isn_user => isn_user})
                expect(isn_user).to_not be_nil
            end

            it "raises CanCan::AccessDenied" do
                expect{Partner::Configuration::IsnUser.update(@no_request, {:id => @isn_user.id, :isn_user => { :id => @isn_user.id, :name => "updated" }})}.to raise_error(CanCan::AccessDenied)
            end

            it "raises NotFound" do
                expect{Partner::Configuration::IsnUser.update(@request, {:id => 9999999, :isn_user => { :id => 9999999, :name => "updated" }})}.to raise_error(ActiveRecord::RecordNotFound)
            end
        end

        describe "destroy" do
            it "deletes an isn_user" do
                Partner::Configuration::IsnUser.destroy(@request, @isn_user.id)

                expect(Partner::Configuration::IsnUser.where(:id => @isn_user.id).first).to be_nil
            end

            it "raises CanCan::AccessDenied" do
                expect{Partner::Configuration::IsnUser.destroy(@no_request, @isn_user.id)}.to raise_error(CanCan::AccessDenied)
            end
        end
    end
end
