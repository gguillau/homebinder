require 'rails_helper'

RSpec.describe Partner::Configuration, :type => :model do
  
  describe "validation" do
    describe "maintenance_note" do
      
      it "can be nil" do
        config = build(:partner_configuration, partner_id: 1, maintenance_note: nil)
        expect(config).to be_valid
      end
      
      it "cannot be shares" do
        config = build(:partner_configuration, partner_id: 1, maintenance_note: Faker::Lorem.characters(250), default_binder_action: "shares" )
        expect(config).to_not be_valid
      end
      
      it "can be share" do
        config = build(:partner_configuration, partner_id: 1, maintenance_note: Faker::Lorem.characters(250), default_binder_action: "share" )
        expect(config).to be_valid
      end
      
      it "can be transfer" do
        config = build(:partner_configuration, partner_id: 1, maintenance_note: Faker::Lorem.characters(250), default_binder_action: "transfer" )
        expect(config).to be_valid
      end
      
      it "can be an empty string" do
        config = build(:partner_configuration, partner_id: 1, maintenance_note: "")
        expect(config).to be_valid
      end
      
      it "cannot be longer than 10000 characters" do
        config = build(:partner_configuration, partner_id: 1, maintenance_note: Faker::Lorem.characters(10001) )
        expect(config).to_not be_valid
      end
      
      it "can be 10000 characters" do
        config = build(:partner_configuration, partner_id: 1, maintenance_note: Faker::Lorem.characters(10000) )
        expect(config).to be_valid
      end
      
      it "can be 1 character" do
        config = build(:partner_configuration, partner_id: 1, maintenance_note: Faker::Lorem.characters(1) )
        expect(config).to be_valid
      end
      
      it "can be between 1 and 1000 character" do
        config = build(:partner_configuration, partner_id: 1, maintenance_note: Faker::Lorem.characters(250) )
        expect(config).to be_valid
      end
      
      it "requires a valid default binder template" do
        partner = create(:partner, created_at: 1.month.ago)
        config = create(:partner_configuration, partner: partner, maintenance_note: Faker::Lorem.characters(250) )
        expect(config).to_not be_valid
        expect(config.errors.full_messages.first).to eq("Default binder template is required")
      end
      
    end
  end
  
  describe "actions" do
    before do
      @user = create(:user, :role => "broker")
      @partner = create(:partner, :partner_type => "broker")
      @partner.reload
      @config = @partner.partner_configuration
      @user.add_role :partner_admin, @partner
      @request = HBRequest.new.create_request(@user)
      @no_access = create(:user, :role => "homeowner")
      @no_request = HBRequest.new.create_request(@no_access)
    end
      
    describe "show" do
      it "raises not found when the partner does not exist" do
        expect{Partner::Configuration.show(@no_request, {:id => 9999999})}.to raise_exception(ActiveRecord::RecordNotFound)
      end
      
      it "verifies the user can read the partner" do
        expect{Partner::Configuration.show(@no_request, {:id => @config.id})}.to raise_exception(CanCan::AccessDenied)
      end
      
      it "gets the cofig" do
        # check that the config exists
        config = Partner::Configuration.show(@request, {:id => @config.id})
        expect(config).to_not be_nil
      end
    end
    
    describe "update" do
      it "raises not found when the config does not exist" do
        params = {
          :id => 9999999,
          :partner_configuration => {
            :partner_id => @partner.id,
            :maintenance_note => "note"
          }
        }
        expect{Partner::Configuration.update(@request, params)}.to raise_exception(ActiveRecord::RecordNotFound)
      end
      
      it "verifies the user can read the partner" do
        params = {
          :id => @config.id,
          :partner_configuration => {
            :partner_id => @partner.id,
            :maintenance_note => "note"
          }
        }
        expect{Partner::Configuration.update(@no_request, params)}.to raise_exception(CanCan::AccessDenied)
      end
      
      it "raises an exception when configuration is not provided" do
        params = {
          :id => @config.id
        }
        expect{Partner::Configuration.update(@request, params)}.to raise_exception(BadRequestException)
      end
      
      it "updates an existing configuration object" do
        params = {
          :id => @config.id,
          :partner_configuration => {
            :partner_id => @partner.id,
            :maintenance_note => "note"
          }
        }
        params = ActionController::Parameters.new(params)
        config = Partner::Configuration.update(@request, params)
        expect(config).to_not be_nil
        expect(config.partner_id).to eq(@partner.id)
        expect(config.maintenance_note).to eq("note")
        expect(Partner::Configuration.where(:partner_id => @partner.id).count).to eq(1)
      end
    end
  end
  
end