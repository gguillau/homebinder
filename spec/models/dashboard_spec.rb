require 'rails_helper'

RSpec.describe Dashboard, :type => :model do
  
  it "has a valid factory" do
    board = build :dashboard
    expect(board).to be_valid
  end
  
  describe "belongs_to organization" do
    it "gets the organization" do
      org = create(:organization)
      dashboard = create(:dashboard, organization_id: org.id, system: false, scope: DashboardScope::BINDER)
      
      expect(dashboard.organization).to_not be_nil
    end
  end
  
  describe "belongs_to partner" do
    it "gets the partner" do
      partner = create(:partner)
      dashboard = create(:dashboard, partner_id: partner.id, system: false, scope: DashboardScope::BINDER)
      
      expect(dashboard.partner).to_not be_nil
    end
  end
  
  describe "belongs_to user" do
    it "gets the user" do
      user = create(:user)
      dashboard = create(:dashboard, user_id: user.id, system: false, scope: DashboardScope::USER)
      
      expect(dashboard.user).to_not be_nil
    end
  end
  
  describe "has_many widgets" do
    it "gets widgets" do
      widget = create(:widget)
      dashboard = create(:dashboard)
      create(:dashboard_widget, dashboard_id: dashboard.id, widget_id: widget.id)
      
      expect(dashboard.dashboard_widgets.count).to eq(1)
      expect(dashboard.widgets.count).to eq(1)
    end
  end
  
  describe "validation" do
    describe "name" do
      it "can't be nil" do
        dashboard = build(:dashboard, name: nil)
        expect(dashboard).to_not be_valid
      end
      
      it "must be 100 characters or less" do
        dashboard = build(:dashboard, name: Faker::Lorem.characters(101))
        expect(dashboard).to_not be_valid
      end
    end
    
    describe "description" do
      it "can't be more than 1000 characters" do
        dashboard = build(:dashboard, description: Faker::Lorem.characters(1001))
        expect(dashboard).to_not be_valid
      end
    end
    
    describe "ownership" do
      it "can't have multiple owners" do
        board = build(:dashboard, user_id: 1, partner_id: 1, system: false)
        expect(board).to_not be_valid
        board = build(:dashboard, user_id: 1, organization_id: 1, system: false)
        expect(board).to_not be_valid
        board = build(:dashboard, user_id: 1, system: true)
        expect(board).to_not be_valid
        board = build(:dashboard, organization_id: 1, user_id: 1, system: false)
        expect(board).to_not be_valid
        board = build(:dashboard, organization_id: 1, partner_id: 1, system: false)
        expect(board).to_not be_valid
        board = build(:dashboard, organization_id: 1, system: true)
        expect(board).to_not be_valid
        board = build(:dashboard, partner_id: 1, user_id: 1, system: false)
        expect(board).to_not be_valid
        board = build(:dashboard, partner_id: 1, organization_id: 1, system: false)
        expect(board).to_not be_valid
        board = build(:dashboard, partner_id: 1, system: true)
        expect(board).to_not be_valid
        board = build(:dashboard, system: false)
        expect(board).to_not be_valid
      end
    end
    
    describe "scope" do
      it "validates organization scope" do
        board = build(:dashboard, organization_id: 1, system: false, scope: "scope")
        expect(board).to_not be_valid
      end
      
      it "validates partner scope" do
        board = build(:dashboard, partner_id: 1, system: false, scope: "scope")
        expect(board).to_not be_valid
      end
      
      it "validates system scope" do
        board = build(:dashboard, system: true, scope: "scope")
        expect(board).to_not be_valid
      end
      
      it "validates admin scope" do
        board = build(:dashboard, system: false, scope: "scope")
        expect(board).to_not be_valid
      end
    end
  end
end