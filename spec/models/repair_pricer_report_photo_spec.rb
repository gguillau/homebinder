require 'rails_helper'

RSpec.describe RepairPricerReportPhoto, type: :model do
    
    describe 'when validating' do
        let(:repair_pricer_report_photo) { build_stubbed(:repair_pricer_report_photo) }

        it { expect(:repair_pricer_report_photo).to validate_length_of(:file) }
    end 
end
