require 'rails_helper'

RSpec.describe Contractor, :type => :model do

    it "has a valid factory" do
        contractor = create(:contractor)
        expect(contractor).to be_valid
    end
    
    it "is invalid because of contractor name is required" do
        contractor = build(:contractor, name: nil)
        expect(contractor).not_to be_valid
        expect(contractor.errors.full_messages.first).to eq("Name is required")
    end
    
    it "is invalid because of contractor name is too long" do
        contractor = build(:contractor, name: Faker::Lorem.characters(101))
        expect(contractor).not_to be_valid
        expect(contractor.errors.full_messages.first).to eq("Name is too long (maximum is 100 characters)")
    end
    
    it "is invalid because of contractor url is too long" do
        contractor = build(:contractor, url: Faker::Lorem.characters(251))
        expect(contractor).not_to be_valid
        expect(contractor.errors.full_messages.first).to eq("Url is too long (maximum is 250 characters)")
    end

end
