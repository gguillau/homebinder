require 'rails_helper'

RSpec.describe WarrantyTransaction, type: :model do
    
    it "has a valid factory" do
        warranty = create(:warranty)
        binder = create(:binder)
        binder_transaction = create(:binder_transaction, :binder_id => binder.id)
        warranty_transaction = create :warranty_transaction, :binder_transaction_id => binder_transaction.id, :warranty_id => warranty.id
        expect(warranty_transaction).to be_valid
    end
    
    it "requires a warranty_id" do
        warranty_transaction = build(:warranty_transaction, warranty_id: nil)
        expect(warranty_transaction).to_not be_valid
        expect(warranty_transaction.errors.full_messages.first).to eq("Warranty is required")
    end
    
    it "requires a binder_transaction_id" do
        warranty = create(:warranty)
        warranty_transaction = build(:warranty_transaction, warranty_id: warranty.id, binder_transaction_id: nil)
        expect(warranty_transaction).to_not be_valid
        expect(warranty_transaction.errors.full_messages.first).to eq("Binder transaction is required")
    end
    
    it "requires a unique binder_transaction_id and warranty_id" do
        warranty = create(:warranty)
        binder = create(:binder)
        binder_transaction = create(:binder_transaction, :binder_id => binder.id)
        warranty_transaction = create :warranty_transaction, :binder_transaction_id => binder_transaction.id, :warranty_id => warranty.id
        expect(warranty_transaction).to be_valid
        warranty_transaction = build :warranty_transaction, :binder_transaction_id => binder_transaction.id, :warranty_id => warranty.id
        expect(warranty_transaction).to_not be_valid
        expect(warranty_transaction.errors.full_messages.first).to eq("Warranty has already been taken")
    end
end
