require 'rails_helper'

RSpec.describe Address, :type => :model do
  before :each do
    @address = create(:address)
  end

  it "has a valid factory" do
    expect(@address).to be_valid
  end

  it "is invalid with an address1 length greater than 50" do
    address = build(:address, address1: Faker::Lorem.characters(51))
    expect(address).not_to be_valid
    expect(address.errors.full_messages.first).to eq("Address1 is too long (maximum is 50 characters)")
  end

  it "is invalid with a country length greater than 10" do
    address = build(:address, country: Faker::Lorem.characters(11))
    expect(address).not_to be_valid
    expect(address.errors.full_messages.first).to eq("Country is too long (maximum is 10 characters)")
  end
  
  it "is invalid with an address2 length greater than 50" do
    address = build(:address, address2: Faker::Lorem.characters(51))
    expect(address).not_to be_valid
    expect(address.errors.full_messages.first).to eq("Address2 is too long (maximum is 50 characters)")
  end

  it "is invalid with a city length greater than 50" do
    address = build(:address, city: Faker::Lorem.characters(51))
    expect(address).not_to be_valid
    expect(address.errors.full_messages.first).to eq("City is too long (maximum is 50 characters)")
  end
  
  it "is sets the correct state" do
    address = create(:address, state: "North Carolina")
    expect(address.state).to eq("NC")
  end
  
  it "is sets the correct state and country when country is nil" do
    address = create(:address, state: "North Carolina", country: nil)
    expect(address.state).to eq("NC")
    expect(address.country).to eq("US")
  end
  
  it "sets the correct country" do
    address = create(:address, :country => "usa")
    expect(address.country).to eq("US")
  end

end
