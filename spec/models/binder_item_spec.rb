require 'rails_helper'

RSpec.describe BinderItem, type: :model do
    it "unverified scope" do
        create_list(:binder_item, 10, :verified => true)
        create_list(:binder_item, 10, :verified => false)

        expect(BinderItem.all.count).to eq(20)
        expect(BinderItem.unverified.count).to eq(10)
    end
    
    describe "#item" do
        it "returns the appliance" do
            item = create(:appliance)
            binder_item = create(:binder_item, :appliance => item)
                expect(binder_item.item).to eq(item)
        end
        it "returns the area" do
            item = create(:area)
            binder_item = create(:binder_item, :area => item)
                expect(binder_item.item).to eq(item)
        end
        it "returns the binder_contractor" do
            item = create(:binder_contractor)
            binder_item = create(:binder_item, :binder_contractor => item)
                expect(binder_item.item).to eq(item)
        end
        it "returns the finish" do
            item = create(:finish)
            binder_item = create(:binder_item, :finish => item)
                expect(binder_item.item).to eq(item)
        end
        it "returns the inventory_item" do
            item = create(:inventory_item)
            binder_item = create(:binder_item, :inventory_item => item)
                expect(binder_item.item).to eq(item)
        end
        it "returns the maintenance_item" do
            item = create(:maintenance_item)
            binder_item = create(:binder_item, :maintenance_item => item)
                expect(binder_item.item).to eq(item)
        end
        it "returns the paint" do
            item = create(:paint)
            binder_item = create(:binder_item, :paint => item)
                expect(binder_item.item).to eq(item)
        end
        it "returns the receipt" do
            item = create(:receipt)
            binder_item = create(:binder_item, :receipt => item)
                expect(binder_item.item).to eq(item)
        end
        it "returns the structure" do
            item = create(:structure)
            binder_item = create(:binder_item, :structure => item)
                expect(binder_item.item).to eq(item)
        end
        it "returns the image" do
            item = create(:image)
            binder_item = create(:binder_item, :image => item)
                expect(binder_item.item).to eq(item)
        end
        it "returns the document" do
            item = create(:document)
            binder_item = create(:binder_item, :document => item)
                expect(binder_item.item).to eq(item)
        end
        it "returns the project" do
            item = create(:project)
            binder_item = create(:binder_item, :project => item)
                expect(binder_item.item).to eq(item)
        end
        it "returns the permit" do
            item = create(:permit)
            binder_item = create(:binder_item, :permit => item)
                expect(binder_item.item).to eq(item)
        end
    end
end
