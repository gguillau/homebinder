require 'rails_helper'

RSpec.describe PartnerBinderRole, :type => :model do

    
    it "returns binder" do
        expect(PartnerBinderRole::BINDER.to_s).to eq("binder")
    end
    
    it "returns mainenance_email" do
        expect(PartnerBinderRole::MAINTENANCE_EMAIL.to_s).to eq("mainenance_email")
    end

end
