require 'rails_helper'

RSpec.describe UserResource, :type => :model do
    
    it "has a valid factory" do
        user_resource = build :user_resource
        expect(user_resource).to be_valid
    end
    
    it "is invalid because user_id is required" do
        user_resource = build(:user_resource, user_id: nil)
        expect(user_resource).not_to be_valid
        expect(user_resource.errors.full_messages.first).to eq("User is required")
    end
    
    it "is invalid because marketing_resource_id is required" do
        user_resource = build(:user_resource, marketing_resource_id: nil)
        expect(user_resource).not_to be_valid
        expect(user_resource.errors.full_messages.first).to eq("Marketing resource is required")
    end
    
    it "is invalid because marketing_resource already exists for user" do
        user_resource = create(:user_resource)
        expect(user_resource).to be_valid
        user_resource_2 = build(:user_resource, user_id: user_resource.user_id, marketing_resource_id: user_resource.marketing_resource_id)
        expect(user_resource_2).not_to be_valid
        expect(user_resource_2.errors.full_messages.first).to eq("Marketing resource already exists for user")
    end
end