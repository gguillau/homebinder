require 'rails_helper'

RSpec.describe Tag, :type => :model do
  it "has a valid factory" do
    tag = create :tag
    expect(tag).to be_valid
  end

  describe "#taggable" do
    it "returns area" do
      area = create(:area)
      tag = create(:tag, :taggable => area)
      expect(tag.taggable).to eq(area)
    end
  end

  describe "#get_taggable" do
    it "returns the taggable item" do
      area = create(:area)
      tag = create(:tag, :taggable => area)
      expect(tag.get_taggable).to eq(area)
    end
  end

  describe "#get_tagged_resource" do
    it "returns the tagged item" do
      area = create(:area)
      image = create(:image)
      tag = create(:tag, :taggable => area, :tag => "image_#{image.id}")
      expect(tag.get_tagged_resource).to eq(image)
    end
  end
end
