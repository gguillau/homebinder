require 'rails_helper'

RSpec.describe "Paints", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, email: "user@homebinder.com") }
  let(:binder) { FactoryBot.create(:binder)}
  let(:subscription) {FactoryBot.create(:subscription, binder_id: binder.id, customer_id: "1", plan_id: "free")}
  let(:paint) {FactoryBot.create(:paint, binder_id: binder.id, created_by: user.id)}
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    
    binder.subscription = subscription
    UserBinder.create(:binder_id => binder.id, :user_id => user.id, :role => "owner")
  end

  it "gets all paints for binder" do
    get "/api/v1/paints", params: {binderId: binder.id}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "gets the paint" do
    get "/api/v1/paints/#{paint.id}", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "creates a paint" do
    paint = {
      name: "PAINT",
      binder_id: binder.id
    }
    post "/api/v1/paints", params: {paint: paint}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "updates a paint" do
    put "/api/v1/paints/#{paint.id}", params: {paint: paint.attributes.symbolize_keys.except(:id)}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "deletes a paint" do
    delete "/api/v1/paints/#{paint.id}", params: nil, headers: headers
    expect(response).to have_http_status(:no_content)
  end
  
end