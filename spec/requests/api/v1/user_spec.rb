require 'rails_helper'

RSpec.describe "User", :type => :request do
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:user) { FactoryBot.create(:user, :role => "admin") }
    let(:homeowner) { FactoryBot.create(:user, :role => "homeowner") }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}

    before :each do
        user.add_role(:admin)
    end

    it "gets all users" do
        get "/api/v1/users", params: nil, headers: headers
        expect(response).to have_http_status(:ok)
    end

    it "gets current user" do
        get "/api/v1/users/#{user.id}", params: nil, headers: headers
        expect(response).to have_http_status(:ok)
    end

    it "changes the user email" do
        put "/api/v1/users/#{user.id}/email", params: {:user => {:email => "new_email@test.com"}, :password => user.password, :email => user.email}, headers: headers
        expect(response).to have_http_status(:success)
    end

    it "changes the user password" do
        put "/api/v1/users/#{user.id}/password", params: {:user => {:password => "newpassword"}, :password => user.password}, headers: headers
        expect(response).to have_http_status(:success)
    end

    it "resets the user password" do
        post "/api/v1/users/passwords", params: {user: {email: user.email}}, headers: headers
        expect(response).to have_http_status(:no_content)
    end

    it "updates the user password" do
        token = user.send_reset_password_instructions
        password = Faker::Internet.password
        data = {
            email: user.email,
            reset_password_token: token,
            password: password,
            password_confirmation: password
        }
        put "/api/v1/users/passwords/update", params: {user: data}, headers: headers
        expect(response).to have_http_status(:no_content)
    end

    it "finds user by email" do
        post "/api/v1/users/email", params: {email: homeowner.email}, headers: headers
        expect(response).to have_http_status(:ok)
    end

    it "deletes current user" do
        delete "/api/v1/users/#{user.id}", params: nil, headers: headers
        expect(response).to have_http_status(:no_content)
    end

    it "sign in" do
        create(:user, email: "signin@homebinder.com", password: "password", password_confirmation: "password")

        user = {
            email: "signin@homebinder.com",
            password: "password"
        }

        post "/api/v1/user_tokens", params: user, headers: headers

        expect(response).to have_http_status(:ok)
    end

    it "get waiting binders by id" do
        get "/api/v1/pending_users/10/binders", params: nil, headers: headers

        expect(response).to have_http_status(:ok)
    end

    it "get waiting binders by email" do
        post "/api/v1/pending_users/binders", params: { email: "user@homebinder.com" }, headers: headers

        expect(response).to have_http_status(:ok)
    end

    it "opt out" do
        delete "/api/v1/pending_users/10", params: nil, headers: headers

        expect(response).to have_http_status(:no_content)
    end
end
