require 'rails_helper'

RSpec.describe "Appliance Recalls", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, email: "user@homebinder.com", :role => "admin") }
  let(:binder) { FactoryBot.create(:binder)}
  let(:subscription) {FactoryBot.create(:subscription, binder_id: binder.id, customer_id: "1", plan_id: "free")}
  let(:appliance) {FactoryBot.create(:appliance, binder_id: binder.id, created_by: user.id)}
  let(:appliance_recall) {FactoryBot.create(:appliance_recalls, appliance_id: appliance.id)}
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }

  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    
    binder.subscription = subscription
    UserBinder.create(:binder_id => binder.id, :user_id => user.id, :role => "owner")
  end
  
  it "updates an appliance recall" do
    put "/api/v1/appliance_recalls/#{appliance_recall.id}", params: {appliance_recall: appliance_recall.attributes.symbolize_keys.except(:id)}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
end