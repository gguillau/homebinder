require 'rails_helper'

RSpec.describe "PendingUsers", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
      
  end
   it "get waiting binders by id" do
     get "/api/v1/pending_users/10/binders", params: nil, headers: headers
    
     expect(response).to have_http_status(:ok)
   end
  
   it "get waiting binders by email" do
     post "/api/v1/pending_users/binders", params: { email: "user@homebinder.com" }, headers: headers
    
     expect(response).to have_http_status(:ok)
   end
  
   it "opt out" do
     delete "/api/v1/pending_users/10", params: nil, headers: headers
    
     expect(response).to have_http_status(:no_content)
   end
  
end