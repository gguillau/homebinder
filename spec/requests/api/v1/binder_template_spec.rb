require 'rails_helper'

RSpec.describe "Binder Template", :type => :request do
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, email: "user@homebinder.com", :role => "broker") }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    
    @partner = create(:partner, :partner_type => "broker")
    @config = @partner.partner_configuration
    @template = create(:binder_template, partner_configuration_id: @config.id)
    PartnerUser.create(:partner_id => @partner.id, :user_id => user.id, :role => "admin")
  end
  
  it "gets a binder template" do
    get "/api/v1/binder_templates/#{@template.id}", params: nil, headers: headers
    
    expect(response).to have_http_status(:ok)
  end
  
  it "copies a binder template" do
    get "/api/v1/binder_templates/#{@template.id}/copy", params: nil, headers: headers
    
    expect(response).to have_http_status(:ok)
  end
  
  it "updates a binder template" do
    data = {
      binder_template: {
        id: @template.id,
        name: "default"
      }
    }
    
    put "/api/v1/binder_templates/#{@template.id}", params: data, headers: headers
    
    expect(response).to have_http_status(:ok)
  end
  
  it "destroys a binder template" do
    delete "/api/v1/binder_templates/#{@template.id}", params: nil, headers: headers
    
    expect(response).to have_http_status(:no_content)
  end
  
end