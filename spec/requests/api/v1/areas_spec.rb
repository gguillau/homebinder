require 'rails_helper'

RSpec.describe "Areas", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, email: "user@homebinder.com") }
  let(:binder) { FactoryBot.create(:binder)}
  let(:subscription) {FactoryBot.create(:subscription, binder_id: binder.id, customer_id: "1", plan_id: "free")}
  let(:area) {FactoryBot.create(:area, binder_id: binder.id, created_by: user.id)}
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    
    binder.subscription = subscription
    UserBinder.create(:binder_id => binder.id, :user_id => user.id, :role => "owner")
  end

  it "gets all areas for binder" do
    get "/api/v1/areas", params: {binderId: binder.id}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "gets the area" do
    get "/api/v1/areas/#{area.id}", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "creates an area" do
    area = {
      name: "AREA",
      binder_id: binder.id
    }
    post "/api/v1/areas", params: {area: area}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "updates an area" do
    put "/api/v1/areas/#{area.id}", params: {area: area.attributes.symbolize_keys.except(:id)}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "deletes an area" do
    delete "/api/v1/areas/#{area.id}", params: nil, headers: headers
    expect(response).to have_http_status(:no_content)
  end
  
end