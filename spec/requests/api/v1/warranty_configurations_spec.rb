require 'rails_helper'

RSpec.describe "WarrantyConfiguration", :type => :request do

    let(:user) { FactoryBot.create(:user, role: :admin) }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }

    let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}

    before :each do
        allow(File).to receive(:read)
    end

    it "creates a partner warranty configuration" do
        warranty_plan = create(:warranty_plan)
        warrantyConfiguration = {
            :partner_warranty_plans => [{:id => warranty_plan.id, :price => 2}],
            warranty_configuration: {
                :account_number => "A12301203",
                :partner_id => create(:partner).id,
                :warranty_company_id => create(:warranty_company).id,
                :warranty_plan_id => create(:warranty_plan).id
            }
        }
        post "/api/v1/warranty_configurations", params: warrantyConfiguration, headers: headers
        expect(response).to have_http_status(:ok)
    end

    it "gets a partner warranty configuration" do
        partner_warranty_configuration = create(:warranty_configuration)
        get "/api/v1/warranty_configurations/#{partner_warranty_configuration.id}", params: nil, headers: headers
        expect(response).to have_http_status(:ok)
    end

    it "updates a partner warranty configuration" do
        partner_warranty_configuration = create(:warranty_configuration)
        warranty_plan = create(:warranty_plan)
        put "/api/v1/warranty_configurations/#{partner_warranty_configuration.id}", params: {:partner_warranty_plans => [{:id => warranty_plan.id, :price => 2}], warranty_configuration: {:account_number => "SAD912931"}}, headers: headers
        expect(response).to have_http_status(:ok)
    end

    it "sends an order" do
            binder = create(:binder)
            user = create(:user)

            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")

            warranty_company = create(:warranty_company)
            warranty_plan = create(:warranty_plan)
            warranty_configuration = create(:warranty_configuration)
            homeowner = create(:user)

            homeowner = {
                id: homeowner.id,
                firstName: homeowner.user_profile.first_name,
                lastName: homeowner.user_profile.last_name,
                email: homeowner.email,
                phone: homeowner.user_profile.mobile_phone
            }

            property = {
                address1: "123 Main Street",
                address2: "",
                city: "Boston",
                state: "MA",
                country: "US",
                zip: "02210"
            }

            params = {
                :id => warranty_configuration.id,
                :order => {
                    binder_id: binder.id,
                    property: property,
                    warranty_company_id: warranty_company.id,
                    warranty_plan_id: warranty_plan.id,
                    account_number: warranty_configuration.account_number,
                    homeowner: homeowner,
                    inspection_date: 5.days.ago,
                    price: 25
                }
            }

        post "/api/v1/warranty_configurations/#{warranty_configuration.id}/orders", params: params, headers: headers

        expect(response).to have_http_status(:ok)
    end

    it "deletes a partner warranty configuration" do
        partner_warranty_configuration = create(:warranty_configuration)
        delete "/api/v1/warranty_configurations/#{partner_warranty_configuration.id}", params: nil, headers: headers
        expect(response).to have_http_status(:no_content)
    end
end