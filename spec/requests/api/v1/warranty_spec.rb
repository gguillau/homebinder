require 'rails_helper'

RSpec.describe "Warranty", :type => :request do

    let(:admin) { FactoryBot.create(:user, :role => "admin") }
    let(:binder) { FactoryBot.create(:binder) }
    let(:warranty_plan) { FactoryBot.create(:warranty_plan) }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(admin) }
    let(:file) { {version: "0.0.0"} }
    
    let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
    
    before :each do
        homeowner = {
            firstName: "First",
            lastName: "Last",
            email: "email@gmail.com",
            phone: "534-304-3402"
        }
        property = {
            address1: "123 Main Street",
            address2: "",
            city: "Boston",
            state: "MA",
            country: "US",
            zip: "02210"
        }
    
        @warranty = {
            :binder_id => binder.id,
            :status => "pending",
            :warranty_plan_id => warranty_plan.id,
            # set warranty expiration date to warranty_plan duration + inspection date
            :expiration_date => Date.today,
            :client_first => homeowner[:firstName],
            :client_last => homeowner[:lastName],
            :client_email => homeowner[:email],
            :client_phone => homeowner[:phone],
            :address_attributes => property
        }
    end
    
    it "gets all warranties" do
        get "/api/v1/warranties", params: {:orderBy => "id"}, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "creates a warranty" do
        post "/api/v1/warranties", params: {price: 25, warranty: @warranty}, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "updates a warranty" do
        post "/api/v1/warranties", params: {price: 25, warranty: @warranty}, headers: headers
        expect(response).to have_http_status(:ok)
        warranty = JSON.parse(response.body)
        Warranties::Service.new(warranty["id"]).create_inspection_transaction(5.days.ago)
        Warranties::Service.new(warranty["id"]).create_warranty_transaction(15)
                put "/api/v1/warranties/#{warranty["id"]}", params: {price: 25, warranty: {status: "cancelled"}}, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "deletes a warranty" do
        warranty = create(:warranty)
        delete "/api/v1/warranties/#{warranty.id}", params: nil, headers: headers
        expect(response).to have_http_status(:no_content)
    end
  
end