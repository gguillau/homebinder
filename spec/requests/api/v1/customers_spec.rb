require 'rails_helper'

RSpec.describe "Customers", :type => :request do

    let(:user) { FactoryBot.create(:user, role: :admin) }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    
    let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
    
    before :each do
        allow(Stripe::Customer).to receive(:retrieve).and_return(OpenStruct.new({id: 1, email: "test@gmail.com", source: nil, delete: true}))
        allow(Stripe::Customer).to receive(:create).and_return(OpenStruct.new({id: 1, email: "test@gmail.com", source: nil, delete: true}))
        allow(Stripe::Plan).to receive(:retrieve).and_return({id: 1})
        allow(Stripe::Subscription).to receive(:create).and_return({id: 1})
    end
    
    it "gets a customer" do
        get "/api/v1/customers/1", params: nil, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "creates a customer" do
        post "/api/v1/customers", params: {customer: {email: "test@gmail.com"}, payment: {card: "card"}}, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "updates a customer" do
        put "/api/v1/customers/1", params: {id: 1, customer: {email: "fake@gmail.com", address: {address1: "123 Main Street"}}, payment: {card: "card"}}, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "deletes a customer" do
        delete "/api/v1/customers/1", params: nil, headers: headers
        expect(response).to have_http_status(:no_content)
    end
  
end