require 'rails_helper'

RSpec.describe "Seller Reports", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, email: "user@homebinder.com", role: "admin") }
  let(:binder) { FactoryBot.create(:binder)}
  let(:subscription) {FactoryBot.create(:subscription, binder_id: binder.id, plan_id: "free")}
  let(:seller_report) { FactoryBot.create(:seller_report, :binder_id => binder.id)}
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }

  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    binder.subscription = subscription
    binder.save
    
    UserBinder.create(:user_id => user.id, :binder_id => binder.id, role: "owner")
  end
  
  it "gets the reports" do
    get "/api/v1/seller_reports", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "creates the report" do
    post "/api/v1/seller_reports", params: {seller_report: {binder_id: binder.id}}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "gets the report" do
    get "/api/v1/seller_reports/#{seller_report.code}", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "updates the report" do
    put "/api/v1/seller_reports/#{seller_report.id}", params: {seller_report: seller_report.attributes.symbolize_keys.except(:id)}, headers: headers
    expect(response).to have_http_status(:ok)
  end

end