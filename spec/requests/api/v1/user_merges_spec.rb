require 'rails_helper'

model = 'UserMerge'
RSpec.describe "#{model.pluralize}", type: :request do
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:user) { FactoryBot.create(:user, :role => "admin") }
    let!(:item) { create(model.constantize.table_name.singularize.to_sym) }
    let(:route) { model.constantize.table_name }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}

    describe '#index' do
        it 'gets all items' do
            get "/api/v1/#{route}", headers: headers
            expect(response).to have_http_status(:ok)
        end

        it 'captures the error' do
            allow(model.constantize).to receive(:index).and_raise(BadRequestException)
            get "/api/v1/#{route}", headers: headers
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe '#show' do
        it 'gets a item' do
            get "/api/v1/#{route}/#{item.id}", headers: headers
            expect(response).to have_http_status(:ok)
        end

        it 'captures the error' do
            allow(model.constantize).to receive(:show).and_raise(BadRequestException)
            get "/api/v1/#{route}/#{item.id}", headers: headers
            expect(response).to have_http_status(:bad_request)
        end
    end

    describe '#update' do
        it 'updates an item' do
            patch "/api/v1/#{route}/#{item.id}", params: { "#{route.singularize}" => item.attributes }, headers: headers
            expect(response).to have_http_status(:ok)
        end

        it 'captures the error when BadRequestException' do
            allow(model.constantize).to receive(:update).and_raise(BadRequestException)
            patch "/api/v1/#{route}/#{item.id}", params: { "#{route.singularize}" => item.attributes }, headers: headers
            expect(response).to have_http_status(:bad_request)
        end

        it 'captures the error when UnprocessableException' do
            allow_any_instance_of(model.constantize).to receive(:save).and_return(false)
            patch "/api/v1/#{route}/#{item.id}", params: { "#{route.singularize}" => item.attributes }, headers: headers
            expect(response).to have_http_status(422)
        end
    end
    
    describe '#revert' do
        it 'calls the job' do
            Sidekiq::Testing.disable!
            put "/api/v1/#{route}/#{item.id}/revert", headers: headers
            expect(response).to have_http_status(:no_content)
        end
    end
end
