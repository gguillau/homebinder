require 'rails_helper'

RSpec.describe "Contractors", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:admin) { FactoryBot.create(:user, :role => "admin") }
  let(:jwt) { UserTokenService.create_jwt(admin) }
  let(:contractor) {FactoryBot.create(:contractor, verified: true, created_by: admin.id)}
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    
  end
  
  
  it "gets all the contractors for the current user" do
    get "/api/v1/contractors", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "gets the contractor" do
    get "/api/v1/contractors/#{contractor.id}", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end

end