require 'rails_helper'

RSpec.describe "Automation Error", :type => :request do
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:file) { {version: "0.0.0"} }
    let(:partner) { FactoryBot.create(:partner, name: "Test Inspections", contact: "Test Inspector", partner_key: "axium", :partner_type => "inspector") }
    let(:admin) { FactoryBot.create(:user, :role => "admin") }
    let(:user) { FactoryBot.create(:user, email: partner.email, :role => "inspector") }
    let(:jwt) { UserTokenService.create_jwt(admin) }
    let(:partner_key) { FactoryBot.create(:api_key, partner_id: partner.id, contact_email: "test@gmail.com") }
    let(:config) { partner.partner_configuration }
    let(:template) { FactoryBot.create(:binder_template, partner_configuration_id: config.id) }
    let(:file) { {version: "0.0.0"} }

    let(:headers) { { 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt  } }

    describe "Automation Errors" do
        before do
            PartnerUser.create(:partner_id => partner.id, :user_id => user.id, :role => "admin")
            allow(File).to receive(:read).and_call_original
        end

        it "index errors" do
            get "/api/v1/automation_errors", params: nil, headers: headers
            expect(response).to have_http_status(:ok)
        end
        it "updates an error" do
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}

            parameters = {
                :client => client,
                :property => property,
                :binder_template_id => template.id,
                :agent => agent,
                :transfer => false,
            }

            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

            error = create(:automation_error, partner_name: partner.partner_key, binder_template_id: template.id)
            put "/api/v1/automation_errors/#{error.id}", params: {error: error.attributes.symbolize_keys}, headers: headers
            expect(response).to have_http_status(:ok)
        end
        it "deletes an error" do
            error = create(:automation_error)
            delete "/api/v1/automation_errors/#{error.id}", params: nil, headers: headers
            expect(response).to have_http_status(:no_content)
        end
    end
end
