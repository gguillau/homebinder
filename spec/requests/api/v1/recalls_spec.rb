require 'rails_helper'

RSpec.describe "Recalls", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:admin) { FactoryBot.create(:user, :role => "admin") }
  let(:jwt) { UserTokenService.create_jwt(admin) }
  let(:binder) { FactoryBot.create(:binder)}
  let(:recall) { FactoryBot.create(:recall)}
  let(:subscription) {FactoryBot.create(:subscription, binder_id: binder.id, customer_id: "1", plan_id: "free")}
  let(:appliance) {FactoryBot.create(:appliance, binder_id: binder.id, created_by: admin.id)}
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    
    binder.subscription = subscription
    UserBinder.create(:user_id => admin.id, :binder_id => binder.id, role: "owner")
  end
  
  
  it "gets recalls" do
    get "/api/v1/recalls", params: {}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "downloads all the recalls" do
    post "/api/v1/recalls/download", headers: headers
    expect(response).to have_http_status(:no_content)
  end
  
  it "runs recall service" do
    post "/api/v1/recalls/run_service", headers: headers
    expect(response).to have_http_status(:no_content)
  end
  
  it "updates the recall" do
    put "/api/v1/recalls/#{recall.id}", params: {recall: recall.attributes.symbolize_keys.except(:id)}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "deletes the recall" do
    delete "/api/v1/recalls/#{recall.id}", params: nil, headers: headers
    expect(response).to have_http_status(:no_content)
  end

end