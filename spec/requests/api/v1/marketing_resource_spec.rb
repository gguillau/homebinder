require 'rails_helper'

RSpec.describe "MarketingResources", :type => :request do

    let(:user) { FactoryBot.create(:user, role: :admin) }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    
    let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
    
    before :each do
    end
    
    it "creates a marketing_resource" do
        post "/api/v1/marketing_resources", params: {marketing_resource: {name: "TEST"}}, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "updates a marketing_resource" do
        resource = create(:marketing_resource)
        put "/api/v1/marketing_resources/#{resource.id}", params: {marketing_resource: {name: "TEST"}}, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "deletes a marketing_resource" do
        resource = create(:marketing_resource)
        delete "/api/v1/marketing_resources/#{resource.id}", params: nil, headers: headers
        expect(response).to have_http_status(:no_content)
    end
  
end