require 'rails_helper'

RSpec.describe "Recall Keywords", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:admin) { FactoryBot.create(:user, :role => "admin") }
  let(:jwt) { UserTokenService.create_jwt(admin) }
  let(:recall_keyword) { FactoryBot.create(:recall_keyword) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    
  end
  
  
  it "gets all Recall Keywords" do
    get "/api/v1/recall_keywords", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "creates a new Recall Keyword" do
    post "/api/v1/recall_keywords", params: {recall_keyword: {keyword: "test"}}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "deletes a Recall Keyword" do
    delete "/api/v1/recall_keywords/#{recall_keyword.id}", params: nil, headers: headers
    expect(response).to have_http_status(:no_content)
  end
end