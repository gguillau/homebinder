require 'rails_helper'

RSpec.describe "UserTokens", :type => :request do
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }

    let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key }}

    before :each do
        allow(File).to receive(:read).and_call_original
    end

    describe '#create' do
        it "signin" do
            create(:user, email: "signin@homebinder.com", password: "password", password_confirmation: "password")

            user = {
                email: "signin@homebinder.com",
                password: "password"
            }

            post "/api/v1/user_tokens", params: user, headers: headers

            expect(response).to have_http_status(:ok)
        end

        it "raises an error when BadRequestException" do
            allow(User).to receive(:signin).and_raise(BadRequestException)
            create(:user, email: "signin@homebinder.com", password: "password", password_confirmation: "password")

            user = {
                email: "signin@homebinder.com",
                password: "password"
            }

            post "/api/v1/user_tokens", params: user, headers: headers

            expect(response).to have_http_status(:bad_request)
        end

        it "raises an error when UnprocessableException" do
            user = create(:user, email: "signin@homebinder.com", password: "password", password_confirmation: "password")
            allow_any_instance_of(User).to receive(:save!).and_raise(UnprocessableException.new(user))
            signin = {
                email: "signin@homebinder.com",
                password: "password"
            }

            post "/api/v1/user_tokens", params: signin, headers: headers

            expect(response).to have_http_status(422)
        end
    end

    describe '#destroy' do
        it 'deletes an item' do
            user = create(:user, email: "signin@homebinder.com", password: "password", password_confirmation: "password")

            signin = {
                email: "signin@homebinder.com",
                password: "password"
            }
            post "/api/v1/user_tokens", params: signin, headers: headers
            expect(response).to have_http_status(:ok)
            test_response = JSON.parse(response.body)

            headers["HB-UserToken"] = test_response["token"]
            user = User.find(user.id)

            delete "/api/v1/user_tokens/#{user.sessions.last.token}", params: nil, headers: headers

            expect(response).to have_http_status(:ok)
        end

        it 'captures the error when BadRequestException' do
            allow(Session).to receive(:find_by_token).and_raise(BadRequestException)
            user = create(:user, email: "signin@homebinder.com", password: "password", password_confirmation: "password")

            signin = {
                email: "signin@homebinder.com",
                password: "password"
            }
            post "/api/v1/user_tokens", params: signin, headers: headers
            expect(response).to have_http_status(:ok)
            test_response = JSON.parse(response.body)

            headers["HB-UserToken"] = test_response["token"]
            user = User.find(user.id)

            delete "/api/v1/user_tokens/#{user.sessions.last.token}", params: nil, headers: headers

            expect(response).to have_http_status(:bad_request)
        end

        it 'captures the error when UnprocessableException' do
            user = create(:user, email: "signin@homebinder.com", password: "password", password_confirmation: "password")
            allow_any_instance_of(Session).to receive(:update).and_raise(UnprocessableException.new(user))
            signin = {
                email: "signin@homebinder.com",
                password: "password"
            }
            post "/api/v1/user_tokens", params: signin, headers: headers
            expect(response).to have_http_status(:ok)
            test_response = JSON.parse(response.body)

            headers["HB-UserToken"] = test_response["token"]
            user = User.find(user.id)

            delete "/api/v1/user_tokens/#{user.sessions.last.token}", params: nil, headers: headers

            expect(response).to have_http_status(422)
        end
    end
end
