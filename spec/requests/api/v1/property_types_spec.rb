require 'rails_helper'

RSpec.describe "PropertyTypes", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, email: "user@homebinder.com") }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    
  end

  it "gets all property types" do

    get "/api/v1/property_types", params: nil, headers: headers
    
    expect(response).to have_http_status(:ok)
  end
end