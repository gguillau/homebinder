require 'rails_helper'

RSpec.describe "RepairPricerReport", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:admin) { FactoryBot.create(:user, :role => "admin") }
  let(:jwt) { UserTokenService.create_jwt(admin) }
  let(:repair_pricer_report) { FactoryBot.create(:repair_pricer_report)}
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  it "gets reports" do
    get "/api/v1/repair_pricer_reports", params: {}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "gets the report" do
    get "/api/v1/repair_pricer_reports/#{repair_pricer_report.id}", params: {}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "creates the report" do
    Sidekiq::Testing.disable!
    payload = {
        client_first: "John",
        client_last: "Smith",
        client_email: "jsmith@gmail.com",
        client_phone: "+13444444444",
        buyer_agent_first: "Mary",
        buyer_agent_last: "Smith",
        buyer_agent_email: "msmith@gmail.com",
        order_pool_report: true,
        order_home_history_report: true,
        rush_report: true
    }
    address = {
        address1: "123 Main Street",
        address2: "Apt. 2",
        city: "Boston",
        state: "MA",
        country: "US",
        zip: "02210"
    }
    post "/api/v1/repair_pricer_reports", params: {repair_pricer_report: payload, address: address, payment: {}}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "updates the report" do
    put "/api/v1/repair_pricer_reports/#{repair_pricer_report.id}", params: {repair_pricer_report: {payment_status: "paid"}}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "deletes the report" do
    delete "/api/v1/repair_pricer_reports/#{repair_pricer_report.id}", params: nil, headers: headers
    expect(response).to have_http_status(:no_content)
  end

end