require 'rails_helper'

RSpec.describe "WarrantyPlan", :type => :request do

    let(:user) { FactoryBot.create(:user, role: :admin) }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    
    let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
    
    before :each do
    end
    
    it "gets all warranty_plans" do
        get "/api/v1/warranty_plans", params: nil, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "creates a warranty_plan" do
        post "/api/v1/warranty_plans", params: {warranty_plan: {name: "TEST", description: "description", cycle: "months", duration: 90, warranty_company_id: create(:warranty_company).id, price: 15}}, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "updates a warranty_plan" do
        warranty_plan = create(:warranty_plan)
        put "/api/v1/warranty_plans/#{warranty_plan.id}", params: {warranty_plan: {name: "TEST"}}, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "deletes a warranty_plan" do
        warranty_plan = create(:warranty_plan)
        delete "/api/v1/warranty_plans/#{warranty_plan.id}", params: nil, headers: headers
        expect(response).to have_http_status(:no_content)
    end
  
end