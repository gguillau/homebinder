require 'rails_helper'

RSpec.describe "Finishes", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, email: "user@homebinder.com") }
  let(:binder) { FactoryBot.create(:binder)}
  let(:subscription) {FactoryBot.create(:subscription, binder_id: binder.id, customer_id: "1", plan_id: "free")}
  let(:finish) {FactoryBot.create(:finish, binder_id: binder.id, created_by: user.id)}
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    
    binder.subscription = subscription
    UserBinder.create(:binder_id => binder.id, :user_id => user.id, :role => "owner")
  end

  it "gets all finishes for binder" do
    get "/api/v1/finishes", params: {binderId: binder.id}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "gets the finish" do
    get "/api/v1/finishes/#{finish.id}", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "creates a finish" do
    finish = {
      name: "FINISH",
      binder_id: binder.id
    }
    post "/api/v1/finishes", params: {finish: finish}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "updates a finish" do
    put "/api/v1/finishes/#{finish.id}", params: {finish: finish.attributes.symbolize_keys.except(:id)}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "deletes a finish" do
    delete "/api/v1/finishes/#{finish.id}", params: nil, headers: headers
    expect(response).to have_http_status(:no_content)
  end
  
end