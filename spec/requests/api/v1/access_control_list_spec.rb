require 'rails_helper'

RSpec.describe "Binder access control list", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, email: "user@homebinder.com") }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    
    @binder = create(:binder)
    create(:subscription, binder_id: @binder.id)
    UserBinder.create(:binder_id => @binder.id, :user_id => user.id, :role => "owner")
  end
  
  it "gets" do
    get "/api/v1/binders/#{@binder.id}/acl", params: nil, headers: headers
    
    expect(response).to have_http_status(:ok)
  end
  
  it "removes a user's access" do
    delete "/api/v1/binders/#{@binder.id}/acl/2", params: nil, headers: headers
    
    expect(response).to have_http_status(:no_content)
  end
  
end