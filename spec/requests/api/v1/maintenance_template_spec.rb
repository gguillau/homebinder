require 'rails_helper'

RSpec.describe "Maintenance Template", :type => :request do
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, email: "user@homebinder.com", :role => "inspector") }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    
    @partner = create(:partner)
    @config = @partner.partner_configuration
    @template = create(:binder_template, partner_configuration_id: @config.id)
  end
  
  describe "index" do
    it "gets binder template maintenance templates" do
      user.add_role :partner_admin, @partner
      get "/api/v1/maintenance_templates", params: {binder_template_id: @template.id}, headers: headers
      expect(response).to have_http_status(:ok)
    end
  end

  describe "show" do
    it "gets an maintenance template" do
      user.add_role :partner_admin, @partner
      m = create(:maintenance_template, binder_template_id: @template.id)
      get "/api/v1/maintenance_templates/#{m.id}", params: nil, headers: headers
      
      expect(response).to have_http_status(:ok)
    end
  end
  
  describe "create" do
    it "creates a binder template maintenance template" do
      user.add_role :partner_admin, @partner
      
      data = {
        binder_template_id: @template.id,
        maintenance_template: {
          name: "maintenance",
          binder_template_id: @template.id,
          frequency: "annual",
          due_date: DateTime.now
        }
      }
      
      post "/api/v1/maintenance_templates", params: data, headers: headers
      
      expect(response).to have_http_status(:ok)
    end
  end
  
  describe "update" do
    it "updates a binder template" do
      user.add_role :partner_admin, @partner
      m = create(:maintenance_template, binder_template_id: @template.id)
      
      data = {
        id: m.id,
        maintenance_template: {
          id: m.id,
          name: "maintenance",
          frequency: "annual",
          due_date: DateTime.now
        }
      }
      
      put "/api/v1/maintenance_templates/#{m.id}", params: data, headers: headers
      
      expect(response).to have_http_status(:ok)
    end
  end
  
  describe "delete" do
    it "destroys an maintenance template" do
      user.add_role :partner_admin, @partner
      m = create(:maintenance_template, binder_template_id: @template.id)
      
      delete "/api/v1/maintenance_templates/#{m.id}", params: nil, headers: headers
      
      expect(response).to have_http_status(:no_content)
    end
  end
  
end