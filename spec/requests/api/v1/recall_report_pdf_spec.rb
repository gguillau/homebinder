require 'rails_helper'

RSpec.describe "Recall Report PDF", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, email: "user@homebinder.com") }
  let(:binder) { FactoryBot.create(:binder)}
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }

  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    service = RecallReportPdfService.new(binder)
    service.init
    
    allow_any_instance_of(RecallReportPdfService).to receive(:create).and_return(service.pdf)
    UserBinder.create(:binder_id => binder.id, :user_id => user.id, :role => "owner")
  end
  
  it "gets the PDF report" do
    get "/api/v1/recall_report_pdf", params: {binder_id: binder.id}, headers: headers
    expect(response).to have_http_status(:ok)
  end

end