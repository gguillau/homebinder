require 'rails_helper'

RSpec.describe "Free Trials", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:file) { {version: "0.0.0"} }
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key }}
  
  it "sends an email" do
    post "/api/v1/free_trials", params: {name: "test", email: "test@gmail.com", phone: "+16172335007"}, headers: headers
    expect(response).to have_http_status(:ok)
  end

end