require 'rails_helper'

RSpec.describe "MaintenanceEvents", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, email: "user@homebinder.com") }
  let(:binder) { FactoryBot.create(:binder) }
  let(:maintenance_item) {FactoryBot.create(:maintenance_item, binder_id: binder.id, created_by: user.id)}
  let(:item) {FactoryBot.create(:maintenance_item, binder_id: binder.id, created_by: user.id)}
  let(:maintenance_event) {FactoryBot.create(:maintenance_event, maintenance_item_id: maintenance_item.id)}
  let(:binder_contractor) {FactoryBot.create(:binder_contractor, binder_id: binder.id)}
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    
    UserBinder.create(:binder_id => binder.id, :user_id => user.id, :role => "owner")
  end

  it "gets all maintenance_events" do
    get "/api/v1/maintenance_events", params: {maintenance_item_id: maintenance_item.id}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "gets the maintenance_event" do
    get "/api/v1/maintenance_events/#{maintenance_event.id}", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "creates a maintenance_event" do
    post "/api/v1/maintenance_events", params: {maintenance_event: {maintenance_item_id: maintenance_item.id, do_date: Date.today + 5.months, created_by: user.id}}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "updates a maintenance_event" do
    put "/api/v1/maintenance_events/#{maintenance_event.id}", params: {maintenance_event: maintenance_event.attributes.symbolize_keys.except(:id)}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "deletes a maintenance_event" do
    delete "/api/v1/maintenance_events/#{maintenance_event.id}", params: nil, headers: headers
    expect(response).to have_http_status(:no_content)
  end
  
end