require 'rails_helper'

RSpec.describe "Appliance Template", :type => :request do
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, email: "user@homebinder.com", :role => "inspector") }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    
    @partner = create(:partner)
    @config = @partner.partner_configuration
    @template = create(:binder_template, partner_configuration_id: @config.id)
  end
  
  describe "index" do
    it "gets library appliance templates" do
      user.add_role :partner_admin, @partner
      
      get "/api/v1/appliance_templates", params: {:binder_template_id => @template.id}, headers: headers
      
      expect(response).to have_http_status(:ok)
    end
  end

  describe "show" do
    it "gets an appliance template" do
      user.add_role :partner_admin, @partner
      a = create(:appliance_template, binder_template_id: @template.id)
      get "/api/v1/appliance_templates/#{a.id}", params: nil, headers: headers
      
      expect(response).to have_http_status(:ok)
    end
  end
  
  describe "create" do
    it "creates a binder template appliance template" do
      user.add_role :partner_admin, @partner
      
      data = {
        binder_template_id: @template.id,
        appliance_template: {
          name: "appliance",
          binder_template_id: @template.id,
        }
      }
      
      post "/api/v1/appliance_templates", params: data, headers: headers
      
      expect(response).to have_http_status(:ok)
    end
  end
  
  describe "update" do
    it "updates a binder template" do
      user.add_role :partner_admin, @partner
      a = create(:appliance_template, binder_template_id: @template.id)
      
      data = {
        id: a.id,
        appliance_template: {
          id: a.id,
          name: "default"
        }
      }
      
      put "/api/v1/appliance_templates/#{a.id}", params: data, headers: headers
      
      expect(response).to have_http_status(:ok)
    end
  end
  
  describe "delete" do
    it "destroys an appliance template" do
      user.add_role :partner_admin, @partner
      a = create(:appliance_template, binder_template_id: @template.id)
      
      delete "/api/v1/appliance_templates/#{a.id}", params: nil, headers: headers
      
      expect(response).to have_http_status(:no_content)
    end
  end
  
end