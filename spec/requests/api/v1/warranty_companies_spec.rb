require 'rails_helper'

RSpec.describe "WarrantyCompany", :type => :request do

    let(:user) { FactoryBot.create(:user, role: :admin) }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    
    let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
    
    before :each do
    end
    
    it "gets all warranty_companies" do
        warranty_company = create(:warranty_company)
        get "/api/v1/warranty_companies", params: {:id => warranty_company.id}, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "creates a warranty_company" do
        post "/api/v1/warranty_companies", params: {warranty_company: {name: "TEST", email: "test@gmail.com", contact: "Mike"}}, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "updates a warranty_company" do
        warranty_company = create(:warranty_company)
        put "/api/v1/warranty_companies/#{warranty_company.id}", params: {warranty_company: {name: "TEST"}}, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "deletes a warranty_company" do
        warranty_company = create(:warranty_company)
        delete "/api/v1/warranty_companies/#{warranty_company.id}", params: nil, headers: headers
        expect(response).to have_http_status(:no_content)
    end
  
end