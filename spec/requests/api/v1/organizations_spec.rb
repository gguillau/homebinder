require 'rails_helper'

RSpec.describe "Organizations", :type => :request do
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, email: "user@homebinder.com", role: UserGlobalRoles::ADMIN) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    allow(File).to receive(:read)
    
  end
  
  it "gets all organizations" do
    get "/api/v1/organizations", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "creates a new organization" do
    post "/api/v1/organizations", params: { organization: { name: "the org" }}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "updates an organization" do
    org = create(:organization)
    put "/api/v1/organizations/#{org.id}", params: { organization: { id: org.id, name: "orgg" } }, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "deletes an organization" do
    org = create(:organization)
    delete "/api/v1/organizations/#{org.id}", params: nil, headers: headers
    expect(response).to have_http_status(:no_content)
  end
  
  it "adds a user" do
    org = create(:organization)
    auser = create(:user)
    post "/api/v1/organizations/#{org.id}/users", params: { users: [auser.id] }, headers: headers
    expect(response).to have_http_status(:no_content)
  end
  
  it "removes a user" do
    org = create(:organization)
    auser = create(:user)
    create(:organization_user, organization_id: org.id, user_id: auser.id)
    delete "/api/v1/organizations/#{org.id}/users/#{auser.id}", params: nil, headers: headers
    expect(response).to have_http_status(:no_content)
  end
  
  it "adds a partner" do
    org = create(:organization)
    partner = create(:partner)
    post "/api/v1/organizations/#{org.id}/partners", params: { partners: [partner.id] }, headers: headers
    expect(response).to have_http_status(:no_content)
  end
  
  it "removes a partner" do
    org = create(:organization)
    partner = create(:partner)
    create(:organization_partner, organization_id: org.id, partner_id: partner.id)
    delete "/api/v1/organizations/#{org.id}/partners/#{partner.id}", params: nil, headers: headers
    expect(response).to have_http_status(:no_content)
  end

end