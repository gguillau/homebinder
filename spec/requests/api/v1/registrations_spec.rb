require 'rails_helper'

RSpec.describe 'Registration', type: :request do
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:file) { { version: '0.0.0' } }

    let(:headers) { { 'HTTP_HB_APIKEY' => api_key.key } }

    before :each do
        allow(File).to receive(:read).and_call_original
    end

    it 'registers the user' do
        data = {
            user: {
                email: 'user@homebinder.com',
                password: 'password'
            }
        }

        post '/api/v1/registrations', params: data, headers: headers

        expect(response).to have_http_status(:ok)
    end
end
