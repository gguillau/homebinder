require 'rails_helper'

RSpec.describe "Partners", :type => :request do
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:admin) { FactoryBot.create(:user, email: "admin@homebinder.com", role: "admin") }
    let(:user) { FactoryBot.create(:user, email: "user@homebinder.com", role: "inspector") }
    let(:partner_member) { FactoryBot.create(:user, role: "inspector") }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:admin_jwt) { UserTokenService.create_jwt(admin) }
    let(:file) { {version: "0.0.0"} }
    let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
    let(:admin_headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => admin_jwt }}

    context "when a partner" do
        before :each do
            @partner = create(:partner, :partner_type => "inspector")
            @config = @partner.partner_configuration
            @template = create(:binder_template, partner_configuration_id: @config.id)
            user.add_role(:partner_admin, @partner)
            partner_member.add_role(:member, @partner)
        end

        it "removes a user from the partner account" do
            delete "/api/v1/partners/#{@partner.id}/users/#{partner_member.id}", params: nil, headers: headers

            expect(response).to have_http_status(:ok)
        end

        it "gets the partner" do
            get "/api/v1/partners/#{@partner.id}", params: nil, headers: headers

            expect(response).to have_http_status(:ok)
        end

        it "updates the partner" do
            client = double("Salesforce::Partner", :create => {:success => true})
            expect(Salesforce::Partner).to receive(:new).and_return(client)
            allow(client).to receive(:update_partner)
            put "/api/v1/partners/#{@partner.id}", params: {:partner => @partner.attributes.symbolize_keys.except(:id)}, headers: headers

            expect(response).to have_http_status(:ok)
        end

        it "imports users" do
            post "/api/v1/partners/#{@partner.id}/import", params: {:users => [{"first_name" => "Mike"}], :role => "agent"}, headers: headers
            expect(response).to have_http_status(:ok)
        end

        it "gets analytics_overview" do
            get "/api/v1/partners/#{@partner.id}/analytics/overview", params: {}, headers: headers
            expect(response).to have_http_status(:ok)
        end

        it "gets analytics_logins" do
            get "/api/v1/partners/#{@partner.id}/analytics/logins", params: {}, headers: headers

            expect(response).to have_http_status(:ok)
        end

        it "gets analytics_reminders" do
            get "/api/v1/partners/#{@partner.id}/analytics/maintenance_reminders", params: {}, headers: headers

            expect(response).to have_http_status(:ok)
        end

        it "gets download_reminders" do
            get "/api/v1/partners/#{@partner.id}/analytics/maintenance_reminders/download", params: {}, headers: headers
            expect(response).to have_http_status(:ok)
        end

        it "gets widget_exclusions" do
            get "/api/v1/partners/#{@partner.id}/widget_exclusions", params: {}, headers: headers
            expect(response).to have_http_status(:ok)
        end
        it "gets footprints" do
            get "/api/v1/partners/#{@partner.id}/footprints", params: {}, headers: headers
            expect(response).to have_http_status(:no_content)
        end
    end

    context "when an admin" do
        it "gets all the partners" do
            get "/api/v1/partners", params: nil, headers: admin_headers
            expect(response).to have_http_status(:ok)
        end
        
        it "deletes a partner" do
            @partner = create(:partner)
            delete "/api/v1/partners/#{@partner.id}", params: nil, headers: admin_headers
            expect(response).to have_http_status(:no_content)
        end
    end
end
