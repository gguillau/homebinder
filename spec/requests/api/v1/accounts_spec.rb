require 'rails_helper'

RSpec.describe "Accounts", :type => :request do
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, email: "user@homebinder.com", :role => "admin") }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) { { 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt  } }
  
  describe "Accounts" do
    before do
      allow(File).to receive(:read).and_call_original
      
    end
    
    it "index accounts" do
      get "/api/v1/accounts", params: nil, headers: headers
    
      expect(response).to have_http_status(:ok)
    end
    
    it "get an account" do
      account = create(:account)
      get "/api/v1/accounts/#{account.manager_id}", params: nil, headers: headers
    
      expect(response).to have_http_status(:ok)
    end
    
    it "raises an invalid request route error" do
      manager = create(:partner)
      args = {
        account: {
          manager_id: manager.id,
          name: "test",
          account_type: "single",
          account_sub_type: "free_trial",
          account_status: "active",
          trial_expiration: Time.now + 30.days
        }
      }
      
      post "/api/v1/accountsssss", params: args, headers: headers
      
      message = JSON.parse(response.body)
      expect(response).to have_http_status(:forbidden)
      expect(message["message"]).to eq("Invalid route")
    end
    
    it "creates an account" do
      manager = create(:partner)
      args = {
        account: {
          manager_id: manager.id,
          name: "test",
          account_type: "single",
          account_sub_type: "free_trial",
          account_status: "active",
          trial_expiration: Time.now + 30.days
        }
      }
      
      post "/api/v1/accounts", params: args, headers: headers
      
      expect(response).to have_http_status(:ok)
    end
    
    it "updates an account" do
      partner = create(:partner)
      update = create(:account, manager_id: partner.id)
      account = {
        name: "update",
        trial_expiration: Time.now + 30.days
      }
      
      put "/api/v1/accounts/#{update.id}", params: {account: account}, headers: headers
      expect(response).to have_http_status(:ok)
    end
    
    it "deletes an account" do
      account = create(:account)
      
      delete "/api/v1/accounts/#{account.id}", params: nil, headers: headers
      
      expect(response).to have_http_status(:no_content)
    end
  end
end