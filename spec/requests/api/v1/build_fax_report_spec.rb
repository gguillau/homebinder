require 'rails_helper'

RSpec.describe "BuildFaxReport", :type => :request do

    let(:admin) { FactoryBot.create(:user, :role => "admin") }
    let(:binder) { FactoryBot.create(:binder) }
    let(:build_fax_report) { FactoryBot.create(:build_fax_report) }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(admin) }
    let(:file) { {version: "0.0.0"} }
    
    let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
    
    before {}
    
    it "gets all build_fax_reports" do
        get "/api/v1/build_fax_reports", params: {:orderBy => "id"}, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "gets a build_fax_report" do
        get "/api/v1/build_fax_reports/#{build_fax_report.id}", params: nil, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "creates a build_fax_report" do
        buildfax = Buildfax::Request.new
        allow(Buildfax::Request).to receive(:new).and_return(buildfax)

        file = File.open("#{Rails.root}/spec/assets/SampleDoc.pdf", "rb")
        contents = file.read
        allow(buildfax).to receive(:download_report).and_return(contents)
        build_fax_report = {
            :address => "12 Main Street",
            :city => "Boston",
            :state => "MA",
            :zip => "02210"
        }
                post "/api/v1/build_fax_reports", params: {build_fax_report: build_fax_report}, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "updates a build_fax_report" do
        put "/api/v1/build_fax_reports/#{build_fax_report.id}", params: {build_fax_report: {status: "paid" }}, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "deletes a build_fax_report" do
        delete "/api/v1/build_fax_reports/#{build_fax_report.id}", params: nil, headers: headers
        expect(response).to have_http_status(:no_content)
    end
  
end