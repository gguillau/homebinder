require 'rails_helper'

RSpec.describe "Events", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:admin) { FactoryBot.create(:user, :role => "admin") }
  let(:jwt) { UserTokenService.create_jwt(admin) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    allow(File).to receive(:read).and_call_original
    
  end
  
  it "creates the event" do
    post "/api/v1/events", params: {:event => {:event_name => "test"}}, headers: headers
    expect(response).to have_http_status(:no_content)
  end

end