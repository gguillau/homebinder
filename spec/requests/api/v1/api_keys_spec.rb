require 'rails_helper'

RSpec.describe "API Keys", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:partner_key) {FactoryBot.create(:api_key, key: "TEST",company_name: partner.name, partner_id: partner.id, application_name: "Website", contact_email: partner.email)}
  let(:admin) { FactoryBot.create(:user, :role => "admin") }
  let(:jwt) { UserTokenService.create_jwt(admin) }
  let(:partner) { FactoryBot.create(:partner) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    
    admin.add_role :admin
  end
  
  it "creates a new api key" do
    post "/api/v1/api_keys", params: {partner: {company_name: partner.name, partner_id: partner.id, application_name: "Website", contact_email: partner.email}}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "updates a new api key" do
    partner = {
      company_name: partner_key.company_name, 
      partner_id: partner_key.partner_id, 
      contact_email: partner_key.contact_email
    }
    post "/api/v1/api_keys", params: {partner: partner}, headers: headers
    expect(response).to have_http_status(:ok)
  end


end