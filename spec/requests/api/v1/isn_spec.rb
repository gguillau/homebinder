require 'rails_helper'
require 'webmock/rspec'

RSpec.describe "ISN", :type => :request do
    
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:user) { FactoryBot.create(:user, email: "user@homebinder.com", :role => "inspector") }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    let!(:partner){FactoryBot.create(:partner)}
    let(:item) { FactoryBot.create(:isn_user, :partner_configuration_id => partner.partner_configuration.id)}
    let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}

    before :each do
        PartnerUser.create!(:partner => partner, :user => user, :role => "admin")
    end

    it "index isn_users" do
        get "/api/v1/isn_users", params: {:partner_configuration_id => partner.partner_configuration.id}, headers: headers

        expect(response).to have_http_status(:ok)
    end

    it "get an isn_user" do
        get "/api/v1/isn_users/#{item.id}", params: nil, headers: headers
        expect(response).to have_http_status(:ok)
    end

    it "creates an isn_user" do
        isn_rake = Isn::RakeJob.new
        allow(Isn::RakeJob).to receive(:new).and_return(isn_rake)
        allow(isn_rake).to receive(:get_footprints)
    
        args = {
            isn_user: {
                partner_configuration_id: partner.partner_configuration.id,
                company_key: "test",
                username: "bsmith",
                password: "password",
                name: "Bob Smith"
            }
        }

        post "/api/v1/isn_users", params: args, headers: headers
        expect(response).to have_http_status(:ok)
    end

    it "updates an isn_user" do
        isn_user = {
            name: "update",
        }

        put "/api/v1/isn_users/#{item.id}", params: {isn_user: isn_user}, headers: headers
        expect(response).to have_http_status(:ok)
    end

    it "deletes an isn_user" do
        delete "/api/v1/isn_users/#{item.id}", params: nil, headers: headers
        expect(response).to have_http_status(:no_content)
    end
end
