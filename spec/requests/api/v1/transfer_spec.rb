require 'rails_helper'
require 'mandrill'

RSpec.describe "Transfer", :type => :request do

    let!(:api_key) { FactoryBot.create(:api_key) }
    let!(:user) { FactoryBot.create(:user, email: "user@homebinder.com", :role => "homeowner") }
    let!(:binder) { FactoryBot.create(:binder) }
    let!(:receiver) { FactoryBot.create(:user) }
    let!(:jwt) { UserTokenService.create_jwt(user) }
    let!(:file) { {version: "0.0.0"} }
    let!(:subscription) {FactoryBot.create(:subscription, binder_id: binder.id, customer_id: "1", plan_id: "free")}
    
    let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
    
    before :each do
        allow(File).to receive(:read)
        UserBinder.create(:binder_id => binder.id, :user_id => user.id, :role => "owner")
        binder.subscription = subscription
        binder.save
    end
    
    it "creates a transfer" do
        transfer = {"transfer_type"=>"ownership", "binder_id"=>binder.id, "status"=>"created", "sender_id"=>1, "receiver_id"=>3}
        post "/api/v1/transfers", params: {transfer: transfer}, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "gets a transfer" do
        transfer = create(:transfer, :sender_id => user.id, :receiver_id => receiver.id, :binder_id => binder.id, :transfer_type => "ownership", :status => "created")
        get "/api/v1/transfers/#{transfer.id}", params: nil, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "updates a transfer" do
        transfer = create(:transfer, :sender_id => user.id, :receiver_id => receiver.id, :binder_id => binder.id, :transfer_type => "ownership", :status => "created")
        put "/api/v1/transfers/#{transfer.id}", params: {transfer: transfer.attributes.symbolize_keys.except(:id)}, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "resends a transfer" do
        transfer = create(:transfer, :sender_id => user.id, :receiver_id => receiver.id, :binder_id => binder.id, :transfer_type => "ownership", :status => "sent")
        put "/api/v1/transfers/#{transfer.id}/resend", params: {user: {first_name: "test", last_name: "name", email: "test@gmail.com", phone: "16175002600"}}, headers: headers

        expect(response).to have_http_status(:ok)
    end
    
    it "deletes a transfer" do
        transfer = create(:transfer, :sender_id => user.id, :receiver_id => receiver.id, :binder_id => binder.id, :transfer_type => "ownership", :status => "created")
        delete "/api/v1/transfers/#{transfer.id}", params: nil, headers: headers
        expect(response).to have_http_status(:no_content)
    end
  
end