require 'rails_helper'

RSpec.describe "Binders", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, email: "user@homebinder.com", :role => "admin") }
  let(:binder) { FactoryBot.create(:binder)}
  let(:subscription) {FactoryBot.create(:subscription, binder_id: binder.id, plan_id: "free")}
  let(:appliance) {FactoryBot.create(:appliance, binder_id: binder.id, created_by: user.id)}
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    allow(File).to receive(:read).and_call_original
    
    binder.subscription = subscription
    UserBinder.create(:binder_id => binder.id, :user_id => user.id, :role => "owner")
  end

  it "gets all binders" do
    get "/api/v1/binders", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "gets the binder" do
    get "/api/v1/binders/#{binder.id}", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "updates the binder" do
    put "/api/v1/binders/#{binder.id}", params: {binder: binder.attributes.symbolize_keys.except(:id)}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "deletes the binder" do
    delete "/api/v1/binders/#{binder.id}", params: nil, headers: headers
    expect(response).to have_http_status(:no_content)
  end
  
  it "orphans the binder" do
    put "/api/v1/binders/#{binder.id}/orphan", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "sends feedback on acceptance of the binder" do
    put "/api/v1/binders/#{binder.id}/feedback", params: nil, headers: headers
    expect(response).to have_http_status(:no_content)
  end
  
  it "requests services on acceptance of the binder" do
    Sidekiq::Testing.disable!
    put "/api/v1/binders/#{binder.id}/services", params: {:services => ["Help with Stuff"]}, headers: headers
    expect(response).to have_http_status(:no_content)
  end
  
  it "declines services on acceptance of the binder" do
    Sidekiq::Testing.disable!
    delete "/api/v1/binders/#{binder.id}/services", params: nil, headers: headers
    expect(response).to have_http_status(:no_content)
  end
  
  it "requests more info for apr" do
    put "/api/v1/binders/#{binder.id}/aprWidget", params: nil, headers: headers
    expect(response).to have_http_status(:no_content)
  end

end