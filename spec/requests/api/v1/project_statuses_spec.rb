require 'rails_helper'

RSpec.describe "ProjectStatus", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:admin) { FactoryBot.create(:user, :role => "admin") }
  let(:jwt) { UserTokenService.create_jwt(admin) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    
  end
  
  
  it "gets all the project_statuses" do
    get "/api/v1/project_statuses", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end

end