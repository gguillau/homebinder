require 'rails_helper'

RSpec.describe "Receipts", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, email: "user@homebinder.com", role: "homeowner") }
  let(:binder) { FactoryBot.create(:binder)}
  let(:subscription) {FactoryBot.create(:subscription, binder_id: binder.id, customer_id: "1", plan_id: "free")}
  let(:receipt) {FactoryBot.create(:receipt, binder_id: binder.id, created_by: user.id)}
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    
    binder.subscription = subscription
    UserBinder.create(:binder_id => binder.id, :user_id => user.id, :role => "owner")
  end

  it "gets all receipt for binder" do
    get "/api/v1/receipts", params: {binderId: binder.id}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "gets the receipt" do
    get "/api/v1/receipts/#{receipt.id}", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "creates a receipt" do
    post "/api/v1/receipts", params: {receipt: receipt.attributes.symbolize_keys.except(:id)}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "updates a receipt" do
    put "/api/v1/receipts/#{receipt.id}", params: {receipt: receipt.attributes.symbolize_keys.except(:id)}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "deletes a receipt" do
    delete "/api/v1/receipts/#{receipt.id}", params: nil, headers: headers
    expect(response).to have_http_status(:no_content)
  end
  
end