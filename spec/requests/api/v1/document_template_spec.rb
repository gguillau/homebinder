require 'rails_helper'

RSpec.describe "Document Template", :type => :request do
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, email: "user@homebinder.com", :role => "inspector") }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  let(:document_type) { FactoryBot.create(:document_type) }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    
    @partner = create(:partner)
    @config = @partner.partner_configuration
    @template = create(:binder_template, partner_configuration_id: @config.id)
  end
  
  describe "index" do
    it "gets binder template document templates" do
      user.add_role :partner_admin, @partner
      get "/api/v1/binder_templates/#{@template.id}/document_templates", params: nil, headers: headers
      
      expect(response).to have_http_status(:ok)
    end
  end

  describe "show" do
    it "gets a document template" do
      user.add_role :partner_admin, @partner
      d = create(:document_template, binder_template_id: @template.id)
      get "/api/v1/document_templates/#{d.id}", params: nil, headers: headers
      
      expect(response).to have_http_status(:ok)
    end
  end
  
  describe "create" do
    it "creates a binder template document template" do
      user.add_role :partner_admin, @partner
      file = Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleDoc.pdf'), 'application/pdf')
      
      data = {
        binder_template_id: @template.id,
        document_template: {
          file: file,
          binder_template_id: @template.id,
          document_type_id: document_type.id
        }
      }
      
      post "/api/v1/binder_templates/#{@template.id}/document_templates", params: data, headers: headers
      
      file.close
      
      expect(response).to have_http_status(:ok)
    end
  end
  
  describe "update" do
    it "updates a document template" do
      user.add_role :partner_admin, @partner
      d = create(:document_template, binder_template_id: @template.id)
      
      data = {
        id: d.id,
        document_template: {
          id: d.id,
          notes: "notes"
        }
      }
      
      put "/api/v1/document_templates/#{d.id}", params: data, headers: headers
      
      expect(response).to have_http_status(:ok)
    end
  end
  
  describe "delete" do
    it "destroys an document template" do
      user.add_role :partner_admin, @partner
      d = create(:document_template, binder_template_id: @template.id)
      
      delete "/api/v1/document_templates/#{d.id}", params: nil, headers: headers
      
      expect(response).to have_http_status(:no_content)
    end
  end
  
end