require 'rails_helper'

RSpec.describe "Dashboard", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, role: UserGlobalRoles::ADMIN) }
  let(:partner) { FactoryBot.create(:partner) }
  let(:org) { FactoryBot.create(:organization) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    allow(File).to receive(:read).and_call_original
    
  end

  it "index" do
    get "/api/v1/dashboards", headers: headers
    
    expect(response).to have_http_status(:ok)
  end
  
  it "index system dashboards" do
    get "/api/v1/system/dashboards", headers: headers
    
    expect(response).to have_http_status(:ok)
  end
  
  it "index user dashboards" do
    get "/api/v1/users/#{user.id}/dashboards", headers: headers
    
    expect(response).to have_http_status(:ok)
  end
  
  it "index partner dashboards" do
    inspector = create(:user, :role => "inspector")
    create(:partner_user, partner_id: partner.id, user_id: inspector.id)
    get "/api/v1/partners/#{partner.id}/dashboards", headers: headers
    
    expect(response).to have_http_status(:ok)
  end
  
  it "index organization dashboards" do
    create(:organization_user, organization_id: org.id, user_id: user.id)
    get "/api/v1/organizations/#{org.id}/dashboards", headers: headers
    
    expect(response).to have_http_status(:ok)
  end
  
  it "show" do
    dashboard = create(:dashboard)

    get "/api/v1/dashboards/#{dashboard.id}", headers: headers
    
    expect(response).to have_http_status(:ok)
  end
  
  it "create" do
    post "/api/v1/dashboards", params: { dashboard: { name: "board", system: true, scope: DashboardScope::BINDER }}, headers: headers
    
    expect(response).to have_http_status(:ok)
  end
  
  it "update" do
    board = create(:dashboard)
    
    put "/api/v1/dashboards/#{board.id}", params: { dashboard: { name: "board" }}, headers: headers
    
    expect(response).to have_http_status(:ok)
  end
  
  it "delete" do
    board = create(:dashboard)
    
    delete "/api/v1/dashboards/#{board.id}", headers: headers
    
    expect(response).to have_http_status(:no_content)
  end
  
end