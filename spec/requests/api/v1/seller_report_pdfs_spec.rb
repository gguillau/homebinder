require 'rails_helper'

RSpec.describe "Seller Report PDFs", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, email: "user@homebinder.com", role: "homeowner") }
  let(:binder) { FactoryBot.create(:binder)}
  let(:seller_report) { FactoryBot.create(:seller_report, :binder_id => binder.id)}
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }

  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    service = SellerReportPdfService.new(seller_report)
    service.init
    
    allow_any_instance_of(FullSellerReportPdfService).to receive(:create).and_return(service.pdf)
    allow_any_instance_of(SellerReportPdfService).to receive(:create).and_return(service.pdf)
    UserBinder.create(:user_id => user.id, :binder_id => binder.id, role: "owner")
  end

  
  it "gets the PDF report" do
    get "/api/v1/seller_report_pdfs/#{seller_report.code}", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "gets the full PDF report" do
    get "/api/v1/seller_report_pdfs/#{seller_report.code}", params: {full: true}, headers: headers
    expect(response).to have_http_status(:ok)
  end

end