require 'rails_helper'

RSpec.describe "Coupons", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user) }
  let(:partner) { FactoryBot.create(:partner) }
  let(:coupon) { FactoryBot.create(:coupon, partner_id: partner.id, code: "PARTNER#{partner.id}FREELIFE", duration: "forever") }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    allow(File).to receive(:read).and_call_original
    
    allow(Stripe::Coupon).to receive(:retrieve).and_return(coupon)
    PartnerUser.create(:partner_id => partner.id, :user_id => user.id, :role => "admin")
  end
  
  
  it "gets the coupon" do
    get "/api/v1/coupons/#{coupon.code}", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end

end