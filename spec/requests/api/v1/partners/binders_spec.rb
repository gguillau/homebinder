require 'rails_helper'

RSpec.describe "Partner ABS API Binders", :type => :request do
    let(:partner) { FactoryBot.create(:partner, name: "Test Inspections", contact: "Test Inspector", partner_key: "axium", :partner_type => "inspector") }
    let(:user) { FactoryBot.create(:user, email: partner.email, :role => "inspector") }
    let(:partner_key) { FactoryBot.create(:api_key, partner_id: partner.id, contact_email: "test@gmail.com") }
    let(:config_two) { partner.partner_configuration}
    let(:template_two) { FactoryBot.create(:binder_template, partner_configuration_id: config_two.id) }
    let(:file) { {version: "0.0.0"} }

    let(:headers) {{ 'HTTP_HB_APIKEY' => partner_key.key }}

    before :each do
        allow(File).to receive(:read).and_call_original
        PartnerUser.create(:partner_id => partner.id, :user_id => user.id, :role => "admin")
    end

    it "creates a binder" do
        client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
        property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
        agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}

        parameters = {
            :client => client,
            :property => property,
            :binder_template_id => template_two.id,
            :agent => agent,
            :transfer => false,
        }

        allow(JSON).to receive(:parse).and_call_original
        allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

        post "/api/v1/partners/axium/binders", params: parameters, headers: headers
        expect(response).to have_http_status(:created)
    end
end
