require 'rails_helper'

RSpec.describe "Share", :type => :request do

    let!(:api_key) { FactoryBot.create(:api_key) }
    let!(:user) { FactoryBot.create(:user, email: "user@homebinder.com", role: "homeowner") }
    let!(:binder) { FactoryBot.create(:binder) }
    let!(:with) { FactoryBot.create(:user) }
    let!(:jwt) { UserTokenService.create_jwt(user) }
    let!(:file) { {version: "0.0.0"} }
    
    let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
    
    before :each do
        subscription = create(:subscription, binder_id: binder.id, customer_id: "1", plan_id: "free")
        UserBinder.create(:binder_id => binder.id, :user_id => user.id, :role => "owner")
        binder.subscription = subscription
        binder.save
        allow(File).to receive(:read).and_call_original
    end
    
    it "creates a share" do
        share = {
            user: {
                first: "first",
                last: "first",
                email: "someuser@homebinder.com"
            },
            binder_id: binder.id,
            role_name: "co_owner"
        }
        post "/api/v1/shares", params: {:share => share}, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "gets a share" do
        share = create(:share, status: "sent", shared_by_id: user.id, shared_with_id: with.id, role_name: "co_owner", binder_id: binder.id)
        get "/api/v1/shares/#{share.id}", params: nil, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "updates a share" do
        share = create(:share, status: "sent", shared_by_id: user.id, shared_with_id: with.id, role_name: "co_owner", binder_id: binder.id)

        put "/api/v1/shares/#{share.id}", params: {:share => share.attributes.symbolize_keys.except(:id)}, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "deletes a share" do
        share = create(:share, status: "sent", shared_by_id: user.id, shared_with_id: with.id, role_name: "co_owner", binder_id: binder.id)

        delete "/api/v1/shares/#{share.id}", params: nil, headers: headers
        expect(response).to have_http_status(:no_content)
    end
end