require 'rails_helper'

RSpec.describe "Widget", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, role: UserGlobalRoles::ADMIN) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    allow(File).to receive(:read).and_call_original
    
  end

  it "index" do
    get "/api/v1/widgets", headers: headers
    
    expect(response).to have_http_status(:ok)
  end
  
  it "show" do
    widget = create(:widget)

    get "/api/v1/widgets/#{widget.id}", headers: headers
    
    expect(response).to have_http_status(:ok)
  end
  
  it "create" do
    user.add_role :admin
    
    post "/api/v1/widgets", params: { widget: { name: "widget", key: "widget", category: "General" }}, headers: headers
    
    expect(response).to have_http_status(:ok)
  end
  
  it "update" do
    user.add_role :admin
    widget = create(:widget)
    
    put "/api/v1/widgets/#{widget.id}", params: { widget: { id: widget.id, name: "widget", key: "widget", category: "General" }}, headers: headers
    
    expect(response).to have_http_status(:ok)
  end
  
  it "delete" do
    user.add_role :admin
    widget = create(:widget)
    
    delete "/api/v1/widgets/#{widget.id}", headers: headers
    
    expect(response).to have_http_status(:no_content)
  end
  
end