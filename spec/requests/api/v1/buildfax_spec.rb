require 'rails_helper'

RSpec.describe "Buildfax", :type => :request do

    let(:api_key) { FactoryBot.create(:api_key) }
    let(:user) { FactoryBot.create(:user, email: "user@homebinder.com") }
    let(:binder) { FactoryBot.create(:binder)}
    let(:subscription) {FactoryBot.create(:subscription, binder_id: binder.id, customer_id: "1", plan_id: "free")}
    let(:permit) {FactoryBot.create(:permit, binder_id: binder.id, created_by: user.id)}
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    
    let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
    
    before :each do
        binder.subscription = subscription
        UserBinder.create(:binder_id => binder.id, :user_id => user.id, :role => "owner")
    end
    
    it "gets all permits for binder" do
        address = "#{binder.property.address1}, #{binder.property.city}, #{binder.property.state} #{binder.property.zip}"
        url = "https://delivery.buildfax.com/api/reports/addresses/%s/?output=xml&template=HomeBinder_Dev" % address
        body = "<?xml version='1.0' encoding='UTF-8'?>\n<buildFaxReport display=\"BuildFax report\">\n  <reportHeader display=\"report header\">\n    <serialNumber display=\"serial number\" type=\"string\">20170102193116787101-NIPSTY-98449823</serialNumber>\n    <version display=\"version\" type=\"integer\">20161223</version>\n  </reportHeader>\n  <reportBody display=\"report body\">\n    <requestedAddress display=\"requested addresss\">\n      <fullAddress display=\"full address\" type=\"string\">714 Bartoletti Trail, Blandaside, GA 10038-3082</fullAddress>\n    </requestedAddress>\n  </reportBody>\n</buildFaxReport>\n"
        stub_request(:get, URI.escape(url)).to_return(:status => 200, :body => body)
        get "/api/v1/buildfax/#{binder.id}", params: {}, headers: headers
        expect(response).to have_http_status(:ok)
    end
    
    it "creates permits for binder" do
        post "/api/v1/buildfax/#{binder.id}", params: {permits: [{:permit_number => "MB39-1", :contractors => ["Test"]}]}, headers: headers
        expect(response).to have_http_status(:ok)
    end

  
end