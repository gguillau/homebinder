require 'rails_helper'

RSpec.describe "Images", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, email: "user@homebinder.com") }
  let(:binder) { FactoryBot.create(:binder)}
  let(:subscription) {FactoryBot.create(:subscription, binder_id: binder.id, customer_id: "1", plan_id: "free")}
  let(:image) {FactoryBot.create(:image, binder_id: binder.id, created_by: user.id)}
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    
    binder.subscription = subscription
    UserBinder.create(:binder_id => binder.id, :user_id => user.id, :role => "owner")
  end

  it "gets all images for binder" do
    get "/api/v1/images", params: {binderId: binder.id}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "gets the image" do
    get "/api/v1/images/#{image.id}", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "creates an image" do
    post "/api/v1/images", params: {image: image.attributes.symbolize_keys.except(:id), binder_id: binder.id}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "updates an image" do
    put "/api/v1/images/#{image.id}", params: {image: {id: image.id, binder: {id: binder.id}}}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "deletes an image" do
    delete "/api/v1/images/#{image.id}", params: nil, headers: headers
    expect(response).to have_http_status(:no_content)
  end
  
end