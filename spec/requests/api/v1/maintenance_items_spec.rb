require 'rails_helper'

RSpec.describe "MaintenanceItems", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user, email: "user@homebinder.com") }
  let(:binder) { FactoryBot.create(:binder)}
  let(:subscription) {FactoryBot.create(:subscription, binder_id: binder.id, customer_id: "1", plan_id: "free")}
  let(:maintenance_item) {FactoryBot.create(:maintenance_item, binder_id: binder.id, created_by: user.id)}
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    allow(File).to receive(:read).and_call_original
    
    binder.subscription = subscription
    UserBinder.create(:binder_id => binder.id, :user_id => user.id, :role => "owner")
  end

  it "gets all maintenance_items for binder" do
    get "/api/v1/maintenance_items", params: {binderId: binder.id}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "gets the maintenance_item" do
    get "/api/v1/maintenance_items/#{maintenance_item.id}", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "creates a maintenance_item" do
    item = {
      name: "TES",
      binder_id: binder.id
    }
    post "/api/v1/maintenance_items", params: {maintenance_item: item}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "updates a maintenance_item" do
    put "/api/v1/maintenance_items/#{maintenance_item.id}", params: {maintenance_item: maintenance_item.attributes.symbolize_keys.except(:id)}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "deletes a maintenance_item" do
    delete "/api/v1/maintenance_items/#{maintenance_item.id}", params: nil, headers: headers
    expect(response).to have_http_status(:no_content)
  end
  
end