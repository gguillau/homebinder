require 'rails_helper'

RSpec.describe "Subscriptions", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:admin) { FactoryBot.create(:user, :role => "admin") }
  let(:jwt) { UserTokenService.create_jwt(admin) }
  let(:binder) { FactoryBot.create(:binder) }
  let(:binder_2) { FactoryBot.create(:binder) }
  let(:subscription) {FactoryBot.create(:subscription, binder_id: binder.id)}
  let(:subscription_two) {FactoryBot.create(:subscription, binder_id: binder_2.id)}
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    
    binder.subscription = subscription
    binder_2.subscription = subscription_two
  end
  
  it "gets all the subscription" do
    get "/api/v1/subscriptions", params: {binder_id: binder.id}, headers: headers
    expect(response).to have_http_status(:ok)
  end

  it "gets the subscription" do
    get "/api/v1/subscriptions/#{subscription.id}", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "updates the subscription when an upgrade" do
    put "/api/v1/subscriptions/#{subscription.id}", params: {action: "upgrade"}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "updates the subscription when an update" do
    put "/api/v1/subscriptions/#{subscription.id}", params: {action: "update"}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "updates the subscription when a cancel" do
    put "/api/v1/subscriptions/#{subscription.id}", params: {action: "cancel"}, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "deletes the subscription" do
    delete "/api/v1/subscriptions/#{subscription_two.id}", params: nil, headers: headers
    expect(response).to have_http_status(:no_content)
  end

end