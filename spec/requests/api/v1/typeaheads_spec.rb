require 'rails_helper'

# these tests verify the routes map properly to retrieve typeaheads

RSpec.describe "Typeaheads", :type => :request do
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:user) { FactoryBot.create(:user) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    
  end
  
  describe "GET /api/v1/construction_types" do
    it "gets construction types" do
      t = create(:construction_type)
      
      get "/api/v1/construction_types", params: nil, headers: headers
      
      json = JSON.parse(response.body)
      
      expect(response).to be_success
      expect(json.length).to eq(1)
      expect(json[0]).to eq(t.name)
    end
  end
  
  describe "GET /api/v1/heat_types" do
    it "gets heat types" do
      t = create(:heat_type)
      
      get "/api/v1/heat_types", params: nil, headers: headers
      
      json = JSON.parse(response.body)
      
      expect(response).to be_success
      expect(json.length).to eq(1)
      expect(json[0]).to eq(t.name)
    end
  end
  
  describe "GET /api/v1/heat_sources" do
    it "gets heat sources" do
      t = create(:heat_source)
      
      get "/api/v1/heat_sources", params: nil, headers: headers
      
      json = JSON.parse(response.body)
      
      expect(response).to be_success
      expect(json.length).to eq(1)
      expect(json[0]).to eq(t.name)
    end
  end
  
  describe "GET /api/v1/area_types" do
    it "gets area types" do
      t = create(:area_type)
      
      get "/api/v1/area_types", params: nil, headers: headers
      
      json = JSON.parse(response.body)
      
      expect(response).to be_success
      expect(json.length).to eq(1)
      expect(json[0]).to eq(t.name)
    end
  end
  
  describe "GET /api/v1/paint_manufacturers" do
    it "gets paint manufacturers" do
      t = create(:paint_manufacturer)
      
      get "/api/v1/paint_manufacturers", params: nil, headers: headers
      
      json = JSON.parse(response.body)
      
      expect(response).to be_success
      expect(json.length).to eq(1)
      expect(json[0]).to eq(t.name)
    end
  end
  
  describe "GET /api/v1/paint_types" do
    it "gets paint types" do
      t = create(:paint_type)
      
      get "/api/v1/paint_types", params: nil, headers: headers
      
      json = JSON.parse(response.body)
      
      expect(response).to be_success
      expect(json.length).to eq(1)
      expect(json[0]).to eq(t.name)
    end
  end
  
  describe "GET /api/v1/project_types" do
    it "gets project types" do
      t = create(:project_type)
      
      get "/api/v1/project_types", params: nil, headers: headers
      
      json = JSON.parse(response.body)
      
      expect(response).to be_success
      expect(json.length).to eq(1)
      expect(json[0]).to eq(t.name)
    end
  end
  
  describe "GET /api/v1/contractor_types" do
    it "gets contractor types" do
      t = create(:contractor_contractor_category)
      
      get "/api/v1/contractor_types", params: nil, headers: headers
      
      json = JSON.parse(response.body)
      
      expect(response).to be_success
      expect(json["total"]).to eq(1)
      expect(json["items"][0]["name"]).to eq(t.name)
    end
  end
  
  describe "GET /api/v1/appliance_manufacturers" do
    it "gets paint types" do
      t = create(:appliance_manufacturer)
      
      get "/api/v1/appliance_manufacturers", params: nil, headers: headers
      
      json = JSON.parse(response.body)
      expect(response).to be_success
      expect(json.length).to eq(1)
      expect(json[0]).to eq(t.name)
    end
  end
  
  describe "GET /api/v1/appliance_models" do
    it "gets appliance models" do
      t = create(:appliance_model)
      
      get "/api/v1/appliance_models", params: nil, headers: headers
      
      json = JSON.parse(response.body)
      
      expect(response).to be_success
      expect(json.length).to eq(1)
      expect(json[0]).to eq(t.name)
    end
  end
  
  describe "GET /api/v1/inventory_item_types" do
    it "gets inventory item types" do
      t = create(:inventory_item_type)
      
      get "/api/v1/inventory_item_types", params: nil, headers: headers
      
      json = JSON.parse(response.body)
      
      expect(response).to be_success
      expect(json.length).to eq(1)
      expect(json[0]).to eq(t.name)
    end
  end
  
  describe "GET /api/v1/finish_makes" do
    it "gets finish makes" do
      t = create(:finish_make)
      
      get "/api/v1/finish_makes", params: nil, headers: headers
      
      json = JSON.parse(response.body)
      
      expect(response).to be_success
      expect(json.length).to eq(1)
      expect(json[0]).to eq(t.name)
    end
  end
  
  describe "GET /api/v1/finish_models" do
    it "gets finish models" do
      t = create(:finish_model)
      
      get "/api/v1/finish_models", params: nil, headers: headers
      
      json = JSON.parse(response.body)
      
      expect(response).to be_success
      expect(json.length).to eq(1)
      expect(json[0]).to eq(t.name)
    end
  end
  
  describe "GET /api/v1/appliance_manufacturers/unverified" do
    it "gets unverified appliance_manufacturers" do
      a = create(:appliance_manufacturer, verified: false)
      
      get "/api/v1/appliance_manufacturers/unverified", params: nil, headers: headers
      
      json = JSON.parse(response.body)
      
      expect(response).to be_success
      expect(json.length).to eq(1)
      expect(json[0]).to eq(a.attributes)
    end
  end
  
  describe "PUT /api/v1/appliance_manufacturers/:id" do
    it "updates the verified appliance_manufacturers" do
      a = create(:appliance_manufacturer, verified: false)
      a.verified = true
      put "/api/v1/appliance_manufacturers/#{a.id}", params: {typeahead: a.attributes.symbolize_keys.except(:id)}, headers: headers

      expect(response).to be_success
      appliance_manufacturer = Binder::Appliance::Manufacturer.find(a.id)
      expect(appliance_manufacturer.verified).to eq(true)
    end
  end
  
  describe "DELETE /api/v1/appliance_manufacturers/:id" do
    it "updates the verified appliance_manufacturers" do
      a = create(:appliance_manufacturer, verified: false)

      delete "/api/v1/appliance_manufacturers/#{a.id}", params: nil, headers: headers

      expect(response).to have_http_status(:no_content)
    end
  end
  
end