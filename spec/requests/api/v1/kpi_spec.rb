require 'rails_helper'

RSpec.describe "KPI", :type => :request do

  let(:api_key) { FactoryBot.create(:api_key) }
  let(:admin) { FactoryBot.create(:user, :role => "admin") }
  let(:jwt) { UserTokenService.create_jwt(admin) }
  let(:file) { {version: "0.0.0"} }
  
  let(:headers) {{ 'HTTP_HB_APIKEY' => api_key.key, 'HB-UserToken' => jwt }}
  
  before :each do
    
    admin.add_role :admin
  end
  
  
  it "gets KPIs for binders" do
    get "/api/v1/kpi/binders", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "gets KPI Summary" do
    get "/api/v1/kpi/summary", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end
   
  it "gets KPIs for users" do
    get "/api/v1/kpi/users", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "gets KPIs for Inspector Engagement" do
    get "/api/v1/kpi/engagement/inspector", params: nil, headers: headers
    expect(response).to have_http_status(:ok)
  end
  
  it "gets KPI PDF report" do
    allow_any_instance_of(Kpi::Report).to receive(:get_report).and_return ""
    get "/api/v1/kpi/download", params: nil, headers: headers
    expect(response).to have_http_status(:no_content)
  end
  
  it "gets the account_summaries" do
    get "/api/v1/kpi/account_summaries", params: nil, headers: headers
    expect(response).to have_http_status(:no_content)
  end

end