require 'rails_helper'

RSpec.describe Templates::AddDefaultItemsJob do
    let(:job) {Templates::AddDefaultItemsJob.new}

    describe "perform" do
        it "calls add_default_items" do
            binder_template = create(:binder_template)
            expect_any_instance_of(BinderTemplate).to receive(:add_default_items)
            job.perform(binder_template.id)
        end
    end
end
