require 'rails_helper'

RSpec.describe Templates::ResetDatesJob do
    let(:job) {Templates::ResetDatesJob.new}

    describe "perform" do
        it "resets dates" do
            template = create(:binder_template)
            maintenance_template = create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::QUARTERLY, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::MONTHLY, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::EVERY_OTHER, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::EVERY_THREE, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::EVERY_FOUR, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::EVERY_FOURTY, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::AS_NEEDED, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::ANNUAL, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::SEMI_ANNUAL, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::EVERY_FIVE, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::EVERY_TEN, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::ONCE, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            due_date = maintenance_template.due_date
            template.add_default_items
            job.perform
            maintenance_template.reload
            expect(maintenance_template.due_date.to_date).to eq((due_date + 3.months).to_date)
        end

        it "calls new relic" do
            allow(ErrorService).to receive(:perform_async)

            template = create(:binder_template)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::QUARTERLY, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::MONTHLY, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::EVERY_OTHER, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::EVERY_THREE, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::EVERY_FOUR, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::EVERY_FOURTY, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::AS_NEEDED, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::ANNUAL, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::SEMI_ANNUAL, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::EVERY_FIVE, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::EVERY_TEN, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            create(:maintenance_template, :frequency => Binder::MaintenanceItem::Cycle::ONCE, :binder_template => template, :initial_due_date_interval => nil, :due_date => Date.yesterday)
            allow_any_instance_of(Template::Maintenance).to receive(:save).and_return(false)
            template.add_default_items

            job.perform

            expect(ErrorService).to have_received(:perform_async).at_least(:once)
        end
    end
end
