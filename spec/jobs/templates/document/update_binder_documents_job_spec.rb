require 'rails_helper'

RSpec.describe Templates::Document::UpdateBinderDocumentsJob do
    let(:job) {Templates::Document::UpdateBinderDocumentsJob.new}

    describe "perform" do
        it "updates binder" do
            document_template = create(:document_template, :file => File.new("spec/assets/sampleCSV.csv"))
            document = create(:document, :file => File.new("spec/assets/SampleDoc.pdf"))
            document.library_source_id = document_template.id
            document.save!
            expect(document.file_file_name).to eq("SampleDoc.pdf")
            expect(document.library_source_id).to_not be(nil)
            
            job.perform(document_template.id)
            document.reload
            
            expect(document.file_file_name).to_not eq("SampleDoc.pdf")
        end
        
        it "calls error service" do
            allow(ErrorService).to receive(:perform_async)
            document_template = create(:document_template, :file => File.new("spec/assets/sampleCSV.csv"))
            document = create(:document, :file => File.new("spec/assets/SampleDoc.pdf"))
            document.library_source_id = document_template.id
            document.save!
            expect(document.file_file_name).to eq("SampleDoc.pdf")
            expect(document.library_source_id).to_not be(nil)
            
            allow_any_instance_of(Binder::Document).to receive(:save).and_return(false)
            
            job.perform(document_template.id)
            document.reload
            
            expect(ErrorService).to have_received(:perform_async)
        end
    end
end
