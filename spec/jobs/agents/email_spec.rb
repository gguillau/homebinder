RSpec.describe Agents::EmailJob do
    let(:job) {Agents::EmailJob.new}
    
    before :each do
        # mock KpiMailer
        double = double("KpiMailer", :deliver_later => true)
        allow(KpiMailer).to receive(:send_monthly_agent_metrics).and_return(double)
        
        # mock Kpi::Agents
        kpi_agents = Kpi::Agents.new
        allow(Kpi::Agents).to receive(:new).and_return(kpi_agents)
        allow(kpi_agents).to receive(:get_monthly_metrics)
        
        allow(Date).to receive(:today).and_return(Date.today.beginning_of_month)
        @agent = create(:user, :sign_in_count => 1, :role => "agent")
        @user_binder = create(:user_binder, :user => @agent, :role => "buyer_agent")
    end
    
    describe "perform" do
        it "sends the email to the agent and publishes an event" do
            allow(EventService).to receive(:perform_async)
            
            job.perform
            expect(KpiMailer).to have_received(:send_monthly_agent_metrics)
            expect(EventService).to have_received(:perform_async)
        end
        
        it "does not send the email when it's not the first of the month" do
            allow(Date).to receive(:today).and_return(Date.today.end_of_month)
            job.perform
            expect(KpiMailer).to_not have_received(:send_monthly_agent_metrics)
        end
        
        it "does not send the email when the user is not an agent" do
            @agent.role = "homeowner"
            @agent.save!
            job.perform
            expect(KpiMailer).to_not have_received(:send_monthly_agent_metrics)
        end
        
        it "does not send the email when the agent has not signed in" do
            @agent.sign_in_count = 0
            @agent.save!
            job.perform
            expect(KpiMailer).to_not have_received(:send_monthly_agent_metrics)
        end
        
        it "does not send the email when a user_binder with role buyer_agent does not exist" do
            @user_binder.role = "seller_agent"
            @user_binder.save!
            job.perform
            expect(KpiMailer).to_not have_received(:send_monthly_agent_metrics)
        end
        
        it "does not send the email when the user_binder is not the agent's" do
            @user_binder.user = create(:user, :role => "agent")
            @user_binder.save!
            job.perform
            expect(KpiMailer).to_not have_received(:send_monthly_agent_metrics)
        end
    end
end