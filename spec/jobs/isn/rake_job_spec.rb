require 'rails_helper'

RSpec.describe Isn::RakeJob do
    let(:job) {Isn::RakeJob.new}

    before do
        @user = create(:user, :role => "inspector")
        @partner = create(:partner)
        @partner_config = @partner.partner_configuration
        @isn_user = create(:isn_user, partner_configuration_id: @partner_config.id)
        @no_access = create(:partner)
        @isn_user.company_key = "test"
        @isn_user.api_endpoint = "inspectionsupport.com/test"
        @isn_user.username = "bsmith"
        @isn_user.password = "password"
        @isn_user.save
        @user.add_role :partner_admin, @partner
        @request = HBRequest.new.create_request(@user)
        allow(ErrorService).to receive(:perform_async)
    end

    describe "run" do
        it "does not find any users" do
            allow(job).to receive(:create_request)

            job.perform

            expect(job).to_not have_received(:create_request)
        end

        it "finds 1 user, gets the footprints, and calls process_footprints" do
            @partner_config.show_isn_tab = true
            @partner_config.save

            allow(job).to receive(:create_request).and_call_original
            allow(job).to receive(:process_footprints)

            client = Isn::Client.new(@request, @isn_user.id)
            allow(Isn::Client).to receive(:new).and_return(client)
            allow(client).to receive(:get_footprints).and_return({:footprints => [], :total => 0})

            job.perform

            expect(job).to have_received(:create_request)
            expect(client).to have_received(:get_footprints)
            expect(job).to have_received(:process_footprints)
        end
    end

    describe "#refresh" do
        it "processes the footprints" do
            @partner_config.show_isn_tab = true
            @partner_config.save

            allow(job).to receive(:create_request).and_call_original
            allow(job).to receive(:process_footprints)

            client = Isn::Client.new(@request, @isn_user.id)
            allow(Isn::Client).to receive(:new).and_return(client)
            allow(client).to receive(:get_footprints).and_return({:footprints => [], :total => 0})

            job.refresh({:id => @partner.id})

            expect(job).to have_received(:create_request)
            expect(client).to have_received(:get_footprints)
            expect(job).to have_received(:process_footprints)
        end
    end

    describe "#get_footprints" do
        it "processes the footprints" do
            @partner_config.show_isn_tab = true
            @partner_config.save

            allow(job).to receive(:create_request).and_call_original
            allow(job).to receive(:process_footprints)

            client = Isn::Client.new(@request, @isn_user.id)
            allow(Isn::Client).to receive(:new).and_return(client)
            allow(client).to receive(:get_footprints).and_return({:footprints => [], :total => 0})

            job.get_footprints(nil, @isn_user)

            expect(job).to have_received(:create_request)
            expect(client).to have_received(:get_footprints)
            expect(job).to have_received(:process_footprints)
        end
    end

    describe "create_request" do
        it "returns request" do
            request = job.create_request(@isn_user)
            expect(request.user.id).to eq(@user.id)
        end
    end

    describe "process_footprints" do
        it "doesnt call get_binder_info" do
            allow(job).to receive(:process_footprints).and_call_original
            allow(job).to receive(:create_binder)

            client = Isn::Client.new(@request, @isn_user.id)
            allow(Isn::Client).to receive(:new).and_return(client)
            allow(client).to receive(:get_binder_info)

            job.process_footprints(@request, @isn_user, [])

            expect(job).to_not have_received(:create_binder)
            expect(client).to_not have_received(:get_binder_info)
        end

        it "calls get_binder_info" do
            allow(job).to receive(:process_footprints).and_call_original
            allow(job).to receive(:create_binder)

            client = Isn::Client.new(@request, @isn_user.id)
            allow(Isn::Client).to receive(:new).and_return(client)
            allow(client).to receive(:get_binder_info).and_return({"id"=>"f6094562-abaa-41f6-b3a5-ff0acd4defff"})

            job.process_footprints(@request, @isn_user, [{:id => 1}])

            expect(job).to have_received(:create_binder)
            expect(client).to have_received(:get_binder_info)
        end
    end

    describe "create_binder" do
        it "calls ABS" do
            allow(Partner::Automation::API::Service).to receive(:create_binder)

            info =  {
                :client => {
                    :name => "Harry Potter",
                    :email => "test@test.com",
                    :home_phone => "",
                    :mobile_phone => ""
                },
                :property => {
                    :address1 => "4814 Central St",
                    :address2 => "",
                    :city => "Boston",
                    :state => "MA",
                    :zip => "02210"
                },
                :buyersagent => {
                    :id => 33699,
                    :email => "susan.mccarthy83782@gmail.com",
                    :role => "agent",
                    :sign_in_count => 0,
                    :last_sign_in_at => nil,
                    :created_at => "Sun, 26 Mar 2017 17:44:14 EDT -04:00",
                    :accepted_transfer_at => nil,
                    :user_profile_attributes => {
                        :first_name => "SusanMcCarthy",
                        :last_name => nil,
                        :home_phone => nil,
                        :mobile_phone => "+16212440044",
                        :address_attributes => nil,
                        :company => "The Real Estate Experts LLC",
                    }
                }
            }

            job.create_binder(info, {:datetime => Date.today}, @isn_user)
            expect(Partner::Automation::API::Service).to have_received(:create_binder)
        end
        it "raises an error" do
            allow(Partner::Automation::API::Service).to receive(:create_binder).and_raise(BadRequestException)
            allow(Partner::Automation::Error).to receive(:build)
            info =  {
                :client => {
                    :name => "Harry Potter",
                    :email => "test@test.com",
                    :home_phone => "",
                    :mobile_phone => ""
                },
                :property => {
                    :address1 => "4814 Central St",
                    :address2 => "",
                    :city => "Boston",
                    :state => "MA",
                    :zip => "02210"
                },
                :buyersagent => {
                    :id => 33699,
                    :email => "susan.mccarthy83782@gmail.com",
                    :role => "agent",
                    :sign_in_count => 0,
                    :last_sign_in_at => nil,
                    :created_at => "Sun, 26 Mar 2017 17:44:14 EDT -04:00",
                    :accepted_transfer_at => nil,
                    :user_profile_attributes => {
                        :first_name => "SusanMcCarthy",
                        :last_name => nil,
                        :home_phone => nil,
                        :mobile_phone => "+16212440044",
                        :address_attributes => nil,
                        :company => "The Real Estate Experts LLC",
                    }
                }
            }

            job.create_binder(info, {:datetime => Date.today}, @isn_user)
            expect(Partner::Automation::API::Service).to have_received(:create_binder)
            expect(Partner::Automation::Error).to have_received(:build)
            expect(ErrorService).to have_received(:perform_async)
        end
    end

    describe "set_homeowner" do
        it "returns homeowner info" do
            info =  {
                :client => {
                    :name => "Harry Potter",
                    :email => "test@test.com",
                    :home_phone => "",
                    :mobile_phone => ""
                }
            }

            homeowner = job.set_homeowner(info)
            expect(homeowner).to eq({:first => "Harry", :last => "Potter", :email => "test@test.com", :phone => ""})
        end
    end

    describe "set_agent" do
        it "returns agent info" do
            info =  {
                :buyersagent => {
                    :id => 33699,
                    :email => "susan.mccarthy83782@gmail.com",
                    :role => "agent",
                    :sign_in_count => 0,
                    :last_sign_in_at => nil,
                    :created_at => "Sun, 26 Mar 2017 17:44:14 EDT -04:00",
                    :accepted_transfer_at => nil,
                    :user_profile_attributes => {
                        :first_name => "SusanMcCarthy",
                        :last_name => nil,
                        :home_phone => nil,
                        :mobile_phone => "+16212440044",
                        :address_attributes => {
                            :address1 => "123 Main Street",
                            :address2 => "Apt. 2",
                            :city => "Boston",
                            :state => "MA",
                            :country => "US"
                        },
                        :company => "The Real Estate Experts LLC",
                    }
                }
            }

            agent = job.set_agent(info, :buyersagent)
            expect(agent).to eq({:name=>"The Real Estate Experts LLC", :first=>"SusanMcCarthy", :last=>nil, :email=>"susan.mccarthy83782@gmail.com", :phone=>"+16212440044", :address1 => "123 Main Street", :city => "Boston", :state => "MA", :country => "US"})
        end
    end

    describe "set_property" do
        it "returns property info" do
            info =  {
                :property => {
                    :address1 => "4814 Central St",
                    :address2 => "",
                    :city => "Boston",
                    :state => "MA",
                    :zip => "02210"
                }
            }

            prop = job.set_property(info)
            expect(prop).to eq({:address=>"4814 Central St", :address2=>"", :city=>"Boston", :state=>"MA", :country=>"US", :zip=>"02210"})
        end
    end
end
