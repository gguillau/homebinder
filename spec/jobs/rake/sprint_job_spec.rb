require 'rails_helper'

RSpec.describe Rake::SprintJob do
    let(:job) {Rake::SprintJob.new}
    
    describe "#perform" do
        it "calls the methods" do
            allow(job).to receive(:fix_documents)
            job.perform
            
            expect(job).to have_received(:fix_documents)
        end
    end
end
