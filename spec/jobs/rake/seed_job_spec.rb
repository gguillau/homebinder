require 'rails_helper'

RSpec.describe Rake::SeedJob do
    let(:seed) {Rake::SeedJob.new}
    
    describe "#run" do
        it "calls the methods" do
            allow(seed).to receive(:create_widgets)
            allow(seed).to receive(:create_dashboards)
            allow(seed).to receive(:create_api_keys)
            allow(seed).to receive(:create_binder_library)
            allow(seed).to receive(:create_contractor_types)
            allow(seed).to receive(:create_property)
            allow(seed).to receive(:create_users_and_partners)
            allow(seed).to receive(:import)
            seed.perform
            
            expect(seed).to have_received(:create_widgets)
            expect(seed).to have_received(:create_dashboards)
            expect(seed).to have_received(:create_api_keys)
            expect(seed).to have_received(:create_binder_library)
            expect(seed).to have_received(:create_contractor_types)
            expect(seed).to have_received(:create_property)
            expect(seed).to have_received(:create_users_and_partners)
            expect(seed).to have_received(:import)
        end
    end

    describe "#create_widgets" do
        it "creates the widgets" do
            seed.create_widgets
            expect(Widget.all.count).to eq(15)
        end
    end

    describe "#create_dashboards" do
        it "creates the dashboard and adds 4 widgets" do
            seed.create_dashboards
            dashboard = Dashboard.first
            expect(Dashboard.all.count).to eq(1)
            expect(dashboard).to_not be(nil)
        end
    end

    describe "#create_contractor_types" do
        it "creates the contractor types" do
            seed.create_contractor_types
            expect(Contractor::Category.all.count).to eq(59)
        end
    end

    describe "#import" do
        it "imports data into ES" do
            seed.import
        end
    end

    describe "#create_binder_library" do
        it "creates the binder library" do
            allow_any_instance_of(Template::Appliance).to receive(:save!)
            allow(Template::Appliance).to receive(:create!)
            allow_any_instance_of(Template::Maintenance).to receive(:save!)
            allow(Template::Maintenance).to receive(:create!)
            allow_any_instance_of(Template::Document).to receive(:save!)
            allow(Template::Document).to receive(:create!)
            seed.create_binder_library
            expect(BinderTemplate.all.count).to eq(2)
        end
    end

    describe "#create_users_and_partners" do
        it "creates the default set of users and partners" do
            seed.create_users_and_partners
            expect(User.all.count).to eq(3)
        end
    end

    describe "#create_property" do
        it "creates the default set of property types" do
            seed.create_property
            expect(PropertyType.all.count).to eq(3)
        end
    end

    describe "#create_api_keys" do
        it "creates the default set of api keys" do
            seed.create_api_keys
            expect(ApiKey.all.count).to eq(1)
        end
    end
end
