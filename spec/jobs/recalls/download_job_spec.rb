require 'rails_helper'

RSpec.describe Recalls::DownloadJob do
    let(:job) {Recalls::DownloadJob.new}

    before :each do
        @recall_details = {
            "Title" => "This is a recall. Foo is only in the title",
            "Description" => "Here are the details of the recall. Bar is only in the description",
            "Manufacturers" => [
                { "Name" => "Man" }
            ],
            "Products" => [
                { "Name" => "Thing", "Description" => "Here are the details of the product. Pop is here.", "Type" => "Baz" }
            ]
        }

        @recall_models = "modelABC, modelXYZ, model123"
        @recall = build(:recall, details: JSON.generate(@recall_details), models: @recall_models)
    end

    describe "preload_recalls" do
        it "it gets recalls" do
            Rails.configuration.recall_check[:source] = "homebinder"
            recalls = job.preload_recalls
            expect(recalls.length).to eq(0)
        end

        it "it gets recalls" do
            expect(CPSCService).to receive(:get_all_recalls).and_return({})
            Rails.configuration.recall_check[:source] = "cpsc"
            job.preload_recalls
        end

        it "it gets recalls" do
            allow(CPSCService).to receive(:get_all_recalls).and_return({})
            ConsumerRecall.create!(:list => JSON.generate([@recall]), :refreshed_at => Date.today - Time.new.utc_offset)
            Rails.configuration.recall_check[:source] = "cpsc"
            job.preload_recalls
        end
    end

    describe "check_binders_for_recalls" do
        it "it does a recall on the binder but does not send email" do
            @recall.verified = true
            @recall.ignore = false
            @recall.save
            Rails.configuration.recall_check[:source] = "homebinder"
            user = create(:user, :role => "agent", :sign_in_count => 1)
            binder = create(:binder)
            create(:user_binder, :binder => binder, :user => user, :role => "owner")
            create(:subscription, :binder => binder, :plan_id => "paid")
            appliance = create(:appliance, :binder => binder, :manufacturer => "Man", :appliance_type => "Foo", :model => "modelXYZ")
            create(:appliance_recalls, appliance_id: appliance.id, status: "new", notifications: 0)
            date  = Date.parse("Friday")
            delta = date > Date.today ? 0 : 7
            friday = date + delta
            allow(Date).to receive(:today).and_return(friday)
            eventService = double("EventService", :new => true, :publish_event => true)
            allow(EventService).to receive(:new).and_return(eventService)
            allow(eventService).to receive(:publish_event)
            allow(RecallMailer).to receive(:delay).and_return(RecallMailer)
            allow(RecallMailer).to receive(:notify_of_recalls)
            job.perform
            # analytics
            expect(eventService).to_not have_received(:publish_event)
            # mailer
            expect(RecallMailer).to_not have_received(:notify_of_recalls)
        end
    end

    describe "check_binder_for_recall" do
        it "it does a recall on the binder" do
            expect_any_instance_of(Recalls::DownloadJob).to receive(:preload_recalls)
            expect_any_instance_of(Recalls::DownloadJob).to receive(:check_for_appliance_recall)
            binder = create(:binder)
            create(:subscription, :binder => binder, :plan_id => "paid")
            create(:appliance, :binder => binder, :manufacturer => "Man", :appliance_type => "Foo", :model => "modelXYZ")

            job.check_binder_for_recall(binder)
        end
    end

    describe "check_for_appliance_recall" do
        it "it does a recall on the appliance" do
            match = {
                :strength => 1,
                :recall => {
                    "RecallNumber" => "recallnumber",
                    "RecallDate" => Date.tomorrow
                }
            }
            expect_any_instance_of(Recalls::DownloadJob).to receive(:preload_recalls)
            expect_any_instance_of(Recalls::DownloadJob).to receive(:search_all_recalls).and_return([match])
            allow(CPSCService).to receive(:get_all_recalls).and_return([@recall_details])
            Rails.configuration.recall_check[:source] = "cpsc"
            binder = create(:binder)
            create(:subscription, :binder => binder, :plan_id => "paid")
            appliance = create(:appliance, :binder => binder, :manufacturer => "Man", :appliance_type => "Foo", :model => "modelXYZ")

            job.check_for_appliance_recall(appliance)
        end
    end

    describe "recall_check" do
        it "it does a recall on the binder" do
            match = {
                :strength => 1,
                :recall => {
                    "RecallNumber" => "recallnumber",
                    "RecallDate" => Date.tomorrow
                }
            }
            expect_any_instance_of(Recalls::DownloadJob).to receive(:preload_recalls)
            expect_any_instance_of(Recalls::DownloadJob).to receive(:search_all_recalls).and_return([match])
            Rails.configuration.recall_check[:source] = "cpsc"
            binder = create(:binder)
            create(:subscription, :binder => binder, :plan_id => "paid")
            create(:appliance, :binder => binder, :manufacturer => "Man", :appliance_type => "Foo", :model => "modelXYZ")

            job.recall_check({
                :manufacturer => "Man",
                :model => "modelXYZ",
                :appliance_type => "Bar"
            })
        end
        it "it does a recall on the binder" do
            match = {
                :strength => 1,
                :recall => OpenStruct.new({details: "{}"})
            }
            expect_any_instance_of(Recalls::DownloadJob).to receive(:preload_recalls)
            expect_any_instance_of(Recalls::DownloadJob).to receive(:search_for_recalls).and_return([match])
            Rails.configuration.recall_check[:source] = "homebinder"
            binder = create(:binder)
            create(:subscription, :binder => binder, :plan_id => "paid")
            create(:appliance, :binder => binder, :manufacturer => "Man", :appliance_type => "Foo", :model => "modelXYZ")

            job.recall_check({
                :manufacturer => "Man",
                :model => "modelXYZ",
                :appliance_type => "Bar"
            })
        end
    end

    describe "search_all_recalls" do
        it "it returns a match" do
            job.instance_variable_set(:@recalls, [@recall_details])
            matches = job.search_all_recalls({
                :manufacturer => "Man",
                :model => "bar",
                :appliance_type => "Bar"
            })
            expect(matches.length).to eq(1)
        end
        it "it does not return a match" do
            job.instance_variable_set(:@recalls, [@recall_details])
            matches = job.search_all_recalls({
                :manufacturer => "test",
                :model => "test",
                :appliance_type => "test"
            })
            expect(matches.length).to eq(0)
        end
    end

    context "recall by manufacturer" do
        it "matches on name" do
            expect(job.manufacturer_match?(@recall_details, "mAn")).to be true
        end

        it "matches on title" do
            expect(job.manufacturer_match?(@recall_details, "foo")).to be true
        end

        it "matches on description" do
            expect(job.manufacturer_match?(@recall_details, "baR")).to be true
        end

        it "matches on product name" do
            expect(job.manufacturer_match?(@recall_details, "THING")).to be true
        end

        it "matches on product description" do
            expect(job.manufacturer_match?(@recall_details, "pop")).to be true
        end

        it "does not match when nil" do
            expect(job.manufacturer_match?(@recall_details, nil)).to be false
        end

        it "does not match when empty" do
            expect(job.manufacturer_match?(@recall_details, "")).to be false
        end

        it "does not match on substring" do
            expect(job.manufacturer_match?(@recall_details, "ma")).to be false
        end
    end

    context "recall by appliance type" do
        it "matches on title" do
            expect(job.appliance_type_match?(@recall_details, "foo")).to be true
        end

        it "matches on description" do
            expect(job.appliance_type_match?(@recall_details, "BAr")).to be true
        end

        it "matches on product name" do
            expect(job.appliance_type_match?(@recall_details, "thIng")).to be true
        end

        it "matches on product description" do
            expect(job.appliance_type_match?(@recall_details, "POP")).to be true
        end

        it "matches on product type" do
            expect(job.appliance_type_match?(@recall_details, "baz")).to be true
        end

        it "does not match when nil" do
            expect(job.appliance_type_match?(@recall_details, nil)).to be false
        end

        it "does not match when empty" do
            expect(job.appliance_type_match?(@recall_details, "")).to be false
        end

        it "does not match on substring" do
            expect(job.appliance_type_match?(@recall_details, "dEt")).to be false
        end
    end

    context "recall by model" do
        it "matches when recall model starts with appliance model" do
            expect(job.model_match?(@recall, "MODELABCDEFG")).to be true
        end

        it "matches when appliance model starts with recall model" do
            expect(job.model_match?(@recall, "MODELABCDEFG")).to be true
        end

        it "matches when recall has single model" do
            @recall.models = "modelPPP"
            expect(job.model_match?(@recall, "ModelP")).to be true
        end

        it "does not match when recall has no models" do
            @recall.models = nil
            expect(job.model_match?(@recall, "MODELABCDEFG")).to be false
        end

        it "does not match when appliance model is nil" do
            expect(job.model_match?(@recall, nil)).to be false
        end

        it "does not match when appliances model is empty" do
            expect(job.model_match?(@recall, "ma")).to be false
        end
    end
end
