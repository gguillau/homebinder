require 'rails_helper'

RSpec.describe Stripe::ParseWebhookJob do
    let(:job) {Stripe::ParseWebhookJob.new}

    describe "#perform" do
        it "calls send send_failed_payment_notification for partner admin" do
            user = create(:user, :role => "inspector")
            partner = create(:partner)
            create(:partner_user, :user => user, :partner => partner, :role => "admin")
            create(:account, :stripe_customer_id => "test", :manager_id => partner.id)
            event = {"type" => "charge.failed", "data" => {"object" => {"customer" => "test"} } }
            allow(Stripe::Webhook).to receive(:construct_event).and_return(event)
            expect_any_instance_of(User).to receive(:send_failed_payment_notification)
            body = double("body", :read => true)
            request = double("request", :body => body, :env => {})
            job.perform(request)
        end

        it "calls send send_failed_payment_notification for user" do
            user = create(:user)
            binder = create(:binder)
            create(:user_binder, :binder => binder, :user => user, :role => "owner")
            subscription = create(:subscription, :customer_id => "test", :binder => binder)
            event = {"type" => "charge.failed", "data" => {"object" => {"customer" => "test"} } }
            allow(Stripe::Webhook).to receive(:construct_event).and_return(event)
            expect_any_instance_of(User).to receive(:send_failed_payment_notification)
            body = double("body", :read => true)
            request = double("request", :body => body, :env => {})

            job.perform(request)

            subscription.reload

            expect(subscription.plan_id).to eq("free")
            expect(subscription.payment_status).to eq("failed")
        end
    end
end
