require 'rails_helper'

RSpec.describe AnnualPropertyReviews::ScheduleJob do
    let(:job) {AnnualPropertyReviews::ScheduleJob.new}

    describe "perform" do
        it "calls mailer" do
            mailer = double("AnnualPropertyReviewMailer", :deliver_later => true)
            allow(AnnualPropertyReviewMailer).to receive(:reminder).and_return(mailer)
            create(:annual_property_review, :scheduled_for => Date.tomorrow)
            
            job.perform
            
            allow(AnnualPropertyReviewMailer).to receive(:reminder)
        end
    end
end
