require 'rails_helper'

RSpec.describe Tags::SetResourcesTagsJob, :type => :job do
    let(:job) {Tags::SetResourcesTagsJob.new}

    before :each do
        @user = create :user
        @binder = create(:binder)
        @partner = create(:partner)
        @user.add_role(:owner, @binder)
        @item = create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 1.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
    end

    describe "#perform" do
        it "deletes tags" do
            structure = create(:structure)
            area = create(:area)
            area.tags.new(:tag => "structure_#{structure.id}").save
            structure.tags.new(:tag => "area_#{area.id}").save
            expect(Tag.where(:taggable_id => area.id, :taggable_type => "Binder::Area").count).to eq(1)
            expect(Tag.where(:taggable_id => structure.id, :taggable_type => "Binder::Structure").count).to eq(1)

            job.perform("Binder::Area", area.id)
            job.perform("Binder::Structure", structure.id)
            expect(Tag.where(:taggable_id => area.id, :taggable_type => "Binder::Area").count).to eq(0)
            expect(Tag.where(:taggable_id => structure.id, :taggable_type => "Binder::Structure").count).to eq(0)
        end
        it "updates/removes tags" do
            structure = create(:structure)
            finish = create(:finish)
            project = create(:project)
            area = create(:area)
            area.tags.new(:tag => "structure_#{structure.id}", :scope => "structure_test").save
            area.tags.new(:tag => "finish_#{finish.id}", :scope => "finish_test").save
            expect(area.tags.length).to eq(2)
            tag_1 = ActionController::Parameters.new({:id => area.tags.where(:tag => "structure_#{structure.id}").first.id, :tag => "structure_#{structure.id}", :scope => "updated", :tag_scope => nil})
            tag_2 = ActionController::Parameters.new({:tag => "project_#{project.id}", :scope => "project_test"})
            
            array = [tag_1.to_unsafe_h, tag_2.to_unsafe_h]
            job.perform("Binder::Area", area.id, array)
            area = Binder::Area.find(area.id)
            tag_1 = area.tags.first
            tag_2 = area.tags.second
            expect(area.tags.length).to eq(2)
            expect(tag_1.scope).to eq("updated")
            expect(tag_2.scope).to eq("project_test")
            expect(Tag.where(:tag => "finish_#{finish.id}").count).to eq(0)
        end
    end
end
