require 'rails_helper'

RSpec.describe Users::ResetPasswordsFieldsJob do
    let(:job) {Users::ResetPasswordsFieldsJob.new}

    describe "perform" do
        it "creates the pro and type for lender" do
            user = create(:user, :reset_password_token => SecureRandom.hex(4), :reset_password_sent_at => 1.week.ago)
            job.perform
            user.reload

            expect(user.reset_password_token).to be(nil)
        end
    end
end
