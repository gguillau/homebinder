require 'rails_helper'

RSpec.describe Users::CreateAdditionalAssociationsJob do
    let(:job) {Users::CreateAdditionalAssociationsJob.new}

    describe "perform" do
        it "creates the pro and type for lender" do
            allow(Users::CreateAdditionalAssociationsJob).to receive(:perform_async)
            user = create(:user, :role => "lender")

            job.perform(user.id)
            user.reload

            expect(user.contractor.nil?).to be false
        end

        it "creates the pro and type for builder" do
            allow(Users::CreateAdditionalAssociationsJob).to receive(:perform_async)
            user = create(:user, :role => "builder")

            job.perform(user.id)
            user.reload

            expect(user.contractor.nil?).to be false
        end

        it "creates the pro and type for property_manager" do
            allow(Users::CreateAdditionalAssociationsJob).to receive(:perform_async)
            user = create(:user, :role => "property_manager")

            job.perform(user.id)
            user.reload

            expect(user.contractor.nil?).to be false
        end
    end
end
