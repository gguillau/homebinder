require 'rails_helper'

RSpec.describe Users::UpdateContractorJob do
    let(:job) {Users::UpdateContractorJob.new}

    describe "perform" do
        it "creates the pro and type for lender" do
            allow(Users::UpdateContractorJob).to receive(:perform_async)
            user = create(:user, :role => "agent")
            expect(user.contractor).to_not be nil
            user.user_profile.address.city = "Salt fake City"
            user.user_profile.address.save!
            
            job.perform(user.id)

            expect(user.contractor.address.city).to eq 'Salt fake City'
        end
    end
end
