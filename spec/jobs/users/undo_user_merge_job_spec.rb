
require 'rails_helper'

RSpec.describe Users::UndoUserMergeJob do
    let(:job) {Users::UndoUserMergeJob.new}
    let(:merge_job) {Users::MergeAgentsJob.new}
    
    describe "perform" do
        it "reverts an agent merge" do
            stub_request(:post, "http://0.0.0.0:9200/events/event")
            double = double("AgentMergeMailer", :deliver_later => true)
            allow(AgentMergeMailer).to receive(:notify_agents).and_return(double)
            allow(AgentMergeMailer).to receive(:support_notification).and_return(double)
            
            partner = create(:partner)
            
            create(:contractor_contractor_category, :name => "Real Estate")
            old_agent = create(:user, :role => "agent", :email => "old_email@test.com")
            new_agent = create(:user, :role => "agent", :email => "new_email@test.com")
            merger = create(:user)
            
            create(:partner_user, :partner => partner, :user => new_agent, :role => "agent")
            create(:partner_user, :partner => partner, :user => old_agent, :role => "agent")
            
            create(:user_profile, :user => old_agent)
            create(:user_profile, :user => new_agent)
            create(:user_binder, :user => old_agent)
            create(:partner_user, :user => old_agent, :role => "agent")
            create(:binder_contractor, :contractor => old_agent.contractor)
            create(:partner_contractor, :contractor => old_agent.contractor)
            create(:partner_client, :client => old_agent)
            create(:dashboard, :user => old_agent, :system => false)
            create(:binder_item, :user => old_agent)
            create(:transfer, :receiver_id => old_agent.id)
            create(:transfer, :sender_id => old_agent.id)
            create(:share, :shared_with_id => old_agent.id)
            create(:share, :shared_by_id => old_agent.id)
            
            create(:user_contractor, :user => old_agent)
            create(:organization_user, :user => old_agent)
            create(:partner_binder, :client_id => old_agent.id)
            create(:user_binder_transaction, :user => old_agent)
            create(:user_resource, :user => old_agent)
            
            expect(new_agent.user_binders.count).to eq(0)
            expect(new_agent.partner_users.count).to eq(1)
            expect(new_agent.contractor.binder_contractors.count).to eq(0)
            expect(new_agent.contractor.template_contractors.count).to eq(0)
            expect(new_agent.dashboards.count).to eq(0)
            expect(new_agent.binder_items.count).to eq(0)
            expect(new_agent.transfers_sent.count).to eq(0)
            expect(new_agent.transfers_received.count).to eq(0)
            expect(new_agent.shares_sent.count).to eq(0)
            expect(new_agent.shares_received.count).to eq(0)
            
            expect(new_agent.user_contractors.count).to eq(0)
            expect(new_agent.organizations.count).to eq(0)
            expect(new_agent.partner_binders.count).to eq(0)
            expect(new_agent.dashboards.count).to eq(0)
            expect(new_agent.binder_items.count).to eq(0)
            expect(new_agent.user_binder_transactions.count).to eq(0)
            expect(new_agent.user_resources.count).to eq(0)
            
            expect(old_agent.partner_users.count).to eq(2)
            
            footprint = merge_job.create_footprint(old_agent, new_agent)
            merge_job.perform(old_agent.id, new_agent.id, merger.id)
            
            expect(new_agent.user_binders.count).to eq(1)
            expect(new_agent.partner_users.count).to eq(2)
            expect(new_agent.contractor.binder_contractors.count).to eq(1)
            expect(new_agent.contractor.partner_contractors.count).to eq(1)
            expect(new_agent.user_binders.count).to eq(1)
            expect(new_agent.dashboards.count).to eq(1)
            expect(new_agent.binder_items.count).to eq(1)
            expect(new_agent.transfers_sent.count).to eq(1)
            expect(new_agent.transfers_received.count).to eq(1)
            expect(new_agent.shares_sent.count).to eq(1)
            expect(new_agent.shares_received.count).to eq(1)
            
            expect(new_agent.user_contractors.count).to eq(1)
            expect(new_agent.organizations.count).to eq(1)
            expect(new_agent.partner_binders.count).to eq(1)
            expect(new_agent.dashboards.count).to eq(1)
            expect(new_agent.binder_items.count).to eq(1)
            expect(new_agent.user_binder_transactions.count).to eq(1)
            expect(new_agent.user_resources.count).to eq(1)
            expect{User.find(old_agent.id)}.to raise_error ActiveRecord::RecordNotFound
            
            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(footprint)
            
            user_merge = UserMerge.last
            job.perform(user_merge.id)
            new_agent.reload
            
            expect(new_agent.user_binders.count).to eq(0)
            expect(new_agent.partner_users.count).to eq(1)
            expect(new_agent.contractor.binder_contractors.count).to eq(0)
            expect(new_agent.contractor.template_contractors.count).to eq(0)
            expect(new_agent.dashboards.count).to eq(0)
            expect(new_agent.binder_items.count).to eq(0)
            expect(new_agent.transfers_sent.count).to eq(0)
            expect(new_agent.transfers_received.count).to eq(0)
            expect(new_agent.shares_sent.count).to eq(0)
            expect(new_agent.shares_received.count).to eq(0)
            expect(new_agent.user_contractors.count).to eq(0)
            expect(new_agent.organizations.count).to eq(0)
            expect(new_agent.partner_binders.count).to eq(0)
            expect(new_agent.dashboards.count).to eq(0)
            expect(new_agent.binder_items.count).to eq(0)
            expect(new_agent.user_binder_transactions.count).to eq(0)
            expect(new_agent.user_resources.count).to eq(0)
            reverted = User.find_by_email old_agent.email

            expect(reverted.user_binders.count).to eq(1)
            expect(reverted.partner_users.count).to eq(2)
            expect(reverted.contractor.binder_contractors.count).to eq(1)
            expect(reverted.dashboards.count).to eq(1)
            expect(reverted.binder_items.count).to eq(1)
            expect(reverted.transfers_sent.count).to eq(1)
            expect(reverted.transfers_received.count).to eq(1)
            expect(reverted.shares_sent.count).to eq(1)
            expect(reverted.shares_received.count).to eq(1)
            expect(reverted.user_contractors.count).to eq(1)
            expect(reverted.organizations.count).to eq(1)
            expect(reverted.partner_binders.count).to eq(1)
            expect(reverted.dashboards.count).to eq(1)
            expect(reverted.binder_items.count).to eq(1)
            expect(reverted.user_binder_transactions.count).to eq(1)
            expect(reverted.user_resources.count).to eq(1)
        end
    end
end