require 'rails_helper'

RSpec.describe Users::MergeAgentsJob do
    let(:job) {Users::MergeAgentsJob.new}
    
    describe "perform" do
        it "merges the agents info and deletes the old agent" do
            stub_request(:post, "http://0.0.0.0:9200/events/event")
            double = double("AgentMergeMailer", :deliver_later => true)
            allow(AgentMergeMailer).to receive(:notify_agents).and_return(double)
            allow(AgentMergeMailer).to receive(:support_notification).and_return(double)
            
            partner = create(:partner)
            
            create(:contractor_contractor_category, :name => "Real Estate")
            old_agent = create(:user, :role => "agent", :email => "old_email@test.com")
            new_agent = create(:user, :role => "agent", :email => "new_email@test.com")
            merger = create(:user)
            
            create(:partner_user, :partner => partner, :user => new_agent, :role => "agent")
            create(:partner_user, :partner => partner, :user => old_agent, :role => "agent")
            
            create(:user_profile, :user => old_agent)
            create(:user_profile, :user => new_agent)
            create(:user_binder, :user => old_agent)
            create(:partner_user, :user => old_agent, :role => "agent")
            create(:binder_contractor, :contractor => old_agent.contractor)
            create(:partner_contractor, :contractor => old_agent.contractor)
            create(:partner_client, :client => old_agent)
            create(:dashboard, :user => old_agent, :system => false)
            create(:binder_item, :user => old_agent)
            create(:transfer, :receiver_id => old_agent.id)
            create(:transfer, :sender_id => old_agent.id)
            create(:share, :shared_with_id => old_agent.id)
            create(:share, :shared_by_id => old_agent.id)
            
            create(:user_contractor, :user => old_agent)
            create(:organization_user, :user => old_agent)
            create(:partner_binder, :client_id => old_agent.id)
            create(:user_binder_transaction, :user => old_agent)
            create(:user_resource, :user => old_agent)
            
            expect(new_agent.user_binders.count).to eq(0)
            expect(new_agent.partner_users.count).to eq(1)
            expect(new_agent.contractor.binder_contractors.count).to eq(0)
            expect(new_agent.contractor.template_contractors.count).to eq(0)
            expect(new_agent.dashboards.count).to eq(0)
            expect(new_agent.binder_items.count).to eq(0)
            expect(new_agent.transfers_sent.count).to eq(0)
            expect(new_agent.transfers_received.count).to eq(0)
            expect(new_agent.shares_sent.count).to eq(0)
            
            expect(new_agent.user_contractors.count).to eq(0)
            expect(new_agent.organizations.count).to eq(0)
            expect(new_agent.partner_binders.count).to eq(0)
            expect(new_agent.dashboards.count).to eq(0)
            expect(new_agent.binder_items.count).to eq(0)
            expect(new_agent.user_binder_transactions.count).to eq(0)
            expect(new_agent.user_resources.count).to eq(0)
            
            job.perform(old_agent.id, new_agent.id, merger.id)
            
            expect(new_agent.user_binders.count).to eq(1)
            expect(new_agent.partner_users.count).to eq(2)
            expect(new_agent.contractor.binder_contractors.count).to eq(1)
            expect(new_agent.contractor.partner_contractors.count).to eq(1)
            expect(new_agent.user_binders.count).to eq(1)
            expect(new_agent.dashboards.count).to eq(1)
            expect(new_agent.binder_items.count).to eq(1)
            expect(new_agent.transfers_sent.count).to eq(1)
            expect(new_agent.transfers_received.count).to eq(1)
            expect(new_agent.shares_sent.count).to eq(1)
            expect(new_agent.shares_received.count).to eq(1)
            
            expect(new_agent.user_contractors.count).to eq(1)
            expect(new_agent.organizations.count).to eq(1)
            expect(new_agent.partner_binders.count).to eq(1)
            expect(new_agent.dashboards.count).to eq(1)
            expect(new_agent.binder_items.count).to eq(1)
            expect(new_agent.user_binder_transactions.count).to eq(1)
            expect(new_agent.user_resources.count).to eq(1)
            
            expect{User.find(old_agent.id)}.to raise_error ActiveRecord::RecordNotFound
            expect(AgentMergeMailer).to have_received(:notify_agents)
            expect(AgentMergeMailer).to have_received(:support_notification)
        end
    end
    
    describe "delete_shared_partner_data" do
        it "deletes the shared partner_users and partner_clients" do
            partner1 = create(:partner)
            partner2 = create(:partner)
            old_agent = create(:user, :role => "agent")
            new_agent = create(:user, :role => "agent")
            
            delete1 = create(:partner_user, :partner => partner1, :user => old_agent, :role => "agent")
            create(:partner_user, :partner => partner2, :user => old_agent, :role => "agent")
            create(:partner_user, :partner => partner1, :user => new_agent, :role => "agent")
            
            delete2 = create(:partner_client, :partner => partner1, :client => old_agent)
            create(:partner_client, :partner => partner2, :client => old_agent)
            create(:partner_client, :partner => partner1, :client => new_agent)
            
            shared_partners = old_agent.partners.pluck(:id) & new_agent.partners.pluck(:id)
            
            job.delete_shared_partner_data(shared_partners, old_agent.id)
            
            expect{PartnerUser.find(delete1.id)}.to raise_error ActiveRecord::RecordNotFound
            expect{PartnerClient.find(delete2.id)}.to raise_error ActiveRecord::RecordNotFound
        end
    end
    
    describe "get_agent_data" do
        it "gets the agent's branding along with their user id" do
            agent = create(:user, :role => "agent")
            branding = agent.user_profile.branding
            branding[:id] = agent.id
            
            data = job.get_agent_data(agent)
            
            expect(data).to eq(branding)
        end
    end

end