require 'rails_helper'

RSpec.describe Users::CreateHomeProJob do
    let(:job) {Users::CreateHomeProJob.new}

    describe "perform" do
        it "creates the user account for home pro when created by inspector" do
            allow(Users::CreateHomeProJob).to receive(:perform_async)
            inspector = create(:user, :role => "inspector")
            contractor = create(:contractor, :email => "test@gmail.com", :created_by => inspector.id)

            job.perform(contractor.id)
            contractor.reload

            expect(contractor.user.nil?).to be false
        end
        
        it "creates the user account for home pro when created by homeowner" do
            allow(Users::CreateHomeProJob).to receive(:perform_async)
            homeowner = create(:user, :role => "homeowner")
            contractor = create(:contractor, :email => "test@gmail.com", :created_by => homeowner.id)

            job.perform(contractor.id)
            contractor.reload

            expect(contractor.user.nil?).to be false
        end
        
        it "raises an UnprocessableException and calls ErrorService" do
            allow(ErrorService).to receive(:perform_async)
            allow(Users::CreateHomeProJob).to receive(:perform_async)
            inspector = create(:user, :role => "homeowner")
            allow(Partner).to receive(:build).and_raise(UnprocessableException.new(inspector))
            contractor = create(:contractor, :email => "test@gmail.com", :created_by => inspector.id)

            job.perform(contractor.id)
            contractor.reload

            expect(ErrorService).to have_received(:perform_async)
        end
        
        it "raises an BadRequestException and calls ErrorService" do
            allow(ErrorService).to receive(:perform_async)
            allow(Users::CreateHomeProJob).to receive(:perform_async)
            inspector = create(:user, :role => "homeowner")
            allow(Partner).to receive(:build).and_raise(BadRequestException.new)
            contractor = create(:contractor, :email => "test@gmail.com", :created_by => inspector.id)

            job.perform(contractor.id)
            contractor.reload

            expect(ErrorService).to have_received(:perform_async)
        end
    end
end
