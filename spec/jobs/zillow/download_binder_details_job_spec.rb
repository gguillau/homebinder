require 'rails_helper'

RSpec.describe Zillow::DownloadBinderDetailsJob, :type => :service do
    before :each do
        @user = create(:user)
        @binder = create(:binder)
    end

    context "when there are no photos" do
        describe "get_property_photo" do
            it "returns empty array when call is nil" do
                zillow = Zillow::Request.new
                allow(Zillow::Request).to receive(:new).and_return(zillow)
                allow(zillow).to receive(:call).and_return(nil)
                job = Zillow::DownloadBinderDetailsJob.new
                job.binder = @binder
                photos = job.get_property_photo
                expect(photos.length).to eq(0)
            end
            it "returns empty array when code is not equal to 0" do
                call = {
                    "searchresults" => {
                        "message"=> {
                            "text" => "Request successfully processed",
                            "code"=> "1"
                        }
                    }
                }
                zillow = Zillow::Request.new
                allow(Zillow::Request).to receive(:new).and_return(zillow)
                allow(zillow).to receive(:call).and_return(call)
                job = Zillow::DownloadBinderDetailsJob.new
                job.binder = @binder
                photos = job.get_property_photo
                expect(photos.length).to eq(0)
            end
        end
        describe "get_binder_info" do
            it "returns an empty array when results are empty" do
                job = Zillow::DownloadBinderDetailsJob.new
                job.binder = @binder
                photos = job.get_binder_info([])
                expect(photos.length).to eq(0)
            end
            it "returns empty array when results code is not equal to 0" do
                call = {
                    "updatedPropertyDetails" => {
                        "request" => {
                            "zpid" => Faker::Number.number(10)
                        },
                        "message" => {
                            "text" => "Error",
                            "code" => "1"
                        },
                        "response" => {
                            "images"=>{"count"=>"1", "image"=>{"url"=>"https://dummyimage.com/300/09f/fff.png"}}
                        }
                    }
                }
                # mock calls
                zillow = Zillow::Request.new
                allow(Zillow::Request).to receive(:new).and_return(zillow)
                allow(zillow).to receive(:call).and_return(call)

                job = Zillow::DownloadBinderDetailsJob.new
                job.binder = @binder
                photos = job.get_binder_info([{"zpid" => 1}])
                expect(photos.length).to eq(0)
            end
        end
    end

    context "when there are photos" do
        before :each do
            first_call = {
                "searchresults" => {
                    "message"=> {
                        "text" => "Request successfully processed",
                        "code"=>"0"
                    },
                    "response"=> {
                        "results" => {
                            "result" => {
                                "zpid" => Faker::Number.number(10)
                            }
                        }
                    }
                }
            }
            second_call = {
                "updatedPropertyDetails" => {
                    "request" => {
                        "zpid" => Faker::Number.number(10)
                    },
                    "message" => {
                        "text" => "Request successfully processed",
                        "code" => "0"
                    },
                    "response" => {
                        "images"=>{"count"=>"1", "image"=>{"url"=>"https://dummyimage.com/300/09f/fff.png"}}
                    }
                }
            }
            # mock calls
            zillow = Zillow::Request.new
            allow(Zillow::Request).to receive(:new).and_return(zillow)
            allow(zillow).to receive(:call).and_return(first_call, second_call)
        end
        describe "check_zillow" do
            it "checks for images and creates 1 image" do
                allow(URI).to receive(:parse).and_return(File.open('spec/assets/SampleImage.png'))
                Zillow::DownloadBinderDetailsJob.new.perform(@binder.id)
                expect(@binder.images.count).to eq(1)
            end
        end
        describe "create_images" do
            it "adds images to the binder" do
                allow(URI).to receive(:parse).and_return(File.open('spec/assets/SampleImage.png'))
                job = Zillow::DownloadBinderDetailsJob.new
                job.binder = @binder
                job.create_images(["https://dummyimage.com/300/09f/fff.png"])
                @binder.reload
                hero = @binder.images[0]
                expect(@binder.images.count).to eq(1)
                expect(@binder.hero_image_id).to be(hero.id)
            end
            it "adds images to the binder but calls New Relic when binder doesnt save" do
                allow(URI).to receive(:parse).and_return(File.open('spec/assets/SampleImage.png'))
                allow_any_instance_of(Binder).to receive(:save).and_return(false)
                allow(ErrorService).to receive(:perform_async)
                job = Zillow::DownloadBinderDetailsJob.new
                job.binder = @binder
                job.create_images(["https://dummyimage.com/300/09f/fff.png"])
                @binder.reload
                expect(@binder.images.count).to eq(1)
                expect(@binder.hero_image_id).to be(nil)
                expect(ErrorService).to have_received(:perform_async)
            end
            it "sends an error to New Relic" do
                allow(ErrorService).to receive(:perform_async)
                allow_any_instance_of(Binder::Image).to receive(:save).and_return(false)
                job = Zillow::DownloadBinderDetailsJob.new
                job.binder = @binder
                job.create_images(["https://dummyimage.com/300/09f/fff.png"])
                expect(ErrorService).to have_received(:perform_async)
            end
            it "sends an error to New Relic" do
                allow(ErrorService).to receive(:perform_async)
                allow_any_instance_of(Binder).to receive(:save).and_return(false)
                job = Zillow::DownloadBinderDetailsJob.new
                job.binder = @binder
                job.create_images(["https://dummyimage.com/300/09f/fff.png"])
                expect(ErrorService).to have_received(:perform_async)
            end
            it "sends an error to New Relic" do
                allow(ErrorService).to receive(:perform_async)
                allow_any_instance_of(Binder::Image).to receive(:save).and_raise(BadRequestException)
                job = Zillow::DownloadBinderDetailsJob.new
                job.binder = @binder
                job.create_images(["https://dummyimage.com/300/09f/fff.png"])
                expect(ErrorService).to have_received(:perform_async)
            end
        end
        describe "get_images" do
            it "returns two images" do
                hash = {
                    "images" => {
                        "count" => 2,
                        "image" => {
                            "url" => ["one", "two"]
                        }
                    }
                }
                job = Zillow::DownloadBinderDetailsJob.new
                job.binder = @binder
                images = job.get_images(hash)
                expect(images.count).to eq(2)
            end
            it "returns 0 images" do
                hash = {
                    "images" => {
                        "count" => 0
                    }
                }
                job = Zillow::DownloadBinderDetailsJob.new
                job.binder = @binder
                images = job.get_images(hash)
                expect(images.count).to eq(0)
            end
        end
        describe "set_additional_binder_property_fields" do
            it "adds additional data to binder" do
                result = {"zpid"=>"57058228", "links"=>{"homedetails"=>"http://www.zillow.com/homedetails/208-Baldwin-Rd-Carlisle-MA-01741/57058228_zpid/", "graphsanddata"=>"http://www.zillow.com/homedetails/208-Baldwin-Rd-Carlisle-MA-01741/57058228_zpid/#charts-and-data", "mapthishome"=>"http://www.zillow.com/homes/57058228_zpid/", "comparables"=>"http://www.zillow.com/homes/comps/57058228_zpid/"}, "address"=>{"street"=>"208 Baldwin Rd", "zipcode"=>"01741", "city"=>"Carlisle", "state"=>"MA", "latitude"=>"42.516943", "longitude"=>"-71.34711"}, "FIPScounty"=>"25017", "useCode"=>"SingleFamily", "taxAssessmentYear"=>"2019", "taxAssessment"=>"714600.0", "yearBuilt"=>"1968", "lotSizeSqFt"=>"95832", "finishedSqFt"=>"2950", "bathrooms"=>"2.5", "bedrooms"=>"4", "totalRooms"=>"8", "lastSoldDate"=>"08/22/2008", "lastSoldPrice"=>{"__content__"=>"849000", "currency"=>"USD"}, "zestimate"=>{"amount"=>{"__content__"=>"818368", "currency"=>"USD"}, "last_updated"=>"08/22/2019", "oneWeekChange"=>{"deprecated"=>"true"}, "valueChange"=>{"__content__"=>"3844", "duration"=>"30", "currency"=>"USD"}, "valuationRange"=>{"low"=>{"__content__"=>"761082", "currency"=>"USD"}, "high"=>{"__content__"=>"875654", "currency"=>"USD"}}, "percentile"=>"0"}, "localRealEstate"=>{"region"=>{"zindexValue"=>"816,200", "links"=>{"overview"=>"http://www.zillow.com/local-info/MA-Carlisle/r_37751/", "forSaleByOwner"=>"http://www.zillow.com/carlisle-ma/fsbo/", "forSale"=>"http://www.zillow.com/carlisle-ma/"}, "name"=>"Carlisle", "id"=>"37751", "type"=>"city"}}}
                job = Zillow::DownloadBinderDetailsJob.new
                job.binder = @binder
                job.set_additional_binder_property_fields(result)
                @binder.reload

                expect(@binder.areas.count).to eq(8)
                expect(@binder.areas.pluck(:name)).to include "Master Bedroom"
                expect(@binder.areas.pluck(:name)).to include "2nd Bedroom"
                expect(@binder.areas.pluck(:name)).to include "3rd Bedroom"
                expect(@binder.areas.pluck(:name)).to include "4th Bedroom"
            end
        end
    end
end
