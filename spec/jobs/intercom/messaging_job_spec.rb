require 'rails_helper'

RSpec.describe Intercom::MessagingJob do
    let(:job) {Intercom::MessagingJob.new}
    let(:user){create :user}

    before :each do
        @intercom = double("Intercom::Client", :new => true, :users => true, :events => true, :messages => true)
        allow(Intercom::Client).to receive(:new).and_return(@intercom)
        allow(@intercom).to receive(:users).and_return(OpenStruct.new({:find => true}))
        allow(@intercom.users).to receive(:find)
        allow(@intercom).to receive(:events).and_return(OpenStruct.new({:create => true}))
        allow(@intercom.events).to receive(:create)
        allow(@intercom).to receive(:messages).and_return(OpenStruct.new({:create => true}))
        allow(@intercom.messages).to receive(:create)
        allow(ErrorService).to receive(:perform_async)
    end

    describe "#perform" do
        it "returns nil because user is nil" do
            job.perform(nil, "")
            expect(@intercom.users).to_not have_received(:find)
        end
        it "returns nil because user is an admin" do
            user.role = "admin"
            user.save
            job.perform(user.id, "")
            expect(@intercom.users).to_not have_received(:find)
        end
        it "returns nil because user is not in intercom" do
            allow(@intercom.users).to receive(:find).and_return(nil)
            job.perform(user.id, "")
            expect(@intercom.users).to have_received(:find)
            expect(@intercom.events).to_not have_received(:create)
        end
        it "returns nil because user_id is nil in intercom" do
            allow(@intercom.users).to receive(:find).and_return(OpenStruct.new({:user_id => nil}))
            job.perform(user.id, "")
            expect(@intercom.events).to_not have_received(:create)
        end
        it "posts an event but does not send a message" do
            user.last_intercom_error = 23.hours.ago
            user.save
            allow(@intercom.users).to receive(:find).and_return(OpenStruct.new({:user_id => 1}))
            job.perform(user.id, "")
            expect(@intercom.events).to have_received(:create)
            expect(@intercom.messages).to_not have_received(:create)
        end
        it "posts an event but does not send a message" do
            user.last_intercom_error = 12.hours.ago
            user.save
            allow(@intercom.users).to receive(:find).and_return(OpenStruct.new({:user_id => 1}))
            job.perform(user.id, "")
            expect(@intercom.events).to have_received(:create)
            expect(@intercom.messages).to_not have_received(:create)
        end
        it "posts an event but does not send a message" do
            user.last_intercom_error = 1.minute.ago
            user.save
            allow(@intercom.users).to receive(:find).and_return(OpenStruct.new({:user_id => 1}))
            job.perform(user.id, "")
            expect(@intercom.events).to have_received(:create)
            expect(@intercom.messages).to_not have_received(:create)
        end
        it "posts an event and sends a message" do
            user.last_intercom_error = 25.hours.ago
            user.save
            allow(@intercom.users).to receive(:find).and_return(OpenStruct.new({:user_id => 1}))
            job.perform(user.id, "")
            expect(@intercom.events).to have_received(:create)
            expect(@intercom.messages).to have_received(:create)
        end
        it "posts an event and sends a message with user name" do
            user.last_intercom_error = 25.hours.ago
            user.save
            allow(@intercom.users).to receive(:find).and_return(OpenStruct.new({:user_id => 1, :name => "First Name"}))
            job.perform(user.id, "")
            expect(@intercom.events).to have_received(:create)
            expect(@intercom.messages).to have_received(:create)
        end
    end

    describe "#send_message" do
        it "sends a message" do
            job.hb_user = user
            job.error = "Binder not found"
            job.intercom = @intercom
            job.send_message("inapp", "Hey User", 1)
            expect(@intercom.messages).to have_received(:create)
        end
        it "attempts to send a message but raises error" do
            allow(@intercom.messages).to receive(:create).and_raise(BadRequestException, "API not in service")
            job.hb_user = user
            job.error = "Binder not found"
            job.intercom = @intercom
            job.send_message("inapp", "Hey User", 1)
            expect(@intercom.messages).to have_received(:create)
            expect(ErrorService).to have_received(:perform_async)#.with("API not in service", {:custom_params => {:user_id => user.id}, :trace_only => true})
        end
    end

    describe "#post_event" do
        it "posts an event" do
            job.hb_user = user
            job.error = "Binder not found"
            job.intercom = @intercom
            job.post_event("user-error")
            expect(@intercom.events).to have_received(:create)
        end
        it "attempts to post an event but raises error" do
            allow(@intercom.events).to receive(:create).and_raise(BadRequestException, "API not in service")
            job.hb_user = user
            job.error = "Binder not found"
            job.intercom = @intercom
            job.post_event("user-error")
            expect(@intercom.events).to have_received(:create)
            expect(ErrorService).to have_received(:perform_async)#.with("API not in service", {:custom_params => {:user_id => user.id}, :trace_only => true})
        end
    end

    describe "#find_user_by_email" do
        it "raises an error" do
            allow(@intercom).to receive(:users).and_raise(BadRequestException)
            job.hb_user = user
            job.error = "Binder not found"
            job.intercom = @intercom
            job.find_user_by_email
            allow(ErrorService).to receive(:perform_async)
        end
    end

    describe "#get_token_name" do
        it "returns production token" do
            allow(Host).to receive(:path).and_return(Host::PRODUCTION_HOST)
            token = job.get_token_name
            expect(token).to eq(Intercom::MessagingJob::INTERCOM_PRODUCTION_TOKEN)
        end
    end
end
