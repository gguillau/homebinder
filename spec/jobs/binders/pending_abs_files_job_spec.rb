require 'rails_helper'

RSpec.describe Binders::PendingAbsFilesJob do
    let(:job) {Binders::PendingAbsFilesJob.new}

    before :each do
        allow(ErrorService).to receive(:perform_async)
    end
    
    describe "perform" do
        it "deletes binder and all user binders" do
            create(:contractor_contractor_category, :name => "Real Estate")
            create(:contractor_contractor_category, :name => "Home Inspector")
            partner = create(:partner, partner_key: "test")
            user = create(:user, email: partner.email, :role => "inspector")
            create(:user_profile, :user_id => user.id)
            user.add_role :partner_admin, partner
            config = partner.partner_configuration
            config.default_user_branding_id = user.user_profile.id
            config.save
            template = create(:binder_template, partner_configuration_id: config.id, transfer_note: "This is a transfer note")
            create(:appliance_template, binder_template_id: template.id)
            partner_contractor = create(:partner_contractor, :notes => "THIS IS A NOTE")
            create(:contractor_template, binder_template_id: template.id, :partner_contractor => partner_contractor)
            create(:document_template, binder_template_id: template.id)
            create(:maintenance_template, binder_template_id: template.id, due_date: nil, frequency: "As Needed", initial_due_date_interval: 200)

            image = {file: "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read), name: "SampleImage.png"}
            files = [{file: "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read), name: "SampleDoc.pdf"}]

            client =  {first: "Mike", last: "Breen", email: "test@gmail.com", phone: "712-593-3410"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}

            parameters = {
                :key => "test",
                :client => client,
                :property => property,
                :binder_template_id => template.id,
                :agent => agent,
                :property_photo => image,
                :documents => files
            }

            allow(Binders::PendingAbsFilesJob).to receive(:perform_async)
            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

            response = Partner::Automation::Service.create_binder(parameters)
            binder = Binder.find(response[:id])
            
            expect(binder.documents.count).to eq 0
            expect(binder.abs_files.count).to eq 1

            job.perform(binder.abs_files.first.id)
            
            expect(binder.documents.count).to eq 2
        end
    end
end
