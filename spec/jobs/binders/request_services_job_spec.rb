require 'rails_helper'

RSpec.describe Binders::RequestServicesJob do
    let(:job) {Binders::RequestServicesJob.new}

    before :each do
        allow(ErrorService).to receive(:perform_async)
    end
    
    describe "#perform" do
        it "requests services on the binder" do
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards

            double = double("AccountMailer", :deliver_later => true)
            allow(AccountMailer).to receive(:send_services_requested).and_return(double)

            user = create(:user, :email => "orphans@homebinder.com")
            binder = create(:binder)
            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")
            
            params = ActionController::Parameters.new({:services => ["Planning projects or getting quotes from local home pros", "Setting up utilities (gas, electric, water)"], :email => "support@homebinder.com"})
            job.perform(user.id, binder.id, params.to_unsafe_h)

            expect(AccountMailer).to have_received(:send_services_requested).exactly(1).times
        end
    end
end
