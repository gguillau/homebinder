require 'rails_helper'

RSpec.describe Binders::UpgradeBindersJob do
    let(:job) {Binders::UpgradeBindersJob.new}

    before :each do
        allow(ErrorService).to receive(:perform_async)
    end
    
    describe "#perform" do
        it "sends an email when item has not been verified" do
            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true})
            allow(Stripe::Customer).to receive(:create).and_return(stripe_customer)
            
            user = create(:user, :role => "inspector")
            partner = create(:partner)
            create(:coupon, :partner_id => partner.id)
            create(:partner_user, :user => user, :partner => partner, :role => "admin")
            binder = create(:binder)
            create(:partner_binder, :binder => binder, :partner => partner)
            create(:user_binder, :binder => binder, :user => user, :role => "owner")
            expect(binder.subscription.present?).to be(false)
            
            job.perform
            
            binder.reload
            expect(binder.subscription.present?).to be(true)
        end
    end
end
