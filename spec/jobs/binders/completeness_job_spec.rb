require 'rails_helper'

RSpec.describe Binders::CompletenessJob do
    let(:job) {Binders::CompletenessJob.new}

    before :each do
        allow(ErrorService).to receive(:perform_async)
    end
    
    describe "perform" do
        it "calculates the right score when binder has binder contractors" do
            admin = create(:user, :role => "admin")
            inspector = create(:user, :role => "inspector")
            
            homeowner = create(:user, :role => "homeowner")
            binder = create(:binder)
            create(:user_binder, :binder => binder, :user => homeowner, :role => "owner")
            create_list(:binder_contractor, 15, :binder => binder, :creator => homeowner)
            create_list(:binder_contractor, 15, :binder => binder, :creator => admin)
            create_list(:binder_contractor, 15, :binder => binder, :creator => inspector)
            
            job.perform(binder.id)
            
            binder.reload
            expect(binder.completeness_score * 100).to eq((20.to_f/119.to_f).round(2) * 100)
        end
        
        it "calculates the right score when binder meets all criteria" do
            admin = create(:user, :role => "admin")
            inspector = create(:user, :role => "inspector")
            
            homeowner = create(:user, :role => "homeowner")
            binder = create(:binder)
            create(:user_binder, :binder => binder, :user => homeowner, :role => "owner")
            create_list(:binder_contractor, 1, :binder => binder, :creator => homeowner)
            create_list(:binder_contractor, 1, :binder => binder, :creator => admin)
            create_list(:binder_contractor, 1, :binder => binder, :creator => inspector)
            create_list(:appliance, 10, :binder => binder, :manufacturer => "test", :model_number => "fake")
            create_list(:project, 10, :binder => binder)
            create_list(:document, 10, :binder => binder)
            document_type = create(:document_type, :name => "Home Inspection Report")
            create(:document, :binder => binder, :document_type_id => document_type.id)
            create_list(:area, 10, :binder => binder)
            create_list(:paint, 10, :binder => binder)
            create_list(:permit, 10, :binder => binder)
            create_list(:inventory_item, 10, :binder => binder)
            create_list(:maintenance_item, 10, :binder => binder)
            binder.property.purchase_price = 10
            binder.property.purchase_date = 2.years.ago
            binder.property.save!
            
            job.perform(binder.id)
            
            binder.reload
            expect(binder.completeness_score * 100).to eq((103.to_f/119.to_f).round(2) * 100)
        end
    end
end
