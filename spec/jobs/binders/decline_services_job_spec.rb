require 'rails_helper'

RSpec.describe Binders::DeclineServicesJob do
    let(:job) {Binders::DeclineServicesJob.new}

    before :each do
        allow(ErrorService).to receive(:perform_async)
    end
    
    describe "#perform" do
        it "declines services on the binder" do
            allow(EventService).to receive(:perform_async)

            user = create(:user, :email => "orphans@homebinder.com")
            binder = create(:binder)
            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")

            job.perform(user.id, binder.id, {:services => ["Planning projects or getting quotes from local home pros", "Setting up utilities (gas, electric, water)"], :email => "support@homebinder.com"})
            
            expect(EventService).to have_received(:perform_async).exactly(4).times
        end
    end
end
