require 'rails_helper'

RSpec.describe Binders::NotifyUnverifiedItemsJob do
    let(:job) {Binders::NotifyUnverifiedItemsJob.new}

    describe "perform" do
        it "calls BinderMailer" do
            homeowner_1 = create(:user, :role => "homeowner")
            binder = create(:binder)
            create(:user_binder, :binder => binder, :user => homeowner_1, :role => "owner")
            create(:binder_item, :binder => binder, :verified => false, :last_notify_date => nil)
            mailer = double("BinderMailer", :deliver_later => true)
            allow(BinderMailer).to receive(:notify_of_unverified_items).and_return(mailer)
            
            job.perform
            
            expect(BinderMailer).to have_received(:notify_of_unverified_items)
        end
    end
end
