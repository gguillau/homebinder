require 'rails_helper'

RSpec.describe Binders::UpdateExistingBindersFromTemplateJob do
    let(:job) {Binders::UpdateExistingBindersFromTemplateJob.new}

    before :each do
        allow(ErrorService).to receive(:perform_async)
    end
    
    describe "perform" do
        it "calls update_existing_binder_from_template" do
            allow(Binders::UpdateExistingBindersFromTemplateJob).to receive (:perform_async)
            
            user = create(:user)            
            binder_template = create(:binder_template)
            binder = create(:binder, binder_template_id: binder_template.id)
            create(:user_binder, user: user, binder: binder, role: "owner")
            contractor_template = create(:contractor_template, binder_template_id: binder_template.id)
            expect(binder.binder_contractors.count).to eq(0)
            
            job.perform(contractor_template.id)
            
            binder.reload
            expect(binder.binder_contractors.count).to eq(1)
        end
    end
end
