require 'rails_helper'

RSpec.describe Binders::BatchUploadJob, :type => :model do
    let(:job) {Binders::BatchUploadJob.new}
    
    describe "#perform" do
        it "creates the binder" do
            mailer = double("AccountMailer", :deliver_later => true)
            allow(AccountMailer).to receive(:batch_binder_upload_confirmation).and_return(mailer)
            binder = { first_name: "Bob", last_name: "Smith", email: "test@gmail.com", address1: "123 Main Street", city: "Boston", state: "MA", country: "US" }
            partner = create(:partner)
            user = create(:user, :role => "inspector")
            create(:partner_user, :user => user, :partner => partner, :role => "admin")
            template = create(:binder_template, :partner_configuration => partner.partner_configuration)
            partner.partner_configuration.default_binder_template_id = template.id
            partner.partner_configuration.save!
            
            params = {
                :partnerId => partner.id,
                :binders => [binder]
            }
            expect(partner.binders.count).to eq(0)
            parameters = ActionController::Parameters.new(params)
            
            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return({:key=>"", :client=>{:first=>"Bob", :last=>"Smith", :email=>"test@gmail.com", :phone=>nil}, :property=>{:address=>"123 Main Street", :address2=>nil, :city=>"Boston", :state=>"MA", :postalcode=>nil, :country=>"US"}, :binder_template_id=>template.id})
            
            job.perform(user.id, parameters.to_unsafe_h)

            expect(partner.binders.count).to eq(1)
            allow(AccountMailer).to receive(:batch_binder_upload_confirmation).with(user.id, 1, 0)
        end
        
        it "creates the binder and prevents duplicate in csv file" do
            mailer = double("AccountMailer", :deliver_later => true)
            allow(AccountMailer).to receive(:batch_binder_upload_confirmation).and_return(mailer)
            binder = { first_name: "Bob", last_name: "Smith", email: "test@gmail.com", address1: "123 Main Street", city: "Boston", state: "MA", country: "US" }
            partner = create(:partner)
            user = create(:user, :role => "inspector")
            create(:partner_user, :user => user, :partner => partner, :role => "admin")
            template = create(:binder_template, :partner_configuration => partner.partner_configuration)
            partner.partner_configuration.default_binder_template_id = template.id
            partner.partner_configuration.save!
            
            params = {
                :partnerId => partner.id,
                :binders => [binder, binder]
            }
            expect(partner.binders.count).to eq(0)
            parameters = ActionController::Parameters.new(params)
            
            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return({:key=>"", :client=>{:first=>"Bob", :last=>"Smith", :email=>"test@gmail.com", :phone=>nil}, :property=>{:address=>"123 Main Street", :address2=>nil, :city=>"Boston", :state=>"MA", :postalcode=>nil, :country=>"US"}, :binder_template_id=>template.id})
            
            job.perform(user.id, parameters.to_unsafe_h)

            expect(partner.binders.count).to eq(1)
            allow(AccountMailer).to receive(:batch_binder_upload_confirmation).with(user.id, 1, 1)
        end
        
        it "creates the binder and prevents existing duplicate and duplicate in file" do
            mailer = double("AccountMailer", :deliver_later => true)
            allow(AccountMailer).to receive(:batch_binder_upload_confirmation).and_return(mailer)
            pending_binder = { first_name: "Bob", last_name: "Smith", email: "test@gmail.com", address1: "123 Main Street", city: "Boston", state: "MA", country: "US", zip: "02210" }
            partner = create(:partner)
            user = create(:user, :role => "inspector")
            binder = create(:binder)
            create(:user_binder, :binder => binder, :user => user, :role => "owner")
            create(:partner_binder, :binder => binder, :partner => partner)
            create(:partner_user, :user => user, :partner => partner, :role => "admin")
            template = create(:binder_template, :partner_configuration => partner.partner_configuration)
            partner.partner_configuration.default_binder_template_id = template.id
            partner.partner_configuration.save!
            
            duplicate_binder = { first_name: "Bob", last_name: "Smith", email: "test@gmail.com", address1: binder.property.address1, city: binder.property.city, state: binder.property.state, zip: binder.property.zip, country: binder.property.country }
            
            params = {
                :partnerId => partner.id,
                :binders => [pending_binder, pending_binder, duplicate_binder]
            }
            expect(partner.binders.count).to eq(1)
            parameters = ActionController::Parameters.new(params)
            
            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return({:key=>"", :client=>{:first=>"Bob", :last=>"Smith", :email=>"test@gmail.com", :phone=>nil}, :property=>{:address=>"123 Main Street", :address2=>nil, :city=>"Boston", :state=>"MA", :postalcode=>nil, :country=>"US"}, :binder_template_id=>template.id})
            
            job.perform(user.id, parameters.to_unsafe_h)

            expect(partner.binders.count).to eq(2)
            allow(AccountMailer).to receive(:batch_binder_upload_confirmation).with(user.id, 1, 2)
        end
    end
end