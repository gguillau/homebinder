require 'rails_helper'

RSpec.describe Binders::RemovalJob do
    let(:job) {Binders::RemovalJob.new}

    before :each do
        allow(ErrorService).to receive(:perform_async)
    end
    
    describe "perform" do
        it "deletes binder and all user binders" do
            homeowner_1 = create(:user, :role => "homeowner")
            homeowner_2 = create(:user, :role => "homeowner")
            homeowner_3 = create(:user, :role => "homeowner")
            homeowner_4 = create(:user, :role => "homeowner")
            binder = create(:binder)
            create(:user_binder, :binder => binder, :user => homeowner_1, :role => "owner")
            create(:user_binder, :binder => binder, :user => homeowner_2, :role => "co_owner")
            create(:user_binder, :binder => binder, :user => homeowner_3, :role => "reader")
            create(:user_binder, :binder => binder, :user => homeowner_4, :role => "reader_writer")
            
            job.perform(binder.id)
            
            expect {Binder.find(binder.id)}.to raise_exception(ActiveRecord::RecordNotFound)
        end
    end
end
