require 'rails_helper'

RSpec.describe Binders::MaintenanceRemindersJob do
    let(:job) {Binders::MaintenanceRemindersJob.new}

    before :each do
        allow(ErrorService).to receive(:perform_async)
        @user = create :user
        @binder = create(:binder)
        @user.add_role(:owner, @binder)
        @item = create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 1.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
        double = double("MaintenanceNotifyMailer", :deliver_later => true)
        allow(MaintenanceNotifyMailer).to receive(:notify_email).and_return(double)
    end

    describe "perform" do
        context "when owner and co owner are not homeowner" do
            it "doesnt send an email because of users roles" do
                inspector_1 = create(:user, :role => "inspector")
                inspector_2 = create(:user, :role => "inspector")
                binder = create(:binder)
                inspector_1.add_role(:owner, binder)
                inspector_2.add_role(:co_owner, binder)
                create(:maintenance_item, binder_id: binder.id, do_date: Date.today + 1.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                job.perform

                expect(MaintenanceNotifyMailer).to_not have_received(:notify_email)
            end
        end
        context "when user sign_in_count is less than 1" do
            it "does not send an email" do
                job.perform

                expect(MaintenanceNotifyMailer).to_not have_received(:notify_email)
            end
        end
        context "when user sign_in_count is greater than 0" do
            before :each do
                @user.sign_in_count = 1
                @user.save
            end
            it "doesnt send an email because of existing notification" do
                @item.maintenance_events.create!(:do_date => DateTime.now.tomorrow.to_date, :last_notify_date => Date.today, :created_by => @user.id, :completed_date => nil)
                job.perform

                expect(MaintenanceNotifyMailer).to_not have_received(:notify_email)
            end
            it "sends an email" do
                job.perform

                expect(MaintenanceNotifyMailer).to have_received(:notify_email).exactly(1).times
                expect(@item.last_notified_event.last_notify_date).to eq(Date.current)
            end
            it "sends an email only once" do
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 2.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 3.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 4.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                event = @item.next_event
                next_due_date =  event.do_date.advance(:months => 6)
                job.perform
                expect(MaintenanceNotifyMailer).to have_received(:notify_email).exactly(1).times
                expect(@item.last_notified_event.last_notify_date).to eq(Date.current)
                expect(@item.next_event.do_date).to eq(next_due_date)
            end
            it "sends an email exactly two times" do
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 1.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 3.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 4.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                job.perform

                expect(MaintenanceNotifyMailer).to have_received(:notify_email).exactly(2).times
                expect(@item.last_notified_event.last_notify_date).to eq(Date.current)
            end
            it "sends an email exactly three times" do
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 1.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 1.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 4.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                job.perform

                expect(MaintenanceNotifyMailer).to have_received(:notify_email).exactly(3).times
                expect(@item.last_notified_event.last_notify_date).to eq(Date.current)
            end
            it "sends an email exactly four times" do
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 1.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 1.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 1.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                job.perform

                expect(MaintenanceNotifyMailer).to have_received(:notify_email).exactly(4).times
                expect(@item.last_notified_event.last_notify_date).to eq(Date.current)
            end
        end
        context "when user and coOwner sign_in_count is greater than 0" do
            before :each do
                @user.sign_in_count = 1
                @user.save
                coOwner = create(:user, :role => "homeowner", :sign_in_count => 1)
                create(:user_binder, :user => coOwner, :binder => @binder, :role => "co_owner")
            end
            it "sends an email twice" do
                job.perform

                expect(MaintenanceNotifyMailer).to have_received(:notify_email).exactly(2).times
                expect(@item.last_notified_event.last_notify_date).to eq(Date.current)
            end
            it "sends an email only twice" do
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 2.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 3.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 4.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                event = @item.next_event
                next_due_date =  event.do_date.advance(:months => 6)
                job.perform
                expect(MaintenanceNotifyMailer).to have_received(:notify_email).exactly(2).times
                expect(@item.last_notified_event.last_notify_date).to eq(Date.current)
                expect(@item.next_event.do_date).to eq(next_due_date)
            end
            it "sends an email exactly four times" do
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 1.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 3.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 4.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                job.perform

                expect(MaintenanceNotifyMailer).to have_received(:notify_email).exactly(4).times
                expect(@item.last_notified_event.last_notify_date).to eq(Date.current)
            end
            it "sends an email exactly six times" do
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 1.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 1.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 4.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                job.perform

                expect(MaintenanceNotifyMailer).to have_received(:notify_email).exactly(6).times
                expect(@item.last_notified_event.last_notify_date).to eq(Date.current)
            end
            it "sends an email exactly eight times" do
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 1.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 1.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                create(:maintenance_item, binder_id: @binder.id, do_date: Date.today + 1.day, email_notifications: true, maintenance_cycle: "Months", interval: "6")
                job.perform

                expect(MaintenanceNotifyMailer).to have_received(:notify_email).exactly(8).times
                expect(@item.last_notified_event.last_notify_date).to eq(Date.current)
            end
        end
        context "with owner and multiple coOwners" do
            before :each do
                @user.sign_in_count = 1
                @user.save
                # create coOwners
                coOwner1 = create(:user, :role => "homeowner", :sign_in_count => 1)
                coOwner2 = create(:user, :role => "homeowner", :sign_in_count => 1)
                coOwner3 = create(:user, :role => "homeowner", :sign_in_count => 1)
                coOwner4 = create(:user, :role => "homeowner", :sign_in_count => 1)
                # add coOwner roles to binder
                create(:user_binder, :user => coOwner1, :binder => @binder, :role => "co_owner")
                create(:user_binder, :user => coOwner2, :binder => @binder, :role => "co_owner")
                create(:user_binder, :user => coOwner3, :binder => @binder, :role => "co_owner")
                create(:user_binder, :user => coOwner4, :binder => @binder, :role => "co_owner")
            end
            it "sends an email 5 times" do
                job.perform

                expect(MaintenanceNotifyMailer).to have_received(:notify_email).exactly(5).times
                expect(@item.last_notified_event.last_notify_date).to eq(Date.current)
            end
        end
    end
end
