require 'rails_helper'

RSpec.describe Elasticsearch::ImportJob do
    let(:job) {Elasticsearch::ImportJob.new}

    describe "#perform" do
        it "imports data into ES" do
            indices = OpenStruct.new({:exists? => false})
            client = OpenStruct.new({:indices => indices})
            __elasticsearch__ = OpenStruct.new({:client => client, :create_index! => true})
            model = OpenStruct.new({:__elasticsearch__ => __elasticsearch__, :index_name => "users", :import => true})
            models = [model]

            allow(indices).to receive(:exists?).with(index: model.index_name).and_return(false)
            allow(model).to receive(:import)
            allow(ActiveRecord::Base).to receive(:descendants).and_return(models)

            job.perform

            expect(model).to have_received(:import)
        end

        it "calls report_error" do
            allow(ErrorService).to receive(:perform_async)

            allow(ActiveRecord::Base).to receive(:descendants).and_raise(BadRequestException)

            job.perform

            expect(ErrorService).to have_received(:perform_async)
        end
    end
end
