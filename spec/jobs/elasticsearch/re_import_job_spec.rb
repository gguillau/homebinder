require 'rails_helper'

RSpec.describe Elasticsearch::ReImportJob do
    let(:job) {Elasticsearch::ReImportJob.new}

    describe "#perform" do
        it "imports data into ES" do
            __elasticsearch__ = OpenStruct.new({:create_index! => true, :delete_index! => true, :import => true})
            model = OpenStruct.new({:__elasticsearch__ => __elasticsearch__})
            string = "model"
            allow(string).to receive_message_chain(:classify, :constantize).and_return(model)

            allow(model).to receive_message_chain(:__elasticsearch__, :delete_index!)
            allow(model).to receive_message_chain(:__elasticsearch__, :create_index!)
            allow(model).to receive_message_chain(:__elasticsearch__, :import)

            job.perform(string)

            expect(model).to have_received(:__elasticsearch__).exactly(3).times
        end

        it "calls report_error" do
            allow(ErrorService).to receive(:perform_async)
            
            job.perform("test")

            expect(ErrorService).to have_received(:perform_async)
        end
    end
end
