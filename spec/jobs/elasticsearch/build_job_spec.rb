require 'rails_helper'

RSpec.describe Elasticsearch::BuildJob do
    let(:job) {Elasticsearch::BuildJob.new}
    
    describe "#perform" do
        it "rebuilds the indices and calls import" do
            array = [{"index"=>"binder_binder_contractor_types"}, {"index"=>"addresses"}]
            cat = OpenStruct.new({:indices => true})
            indices = OpenStruct.new({:delete => true})
            client = OpenStruct.new({:cat => cat, :indices => indices})
            index = double("Elasticsearch::Index", :client => client)
            
            allow(Elasticsearch::IndexJob).to receive(:new).and_return(index)
            allow(cat).to receive(:indices).and_return(array)
            allow(indices).to receive(:delete)
            
            allow(Elasticsearch::ImportJob).to receive(:perform_async)
            
            job.perform

            expect(Elasticsearch::ImportJob).to have_received(:perform_async)
        end
        
        it "calls report_error" do
            allow(ErrorService).to receive(:perform_async)
            
            allow(Elasticsearch::IndexJob).to receive(:new).and_raise(BadRequestException)
            job.perform
            
            expect(ErrorService).to have_received(:perform_async)
        end
    end
end
