require 'rails_helper'

RSpec.describe Partners::WorkaroundJob do
    let(:job) {Partners::WorkaroundJob.new}

    before :each do
        allow(ErrorService).to receive(:perform_async)
    end

    describe "perform" do
        it "calls send_error" do
            allow(WorkaroundService).to receive(:new).and_raise(BadRequestException)
            # set the stubs
            create(:inspection_report_job)
            job.perform

            expect(ErrorService).to have_received(:perform_async)
        end
        
        it "creates a binder" do
            # set the stubs
            user = create(:user, :role => "inspector")
            partner = create(:partner, :email => user.email)
            inspection_report_job = create(:inspection_report_job, :partner => partner)
            binder_template = create(:binder_template, partner_configuration_id: partner.partner_configuration.id)

            user.add_role :partner_admin, partner
            config = partner.partner_configuration
            config.default_binder_template_id = binder_template.id
            config.default_user_branding_id = user.user_profile.id
            config.save!

            payload = {:key=>"", :client=>{:first=>"Jack", :last=>"Smith", :email=>"test@gmail.com", :phone=>"+13854920991"}, :property=>{:address=>"123 Main Street", :address2=>"", :city=>"Boston", :state=>"MA", :country=>"US", :zip=>"02210"}, :binder_template_id=> binder_template.id, :buyer_agent=>{}, :documents=>[{:url=>"https://dd3j5qtgf8uys.cloudfront.net.cloudfront.net/inspection_report_jobs2/SampleDoc.pdf?Expires=1567873507&Signature=RaOZmbHxxIvaagBVKmfb8ZNS0gS3c1s0RpfOQQFUvyTPl-QUviup~q5b~1jmJLN9nmM-jtbGuYtT8s920F0vrJSDou5ZEMrG8OF0Xg~x7UTut0qxCVqyafFBnPOeRD1Ubezqlr-V-rAZu7aOt07X8Kj8CVoMD6kIL1dnDUfvYxWo2Z76Bet-fesGsqjFBykKVzqnNv7pION0okD9m5UCOT0Dlw4UE-36kP7Q-rEhDdomxzxYCebzPK2OuECxPzJKxKIj~j3G5MU-y8XsV3pPP5Pgogr5oc-Cu2N6VHiewjTurcsnsvl536dDX8D6esdGNVDBshlyZ5ZqQ4STrqeH8w__&Key-Pair-Id=APKAIDZTFROKCRBC32FQ", :name=>"Home Inspection Report.pdf", :document_type=>"Home Inspection Report"}], :inspection_date=>"2019-03-19", :method=>"api"}
            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(payload)

            workaround_service = WorkaroundService.new
            allow(WorkaroundService).to receive(:new).and_return(workaround_service)
            output = {
                "homeowner_first_name" => "Jack",
                "homeowner_last_name" => "Smith",
                "homeowner_email" => "test@gmail.com",
                "homeowner_phone" => "+13854920991",
                "address1" => "123 Main Street",
                "address2" => "",
                "city" => "Boston",
                "state" => "MA",
                "country" => "US",
                "zip" => "02210",
                "inspection_date" => "03-19-2019",
            }
            allow(workaround_service).to receive(:get_call).and_return(output)

            job.perform

            inspection_report_job.reload
            expect(inspection_report_job.status).to eq("binder_created")
        end
    end
end
