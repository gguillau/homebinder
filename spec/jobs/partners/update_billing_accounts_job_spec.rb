require 'rails_helper'

RSpec.describe Partners::UpdateBillingAccountsJob do
    let(:job) {Partners::UpdateBillingAccountsJob.new}
    
    before :each do
        allow(ErrorService).to receive(:perform_async)
        @partner = create(:partner)
        @account = create(:account, :manager_id => @partner.id, :trial_expiration => Date.yesterday)
        @partner.account_id = @account.id
        @partner.save
    end
    
    it "raises an error" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true)
        allow(salesforce).to receive(:authenticate).and_raise(BadRequestException)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)
        expect{ job.perform}.to raise_error BadRequestException
        expect(ErrorService).to have_received(:perform_async)
    end
    
    it "does not save billing account and sends an error to New Relic" do
        # set the salesforce mock object
        salesforce_account = {
            :account_status__c => "Status",
            :account_sub_status__c => "",
            :account_sub_type__c => "",
            :account_type__c => "",
            :account_billing_frequency__c => "",
            :account_trial_expiration__c => "",
            :account_billing_activation__c => "",
            :account_subscription_amount__c => "",
            :account_transaction_amount__c => "",
            :account_payment_type__c => ""
        }
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true)
        allow(salesforce).to receive(:authenticate)
        allow(salesforce).to receive(:find_where).and_return([salesforce_account])
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow_any_instance_of(Address).to receive(:save).and_return(false)
        job.perform
        expect(ErrorService).to have_received(:perform_async).with(Partners::UpdateBillingAccountsJob::BILLING_SAVE_ERROR % "Account status is invalid", {:partner_id => @partner.id})
    end
    
    it "updates the partner billing account" do
        # set the salesforce mock object
        salesforce_account = {
            :account_status__c => "Expired",
            :account_sub_status__c => "",
            :account_sub_type__c => "",
            :account_type__c => "",
            :account_billing_frequency__c => "",
            :account_trial_expiration__c => Date.today,
            :account_billing_activation__c => Date.yesterday,
            :account_subscription_amount__c => "",
            :account_transaction_amount__c => "",
            :account_payment_type__c => ""
        }
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true)
        allow(salesforce).to receive(:authenticate)
        allow(salesforce).to receive(:find_where).and_return([salesforce_account])
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)
        expect(@account.account_status).to eq("active")
        job.perform
        @account.reload
        expect(@account.account_status).to eq("expired")
    end
    
    it "it performs a downcase on the string and replaces the spaces" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        field = job.format_value("active", "Free Trial")
        expect(field).to eq("free_trial")
    end
    
    it "it returns active" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        field = job.format_value("active", nil)
        expect(field).to eq("active")
    end
end