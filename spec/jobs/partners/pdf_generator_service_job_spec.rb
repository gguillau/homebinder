require 'rails_helper'

RSpec.describe Partners::PdfGeneratorServiceJob, :type => :service do
    let(:job) {Partners::PdfGeneratorServiceJob.new}

    before :each do
        # create spies
        double = double("AccountMailer", :deliver_later => true)
        allow(AccountMailer).to receive(:send_marketing_materials_notification).and_return(double)
    end

    describe "perform" do
        it "returns when the partner does not exist" do
            job.perform(1)
            expect(AccountMailer).to_not have_received(:send_marketing_materials_notification)
        end
        
        it "calls the AccountMailer" do
            allow(job).to receive(:pdf_url).and_return("url")
            allow(URI).to receive(:parse).and_return(File.open('spec/assets/SampleDoc.pdf'))
            
            partner = create(:partner)
            job.perform(partner.id)
            expect(AccountMailer).to have_received(:send_marketing_materials_notification)
        end
    end
    
    describe "pdf_url" do
        it "does things" do
            partner = create(:partner)

            response = job.pdf_url(partner, 1)
            expect(response).to eq("response")
        end
        
        it "doe" do
            stub_request(:any, /pdfgeneratorapi/).to_return(:status => 400, :body => '{"response": {}}')
            partner = create(:partner)

            expect{job.pdf_url(partner, 1)}.to raise_error BadRequestException
        end
    end
    
    describe "generate_pdf" do
        it "updates the resource if it already exists" do
            allow(job).to receive(:pdf_url).and_return("url")
            allow(URI).to receive(:parse).and_return("url")
            
            partner = create(:partner)
            marketing_resource = create(:marketing_resource, :document_template_id => 1)
            create(:partner_resource, :marketing_resource => marketing_resource, :partner => partner)
            allow(MarketingResource).to receive(:where).and_return([marketing_resource])
            allow(marketing_resource).to receive(:update)
            
            job.generate_pdf("name", "description", partner, 1)
            
            expect(marketing_resource).to have_received(:update)
        end
        
        it "creates the resource if it does not exist" do
            allow(job).to receive(:pdf_url).and_return("https://homebinder.com")
            allow(URI).to receive(:parse).and_return("url")
            
            partner = create(:partner)
            marketing_resource = create(:marketing_resource, :document_template_id => 1)
            create(:partner_resource, :marketing_resource => marketing_resource, :partner => partner)
            allow(MarketingResource).to receive(:where).and_return([])
            allow(MarketingResource).to receive(:create!).and_return(MarketingResource.create)
            allow(PartnerResource).to receive(:create!).and_return(nil)
            
            job.generate_pdf("name", "description", partner, 1)
            
            expect(MarketingResource).to have_received(:create!)
            expect(PartnerResource).to have_received(:create!)
        end
    end
    
    describe "workspace" do
        it "returns the workspace for production" do
            allow(Host).to receive(:path).and_return(Host::PRODUCTION_HOST)
            # get the workspace
            workspace = job.workspace

            expect(workspace).to eq(ENV["PDF_GENERATOR_PRODUCTION_WORKSPACE"].to_s)
        end
    
        it "returns the workspace for test" do
            allow(Host).to receive(:path).and_return("Test")
            # get the workspace
            workspace = job.workspace

            expect(workspace).to eq(ENV["PDF_GENERATOR_TEST_WORKSPACE"].to_s)
        end
    end
    
    describe "base_url" do
        it "returns the base_url for production" do
            allow(Host).to receive(:path).and_return(Host::PRODUCTION_HOST)
            # get the base_url
            base_url = job.base_url

            expect(base_url).to eq(ENV["PDF_GENERATOR_PRODUCTION_BASE_URL"].to_s)
        end
    
        it "returns the base_url for test" do
            allow(Host).to receive(:path).and_return("Test")
            # get the base_url
            base_url = job.base_url

            expect(base_url).to eq(ENV["PDF_GENERATOR_TEST_BASE_URL"].to_s)
        end
    end
    
    describe "secret" do
        it "returns the secret for production" do
            allow(Host).to receive(:path).and_return(Host::PRODUCTION_HOST)
            # get the secret
            secret = job.secret

            expect(secret).to eq(ENV["PDF_GENERATOR_PRODUCTION_SECRET"].to_s)
        end
    
        it "returns the secret for test" do
            allow(Host).to receive(:path).and_return("Test")
            # get the secret
            secret = job.secret

            expect(secret).to eq(ENV["PDF_GENERATOR_TEST_SECRET"].to_s)
        end
    end
    
    describe "key" do
        it "returns the key for production" do
            allow(Host).to receive(:path).and_return(Host::PRODUCTION_HOST)
            # get the key
            key = job.key

            expect(key).to eq(ENV["PDF_GENERATOR_PRODUCTION_KEY"].to_s)
        end
    
        it "returns the key for test" do
            allow(Host).to receive(:path).and_return("Test")
            # get the key
            key = job.key

            expect(key).to eq(ENV["PDF_GENERATOR_TEST_KEY"].to_s)
        end
    end

end