require 'rails_helper'

RSpec.describe Partners::BrokerMintJob do
    let(:job) {Partners::BrokerMintJob.new}
    
    before :each do
        allow(ErrorService).to receive(:perform_async)
    end
    
    describe "perform" do
        it "calls retrieve_transaction" do
            partner = create(:partner)
            partner.partner_configuration.broker_mint_api_key = "test"
            partner.partner_configuration.save!
            
            brokermint_client = Brokermint::Client.new
            allow(Brokermint::Client).to receive(:new).and_return(brokermint_client)
            allow(brokermint_client).to receive(:get_transactions).and_return([{"id" => 1}])
            
            allow(job).to receive(:retrieve_transaction)
            
            job.perform
            
            expect(job).to have_received(:retrieve_transaction)
        end
        
        it "calls report_error" do
            partner = create(:partner)
            partner.partner_configuration.broker_mint_api_key = "test"
            partner.partner_configuration.save!
            
            allow(Brokermint::Client).to receive(:new).and_raise(BadRequestException)

            job.perform
            
            expect(ErrorService).to have_received(:perform_async)
        end
    end
    
    describe "retrieve_transaction" do
        it "does not call create_binder because status is listing" do
            partner = create(:partner)
            partner.partner_configuration.broker_mint_api_key = "test"
            partner.partner_configuration.save!
            
            brokermint_client = Brokermint::Client.new
            allow(Brokermint::Client).to receive(:new).and_return(brokermint_client)
            allow(brokermint_client).to receive(:get_transaction).and_return({"id" => 1, "status" => "listing"})
            allow(brokermint_client).to receive(:get_transaction_participants).and_return([{"id" => 1, "type" => "Contact", "role" => "Buyer"}])
            allow(brokermint_client).to receive(:get_contact).and_return({"id" => 1})

            allow(job).to receive(:create_binder)
            
            job.retrieve_transaction("test", 1)
            
            expect(job).to_not have_received(:create_binder)
        end
        
        it "calls create_binder" do
            partner = create(:partner)
            partner.partner_configuration.broker_mint_api_key = "test"
            partner.partner_configuration.save!
            
            brokermint_client = Brokermint::Client.new
            allow(Brokermint::Client).to receive(:new).and_return(brokermint_client)
            allow(brokermint_client).to receive(:get_transaction).and_return({"id" => 1, "status" => "closed"})
            allow(brokermint_client).to receive(:get_transaction_participants).and_return([{"id" => 1, "type" => "Contact", "role" => "Buyer"}])
            allow(brokermint_client).to receive(:get_contact).and_return({"id" => 1})
            
            allow(job).to receive(:create_binder)
            
            job.retrieve_transaction("test", 1)
            
            expect(job).to have_received(:create_binder)
        end
        
        it "calls report_error" do
            partner = create(:partner)
            partner.partner_configuration.broker_mint_api_key = "test"
            partner.partner_configuration.save!
            
            allow(Brokermint::Client).to receive(:new).and_raise(BadRequestException)

            job.retrieve_transaction("test", 1)
            
            expect(ErrorService).to have_received(:perform_async)
        end
    end
    
    describe "create_binder" do
        it "calls Partner::Automation::API::Service create_binder" do
            partner = create(:partner)
            partner.partner_configuration.broker_mint_api_key = "test"
            partner.partner_configuration.save!
            
            buyer = {
                "first_name": "John",
                "last_name": "Smith",
                "email": "johnsmith@gmail.com",
                "phone": "+13324234234"
            }
            
            transaction = {
                "address": "123 Main Street",
                "city": "Boston",
                "state": "MA",
                "zip": "02210"
            }
            
            allow(Partner::Automation::API::Service).to receive(:create_binder)
            
            job.create_binder(partner, buyer, transaction)
            
            expect(Partner::Automation::API::Service).to have_received(:create_binder)
        end
        
        it "calls report_error" do
            partner = create(:partner)
            partner.partner_configuration.broker_mint_api_key = "test"
            partner.partner_configuration.save!
            
            buyer = {
                "first_name": "John",
                "last_name": "Smith",
                "email": "johnsmith@gmail.com",
                "phone": "+13324234234"
            }
            
            transaction = {
                "address": "123 Main Street",
                "city": "Boston",
                "state": "MA",
                "zip": "02210"
            }
            
            allow(Partner::Automation::API::Service).to receive(:create_binder).and_raise(BadRequestException)

            job.create_binder(partner, buyer, transaction)
            
            expect(ErrorService).to have_received(:perform_async)
        end
    end
end