require 'rails_helper'

RSpec.describe Partners::RemovalJob do
    let(:job) {Partners::RemovalJob.new}

    before :each do
        allow(ErrorService).to receive(:perform_async)
    end

    describe "#perform" do
        it "It calls intercom" do
            companies = OpenStruct.new({:find => true, :save => true, :create => true})
            events = OpenStruct.new({:find => true, :save => true, :create => true})
            intercom = double("Intercom::HomeBinderClient", :metadata_for_partner => true, :intercom => OpenStruct.new({companies: companies, events: events}))
            allow(Intercom::HomeBinderClient).to receive(:new).and_return(intercom)

            allow(intercom).to receive(:companies).and_return(companies)
            allow(intercom).to receive(:events).and_return(events)
            allow(intercom).to receive(:metadata_for_partner).and_return({})
            allow(intercom.companies).to receive(:create)
            allow(intercom.events).to receive(:create)
            
            user = create(:user, :role => "inspector")
            partner = create(:partner, :email => user.email)
            template = partner.partner_configuration.generate_default_template
            partner.partner_configuration.default_binder_template_id = template.id
            partner.partner_configuration.save!
            
            create(:partner_user, :user => user, :partner => partner, :role => 'admin')
            
            job.perform(partner.id)
            
            expect(intercom).to have_received(:companies)
            expect(intercom).to have_received(:events)
        end
        
        it "It calls error service" do
            companies = OpenStruct.new({:find => true, :save => true, :create => true})
            events = OpenStruct.new({:find => true, :save => true, :create => true})
            intercom = double("Intercom::HomeBinderClient", :metadata_for_partner => true, :intercom => OpenStruct.new({companies: companies, events: events}))
            allow(Intercom::HomeBinderClient).to receive(:new).and_return(intercom)

            allow(intercom).to receive(:events).and_return(events)
            allow(intercom.events).to receive(:create).and_raise(BadRequestException)
            
            user = create(:user, :role => "inspector")
            partner = create(:partner, :email => user.email)
            template = partner.partner_configuration.generate_default_template
            partner.partner_configuration.default_binder_template_id = template.id
            partner.partner_configuration.save!
            
            create(:partner_user, :user => user, :partner => partner, :role => 'admin')
            
            job.perform(partner.id)
            
            expect(ErrorService).to have_received(:perform_async)
        end
    end
end
