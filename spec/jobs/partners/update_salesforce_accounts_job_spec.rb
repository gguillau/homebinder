require 'rails_helper'

RSpec.describe Partners::UpdateSalesforceAccountsJob do
    let(:job) {Partners::UpdateSalesforceAccountsJob.new}
    
    describe "perform" do
        it "calls update_partner" do
            create(:partner)
            expect_any_instance_of(Salesforce::Partner).to receive(:update_partner)
            job.perform
        end
        
        it "calls ErrorService" do
            allow(ErrorService).to receive(:perform_async)
            create(:partner)
            allow_any_instance_of(Salesforce::Partner).to receive(:update_partner).and_raise(BadRequestException)
            job.perform
            
            expect(ErrorService).to have_received(:perform_async)
        end
    end
end