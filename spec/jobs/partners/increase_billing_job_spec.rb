require 'rails_helper'

RSpec.describe Partners::IncreaseBillingJob do
    let(:job) {Partners::IncreaseBillingJob.new}

    before :each do
        allow(ErrorService).to receive(:perform_async)
    end
    
    describe "#perform" do
        it "it increase billing for partner" do
            partner = create(:partner)
            account = create(:account, :manager_id => partner.id, :stripe_customer_id => "test", :subscription_amount_cents => 1600, :account_status => "active")
            create_list(:partner_binder, 51, :partner => partner)
            
            plan = OpenStruct.new({:amount => 1600})
            subscription = OpenStruct.new({:plan => plan})
            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", subscription: subscription, save: true})
            allow(Stripe::Customer).to receive(:retrieve).and_return(stripe_customer)
            allow(stripe_customer.subscription).to receive(:plan).and_return(plan)
            
            job.perform
            
            account.reload
            expect(account.subscription_amount_cents).to eq(4900)
        end
        
        it "it calls new relic" do
            partner = create(:partner)
            create(:account, :manager_id => partner.id, :stripe_customer_id => "test", :subscription_amount_cents => 1600, :account_status => "active")
            create_list(:partner_binder, 51, :partner => partner)
            
            allow(Stripe::Customer).to receive(:retrieve).and_raise(BadRequestException)
            
            job.perform
            
            expect(ErrorService).to have_received(:perform_async)
        end
    end
end
