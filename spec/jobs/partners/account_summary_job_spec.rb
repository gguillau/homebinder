require 'rails_helper'

RSpec.describe Partners::AccountSummaryJob do
    let(:job) {Partners::AccountSummaryJob.new}

    before :each do
        allow(ErrorService).to receive(:perform_async)
    end

    describe "#perform" do
        before :each do
            @admin = create(:user, :role => "admin")
            @partner = create(:partner, :partner_type => "inspector")
            @partner_2 = create(:partner)
            @partner_user = create(:user, :role => "inspector")
            @partner_user.add_role :partner_admin, @partner
        end

        it "It sends an email" do
            # create account
            account = create(:account, manager_id: @partner.id, account_status: "active")
            @partner.account_id = account.id
            @partner.save
            account = create(:account, manager_id: @partner_2.id, account_status: "active")
            @partner_2.account_id = account.id
            @partner_2.save
            # create spy
            double = double("KpiMailer", :deliver_later => true)
            allow(KpiMailer).to receive(:send_account_summary).and_return(double)

            job.perform(@admin.id)
            expect(KpiMailer).to have_received(:send_account_summary)
        end
    end
end
