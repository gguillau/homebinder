require 'rails_helper'

RSpec.describe Partners::RepairPricerStatusJob do
    let(:job) {Partners::RepairPricerStatusJob.new}
    
    before :each do
        allow(ErrorService).to receive(:perform_async)
    end

    describe "check_report_status" do
        it "calls report_error" do
            allow(RepairPricer::Client).to receive(:new).and_raise(BadRequestException)
            # set the stubs
            create(:repair_pricer_report)
            Partners::RepairPricerStatusJob.new.perform
            
            expect(ErrorService).to have_received(:perform_async)
        end
        
        it "calls download_report" do
            # set the stubs
            repair_pricer_report = create(:repair_pricer_report)
            repair_pricer_client = RepairPricer::Client.new
            allow(RepairPricer::Client).to receive(:new).and_return(repair_pricer_client)
            allow(repair_pricer_client).to receive(:get_report).and_return({"status" => "10"})
            job.perform
            
            repair_pricer_report.reload
            expect(repair_pricer_report.status).to eq("submitted")
        end
        
        it "sets status to accepted" do
            # set the stubs
            repair_pricer_report = create(:repair_pricer_report)
            repair_pricer_client = RepairPricer::Client.new
            allow(RepairPricer::Client).to receive(:new).and_return(repair_pricer_client)
            allow(repair_pricer_client).to receive(:get_report).and_return({"status" => "20"})
            job.perform
            
            repair_pricer_report.reload
            expect(repair_pricer_report.status).to eq("accepted")
        end
        
        it "sets status to in_progress" do
            # set the stubs
            repair_pricer_report = create(:repair_pricer_report)
            repair_pricer_client = RepairPricer::Client.new
            allow(RepairPricer::Client).to receive(:new).and_return(repair_pricer_client)
            allow(repair_pricer_client).to receive(:get_report).and_return({"status" => "30"})
            job.perform
            
            repair_pricer_report.reload
            expect(repair_pricer_report.status).to eq("in_progress")
        end
        
        it "sets status to on_hold" do
            # set the stubs
            repair_pricer_report = create(:repair_pricer_report)
            repair_pricer_client = RepairPricer::Client.new
            allow(RepairPricer::Client).to receive(:new).and_return(repair_pricer_client)
            allow(repair_pricer_client).to receive(:get_report).and_return({"status" => "40"})
            job.perform
            
            repair_pricer_report.reload
            expect(repair_pricer_report.status).to eq("on_hold")
        end
        
        it "sets status to rejected" do
            # set the stubs
            repair_pricer_report = create(:repair_pricer_report)
            repair_pricer_client = RepairPricer::Client.new
            allow(RepairPricer::Client).to receive(:new).and_return(repair_pricer_client)
            allow(repair_pricer_client).to receive(:get_report).and_return({"status" => "99"})
            job.perform
            
            repair_pricer_report.reload
            expect(repair_pricer_report.status).to eq("rejected")
        end
        
        it "calls download_report" do
            # set the stubs
            create(:repair_pricer_report)
            allow(job).to receive(:download_report)
            repair_pricer_client = RepairPricer::Client.new
            allow(RepairPricer::Client).to receive(:new).and_return(repair_pricer_client)
            allow(repair_pricer_client).to receive(:get_report).and_return({"status" => "50"})
            job.perform
            expect(job).to have_received(:download_report)
        end
    end
    
    describe "download_report_items" do
        it "calls report_error" do
            allow(RepairPricer::Client).to receive(:new).and_raise(BadRequestException)
            # set the stubs
            repair_pricer_report = create(:repair_pricer_report)
            job.download_report_items(repair_pricer_report.report_id)
            
            expect(ErrorService).to have_received(:perform_async)
        end
        
        it "calls download_report" do
            # set the stubs
            repair_pricer_report = create(:repair_pricer_report)
            repair_pricer_client = RepairPricer::Client.new
            allow(RepairPricer::Client).to receive(:new).and_return(repair_pricer_client)
            allow(repair_pricer_client).to receive(:download_report).and_return({
                "id"=>"206055", "data"=>[
                    {   "ID" => 1, 
                        "Repair" => 2, 
                        "Items" => {
                            "Page" =>"1", 
                            "Code" =>"18",
                            "Contractor" =>"Appliance",
                            "Item" => "Dishwashers (1 EA)",
                            "Action" =>"Remove & reset dishwasher",
                            "DPrice" => "52",
                            "PPrice" => "0",
                            "BPrice" => "0",
                            "CPrice" => "0"
                        }
                    }
                ]
            })
            
            expect(repair_pricer_report.repair_pricer_report_findings.count).to eq(0)
            job.download_report_items(repair_pricer_report.report_id)
            expect(repair_pricer_report.repair_pricer_report_findings.count).to eq(1)
        end
    end
    
    describe "download_report" do
        it "calls report_error" do
            allow(URI).to receive(:parse).and_raise(BadRequestException)
            repair_pricer_report = create(:repair_pricer_report)
            
            job.download_report(repair_pricer_report.report_id, {"reporturl" => "https://www.nachi.org/documents2012/Checklist/CBHI%20Home%20Inspection%20Report.pdf"})
            
            expect(ErrorService).to have_received(:perform_async)
        end
        
        it "calls download_report" do
            # set the stubs
            repair_pricer_report = create(:repair_pricer_report)
            binder = repair_pricer_report.binder
            allow(URI).to receive(:parse).and_return(File.open('spec/assets/SampleDoc.pdf'))
            
            expect(repair_pricer_report.repair_report?).to be(false)
            expect(binder.documents.count).to eq(0)
            
            job.download_report(repair_pricer_report.report_id, {"reporturl" => "https://www.nachi.org/documents2012/Checklist/CBHI%20Home%20Inspection%20Report.pdf"})
            repair_pricer_report.reload
            
            expect(repair_pricer_report.repair_report?).to be(true)
            expect(binder.documents.count).to eq(1)
        end
    end
    
    describe "download_pool_report" do
        it "calls report_error" do
            allow(URI).to receive(:parse).and_raise(BadRequestException)
            repair_pricer_report = create(:repair_pricer_report)
            
            job.download_pool_report(repair_pricer_report.report_id, {"reporturlpool" => "https://www.nachi.org/documents2012/Checklist/CBHI%20Home%20Inspection%20Report.pdf"})
            
            expect(ErrorService).to have_received(:perform_async)
        end
        
        it "calls download_pool_report" do
            # set the stubs
            repair_pricer_report = create(:repair_pricer_report)
            binder = repair_pricer_report.binder
            allow(URI).to receive(:parse).and_return(File.open('spec/assets/SampleDoc.pdf'))
            
            expect(repair_pricer_report.pool_report?).to be(false)
            expect(binder.documents.count).to eq(0)
            
            job.download_pool_report(repair_pricer_report.report_id, {"reporturlpool" => "https://www.nachi.org/documents2012/Checklist/CBHI%20Home%20Inspection%20Report.pdf"})
            repair_pricer_report.reload
            
            expect(repair_pricer_report.pool_report?).to be(true)
            expect(binder.documents.count).to eq(1)
        end
    end
    
    describe "download_home_history_report" do
        it "calls report_error" do
            allow(URI).to receive(:parse).and_raise(BadRequestException)
            repair_pricer_report = create(:repair_pricer_report)
            
            job.download_home_history_report(repair_pricer_report.report_id, {"reporturlhh" => "https://www.nachi.org/documents2012/Checklist/CBHI%20Home%20Inspection%20Report.pdf"})
            
            expect(ErrorService).to have_received(:perform_async)
        end
        
        it "calls download_home_history_report" do
            # set the stubs
            repair_pricer_report = create(:repair_pricer_report)
            binder = repair_pricer_report.binder
            allow(URI).to receive(:parse).and_return(File.open('spec/assets/SampleDoc.pdf'))
            
            expect(repair_pricer_report.home_history_report?).to be(false)
            expect(binder.documents.count).to eq(0)
            
            job.download_home_history_report(repair_pricer_report.report_id, {"reporturlhh" => "https://www.nachi.org/documents2012/Checklist/CBHI%20Home%20Inspection%20Report.pdf"})
            repair_pricer_report.reload
            
            expect(repair_pricer_report.home_history_report?).to be(true)
            expect(binder.documents.count).to eq(1)
        end
    end
    
    describe "add_document_to_binder" do
        it "calls report_error" do
            allow(URI).to receive(:parse).and_raise(BadRequestException)
            repair_pricer_report = create(:repair_pricer_report)
            
            job.add_document_to_binder(repair_pricer_report.binder.id, "https://www.nachi.org/documents2012/Checklist/CBHI%20Home%20Inspection%20Report.pdf")
            
            expect(ErrorService).to have_received(:perform_async)
        end
        
        it "adds the document to the binder" do
            # set the stubs
            repair_pricer_report = create(:repair_pricer_report)
            binder = repair_pricer_report.binder
            allow(URI).to receive(:parse).and_return(File.open('spec/assets/SampleDoc.pdf'))

            expect(binder.documents.count).to eq(0)
            
            job.add_document_to_binder(binder.id, "https://www.nachi.org/documents2012/Checklist/CBHI%20Home%20Inspection%20Report.pdf")

            expect(binder.documents.count).to eq(1)
        end
    end
end