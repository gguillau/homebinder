require 'rails_helper'

RSpec.describe Partners::SwitchBinderJob do
    let(:job) {Partners::SwitchBinderJob.new}

    before :each do
        allow(ErrorService).to receive(:perform_async)
    end

    describe "#perform" do
        it "It calls intercom" do
            partner_1 = create(:partner,partner_key: "test")
            partner_2 = create(:partner)
            
            create(:contractor_contractor_category, :name => "Real Estate")
            create(:contractor_contractor_category, :name => "Home Inspector")
            user = create(:user, email: partner_1.email, :role => "inspector")
            create(:user_profile, :user_id => user.id)
            binder = create(:binder)
            user.add_role :partner_admin, partner_1
            user.add_role :owner, binder
            config = partner_1.partner_configuration
            config.default_user_branding_id = user.user_profile.id
            config.save
            template = create(:binder_template, partner_configuration_id: config.id, transfer_note: "This is a transfer note")
            create(:appliance_template, binder_template_id: template.id)
            partner_contractor = create(:partner_contractor, :notes => "THIS IS A NOTE")
            create(:contractor_template, binder_template_id: template.id, :partner_contractor => partner_contractor)
            create(:document_template, binder_template_id: template.id)
            create(:maintenance_template, binder_template_id: template.id, due_date: nil, frequency: "As Needed", initial_due_date_interval: 200)
            
            user_2 = create(:user, email: partner_2.email, :role => "inspector")
            create(:user_profile, :user_id => user_2.id)
            user_2.add_role :partner_admin, partner_2
            config_2 = partner_2.partner_configuration
            config_2.default_user_branding_id = user_2.user_profile.id
            config_2.save
            template = create(:binder_template, partner_configuration_id: config_2.id, transfer_note: "This is a transfer note")
            config_2.default_binder_template_id = template.id
            config_2.save!
            create(:appliance_template, binder_template_id: template.id)
            partner_contractor = create(:partner_contractor, :notes => "THIS IS A NOTE")
            create(:contractor_template, binder_template_id: template.id, :partner_contractor => partner_contractor)
            create(:document_template, binder_template_id: template.id)
            create(:maintenance_template, binder_template_id: template.id, due_date: nil, frequency: "As Needed", initial_due_date_interval: 200)
                
            job.perform(partner_1.id, partner_2.id, binder.id)
            
            binder.reload
            expect(binder.owner.id).to eq(user_2.id)
        end
    end
end
