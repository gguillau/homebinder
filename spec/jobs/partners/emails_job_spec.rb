require 'rails_helper'

RSpec.describe Partners::EmailsJob do
    let(:job) {Partners::EmailsJob.new}

    before :each do
        allow(ErrorService).to receive(:perform_async)
    end

    describe "#perform" do
        before :each do
            @partner = create(:partner, :partner_type => "inspector")
            @user = create(:user, :role => "inspector")
            @user.add_role :partner_admin, @partner

            allow(Partner).to receive(:get_biweekly_metrics).and_call_original

            recurrence = Recurrence.new(:every => :week, :on => :monday, :interval => 2, :starts => '2019-06-20')
            allow(Recurrence).to receive(:new).and_return(recurrence)
        end

        it "does not do the check on a sunday" do
            account = create(:account, manager_id: @partner.id, account_status: "active", transaction_amount_cents: 7000)
            @partner.account_id = account.id
            @partner.save

            allow(Date).to receive(:today).and_return(Date.today.sunday)
            job.perform

            expect(Partner).to_not have_received(:get_biweekly_metrics)
        end

        it "does not do the check on a monday not in the recurrence" do
            account = create(:account, manager_id: @partner.id, account_status: "active", transaction_amount_cents: 7000)
            @partner.account_id = account.id
            @partner.save

            allow(Date).to receive(:today).and_return(Date.new(2019,7,8))
            job.perform

            expect(Partner).to_not have_received(:get_biweekly_metrics)
        end

        it "fails the check because partner has no account" do
            allow(Date).to receive(:today).and_return(Date.new(2019,7,1))
            job.perform

            expect(Partner).to_not have_received(:get_biweekly_metrics)
        end

        it "does the check up to December 2037" do
            account = create(:account, manager_id: @partner.id, account_status: "active", transaction_amount_cents: 7000)
            @partner.account_id = account.id
            @partner.save

            allow(Date).to receive(:today).and_return(Date.new(2037,12,21))
            job.perform

            expect(Partner).to have_received(:get_biweekly_metrics)
        end

        it "does the check when transaction_amount_cents is greater than 0" do
            account = create(:account, manager_id: @partner.id, account_status: "active", transaction_amount_cents: 7000)
            @partner.account_id = account.id
            @partner.save

            allow(Date).to receive(:today).and_return(Date.new(2019,7,1))
            job.perform

            expect(Partner).to have_received(:get_biweekly_metrics)
        end

        it "does the check when subscription_amount_cents is greater than 0" do
            account = create(:account, manager_id: @partner.id, account_status: "active", subscription_amount_cents: 7000)
            @partner.account_id = account.id
            @partner.save

            allow(Date).to receive(:today).and_return(Date.new(2019,7,1))
            job.perform

            expect(Partner).to have_received(:get_biweekly_metrics)
        end

        it "it raises an error" do
            account = create(:account, manager_id: @partner.id, account_status: "active", subscription_amount_cents: 7000)
            @partner.account_id = account.id
            @partner.save

            allow(Date).to receive(:today).and_return(Date.new(2019,7,1))
            allow(Partner).to receive(:get_biweekly_metrics).and_raise(BadRequestException)
            job.perform

            expect(ErrorService).to have_received(:perform_async)
        end
        
        it "calls KpiMailer send_weekly_metrics" do
            account = create(:account, manager_id: @partner.id, account_status: "active", subscription_amount_cents: 7000)
            @partner.account_id = account.id
            @partner.save
            
            mailer = double("KpiMailer", :deliver_later => true)
            allow(KpiMailer).to receive(:send_weekly_metrics).and_return(mailer)
            allow(Date).to receive(:today).and_return(Date.new(2019,7,1))
            allow(Partner).to receive(:get_biweekly_metrics)
            job.perform

            expect(KpiMailer).to have_received(:send_weekly_metrics)
        end
    end
end
