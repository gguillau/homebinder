require 'rails_helper'

RSpec.describe Warranties::DownloadJob do
    let(:job) {Warranties::DownloadJob.new}
    
    before :each do
        double = double("WarrantyMailer", :deliver_later => true)
        allow(WarrantyMailer).to receive(:send_warranty_report).and_return(double)
    end
    
    describe "#perform" do
        it "It sends an email to user for report" do
            admin = create(:user, :role => "admin")
            homeowner = create(:user, :role => "homeowner")
            binder = create(:binder)
            partner = create(:partner)
            create(:partner_binder, :partner_id => partner.id, :binder_id => binder.id)
            create(:user_binder, :binder_id => binder.id, :user_id => homeowner.id, :role => "owner")
            warranty_company = create(:warranty_company)
            warranty_plan = create(:warranty_plan, :warranty_company_id => warranty_company.id)
            warranty = create(:warranty, :warranty_plan_id => warranty_plan.id, :binder_id => binder.id, :status => "pending")
            binder_transaction = create(:binder_transaction, :transaction_type => "buy_side_inspection", :transaction_date => 5.days.ago, :binder_id => binder.id)
            create(:warranty_transaction, :binder_transaction_id => binder_transaction.id, :warranty_id => warranty.id)
            create(:partner_binder_transaction, :binder_transaction_id => binder_transaction.id, :partner_id => partner.id)
            create(:warranty_configuration, :partner_id => partner.id, :warranty_company_id => warranty_company.id, :warranty_plan_id => warranty_plan.id)
            expect(warranty.status).to eq("pending")
            
            job.perform(admin.id)
            
            # check the call
            expect(WarrantyMailer).to have_received(:send_warranty_report)
        end
    end
    
end
