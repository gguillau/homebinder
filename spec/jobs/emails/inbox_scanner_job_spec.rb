require 'rails_helper'
require 'mail'

RSpec.describe Email::InboxScannerJob do
    let(:job) {Email::InboxScannerJob.new}

    before :each do
        allow(ErrorService).to receive(:perform_async)
    end

    let(:api_key) { FactoryBot.create(:api_key) }
    let(:partner) { FactoryBot.create(:partner, name: "Test Inspections", contact: "Test Inspector", partner_key: "axium", :partner_type => "inspector") }
    let(:user) { FactoryBot.create(:user, email: partner.email, :role => "inspector")}
    let(:partner_two) { FactoryBot.create(:partner, partner_key: "axium") }
    let(:partner_key) { FactoryBot.create(:api_key, partner_id: partner.id) }
    let(:config) { partner_two.partner_configuration }
    let(:template) { FactoryBot.create(:binder_template, partner_configuration_id: config.id) }
    let(:config_two) { partner.partner_configuration}
    let(:template_two) { FactoryBot.create(:binder_template, partner_configuration_id: config_two.id) }
    let(:mail) {Mail::Message.new}

    before :each do
        user.add_role :partner_admin, partner
        config = partner.partner_configuration
        config.default_user_branding_id = user.user_profile.id
        config.save
        allow(Partner::Automation::Error).to receive(:build).and_call_original
        @parser = Partner::Automation::EmailParser.new
        allow(Partner::Automation::EmailParser).to receive(:new).and_return(@parser)
        allow(@parser).to receive(:parse).and_call_original
    end

    describe "#run" do
        context 'when there are no emails' do
            it "It does nothing" do
                emails = []
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => emails}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)
                job.perform
                expect(@parser).to_not receive(:parse)
            end
        end
        context 'when there are emails' do
            it "It does nothing" do
                mail = Mail::Message.new
                emails = [mail]
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => emails}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)
                job.perform
                expect(@parser).to_not receive(:parse)
            end
            it "It raises an error" do
                allow(Host).to receive(:path).and_return(Host::PRODUCTION_HOST)
                allow(Partner::Automation::EmailParser).to receive(:new).and_raise(BadRequestException)
                subject = "New Binder Request"
                from = ["test@partner.com"]
                mail = double("Mail::Message", :move_to => true, :subject => subject, :from => from, :body => OpenStruct.new({:decoded => ""}), :multipart? => false, :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}), :attachments => [])
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                allow(mail).to receive(:move_to)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)

                job.perform
                expect(ErrorService).to have_received(:perform_async)
            end
            it "It raises an Invalid API Key error" do
                subject = "New Test Binder Request"
                from = ["test@partner.com"]
                mail = double("Mail::Message", :move_to => true, :subject => subject, :from => from, :body => OpenStruct.new({:decoded => ""}), :multipart? => false, :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}), :attachments => [])
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                allow(mail).to receive(:move_to)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)
                error = BadRequestException.new "Invalid Partner Key"
                job.perform
                expect(mail).to have_received(:move_to)
                expect(@parser).to have_received(:parse).with(mail)
                expect(mail).to have_received(:move_to)
                #expect(Slack::BinderAutomation).to have_received(:new_binder_email_request).with(instance_of(Slack::Web::Client), "test@partner.com")
                #expect(Slack::BinderAutomation).to have_received(:new_binder_request_error).with(instance_of(Slack::Web::Client), instance_of(BadRequestException))
                expect(Partner::Automation::Error).to have_received(:build)
                expect(Partner::Automation::Error.last.error_message).to eq("Invalid Partner Key")
            end
            it "It raises an Invalid API Key error and sends an email with incorrect api key" do
                subject = "New Test Binder Request"
                from = ["test@partner.com"]
                body = "API Key: test\r\n"
                mail = double("Mail::Message", :move_to => true, :subject => subject, :from => from, :body => OpenStruct.new({:decoded => body}), :multipart? => false, :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => body})}), :attachments => [])
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)
                error = BadRequestException.new("Invalid API Key")
                allow(mail).to receive(:move_to)
                job.perform
                expect(mail).to have_received(:move_to)
                expect(@parser).to have_received(:parse).with(mail)
                #expect(Slack::BinderAutomation).to have_received(:new_binder_email_request).with(instance_of(Slack::Web::Client), "test@partner.com")
                #expect(Slack::BinderAutomation).to have_received(:new_binder_request_error).with(instance_of(Slack::Web::Client), instance_of(BadRequestException))
                expect(Partner::Automation::Error).to have_received(:build)
                expect(Partner::Automation::Error.last.error_message).to eq("Invalid Partner Key")
            end
            it "It raises an Invalid Partner Key error and sends an email with incorrect Partner key" do
                subject = "New Test Binder Request"
                from = ["test@partner.com"]
                body = "API Key: #{partner_key.key}\r\n"
                mail = double("Mail::Message", :move_to => true, :subject => subject, :from => from, :body => OpenStruct.new({:decoded => body}), :multipart? => false, :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => body})}), :attachments => [])
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)
                error = BadRequestException.new("Invalid Partner Key")
                allow(mail).to receive(:move_to)
                job.perform
                expect(mail).to have_received(:move_to)
                expect(@parser).to have_received(:parse).with(mail)
                #expect(Slack::BinderAutomation).to have_received(:new_binder_email_request).with(instance_of(Slack::Web::Client), "test@partner.com")
                #expect(Slack::BinderAutomation).to have_received(:new_binder_request_error).with(instance_of(Slack::Web::Client), instance_of(BadRequestException))
                expect(Partner::Automation::Error).to have_received(:build)
                expect(Partner::Automation::Error.last.error_message).to eq("Invalid Partner Key")
            end
            it "It raises a Client information not included error and sends an email" do
                subject = "New Test Binder Request"
                from = ["test@partner.com"]
                body = "API Key: #{partner_key.key}\r\nPartner Name: axium\r\n"
                mail = double("Mail::Message", :move_to => true, :subject => subject, :from => from, :body => OpenStruct.new({:decoded => body}), :multipart? => false, :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => body})}), :attachments => [])
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)
                error = BadRequestException.new("Client information not included")
                allow(mail).to receive(:move_to)
                job.perform
                expect(mail).to have_received(:move_to)
                expect(@parser).to have_received(:parse).with(mail)
                #expect(Slack::BinderAutomation).to have_received(:new_binder_email_request).with(instance_of(Slack::Web::Client), "test@partner.com")
                #expect(Slack::BinderAutomation).to have_received(:new_binder_request_error).with(instance_of(Slack::Web::Client), instance_of(BadRequestException))
                expect(Partner::Automation::Error).to have_received(:build)
                expect(Partner::Automation::Error.last.error_message).to eq("Client information not included")
            end
            it "It raises a Client information not included error and sends an email" do
                subject = "New Test Binder Request"
                from = ["test@partner.com"]
                body = "API Key: #{partner_key.key}\r\nPartner Name: axium\r\nClient First: \r\n"
                mail = double("Mail::Message", :move_to => true, :subject => subject, :from => from, :body => OpenStruct.new({:decoded => body}), :multipart? => false, :attachments => [], :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}))
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)
                error = BadRequestException.new "Property information not included"
                allow(mail).to receive(:move_to)
                job.perform
                expect(mail).to have_received(:move_to)
                expect(@parser).to have_received(:parse).with(mail)
                #expect(Slack::BinderAutomation).to have_received(:new_binder_email_request).with(instance_of(Slack::Web::Client), "test@partner.com")
                #expect(Slack::BinderAutomation).to have_received(:new_binder_request_error).with(instance_of(Slack::Web::Client), instance_of(BadRequestException))
                expect(Partner::Automation::Error).to have_received(:build)
                expect(Partner::Automation::Error.last.error_message).to eq("Property information not included")
            end
            it "It raises a Client First Name required error and sends an email" do
                subject = "New Test Binder Request"
                from = ["test@partner.com"]
                body = "API Key: #{partner_key.key}\r\nPartner Name: axium\r\nClient First: \r\nProperty Address: \r\n"
                mail = double("Mail::Message", :move_to => true, :subject => subject, :from => from, :body => OpenStruct.new({:decoded => body}), :multipart? => false, :attachments => [], :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}))
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)
                error = BadRequestException.new "Client First Name required"
                allow(mail).to receive(:move_to)
                job.perform
                expect(mail).to have_received(:move_to)
                expect(@parser).to have_received(:parse).with(mail)
                #expect(Slack::BinderAutomation).to have_received(:new_binder_email_request).with(instance_of(Slack::Web::Client), "test@partner.com")
                #expect(Slack::BinderAutomation).to have_received(:new_binder_request_error).with(instance_of(Slack::Web::Client), instance_of(BadRequestException))
                expect(Partner::Automation::Error).to have_received(:build)
                expect(Partner::Automation::Error.last.error_message).to eq("Client First Name required")
            end
            it "It raises a Property Address required error and sends an email" do
                subject = "New Test Binder Request"
                from = ["test@partner.com"]
                body = "API Key: #{partner_key.key}\r\nPartner Name: axium\r\nClient First: Jack\r\nClient Last: Frost\r\nClient Email: jackfrost@gmail.com\r\nProperty Address: \r\n"
                mail = double("Mail::Message", :move_to => true, :subject => subject, :from => from, :body => OpenStruct.new({:decoded => body}), :multipart? => false, :attachments => [], :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}))
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)
                error = BadRequestException.new "Property Address required"
                allow(mail).to receive(:move_to)
                job.perform
                expect(mail).to have_received(:move_to)
                expect(@parser).to have_received(:parse).with(mail)
                #expect(Slack::BinderAutomation).to have_received(:new_binder_email_request).with(instance_of(Slack::Web::Client), "test@partner.com")
                #expect(Slack::BinderAutomation).to have_received(:new_binder_request_error).with(instance_of(Slack::Web::Client), instance_of(BadRequestException))
                expect(Partner::Automation::Error).to have_received(:build)
                expect(Partner::Automation::Error.last.error_message).to eq("Property Address required")
            end
            it "It raises a Property City required error and sends an email" do
                subject = "New Test Binder Request"
                from = ["test@partner.com"]
                body = "API Key: #{partner_key.key}\r\nPartner Name: axium\r\nClient First: Jack\r\nClient Last: Frost\r\nClient Email: jackfrost@gmail.com\r\nProperty Address: 321 Summer Street\r\nProperty Address2: \r\n"
                mail = double("Mail::Message", :move_to => true, :subject => subject, :from => from, :body => OpenStruct.new({:decoded => body}), :multipart? => false, :attachments => [], :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}))
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)

                data = {:api_key=>"#{partner_key.key}", :key=>"axium", :client=>{:first=>"Jack", :last=>"Frost", :email => "jackfrost@gmail.com"}, :property=>{:address=>"321 Summer Street", :address2=> ""}, :documents=>[]}
                error = BadRequestException.new "Property City required"
                allow(mail).to receive(:move_to)
                job.perform
                expect(mail).to have_received(:move_to)
                expect(@parser).to have_received(:parse).with(mail)
                #expect(Slack::BinderAutomation).to have_received(:new_binder_email_request).with(instance_of(Slack::Web::Client), "test@partner.com")
                #expect(Slack::BinderAutomation).to have_received(:new_binder_request_error).with(instance_of(Slack::Web::Client), instance_of(BadRequestException))
                expect(Partner::Automation::Error).to have_received(:build)
                expect(Partner::Automation::Error.last.error_message).to eq("Property City required")
            end
            it "It raises a Property State required error and sends an email" do
                subject = "New Test Binder Request"
                from = ["test@partner.com"]
                body = "API Key: #{partner_key.key}\r\nPartner Name: axium\r\nClient First: Jack\r\nClient Last: Frost\r\nClient Email: jackfrost@gmail.com\r\nProperty Address: 321 Summer Street\r\nProperty Address2: \r\nProperty City: Boston\r\n"
                mail = double("Mail::Message", :move_to => true, :subject => subject, :from => from, :body => OpenStruct.new({:decoded => body}), :multipart? => false, :attachments => [], :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}))
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)

                data = {:api_key=>"#{partner_key.key}", :key=>"axium", :client=>{:first=>"Jack", :last=>"Frost", :email => "jackfrost@gmail.com"}, :property=>{:address=>"321 Summer Street", :address2=> "", :city => "Boston"}, :documents=>[]}
                error = BadRequestException.new "Property State required"
                allow(mail).to receive(:move_to)
                job.perform
                expect(mail).to have_received(:move_to)
                expect(@parser).to have_received(:parse).with(mail)
                #expect(Slack::BinderAutomation).to have_received(:new_binder_email_request).with(instance_of(Slack::Web::Client), "test@partner.com")
                #expect(Slack::BinderAutomation).to have_received(:new_binder_request_error).with(instance_of(Slack::Web::Client), instance_of(BadRequestException))
                expect(Partner::Automation::Error).to have_received(:build)
                expect(Partner::Automation::Error.last.error_message).to eq("Property State required")
            end
            it "It raises a Property Country required error and sends an email" do
                subject = "New Test Binder Request"
                from = ["test@partner.com"]
                body = "API Key: #{partner_key.key}\r\nPartner Name: axium\r\nClient First: Jack\r\nClient Last: Frost\r\nClient Email: jackfrost@gmail.com\r\nProperty Address: 321 Summer Street\r\nProperty Address2: \r\nProperty City: Boston\r\nProperty State: MA\r\nProperty PostalCode: 02210\r\n"
                mail = double("Mail::Message", :move_to => true, :subject => subject, :from => from, :body => OpenStruct.new({:decoded => body}), :multipart? => false, :attachments => [], :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}))
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)

                data = {:api_key=>"#{partner_key.key}", :key=>"axium", :client=>{:first=>"Jack", :last=>"Frost", :email => "jackfrost@gmail.com"}, :property=>{:address=>"321 Summer Street", :address2=> "", :city => "Boston", :state => "MA", :postalcode => "02210"}, :documents=>[]}
                error = BadRequestException.new "Property Country required"
                allow(mail).to receive(:move_to)
                job.perform
                expect(mail).to have_received(:move_to)
                expect(@parser).to have_received(:parse).with(mail)
                #expect(Slack::BinderAutomation).to have_received(:new_binder_email_request).with(instance_of(Slack::Web::Client), "test@partner.com")
                #expect(Slack::BinderAutomation).to have_received(:new_binder_request_error).with(instance_of(Slack::Web::Client), instance_of(BadRequestException))
                expect(Partner::Automation::Error).to have_received(:build)
                expect(Partner::Automation::Error.last.error_message).to eq("Property Country required")
            end
            it "It raises a BinderTemplateId required error and sends an email" do
                subject = "New Test Binder Request"
                from = ["test@partner.com"]
                body = "API Key: #{partner_key.key}\r\nPartner Name: axium\r\nClient First: Jack\r\nClient Last: Frost\r\nClient Email: jackfrost@gmail.com\r\nProperty Address: 321 Summer Street\r\nProperty Address2: \r\nProperty City: Boston\r\nProperty State: MA\r\nProperty PostalCode: 02210\r\nProperty Country: US\r\nBinderTemplateId: \r\n"
                mail = double("Mail::Message", :move_to => true, :subject => subject, :from => from, :body => OpenStruct.new({:decoded => body}), :multipart? => false, :attachments => [], :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}))
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)

                data = {:api_key=>"#{partner_key.key}", :key=>"axium", :client=>{:first=>"Jack", :last=>"Frost", :email => "jackfrost@gmail.com"}, :property=>{:address=>"321 Summer Street", :address2=> "", :city => "Boston", :state => "MA", :postalcode => "02210", :country => "US"}, :binder_template_id => "", :documents=>[]}
                error = BadRequestException.new "Binder Template ID required"
                allow(mail).to receive(:move_to)
                job.perform
                expect(mail).to have_received(:move_to)
                expect(@parser).to have_received(:parse).with(mail)
                #expect(Slack::BinderAutomation).to have_received(:new_binder_email_request).with(instance_of(Slack::Web::Client), "test@partner.com")
                #expect(Slack::BinderAutomation).to have_received(:new_binder_request_error).with(instance_of(Slack::Web::Client), instance_of(BadRequestException))
                expect(Partner::Automation::Error).to have_received(:build)
                expect(Partner::Automation::Error.last.error_message).to eq("Binder Template ID required")
            end
            it "It creates a binder without agent, inspection report and property photo" do
                body = "API Key: #{partner_key.key}\r\nPartner Name: axium\r\nClient First: Jack\r\nClient Last: Frost\r\nClient Email: jackfrost@gmail.com\r\nProperty Address: 321 Summer Street\r\nProperty Address2: \r\nProperty City: Boston\r\nProperty State: MA\r\nProperty PostalCode: 02210\r\nProperty Country: US\r\nBinderTemplateId: #{template_two.id}\r\nAutomatic Transfer: true\r\n"
                mail = double("Mail::Message", :move_to => true, :subject => "New Test Binder Request", :from => "test@partner.com", :body => OpenStruct.new({:decoded => body}), :multipart? => false, :attachments => [], :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}))
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)

                parameters = {:api_key=>"b3e6f2e29d67494882a48cc1f26579fa", :key=>"axium", :client=>{:first=>"Jack", :last=>"Frost", :email=>"jackfrost@gmail.com"}, :property=>{:address=>"321 Summer Street", :address2=>"", :city=>"Boston", :state=>"MA", :postalcode=>"02210", :country=>"US"}, :binder_template_id=> template_two.id, :documents=>[], :method=>"email"}

                allow(JSON).to receive(:parse).and_call_original
                allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

                allow(mail).to receive(:move_to)
                job.perform
                expect(mail).to have_received(:move_to)
                expect(@parser).to have_received(:parse).with(mail)
                expect(Partner::Automation::Error).to_not have_received(:build)
                binder = Binder.first
                expect(binder.name).to eq("Jack Frost Home")
                expect(binder.property.address1).to eq("321 Summer Street")
                expect(binder.property.address2).to eq("")
                expect(binder.property.city).to eq("Boston")
                expect(binder.property.state).to eq("MA")
                expect(binder.property.zip).to eq("02210")
                expect(binder.property.country).to eq("US")
                owner = binder.owner
                expect(owner.email).to eq(user.email)
                expect(owner.binders.count).to eq(1)
            end
            it "It creates a binder without agent, inspection report and property photo" do
                body = "API Key: #{partner_key.key}\r\nPartner Name: axium\r\nClient First: Jack\r\nClient Last: Frost\r\nClient Email: jackfrost@gmail.com\r\nClient Phone: 1-340-230-2312\r\nProperty Address: 321 Summer Street\r\nProperty Address2: \r\nProperty City: Boston\r\nProperty State: MA\r\nProperty PostalCode: 02210\r\nProperty Country: US\r\nBinderTemplateId: #{template_two.id}\r\nAutomatic Transfer: true\r\n"
                mail = double("Mail::Message", :move_to => true, :subject => "New Test Binder Request", :from => "test@partner.com", :body => OpenStruct.new({:decoded => body}), :multipart? => false, :attachments => [], :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}))
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)

                parameters = {:api_key=>"b3e6f2e29d67494882a48cc1f26579fa", :key=>"axium", :client=>{:first=>"Jack", :last=>"Frost", :email=>"jackfrost@gmail.com", :phone=>"1-340-230-2312"}, :property=>{:address=>"321 Summer Street", :address2=>"", :city=>"Boston", :state=>"MA", :postalcode=>"02210", :country=>"US"}, :binder_template_id=> template_two.id, :documents=>[], :method=>"email"}
                allow(JSON).to receive(:parse).and_call_original
                allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

                allow(mail).to receive(:move_to)
                job.perform
                # make sure correct methods were called
                expect(mail).to have_received(:move_to)
                expect(@parser).to have_received(:parse).with(mail)
                expect(Partner::Automation::Error).to_not have_received(:build)
                # check binder attributes
                binder = Binder.first
                expect(binder.name).to eq("Jack Frost Home")
                expect(binder.property.address1).to eq("321 Summer Street")
                expect(binder.property.address2).to eq("")
                expect(binder.property.city).to eq("Boston")
                expect(binder.property.state).to eq("MA")
                expect(binder.property.zip).to eq("02210")
                expect(binder.property.country).to eq("US")
                # make sure partner is still the owner
                owner = binder.owner
                expect(owner.email).to eq(user.email)
                expect(owner.binders.count).to eq(1)
                #expect(Slack::BinderAutomation).to have_received(:new_binder_email_request).with(instance_of(Slack::Web::Client), "t")
                # get the homeowner just created
                homeowner = User.where(:role => "homeowner").last
                expect(homeowner.user_profile.first_name).to eq("Jack")
                expect(homeowner.user_profile.last_name).to eq("Frost")
                expect(homeowner.email).to eq("jackfrost@gmail.com")
                expect(homeowner.user_profile.mobile_phone).to eq("+13402302312")
            end
            it "It creates a binder with inspection report" do
                body = "API Key: #{partner_key.key}\r\nPartner Name: axium\r\nClient First: Jack\r\nClient Last: Frost\r\nClient Email: jackfrost@gmail.com\r\nProperty Address: 321 Summer Street\r\nProperty Address2: \r\nProperty City: Boston\r\nProperty State: MA\r\nProperty PostalCode: 02210\r\nProperty Country: US\r\nBinderTemplateId: #{template_two.id}\r\nAutomatic Transfer: true\r\n"

                mail = double("Mail::Message", :move_to => true, :subject => "New Test Binder Request", :from => "test@partner.com", :body => OpenStruct.new({:decoded => body}), :multipart? => false, :attachments => [], :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}))

                mail.attachments.push(OpenStruct.new({:filename => "SampleDoc.pdf", :content_type => 'application/pdf', :decoded => "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read)}))
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)

                parameters = {:api_key=>"b3e6f2e29d67494882a48cc1f26579fa", :key=>"axium", :client=>{:first=>"Jack", :last=>"Frost", :email=>"jackfrost@gmail.com", :phone=>"1-340-230-2312"}, :property=>{:address=>"321 Summer Street", :address2=>"", :city=>"Boston", :state=>"MA", :postalcode=>"02210", :country=>"US"}, :binder_template_id=> template_two.id, :documents=>[{file: mail.attachments.first.decoded, name: "SampleDoc.pdf"}], :method=>"email"}
                allow(JSON).to receive(:parse).and_call_original
                allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

                allow(mail).to receive(:move_to)
                job.perform

                # check binder attributes
                binder = Binder.first
                expect(binder.documents.length).to eq(1)
            end
            it "It creates a binder with property photo" do
                body = "API Key: #{partner_key.key}\r\nPartner Name: axium\r\nClient First: Jack\r\nClient Last: Frost\r\nClient Email: jackfrost@gmail.com\r\nProperty Address: 321 Summer Street\r\nProperty Address2: \r\nProperty City: Boston\r\nProperty State: MA\r\nProperty PostalCode: 02210\r\nProperty Country: US\r\nBinderTemplateId: #{template_two.id}\r\nAutomatic Transfer: true\r\n"

                mail = double("Mail::Message", :move_to => true, :subject => "New Test Binder Request", :from => "test@partner.com", :body => OpenStruct.new({:decoded => body}), :multipart? => false, :attachments => [], :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}))
                mail.attachments.push(OpenStruct.new({:content_type => 'image/png', :decoded => "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read)}))
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)

                image = {file: "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read), name: "SampleImage.png"}

                client =  {first: "Mike", last: "Breen", email: nil, phone: "712-593-3410"}
                property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
                agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}

                parameters = {
                    :key => "test",
                    :client => client,
                    :property => property,
                    :binder_template_id => template_two.id,
                    :agent => agent,
                    :property_photo => image,
                    :documents => []
                }

                allow(JSON).to receive(:parse).and_call_original
                allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

                allow(mail).to receive(:move_to)
                job.perform

                # check binder attributes
                binder = Binder.first
                expect(binder.images.length).to eq(1)
            end
            it "It creates a binder with property photo and document" do
                body = "API Key: #{partner_key.key}\r\nPartner Name: axium\r\nClient First: Jack\r\nClient Last: Frost\r\nClient Email: jackfrost@gmail.com\r\nProperty Address: 321 Summer Street\r\nProperty Address2: \r\nProperty City: Boston\r\nProperty State: MA\r\nProperty PostalCode: 02210\r\nProperty Country: US\r\nBinderTemplateId: #{template_two.id}\r\nAutomatic Transfer: true\r\n"

                mail = double("Mail::Message", :move_to => true, :subject => "New Test Binder Request", :from => "test@partner.com", :body => OpenStruct.new({:decoded => body}), :multipart? => false, :attachments => [], :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}))
                mail.attachments.push(OpenStruct.new({:filename => "SampleDoc.pdf", :content_type => 'application/pdf', :decoded => "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read)}))
                mail.attachments.push(OpenStruct.new({:content_type => 'image/png', :decoded => "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read)}))
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)

                image = {file: "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read), name: "SampleImage.png"}
                files = [{file: "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read), name: "SampleDoc.pdf"}]

                client =  {first: "Mike", last: "Breen", email: nil, phone: "712-593-3410"}
                property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
                agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}

                parameters = {
                    :key => "test",
                    :client => client,
                    :property => property,
                    :binder_template_id => template_two.id,
                    :agent => agent,
                    :property_photo => image,
                    :documents => files
                }

                allow(JSON).to receive(:parse).and_call_original
                allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

                allow(mail).to receive(:move_to)
                job.perform

                # check binder attributes
                binder = Binder.first
                expect(binder.create_method).to eq("email")
                expect(binder.documents.length).to eq(1)
                expect(binder.images.length).to eq(1)
            end
            it "It creates a binder with updated fields" do
                body = "API Key: #{partner_key.key}\r\nPartner Name: axium\r\nClient First: Jack\r\nClient Last: Frost\r\nClient Email: jackfrost@gmail.com\r\nProperty Address: 321 Summer Street\r\nProperty Address2: \r\nProperty City: Boston\r\nProperty State: MA\r\nProperty PostalCode: 02210\r\nProperty Country: US\r\nBinderTemplateId: #{template_two.id}\r\nBuyerAgent Name: Keller Williams\r\nBuyerAgent First: Mike\r\nBuyerAgent Last: Davis\r\nBuyerAgent Phone: 1-234-503-2312\r\nBuyerAgent Email: mikedavis@gmail.com\r\nSellerAgent Name: ERA\r\nSellerAgent First: Steve\r\nSellerAgent Last: Jackson\r\nSellerAgent Phone: 1-495-340-2919\nSellerAgent Email: steve@gmail.com\nSellerAgent HeadShot URL: https://dummyimage.com/300/09f/fff.png\nSellerAgent Logo URL: https://dummyimage.com/300/09f/fff.png"

                mail = double("Mail::Message", :move_to => true, :subject => "New Test Binder Request", :from => "test@partner.com", :body => OpenStruct.new({:decoded => body}), :multipart? => false, :attachments => [], :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}))
                mail.attachments.push(OpenStruct.new({:filename => "SampleDoc.pdf", :content_type => 'application/pdf', :decoded => "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read)}))
                mail.attachments.push(OpenStruct.new({:content_type => 'image/png', :decoded => "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read)}))
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)

                image = {file: "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read), name: "SampleImage.png"}
                files = [{file: "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read), name: "SampleDoc.pdf"}]

                client =  {first: "Mike", last: "Breen", email: nil, phone: "712-593-3410"}
                property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
                agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}

                parameters = {
                    :key => "test",
                    :client => client,
                    :property => property,
                    :binder_template_id => template_two.id,
                    :buyer_agent => {:name=>"Keller Williams", :first=>"Mike", :last=>"Davis", :phone=>"1-234-503-2312", :email=>"mikedavis@gmail.com"},
                    :seller_agent => {:name=>"ERA", :first=>"Steve", :last=>"Jackson", :phone=>"1-495-340-2919", :email=>"steve@gmail.com", :headshot=>{:url=>"https://dummyimage.com/300/09f/fff.png"}, :logo=>{:url=>"https://dummyimage.com/300/09f/fff.png"}},
                    :property_photo => image,
                    :documents => files
                }

                allow(JSON).to receive(:parse).and_call_original
                allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

                allow(mail).to receive(:move_to)
                job.perform

                # check binder attributes
                binder = Binder.first
                expect(binder.create_method).to eq("email")
                expect(binder.documents.length).to eq(1)
                expect(binder.images.length).to eq(1)
                # check that the agents were added
                mike = User.find_by_email("mikedavis@gmail.com")
                steve = User.find_by_email("steve@gmail.com")
                expect(mike).to_not be(nil)
                expect(steve).to_not be(nil)
            end
            it "It creates a binder with updated fields - transaction related" do
                body = "Inspection Date: 07-29-2017}\nProject Number: 2NASX012\nTransaction Type: BUY\nAPI Key: #{partner_key.key}\r\nPartner Name: axium\r\nClient First: Jack\r\nClient Last: Frost\r\nClient Email: jackfrost@gmail.com\r\nProperty Address: 321 Summer Street\r\nProperty Address2: \r\nProperty City: Boston\r\nProperty State: MA\r\nProperty PostalCode: 02210\r\nProperty Country: US\r\nBinderTemplateId: #{template_two.id}\r\nBuyerAgent Name: Keller Williams\r\nBuyerAgent First: Mike\r\nBuyerAgent Last: Davis\r\nBuyerAgent Phone: 1-234-503-2312\r\nBuyerAgent Email: mikedavis@gmail.com\r\nSellerAgent Name: ERA\r\nSellerAgent First: Steve\r\nSellerAgent Last: Jackson\r\nSellerAgent Phone: 1-495-340-2919\nSellerAgent Email: steve@gmail.com\nSellerAgent HeadShot URL: https://dummyimage.com/300/09f/fff.png\nSellerAgent Logo URL: https://dummyimage.com/300/09f/fff.png"

                mail = double("Mail::Message", :move_to => true, :subject => "New Test Binder Request", :from => "test@partner.com", :body => OpenStruct.new({:decoded => body}), :multipart? => false, :attachments => [], :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}))
                mail.attachments.push(OpenStruct.new({:filename => "SampleDoc.pdf", :content_type => 'application/pdf', :decoded => "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read)}))
                mail.attachments.push(OpenStruct.new({:content_type => 'image/png', :decoded => "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read)}))
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)

                image = {file: "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read), name: "SampleImage.png"}
                files = [{file: "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read), name: "SampleDoc.pdf"}]

                client =  {first: "Mike", last: "Breen", email: nil, phone: "712-593-3410"}
                property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
                agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}

                parameters = {
                    :key => "test",
                    :client => client,
                    :property => property,
                    :binder_template_id => template_two.id,
                    :transaction_type => "BUY",
                    :buyer_agent => {:name=>"Keller Williams", :first=>"Mike", :last=>"Davis", :phone=>"1-234-503-2312", :email=>"mikedavis@gmail.com"},
                    :seller_agent => {:name=>"ERA", :first=>"Steve", :last=>"Jackson", :phone=>"1-495-340-2919", :email=>"steve@gmail.com", :headshot=>{:url=>"https://dummyimage.com/300/09f/fff.png"}, :logo=>{:url=>"https://dummyimage.com/300/09f/fff.png"}},
                    :property_photo => image,
                    :documents => files
                }

                allow(JSON).to receive(:parse).and_call_original
                allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

                allow(mail).to receive(:move_to)
                job.perform

                # check binder attributes
                binder = Binder.first
                expect(binder.create_method).to eq("email")
                expect(binder.documents.length).to eq(1)
                expect(binder.images.length).to eq(1)
                # check that the agents were added
                mike = User.find_by_email("mikedavis@gmail.com")
                steve = User.find_by_email("steve@gmail.com")
                expect(mike).to_not be(nil)
                expect(steve).to_not be(nil)
                expect(binder.transactions.length).to eq(1)
            end
            it "It creates a binder with updated fields - transaction related" do
                body = "Inspection Date: 07/29/2017\nProject Number: 2NASX012\nTransaction Type: BUY\nAPI Key: #{partner_key.key}\r\nPartner Name: axium\r\nClient First: Jack\r\nClient Last: Frost\r\nClient Email: jackfrost@gmail.com\r\nProperty Address: 321 Summer Street\r\nProperty Address2: \r\nProperty City: Boston\r\nProperty State: MA\r\nProperty PostalCode: 02210\r\nProperty Country: US\r\nBinderTemplateId: #{template_two.id}\r\nBuyerAgent Name: Keller Williams\r\nBuyerAgent First: Mike\r\nBuyerAgent Last: Davis\r\nBuyerAgent Phone: 1-234-503-2312\r\nBuyerAgent Email: mikedavis@gmail.com\r\nSellerAgent Name: ERA\r\nSellerAgent First: Steve\r\nSellerAgent Last: Jackson\r\nSellerAgent Phone: 1-495-340-2919\nSellerAgent Email: steve@gmail.com\nSellerAgent HeadShot URL: https://dummyimage.com/300/09f/fff.png\nSellerAgent Logo URL: https://dummyimage.com/300/09f/fff.png"

                mail = double("Mail::Message", :move_to => true, :subject => "New Test Binder Request", :from => "test@partner.com", :body => OpenStruct.new({:decoded => body}), :multipart? => false, :attachments => [], :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}))
                mail.attachments.push(OpenStruct.new({:filename => "SampleDoc.pdf", :content_type => 'application/pdf', :decoded => "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read)}))
                mail.attachments.push(OpenStruct.new({:content_type => 'image/png', :decoded => "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read)}))
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)

                image = {file: "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read), name: "SampleImage.png"}
                files = [{file: "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read), name: "SampleDoc.pdf"}]

                client =  {first: "Mike", last: "Breen", email: nil, phone: "712-593-3410"}
                property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
                agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}

                parameters = {
                    :key => "test",
                    :client => client,
                    :property => property,
                    :binder_template_id => template_two.id,
                    :transaction_type => "BUY",
                    :buyer_agent => {:name=>"Keller Williams", :first=>"Mike", :last=>"Davis", :phone=>"1-234-503-2312", :email=>"mikedavis@gmail.com"},
                    :seller_agent => {:name=>"ERA", :first=>"Steve", :last=>"Jackson", :phone=>"1-495-340-2919", :email=>"steve@gmail.com", :headshot=>{:url=>"https://dummyimage.com/300/09f/fff.png"}, :logo=>{:url=>"https://dummyimage.com/300/09f/fff.png"}},
                    :property_photo => image,
                    :documents => files
                }

                allow(JSON).to receive(:parse).and_call_original
                allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

                allow(mail).to receive(:move_to)
                job.perform

                # check binder attributes
                binder = Binder.first
                expect(binder.create_method).to eq("email")
                expect(binder.documents.length).to eq(1)
                expect(binder.images.length).to eq(1)
                # check that the agents were added
                mike = User.find_by_email("mikedavis@gmail.com")
                steve = User.find_by_email("steve@gmail.com")
                expect(mike).to_not be(nil)
                expect(steve).to_not be(nil)
                expect(binder.transactions.length).to eq(1)
            end
            it "It creates a binder with the correct client email tag and creates document with ReportURL" do
                document_type = create(:document_type, :name => "Home Inspection Report")

                allow(Partner::Automation::Service).to receive(:open)

                page = double("Page", :css => [{"href" => "downloadme!"}])
                allow(URI).to receive(:parse).and_call_original
                allow(URI).to receive(:parse).with("test.com").and_return(File.open('spec/assets/SampleDoc.pdf'))
                allow(Nokogiri::HTML::Document).to receive(:parse).and_return(page)

                body = "API Key: #{partner_key.key}\r\nPartner Name: axium\r\nClient First: Jack\r\nClient Last: Frost\r\nClient Email: jackfrost@gmail.com;monkey@test.com\r\nClient Phone: 1-340-230-2312\r\nProperty Address: 321 Summer Street\r\nProperty Address2: \r\nProperty City: Boston\r\nProperty State: MA\r\nProperty PostalCode: 02210\r\nProperty Country: US\r\nBinderTemplateId: #{template_two.id}\r\nAutomatic Transfer: true\r\nReportURL: https://www.nachi.org/documents2012/Checklist/CBHI%20Home%20Inspection%20Report.pdf"
                mail = double("Mail::Message", :move_to => true, :subject => "New Test Binder Request", :from => "test@partner.com", :body => OpenStruct.new({:decoded => body}), :multipart? => false, :attachments => [], :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}))
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)

                image = {file: "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read), name: "SampleImage.png"}
                files = [{file: "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read), name: "SampleDoc.pdf"}]

                client =  {first: "Mike", last: "Breen", email: nil, phone: "712-593-3410"}
                property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
                agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}

                parameters = {
                    :key => "test",
                    :client => client,
                    :property => property,
                    :binder_template_id => template_two.id,
                    :transaction_type => "BUY",
                    :buyer_agent => {:name=>"Keller Williams", :first=>"Mike", :last=>"Davis", :phone=>"1-234-503-2312", :email=>"mikedavis@gmail.com"},
                    :seller_agent => {:name=>"ERA", :first=>"Steve", :last=>"Jackson", :phone=>"1-495-340-2919", :email=>"steve@gmail.com", :headshot=>{:url=>"https://dummyimage.com/300/09f/fff.png"}, :logo=>{:url=>"https://dummyimage.com/300/09f/fff.png"}},
                    :property_photo => image,
                    :inspection_report_url => "test.com"
                }

                allow(JSON).to receive(:parse).and_call_original
                allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

                allow(mail).to receive(:move_to)
                job.perform
                # get the homeowner just created
                homeowner = User.where(:role => "homeowner").last
                expect(homeowner.user_profile.first_name).to eq("Jack")

                expect(homeowner.user_profile.last_name).to eq("Frost")
                expect(homeowner.email).to eq("jackfrost@gmail.com")
                expect(homeowner.user_profile.mobile_phone).to eq("+13402302312")

                # creates binder documents
                binder = Binder.last
                expect(binder.name).to eq("Jack Frost Home")
                expect(binder.documents.count).to eq(1)
                expect(binder.documents.first.document_type_id).to eq(document_type.id)
            end
            it "It creates a binder with the correct client email tag and creates document with Report URL" do
                document_type = create(:document_type, :name => "Home Inspection Report")

                allow(Partner::Automation::Service).to receive(:open)

                page = double("Page", :css => [{"href" => "downloadme!"}])
                allow(URI).to receive(:parse).and_call_original
                allow(URI).to receive(:parse).with("test.com").and_return(File.open('spec/assets/SampleDoc.pdf'))
                allow(Nokogiri::HTML::Document).to receive(:parse).and_return(page)

                body = "API Key: #{partner_key.key}\r\nPartner Name: axium\r\nClient First: Jack\r\nClient Last: Frost\r\nClient Email: jackfrost@gmail.com;monkey@test.com\r\nClient Phone: 1-340-230-2312\r\nProperty Address: 321 Summer Street\r\nProperty Address2: \r\nProperty City: Boston\r\nProperty State: MA\r\nProperty PostalCode: 02210\r\nProperty Country: US\r\nBinderTemplateId: #{template_two.id}\r\nAutomatic Transfer: true\r\nReport URL: https://www.nachi.org/documents2012/Checklist/CBHI%20Home%20Inspection%20Report.pdf"
                mail = double("Mail::Message", :move_to => true, :subject => "New Test Binder Request", :from => "test@partner.com", :body => OpenStruct.new({:decoded => body}), :multipart? => false, :attachments => [], :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}))
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)

                image = {file: "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read), name: "SampleImage.png"}
                files = [{file: "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read), name: "SampleDoc.pdf"}]

                client =  {first: "Mike", last: "Breen", email: nil, phone: "712-593-3410"}
                property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
                agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}

                parameters = {
                    :key => "test",
                    :client => client,
                    :property => property,
                    :binder_template_id => template_two.id,
                    :transaction_type => "BUY",
                    :buyer_agent => {:name=>"Keller Williams", :first=>"Mike", :last=>"Davis", :phone=>"1-234-503-2312", :email=>"mikedavis@gmail.com"},
                    :seller_agent => {:name=>"ERA", :first=>"Steve", :last=>"Jackson", :phone=>"1-495-340-2919", :email=>"steve@gmail.com", :headshot=>{:url=>"https://dummyimage.com/300/09f/fff.png"}, :logo=>{:url=>"https://dummyimage.com/300/09f/fff.png"}},
                    :property_photo => image,
                    :inspection_report_url => "test.com"
                }

                allow(JSON).to receive(:parse).and_call_original
                allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

                allow(mail).to receive(:move_to)
                job.perform
                # get the homeowner just created
                homeowner = User.where(:role => "homeowner").last
                expect(homeowner.user_profile.first_name).to eq("Jack")

                expect(homeowner.user_profile.last_name).to eq("Frost")
                expect(homeowner.email).to eq("jackfrost@gmail.com")
                expect(homeowner.user_profile.mobile_phone).to eq("+13402302312")

                # creates binder documents
                binder = Binder.last
                expect(binder.name).to eq("Jack Frost Home")
                #expect(binder.documents.count).to eq(1)
                expect(binder.documents.first.document_type_id).to eq(document_type.id)
            end
        end
    end

    describe "#re_run" do
        it "It adds a documents" do
            property = create(:property, :country => "US", :address1 => "321 Summer Street", :city => "Boston", :state => "MA", :zip => "02210")
            body = "API Key: #{partner_key.key}\r\nPartner Name: axium\r\nClient First: Jack\r\nClient Last: Frost\r\nClient Email: jackfrost@gmail.com\r\nProperty Address: 321 Summer Street\r\nProperty Address2: \r\nProperty City: Boston\r\nProperty State: MA\r\nProperty PostalCode: 02210\r\nProperty Country: US\r\nBinderTemplateId: #{template_two.id}\r\nAutomatic Transfer: true\r\n"

            mail = double("Mail::Message", :move_to => true, :subject => "New Test Binder Request", :from => "test@partner.com", :body => OpenStruct.new({:decoded => body}), :multipart? => false, :attachments => [], :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}))

            mail.attachments.push(OpenStruct.new({:filename => "SampleDoc.pdf", :content_type => 'application/pdf', :mime_type => 'application/pdf', :decoded => File.open('spec/assets/SampleDoc.pdf', 'rb').read}))
            @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
            expect(Gmail).to receive(:new).with(ENV["GOOGLE_BINDER_CREDENTIALS"], ENV["GOOGLE_BINDER_PASSWORD"]).and_return(@gmail)

            # check binder attributes
            binder = property.binder
            expect(binder.documents.length).to eq(0)

            job.re_run

            binder.reload
            expect(binder.documents.length).to eq(1)
        end
    end
end
