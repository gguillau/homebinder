require 'rails_helper'

RSpec.describe Email::ProcessReportsJob do
    let(:job) {Email::ProcessReportsJob.new}
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:partner) { FactoryBot.create(:partner) }
    let(:mail) {Mail::Message.new}

    before :each do
        allow(ErrorService).to receive(:perform_async)

        @service = WorkaroundService.new
        allow(WorkaroundService).to receive(:new).and_return(@service)
        allow(@service).to receive(:post_call)
    end

    describe "#run" do
        context 'when there are no emails' do
            it "It does nothing" do
                emails = []
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => emails}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_INSPECTION_REPORT_CREDENTIALS"], ENV["GOOGLE_INSPECTION_REPORT_PASSWORD"]).and_return(@gmail)
                job.perform
                expect(@service).to_not receive(:post_call)
            end
        end
        context 'when there are emails' do
            it "It does nothing" do
                mail = Mail::Message.new
                emails = [mail]
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => emails}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_INSPECTION_REPORT_CREDENTIALS"], ENV["GOOGLE_INSPECTION_REPORT_PASSWORD"]).and_return(@gmail)
                job.perform
                expect(@service).to_not receive(:post_call)
            end
            it "It raises an error" do
                allow(Host).to receive(:path).and_return(Host::PRODUCTION_HOST)
                allow(InspectionReportJob).to receive(:create).and_raise(BadRequestException)
                subject = "new inspection report request"
                from = ["#{partner.email}"]
                mail = double("Mail::Message", :move_to => true, :subject => subject, :from => from, :body => OpenStruct.new({:decoded => ""}), :multipart? => false, :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}), :attachments => [])
                mail.attachments.push(OpenStruct.new({:filename => "SampleDoc.pdf", :content_type => 'application/pdf', :decoded => File.open("spec/assets/SampleDoc.pdf", "rb").read}))
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                allow(mail).to receive(:move_to)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_INSPECTION_REPORT_CREDENTIALS"], ENV["GOOGLE_INSPECTION_REPORT_PASSWORD"]).and_return(@gmail)

                job.perform
                expect(ErrorService).to have_received(:perform_async)
            end
            it "It creates a job with inspection report" do
                mail = double("Mail::Message", :move_to => true, :subject => "New Test Inspection Report Request", :from => ["#{partner.email}"], :body => OpenStruct.new({:decoded => ""}), :multipart? => false, :attachments => [], :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => ""})}))

                mail.attachments.push(OpenStruct.new({:filename => "SampleDoc.pdf", :content_type => 'application/pdf', :decoded => File.open("spec/assets/SampleDoc.pdf", "rb").read}))
                @gmail = double("Gmail", :mailbox => OpenStruct.new({:emails => [mail]}), :logout => true)
                expect(Gmail).to receive(:new).with(ENV["GOOGLE_INSPECTION_REPORT_CREDENTIALS"], ENV["GOOGLE_INSPECTION_REPORT_PASSWORD"]).and_return(@gmail)

                allow(mail).to receive(:move_to)
                job.perform

                # check job attributes
                job = InspectionReportJob.first
                expect(job.partner_id).to eq(partner.id)
            end
        end
    end
end
