require 'rails_helper'

RSpec.describe EnumValidator, :type => :model do
  
  it "validates value is not in enum" do
    val = EnumValidator.new({
      cls: AnnualPropertyReview,
      attributes: [:status],
      value: "active"
    })
    record = build(:annual_property_review)
    val.validate(record)
    expect(record.errors.empty?).to eq(false)
  end
  
  it "raises an error" do
    val = EnumValidator.new({
      attributes: [:status],
      value: "active"
    })
    record = build(:annual_property_review)
    
    expect{val.validate(record)}.to raise_error ArgumentError
  end
  
end