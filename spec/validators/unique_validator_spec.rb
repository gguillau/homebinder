require 'rails_helper'

RSpec.describe UniqueValidator, :type => :model do
  
  it "validates new unique value" do
    record = build(:binder, name: "new")
    val = UniqueValidator.new({
      attributes: [:name]
    })
    val.validate(record)
    expect(record.errors.empty?).to eq(true)
  end
  
  it "validates existing non unique value" do
    create(:binder, name: "binder")
    record = build(:binder, name: "binder")
    val = UniqueValidator.new({
      attributes: [:name]
    })
    val.validate(record)
    expect(record.errors.include?(:name)).to eq(true)
  end
  
  it "validates existing unique value" do
    create(:binder, name: "binder")
    record = build(:binder, name: "binder2")
    val = UniqueValidator.new({
      attributes: [:name]
    })
    val.validate(record)
    expect(record.errors.empty?).to eq(true)
  end
  
  it "validates value on same record" do
    record = create(:binder, name: "same")
    val = UniqueValidator.new({
      attributes: [:name]
    })
    val.validate(record)
    expect(record.errors.empty?).to eq(true)
  end
end