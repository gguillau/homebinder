require 'rails_helper'

RSpec.describe AnnualPropertyReviews::AprPdfGenerator, :type => :service do
    
    describe "init" do
        it "sets the apr, pdf and pdf font" do
            apr = create(:annual_property_review)

            service = AnnualPropertyReviews::AprPdfGenerator.new(apr.id)
            service.init
            expect(service.apr).to eq(apr)
            expect(service.text).to eq("Annual Property Review \u2122 Report #{apr.created_at.year}")
            expect(service.pdf).to_not be(nil)
            expect(service.grey_color).to_not be("555555")
            expect(service.pdf.font_families).to_not be(nil)
        end
    end

    describe "generate" do
        it "calls add_title" do
            allow(AnnualPropertyReviewMailer).to receive(:delay).and_return(AnnualPropertyReviewMailer)
            allow(AnnualPropertyReviewMailer).to receive(:send_full_report)

            logo = Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleImage.png'), 'image/png')
            reviewer_company = create(:partner, :logo => logo)
            apr = create(:annual_property_review, :reviewer_company => reviewer_company)
            create(:annual_property_review_photo, :file => logo, :annual_property_review => apr)

            service = AnnualPropertyReviews::AprPdfGenerator.new(apr.id)
            
            service.init
            allow(service).to receive(:add_title)
            allow(service).to receive(:add_hero_photo)
            allow(service).to receive(:add_details)
            allow(service).to receive(:add_homeowner_comments)
            allow(service).to receive(:add_findings)
            allow(service).to receive(:add_capital_items)
            allow(service).to receive(:add_reviewer_comments)
            allow(service).to receive(:add_liability)
            allow(service).to receive(:add_footer)
            allow(service.pdf).to receive(:image)
            allow(service.pdf).to receive(:formatted_text_box)
            allow(service).to receive(:open).and_return("#{Rails.root}/spec/assets/flatlogonotext.png")
            
            service.generate

            expect(service).to have_received(:add_title)
        end
    end

    describe "add_title" do
        it "adds the title to the report" do
            apr = create(:annual_property_review)

            service = AnnualPropertyReviews::AprPdfGenerator.new(apr.id)

            service.init
            allow(service.pdf).to receive(:repeat).and_call_original
            service.add_title

            expect(service.pdf).to have_received(:repeat).with(:all, :dynamic => true)
        end
    end

    describe "add_details" do
        it "adds the apr details to the report" do
            apr = create(:annual_property_review)

            service = AnnualPropertyReviews::AprPdfGenerator.new(apr.id)

            service.init
            allow(service.pdf).to receive(:table).and_call_original
            service.add_details
            details = [[{:content => "APR Details", :colspan => 2}], ["Review Date", "No Review Date Set"], ["Prepared for", apr.client_first_name + " " + apr.client_last_name], ["Address", apr.address1 + (apr.address2 ? " " + apr.address2 : "") + ", " + apr.city + ", " + apr.state], ["Reviewer Company", apr.reviewer_company.name], ["Reviewer Phone", "(610)-600-4000"], ["Reviewer", "Not assigned"]]

            expect(service.pdf).to have_received(:table).with(details, :header => false, :width => 250, :cell_style => { :inline_format => true, :border_lines => [:solid]}, :position => :right)
        end
    end

    describe "add_homeowner_comments" do
        it "adds the apr homeowner comments to the report" do
            apr = create(:annual_property_review)

            service = AnnualPropertyReviews::AprPdfGenerator.new(apr.id)

            service.init
            allow(service.pdf).to receive(:table).and_call_original
            service.add_homeowner_comments
            details = [["Homeowner Comments"], ["No Homeowner Comments"]]

            expect(service.pdf).to have_received(:table).with(details, :header => false, :width => service.pdf.bounds.width, :cell_style => { :inline_format => true, :border_lines => [:solid]})
        end
    end

    describe "add_findings" do
        it "adds 0 apr findings to the report" do
            apr = create(:annual_property_review)

            service = AnnualPropertyReviews::AprPdfGenerator.new(apr.id)

            service.init
            allow(service.pdf).to receive(:table).and_call_original
            service.add_findings
            details = [[{:content => "Findings", :colspan => 6}], ["Photo", "Finding", "Priority", "Cost", "Details", "Home Pro"], [{:content => "No Findings Added", :colspan => 6}]]

            expect(service.pdf).to have_received(:table).with(details, :header => false, :column_widths => [90, 90, 90, 90, 90, 90], :cell_style => { :inline_format => true, :border_lines => [:solid]})
        end

        it "adds 1 apr findings to the report" do
            apr = create(:annual_property_review)
            finding = create(:annual_property_review_finding, :annual_property_review => apr)

            service = AnnualPropertyReviews::AprPdfGenerator.new(apr.id)
            
            service.init
            allow(service.pdf).to receive(:image)
            allow(service.pdf).to receive(:formatted_text_box)
            allow(service.pdf).to receive(:table)
            
            service.add_findings
            image = {:image => "#{Rails.root}/public/img/no-image-available.jpg",:fit => [50, 50]}
            details = [[{:content => "Findings", :colspan => 6}], ["Photo", "Finding", "Priority", "Cost", "Details", "Home Pro"], [image, finding.name, finding.priority.upcase, finding.cost, finding.details, "None"]]

            expect(service.pdf).to have_received(:table).with(details, :header => false, :column_widths => [90.0, 90.0, 90.0, 90.0, 90.0, 90.0], :cell_style => { :inline_format => true, :border_lines => [:solid]})
        end
    end

    describe "add_capital_items" do
        it "adds 0 apr capital_item to the report" do
            apr = create(:annual_property_review)

            service = AnnualPropertyReviews::AprPdfGenerator.new(apr.id)

            service.init
            allow(service.pdf).to receive(:table).and_call_original
            service.add_capital_items
            details = [[{:content => "Home Planning", :colspan => 4}], ["Name", "Remaining Useful Life", "Cost", "Comments"], [{:content => "No Home Planning Items Added", :colspan => 4}]]

            expect(service.pdf).to have_received(:table).with(details, :header => false, :column_widths => [144.0, 132.0, 132.0, 132.0], :cell_style => { :inline_format => true, :border_lines => [:solid]})
        end

        it "adds 1 apr capital_item to the report" do
            apr = create(:annual_property_review)
            item = create(:annual_property_review_capital_item, :annual_property_review => apr)

            service = AnnualPropertyReviews::AprPdfGenerator.new(apr.id)

            service.init
            allow(service.pdf).to receive(:table).and_call_original
            service.add_capital_items
            details = [[{:content => "Home Planning", :colspan => 4}], ["Name", "Remaining Useful Life", "Cost", "Comments"], [item.name, item.rul, item.cost, "No Comments Added"]]

            expect(service.pdf).to have_received(:table).with(details, :header => false, :column_widths => [144.0, 132.0, 132.0, 132.0], :cell_style => { :inline_format => true, :border_lines => [:solid]})
        end
    end

    describe "add_reviewer_comments" do
        it "adds reviewer comments to the report" do
            apr = create(:annual_property_review)

            service = AnnualPropertyReviews::AprPdfGenerator.new(apr.id)

            service.init
            allow(service.pdf).to receive(:table).and_call_original
            service.add_reviewer_comments
            details = [["Reviewer Comments"], ["No Reviewer Comments"]]

            expect(service.pdf).to have_received(:table).with(details, :header => false, :width => service.pdf.bounds.width, :cell_style => { :inline_format => true, :border_lines => [:solid]})
        end
    end

    describe "add_liability" do
        it "adds liability to the report" do
            apr = create(:annual_property_review)

            service = AnnualPropertyReviews::AprPdfGenerator.new(apr.id)

            service.init
            allow(service.pdf).to receive(:formatted_text_box)

            service.add_liability

            expect(service.pdf).to have_received(:formatted_text_box)
                                       .with(
                                           [
                                               {:text => "Limitation of Liability", :size => 12, :color => "767676"}
                                           ],
                                           {:at => [0, 612.0], :width => 400}
                                       )
        end
    end

    describe "add_footer" do
        it "adds footer" do
            apr = create(:annual_property_review)

            service = AnnualPropertyReviews::AprPdfGenerator.new(apr.id)

            service.init
            allow(service.pdf).to receive(:repeat)
            service.add_footer

            expect(service.pdf).to have_received(:repeat)
        end
    end

    describe "convert_bullet_points" do
        it "converts the bullet points" do
            apr = create(:annual_property_review, :property_reviewer_comments => "<ol><li></li></ol>")

            service = AnnualPropertyReviews::AprPdfGenerator.new(apr.id)

            expect(service.apr.property_reviewer_comments).to eq("\n\n•&nbsp;\n\n")
        end
    end
    
end
