require 'rails_helper'

RSpec.describe RepairPricer::Client, :type => :service do
    
    describe "create_report" do
        it "creates a report and returns status of 10" do
            repair_pricer_report = create(:repair_pricer_report)
            repair_pricer_request = RepairPricer::Request.new
            allow(RepairPricer::Request).to receive(:new).and_return(repair_pricer_request)
            allow(repair_pricer_request).to receive(:post_call).and_return({"id" => "103", "status" => "10"})
            params = {
                :id => repair_pricer_report,
                :fileurl => "https://www.nachi.org/documents2012/Checklist/CBHI%20Home%20Inspection%20Report.pdf",
                :fname => Faker::Name.first_name,
                :lname => Faker::Name.last_name,
                :email => Faker::Internet.email,
                :address1 => Faker::Address.street_address,
                :city => Faker::Address.city,
                :state => Faker::Address.state_abbr,
                :zip => Faker::Address.zip
            }
            
            RepairPricer::Client.new.create_report(params)
            
            repair_pricer_report.reload
            expect(repair_pricer_report.report_id).to eq("103")
            expect(repair_pricer_report.status).to eq("submitted")
        end
        
        it "creates a report and returns status of 20" do
            repair_pricer_report = create(:repair_pricer_report)
            repair_pricer_request = RepairPricer::Request.new
            allow(RepairPricer::Request).to receive(:new).and_return(repair_pricer_request)
            allow(repair_pricer_request).to receive(:post_call).and_return({"id" => "103", "status" => "20"})
            params = {
                :id => repair_pricer_report,
                :fileurl => "https://www.nachi.org/documents2012/Checklist/CBHI%20Home%20Inspection%20Report.pdf",
                :fname => Faker::Name.first_name,
                :lname => Faker::Name.last_name,
                :email => Faker::Internet.email,
                :address1 => Faker::Address.street_address,
                :city => Faker::Address.city,
                :state => Faker::Address.state_abbr,
                :zip => Faker::Address.zip
            }
            
            RepairPricer::Client.new.create_report(params)
            
            repair_pricer_report.reload
            expect(repair_pricer_report.report_id).to eq("103")
            expect(repair_pricer_report.status).to eq("accepted")
        end
        
        it "creates a report and returns status of 30" do
            repair_pricer_report = create(:repair_pricer_report)
            repair_pricer_request = RepairPricer::Request.new
            allow(RepairPricer::Request).to receive(:new).and_return(repair_pricer_request)
            allow(repair_pricer_request).to receive(:post_call).and_return({"id" => "103", "status" => "30"})
            params = {
                :id => repair_pricer_report,
                :fileurl => "https://www.nachi.org/documents2012/Checklist/CBHI%20Home%20Inspection%20Report.pdf",
                :fname => Faker::Name.first_name,
                :lname => Faker::Name.last_name,
                :email => Faker::Internet.email,
                :address1 => Faker::Address.street_address,
                :city => Faker::Address.city,
                :state => Faker::Address.state_abbr,
                :zip => Faker::Address.zip
            }
            
            RepairPricer::Client.new.create_report(params)
            
            repair_pricer_report.reload
            expect(repair_pricer_report.report_id).to eq("103")
            expect(repair_pricer_report.status).to eq("in_progress")
        end
        
        it "creates a report and returns status of 40" do
            repair_pricer_report = create(:repair_pricer_report)
            repair_pricer_request = RepairPricer::Request.new
            allow(RepairPricer::Request).to receive(:new).and_return(repair_pricer_request)
            allow(repair_pricer_request).to receive(:post_call).and_return({"id" => "103", "status" => "40"})
            params = {
                :id => repair_pricer_report,
                :fileurl => "https://www.nachi.org/documents2012/Checklist/CBHI%20Home%20Inspection%20Report.pdf",
                :fname => Faker::Name.first_name,
                :lname => Faker::Name.last_name,
                :email => Faker::Internet.email,
                :address1 => Faker::Address.street_address,
                :city => Faker::Address.city,
                :state => Faker::Address.state_abbr,
                :zip => Faker::Address.zip
            }
            
            RepairPricer::Client.new.create_report(params)
            
            repair_pricer_report.reload
            expect(repair_pricer_report.report_id).to eq("103")
            expect(repair_pricer_report.status).to eq("on_hold")
        end
        
        it "creates a report and returns status of 50" do
            repair_pricer_report = create(:repair_pricer_report)
            repair_pricer_request = RepairPricer::Request.new
            allow(RepairPricer::Request).to receive(:new).and_return(repair_pricer_request)
            allow(repair_pricer_request).to receive(:post_call).and_return({"id" => "103", "status" => "50"})
            params = {
                :id => repair_pricer_report,
                :fileurl => "https://www.nachi.org/documents2012/Checklist/CBHI%20Home%20Inspection%20Report.pdf",
                :fname => Faker::Name.first_name,
                :lname => Faker::Name.last_name,
                :email => Faker::Internet.email,
                :address1 => Faker::Address.street_address,
                :city => Faker::Address.city,
                :state => Faker::Address.state_abbr,
                :zip => Faker::Address.zip
            }
            
            RepairPricer::Client.new.create_report(params)
            
            repair_pricer_report.reload
            expect(repair_pricer_report.report_id).to eq("103")
            expect(repair_pricer_report.status).to eq("ready")
        end
    end
    
    describe "get_report" do
        it "gets the report status" do
            repair_pricer_request = RepairPricer::Request.new
            allow(RepairPricer::Request).to receive(:new).and_return(repair_pricer_request)
            allow(repair_pricer_request).to receive(:post_call).and_return({"id" => "103"})
                report = RepairPricer::Client.new.get_report(1)
                expect(report["id"]).to eq("103")
        end
    end
    
    describe "download_report" do
        it "gets the report status" do
            repair_pricer_request = RepairPricer::Request.new
            allow(RepairPricer::Request).to receive(:new).and_return(repair_pricer_request)
            allow(repair_pricer_request).to receive(:post_call).and_return({"id" => "103"})
                report = RepairPricer::Client.new.download_report(1)
                expect(report["id"]).to eq("103")
        end
    end
  
end