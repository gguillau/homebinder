require 'rails_helper'

RSpec.describe RepairPricer::Request do

    describe "#post_call" do
        it "returns 201 response" do
            url = "http://www.test.com"
            stub_request(:post, URI.escape(url)).to_return(:status => 201, :body => "{}")
            response = RepairPricer::Request.new.post_call(url, {}, {})
            
            # expect the response and the body to be equal
            expect(response).to eq({})
        end
        it "returns 403 response" do
            url = "http://www.test.com"
            stub_request(:post, URI.escape(url)).to_return(:status => 403, :body => "")

            # expect method to raise an error
            expect{RepairPricer::Request.new.post_call(url, {}, {})}.to raise_error BadRequestException
        end
    end
end