require 'rails_helper'

RSpec.describe Intercom::HomeBinderClient do
    
    describe "#get_token_name" do
        it "returns production token" do
            allow(Host).to receive(:path).and_return(Host::PRODUCTION_HOST)
            token = Intercom::HomeBinderClient.new.get_token_name
            expect(token).to eq(Intercom::HomeBinderClient::INTERCOM_PRODUCTION_TOKEN)
        end
    end
  
end