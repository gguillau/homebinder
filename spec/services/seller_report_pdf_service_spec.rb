require 'rails_helper'
require 'mandrill'

RSpec.describe SellerReportPdfService, :type => :service do

    describe "create" do
        it "creates a pdf" do
            binder = create(:binder)
            image = create(:image)
            binder.hero_image_id = image.id
            binder.save!

            seller_report = create(:seller_report, :binder => binder)
            service = SellerReportPdfService.new(seller_report)
            allow(service).to receive(:add_logos)
            allow(service).to receive(:add_title)
            
            pdf = service.create

            expect(pdf).to_not be(nil)
        end
    end

    describe "init" do
        it "sets the binder, pdf and pdf font" do
            binder = create(:binder)
            seller_report = create(:seller_report, :binder => binder)

            service = SellerReportPdfService.new(seller_report)
            allow(service).to receive_message_chain(:open, :read) { Faker::LoremPixel.image(secure: false) }
            
            expect(service.binder).to eq(binder)
            expect(service.seller_report).to eq(seller_report)
            expect(service.text).to eq("Seller's Report for")
            expect(service.full_report).to be(false)
            expect(service.pdf).to_not be(nil)
            expect(service.pdf.font_families).to_not be(nil)
        end
    end

    describe "add_logos" do
        it "adds homebinder and branding logo" do
            binder = create(:binder, :details => "This is text")
            logo = Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleImage.png'), 'image/png')
            user_profile = create(:user_profile, :logo => logo)
            create(:binder_branding, :binder => binder, :user_profile => user_profile)
            seller_report = create(:seller_report, :binder => binder)

            service = SellerReportPdfService.new(seller_report)
            allow(service).to receive_message_chain(:open, :read) { Faker::LoremPixel.image(secure: false) }
            allow(service.pdf).to receive(:image)

            service.init
            service.add_logos

            expect(service.pdf).to have_received(:image).twice
        end
    end

    describe "add_title" do
        it "adds the title to the report" do
            binder = create(:binder, :details => "This is text")
            seller_report = create(:seller_report, :binder => binder)

            service = SellerReportPdfService.new(seller_report)
            allow(service.pdf).to receive(:formatted_text_box)
            allow(service.pdf).to receive(:text)

            service.init
            service.add_title

            expect(service.pdf).to have_received(:formatted_text_box).with([
                                       { :text => "#{service.text}\n", :color => "999999", :size => 8 },
                                       { :text => "#{binder.property.address1}\n", :size => 22, :font => "Arvo" },
                                       { :text => "#{binder.property.city}, #{binder.property.state} #{binder.property.zip}", :color => '999999', :size => 12, :font => "Arvo" }
                                   ], :at => [50, 665], :width => 400, :height => 80)
        end
    end

    describe "det_item_ct" do
        it "sets item count for the pdf to 0 for all items" do
            binder = create(:binder)
            seller_report = create(:seller_report, :binder => binder)

            service = SellerReportPdfService.new(seller_report)

            service.det_item_ct

            expect(service.aprintcount).to eq(0)
            expect(service.mprintcount).to eq(0)
            expect(service.pprintcount).to eq(0)
            expect(service.cprintcount).to eq(0)
            expect(service.paprintcount).to eq(0)
            expect(service.fprintcount).to eq(0)
            expect(service.dprintcount).to eq(0)
            expect(service.ptprintcount).to eq(0)
        end

        it "sets item count for the pdf to 2 for all items" do
            binder = create(:binder)
            create_list(:appliance, 2, :binder => binder)
            create_list(:maintenance_item, 2, :binder => binder)
            create_list(:binder_contractor, 2, :binder => binder)
            create_list(:project, 2, :binder => binder, :status => "Completed")
            create_list(:paint, 2, :binder => binder)
            create_list(:finish, 2, :binder => binder)
            create_list(:document, 2, :binder => binder)
            create_list(:permit, 2, :binder => binder)
            seller_report = create(:seller_report, :binder => binder)

            binder.appliances.each do |app|
                create(:seller_report_item, :appliance_id => app.id, :include => true)
            end
                binder.maintenance_items.each do |item|
                create(:seller_report_item, :maintenance_item_id => item.id, :include => true)
            end
                binder.projects.each do |project|
                create(:seller_report_item, :project_id => project.id, :include => true)
            end
                binder.binder_contractors.each do |binder_contractor|
                create(:seller_report_item, :binder_contractor_id => binder_contractor.id, :include => true)
            end
                binder.paints.each do |paint|
                create(:seller_report_item, :paint_id => paint.id, :include => true)
            end
                binder.permits.each do |perm|
                create(:seller_report_item, :permit_id => perm.id, :include => true)
            end
                binder.finishes.each do |finish|
                create(:seller_report_item, :finish_id => finish.id, :include => true)
            end
                binder.documents.each do |doc|
                create(:seller_report_item, :document_id => doc.id, :include => true)
            end
                binder.images.each do |img|
                create(:seller_report_item, :image_id => img.id, :include => true)
            end

            service = SellerReportPdfService.new(seller_report)

            service.det_item_ct

            expect(service.aprintcount).to eq(2)
            expect(service.mprintcount).to eq(2)
            expect(service.cprintcount).to eq(2)
            expect(service.pprintcount).to eq(2)
            expect(service.paprintcount).to eq(2)
            expect(service.fprintcount).to eq(2)
            expect(service.dprintcount).to eq(2)
            expect(service.ptprintcount).to eq(2)
        end

        it "sets item count for the pdf to 3 for all items" do
            binder = create(:binder)
            create_list(:appliance, 4, :binder => binder)
            create_list(:maintenance_item, 4, :binder => binder)
            create_list(:binder_contractor, 4, :binder => binder)
            create_list(:project, 4, :binder => binder, :status => "Completed")
            create_list(:paint, 4, :binder => binder)
            create_list(:finish, 4, :binder => binder)
            create_list(:document, 4, :binder => binder)
            create_list(:permit, 4, :binder => binder)
            seller_report = create(:seller_report, :binder => binder)

            binder.appliances.each do |app|
                create(:seller_report_item, :appliance_id => app.id, :include => true)
            end
                binder.maintenance_items.each do |item|
                create(:seller_report_item, :maintenance_item_id => item.id, :include => true)
            end
                binder.projects.each do |project|
                create(:seller_report_item, :project_id => project.id, :include => true)
            end
                binder.binder_contractors.each do |binder_contractor|
                create(:seller_report_item, :binder_contractor_id => binder_contractor.id, :include => true)
            end
                binder.paints.each do |paint|
                create(:seller_report_item, :paint_id => paint.id, :include => true)
            end
                binder.permits.each do |perm|
                create(:seller_report_item, :permit_id => perm.id, :include => true)
            end
                binder.finishes.each do |finish|
                create(:seller_report_item, :finish_id => finish.id, :include => true)
            end
                binder.documents.each do |doc|
                create(:seller_report_item, :document_id => doc.id, :include => true)
            end
                binder.images.each do |img|
                create(:seller_report_item, :image_id => img.id, :include => true)
            end

            service = SellerReportPdfService.new(seller_report)

            service.det_item_ct

            expect(service.aprintcount).to eq(3)
            expect(service.mprintcount).to eq(3)
            expect(service.cprintcount).to eq(3)
            expect(service.pprintcount).to eq(3)
            expect(service.paprintcount).to eq(3)
            expect(service.fprintcount).to eq(3)
            expect(service.dprintcount).to eq(3)
            expect(service.ptprintcount).to eq(3)
        end
    end

    describe "add_details" do
        it "adds the binder details to the report" do
            binder = create(:binder, :details => "This is text")
            seller_report = create(:seller_report, :binder => binder)

            service = SellerReportPdfService.new(seller_report)
            allow(service.pdf).to receive(:formatted_text_box)
            allow(service.pdf).to receive(:text)

            service.init
            service.add_details

            expect(service.pdf).to have_received(:formatted_text_box).with([
                                       { :text => "Home Details", :size => 12, :color => "ff6633" },
                                   ], :at => [0, 636.0], :width => 400)

            expect(service.pdf).to have_received(:text).with("This is text", {:size => 9, :color=>"000000", :align => :justify})
        end
    end

    context "adding items" do
        let(:binder) { create(:binder) }
        let(:seller_report) { create(:seller_report, :binder => binder) }

        context "adding more than 3 items" do
            before :each do
                create_list(:appliance, 4, :binder => binder)
                create_list(:maintenance_item, 4, :binder => binder)
                create_list(:binder_contractor, 4, :binder => binder)
                create_list(:project, 4, :binder => binder, :status => "Completed")
                create_list(:paint, 2, :binder => binder)
                create_list(:finish, 2, :binder => binder)
                create_list(:permit, 2, :binder => binder)
                create_list(:document, 2, :binder => binder)

                binder.appliances.each do |app|
                    create(:seller_report_item, :appliance_id => app.id, :include => true)
                end
                        binder.maintenance_items.each do |item|
                    create(:seller_report_item, :maintenance_item_id => item.id, :include => true)
                end
                        binder.projects.each do |project|
                    create(:seller_report_item, :project_id => project.id, :include => true)
                end
                        binder.binder_contractors.each do |binder_contractor|
                    create(:seller_report_item, :binder_contractor_id => binder_contractor.id, :include => true)
                end
                        binder.paints.each do |paint|
                    create(:seller_report_item, :paint_id => paint.id, :include => true)
                end
                        binder.permits.each do |perm|
                    create(:seller_report_item, :permit_id => perm.id, :include => true)
                end
                        binder.finishes.each do |finish|
                    create(:seller_report_item, :finish_id => finish.id, :include => true)
                end
                        binder.documents.each do |doc|
                    create(:seller_report_item, :document_id => doc.id, :include => true)
                end
                        binder.images.each do |img|
                    create(:seller_report_item, :image_id => img.id, :include => true)
                end
            end

            describe "add_appliances" do
                it "adds the appliances to the report" do
                    service = SellerReportPdfService.new(seller_report)
                    allow(service.pdf).to receive(:formatted_text_box)
                    allow(service.pdf).to receive(:text)

                    service.init
                    service.det_item_ct
                    service.add_appliances

                    expect(service.pdf).to have_received(:formatted_text_box).with([
                                               { :text => "Appliance", :size => 12, :color => "ff6633" },
                                           ], :at => [0, 644.0], :width => 400)
                end
            end

            describe "add_maintenance" do
                it "adds the maintenance_items to the report" do
                    service = SellerReportPdfService.new(seller_report)
                    allow(service.pdf).to receive(:formatted_text_box)
                    allow(service.pdf).to receive(:text)

                    service.init
                    service.det_item_ct
                    service.add_maintenance

                    expect(service.pdf).to have_received(:formatted_text_box).with([
                                               { :text => "Maintenance", :size => 12, :color => "ff6633" },
                                           ], :at => [0, 636.0], :width => 400)
                end
            end

            describe "add_projects" do
                it "adds the projects to the report" do
                    service = SellerReportPdfService.new(seller_report)
                    allow(service.pdf).to receive(:formatted_text_box)
                    allow(service.pdf).to receive(:text)

                    service.init
                    service.det_item_ct
                    service.add_projects

                    expect(service.pdf).to have_received(:formatted_text_box).with([
                                               { :text => "Home Improvements", :size => 12, :color => "ff6633" },
                                           ], :at => [0, 636.0], :width => 400)
                end
            end

            describe "add_contractors" do
                it "adds the contractors to the report" do
                    service = SellerReportPdfService.new(seller_report)
                    allow(service.pdf).to receive(:formatted_text_box)
                    allow(service.pdf).to receive(:text)

                    service.init
                    service.det_item_ct
                    service.add_contractors

                    expect(service.pdf).to have_received(:formatted_text_box).with([
                                               { :text => "Home Pros", :size => 12, :color => "ff6633" },
                                           ], :at => [0, 636.0], :width => 400)
                end
            end

            describe "add_paints" do
                it "adds the paints to the report" do
                    service = SellerReportPdfService.new(seller_report)
                    allow(service.pdf).to receive(:formatted_text_box)
                    allow(service.pdf).to receive(:text)

                    service.init
                    service.det_item_ct
                    service.add_paints

                    expect(service.pdf).to have_received(:formatted_text_box).with([
                                               { :text => "Paints", :size => 12, :color => "ff6633" },
                                           ], :at => [0, 631.0], :width => 400)
                end
            end
                describe "add_permits" do
                it "adds the permits to the report" do
                    service = SellerReportPdfService.new(seller_report)
                    allow(service.pdf).to receive(:formatted_text_box)
                    allow(service.pdf).to receive(:text)

                    service.init
                    service.det_item_ct
                    service.add_permits

                    expect(service.pdf).to have_received(:formatted_text_box).with([
                                               { :text => "Permits", :size => 12, :color => "ff6633" },
                                           ], :at => [0, 631.0], :width => 400)
                end
            end

            describe "add_finishes" do
                it "adds the finishes to the report" do
                    service = SellerReportPdfService.new(seller_report)
                    allow(service.pdf).to receive(:formatted_text_box)
                    allow(service.pdf).to receive(:text)

                    service.init
                    service.det_item_ct
                    service.add_finishes

                    expect(service.pdf).to have_received(:formatted_text_box).with([
                                               { :text => "Finishes", :size => 12, :color => "ff6633" },
                                           ], :at => [0, 631.0], :width => 400)
                end
            end

            describe "add_documents" do
                it "adds the documents to the report" do
                    service = SellerReportPdfService.new(seller_report)
                    allow(service.pdf).to receive(:formatted_text_box)
                    allow(service.pdf).to receive(:text)

                    service.init
                    service.det_item_ct
                    service.add_documents

                    expect(service.pdf).to have_received(:formatted_text_box).with([
                                               { :text => "Documents", :size => 12, :color => "ff6633" },
                                           ], :at => [0, 631.0], :width => 400)
                end
            end
        end

        context "adding 1 item" do
            describe "add_appliances" do
                it "adds the appliances to the report" do
                    create_list(:appliance, 4, :binder => binder, :manufacturer => nil)
                    binder.appliances.each do |app|
                        create(:seller_report_item, :appliance_id => app.id, :include => true)
                    end

                    service = SellerReportPdfService.new(seller_report)
                    allow(service.pdf).to receive(:formatted_text_box)
                    allow(service.pdf).to receive(:text)

                    service.init
                    service.det_item_ct
                    service.add_appliances

                    expect(service.pdf).to have_received(:formatted_text_box).with([
                                               { :text => "Appliance", :size => 12, :color => "ff6633" },
                                           ], :at => [0, 644.0], :width => 400)
                end
            end

            describe "add_maintenance" do
                it "adds the maintenance_items to the report" do
                    create_list(:maintenance_item, 4, :binder => binder)
                    binder.maintenance_items.each do |item|
                        create(:seller_report_item, :maintenance_item_id => item.id, :include => true)
                    end

                    service = SellerReportPdfService.new(seller_report)
                    allow(service.pdf).to receive(:formatted_text_box)
                    allow(service.pdf).to receive(:text)

                    service.init
                    service.det_item_ct
                    service.add_maintenance

                    expect(service.pdf).to have_received(:formatted_text_box).with([
                                               { :text => "Maintenance", :size => 12, :color => "ff6633" },
                                           ], :at => [0, 636.0], :width => 400)
                end
            end

            describe "add_projects" do
                it "adds the projects to the report" do
                    create_list(:project, 4, :binder => binder, :status => "Completed", :end_date => nil)
                    binder.projects.each do |project|
                        create(:seller_report_item, :project_id => project.id, :include => true)
                    end

                    service = SellerReportPdfService.new(seller_report)
                    allow(service.pdf).to receive(:formatted_text_box)
                    allow(service.pdf).to receive(:text)

                    service.init
                    service.det_item_ct
                    service.add_projects

                    expect(service.pdf).to have_received(:formatted_text_box).with([
                                               { :text => "Home Improvements", :size => 12, :color => "ff6633" },
                                           ], :at => [0, 636.0], :width => 400)
                end
            end

            describe "add_contractors" do
                it "adds the contractors to the report" do
                    create_list(:binder_contractor, 4, :binder => binder)
                    binder.binder_contractors.each do |binder_contractor|
                        create(:seller_report_item, :binder_contractor_id => binder_contractor.id, :include => true)
                    end

                    service = SellerReportPdfService.new(seller_report)
                    allow(service.pdf).to receive(:formatted_text_box)
                    allow(service.pdf).to receive(:text)

                    service.init
                    service.det_item_ct
                    service.add_contractors

                    expect(service.pdf).to have_received(:formatted_text_box).with([
                                               { :text => "Home Pros", :size => 12, :color => "ff6633" },
                                           ], :at => [0, 636.0], :width => 400)
                end
            end

            describe "add_paints" do
                it "adds the paints to the report" do
                    create_list(:paint, 4, :binder => binder)
                    binder.paints.each do |paint|
                        create(:seller_report_item, :paint_id => paint.id, :include => true)
                    end

                    service = SellerReportPdfService.new(seller_report)
                    allow(service.pdf).to receive(:formatted_text_box)
                    allow(service.pdf).to receive(:text)

                    service.init
                    service.det_item_ct
                    service.add_paints

                    expect(service.pdf).to have_received(:formatted_text_box).with([
                                               { :text => "Paints", :size => 12, :color => "ff6633" },
                                           ], :at => [0, 631.0], :width => 400)
                end
            end
                describe "add_permits" do
                it "adds the permits to the report" do
                    create_list(:permit, 4, :binder => binder)
                    binder.permits.each do |perm|
                        create(:seller_report_item, :permit_id => perm.id, :include => true)
                    end

                    service = SellerReportPdfService.new(seller_report)
                    allow(service.pdf).to receive(:formatted_text_box)
                    allow(service.pdf).to receive(:text)

                    service.init
                    service.det_item_ct
                    service.add_permits

                    expect(service.pdf).to have_received(:formatted_text_box).with([
                                               { :text => "Permits", :size => 12, :color => "ff6633" },
                                           ], :at => [0, 631.0], :width => 400)
                end
            end

            describe "add_finishes" do
                it "adds the finishes to the report" do
                    create_list(:finish, 4, :binder => binder)
                    binder.finishes.each do |finish|
                        create(:seller_report_item, :finish_id => finish.id, :include => true)
                    end

                    service = SellerReportPdfService.new(seller_report)
                    allow(service.pdf).to receive(:formatted_text_box)
                    allow(service.pdf).to receive(:text)

                    service.init
                    service.det_item_ct
                    service.add_finishes

                    expect(service.pdf).to have_received(:formatted_text_box).with([
                                               { :text => "Finishes", :size => 12, :color => "ff6633" },
                                           ], :at => [0, 631.0], :width => 400)
                end
            end

            describe "add_documents" do
                it "adds the documents to the report" do
                    (0..3).each do |i|
                        create(:document, :binder => binder, :file_file_name => "test#{i}.pdf")
                    end

                    binder.documents.each do |doc|
                        create(:seller_report_item, :document_id => doc.id, :include => true)
                    end

                    service = SellerReportPdfService.new(seller_report)
                    allow(service.pdf).to receive(:formatted_text_box)
                    allow(service.pdf).to receive(:text)

                    service.init
                    service.det_item_ct
                    service.add_documents

                    expect(service.pdf).to have_received(:formatted_text_box).with([
                                               { :text => "Documents", :size => 12, :color => "ff6633" },
                                           ], :at => [0, 631.0], :width => 400)
                end
            end
        end
    end

    describe "add_footer" do
        it "adds footer" do
            binder = create(:binder, :details => "This is text")
            seller_report = create(:seller_report, :binder => binder)

            service = SellerReportPdfService.new(seller_report)
            allow(service.pdf).to receive(:repeat)

            service.init
            service.add_footer

            expect(service.pdf).to have_received(:repeat)
        end
    end
end
