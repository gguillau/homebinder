require 'rails_helper'

RSpec.describe Constraint::HomeBinder, :type => :model do
    
  describe "verify_key?" do

    it "returns true" do
      key = create :api_key
      value = Constraint::HomeBinder.verify_key(key.key)
      
      expect(value).to eq(true)
    end
    
    it "returns false" do
      key = create :api_key, key: "test"
      value = Constraint::HomeBinder.verify_key(key.key)
      
      expect(value).to eq(false)
    end
    
    it "returns false" do
      value = Constraint::HomeBinder.verify_key(nil)

      expect(value).to eq(false)
    end
    
  end
  
end
