require 'rails_helper'

RSpec.describe Constraint::Partner, :type => :model do
    
  describe "verify_key?" do

    it "returns true" do
      partner = create :partner
      key = create :api_key, partner_id: partner.id
    
      value = Constraint::Partner.find_partner(key.key)
      
      expect(value).to eq(true)
    end
    
    it "returns false" do
      key = create :api_key, :partner_id => nil
      value = Constraint::Partner.find_partner(key.key)
      
      expect(value).to eq(false)
    end

  end
  
end
