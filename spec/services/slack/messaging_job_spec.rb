require 'rails_helper'
RSpec.describe Slack::MessagingJob, :type => :model do
    
    describe "#send_message" do
        it "It sends a message to binder automation channel" do
            slack = double("Slack::Web::Client", :chat_postMessage => true)
            allow(Slack::Web::Client).to receive(:new).and_return(slack)
            allow(slack).to receive(:chat_postMessage)
            
            Slack::MessagingJob.perform_async("", "text")
            expect(slack).to have_received(:chat_postMessage)
        end
    end
end