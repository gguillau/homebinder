require 'rails_helper'
require 'mandrill'

RSpec.describe Payments::Customer do
    
    let(:user){create :user}
    let(:stripe_customer){OpenStruct.new({id: 1, email: "test@gmail.com", source: nil, delete: true})}

    before :each do
        Sidekiq::Testing.fake!
        # mock the stripe api
        allow(stripe_customer).to receive(:delete).and_call_original
        allow(Stripe::Customer).to receive(:retrieve).and_return(stripe_customer)
        allow(Stripe::Customer).to receive(:create).and_return(stripe_customer)
        allow(Stripe::Plan).to receive(:retrieve).and_return(OpenStruct.new({id: 1, amount: 4900}))
        allow(Stripe::Subscription).to receive(:create).and_return({id: 1})
    end

    describe "#show" do
        it "gets a customer" do
            customer = Payments::Customer.new(user).show({id: 1})
            expect(customer.id).to eq(1)
        end
        it "gets a customer" do
            allow(Stripe::Customer).to receive(:retrieve).and_raise(BadRequestException.new("Error"))
            expect{Payments::Customer.new(user).show({id: 1})}.to raise_error BadRequestException
        end
    end

    describe "#create" do
        context "when no errors are raised" do
            before :each do
                allow(Partners::PdfGeneratorServiceJob).to receive(:perform_async)
                allow(SubscriptionMailer).to receive(:notify_customer_subscription).and_return(double("SubscriptionMailer", :deliver_later => true))
                @partner = create(:partner)
                @account = create(:account, :manager_id => @partner.id)
                params = {customer: {email: "test@gmail.com", address: {address1: "123 Main Street"}}, account_id: @account.id, payment: {card: "card"}}
                @customer = Payments::Customer.new(user).create(ActionController::Parameters.new(params))
                @account.reload
            end
            it "updates billing account" do
                expect(@account.stripe_customer_id).to eq("1")
                expect(@account.account_status).to eq("active")
                expect(@account.account_sub_type).to eq("paid")
                expect(@account.account_sub_status).to eq("none")
                expect(@account.billing_activation.to_date).to eq(Date.today)
                expect(@account.billing_frequency).to eq("monthly")
                expect(@account.payment_type).to eq("subscription")
                expect(@account.subscription_amount_cents).to eq(4900)
            end
            it "creates a customer" do
                expect(@customer.id).to eq(1)
            end
            it "calls Partners::PdfGeneratorServiceJob" do
                expect(Partners::PdfGeneratorServiceJob).to have_received(:perform_async)
            end
            it "calls Mailer" do
                expect(SubscriptionMailer).to have_received(:notify_customer_subscription)
            end
        end
        context "when errors are raised" do
            it "raises BadRequestException" do
                allow(Stripe::Customer).to receive(:create).and_raise(BadRequestException.new("Error"))
                partner = create(:partner)
                account = create(:account, :manager_id => partner.id)
                params = {customer: {email: "test@gmail.com", address: {address1: "123 Main Street"}}, account_id: account.id, payment: {card: "card"}}
                expect{Payments::Customer.new(user).create(params)}.to raise_error BadRequestException
            end
            it "raises BadRequestException" do
                allow(Stripe::Subscription).to receive(:create).and_raise(BadRequestException.new("Error"))
                partner = create(:partner)
                account = create(:account, :manager_id => partner.id)
                params = {customer: {email: "test@gmail.com", address: {address1: "123 Main Street"}}, account_id: account.id, payment: {card: "card"}}
                expect{Payments::Customer.new(user).create(params)}.to raise_error BadRequestException
            end
        end
    end

    describe "#update" do
        it "updates a customer" do
            partner = create(:partner)
            account = create(:account, :manager_id => partner.id)
            address = {}
            customer = Payments::Customer.new(user).update({id: 1, customer: {email: "fake@gmail.com", address: address}, payment: {card: "card"}, account_id: account.id})
            expect(customer.id).to eq(1)
        end
    end

    describe "#delete" do
        it "deletes a customer" do
            Payments::Customer.new(user).delete({id: 1})
            expect(stripe_customer).to have_received(:delete)
        end
    end

    describe "#find_plan" do
        it "raises an error" do
            allow(Stripe::Plan).to receive(:retrieve).and_raise(BadRequestException.new "Stripe error")
            expect{Payments::Customer.new(user).find_plan({plan: "Amerispec Service Inspections Subscription", cost: 165, customer: {country: "CA"}})}.to raise_error BadRequestException
        end
        it "creates a plan" do
            allow(Stripe::Plan).to receive(:retrieve).and_raise(BadRequestException, "No such Plan: Amerispec Service Inspections Subscription")
            client = Payments::Customer.new(user)
            allow(Payments::Customer).to receive(:new).and_return(client)
            allow(client).to receive(:create_plan)
            client.find_plan({plan: "Amerispec Service Inspections Subscription", cost: 165, customer: {country: "CA"}})
            expect(client).to have_received(:create_plan)
        end
        it "returns a plan" do
            plan = Payments::Customer.new(user).find_plan({plan: "Amerispec Service Inspections Subscription", cost: 165, customer: {country: "CA"}})
            expect(plan).to_not be(nil)
            expect(plan.id).to eq(1)
            expect(plan.amount).to eq(4900)
        end
    end

    describe "#create_plan" do
        it "raises an error" do
            allow(Stripe::Plan).to receive(:create).and_raise(BadRequestException.new "Stripe error")
            expect{Payments::Customer.new(user).create_plan("Amerispec Service Inspections Subscription", 165, "usd")}.to raise_error BadRequestException
        end
        it "creates a plan" do
            allow(Stripe::Plan).to receive(:create).and_return(OpenStruct.new({id: 1, amount: 4900}))
            plan = Payments::Customer.new(user).create_plan("Amerispec Service Inspections Subscription", 165, "usd")
            expect(plan).to_not be(nil)
            expect(plan.id).to eq(1)
            expect(plan.amount).to eq(4900)
        end
    end

    describe "#find_country_code" do
        it "returns usd" do
            currency = Payments::Customer.new(user).find_country_code({customer: {country: nil}})
            expect(currency).to eq("usd")
        end
        it "returns usd" do
            currency = Payments::Customer.new(user).find_country_code({customer: {country: "OAKDASKDSAODK"}})
            expect(currency).to eq("usd")
        end
        it "returns usd" do
            currency = Payments::Customer.new(user).find_country_code({customer: {country: "US"}})
            expect(currency).to eq("usd")
        end
        it "returns cad" do
            currency = Payments::Customer.new(user).find_country_code({customer: {country: "CA"}})
            expect(currency).to eq("cad")
        end
    end

    describe "#get_cost" do
        it "returns 6000" do
            cost = Payments::Customer.new(user).get_cost(60)
            expect(cost).to eq(6000)
        end
    end
end
