require 'rails_helper'
require 'stripe'

RSpec.describe Payments::CreditCardProcessor do
  
  before :each do
    @user = create(:user)
  end
  
  describe "charge_card" do
    it "charges to the provided card" do
      token = Stripe::Token.create(
        card: {
          number: "5555555555554444",
          exp_month: 06,
          exp_year: DateTime.now.year + 1,
          cvc: 123
        }
      )
      
      processor = Payments::CreditCardProcessor.new(@user)
      charge = processor.charge_card({
        :card => token,
        :amount => 15000,
        :description => "test charge card"
      })
      
      expect(charge).to_not be_nil
      expect(charge.amount).to eq(15000)
      expect(charge.description).to eq("test charge card")
    end
    
    it "rescues from an error" do
      processor = Payments::CreditCardProcessor.new(@user)
      allow(Stripe::Charge).to receive(:create).and_raise(BadRequestException)
      expect{processor.charge_card({})}.to raise_error BadRequestException
    end
  end
  
  describe "refund" do
    it "creates a refund" do
      token = Stripe::Token.create(
        card: {
          number: "4242424242424242",
          exp_month: 06,
          exp_year: DateTime.now.year + 1,
          cvc: 123
        }
      )
      
      charge = Stripe::Charge.create(
        amount: 10000,
        currency: "usd",
        source: token
      )
      
      processor = Payments::CreditCardProcessor.new(@user)
      refund = processor.refund(charge.id)
      
      expect(refund).to_not be_nil
    end
    
    it "rescues from an error" do
      processor = Payments::CreditCardProcessor.new(@user)
      allow(Stripe::Refund).to receive(:create).and_raise(BadRequestException)
      expect{processor.refund(1)}.to raise_error BadRequestException
    end
  end
  
end