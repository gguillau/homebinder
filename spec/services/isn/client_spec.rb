require 'rails_helper'

RSpec.describe Isn::Client, :type => :model do
    describe "client" do
        before do
            @user = create(:user, :role => "inspector")
            @partner = create(:partner)
            @partner_config = @partner.partner_configuration
            @isn_user = create(:isn_user, partner_configuration_id: @partner_config.id)
            @no_access = create(:partner)
            @isn_user.company_key = "test"
            @isn_user.api_endpoint = "inspectionsupport.com/test"
            @isn_user.username = "bsmith"
            @isn_user.password = "password"
            @isn_user.save
            @user.add_role :partner_admin, @partner
            @request = HBRequest.new.create_request(@user)
        end

        describe "error handling" do
            it "raises not found when the partner does not exist" do
                expect{Isn::Client.new(@request, 9999999).get_footprints}.to raise_error(ActiveRecord::RecordNotFound)
            end

            it "raises permission denied when the user does not have read access to the partner" do
                @isn_user.partner_configuration_id = @no_access.partner_configuration.id
                @isn_user.save
                expect{Isn::Client.new(@request, @isn_user.id).get_footprints}.to raise_error(CanCan::AccessDenied)
            end

            it "raises bad request when isn configuration is nil" do
                @isn_user.destroy
                expect{Isn::Client.new(@request, @isn_user.id).get_footprints}.to raise_error(ActiveRecord::RecordNotFound)
            end
        end

        describe "API calls" do
            describe "execute_request" do
                it "returns footprints" do
                    client = Isn::Client.new(@request, @isn_user.id)
                    allow(client).to receive(:call_isn).and_return({"footprints" => []})
                                resp = client.execute_request({:request => {:route => "agents"}})
                    expect(resp).to eq({"footprints" => []})
                end
                        it "returns footprints" do
                    client = Isn::Client.new(@request, @isn_user.id)
                    allow(client).to receive(:call_isn).and_return({"footprints" => [{"datetime" => Date.today}]})
                    allow(client).to receive(:create_footprints).and_return([{:id => 1}])
                                resp = client.get_footprints
                    expect(resp).to eq({:footprints => [{:id => 1}], :total => 1})
                end
                        it "returns the client info" do
                    client = Isn::Client.new(@request, @isn_user.id)
                    allow(client).to receive(:call_isn).and_return({"client" => {"first_name" => "Bob"}})
                                resp = client.get_client(1)
                    expect(resp).to eq({"first_name" => "Bob"})
                end
                        it "returns the api call" do
                    client = Isn::Client.new(@request, @isn_user.id)

                    isn_request = double("Isn::Request", :call => {})
                    allow(Isn::Request).to receive(:new).and_return(isn_request)
                    allow(isn_request).to receive(:call).and_return({"url" => "inspectionsupport.net/test"})
                                client.call_isn("orders/footprint/", {:id => 1, :name => "test"})
                                expect(isn_request).to have_received(:call).at_least(:once)
                end
            end
                describe "footprints" do
                it "returns array" do
                    isn = {
                        "client" => {},
                        "property" => {}
                    }
                        client = Isn::Client.new(@request, @isn_user.id)
                    allow(client).to receive(:get_client_info).and_return(isn)
                                footprints = client.create_footprints([{"id" => "fakeid", "datetime" => Date.today}])
                    expect(footprints).to eq([{:id => "fakeid", :datetime => Date.today, :client => nil, :property => nil}])
                end
            end

            describe "clients" do
                it "returns empty hash" do
                    isn = {:client=>{:name=>" ", :email=>nil, :home_phone=>nil, :mobile_phone=>nil}, :property=>{:address1=>nil, :address2=>nil, :city=>nil, :state=>nil, :zip=>nil}, :buyersagent=>{}, :sellersagent=>{}}
                    client = Isn::Client.new(@request, @isn_user.id).get_client_info({})
                    expect(client).to eq(isn)
                end
                        it "returns homeowner, buyer agent and seller agent" do
                    fp = {
                        "client" => "client",
                        "buyersagent" => "buyersagent",
                        "sellersagent" => "sellersagent",
                        "address1" => "123 Main Street",
                        "city" => "Boston",
                        "state" => "MA",
                        "zip" => "02210"
                    }
                    homeowner = {
                        "first" => "Test",
                        "last" => "Homeowner", 
                        "email" => "test@test.com", 
                        "home_phone" => "", 
                        "mobile_phone" => ""
                    }
                    agent = {
                        :email => "test@gmail.com",
                        :user_profile_attributes => {
                            :first_name => "first",
                            :last_name => "last",
                            :website => "url",
                            :home_phone => nil,
                            :mobile_phone => nil,
                            :company => "Keller Williams",
                            :address_attributes => {
                                :address1 => "address1",
                                :address2 => "address2",
                                :city => "city",
                                :state => "state",
                                :zip => "zip",
                                :country => "US"
                            }
                        }
                    }
                    info = {
                        :client => {
                            :name => "Test Homeowner", 
                            :email => "test@test.com", 
                            :home_phone => nil, 
                            :mobile_phone => nil
                        }, 
                        :property => {
                            :address1 => "123 Main Street", 
                            :address2 => nil, 
                            :city => "Boston", 
                            :state => "MA", 
                            :zip => "02210"
                        }, 
                        :buyersagent => agent,
                        :sellersagent => agent
                    }
                    client = Isn::Client.new(@request, @isn_user.id)
                    allow(client).to receive(:get_client).and_return(homeowner)
                    allow(client).to receive(:get_agent).and_return(agent)
                                hash = client.get_client_info(fp)
                    expect(hash).to eq(info)
                end
            end

            describe "agents" do
                it "gets an existing agent" do
                    address = {:address1 => "123 Main Street", :city => "Boston", :state => "MA", :country => "US"}
                    agent = create(:user, :role => "agent", :user_profile_attributes => {:address_attributes => address, :first_name => "Agent", :last_name => "Test"})

                    params = {
                        "agent" => {
                            "email" => agent.email
                        }
                    }
                    client = Isn::Client.new(@request, @isn_user.id)
                    allow(client).to receive(:execute_request).and_return(params)

                    user = client.get_agent("test")
                    expect(user[:email]).to eq(agent.email)
                end

                it "returns a new agent" do
                    params = {
                        "agent" => {
                            "first" => "first",
                            "last" => "last",
                            "url" => "url",
                            "homephone" => "homephone",
                            "mobilephone" => "mobilephone",
                            "email" => "test@gmail.com",
                            "agency" => "fake",
                            "address1" => "address1",
                            "address2" => "address2",
                            "city" => "city",
                            "state" => "state",
                            "zip" => "zip"
                        }
                    }

                    company = {
                        "agency" => {
                            "name" => "Keller Williams"
                        }
                    }

                    found = {
                        :email => "test@gmail.com",
                        :user_profile_attributes => {
                            :first_name => "first",
                            :last_name => "last",
                            :website => "url",
                            :home_phone => nil,
                            :mobile_phone => nil,
                            :company => "Keller Williams",
                            :address_attributes => {
                                :address1 => "address1",
                                :address2 => "address2",
                                :city => "city",
                                :state => "state",
                                :zip => "zip",
                                :country => "US"
                            }
                        }
                    }

                    client = Isn::Client.new(@request, @isn_user.id)
                    allow(client).to receive(:execute_request).and_return(params, company)

                    user = client.get_agent("test")
                    expect(user).to eq(found)
                end
            end

            describe "binder" do
                it "returns information needed to create a binder" do
                    agent = {
                        :email => "test@gmail.com",
                        :user_profile_attributes => {
                            :first_name => "first",
                            :last_name => "last",
                            :website => "url",
                            :home_phone => nil,
                            :mobile_phone => nil,
                            :company => "Keller Williams",
                            :address_attributes => {
                                :address1 => "address1",
                                :address2 => "address2",
                                :city => "city",
                                :state => "state",
                                :zip => "zip",
                                :country => "US"
                            }
                        }
                    }
                    info = {
                        :client => {
                            :name => "Test Homeowner", 
                            :email => "test@test.com", 
                            :home_phone => nil, 
                            :mobile_phone => nil
                        }, 
                        :property => {
                            :address1 => "123 Main Street", 
                            :address2 => nil, 
                            :city => "Boston", 
                            :state => "MA", 
                            :zip => "02210"
                        }, 
                        :buyersagent => agent,
                        :sellersagent => agent
                    }
                                client = Isn::Client.new(@request, @isn_user.id)
                    allow(client).to receive(:call_isn).and_return({"footprints" => [{"id" => "fake", "datetime" => Date.today}] })
                    allow(client).to receive(:get_client_info).and_return(info)
                    hash = client.get_binder_info("fake")
                    expect(hash).to eq(info)
                end
            end
        end
    end
end
