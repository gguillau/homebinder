require 'rails_helper'

RSpec.describe Isn::Request, :type => :service do
    it "sends a request and returns a 400 response"  do
        message = "{\n    \"status\": \"error\",\n    \"message\": \"You must login to use this service (username received: 'bsmith' - password had 4 characters)\"\n}"
        # stub the request and return a 400 response status
        stub_request(:get, URI.escape("http://test.com")).to_return(:status => 400, :body => message)
        # expect method to raise an error
        expect{Isn::Request.new.call("http://test.com", nil)}.to raise_error BadRequestException, "Isn::Request Error - You must login to use this service (username received: 'bsmith' - password had 4 characters) - http://test.com"
    end
end