require 'rails_helper'

RSpec.describe Buildfax::Request do
  
    before :each do
        @binder = create(:binder)
    end
    
    it "returns the username and password for buildfax" do

        # get the authentication
        auth = Buildfax::Request.new.auth

        expect(auth).to eq({ username: ENV["BUILD_FAX_USERNAME"], password: ENV["BUILD_FAX_PASSWORD"]})
    end
    
    it "sends a request and returns a 200 response" do
        # get the binder address
        address = "#{@binder.property.address1}, #{@binder.property.city}, #{@binder.property.state} #{@binder.property.zip}"
        url = "https://delivery.buildfax.com/api/reports/addresses/%s/?output=xml&template=HomeBinder_Dev" % address
        body = "<?xml version='1.0' encoding='UTF-8'?>\n<buildFaxReport display=\"BuildFax report\">\n  <reportHeader display=\"report header\">\n    <serialNumber display=\"serial number\" type=\"string\">20170102193116787101-NIPSTY-98449823</serialNumber>\n    <version display=\"version\" type=\"integer\">20161223</version>\n  </reportHeader>\n  <reportBody display=\"report body\">\n    <requestedAddress display=\"requested addresss\">\n      <fullAddress display=\"full address\" type=\"string\">714 Bartoletti Trail, Blandaside, GA 10038-3082</fullAddress>\n    </requestedAddress>\n  </reportBody>\n</buildFaxReport>\n"
        stub_request(:get, URI.escape(url)).to_return(:status => 200, :body => body)
        # send the request
        response = Buildfax::Request.new.send_request(address)
        # expect the response and the body to be equal
        expect(response).to eq(MultiXml.parse(body))
    end
    
    it "sends a request and returns a 400 response" do
        # get the binder address
        address = "#{@binder.property.address1}, #{@binder.property.city}, #{@binder.property.state} #{@binder.property.zip}"
        url = "https://delivery.buildfax.com/api/reports/addresses/%s/?output=xml&template=HomeBinder_Dev" % address
        # stub the request and return a 400 response status
        stub_request(:get, URI.escape(url)).to_return(:status => 401, :body => "Forbidden")
        # expect method to raise an error
        expect{Buildfax::Request.new.send_request(address)}.to raise_error BadRequestException
    end
    
    describe "#template" do
        it "returns production template" do
            allow(Host).to receive(:path).and_return(Host::PRODUCTION_HOST)
            template = Buildfax::Request.new.template
            expect(template).to eq(ENV["BUILD_FAX_PRODUCTION_TEMPLATE"].to_s)
        end
    end
    
    describe "#download_report" do
        it "returns 200 response" do
            # get the binder address
            address = "#{@binder.property.address1}, #{@binder.property.city}, #{@binder.property.state} #{@binder.property.zip}"
            url = "https://delivery.buildfax.com/api/reports/addresses/#{URI.encode(address)}/?output=PDF&template=BuildFax%20Permit%20Timeline%20Report"
            body = "<?xml version='1.0' encoding='UTF-8'?>\n<buildFaxReport display=\"BuildFax report\">\n  <reportHeader display=\"report header\">\n    <serialNumber display=\"serial number\" type=\"string\">20170102193116787101-NIPSTY-98449823</serialNumber>\n    <version display=\"version\" type=\"integer\">20161223</version>\n  </reportHeader>\n  <reportBody display=\"report body\">\n    <requestedAddress display=\"requested addresss\">\n      <fullAddress display=\"full address\" type=\"string\">714 Bartoletti Trail, Blandaside, GA 10038-3082</fullAddress>\n    </requestedAddress>\n  </reportBody>\n</buildFaxReport>\n"

            stub_request(:get, url).to_return(:status => 200, :body => body)
                response = Buildfax::Request.new.download_report(address)
                # expect the response and the body to be equal
            expect(response).to eq(body)
        end
        it "returns 403 response" do
            # get the binder address
            address = "#{@binder.property.address1}, #{@binder.property.city}, #{@binder.property.state} #{@binder.property.zip}"
            url = "https://delivery.buildfax.com/api/reports/addresses/#{URI.encode(address)}/?output=PDF&template=BuildFax%20Permit%20Timeline%20Report"

            stub_request(:get, url).to_return(:status => 403, :body => "error")

            # expect method to raise an error
            expect{Buildfax::Request.new.download_report(address)}.to raise_error BadRequestException
        end
    end
  
end