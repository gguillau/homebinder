require 'rails_helper'

RSpec.describe Buildfax::Client do
  
    before :each do
        @user = create(:user)
        @no_access = create(:user)
        @binder = create(:binder)
        create(:subscription, binder_id: @binder.id, plan_id: "free")
        @user.add_role(:owner, @binder)
        allow(ErrorService).to receive(:perform_async)
    end
    
    it "gets all permits for a binder" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        # get the binder address
        address = "#{@binder.property.address1}, #{@binder.property.city}, #{@binder.property.state} #{@binder.property.zip}"
        url = "https://delivery.buildfax.com/api/reports/addresses/%s/?output=xml&template=HomeBinder_Dev" % address
        body = "<?xml version='1.0' encoding='UTF-8'?>\n<buildFaxReport display=\"BuildFax report\">\n  <reportHeader display=\"report header\">\n    <serialNumber display=\"serial number\" type=\"string\">20170102193116787101-NIPSTY-98449823</serialNumber>\n    <version display=\"version\" type=\"integer\">20161223</version>\n  </reportHeader>\n  <reportBody display=\"report body\">\n    <requestedAddress display=\"requested addresss\">\n      <fullAddress display=\"full address\" type=\"string\">714 Bartoletti Trail, Blandaside, GA 10038-3082</fullAddress>\n    </requestedAddress>\n  </reportBody>\n</buildFaxReport>\n"
        stub_request(:get, URI.escape(url)).to_return(:status => 200, :body => body)
        # get the permits
        results = Buildfax::Client.new(request, @binder.id).get_permits(@binder.id)

        expect(results[:code]).to eq(0)
        expect(results[:message]).to eq("We apologize, but we do not have electronic coverage for your area at this time.")
        expect(results[:permits].count).to eq(0)
    end
    
    it "checks binder permissions returns the binder" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        # get the binder
        binder = Buildfax::Client.new(request, @binder.id).check_permissions(@binder.id)

        expect(binder).to eq(@binder)
    end

    it "checks binder permissions raises an error" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@no_access)
        request.user = @no_access
        # get the binder
        expect{Buildfax::Client.new(request, 999999).check_permissions(999999)}.to raise_error NotFoundException
    end
    
    it "checks binder permissions raises an error" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@no_access)
        request.user = @no_access
        # get the binder
        expect{Buildfax::Client.new(request, @binder.id).check_permissions(@binder.id)}.to raise_error CanCan::AccessDenied
    end
    
    it "gets the results for a binder" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        # get the binder address
        address = "#{@binder.property.address1}, #{@binder.property.city}, #{@binder.property.state} #{@binder.property.zip}"
        url = "https://delivery.buildfax.com/api/reports/addresses/%s/?output=xml&template=HomeBinder_Dev" % address
        body = "<?xml version='1.0' encoding='UTF-8'?>\n<buildFaxReport display=\"BuildFax report\">\n  <reportHeader display=\"report header\">\n    <serialNumber display=\"serial number\" type=\"string\">20170102193116787101-NIPSTY-98449823</serialNumber>\n    <version display=\"version\" type=\"integer\">20161223</version>\n  </reportHeader>\n  <reportBody display=\"report body\">\n    <requestedAddress display=\"requested addresss\">\n      <fullAddress display=\"full address\" type=\"string\">714 Bartoletti Trail, Blandaside, GA 10038-3082</fullAddress>\n    </requestedAddress>\n  </reportBody>\n</buildFaxReport>\n"
        stub_request(:get, URI.escape(url)).to_return(:status => 200, :body => body)
        # get the results
        results = Buildfax::Client.new(request, @binder.id).send_request(address)

        expect(results[:code]).to eq(0)
        expect(results[:message]).to eq("We apologize, but we do not have electronic coverage for your area at this time.")
        expect(results[:permits].count).to eq(0)
    end
    
    it "gets the results for a binder and returns no date coverage" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        # get the binder address
        address = "#{@binder.property.address1}, #{@binder.property.city}, #{@binder.property.state} #{@binder.property.zip}"
        min = 10.years.ago
        max = Date.today
        report = {
            "buildFaxReport" => {
                "reportBody" => {
                    "jurisdiction" => {
                        "property" => {
                            "permit" => {
                                "permitNum"=> nil
                            }
                        },
                        "exclusionMinDate" => {
                            "__content__" => min
                        }, 
                        "exclusionMaxDate" => {
                            "__content__" => max
                        }
                    }
                }
            }
        }
        allow_any_instance_of(Buildfax::Request).to receive(:send_request).and_return(report)
        message = Buildfax::Client::NO_DATE_COVERAGE % [Buildfax::Client::DATES % [min.strftime("%A %B %d, %Y"), max.strftime("%A %B %d, %Y")]]
        # get the results
        results = Buildfax::Client.new(request, @binder.id).send_request(address)

        expect(results[:code]).to eq(1)
        expect(results[:message]).to eq(message)
        expect(results[:permits].count).to eq(0)
    end
    
    it "gets the results for a binder and returns no date coverage" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        # get the binder address
        address = "#{@binder.property.address1}, #{@binder.property.city}, #{@binder.property.state} #{@binder.property.zip}"
        min = 10.years.ago
        max = Date.today
        report = {
            "buildFaxReport" => {
                "reportBody" => {
                    "jurisdiction" => {
                        "property" => {
                            "permit" => {
                                "permitNum"=>{"__content__"=>"0100000916", "display"=>"permit number"}
                            }
                        },
                        "exclusionMinDate" => {
                            "__content__" => min
                        }, 
                        "exclusionMaxDate" => {
                            "__content__" => max
                        }
                    }
                }
            }
        }
        allow_any_instance_of(Buildfax::Request).to receive(:send_request).and_return(report)
        message = Buildfax::Client::COVERAGE_EXIST % [Buildfax::Client::DATES % [min.strftime("%A %B %d, %Y"), max.strftime("%A %B %d, %Y")]]
        # get the results
        results = Buildfax::Client.new(request, @binder.id).send_request(address)

        expect(results[:code]).to eq(2)
        expect(results[:message]).to eq(message)
        expect(results[:permits].count).to eq(1)
    end
    
    it "gets the results for a binder and returns coverage exists" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        # get the binder address
        address = "#{@binder.property.address1}, #{@binder.property.city}, #{@binder.property.state} #{@binder.property.zip}"
        min = 10.years.ago
        max = Date.today
        report = {
            "buildFaxReport" => {
                "reportBody" => {
                    "jurisdiction" => {
                        "property" => [
                            "permit" => {
                                "permitNum"=>{"__content__"=>"0100000916", "display"=>"permit number"}
                            }
                        ],
                        "exclusionMinDate" => {
                            "__content__" => min
                        }, 
                        "exclusionMaxDate" => {
                            "__content__" => max
                        }
                    }
                }
            }
        }
        allow_any_instance_of(Buildfax::Request).to receive(:send_request).and_return(report)
        message = Buildfax::Client::COVERAGE_EXIST % [Buildfax::Client::DATES % [min.strftime("%A %B %d, %Y"), max.strftime("%A %B %d, %Y")]]
        # get the results
        results = Buildfax::Client.new(request, @binder.id).send_request(address)

        expect(results[:code]).to eq(2)
        expect(results[:message]).to eq(message)
        expect(results[:permits].count).to eq(1)
    end
    
    it "gets the coverage dates for a binder" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        min = 10.years.ago
        max = Date.today
        jurisdiction = {"exclusionMinDate" => {"__content__" => min}, "exclusionMaxDate" => {"__content__" => max}}
        # get the results
        dates = Buildfax::Client.new(request, @binder.id).get_coverage_dates(jurisdiction)

        expect(dates).to eq("#{min.strftime("%A %B %d, %Y")} - #{max.strftime("%A %B %d, %Y")}")
    end
    
    it "gets the permits from an array and nested hash" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        report = {
            "buildFaxReport" => {
                "reportBody" => {
                    "jurisdiction" => {
                        "property" => [
                            "permit" => {
                                "permitNum"=>{"__content__"=>"0100000916", "display"=>"permit number"}
                            }
                        ]
                    }
                }
            }
        }

        # get the results
        permits = Buildfax::Client.new(request, @binder.id).parse_array(report)

        expect(permits.length).to eq(1)
        expect(permits.first["permitNum"]["__content__"]).to eq("0100000916")
    end
    
    it "gets the permits from a hash and returns an empty array" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        report = {
            "buildFaxReport" => {
                "reportBody" => {
                    "jurisdiction" => {
                        "property" => {
                            "permit" => []
                        }
                    }
                }
            }
        }

        # get the results
        permits = Buildfax::Client.new(request, @binder.id).parse_hash(report)

        expect(permits.length).to eq(0)
    end
    
    it "gets the permits from a hash and returns an array with one permit" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        report = {
            "buildFaxReport" => {
                "reportBody" => {
                    "jurisdiction" => {
                        "property" => {
                            "permit" => {
                                "permitNum"=>{"__content__"=>"0100000916", "display"=>"permit number"}
                            }
                        }
                    }
                }
            }
        }

        # get the results
        permits = Buildfax::Client.new(request, @binder.id).parse_hash(report)

        expect(permits.length).to eq(1)
        expect(permits.first["permitNum"]["__content__"]).to eq("0100000916")
    end
    
    it "gets the permits from a hash and returns an array of permits" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        report = {
            "buildFaxReport" => {
                "reportBody" => {
                    "jurisdiction" => {
                        "property" => {
                            "permit" => [{
                                "permitNum"=>{"__content__"=>"0100000916", "display"=>"permit number"}
                            }]
                        }
                    }
                }
            }
        }

        # get the results
        permits = Buildfax::Client.new(request, @binder.id).parse_hash(report)

        expect(permits.length).to eq(1)
        expect(permits.first["permitNum"]["__content__"]).to eq("0100000916")
    end
    
    it "gets the permits from a hash and returns an array of permits" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        object = {
            "permit" => [{
                "permitNum"=>{"__content__"=>"0100000916", "display"=>"permit number"}
            }]
        }

        # get the results
        permits = Buildfax::Client.new(request, @binder.id).find_permits(object)

        expect(permits.length).to eq(1)
        expect(permits.first["permitNum"]["__content__"]).to eq("0100000916")
    end
    
    it "creates the permits" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        permits = [
            ActionController::Parameters.new({:permit_number => "PN0539324", :binder_id => @binder.id, :created_by => @user.id, :contractors => ["General Contractor"]})
        ]
        parameters = {
            :binderId => @binder.id,
            :permits => permits
        }

        # get the results
        permits = Buildfax::Client.new(request, @binder.id).create_permits(parameters)
        @binder.reload
        contractors = @binder.binder_contractors
        expect(permits.length).to eq(1)
        expect(permits.first.permit_number).to eq("PN0539324")
        expect(contractors.first.contact).to eq("General Contractor")
    end
    
    it "creates the permit" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        permits = [
            ActionController::Parameters.new({:permit_number => "PN0539324", :binder_id => @binder.id, :created_by => @user.id, :contractors => ["General Contractor"]})
        ]
        allow_any_instance_of(Binder::Permit).to receive(:save).and_return(false)

        # get the results
        Buildfax::Client.new(request, @binder.id).create_permit(permits.first)
        expect(ErrorService).to have_received(:perform_async)
    end
    
    it "does not create the permit" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        permits = [
            ActionController::Parameters.new({:permit_number => "PN0539324", :binder_id => @binder.id, :created_by => @user.id, :contractors => ["General Contractor"]})
        ]

        # get the results
        permit = Buildfax::Client.new(request, @binder.id).create_permit(permits.first)
        @binder.reload
        contractors = @binder.binder_contractors
        expect(permit.permit_number).to eq("PN0539324")
        expect(contractors.first.contact).to eq("General Contractor")
        expect(@binder.permits.length).to eq(1)
    end
    
    it "doesn't create a duplicate contractor and returns an existing id" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        
        bc = create(:binder_contractor, :created_by => @user.id, :binder_id => @binder.id)
        binder_contractor = Binder::BinderContractor.find(bc.id)
        
        # get the id
        id = Buildfax::Client.new(request, @binder.id).create_contractor(binder_contractor.contact)

        expect(id).to eq(binder_contractor.id)
    end
    
    it "creates a new contractor" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        # get the id
        id = Buildfax::Client.new(request, @binder.id).create_contractor("General Contractor")
        contractor = Binder::BinderContractor.find(id)
        expect(contractor.contact).to eq("General Contractor")
    end
    
    it "doesn't save the contractor name because it's too long" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        # get the id
        id = Buildfax::Client.new(request, @binder.id).create_contractor(Faker::Lorem.characters(256))

        expect(id).to eq(nil)
        expect(ErrorService).to have_received(:perform_async)
    end
    
    it "gets the contractor name from the nested hash" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        permit = {"contractor" => {"fullName" => { "__content__" => "General Contractor"}}}
        # get the value
        name = Buildfax::Client.new(request, @binder.id).get_contractor(permit, "contractor")

        expect(name).to eq(["General Contractor"])
    end
    
    it "gets the contractor name from the nested array" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        permit = {"contractor" => [{"fullName" => { "__content__" => "General Contractor"}}] }
        # get the value
        name = Buildfax::Client.new(request, @binder.id).get_contractor(permit, "contractor")

        expect(name).to eq(["General Contractor"])
    end
    
    it "gets the contractor name from the nested array" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        permit = {"contractor" => [{"companyName" => { "__content__" => "General Contractor"}}] }
        # get the value
        name = Buildfax::Client.new(request, @binder.id).get_contractor(permit, "contractor")

        expect(name).to eq(["General Contractor"])
    end
    
    it "gets the contractor name from the nested hash" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        permit = {"contractor"=>{"companyName"=>{"__content__"=>"General Contractor", "display"=>"company name"}, "display"=>"contractor", "fieldset"=>"contractor"} }
        # get the value
        name = Buildfax::Client.new(request, @binder.id).get_contractor(permit, "contractor")

        expect(name).to eq(["General Contractor"])
    end
    
    it "returns true" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        permits = [
            ActionController::Parameters.new({:permit_number => "PN0539324", :binder_id => @binder.id, :created_by => @user.id})
        ]

        # get the value
        boolean = Buildfax::Client.new(request, @binder.id).can_create(permits.first)

        expect(boolean).to eq(true)
    end
    
    it "returns false" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        create(:permit, :permit_number => "PN0539324", :binder_id => @binder.id, :created_by => @user.id)
        permits = [
            ActionController::Parameters.new({:permit_number => "PN0539324", :binder_id => @binder.id, :created_by => @user.id})
        ]

        # get the value
        boolean = Buildfax::Client.new(request, @binder.id).can_create(permits.first)

        expect(boolean).to eq(false)
    end
    
    it "returns a formatted hash of values for a permit with a nested contractors array" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        permit = {
            "permitNum"=>
                {"__content__"=>"0100000916", "display"=>"permit number"},
            "proposedUse"=>
                {"__content__"=>"use", "display"=>"permit use"},
            "workClass"=>
                {"__content__"=>"class", "display"=>"permit class"},
            "permitTypePreferred"=>
                {"__content__"=>"type", "display"=>"permit type"},
            "contractor"=> [
                {"fullName"=>{"__content__"=>"FULCHER TONY C", "display"=>"full name"}
                }
            ]
            }
        # get the permit
        permit = Buildfax::Client.new(request, @binder.id).parse_permit(permit)

        expect(permit[:permit_number]).to eq("0100000916")
        expect(permit[:proposed_use]).to eq("use")
        expect(permit[:work_class]).to eq("class")
        expect(permit[:permit_type]).to eq("type")
        expect(permit[:status]).to eq("Not Available")
        expect(permit[:valuation_amount]).to eq("Not Available")
        expect(permit[:details]).to eq("Not Available")
        expect(permit[:permit_date]).to eq("Not Available")
        expect(permit[:binder_id]).to eq(@binder.id)
        expect(permit[:created_by]).to eq(@user.id)
        expect(permit[:import]).to eq(true)
        expect(permit[:contractors]).to eq(["FULCHER TONY C"])
    end
    
    it "returns a formatted hash of values for a permit with a nested contractor has" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        permit = {
            "permitNum"=>
                {"__content__"=>"0100000916", "display"=>"permit number"},
            "proposedUse"=>
                {"__content__"=>"use", "display"=>"permit use"},
            "workClass"=>
                {"__content__"=>"class", "display"=>"permit class"},
            "permitTypePreferred"=>
                {"__content__"=>"type", "display"=>"permit type"},
            "contractor"=> 
                {"fullName"=>{"__content__"=>"FULCHER TONY C", "display"=>"full name"}
                }
            }
        # get the permit
        permit = Buildfax::Client.new(request, @binder.id).parse_permit(permit)

        expect(permit[:permit_number]).to eq("0100000916")
        expect(permit[:proposed_use]).to eq("use")
        expect(permit[:work_class]).to eq("class")
        expect(permit[:permit_type]).to eq("type")
        expect(permit[:status]).to eq("Not Available")
        expect(permit[:valuation_amount]).to eq("Not Available")
        expect(permit[:details]).to eq("Not Available")
        expect(permit[:permit_date]).to eq("Not Available")
        expect(permit[:binder_id]).to eq(@binder.id)
        expect(permit[:created_by]).to eq(@user.id)
        expect(permit[:import]).to eq(true)
        expect(permit[:contractors]).to eq(["FULCHER TONY C"])
    end
    
    it "returns a formatted hash of values for a permit with a nested contractor has and truncates the description" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        permit = {
            "permitNum"=>
                {"__content__"=>"0100000916", "display"=>"permit number"},
            "proposedUse"=>
                {"__content__"=>"use", "display"=>"permit use"},
            "workClass"=>
                {"__content__"=>"class", "display"=>"permit class"},
            "permitTypePreferred"=>
                {"__content__"=>"type", "display"=>"permit type"},
            "contractor"=> 
                {"fullName"=>{"__content__"=>"FULCHER TONY C", "display"=>"full name"}
                },
            "description" =>
                {"__content__" => Faker::Lorem.characters(1500)}
            }
        # get the permit
        permit = Buildfax::Client.new(request, @binder.id).parse_permit(permit)

        expect(permit[:permit_number]).to eq("0100000916")
        expect(permit[:proposed_use]).to eq("use")
        expect(permit[:work_class]).to eq("class")
        expect(permit[:permit_type]).to eq("type")
        expect(permit[:status]).to eq("Not Available")
        expect(permit[:valuation_amount]).to eq("Not Available")
        expect(permit[:permit_date]).to eq("Not Available")
        expect(permit[:details].length).to eq(1000)
        expect(permit[:binder_id]).to eq(@binder.id)
        expect(permit[:created_by]).to eq(@user.id)
        expect(permit[:import]).to eq(true)
        expect(permit[:contractors]).to eq(["FULCHER TONY C"])
    end
    
    it "returns the correct value for a permit attribute" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        permit = {
            "permitNum"=>
                {"__content__"=>"0100000916", "display"=>"permit number"},
            "proposedUse"=>
                {"__content__"=>"use", "display"=>"permit use"},
            "workClass"=>
                {"__content__"=>"class", "display"=>"permit class"},
            "permitTypePreferred"=>
                {"__content__"=>"type", "display"=>"permit type"},
            }
        # get the value
        value = Buildfax::Client.new(request, @binder.id).validate_field(permit, "permitNum")

        expect(value).to eq("0100000916")
    end
    
    it "returns nil for a permit attribute" do
        # set up the request
        request = HBRequest.new
        request.ability = Ability.new(@user)
        request.user = @user
        permit = {
            "permitNum"=>
                {"__content__"=>"0100000916", "display"=>"permit number"},
            "proposedUse"=>
                {"__content__"=>"use", "display"=>"permit use"},
            "workClass"=>
                {"__content__"=>"class", "display"=>"permit class"},
            "permitTypePreferred"=>
                {"__content__"=>"type", "display"=>"permit type"},
            }
        # get the value
        value = Buildfax::Client.new(request, @binder.id).validate_field(permit, "preferredDate")

        expect(value).to eq("Not Available")
    end
  
end