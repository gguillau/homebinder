require 'rails_helper'

RSpec.describe Typeahead do
  
  ApplianceType = Binder::Appliance::Category
  ApplianceManufacturer = Binder::Appliance::Manufacturer
  ApplianceModel = Binder::Appliance::Model
  AreaType = Binder::Area::Category
  ContractorType = Contractor::Category
  FinishMake = Binder::Finish::Make
  FinishModel = Binder::Finish::Model
  InventoryItemType = Binder::InventoryItem::Category
  MaintenanceType = Binder::MaintenanceItem::Category
  PaintType = Binder::Paint::Category
  PaintManufacturer = Binder::Paint::Manufacturer
  PaintSheen = Binder::Paint::Sheen
  ProjectType = Binder::Project::Category
  BuildingType = Binder::Structure::BuildingType
  ConstructionStyle = Binder::Structure::ConstructionStyle
  ConstructionType = Binder::Structure::ConstructionType
  HeatType = Binder::Structure::HeatType
  HeatSource = Binder::Structure::HeatSource
  
  context "when doing general typeahead" do
    
    it "adds typeahead value" do
      area = create(:area, name:"area 51", binder_id: create(:binder).id, area_type: "Outhouse", created_by: 999)
      Typeahead.add_typeahead_values area
      
      expect(AreaType.where(:name => "Outhouse", :verified => false, :created_by => 999).count).to eq(1)
    end
    
    it "ignores nil typeahead value" do
      area = create(:area, binder_id: create(:binder).id, area_type: nil, created_by: 999)
      Typeahead.add_typeahead_values area
      
      expect(AreaType.where(:name => nil, :verified => false, :created_by => 999).count).to eq(0)
    end
    
    it "ignores duplicate typeahead values" do
      AreaType.new(name: "Dog House", created_by: 999, verified: false).save
      
      area = create(:area, name: "area 51", area_type: "Dog House", created_by: 999)
      Typeahead.add_typeahead_values area
      
      expect(AreaType.where(:name => "Dog House", :verified => false, :created_by => 999).count).to eq(1)
    end
    
    it "ignore verified duplicate typeahead values" do
      AreaType.new(name: "Dog House", created_by: 0, verified: true).save
      
      area = create(:area, name: "area 51", area_type: "Dog House", created_by: 999)
      Typeahead.add_typeahead_values area
      
      expect(AreaType.where(:name => "Dog House").count).to eq(1)
    end
    
    it "updates a typeahead value" do
      AreaType.new(name: "locker", created_by: 999, verified: false).save

      area = create(:area, name: "area 51", area_type: "Locker", created_by: 999)
      Typeahead.add_typeahead_values area
      
      expect(AreaType.where(:name => "locker", :verified => false, :created_by => 999).count).to eq(0)
      expect(AreaType.where(:name => "Locker", :verified => false, :created_by => 999).count).to eq(1)
    end
    
    it "ignores verified typeahead value" do
      area = create(:area, name: "area 51", area_type: "Kitchen", created_by: 999)
      Typeahead.add_typeahead_values area
      
      expect(AreaType.where(:name => "Kitchen").count).to eq(1)
    end
    
    it "maps a typeahead value" do
      appliance = create(:appliance, name: "my toaster", model: "Boo", created_by: 999)
      Typeahead.add_typeahead_values appliance

      expect(ApplianceModel.where(:name => "Boo", :created_by => 999, :verified => false).count).to eq(1)
    end
    
    it "gets typeahead values" do
      user = create(:user)
      AreaType.new(name: "foo", verified: true, created_by: 0).save
      AreaType.new(name: "bar", verified: false, created_by: user.id).save
      AreaType.new(name: "hello", verified: false, created_by: 6).save
      
      area_types = Typeahead.get_typeahead_values(user, "area_type")
      
      expect(area_types.count).to eq(2)
      expect(area_types[0]).to eq("bar")
      expect(area_types[1]).to eq("foo")
    end
    
    it "gets searched typeahead values" do
      user = create(:user)
      AreaType.new(name: "abc", verified: true, created_by: 0).save
      AreaType.new(name: "abcd", verified: true, created_by: 0).save
      AreaType.new(name: "abcde", verified: true, created_by: 0).save
      AreaType.new(name: "abcdef", verified: false, created_by: user.id).save
      AreaType.new(name: "abcdefg", verified: false, created_by: 500).save
      
      results = Typeahead.get_typeahead_values(user, "AreaType", "xyz" )
      expect(results.count).to eq(0)
      
      results = Typeahead.get_typeahead_values(user, "area_type", "abc")
      expect(results.count).to eq(4)
      
      results = Typeahead.get_typeahead_values(user, "area_type", "abcd")
      expect(results.count).to eq(3)
      
      results = Typeahead.get_typeahead_values(user, "AreaType", "abcdef")
      expect(results.count).to eq(1)
      
      results = Typeahead.get_typeahead_values(user, "area_type", "abcde")
      expect(results.count).to eq(2)
      expect(results[0]).to eq("abcde")
      expect(results[1]).to eq("abcdef")
    end
  
  end

  context "when doing appliance typeahead" do
    it "saves appliance type" do
      appliance = create(:appliance, name: "appliance 1", binder_id: create(:binder).id, appliance_type: "Foo", created_by: 5)
      Typeahead.add_typeahead_values appliance
      
      expect(ApplianceType.where(:name => "Foo", :created_by => 5, :verified => false).count).to eq(1)
    end
    
    it "saves appliance manufacturer" do
      appliance = create(:appliance, name: "appliance 1", binder_id: create(:binder).id, manufacturer: "Foo", created_by: 5)
      Typeahead.add_typeahead_values appliance
      
      expect(ApplianceManufacturer.where(:name => "Foo", :created_by => 5, :verified => false).count).to eq(1)
    end
    
    it "saves appliance model" do
      appliance = create(:appliance, name: "appliance 1", binder_id: create(:binder).id, model: "Foo", created_by: 5)
      Typeahead.add_typeahead_values appliance
      
      expect(ApplianceModel.where(:name => "Foo", :created_by => 5, :verified => false).count).to eq(1)
    end
  end
  
  context "when doing area typeahead" do
    it "saves area type" do
      area = create(:area, name: "area 1", binder_id: create(:binder).id, area_type: "Baz", created_by: 5)
      Typeahead.add_typeahead_values area
      
      expect(AreaType.where(:name => "Baz", :created_by => 5, :verified => false).count).to eq(1)
    end
  end

  context "when doing contractor typeahead" do
    it "saves contractor type" do
      contractor = create(:contractor, name: "contractor 1", contractor_type: "Foo", created_by: 5)
      Typeahead.add_typeahead_values contractor
      
      expect(ContractorType.where(:name => "Foo", :created_by => 5, :verified => false).count).to eq(1)
    end
  end

  context "when doing finish typeahead" do
    it "saves finish make" do
      finish = create(:finish, name: "finish 1", binder_id: create(:binder).id, make: "Hello", created_by: 5)
      Typeahead.add_typeahead_values finish
      
      expect(FinishMake.where(:name => "Hello", :created_by => 5, :verified => false).count).to eq(1)
    end
    
    it "saves finish model" do
      finish = create(:finish, name: "finish 1", binder_id: create(:binder).id, model: "World", created_by: 5)
      Typeahead.add_typeahead_values finish
      
      expect(FinishModel.where(:name => "World", :created_by => 5, :verified => false).count).to eq(1)
    end
  end

  context "when doing inventory item typeahead" do
    it "saves inventory item type" do
      item = create(:inventory_item, name: "item 1", binder_id: create(:binder).id, inventory_item_type: "Foo", created_by: 5)
      Typeahead.add_typeahead_values item
      
      expect(InventoryItemType.where(:name => "Foo", :created_by => 5, :verified => false).count).to eq(1)
    end
  end

  context "when doing maintenance item typeahead" do
    it "saves maintenance type" do
      item = create(:maintenance_item, name: "item 1", binder_id: create(:binder).id, maintenance_type: "Foo", created_by: 5)
      Typeahead.add_typeahead_values item
      
      expect(MaintenanceType.where(:name => "Foo", :created_by => 5, :verified => false).count).to eq(1)
    end
  end

  context "when doing paint typeahead" do
    it "saves paint type" do
      paint = create(:paint, name: "paint 1", binder_id: create(:binder).id, paint_type: "Foo", created_by: 5)
      Typeahead.add_typeahead_values paint
      
      expect(PaintType.where(:name => "Foo", :created_by => 5, :verified => false).count).to eq(1)
    end
    
    it "saves paint manufacturer" do
      paint = create(:paint, name: "paint 1", binder_id: create(:binder).id, manufacturer: "Foo", created_by: 5)
      Typeahead.add_typeahead_values paint
      
      expect(PaintManufacturer.where(:name => "Foo", :created_by => 5, :verified => false).count).to eq(1)
    end
    
    it "saves paint sheen" do
      paint = create(:paint, name: "paint 1", binder_id: create(:binder).id, sheen: "Foo", created_by: 5)
      Typeahead.add_typeahead_values paint
      
      expect(PaintSheen.where(:name => "Foo", :created_by => 5, :verified => false).count).to eq(1)
    end
  end

  context "when doing project typeahead" do
    it "saves project type" do
      project = create(:project, name: "project 1", binder_id: create(:binder).id, project_type: "Foo", created_by: 5)
      Typeahead.add_typeahead_values project
      
      expect(ProjectType.where(:name => "Foo", :created_by => 5, :verified => false).count).to eq(1)
    end
  end

  context "when doing structure typeahead" do
    it "saves building type" do
      struct = create(:structure, name: "structure 1", binder_id: create(:binder).id, building_type: "Foo", created_by: 5)
      Typeahead.add_typeahead_values struct
      
      expect(BuildingType.where(:name => "Foo", :created_by => 5, :verified => false).count).to eq(1)
    end
    
    it "saves construction style" do
      struct = create(:structure, name: "structure 1", binder_id: create(:binder).id, construction_style: "Foo", created_by: 5)
      Typeahead.add_typeahead_values struct
      
      expect(ConstructionStyle.where(:name => "Foo", :created_by => 5, :verified => false).count).to eq(1)
    end
    
    it "saves construction type" do
      struct = create(:structure, name: "structure 1", binder_id: create(:binder).id, construction_type: "Foo", created_by: 5)
      Typeahead.add_typeahead_values struct
      
      expect(ConstructionType.where(:name => "Foo", :created_by => 5, :verified => false).count).to eq(1)
    end
    
    it "saves heat type" do
      struct = create(:structure, name: "structure 1", binder_id: create(:binder).id, heat_type: "Foo", created_by: 5)
      Typeahead.add_typeahead_values struct
      
      expect(HeatType.where(:name => "Foo", :created_by => 5, :verified => false).count).to eq(1)
    end
    
    it "saves heat source" do
      struct = create(:structure, name: "structure 1", binder_id: create(:binder).id, heat_source: "Foo", created_by: 5)
      Typeahead.add_typeahead_values struct
      
      expect(HeatSource.where(:name => "Foo", :created_by => 5, :verified => false).count).to eq(1)
    end
  end

end