require 'rails_helper'

RSpec.describe Brokermint::Request do

    describe "#call" do
        it "returns 201 response" do
            url = "http://www.test.com"
            stub_request(:get, URI.escape(url)).to_return(:status => 201, :body => "{}")
            response = Brokermint::Request.new.call(url, "test")
            
            # expect the response and the body to be equal
            expect(response).to eq({})
        end
        it "returns 403 response" do
            url = "http://www.test.com"
            stub_request(:get, URI.escape(url)).to_return(:status => 403, :body => "")

            # expect method to raise an error
            expect{Brokermint::Request.new.call(url, "test")}.to raise_error BadRequestException
        end
    end
end