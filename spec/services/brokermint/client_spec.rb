require 'rails_helper'

RSpec.describe Brokermint::Client, :type => :service do
    
    describe "get_transactions" do
        it "returns an empty array" do
            transactions = Brokermint::Client.new.get_transactions(nil)
            
            expect(transactions.length).to eq(0)
        end
        
        it "returns an array" do
            brokermint_request = Brokermint::Request.new
            allow(Brokermint::Request).to receive(:new).and_return(brokermint_request)
            allow(brokermint_request).to receive(:call).and_return([{"id" => 1}])
            
            transactions = Brokermint::Client.new.get_transactions("test")
            
            expect(transactions.length).to eq(1)
        end
    end
    
    describe "get_transaction" do
        it "returns an empty hash" do
            transaction = Brokermint::Client.new.get_transaction(nil, 1)
            
            expect(transaction.empty?).to be(true)
        end
        
        it "returns an empty hash" do
            transaction = Brokermint::Client.new.get_transaction("test", nil)
            
            expect(transaction.empty?).to be(true)
        end
        
        it "returns a hash" do
            brokermint_request = Brokermint::Request.new
            allow(Brokermint::Request).to receive(:new).and_return(brokermint_request)
            allow(brokermint_request).to receive(:call).and_return({"id" => 1})
            
            transaction = Brokermint::Client.new.get_transaction("test", 1)
            
            expect(transaction.empty?).to_not be(true)
        end
    end
    
    describe "get_contacts" do
        it "returns an empty array" do
            contacts = Brokermint::Client.new.get_contacts(nil)
            
            expect(contacts.length).to eq(0)
        end
        
        it "returns an array" do
            brokermint_request = Brokermint::Request.new
            allow(Brokermint::Request).to receive(:new).and_return(brokermint_request)
            allow(brokermint_request).to receive(:call).and_return([{"id" => 1}])
            
            contacts = Brokermint::Client.new.get_contacts("test")
            
            expect(contacts.length).to eq(1)
        end
    end
    
    describe "get_contact" do
        it "returns an empty hash" do
            contact = Brokermint::Client.new.get_contact(nil, 1)
            
            expect(contact.empty?).to be(true)
        end
        
        it "returns an empty hash" do
            contact = Brokermint::Client.new.get_contact("test", nil)
            
            expect(contact.empty?).to be(true)
        end
        
        it "returns a hash" do
            brokermint_request = Brokermint::Request.new
            allow(Brokermint::Request).to receive(:new).and_return(brokermint_request)
            allow(brokermint_request).to receive(:call).and_return({"id" => 1})
            
            contact = Brokermint::Client.new.get_contact("test", 1)
            
            expect(contact.empty?).to_not be(true)
        end
    end
    
    describe "get_transaction_participants" do
        it "returns an empty array" do
            transaction_participants = Brokermint::Client.new.get_transaction_participants(nil, 1)
            
            expect(transaction_participants.length).to eq(0)
        end
        
        it "returns an empty array" do
            transaction_participants = Brokermint::Client.new.get_transaction_participants("test", nil)
            
            expect(transaction_participants.length).to eq(0)
        end
        
        it "returns an array" do
            brokermint_request = Brokermint::Request.new
            allow(Brokermint::Request).to receive(:new).and_return(brokermint_request)
            allow(brokermint_request).to receive(:call).and_return([{"id" => 1}])
            
            transaction_participants = Brokermint::Client.new.get_transaction_participants("test", 1)
            
            expect(transaction_participants.length).to eq(1)
        end
    end
  
end