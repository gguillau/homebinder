require 'rails_helper'

RSpec.describe UserTokenService do
    
    describe "#verify_jwt_token" do
        it "verifies the user" do
            user = create(:user)
            token = UserTokenService.create_jwt user
            token_user = UserTokenService.verify_jwt_token token
                expect(token_user).to eq(user)
        end
        it "raises an error because of a decode error" do
            expect {UserTokenService.verify_jwt_token "02032013"}.to raise_error InvalidUserTokenException
        end
        it "raises an error because user doesn't exist" do
            user = create(:user)
            token = UserTokenService.create_jwt user
            user.destroy
                expect {UserTokenService.verify_jwt_token token}.to raise_error InvalidUserTokenException
        end
    end
    
    describe "#get_identity_key" do
        it "returns production key" do
            allow(Host).to receive(:path).and_return(Host::PRODUCTION_HOST)
            key = UserTokenService.get_identity_key
            expect(key).to eq(ENV["INTERCOM_PRODUCTION_IDENTITY_SECRET"])
        end
    end
end