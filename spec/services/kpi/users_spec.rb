require 'rails_helper'

class KpiUsersTest
    include Kpi::Users
end

RSpec.describe Kpi::Users, :type => :model do
    describe "#get_user_numbers" do
        it "returns a hash of the counts" do
            kpi_user_test = KpiUsersTest.new
            allow(kpi_user_test).to receive(:get_user_count).and_return(0)
            allow(kpi_user_test).to receive(:get_logins).and_return(1)
            allow(kpi_user_test).to receive(:get_no_logins).and_return(2)
            
            expect(kpi_user_test.get_user_numbers).to eq({:count => 0, :logins => 1, :nologins => 2})
        end
    end
    
    describe "#get_user_count" do
        it "returns a hash of the counts" do
            kpi_user_test = KpiUsersTest.new
            allow(kpi_user_test).to receive(:get_all_users_count).and_return(0)
            allow(kpi_user_test).to receive(:get_agents_count).and_return(1)
            allow(kpi_user_test).to receive(:get_inspectors_count).and_return(2)
            allow(kpi_user_test).to receive(:get_partner_count).and_return(3)
            
            expect(kpi_user_test.get_user_count).to eq({:all => 0, :agents => 1, :inspectors => 2, :partners => 3})
        end
    end
    
    describe "#get_all_users_count" do
        it "returns counts of users" do
            create(:user)                                       # 7.days.ago..Time.now
            create(:user, :created_at => 8.days.ago)            # 30.days.ago..Time.now
            create_list(:user, 3, :created_at => 31.days.ago)   # 90.days.ago..Time.now
            create_list(:user, 6, :created_at => 91.days.ago)   # 6.months.ago..Time.now
            create_list(:user, 12, :created_at => 7.months.ago) # 1.year.ago..Time.now
            create(:user, :created_at => 2.years.ago)           # All time
            
            expect(KpiUsersTest.new.get_all_users_count).to eq({:last_seven => 1, :last_thirty => 2, :last_ninety => 1, :half_year => 1, :year => 1, :total => 24})
        end
    end
    
    describe "#get_partner_count" do
        it "returns counts of partners" do
            create(:partner)                                       # 7.days.ago..Time.now
            create(:partner, :created_at => 8.days.ago)            # 30.days.ago..Time.now
            create_list(:partner, 3, :created_at => 31.days.ago)   # 90.days.ago..Time.now
            create_list(:partner, 6, :created_at => 91.days.ago)   # 6.months.ago..Time.now
            create_list(:partner, 12, :created_at => 7.months.ago) # 1.year.ago..Time.now
            create(:partner, :created_at => 2.years.ago)           # All time
            
            expect(KpiUsersTest.new.get_partner_count).to eq({:last_seven => 1, :last_thirty => 2, :last_ninety => 1, :half_year => 1, :year => 1, :total => 24})
        end
    end
    
    describe "#get_agents_count" do
        it "returns counts of brokers" do
            create(:partner, :partner_type => "broker")                                       # 7.days.ago..Time.now
            create(:partner, :partner_type => "broker", :created_at => 8.days.ago)            # 30.days.ago..Time.now
            create_list(:partner, 3, :partner_type => "broker", :created_at => 31.days.ago)   # 90.days.ago..Time.now
            create_list(:partner, 6, :partner_type => "broker", :created_at => 91.days.ago)   # 6.months.ago..Time.now
            create_list(:partner, 12, :partner_type => "broker", :created_at => 7.months.ago) # 1.year.ago..Time.now
            create(:partner, :partner_type => "broker", :created_at => 2.years.ago)           # All time
            
            expect(KpiUsersTest.new.get_agents_count).to eq({:last_seven => 1, :last_thirty => 2, :last_ninety => 1, :half_year => 1, :year => 1, :total => 24})
        end
    end
    
    describe "#get_inspectors_count" do
        it "returns counts of inspectors" do
            create(:partner, :partner_type => "inspector")                                       # 7.days.ago..Time.now
            create(:partner, :partner_type => "inspector", :created_at => 8.days.ago)            # 30.days.ago..Time.now
            create_list(:partner, 3, :partner_type => "inspector", :created_at => 31.days.ago)   # 90.days.ago..Time.now
            create_list(:partner, 6, :partner_type => "inspector", :created_at => 91.days.ago)   # 6.months.ago..Time.now
            create_list(:partner, 12, :partner_type => "inspector", :created_at => 7.months.ago) # 1.year.ago..Time.now
            create(:partner, :partner_type => "inspector", :created_at => 2.years.ago)           # All time
            
            expect(KpiUsersTest.new.get_inspectors_count).to eq({:last_seven => 1, :last_thirty => 2, :last_ninety => 1, :half_year => 1, :year => 1, :total => 24})
        end
    end
    
    describe "#get_logins" do
        it "returns a hash of the counts" do
            kpi_user_test = KpiUsersTest.new
            allow(kpi_user_test).to receive(:get_all_logins).and_return(0)
            allow(kpi_user_test).to receive(:get_organic_logins).and_return(1)
            allow(kpi_user_test).to receive(:get_inspectors_logins).and_return(2)
            allow(kpi_user_test).to receive(:get_agents_logins).and_return(3)
            allow(kpi_user_test).to receive(:get_partners_logins).and_return(4)
            
            expect(kpi_user_test.get_logins).to eq({:all => 0, :organic => 1, :inspectors => 2, :agents => 3, :partners => 4})
        end
    end
    
    describe "#get_no_logins" do
        it "returns number of users that haven't logged in" do
            create(:user, :role => "admin", :last_sign_in_at => nil)
            create(:user, :role => "homeowner", :last_sign_in_at => nil)
            create(:user, :role => "inspector", :last_sign_in_at => nil)
            create(:user, :role => "broker", :last_sign_in_at => nil)
            create(:user, :role => "hoa", :last_sign_in_at => nil)
            
            expect(KpiUsersTest.new.get_no_logins).to eq({:all => {:count => 5}, :organic => {:count => 0}, :partners => {:count => 3}, :inspectors => {:count => 1}, :agents => {:count => 1}})
        end
    end
    
    describe "#get_all_logins" do
        it "returns number of users that have logged in" do
            create(:user, :last_sign_in_at => 1.day.ago)    # 30.days.ago..Time.now
            create(:user, :last_sign_in_at => 31.days.ago)  # 60.days.ago..Time.now
            create(:user, :last_sign_in_at => 61.days.ago)  # 90.days.ago..Time.now
            create(:user, :last_sign_in_at => 91.days.ago)  # 360.days.ago..Time.now
            create(:user, :last_sign_in_at => 361.days.ago) # 1.year.ago..Time.now
            create(:user, :last_sign_in_at => 2.years.ago)  # All time
            
            expect(KpiUsersTest.new.get_all_logins).to eq({:last_thirty=>{:count=>1, :percentage=>1}, :last_sixty=>{:count=>2, :percentage=>1}, :last_ninety=>{:count=>3, :percentage=>1}, :half_year=>{:count=>4, :percentage=>1}, :year=>{:count=>5, :percentage=>1}, :total=>{:count=>6, :percentage=>1}})
        end
    end
    
    describe "#get_organic_logins" do
        it "returns number of registered homeowners that have logged in" do
            create(:user, :role => "homeowner", :create_method => "register", :last_sign_in_at => 1.day.ago)    # 30.days.ago..Time.now
            create(:user, :role => "homeowner", :create_method => "register", :last_sign_in_at => 31.days.ago)  # 60.days.ago..Time.now
            create(:user, :role => "homeowner", :create_method => "register", :last_sign_in_at => 61.days.ago)  # 90.days.ago..Time.now
            create(:user, :role => "homeowner", :create_method => "register", :last_sign_in_at => 91.days.ago)  # 6.months.ago..Time.now
            create(:user, :role => "homeowner", :create_method => "register", :last_sign_in_at => 7.months.ago) # 1.year.ago..Time.now
            create(:user, :role => "homeowner", :create_method => "register", :last_sign_in_at => 2.years.ago)  # All time
            
            expect(KpiUsersTest.new.get_organic_logins).to eq({:last_thirty=>{:count=>1, :percentage=>Float::INFINITY}, :last_sixty=>{:count=>2, :percentage=>Float::INFINITY}, :last_ninety=>{:count=>3, :percentage=>Float::INFINITY}, :half_year=>{:count=>4, :percentage=>Float::INFINITY}, :year=>{:count=>5, :percentage=>Float::INFINITY}, :total=>{:count=>6, :percentage=>Float::INFINITY}})
        end
    end
    
    describe "#get_organic_nologin" do
        it "returns number of registered homeowners that have not logged in" do
            create_list(:user, 2, :role => "homeowner", :create_method => "register", :last_sign_in_at => 1.day.ago)
            
            expect(KpiUsersTest.new.get_organic_nologin).to eq({:count => 2})
        end
    end
    
    describe "#get_inspectors_logins" do
        it "returns number of inspectors that have logged in" do
            create(:user, :role => "inspector", :last_sign_in_at => 1.day.ago)    # 30.days.ago..Time.now
            create(:user, :role => "inspector", :last_sign_in_at => 31.days.ago)  # 60.days.ago..Time.now
            create(:user, :role => "inspector", :last_sign_in_at => 61.days.ago)  # 90.days.ago..Time.now
            create(:user, :role => "inspector", :last_sign_in_at => 91.days.ago)  # 6.months.ago..Time.now
            create(:user, :role => "inspector", :last_sign_in_at => 7.months.ago) # 1.year.ago..Time.now
            create(:user, :role => "inspector", :last_sign_in_at => 2.years.ago)  # All time
            
            expect(KpiUsersTest.new.get_inspectors_logins).to eq({:last_thirty => 1.0/6.0, :last_sixty => 2.0/6.0, :last_ninety => 3.0/6.0, :half_year => 4.0/6.0, :year => 5.0/6.0})
        end
    end
    
    describe "#get_agents_logins" do
        it "returns number of inspectors that have logged in" do
            create(:user, :role => "broker", :last_sign_in_at => 1.day.ago)    # 30.days.ago..Time.now
            create(:user, :role => "broker", :last_sign_in_at => 31.days.ago)  # 60.days.ago..Time.now
            create(:user, :role => "broker", :last_sign_in_at => 61.days.ago)  # 90.days.ago..Time.now
            create(:user, :role => "broker", :last_sign_in_at => 91.days.ago)  # 6.months.ago..Time.now
            create(:user, :role => "broker", :last_sign_in_at => 7.months.ago) # 1.year.ago..Time.now
            create(:user, :role => "broker", :last_sign_in_at => 2.years.ago)  # All time
            
            expect(KpiUsersTest.new.get_agents_logins).to eq({:last_thirty => 1.0/6.0, :last_sixty => 2.0/6.0, :last_ninety => 3.0/6.0, :half_year => 4.0/6.0, :year => 5.0/6.0})
        end
    end
    
    describe "#get_partners_logins" do
        it "returns number of agents that have logged in" do
            create(:user, :role => "admin", :last_sign_in_at => 1.day.ago)
            create(:user, :role => "homeowner", :last_sign_in_at => 1.day.ago)
            create(:user, :role => "broker", :last_sign_in_at => 1.day.ago)
            create(:user, :role => "inspector", :last_sign_in_at => 1.day.ago)      # 30.days.ago..Time.now
            create(:user, :role => "hoa", :last_sign_in_at => 31.days.ago)          # 60.days.ago..Time.now
            create(:user, :role => "inspector", :last_sign_in_at => 61.days.ago)    # 90.days.ago..Time.now
            create(:user, :role => "hoa", :last_sign_in_at => 91.days.ago)          # 6.months.ago..Time.now
            create(:user, :role => "inspector", :last_sign_in_at => 7.months.ago)   # 1.year.ago..Time.now
            create(:user, :role => "hoa", :last_sign_in_at => 2.years.ago)          # All time
            
            expect(KpiUsersTest.new.get_partners_logins).to eq({:last_thirty => 1.0/6.0, :last_sixty => 2.0/6.0, :last_ninety => 3.0/6.0, :half_year => 4.0/6.0, :year => 5.0/6.0})
        end
    end
end