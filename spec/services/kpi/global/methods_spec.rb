require 'rails_helper'

RSpec.describe Kpi::Global::Methods, :type => :model do
    describe "#percent" do
        it "returns 0 if percentage is not a number" do
            result = Partner.percent(0.0/0.0)
            expect(result).to eq(0)
        end
        
        it "returns percentage if percentage a number" do
            result = Partner.percent(100.0)
            expect(result).to eq(100.0)
        end
    end
    
    describe "#percentage" do
        it "returns 'n/a' if the denominator is 0" do
            result = Partner.percentage(1, 0)
            expect(result).to eq("n/a")
        end
        
        it "returns 50 when passed 1 and 2" do
            result = Partner.percentage(1, 2)
            expect(result).to eq(50)
        end
        
        it "returns 33 when passed 1 and 3 (round down)" do
            result = Partner.percentage(1, 3)
            expect(result).to eq(33)
        end
        
        it "returns 67 when passed 2 and 3 (round up)" do
            result = Partner.percentage(2, 3)
            expect(result).to eq(67)
        end
    end

    describe "#year_over_year" do
        it "returns 'n/a' if this_year is not a number" do
            result = Partner.year_over_year("1", 0)
            expect(result).to eq("n/a")
        end
        
        it "returns 'n/a' if last_year is not a number" do
            result = Partner.year_over_year(1, "0")
            expect(result).to eq("n/a")
        end
        
        it "returns 'n/a' if last_year is 0" do
            result = Partner.year_over_year(1, 0)
            expect(result).to eq("n/a")
        end
        
        it "returns 79 when passed 122 and 68" do
            result = Partner.year_over_year(122, 68)
            expect(result).to eq(79)
        end
        
        it "returns -44 when passed 68 and 122" do
            result = Partner.year_over_year(68, 122)
            expect(result).to eq(-44)
        end
    end
    
    describe "#num_to_string" do
        it "returns string unchanged if passed a string" do
            result = Partner.num_to_string("string")
            expect(result).to eq("string")
        end
        
        it "returns string unchanged if passed a numeric string" do
            result = Partner.num_to_string("5")
            expect(result).to eq("5")
        end
        
        it "returns string with + in front if passed postive int" do
            result = Partner.num_to_string(5)
            expect(result).to eq("+5")
        end
        
        it "returns string with + in front if passed postive float" do
            result = Partner.num_to_string(5.5)
            expect(result).to eq("+5.5")
        end
        
        it "returns string with - in front if passed negative int" do
            result = Partner.num_to_string(-5)
            expect(result).to eq("-5")
        end
        
        it "returns string with - in front if passed negative float" do
            result = Partner.num_to_string(-5.5)
            expect(result).to eq("-5.5")
        end

    end

end