require 'rails_helper'

RSpec.describe Kpi::Binders, :type => :model do

    let(:admin) { FactoryBot.create(:user) }
    let(:homeowner) { FactoryBot.create(:user, :role => "homeowner") }
    let(:inspector_user) { FactoryBot.create(:user, :role => "inspector") }
    let(:broker_user) { FactoryBot.create(:user, :role => "broker") }
    let(:inspector) { FactoryBot.create(:partner, email: inspector_user.email, partner_type: "inspector") }
    let(:broker) { FactoryBot.create(:partner, email: broker_user.email, partner_type: "broker") }
    
    
    before :each do
        admin.add_role :admin
        inspector_user.add_role :partner_admin, inspector
        broker_user.add_role :partner_admin, broker
    end
    
    describe "#get_binder_totals" do
        context 'when not an admin' do
            it "It raises an error" do
                request = HBRequest.new.create_request(homeowner)
                expect {Kpi::Summary.new(request).get_binder_totals}.to raise_error CanCan::AccessDenied
            end
        end
        context 'when an admin' do
            it "It does not raise an error" do
                request = HBRequest.new.create_request(admin)
                expect {Kpi::Summary.new(request).get_binder_totals}.to_not raise_error
            end
        end
    end
    
    describe "#get_all_binders" do
        it "returns binders" do
            # total binders
            homeowner_binders = create_list(:binder, 5, created_at: 6.days.ago, created_by: homeowner.id)
            homeowner_binders.each {|binder| UserBinder.create(:binder_id => binder.id, :user_id => homeowner.id, :role => "owner")}
            admin_binders =  create_list(:binder, 10, created_at: 29.days.ago, created_by: admin.id)
            admin_binders.each {|binder| UserBinder.create(:binder_id => binder.id, :user_id => admin.id, :role => "owner")}
            broker_user_binders =  create_list(:binder, 15, created_at: 89.days.ago, created_by: broker_user.id)
            broker_user_binders.each {|binder| UserBinder.create(:binder_id => binder.id, :user_id => broker_user.id, :role => "owner")}
            binders =  create_list(:binder, 20, created_at: 5.months.ago, created_by: inspector_user.id)
            binders.each {|binder| UserBinder.create(:binder_id => binder.id, :user_id => inspector_user.id, :role => "owner")}
            create_list(:binder, 25, created_at: 11.months.ago, created_by: 99999)
                request = HBRequest.new.create_request(admin)
                # total binders
            summary = Kpi::Summary.new(request).get_binder_numbers
                expect(summary[:all][:last_seven][:count]).to eq(5)
            expect(summary[:all][:total][:count]).to eq(75)
            expect(summary[:homeowner][:last_seven][:count]).to eq(5)
            expect(summary[:admin][:last_thirty][:count]).to eq(10)
            expect(summary[:agents][:total][:count]).to eq(15)
            expect(summary[:inspectors][:total][:count]).to eq(20)
            expect(summary[:orphan][:total][:count]).to eq(25)
            expect(summary[:partners][:total][:count]).to eq(35)
        end
    end

end