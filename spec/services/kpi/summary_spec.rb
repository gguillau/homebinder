require 'rails_helper'

RSpec.describe Kpi::Summary, :type => :model do

    let(:admin) { FactoryBot.create(:user) }
    let(:user) { FactoryBot.create(:user) }
    
    before :each do
        admin.add_role :admin
    end
    
    describe "#query_for_users" do
        context 'when not an admin' do
            it "It raises an error" do
                non_admin = create(:user)
                request = HBRequest.new.create_request(non_admin)
                        expect {Kpi::Summary.new(request).get_user_totals}.to raise_error CanCan::AccessDenied
            end
        end
                context 'when an admin' do
            it "It does not raise an error" do
                        request = HBRequest.new.create_request(admin)
                        expect {Kpi::Summary.new(request).get_user_totals}.to_not raise_error 
            end
        end
    end
    
    describe "#get_all_users_count" do

        it "It returns users" do
                create_list(:user, 0, created_at: 6.days.ago)
            create_list(:user, 2, created_at: 29.days.ago)
            create_list(:user, 3, created_at: 89.days.ago)
            create_list(:user, 4, created_at: 5.months.ago)
            create_list(:user, 5, created_at: 11.months.ago)
                request = HBRequest.new.create_request(admin)
                users = Kpi::Summary.new(request).get_all_users_count

            expect(users[:total]).to eq(15)
        end
    end
    
    describe "#get_partner_count" do

        it "It returns partners" do
                create_list(:partner, 1, created_at: 6.days.ago)
            create_list(:partner, 2, created_at: 29.days.ago)
            create_list(:partner, 3, created_at: 89.days.ago)
            create_list(:partner, 4, created_at: 5.months.ago)
            create_list(:partner, 5, created_at: 11.months.ago)
                request = HBRequest.new.create_request(admin)
                partners = Kpi::Summary.new(request).get_partner_count

            expect(partners[:total]).to eq(15)
        end
    end
    
    describe "#get_agents_count" do

        it "It returns agents" do
                create_list(:partner, 1, created_at: 6.days.ago, partner_type: "broker")
            create_list(:partner, 2, created_at: 29.days.ago, partner_type: "inspector")
            create_list(:partner, 3, created_at: 89.days.ago, partner_type: "inspector")
            create_list(:partner, 4, created_at: 5.months.ago, partner_type: "broker")
            create_list(:partner, 5, created_at: 11.months.ago, partner_type: "broker")
                request = HBRequest.new.create_request(admin)
                agents = Kpi::Summary.new(request).get_agents_count

            expect(agents[:total]).to eq(10)
        end
    end
    
    describe "#get_inspectors_count" do

        it "It returns inspectors" do
                create_list(:partner, 1, created_at: 6.days.ago, partner_type: "broker")
            create_list(:partner, 2, created_at: 29.days.ago, partner_type: "inspector")
            create_list(:partner, 3, created_at: 89.days.ago, partner_type: "inspector")
            create_list(:partner, 4, created_at: 5.months.ago, partner_type: "broker")
            create_list(:partner, 5, created_at: 11.months.ago, partner_type: "broker")
                request = HBRequest.new.create_request(admin)
                inspectors = Kpi::Summary.new(request).get_inspectors_count

            expect(inspectors[:total]).to eq(5)
        end
    end
end