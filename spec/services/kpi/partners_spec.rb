require 'rails_helper'

RSpec.describe Partner, :type => :model do
    describe "#get_weekly_metrics" do
        context 'when partner has no associated user' do
            it "It raises an error" do
                partner = create(:partner)
                expect {Partner.get_weekly_metrics(partner.id)}.to raise_error BadRequestException
            end
        end
        context 'when partner has a user' do
            it "It returns 10 total binders" do
                user = create(:user, role: "inspector")
                partner = create(:partner, :partner_type => "inspector")
                create_list(:binder, 10, created_by: user.id)
                user.add_role :partner_admin, partner
                metrics = Partner.get_weekly_metrics(partner.id)
                expect(metrics[:total_binders]).to eq(10)
            end
            it "It returns 6 binders created within last 7 days" do
                user = create(:user, role: "inspector")
                partner = create(:partner, :partner_type => "inspector")
                create_list(:binder, 6, created_by: user.id, created_at: 5.days.ago)
                user.add_role :partner_admin, partner
                metrics = Partner.get_weekly_metrics(partner.id)
                expect(metrics[:total_binders_past_week]).to eq(6)
            end
            it "It returns the correct user" do
                user = create(:user, role: "inspector")
                partner = create(:partner, :partner_type => "inspector")
                create_list(:binder, 6, created_by: user.id, created_at: 5.days.ago)
                user.add_role :partner_admin, partner
                metrics = Partner.get_weekly_metrics(partner.id)
                expect(metrics[:user]).to eq(user)
            end
        end
    end

    describe "#get_biweekly_metrics" do
        it "raises an error if the partner does not exist" do
            expect {Partner.get_biweekly_metrics(1)}.to raise_error BadRequestException
        end
    end

    describe "#total_binders" do
        it "It returns 30 total binders" do
            user = create(:user, :role => "inspector")
            partner = create(:partner)
            user.add_role :partner_admin, partner
            user.save
            create_list(:binder, 30, created_by: user.id)

            count = Partner.total_binders(user)
            expect(count).to eq(30)
        end
    end

    describe "#total_binders_by_partner_binders" do
        it "returns 1 total binder for partner_1 and 2 total binders for partner_2" do
            partner_1 = create(:partner, :partner_type => "inspector")
            partner_2 = create(:partner, :partner_type => "inspector")

            binder_1 = create(:binder)
            partner_binder_1 = create(:partner_binder, :partner => partner_1, :binder => binder_1)

            binder_2 = create(:binder)
            partner_binder_2 = create(:partner_binder, :partner => partner_2, :binder => binder_2)

            binder_3 = create(:binder)
            partner_binder_3 = create(:partner_binder, :partner => partner_2, :binder => binder_3)

            total_binders_1 = Partner.total_binders_by_partner_binders(partner_1.id)
            total_binders_2 = Partner.total_binders_by_partner_binders(partner_2.id)

            expect(total_binders_1).to eq(1)
            expect(total_binders_2).to eq(2)
        end

        it "returns only binders within the specified time-frame" do
            partner = create(:partner, :partner_type => "inspector")

            binder_recent = create(:binder)
            partner_binder_recent = create(:partner_binder, :partner => partner, :binder => binder_recent, :created_at => Time.now)

            binder_old = create(:binder)
            partner_binder_old = create(:partner_binder, :partner => partner, :binder => binder_old, :created_at => 31.days.ago)

            total_binders = Partner.total_binders_by_partner_binders(partner.id, {:created_at => 30.days.ago..Time.now})

            expect(total_binders).to eq(1)
        end
    end

    describe "#total_binders_past_week" do
        it "It returns 10 total binders" do
            user = create(:user, :role => "inspector")
            partner = create(:partner)
            user.add_role :partner_admin, partner
            user.save
            create_list(:binder, 10, created_by: user.id, created_at: 6.days.ago)

            count = Partner.total_binders([user.id], {:created_at => 1.month.ago..Time.now})
            expect(count).to eq(10)
        end
    end

    describe "#total_binders_past_month" do
        it "It returns 6 total binders" do
            user = create(:user, :role => "inspector")
            partner = create(:partner)
            user.add_role :partner_admin, partner
            user.save
            create_list(:binder, 6, created_by: user.id, created_at: 27.days.ago)

            count = Partner.total_binders([user.id], {:created_at => 1.month.ago..Time.now})
            expect(count).to eq(6)
        end
    end

    describe "#total_binders_not_transferred" do
        it "It returns 13 total binders" do
            user = create(:user, :role => "inspector")
            partner = create(:partner)
            user.add_role :partner_admin, partner
            user.save
            create_list(:binder, 13, created_by: user.id, created_at: 100.days.ago)
            Binder.where(:created_by => user.id).each do |binder|
                user.add_role :owner, binder
            end

            count = Partner.total_binders_not_transferred(user)
            expect(count).to eq(13)
        end
    end

    describe "#maintenance_reminders_sent" do
        it "It returns 30 total events" do
            user = create(:user, :role => "inspector")
            partner = create(:partner)
            user.add_role :partner_admin, partner
            user.save
            create_list(:maintenance_event, 30, created_by: user.id, last_notify_date: 1.weeks.ago.beginning_of_week(start_day = :sunday) + 1.day)
            count = Partner.maintenance_reminders_sent([user.id], 1.weeks.ago.beginning_of_week(start_day = :sunday), 1.weeks.ago.end_of_week(start_day = :sunday))

            expect(count).to eq(30)
        end
    end

    describe "#get_maintenance_reminders" do
        it "It returns 1 total event" do
            user = create(:user, :role => "inspector")
            homeowner = create(:user, :role => "homeowner")
            binder = create(:binder)
            homeowner.add_role :owner, binder
            partner = create(:partner)
            user.add_role :partner_admin, partner
            user.save
            item = create(:maintenance_item, binder_id: binder.id)
            create(:maintenance_event, maintenance_item_id: item.id, created_by: user.id, last_notify_date: 1.weeks.ago.beginning_of_week(start_day = :sunday) + 1.day)
            list = Partner.get_maintenance_reminders(partner.id)
            expect(list.first.attributes.length).to eq(7)
        end
    end

    describe "#find_user" do
        it "It returns the correct user for the partner admin role" do
            user = create(:user, :role => "inspector")
            partner = create(:partner)
            user.add_role :partner_admin, partner
            user.save
            partner_user = Partner.find_user(partner.id)
            expect(partner_user).to eq(user)
        end
        it "It returns the correct user with the partner member role" do
            user = create(:user, :role => "inspector")
            partner = create(:partner, :partner_type => "inspector")
            user.add_role :partner_admin, partner
            user.save
            partner_user = Partner.find_user(partner.id)
            expect(partner_user).to eq(user)
        end
    end

    describe "#get_homeowner_acceptance_rate" do
        it "returns total rate up to present, rounded up" do
            partner = create(:partner, :partner_type => "inspector")

            client_1 = create(:user, :sign_in_count => 0)
            create(:partner_client, :partner_id => partner.id, :client_id => client_1.id)

            client_2 = create(:user, :sign_in_count => 1)
            create(:partner_client, :partner_id => partner.id, :client_id => client_2.id)

            client_3 = create(:user, :sign_in_count => 1)
            create(:partner_client, :partner_id => partner.id, :client_id => client_3.id)
            
            created_before = Time.now

            allow(PartnerClient).to receive(:index).with({}, {searchType: 'aggregation', query: 'partner_id: ' + partner.id.to_s, lte: created_before.strftime("%m/%d/%Y"), range: 'created_at', search: true}).and_return(3)
            allow(PartnerClient).to receive(:index).with({}, {searchType: 'aggregation', query: 'client.sign_in_count:>0 AND partner_id: ' + partner.id.to_s, lte: created_before.strftime("%m/%d/%Y"), range: 'created_at', search: true}).and_return(2)
            
            acceptance_rate = Partner.get_homeowner_acceptance_rate(partner.id, created_before)

            expect(acceptance_rate).to eq(67)
        end

        it "returns total rate up to present, rounded down" do
            partner = create(:partner, :partner_type => "inspector")

            client_1 = create(:user, :sign_in_count => 0)
            create(:partner_client, :partner_id => partner.id, :client_id => client_1.id)

            client_2 = create(:user, :sign_in_count => 0)
            create(:partner_client, :partner_id => partner.id, :client_id => client_2.id)

            client_3 = create(:user, :sign_in_count => 1)
            create(:partner_client, :partner_id => partner.id, :client_id => client_3.id)
            
            created_before = Time.now

            allow(PartnerClient).to receive(:index).with({}, {searchType: 'aggregation', query: 'partner_id: ' + partner.id.to_s, lte: created_before.strftime("%m/%d/%Y"), range: 'created_at', search: true}).and_return(3)
            allow(PartnerClient).to receive(:index).with({}, {searchType: 'aggregation', query: 'client.sign_in_count:>0 AND partner_id: ' + partner.id.to_s, lte: created_before.strftime("%m/%d/%Y"), range: 'created_at', search: true}).and_return(1)
            
            acceptance_rate = Partner.get_homeowner_acceptance_rate(partner.id, created_before)

            expect(acceptance_rate).to eq(33)
        end

        it "returns 0 if there are no partner clients" do
            partner = create(:partner, :partner_type => "inspector")
            created_before = Time.now

            allow(PartnerClient).to receive(:index).with({}, {searchType: 'aggregation', query: 'partner_id: ' + partner.id.to_s, lte: created_before.strftime("%m/%d/%Y"), range: 'created_at', search: true}).and_return(0)
            allow(PartnerClient).to receive(:index).with({}, {searchType: 'aggregation', query: 'client.sign_in_count:>0 AND partner_id: ' + partner.id.to_s, lte: created_before.strftime("%m/%d/%Y"), range: 'created_at', search: true}).and_return(0)
            
            acceptance_rate = Partner.get_homeowner_acceptance_rate(partner.id, created_before)
            expect(acceptance_rate).to eq(0)
        end

        it "returns total rate from 30 days ago" do
            partner = create(:partner, :partner_type => "inspector")

            client_1 = create(:user, :sign_in_count => 0)
            create(:partner_client, :partner_id => partner.id, :client_id => client_1.id, :created_at => Time.now)

            client_2 = create(:user, :sign_in_count => 0)
            create(:partner_client, :partner_id => partner.id, :client_id => client_2.id, :created_at => 31.days.ago)

            client_3 = create(:user, :sign_in_count => 1)
            create(:partner_client, :partner_id => partner.id, :client_id => client_3.id, :created_at => 31.days.ago)
            
            created_before = 30.days.ago

            allow(PartnerClient).to receive(:index).with({}, {searchType: 'aggregation', query: 'partner_id: ' + partner.id.to_s, lte: created_before.strftime("%m/%d/%Y"), range: 'created_at', search: true}).and_return(2)
            allow(PartnerClient).to receive(:index).with({}, {searchType: 'aggregation', query: 'client.sign_in_count:>0 AND partner_id: ' + partner.id.to_s, lte: created_before.strftime("%m/%d/%Y"), range: 'created_at', search: true}).and_return(1)
            
            
            acceptance_rate = Partner.get_homeowner_acceptance_rate(partner.id, created_before)

            expect(acceptance_rate).to eq(50)
        end
    end

    describe "#get_agent_analytics" do
        it "returns the agent analytics" do
            partner = create(:partner, :partner_type => "inspector")
            partner_binder1 = create(:partner_binder, :partner_id => partner.id)
            partner_binder2 = create(:partner_binder, :partner_id => partner.id)
            partner_binder3 = create(:partner_binder, :partner_id => partner.id)
            
            agent1 = create(:user, :role => "agent", :sign_in_count => 1)
            create(:partner_user, :partner_id => partner.id, :user_id => agent1.id, :role => "agent")
            create(:user_binder, :user_id => agent1.id, :binder_id => partner_binder1.binder_id)
            
            agent2 = create(:user, :role => "agent", :sign_in_count => 2)
            create(:partner_user, :partner_id => partner.id, :user_id => agent2.id, :role => "agent")
            create(:user_binder, :user_id => agent2.id, :binder_id => partner_binder2.binder_id)
 
            agent3 = create(:user, :role => "agent", :sign_in_count => 0, :created_at => 1.year.ago)
            create(:partner_user, :partner_id => partner.id, :user_id => agent3.id, :role => "agent")
            create(:user_binder, :user_id => agent3.id, :binder_id => partner_binder3.binder_id)
            
            agent_analytics = Partner.get_agent_analytics(partner.id)

            expect(agent_analytics[:agentLogins]).to eq(3)
            expect(agent_analytics[:agentCount]).to eq(3)
            expect(agent_analytics[:agentsThisYear]).to eq(2)
            expect(agent_analytics[:agentsLastYear]).to eq(1)
            expect(agent_analytics[:agentsYoy]).to eq("+100")
            expect(agent_analytics[:agentBindersYoy]).to eq("n/a")
        end
    end
    
    describe "#get_agent_binders" do
        it "returns the correct number of agent binders for existing agents" do
            partner = create(:partner, :partner_type => "inspector")
            partner_binder1 = create(:partner_binder, :partner_id => partner.id)
            partner_binder2 = create(:partner_binder, :partner_id => partner.id)
            partner_binder3 = create(:partner_binder, :partner_id => partner.id)
            partner_binder4 = create(:partner_binder, :partner_id => partner.id)
            
            agent1 = create(:user, :role => "agent", :sign_in_count => 1)
            create(:partner_user, :partner_id => partner.id, :user_id => agent1.id, :role => "agent")
            create(:user_binder, :user_id => agent1.id, :binder_id => partner_binder1.binder_id)
            
            agent2 = create(:user, :role => "agent", :sign_in_count => 2)
            create(:partner_user, :partner_id => partner.id, :user_id => agent2.id, :role => "agent")
            create(:user_binder, :user_id => agent2.id, :binder_id => partner_binder2.binder_id, :created_at => 1.year.ago)
            
            agent_binders_this_year, agent_binders_last_year = Partner.get_agent_binders(partner.id)
            
            expect(agent_binders_this_year).to eq(0)
            expect(agent_binders_last_year).to eq(0)
            
            create(:user_binder, :user_id => agent1.id, :binder_id => partner_binder3.binder_id)
            create(:user_binder, :user_id => agent2.id, :binder_id => partner_binder4.binder_id)
            
            agent_binders_this_year, agent_binders_last_year = Partner.get_agent_binders(partner.id)
            
            expect(agent_binders_this_year).to eq(3)
            expect(agent_binders_last_year).to eq(1)
        end
    end

    describe "#get_partner_users" do
        it "It returns the correct user id" do
            user = create(:user, :role => "inspector")
            partner = create(:partner)
            user.add_role :partner_admin, partner
            user.save
            partner_users = Partner.get_partner_users(partner.id)
            expect(partner_users.first).to eq(user.id)
        end
        it "It returns all user ids" do
            user1 = create(:user, :role => "inspector")
            partner = create(:partner)
            user1.add_role :partner_admin, partner
            user1.save
            user2 = create(:user, :role => "inspector")
            user2.add_role :member, partner
            user2.save
            user3 = create(:user, :role => "inspector")
            user3.add_role :member, partner
            user3.save
            user4 = create(:user, :role => "inspector")
            user4.add_role :member, partner
            user4.save
            user5 = create(:user, :role => "inspector")
            partner2 = create(:partner, :partner_type => "inspector")
            user5.add_role :partner_admin, partner2
            user5.save
            partner_users = Partner.get_partner_users(partner.id)
            expect(partner_users).to eq([user1.id, user2.id, user3.id, user4.id])
            expect(partner_users).to_not include(user5.id)
        end
    end

    describe "#total_with_binders_by_paid_customers" do
        it "It returns the correct # of binders created by inspectors" do
            # create the partners
            inspector_user_one = create(:user, :role => "inspector")
            inspector_partner_one = create(:partner)
            inspector_user_one.add_role :partner_admin, inspector_partner_one
            inspector_user_two = create(:user, :role => "inspector")
            inspector_partner_two = create(:partner)
            inspector_user_two.add_role :partner_admin, inspector_partner_two
            inspector_user_three = create(:user, :role => "inspector")
            inspector_partner_three = create(:partner)
            inspector_user_three.add_role :partner_admin, inspector_partner_three
            inspector_user_four = create(:user, :role => "inspector")
            inspector_partner_four = create(:partner)
            inspector_user_four.add_role :partner_admin, inspector_partner_four
            # create accounts
            create(:account, :manager_id => inspector_partner_one.id, :account_status => "active", :subscription_amount_cents => 4900)
            create(:account, :manager_id => inspector_partner_two.id, :account_status => "active", :subscription_amount_cents => 4900)
            create(:account, :manager_id => inspector_partner_three.id, :account_status => "active", :subscription_amount_cents => 4900)
            create(:account, :manager_id => inspector_partner_four.id, :account_status => "active", :subscription_amount_cents => 4900)
            # create the binders
            binder_one = create(:binder, :created_by => inspector_user_two.id, :created_at => 6.days.ago)
            inspector_user_two.add_role :owner, binder_one
            binder_two = create(:binder, :created_by => inspector_user_four.id, :created_at => 6.days.ago)
            inspector_user_four.add_role :owner, binder_two
            # create an admin
            admin = create(:user)
            admin.add_role :admin
            # set up the request
            request = HBRequest.new.create_request(admin)
            # get the percentage
            percent = Kpi::Summary.new(request).total_with_binders_by_paid_customers("inspector")
            expect(percent).to eq(0.5)
        end
    end

    describe "#homeowners_with_binders_created_by_partner_type" do
        it "It returns the correct # of homeowners with binders created by inspectors" do
            # create the partner
            inspector_user_one = create(:user, :role => "inspector")
            inspector_partner_one = create(:partner)
            inspector_user_one.add_role :partner_admin, inspector_partner_one
            # create the homeowner and make sure the user has signed in
            homeowner = create(:user, :sign_in_count => 10)
            # create the binders
            binder_one = create(:binder, :created_by => inspector_user_one.id, :created_at => 62.days.ago)
            homeowner.add_role :owner, binder_one
            # create an admin
            admin = create(:user)
            admin.add_role :admin
            # set up the request
            request = HBRequest.new.create_request(admin)
            clause = Kpi::Summary.new(request).get_clause("inspector")
            week_start = 1.weeks.ago.beginning_of_week(start_day = :sunday)
            week_end = 1.weeks.ago.end_of_week(start_day = :sunday)
            dates = [(week_start - 60.days)..week_end]
            ids = Binder.where(clause).where(created_at: dates).pluck(:id)
            # get the count
            count = Kpi::Summary.new(request).homeowners_with_binders_created_by_partner_type(ids, "inspector")
            expect(count).to eq(1)
        end
    end

    describe "#homeowners_with_binders_created_by_partner_type_dates" do
        it "It returns the correct # of homeowners who have signed in the past 60 days with binders created by inspectors" do
            # create the partner
            inspector_user_one = create(:user, :role => "inspector")
            inspector_partner_one = create(:partner)
            inspector_user_one.add_role :partner_admin, inspector_partner_one
            inspector_user_one.save
            # create the homeowner and make sure the user has signed in
            homeowner = create(:user, :sign_in_count => 10, :last_sign_in_at => 12.days.ago)
            # create the binders
            binder_one = create(:binder, :created_by => inspector_user_one.id, :created_at => 62.days.ago)
            homeowner.add_role :owner, binder_one
            # create an admin
            admin = create(:user)
            admin.add_role :admin

            # set up the request
            request = HBRequest.new.create_request(admin)
            start_day = :sunday
            week_end = 1.weeks.ago.end_of_week(start_day)
            # get the count
            count = Kpi::Summary.new(request).homeowners_with_binders_created_by_partner_type_dates(week_end, "inspector")
            expect(count).to eq(1)
        end
    end

    describe "#retention_rate" do
        it "It returns the correct retention_rate for inspectors" do
            # create the partners
            inspector_user_one = create(:user, :role => "inspector")
            inspector_partner_one = create(:partner)
            inspector_user_one.add_role :partner_admin, inspector_partner_one
            inspector_user_two = create(:user, :role => "inspector")
            inspector_partner_two = create(:partner)
            inspector_user_two.add_role :partner_admin, inspector_partner_two

            inspector_user_three = create(:user, :role => "inspector")
            inspector_partner_three = create(:partner)
            inspector_user_three.add_role :partner_admin, inspector_partner_three
            inspector_user_four = create(:user, :role => "inspector")
            inspector_partner_four = create(:partner)
            inspector_user_four.add_role :partner_admin, inspector_partner_four

            # create accounts
            create(:account, :manager_id => inspector_partner_one.id, :account_status => "active", :subscription_amount_cents => 4900)
            create(:account, :manager_id => inspector_partner_two.id, :account_status => "expired", :subscription_amount_cents => 4900)
            create(:account, :manager_id => inspector_partner_three.id, :account_status => "expired", :subscription_amount_cents => 4900)
            create(:account, :manager_id => inspector_partner_four.id, :account_status => "active", :subscription_amount_cents => 4900)
            # create an admin
            admin = create(:user)
            admin.add_role :admin
            # set up the request
            request = HBRequest.new.create_request(admin)
            # get the percentage
            percent = Kpi::Summary.new(request).retention_rate("inspector")
            expect(percent).to eq(0.5)
        end
    end

    describe "#total_acv" do
        it "It returns the total_acv for all partners" do
            # create the partners
            inspector_user_one = create(:user, :role => "inspector")
            inspector_partner_one = create(:partner)
            inspector_user_one.add_role :partner_admin, inspector_partner_one

            inspector_user_two = create(:user, :role => "inspector")
            inspector_partner_two = create(:partner)
            inspector_user_two.add_role :partner_admin, inspector_partner_two

            inspector_user_three = create(:user, :role => "inspector")
            inspector_partner_three = create(:partner)
            inspector_user_three.add_role :partner_admin, inspector_partner_three

            inspector_user_four = create(:user, :role => "inspector")
            inspector_partner_four = create(:partner)
            inspector_user_four.add_role :partner_admin, inspector_partner_four
            # create accounts
            create(:account, :manager_id => inspector_partner_one.id, :account_status => "active", :subscription_amount_cents => 4900, :billing_frequency => "monthly")
            create(:account, :manager_id => inspector_partner_two.id, :account_status => "expired", :subscription_amount_cents => 4900, :billing_frequency => "monthly")
            create(:account, :manager_id => inspector_partner_three.id, :account_status => "expired", :subscription_amount_cents => 4900, :billing_frequency => "monthly")
            create(:account, :manager_id => inspector_partner_four.id, :account_status => "active", :subscription_amount_cents => 4900, :billing_frequency => "monthly")
            # create an admin
            admin = create(:user)
            admin.add_role :admin
            # set up the request
            request = HBRequest.new.create_request(admin)
            # get the percentage
            acv = Kpi::Summary.new(request).total_acv
            expect(acv).to eq(1176)
        end
    end
end
