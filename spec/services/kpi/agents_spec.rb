require 'rails_helper'

RSpec.describe Kpi::Agents, :type => :model do
    describe "#get_monthly_metrics" do
        it "returns agent's metrics" do
            kpi_agents = Kpi::Agents.new
            allow(Kpi::Agents).to receive(:new).and_return(kpi_agents)
            allow(kpi_agents).to receive(:get_referring_partners).and_return(1)
            allow(kpi_agents).to receive(:get_maintenance_emails_sent).and_return(1)
            allow(kpi_agents).to receive(:get_annual_maintenance_emails).and_return(1)
            
            agent = create(:user, :role => "agent")
            binder = create(:binder)
            create(:user_binder, :binder => binder, :user => agent, :role => "buyer_agent")
            
            metrics = Kpi::Agents.new.get_monthly_metrics(agent.id)
        
            expect(metrics).to eq({referring_partners: 1, total_binders: 1, maintenance_emails_sent: 1, annual_maintenance_emails: 1})
        end
    end
    
    describe "#get_referring_partners" do
        it "returns the referring partner" do
            agent1 = create(:user, :role => "agent")
            agent2 = create(:user, :role => "agent")
            
            # partner1 is a referring partner because the partner_user is for agent1 and the role is agent
            partner1 = create(:partner)
            create(:partner_user, :partner => partner1, :user => agent1, :role => "agent")
            
            # partner2 is not a referring partner because the partner_user is for agent2
            partner2 = create(:partner)
            create(:partner_user, :partner => partner2, :user => agent2, :role => "agent")
            
            # partner3 is not a referring partner because the partner_user role is for a homeowner
            partner3 = create(:partner)
            create(:partner_user, :partner => partner3, :user => agent1, :role => "homeowner")
        
            referring_partners = Kpi::Agents.new.get_referring_partners(agent1.id)
        
            expect(referring_partners).to eq([partner1])
        end
    end
    
    describe "#get_maintenance_emails_sent" do
        it "returns the number of all time maintenance emails sent" do
            allow(Event).to receive(:index).and_return(1)
            binder = create(:binder)
            maintenance_emails_sent = Kpi::Agents.new.get_maintenance_emails_sent([binder])
            expect(maintenance_emails_sent).to eq(1)
        end
    end
    
    describe "#get_annual_maintenance_emails" do
        it "returns the number of maintenance emails that will be sent this year" do
            binder = create(:binder)
            create(:maintenance_item, :binder => binder, :maintenance_cycle => "Years", :interval => 1)
            create(:maintenance_item, :binder => binder, :maintenance_cycle => "Months", :interval => 1)
            create(:maintenance_item, :binder => binder, :maintenance_cycle => "Weeks", :interval => 1)
            create(:maintenance_item, :binder => binder, :maintenance_cycle => "Days", :interval => 1)
            
            maintenance_emails_sent = Kpi::Agents.new.get_annual_maintenance_emails([binder])
            expect(maintenance_emails_sent).to eq(430)
        end
        
        it "returns the number of maintenance emails that will be sent this year with proper rounding" do
            binder = create(:binder)
            create(:maintenance_item, :binder => binder, :maintenance_cycle => "Years", :interval => 5)
            create(:maintenance_item, :binder => binder, :maintenance_cycle => "Months", :interval => 4)
            create(:maintenance_item, :binder => binder, :maintenance_cycle => "Weeks", :interval => 3)
            create(:maintenance_item, :binder => binder, :maintenance_cycle => "Days", :interval => 2)
            
            maintenance_emails_sent = Kpi::Agents.new.get_annual_maintenance_emails([binder])
            expect(maintenance_emails_sent).to eq(203)
        end
    end
end