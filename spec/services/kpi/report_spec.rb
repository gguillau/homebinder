require 'rails_helper'
require 'cancancan'

RSpec.describe Kpi::Report, :type => :model do

    let(:admin) { FactoryBot.create(:user) }
    let(:user) { FactoryBot.create(:user) }
    
    before :each do
        admin.add_role :admin
    end
    
    describe "#get_report" do
        context 'when not an admin' do
            it "It raises an error" do
                expect {Kpi::Report.new(user).get_report}.to raise_error ::CanCan::AccessDenied
            end
        end
        context 'when an admin' do
            it "It does not raise an error" do
                service = Kpi::Report.new(admin)
                allow(service).to receive(:add_logos)
                
                expect {service.get_report}.to_not raise_error
            end
        end
    end
end