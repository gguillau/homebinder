require 'rails_helper'

RSpec.describe Widgets::WidgetService do

    describe "remove_widget" do
        it "removes a widget from a dashboard" do
            user = create(:user)
            binder = create(:binder)
            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")
                Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
                # create a widget
            Widget.create(:name => "Third Party Services", :key => "thirdPartyServices", :description => "Allow homeowners to request services from third party companies", :category => "Binder")
                dashboard = Dashboards::DashboardLookup.new(user).for_binder({:id => binder.id})
                # confirm dashboard has no widgets
            expect(dashboard.widgets.count).to eq(4)
                # add the widget to dashboard
            feedbackWidget = Widget.find_by_name("Third Party Services")
            dashboard.widgets << feedbackWidget
            dashboard.save
                # confirm addition
            expect(dashboard.widgets.count).to eq(5)
                # remove widget
            Widgets::WidgetService.new(user).remove_widget("Third Party Services", binder)
                # confirm removal
            expect(dashboard.widgets.count).to eq(4)
                # expect Widget to still exist
            feedbackWidget = Widget.find_by_name("Third Party Services")
            expect(feedbackWidget).to_not be(nil)
        end
    end
    
    describe "add_widget" do
        it "does not add selfe" do
            user = create(:user, :role => "inspector")
            homeowner = create(:user, :role => "homeowner")
            binder = create(:binder)
            create(:transfer, :sender => user, :receiver => homeowner, :binder => binder)
            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")
                Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
                # create a widget
            Widget.create(:name => "HomeSelfe Form", :key => "homeSelfe", :description => "Allow homeowners to request services from third party companies", :category => "Binder")
                # add the widget to the homeowner dashboard
            Widgets::WidgetService.new(user).add_widget("HomeSelfe Form", binder)
                dashboard = Dashboards::DashboardLookup.new(user).for_binder({:id => binder.id})
                # confirm dashboard has 4 widgets
            expect(dashboard.name).to_not eq("Default")
            expect(dashboard.widgets.count).to eq(4)
            expect(dashboard.widgets.include?(Widget.find_by_name("HomeSelfe Form"))).to eq (false)
        end
        it "add selfe" do
            user = create(:user, :role => "homeowner")
            binder = create(:binder)
            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")
                Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
                # create a widget
            Widget.create(:name => "HomeSelfe Form", :key => "homeSelfe", :description => "Allow homeowners to request services from third party companies", :category => "Binder")
                # add the widget to the homeowner dashboard
            Widgets::WidgetService.new(user).add_widget("HomeSelfe Form", binder)
                dashboard = Dashboards::DashboardLookup.new(user).for_binder({:id => binder.id})
                # confirm dashboard has 5 widgets
            expect(dashboard.widgets.count).to eq(5)
            expect(dashboard.name).to_not eq("Default")
            expect(dashboard.name).to eq(binder.name)
            expect(dashboard.widgets.include?(Widget.find_by_name("HomeSelfe Form"))).to eq (true)
        end
    end
end