require 'rails_helper'
include Organizations

RSpec.describe Organizations::OrganizationBuilder, :type => :model do
  
  describe "add_partner" do
    it "authorizes the user" do
      user = create(:user, role: :homeowner)
      org = create(:organization)
      partner = create(:partner)
      
      expect{ OrganizationBuilder.new(user).add_partner({ id: org.id, partners: [ partner.id ] })}.to raise_exception(CanCan::AccessDenied)
    end
    
    it "verifies partner exists" do
      user = create(:user, role: :admin)
      org = create(:organization)
      partner = create(:partner)
      
      expect{ OrganizationBuilder.new(user).add_partner({ id: org.id, partners: [ partner.id, 9999999 ] })}.to raise_exception(ActiveRecord::RecordNotFound)
      expect(org.partners.count).to eq(0)
    end
    
    it "verifies partner not already added" do
      user = create(:user, role: :admin)
      org = create(:organization)
      partner = create(:partner)
      create(:organization_partner, organization_id: org.id, partner_id: partner.id)
      
      expect{ OrganizationBuilder.new(user).add_partner({ id: org.id, partners: [ partner.id ] })}.to raise_exception(BadRequestException)
    end
    
    it "as admin" do
      user = create(:user, role: :admin)
      org = create(:organization)
      partner1 = create(:partner)
      partner2 = create(:partner)
      
      OrganizationBuilder.new(user).add_partner({ id: org.id, partners: [ partner1.id, partner2.id ] })
      
      org.reload
      
      expect(org.partners.count).to eq(2)
    end
    
    it "as organization member" do
      user = create(:user, role: :inspector)
      org = create(:organization)
      create(:organization_user, organization_id: org.id, user_id: user.id)
      partner1 = create(:partner)
      partner2 = create(:partner)
      
      OrganizationBuilder.new(user).add_partner({ id: org.id, partners: [ partner1.id, partner2.id ] })
      
      org.reload
      
      expect(org.partners.count).to eq(2)
    end
    
    it "handles duplicate ids" do
      user = create(:user, role: :admin)
      org = create(:organization)
      partner1 = create(:partner)
      
      OrganizationBuilder.new(user).add_partner({ id: org.id, partners: [ partner1.id, partner1.id ] })
      
      org.reload
      
      expect(org.partners.count).to eq(1)
    end
  end
  
  describe "remove_partner" do
    it "authorizes the user" do
      user = create(:user, role: :inspector)
      org = create(:organization)
      partner = create(:partner)
      create(:organization_partner, organization_id: org.id, partner_id: partner.id)
      
      expect{ OrganizationBuilder.new(user).remove_partner({ id: org.id, partner_id: 9999999 })}.to raise_exception(CanCan::AccessDenied)
    end
    
    it "as admin" do
      user = create(:user, role: :admin)
      org = create(:organization)
      partner = create(:partner)
      create(:organization_partner, organization_id: org.id, partner_id: partner.id)
      
      OrganizationBuilder.new(user).remove_partner({ id: org.id, partner_id: partner.id })
      
      expect(org.partners.count).to eq(0)
    end
    
    it "as organization member" do
      user = create(:user, role: :inspector)
      org = create(:organization)
      create(:organization_user, organization_id: org.id, user_id: user.id)
      partner1 = create(:partner)
      create(:organization_partner, organization_id: org.id, partner_id: partner1.id)
      
      OrganizationBuilder.new(user).remove_partner({ id: org.id, partner_id: partner1.id })
      
      expect(org.partners.count).to eq(0)
    end
  end
  
  describe "add_user" do
    it "authorizes the user" do
      user = create(:user, role: :homeowner)
      org = create(:organization)
      expect{ OrganizationBuilder.new(user).add_user({id: org.id, users: [] })}.to raise_exception(CanCan::AccessDenied)
    end
    
    it "verifies users exist" do
      user = create(:user, role: :admin)
      org = create(:organization)
      user1 = create(:user)
      expect{ OrganizationBuilder.new(user).add_user({id: org.id, users: [ user1.id, 9999999 ] })}.to raise_exception(ActiveRecord::RecordNotFound)
    end
    
    it "verifies user not already added" do
       user = create(:user, role: :admin)
      org = create(:organization)
      user1 = create(:user)
      create(:organization_user, organization_id: org.id, user_id: user1.id, role: "user")
      expect{ OrganizationBuilder.new(user).add_user({id: org.id, users: [ user1.id ] })}.to raise_exception(BadRequestException) 
    end
    
    it "as admin" do
      user = create(:user, role: :admin)
      org = create(:organization)
      user1 = create(:user)
      OrganizationBuilder.new(user).add_user({id: org.id, users: [ user1.id ] })
      
      org.reload
      
      expect(org.users.count).to eq(1)
    end
    
    it "as organization member" do
      user = create(:user, role: :inspector)
      org = create(:organization)
      create(:organization_user, organization_id: org.id, user_id: user.id, role: "user")
      user1 = create(:user)
      user2 = create(:user)
      OrganizationBuilder.new(user).add_user({id: org.id, users: [ user1.id, user2.id ] })
      
      org.reload
      
      expect(org.users.count).to eq(3)
    end
    
    it "handles duplicate ids" do
      user = create(:user, role: :admin)
      org = create(:organization)
      user1 = create(:user)
      OrganizationBuilder.new(user).add_user({id: org.id, users: [ user1.id, user1.id ] })
      
      org.reload
      
      expect(org.users.count).to eq(1)
    end
  end
  
  describe "remove_user" do
    it "authorizes the user" do
      user = create(:user, role: :homeowner)
      org = create(:organization)
      user1 = create(:user)
      create(:organization_user, organization_id: org.id, user_id: user1.id, role: "user")
      expect{ OrganizationBuilder.new(user).remove_user({id: org.id, user_id: 999999 })}.to raise_exception(CanCan::AccessDenied)
    end
    
    it "as admin" do
      user = create(:user, role: :admin)
      org = create(:organization)
      user1 = create(:user)
      create(:organization_user, organization_id: org.id, user_id: user1.id, role: "user")
      OrganizationBuilder.new(user).remove_user({id: org.id, user_id: user1.id })
      
      expect(org.users.count).to eq(0)
    end
    
    it "as organization member" do
      user = create(:user, role: :inspector)
      org = create(:organization)
      create(:organization_user, organization_id: org.id, user_id: user.id, role: "user")
      user1 = create(:user)
      create(:organization_user, organization_id: org.id, user_id: user1.id, role: "user")
      OrganizationBuilder.new(user).remove_user({id: org.id, user_id: user1.id })
      
      expect(org.users.count).to eq(1)
    end
  end

end