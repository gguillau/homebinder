require 'rails_helper'

RSpec.describe Binders::BinderAnalytics, :type => :model do
    let(:binder){create :binder}

    before :each do
        @salesforce = double("Salesforce::Binder", :new_binder => {:success => true}, :update_binder => {:success => true})
        allow(Salesforce::Binder).to receive(:new).and_return(@salesforce)
        allow(@salesforce).to receive(:new_binder)
        allow(@salesforce).to receive(:update_binder)
        @intercom = double("Intercom::Client", :new => true, :events => true)
        allow(Intercom::Client).to receive(:new).and_return(@intercom)
        allow(@intercom).to receive(:events).and_return(OpenStruct.new({:create => true}))
        allow(@intercom.events).to receive(:create)
    end

    describe "created" do
        it "calls Salesforce" do
            Binders::BinderAnalytics.created(binder.id)
            expect(@salesforce).to have_received(:new_binder).with(binder.id)
        end
    end

    describe "updated" do
        it "calls Salesforce" do
            Binders::BinderAnalytics.updated(binder.id)
            expect(@salesforce).to have_received(:update_binder).with(binder.id)
        end
    end

    describe "transfer" do
        it "calls intercom and salesforce and keen" do
            sender = create(:user)
            receiver = create(:user)
            partner = create(:partner)
            transfer = create(:transfer, binder: binder, sender: sender, receiver: receiver)
            allow(partner.logo).to receive(:present?).and_return(true)
            allow(partner.logo).to receive(:expiring_cloud_front_url).and_return("")
            Binders::BinderAnalytics.transfer(transfer.id, partner.id)

            expect(@salesforce).to have_received(:update_binder).with(binder.id)
            expect(@intercom.events).to have_received(:create)
        end
    end

    describe "share" do
        it "calls intercom and salesforce and keen" do
            sender = create(:user)
            receiver = create(:user)
            share = create(:share, binder: binder, sender: sender, receiver: receiver)
            Binders::BinderAnalytics.share(share.id)

            expect(@salesforce).to have_received(:update_binder).with(binder.id)
            expect(@intercom.events).to have_received(:create)
        end
    end
end
