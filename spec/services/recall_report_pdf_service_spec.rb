require 'rails_helper'

RSpec.describe RecallReportPdfService, :type => :service do
    describe "init" do
        it "sets the binder, pdf and pdf font" do
            binder = create(:binder)
            service = RecallReportPdfService.new(binder)

            expect(service.binder).to eq(binder)
            expect(service.pdf).to_not be(nil)
            expect(service.text).to eq("Recall Report for")
            expect(service.pdf.font_families).to_not be(nil)
        end
    end

    describe "create" do
        it "sets the binder, pdf and pdf font" do
            binder = create(:binder)

            service = RecallReportPdfService.new(binder)
            allow(service.pdf).to receive(:image)
            allow(service.pdf).to receive(:formatted_text_box)

            pdf = service.create
            expect(pdf).to_not be(nil)
        end
    end

    describe "add_logos" do
        it "adds binder branding" do
            binder = create(:binder)
            user_profile = create(:user_profile, :logo => Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleImage.png'), 'image/png'))
            create(:binder_branding, :binder => binder, :user_profile => user_profile)
            service = RecallReportPdfService.new(binder)

            allow(service.pdf).to receive(:image)
            service.add_logos
            expect(service.pdf).to have_received(:image).twice
        end
    end

    describe "add_title" do
        it "adds pdf title" do
            binder = create(:binder)
            service = RecallReportPdfService.new(binder)
            allow(service.pdf).to receive(:formatted_text_box)
            service.add_title

            expect(service.pdf).to have_received(:formatted_text_box).with([
                                       { :text => "Recall Report for\n", :color => "999999", :size => 8 },
                                       { :text => "#{binder.property.address1}\n", :size => 22, :font => "Arvo" },
                                       { :text => "#{binder.property.city}, #{binder.property.state} #{binder.property.zip}", :color => '999999', :size => 12, :font => "Arvo" }
                                   ], :at => [50, 665], :width => 400, :height => 80).once
        end
    end

    describe "add_recall_title" do
        it "adds recall title with no recalls" do
            binder = create(:binder)
            service = RecallReportPdfService.new(binder)
            allow(service.pdf).to receive(:text)
            allow(service.pdf).to receive(:image)
            allow(service.pdf).to receive(:formatted_text_box)

            service.init

            service.add_logos
            service.add_title
            service.add_recall_title
            text = "NO POTENTIAL RECALLS IDENTIFIED"
            expect(service.pdf).to have_received(:text).with("#{text}", :size => 20, :color => "000000", :align => :center).once
        end

        it "adds recall title with recalls" do
            binder = create(:binder)
            appliance = create(:appliance, :binder => binder)
            create(:appliance_recalls, :appliance => appliance)
            service = RecallReportPdfService.new(binder)
            allow(service.pdf).to receive(:text)
            allow(service.pdf).to receive(:image)
            allow(service.pdf).to receive(:formatted_text_box)

            service.init
            service.add_logos
            service.add_title
            service.add_recall_title

            text = "POTENTIAL RECALLS IDENTIFIED"
            expect(service.pdf).to have_received(:text).with("#{text}", :size => 20, :color => "000000", :align => :center).once
        end
    end

    describe "add_recall_details" do
        it "adds recall details with N/A" do
            binder = create(:binder)
            service = RecallReportPdfService.new(binder)
            allow(service.pdf).to receive(:formatted_text_box)
            allow(service.pdf).to receive(:image)
            allow(service.pdf).to receive(:formatted_text_box)

            service.init
            service.add_logos
            service.add_title
            service.add_recall_title
            service.add_recall_details
            lastrecall = "N/A"

            expect(service.pdf).to have_received(:formatted_text_box).with([
                                       { :text => "Last Recall: #{lastrecall}", :size => 14, :color => "000000", :font_style => "bold" },
                                   ], :at => [0, service.pdf.cursor], :width => 400)
        end

        it "adds recall title with recalls" do
            binder = create(:binder)
            appliance = create(:appliance, :binder => binder)
            create(:appliance_recalls, :appliance => appliance)
            service = RecallReportPdfService.new(binder)
            allow(service.pdf).to receive(:formatted_text_box)
            allow(service.pdf).to receive(:image)
            allow(service.pdf).to receive(:formatted_text_box)

            service.init
            service.add_logos
            service.add_title
            service.add_recall_title
            service.add_recall_details

            lastrecall = binder.last_recall.strftime("%B %d, %Y")
            expect(service.pdf).to have_received(:formatted_text_box).with([
                                       { :text => "Last Recall: #{lastrecall}", :size => 14, :color => "000000", :font_style => "bold" },
                                   ], :at => [0, service.pdf.cursor], :width => 400)
        end
    end

    describe "add_appliances" do
        it "adds no appliances" do
            binder = create(:binder)
            service = RecallReportPdfService.new(binder)
            allow(service.pdf).to receive(:formatted_text_box)
            service.init
            service.add_appliances

            expect(service.pdf).to_not have_received(:formatted_text_box).with([
                                           { :text => "Appliance List", :size => 12, :color => "ff6633" },
                                       ], :at => [0, service.pdf.cursor], :width => 400)
        end

        it "adds appliances" do
            binder = create(:binder)
            appliance = create(:appliance, :binder => binder)
            create(:appliance_recalls, :appliance => appliance)
            service = RecallReportPdfService.new(binder)
            allow(service.pdf).to receive(:formatted_text_box)
            service.init
            service.add_appliances
            expect(service.pdf).to have_received(:formatted_text_box).with([
                                       { :text => "Appliance List", :size => 12, :color => "ff6633" },
                                   ], :at => [0, 606.0], :width => 400)
        end

        it "adds appliances without recall info" do
            binder = create(:binder)
            create(:appliance, :binder => binder)
            service = RecallReportPdfService.new(binder)
            allow(service.pdf).to receive(:formatted_text_box)
            service.init
            service.add_appliances
            expect(service.pdf).to have_received(:formatted_text_box).with([
                                       { :text => "Appliance List", :size => 12, :color => "ff6633" },
                                   ], :at => [0, 606.0], :width => 400)
        end

        it "adds appliances without manufacturer info" do
            binder = create(:binder)
            create(:appliance, :binder => binder, :manufacturer => nil)
            service = RecallReportPdfService.new(binder)
            allow(service.pdf).to receive(:formatted_text_box)
            service.init
            service.add_appliances
            expect(service.pdf).to have_received(:formatted_text_box).with([
                                       { :text => "Appliance List", :size => 12, :color => "ff6633" },
                                   ], :at => [0, 606.0], :width => 400)
        end
    end

    describe "add_footer" do
        it "adds footer" do
            binder = create(:binder)
            service = RecallReportPdfService.new(binder)
            allow(service.pdf).to receive(:formatted_text_box)
            allow(service.pdf).to receive(:bounding_box)
            allow(service.pdf).to receive(:repeat)
            service.init
            service.add_footer
            
            expect(service.pdf).to have_received(:repeat)
        end
    end
end
