require 'rails_helper'
RSpec.describe TextService::Tool, :type => :service do
    
    describe "#parse" do
        it "updates the transfer object" do
            transfer = create(:transfer, :text_message_sid => "test")
            params = {"To" => "+16172335007", "MessageSid" => "test", "SmsStatus" => "delivered"}
            TextService::Tool.new.parse(params)
            transfer.reload
            
            expect(transfer.text_message_status).to eq("delivered")
        end
    end
end