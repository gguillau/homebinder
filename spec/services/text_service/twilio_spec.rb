require 'rails_helper'
RSpec.describe TextService::Twilio, :type => :service do
    
    describe "#client" do
        it "returns mocked client" do
            double = double("Twilio::REST::Client")
            allow(Twilio::REST::Client).to receive(:new).and_return(double)
            allow(TextService::Twilio).to receive(:new).and_call_original
            expect(TextService::Twilio.new.client).to eq(double)
        end
    end
    
    describe "#auth_token" do
        it "returns the correct token" do
            allow(TextService::Twilio).to receive(:new).and_call_original
            expect(TextService::Twilio.new.auth_token).to eq(ENV["TWILIO_AUTH_TOKEN"])
        end
    end
    
    describe "#account_sid" do
        it "returns the correct account_sid" do
            allow(TextService::Twilio).to receive(:new).and_call_original
            expect(TextService::Twilio.new.account_sid).to eq(ENV["TWILIO_ACCOUNT_SID"])
        end
    end
end