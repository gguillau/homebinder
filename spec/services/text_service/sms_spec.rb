require 'rails_helper'
RSpec.describe TextService::Sms, :type => :service do
    
    describe "#send_message" do
        it "sends a text" do
            result = TextService::Sms.new.send_message("+17812335007", "Hi")
            expect(result).to eq("message sent")
        end
    end
    
    describe "#run_at" do
        it "returns 2" do
            allow(Time.zone).to receive(:parse).with("9:59 AM").and_return(1)
            allow(Time.zone).to receive(:parse).with("2:01 PM").and_return(3)
            allow(Time.zone).to receive(:now).and_return(2)
            allow(Time).to receive(:now).and_return(2)
            
            time = TextService::Sms.new.run_at
            expect(time).to eq(2)
        end
        
        it "returns 10 AM today" do
            allow(Time.zone).to receive(:parse).with("9:59 AM").and_return(2)
            allow(Time.zone).to receive(:parse).with("2:01 PM").and_return(3)
            allow(Time.zone).to receive(:now).and_return(1)
            
            time = TextService::Sms.new.run_at
            
            expect(time.strftime("%A")).to eq(Time.zone.today.at_beginning_of_day.advance(hours: 10).strftime("%A"))
        end
        
        it "returns 10 AM tomorrow" do
            allow(Time.zone).to receive(:parse).with("9:59 AM").and_return(1)
            allow(Time.zone).to receive(:parse).with("2:01 PM").and_return(2)
            allow(Time.zone).to receive(:now).and_return(3)
            
            time = TextService::Sms.new.run_at
            
            expect(time.strftime("%A")).to eq(Time.zone.tomorrow.at_beginning_of_day.advance(hours: 10) .strftime("%A"))
        end
    end
end