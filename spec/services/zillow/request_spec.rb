require 'rails_helper'

RSpec.describe Zillow::Request do
    
    before :each do
        # mock calls
        zillow = Zillow::Request.new
        allow(Zillow::Request).to receive(:new).and_return(zillow)
        allow(zillow).to receive(:call).and_call_original
    end

    describe "#download_report" do
        it "returns 200 response" do
            url = "http://www.test.com"
            stub_request(:get, URI.escape(url)).to_return(:status => 200, :body => "")
                response = Zillow::Request.new.call(url)
                # expect the response and the body to be equal
            expect(response).to eq({})
        end
        it "returns nil response" do
            allow(MultiXml).to receive(:parse).and_raise BadRequestException.new("Error")
            url = "http://www.test.com"
            stub_request(:get, URI.escape(url)).to_return(:status => 200, :body => "")
                response = Zillow::Request.new.call(url)
                # expect the response and the body to be equal
            expect(response).to eq(nil)
        end
        it "returns 403 response" do
            url = "http://www.test.com"
            stub_request(:get, URI.escape(url)).to_return(:status => 403, :body => "")

            # expect method to raise an error
            expect{Zillow::Request.new.call(url)}.to raise_error BadRequestException
        end
    end
  
end