require 'rails_helper'
require 'mandrill'

RSpec.describe Homebinder::Warranties::Service do
    
    before :each do
        allow(WarrantyMailer).to receive(:delay).and_return(WarrantyMailer)
        allow(WarrantyMailer).to receive(:send_warranty_report)
    end
    
    describe "#download" do
        it "requires a user" do
            expect{Homebinder::Warranties::Service.new.download(nil)}.to raise_exception BadRequestException
        end
        it "requires an admin" do
            user = create(:user)
            expect{Homebinder::Warranties::Service.new.download(user)}.to raise_exception BadRequestException
        end
        it "sends the email" do
            admin = create(:user, :role => "admin")
                Homebinder::Warranties::Service.new.download(admin)
                # check the call
            expect(WarrantyMailer).to have_received(:send_warranty_report)
        end
    end
end