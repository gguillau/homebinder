require 'rails_helper'

RSpec.describe SubscriptionService, :type => :service do
    before :each do
        @binder = create(:binder)
        allow(SubscriptionService).to receive(:create_subscription).and_call_original
    end

    describe "verify" do
        it "creates a subscription and verifies the binder" do
            expect{ SubscriptionService.verify(@binder)}.not_to raise_error
            expect(SubscriptionService).to have_received(:create_subscription)
        end

        it "raises an error" do
            create(:subscription, binder_id: @binder.id, :payment_status => "failed")
            expect{ SubscriptionService.verify(@binder)}.to raise_error(SubscriptionError)
        end
    end

    describe "get_plan_id" do
        it "returns homeownerplan299" do
            name = SubscriptionService.get_plan_id("standard")
            expect(name).to eq("homeownerplan299")
        end

        it "returns free" do
            name = SubscriptionService.get_plan_id("free")
            expect(name).to eq("free")
        end
    end

    describe "get_plan_name" do
        it "returns homeownerplan299" do
            id = SubscriptionService.get_plan_name("homeownerplan299")
            expect(id).to eq("standard")
        end

        it "returns free" do
            id = SubscriptionService.get_plan_name("free")
            expect(id).to eq("free")
        end
    end

    describe "update_card" do
        it "raises an error" do
            expect{SubscriptionService.update_card(nil, nil)}.to raise_error CardRequiredException
        end
        it "raises an error" do
            expect{SubscriptionService.update_card(nil, {})}.to raise_error CardRequiredException
        end
        it "raises an error" do
            expect{SubscriptionService.update_card(nil, "")}.to raise_error CardRequiredException
        end
        it "raises an error" do
            subscription = create(:subscription)
            token =   OpenStruct.new({
                "id" => "tok_Bg5v5Q8HFSH26d",
                "object" => "token",
                "card" => {
                    "id" =>"card_Bg5vczxbqKyzy3",
                    "object" =>" card",
                    "address_city" => nil,
                    "address_country" => nil,
                    "address_line1" => nil,
                    "address_line1_check" => nil,
                    "address_line2" => nil,
                    "address_state" => nil,
                    "address_zip" => nil,
                    "address_zip_check" => nil,
                    "brand" => "Visa",
                    "country" => "US",
                    "cvc_check" => nil,
                    "dynamic_last4" => nil,
                    "exp_month" => 10,
                    "exp_year" => 2017,
                    "fingerprint" => "SkUolUdsHDm1Ldla",
                    "funding" => "credit",
                    "last4" => "4242",
                    "metadata" => {},
                    "name" => nil,
                    "tokenization_method" => nil,
                    "customer" => nil,
                    "type" => "Visa"
                },
                "client_ip" => "104.154.137.220",
                "created" => 1509394805,
                "livemode" => false,
                "type" => "card",
                "used" => false
            })

            allow(Stripe::Token).to receive(:create).and_return(token)

            card = {:number => "4242424242424242", :exp_month => Date.today.month, :exp_year => Date.today.year, :cvc => "111"}
            expect{SubscriptionService.update_card(subscription, card)}.to raise_error BadRequestException, "Customer ID Required"
        end
        it "updates the card" do
            subscription = create(:subscription, :customer_id => 1)
            token =   OpenStruct.new({
                "id" => "tok_Bg5v5Q8HFSH26d",
                "object" => "token",
                "card" => {
                    "id" =>"card_Bg5vczxbqKyzy3",
                    "object" =>" card",
                    "address_city" => nil,
                    "address_country" => nil,
                    "address_line1" => nil,
                    "address_line1_check" => nil,
                    "address_line2" => nil,
                    "address_state" => nil,
                    "address_zip" => nil,
                    "address_zip_check" => nil,
                    "brand" => "Visa",
                    "country" => "US",
                    "cvc_check" => nil,
                    "dynamic_last4" => nil,
                    "exp_month" => 10,
                    "exp_year" => 2017,
                    "fingerprint" => "SkUolUdsHDm1Ldla",
                    "funding" => "credit",
                    "last4" => "4242",
                    "metadata" => {},
                    "name" => nil,
                    "tokenization_method" => nil,
                    "customer" => nil,
                    "type" => "Visa"
                },
                "client_ip" => "104.154.137.220",
                "created" => 1509394805,
                "livemode" => false,
                "type" => "card",
                "used" => false
            })

            allow(Stripe::Token).to receive(:create).and_return(token)
            retrieved_card = OpenStruct.new({:delete => true})
            cards = OpenStruct.new({:retrieve => true, :create => true})
            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, delete: true, default_card: {}, cards: cards})
            allow(Stripe::Customer).to receive(:retrieve).and_return(stripe_customer)
            allow(stripe_customer.cards).to receive(:retrieve).and_return(retrieved_card)
            allow(stripe_customer.cards).to receive(:create).and_return(true)
            card = {:number => "4242424242424242", :exp_month => Date.today.month, :exp_year => Date.today.year, :cvc => "111"}
            SubscriptionService.update_card(subscription, card)
        end
        it "updates the card and failed payments" do
            subscription = create(:subscription, :customer_id => 1, :payment_status => "failed")
            token =   OpenStruct.new({
                "id" => "tok_Bg5v5Q8HFSH26d",
                "object" => "token",
                "card" => {
                    "id" =>"card_Bg5vczxbqKyzy3",
                    "object" =>" card",
                    "address_city" => nil,
                    "address_country" => nil,
                    "address_line1" => nil,
                    "address_line1_check" => nil,
                    "address_line2" => nil,
                    "address_state" => nil,
                    "address_zip" => nil,
                    "address_zip_check" => nil,
                    "brand" => "Visa",
                    "country" => "US",
                    "cvc_check" => nil,
                    "dynamic_last4" => nil,
                    "exp_month" => 10,
                    "exp_year" => 2017,
                    "fingerprint" => "SkUolUdsHDm1Ldla",
                    "funding" => "credit",
                    "last4" => "4242",
                    "metadata" => {},
                    "name" => nil,
                    "tokenization_method" => nil,
                    "customer" => nil,
                    "type" => "Visa"
                },
                "client_ip" => "104.154.137.220",
                "created" => 1509394805,
                "livemode" => false,
                "type" => "card",
                "used" => false
            })

            allow(Stripe::Token).to receive(:create).and_return(token)
            allow(Stripe::Invoice).to receive(:all).and_return([OpenStruct.new({:paid => false, :pay => true})])
            retrieved_card = OpenStruct.new({:delete => true})
            cards = OpenStruct.new({:retrieve => true, :create => true})
            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, delete: true, default_card: {}, cards: cards})
            allow(Stripe::Customer).to receive(:retrieve).and_return(stripe_customer)
            allow(stripe_customer.cards).to receive(:retrieve).and_return(retrieved_card)
            allow(stripe_customer.cards).to receive(:create).and_return(true)
            card = {:number => "4242424242424242", :exp_month => Date.today.month, :exp_year => Date.today.year, :cvc => "111"}
            SubscriptionService.update_card(subscription, card)
        end
    end

    describe "upgrade" do
        it "returns because subscription is nil" do
            result = SubscriptionService.upgrade(nil, nil, nil)
            expect(result).to be(nil)
        end
        it "raises error" do
            subscription = create(:subscription)
            expect{SubscriptionService.upgrade(subscription, nil, nil)}.to raise_error CardRequiredException
        end
        it "attempts to create customer and raise error" do
            token =   OpenStruct.new({
                "id" => "tok_Bg5v5Q8HFSH26d",
                "object" => "token",
                "card" => {
                    "id" =>"card_Bg5vczxbqKyzy3",
                    "object" =>" card",
                    "address_city" => nil,
                    "address_country" => nil,
                    "address_line1" => nil,
                    "address_line1_check" => nil,
                    "address_line2" => nil,
                    "address_state" => nil,
                    "address_zip" => nil,
                    "address_zip_check" => nil,
                    "brand" => "Visa",
                    "country" => "US",
                    "cvc_check" => nil,
                    "dynamic_last4" => nil,
                    "exp_month" => 10,
                    "exp_year" => 2017,
                    "fingerprint" => "SkUolUdsHDm1Ldla",
                    "funding" => "credit",
                    "last4" => "4242",
                    "metadata" => {},
                    "name" => nil,
                    "tokenization_method" => nil,
                    "customer" => nil,
                    "type" => "Visa"
                },
                "client_ip" => "104.154.137.220",
                "created" => 1509394805,
                "livemode" => false,
                "type" => "card",
                "used" => false
            })

            allow(Stripe::Token).to receive(:create).and_return(token)
            subscription = create(:subscription)
            allow(Stripe::Customer).to receive(:create).and_raise UnprocessableException
            card = {:number => "4242424242424242", :exp_month => Date.today.month, :exp_year => Date.today.year, :cvc => "111"}
            expect{SubscriptionService.upgrade(subscription, card, nil)}.to raise_error BadRequestException
        end
        it "raises an error" do
            token =   OpenStruct.new({
                "id" => "tok_Bg5v5Q8HFSH26d",
                "object" => "token",
                "card" => {
                    "id" =>"card_Bg5vczxbqKyzy3",
                    "object" =>" card",
                    "address_city" => nil,
                    "address_country" => nil,
                    "address_line1" => nil,
                    "address_line1_check" => nil,
                    "address_line2" => nil,
                    "address_state" => nil,
                    "address_zip" => nil,
                    "address_zip_check" => nil,
                    "brand" => "Visa",
                    "country" => "US",
                    "cvc_check" => nil,
                    "dynamic_last4" => nil,
                    "exp_month" => 10,
                    "exp_year" => 2017,
                    "fingerprint" => "SkUolUdsHDm1Ldla",
                    "funding" => "credit",
                    "last4" => "4242",
                    "metadata" => {},
                    "name" => nil,
                    "tokenization_method" => nil,
                    "customer" => nil,
                    "type" => "Visa"
                },
                "client_ip" => "104.154.137.220",
                "created" => 1509394805,
                "livemode" => false,
                "type" => "card",
                "used" => false
            })

            allow(Stripe::Token).to receive(:create).and_return(token)
            subscription = create(:subscription)
            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, delete: true})
            allow(Stripe::Customer).to receive(:create).and_return(stripe_customer)
            card = {:number => "4242424242424242", :exp_month => Date.today.month, :exp_year => Date.today.year, :cvc => "111"}
            allow(subscription).to receive(:save).and_return(false)
            expect{SubscriptionService.upgrade(subscription, card, nil)}.to raise_error UnprocessableException
        end
        it "updates" do
            token =   OpenStruct.new({
                "id" => "tok_Bg5v5Q8HFSH26d",
                "object" => "token",
                "card" => {
                    "id" =>"card_Bg5vczxbqKyzy3",
                    "object" =>" card",
                    "address_city" => nil,
                    "address_country" => nil,
                    "address_line1" => nil,
                    "address_line1_check" => nil,
                    "address_line2" => nil,
                    "address_state" => nil,
                    "address_zip" => nil,
                    "address_zip_check" => nil,
                    "brand" => "Visa",
                    "country" => "US",
                    "cvc_check" => nil,
                    "dynamic_last4" => nil,
                    "exp_month" => 10,
                    "exp_year" => 2017,
                    "fingerprint" => "SkUolUdsHDm1Ldla",
                    "funding" => "credit",
                    "last4" => "4242",
                    "metadata" => {},
                    "name" => nil,
                    "tokenization_method" => nil,
                    "customer" => nil,
                    "type" => "Visa"
                },
                "client_ip" => "104.154.137.220",
                "created" => 1509394805,
                "livemode" => false,
                "type" => "card",
                "used" => false
            })

            allow(Stripe::Token).to receive(:create).and_return(token)
            owner = create(:user, :role => "homeowner")
            binder = create(:binder)
            create(:user_binder, :user => owner, :binder => binder, :role => "owner")
            subscription = create(:subscription, :binder => binder)
            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, delete: true})
            allow(Stripe::Customer).to receive(:create).and_return(stripe_customer)
            card = {:number => "4242424242424242", :exp_month => Date.today.month, :exp_year => Date.today.year, :cvc => "111"}
            result = SubscriptionService.upgrade(subscription, card, nil)
            expect(result).to_not be(nil)
            expect(result.payment_status).to eq("paid")
        end
    end

    describe "cancel" do
        it "returns because subscription is nil" do
            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, delete: true})
            allow(Stripe::Customer).to receive(:retrieve).and_return(stripe_customer)

            result = SubscriptionService.cancel(nil)

            expect(result).to be(nil)
        end
        it "returns because customer_id is nil" do
            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, delete: true})
            allow(Stripe::Customer).to receive(:retrieve).and_return(stripe_customer)
            subscription = create(:subscription, :payment_status => "paid")
            SubscriptionService.cancel(subscription)
            subscription.reload
            expect(subscription.payment_status).to eq("paid")
        end
        it "raises an UnprocessableException" do
            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, delete: true})
            allow(Stripe::Customer).to receive(:retrieve).and_return(stripe_customer)
            subscription = create(:subscription, :payment_status => "paid", :customer_id => 1)
            allow(subscription).to receive(:save).and_return(false)
            expect{SubscriptionService.cancel(subscription)}.to raise_error UnprocessableException
        end
        it "cancels the subscription" do
            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, delete: true})
            allow(Stripe::Customer).to receive(:retrieve).and_return(stripe_customer)
            subscription = create(:subscription, :customer_id => 1)
            subscription = SubscriptionService.cancel(subscription)
            expect(subscription.payment_status).to be(nil)
        end
    end

    describe "create_subscription" do
        it "creates the subscription" do
            subscription = SubscriptionService.create_subscription(@binder, nil)
            expect(subscription.binder_id).to eq(@binder.id)
        end

        it "sends a report to New Relic when save fails" do
            allow(ErrorService).to receive(:perform_async)
            allow_any_instance_of(Subscription).to receive(:save).and_return(false)

            subscription = SubscriptionService.create_subscription(@binder, nil)

            expect(subscription).to be(nil)
            expect(ErrorService).to have_received(:perform_async)
        end
    end

    describe "update_subscription" do
        it "upgrades the subscription" do
            allow(SubscriptionService).to receive(:upgrade)

            SubscriptionService.update_subscription("upgrade", nil, nil, nil)

            expect(SubscriptionService).to have_received(:upgrade)
        end

        it "updates the card" do
            allow(SubscriptionService).to receive(:update_card)

            SubscriptionService.update_subscription("update", nil, nil, nil)

            expect(SubscriptionService).to have_received(:update_card)
        end

        it "cancels the subscription" do
            allow(SubscriptionService).to receive(:cancel)

            SubscriptionService.update_subscription("cancel", nil, nil, nil)

            expect(SubscriptionService).to have_received(:cancel)
        end
    end

    describe "is_free_for_life?" do
        it "returns false" do
            subscription = create(:subscription)
            result = SubscriptionService.is_free_for_life?(subscription)
            expect(result).to eq(false)
        end
        it "returns false" do
            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, delete: true, subscriptions: nil})
            allow(Stripe::Customer).to receive(:retrieve).and_return(stripe_customer)
            subscription = create(:subscription, :customer_id => 1)
            result = SubscriptionService.is_free_for_life?(subscription)
            expect(result).to eq(false)
        end
        it "returns false" do
            sub = OpenStruct.new({:plan =>  nil})
            subscriptions = OpenStruct.new({:total_count => 1, :data => [sub]})
            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, delete: true, subscriptions: subscriptions})
            allow(Stripe::Customer).to receive(:retrieve).and_return(stripe_customer)
            subscription = create(:subscription, :customer_id => 1)
            result = SubscriptionService.is_free_for_life?(subscription)
            expect(result).to eq(false)
        end
        it "returns false" do
            sub = OpenStruct.new({:plan =>  nil})
            subscriptions = OpenStruct.new({:total_count => 1, :data => [sub]})
            discount = OpenStruct.new({})
            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, delete: true, subscriptions: subscriptions, discount: discount})
            allow(Stripe::Customer).to receive(:retrieve).and_return(stripe_customer)
            subscription = create(:subscription, :customer_id => 1)
            result = SubscriptionService.is_free_for_life?(subscription)
            expect(result).to eq(false)
        end
        it "returns true" do
            sub = OpenStruct.new({:plan =>  OpenStruct.new({:amount => 100})})
            subscriptions = OpenStruct.new({:total_count => 1, :data => [sub]})
            discount = OpenStruct.new({:coupon => OpenStruct.new({:amount_off => 100, :duration => "forever"})})
            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, delete: true, subscriptions: subscriptions, discount: discount})
            allow(Stripe::Customer).to receive(:retrieve).and_return(stripe_customer)
            subscription = create(:subscription, :customer_id => 1)
            result = SubscriptionService.is_free_for_life?(subscription)
            expect(result).to eq(true)
        end
        it "returns true" do
            sub = OpenStruct.new({:plan =>  OpenStruct.new({:amount => 99})})
            subscriptions = OpenStruct.new({:total_count => 1, :data => [sub]})
            discount = OpenStruct.new({:coupon => OpenStruct.new({:amount_off => 100, :percent_off => 100, :duration => "forever"})})
            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, delete: true, subscriptions: subscriptions, discount: discount})
            allow(Stripe::Customer).to receive(:retrieve).and_return(stripe_customer)
            subscription = create(:subscription, :customer_id => 1)
            result = SubscriptionService.is_free_for_life?(subscription)
            expect(result).to eq(true)
        end
        it "returns false" do
            sub = OpenStruct.new({:plan =>  OpenStruct.new({:amount => 99})})
            subscriptions = OpenStruct.new({:total_count => 1, :data => [sub]})
            discount = OpenStruct.new({:coupon => OpenStruct.new({:amount_off => 100, :percent_off => 1, :duration => "forever"})})
            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, delete: true, subscriptions: subscriptions, discount: discount})
            allow(Stripe::Customer).to receive(:retrieve).and_return(stripe_customer)
            subscription = create(:subscription, :customer_id => 1)
            result = SubscriptionService.is_free_for_life?(subscription)
            expect(result).to eq(false)
        end
    end

    describe "copy_subscription" do
        let(:stripe_customer) {OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, delete: true, subscriptions: {"data" => [OpenStruct.new()] }})}
        before :each do
            allow(Stripe::Customer).to receive(:retrieve).and_return(stripe_customer)
            allow(Stripe::Customer).to receive(:create).and_return(stripe_customer)
        end
        it "creates the copy" do
            source = create(:subscription)
            dest = create(:subscription)
            SubscriptionService.copy_subscription(source, dest)
            expect(dest.payment_status).to eq("paid")
        end
        it "raises an error and notifies new relic" do
            source = create(:subscription)
            dest = create(:subscription)
            allow(ErrorService).to receive(:perform_async)
            allow(stripe_customer).to receive(:save).and_raise BadRequestException, "Error"

            SubscriptionService.copy_subscription(source, dest)
            expect(ErrorService).to have_received(:perform_async).once
        end
        it "raises an error and notifies new relic" do
            source = create(:subscription)
            dest = create(:subscription)
            allow(ErrorService).to receive(:perform_async)
            allow(dest).to receive(:save).and_return(false)
            SubscriptionService.copy_subscription(source, dest)
            expect(ErrorService).to have_received(:perform_async).once
        end
    end

    describe "create_subscription" do
        it "returns subscription and upgrades" do
            allow(SubscriptionService).to receive(:upgrade)

            binder = create(:binder)
            partner = create(:partner)
            create(:coupon, :partner => partner)
            subscription = SubscriptionService.create_subscription(binder, partner)
            expect(subscription).to_not be(nil)
            expect(SubscriptionService).to have_received(:upgrade)
        end
    end

    describe "create_card_token" do
        it "returns token" do
            token =   {
                "id" => "tok_Bg5v5Q8HFSH26d",
                "object" => "token",
                "card" => {
                    "id" =>"card_Bg5vczxbqKyzy3",
                    "object" =>" card",
                    "address_city" => nil,
                    "address_country" => nil,
                    "address_line1" => nil,
                    "address_line1_check" => nil,
                    "address_line2" => nil,
                    "address_state" => nil,
                    "address_zip" => nil,
                    "address_zip_check" => nil,
                    "brand" => "Visa",
                    "country" => "US",
                    "cvc_check" => nil,
                    "dynamic_last4" => nil,
                    "exp_month" => 10,
                    "exp_year" => 2017,
                    "fingerprint" => "SkUolUdsHDm1Ldla",
                    "funding" => "credit",
                    "last4" => "4242",
                    "metadata" => {},
                    "name" => nil,
                    "tokenization_method" => nil,
                    "customer" => nil,
                    "type" => "Visa"
                },
                "client_ip" => "104.154.137.220",
                "created" => 1509394805,
                "livemode" => false,
                "type" => "card",
                "used" => false
            }

            allow(Stripe::Token).to receive(:create).and_return(token)

            card = {:number => "4242424242424242", :exp_month => Date.today.month, :exp_year => Date.today.year, :cvc => "111"}
            created_token = SubscriptionService.create_card_token(card)

            expect(created_token).to eq(token)
        end
    end

    describe "is_card_required?" do
        it "returns true" do
            result = SubscriptionService.is_card_required?(nil, nil)
            expect(result).to eq(true)
        end

        it "returns true" do
            result = SubscriptionService.is_card_required?("", nil)
            expect(result).to eq(true)
        end

        it "returns false" do
            allow(Stripe::Plan).to receive(:retrieve).and_return(OpenStruct.new({id: 1, amount: 4900}))
            allow(Stripe::Coupon).to receive(:retrieve).and_return(OpenStruct.new({id: 1, amount_off: 4900}))

            result = SubscriptionService.is_card_required?("coupon", "plan")
            expect(result).to eq(false)
        end

        it "returns true" do
            allow(Stripe::Plan).to receive(:retrieve).and_return(OpenStruct.new({id: 1, amount: 4900}))
            allow(Stripe::Coupon).to receive(:retrieve).and_return(OpenStruct.new({id: 1, duration: "repeating", amount_off: 4800, percent_off: 100}))

            result = SubscriptionService.is_card_required?("coupon", "plan")
            expect(result).to eq(true)
        end

        it "returns false" do
            allow(Stripe::Plan).to receive(:retrieve).and_return(OpenStruct.new({id: 1, amount: 4900}))
            allow(Stripe::Coupon).to receive(:retrieve).and_return(OpenStruct.new({id: 1, duration: "forever", amount_off: 4800, percent_off: 100}))

            result = SubscriptionService.is_card_required?("coupon", "plan")
            expect(result).to eq(false)
        end

        it "returns true" do
            allow(Stripe::Plan).to receive(:retrieve).and_return(OpenStruct.new({id: 1, amount: 4900}))
            allow(Stripe::Coupon).to receive(:retrieve).and_return(OpenStruct.new({id: 1, amount_off: 4800, percent_off: 1}))

            result = SubscriptionService.is_card_required?("coupon", "plan")
            expect(result).to eq(true)
        end
    end
end
