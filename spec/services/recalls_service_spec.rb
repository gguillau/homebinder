require 'rails_helper'

RSpec.describe RecallService, :type => :service do
    before :each do
        @recall_details = {
            "Title" => "This is a recall. Foo is only in the title",
            "Description" => "Here are the details of the recall. Bar is only in the description",
            "Manufacturers" => [
                { "Name" => "Man" }
            ],
            "Products" => [
                { "Name" => "Thing", "Description" => "Here are the details of the product. Pop is here.", "Type" => "Baz" }
            ]
        }

        @recall_models = "modelABC, modelXYZ, model123"
        @recall = build(:recall, details: JSON.generate(@recall_details), models: @recall_models)
    end

    describe "download_recalls" do
        it "it calls CPSCService" do
            allow(CPSCService).to receive(:delay).and_return(CPSCService)
            allow(CPSCService).to receive(:get_new_recalls)

            RecallService.download_recalls

            expect(CPSCService).to have_received(:get_new_recalls)
        end
    end

    describe "get_new_recalls" do
        it "it gets 0 unverified recalls" do
            recalls = RecallService.get_new_recalls

            expect(recalls.length).to eq(0)
        end

        it "it gets 1 unverified recall" do
            create(:recall, :verified => false)
            recalls = RecallService.get_new_recalls

            expect(recalls.length).to eq(1)
        end
    end
end
