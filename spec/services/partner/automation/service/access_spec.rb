require 'rails_helper'
require 'mandrill'

RSpec.describe Partner::Automation::Service::Access, :type => :model do
    before :each do
        allow(Slack::MessagingJob).to receive(:perform_async)
    end

    describe "transfer_binders" do
        context "when default_binder_action is transfer" do
            before :each do
                @partner = create(:partner, name: "Test Inspections")
                @partner_user = create(:user, email: @partner.email, :role => "inspector")
                create(:partner_user, :partner_id => @partner.id, :user_id => @partner_user.id, :role => "admin")
                @partner.partner_configuration.automation_binder_action = true
                @partner.partner_configuration.binder_action_delay_length = 5
                @partner.save
                @partner_binder = create(:binder, created_by: @partner_user.id, created_at: 6.hours.ago)
                create(:subscription, binder_id: @partner_binder.id, plan_id: "free")
                @homeowner = create(:user)

                # create transfer
                @partner_binder.transfers.new(:status => "created", :transfer_type => "ownership", :receiver_id => @homeowner.id, :sender_id => @partner_user.id).save

                @partner_user.add_role :owner, @partner_binder
                @partner.add_role :binder, @partner_binder, @homeowner.id
            end

            it "transfers the partner binders" do
                Partner::Automation::Service::Access.new.transfer_binders

                binders = Binder.with_role(:owner, @partner_user)
                homeowner = User.with_role(:owner, @partner_binder).first

                expect(binders.count).to eq(0)
                expect(homeowner.email).to eq(@homeowner.email)
            end
        end

        context "when default_binder_action is share" do
            before :each do
                @partner = create(:partner, name: "Test Inspections")
                @partner_user = create(:user, email: @partner.email, :role => @partner.partner_type)
                create(:partner_user, :partner_id => @partner.id, :user_id => @partner_user.id, :role => "admin")
                @partner.partner_configuration.automation_binder_action = true
                @partner.partner_configuration.default_binder_action = "share"
                @partner.partner_configuration.binder_action_delay_length = 5
                @partner.save
                @partner_binder = create(:binder, created_by: @partner_user.id, created_at: 6.hours.ago.in_time_zone)
                create(:subscription, binder_id: @partner_binder.id, plan_id: "free")
                @homeowner = create(:user, :role => "homeowner")

                # create transfer
                @partner_binder.transfers.new(:status => "created", :transfer_type => "ownership", :receiver_id => @homeowner.id, :sender_id => @partner_user.id).save!

                @partner_user.add_role :owner, @partner_binder
                @partner.add_role :binder, @partner_binder, @homeowner.id
            end

            it "shares the partner binders" do
                # call the task
                Partner::Automation::Service::Access.new.transfer_binders

                homeowner = User.with_role(:co_owner, @partner_binder).first

                # make sure homeowner has share access
                expect(homeowner.email).to eq(@homeowner.email)
            end
        end

        context "when automation_binder_action is false" do
            before :each do
                @partner = create(:partner, name: "Test Inspections")
                @partner_user = create(:user, email: @partner.email, :role => @partner.partner_type)
                create(:partner_user, :partner_id => @partner.id, :user_id => @partner_user.id, :role => "admin")
                @partner.partner_configuration.automation_binder_action = false
                @partner.partner_configuration.default_binder_action = "share"
                @partner.save
                @partner_binder = create(:binder, created_by: @partner_user.id, created_at: 6.hours.ago.in_time_zone)
                create(:subscription, binder_id: @partner_binder.id, plan_id: "free")
                @homeowner = create(:user)

                # create transfer
                @partner_binder.transfers.new(:status => "created", :transfer_type => "ownership", :receiver_id => @homeowner.id, :sender_id => @partner_user.id).save
                @partner_user.add_role :owner, @partner_binder
                @partner.add_role :binder, @partner_binder, @homeowner.id
            end

            it "doesn't transfer the partner binders" do
                # call the task
                Partner::Automation::Service::Access.new.transfer_binders
                owner = User.with_role(:owner, @partner_binder).first

                expect(owner.email).to eq(@partner_user.email)
            end
        end

        context "when automation_binder_action is true" do
            before :each do
                @partner = create(:partner, name: "Test Inspections")
                @partner_user = create(:user, email: @partner.email, :role => @partner.partner_type)
                create(:partner_user, :partner_id => @partner.id, :user_id => @partner_user.id, :role => "admin")
                @partner.partner_configuration.automation_binder_action = true
                @partner.partner_configuration.default_binder_action = "transfer"
                @partner.partner_configuration.binder_action_delay_length = 5
                @partner.save
                @partner_binder = create(:binder, created_by: @partner_user.id, created_at: 3.hours.ago.in_time_zone)
                create(:subscription, binder_id: @partner_binder.id, plan_id: "free")
                @homeowner = create(:user)

                # create transfer
                @partner_binder.transfers.new(:status => "created", :transfer_type => "ownership", :receiver_id => @homeowner.id, :sender_id => @partner_user.id).save
                @partner_user.add_role :owner, @partner_binder
                @partner.add_role :binder, @partner_binder, @homeowner.id
            end

            it "doesn't transfer the partner binders because of clock" do
                # call the task
                Partner::Automation::Service::Access.new.transfer_binders
                owner = User.with_role(:owner, @partner_binder).first

                expect(owner.email).to eq(@partner_user.email)
            end

            it "doesn't share the partner binders because of clock" do
                # call the task
                Partner::Automation::Service::Access.new.transfer_binders
                co_owner = User.with_role(:co_owner, @partner_binder).first

                expect(co_owner).to eq(nil)
            end
        end
    end

    describe "can_transfer_access" do
        before :each do
            @partner = create(:partner, name: "Test Inspections")
            @partner_user = create(:user, email: @partner.email, :role => "inspector")
            create(:partner_user, :partner_id => @partner.id, :user_id => @partner_user.id)
            @partner.partner_configuration.automation_binder_action = true
            @partner.partner_configuration.binder_action_delay_length = 5
            @partner.save
            @partner_binder = create(:binder, created_by: @partner_user.id, created_at: 6.hours.ago)
            create(:subscription, binder_id: @partner_binder.id, plan_id: "free")
            @homeowner = create(:user)

            # create transfer
            @partner_binder.transfers.new(:status => "created", :transfer_type => "ownership", :receiver_id => @homeowner.id, :sender_id => @partner_user.id).save
        end

        it "returns true" do
            value = Partner::Automation::Service::Access.new.can_transfer_access(@partner_binder, @partner.partner_configuration)
            expect(value).to eq(true)
        end

        it "returns false" do
            @partner_binder.created_at = 3.hours.ago
            @partner_binder.save

            value = Partner::Automation::Service::Access.new.can_transfer_access(@partner_binder, @partner.partner_configuration)
            expect(value).to eq(false)
        end
    end

    describe "can_share" do
        before :each do
            @partner = create(:partner, name: "Test Inspections")
            @partner_user = create(:user, email: @partner.email, :role => "inspector")
            create(:partner_user, :partner_id => @partner.id, :user_id => @partner_user.id)
            @partner.partner_configuration.automation_binder_action = true
            @partner.save
            @partner_binder = create(:binder, created_by: @partner_user.id, created_at: 6.hours.ago)
            create(:subscription, binder_id: @partner_binder.id, plan_id: "free")
            @homeowner = create(:user)

            # create transfer
            @partner_binder.transfers.new(:status => "created", :transfer_type => "ownership", :receiver_id => @homeowner.id, :sender_id => @partner_user.id).save
        end

        it "returns true" do
            value = Partner::Automation::Service::Access.new.can_share(@partner_binder)
            expect(value).to eq(true)
        end

        it "returns false when transfer doesnt exist" do
            # destroy all the transfers
            @partner_binder.transfers.destroy_all

            value = Partner::Automation::Service::Access.new.can_share(@partner_binder)
            expect(value).to eq(false)
            #expect(messaging).to have_received(:send_message)
        end

        it "returns false when receiver doesnt exist" do
            # destroy the receiver
            @partner_binder.transfers.last.receiver.destroy

            value = Partner::Automation::Service::Access.new.can_share(@partner_binder)
            expect(value).to eq(false)
            #expect(messaging).to have_received(:send_message)
        end

        it "returns false when receiver already has access" do
            # add the receiver as a user on the binder
            user = @partner_binder.transfers.last.receiver
            UserBinder.create(:role => "co_owner", :user_id => user.id, :binder_id => @partner_binder.id)

            value = Partner::Automation::Service::Access.new.can_share(@partner_binder)
            expect(value).to eq(false)
            #expect(messaging).to_not have_received(:send_message)
        end
    end

    describe "check_binder_action" do
        before :each do
            @partner = create(:partner, name: "Test Inspections")
            @partner_user = create(:user, email: @partner.email, :role => "inspector")
            create(:partner_user, :partner_id => @partner.id, :user_id => @partner_user.id)
            @partner.partner_configuration.automation_binder_action = true
            @partner.save
            @partner_binder = create(:binder, created_by: @partner_user.id, created_at: 6.hours.ago)
            create(:subscription, binder_id: @partner_binder.id, plan_id: "free")
            @homeowner = create(:user)

            # create transfer
            @partner_binder.transfers.new(:status => "created", :transfer_type => "ownership", :receiver_id => @homeowner.id, :sender_id => @partner_user.id).save
        end

        it "returns transfer" do
            value = Partner::Automation::Service::Access.new.check_binder_action(@partner.partner_configuration)
            expect(value).to eq("transfer")
        end

        it "returns share" do
            @partner.partner_configuration.default_binder_action = "share"
            @partner.save

            value = Partner::Automation::Service::Access.new.check_binder_action(@partner.partner_configuration)
            expect(value).to eq("share")
        end
    end

    describe "can_perform_action" do
        before :each do
            @partner = create(:partner, name: "Test Inspections")
            @partner_user = create(:user, email: @partner.email, :role => "inspector")
            create(:partner_user, :partner_id => @partner.id, :user_id => @partner_user.id)
            @partner.partner_configuration.automation_binder_action = true
            @partner.save
            @partner_binder = create(:binder, created_by: @partner_user.id, created_at: 6.hours.ago)
            create(:subscription, binder_id: @partner_binder.id, plan_id: "free")
            @homeowner = create(:user)

            # create transfer
            @partner_binder.transfers.new(:status => "created", :transfer_type => "ownership", :receiver_id => @homeowner.id, :sender_id => @partner_user.id).save
        end

        it "returns true" do
            value = Partner::Automation::Service::Access.new.can_perform_action(@partner.partner_configuration)
            expect(value).to eq(true)
        end

        it "returns false" do
            @partner.partner_configuration.automation_binder_action = false
            @partner.save

            value = Partner::Automation::Service::Access.new.can_perform_action(@partner.partner_configuration)
            expect(value).to eq(false)
        end
    end
end