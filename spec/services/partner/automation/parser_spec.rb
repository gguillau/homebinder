require 'rails_helper'

RSpec.describe Partner::Automation::EmailParser, :type => :model do
    
    let(:api_key) {Faker::Lorem.characters(20)}
    let(:partner_name) {"test"}
    let(:partner_email) {"test@gmail.com"}
    let(:binder_template_id) {Faker::Number.number(3)}
    let(:client_first) {"Bob"}
    let(:client_last) {"Smith"}
    let(:client_email) {"bobsmith@gmail.com"}
    let(:property_address1) {"320 Main Street"}
    let(:property_address2) {"Apt 2"}
    let(:property_city) {"Springfield"}
    let(:property_state) {"MO"}
    let(:property_zip) {""}
    let(:property_country) {"US"}
    let(:agent_name) {"Keller Williams"}
    let(:agent_contact) {"Kasey Stevenson"}  
    let(:agent_phone) {"333-333-3333"}
    let(:agent_email) {"kstevens@gmail.com"}
    let(:transaction_type) {"BUY"}
    let(:inspection_date) {Date.today}
    let(:project_number) {"42ASXm"}
    
    describe "#parse" do
        it "correctly parses the email from plain text" do
            plain_part = "API Key:&nbsp;#{api_key}\nPartner Name:&nbsp;#{partner_name}\nBinderTemplateId: #{binder_template_id}\nClient First: #{client_first}\nClient Last: #{client_last}\nClient Email: #{client_email}\nProperty Address: #{property_address1}\nProperty Address2: #{property_address2}\nProperty City: #{property_city}\nProperty State: #{property_state}\nProperty PostalCode: #{property_zip}\nProperty Country: #{property_country}\nAgent Name: #{agent_name}\nAgent Contact: #{agent_contact}\nAgent Phone: #{agent_phone}\nAgent Email: #{agent_email}\nAutomatic Transfer: true"
            email = {:multipart => false, :body => OpenStruct.new({:decoded => plain_part}), :attachments => []}
            request = Partner::Automation::EmailParser.new.parse(OpenStruct.new(email))
                parameters = {
                :api_key => api_key, 
                :key => partner_name, 
                :binder_template_id => binder_template_id, 
                :client => {
                    :first => client_first, 
                    :last => client_last, 
                    :email => client_email
                }, 
                :property => {
                    :address => property_address1, 
                    :address2 => property_address2, 
                    :city => property_city, 
                    :state => property_state, 
                    :postalcode => property_zip, 
                    :country => property_country
                }, 
                :agent => {
                    :name => agent_name, 
                    :contact => agent_contact, 
                    :phone => agent_phone, 
                    :email => agent_email
                }, 
                :documents => []
            }
                expect(request).to eq(parameters)
        end
        it "correctly parses the email from plain text" do
            plain_part = "API Key: #{api_key}\nPartner Name: #{partner_name}\nBinderTemplateId: #{binder_template_id}\nClient First: #{client_first}\nClient Last: #{client_last}\nClient Email: #{client_email}\nProperty Address:&nbsp;#{property_address1}\nProperty Address2:&nbsp;#{property_address2}\nProperty City: #{property_city}\nProperty State: #{property_state}\nProperty PostalCode: #{property_zip}\nProperty Country: #{property_country}\nAgent Name:&nbsp;#{agent_name}\nAgent Contact:&nbsp;#{agent_contact}\nAgent Phone:&nbsp;#{agent_phone}\nAgent Email: #{agent_email}\nAutomatic Transfer: false\n&nbsp;\n&nbsp;\n\n"
            email = {:multipart => false, :body => OpenStruct.new({:decoded => plain_part}), :attachments => []}
            request = Partner::Automation::EmailParser.new.parse(OpenStruct.new(email))
                parameters = {
                :api_key => api_key, 
                :key => partner_name, 
                :binder_template_id => binder_template_id, 
                :client => {
                    :first => client_first, 
                    :last => client_last, 
                    :email => client_email
                }, 
                :property => {
                    :address => property_address1, 
                    :address2 => property_address2, 
                    :city => property_city, 
                    :state => property_state, 
                    :postalcode => property_zip, 
                    :country => property_country
                }, 
                :agent => {
                    :name => agent_name, 
                    :contact => agent_contact, 
                    :phone => agent_phone, 
                    :email => agent_email
                }, 
                :documents => []
            }
            expect(request).to eq(parameters)
        end
        it "correctly parses the email from plain text containing html" do
            plain_part = "<p>API Key: #{api_key}</p>\n<p>Partner Name: #{partner_name}</p>\n<p>BinderTemplateId: #{binder_template_id}</p>\n<p>Client First: #{client_first}</p>\n<p>Client Last: #{client_last}</p>\n<p>Client Email: #{client_email}</p>\n<p>Property Address: #{property_address1}</p>\n<p>Property Address2: #{property_address2}</p>\n<p>Property City: #{property_city}</p>\n<p>Property State: #{property_state}</p>\n<p>Property PostalCode: #{property_zip}</p>\n<p>Property Country: #{property_country}</p>\n<p>Agent Name: #{agent_name}</p>\n<p>Agent Contact: #{agent_contact}</p>\n<p>Agent Phone:#{agent_phone}</p>\n<p>Agent Email: #{agent_email}</p>\n\n"
            email = {:multipart => false, :body => OpenStruct.new({:decoded => plain_part}), :attachments => []}
            request = Partner::Automation::EmailParser.new.parse(OpenStruct.new(email))
                parameters = {
                :api_key => api_key, 
                :key => partner_name, 
                :binder_template_id => binder_template_id, 
                :client => {
                    :first => client_first, 
                    :last => client_last, 
                    :email => client_email
                }, 
                :property => {
                    :address => property_address1, 
                    :address2 => property_address2, 
                    :city => property_city, 
                    :state => property_state, 
                    :postalcode => property_zip, 
                    :country => property_country
                }, 
                :agent => {
                    :name => agent_name, 
                    :contact => agent_contact, 
                    :phone => agent_phone, 
                    :email => agent_email
                }, 
                :documents => []
            }
            expect(request).to eq(parameters)
        end
        it "correctly parses the email from html" do
            html_part = "<span style=\"\" >API Key: #{api_key}<br/>Partner Name: #{partner_name}<br/>BinderTemplateID: #{binder_template_id}<br/>Client First: #{client_first}<br/>Client Last: #{client_last}<br/>Client Email: #{client_email}<br/>Property Address: #{property_address1}<br/>Property City: #{property_city}<br/>Property State: #{property_state}<br/>Property PostalCode: #{property_zip}<br/>Property Country: #{property_country}<br/>Agent Name: #{agent_name}<br/>Agent Contact: #{agent_contact}<br/>Agent Phone: #{agent_phone}<br/>Agent Email: #{agent_email}<br/>Automatic Transfer: true<br/><br/>Email sent by: admin<br/>1/17/2017 2:00:48 PM<br/><br/></span>"
            email = {:multipart => false, :body => OpenStruct.new({:decoded => nil}), :html_part => OpenStruct.new({:body => OpenStruct.new({:decoded => html_part})}), :attachments => []}
            request = Partner::Automation::EmailParser.new.parse(OpenStruct.new(email))
                parameters = {
                :api_key => api_key, 
                :key => partner_name, 
                :binder_template_id => binder_template_id, 
                :client => {
                    :first => client_first, 
                    :last => client_last, 
                    :email => client_email
                }, 
                :property => {
                    :address => property_address1, 
                    :city => property_city, 
                    :state => property_state, 
                    :postalcode => property_zip, 
                    :country => property_country
                }, 
                :agent => {
                    :name => agent_name, 
                    :contact => agent_contact, 
                    :phone => agent_phone, 
                    :email => agent_email
                }, 
                :documents => []
            }
            expect(request).to eq(parameters)
        end
        it "correctly parses the email from plain text using updated API" do
            plain_part = "Inspection Date: #{inspection_date}\nProject Number: #{project_number}\nTransaction Type: #{transaction_type}\nAPI Key:&nbsp;#{api_key}\nPartner Name:&nbsp;#{partner_name}\nPartner Email: #{partner_email}\nBinderTemplateId: #{binder_template_id}\nClient First: #{client_first}\nClient Last: #{client_last}\nClient Email: #{client_email}\nProperty Address: #{property_address1}\nProperty Address2: #{property_address2}\nProperty City: #{property_city}\nProperty State: #{property_state}\nProperty PostalCode: #{property_zip}\nProperty Country: #{property_country}\nBuyerAgent Name: #{agent_name}\nBuyerAgent First: #{agent_contact}\nBuyerAgent Phone: #{agent_phone}\nBuyerAgent Email: #{agent_email}\nSellerAgent Name: #{agent_name}\nSellerAgent First: #{agent_contact}\nSellerAgent Phone: #{agent_phone}\nSellerAgent Email: #{agent_email}\nAutomatic Transfer: true"
            email = {:multipart => false, :body => OpenStruct.new({:decoded => plain_part}), :attachments => []}
            request = Partner::Automation::EmailParser.new.parse(OpenStruct.new(email))
                parameters = {
                :api_key => api_key, 
                :key => partner_name, 
                :partner_email => partner_email,
                :binder_template_id => binder_template_id, 
                :client => {
                    :first => client_first, 
                    :last => client_last, 
                    :email => client_email
                }, 
                :property => {
                    :address => property_address1, 
                    :address2 => property_address2, 
                    :city => property_city, 
                    :state => property_state, 
                    :postalcode => property_zip, 
                    :country => property_country
                }, 
                :buyer_agent => {
                    :name => agent_name, 
                    :first => agent_contact, 
                    :phone => agent_phone, 
                    :email => agent_email
                }, 
                :seller_agent => {
                    :name => agent_name, 
                    :first => agent_contact, 
                    :phone => agent_phone, 
                    :email => agent_email
                }, 
                :inspection_date => "#{inspection_date.to_s}\n", 
                :project_number => project_number, 
                :transaction_type => transaction_type,
                :documents => []
            }
                expect(request).to eq(parameters)
        end
        it "correctly parses the email from plain text using updated API for property photo" do
            plain_part = "Property PhotoURL: https://dummyimage.com/300/09f/fff.png\nAPI Key:&nbsp;#{api_key}\nPartner Name:&nbsp;#{partner_name}\nPartner Email: #{partner_email}\nBinderTemplateId: #{binder_template_id}\nClient First: #{client_first}\nClient Last: #{client_last}\nClient Email: #{client_email}\nProperty Address: #{property_address1}\nProperty Address2: #{property_address2}\nProperty City: #{property_city}\nProperty State: #{property_state}\nProperty PostalCode: #{property_zip}\nProperty Country: #{property_country}\nBuyerAgent Name: #{agent_name}\nBuyerAgent First: #{agent_contact}\nBuyerAgent Phone: #{agent_phone}\nBuyerAgent Email: #{agent_email}\nSellerAgent Name: #{agent_name}\nSellerAgent First: #{agent_contact}\nSellerAgent Phone: #{agent_phone}\nSellerAgent Email: #{agent_email}\nSellerAgent Logo URL: https://dummyimage.com/300/09f/fff.png"
            email = {:multipart => false, :body => OpenStruct.new({:decoded => plain_part}), :attachments => []}
            email[:attachments].push(OpenStruct.new({:content_type => 'image/png', :decoded => File.open("spec/assets/SampleImage.png", "rb").read}))
            request = Partner::Automation::EmailParser.new.parse(OpenStruct.new(email))
                expect(request[:property_photo]).to_not be(nil)
            expect(request[:property_photo][:url]).to eq("https://dummyimage.com/300/09f/fff.png")
            expect(request[:seller_agent][:logo][:url]).to eq("https://dummyimage.com/300/09f/fff.png")
        end
        it "correctly parses the email from plain text using updated API for agent logo and headshot" do
            plain_part = "API Key:&nbsp;#{api_key}\nPartner Name:&nbsp;#{partner_name}\nPartner Email: #{partner_email}\nBinderTemplateId: #{binder_template_id}\nClient First: #{client_first}\nClient Last: #{client_last}\nClient Email: #{client_email}\nProperty Address: #{property_address1}\nProperty Address2: #{property_address2}\nProperty City: #{property_city}\nProperty State: #{property_state}\nProperty PostalCode: #{property_zip}\nProperty Country: #{property_country}\nProperty YearBuilt: 1929\nProperty Squarefootage: 2500\nBuyerAgent Name: #{agent_name}\nBuyerAgent First: #{agent_contact}\nBuyerAgent Phone: #{agent_phone}\nBuyerAgent Email: #{agent_email}\nBuyerAgent HeadShot URL: https://dummyimage.com/300/09f/fff.png\nBuyerAgent HeadShot Logo: https://dummyimage.com/300/09f/fff.png\nSellerAgent Name: #{agent_name}\nSellerAgent First: #{agent_contact}\nSellerAgent Phone: #{agent_phone}\nSellerAgent Email: #{agent_email}\nSellerAgent HeadShot URL: https://dummyimage.com/300/09f/fff.png\nSellerAgent HeadShot Logo: https://dummyimage.com/300/09f/fff.png\nAutomatic Transfer: true"
            email = {:multipart => false, :body => OpenStruct.new({:decoded => plain_part}), :attachments => []}
            email[:attachments].push(OpenStruct.new({:content_type => 'image/png', :decoded => File.open("spec/assets/SampleImage.png", "rb").read}))
            request = Partner::Automation::EmailParser.new.parse(OpenStruct.new(email))
                expect(request[:seller_agent][:headshot]).to_not be(nil)
            expect(request[:seller_agent][:headshot][:url]).to eq("https://dummyimage.com/300/09f/fff.png")
            expect(request[:seller_agent][:headshot][:logo]).to eq("https://dummyimage.com/300/09f/fff.png")
        end
    end
    
    describe "#getParams" do
        it "returns the correct params from the array" do
            array = ["API Key: 5fzf1hgi8c4kolwf3yqe", "Partner Name: test", "BinderTemplateId: 275", "Client First: Bob", "Client Last: Smith", "Client Email: bobsmith@gmail.com", "Property Address: 320 Main Street", "Property Address2: Apt 2", "Property City: Springfield", "Property State: MO", "Property PostalCode: ", "Property Country: US", "Agent Name: Keller Williams", "Agent Contact: Kasey Stevenson", "Agent Phone:333-333-3333", "Agent Email: kstevens@gmail.com"]
            parameters = Partner::Automation::EmailParser.new.getParams(array)

            list =  ["API Key: 5fzf1hgi8c4kolwf3yqe", "Partner Name: test", "BinderTemplateId: 275", "Client First: Bob", "Client Last: Smith", "Client Email: bobsmith@gmail.com", "Property Address: 320 Main Street", "Property Address2: Apt 2", "Property City: Springfield", "Property State: MO", "Property PostalCode: ", "Property Country: US", "Agent Name: Keller Williams", "Agent Contact: Kasey Stevenson", "Agent Phone:333-333-3333", "Agent Email: kstevens@gmail.com"]
                expect(parameters).to eq(list)
        end
        it "returns the correct params from the array" do
            array = ["API Key:&nbsp;cna0xqsclkalbmm7mb05", "Partner Name:&nbsp;test", "BinderTemplateId: 547", "Client First: Bob", "Client Last: Smith", "Client Email: bobsmith@gmail.com", "Property Address: 320 Main Street", "Property Address2: Apt 2", "Property City: Springfield", "Property State: MO", "Property PostalCode: ", "Property Country: US", "Agent Name: Keller Williams", "Agent Contact: Kasey Stevenson", "Agent Phone: 333-333-3333", "Agent Email: kstevens@gmail.com", "Automatic Transfer: true"]
            parameters = Partner::Automation::EmailParser.new.getParams(array)
            list = ["API Key:&nbsp;cna0xqsclkalbmm7mb05", "Partner Name:&nbsp;test", "BinderTemplateId: 547", "Client First: Bob","Client Last: Smith", "Client Email: bobsmith@gmail.com", "Property Address: 320 Main Street", "Property Address2: Apt 2", "Property City: Springfield", "Property State: MO", "Property PostalCode: ", "Property Country: US", "Agent Name: Keller Williams", "Agent Contact: Kasey Stevenson", "Agent Phone: 333-333-3333", "Agent Email: kstevens@gmail.com"]
            expect(parameters).to eq(list)
        end
    end
    
    describe "#variables" do
        it "includes the right variables" do
            variables = Partner::Automation::EmailParser.new.variables
                api = variables.include?("API")
            partner = variables.include?("Partner")
            client = variables.include?("Client")
            property = variables.include?("Property")
            template = variables.include?("BinderTemplate")
            agent = variables.include?("Agent")
                expect(api).to eq(true)
            expect(partner).to eq(true)
            expect(client).to eq(true)
            expect(property).to eq(true)
            expect(template).to eq(true)
            expect(agent).to eq(true)
        end
    end
    
    describe "#format_value" do
        it "should format the email correctly and remove the comma" do
            value = Partner::Automation::EmailParser.new.format_value(["client", "email"], "test@gmail.com, test.test.test@gmail.com")
            expect(value).to eq("test@gmail.com")
        end
        it "should format the email correctly and remove the semi-colon" do
            value = Partner::Automation::EmailParser.new.format_value(["client", "email"], "test@gmail.com; test.test.test@gmail.com")
            expect(value).to eq("test@gmail.com")
        end
        it "should format the email correctly and remove the semi-colon and equal sign" do
            value = Partner::Automation::EmailParser.new.format_value(["client", "email"], "test@gmail.com=; test.test.test@gmail.com=")
            expect(value).to eq("test@gmail.com")
        end
        it "should format the email correctly and remove the second email and semi colon" do
            value = Partner::Automation::EmailParser.new.format_value(["agent", "email"], "monkey@test.com;monkey@test.com")
            expect(value).to eq("monkey@test.com")
        end
        it "should format the email correctly and remove the second email and comma" do
            value = Partner::Automation::EmailParser.new.format_value(["agent", "email"], "monkey@test.com,monkey@test.com")
            expect(value).to eq("monkey@test.com")
        end
        it "should format the email correctly and return an empty string" do
            value = Partner::Automation::EmailParser.new.format_value(["agent", "email"], "NOT PROVIDED")
            expect(value).to eq("")
        end
        it "should format the email correctly and return an empty string" do
            value = Partner::Automation::EmailParser.new.format_value(["agent", "email"], " \r")
            expect(value).to eq("")
        end
        it "should format the email correctly and return an empty string" do
            value = Partner::Automation::EmailParser.new.format_value(["agent", "email"], " \n")
            expect(value).to eq("")
        end
    end
    
    describe "#formatParams" do
        it "should correctly format the array into a hash" do
            parameters = ["API Key: b3e6f2e29d67494882a48cc1f26579fa\r", "Partner Name: axium\r", "Client First: Jack\r", "Client Last: Frost\r", "Client Email: jackfrost@gmail.com;monkey@test.com\r", "Property Address: 321 Summer Street\r", "Property Address2: \r", "Property City: Boston\r", "Property State: MA\r", "Property PostalCode: 02210\r", "Property Country: US\r", "BinderTemplateId: 1\r"]
            hash = Partner::Automation::EmailParser.new.formatParams(parameters)
            expect(hash).to eq({:api_key=>"b3e6f2e29d67494882a48cc1f26579fa", :key=>"axium", :client=>{:first=>"Jack", :last=>"Frost", :email=>"jackfrost@gmail.com"}, :property=>{:address=>"321 Summer Street", :address2=>"", :city=>"Boston", :state=>"MA", :postalcode=>"02210", :country=>"US"}, :binder_template_id=>"1"})
        end
    end
    
    describe "#formatKeys" do
        it "should return an array with two values" do
            keys = "API Key"
            array = Partner::Automation::EmailParser.new.formatKeys(keys)
                expect(array[0]).to eq("api")
            expect(array[1]).to eq("key")
        end
        it "should return an array with one value" do
            keys = "BinderTemplateId"
            array = Partner::Automation::EmailParser.new.formatKeys(keys)
                expect(array[0]).to eq("bindertemplateid")
        end
    end
    
    describe "#attachReport" do
        it "should not add a document to the request" do
            request = {}
            email = OpenStruct.new({:attachments => []})
            email.attachments.push(OpenStruct.new({:content_type => 'image/png', :decoded => File.open("spec/assets/SampleImage.png", "rb").read}))
                request = Partner::Automation::EmailParser.new.attachReport(request, email)
            expect(request[:documents].length).to eq(0)
        end
        it "should add a document to the request" do
            request = {}
            email = OpenStruct.new({:attachments => []})
            email.attachments.push(OpenStruct.new({:content_type => 'application/pdf', :decoded => File.open("spec/assets/SampleDoc.pdf", "rb").read}))
                request = Partner::Automation::EmailParser.new.attachReport(request, email)
            expect(request[:documents].length).to eq(1)
        end
    end

    describe "#attachImage" do
        it "should add an image to the request" do
            request = {}
            email = OpenStruct.new({:attachments => []})
            email.attachments.push(OpenStruct.new({:content_type => 'image/png', :decoded => File.open("spec/assets/SampleImage.png", "rb").read}))
            email.attachments.push(OpenStruct.new({:content_type => 'image/png', :decoded => File.open("spec/assets/SampleImage.png", "rb").read}))
            email.attachments.push(OpenStruct.new({:content_type => 'image/png', :decoded => File.open("spec/assets/SampleImage.png", "rb").read}))
            email.attachments.push(OpenStruct.new({:content_type => 'image/png', :decoded => File.open("spec/assets/SampleImage.png", "rb").read}))
            email.attachments.push(OpenStruct.new({:content_type => 'image/png', :decoded => File.open("spec/assets/SampleImage.png", "rb").read}))
            request = Partner::Automation::EmailParser.new.attachImage(request, email)
                expect(request[:property_photo]).to_not be(nil)
            expect(request[:additional_images].length).to be(4)
        end
        it "should not add an image to the request" do
            request = {}
            email = OpenStruct.new({:attachments => []})
            email.attachments.push(OpenStruct.new({:content_type => 'application/pdf', :decoded => File.open("spec/assets/SampleDoc.pdf", "rb").read}))
                request = Partner::Automation::EmailParser.new.attachImage(request, email)
            expect(request[:property_photo]).to be(nil)
        end
    end

end