require 'rails_helper'

RSpec.describe Partner::Automation::Error, :type => :model do
    let(:partner) { FactoryBot.create(:partner, name: "Test Inspections", contact: "Test Inspector", partner_key: "axium", :partner_type => "inspector") }
    let(:user) { FactoryBot.create(:user, email: partner.email, :role => "inspector") }
    let(:admin) { FactoryBot.create(:user, :role => "admin") }
    let(:config) { partner.partner_configuration}
    let(:template) { FactoryBot.create(:binder_template, partner_configuration_id: config.id) }

    describe 'when validating' do
        let(:automation_error) { build_stubbed(:automation_error) }

        Partner::Automation::Error.attribute_names.each do |att|
            it { expect(automation_error).to validate_length_of(att.to_sym) }
        end
    end

    before :each do
        user.add_role :partner_admin, partner
        config.default_user_branding_id = user.user_profile.id
        config.save!
    end

    describe "build" do
        it "creates a new ABS error" do
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}
            parameters = {
                :key => "axium",
                :client => client,
                :property => property,
                :binder_template_id => template.id,
                :agent => agent
            }
            Partner::Automation::Error.build(parameters, "Property Information required")
            error = Partner::Automation::Error.last
            expect(error.partner_name).to eq(parameters[:key])
        end
    end

    describe "update" do
        it "updates an ABS error and creates a binder" do
            image = {file: "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read), name: "SampleImage.png"}
            files = [{file: "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read), name: "SampleDoc.pdf"}]

            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}
            parameters = {
                :key => "axium",
                :client => client,
                :property => property,
                :binder_template_id => template.id,
                :agent => agent,
                :documents => files,
                :property_photo => image
            }

            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

            Partner::Automation::Error.build(parameters, "Property Information required")
            error = Partner::Automation::Error.last
            request = HBRequest.new.create_request(admin)
            Partner::Automation::Error.update(request, {:id => error.id, :error => error.attributes.symbolize_keys})
            binder = Binder.last
            expect(binder.name).to eq("Mike Breen Home")
        end
    end

    describe "destroy" do
        it "destroy an ABS error" do
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}
            parameters = {
                :key => "axium",
                :client => client,
                :property => property,
                :binder_template_id => template.id,
                :agent => agent
            }
            request = HBRequest.new.create_request(admin)
            Partner::Automation::Error.build(parameters, "Property Information required")
            error = Partner::Automation::Error.last
            Partner::Automation::Error.destroy(request, error.id)
            object = Partner::Automation::Error.find_by_id(error.id)
            expect(object).to be(nil)
        end
    end
end
