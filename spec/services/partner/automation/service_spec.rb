require 'rails_helper'

RSpec.describe Partner::Automation::Service, :type => :model do
    let(:messaging) {Slack::Messaging.new}

    before :each do
        create(:contractor_contractor_category, :name => "Real Estate")
        create(:contractor_contractor_category, :name => "Home Inspector")
        @partner = create(:partner, partner_key: "test")
        @user = create(:user, email: @partner.email, :role => "inspector")
        create(:user_profile, :user_id => @user.id)
        @binder = create(:binder)
        @user.add_role :partner_admin, @partner
        @user.add_role :owner, @binder
        @config = @partner.partner_configuration
        @config.default_user_branding_id = @user.user_profile.id
        @config.save
        @template = create(:binder_template, partner_configuration_id: @config.id, transfer_note: "This is a transfer note")
        create(:appliance_template, binder_template_id: @template.id)
        partner_contractor = create(:partner_contractor, :notes => "THIS IS A NOTE")
        create(:contractor_template, binder_template_id: @template.id, :partner_contractor => partner_contractor)
        create(:document_template, binder_template_id: @template.id)
        create(:maintenance_template, binder_template_id: @template.id, due_date: nil, frequency: "As Needed", initial_due_date_interval: 200)

        allow(ErrorService).to receive(:perform_async)
    end

    describe "#verify_duplicate" do
        it "raises an error because binder property already exists and user is owner" do
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}

            parameters = {
                :key => "test",
                :client => client,
                :property => property,
                :binder_template_id => @template.id,
                :agent => agent,
                :property_photo => nil,
                :documents => []
            }

            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

            # create the binder
            binder = Partner::Automation::Service.create_binder(parameters)

            # remove current owner and set the homeowner as owner
            UserBinder.where(:binder_id => binder[:id], :role => "owner").destroy_all
            homeowner = User.find_by_email("transfers@homebinder.com")
            UserBinder.create(:binder_id => binder[:id], :role => "owner", :user_id => homeowner.id)

            address1 = parameters[:property][:address]
            address2 = parameters[:property][:address2].present? ? parameters[:property][:address2] : "".freeze
            city = parameters[:property][:city]
            state = parameters[:property][:state]
            postalcode = parameters[:property][:postalcode].present? ? parameters[:property][:postalcode]: "".freeze
            country = parameters[:property][:country]
            binder_property = "#{address1} #{address2}, #{city}, #{state} #{postalcode} #{country}"

            message = Partner::Automation::Service::DUPLICATE_BINDER_PROPERTY % binder_property

            # then try verify property already exists
            expect {Partner::Automation::Service.verify_duplicate(parameters)}.to raise_error BadRequestException, message
        end

        it "raises an error because binder property already exists and user is impending owner" do
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}

            parameters = {
                :key => "test",
                :client => client,
                :property => property,
                :binder_template_id => @template.id,
                :agent => agent,
                :property_photo => nil,
                :documents => []
            }

            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

            # create the binder
            binder = Partner::Automation::Service.create_binder(parameters)

            binder = Binder.find(binder[:id])

            address1 = parameters[:property][:address]
            address2 = parameters[:property][:address2].present? ? parameters[:property][:address2] : "".freeze
            city = parameters[:property][:city]
            state = parameters[:property][:state]
            postalcode = parameters[:property][:postalcode].present? ? parameters[:property][:postalcode]: "".freeze
            country = parameters[:property][:country]
            binder_property = "#{address1} #{address2}, #{city}, #{state} #{postalcode} #{country}"

            message = Partner::Automation::Service::DUPLICATE_BINDER_PROPERTY % binder_property

            # then try verify property already exists
            expect {Partner::Automation::Service.verify_duplicate(parameters)}.to raise_error BadRequestException, message
        end

        it "raises an error because binder property already exists in 72 hour window even when theres no pending transfers/users" do
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}

            parameters = {
                :key => "test",
                :client => client,
                :property => property,
                :binder_template_id => @template.id,
                :agent => agent,
                :property_photo => nil,
                :documents => []
            }

            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

            # create the binder
            binder = Partner::Automation::Service.create_binder(parameters)

            binder = Binder.find(binder[:id])

            address1 = parameters[:property][:address]
            address2 = parameters[:property][:address2].present? ? parameters[:property][:address2] : "".freeze
            city = parameters[:property][:city]
            state = parameters[:property][:state]
            postalcode = parameters[:property][:postalcode].present? ? parameters[:property][:postalcode]: "".freeze
            country = parameters[:property][:country]
            binder_property = "#{address1} #{address2}, #{city}, #{state} #{postalcode} #{country}"

            message = Partner::Automation::Service::DUPLICATE_BINDER_PROPERTY % binder_property

            binder.user_binders.destroy_all
            binder.transfers.destroy_all

            # then try verify property already exists
            expect {Partner::Automation::Service.verify_duplicate(parameters)}.to raise_error BadRequestException, message
        end

        it "does not raise an error because binder property already exists" do
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}

            parameters = {
                :key => "test",
                :client => client,
                :property => property,
                :binder_template_id => @template.id,
                :agent => agent,
                :property_photo => nil,
                :documents => []
            }
            expect {Partner::Automation::Service.verify_duplicate(parameters)}.to_not raise_error
        end
    end

    describe "#verify_state_and_country" do
        it "raises an error because there is no country" do
            expect {Partner::Automation::Service.verify_state_and_country({:property => {:country => ""}})}.to raise_error BadRequestException
        end

        it "raises an error because state is empty" do
            expect {Partner::Automation::Service.verify_state_and_country({:property => {:country => "United States", :state => ""}})}.to raise_error BadRequestException
        end

        it "returns state and country for US uppercase" do
            params = Partner::Automation::Service.verify_state_and_country({:property => {:country => "US", :state => "Massachusetts"}})
            expect(params[:property][:country]).to eq("US")
            expect(params[:property][:state]).to eq("MA")
        end

        it "returns state and country for US downcase" do
            params = Partner::Automation::Service.verify_state_and_country({:property => {:country => "us", :state => "Massachusetts"}})
            expect(params[:property][:country]).to eq("US")
            expect(params[:property][:state]).to eq("MA")
        end

        it "returns state and country for USA uppercase" do
            params = Partner::Automation::Service.verify_state_and_country({:property => {:country => "USA", :state => "Massachusetts"}})
            expect(params[:property][:country]).to eq("US")
            expect(params[:property][:state]).to eq("MA")
        end

        it "returns state and country for USA downcased" do
            params = Partner::Automation::Service.verify_state_and_country({:property => {:country => "usa", :state => "Massachusetts"}})
            expect(params[:property][:country]).to eq("US")
            expect(params[:property][:state]).to eq("MA")
        end

        it "returns state and country for United States downcased" do
            params = Partner::Automation::Service.verify_state_and_country({:property => {:country => "united states", :state => "Massachusetts"}})
            expect(params[:property][:country]).to eq("US")
            expect(params[:property][:state]).to eq("MA")
        end

        it "returns state and country for United States uppercase" do
            params = Partner::Automation::Service.verify_state_and_country({:property => {:country => "UNITED STATES", :state => "Massachusetts"}})
            expect(params[:property][:country]).to eq("US")
            expect(params[:property][:state]).to eq("MA")
        end

        it "returns state and country for United States titleized" do
            params = Partner::Automation::Service.verify_state_and_country({:property => {:country => "United States", :state => "Massachusetts"}})
            expect(params[:property][:country]).to eq("US")
            expect(params[:property][:state]).to eq("MA")
        end

        it "returns state and country for CA" do
            params = Partner::Automation::Service.verify_state_and_country({:property => {:country => "CA", :state => "Ontario"}})
            expect(params[:property][:country]).to eq("CA")
            expect(params[:property][:state]).to eq("ON")
        end
    end

    describe "#create_image" do
        it "does not create image" do
            allow_any_instance_of(Binder::Image).to receive(:save).and_return(false)

            Partner::Automation::Service.create_image(@binder.id, {file: "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read), name: "SampleImage.png"})
            @binder.reload

            expect(@binder.images.length).to eq(0)
            expect(ErrorService).to have_received(:perform_async).once
        end

        it "create image" do
            Partner::Automation::Service.create_image(@binder.id, {file: "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read), name: "SampleImage.png"})
            @binder.reload

            expect(@binder.images.length).to eq(1)
            expect(@binder.images[0].file_file_name).to eq("SampleImage.png")
            expect(@binder.hero_image_id).to eq(@binder.images[0].id)
        end

        it "does not create an image" do
            allow_any_instance_of(Binder::Image).to receive(:save).and_return(false)
            allow(URI).to receive(:parse).and_return(File.open('spec/assets/SampleDoc.pdf'))
            Partner::Automation::Service.create_image(@binder.id, {url: "fakeurl"})
            @binder.reload

            expect(@binder.images.length).to eq(0)
            expect(@binder.hero_image_id).to be(nil)
        end

        it "create image" do
            allow(Partner::Automation::Service).to receive(:image_url).and_return(File.new('spec/assets/SampleImage.png'))
            Partner::Automation::Service.create_image(@binder.id, {url: "fakeurl"})
            @binder.reload

            expect(@binder.images.length).to eq(1)
            expect(@binder.images[0].file_file_name).to eq("SampleImage.png")
            expect(@binder.hero_image_id).to eq(@binder.images[0].id)
        end

        it "does not create an image" do
            Partner::Automation::Service.create_image(@binder.id, {file: {}})
            @binder.reload

            expect(@binder.images.length).to eq(0)
            expect(@binder.hero_image_id).to be(nil)
        end

        it "does not create an image when url is https://s3.amazonaws.com/isn-cdn/images/no-propertyfound.png" do
            Partner::Automation::Service.create_image(@binder.id, {url: "https://s3.amazonaws.com/isn-cdn/images/no-propertyfound.png"})
            @binder.reload

            expect(@binder.images.length).to eq(0)
            expect(@binder.hero_image_id).to be(nil)
        end

        it "raises an error" do
            allow_any_instance_of(Binder::Image).to receive(:save).and_return(false)
            Partner::Automation::Service.create_image(@binder.id, {file: "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read), name: "SampleDoc.pdf"})

            expect(ErrorService).to have_received(:perform_async)
        end
    end

    describe "#create_documents" do
        it "does not create a document" do
            allow_any_instance_of(Binder::Document).to receive(:save).and_return(false)

            files = [{file: "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read), name: "SampleDoc.pdf"}]

            Partner::Automation::Service.create_documents(@binder.id, files)
            @binder.reload

            expect(@binder.documents.length).to eq(0)
            expect(ErrorService).to have_received(:perform_async).once
        end

        it "creates a document" do
            files = [{file: "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read), name: "SampleDoc.pdf"}]

            Partner::Automation::Service.create_documents(@binder.id, files)
            @binder.reload

            expect(@binder.documents.length).to eq(1)
            expect(@binder.documents[0].file_file_name).to eq("SampleDoc.pdf")
        end

        it "does not create a document" do
            Partner::Automation::Service.create_documents(@binder.id, [])
            @binder.reload

            expect(@binder.documents.length).to eq(0)
        end

        it "does not create the duplicate document" do
            expect(@binder.documents.length).to eq(0)

            # add documents to binder
            Partner::Automation::Service.delay.add_template_docs(@binder.id, @template.id)

            @binder.reload
            expect(@binder.documents.length).to eq(1)

            files = [{file: "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read), name: @binder.documents.first.file_file_name}]

            Partner::Automation::Service.create_documents(@binder.id, files)
            @binder.reload

            expect(@binder.documents.length).to eq(1)
        end
    end

    describe "#set_binder_name" do
        it "set the binder name to the client first and last name" do
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            params = {
                :client => client
            }
            binder_name = Partner::Automation::Service.set_binder_name(params)
            expect(binder_name).to eq("Mike Breen Home")
        end

        it "set the binder name to My Home" do
            client =  {first: Faker::Lorem.characters(25), last: Faker::Lorem.characters(25), email: "transfers@homebinder.com"}
            params = {
                :client => client
            }
            binder_name = Partner::Automation::Service.set_binder_name(params)
            expect(binder_name).to eq("My Home")
        end
    end

    describe "#verify_params" do
        it "raises an error" do
            expect {Partner::Automation::Service.verify_params({client: {}}) }.to raise_error BadRequestException, "Client information not included"
        end

        it "raises an error" do
            expect {Partner::Automation::Service.verify_params({client: {first: ""}}) }.to raise_error BadRequestException, "Property information not included"
        end

        it "raises an error" do
            expect {Partner::Automation::Service.verify_params({client: {first: ""}, property: {city: ""}}) }.to raise_error BadRequestException, "Client First Name required"
        end

        it "raises an error" do
            params = {client: {first: "John", email: "john@gmail.com"}, property: {address: ""}}
            expect {Partner::Automation::Service.verify_params(params) }.to raise_error BadRequestException, "Property Address required"
        end

        it "raises an error" do
            params = {client: {first: "John", email: "john@gmail.com"}, property: {address: "123 main street"}}
            expect {Partner::Automation::Service.verify_params(params) }.to raise_error BadRequestException, "Property City required"
        end

        it "raises an error" do
            params = {client: {first: "John", email: "john@gmail.com"}, property: {address: "123 main street", city: "Boston"}}
            expect {Partner::Automation::Service.verify_params(params) }.to raise_error BadRequestException, "Property State required"
        end

        it "raises an error" do
            params = {client: {first: "John", email: "john@gmail.com"}, property: {address: "123 main street", city: "Boston", state: "MA", postalcode: "02210", country: ""}}
            expect {Partner::Automation::Service.verify_params(params) }.to raise_error BadRequestException, "Property Country required"
        end

        it "raises an error" do
            params = {client: {first: "John", email: "john@gmail.com"}, property: {address: "123 main street", city: "Boston", state: "MA", postalcode: "02210", country: "US"}}
            expect {Partner::Automation::Service.verify_params(params) }.to raise_error BadRequestException, "Binder Template ID required"
        end

        it "sets the date to nil" do
            params = {
                client: {
                    first: "John", email: "john@gmail.com"

                },
                property: {
                    address: "123 main street", city: "Boston", state: "MA", postalcode: "02210", country: "US"

                },
                binder_template_id: 1,
                inspection_date: "fakedate"
            }
            Partner::Automation::Service.verify_params(params)

            expect(params[:inspection_date]).to eq(nil)
        end
    end

    describe "#verify_email" do
        it "changes the invalid client email to orphans@homebinder.com" do
            params = {client: {first: "John", email: "john@gmail.com#9;"}, property: {address: "123 main street", city: "Boston", state: "MA", postalcode: "02210", country: "US", binder_template_id: @template.id}}
            Partner::Automation::Service.verify_email(params)
            expect(params[:client][:email]).to eq("orphans@homebinder.com")
        end

        it "changes the invalid client email to orphans@homebinder.com" do
            params = {client: {first: "John", email: "john*!@gmail.com#9,"}, property: {address: "123 main street", city: "Boston", state: "MA", postalcode: "02210", country: "US", binder_template_id: @template.id}}
            Partner::Automation::Service.verify_email(params)
            expect(params[:client][:email]).to eq("orphans@homebinder.com")
        end

        it "changes the invalid client email to orphans@homebinder.com" do
            params = {client: {first: "John", email: "john*!@gmail.gov#9,!"}, property: {address: "123 main street", city: "Boston", state: "MA", postalcode: "02210", country: "US", binder_template_id: @template.id}}
            Partner::Automation::Service.verify_email(params)
            expect(params[:client][:email]).to eq("orphans@homebinder.com")
        end

        it "changes the invalid agent email to nil" do
            agent = {email: "invalidagentemail@gmail.com!0;"}
            params = {client: {first: "John", email: "john@gmail.com"}, property: {address: "123 main street", city: "Boston", state: "MA", postalcode: "02210", country: "US", binder_template_id: @template.id}, agent: agent}
            Partner::Automation::Service.verify_email(params)
            expect(params[:client][:email]).to eq("john@gmail.com")
            expect(params[:agent][:email]).to be(nil)
        end
    end

    describe "#add_as_contractor" do
        it "sends an error to New Relic" do
            allow(Binder::BinderContractor).to receive(:build).and_raise(BadRequestException.new("Test"))

            cont = OpenStruct.new({
                name: @partner.contact,
                phone: @partner.phone,
                email: @partner.email,
                contractor_id: @user.contractor.id
            })

            expect{Partner::Automation::Service.add_as_contractor(cont, @user.id, @binder.id, [{contractor_category_id: create(:contractor_contractor_category).id}], [])}.to raise_error BadRequestException
        end

        it "creates a binder contractor" do
            cont = OpenStruct.new({
                name: @partner.contact,
                phone: @partner.phone,
                email: @partner.email,
                contractor_id: @user.contractor.id
            })

            Partner::Automation::Service.add_as_contractor(cont, @user.id, @binder.id, [{contractor_category_id: create(:contractor_contractor_category).id}], [])
            @binder.reload

            expect(@binder.binder_contractors.length).to eq(1)
            expect(@binder.binder_contractors[0].contact).to eq(@partner.contact)
            expect(@binder.binder_contractors[0].contractor.phone).to eq(@partner.phone)
        end

        it "creates a binder contractor with the phone number set to nil" do
            cont = OpenStruct.new({
                name: @partner.contact,
                phone: "239 405-4400 239 285-1666",
                email: @partner.email,
                contractor_id: @user.contractor.id
            })

            Partner::Automation::Service.add_as_contractor(cont, @user.id, @binder.id, [{contractor_category_id: create(:contractor_contractor_category).id}], [])
            @binder.reload

            expect(@binder.binder_contractors.length).to eq(1)
            expect(@binder.binder_contractors[0].contact).to eq(@partner.contact)
            expect(@binder.binder_contractors[0].contractor.phone).to_not eq(nil)
        end

        it "creates a binder contractor with the phone number set to nil" do
            cont = OpenStruct.new({
                name: @partner.contact,
                phone: "970 371-8245",
                email: @partner.email,
                contractor_id: @user.contractor.id
            })

            Partner::Automation::Service.add_as_contractor(cont, @user.id, @binder.id, [{contractor_category_id: create(:contractor_contractor_category).id}], [])
            @binder.reload

            expect(@binder.binder_contractors.length).to eq(1)
            expect(@binder.binder_contractors[0].contact).to eq(@partner.contact)
        end
    end

    describe "#create_agent" do
        it "sends an error to New Relic" do
            allow(User).to receive(:build).and_raise(BadRequestException.new("Test"))

            agent = {name: "Keller Williams", contact: "Carmen Jones", email: "carmenjones@gmail.com", phone: "781-429-3034"}
            expect{Partner::Automation::Service.create_agents(@binder.id, {:agent => agent}, @partner.id, @user.id)}.to raise_error BadRequestException
        end

        it "sends an error to New Relic" do
            allow(User).to receive(:build).and_raise(UnprocessableException.new(@user))

            agent = {name: "Keller Williams", contact: "Carmen Jones", email: "carmenjones@gmail.com", phone: "781-429-3034"}
            expect{Partner::Automation::Service.create_agents(@binder.id, {:agent => agent}, @partner.id, @user.id)}.to raise_error UnprocessableException
        end

        it "does not call error service" do
            allow(User).to receive(:build).and_raise(BadRequestException.new("404 Not Found"))

            agent = {name: "Keller Williams", contact: "Carmen Jones", email: "carmenjones@gmail.com", phone: "781-429-3034"}
            expect{Partner::Automation::Service.create_agents(@binder.id, {:agent => agent}, @partner.id, @user.id)}.to raise_error BadRequestException
        end

        it "creates an agent and adds them to binder as a contractor" do
            agent = {name: "Keller Williams", contact: "Carmen Jones", email: "carmenjones@gmail.com", phone: "781-429-3034"}
            Partner::Automation::Service.create_agents(@binder.id, {:agent => agent}, @partner.id, @user.id)
            @binder.reload

            expect(@binder.binder_contractors.length).to eq(1)
            expect(@binder.binder_contractors[0].contact).to eq("Carmen Jones")
            expect(@binder.binder_contractors[0].contractor.phone).to eq("+17814293034")
            expect(@binder.binder_contractors[0].contractor.email).to eq("carmenjones@gmail.com")

            agent = User.where(:role => "agent").last

            expect(agent.user_profile.company).to eq("Keller Williams")
            expect(agent.user_profile.first_name).to eq("Carmen")
            expect(agent.user_profile.last_name).to eq("Jones")
            expect(agent.email).to eq("carmenjones@gmail.com")
            expect(agent.user_profile.mobile_phone).to eq("+17814293034")
            expect(User.where(:role => "agent").last.user_profile.display_name).to eq("Carmen Jones")
        end

        it "does not create an agent" do
            agent = {name: "NOT PROVIDED", contact: "NOT PROVIDED", email: "carmenjones@gmail.com", phone: "NOT PROVIDED"}
            Partner::Automation::Service.create_agents(@binder.id, {:agent => agent}, @partner.id, @user.id)
            @binder.reload

            expect(@binder.binder_contractors.length).to eq(0)
            expect(User.where(:role => "agent").count).to eq(0)
        end

        it "does not create an agent" do
            agent = {name: false, contact: "Name", email: "carmenjones@gmail.com", phone: "NOT PROVIDED"}
            Partner::Automation::Service.create_agents(@binder.id, {:agent => agent}, @partner.id, @user.id)
            @binder.reload

            expect(@binder.binder_contractors.length).to eq(1)
        end

        it "does not create an agent" do
            agent = {name: "name", contact: false, email: "carmenjones@gmail.com", phone: "NOT PROVIDED"}
            Partner::Automation::Service.create_agents(@binder.id, {:agent => agent}, @partner.id, @user.id)
            @binder.reload

            expect(@binder.binder_contractors.length).to eq(1)
        end

        it "it creates agent with name and contact as same values when name is empty" do
            agent = {name: "", contact: "Carmen Jones", email: "carmenjones@gmail.com", phone: "781-429-3034"}
            Partner::Automation::Service.create_agents(@binder.id, {:agent => agent}, @partner.id, @user.id)
            @binder.reload

            expect(@binder.binder_contractors[0].contact).to eq("Carmen Jones")
            expect(@binder.binder_contractors[0].contractor.name).to eq("Carmen Jones".downcase)
            expect(User.where(:role => "agent").last.user_profile.display_name).to eq("Carmen Jones")
        end

        it "it creates agent with name and contact as same values when contact is empty" do
            agent = {name: "Carmen Jones", contact: "", email: "carmenjones@gmail.com", phone: "781-429-3034"}
            Partner::Automation::Service.create_agents(@binder.id, {:agent => agent}, @partner.id, @user.id)
            @binder.reload

            expect(@binder.binder_contractors[0].contact).to eq("Carmen Jones")
            expect(@binder.binder_contractors[0].contractor.name).to eq("Carmen Jones".downcase)
            expect(User.where(:role => "agent").last.user_profile.display_name).to eq("Carmen Jones")
        end

        it "it sets agent phone to nil" do
            agent = {name: "Carmen Jones", contact: "", email: "carmenjones@gmail.com", phone: "Not Provided"}
            Partner::Automation::Service.create_agents(@binder.id, {:agent => agent}, @partner.id, @user.id)
            @binder.reload

            expect(User.where(:role => "agent").last.user_profile.mobile_phone).to be(nil)
        end

        it "it sets agent contact to agent name" do
            agent = {name: "Carmen Jones", contact: "Not Provided", email: "carmenjones@gmail.com", phone: "Not Provided"}
            Partner::Automation::Service.create_agents(@binder.id, {:agent => agent}, @partner.id, @user.id)
            @binder.reload

            expect(@binder.binder_contractors[0].contact).to eq("Carmen Jones")
            expect(@binder.binder_contractors[0].contractor.name).to eq("Carmen Jones".downcase)
        end

        it "it sets agent phone to nil" do
            agent = {name: "Carmen Jones", contact: "", email: "carmenjones@gmail.com", phone: "202350293"}
            Partner::Automation::Service.create_agents(@binder.id, {:agent => agent}, @partner.id, @user.id)
            @binder.reload

            expect(User.where(:role => "agent").last.user_profile.mobile_phone).to be(nil)
        end

        it "it sets agent phone to nil when phone is nil" do
            agent = {name: "Carmen Jones", contact: "", email: "carmenjones@gmail.com", phone: nil}
            Partner::Automation::Service.create_agents(@binder.id, {:agent => agent}, @partner.id, @user.id)
            @binder.reload

            expect(User.where(:role => "agent").last.user_profile.mobile_phone).to be(nil)
        end

        it "creates an agent with headshot from url" do
            agent = {email: "carmenjones@gmail.com", phone: "781-429-3034", first: "Carmen", last: "Jones", name: "Keller Williams", address1: "321 Central Street", city: "Boston", state: "MA", country: "US", postalcode: "02210", headshot: {url: "https://dummyimage.com/300/09f/fff.png"}}
            Partner::Automation::Service.create_agents(@binder.id, {:buyer_agent => agent}, @partner.id, @user.id)

            user_agent = User.find_by_email("carmenjones@gmail.com")

            # verify attributes
            expect(user_agent.user_profile.address.address1).to eq("321 Central Street")
            expect(user_agent.user_profile.address.city).to eq("Boston")
            expect(user_agent.user_profile.address.state).to eq("MA")
            expect(user_agent.user_profile.address.zip).to eq("02210")
            expect(user_agent.user_profile.head_shot).to_not be(nil)
        end

        it "creates an agent with headshot from file" do
            agent = {email: "carmenjones@gmail.com", phone: "781-429-3034", first: "Carmen", last: "Jones", name: "Keller Williams", headshot: {file: "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read), name: "SampleImage.png"}}
            Partner::Automation::Service.create_agents(@binder.id, {:buyer_agent => agent}, @partner.id, @user.id)

            user_agent = User.find_by_email("carmenjones@gmail.com")
            expect(user_agent.user_profile.head_shot.presence).to_not be nil
        end

        it "creates an agent with logo from file" do
            agent = {email: "carmenjones@gmail.com", phone: "781-429-3034", first: "Carmen", last: "Jones", name: "Keller Williams", headshot: {url: nil}, logo: {file: "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read), name: "SampleImage.png"}}
            Partner::Automation::Service.create_agents(@binder.id, {:buyer_agent => agent}, @partner.id, @user.id)

            user_agent = User.find_by_email("carmenjones@gmail.com")
            expect(user_agent.user_profile.logo.presence).to_not be(nil)
        end

        it "creates an agent but logo and headshot are nil" do
            agent = {email: "carmenjones@gmail.com", phone: "781-429-3034", first: "Carmen", last: "Jones", name: "Keller Williams", headshot: {url: "https://dlil96nns7nd5.cloudfront.net/images/agency-no-logo.gif"}, logo: {url: "https://dlil96nns7nd5.cloudfront.net/images/agency-no-logo.gif"}}
            Partner::Automation::Service.create_agents(@binder.id, {:buyer_agent => agent}, @partner.id, @user.id)

            user_agent = User.find_by_email("carmenjones@gmail.com")
            expect(user_agent.user_profile.logo_file_name).to be(nil)
            expect(user_agent.user_profile.head_shot_file_name).to be(nil)
        end
        
        it "adds an existing agent even when there's two emails sent" do
            create(:user, :email => "carmenjones@gmail.com", :role => "agent")
            agent = {email: "carmenjones@gmail.com, chris.batson@engelvoelkers.com", phone: "781-429-3034", first: "Carmen", last: "Jones", name: "Keller Williams", headshot: {url: "https://dlil96nns7nd5.cloudfront.net/images/agency-no-logo.gif"}, logo: {url: "https://dlil96nns7nd5.cloudfront.net/images/agency-no-logo.gif"}}
            Partner::Automation::Service.create_agents(@binder.id, {:buyer_agent => agent}, @partner.id, @user.id)

            user_agent = User.find_by_email("carmenjones@gmail.com")
            expect(user_agent.user_profile.logo_file_name).to be(nil)
            expect(user_agent.user_profile.head_shot_file_name).to be(nil)
        end
    end

    describe "#add_templates" do
        it "tries to add items to binder" do
            params = {
                :maintenance_items => [
                    {
                        name: "Smoke Alarm",
                        do_date: Date.today + 5.weeks,
                        maintenance_cycle: "Years",
                        interval: 1,
                        details: "details"
                    }
                ],
                :projects => [
                    {
                        name: "Replace doors",
                        status: "Planning",
                        project_type: "replacement",
                        start_date: 3.weeks.ago,
                        end_date: Date.today,
                        cost: 0,
                        details: "details",
                    }
                ],
                :home_professionals => [
                    {
                        contact: "Joe Smith",
                        contractor_attributes: {
                            name: "Joe Smith Company",
                            contractor_type: "plumber",
                            phone: "781-423-4305",
                            email: "joesmith@gmail.com",
                            url: "joesmith.com",
                            address_attributes: {
                                address1: "123 Main Street",
                                address2: "Apt. 2",
                                city: "Boston",
                                state: "MA",
                                zip: "01906"
                            }
                        },
                        details: "Details"
                    }
                ],
                :appliances => [
                    {
                        name: "refrigerator",
                        manufacturer: "GE",
                        model: "R300SX",
                        serial_no: "ZL844934B",
                        install_date: 3.days.ago,
                        warranty: "Yes",
                        # purchase_attributes: {
                        #     store: nil,
                        #     price: nil,
                        #     date: nil
                        # },
                        details: "Yes",
                    }
                ]
            }

            allow_any_instance_of(Binder::Appliance).to receive(:save).and_return(false)
            allow_any_instance_of(Binder::MaintenanceItem).to receive(:save).and_return(false)
            allow_any_instance_of(Binder::BinderContractor).to receive(:save).and_return(false)
            allow_any_instance_of(Binder::Project).to receive(:save).and_return(false)
            allow_any_instance_of(Binder::Document).to receive(:save).and_return(false)

            # add appliances to binder
            Partner::Automation::Service.add_appliances(params[:appliances], @binder.id, @template.id, @user.id)

            # add maintenance items to binder
            Partner::Automation::Service.add_maintenance_items(params[:maintenance_items], @binder.id, @template.id, @user.id)

            # add contractors to binder
            Partner::Automation::Service.add_contractors(params[:home_professionals], @binder.id, @template.id, @user.id)

            # add projects to binder
            Partner::Automation::Service.add_projects(params[:projects], @binder.id)

            # add documents to binder
            Partner::Automation::Service.add_template_docs(@binder.id, @template.id)

            expect(ErrorService).to have_received(:perform_async).once.exactly(5).times
        end

        it "tries to adds templates to binder" do
            Binder::MaintenanceItem::Cycle.frequencies.each do |freq|
                create(:maintenance_template, binder_template_id: @template.id, due_date: nil, frequency: freq, initial_due_date_interval: 200)
            end

            allow_any_instance_of(Binder::Appliance).to receive(:save).and_return(false)
            allow_any_instance_of(Binder::MaintenanceItem).to receive(:save).and_return(false)
            allow_any_instance_of(Binder::BinderContractor).to receive(:save).and_return(false)
            allow_any_instance_of(Binder::Project).to receive(:save).and_return(false)
            allow_any_instance_of(Binder::Document).to receive(:save).and_return(false)

            # add appliances to binder
            Partner::Automation::Service.add_appliances([], @binder.id, @template.id, @user.id)

            # add maintenance items to binder
            Partner::Automation::Service.add_maintenance_items([], @binder.id, @template.id, @user.id)

            # add contractors to binder
            expect{Partner::Automation::Service.add_contractors([], @binder.id, @template.id, @user.id)}.to raise_error UnprocessableException

            # add documents to binder
            Partner::Automation::Service.add_template_docs(@binder.id, @template.id)

            expect(ErrorService).to have_received(:perform_async).once.exactly(15).times
        end

        it "adds templates to binder" do
            @binder.details = nil
            @binder.save!

            expect(@binder.maintenance_items.length).to eq(0)
            expect(@binder.appliances.length).to eq(0)
            expect(@binder.binder_contractors.length).to eq(0)
            expect(@binder.documents.length).to eq(0)

            Binder::MaintenanceItem::Cycle.frequencies.each do |freq|
                create(:maintenance_template, binder_template_id: @template.id, due_date: nil, frequency: freq, initial_due_date_interval: 200)
            end

            # add the transfer note to the binder details
            Partner::Automation::Service.delay.add_transfer_note(@binder.id, @template.id)

            # add appliances to binder
            Partner::Automation::Service.add_appliances([], @binder.id, @template.id, @user.id)

            # add maintenance items to binder
            Partner::Automation::Service.add_maintenance_items([], @binder.id, @template.id, @user.id)

            # add contractors to binder
            Partner::Automation::Service.add_contractors([], @binder.id, @template.id, @user.id)

            # add documents to binder
            Partner::Automation::Service.add_template_docs(@binder.id, @template.id)
            @binder.reload

            expect(@binder.details).to eq("This is a transfer note")
            expect(@binder.maintenance_items.length).to eq(13)
            expect(@binder.maintenance_items.first.do_date.to_date).to eq(Date.today + 200)
            expect(@binder.maintenance_items.where(:interval => 1).count).to eq(4)
            expect(@binder.maintenance_items.where(:maintenance_cycle => "As Needed").count).to eq(1)
            expect(@binder.appliances.length).to eq(1)
            expect(@binder.binder_contractors.length).to eq(1)
            expect(@binder.binder_contractors.first.details).to eq("THIS IS A NOTE")
            expect(@binder.documents.length).to eq(1)
        end
    end

    describe "#verify_template" do
        it "raises an error because template does not exist" do
            expect {Partner::Automation::Service.verify_template({binder_template_id: 999}, @user) }.to raise_error BadRequestException, "Binder Template with id: 999 not found"
        end

        it "raises an error because partner is not authorized to use it" do
            temp = create(:partner)
            temp_config = temp.partner_configuration
            temp_template = create(:binder_template, partner_configuration_id: temp_config.id)

            expect {Partner::Automation::Service.verify_template({binder_template_id: temp_template.id}, @user) }.to raise_error BadRequestException, "You are not authorized to use template ID # #{temp_template.id}"
        end

        it "returns the template" do
            template = Partner::Automation::Service.verify_template({binder_template_id: @template.id}, @user)

            expect(template).to_not be(nil)
            expect(template).to_not be(@template.id)
        end
    end

    describe "#add_binder_branding" do
        it "adds no branding" do
            # set the branding to none
            @config.default_user_branding_id = @user.user_profile.id
            @config.default_user_branding_option = "none"
            @config.save

            # add the branding
            Partner::Automation::Service.add_binder_branding(@binder, @partner, "test@gmail.com")

            @binder.reload

            expect(@binder.binder_brandings.length).to eq(2)
        end

        it "adds the user branding to recall, maintenance and binder brandings when partner is an inspector" do
            # set the branding to none
            @config.default_user_branding_id = @user.user_profile.id
            @config.default_user_branding_option = "user"
            @config.save

            # add the branding
            Partner::Automation::Service.add_binder_branding(@binder, @partner, "test@gmail.com")

            @binder.reload

            expect(@binder.binder_brandings.length).to eq(5)
        end

        it "adds the user branding to recall, maintenance and binder brandings when partner is a broker" do
            # change the partner type
            @partner.partner_type = "broker"
            @partner.save
            # set the branding to none
            @config.default_user_branding_id = @user.user_profile.id
            @config.default_user_branding_option = "user"
            @config.save

            # add the branding
            Partner::Automation::Service.add_binder_branding(@binder, @partner, "test@gmail.com")

            @binder.reload

            expect(@binder.binder_brandings.length).to eq(6)
        end

        it "adds the agent branding to recall, maintenance binder and seller report brandings" do
            # set the branding to none
            @config.default_user_branding_id = @user.user_profile.id
            @config.default_user_branding_option = "agent"
            @config.save

            # create the agent
            agent = create(:user, :role => "agent")
            create(:user_profile, :user_id => agent.id)

            UserBinder.create(:binder_id => @binder.id, :user_id => agent.id, :role => "buyer_agent")

            # add the branding
            Partner::Automation::Service.add_binder_branding(@binder, @partner, "test@gmail.com")

            @binder.reload

            expect(@binder.binder_brandings.length).to eq(6)

            # check that a user with the role agent was added
            count = 0
            @binder.binder_brandings.each do |branding|
                next if branding.user_profile.nil?
                next if branding.user_profile.user.nil?
                count += 1 if branding.user_profile.user.role === "agent"
            end

            expect(count).to eq(4)
        end

        it "adds the agent and user branding to recall, maintenance binder and seller report brandings" do
            # set the branding to none
            @config.default_user_branding_id = @user.user_profile.id
            @config.default_user_branding_option = "co_brand"
            @config.save

            # create the agent
            agent = create(:user, :role => "agent")
            create(:user_profile, :user_id => agent.id)

            UserBinder.create(:binder_id => @binder.id, :user_id => agent.id, :role => "buyer_agent")

            # add the branding
            Partner::Automation::Service.add_binder_branding(@binder, @partner, "test@gmail.com")

            @binder.reload

            expect(@binder.binder_brandings.length).to eq(8)

            # check that a user with the role agent was added
            count = 0
            @binder.binder_brandings.each do |branding|
                next if branding.user_profile.nil?
                next if branding.user_profile.user.nil?
                count += 1 if branding.user_profile.user.role === "agent"
            end

            expect(count).to eq(3)
        end

        it "adds no branding" do
            # set the branding to none
            @config.default_user_branding_id = @user.user_profile.id
            @config.default_user_branding_option = nil
            @config.save

            # add the branding
            Partner::Automation::Service.add_binder_branding(@binder, @partner, "test@gmail.com")

            @binder.reload

            expect(@binder.binder_brandings.length).to eq(2)
        end

        it "adds no branding" do
            # set the branding to none
            @config.default_user_branding_id = @user.user_profile.id
            @config.default_user_branding_option = ""
            @config.save

            # add the branding
            Partner::Automation::Service.add_binder_branding(@binder, @partner, "test@gmail.com")

            @binder.reload

            expect(@binder.binder_brandings.length).to eq(2)
        end

        it "adds no branding" do
            # set the branding to none
            @config.default_user_branding_id = @user.user_profile.id
            @config.default_user_branding_option = "test"
            @config.save

            # add the branding
            Partner::Automation::Service.add_binder_branding(@binder, @partner, "test@gmail.com")

            @binder.reload

            expect(@binder.binder_brandings.length).to eq(2)
        end
    end

    describe "#create_homeowner" do
        it "sets homeowner to orphans when email is invalid" do
            client =  {first: "Mike", last: "Breen", email: "transfers", phone: "712-593-3410"}

            homeowner = Partner::Automation::Service.create_homeowner({:client => client}, @binder)

            expect(homeowner.email).to eq("orphans@homebinder.com")
        end

        it "sets homeowner to orphans when email is invalid" do
            create(:user, :email => "orphans@homebinder.com")
            client =  {first: "Mike", last: "Breen", email: "transfers", phone: "712-593-3410"}

            homeowner = Partner::Automation::Service.create_homeowner({:client => client}, @binder)

            expect(homeowner.email).to eq("orphans@homebinder.com")
        end

        it "creates the homeowner" do
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com", phone: "712-593-3410"}

            # creates the homeowner
            homeowner = Partner::Automation::Service.create_homeowner({:client => client, :transaction_type => "sell_side"}, @binder)

            expect(homeowner).to_not be(nil)
            expect(homeowner.valid?).to be(true)
            expect(homeowner.create_method).to eq("sell_side_inspection")
            expect(homeowner.email).to eq("transfers@homebinder.com")
        end

        it "returns the homeowner since user already exists" do
            user = create(:user, :email => "transfers@homebinder.com")

            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com", phone: "712-593-3410"}

            # creates/returns the homeowner
            homeowner = Partner::Automation::Service.create_homeowner({:client => client}, @binder)

            expect(homeowner).to_not be(nil)
            expect(homeowner.id).to eq(user.id)
            expect(homeowner.email).to eq("transfers@homebinder.com")
        end

        it "returns the user for orphans@homebinder.com" do
            client =  {first: "Mike", last: "Breen", email: "orphans@homebinder.com", phone: "712-593-3410"}

            # creates/returns the homeowner
            homeowner = Partner::Automation::Service.create_homeowner({:client => client}, @binder)

            expect(homeowner).to_not be(nil)
            expect(homeowner.email).to eq("orphans@homebinder.com")
        end

        it "returns the user for orphans@homebinder.com" do
            user = create(:user, :email => "orphans@homebinder.com")
            client =  {first: "Mike", last: "Breen", email: "orphans@homebinder.com", phone: "712-593-3410"}

            # creates/returns the homeowner
            homeowner = Partner::Automation::Service.create_homeowner({:client => client}, @binder)

            expect(homeowner).to_not be(nil)
            expect(homeowner.id).to eq(user.id)
            expect(homeowner.email).to eq("orphans@homebinder.com")
        end
    end

    describe "#set_binder_owner" do
        it "sets binder owner to partner user" do
            partner_user = create(:user, :role => "inspector")
            create(:partner_user, :user => partner_user, :partner => @partner, :role => "partner_member")

            owner = Partner::Automation::Service.set_binder_owner(@binder, @partner, partner_user.email)

            expect(owner.email).to eq(partner_user.email)
        end

        it "sets binder owner to partner admin" do
            @partner.partner_configuration.default_user_branding_id = -1
            @partner.partner_configuration.save

            UserBinder.where(:user_id => @user.id, :binder_id => @binder.id, :role => "owner").destroy_all

            owner = Partner::Automation::Service.set_binder_owner(@binder, @partner, "")

            expect(owner.email).to eq(@user.email)
        end
    end

    describe "#create_transfer" do
        it "creates the user for orphans@homebinder.com and creates the transfer" do
            user = create(:user, :email => "orphans@homebinder.com")
            client =  {first: "Mike", last: "Breen", email: "orphans@homebinder.com", phone: "712-593-3410"}

            # creates/returns the homeowner
            homeowner = Partner::Automation::Service.create_homeowner({:client => client}, @binder)

            # creates the transfer
            transfer = Partner::Automation::Service.create_transfer(@binder, homeowner, @user)

            expect(homeowner).to_not be(nil)
            expect(homeowner.id).to eq(user.id)
            expect(homeowner.email).to eq("orphans@homebinder.com")
            expect(transfer).to_not be(nil)
            expect(transfer.sender.id).to eq(@user.id)
            expect(transfer.receiver.id).to eq(homeowner.id)
        end
    end

    describe "#transfer_to_orphans" do
        it "creates the user for orphans@homebinder.com and creates the transfer" do
            user = create(:user, :email => "orphans@homebinder.com")
            client =  {first: "Mike", last: "Breen", email: "orphans@homebinder.com", phone: "712-593-3410"}

            # creates/returns the homeowner
            homeowner = Partner::Automation::Service.create_homeowner({:client => client}, @binder)

            # creates the transfer
            transfer = Partner::Automation::Service.create_transfer(@binder, homeowner, @user)

            # transfer the binder to orphans
            Partner::Automation::Service.transfer_to_orphans(@binder, @user, transfer, homeowner)

            transfer.reload

            expect(homeowner).to_not be(nil)
            expect(homeowner.id).to eq(user.id)
            expect(homeowner.email).to eq("orphans@homebinder.com")
            expect(transfer).to_not be(nil)
            expect(transfer.sender.id).to eq(@user.id)
            expect(transfer.receiver.id).to eq(homeowner.id)

            expect(transfer.status).to eq("pending")
            expect(@binder.owner.id).to eq(homeowner.id)
        end

        it "calls new relic" do
            user = create(:user, :email => "orphans@homebinder.com")
            client =  {first: "Mike", last: "Breen", email: "orphans@homebinder.com", phone: "712-593-3410"}

            # creates/returns the homeowner
            homeowner = Partner::Automation::Service.create_homeowner({:client => client}, @binder)

            # creates the transfer
            transfer = Partner::Automation::Service.create_transfer(@binder, homeowner, @user)

            allow(Binder::Transfer).to receive(:execute).and_raise(UnprocessableException.new(user))

            # transfer the binder to orphans
            Partner::Automation::Service.transfer_to_orphans(@binder, @user, transfer, homeowner)

            expect(ErrorService).to have_received(:perform_async).with(nil, {binder_id: @binder.id})
        end

        it "calls new relic" do
            create(:user, :email => "orphans@homebinder.com")
            client =  {first: "Mike", last: "Breen", email: "orphans@homebinder.com", phone: "712-593-3410"}

            # creates/returns the homeowner
            homeowner = Partner::Automation::Service.create_homeowner({:client => client}, @binder)

            # creates the transfer
            transfer = Partner::Automation::Service.create_transfer(@binder, homeowner, @user)

            allow(Binder::Transfer).to receive(:execute).and_raise(BadRequestException.new("message"))

            # transfer the binder to orphans
            Partner::Automation::Service.transfer_to_orphans(@binder, @user, transfer, homeowner)

            expect(ErrorService).to have_received(:perform_async).with("message", {binder_id: @binder.id})
        end
    end

    describe "#create_transaction" do
        it "create buy_side_inspection" do
            params = {
                :inspection_date => Date.today,
                :transaction_type => "BUY"
            }
            Partner::Automation::Service.create_transaction(@binder, params, @partner)

            expect(@binder.transactions.count).to eq(1)
        end

        it "create sell_side_inspection" do
            params = {
                :inspection_date => Date.today,
                :transaction_type => "SELL"
            }
            Partner::Automation::Service.create_transaction(@binder, params, @partner)

            expect(@binder.transactions.count).to eq(1)
        end

        it "create buy_side_inspection" do
            params = {
                :inspection_date => Date.today
            }
            Partner::Automation::Service.create_transaction(@binder, params, @partner)

            expect(@binder.transactions.count).to eq(1)
        end
    end

    describe "#image_url" do
        it "raises an error" do
            allow(Paperclip).to receive_message_chain(:io_adapters, :for).and_raise BadRequestException
            Partner::Automation::Service.image_url(nil)
            expect(ErrorService).to have_received(:perform_async).once
        end
    end

    describe "#add_agent_headshot_logo" do
        it "adds headshot and logo through file" do
            agent = create(:user, :role => "agent")
            params = {
                :headshot => {
                    :file => "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read)
                },
                :logo => {
                    :file => "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read)
                }
            }

            Partner::Automation::Service.add_agent_headshot_logo(params, agent)

            expect(agent.user_profile.head_shot_file_name).to eq "data"
            expect(agent.user_profile.logo_file_name).to eq "data"
        end
    end

    describe "#add_additional_services" do
        it "does nothing when partner warranty config is nil" do
            client =  {first: "Mike", last: "Breen", email: "orphans@homebinder.com", phone: "712-593-3410"}

            # creates/returns the homeowner
            homeowner = Partner::Automation::Service.create_homeowner({:client => client}, @binder)

            # add the warranties
            Partner::Automation::Service.add_additional_services(@binder.id, nil, @user.id, @partner.id, homeowner.id)

            expect(@binder.warranties.count).to eq(0)
        end

        it "does nothing when partner warranty config automatic_purchases is false" do
            create(:warranty_configuration, :partner_id => @partner.id)
            client =  {first: "Mike", last: "Breen", email: "orphans@homebinder.com", phone: "712-593-3410"}

            # creates/returns the homeowner
            homeowner = Partner::Automation::Service.create_homeowner({:client => client}, @binder)

            # add the warranties
            Partner::Automation::Service.add_additional_services(@binder.id, nil, @user.id, @partner.id, homeowner.id)

            expect(@binder.warranties.count).to eq(0)
        end

        it "creates the warranty" do
            allow(Slack::MessagingJob).to receive(:perform_async)

            company = create(:warranty_company)
            plan = create(:warranty_plan, :warranty_company_id => company.id)
            create(:partner_warranty_plan, :price => plan.price, :warranty_plan_id => plan.id, :partner_id => @partner.id)
            create(:warranty_configuration, :partner_id => @partner.id, :automatic_purchases => true, :warranty_plan_id => plan.id)
            client =  {first: "Mike", last: "Breen", email: "mike@breen.com", phone: "712-593-3410"}

            # creates/returns the homeowner
            homeowner = Partner::Automation::Service.create_homeowner({:client => client}, @binder)

            # add the warranties
            Partner::Automation::Service.add_additional_services(@binder.id, 5.days.ago, @user.id, @partner.id, homeowner.id)

            expect(@binder.warranties.count).to eq(1)
            expect(Slack::MessagingJob).to_not have_received(:perform_async)
        end

        it "creates the warranty even when inspection_date is nil" do
            allow(Slack::MessagingJob).to receive(:perform_async)

            company = create(:warranty_company)
            plan = create(:warranty_plan, :warranty_company_id => company.id)
            create(:partner_warranty_plan, :price => plan.price, :warranty_plan_id => plan.id, :partner_id => @partner.id)
            create(:warranty_configuration, :partner_id => @partner.id, :automatic_purchases => true, :warranty_plan_id => plan.id)
            client =  {first: "Mike", last: "Breen", email: "mike@breen.com", phone: "712-593-3410"}

            # creates/returns the homeowner
            homeowner = Partner::Automation::Service.create_homeowner({:client => client}, @binder)

            # add the warranties
            Partner::Automation::Service.add_additional_services(@binder.id, nil, @user.id, @partner.id, homeowner.id)

            expect(@binder.warranties.count).to eq(1)
            expect(Slack::MessagingJob).to_not have_received(:perform_async)
        end
    end

    describe "#create_binder" do
        it "raises an UnprocessableException" do
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com", phone: "712-593-3410"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: Faker::Lorem.characters(21), country: "US"}
            agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}

            parameters = {
                :key => "test",
                :client => client,
                :property => property,
                :binder_template_id => @template.id
            }

            expect{ Partner::Automation::Service.create_binder(parameters) }.to raise_error UnprocessableException
        end

        it "creates the binder" do
            image = {file: "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read)}
            files = [{file: "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read), name: "SampleDoc.pdf"}]

            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com", phone: "712-593-3410"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}

            parameters = {
                :key => "test",
                :client => client,
                :property => property,
                :binder_template_id => @template.id,
                :agent => agent,
                :property_photo => image,
                :documents => files
            }

            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

            new_binder = Partner::Automation::Service.create_binder(parameters)

            homeowner = User.where(:role => "homeowner").last
            @user.reload

            expect(new_binder[:name]).to eq("Mike Breen Home")
            expect(homeowner.user_profile.mobile_phone).to eq("+17125933410")
            expect(@user.binders.length).to eq(2)
            expect(User.where(:role => "agent").last.email).to eq("carmensmith@gmail.com")
        end

        it "creates the binder for broker" do
            @user.role = "broker"
            @user.save
            @partner.partner_type = "broker"
            @partner.save

            image = {file: "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read), name: "SampleImage.png"}
            files = [{file: "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read), name: "SampleDoc.pdf"}]

            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com", phone: "712-593-3410"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}

            parameters = {
                :key => "test",
                :client => client,
                :property => property,
                :binder_template_id => @template.id,
                :agent => agent,
                :property_photo => image,
                :documents => files
            }

            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

            new_binder = Partner::Automation::Service.create_binder(parameters)

            homeowner = User.where(:role => "homeowner").last
            @user.reload

            expect(new_binder[:name]).to eq("Mike Breen Home")
            expect(homeowner.user_profile.mobile_phone).to eq("+17125933410")
            expect(@user.binders.length).to eq(2)
            expect(User.where(:role => "agent").last.email).to eq("carmensmith@gmail.com")
        end

        it "creates the binder and automatically transfers the binder to orphans" do
            image = {file: "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read), name: "SampleImage.png"}
            files = [{file: "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read), name: "SampleDoc.pdf"}]

            client =  {first: "Mike", last: "Breen", email: nil, phone: "712-593-3410"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}

            parameters = {
                :key => "test",
                :client => client,
                :property => property,
                :binder_template_id => @template.id,
                :agent => agent,
                :property_photo => image,
                :documents => files
            }
            
            allow(Partner::Automation::Service).to receive(:add_as_contractor)
            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

            new_binder = Partner::Automation::Service.create_binder(parameters)

            # find the binder and get the owner
            binder = Binder.find(new_binder[:id])
            owner = binder.get_owner

            expect(owner.email).to eq("orphans@homebinder.com")
        end
    end
end
