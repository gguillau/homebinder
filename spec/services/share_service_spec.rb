require 'rails_helper'

RSpec.describe ShareService do
    
    describe "handle_webhook" do
        it "updates the share status" do
            # create the users/binders/share
            sender = create(:user)
            receiver = create(:user)
            binder = create(:binder)
            share = create(:share, :status => "sent", :binder_id => binder.id, :shared_with_id => receiver.id, :shared_by_id => sender.id, :role_name => "co_owner")

            # share the binder
            Shares::CreateJob.perform_async(share.id)

            # handle the webhook
            ShareService.new(share.id).handle_webhook("delivery_failed")

            # make sure the status was changed
            share.reload
            expect(share.status).to eq("delivery_failed")
        end
    end
end