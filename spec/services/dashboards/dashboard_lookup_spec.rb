require 'rails_helper'

RSpec.describe Dashboards::DashboardLookup, :type => :model do
  before :each do
    @user = create(:user, role: UserGlobalRoles::HOMEOWNER)
    @admin = create(:user, role: UserGlobalRoles::ADMIN)
    @agent = create(:user, role: UserGlobalRoles::AGENT)
    @org = create(:organization)
    @partner = create(:partner)
    @widget = create(:widget)
    @widget2 = create(:widget)
    @binder = create(:binder)
  end
  
  describe "for_binder" do
    it "requires id of the dashboard" do
      @lookup = Dashboards::DashboardLookup.new(@user)
      expect{@lookup.for_binder({})}.to raise_exception(BadRequestException)
    end
    
    it "finds the binder" do
      @lookup = Dashboards::DashboardLookup.new(@user)
      expect{@lookup.for_binder({id: 999999999})}.to raise_exception(ActiveRecord::RecordNotFound)
    end
    
    it "uses a user binder dashboard" do
      board = create(:dashboard, name: "board", user_id: @user.id, system: false, default: false, scope: DashboardScope::BINDER)
      create(:user_binder, user_id: @user.id, binder_id: @binder.id, dashboard_id: board.id, :role => "owner")
      @lookup = Dashboards::DashboardLookup.new(@user)
      result = @lookup.for_binder({id: @binder.id})
      expect(result).to_not be_nil
      expect(result.id).to equal(board.id)
    end
    
    it "uses a partner binder dashboard" do
      board = create(:dashboard, name: "board", partner_id: @partner.id, system: false, default: false, scope: DashboardScope::BINDER)
      create(:dashboard_widget, dashboard_id: board.id, widget_id: @widget.id, index: 0)
      create(:dashboard_widget, dashboard_id: board.id, widget_id: @widget2.id, index: 1)
      create(:user_binder, user_id: @user.id, binder_id: @binder.id, dashboard_id: nil, :role => "owner")
      create(:partner_binder, partner_id: @partner.id, binder_id: @binder.id, dashboard_id: board.id, role: PartnerBinderRole::BINDER)
      @lookup = Dashboards::DashboardLookup.new(@user)
      result = @lookup.for_binder({id: @binder.id})
      expect(result.name).to eq(@binder.name)
      expect(result.dashboard_widgets.count).to eq(2)
      expect(result.dashboard_widgets[0].widget_id).to eq(@widget.id)
      expect(result.dashboard_widgets[0].index).to eq(0)
      expect(result.dashboard_widgets[1].widget_id).to eq(@widget2.id)
      expect(result.dashboard_widgets[1].index).to eq(1)
    end
    
    it "uses a partner default dashboard" do
      board = create(:dashboard, name: "board", partner_id: @partner.id, system: false, default: true, scope: DashboardScope::BINDER)
      create(:dashboard_widget, dashboard_id: board.id, widget_id: @widget.id, index: 0)
      create(:dashboard_widget, dashboard_id: board.id, widget_id: @widget2.id, index: 1)
      create(:user_binder, user_id: @user.id, binder_id: @binder.id, dashboard_id: nil, :role => "owner")
      create(:partner_binder, partner_id: @partner.id, binder_id: @binder.id, dashboard_id: nil, role: PartnerBinderRole::BINDER)
      @lookup = Dashboards::DashboardLookup.new(@user)
      result = @lookup.for_binder({id: @binder.id})
      expect(result.name).to eq(@binder.name)
      expect(result.dashboard_widgets.count).to eq(2)
      expect(result.dashboard_widgets[0].widget_id).to eq(@widget.id)
      expect(result.dashboard_widgets[0].index).to eq(0)
      expect(result.dashboard_widgets[1].widget_id).to eq(@widget2.id)
      expect(result.dashboard_widgets[1].index).to eq(1)
    end
    
    it "uses a partner default dashboard for admin" do
      board = create(:dashboard, name: "board", partner_id: @partner.id, system: false, default: true, scope: DashboardScope::BINDER)
      create(:dashboard_widget, dashboard_id: board.id, widget_id: @widget.id, index: 0)
      create(:dashboard_widget, dashboard_id: board.id, widget_id: @widget2.id, index: 1)
      create(:user_binder, user_id: @user.id, binder_id: @binder.id, dashboard_id: nil, :role => "owner")
      create(:partner_binder, partner_id: @partner.id, binder_id: @binder.id, dashboard_id: nil, role: PartnerBinderRole::BINDER)
      @lookup = Dashboards::DashboardLookup.new(@admin)
      result = @lookup.for_binder({id: @binder.id})
      expect(result.name).to eq(board.name)
    end
    
    it "uses an organization dashboard" do
      create(:organization_partner, organization_id: @org.id, partner_id: @partner.id)
      board = create(:dashboard, name: "board", organization_id: @org.id, system: false, default: true, scope: DashboardScope::BINDER)
      create(:dashboard_widget, dashboard_id: board.id, widget_id: @widget.id, index: 0)
      create(:dashboard_widget, dashboard_id: board.id, widget_id: @widget2.id, index: 1)
      create(:user_binder, user_id: @user.id, binder_id: @binder.id, dashboard_id: nil, :role => "owner")
      create(:partner_binder, partner_id: @partner.id, binder_id: @binder.id, dashboard_id: nil, role: PartnerBinderRole::BINDER)
      @lookup = Dashboards::DashboardLookup.new(@user)
      result = @lookup.for_binder({id: @binder.id})
      expect(result.name).to eq(@binder.name)
      expect(result.dashboard_widgets.count).to eq(2)
      expect(result.dashboard_widgets[0].widget_id).to eq(@widget.id)
      expect(result.dashboard_widgets[0].index).to eq(0)
      expect(result.dashboard_widgets[1].widget_id).to eq(@widget2.id)
      expect(result.dashboard_widgets[1].index).to eq(1)
    end
    
    it "uses the first default organization dashboard" do
      org2 = create(:organization)
      create(:organization_partner, organization_id: org2.id, partner_id: @partner.id)
      create(:organization_partner, organization_id: @org.id, partner_id: @partner.id)
      board = create(:dashboard, name: "board", organization_id: @org.id, system: false, default: true, scope: DashboardScope::BINDER)
      create(:dashboard_widget, dashboard_id: board.id, widget_id: @widget.id, index: 0)
      create(:dashboard_widget, dashboard_id: board.id, widget_id: @widget2.id, index: 1)
      create(:user_binder, user_id: @user.id, binder_id: @binder.id, dashboard_id: nil, :role => "owner")
      create(:partner_binder, partner_id: @partner.id, binder_id: @binder.id, dashboard_id: nil, role: PartnerBinderRole::BINDER)
      @lookup = Dashboards::DashboardLookup.new(@user)
      result = @lookup.for_binder({id: @binder.id})
      expect(result.name).to eq(@binder.name)
      expect(result.dashboard_widgets.count).to eq(2)
      expect(result.dashboard_widgets[0].widget_id).to eq(@widget.id)
      expect(result.dashboard_widgets[0].index).to eq(0)
      expect(result.dashboard_widgets[1].widget_id).to eq(@widget2.id)
      expect(result.dashboard_widgets[1].index).to eq(1)
    end
    
    it "uses a system dashboard" do
      board = create(:dashboard, name: "board", system: true, default: true, scope: DashboardScope::BINDER)
      create(:dashboard_widget, dashboard_id: board.id, widget_id: @widget.id, index: 0)
      create(:dashboard_widget, dashboard_id: board.id, widget_id: @widget2.id, index: 1)
      create(:user_binder, user_id: @user.id, binder_id: @binder.id, dashboard_id: nil, :role => "owner")
      create(:partner_binder, partner_id: @partner.id, binder_id: @binder.id, dashboard_id: nil, role: PartnerBinderRole::BINDER)
      @lookup = Dashboards::DashboardLookup.new(@user)
      result = @lookup.for_binder({id: @binder.id})
      expect(result.name).to eq(@binder.name)
      expect(result.dashboard_widgets.count).to eq(2)
      expect(result.dashboard_widgets[0].widget_id).to eq(@widget.id)
      expect(result.dashboard_widgets[0].index).to eq(0)
      expect(result.dashboard_widgets[1].widget_id).to eq(@widget2.id)
      expect(result.dashboard_widgets[1].index).to eq(1)  
    end
    
    it "uses a system dashboard for admin" do
      board = create(:dashboard, name: "board", system: true, default: true, scope: DashboardScope::BINDER)
      create(:dashboard_widget, dashboard_id: board.id, widget_id: @widget.id, index: 0)
      create(:dashboard_widget, dashboard_id: board.id, widget_id: @widget2.id, index: 1)
      create(:user_binder, user_id: @user.id, binder_id: @binder.id, dashboard_id: nil, :role => "owner")
      create(:partner_binder, partner_id: @partner.id, binder_id: @binder.id, dashboard_id: nil, role: PartnerBinderRole::BINDER)
      @lookup = Dashboards::DashboardLookup.new(@admin)
      result = @lookup.for_binder({id: @binder.id})
      expect(result.name).to eq(board.name)
    end
  end
  
  describe "for_user" do
    describe "user dashboard" do
      it "gets" do
        board = create(:dashboard, user_id: @user.id, default: true, system: false, scope: DashboardScope::USER)
        @lookup = Dashboards::DashboardLookup.new(@user)
        found = @lookup.for_user
        expect(found.id).to eq(board.id)
      end
    end
    
    describe "partner dashboard" do
      it "gets" do
        @user.role = "inspector"
        @user.save
        create(:partner_user, partner_id: @partner.id, user_id: @user.id)
        board = create(:dashboard, partner_id: @partner.id, default: true, system: false, scope: DashboardScope::USER)
        @lookup = Dashboards::DashboardLookup.new(@user)
        found = @lookup.for_user
        expect(found.id).to eq(board.id)
      end
    end
    
    describe "organization dashboard" do
      it "gets" do
        create(:organization_user, organization_id: @org.id, user_id: @user.id)
        board = create(:dashboard, organization_id: @org.id, default: true, system: false, scope: DashboardScope::USER)
        @lookup = Dashboards::DashboardLookup.new(@user)
        found = @lookup.for_user
        expect(found.id).to eq(board.id)
      end
    end
    
    describe "admin" do
      it "gets" do
        board = create(:dashboard, system: true, default: true, scope: DashboardScope::ADMIN)
        @lookup = Dashboards::DashboardLookup.new(@admin)
        found = @lookup.for_user
        expect(found.id).to eq(board.id)
      end
    end
    
    describe "inspector" do
      it "gets" do
        insp = create(:user, role: UserGlobalRoles::INSPECTOR)
        create(:partner_user, partner_id: @partner.id, user_id: insp.id)
        board = create(:dashboard, system: true, default: true, scope: DashboardScope::INSPECTOR)
        @lookup = Dashboards::DashboardLookup.new(insp)
        found = @lookup.for_user
        expect(found.id).to eq(board.id)
      end
    end
    
    describe "broker" do
      it "gets" do
        partner = create(:partner, :partner_type => "broker")
        broker = create(:user, role: UserGlobalRoles::BROKER)
        create(:partner_user, partner_id: partner.id, user_id: broker.id)
        board = create(:dashboard, system: true, default: true, scope: DashboardScope::BROKER)
        @lookup = Dashboards::DashboardLookup.new(broker)
        found = @lookup.for_user
        expect(found.id).to eq(board.id)
      end
    end
    
    describe "partner" do
      it "gets" do
        part = create(:user, role: UserGlobalRoles::PARTNER)
        create(:partner_user, partner_id: @partner.id, user_id: part.id)
        board = create(:dashboard, system: true, default: true, scope: DashboardScope::PARTNER)
        @lookup = Dashboards::DashboardLookup.new(part)
        found = @lookup.for_user
        expect(found.id).to eq(board.id)
      end
    end
    
    describe "homeowner" do
      it "gets" do
        board = create(:dashboard, system: true, default: true, scope: DashboardScope::USER)
        @lookup = Dashboards::DashboardLookup.new(@user)
        found = @lookup.for_user
        expect(found.id).to eq(board.id)
      end
    end
    
    describe "agent" do
      it "gets" do
        board = create(:dashboard, system: true, default: true, scope: DashboardScope::USER)
        @lookup = Dashboards::DashboardLookup.new(@agent)
        found = @lookup.for_user
        expect(found.id).to eq(board.id)
      end
    end
  end
end