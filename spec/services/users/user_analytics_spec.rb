require 'rails_helper'

RSpec.describe Users::UserAnalytics, :type => :service do
    let(:user) {FactoryBot.create(:user, :role => "homeowner")}

    before :each do
        allow(ErrorService).to receive(:perform_async)
    end

    describe "#update_user" do
        it "raises an error" do
            user = create(:user)
            allow_any_instance_of(User).to receive(:save!).and_raise(BadRequestException)
            Users::UserAnalytics.update_user(user.id)
            expect(ErrorService).to have_received(:perform_async)
        end
    end

    describe "#transfer_received" do
        it "raises an error" do
            transfer = create(:transfer)
            allow_any_instance_of(Binder::Transfer).to receive(:access_token).and_raise BadRequestException
            Users::UserAnalytics.transfer_received(transfer.id)
            expect(ErrorService).to have_received(:perform_async)
        end
    end

    describe "#share_received" do
        it "raises an error" do
            share = create(:share)
            allow_any_instance_of(Binder::Share).to receive(:access_token).and_raise BadRequestException
            Users::UserAnalytics.share_received(share.id)
            expect(ErrorService).to have_received(:perform_async)
        end
    end

    describe "#destroyed" do
        it "raises an error" do
            intercom = double("Intercom::HomeBinderClient", :intercom => {:new => true, :users => true, :events => true, :messages => true})
            allow(Intercom::HomeBinderClient).to receive(:new).and_return(intercom)

            allow(intercom).to receive(:users).and_return(OpenStruct.new({:find => true, :save => true}))
            allow(intercom.users).to receive(:find).and_raise(BadRequestException)

            Users::UserAnalytics.destroyed(user.id)
            expect(ErrorService).to have_received(:perform_async)
        end
    end

    describe "#logged_in" do
        it "creates the events" do
            users = OpenStruct.new({:find => true, :save => true, :create => true})
            events = OpenStruct.new({:find => true, :save => true, :create => true})
            intercom = double("Intercom::HomeBinderClient", :intercom => OpenStruct.new({users: users, events: events}))
            allow(Intercom::HomeBinderClient).to receive(:new).and_return(intercom)

            allow(intercom).to receive(:users).and_return(users)
            allow(intercom).to receive(:events).and_return(events)
            allow(intercom.users).to receive(:create)
            allow(intercom.events).to receive(:create)

            Users::UserAnalytics.logged_in(user.id)

            expect(intercom.users).to have_received(:create)
            expect(intercom.events).to have_received(:create)
        end
    end

    describe "#unsubscribe" do
        it "raises an error" do
            allow(Intercom::HomeBinderClient).to receive(:new).and_raise BadRequestException
            Users::UserAnalytics.unsubscribe(user.id)
            expect(ErrorService).to have_received(:perform_async)
        end

        it "unsubscribes the user" do
            intercom = double("Intercom::Client", :new => true, :users => true, :events => true, :messages => true)
            allow(Intercom::Client).to receive(:new).and_return(intercom)

            allow(intercom).to receive(:users).and_return(OpenStruct.new({:find => true, :save => true}))
            allow(intercom.users).to receive(:save)
            allow(intercom.users).to receive(:find).and_return(OpenStruct.new({:unsubscribed_from_emails => false}))

            Users::UserAnalytics.unsubscribe(user.id)

            expect(intercom.users).to have_received(:save)
        end
    end
end
