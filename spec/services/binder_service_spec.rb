require 'rails_helper'

RSpec.describe BinderService, :type => :service do
    let(:user) {FactoryBot.create(:user, :role => "inspector")}
    let(:partner) {FactoryBot.create(:partner)}
    let(:binder_1) {FactoryBot.create(:binder, :primary => true)}
    let!(:partner_user) {FactoryBot.create(:partner_user, :partner => partner, :user => user, :role => "admin")}
    let!(:user_binder) {FactoryBot.create(:user_binder, :binder => binder_1, :user => user, :role => "owner")}
    let!(:partner_binder) {FactoryBot.create(:partner_binder, :binder => binder_1, :partner => partner, :role => "binder")}
    let!(:coupon) {FactoryBot.create(:coupon, :partner => partner)}
    let!(:subscription) {FactoryBot.create(:subscription, :binder_id => binder_1.id, :plan_id => "free")}
    let(:request) {HBRequest.new.create_request(user)}
    let(:service) {BinderService.new(user, binder_1)}
    let(:account_mailer) {double("AccountMailer", :deliver_later => true)}
    
    before :each do
        partner.partner_configuration.default_user_branding_id = user.user_profile.id
        partner.partner_configuration.save!
        allow(SubscriptionService).to receive(:upgrade)
    end

    describe "#check_primary" do
        it "sets new primary" do
            binder_2 = create(:binder, :primary => true)
            create(:user_binder, :binder => binder_2, :user => user, :role => "owner")
            service.check_primary

            binder_2.reload

            expect(binder_2.primary).to eq(false)
        end
    end

    describe "#after_create" do
        it "adds additional objects to the binder as inspector" do
            partner.partner_configuration.generate_default_template
            template = partner.partner_configuration.binder_templates.first
            homeowner = create(:user, :role => "homeowner")

            params = {}
            params[:homeowner_id] = homeowner.id
            params[:binder_template_id] = template.id
            params[:user_binders] = [:user_id => user.id, :role => "owner"]
            params[:partner_binders] = [:partner_id => partner.id, :role => "binder"]
            params[:inspection_date] = Date.tomorrow
            params[:binder_brandings] = [{:partner_id => partner.id, :user_branding_id => user.user_profile.id, :scope => "binder"}]
            partner_binder.destroy

            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, delete: true})
            allow(Stripe::Customer).to receive(:create).and_return(stripe_customer)

            service.after_create(params)
            user.reload
            partner.reload

            user_binder = UserBinder.where(user_id: user.id, binder_id: binder_1.id, role: UserBinderRoles::OWNER).first
            partner_binder = PartnerBinder.where(partner_id: partner.id, binder_id: binder_1.id, role: PartnerBinderRole::BINDER).first

            expect(user.binders.count).to eq(1)
            expect(partner.binders.count).to eq(1)
            expect(user_binder.user_id).to eq(user.id)
            expect(user_binder.binder_id).to eq(binder_1.id)
            expect(partner_binder.partner_id).to eq(partner.id)
            expect(partner_binder.binder_id).to eq(binder_1.id)
        end

        it "adds additional objects to the binder as broker" do
            user = create(:user, :role => "broker")
            binder = create(:binder)
            create(:user_binder, :binder => binder, :user => user, :role => "owner")
            partner = create(:partner, :partner_type => "broker")
            partner.partner_configuration.default_user_branding_id = user.user_profile.id
            partner.partner_configuration.save!
            create(:partner_user, :user => user, :partner => partner, :role => "admin")
            binder_2 = create(:binder)
            create(:user_binder, :binder => binder_2, :user => user, :role => "owner")
            create(:subscription, :binder_id => binder_2.id, :plan_id => "free")

            partner.partner_configuration.generate_default_template
            template = partner.partner_configuration.binder_templates.first
            homeowner = create(:user, :role => "homeowner")

            params = {}
            params[:homeowner_id] = homeowner.id
            params[:binder_template_id] = template.id
            params[:user_binders] = [:user_id => user.id, :role => "owner"]
            params[:partner_binders] = [:partner_id => partner.id, :role => "binder"]
            params[:inspection_date] = Date.tomorrow
            params[:binder_brandings] = [{:partner_id => partner.id, :user_branding_id => user.user_profile.id, :scope => "binder"}]

            BinderService.new(user, binder).after_create(params)
            user.reload
            partner.reload

            expect(user.binders.count).to eq(2)
        end
        
        it "adds additional objects to the binder as agent" do
            user = create(:user, :role => "agent")
            binder = create(:binder)
            create(:user_binder, :binder => binder, :user => user, :role => "owner")
            create(:subscription, :binder_id => binder.id, :plan_id => "free")

            homeowner = create(:user, :role => "homeowner")

            params = {}
            params[:homeowner_id] = homeowner.id
            params[:user_binders] = [:user_id => user.id, :role => "owner"]
            
            create_list(:user_contractor, 10, :user => user)
            
            BinderService.new(user, binder).after_create(params)
            
            expect(binder.binder_contractors.count).to eq(10)
        end

        it "adds additional objects to the binder as homeowner and adds widget" do
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards

            user = create(:user, :role => "homeowner")
            binder = create(:binder)
            create(:user_binder, :binder => binder, :user => user, :role => "owner")

            binder_2 = create(:binder)
            create(:user_binder, :binder => binder_2, :user => user, :role => "owner")
            create(:subscription, :binder_id => binder_2.id, :plan_id => "free")

            params = {}
            params[:user_binders] = [:user_id => user.id, :role => "owner"]

            BinderService.new(user, binder).after_create(params)
            user.reload

            expect(user.binders.count).to eq(2)

            dashboard = Dashboards::DashboardLookup.new(user).for_binder({:id => binder.id})
            expect(dashboard.widgets.where(:name => "Third Party Services").exists?).to be(true)
        end
    end

    describe "#orphan" do
        it "orphans the binder" do
            create(:user, :role => "homeowner", :email => "orphans@homebinder.com")

            service.orphan

            owner = User.with_role(:owner, binder_1).first
            expect(owner.email).to eq("orphans@homebinder.com")
        end
    end

    describe "#send_partner_feedback" do
        it "sends feedback on the binder" do
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards

            allow(AccountMailer).to receive(:send_partner_feedback).and_return(account_mailer)

            user = create(:user, :email => "orphans@homebinder.com")
            branding = create(:user, :role => "inspector")
            partner = create(:partner)
            create(:partner_user, :user => branding, :partner => partner, :role => "admin")

            binder = create(:binder)
            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")
            create(:binder_branding, :binder_id => binder.id, :user_branding_id => branding.user_profile.id, :scope => "binder")

            BinderService.new(user, binder).send_partner_feedback

            expect(AccountMailer).to have_received(:send_partner_feedback).exactly(1).times
        end
    end

    describe "#learn_more_apr" do
        it "requests more apr info for the binder" do
            allow(AccountMailer).to receive(:apr_widget).and_return(account_mailer)

            user = create(:user, :email => "orphans@homebinder.com")
            binder = create(:binder)
            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")

            BinderService.new(user, binder).learn_more_apr

            expect(AccountMailer).to have_received(:apr_widget).exactly(1).times
        end
    end

    describe "#secure24" do
        it "requests secure24 for the binder" do
            allow(AccountMailer).to receive(:secure24).and_return(account_mailer)

            user = create(:user, :email => "orphans@homebinder.com")
            branding = create(:user, :role => "inspector")
            partner = create(:partner)
            create(:partner_user, :user => branding, :partner => partner, :role => "admin")

            binder = create(:binder)
            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")
            create(:binder_branding, :binder_id => binder.id, :user_branding_id => branding.user_profile.id, :scope => "binder")

            BinderService.new(user, binder).secure24

            expect(AccountMailer).to have_received(:secure24).exactly(1).times
        end
    end

    describe "#inventory_report" do
        it "requests inventory_report for binder" do
            allow(AccountMailer).to receive(:secure24).and_return(account_mailer)
            
            user = create(:user, :email => "orphans@homebinder.com")

            binder = create(:binder)
            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")

            report_service = double("InventoryReportPdfService", :create => "")
            allow(InventoryReportPdfService).to receive(:new).and_return(report_service)
            allow(report_service).to receive(:create).and_return("")
            BinderService.new(user, binder).inventory_report

            expect(report_service).to have_received(:create)
        end
    end

    describe "#capital_expense_report" do
        it "requests capital_expense_report for binder" do
            user = create(:user, :email => "orphans@homebinder.com")

            binder = create(:binder)
            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")

            report_service = double("CapitalExpenseReportPdfService", :create => true)
            allow(CapitalExpenseReportPdfService).to receive(:new).and_return(report_service)
            allow(report_service).to receive(:create).and_return ""
            BinderService.new(user, binder).capital_expense_report

            expect(report_service).to have_received(:create)
        end
    end

    describe "#inject_homeowner_guide" do
        it "inject_homeowner_guide into binder" do
            user = create(:user, :email => "orphans@homebinder.com")

            binder = create(:binder)
            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")
            binder_template = create(:binder_template, :system => true)
            create(:document_template, :binder_template => binder_template)
            service = BinderService.new(user, binder)
            allow(service).to receive(:download).and_return(File.open('spec/assets/SampleDoc.pdf'))
            service.inject_homeowner_guide
            binder.reload
            expect(binder.documents.count).to eq(1)
        end
    end

    describe "#add_partner" do
        it "adds the partner to the binder" do
            binder_2 = create(:binder, :primary => true)
            homeowner = create(:user, :role => "homeowner")
            create(:user_binder, :binder => binder_2, :user => user, :role => "owner")
            params = {:data => {:role => "owner", :partner_id => partner.id, :client_id => homeowner.id}}

            # make sure partner count and binder count are 1
            expect(binder_2.partners.count).to eq(0)
            expect(partner.binders.count).to eq(1)

            BinderService.new(user, binder_2).add_partner(params)

            expect(binder_1.partners.count).to eq(1)
            expect(partner.binders.count).to eq(2)
        end
    end

    describe "#get_partner" do
        it "gets the partner for the binder" do
            get_partner = BinderService.new(user, binder_1).get_partner
            expect(get_partner["id"]).to eq(partner.id)
        end
    end

    describe "#delete_partner" do
        it "deletes the partner for the binder" do
            BinderService.new(user, binder_1).delete_partner({:partner_binder_id => partner_binder.id})
            expect{PartnerBinder.find(partner_binder.id)}.to raise_error ActiveRecord::RecordNotFound
        end
    end

    describe "#add_user" do
        it "raises BadRequestException" do
            expect{service.add_user({})}.to raise_error BadRequestException
        end

        it "raises BadRequestException" do
            expect{service.add_user({:id => 9999})}.to raise_error BadRequestException
        end

        it "raises error when role is incorrect" do
            expect{service.add_user({:user_id => 99999999, :role => "test"})}.to raise_error(BadRequestException)
        end

        it "raises error when user is not found" do
            expect{service.add_user({:user_id => 99999999, :role => "co_owner"})}.to raise_error(ActiveRecord::RecordNotFound)
        end

        it "adds the user to binder" do
            add_user = create(:user)
            user_binder = service.add_user({:user_id => add_user.id, :role => "co_owner"})

            expect(user_binder).to_not be(nil)
        end

        it "adds the agent to binder" do
            expect(binder_1.binder_contractors.count).to eq(0)
            create(:contractor_contractor_category, :name => "Real Estate")
            agent = create(:user, :role => "agent", created_by: user.id)
            user_binder = service.add_user({:user_id => agent.id, :role => "buyer_agent"})

            expect(user_binder).to_not be(nil)
            expect(binder_1.binder_contractors.count).to eq(1)
        end
    end

    describe "#get_acl" do
        it "gets the list of users with access" do
            user1 = create(:user)
            user1.add_role("co_owner", binder_1)
            user2 = create(:user)
            user2.add_role("reader", binder_1)
            user3 = create(:user)
            user3.add_role("writer", binder_1)

            list = service.get_acl

            expect(list.length).to eq(4)

            expect(list.index { |item| item[:user][:id] == user.id and item[:role] == "owner" }).to_not be_nil
            expect(list.index { |item| item[:user][:id] == user1.id and item[:role] == "co_owner" }).to_not be_nil
            expect(list.index { |item| item[:user][:id] == user2.id and item[:role] == "reader" }).to_not be_nil
            expect(list.index { |item| item[:user][:id] == user3.id and item[:role] == "writer" }).to_not be_nil
        end
    end

    describe "#delete_acl" do
        it "removes access" do
            user1 = create(:user)
            user1.add_role :co_owner, binder_1

            service.delete_acl({:user_id => user1.id})

            expect(user1.has_role?(:co_owner, binder_1)).to eq(false)
            expect(user1.binders.count).to eq(0)
        end
    end

    describe "upgrade" do
        it "attempts to create customer and raise error" do
            token =   OpenStruct.new({
                "id" => "tok_Bg5v5Q8HFSH26d",
                "object" => "token",
                "card" => {
                    "id" =>"card_Bg5vczxbqKyzy3",
                    "object" =>" card",
                    "address_city" => nil,
                    "address_country" => nil,
                    "address_line1" => nil,
                    "address_line1_check" => nil,
                    "address_line2" => nil,
                    "address_state" => nil,
                    "address_zip" => nil,
                    "address_zip_check" => nil,
                    "brand" => "Visa",
                    "country" => "US",
                    "cvc_check" => nil,
                    "dynamic_last4" => nil,
                    "exp_month" => 10,
                    "exp_year" => 2017,
                    "fingerprint" => "SkUolUdsHDm1Ldla",
                    "funding" => "credit",
                    "last4" => "4242",
                    "metadata" => {},
                    "name" => nil,
                    "tokenization_method" => nil,
                    "customer" => nil,
                    "type" => "Visa"
                },
                "client_ip" => "104.154.137.220",
                "created" => 1509394805,
                "livemode" => false,
                "type" => "card",
                "used" => false
            })

            allow(Stripe::Token).to receive(:create).and_return(token)
            allow(Stripe::Customer).to receive(:create).and_raise UnprocessableException

            expect{service.upgrade(nil)}.to raise_error BadRequestException
        end

        it "raises an error" do
            token =   OpenStruct.new({
                "id" => "tok_Bg5v5Q8HFSH26d",
                "object" => "token",
                "card" => {
                    "id" =>"card_Bg5vczxbqKyzy3",
                    "object" =>" card",
                    "address_city" => nil,
                    "address_country" => nil,
                    "address_line1" => nil,
                    "address_line1_check" => nil,
                    "address_line2" => nil,
                    "address_state" => nil,
                    "address_zip" => nil,
                    "address_zip_check" => nil,
                    "brand" => "Visa",
                    "country" => "US",
                    "cvc_check" => nil,
                    "dynamic_last4" => nil,
                    "exp_month" => 10,
                    "exp_year" => 2017,
                    "fingerprint" => "SkUolUdsHDm1Ldla",
                    "funding" => "credit",
                    "last4" => "4242",
                    "metadata" => {},
                    "name" => nil,
                    "tokenization_method" => nil,
                    "customer" => nil,
                    "type" => "Visa"
                },
                "client_ip" => "104.154.137.220",
                "created" => 1509394805,
                "livemode" => false,
                "type" => "card",
                "used" => false
            })

            allow(Stripe::Token).to receive(:create).and_return(token)
            binder_1.subscription.destroy
            subscription = create(:subscription, :binder => binder_1)

            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, delete: true})
            allow(Stripe::Customer).to receive(:create).and_return(stripe_customer)
            allow(subscription).to receive(:save).and_return(false)

            expect{service.upgrade(nil)}.to raise_error UnprocessableException
        end

        it "updates" do
            token =   OpenStruct.new({
                "id" => "tok_Bg5v5Q8HFSH26d",
                "object" => "token",
                "card" => {
                    "id" =>"card_Bg5vczxbqKyzy3",
                    "object" =>" card",
                    "address_city" => nil,
                    "address_country" => nil,
                    "address_line1" => nil,
                    "address_line1_check" => nil,
                    "address_line2" => nil,
                    "address_state" => nil,
                    "address_zip" => nil,
                    "address_zip_check" => nil,
                    "brand" => "Visa",
                    "country" => "US",
                    "cvc_check" => nil,
                    "dynamic_last4" => nil,
                    "exp_month" => 10,
                    "exp_year" => 2017,
                    "fingerprint" => "SkUolUdsHDm1Ldla",
                    "funding" => "credit",
                    "last4" => "4242",
                    "metadata" => {},
                    "name" => nil,
                    "tokenization_method" => nil,
                    "customer" => nil,
                    "type" => "Visa"
                },
                "client_ip" => "104.154.137.220",
                "created" => 1509394805,
                "livemode" => false,
                "type" => "card",
                "used" => false
            })

            allow(Stripe::Token).to receive(:create).and_return(token)
            binder_1.subscription.destroy
            create(:subscription, :binder => binder_1)

            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, delete: true})
            allow(Stripe::Customer).to receive(:create).and_return(stripe_customer)

            result = service.upgrade(nil)
            expect(result).to_not be(nil)
            expect(result.payment_status).to eq("paid")
        end
    end
end
