require 'rails_helper'
require 'mandrill'

RSpec.describe TransferService, :type => :service do

    before :each do
        # create spies
        double = double("AnnualPropertyReviewMailer", :deliver_later => true)
        allow(TransferMailer).to receive(:notify_email_to_agent).and_return(double)
        allow(TransferMailer).to receive(:notify_email_from_inspector).and_return(double)
        allow(TransferMailer).to receive(:notify_email_from_broker).and_return(double)
        allow(TransferMailer).to receive(:notify_email_from_partner).and_return(double)
        allow(TransferMailer).to receive(:notify_email_from_hoa).and_return(double)
        allow(TransferMailer).to receive(:notify_email).and_return(double)
    end

    describe "send_email" do
        it "sends the transfer and updates the status" do
            sender = create(:user)
            receiver = create(:user)
            binder = create(:binder)

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "ownership")

            TransferService.new(transfer.id).send_email(false, false)

            expect(TransferMailer).to have_received(:notify_email)
        end

        it "sends the transfer and updates the status" do
            sender = create(:user)
            receiver = create(:user, :sign_in_count => 1)
            binder = create(:binder)

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "ownership")

            TransferService.new(transfer.id).send_email(false, false)

            expect(TransferMailer).to have_received(:notify_email)
        end

        it "sends the transfer from an inspector but no email to agent, and updates the status" do
            sender = create(:user, :role => "inspector")
            receiver = create(:user, :sign_in_count => 0)
            binder = create(:binder)
            partner = create(:partner, :partner_type => "inspector")
            agent = create(:user, :role => "agent")

            partner.partner_configuration.send_agents_transfer_notification = "always"
            partner.save

            create(:partner_user, :partner_id => partner.id, :user_id => sender.id, :role => "admin")
            create(:user_binder, :user_id => agent.id, :binder_id => binder.id, :role => "buyer_agent")

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "ownership")

            TransferService.new(transfer.id).send_email(true, false)

            # make sure email was sent to receiver and agent
            expect(TransferMailer).to have_received(:notify_email_from_inspector)
            expect(TransferMailer).to_not have_received(:notify_email_to_agent)
        end

        it "sends the transfer from an inspector, email to agent when notification is always, and updates the status" do
            sender = create(:user, :role => "inspector")
            receiver = create(:user, :sign_in_count => 0)
            binder = create(:binder)
            partner = create(:partner, :partner_type => "inspector")
            agent = create(:user, :role => "agent")

            partner.partner_configuration.send_agents_transfer_notification = "always"
            partner.save

            create(:partner_user, :partner_id => partner.id, :user_id => sender.id, :role => "admin")
            create(:user_binder, :user_id => agent.id, :binder_id => binder.id, :role => "buyer_agent")

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "ownership")

            TransferService.new(transfer.id).send_email(false, false)

            # make sure email was sent to receiver and agent
            expect(TransferMailer).to have_received(:notify_email_from_inspector)
            expect(TransferMailer).to have_received(:notify_email_to_agent)
        end
        
        it "sends the transfer from an inspector, email to agent when notification is once, and updates the status" do
            sender = create(:user, :role => "inspector")
            receiver = create(:user, :sign_in_count => 0)
            binder = create(:binder)
            partner = create(:partner, :partner_type => "inspector")
            agent = create(:user, :role => "agent")

            partner.partner_configuration.send_agents_transfer_notification = "once"
            partner.save

            create(:partner_user, :partner_id => partner.id, :user_id => sender.id, :role => "admin")
            create(:user_binder, :user_id => agent.id, :binder_id => binder.id, :role => "buyer_agent")

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "ownership")

            TransferService.new(transfer.id).send_email(false, false)

            # make sure email was sent to receiver and agent
            expect(TransferMailer).to have_received(:notify_email_from_inspector)
            expect(TransferMailer).to have_received(:notify_email_to_agent)
        end

        it "sends the transfer from hoa and updates the status" do
            sender = create(:user, :role => "hoa")
            receiver = create(:user, :sign_in_count => 0)
            binder = create(:binder)
            partner = create(:partner, :partner_type => "hoa")
            create(:partner_user, :partner_id => partner.id, :user_id => sender.id, :role => "admin")

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "ownership")

            TransferService.new(transfer.id).send_email(false, false)

            # make sure email was sent to receiver
            expect(TransferMailer).to have_received(:notify_email_from_hoa)
        end

        it "sends the transfer from broker and updates the status" do
            sender = create(:user, :role => "broker")
            receiver = create(:user, :sign_in_count => 0)
            binder = create(:binder)
            partner = create(:partner, :partner_type => "broker")
            create(:partner_user, :partner_id => partner.id, :user_id => sender.id, :role => "admin")

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "ownership")

            TransferService.new(transfer.id).send_email(false, false)

            # make sure email was sent to receiver
            expect(TransferMailer).to have_received(:notify_email_from_broker)
        end
        
        it "sends the transfer from broker and updates the status" do
            sender = create(:user, :role => "lender")
            receiver = create(:user, :sign_in_count => 0)
            binder = create(:binder)
            partner = create(:partner, :partner_type => "lender")
            create(:partner_user, :partner_id => partner.id, :user_id => sender.id, :role => "admin")

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "ownership")

            TransferService.new(transfer.id).send_email(false, false)

            # make sure email was sent to receiver
            expect(TransferMailer).to have_received(:notify_email_from_partner)
        end
        
        it "sends the transfer from agent and updates the status" do
            sender = create(:user, :role => "agent")
            receiver = create(:user, :sign_in_count => 0)
            binder = create(:binder)
            partner = create(:partner, :partner_type => "inspector")
            create(:partner_user, :partner_id => partner.id, :user_id => sender.id, :role => "agent")

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "ownership")

            TransferService.new(transfer.id).send_email(false, false)

            # make sure email was sent to receiver
            expect(TransferMailer).to have_received(:notify_email)
        end
    end

    describe "transfer_ownership" do
        it "transfers ownership" do
            sender = create(:user)
            receiver = create(:user)
            binder = create(:binder)

            user_binder = create(:user_binder, :user_id => sender.id, :binder_id => binder.id, :role => "owner")
            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "ownership")

            TransferService.new(transfer.id).transfer_ownership

            owner = User.with_role(:owner, binder).first
            expect(owner.email).to eq(receiver.email)
            expect(UserBinder.where(:id => user_binder.id).count).to eq(0)
        end
    end

    describe "transfer_back_ownership" do
        it "transfers ownership to orphans" do
            sender = create(:user)
            orphans = create(:user, :email => "orphans@homebinder.com")
            receiver = create(:user)
            binder = create(:binder)

            user_binder = create(:user_binder, :user_id => sender.id, :binder_id => binder.id, :role => "owner")
            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "ownership")

            TransferService.new(transfer.id).transfer_back_ownership

            owner = User.with_role(:owner, binder).first
            expect(owner.email).to eq(orphans.email)
            expect(UserBinder.where(:id => user_binder.id).count).to eq(0)
        end
    end

    describe "transfer_copy" do
        it "makes a copy of a binder" do
            sender = create(:user)
            receiver = create(:user, :create_method => "transfer")
            binder = create(:binder)
            create(:seller_report, :binder => binder)

            sub = OpenStruct.new({:plan =>  OpenStruct.new({:amount => 100})})
            subscriptions = OpenStruct.new({:total_count => 1, :data => [sub]})
            discount = OpenStruct.new({:coupon => OpenStruct.new({:amount_off => 100, :duration => "forever"})})
            stripe_customer = OpenStruct.new({id: 1, email: "test@gmail.com", save: true, source: nil, delete: true, subscriptions: subscriptions, discount: discount})
            allow(Stripe::Customer).to receive(:retrieve).and_return(stripe_customer)

            subscription = create(:subscription, :customer_id => 1)
            binder.subscription = subscription
            binder.save!

            create_list(:appliance, 2, :binder => binder)

            create_list(:maintenance_item, 2, :binder => binder)
            create_list(:binder_contractor, 2, :binder => binder)
            create_list(:project, 2, :binder => binder, :status => "Completed")
            create_list(:paint, 2, :binder => binder)
            create_list(:finish, 2, :binder => binder)
            create_list(:document, 2, :binder => binder)
            create_list(:image, 2, :binder => binder)
            create_list(:structure, 2, :binder => binder)
            create_list(:area, 2, :binder => binder)

            binder.appliances.each do |app|
                app.tags.new(:tag => "test").save!
                create(:seller_report_item, :appliance_id => app.id, :include => true)
            end

            binder.maintenance_items.each do |item|
                create(:seller_report_item, :maintenance_item_id => item.id, :include => true)
            end

            binder.projects.each do |project|
                create(:seller_report_item, :project_id => project.id, :include => true)
            end

            binder.binder_contractors.each do |binder_contractor|
                create(:seller_report_item, :binder_contractor_id => binder_contractor.id, :include => true)
            end

            binder.paints.each do |paint|
                create(:seller_report_item, :paint_id => paint.id, :include => true)
            end

            binder.finishes.each do |finish|
                create(:seller_report_item, :finish_id => finish.id, :include => true)
            end

            binder.documents.each do |doc|
                create(:seller_report_item, :document_id => doc.id, :include => true)
            end

            binder.images.each do |img|
                create(:seller_report_item, :image_id => img.id, :include => true)
            end

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "copy")

            # make sure receiver doesnt have any binders
            expect(receiver.binders.count).to eq(0)

            TransferService.new(transfer.id).transfer_copy

            # make sure receiver received a copy
            expect(receiver.binders.count).to eq(1)

            # get the first binder
            copy = receiver.binders.first

            # make sure the binder is a copy
            expect(binder.name).to eq(copy.name)

            # make sure the new binder has a pending transfer
            expect(copy.transfers.count).to eq(1)

            # make sure receiver waiting binders count is 1
            expect(receiver.waiting_binders[:binders].length).to eq(1)
        end
    end

    describe "update_receiver" do
        it "updates to_user" do
            sender = create(:user)
            receiver = create(:user)
            binder = create(:binder)

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "copy")

            to_user = {
                :first => "first",
                :last => "last",
                :phone => "1-249-591-2395",
                :email => "test@gmail.com"
            }

            TransferService.new(transfer.id).update_receiver(to_user)

            # make sure receiver information was updated
            receiver.reload
            expect(receiver.email).to eq("test@gmail.com")
            expect(receiver.user_profile.first_name).to eq("first")
            expect(receiver.user_profile.last_name).to eq("last")
            expect(receiver.user_profile.mobile_phone).to eq("+12495912395")
        end

        it "raises an error" do
            sender = create(:user)
            receiver = create(:user)
            binder = create(:binder)

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "copy")

            to_user = {
                :first => "first",
                :last => "last",
                :phone => "1-249-591-2395",
                :email => "test@gmail.com"
            }

            allow_any_instance_of(User).to receive(:update_attributes).and_return(false)
            expect{TransferService.new(transfer.id).update_receiver(to_user)}.to raise_error UnprocessableException
        end
    end

    describe "handle_webhook" do
        it "updates the transfer status" do
            # create the users/binders/transfer
            sender = create(:user)
            receiver = create(:user)
            binder = create(:binder)
            create(:user_binder, :user_id => sender.id, :binder_id => binder.id, :role => "owner")
            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "copy")

            params = {
                id: transfer.id,
                user: {
                        first: receiver.user_profile.first_name,
                        last: receiver.user_profile.last_name,
                        email: receiver.email,
                        phone: receiver.user_profile.mobile_phone
                    },
                transfer_type: "ownership",
                cc_transfer_by: false
            }

            # transfer the binder
            Binder::Transfer.execute(sender, params)

            # handle the webhook
            TransferService.new(transfer.id).handle_webhook("delivery_failed")

            # make sure the transfer was changed
            transfer.reload
            expect(transfer.status).to eq("delivery_failed")
        end
    end

    describe "get_partner" do
        it "retrieves nil partner" do
            sender = create(:user)
            receiver = create(:user)
            binder = create(:binder)

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "copy")

            # make sure receiver doesnt have any binders
            expect(receiver.binders.count).to eq(0)

            partner = TransferService.new(transfer.id).get_partner

            # make sure partner is nil
            expect(partner).to be(nil)
        end

        it "retrieves nil partner for inspector" do
            sender = create(:user, :role => "inspector")
            receiver = create(:user)
            binder = create(:binder)

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "copy")

            partner = TransferService.new(transfer.id).get_partner

            # make sure partner is nil
            expect(partner).to be(nil)
        end

        it "retrieves nil partner for admin" do
            sender = create(:user, :role => "admin")
            receiver = create(:user)
            binder = create(:binder)

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "copy")

            partner = TransferService.new(transfer.id).get_partner

            # make sure partner is nil
            expect(partner).to be(nil)
        end

        it "retrieves partner for inspector" do
            sender = create(:user, :role => "inspector")
            receiver = create(:user)
            partner = create(:partner)
            create(:partner_user, :partner_id => partner.id, :user_id => sender.id, :role => "admin")
            binder = create(:binder)

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "copy")

            found_partner = TransferService.new(transfer.id).get_partner

            # make sure partner is not nil
            expect(found_partner).to_not be(nil)
            expect(found_partner).to_not be(partner.id)
        end

        it "retrieves partner for broker" do
            sender = create(:user, :role => "broker")
            receiver = create(:user)
            partner = create(:partner, :partner_type => "broker")
            create(:partner_user, :partner_id => partner.id, :user_id => sender.id, :role => "admin")
            binder = create(:binder)

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "copy")

            found_partner = TransferService.new(transfer.id).get_partner

            # make sure partner is not nil
            expect(found_partner).to_not be(nil)
            expect(found_partner).to_not be(partner.id)
        end
    end

    describe "copy_item" do
        it "returns copy" do
            sender = create(:user)
            receiver = create(:user)
            binder = create(:binder)
            maintenance_item = create(:maintenance_item, :binder_id => binder.id)
            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "copy")

            item = TransferService.new(transfer.id).copy_item(maintenance_item)

            # make sure item is equal to maintenance_item
            expect(item.name).to eq(maintenance_item.name)
        end

        it "returns nil" do
            sender = create(:user)
            receiver = create(:user)
            binder = create(:binder)

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "copy")

            item = TransferService.new(transfer.id).copy_item(nil)

            # make sure item is nil
            expect(item).to be(nil)
        end
    end

    describe "send_agent_email" do
        it "does not send agent email because partner is nil" do
            sender = create(:user)
            receiver = create(:user)
            binder = create(:binder)

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "copy")

            TransferService.new(transfer.id).send_agent_email

            expect(TransferMailer).to_not have_received(:notify_email_to_agent)
        end

        it "does not send agent email because agent_binder is nil" do
            sender = create(:user, :role => "inspector")
            receiver = create(:user)
            partner = create(:partner)
            binder = create(:binder)

            create(:partner_user, :partner_id => partner.id, :user_id => sender.id, :role => "admin")

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "copy")

            TransferService.new(transfer.id).send_agent_email

            expect(TransferMailer).to_not have_received(:notify_email_to_agent)
        end

        it "does not send agent email partner does not have send_agents_transfer_notification set as true" do
            sender = create(:user, :role => "inspector")
            receiver = create(:user)
            partner = create(:partner)
            binder = create(:binder)
            agent = create(:user, :role => "agent")
            
            partner.partner_configuration.send_agents_transfer_notification = "never"
            partner.partner_configuration.save!
            
            create(:partner_user, :partner_id => partner.id, :user_id => sender.id, :role => "admin")
            create(:user_binder, :user_id => agent.id, :binder_id => binder.id, :role => "buyer_agent")

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "copy")

            TransferService.new(transfer.id).send_agent_email

            expect(TransferMailer).to_not have_received(:notify_email_to_agent)
        end

        it "sends agent email when transaction_type is sell_side_inspection" do
            sender = create(:user, :role => "inspector")
            receiver = create(:user)
            partner = create(:partner)
            binder = create(:binder)
            agent = create(:user, :role => "agent")

            partner.partner_configuration.send_agents_transfer_notification = "always"
            partner.save

            create(:partner_user, :partner_id => partner.id, :user_id => sender.id, :role => "admin")
            create(:user_binder, :user_id => agent.id, :binder_id => binder.id, :role => "buyer_agent")
            binder.transactions.create!(:transaction_type => "sell_side_inspection")

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "copy")

            TransferService.new(transfer.id).send_agent_email

            expect(TransferMailer).to have_received(:notify_email_to_agent)
        end

        it "sends agent email" do
            sender = create(:user, :role => "inspector")
            receiver = create(:user)
            partner = create(:partner)
            binder = create(:binder)
            agent = create(:user, :role => "agent")

            partner.partner_configuration.send_agents_transfer_notification = "always"
            partner.save

            create(:partner_user, :partner_id => partner.id, :user_id => sender.id, :role => "admin")
            create(:user_binder, :user_id => agent.id, :binder_id => binder.id, :role => "buyer_agent")

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "copy")

            TransferService.new(transfer.id).send_agent_email

            expect(TransferMailer).to have_received(:notify_email_to_agent)
        end
    end

    describe "add_widgets" do
        it "does not add two widgets to dashboard" do
            sender = create(:user)
            receiver = create(:user)
            binder = create(:binder)

            create(:user_binder, :user_id => sender.id, :binder_id => binder.id, :role => "owner")

            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "copy")

            TransferService.new(transfer.id).add_widgets

            dashboard = Dashboards::DashboardLookup.new(sender).for_binder({:id => binder.id})

            expect(dashboard.widgets.count).to eq(4)
        end

        it "does not add two widgets to dashboard since user does not belong to a partner" do
            sender = create(:user)
            receiver = create(:user)
            binder = create(:binder)

            create(:user_binder, :user_id => receiver.id, :binder_id => binder.id, :role => "owner")

            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "accepted", :transfer_type => "ownership")

            TransferService.new(transfer.id).add_widgets

            dashboard = Dashboards::DashboardLookup.new(sender).for_binder({:id => binder.id})

            expect(dashboard.widgets.count).to eq(4)
        end

        it "does not add two widgets to dashboard because widgets are excluded" do
            sender = create(:user, :role => "inspector")
            partner = create(:partner)
            create(:partner_user, :user_id => sender.id, :partner_id => partner.id, :role => "admin")
            receiver = create(:user)
            binder = create(:binder)

            create(:user_binder, :user_id => receiver.id, :binder_id => binder.id, :role => "owner")
            create(:partner_binder, :binder_id => binder.id, :partner_id => partner.id)

            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards

            Widget.find_each do |widget|
                create(:widget_exclusion, :partner_id => partner.id, :widget_id => widget.id)
            end

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "accepted", :transfer_type => "ownership")

            TransferService.new(transfer.id).add_widgets

            dashboard = Dashboards::DashboardLookup.new(receiver).for_binder({:id => binder.id})

            expect(dashboard.widgets.count).to eq(4)
        end

        it "adds two widgets to dashboard" do
            sender = create(:user, :role => "inspector")
            partner = create(:partner)
            partner.partner_configuration.repair_pricer_enabled = true
            partner.partner_configuration.repair_pricer_messaging_enabled = true
            partner.save!
            create(:partner_user, :user_id => sender.id, :partner_id => partner.id, :role => "admin")
            receiver = create(:user)
            binder = create(:binder)

            create(:user_binder, :user_id => receiver.id, :binder_id => binder.id, :role => "owner")

            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "accepted", :transfer_type => "ownership")

            TransferService.new(transfer.id).add_widgets

            dashboard = Dashboards::DashboardLookup.new(receiver).for_binder({:id => binder.id})

            expect(dashboard.widgets.count).to eq(7)
        end

        it "adds three widgets to dashboard" do
            sender = create(:user, :role => "inspector")
            partner = create(:partner)
            partner.partner_configuration.repair_pricer_enabled = true
            partner.partner_configuration.repair_pricer_messaging_enabled = true
            partner.save!
            create(:partner_user, :user_id => sender.id, :partner_id => partner.id, :role => "admin")
            receiver = create(:user)
            binder = create(:binder)
            create(:partner_binder, :partner => partner, :binder => binder)
            organization = create(:organization, :name => "Amerispec")
            create(:organization_partner, :partner => partner, :organization => organization)
            create(:user_binder, :user_id => receiver.id, :binder_id => binder.id, :role => "owner")

            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "accepted", :transfer_type => "ownership")

            TransferService.new(transfer.id).add_widgets

            dashboard = Dashboards::DashboardLookup.new(receiver).for_binder({:id => binder.id})

            expect(dashboard.widgets.count).to eq(9)
        end

        it "adds four widgets to dashboard" do
            sender = create(:user, :role => "inspector")
            agent = create(:user, :role => "agent")
            partner = create(:partner)
            partner.partner_configuration.repair_pricer_enabled = true
            partner.partner_configuration.repair_pricer_messaging_enabled = true
            partner.save!
            create(:partner_user, :user_id => sender.id, :partner_id => partner.id, :role => "admin")
            receiver = create(:user)
            binder = create(:binder)
            create(:partner_binder, :partner => partner, :binder => binder)
            organization = create(:organization, :name => "Amerispec")
            create(:organization_partner, :partner => partner, :organization => organization)
            create(:user_binder, :user_id => receiver.id, :binder_id => binder.id, :role => "owner")
            create(:user_binder, :user_id => agent.id, :binder_id => binder.id, :role => "buyer_agent")
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "accepted", :transfer_type => "ownership")

            TransferService.new(transfer.id).add_widgets

            dashboard = Dashboards::DashboardLookup.new(receiver).for_binder({:id => binder.id})

            expect(dashboard.widgets.count).to eq(9)
        end
        
        it "adds 11 widgets to dashboard" do
            sender = create(:user, :role => "inspector")
            agent = create(:user, :role => "agent")
            partner = create(:partner)
            partner.partner_configuration.repair_pricer_enabled = true
            partner.partner_configuration.repair_pricer_messaging_enabled = true
            partner.save!
            create(:partner_user, :user_id => sender.id, :partner_id => partner.id, :role => "admin")
            receiver = create(:user)
            binder = create(:binder)
            create(:partner_binder, :partner => partner, :binder => binder)
            organization = create(:organization, :name => "Amerispec")
            create(:organization_partner, :partner => partner, :organization => organization)
            create(:user_binder, :user_id => receiver.id, :binder_id => binder.id, :role => "owner")
            create(:user_binder, :user_id => agent.id, :binder_id => binder.id, :role => "buyer_agent")
            document_type = create(:document_type, :name => "Home Inspection Report")
            create(:document, :document_type_id => document_type.id, binder: binder)
            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards
                
            customWidget = Widget.find_by_name("Custom")
            create(:partner_widget, :partner => partner, :widget => customWidget)
            
            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "accepted", :transfer_type => "ownership")

            TransferService.new(transfer.id).add_widgets

            dashboard = Dashboards::DashboardLookup.new(receiver).for_binder({:id => binder.id})

            expect(dashboard.widgets.count).to eq(11)
        end

        it "adds 2 widgets to dashboard when transaction type is sell_side_inspection" do
            sender = create(:user, :role => "inspector")
            agent = create(:user, :role => "agent")
            partner = create(:partner)
            partner.partner_configuration.repair_pricer_enabled = true
            partner.partner_configuration.repair_pricer_messaging_enabled = true
            partner.save!
            create(:partner_user, :user_id => sender.id, :partner_id => partner.id, :role => "admin")
            receiver = create(:user)
            binder = create(:binder)
            create(:binder_transaction, :transaction_type => "sell_side_inspection", :binder => binder)
            create(:partner_binder, :partner => partner, :binder => binder)
            organization = create(:organization, :name => "Amerispec")
            create(:organization_partner, :partner => partner, :organization => organization)
            create(:user_binder, :user_id => receiver.id, :binder_id => binder.id, :role => "owner")
            create(:user_binder, :user_id => agent.id, :binder_id => binder.id, :role => "buyer_agent")

            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "accepted", :transfer_type => "ownership")

            TransferService.new(transfer.id).add_widgets

            dashboard = Dashboards::DashboardLookup.new(receiver).for_binder({:id => binder.id})

            expect(dashboard.widgets.count).to eq(6)
        end

        it "adds only 1 widget to dashboard" do
            sender = create(:user, :role => "inspector")
            partner = create(:partner)
            partner.partner_configuration.repair_pricer_enabled = true
            partner.partner_configuration.repair_pricer_messaging_enabled = true
            partner.save!
            create(:partner_user, :user_id => sender.id, :partner_id => partner.id, :role => "admin")
            receiver = create(:user)
            binder = create(:binder)

            create(:user_binder, :user_id => receiver.id, :binder_id => binder.id, :role => "owner")

            # change the property country
            binder.property.state = "BC"
            binder.property.country = "CA"
            binder.property.save

            Rake::SeedJob.new.create_widgets
            Rake::SeedJob.new.create_dashboards

            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "accepted", :transfer_type => "ownership")

            TransferService.new(transfer.id).add_widgets

            dashboard = Dashboards::DashboardLookup.new(receiver).for_binder({:id => binder.id})

            expect(dashboard.widgets.count).to eq(6)
        end
    end
    
    describe "send_text_message" do
        it "does not send message" do
            link = OpenStruct.new({:short_url => "link"})
            bitly = OpenStruct.new({:create => link})
            allow(BitlyService).to receive(:new).and_return(bitly)
            allow(bitly).to receive(:create).and_return(link)
            
            message = OpenStruct.new({:sid => "FAKE"})
            text_service = OpenStruct.new({:send_message => message})
            allow(TextService::Sms).to receive(:new).and_return(text_service)
            allow(text_service).to receive(:send_message).and_return(message)
            
            sender = create(:user)
            receiver = create(:user)
            binder = create(:binder)
            transfer = create(:transfer, :sender_id => sender.id, :receiver_id => receiver.id, :binder_id => binder.id, :status => "created", :transfer_type => "copy")

            TransferService.new(transfer.id).send_text_message
            transfer.reload
            
            expect(transfer.text_message_sid).to eq("FAKE")
        end
    end
end