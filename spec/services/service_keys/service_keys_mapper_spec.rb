require 'rails_helper'

RSpec.describe ServiceKeys::ServiceKeysMapper do
    
    describe "#get_keys" do
        it "returns nil when argument is nil" do
            key = ServiceKeys::ServiceKeysMapper.get_keys(nil)
            expect(key).to be(nil)
        end
        it "returns nil when argument is not an API Key" do
            key = ServiceKeys::ServiceKeysMapper.get_keys("test")
            expect(key).to be(nil)
        end
        it "returns nil when company name is not HomeBinder" do
            api_key = create(:api_key, :key => "test", :company_name => "test")
            key = ServiceKeys::ServiceKeysMapper.get_keys(api_key)
            expect(key).to be(nil)
        end
        it "returns the stripe key" do
            create(:api_key, :key => "test", :company_name => "HomeBinder.com")
            key = ServiceKeys::ServiceKeysMapper.get_keys("test")
            expect(key).to_not be(nil)
        end
    end
  
end