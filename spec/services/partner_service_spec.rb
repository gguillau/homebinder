require 'rails_helper'

RSpec.describe PartnerService, :type => :service do
    before :each do
        @admin = create(:user, :role => "admin")
        @user = create(:user, :role => "inspector")
        partner = {
            name: "Test Partner Company",
            contact: "Test Partner Name",
            email: @user.email,
            partner_type: "inspector",
            code: "test",
            address_attributes: {
                country: "US"
            }
        }

        partner = ActionController::Parameters.new(partner)
        @partner = Partner.new(partner.permit!)
        @partner.save!

        PartnerUser.create!(partner_id: @partner.id, user_id: @user.id, role: UserPartnerRoles::ADMIN)

        share_mailer = double("ShareMailer", :deliver_later => true)
        allow(ShareMailer).to receive(:notify_email).and_return(share_mailer)
        allow(AccountMailer).to receive(:auto_generated_support_email).and_return(share_mailer)
        allow(WarrantyMailer).to receive(:send_warranty_request).and_return(share_mailer)
        allow(UserMailer).to receive(:send_invitation).and_return(share_mailer)
    end

    describe "#after_create" do
        it "adds config and billing objects to partner" do
            PartnerService.new(@user, @partner).after_create

            expect(@partner.code).to_not be_nil
            expect(@partner.partner_configuration).to_not be_nil
            expect(@partner.account).to_not be_nil
            expect(@partner.api_key).to_not be_nil
            expect(@partner.partner_key).to_not be_nil
        end
    end

    describe "#create_configuration" do
        it "creates the config object for a partner" do
            note = "Thank you for trusting me with your business. I wish you the best of luck in your home ownership and would welcome the opportunity to work with any friends or family members that you know who might be in need of my services. I'm never too busy for your referrals!"
            PartnerService.new(@user, @partner).create_configuration

            expect(@partner.partner_configuration).to_not be(nil)
            expect(@partner.partner_configuration.default_user_branding_id).to eq(@user.user_profile.id)
            expect(@partner.partner_configuration.repair_pricer_enabled).to be(true)
            expect(@partner.partner_configuration.binder_templates.count).to eq(1)
            expect(@partner.partner_configuration.binder_templates.first.transfer_note).to eq(note)
        end
    end

    describe "#create_billing_account" do
        it "creates the billing account object for a partner" do
            PartnerService.new(@user, @partner).create_billing_account

            expect(@partner.account).to_not be(nil)
            expect(@partner.account.manager_id).to eq(@partner.id)
            expect(@partner.account.name).to eq("#{@partner.name} - #{@partner.id}")
            expect(@partner.account.account_type).to eq("single")
            expect(@partner.account.account_sub_type).to eq("free_trial")
            expect(@partner.account.account_status).to eq("active")
            date = Date.today + 14.days
            expect(@partner.account.trial_expiration.to_date).to eq(date)
        end
    end

    describe "#generate_api_key" do
        it "creates the api_key for a partner" do
            PartnerService.new(@user, @partner).generate_api_key

            expect(@partner.api_key).to_not be(nil)
            expect(@partner.api_key.company_name).to eq(@partner.name)
            expect(@partner.api_key.application_name).to eq("Website")
            expect(@partner.api_key.contact_email).to eq(@partner.email)
        end
    end

    describe "#generate_route_name" do
        it "generates route name using partner contact" do
            partner = create(:partner, :name => "fnxtoxysrvtyqnjyfnxtoxysrvtyqnjy", :contact => "Test User")
            partner = PartnerService.new(@admin, partner).generate_route_name

            expect(partner.partner_key).to eq("tuser")
        end

        it "generates route name using partner contact" do
            partner = create(:partner, :name => "abc", :contact => "Test User")
            partner = PartnerService.new(@admin, partner).generate_route_name

            expect(partner.partner_key).to eq("tuser")
        end

        it "reports an error when partner with contact and partner_key already exist" do
            allow(ErrorService).to receive(:perform_async)

            create(:partner, :name => "abc", :contact => "Test User", :partner_key => "tuser")
            partner = create(:partner, :name => "abc", :contact => "Test User")
            PartnerService.new(@admin, partner).generate_route_name

            expect(ErrorService).to have_received(:perform_async)
        end
    end

    describe "#update_coupon" do
        it "creates a coupon" do
            allow(Stripe::Coupon).to receive(:retrieve).and_raise BadRequestException
            allow(Stripe::Coupon).to receive(:create)

            PartnerService.new(@user, @partner).update_coupon
            expect(Stripe::Coupon).to have_received(:create)
        end
    end

    describe "#create_coupon" do
        it "calls new relic" do
            allow(ErrorService).to receive(:perform_async)
            allow(Stripe::Coupon).to receive(:create).and_raise BadRequestException

            PartnerService.new(@user, @partner).create_coupon("fake")
            expect(ErrorService).to have_received(:perform_async)
        end
    end

    describe "#after_update" do
        it "updates the partner" do
            service = PartnerService.new(@user, @partner)

            allow(Partners::UpdatedJob).to receive(:perform_async)
            allow(service).to receive(:update_salesforce)
            allow(service).to receive(:update_coupon)
            allow(service).to receive(:update_users)

            service.after_update

            expect(service).to have_received(:update_salesforce)
            expect(service).to have_received(:update_coupon)
            expect(service).to have_received(:update_users)
            expect(Partners::UpdatedJob).to have_received(:perform_async)
        end
    end

    describe "#complete_onboarding" do
        it "adds the inspector to the organization" do
            create(:organization, :name => "HomeBinder Inspectors", :description => "All HomeBinder inspectors")

            @partner.contact = "Test Name"
            partner = ActionController::Parameters.new(@partner.attributes.except(:id))
            data = {:employees => {:range => "50+"}, :inspections => {:range => "1100+"},:source => {"Social Media"=> true}, :software => {"Other" => true, "Horizon" => true, "ISN" => true}}

            service = PartnerService.new(@user, @partner)
            allow(service).to receive(:create_salesforce_account)
            allow(service).to receive(:add_to_org)
            allow(service).to receive(:share_binder)

            partner = service.complete_onboarding(ActionController::Parameters.new({partner: {partner: partner, data: data}}))

            expect(partner).to eq(@partner)
            expect(@partner.contact).to eq("Test Name")
            expect(@partner.completed_onboarding).to eq(true)
            expect(service).to have_received(:create_salesforce_account)
            expect(service).to have_received(:add_to_org)
            expect(service).to have_received(:share_binder)
            expect(AccountMailer).to have_received(:auto_generated_support_email)
        end
    end

    describe "#add_to_org" do
        it "adds partner to org and creates resources" do
            org = create(:organization, :name => "HomeBinder Inspectors", :description => "All HomeBinder inspectors")
            resource = create(:marketing_resource, :user_type => "inspector")
            OrganizationResource.create(:organization_id => org.id, :marketing_resource_id => resource.id)

            # check that the user does not have any resources
            expect(@partner.resources.count).to eq(0)
            expect(@partner.organizations.count).to eq(0)

            # add the partner to the org
            PartnerService.new(@user, @partner).add_to_org("HomeBinder Inspectors", "All HomeBinder inspectors")
            @partner.reload

            expect(@partner.resources.count).to eq(1)
            expect(@partner.organizations.count).to eq(1)
        end
    end

    describe "#share_binder" do
        it "shares a binder with the partner" do
            user = create(:user, email: "johnsmith@sampleinspector.com", role: "inspector")
            binder = create(:binder)
            user.add_role :owner, binder

            # check that the user has 0 binders
            expect(@user.binders.count).to eq(0)
            # share the binder
            PartnerService.new(@user, @partner).share_binder

            # check that the user does have share access
            @user.reload
            expect(@user.binders.count).to eq(1)

            # make sure the user sharing the binder still has access
            user = User.find_by_email("johnsmith@sampleinspector.com")
            expect(user.binders.count).to eq(1)
        end

        it "sends an error to New Relic when user is not found" do
            allow(ErrorService).to receive(:perform_async)
            PartnerService.new(@user, @partner).share_binder
            expect(ErrorService).to have_received(:perform_async).with("Sample user johnsmith@sampleinspector.com not found", {:task => "share_binder"})
        end

        it "sends an error to New Relic when binder is not found" do
            allow(ErrorService).to receive(:perform_async)
            create(:user, email: "johnsmith@sampleinspector.com", role: "inspector")

            PartnerService.new(@user, @partner).share_binder
            expect(ErrorService).to have_received(:perform_async).with("Sample binder for johnsmith@sampleinspector.com not found", {:task => "share_binder"})
        end
    end

    describe "#enable_warranty_account" do
        it "enables the warranty account and sends an email" do
            @partner.create_partner_configuration

            PartnerService.new(@user, @partner).enable_warranty_account
            @partner.reload

            expect(@partner.partner_configuration.active_warranty_account).to eq(true)

            # check the call
            expect(WarrantyMailer).to have_received(:send_warranty_request)
        end
    end

    describe "#add_user" do
        it "creates a new user and adds the user" do
            user = create(:user, email: "test@gmail.com", role: "inspector")
            PartnerService.new(@user, @partner).add_user(user, "inspector")

            expect(user.email).to eq("test@gmail.com")
            expect(user.partners.count).to eq(1)
            expect(@partner.users.count).to eq(2)
            expect(UserMailer).to have_received(:send_invitation)
        end

        it "adds the user" do
            add_me = create(:user, :email => "addme@test.com", :role => "agent")
            PartnerService.new(@user, @partner).add_user(add_me, "agent")

            expect(add_me.partners.count).to eq(1)
            expect(@partner.users.count).to eq(2)
        end
        it "adds the user and changes the role" do
            add_me = create(:user, :email => "addme@test.com", :role => "homeowner")
            expect(add_me.role).to eq("homeowner")
            PartnerService.new(@user, @partner).add_user(add_me, "agent")

            expect(add_me.partners.count).to eq(1)
            expect(@partner.users.count).to eq(2)
            # expect the user role to change
            add_me.reload
            expect(add_me.role).to eq("agent")
        end
    end

    describe "#remove_user_from_partner" do
        it "does not remove the user because us is the admin" do
            expect{PartnerService.new(@user, @partner).remove_user({:user_id => @user.id})}.to raise_exception(BadRequestException)
        end

        it "removes the user" do
            @partner.create_partner_configuration
            member = create(:user, :role => "inspector", :user_profile_attributes => {:company => @partner.name})
            member.add_role :member, @partner

            # set the default branding id
            @partner.partner_configuration.default_user_branding_id = member.user_profile.id
            @partner.save

            # create a binder
            binder = create(:binder)
            UserBinder.create(:binder_id => binder.id, :user_id => member.id, :role => "owner")

            expect(@user.binders.count).to eq(0)
            expect(@partner.partner_configuration.default_user_branding_id).to eq(member.user_profile.id)

            PartnerService.new(@user, @partner).remove_user({:user_id => member.id})

            expect{User.find(member.id)}.to raise_exception(ActiveRecord::RecordNotFound)
            expect(@user.binders.count).to eq(1)
            expect(@partner.partner_configuration.default_user_branding_id).to eq(@user.user_profile.id)
        end
    end

    describe "#change_default_branding_user" do
        it "sends an error" do
            @partner.create_partner_configuration
            allow(ErrorService).to receive(:perform_async)
            inspector = create(:user, :role => "inspector")
            create(:partner_user, :role => "partner_member", :partner => @partner, :user => inspector)

            @partner.partner_configuration.default_user_branding_id = inspector.user_profile.id
            @partner.save!
            @partner.email = nil

            PartnerService.new(@user, @partner).change_default_branding_user(inspector, @user)

            expect(ErrorService).to have_received(:perform_async)
        end
    end

    describe "#find_warranty_plans" do
        it "returns the plans" do
            plans = PartnerService.new(@user, @partner).find_warranty_plans
            expect(plans.length).to eq(0)
        end
    end

    describe "widget_exclusions" do
        it "returns the widget_exclusions" do
            WidgetExclusion.create(:partner_id => @partner.id)
            widget_exclusions = PartnerService.new(@user, @partner).widget_exclusions

            expect(widget_exclusions.length).to eq(1)
        end
    end

    describe "#import_users" do
        it "creates a user" do
            user = {"first_name" => "Mike", "last_name" => "Davis", "email" => "mikedavis@gmail.com", "company" => "Keller Williams"}
            PartnerService.new(@user, @partner).import_users({:role => "agent", :users => [user]})

            user = User.last

            expect(user.email).to eq("mikedavis@gmail.com")
            expect(user.partners.count).to eq(1)
            expect(@partner.users.count).to eq(2)
        end

        it "creates the users" do
            users = []
            number = 1

            2.times do
                users.push({"first_name" => "Mike", "last_name" => "Davis", "email" => "mikedavis#{number}@gmail.com", "company" => "Keller Williams"})
                number += 1
            end

            allow(users).to receive(:length).and_return(51)

            PartnerService.new(@user, @partner).import_users({:role => "agent", :users => users})

            expect(@partner.users.count).to eq(3)
        end
    end

    describe "#create_imported_users" do
        it "raises an error" do
            allow(ErrorService).to receive(:perform_async)
            users = [{"first_name" => "Mike", "last_name" => "Davis", "email" => "mike", "company" => "Keller Williams"}]
            PartnerService.new(@user, @partner).create_imported_users(users, "agent")

            expect(ErrorService).to have_received(:perform_async)
        end
        it "raises an error" do
            allow(User).to receive(:build).and_raise(BadRequestException)
            allow(ErrorService).to receive(:perform_async)
            users = [{"first_name" => "Mike", "last_name" => "Davis", "email" => "mike", "company" => "Keller Williams"}]
            PartnerService.new(@user, @partner).create_imported_users(users, "agent")

            expect(ErrorService).to have_received(:perform_async)
        end
    end

    describe "#verify_user" do
        it "returns true" do
            boolean = PartnerService.new(@user, @partner).verify_user({}, "")
            expect(boolean).to eq(true)
        end

        it "returns true" do
            boolean = PartnerService.new(@user, @partner).verify_user({"first_name" => "Jerry"}, "")
            expect(boolean).to eq(true)
        end

        it "returns true" do
            boolean = PartnerService.new(@user, @partner).verify_user({"first_name" => "Jerry", "last_name" => "Lewis"}, "")
            expect(boolean).to eq(true)
        end

        it "returns true" do
            boolean = PartnerService.new(@user, @partner).verify_user({"first_name" => "Jerry", "last_name" => "Lewis", "email" => "jerry@gmail.com"}, "")
            expect(boolean).to eq(true)
        end

        it "returns true" do
            boolean = PartnerService.new(@user, @partner).verify_user({"first_name" => ""}, "")
            expect(boolean).to eq(true)
        end

        it "returns true" do
            boolean = PartnerService.new(@user, @partner).verify_user({"first_name" => "Jerry", "last_name" => ""}, "")
            expect(boolean).to eq(true)
        end

        it "returns true" do
            boolean = PartnerService.new(@user, @partner).verify_user({"first_name" => "Jerry", "last_name" => "Lewis", "email" => ""}, "")
            expect(boolean).to eq(true)
        end

        it "returns true" do
            boolean = PartnerService.new(@user, @partner).verify_user({"first_name" => "Jerry", "last_name" => "Lewis", "email" => "est@gmail.com", "company" => ""}, "")
            expect(boolean).to eq(true)
        end

        it "returns false" do
            boolean = PartnerService.new(@user, @partner).verify_user({"first_name" => "Jerry", "last_name" => "Lewis", "email" => "test@gmail.com", "company" => "ERA"}, "")
            expect(boolean).to eq(false)
        end
    end

    describe "create_salesforce_account" do
        it "raises an exception" do
            data = {}
            expect{PartnerService.new(@user, @partner).create_salesforce_account(data)}.to raise_exception BadRequestException
        end

        it "raises an exception" do
            data = {:employees => {}}
            expect{PartnerService.new(@user, @partner).create_salesforce_account(data)}.to raise_exception BadRequestException
        end

        it "raises an exception" do
            data = {:employees => {:range => "5-10"}}
            expect{PartnerService.new(@user, @partner).create_salesforce_account(data)}.to raise_exception BadRequestException
        end

        it "raises an exception" do
            data = {:employees => {:range => "5-10"}, :inspections => {}}
            expect{PartnerService.new(@user, @partner).create_salesforce_account(data)}.to raise_exception BadRequestException
        end

        it "raises an exception" do
            data = {:employees => {:range => "5-10"}, :inspections => {:range => "500-1000"}}
            expect{PartnerService.new(@user, @partner).create_salesforce_account(data)}.to raise_exception BadRequestException
        end

        it "raises an exception" do
            data = {:employees => {:range => "5-10"}, :inspections => {:range => "500-1000"}, :software => {"ISN" => true, "Horizon" => true}}
            expect{PartnerService.new(@user, @partner).create_salesforce_account(data)}.to raise_exception BadRequestException
        end

        it "Creates the partner account in salesforce" do
            data = {:employees => {:range => "5-10"}, :inspections => {:range => "500-1000"}, :software => {"ISN" => true, "Horizon" => true}, :source => {"Trade Show"=> true}}
            client = double("Salesforce::Partner", :create => {:success => true})
            expect(Salesforce::Partner).to receive(:new).and_return(client)
            allow(client).to receive(:new_partner)

            PartnerService.new(@user, @partner).create_salesforce_account(data)

            expect(client).to have_received(:new_partner)
        end

        it "Creates the partner account in salesforce" do
            data = {:employees => {:range => "5-10"}, :inspections => {:range => "500-1000"}, :software => {"ISN" => true, "Horizon" => true, "Other" => true}, :source => {"Trade Show"=> true}}
            client = double("Salesforce::Partner", :create => {:success => true})
            expect(Salesforce::Partner).to receive(:new).and_return(client)
            allow(client).to receive(:new_partner)

            PartnerService.new(@user, @partner).create_salesforce_account(data)

            expect(client).to have_received(:new_partner)
        end
    end

    describe "add_user_image" do
        it "returns nil" do
            image = PartnerService.new(@user, @partner).add_user_image("")
            expect(image).to be(nil)
        end
        it "returns nil" do
            image = PartnerService.new(@user, @partner).add_user_image(nil)
            expect(image).to be(nil)
        end
        it "returns nil" do
            image = PartnerService.new(@user, @partner).add_user_image("string")
            expect(image).to be(nil)
        end
        it "returns not nil" do
            image = PartnerService.new(@user, @partner).add_user_image("https://dummyimage.com/300/09f/fff.png")
            expect(image).to_not be(nil)
        end
        it "returns nil" do
            allow(URI).to receive(:encode).and_raise BadRequestException
            image = PartnerService.new(@user, @partner).add_user_image("https://dummyimage.com/300/09f/fff.pngasdasdasds")
            expect(image).to be(nil)
        end
    end

    describe "update_salesforce" do
        it "updates partner in salesforce" do
            client = double("Salesforce::Partner", :update_partner => {:success => true})
            expect(Salesforce::Partner).to receive(:new).and_return(client)
            allow(client).to receive(:update_partner)

            PartnerService.new(@user, @partner).update_salesforce

            expect(client).to have_received(:update_partner)
        end
    end

    describe "update_user_role" do
        it "raises a BadRequestException because id is missing" do
            # create the request
            expect{PartnerService.new(@user, @partner).update_user_role({})}.to raise_exception(BadRequestException)
        end
        it "raises a BadRequestException because user is missing" do
            # create the request
            expect{PartnerService.new(@user, @partner).update_user_role({:id => 1})}.to raise_exception(BadRequestException)
        end
        it "raises a BadRequestException because user id is missing" do
            # create the request
            expect{PartnerService.new(@user, @partner).update_user_role({:id => 1, :user => {}})}.to raise_exception(BadRequestException)
        end
        it "raises a BadRequestException because user role missing" do
            # create the request
            expect{PartnerService.new(@user, @partner).update_user_role({:id => 1, :user => {:id => 1}})}.to raise_exception(BadRequestException)
        end
        it "raises a ActiveRecord::RecordNotFound because user doesn't exist" do
            # create the request
            expect{PartnerService.new(@user, @partner).update_user_role({:id => 1, :user => {:id => 999999, :role => "admin"}})}.to raise_exception(ActiveRecord::RecordNotFound)
        end
        it "raises a CanCan::AccessDenied because homeowner tries to edit user/partner" do
            # create the request
            homeowner = create(:user, :role => "homeowner")
            expect{PartnerService.new(homeowner, @partner).update_user_role({:id => @partner.id, :user => {:id => @user.id, :role => "member"}})}.to raise_exception(CanCan::AccessDenied)
        end

        it "raises a BadRequestExceptionbecause partner user doesn't exist" do
            # create the request
            inspector = create(:user, :role => "inspector")
            expect{PartnerService.new(@admin, @partner).update_user_role({:id => @partner.id, :user => {:id => inspector.id, :role => "member"}})}.to raise_exception(BadRequestException)
        end

        it "updates the partner user" do
            # find the partner user
            partner_user = PartnerUser.where(:partner_id => @partner.id, :user_id => @user.id).first

            expect(partner_user).to_not be(nil)
            expect(partner_user.role).to eq("admin")

            PartnerService.new(@user, @partner).update_user_role({:id => @partner.id, :user => {:id => @user.id, :role => "member"}})

            # reload the partner user and make sure the role changed
            partner_user.reload
            expect(partner_user).to_not be(nil)
            expect(partner_user.role).to eq("member")
        end
    end
end
