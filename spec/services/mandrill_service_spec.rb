require 'rails_helper'

RSpec.describe MandrillService, :type => :model do
    before :each do
        create(:user, :email => "orphans@homebinder.com")
    end

    describe "#parse_webhook" do
        context "when a partner" do
            before :each do
                # set up the partner and configuration
                @partner = create(:partner, name: "Test Inspections", contact: "Test Inspector", partner_key: "test")
                @user = create(:user, email: @partner.email, role: "inspector")
                @user.add_role :partner_admin, @partner
                @partner.partner_configuration.automation_binder_action = true
                @partner.partner_configuration.default_user_branding_id =  @user.user_profile.id
                @partner.partner_configuration.default_binder_action = "transfer"
                @partner.save
                @config = @partner.partner_configuration
                @template = create(:binder_template, partner_configuration_id: @config.id, transfer_note: "This is a transfer note")
                @config.default_binder_template_id = @template.id
                @config.save
                create(:appliance_template, binder_template_id: @template.id)
                create(:contractor_template, binder_template_id: @template.id)
                create(:document_template, binder_template_id: @template.id)
                create(:maintenance_template, binder_template_id: @template.id, due_date: nil, frequency: "As Needed", initial_due_date_interval: 200)
                # create the binder
                client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
                property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
                agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}

                parameters = {
                    :key => "test",
                    :client => client,
                    :property => property,
                    :binder_template_id => @template.id,
                    :agent => agent,
                    :property_photo => nil,
                    :documents => nil
                }

                allow(JSON).to receive(:parse).and_call_original
                allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

                binder = Partner::Automation::Service.create_binder(parameters)
                @binder = Binder.find(binder[:id])
                date = Date.today.sunday - 2.day
                allow(Date).to receive(:today).and_return(date)
            end
            it "transfers binder then cancels transfer" do
                # change the creation time so we can transfer the binder
                @binder.created_at = 12.hours.ago
                @binder.save
                # run ABS automatic transfers
                Partner::Automation::Service::Access.new.transfer_binders
                @binder.reload
                # make sure owner is new user
                user = User.with_role(:owner, @binder).first
                expect(user.email).to eq("transfers@homebinder.com")
                # make sure transfer exists
                transfer = @binder.transfers.first
                expect(@binder.transfers.count).to eq(1)
                expect(transfer.status).to eq("pending")
                # stub a fake bounced email
                parameters = {"mandrill_events" => "[{\"event\":\"hard_bounce\",\"msg\":{\"ts\":1365109999,\"subject\":\"Last Step\",\"email\":\"transfers@homebinder.com\",\"sender\":\"#{@partner.email}\",\"tags\":[\"webhook-example\"],\"state\":\"bounced\",\"metadata\":{\"transfer_id\":#{transfer.id},\"email_type\":\"transfer\"},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"_version\":\"exampleaaaaaaaaaaaaaaa\",\"bounce_description\":\"bad_mailbox\",\"bgtools_code\":10,\"diag\":\"smtp;550 5.1.1 The email account that you tried to reach does not exist. Please try double-checking the recipient's email address for typos or unnecessary spaces.\"},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"ts\":1477310208}]"}
                MandrillService.new.parse_webhook(parameters)
                # make sure owner is old user
                user = User.with_role(:owner, @binder).first
                expect(user.email).to eq("orphans@homebinder.com")
                # make sure the user no longer exists
                user = User.find_by_email("transfers@homebinder.com")
                expect(user).to be(nil)
                # check transfer status
                transfer.reload
                expect(transfer.status).to eq("delivery_failed")
            end
            it "transfers binder then cancels transfer after an unsub" do
                # change the creation time so we can transfer the binder
                @binder.created_at = 12.hours.ago
                @binder.save
                # run ABS automatic transfers
                Partner::Automation::Service::Access.new.transfer_binders
                @binder.reload
                # make sure owner is new user
                user = User.with_role(:owner, @binder).first
                expect(user.email).to eq("transfers@homebinder.com")
                # make sure transfer exists
                transfer = @binder.transfers.first
                expect(@binder.transfers.count).to eq(1)
                expect(transfer.status).to eq("pending")
                # stub a fake bounced email
                parameters = {"mandrill_events" => "[{\"event\":\"unsub\",\"msg\":{\"ts\":1365109999,\"subject\":\"Last Step\",\"email\":\"transfers@homebinder.com\",\"sender\":\"#{@partner.email}\",\"tags\":[\"webhook-example\"],\"state\":\"bounced\",\"metadata\":{\"transfer_id\":#{transfer.id},\"email_type\":\"transfer\"},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"_version\":\"exampleaaaaaaaaaaaaaaa\",\"bounce_description\":\"bad_mailbox\",\"bgtools_code\":10,\"diag\":\"smtp;550 5.1.1 The email account that you tried to reach does not exist. Please try double-checking the recipient's email address for typos or unnecessary spaces.\"},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"ts\":1477310208}]"}
                MandrillService.new.parse_webhook(parameters)
                # make sure owner is old user
                user = User.with_role(:owner, @binder).first
                expect(user.email).to eq("orphans@homebinder.com")

                # make sure the user no longer exists
                user = User.find_by_email("transfers@homebinder.com")
                expect(user).to be(nil)
                # check transfer status
                transfer.reload
                expect(transfer.status).to eq("delivery_failed")
            end
            it "shares binder then removes the co-owner" do
                # change the partner default action
                @partner.partner_configuration.default_binder_action = "share"
                @partner.save
                # change the creation time so we can share the binder
                @binder.created_at = 12.hours.ago
                @binder.save
                # run ABS automatic transfers
                Partner::Automation::Service::Access.new.transfer_binders
                @binder.reload

                # make sure co-owner is new user
                co_owner = User.with_role(:co_owner, @binder).first
                expect(co_owner.email).to eq("transfers@homebinder.com")
                # make sure owner is old user
                owner = User.with_role(:owner, @binder).first
                expect(owner.email).to eq(@partner.email)
                # make sure share exists
                share = @binder.shares.first
                expect(@binder.shares.count).to eq(1)
                expect(share.status).to eq("pending")
                # stub a fake bounced email
                parameters = {"mandrill_events" => "[{\"event\":\"hard_bounce\",\"msg\":{\"ts\":1365109999,\"subject\":\"Last Step\",\"email\":\"transfers@homebinder.com\",\"sender\":\"#{@partner.email}\",\"tags\":[\"webhook-example\"],\"state\":\"bounced\",\"metadata\":{\"share_id\":#{share.id},\"email_type\":\"share\"},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"_version\":\"exampleaaaaaaaaaaaaaaa\",\"bounce_description\":\"bad_mailbox\",\"bgtools_code\":10,\"diag\":\"smtp;550 5.1.1 The email account that you tried to reach does not exist. Please try double-checking the recipient's email address for typos or unnecessary spaces.\"},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"ts\":1477310208}]"}
                MandrillService.new.parse_webhook(parameters)
                # make sure owner is still the old user
                owner = User.with_role(:owner, @binder).first
                expect(owner.email).to eq(@user.email)
                # make sure co_owner doesn't exist
                co_owner = User.with_role(:co_owner, @binder).first
                expect(co_owner).to eq(nil)
                # make sure the user no longer exists
                user = User.find_by_email("transfers@homebinder.com")
                expect(user).to be(nil)
                # check share status
                share.reload
                expect(share.status).to eq("delivery_failed")
            end
            it "shares binder then removes the co-owner after an unsub" do
                # change the partner default action
                @partner.partner_configuration.default_binder_action = "share"
                @partner.save
                # change the creation time so we can share the binder
                @binder.created_at = 12.hours.ago
                @binder.save
                # run ABS automatic transfers
                Partner::Automation::Service::Access.new.transfer_binders
                @binder.reload
                # make sure co-owner is new user
                co_owner = User.with_role(:co_owner, @binder).first
                expect(co_owner.email).to eq("transfers@homebinder.com")
                # make sure owner is old user
                owner = User.with_role(:owner, @binder).first
                expect(owner.email).to eq(@partner.email)
                # make sure share exists
                share = @binder.shares.first
                expect(@binder.shares.count).to eq(1)
                expect(share.status).to eq("pending")
                # stub a fake bounced email
                parameters = {"mandrill_events" => "[{\"event\":\"unsub\",\"msg\":{\"ts\":1365109999,\"subject\":\"Last Step\",\"email\":\"transfers@homebinder.com\",\"sender\":\"#{@partner.email}\",\"tags\":[\"webhook-example\"],\"state\":\"bounced\",\"metadata\":{\"share_id\":#{share.id},\"email_type\":\"share\"},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"_version\":\"exampleaaaaaaaaaaaaaaa\",\"bounce_description\":\"bad_mailbox\",\"bgtools_code\":10,\"diag\":\"smtp;550 5.1.1 The email account that you tried to reach does not exist. Please try double-checking the recipient's email address for typos or unnecessary spaces.\"},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"ts\":1477310208}]"}
                MandrillService.new.parse_webhook(parameters)
                # make sure owner is still the old user
                owner = User.with_role(:owner, @binder).first
                expect(owner.email).to eq(@user.email)
                # make sure co_owner doesn't exist
                co_owner = User.with_role(:co_owner, @binder).first
                expect(co_owner).to eq(nil)
                # make sure the user no longer exists
                user = User.find_by_email("transfers@homebinder.com")
                expect(user).to be(nil)
                # check share status
                share.reload
                expect(share.status).to eq("delivery_failed")
            end
            it "transfers binder then updates the transfer status" do
                # change the creation time so we can transfer the binder
                @binder.created_at = 12.hours.ago
                @binder.save
                # run ABS automatic transfers
                Partner::Automation::Service::Access.new.transfer_binders
                @binder.reload
                # make sure owner is new user
                user = User.with_role(:owner, @binder).first
                expect(user.email).to eq("transfers@homebinder.com")
                # make sure transfer exists
                transfer = @binder.transfers.first
                expect(@binder.transfers.count).to eq(1)
                expect(transfer.status).to eq("pending")
                # stub a fake bounced email
                parameters = {"mandrill_events" => "[{\"event\":\"send\",\"msg\":{\"ts\":1365109999,\"subject\":\"Last Step\",\"email\":\"transfers@homebinder.com\",\"sender\":\"#{@partner.email}\",\"tags\":[\"webhook-example\"],\"state\":\"bounced\",\"metadata\":{\"transfer_id\":#{transfer.id},\"email_type\":\"transfer\"},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"_version\":\"exampleaaaaaaaaaaaaaaa\",\"bounce_description\":\"bad_mailbox\",\"bgtools_code\":10,\"diag\":\"smtp;550 5.1.1 The email account that you tried to reach does not exist. Please try double-checking the recipient's email address for typos or unnecessary spaces.\"},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"ts\":1477310208}]"}
                MandrillService.new.parse_webhook(parameters)
                transfer.reload
                expect(transfer.status).to eq("sent")
            end
            it "shares binder then updates the share status" do
                share = create(:share, :status => "created")
                # stub a fake bounced email
                parameters = {"mandrill_events" => "[{\"event\":\"send\",\"msg\":{\"ts\":1365109999,\"subject\":\"Last Step\",\"email\":\"transfers@homebinder.com\",\"sender\":\"#{@partner.email}\",\"tags\":[\"webhook-example\"],\"state\":\"bounced\",\"metadata\":{\"share_id\":#{share.id},\"email_type\":\"share\"},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"_version\":\"exampleaaaaaaaaaaaaaaa\",\"bounce_description\":\"bad_mailbox\",\"bgtools_code\":10,\"diag\":\"smtp;550 5.1.1 The email account that you tried to reach does not exist. Please try double-checking the recipient's email address for typos or unnecessary spaces.\"},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"ts\":1477310208}]"}
                MandrillService.new.parse_webhook(parameters)
                share.reload
                expect(share.status).to eq("shared")
            end
        end
        context "when a regular user" do
            before :each do
                @user = create(:user)
                @binder = create(:binder, created_by: @user.id)
                create(:subscription, binder_id: @binder.id, plan_id: "free")
                @user.add_role :owner, @binder
                @user.save
                allow(Users::UserAnalytics).to receive(:unsubscribe)
            end
            it "unsubscribes the user in intercom after an unsubscribe" do
                # stub a fake bounced email
                parameters = {"mandrill_events" => "[{\"event\":\"unsub\",\"msg\":{\"ts\":1365109999,\"subject\":\"Homebinder Monthly Summary\",\"email\":\"#{@user.email}\",\"sender\":\"#{@user.email}\",\"tags\":[\"webhook-example\"],\"state\":\"bounced\",\"metadata\":{\"user_id\":111},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"_version\":\"exampleaaaaaaaaaaaaaaa\",\"bounce_description\":\"bad_mailbox\",\"bgtools_code\":10,\"diag\":\"smtp;550 5.1.1 The email account that you tried to reach does not exist. Please try double-checking the recipient's email address for typos or unnecessary spaces.\"},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"ts\":1477310208}]"}
                expect(Users::UserAnalytics).to receive(:unsubscribe)
                MandrillService.new.parse_webhook(parameters)
            end
            it "unsubscribes the user in intercom after a spam complaint" do
                # stub a fake bounced email
                parameters = {"mandrill_events" => "[{\"event\":\"spam\",\"msg\":{\"ts\":1365109999,\"subject\":\"Homebinder Monthly Summary\",\"email\":\"#{@user.email}\",\"sender\":\"#{@user.email}\",\"tags\":[\"webhook-example\"],\"state\":\"bounced\",\"metadata\":{\"user_id\":111},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"_version\":\"exampleaaaaaaaaaaaaaaa\",\"bounce_description\":\"bad_mailbox\",\"bgtools_code\":10,\"diag\":\"smtp;550 5.1.1 The email account that you tried to reach does not exist. Please try double-checking the recipient's email address for typos or unnecessary spaces.\"},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"ts\":1477310208}]"}
                expect(Users::UserAnalytics).to receive(:unsubscribe)
                MandrillService.new.parse_webhook(parameters)
            end
            it "unsubscribes the user in intercom after a spam complaint and but does not delete the user" do
                # stub a fake bounced email
                parameters = {"mandrill_events" => "[{\"event\":\"spam\",\"msg\":{\"ts\":1365109999,\"subject\":\"Last Step\",\"email\":\"#{@user.email}\",\"sender\":\"#{@user.email}\",\"tags\":[\"webhook-example\"],\"state\":\"bounced\",\"metadata\":{\"user_id\":111},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"_version\":\"exampleaaaaaaaaaaaaaaa\",\"bounce_description\":\"bad_mailbox\",\"bgtools_code\":10,\"diag\":\"smtp;550 5.1.1 The email account that you tried to reach does not exist. Please try double-checking the recipient's email address for typos or unnecessary spaces.\"},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"ts\":1477310208}]"}
                expect(Users::UserAnalytics).to receive(:unsubscribe)
                MandrillService.new.parse_webhook(parameters)
                user = User.find_by_id(@user.id)
                expect(user).to_not be(nil)
            end
        end

        context "when event type is click/open" do
            it "calls handle_webhook_click" do
                service = MandrillService.new

                allow(service).to receive(:handle_webhook_click)

                parameters = {"mandrill_events" => "[{\"event\":\"click\"}]"}
                service.parse_webhook(parameters)

                expect(service).to have_received(:handle_webhook_click)
            end

            it "calls handle_webhook_open" do
                service = MandrillService.new

                allow(service).to receive(:handle_webhook_open)

                parameters = {"mandrill_events" => "[{\"event\":\"open\"}]"}
                service.parse_webhook(parameters)

                expect(service).to have_received(:handle_webhook_open)
            end
        end
    end

    describe "#handle_webhook_bounce" do
        it "parses webhook for maintenance reminder" do
            event = create(:maintenance_event)
            MandrillService.new.handle_webhook_bounce({"metadata" => {"email_type" => "maintenance_reminder", "maintenance_event_id" => event.id}})

            event.reload
            expect(event.email_status).to eq("delivery_failed")
        end
    end

    describe "#handle_webhook_send" do
        it "parses webhook for maintenance reminder" do
            event = create(:maintenance_event)
            MandrillService.new.handle_webhook_send({"metadata" => {"email_type" => "maintenance_reminder", "maintenance_event_id" => event.id}})

            event.reload
            expect(event.email_status).to eq("sent")
        end
    end

    describe "#handle_webhook_click" do
        it "parses webhook for maintenance reminder" do
            event = create(:maintenance_event)
            MandrillService.new.handle_webhook_click({"metadata" => {"email_type" => "maintenance_reminder", "maintenance_event_id" => event.id}})

            event.reload
            expect(event.clicks).to eq(1)
        end
    end

    describe "#handle_webhook_open" do
        it "parses webhook for maintenance reminder" do
            event = create(:maintenance_event)
            MandrillService.new.handle_webhook_open({"metadata" => {"email_type" => "maintenance_reminder", "maintenance_event_id" => event.id}})

            event.reload
            expect(event.opens).to eq(1)
        end
    end
end
