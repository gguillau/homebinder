require 'rails_helper'

RSpec.describe OneSourceService do
  
    before :each do
        @binder = create(:binder)
    end
    
    it "returns the secret key for production" do
        allow(Host).to receive(:path).and_return(Host::PRODUCTION_HOST)
        # get the secret_key
        secret_key = OneSourceService.new.secret_key

        expect(secret_key).to eq(ENV["ONE_SOURCE_PRODUCTION_SECRET"].to_s)
    end
    
    it "returns the secret key for test" do
        allow(Host).to receive(:path).and_return("Test")
        # get the secret_key
        secret_key = OneSourceService.new.secret_key

        expect(secret_key).to eq(ENV["ONE_SOURCE_TEST_SECRET"].to_s)
    end
    
    it "returns the url for production" do
        allow(Host).to receive(:path).and_return(Host::PRODUCTION_HOST)
        # get the url
        url = OneSourceService.new.url

        expect(url).to eq(ENV["ONE_SOURCE_PRODUCTION_URL"].to_s)
    end
    
    it "returns the url for test" do
        allow(Host).to receive(:path).and_return("Test")
        # get the url
        url = OneSourceService.new.url

        expect(url).to eq(ENV["ONE_SOURCE_TEST_URL"].to_s)
    end
    
    it "raises an error when response is 400" do
        stub_request(:any, /yourhouseconnections/).to_return(:status => 400, :body => '{"ResultCode": 200}')
        binder = create(:binder)
        owner = create(:user)
        create(:user_binder, :role => "owner", :binder => binder, :user => owner)
        expect{OneSourceService.new.request_services(binder)}.to raise_error(BadRequestException)
    end
    
    it "raises an error when response is 200 but ResultCode is 400" do
        stub_request(:any, /yourhouseconnections/).to_return(:status => 200, :body => '{"ResultCode": 400}')
        binder = create(:binder)
        owner = create(:user)
        create(:user_binder, :role => "owner", :binder => binder, :user => owner)
        expect{OneSourceService.new.request_services(binder)}.to raise_error(BadRequestException)
    end
    
    it "does not raise an error" do
        stub_request(:any, /yourhouseconnections/).to_return(:status => 200, :body => '{"ResultCode": 200}')
        binder = create(:binder)
        owner = create(:user)
        create(:user_binder, :role => "owner", :binder => binder, :user => owner)
        expect{OneSourceService.new.request_services(binder)}.to_not raise_error
    end

end