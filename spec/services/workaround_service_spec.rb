require 'rails_helper'

RSpec.describe WorkaroundService do

    describe "#post_call" do
        it "returns 201 response" do
            response = WorkaroundService.new.post_call(1, "link")
            
            # expect the response and the body to be equal
            expect(response).to eq({})
        end
        it "returns 403 response" do
            url = "https://api.sandbox.repairpriceronline.com/homebinder/ex_create/"
            stub_request(:post, URI.escape(url)).to_return(:status => 403, :body => "")

            # expect method to raise an error
            expect{WorkaroundService.new.post_call(1, "link")}.to raise_error BadRequestException
        end
    end
    
    describe "#get_call" do
        it "returns 201 response" do
            response = WorkaroundService.new.get_call(1)
            
            # expect the response and the body to be equal
            expect(response).to eq({})
        end
        it "returns 403 response" do
            url = "https://api.sandbox.repairpriceronline.com/homebinder/ex_status/"
            stub_request(:post, URI.escape(url)).to_return(:status => 403, :body => "")

            # expect method to raise an error
            expect{WorkaroundService.new.get_call(1)}.to raise_error BadRequestException
        end
    end
    
    describe "#api_key" do
        it "returns the api key for production" do
            allow(Host).to receive(:path).and_return(Host::PRODUCTION_HOST)
            # get the api_key
            api_key = WorkaroundService.new.api_key
    
            expect(api_key).to eq(ENV["WORKAROUND_PRODUCTION_KEY"].to_s)
        end
    end
end