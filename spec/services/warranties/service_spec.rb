require 'rails_helper'
require 'mandrill'

RSpec.describe Warranties::Service do
    
    before :each do
        allow(WarrantyMailer).to receive(:delay).and_return(WarrantyMailer)
        allow(WarrantyMailer).to receive(:send_warranty_report)
    end
    
    describe "#create_warranty_transaction" do
        it "creates the warranty transaction" do
            binder = create(:binder)

            warranty_company = create(:warranty_company)  
            warranty_plan = create(:warranty_plan, :warranty_company_id => warranty_company.id)  

            # make sure warranty count is 0
            expect(binder.warranties.length).to eq(0)
            expect(binder.transactions.count).to eq(0)
                warranty = create(:warranty, :warranty_plan_id => warranty_plan.id, :binder_id => binder.id)
                Warranties::Service.new(warranty.id).create_warranty_transaction(24)
                # make sure warranty/transaction was created
            expect(warranty.binder_transaction).to_not be(nil)
            expect(binder.warranties.count).to eq(1)
            expect(binder.transactions.count).to eq(1)
        end
        it "creates the warranty transaction" do
            binder = create(:binder)

            warranty_company = create(:warranty_company)  
            warranty_plan = create(:warranty_plan, :warranty_company_id => warranty_company.id)  

            # make sure warranty count is 0
            expect(binder.warranties.length).to eq(0)
            expect(binder.transactions.count).to eq(0)
                warranty = create(:warranty, :warranty_plan_id => warranty_plan.id, :binder_id => binder.id)
            allow_any_instance_of(Binder::Transaction).to receive(:save).and_return(false)
                expect{Warranties::Service.new(warranty.id).create_warranty_transaction(24)}.to raise_error UnprocessableException
        end
    end
    
    describe "#create_inspection_transaction" do
        it "creates the inspection transaction" do
            binder = create(:binder)

            warranty_company = create(:warranty_company)  
            warranty_plan = create(:warranty_plan, :warranty_company_id => warranty_company.id)  

            # make sure warranty count is 0
            expect(binder.warranties.length).to eq(0)
            expect(binder.transactions.count).to eq(0)
                warranty = create(:warranty, :warranty_plan_id => warranty_plan.id, :binder_id => binder.id)
                Warranties::Service.new(warranty.id).create_inspection_transaction(Date.today)

            # make sure warranty/transaction was created
            expect(binder.warranties.count).to eq(1)
            expect(binder.transactions.count).to eq(1)
        end
        it "creates the inspection transaction" do
            binder = create(:binder)

            warranty_company = create(:warranty_company)  
            warranty_plan = create(:warranty_plan, :warranty_company_id => warranty_company.id)  

            # make sure warranty count is 0
            expect(binder.warranties.length).to eq(0)
            expect(binder.transactions.count).to eq(0)
                warranty = create(:warranty, :warranty_plan_id => warranty_plan.id, :binder_id => binder.id)
            allow_any_instance_of(Binder::Transaction).to receive(:save).and_return(false)
                expect{Warranties::Service.new(warranty.id).create_inspection_transaction(Date.today)}.to raise_error UnprocessableException
        end
        it "updates the inspection transaction" do
            binder = create(:binder)

            warranty_company = create(:warranty_company)  
            warranty_plan = create(:warranty_plan, :warranty_company_id => warranty_company.id)  
                create(:binder_transaction, :transaction_type => "buy_side_inspection", :binder => binder)
                # make sure warranty count is 0
            expect(binder.warranties.length).to eq(0)
            expect(binder.transactions.count).to eq(1)
                warranty = create(:warranty, :warranty_plan_id => warranty_plan.id, :binder_id => binder.id)
            Warranties::Service.new(warranty.id).create_inspection_transaction(Date.today)

            # make sure warranty/transaction was created
            expect(binder.warranties.count).to eq(1)
            expect(binder.transactions.count).to eq(1)
        end
        it "raises an error" do
            binder = create(:binder)

            warranty_company = create(:warranty_company)  
            warranty_plan = create(:warranty_plan, :warranty_company_id => warranty_company.id)  
                create(:binder_transaction, :transaction_type => "buy_side_inspection", :binder => binder)
                # make sure warranty count is 0
            expect(binder.warranties.length).to eq(0)
            expect(binder.transactions.count).to eq(1)
                warranty = create(:warranty, :warranty_plan_id => warranty_plan.id, :binder_id => binder.id)
            allow_any_instance_of(Binder::Transaction).to receive(:save).and_return(false)
                expect{Warranties::Service.new(warranty.id).create_inspection_transaction(Date.today)}.to raise_error UnprocessableException
        end
    end
end