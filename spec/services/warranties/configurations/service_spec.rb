require 'rails_helper'
require 'mandrill'

RSpec.describe Warranties::Configurations::Service, :type => :service do
    describe "#send_order" do
        it "requires an order" do
            binder = create(:binder)
            user = create(:user)

            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")

            warranty_company = create(:warranty_company)
            warranty_plan = create(:warranty_plan)
            warranty_configuration = create(:warranty_configuration)
            homeowner = create(:user)

            homeowner = {
                id: homeowner.id,
                firstName: homeowner.user_profile.first_name,
                lastName: homeowner.user_profile.last_name,
                email: homeowner.email,
                phone: homeowner.user_profile.mobile_phone
            }

            params = {
                :id => warranty_configuration.id
            }

            expect{Warranties::Configurations::Service.new(warranty_configuration.id).order(user, params)}.to raise_exception BadRequestException, "Order Required"
        end

        it "requires a homeowner" do
            binder = create(:binder)
            user = create(:user)

            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")

            warranty_company = create(:warranty_company)
            warranty_plan = create(:warranty_plan)
            warranty_configuration = create(:warranty_configuration)
            homeowner = create(:user)

            homeowner = {
                id: homeowner.id,
                firstName: homeowner.user_profile.first_name,
                lastName: homeowner.user_profile.last_name,
                email: homeowner.email,
                phone: homeowner.user_profile.mobile_phone
            }

            params = {
                :id => warranty_configuration.id,
                :order => {
                    binder_id: binder.id,
                }
            }

            expect{Warranties::Configurations::Service.new(warranty_configuration.id).order(user, params)}.to raise_exception BadRequestException, "Homeowner Required"
        end

        it "requires a property" do
            binder = create(:binder)
            user = create(:user)

            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")

            warranty_company = create(:warranty_company)
            warranty_plan = create(:warranty_plan)
            warranty_configuration = create(:warranty_configuration)
            homeowner = create(:user)

            homeowner = {
                id: homeowner.id,
                firstName: homeowner.user_profile.first_name,
                lastName: homeowner.user_profile.last_name,
                email: homeowner.email,
                phone: homeowner.user_profile.mobile_phone
            }

            params = {
                :id => warranty_configuration.id,
                :order => {
                    homeowner: homeowner,
                }
            }

            expect{Warranties::Configurations::Service.new(warranty_configuration.id).order(user, params)}.to raise_exception BadRequestException, "Property Required"
        end

        it "requires an Inspection date" do
            binder = create(:binder)
            user = create(:user)

            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")

            warranty_company = create(:warranty_company)
            warranty_plan = create(:warranty_plan)
            warranty_configuration = create(:warranty_configuration)
            homeowner = create(:user)

            homeowner = {
                id: homeowner.id,
                firstName: homeowner.user_profile.first_name,
                lastName: homeowner.user_profile.last_name,
                email: homeowner.email,
                phone: homeowner.user_profile.mobile_phone
            }

            property = {
                address1: "123 Main Street"
            }

            params = {
                :id => warranty_configuration.id,
                :order => {
                    homeowner: homeowner,
                    property: property
                }
            }

            expect{Warranties::Configurations::Service.new(warranty_configuration.id).order(user, params)}.to raise_exception BadRequestException, "Inspection Date Required"
        end

        it "requires a Binder ID" do
            binder = create(:binder)
            user = create(:user)

            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")

            warranty_company = create(:warranty_company)
            warranty_plan = create(:warranty_plan)
            warranty_configuration = create(:warranty_configuration)
            homeowner = create(:user)

            homeowner = {
                id: homeowner.id,
                firstName: homeowner.user_profile.first_name,
                lastName: homeowner.user_profile.last_name,
                email: homeowner.email,
                phone: homeowner.user_profile.mobile_phone
            }

            property = {
                address1: "123 Main Street"
            }

            params = {
                :id => warranty_configuration.id,
                :order => {
                    homeowner: homeowner,
                    property: property,
                    inspection_date: 5.days.ago
                }
            }

            expect{Warranties::Configurations::Service.new(warranty_configuration.id).order(user, params)}.to raise_exception BadRequestException, "Binder ID Required"
        end

        it "requires a warranty plan" do
            binder = create(:binder)
            user = create(:user)

            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")

            warranty_company = create(:warranty_company)
            warranty_plan = create(:warranty_plan)
            warranty_configuration = create(:warranty_configuration)
            homeowner = create(:user)

            homeowner = {
                id: homeowner.id,
                firstName: homeowner.user_profile.first_name,
                lastName: homeowner.user_profile.last_name,
                email: homeowner.email,
                phone: homeowner.user_profile.mobile_phone
            }

            property = {
                address1: "123 Main Street"
            }

            params = {
                :id => warranty_configuration.id,
                :order => {
                    homeowner: homeowner,
                    property: property,
                    inspection_date: 5.days.ago,
                    binder_id: binder.id
                }
            }

            expect{Warranties::Configurations::Service.new(warranty_configuration.id).order(user, params)}.to raise_exception BadRequestException, "Warranty Plan Required"
        end

        it "requires a warranty price" do
            binder = create(:binder)
            user = create(:user)

            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")

            warranty_company = create(:warranty_company)
            warranty_plan = create(:warranty_plan)
            warranty_configuration = create(:warranty_configuration)
            homeowner = create(:user)

            homeowner = {
                id: homeowner.id,
                firstName: homeowner.user_profile.first_name,
                lastName: homeowner.user_profile.last_name,
                email: homeowner.email,
                phone: homeowner.user_profile.mobile_phone
            }

            property = {
                address1: "123 Main Street"
            }

            params = {
                :id => warranty_configuration.id,
                :order => {
                    homeowner: homeowner,
                    property: property,
                    inspection_date: 5.days.ago,
                    binder_id: binder.id,
                    warranty_plan_id: warranty_plan.id
                }
            }

            expect{Warranties::Configurations::Service.new(warranty_configuration.id).order(user, params)}.to raise_exception BadRequestException, "Warranty Price Required"
        end

        it "sends an order" do
            binder = create(:binder)
            user = create(:user)

            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")

            warranty_company = create(:warranty_company)
            warranty_plan = create(:warranty_plan)
            warranty_configuration = create(:warranty_configuration)
            homeowner = create(:user)

            homeowner = {
                id: homeowner.id,
                firstName: homeowner.user_profile.first_name,
                lastName: homeowner.user_profile.last_name,
                email: homeowner.email,
                phone: homeowner.user_profile.mobile_phone
            }

            property = {
                address1: "123 Main Street",
                address2: "",
                city: "Boston",
                state: "MA",
                country: "US",
                zip: "02210"
            }

            params = {
                :id => warranty_configuration.id,
                :order => {
                    binder_id: binder.id,
                    property: property,
                    warranty_company_id: warranty_company.id,
                    warranty_plan_id: warranty_plan.id,
                    account_number: warranty_configuration.account_number,
                    homeowner: homeowner,
                    inspection_date: 5.days.ago,
                    price: 25
                }
            }

            # make sure warranty count is 0
            expect(binder.warranties.length).to eq(0)

            Warranties::Configurations::Service.new(warranty_configuration.id).order(user, params)

            # make sure warranty was created
            expect(binder.warranties.count).to eq(1)
        end
    end

    describe "#update_homeowner" do
        it "requires id" do
            warranty_configuration = create(:warranty_configuration)

            update = {}

            expect{Warranties::Configurations::Service.new(warranty_configuration.id).update_homeowner(update)}.to raise_exception BadRequestException, "Homeowner User ID Required"
        end

        it "requires firstname" do
            warranty_configuration = create(:warranty_configuration)
            homeowner = create(:user)

            update = {
                id: homeowner.id
            }

            expect{Warranties::Configurations::Service.new(warranty_configuration.id).update_homeowner(update)}.to raise_exception BadRequestException, "Homeowner First Name Required"
        end

        it "requires last name" do
            warranty_configuration = create(:warranty_configuration)
            homeowner = create(:user)

            update = {
                id: homeowner.id,
                firstName: homeowner.user_profile.first_name
            }

            expect{Warranties::Configurations::Service.new(warranty_configuration.id).update_homeowner(update)}.to raise_exception BadRequestException, "Homeowner Last Name Required"
        end

        it "requires email" do
            warranty_configuration = create(:warranty_configuration)
            homeowner = create(:user)

            update = {
                id: homeowner.id,
                firstName: homeowner.user_profile.first_name,
                lastName: homeowner.user_profile.last_name
            }

            expect{Warranties::Configurations::Service.new(warranty_configuration.id).update_homeowner(update)}.to raise_exception BadRequestException, "Homeowner Email Required"
        end

        it "raises an error" do
            allow_any_instance_of(User).to receive(:update_attributes).and_return(false)
            warranty_configuration = create(:warranty_configuration)
            homeowner = create(:user)

            update = {
                id: homeowner.id,
                firstName: homeowner.user_profile.first_name,
                lastName: homeowner.user_profile.last_name,
                email: "test@gmail.com",
                phone: homeowner.user_profile.mobile_phone
            }

            expect{Warranties::Configurations::Service.new(warranty_configuration.id).update_homeowner(update)}.to raise_error UnprocessableException
        end

        it "updates the homeowner" do
            warranty_configuration = create(:warranty_configuration)
            homeowner = create(:user)

            update = {
                id: homeowner.id,
                firstName: homeowner.user_profile.first_name,
                lastName: homeowner.user_profile.last_name,
                email: "test@gmail.com",
                phone: homeowner.user_profile.mobile_phone
            }

            Warranties::Configurations::Service.new(warranty_configuration.id).update_homeowner(update)
            homeowner.reload

            # make sure warranty was created
            expect(homeowner.email).to eq("test@gmail.com")
        end
    end

    describe "#create_warranty" do
        it "requires property_address1" do
            user = create(:user)
            warranty_configuration = create(:warranty_configuration)

            property = {}

            params = {
                :id => warranty_configuration.id,
                :order => {
                    property: property
                }
            }

            expect{Warranties::Configurations::Service.new(warranty_configuration.id).create_warranty(user, params)}.to raise_exception BadRequestException, "Property Address1 Required"
        end

        it "requires property_city" do
            user = create(:user)
            warranty_configuration = create(:warranty_configuration)

            property = {
                address1: "123 Main Street"
            }

            params = {
                :id => warranty_configuration.id,
                :order => {
                    property: property
                }
            }

            expect{Warranties::Configurations::Service.new(warranty_configuration.id).create_warranty(user, params)}.to raise_exception BadRequestException, "Property City Required"
        end

        it "requires property_state" do
            user = create(:user)
            warranty_configuration = create(:warranty_configuration)

            property = {
                address1: "123 Main Street",
                city: "Boston"
            }

            params = {
                :id => warranty_configuration.id,
                :order => {
                    property: property
                }
            }

            expect{Warranties::Configurations::Service.new(warranty_configuration.id).create_warranty(user, params)}.to raise_exception BadRequestException, "Property State Required"
        end

        it "requires property_country" do
            user = create(:user)
            warranty_configuration = create(:warranty_configuration)

            property = {
                address1: "123 Main Street",
                city: "Boston",
                state: "MA"
            }

            params = {
                :id => warranty_configuration.id,
                :order => {
                    property: property
                }
            }

            expect{Warranties::Configurations::Service.new(warranty_configuration.id).create_warranty(user, params)}.to raise_exception BadRequestException, "Property Country Required"
        end

        it "creates the warranty when cycle is days" do
            binder = create(:binder)
            user = create(:user)

            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")

            warranty_company = create(:warranty_company)
            warranty_plan = create(:warranty_plan, :warranty_company => warranty_company, :cycle => "days")
            warranty_configuration = create(:warranty_configuration)
            homeowner = create(:user)

            homeowner = {
                id: homeowner.id,
                firstName: homeowner.user_profile.first_name,
                lastName: homeowner.user_profile.last_name,
                email: homeowner.email,
                phone: homeowner.user_profile.mobile_phone
            }

            property = {
                address1: "123 Main Street",
                address2: "",
                city: "Boston",
                state: "MA",
                country: "US",
                zip: "02210"
            }

            params = {
                :id => warranty_configuration.id,
                :order => {
                    homeowner: homeowner,
                    property: property,
                    inspection_date: 5.days.ago,
                    binder_id: binder.id,
                    warranty_plan_id: warranty_plan.id
                }
            }

            # make sure warranty count is 0
            expect(binder.warranties.length).to eq(0)

            Warranties::Configurations::Service.new(warranty_configuration.id).create_warranty(user, params)

            # make sure warranty was created
            expect(binder.warranties.count).to eq(1)
        end

        it "creates the warranty when cycle is weeks" do
            binder = create(:binder)
            user = create(:user)

            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")

            warranty_company = create(:warranty_company)
            warranty_plan = create(:warranty_plan, :warranty_company => warranty_company, :cycle => "weeks")
            warranty_configuration = create(:warranty_configuration)
            homeowner = create(:user)

            homeowner = {
                id: homeowner.id,
                firstName: homeowner.user_profile.first_name,
                lastName: homeowner.user_profile.last_name,
                email: homeowner.email,
                phone: homeowner.user_profile.mobile_phone
            }

            property = {
                address1: "123 Main Street",
                address2: "",
                city: "Boston",
                state: "MA",
                country: "US",
                zip: "02210"
            }

            params = {
                :id => warranty_configuration.id,
                :order => {
                    homeowner: homeowner,
                    property: property,
                    inspection_date: 5.days.ago,
                    binder_id: binder.id,
                    warranty_plan_id: warranty_plan.id
                }
            }

            # make sure warranty count is 0
            expect(binder.warranties.length).to eq(0)

            Warranties::Configurations::Service.new(warranty_configuration.id).create_warranty(user, params)

            # make sure warranty was created
            expect(binder.warranties.count).to eq(1)
        end

        it "creates the warranty when cycle is months" do
            binder = create(:binder)
            user = create(:user)

            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")

            warranty_company = create(:warranty_company)
            warranty_plan = create(:warranty_plan, :warranty_company => warranty_company, :cycle => "months")
            warranty_configuration = create(:warranty_configuration)
            homeowner = create(:user)

            homeowner = {
                id: homeowner.id,
                firstName: homeowner.user_profile.first_name,
                lastName: homeowner.user_profile.last_name,
                email: homeowner.email,
                phone: homeowner.user_profile.mobile_phone
            }

            property = {
                address1: "123 Main Street",
                address2: "",
                city: "Boston",
                state: "MA",
                country: "US",
                zip: "02210"
            }

            params = {
                :id => warranty_configuration.id,
                :order => {
                    homeowner: homeowner,
                    property: property,
                    inspection_date: 5.days.ago,
                    binder_id: binder.id,
                    warranty_plan_id: warranty_plan.id
                }
            }

            # make sure warranty count is 0
            expect(binder.warranties.length).to eq(0)

            Warranties::Configurations::Service.new(warranty_configuration.id).create_warranty(user, params)

            # make sure warranty was created
            expect(binder.warranties.count).to eq(1)
        end

        it "creates the warranty when cycle is years" do
            binder = create(:binder)
            user = create(:user)

            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")

            warranty_company = create(:warranty_company)
            warranty_plan = create(:warranty_plan, :warranty_company => warranty_company, :cycle => "years")
            warranty_configuration = create(:warranty_configuration)
            homeowner = create(:user)

            homeowner = {
                id: homeowner.id,
                firstName: homeowner.user_profile.first_name,
                lastName: homeowner.user_profile.last_name,
                email: homeowner.email,
                phone: homeowner.user_profile.mobile_phone
            }

            property = {
                address1: "123 Main Street",
                address2: "",
                city: "Boston",
                state: "MA",
                country: "US",
                zip: "02210"
            }

            params = {
                :id => warranty_configuration.id,
                :order => {
                    homeowner: homeowner,
                    property: property,
                    inspection_date: 5.days.ago,
                    binder_id: binder.id,
                    warranty_plan_id: warranty_plan.id
                }
            }

            # make sure warranty count is 0
            expect(binder.warranties.length).to eq(0)

            Warranties::Configurations::Service.new(warranty_configuration.id).create_warranty(user, params)

            # make sure warranty was created
            expect(binder.warranties.count).to eq(1)
        end
    end
end
