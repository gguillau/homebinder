require 'rails_helper'

RSpec.describe Warranties::CP::Request, :type => :service do
    
    describe "#send_request" do
        it "returns 201 response" do
            warranty = create(:warranty)
            warranty_transaction = create(:warranty_transaction, :warranty => warranty)
            partner_binder_transaction = create(:partner_binder_transaction, :binder_transaction => warranty_transaction.binder_transaction)
            create(:warranty_configuration, :partner => partner_binder_transaction.partner)
            stub_request(:post, URI.escape(Warranties::CP::Request.new.url)).to_return(:status => 201, :body => '{"orderId": 200}')
            orderId = Warranties::CP::Request.new.send_request(warranty.id, Date.today)
            
            # expect the response and the body to be equal
            expect(orderId).to eq(200)
        end
        
        it "returns 403 response" do
            warranty = create(:warranty)
            warranty_transaction = create(:warranty_transaction, :warranty => warranty)
            partner_binder_transaction = create(:partner_binder_transaction, :binder_transaction => warranty_transaction.binder_transaction)
            create(:warranty_configuration, :partner => partner_binder_transaction.partner)
            stub_request(:post, URI.escape(Warranties::CP::Request.new.url)).to_return(:status => 403, :body => '{"errors": ["errors"]}')

            # expect method to raise an error
            expect{Warranties::CP::Request.new.send_request(warranty.id, Date.today)}.to raise_error BadRequestException
        end
        
        it "returns 403 response" do
            warranty = create(:warranty)
            warranty_transaction = create(:warranty_transaction, :warranty => warranty)
            partner_binder_transaction = create(:partner_binder_transaction, :binder_transaction => warranty_transaction.binder_transaction)
            create(:warranty_configuration, :partner => partner_binder_transaction.partner)
            stub_request(:post, URI.escape(Warranties::CP::Request.new.url)).to_return(:status => 403, :body => '')

            # expect method to raise an error
            expect{Warranties::CP::Request.new.send_request(warranty.id, Date.today)}.to raise_error BadRequestException
        end
    end
    
    describe "#url" do
        it "returns url for production" do
            allow(Host).to receive(:path).and_return(Host::PRODUCTION_HOST)
            
            url = Warranties::CP::Request.new.url
            expect(url).to eq(ENV["CP_PRODUCTION_URL"].to_s)
        end
    end
end