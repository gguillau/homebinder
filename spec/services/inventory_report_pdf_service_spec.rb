require 'rails_helper'

RSpec.describe InventoryReportPdfService, :type => :service do

    describe "init" do
        it "sets the binder, pdf and pdf font" do
            binder = create(:binder)
            user = create(:user, :role => "admin")

            service = InventoryReportPdfService.new(binder, user)

            expect(service.binder).to eq(binder)
            expect(service.user).to eq(user)
            expect(service.text).to eq("Inventory Report for")
            expect(service.pdf).to_not be(nil)
        end
    end

    describe "create" do
        it "creates pdf with inventory items" do
            binder = create(:binder)
            user = create(:user, :role => "admin")
            inventory_item = create(:inventory_item, :binder => binder)
            create(:inventory_item, :binder => binder)
            
            image = create(:image, :inventory_item_id => inventory_item.id)
            allow(image.file).to receive(:expiring_cloud_front_url).and_return("")
            
            service = InventoryReportPdfService.new(binder, user)
            
            allow(service).to receive(:add_logos)
            allow(service.pdf).to receive(:formatted_text_box)
            allow(service).to receive(:open).and_return("#{Rails.root}/spec/assets/flatlogonotext.png")
            
            service.create
            expect(service).to have_received(:add_logos)
        end
        it "creates pdf with no inventory items" do
            binder = create(:binder)
            user = create(:user, :role => "admin")
            
            service = InventoryReportPdfService.new(binder, user)
            
            allow(service.pdf).to receive(:image)
            allow(service.pdf).to receive(:formatted_text_box)
            
            service.create
        end
    end
end