require 'rails_helper'

RSpec.describe Salesforce::Homeowner, :type => :model do
  
    before :each do
        @user = create(:user)
        allow(ErrorService).to receive(:perform_async)
    end
    
    it "does not create the homeowner when nil argument passed" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return({:success => true, :id => 10})
        Salesforce::Homeowner.new.new_homeowner(nil)
        expect(salesforce).to_not have_received(:create!)
    end
    
    it "does not create the homeowner when non-homeowner argument passed" do
        # create admin
        admin = create(:user, :role => "admin")
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return({:success => true, :id => 10})
        Salesforce::Homeowner.new.new_homeowner(admin)
        expect(salesforce).to_not have_received(:create!)
    end
    
    it "raises an error and returns nil" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:find_where).and_return([{:id => 10}])
        allow(salesforce).to receive(:create!).and_raise(Savon::Error)
        value = Salesforce::Homeowner.new.new_homeowner(@user)
        expect(value).to eq(nil)
    end
    
    it "sends an error to New Relic" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:find_where).and_return([{:id => 10}])
        allow(salesforce).to receive(:create!).and_return(false)
        Salesforce::Homeowner.new.new_homeowner(@user)
        expect(ErrorService).to have_received(:perform_async)
    end

    it "sends an error to New Relic" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:find_where).and_return([{:id => 10}])
        allow(salesforce).to receive(:create!).and_return({:success => false})
        Salesforce::Homeowner.new.new_homeowner(@user)
        expect(ErrorService).to have_received(:perform_async)
    end
    
    it "creates the account and sends an error to new relic" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:find_where).and_return([{:id => 10}])
        allow(salesforce).to receive(:create!).and_return({:success => true, :id => 10})
        allow_any_instance_of(User).to receive(:save).and_return(false)
        Salesforce::Homeowner.new.new_homeowner(@user)
        expect(salesforce).to have_received(:create!).with(
            "Homeowner__c", 
            User_ID__c: @user.id,
            Email__c: @user.email,
            Sign_In_Count__c: @user.sign_in_count,
            Create_Method__c: @user.create_method,
            Accepted_Transfer_Date__c: Salesforce::Homeowner.new.format_date(@user.accepted_transfer_at),
            Role__c: @user.role,
            DOB__c: Salesforce::Homeowner.new.format_date(@user.user_profile.dob),
            First_Name__c: @user.user_profile.first_name,
            Last_Name__c: @user.user_profile.last_name,
            Gender__c: @user.user_profile.sex,
            HB_Account_Created_Date__c: Salesforce::Homeowner.new.format_date(@user.created_at),
            Monthly_Email__c: @user.user_profile.monthly_email,
            Phone__c: @user.user_profile.mobile_phone,
            Do_Not_Contact__c: @user.allow_calls
        )
        expect(ErrorService).to have_received(:perform_async)
    end
    
    it "creates the account" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:find_where).and_return([{:id => 10}])
        allow(salesforce).to receive(:create!).and_return({:success => true, :id => 10})
        Salesforce::Homeowner.new.new_homeowner(@user)
        expect(salesforce).to have_received(:create!).with(
            "Homeowner__c", 
            User_ID__c: @user.id,
            Email__c: @user.email,
            Sign_In_Count__c: @user.sign_in_count,
            Create_Method__c: @user.create_method,
            Accepted_Transfer_Date__c: Salesforce::Homeowner.new.format_date(@user.accepted_transfer_at),
            Role__c: @user.role,
            DOB__c: Salesforce::Homeowner.new.format_date(@user.user_profile.dob),
            First_Name__c: @user.user_profile.first_name,
            Last_Name__c: @user.user_profile.last_name,
            Gender__c: @user.user_profile.sex,
            HB_Account_Created_Date__c: Salesforce::Homeowner.new.format_date(@user.created_at),
            Monthly_Email__c: @user.user_profile.monthly_email,
            Phone__c: @user.user_profile.mobile_phone,
            Do_Not_Contact__c: @user.allow_calls
        )
        @user.reload
        expect(@user.salesforce_id).to eq("10")
    end
    
    it "updates the account individually" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:find_where).and_return([{:id => 1}])
        allow(salesforce).to receive(:update!).and_return({:success => true})
        Salesforce::Homeowner.new.update_homeowner(@user)
        expect(salesforce).to have_received(:update!).with(
            "Homeowner__c", 
            ID: 1,
            User_ID__c: @user.id,
            Email__c: @user.email,
            Sign_In_Count__c: @user.sign_in_count,
            Create_Method__c: @user.create_method,
            Accepted_Transfer_Date__c: Salesforce::Homeowner.new.format_date(@user.accepted_transfer_at),
            Role__c: @user.role,
            DOB__c: Salesforce::Homeowner.new.format_date(@user.user_profile.dob),
            First_Name__c: @user.user_profile.first_name,
            Last_Name__c: @user.user_profile.last_name,
            Gender__c: @user.user_profile.sex,
            HB_Account_Created_Date__c: Salesforce::Homeowner.new.format_date(@user.created_at),
            Monthly_Email__c: @user.user_profile.monthly_email,
            Phone__c: @user.user_profile.mobile_phone,
            Do_Not_Contact__c: @user.allow_calls
        )
    end
    
    it "it sends an error to New Relic" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        Salesforce::Homeowner.new.review_result(false, @user, "FAKE_ERROR")
        expect(ErrorService).to have_received(:perform_async).with("FAKE_ERROR", {:user_id => @user.id})
    end
    
    it "it sends an error to New Relic" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        Salesforce::Homeowner.new.review_result({:success => "false"}, @user, "FAKE_ERROR")
        expect(ErrorService).to have_received(:perform_async).with("FAKE_ERROR", {:user_id => @user.id})
    end
    
    it "deletes the account" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:find_where).and_return([{:id => 1}])
        allow(salesforce).to receive(:destroy!).and_return({:success => true})
        Salesforce::Homeowner.new.delete_salesforce_object(@user.id)
        expect(salesforce).to have_received(:destroy!).with(1)
    end
  
end