require 'rails_helper'

RSpec.describe Salesforce::Binder, :type => :model do
    before :each do
        @user = create(:user)
        @binder = create(:binder)
        create(:user_binder, :user_id => @user.id, :binder_id => @binder.id, :role => "owner")
    end

    it "does not create the binder when nil argument passed" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return({:success => true, :id => 10})
        Salesforce::Binder.new.new_binder(nil)
        expect(salesforce).to_not have_received(:create!)
    end

    it "does not create the binder when no owner is present" do
        # create binder
        binder = create(:binder)
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return({:success => true, :id => 10})
        Salesforce::Binder.new.new_binder(binder)
        expect(salesforce).to_not have_received(:create!)
    end

    it "sends an error" do
        # set the stubs
        allow(ErrorService).to receive(:perform_async)

        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return(false)
        allow(salesforce).to receive(:find_where).and_return([{:id => 1}])
        Salesforce::Binder.new.new_binder(@binder)
        expect(ErrorService).to have_received(:perform_async)
    end

    it "sends an error" do
        # set the stubs
        allow(ErrorService).to receive(:perform_async)
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return({:success => false})
        allow(salesforce).to receive(:find_where).and_return([{:id => 1}])
        Salesforce::Binder.new.new_binder(@binder)
        expect(ErrorService).to have_received(:perform_async)
    end

    it "returns nil" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_raise(Savon::Error)
        allow(salesforce).to receive(:find_where).and_return([{:id => 1}])
        value = Salesforce::Binder.new.create_binder(@binder)
        expect(value).to be(nil)
    end

    it "creates the account" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return({:success => true, :id => 10})
        allow(salesforce).to receive(:find_where).and_return([{:id => 1}])
        Salesforce::Binder.new.new_binder(@binder)
        expect(salesforce).to have_received(:create!).with(
            "Binder__c",
            Name: @binder.name,
            Binder_ID__c: @binder.id,
            Primary__c: @binder.primary,
            Active__c: @binder.active,
            Create_Method__c: @binder.create_method,
            Created_Date__c: Salesforce::Binder.new.format_date(@binder.created_at),
            Updated_Date__c: Salesforce::Binder.new.format_date(@binder.updated_at),
            Details__c: Salesforce::Binder.new.format_value(@binder.details),
            Address__c: @binder.property.address1,
            Address_Line_2__c: @binder.property.address1,
            City__c: @binder.property.city,
            State__c: @binder.property.state,
            Zip_Code__c: @binder.property.zip,
            Country__c: @binder.property.country,
            Homeowner_Name__c: 1
        )
    end

    it "updates the account individually" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:find_where).and_return([{:id => 1}])
        allow(salesforce).to receive(:update!).and_return({:success => true})
        Salesforce::Binder.new.update_binder(@binder)
        expect(salesforce).to have_received(:update!).with(
            "Binder__c",
            ID: 1,
            Name: @binder.name,
            Binder_ID__c: @binder.id,
            Primary__c: @binder.primary,
            Active__c: @binder.active,
            Create_Method__c: @binder.create_method,
            Created_Date__c: Salesforce::Binder.new.format_date(@binder.created_at),
            Updated_Date__c: Salesforce::Binder.new.format_date(@binder.updated_at),
            Details__c: Salesforce::Binder.new.format_value(@binder.details),
            Address__c: @binder.property.address1,
            Address_Line_2__c: @binder.property.address1,
            City__c: @binder.property.city,
            State__c: @binder.property.state,
            Zip_Code__c: @binder.property.zip,
            Country__c: @binder.property.country,
            Homeowner_Name__c: 1
        )
    end

    it "it sends an error to New Relic" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(ErrorService).to receive(:perform_async)
        Salesforce::Binder.new.review_result(false, @binder, "FAKE_ERROR")
        expect(ErrorService).to have_received(:perform_async).with("FAKE_ERROR", {:binder_id => @binder.id})
    end

    it "it sends an error to New Relic" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(ErrorService).to receive(:perform_async)
        Salesforce::Binder.new.review_result({:success => "false"}, @binder, "FAKE_ERROR")
        expect(ErrorService).to have_received(:perform_async).with("FAKE_ERROR", {:binder_id => @binder.id})
    end

    it "deletes the account" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:find_where).and_return([{:id => 1}])
        allow(salesforce).to receive(:destroy!).and_return({:success => true})
        Salesforce::Binder.new.delete_salesforce_object(@binder.id)
        expect(salesforce).to have_received(:destroy!).with(1)
    end
end
