require 'rails_helper'

RSpec.describe Salesforce::Partner, :type => :model do

    before :each do
        @partner = create(:partner)
        @account = create(:account, manager_id: @partner.id)
        @partner.account_id = @account.id
        @partner.save
        allow(ErrorService).to receive(:perform_async)
    end

    it "raises an error" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true)
        allow(salesforce).to receive(:authenticate).and_raise(BadRequestException)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)

        expect{ Salesforce::Partner.new.new_partner(@partner, {})}.to raise_error BadRequestException
        expect(ErrorService).to have_received(:perform_async)
    end

    it "raises an error when creating an account" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)
        expect(salesforce).to receive(:create!).and_return(false)

        Salesforce::Partner.new.new_partner(@partner, OpenStruct.new({employees: "50", source: "Social Media"}))
        expect(ErrorService).to have_received(:perform_async)
    end

    it "raises an error when creating an account" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)
        expect(salesforce).to receive(:create!).and_return(false)

        Salesforce::Partner.new.new_partner(@partner, OpenStruct.new({employees: "50", source: "Social Media", volume: "1100", software: nil, admin_software: "ISN"}))
        expect(ErrorService).to have_received(:perform_async)
    end

    it "raises an error when creating an account" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)
        expect(salesforce).to receive(:create!).and_return({:success => false})

        Salesforce::Partner.new.new_partner(@partner, OpenStruct.new({employees: "50", source: "Social Media", volume: "1100", software: nil, admin_software: "ISN"}))
        expect(ErrorService).to have_received(:perform_async)
    end

    it "creates the account, opportunity and contact" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)
        expect(salesforce).to receive(:create!).exactly(3).times.and_return({:success => true, :id => 10})

        Salesforce::Partner.new.new_partner(@partner, OpenStruct.new({employees: "50", source: "Social Media", volume: "1100", software: nil, admin_software: "ISN"}))
        expect(ErrorService).to_not have_received(:perform_async)
    end

    it "updates the account and contact" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)
        expect(salesforce).to receive(:find_where).exactly(2).times.and_return([{:id => 10}])
        expect(salesforce).to receive(:update!).exactly(1).times.and_return({:success => true})

        Salesforce::Partner.new.update_partner(@partner)
        expect(ErrorService).to_not have_received(:perform_async)
    end

    it "raises an error and returns false" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_raise(Savon::Error)

        survey = OpenStruct.new({employees: "50", source: "Social Media", volume: "1100", software: nil, admin_software: "ISN"})
        boolean = Salesforce::Partner.new.create_account(@partner, survey, nil)

        expect(boolean).to eq(false)
    end

    it "creates the account individually" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return({:success => true})

        survey = OpenStruct.new({employees: "50", source: "Social Media", volume: "1100", software: nil, admin_software: "ISN"})
        Salesforce::Partner.new.create_account(@partner, survey, nil)
        expect(salesforce).to have_received(:create!).with(
            "Account",
            Name: @partner.contact + " - " + @partner.name,
            Phone: @partner.phone,
            NumberOfEmployees: "50",
            # create custom fields
            Partner_ID__c: @partner.id,
            Affiliate_Code__c: @partner.affiliate_code,
            Category__c: "Multi-Inspector Firm",
            Volume__c: "1100",
            Software_Administrative__c: "ISN",
            Software_Report_Writing__c: nil
        )
    end

    it "creates the account individually" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return({:success => true})

        survey = OpenStruct.new({employees: "1", source: "Social Media", volume: "1100", software: nil, admin_software: "ISN"})
        Salesforce::Partner.new.create_account(@partner, survey, nil)
        expect(salesforce).to have_received(:create!).with(
            "Account",
            Name: @partner.contact + " - " + @partner.name,
            Phone: @partner.phone,
            NumberOfEmployees: "1",
            # create custom fields
            Partner_ID__c: @partner.id,
            Affiliate_Code__c: @partner.affiliate_code,
            Category__c: "Single Inspector Firm",
            Volume__c: "1100",
            Software_Administrative__c: "ISN",
            Software_Report_Writing__c: nil
        )
    end

    it "creates the contact individually" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return({:success => true})

        Salesforce::Partner.new.create_contact(@partner, 10, ["Mike", "Davis"])
        expect(salesforce).to have_received(:create!).with(
            "Contact",
            AccountId: 10,
            FirstName: "Mike",
            Email: @partner.email,
            Phone: @partner.phone,
            LastName: "Davis"
        )
    end

    it "creates the opportunity individually" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return({:success => true})

        survey = OpenStruct.new({employees: "50", source: "Social Media", volume: "1100", software: nil, admin_software: "ISN"})
        Salesforce::Partner.new.create_opportunity(@partner, survey, 10, ["Mike", "Davis"])
        expect(salesforce).to have_received(:create!).with(
            "Opportunity",
            AccountId: 10,
            Name: @partner.contact,
            CloseDate: Date.today + 30.days,
            StageName: "Qualifying",
            LeadSource: "Social Media",
            Amount: 2350,
            # Custom Fields
            Category__c: "Multi-Inspector Firm",
            Affiliate_Code__c: @partner.affiliate_code,
            Volume__c: "1100",
            Software_Administrative__c: "ISN",
            Software_Report_Writing__c: nil
        )
    end

    it "creates the opportunity individually" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return({:success => true})

        survey = OpenStruct.new({employees: "1", source: "Social Media", volume: "1100", software: "Horizon", admin_software: "ISN"})
        Salesforce::Partner.new.create_opportunity(@partner, survey, 10, ["Mike", "Davis"])
        expect(salesforce).to have_received(:create!).with(
            "Opportunity",
            AccountId: 10,
            Name: @partner.contact,
            CloseDate: Date.today + 30.days,
            StageName: "Qualifying",
            LeadSource: "Social Media",
            Amount: 2350,
            # Custom Fields
            Category__c: "Single Inspector Firm",
            Affiliate_Code__c: @partner.affiliate_code,
            Volume__c: "1100",
            Software_Administrative__c: "ISN",
            Software_Report_Writing__c: "Horizon"
        )
    end
    
    it "creates the opportunity individually for lender" do
        partner = create(:partner, :partner_type => "lender")
        account = create(:account, manager_id: partner.id)
        partner.account_id = account.id
        partner.save
        
        allow(ErrorService).to receive(:perform_async)
        
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return({:success => true})

        survey = OpenStruct.new({employees: "1", source: "Social Media", volume: "1100", software: "Horizon", admin_software: "ISN"})
        Salesforce::Partner.new.create_opportunity(partner, survey, 10, ["Mike", "Davis"])
        expect(salesforce).to have_received(:create!).with(
            "Opportunity",
            AccountId: 10,
            Name: partner.contact,
            CloseDate: Date.today + 30.days,
            StageName: "Qualifying",
            Amount: 8800,
            # Custom Fields
            Volume__c: "1100",
        )
    end

    it "returns 192" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)

        amount = Salesforce::Partner.new.get_opportunity_amount("10")
        expect(amount).to eq(192)
    end

    it "returns 192" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)

        amount = Salesforce::Partner.new.get_opportunity_amount("50")
        expect(amount).to eq(192)
    end

    it "returns 648" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)

        amount = Salesforce::Partner.new.get_opportunity_amount("250")
        expect(amount).to eq(648)
    end

    it "returns 648" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)

        amount = Salesforce::Partner.new.get_opportunity_amount("500")
        expect(amount).to eq(648)
    end

    it "returns 1000" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)

        amount = Salesforce::Partner.new.get_opportunity_amount("1000")
        expect(amount).to eq(1175)
    end

    it "returns 2000" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)

        amount = Salesforce::Partner.new.get_opportunity_amount("1100")
        expect(amount).to eq(2350)
    end

    it "updates the account individually"  do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:update!).and_return({:success => true})

        Salesforce::Partner.new.update_salesforce_account(@partner, {:id => 10})
        expect(salesforce).to have_received(:update!).with(
            "Account",
            ID: 10,
            #Name: @partner.contact + " - " + @partner.name,
            Phone: @partner.phone,
            # custom fields in Salesforce
            Affiliate_Code__c: @partner.affiliate_code,
            Account_Number__c: @account.account_number,
            Account_Status__c: "Active",
            Account_Sub_Status__c: nil,
            Account_Sub_Type__c: "Free Trial",
            Account_Type__c: @account.account_type,
            BillingStreet: nil,
            BillingCity: nil,
            BillingState: nil,
            BillingCountry: "US",
            BillingPostalCode: nil,
            Account_Billing_Frequency__c: nil,
            Account_Trial_Expiration__c: @account.trial_expiration.strftime("%F"),
            Account_Billing_Activation__c: nil,
            Account_Subscription_Amount__c: @account.subscription_amount,
            Account_Transaction_Amount__c: @account.transaction_amount,
            Account_Payment_Type__c: @account.payment_type,
            Binders_Last_7_Days__c: 0,
            Binders_Last_30_Days__c: 0,
            Total_Binders__c: 0,
            Binders_Not_Transferred__c: 0,
            Automatic_Transfer__c: false,
            Transfer_Delay__c: 1,
            Default_User_Branding_Option__c: "co_brand",
            Agent_Notification__c: "always",
            Self_Serve_Link__c: "/partners//homeowners",
            Show_ISN_Tab__c: false,
            Binder_Action_Option__c: "transfer",
            Recommended_Home_Pros__c: 0,
            Stripe_Customer_ID__c: nil,
            Enable_Repair_Pricer__c: false,
            Repair_Pricer_Pre_Paid_Reports__c: false,
            Report_Link__c: false
        )
    end

    it "updates the contact individually" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:update!).and_return({:success => true})

        Salesforce::Partner.new.update_salesforce_contact(@partner, {:id => 10})
        expect(salesforce).to have_received(:update!).with(
            "Contact",
            ID: 10,
            FirstName: @partner.contact.split(Partner::Automation::EmailParser::SPACE)[0],
            Email: @partner.email,
            Phone: @partner.phone,
            LastName:  @partner.contact.split(Partner::Automation::EmailParser::SPACE)[1]
        )
    end

    it "it formats the date" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)

        date = Salesforce::Partner.new.format_date(Date.new(2001,2,3))
        expect(date).to eq("2001-02-03")
    end

    it "it performs a titleize on the string" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        expect(Soapforce::Client).to receive(:new).and_return(salesforce)

        field = Salesforce::Partner.new.format_value("free_trial")
        expect(field).to eq("Free Trial")
    end

    it "it sends an error to New Relic" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        Salesforce::Partner.new.review_result(false, @partner, "FAKE_ERROR")

        expect(ErrorService).to have_received(:perform_async).with("FAKE_ERROR", {:partner_id => @partner.id})
    end

    it "it sends an error to New Relic" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)

        Salesforce::Partner.new.review_result({:success => "false"}, @partner, "FAKE_ERROR")

        expect(ErrorService).to have_received(:perform_async).with("FAKE_ERROR", {:partner_id => @partner.id})
    end

end