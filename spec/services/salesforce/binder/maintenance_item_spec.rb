require 'rails_helper'

RSpec.describe Salesforce::Binder::MaintenanceItem, :type => :model do
  
    before :each do
        @user = create(:user)
        @binder = create(:binder)
        create(:user_binder, :user_id => @user.id, :binder_id => @binder.id, :role => "owner")
        @item = create(:maintenance_item, :binder_id => @binder.id)
    end
    
    it "does not create the maintenance_item when nil argument passed" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return({:success => true, :id => 10})
        Salesforce::Binder::MaintenanceItem.new.new_item(nil)
        expect(salesforce).to_not have_received(:create!)
    end
    
    it "raises an error" do
        # set the stubs
        allow(ErrorService).to receive(:perform_async)
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_raise(Savon::Error)
        allow(salesforce).to receive(:find_where).and_return([{:id => 1}])
        Salesforce::Binder::MaintenanceItem.new.new_item(@item)
        expect(ErrorService).to have_received(:perform_async)
    end
    
    it "sends an error to newrelic" do
        # set the stubs
        allow(ErrorService).to receive(:perform_async)
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return(false)
        allow(salesforce).to receive(:find_where).and_return([{:id => 1}])
        Salesforce::Binder::MaintenanceItem.new.new_item(@item)
        expect(ErrorService).to have_received(:perform_async)
    end
    
    it "sends an error to newrelic" do
        # set the stubs
        allow(ErrorService).to receive(:perform_async)
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return({:success => false})
        allow(salesforce).to receive(:find_where).and_return([{:id => 1}])
        Salesforce::Binder::MaintenanceItem.new.new_item(@item)
        expect(ErrorService).to have_received(:perform_async)
    end

    it "creates the account" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return({:success => true, :id => 10})
        allow(salesforce).to receive(:find_where).and_return([{:id => 1}])
        Salesforce::Binder::MaintenanceItem.new.new_item(@item)
        expect(salesforce).to have_received(:create!).with(
            "Maintenance_Item__c",
            Name: @item.name,
            Maintenance_ID__c: @item.id,
            Binder__c: 1,
            Additional_details__c: Salesforce::Binder::MaintenanceItem.new.format_value(@item.details),
            Email_Notifications__c: @item.email_notifications,
            Last_Completed_On__c: Salesforce::Binder::MaintenanceItem.new.format_date(@item.get_last_completed_date),
            Due_Next_On__c: Salesforce::Binder::MaintenanceItem.new.format_date(@item.do_date),
            Occurs_Every__c: @item.interval,
            Time__c: @item.maintenance_cycle
        )
    end
    
    it "updates the account individually" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:find_where).and_return([{:id => 1}])
        allow(salesforce).to receive(:update!).and_return({:success => true})
        Salesforce::Binder::MaintenanceItem.new.update_item(@item)
        expect(salesforce).to have_received(:update!).with(
            "Maintenance_Item__c", 
            ID: 1,
            Name: @item.name,
            Maintenance_ID__c: @item.id,
            Binder__c: 1,
            Additional_details__c: Salesforce::Binder::MaintenanceItem.new.format_value(@item.details),
            Email_Notifications__c: @item.email_notifications,
            Last_Completed_On__c: Salesforce::Binder::MaintenanceItem.new.format_date(@item.get_last_completed_date),
            Due_Next_On__c: Salesforce::Binder::MaintenanceItem.new.format_date(@item.do_date),
            Occurs_Every__c: @item.interval,
            Time__c: @item.maintenance_cycle
        )
    end
    
    it "it sends an error to New Relic" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(ErrorService).to receive(:perform_async)
        Salesforce::Binder::MaintenanceItem.new.review_result(false, @item, "FAKE_ERROR")
        expect(ErrorService).to have_received(:perform_async).with("FAKE_ERROR", {:maintenance_item_id => @item.id})
    end
    
    it "it sends an error to New Relic" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(ErrorService).to receive(:perform_async)
        Salesforce::Binder::MaintenanceItem.new.review_result({:success => "false"}, @item, "FAKE_ERROR")
        expect(ErrorService).to have_received(:perform_async).with("FAKE_ERROR", {:maintenance_item_id => @item.id})
    end
    
    it "deletes the account" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:find_where).and_return([{:id => 1}])
        allow(salesforce).to receive(:destroy!).and_return({:success => true})
        Salesforce::Binder::MaintenanceItem.new.delete_salesforce_object(@item.id)
        expect(salesforce).to have_received(:destroy!).with(1)
    end
  
end