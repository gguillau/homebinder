require 'rails_helper'

RSpec.describe Salesforce::Binder::ServiceRequest, :type => :model do
  
    before :each do
        @user = create(:user)
        @binder = create(:binder)
        create(:user_binder, :user_id => @user.id, :binder_id => @binder.id, :role => "owner")
    end
    
    it "does not create the request when nil argument passed" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return({:success => true, :id => 10})
        Salesforce::Binder::ServiceRequest.new.new_service_request(nil, @binder, "status")
        expect(salesforce).to_not have_received(:create!)
    end
    
    it "does not create the request when nil argument passed" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return({:success => true, :id => 10})
        Salesforce::Binder::ServiceRequest.new.new_service_request("Choosing satellite TV, cable or internet providers", nil, "status")
        expect(salesforce).to_not have_received(:create!)
    end
    
    it "does not create the request when nil argument passed" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return({:success => true, :id => 10})
        Salesforce::Binder::ServiceRequest.new.new_service_request("Choosing satellite TV, cable or internet providers", @binder, nil)
        expect(salesforce).to_not have_received(:create!)
    end
    
    it "returns nil" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_raise(Savon::Error)
        allow(salesforce).to receive(:find_where).and_return([{:id => 1}])
        value = Salesforce::Binder::ServiceRequest.new.create_service_request("Choosing satellite TV, cable or internet providers", @binder, "requested")
        expect(value).to be(nil)
    end
    
    it "sends an error" do
        # set the stubs
        allow(ErrorService).to receive(:perform_async)
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return(false)
        allow(salesforce).to receive(:find_where).and_return([{:id => 1}])
        Salesforce::Binder::ServiceRequest.new.new_service_request("Choosing satellite TV, cable or internet providers", @binder, "requested")
        expect(ErrorService).to have_received(:perform_async)
    end
    
    it "sends an error" do
        # set the stubs
        allow(ErrorService).to receive(:perform_async)
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return({:success => false})
        allow(salesforce).to receive(:find_where).and_return([{:id => 1}])
        Salesforce::Binder::ServiceRequest.new.new_service_request("Choosing satellite TV, cable or internet providers", @binder, "requested")
        expect(ErrorService).to have_received(:perform_async)
    end

    it "creates the request" do
        # set the stubs
        salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
        allow(Soapforce::Client).to receive(:new).and_return(salesforce)
        allow(salesforce).to receive(:create!).and_return({:success => true, :id => 10})
        allow(salesforce).to receive(:find_where).and_return([{:id => 1}])
        Salesforce::Binder::ServiceRequest.new.new_service_request("Choosing satellite TV, cable or internet providers", @binder, "requested")
        expect(salesforce).to have_received(:create!).with(
            "Service_Request__c", 
            Name: "Choosing satellite TV, cable or internet providers",
            Status__c: "requested",
            Request_Date__c: Salesforce::Binder::ServiceRequest.new.format_date(Date.today),
            Binder__c: 1
        )
    end

end