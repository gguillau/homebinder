require 'rails_helper'

RSpec.describe Salesforce::Base, :type => :model do

    # testing for production credentials

    describe "#initialize" do
        it "initializes production client" do
            allow(Host).to receive(:path).and_return(Host::PRODUCTION_HOST)
            salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
            allow(Soapforce::Client).to receive(:new).and_return(salesforce)
            expect{Salesforce::Base.new}.to_not raise_error
        end
        it "raises an error and sends message to New Relic" do
            allow(ErrorService).to receive(:perform_async)
            allow(Soapforce::Client).to receive(:new).and_raise(BadRequestException)
            expect{Salesforce::Base.new}.to raise_error BadRequestException
            expect(ErrorService).to have_received(:perform_async)
        end
    end

    describe "#format_value" do
        it "initializes production client" do
            salesforce = double("Soapforce::Client", :authenticate => true, :logout => true, :create => false)
            allow(Soapforce::Client).to receive(:new).and_return(salesforce)
            client = Salesforce::Base.new
            value = client.format_value(Date.today)
            expect(value).to eq(Date.today.strftime("%F"))
        end
    end
end
