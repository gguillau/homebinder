require 'rails_helper'

RSpec.describe EnergysageService do
    describe "#request_services" do
        it "raises an error when response is 400" do
            stub_request(:any, /energysage/).to_return(:status => 400, :body => '{"ResultCode": 400}')
            binder = create(:binder)
            binder.property.country = "US"
            binder.property.state = "MA"
            binder.property.zip = 55555
            binder.property.lat = 0
            binder.property.long = 0
            binder.property.save!
            owner = create(:user)
            create(:user_binder, :role => "owner", :binder => binder, :user => owner)
            
            expect{EnergysageService.new.request_services(binder.id)}.to raise_error(BadRequestException)
        end
        
        it "returns when invalid_address" do
            allow(HTTParty).to receive(:get)
            stub_request(:any, /energysage/).to_return(:status => 400, :body => '{"ResultCode": 400}')
            allow_any_instance_of(Binder::Property).to receive(:set_valid_address)
            binder = create(:binder, :property_attributes => {:address1 => "123 Main Street", :city => "Boston", :country => "US", :state => "MA", :zip => 55555, :lat => nil, :long => nil})
            owner = create(:user)
            create(:user_binder, :role => "owner", :binder => binder, :user => owner)
            
            
            url = EnergysageService.new.request_services(binder.id)
            
            expect(url).to be(nil)
            expect(HTTParty).to_not have_received(:get)
        end
    
        it "does not raise an error" do
            stub_request(:any, /energysage/).to_return(:status => 200, :body => '{"ResultCode": 200}')
            binder = create(:binder)
            binder.property.country = "US"
            binder.property.state = "MA"
            binder.property.zip = 55555
            binder.property.lat = 0
            binder.property.long = 0
            binder.property.save!
            owner = create(:user)
            create(:user_binder, :role => "owner", :binder => binder, :user => owner)
            expect{EnergysageService.new.request_services(binder.id)}.to_not raise_error
        end
    end
    
    describe "#base_url" do
        it "returns the url for production" do
            allow(Host).to receive(:path).and_return(Host::PRODUCTION_HOST)
            # get the url
            base_url = EnergysageService.new.base_url

            expect(base_url).to eq(ENV["ENERGYSAGE_PRODUCTION_BASE_URL"].to_s)
        end
    
        it "returns the url for test" do
            allow(Host).to receive(:path).and_return("Test")
            # get the url
            base_url = EnergysageService.new.base_url

            expect(base_url).to eq(ENV["ENERGYSAGE_TEST_BASE_URL"].to_s)
        end
    end
    
    describe "#secret_key" do
        it "returns the secret key for production" do
            allow(Host).to receive(:path).and_return(Host::PRODUCTION_HOST)
            # get the secret_key
            secret_key = EnergysageService.new.secret_key

            expect(secret_key).to eq(ENV["ENERGYSAGE_PRODUCTION_SECRET"].to_s)
        end
    
        it "returns the secret key for test" do
            allow(Host).to receive(:path).and_return("Test")
            # get the secret_key
            secret_key = EnergysageService.new.secret_key

            expect(secret_key).to eq(ENV["ENERGYSAGE_TEST_SECRET"].to_s)
        end
    end
    
    describe "#valid_location" do
        it "returns false when country is Canada" do
            country = "CA"
            state = "MA"
            result = EnergysageService.new.valid_location(country, state)
            
            expect(result).to eq(false)
        end
        
        it "returns false when country is US and state is Alaska (not covered)" do
            country = "US"
            state = "AK"
            result = EnergysageService.new.valid_location(country, state)
            
            expect(result).to eq(false)
        end
        
        it "returns true when country is US and state is Massachusetts (covered)" do
            country = "US"
            state = "MA"
            result = EnergysageService.new.valid_location(country, state)
            
            expect(result).to eq(true)
        end
    end
    
    describe "#valid_address" do
        it "returns false when zip is missing" do
            binder = create(:binder)
            binder.property.zip = nil
            binder.property.lat = 0
            binder.property.long = 0
            result = EnergysageService.new.valid_address(binder)
            
            expect(result).to eq(false)
        end
        
        it "returns false when lat is missing" do
            binder = create(:binder)
            binder.property.zip = 55555
            binder.property.lat = nil
            binder.property.long = 0
            result = EnergysageService.new.valid_address(binder)
            
            expect(result).to eq(false)
        end
        
        it "returns false when long is missing" do
            binder = create(:binder)
            binder.property.zip = 55555
            binder.property.lat = 0
            binder.property.long = nil
            result = EnergysageService.new.valid_address(binder)
            
            expect(result).to eq(false)
        end
        
        it "returns true when no attribute is missing" do
            binder = create(:binder)
            binder.property.zip = 55555
            binder.property.lat = 0
            binder.property.long = 0
            result = EnergysageService.new.valid_address(binder)
            
            expect(result).to eq(true)
        end
    end
    
    describe "#set_home_type" do
        it "sets ht attribute in data to single family" do
            binder = create(:binder)
            binder.property.property_type = 'Single Family'
            data = {}
            EnergysageService.new.set_home_type(binder, data)
            expect(data[:ht]).to eq('sf')
        end
        
        it "sets ht attribute in data to multi family" do
            binder = create(:binder)
            binder.property.property_type = 'Multi Family'
            data = {}
            EnergysageService.new.set_home_type(binder, data)
            expect(data[:ht]).to eq('mf')
        end
    end
end