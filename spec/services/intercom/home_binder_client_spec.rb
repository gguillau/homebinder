require 'rails_helper'

RSpec.describe Intercom::HomeBinderClient, :type => :model do
    describe "#metadata_for_partner" do
        it "returns all metadata for partner" do
            intercom = double("Intercom::Client", :new => true, :events => true)
            allow(Intercom::Client).to receive(:new).and_return(intercom)
            
            partner = create(:partner)
            account = create(:account)
            user = create(:user, :role => "inspector")
            create(:partner_user, :partner_id => partner.id, :user => user, :role => "admin")
            create(:api_key, :partner_id => partner.id, :key => SecureRandom.hex(4))
            partner.account_id = account.id
            partner.save!
            client = Intercom::HomeBinderClient.new
            
            client.metadata_for_partner(partner)
            
        end
    end
end
