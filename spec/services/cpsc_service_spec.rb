require 'rails_helper'

RSpec.describe CPSCService, :type => :service do
    describe "#get_new_recalls" do
        it "gets recalls by keyword" do
            create(:recall_keyword, :keyword => "test")
            allow(CPSCService).to receive(:get_recalls_by_parameter)
            CPSCService.get_new_recalls
            expect(CPSCService).to have_received(:get_recalls_by_parameter).twice
        end
    end

    describe "#get_recalls_by_parameter" do
        it "creates and updates recalls" do
            Sidekiq::Testing.disable!
            url = "http://www.saferproducts.gov/RestWebServices/Recall?RecallTitle=test&format=json"
            stub_request(:get, URI.escape(url)).to_return(:status => 200, :body => "")
            recalls = [
                {
                    "RecallID" => 320,
                    "RecallNumber"=>"10327",
                    "RecallDate"=>"2010-08-26T00:00:00"
                },
                {
                    "RecallID" => 321,
                    "RecallNumber"=>"10328",
                    "RecallDate"=>"2010-08-26T00:00:00"
                }
            ]
            create(:recall, :number => "10327", :details => nil)
            allow(JSON).to receive(:parse).and_return(recalls)
            CPSCService.get_recalls_by_parameter("RecallTitle", "test")
            expect(Recall.all.count).to eq(2)
        end
        it "sends message to New Relic" do
            url = "http://www.saferproducts.gov/RestWebServices/Recall?RecallTitle=test&format=json"
            stub_request(:get, URI.escape(url)).to_return(:status => 200, :body => "")
            allow(JSON).to receive(:parse).and_raise BadRequestException
            allow(ErrorService).to receive(:perform_async)
            CPSCService.get_recalls_by_parameter("RecallTitle", "test")
            expect(ErrorService).to have_received(:perform_async)
        end
        it "sends message to New Relic" do
            url = "http://www.saferproducts.gov/RestWebServices/Recall?RecallTitle=test&format=json"
            stub_request(:get, URI.escape(url)).to_return(:status => 400, :body => "")
            allow(ErrorService).to receive(:perform_async)
            CPSCService.get_recalls_by_parameter("RecallTitle", "test")
            expect(ErrorService).to have_received(:perform_async)
        end
    end

    describe "#get_all_recalls" do
        it "returns recalls" do
            url = "http://www.saferproducts.gov/RestWebServices/Recall?format=json"
            stub_request(:get, URI.escape(url)).to_return(:status => 200, :body => "{\"name\":\"test recall\"}")
            recalls = CPSCService.get_all_recalls
            expect(recalls).to eq({"name"=>"test recall"})
        end
        it "sends message to New Relic" do
            url = "http://www.saferproducts.gov/RestWebServices/Recall?format=json"
            stub_request(:get, URI.escape(url)).to_return(:status => 200, :body => "{\"name\":\"test recall\"}")
            allow(JSON).to receive(:parse).and_raise BadRequestException
            allow(ErrorService).to receive(:perform_async)
            recalls = CPSCService.get_all_recalls
            expect(recalls).to eq([])
            expect(ErrorService).to have_received(:perform_async)
        end
        it "sends message to New Relic" do
            url = "http://www.saferproducts.gov/RestWebServices/Recall?format=json"
            stub_request(:get, URI.escape(url)).to_return(:status => 400)
            allow(ErrorService).to receive(:perform_async)
            recalls = CPSCService.get_all_recalls
            expect(recalls).to eq([])
            expect(ErrorService).to have_received(:perform_async)
        end
    end
end
