require 'rails_helper'

RSpec.describe CapitalExpenseReportPdfService, :type => :service do

    describe "init" do
        it "sets the binder, pdf and pdf font" do
            binder = create(:binder)
            user = create(:user, :role => "admin")

            service = CapitalExpenseReportPdfService.new(binder, user)

            expect(service.binder).to eq(binder)
            expect(service.user).to eq(user)
            expect(service.text).to eq("Capital Expense Report for")
            expect(service.pdf).to_not be(nil)
        end
    end

    describe "create" do
        it "creates pdf with receipts" do
            binder = create(:binder)
            user = create(:user, :role => "admin")
            create_list(:receipt, 10, :binder => binder)
            receipt = binder.receipts.first
            
            receipt.create_purchase
            service = CapitalExpenseReportPdfService.new(binder, user)
            
            allow(service).to receive(:add_logos)
            allow(service.pdf).to receive(:formatted_text_box)
            allow(service).to receive(:open).and_return("#{Rails.root}/public/img/flatlogonotext.png")
            
            service.create
            
            expect(service).to have_received(:add_logos)
        end
        
        it "creates pdf with no receipts" do
            binder = create(:binder)
            binder.property.purchase_price = 10000
            binder.property.save!
            user = create(:user, :role => "admin")
            service = CapitalExpenseReportPdfService.new(binder, user)
            
            allow(service.pdf).to receive(:image)
            allow(service.pdf).to receive(:formatted_text_box)
            
            service.create
        end
    end
end