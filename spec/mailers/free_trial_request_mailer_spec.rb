require 'rails_helper'

RSpec.describe FreeTrialRequestMailer, :type => :mailer do
    
    let(:user) { FactoryBot.create(:user) }

    describe "#notify_free_trial_request_mail" do
        it "It sends a the email" do
            params = {:user => {:name => user.user_profile.display_name, :phone => "+1-233-444-5555", :email => user.email}}
            mail = FreeTrialRequestMailer.new.notify_free_trial_request_mail(params[:user][:name], params[:user][:email], params[:user][:phone])
            expect(mail[:subject]).to eq("Mobile Free Trial Request")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq("support@homebinder.com")
            expect(mail[:metadata][:email_type]).to eq("notify_free_trial_request_mail")
            expect(mail[:global_merge_vars].first["content"]).to eq(user.user_profile.display_name)
            expect(mail[:global_merge_vars].second["content"]).to eq(user.email)
            expect(mail[:global_merge_vars].third["content"]).to eq("+1 (233) 444-5555")
        end
    end
    
end