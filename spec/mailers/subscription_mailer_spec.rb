require 'rails_helper'

RSpec.describe SubscriptionMailer, :type => :mailer do
    
    let(:user) { FactoryBot.create(:user) }
    let(:binder) { FactoryBot.create(:binder) }
    let(:plan) { OpenStruct.new({:object => OpenStruct.new({:plan => OpenStruct.new({:name => ""}), :total => 100})})}

    describe "#notify_customer_subscription" do
        it "It sends a new subscription email to support" do
            customer = {:name => "name", :email => "email", :id => 1, :phone => {:national => ""}}
            mail = SubscriptionMailer.new.notify_customer_subscription(customer)
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:subject]).to eq("New Customer Subscription")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:to].first[:email]).to eq("support@homebinder.com")
            expect(mail[:metadata][:email_type]).to eq("notify_customer_subscription")
        end
    end
    
    describe "#notify_homeowner_subscription" do
        it "It sends a new subscription email to support" do
            binder = create(:binder)
            user = create(:user, :role => "homeowner")
            create(:user_binder, :user => user, :binder => binder, :role => "owner")
            subscription = create(:subscription, :binder => binder)
            mail = SubscriptionMailer.new.notify_homeowner_subscription(subscription.id)
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:subject]).to eq("Thank you for upgrading to Homeowner Edition!")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:to].first[:email]).to eq(user.email)
            expect(mail[:metadata][:email_type]).to eq("notify_homeowner_subscription")
        end
    end
end