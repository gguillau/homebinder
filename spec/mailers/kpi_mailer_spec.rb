require 'rails_helper'

RSpec.describe KpiMailer, :type => :mailer do
    
    describe "#send_weekly_metrics" do
        it "sends an email to the partner" do
            user = create(:user, :role => "inspector")
            partner = create(:partner)
            PartnerUser.create(:partner_id => partner.id, :user_id => user.id, :role => "admin")
            metrics = {:user => user}

            mail = KpiMailer.new.send_weekly_metrics(partner, metrics)
            
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:subject]).to eq("HomeBinder Analytics Update For %s" % partner.name)
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:to].first[:email]).to eq(metrics[:user].email)
            expect(mail[:metadata][:email_type]).to eq("weekly_partner_metrics")
        end
        
        it "does not send an email if the partner admin does not exist" do
            user = create(:user, :role => "inspector")
            partner = create(:partner)
            PartnerUser.create(:partner_id => partner.id, :user_id => user.id, :role => "inspector")
            metrics = {}

            mail = KpiMailer.new.send_weekly_metrics(partner, metrics)
            
            expect(mail).to eq(nil)
        end
    end
    
    describe "#send_monthly_agent_metrics" do
        it "sends an email to the agent" do
            agent = create(:user, :role => "agent")

            mail = KpiMailer.new.send_monthly_agent_metrics(agent.id)
            
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:subject]).to eq("Monthly Metrics Update for HomeBinder Agents")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:to].first[:email]).to eq(agent.email)
            expect(mail[:metadata][:email_type]).to eq("monthly_agent_metrics")
        end
    end
    
    describe "#send_account_summary" do
        it "It sends an email to the user" do
            user = create(:user)
            mail = KpiMailer.new.send_account_summary(user, "")

            expect(mail[:to].first[:email]).to eq(user.email)
            expect(mail[:subject]).to eq("Partner Accounts Summary")
        end
    end
    
    describe "#send_kpi_metrics" do
        it "It sends an email to the user" do
            user = create(:user, :role => "admin")
            allow_any_instance_of(Kpi::Report).to receive(:get_report).and_return ""
            mail = KpiMailer.new.send_kpi_metrics(user)

            expect(mail[:to].first[:email]).to eq(user.email)
            expect(mail[:subject]).to eq("KPI Summary")
        end
    end
    
    describe "#format_referring_partners" do
        it "returns the contacts of the partners" do
            partner1 = create(:partner)
            partner2 = create(:partner)
            partners = [partner1, partner2]
            
            expect(KpiMailer.new.send(:format_referring_partners, partners)).to eq([partner1.contact, partner2.contact])
        end
        
        it "returns an empty array when the partners are empty" do
            partners = []
            expect(KpiMailer.new.send(:format_referring_partners, partners)).to eq([])
        end
    end
    
end