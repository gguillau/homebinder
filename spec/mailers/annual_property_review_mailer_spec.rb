require 'rails_helper'

RSpec.describe AnnualPropertyReviewMailer, :type => :mailer do
    
    describe "#new_client_apr" do
        it "It sends an email notification to apr client" do
            apr = create(:annual_property_review)
            mail = AnnualPropertyReviewMailer.new.new_client_apr(apr.id)

            expect(mail[:subject]).to eq("Annual Property Review Request for #{apr.address1} #{apr.city} #{apr.state}")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(apr.client_email)
            expect(mail[:metadata][:email_type]).to eq("apr_created")
        end
    end
    
    describe "#new_company_apr" do
        it "It sends an email notification to apr reviewer_company" do
            apr = create(:annual_property_review)
            mail = AnnualPropertyReviewMailer.new.new_company_apr(apr.id)

            expect(mail[:subject]).to eq("Annual Property Review Request for #{apr.address1} #{apr.city} #{apr.state}")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(apr.reviewer_company.email)
            expect(mail[:metadata][:email_type]).to eq("apr_created")
        end
    end
    
    describe "#new_support_apr" do
        it "It sends an email notification to HomeBinder" do
            apr = create(:annual_property_review)
            mail = AnnualPropertyReviewMailer.new.new_support_apr(apr.id)

            expect(mail[:subject]).to eq("Annual Property Review Request for #{apr.address1} #{apr.city} #{apr.state}")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq("support@homebinder.com")
            expect(mail[:metadata][:email_type]).to eq("apr_created")
        end
        it "It sends an email notification to HomeBinder with unassigned company" do
            apr = create(:annual_property_review, :reviewer_company_id => nil)
            mail = AnnualPropertyReviewMailer.new.new_support_apr(apr.id)

            expect(mail[:subject]).to eq("Annual Property Review Request for #{apr.address1} #{apr.city} #{apr.state}")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq("support@homebinder.com")
            expect(mail[:metadata][:email_type]).to eq("apr_created")
        end
    end
    
    describe "#scheduled" do
        it "It sends an email notification to apr client" do
            apr = create(:annual_property_review, :scheduled_for => Date.tomorrow)
            mail = AnnualPropertyReviewMailer.new.scheduled(apr.id)

            expect(mail[:subject]).to eq("APR Scheduled for #{apr.address1} #{apr.city} #{apr.state}")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(apr.client_email)
            expect(mail[:metadata][:email_type]).to eq("apr_scheduled")
        end
    end
    
    describe "#completed" do
        it "It sends an email notification to the apr client" do
            apr = create(:annual_property_review)
            mail = AnnualPropertyReviewMailer.new.completed(apr.id)

            expect(mail[:subject]).to eq("APR Report Completed for #{apr.address1} #{apr.city} #{apr.state}")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(apr.client_email)
            expect(mail[:metadata][:email_type]).to eq("apr_scheduled")
        end
    end
    
    describe "#canceled" do
        it "It sends an email notification to receiver" do
            apr = create(:annual_property_review)
            sender = {:name => "Sender", :email => "sender@test.com"}
            receiver = {:name => "Receiver", :email => "receiver@test.com"}
                mail = AnnualPropertyReviewMailer.new.canceled(apr.id, sender, receiver)

            expect(mail[:subject]).to eq("APR Report Canceled for #{apr.address1} #{apr.city} #{apr.state}")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq("receiver@test.com")
            expect(mail[:metadata][:email_type]).to eq("apr_canceled")
        end
    end
    
    describe "#on_hold" do
        it "It sends an email notification to receiver" do
            apr = create(:annual_property_review)
            sender = {:name => "Sender", :email => "sender@test.com"}
            receiver = {:name => "Receiver", :email => "receiver@test.com"}
                mail = AnnualPropertyReviewMailer.new.on_hold(apr.id, sender, receiver)

            expect(mail[:subject]).to eq("APR Report On Hold for #{apr.address1} #{apr.city} #{apr.state}")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq("receiver@test.com")
            expect(mail[:metadata][:email_type]).to eq("apr_on_hold")
        end
    end
    
    describe "#reminder" do
        it "It sends an email notification reminder to apr client" do
            apr = create(:annual_property_review, :scheduled_for => Date.tomorrow)
            mail = AnnualPropertyReviewMailer.new.reminder(apr.id)

            expect(mail[:subject]).to eq("APR Review Reminder for #{apr.address1} #{apr.city} #{apr.state}")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(apr.client_email)
            expect(mail[:metadata][:email_type]).to eq("apr_reminder")
        end
    end
    
    describe "#message_to_homeowner" do
        it "It sends an email notification message to apr client" do
            apr = create(:annual_property_review)
            mail = AnnualPropertyReviewMailer.new.message_to_homeowner(apr.id, "Hey!")

            expect(mail[:subject]).to eq("New Message from #{apr.reviewer_company.name}")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(apr.client_email)
            expect(mail[:metadata][:email_type]).to eq("apr_message")
        end
    end
    
    describe "#message_to_reviewer" do
        it "It sends an email notification message to apr reviewer_company" do
            apr = create(:annual_property_review)
            mail = AnnualPropertyReviewMailer.new.message_to_reviewer(apr.id, "Hey!")

            expect(mail[:subject]).to eq("New Message from #{apr.client_first_name}")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(apr.reviewer_company.email)
            expect(mail[:metadata][:email_type]).to eq("apr_message")
        end
    end
    
    describe "#request_quote" do
        it "It sends an email notification quote to apr reviewer_company" do
            Sidekiq::Testing.disable!
            apr = create(:annual_property_review)
            partner_contractor = create(:partner_contractor, email: "test@gmail.com")
            recommended_homepro = create(:contractor_template, partner_contractor: partner_contractor)
            
            mail = AnnualPropertyReviewMailer.new.request_quote(apr.id, recommended_homepro.id, "Hey!", [])
            
            expect(mail[:subject]).to eq("Quote Request for #{apr.address1} #{apr.city} #{apr.state}")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq("test@gmail.com")
            expect(mail[:metadata][:email_type]).to eq("apr_quote")
        end
    end
    
    describe "#send_full_report" do
        it "It sends pdf report to user" do
            allow_any_instance_of(AnnualPropertyReviews::AprPdfGenerator).to receive(:generate).and_return("")
            apr = create(:annual_property_review)
            user = create(:user)
            mail = AnnualPropertyReviewMailer.new.send_full_report(apr.id, user.id)

            expect(mail[:subject]).to eq("Annual Property Review Full PDF Report")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(user.email)
            expect(mail[:metadata][:email_type]).to eq("annual_property_review_full_report")
        end
        it "It sends pdf report to apr client" do
            allow_any_instance_of(AnnualPropertyReviews::AprPdfGenerator).to receive(:generate).and_return("")
            Sidekiq::Testing.disable!
            apr = create(:annual_property_review)
            mail = AnnualPropertyReviewMailer.new.send_full_report(apr.id, nil)

            expect(mail[:subject]).to eq("Annual Property Review Full PDF Report")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(apr.client_email)
            expect(mail[:metadata][:email_type]).to eq("annual_property_review_full_report")
        end
    end
end