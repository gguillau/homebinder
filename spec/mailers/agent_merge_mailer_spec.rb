require 'rails_helper'

RSpec.describe AgentMergeMailer, :type => :mailer do
    describe "#notify_agents" do
        it "sends notifications about the merge to both agents" do
            old_agent_data = {
                id: 1,
                email: "old-agent@test.com",
                first_name: "Old Agent"
            }
            new_agent_data = {
                id: 2,
                email: "new-agent@test.com",
                first_name: "New Agent"
            }
            merger = create(:user, :role => "admin")

            mail = AgentMergeMailer.new.notify_agents(old_agent_data, new_agent_data, merger.id)

            expect(mail[:subject]).to eq("Your duplicate accounts have been merged")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq("new-agent@test.com")
            expect(mail[:metadata][:email_type]).to eq("agent_merge_email")
        end
    end
    
    describe "#support_notification" do
        it "sends a notification about the merge to HomeBinder support" do
            old_agent_data = {
                id: 1,
                email: "old-agent@test.com",
                first_name: "Old Agent"
            }
            new_agent_data = {
                id: 2,
                email: "new-agent@test.com",
                first_name: "New Agent",
                last_name: "Last Name"
            }
            merger = create(:user, :role => "admin")

            mail = AgentMergeMailer.new.support_notification(old_agent_data, new_agent_data, merger.id, [])

            expect(mail[:subject]).to eq("Agent Merge Alert: New Agent Last Name by HomeBinder admin")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq("support@homebinder.com")
            expect(mail[:metadata][:email_type]).to eq("agent_merge_support_email")
            expect(mail[:global_merge_vars].fourth["content"]).to eq([])
            expect(mail[:global_merge_vars].fifth["content"]).to eq(0)
        end
    end
    
    describe "#get_merger_data" do
        it "gets the merger data for an admin" do
            merger = create(:user, :role => "admin")

            result = AgentMergeMailer.new.get_merger_data(merger.id)

            expect(result[:user_id]).to eq(merger.id)
            expect(result[:partner_id]).to eq(nil)
            expect(result[:name]).to eq("HomeBinder admin")
            expect(result[:company]).to eq("https://www.homebinder.com")
            expect(result[:email]).to eq(merger.email)
        end
        
        it "gets the merger data for a partner" do
            merger = create(:user, :role => "partner")
            partner = create(:partner)
            create(:partner_user, :partner => partner, :user => merger)

            result = AgentMergeMailer.new.get_merger_data(merger.id)

            expect(result[:user_id]).to eq(merger.id)
            expect(result[:partner_id]).to eq(partner.id)
            expect(result[:name]).to eq(partner.contact)
            expect(result[:company]).to eq(partner.name)
            expect(result[:email]).to eq(merger.email)
        end
    end
    
end