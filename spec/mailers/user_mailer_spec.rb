require 'rails_helper'

RSpec.describe UserMailer, :type => :mailer do

    let(:user) { FactoryBot.create(:user, email: "valid@homebinder.com") }
    let(:binders) { FactoryBot.create_list(:binder, 10) }
    
    describe "#send_reset_password_instructions" do
        it "It sends the password reset email" do
            mail = UserMailer.new.send_reset_password_instructions(user.id, "token")

            expect(mail[:subject]).to eq("HomeBinder Password Reset")
        end
    end
    
    describe "#send_invitation" do
        it "It sends the invitation email for admin" do
            admin = create(:user, role: 'admin')
            mail = UserMailer.new.send_invitation(admin.id)

            expect(mail[:subject]).to eq("Welcome to HomeBinder!")
        end
        
        it "It sends the invitation email for inspector" do
            inspector = create(:user, role: 'inspector')
            mail = UserMailer.new.send_invitation(inspector.id)

            expect(mail[:subject]).to eq("Welcome to HomeBinder!")
        end
        
        it "It sends the invitation email for inspector associated with partner" do
            inspector = create(:user, role: 'inspector')
            create(:partner_user, user: inspector)
            mail = UserMailer.new.send_invitation(inspector.id)

            expect(mail[:subject]).to eq("Welcome to HomeBinder!")
        end
        
        it "It sends the invitation email for home pro" do
            inspector = create(:user, role: 'inspector')
            homepro = create(:user, role: 'homepro', creator: inspector)
            mail = UserMailer.new.send_invitation(homepro.id)

            expect(mail[:subject]).to eq("Access your HomeBinder Profile")
        end
    end

    describe "#monthly_email" do
        it "It sends the monthly notification email" do
            binders.each do |binder|
                create(:user_binder, :binder_id => binder.id, :user_id => user.id, :role => "owner")
            end

            @binder = user.binders.first
            mail = UserMailer.new.monthly_email(user.id, @binder.id)

            expect(mail[:subject]).to eq("Your Monthly HomeBinder Summary for #{@binder.property.address1}")
        end
    end
    
    describe "#custom_support_email" do
        it "It sends the custom_support_email email" do
            binders.each do |binder|
                create(:user_binder, :binder_id => binder.id, :user_id => user.id, :role => "owner")
            end

            @binder = user.binders.first
            mail = UserMailer.new.custom_support_email(user.id, @binder.id)

            expect(mail[:subject]).to eq("A new easy way to add to your HomeBinder")
        end
    end

    describe "#notify_agent" do
        let(:agent) {create(:user, :role => "agent", :sign_in_count => 0)}
        let(:inspector){create(:user, :role => "inspector")}
        let(:partner){create(:partner, :partner_type => "inspector")}
        let!(:partner_user_1){create(:partner_user, :user => inspector, :partner => partner, :role => "admin")}
        let!(:partner_user_2){create(:partner_user, :user => agent, :partner => partner, :role => "agent")}

        it "It sends the profile invitation_email to the agent even when not added by partner" do
            new_agent = create(:user, :role => "agent", :sign_in_count => 0)
            mail = UserMailer.new.notify_agent(new_agent.id)
            
            expect(mail[:subject]).to eq("Access your HomeBinder Profile")
            expect(mail[:global_merge_vars][2]["content"]).to eq("The HomeBinder Team")
        end
        
        it "It sends the profile invitation_email to the agent" do

            partner.partner_configuration.send_agents_transfer_notification = "always"
            partner.save!

            mail = UserMailer.new.notify_agent(agent.id)

            expect(mail[:subject]).to eq("Access your HomeBinder Profile")
        end

        it "It sends a custom profile invitation_email to the agent" do

            partner.partner_configuration.send_agents_transfer_notification = "always"
            partner.save!

            mail = UserMailer.new.notify_agent(agent.id, nil, "Custom Agent Profile Invitation", partner.id)

            expect(mail[:subject]).to eq("Access your HomeBinder Profile")
            expect(mail[:global_merge_vars].third["content"]).to eq(partner.name)
        end
        
        it "It sends an email with content from invitee" do

            partner.partner_configuration.send_agents_transfer_notification = "always"
            partner.save!

            mail = UserMailer.new.notify_agent(agent.id, user.id)

            expect(mail[:subject]).to eq("Access your HomeBinder Profile")
            expect(mail[:global_merge_vars].third["content"]).to eq(user.user_profile.company)
        end

        it "sends an error to Newrelic" do
            allow(ErrorService).to receive(:perform_async)
            # mock the mandrill api so we don't actually send an email
            mandrill = double("Mandrill::API", :messages => OpenStruct.new({:send => true, :send_template => true}))
            allow(Mandrill::API).to receive(:new).with(MandrillService.new.password).and_return(mandrill)
            allow(mandrill.messages).to receive(:send_template).and_raise(BadRequestException)

            partner.partner_configuration.send_agents_transfer_notification = "always"
            partner.save!

            UserMailer.new.notify_agent(agent.id)

            expect(ErrorService).to have_received(:perform_async)
        end
    end

end