require 'rails_helper'

RSpec.describe RecallMailer, :type => :mailer do
    let(:user) { FactoryBot.create(:user) }
    let(:binder) { FactoryBot.create(:binder) }
    let(:appliance) { FactoryBot.create(:appliance, binder: binder) }
    let(:inspector_user) { FactoryBot.create(:user, :role => "inspector") }
    let(:agent) { FactoryBot.create(:user, :role => "agent") }

    describe "#notify_of_recalls" do
        it "It sends a recall notification email" do
            mail = RecallMailer.new.notify_of_recalls(appliance.id, user.id, "link", "confirmed_recall")

            expect(mail[:subject]).to eq("Recall identified on appliance at #{binder.full_address}")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(user.email)
            expect(mail[:metadata][:email_type]).to eq("recall_email")
            expect(mail[:global_merge_vars][0]["content"]).to eq(user.user_profile.display_name)
            expect(mail[:global_merge_vars][1]["content"]["id"]).to eq(appliance.id)
            expect(mail[:global_merge_vars][2]["content"]).to eq("link")
        end
        it "It sends a notification email with branding users" do
            create(:binder_branding, :binder_id => binder.id, :user_branding_id => inspector_user.user_profile.id, :scope => "recall_email")
            create(:binder_branding, :binder_id => binder.id, :user_branding_id => agent.user_profile.id, :scope => "recall_email")
            mail = RecallMailer.new.notify_of_recalls(appliance.id, user.id, "link", "potential_recall")

            expect(mail[:subject]).to eq("Recall identified on appliance at #{binder.full_address}")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(user.email)
            expect(mail[:metadata][:email_type]).to eq("recall_email")
            expect(mail[:global_merge_vars][0]["content"]).to eq(user.user_profile.display_name)
            expect(mail[:global_merge_vars][1]["content"]["id"]).to eq(appliance.id)
            expect(mail[:global_merge_vars][2]["content"]).to eq("link")
            expect(mail[:global_merge_vars][4]["content"][:email]).to eq(inspector_user.email)
        end
    end
end
