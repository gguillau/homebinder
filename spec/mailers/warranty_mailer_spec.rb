require 'rails_helper'

RSpec.describe WarrantyMailer, :type => :mailer do
    describe "#send_warranty_report" do
        it "It sends a warranty report to a user" do
            user = create(:user)
            mail = WarrantyMailer.new.send_warranty_report(user, "")
            expect(mail[:subject]).to eq("45 Day Warranty Report")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(user.email)
            expect(mail[:metadata][:email_type]).to eq("warranty_report")
            expect(mail[:attachments].length).to eq(1)
        end
        it "it sends an error" do
            allow(ErrorService).to receive(:perform_async)
            # mock the mandrill api so we don't actually send an email
            mandrill = double("Mandrill::API", :messages => OpenStruct.new({:send => true, :send_template => true}))
            allow(Mandrill::API).to receive(:new).with(MandrillService.new.password).and_return(mandrill)
            allow(mandrill.messages).to receive(:send).and_raise(BadRequestException)
            user = create(:user)
            WarrantyMailer.new.send_warranty_report(user, "")
            expect(ErrorService).to have_received(:perform_async)
        end
    end

    describe "#send_warranty_request" do
        it "It sends a warranty request to support" do
            partner = create(:partner)
            mail = WarrantyMailer.new.send_warranty_request(partner)
            expect(mail[:subject]).to eq("Partner Warranty Account Request")
            expect(mail[:from_name]).to eq(partner.name)
            expect(mail[:from_email]).to eq(partner.email)
            expect(mail[:to].first[:email]).to eq("support@homebinder.com")
            expect(mail[:metadata][:email_type]).to eq("warranty_request")
            expect(mail[:global_merge_vars].first["content"]["id"]).to eq(partner.id)
        end
    end
end
