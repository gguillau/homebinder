require 'rails_helper'

RSpec.describe MandrillMailer, :type => :mailer do

    describe "#send_email" do
        it "returns email not sent" do
            message = MandrillMailer.new.send_email({:to => [{:email => "orphans@homebinder.com"}]})
            expect(message).to eq("email not sent")
        end
        
        it "It doesn't send the email" do
            stub_const('ENV', ENV.to_hash.merge('DISABLE_EMAILS' => 'true'))

            message = MandrillMailer.new.send_email({:to => [{:email => "test@homebinder.com"}]})
            expect(message).to eq("emails are disabled")
        end
    end
    
    describe "#send_template_email" do
        it "returns email not sent" do
            message = MandrillMailer.new.send_template_email({:to => [{:email => "orphans@homebinder.com"}]})
            expect(message).to eq("email not sent")
        end
        
        it "It doesn't send the email" do
            stub_const('ENV', ENV.to_hash.merge('DISABLE_EMAILS' => 'true'))

            message = MandrillMailer.new.send_template_email({:to => [{:email => "test@homebinder.com"}]})
            expect(message).to eq("emails are disabled")
        end
    end
end