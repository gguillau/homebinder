require 'rails_helper'

RSpec.describe BinderMailer, :type => :mailer do
    
    describe "#notify_of_unverified_items" do
        it "It sends a notification email" do
            user = create(:user)
            binder = create(:binder)
            create(:user_binder, :binder => binder, :user => user, :role => "owner")
            appliance = create(:appliance, :binder => binder)
            create(:binder_item, :binder => binder, :appliance => appliance)

            mail = BinderMailer.new.notify_of_unverified_items(binder)

            expect(mail[:subject]).to eq("Please verify - 1 item(s) added to your HomeBinder!")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(user.email)
            expect(mail[:metadata][:email_type]).to eq("binder_items")
        end
    end

    describe "#inventory_report" do
        it "It sends a inventory_report report to a user" do
            user = create(:user)
            binder = create(:binder)
            allow_any_instance_of(InventoryReportPdfService).to receive(:create).and_return ""
            mail = BinderMailer.new.inventory_report(binder.id, user.id)
            expect(mail[:subject]).to eq("Inventory PDF Report for #{binder.full_address}")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(user.email)
            expect(mail[:metadata][:email_type]).to eq("inventory_report")
            expect(mail[:attachments].length).to eq(1)
        end
    end

    describe "#capital_expense_report" do
        it "It sends a capital_expense report to a user" do
            user = create(:user)
            binder = create(:binder)
            allow_any_instance_of(CapitalExpenseReportPdfService).to receive(:create).and_return ""
            mail = BinderMailer.new.capital_expense_report(binder.id, user.id)
            expect(mail[:subject]).to eq("Capital Expense PDF Report for #{binder.full_address}")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(user.email)
            expect(mail[:metadata][:email_type]).to eq("capital_expense_report")
            expect(mail[:attachments].length).to eq(1)
        end
    end

    describe "#home_pro_work_request" do
        it "It sends a notification email" do
            user = create(:user)
            binder = create(:binder)
            create(:user_binder, :binder => binder, :user => user, :role => "owner")
            appliance = create(:appliance, :binder => binder)
            create(:binder_item, :binder => binder, :appliance => appliance)
            partner = create(:partner)
            pro = create(:contractor)

            mail = BinderMailer.new.home_pro_work_request(user, binder, pro, partner, "text")

            expect(mail[:subject]).to eq("#{user.user_profile.display_name} requesting your services")
            expect(mail[:from_name]).to eq(partner.name)
            expect(mail[:from_email]).to eq(partner.email)
            expect(mail[:to].first[:email]).to eq(pro.email)
            expect(mail[:metadata][:email_type]).to eq("home_pro_work_request")
        end
    end

    describe "#home_pro_work_request_confirmation" do
        it "It sends a notification email" do
            user = create(:user)
            binder = create(:binder)
            create(:user_binder, :binder => binder, :user => user, :role => "owner")
            appliance = create(:appliance, :binder => binder)
            create(:binder_item, :binder => binder, :appliance => appliance)
            partner = create(:partner)
            pro = create(:contractor)

            mail = BinderMailer.new.home_pro_work_request_confirmation(user, binder, pro, partner, "text")

            expect(mail[:subject]).to eq("#{pro.types.pluck(:name).first} Contacted On Your Behalf")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(user.email)
            expect(mail[:metadata][:email_type]).to eq("home_pro_work_request_confirmation")
        end
    end

    describe "#home_pro_work_request_support" do
        it "It sends a notification email" do
            user = create(:user)
            binder = create(:binder)
            create(:user_binder, :binder => binder, :user => user, :role => "owner")
            appliance = create(:appliance, :binder => binder)
            create(:binder_item, :binder => binder, :appliance => appliance)
            partner = create(:partner)
            pro = create(:contractor)

            mail = BinderMailer.new.home_pro_work_request_support(user, binder, pro, partner, "text")

            expect(mail[:subject]).to eq("Action Required: HO Pro Request for #{binder.full_address}")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(mail[:from_email])
            expect(mail[:metadata][:email_type]).to eq("home_pro_work_request_support")
        end
    end
end
