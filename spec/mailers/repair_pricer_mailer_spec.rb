require 'rails_helper'

RSpec.describe RepairPricerMailer, :type => :mailer do

    let(:user) { FactoryBot.create(:user, email: "valid@homebinder.com") }
    let(:binders) { FactoryBot.create_list(:binder, 10) }
    
    describe "#send_repair_pricer_confirmation" do
        it "sends a confirmation email" do
            binders.each do |binder|
                create(:user_binder, :binder_id => binder.id, :user_id => user.id, :role => "owner")
            end
            
            @binder = user.binders.first
            mail = RepairPricerMailer.new.send_repair_pricer_confirmation(user, @binder)

            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:subject]).to eq("Cost Estimate Report Ordered")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:to].first[:email]).to eq("valid@homebinder.com")
        end
    end
    
    describe "#send_inspector_notification" do
        it "sends a notification email to the partner" do
            binders.each do |binder|
                create(:user_binder, :binder_id => binder.id, :user_id => user.id, :role => "owner")
            end
            
            partner = create(:partner)
            @binder = user.binders.first
            mail = RepairPricerMailer.new.send_inspector_notification(user, partner, @binder)

            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:subject]).to eq("Prepaid Repair Pricer Report Ordered")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:to].first[:email]).to eq(partner.email)
        end
    end

    describe "#find_a_pro" do
        it "It sends an email when there's a partner" do       
            repair_pricer_report = create(:repair_pricer_report, client_first: "Jack", client_email:"valid@homebinder.com")
            create(:repair_pricer_report_finding, repair_pricer_report: repair_pricer_report)
            mail = RepairPricerMailer.new.find_a_pro(repair_pricer_report)

            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:subject]).to eq("Take Action on Home Improvements Identified During Your Inspection")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:to].first[:email]).to eq("valid@homebinder.com")
        end
        
        it "It sends an email when there's no partner" do       
            repair_pricer_report = create(:repair_pricer_report, partner: nil, client_email:"valid@homebinder.com")
            mail = RepairPricerMailer.new.find_a_pro(repair_pricer_report)

            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:subject]).to eq("Take Action on Home Improvements Identified During Your Inspection")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:to].first[:email]).to eq("valid@homebinder.com")
        end
    end
    
end