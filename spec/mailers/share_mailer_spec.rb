require 'rails_helper'

RSpec.describe ShareMailer, :type => :mailer do
    
    let(:from) { FactoryBot.create(:user, :role => "inspector") }
    let(:with) { FactoryBot.create(:user) }
    let(:binder) { FactoryBot.create(:binder) }
    let(:user) { FactoryBot.create(:user, :role => "inspector") }
    let(:partner) { FactoryBot.create(:partner) }
    let(:broker) { FactoryBot.create(:partner) }
    let(:share) {FactoryBot.create(:share, :binder_id => binder.id, :shared_with_id => with.id, :shared_by_id => from.id, :role_name => "co_owner", :status => "created")}
    let!(:branding) {FactoryBot.create(:binder_branding, :scope => "share_email", :user_branding_id => from.user_profile.id, :binder => binder) }
    
    before :each do
        user.add_role :partner_admin, partner
    end

    describe "#notify_email" do
        it "It sends an email" do
            mail = ShareMailer.new.notify_email(share.id)

            expect(mail[:to].first[:email]).to eq(with.email)
            expect(mail[:from_email]).to eq(from.email)
            expect(mail[:subject]).to eq("Homebinder.com Share Notification")
        end
    end
    
    describe "#notify_email_from_inspector" do
        it "It sends an email" do
            mail = ShareMailer.new.notify_email_from_inspector(share.id, partner.id)

            expect(mail[:to].first[:email]).to eq(with.email)
            expect(mail[:from_email]).to eq(partner.email)
            expect(mail[:subject]).to eq("Last step in your home inspection for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
    end
    
    describe "#notify_email_from_broker" do
        it "It sends an email" do
            mail = ShareMailer.new.notify_email_from_broker(share.id, broker.id)

            expect(mail[:to].first[:email]).to eq(with.email)
            expect(mail[:from_email]).to eq(broker.email)
            expect(mail[:subject]).to eq("HomeBinder for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
    end
    
    describe "#notify_email_from_partner" do
        it "It sends an email" do
            mail = ShareMailer.new.notify_email_from_partner(share.id, broker.id)

            expect(mail[:to].first[:email]).to eq(with.email)
            expect(mail[:from_email]).to eq(broker.email)
            expect(mail[:subject]).to eq("HomeBinder for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
    end
    
end