require 'rails_helper'

RSpec.describe MaintenanceNotifyMailer, :type => :mailer do
    
    let(:user) { FactoryBot.create(:user) }
    let(:binder) { FactoryBot.create(:binder) }
    let(:item){FactoryBot.create(:maintenance_item)}
    let(:event){FactoryBot.create(:maintenance_event, :maintenance_item_id => item.id)}
    let(:inspector_user) { FactoryBot.create(:user, :role => "inspector") }
    let(:partner) { FactoryBot.create(:partner) }
    let(:config) {partner.partner_configuration}
    let(:agent) { FactoryBot.create(:user, :role => "agent") }
    
    describe "#notify_email" do
        it "It sends a notification email" do
            mail = MaintenanceNotifyMailer.new.notify_email(event, binder, user, item.do_date)
            expect(mail[:subject]).to eq("#{item.name.titleize} Maintenance Due (#{binder.full_address})")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(user.email)
            expect(mail[:metadata][:email_type]).to eq("maintenance_reminder")
            expect(mail[:global_merge_vars].first["content"]).to eq(user.user_profile.display_name)
            expect(mail[:global_merge_vars][4]["content"]).to eq(false)
        end
        it "It sends a notification email for filter" do
            item = create(:maintenance_item, :name => "HVAC Filter")
            event.maintenance_item_id = item.id
            event.save!
            
            mail = MaintenanceNotifyMailer.new.notify_email(event, binder, user, item.do_date)
            
            expect(mail[:subject]).to eq("#{item.name.titleize} Maintenance Due (#{binder.full_address})")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(user.email)
            expect(mail[:metadata][:email_type]).to eq("maintenance_reminder")
            expect(mail[:global_merge_vars].first["content"]).to eq(user.user_profile.display_name)
            expect(mail[:global_merge_vars][4]["content"]).to eq(true)
        end
        it "It sends a notification email with branding users" do
            create(:binder_branding, :binder_id => binder.id, :user_branding_id => inspector_user.user_profile.id, :scope => "maintenance_email")
            create(:binder_branding, :binder_id => binder.id, :user_branding_id => agent.user_profile.id, :scope => "maintenance_email")
            mail = MaintenanceNotifyMailer.new.notify_email(event, binder, user, item.do_date)
            
            expect(mail[:subject]).to eq("#{item.name.titleize} Maintenance Due (#{binder.full_address})")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(user.email)
            expect(mail[:metadata][:email_type]).to eq("maintenance_reminder")
            expect(mail[:global_merge_vars][6]["content"]).to eq("/binders/#{binder.id}/binder_contractors?proClicked=true&maintenanceEventId=#{event.id}")
            expect(mail[:global_merge_vars][7]["content"][:email]).to eq(inspector_user.email)
        end
    end
    
end