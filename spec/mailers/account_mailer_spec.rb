require 'rails_helper'

RSpec.describe AccountMailer, :type => :mailer do

    let(:user) { FactoryBot.create(:user) }
    let(:inspector) { FactoryBot.create(:partner) }
    let(:broker) { FactoryBot.create(:partner, :partner_type => "broker") }
    let(:binder) { FactoryBot.create(:binder) }
    let(:services) { ["Planning projects or getting quotes from qualified professionals in your area"] }
    let(:data) {
        {
            :employees => {:range => "100-500"},
            :inspections => {:range => "40-100"},
            :transactions => {:range => "40-100"},
            :years => {:range => "0-5"},
            :other => "Other",
            :otherSoftware => "otherSoftware",
            :comments => "Comment",
            :software => {"ISN" => true},
            :source => {"Google" => true}
        }
    }

    describe "#auto_generated_support_email" do
        it "It sends a notification email to support@homebinder.com when an inspector" do
            mail = AccountMailer.new.auto_generated_support_email(inspector.id, data)

            expect(mail[:subject]).to eq("New Partner Registration: Partner #{inspector.id}")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq("support@homebinder.com")
            expect(mail[:metadata][:email_type]).to eq("new_partner_registration")
            expect(mail[:global_merge_vars].first["content"]["id"]).to eq(inspector.id)
            expect(mail[:global_merge_vars].second["content"]).to be(nil)
        end
        it "It sends a notification email to support@homebinder.com when a broker" do
            allow(broker.logo).to receive(:present?).and_return(true)
            allow(broker.logo).to receive(:expiring_cloud_front_url).and_return("")

            mail = AccountMailer.new.auto_generated_support_email(broker.id, data)

            expect(mail[:subject]).to eq("New Partner Registration: Partner #{broker.id}")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq("support@homebinder.com")
            expect(mail[:metadata][:email_type]).to eq("new_partner_registration")
            expect(mail[:global_merge_vars].first["content"]["id"]).to eq(broker.id)
            expect(mail[:global_merge_vars].second["content"]).to eq(nil)
        end
    end
    
    describe "#apr_widget" do
        it "It sends a notification feedback email to partner" do
            mail = AccountMailer.new.apr_widget(user.id, binder.id, services)

            expect(mail[:subject]).to eq("More Info about APRs Requested for #{binder.full_address}")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq("support@homebinder.com")
            expect(mail[:metadata][:email_type]).to eq("apr_widget")
            expect(mail[:global_merge_vars].first["content"]).to eq(user.user_profile.display_name)
            expect(mail[:global_merge_vars].second["content"]).to eq(user.email)
            expect(mail[:global_merge_vars].third["content"]).to eq(user.user_profile.mobile_phone)
            expect(mail[:global_merge_vars].fourth["content"]).to eq(binder.full_address)
        end
    end

    describe "#send_partner_feedback" do
        it "It sends a notification feedback email to partner" do
            mail = AccountMailer.new.send_partner_feedback(user.id, inspector.email)

            expect(mail[:subject]).to eq("Client feedback alert from HomeBinder")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(inspector.email)
            expect(mail[:metadata][:email_type]).to eq("send_partner_feedback")
            expect(mail[:global_merge_vars].first["content"]).to eq(user.user_profile.display_name)
        end
    end
    
    describe "#secure24" do
        it "It sends an email to secure24" do
            mail = AccountMailer.new.secure24(user.id, inspector.id)

            expect(mail[:subject]).to eq("Secure24 Request from HomeBinder")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].length).to eq(2)
        end
    end

    describe "#send_services_requested" do
        it "It sends a notification feedback email to partner" do
            mail = AccountMailer.new.send_services_requested(user.id, binder.id, services, "support@homebinder.com", "Help with Projects")

            expect(mail[:subject]).to eq("Help with Projects Requested for #{binder.full_address}")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq("support@homebinder.com")
            expect(mail[:metadata][:email_type]).to eq("send_services_requested")
            expect(mail[:global_merge_vars].first["content"]).to eq(user.user_profile.display_name)
            expect(mail[:global_merge_vars].second["content"]).to eq(user.email)
            expect(mail[:global_merge_vars].third["content"]).to eq(user.user_profile.mobile_phone)
            expect(mail[:global_merge_vars].fourth["content"]).to eq(binder.full_address)
        end
    end

    describe "#send_failed_payment_notification" do
        it "It sends a notification email to the user" do
            user = create(:user)

            mail = AccountMailer.new.send_failed_payment_notification(user.id)

            expect(mail[:subject]).to eq("[Action Required] Please Update Your Billing Information")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(user.email)
            expect(mail[:metadata][:email_type]).to eq("payment_failed")
        end
    end
    
    describe "#batch_binder_upload_confirmation" do
        it "It sends a notification email to the user" do
            user = create(:user)

            mail = AccountMailer.new.batch_binder_upload_confirmation(user.id, 1, 1)

            expect(mail[:subject]).to eq("Batch Binder Upload Confirmation - 1 successfully uploaded")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq(user.email)
            expect(mail[:metadata][:email_type]).to eq("batch_binder_upload_confirmation")
        end
    end
    
    describe "#send_marketing_materials_notification" do
        it "it sends a notification email to partner" do
            partner = create(:partner, :email => "test@homebinder.com", :email_display_name => "Display Name")
            mail = AccountMailer.new.send_marketing_materials_notification(partner)

            expect(mail[:subject]).to eq("Custom Marketing Materials Available")
            expect(mail[:from_name]).to eq("HomeBinder Support")
            expect(mail[:from_email]).to eq("support@homebinder.com")
            expect(mail[:to].first[:email]).to eq("test@homebinder.com")
            expect(mail[:metadata][:email_type]).to eq("send_marketing_materials_notification")
            expect(mail[:global_merge_vars].first["content"]).to eq("Display Name")
        end
    end
end