require 'rails_helper'

RSpec.describe TransferMailer, :type => :mailer do
    let(:transfer_to) { FactoryBot.create(:user) }
    let(:transfer_by) { FactoryBot.create(:user, :role => "inspector") }
    let(:partner) { FactoryBot.create(:partner) }
    let(:binder) { FactoryBot.create(:binder) }
    let!(:partner_binder) { FactoryBot.create(:partner_binder, :binder => binder, :partner => partner, :user => transfer_to) }
    let(:agent) { FactoryBot.create(:user, :role => "agent") }
    let!(:user_binder) { FactoryBot.create(:user_binder, :binder => binder, :user => agent, :role => "buyer_agent") }
    let(:transfer) {FactoryBot.create(:transfer, :binder_id => binder.id, :receiver_id => transfer_to.id, :sender_id => transfer_by.id, :transfer_type => "ownership", :status => "created")}
    let!(:branding) {FactoryBot.create(:binder_branding, :scope => "transfer_email", :user_branding_id => agent.user_profile.id, :binder => binder) }

    before :each do
        transfer_by.add_role :partner_admin, partner
    end

    describe "#notify_email" do
        it "It sends a transfer email and bbc transfers@homebinder.com" do
            mail = TransferMailer.new.notify_email(nil, nil, transfer)

            expect(mail[:from_email]).to eq(transfer_by.email)
            expect(mail[:to].second[:email]).to eq("transfers@homebinder.com")
            expect(mail[:to].first[:email]).to eq(transfer_to.email)
            expect(mail[:subject]).to eq("HomeBinder for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
        it "It sends a transfer email with reminder as a subject" do
            mail = TransferMailer.new.notify_email(true, nil, transfer)
            expect(mail[:from_email]).to eq(transfer_by.email)
            expect(mail[:to].first[:email]).to eq(transfer_to.email)
            expect(mail[:subject]).to eq("Reminder! HomeBinder for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
        it "It sends a transfer email and cc's the transfer_by user" do
            mail = TransferMailer.new.notify_email(nil, true, transfer.id)
            expect(mail[:to].first[:email]).to eq(transfer_to.email)
            expect(mail[:to].second[:email]).to eq(transfer_by.email)
            expect(mail[:to].third[:email]).to eq("transfers@homebinder.com")
            expect(mail[:from_email]).to eq(transfer_by.email)
            expect(mail[:subject]).to eq("HomeBinder for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
    end

    describe "#notify_email_from_inspector" do
        it "It sends a transfer email and bbc transfers@homebinder.com" do
            mail = TransferMailer.new.notify_email_from_inspector(nil, nil, transfer.id, partner.id)

            expect(mail[:to].first[:email]).to eq(transfer_to.email)
            expect(mail[:to].second[:email]).to eq("transfers@homebinder.com")
            expect(mail[:from_email]).to eq(partner.email)
            expect(mail[:subject]).to eq("Last step in your home inspection for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
        it "It sends a transfer email with reminder as a subject" do
            mail = TransferMailer.new.notify_email_from_inspector(true, nil, transfer.id, partner.id)
            expect(mail[:to].first[:email]).to eq(transfer_to.email)
            expect(mail[:from_email]).to eq(partner.email)
            expect(mail[:subject]).to eq("Reminder! Last step in your home inspection for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
        it "It sends a transfer email with reminder as a subject and transaction_type is sell_side_inspection" do
            create(:binder_transaction, :binder => binder, :transaction_type => "sell_side_inspection")
            mail = TransferMailer.new.notify_email_from_inspector(true, nil, transfer.id, partner.id)
            expect(mail[:to].first[:email]).to eq(transfer_to.email)
            expect(mail[:from_email]).to eq(partner.email)
            expect(mail[:subject]).to eq("Reminder! Last step in your home inspection for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
        it "It sends a transfer email and cc's the transfer_by user" do
            mail = TransferMailer.new.notify_email_from_inspector(nil, true, transfer.id, partner.id)
            expect(mail[:to].first[:email]).to eq(transfer_to.email)
            expect(mail[:to].second[:email]).to eq(transfer_by.email)
            expect(mail[:to].third[:email]).to eq("transfers@homebinder.com")
            expect(mail[:from_email]).to eq(partner.email)
            expect(mail[:subject]).to eq("Last step in your home inspection for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
    end

    describe "#notify_email_from_partner" do
        it "It sends a transfer email and bbc transfers@homebinder.com" do
            mail = TransferMailer.new.notify_email_from_partner(nil, nil, transfer.id, partner.id)

            expect(mail[:to].first[:email]).to eq(transfer_to.email)
            expect(mail[:to].second[:email]).to eq("transfers@homebinder.com")
            expect(mail[:from_email]).to eq(partner.email)
            expect(mail[:subject]).to eq("HomeBinder for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
        it "It sends a transfer email with reminder as a subject" do
            mail = TransferMailer.new.notify_email_from_partner(true, nil, transfer.id, partner.id)
            expect(mail[:to].first[:email]).to eq(transfer_to.email)
            expect(mail[:from_email]).to eq(partner.email)
            expect(mail[:subject]).to eq("Reminder! HomeBinder for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
        it "It sends a transfer email with reminder as a subject and transaction_type is sell_side_inspection" do
            create(:binder_transaction, :binder => binder, :transaction_type => "sell_side_inspection")
            mail = TransferMailer.new.notify_email_from_partner(true, nil, transfer.id, partner.id)
            expect(mail[:to].first[:email]).to eq(transfer_to.email)
            expect(mail[:from_email]).to eq(partner.email)
            expect(mail[:subject]).to eq("Reminder! HomeBinder for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
        it "It sends a transfer email and cc's the transfer_by user" do
            mail = TransferMailer.new.notify_email_from_partner(nil, true, transfer.id, partner.id)
            expect(mail[:to].first[:email]).to eq(transfer_to.email)
            expect(mail[:to].second[:email]).to eq(transfer_by.email)
            expect(mail[:to].third[:email]).to eq("transfers@homebinder.com")
            expect(mail[:from_email]).to eq(partner.email)
            expect(mail[:subject]).to eq("HomeBinder for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
    end

    describe "#notify_email_from_broker" do
        it "It sends a transfer email and bbc transfers@homebinder.com" do
            mail = TransferMailer.new.notify_email_from_broker(nil, nil, transfer.id, partner.id)

            expect(mail[:to].first[:email]).to eq(transfer_to.email)
            expect(mail[:to].second[:email]).to eq("transfers@homebinder.com")
            expect(mail[:from_email]).to eq(partner.email)
            expect(mail[:subject]).to eq("HomeBinder for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
        it "It sends a transfer email with reminder as a subject" do
            mail = TransferMailer.new.notify_email_from_broker(true, nil, transfer.id, partner.id)
            expect(mail[:from_email]).to eq(partner.email)
            expect(mail[:subject]).to eq("Reminder! HomeBinder for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
        it "It sends a transfer email and cc's the transfer_by user" do
            mail = TransferMailer.new.notify_email_from_broker(nil, true, transfer.id, partner.id)
            expect(mail[:to].first[:email]).to eq(transfer_to.email)
            expect(mail[:to].second[:email]).to eq(transfer_by.email)
            expect(mail[:to].third[:email]).to eq("transfers@homebinder.com")
            expect(mail[:from_email]).to eq(partner.email)
            expect(mail[:subject]).to eq("HomeBinder for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
    end
    
    describe "#notify_email_from_hoa" do
        it "It sends a transfer email and bbc transfers@homebinder.com" do
            mail = TransferMailer.new.notify_email_from_hoa(nil, nil, transfer.id, partner.id)

            expect(mail[:to].first[:email]).to eq(transfer_to.email)
            expect(mail[:to].second[:email]).to eq("transfers@homebinder.com")
            expect(mail[:from_email]).to eq(partner.email)
            expect(mail[:subject]).to eq("HomeBinder for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
        it "It sends a transfer email with reminder as a subject" do
            mail = TransferMailer.new.notify_email_from_hoa(true, nil, transfer.id, partner.id)
            expect(mail[:from_email]).to eq(partner.email)
            expect(mail[:subject]).to eq("Reminder! HomeBinder for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
        it "It sends a transfer email and cc's the transfer_by user" do
            mail = TransferMailer.new.notify_email_from_hoa(nil, true, transfer.id, partner.id)
            expect(mail[:to].first[:email]).to eq(transfer_to.email)
            expect(mail[:to].second[:email]).to eq(transfer_by.email)
            expect(mail[:to].third[:email]).to eq("transfers@homebinder.com")
            expect(mail[:from_email]).to eq(partner.email)
            expect(mail[:subject]).to eq("HomeBinder for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
    end

    describe "#notify_email_to_agent" do
        it "It returns nil when partner_binders is empty" do
            allow(transfer_by.user_profile.logo).to receive(:present?).and_return(true)
            allow(transfer_by.user_profile.logo).to receive(:expiring_cloud_front_url).and_return("")
            
            binder.partner_binders = []
            mail = TransferMailer.new.notify_email_to_agent(agent.id, transfer.id, partner.id)

            expect(mail).to eq(nil)
        end
        it "It returns nil when binder is orphan" do
            allow(transfer_by.user_profile.logo).to receive(:present?).and_return(true)
            allow(transfer_by.user_profile.logo).to receive(:expiring_cloud_front_url).and_return("")
            
            binder.partner_binders = []
            binder.status = "orphan"
            mail = TransferMailer.new.notify_email_to_agent(agent.id, transfer.id, partner.id)

            expect(mail).to eq(nil)
        end
        it "It returns nil when homeowner is nil" do
            allow(transfer_by.user_profile.logo).to receive(:present?).and_return(true)
            allow(transfer_by.user_profile.logo).to receive(:expiring_cloud_front_url).and_return("")
            
            binder.partner_binders.first.user.destroy
            mail = TransferMailer.new.notify_email_to_agent(agent.id, transfer.id, partner.id)

            expect(mail).to eq(nil)
        end
        it "It sends an email to an agent" do
            allow(transfer_by.user_profile.logo).to receive(:present?).and_return(true)
            allow(transfer_by.user_profile.logo).to receive(:expiring_cloud_front_url).and_return("")
            
            mail = TransferMailer.new.notify_email_to_agent(agent.id, transfer.id, partner.id)

            expect(mail[:to].first[:email]).to eq(agent.email)
            expect(mail[:from_email]).to eq(transfer_by.email)
            expect(mail[:subject]).to eq("HomeBinder for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
        it "It sends an email to an agent when transaction_type is sell_side" do
            create(:binder_transaction, :binder => binder, :transaction_type => "sell_side_inspection")
            allow(transfer_by.user_profile.logo).to receive(:present?).and_return(true)
            allow(transfer_by.user_profile.logo).to receive(:expiring_cloud_front_url).and_return("")
            
            mail = TransferMailer.new.notify_email_to_agent(agent.id, transfer.id, partner.id)

            expect(mail[:to].first[:email]).to eq(agent.email)
            expect(mail[:from_email]).to eq(transfer_by.email)
            expect(mail[:subject]).to eq("HomeBinder for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
        it "It sets the proper subject if partner has pre-paid repair pricer and does not have access" do
            allow(transfer_by.user_profile.logo).to receive(:present?).and_return(true)
            allow(transfer_by.user_profile.logo).to receive(:expiring_cloud_front_url).and_return("")
            
            partner.partner_configuration.repair_pricer_pre_paid_reports = true
            UserBinder.where(:binder_id => binder.id, :role => "buyer_agent").update_all(:access_repair_pricer => false)
            mail = TransferMailer.new.notify_email_to_agent(agent.id, transfer.id, partner.id)

            expect(mail[:to].first[:email]).to eq(agent.email)
            expect(mail[:from_email]).to eq(transfer_by.email)
            expect(mail[:subject]).to eq("HomeBinder for #{binder.property.address1.titleize}, #{binder.property.city.titleize}, #{binder.property.state.upcase}")
        end
        it "It sets the proper subject if partner has pre-paid repair pricer and has access" do
            allow(transfer_by.user_profile.logo).to receive(:present?).and_return(true)
            allow(transfer_by.user_profile.logo).to receive(:expiring_cloud_front_url).and_return("")
            
            partner.partner_configuration.repair_pricer_pre_paid_reports = true
            partner.save!
            
            homeowner = binder.partner_binders.first.user
            client_name = homeowner.user_profile.display_name
            UserBinder.where(:binder_id => binder.id, :role => "buyer_agent").update_all(:access_repair_pricer => true)
            mail = TransferMailer.new.notify_email_to_agent(agent.id, transfer.id, partner.id)

            expect(mail[:to].first[:email]).to eq(agent.email)
            expect(mail[:from_email]).to eq(transfer_by.email)
            expect(mail[:subject]).to eq("#{transfer_by.user_profile.company} set up #{client_name} with a HomeBinder")
        end
    end
end
