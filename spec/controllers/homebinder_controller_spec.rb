require 'rails_helper'

RSpec.describe HomebinderController, :type => :controller do
    let(:user) { FactoryBot.create(:user, role: UserGlobalRoles::ADMIN) }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }
    let(:missing_version_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }
    let(:invalid_version_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt, 'HB-Version' => "1.1.1"} }
    let(:missing_api_session) { { 'HB-Version' => "0.0.0"} }
    let(:invalid_api_session) { { 'HB-APIKey' => "fakekey", 'HB-UserToken' => jwt} }
    let(:missing_token_session) { { 'HB-APIKey' => api_key.key} }
    let(:invalid_token_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => "faketoken"} }

    controller do
        def index
        end
    end

    context "handling rescue_from" do
        before :each do
            request.headers.merge!(valid_session) # Add to request headers
        end

        errors = OpenStruct.new({full_messages: ["test"]})
        resource = OpenStruct.new({errors: errors})
        errors = [
            {name: CanCan::AccessDenied, status: 403},
            {name: PermissionDeniedException, status: 403},
            {name: Paperclip::Error, status: 400},
            {name: ActiveRecord::RecordInvalid, status: 400},
            {name: ActiveRecord::RecordNotFound, status: 404},
            {name: SubscriptionError, status: 402},
            {name: NotFoundException, status: 404},
            {name: BadRequestException, status: 400},
            {name: UnprocessableException, status: 422, message: OpenStruct.new(resource)},
            {name: ActionController::ParameterMissing, status: 422, message: "Test"},
            {name: ActionController::UnpermittedParameters, status: 422, message: []},
            {name: ActiveRecord::ValueTooLong, status: 400, message: "Test"},
            {name: Rack::Timeout::RequestTimeoutException, status: 408, message: "Test"},
            {name: Rack::Timeout::RequestExpiryError, status: 408, message: "Test"},
            {name: Rack::Timeout::RequestTimeoutError, status: 408, message: "Test"},
            {name: ActiveRecord::StatementInvalid, status: 400},
            {name: ActiveModel::UnknownAttributeError.new(nil, nil), status: 400}
        ]
        errors.each do |error|
            it "raises an #{error[:name]} and returns #{error[:status]}" do
                allow(controller).to receive(:index).and_raise(error[:name], error[:message])
                get :index, params: nil
                expect(response).to have_http_status(error[:status])
            end
        end
    end

    describe "#verify_jwt_token" do
        context "no token" do
            before :each do
                request.headers.merge!(missing_token_session) # Add to request headers
            end
            it "renders error" do
                get :index, params: nil
                expect(response).to have_http_status(401)
            end
        end
        context "with invalid token" do
            before :each do
                request.headers.merge!(invalid_token_session) # Add to request headers
            end
            it "renders error" do
                get :index, params: nil
                expect(response).to have_http_status(401)
            end
        end
    end

    describe "#verify_api_key" do
        context "no api key" do
            before :each do
                request.headers.merge!(missing_api_session) # Add to request headers
            end
            it "renders error" do
                get :index, params: nil
                expect(response).to have_http_status(403)
            end
        end
        context "with invalid api key" do
            before :each do
                request.headers.merge!(invalid_api_session) # Add to request headers
            end
            it "renders error" do
                get :index, params: nil
                expect(response).to have_http_status(403)
            end
        end
    end
end
