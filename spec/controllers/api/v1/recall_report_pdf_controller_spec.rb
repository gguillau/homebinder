require 'rails_helper'

RSpec.describe Api::V1::RecallReportPdfController, :type => :controller do
    
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:user) { FactoryBot.create(:user, :role => "admin") }
    let(:binder) { FactoryBot.create(:binder) }
    let(:partner) {FactoryBot.create(:partner)}
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
  
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        service = RecallReportPdfService.new(binder)
        service.init
    
        allow_any_instance_of(RecallReportPdfService).to receive(:create).and_return(service.pdf)
    
        Tags::SetResourcesTagsJob.perform_async "Binder", binder.id, [{ tag: "partner_#{partner.id}"}]
        binder.save
        request.headers.merge!(valid_session) # Add to request headers
    end
    
    describe "GET #index" do
        context "creates the recall report" do
            before do
                get :index, params: {binder_id: binder.id}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end

end
