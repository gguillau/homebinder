require 'rails_helper'

RSpec.describe Api::V1::PendingUsersController, :type => :controller do
    
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:user) { FactoryBot.create(:user) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
  
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        request.headers.merge!(valid_session) # Add to request headers
    end
    
    describe "GET #get_waiting_binders_by_email" do
        context "reset the password for the user" do
            before do
                get :get_waiting_binders_by_email, params: {email: user.email}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "GET #get_waiting_binders_by_id" do
        context "reset the password for the user" do
            before do
                get :get_waiting_binders_by_id, params: {id: user.id}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "DELETE #optout" do
        context "reset the password for the user" do
            before do
                delete :optout, params: {id: user.id}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end

end
