require 'rails_helper'

RSpec.describe Api::V1::ReceiptsController, :type => :controller do

  let(:user) { FactoryBot.create(:user) }
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

  before :each do
    
    request.headers.merge!(valid_session) # Add to request headers
    
    @binder = create(:binder, name: "receipt tests", active: true, created_by: user.id)
    create(:subscription, binder_id: @binder.id, plan_id: 'free')
    UserBinder.create(:binder_id => @binder.id, :user_id => user.id, :role => "owner")
  end
  
  describe "GET #index" do
    context "get all receipts for a binder" do
      before do
        create(:receipt, name: "receipt a", binder_id: @binder.id)
        create(:receipt, name: "receipt b", binder_id: create(:binder).id)
        get :index, params: { binderId: @binder.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the list of receipts" do
        response_body = JSON.parse(response.body)
        expect(response_body["total"]).to eq 1
      end
    end
  end
  
  describe "GET #show" do
    context "get a single receipt" do
      before do
        @receipt = create(:receipt, name: "get me", binder_id: @binder.id)
        get :show, params: { id: @receipt.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the receipt" do
        receipt = JSON.parse(response.body)
        expect(receipt["id"]).to eq(@receipt.id)
      end
    end
  end
  
  describe "POST #create" do
    context "create an receipt" do
      before do
        post :create, params: { receipt: { binder_id: @binder.id, name: "new receipt" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "creates an receipt" do
        receipt = JSON.parse(response.body)
        expect(receipt["name"]).to eq("new receipt")
      end
    end
  end
  
  describe "PUT #update" do
    context "update an receipt" do
      before do
        receipt = create(:receipt, name: "created", binder_id: @binder.id)
        put :update, params: { id: receipt.id, receipt: { binder_id: @binder.id, name: "updated" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "updates an receipt" do
        receipt = JSON.parse(response.body)
        expect(receipt["name"]).to eq("updated")
      end
    end
  end
  
  describe "DELETE #destroy" do
    context "delete an receipt" do
      before do
        receipt = create(:receipt, name: "created", binder_id: @binder.id)
        delete :destroy, params: { id: receipt.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
    end
  end
  
end