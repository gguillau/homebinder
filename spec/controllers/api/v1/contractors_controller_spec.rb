require 'rails_helper'

RSpec.describe Api::V1::ContractorsController, :type => :controller do

    let(:user) { FactoryBot.create(:user) }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:contractor) {FactoryBot.create(:contractor, verified: true, created_by: user.id)}
    let(:file) { {version: "0.0.0"} }
  
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }
    
    before :each do
        request.headers.merge!(valid_session) # Add to request headers
    end
    
    describe "GET #index" do
        context "get all contractors for the user" do
            before do
                get :index, params: {}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
  
    describe "GET #show" do
        context "get a single contractor" do
            before do
                get :show, params:  { id: contractor.id }
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
                it "returns the contractor" do
                cont = JSON.parse(response.body)
                expect(cont["id"]).to eq(contractor.id)
            end
        end
    end

end