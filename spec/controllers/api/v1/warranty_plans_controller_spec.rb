require 'rails_helper'

RSpec.describe Api::V1::WarrantyPlansController, type: :controller do
    
    let(:user) { FactoryBot.create(:user, role: :admin) }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }
    
    before :each do
        request.headers.merge!(valid_session) # Add to request headers
    end
  
    describe "GET #index" do
        context "gets all warranty_plan" do
            before do
                get :index, params: {}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "POST #create" do
        context "create a warranty_plan" do
            before do
                post :create, params: {warranty_plan: {name: "TEST", price: 9, description: "description", cycle: "months", duration: 90, warranty_company_id: create(:warranty_company).id}}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "PUT #update" do
        context "update the warranty_plan" do
            before do
                warranty_plan = create(:warranty_plan)
                put :update, params: {id: warranty_plan.id , warranty_plan: {name: "name"}}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "DELETE #destroy" do
        context "deletes the warranty_plan" do
            before do
                delete :destroy, params: {id: 1}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
end
