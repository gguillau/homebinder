require 'rails_helper'

# these tests require AWS access. Not including them for now
RSpec.describe Api::V1::DocumentsController, :type => :controller do

  let(:user) { FactoryBot.create(:user) }
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

  before :each do
    
    request.headers.merge!(valid_session) # Add to request headers
    
    @binder = create(:binder, name: "document tests", active: true, created_by: user.id)
    create(:subscription, binder_id: @binder.id, plan_id: 'free')
    UserBinder.create(:binder_id => @binder.id, :user_id => user.id, :role => "owner")
  end
  
  describe "GET #index" do
    context "get all documents for a binder" do
      before do
        file = Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleDoc.pdf'), 'application/pdf')
        @doc1 = create(:document, binder_id: @binder.id, file: file)
        @doc2 = create(:document, binder_id: create(:binder).id, file: file)
        get :index, params:  { binderId: @binder.id }
      end
      
      after do
        @doc1.destroy
        @doc2.destroy
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the list of documents" do
        response_body = JSON.parse(response.body)
        expect(response_body["total"]).to eq 1
      end
    end
  end
  
  describe "GET #show" do
    context "get a single document" do
      before do
        file = Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleDoc.pdf'), 'application/pdf')
        @doc3 = create(:document, binder_id: @binder.id, file: file)
        get :show, params:  { id: @doc3.id }
      end
      
      after do
        @doc3.destroy
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the document" do
        doc = JSON.parse(response.body)
        expect(doc["id"]).to eq(@doc3.id)
      end
    end
  end
  
  describe "POST #create" do
    context "create a document" do
      before do
        post :create, params: { binder_id: @binder.id, file: fixture_file_upload("spec/assets/SampleDoc.pdf", "application/pdf")}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
    end
  end
  
  describe "POST #create from a document template" do
    context "create a document" do
      before do
        file = Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleDoc.pdf'), 'application/pdf')
        partner = create(:partner)
        config = partner.partner_configuration
        template = create(:binder_template, partner_configuration_id: config.id)
        dt = create(:document_template, binder_template_id: template.id, file: file)
        post :create, params: {binder_id: @binder.id, document: { document_template_id: dt.id, details: "hello"}}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
    end
  end
  
  
  describe "DELETE #destroy" do
    context "delete an document" do
      before do
        file = Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleDoc.pdf'), 'application/pdf')
        doc = create(:document, binder_id: @binder.id, file: file)
        delete :destroy, params: { id: doc.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:no_content)
      end
    end
  end
  
end