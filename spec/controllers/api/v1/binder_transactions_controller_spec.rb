require 'rails_helper'

RSpec.describe Api::V1::BinderTransactionsController, type: :controller do
    
    let(:model) {controller.controller_name.classify.constantize}
    let(:symbol) {controller.controller_name.singularize.to_sym}
    let(:user) { FactoryBot.create(:user, :role => "admin") }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    let!(:item) {create(symbol)}
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        allow(File).to receive(:read).and_call_original
        request.headers.merge!(valid_session) # Add to request headers
    end

    describe "GET #index" do
        context "get all binder_transactions" do
            before do
                get :index, params: {}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "returns the list of binder_transactions" do
                response_body = JSON.parse(response.body)
                expect(response_body["total"]).to eq 1
                expect(response_body["items"].length).to eq(1)
            end
        end
    end

    describe "GET #show" do
        context "get a single binder_transaction" do
            before do
                get :show, params:  { id: item.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "returns the binder_transaction" do
                binder_transaction = JSON.parse(response.body)
                expect(binder_transaction["id"]).to eq(item.id)
            end
        end
    end

    describe "POST #create" do
        context "create a binder_transaction" do
            before do
                binder = create(:binder)
                post :create, params:  { binder_transaction: { transaction_type: "buy_side_inspection", binder_id: binder.id }}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "creates a binder_transaction" do
                binder_transaction = JSON.parse(response.body)
                expect(binder_transaction["transaction_type"]).to eq("buy_side_inspection")
            end
        end
    end

    describe "DELETE #destroy" do
        context "delete a binder_transaction" do
            before do
                delete :destroy, params:  { id: item.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end

end
