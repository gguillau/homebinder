require 'rails_helper'

RSpec.describe Api::V1::BuildfaxController, :type => :controller do

  let(:user) { FactoryBot.create(:user) }
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

  before :each do
    
    request.headers.merge!(valid_session) # Add to request headers
    
    @binder = create(:binder, name: "permit tests", active: true, created_by: user.id)
    create(:subscription, binder_id: @binder.id, plan_id: 'free')
    UserBinder.create(:binder_id => @binder.id, :user_id => user.id, :role => "owner")
  end
  
  describe "GET #get_permits" do
    context "get all permits for a binder" do
      before do
        address = "#{@binder.property.address1}, #{@binder.property.city}, #{@binder.property.state} #{@binder.property.zip}"
        url = "https://delivery.buildfax.com/api/reports/addresses/%s/?output=xml&template=HomeBinder_Dev" % address
        body = "<?xml version='1.0' encoding='UTF-8'?>\n<buildFaxReport display=\"BuildFax report\">\n  <reportHeader display=\"report header\">\n    <serialNumber display=\"serial number\" type=\"string\">20170102193116787101-NIPSTY-98449823</serialNumber>\n    <version display=\"version\" type=\"integer\">20161223</version>\n  </reportHeader>\n  <reportBody display=\"report body\">\n    <requestedAddress display=\"requested addresss\">\n      <fullAddress display=\"full address\" type=\"string\">714 Bartoletti Trail, Blandaside, GA 10038-3082</fullAddress>\n    </requestedAddress>\n  </reportBody>\n</buildFaxReport>\n"
        stub_request(:get, URI.escape(url)).to_return(:status => 200, :body => body)
        get :get_permits, params:  { :binderId => @binder.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
    end
  end

  describe "POST #create" do
    context "create a permit" do
      before do
        post :create_permits, params: { :binderId => @binder.id, :permits => [{ :binder_id => @binder.id, :permit_number =>"03592", :contractors => ["Name"]}]}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "creates a permit" do
        permits = JSON.parse(response.body)
        expect(permits.first["permit_number"]).to eq("03592")
      end
    end
  end
  
end