require 'rails_helper'

RSpec.describe Api::V1::FinishesController, :type => :controller do

  let(:user) { FactoryBot.create(:user) }
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

  before :each do
    
    request.headers.merge!(valid_session) # Add to request headers
    
    @binder = create(:binder, name: "finish tests", active: true, created_by: user.id)
    create(:subscription, binder_id: @binder.id, plan_id: 'free')
    UserBinder.create(:binder_id => @binder.id, :user_id => user.id, :role => "owner")
  end
  
  describe "GET #index" do
    context "get all finishes for a binder" do
      before do
        create(:finish, name: "finish a", binder_id: @binder.id)
        create(:finish, name: "finish b", binder_id: create(:binder).id)
        get :index, params: { binderId: @binder.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the list of finishes" do
        response_body = JSON.parse(response.body)
        expect(response_body["total"]).to eq 1
      end
    end
  end
  
  describe "GET #show" do
    context "get a single finish" do
      before do
        @finish = create(:finish, name: "get me", binder_id: @binder.id)
        get :show, params: { id: @finish.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the finish" do
        finish = JSON.parse(response.body)
        expect(finish["id"]).to eq(@finish.id)
      end
    end
  end
  
  describe "POST #create" do
    context "create an finish" do
      before do
        post :create, params: { finish: { binder_id: @binder.id, name: "new finish" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "creates an finish" do
        finish = JSON.parse(response.body)
        expect(finish["name"]).to eq("new finish")
      end
    end
  end
  
  describe "PUT #update" do
    context "update an finish" do
      before do
        finish = create(:finish, name: "created", binder_id: @binder.id)
        put :update, params: { id: finish.id, finish: { binder_id: @binder.id, name: "updated" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "updates an finish" do
        finish = JSON.parse(response.body)
        expect(finish["name"]).to eq("updated")
      end
    end
  end
  
  describe "DELETE #destroy" do
    context "delete an finish" do
      before do
        finish = create(:finish, name: "created", binder_id: @binder.id)
        delete :destroy, params: { id: finish.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
    end
  end
  
end