require 'rails_helper'

RSpec.describe Api::V1::DashboardsController, :type => :controller do

  let(:user) { FactoryBot.create(:user, role: UserGlobalRoles::HOMEOWNER) }
  let(:admin) { FactoryBot.create(:user, role: UserGlobalRoles::ADMIN) }
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:admin_jwt) { UserTokenService.create_jwt(admin) }
  let(:file) { {version: "0.0.0"} }
  let(:valid_admin_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => admin_jwt} }
  let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

  context "when admin" do
      before :each do
      
      request.headers.merge!(valid_admin_session) # Add to request headers
    end
    
    describe "GET #owned_by_system" do
      context "get system dashboards" do
        before do
          create(:dashboard, user_id: user.id, system: false)
            get :owned_by_system, params: {}
        end
        it "responds with success" do
          expect(response).to have_http_status(:success)
        end
      end
    end
  end

  context "when homeowner" do
    before :each do
      
      request.headers.merge!(valid_session) # Add to request headers
    end
    
    describe "GET #index" do
      context "get all dashboards" do
        before do
          user.role = UserGlobalRoles::ADMIN
          user.save
            create(:dashboard, user_id: user.id, system: false)
          create(:dashboard)
          get :index
            user.role = UserGlobalRoles::HOMEOWNER
          user.save
        end
        it "responds with success" do
          expect(response).to have_http_status(:success)
        end
      end
    end
    
    describe "GET #owned_by_user" do
      context "get user dashboards" do
        before do
          create(:dashboard, user_id: user.id, system: false)
            get :owned_by_user, params: { id: user.id }
        end
        it "responds with success" do
          expect(response).to have_http_status(:success)
        end
      end
    end
    
    describe "GET #for_binder" do
      context "get user dashboards" do
        before do
          binder = create(:binder)
          create(:dashboard, binder_id: binder.id, system: true)
            get :for_binder, params: { id: binder.id }
        end
        it "responds with success" do
          expect(response).to have_http_status(:success)
        end
      end
    end
    
    describe "GET #owned_by_partner" do
      context "get partner dashboards" do
        before do
          user.role = "inspector"
          user.save
          @partner = create(:partner)
          create(:partner_user, partner_id: @partner.id, user_id: user.id)
          create(:dashboard, partner_id: @partner.id, system: false)
            get :owned_by_partner, params: { id: @partner.id }
        end
        it "responds with success" do
          expect(response).to have_http_status(:success)
        end
      end
    end
    
    describe "GET #owned_by_organization" do
      context "get organization dashboards" do
        before do
          org = create(:organization)
          create(:organization_user, organization_id: org.id, user_id: user.id)
          create(:dashboard, organization_id: org.id, system: false)
            get :owned_by_organization, params: { id: org.id }
        end
        it "responds with success" do
          expect(response).to have_http_status(:success)
        end
      end
    end
    
    describe "GET #show" do
      context "get a single dashboard" do
        before do
          @board = create(:dashboard, user_id: user.id, system: false)
          get :show, params: { id: @board.id }
        end
        it "responds with success" do
          expect(response).to have_http_status(:success)
        end
        it "returns the dashboard" do
          board = JSON.parse(response.body)
          expect(board["id"]).to eq(@board.id)
        end
      end
    end
   
    describe "POST #create" do
      context "create a dashboard" do
        before do
          post :create, params: { dashboard: { name: "board", user_id: user.id, system: false, scope: DashboardScope::BINDER }}
        end
        it "responds with success" do
          expect(response).to have_http_status(:success)
        end
        it "creates a dashboard" do
          board = JSON.parse(response.body)
          expect(board["name"]).to eq("board")
        end
      end
    end
    
    describe "PUT #update" do
      context "update a dashboard" do
        before do
          board = create(:dashboard, user_id: user.id, system: false, scope: DashboardScope::BINDER)
          put :update, params: { id: board.id, dashboard: { name: "board", user_id: user.id, system: false, scope: DashboardScope::BINDER }}
        end
        it "responds with success" do
          expect(response).to have_http_status(:success)
        end
        it "updates a dashboard" do
          board = JSON.parse(response.body)
          expect(board["name"]).to eq("board")
        end
      end
    end
    
    describe "DELETE #destroy" do
      context "delete a dashboard" do
        before do
          board = create(:dashboard, user_id: user.id, system: false, scope: DashboardScope::BINDER)
          delete :destroy, params: { id: board.id }
        end
        it "responds with success" do
          expect(response).to have_http_status(:no_content)
        end
      end
    end
  end
  
end