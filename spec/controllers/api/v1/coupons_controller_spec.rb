require 'rails_helper'

RSpec.describe Api::V1::CouponsController, :type => :controller do

    let(:user) { FactoryBot.create(:user, :role => "admin") }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }
    let(:partner) { FactoryBot.create(:partner) }
    let(:coupon) { FactoryBot.create(:coupon, partner_id: partner.id, code: "PARTNER#{partner.id}FREELIFE", duration: "forever") }
    
    before :each do
        allow(File).to receive(:read).and_call_original
        request.headers.merge!(valid_session) # Add to request headers
    end

    describe "GET #show" do
        context "get a single coupon" do
            it "responds with success" do
                allow(Stripe::Coupon).to receive(:retrieve).and_return(coupon)
                get :show, params:  { id: coupon.code }
                expect(response).to have_http_status(:success)
            end
            it "responds with card error" do
                error = Stripe::CardError.new("test error", {}, 404, http_status: nil, http_body: nil, json_body: {:error => {:message => "error"}})
                allow(Stripe::Coupon).to receive(:retrieve).and_raise(error)
                get :show, params:  { id: coupon.code }
                expect(response).to have_http_status(:bad_request)
            end
            it "responds with card error" do
                error = Stripe::InvalidRequestError.new("test error", {}, http_status: nil, http_body: nil, json_body: {:error => {:message => "error"}})
                allow(Stripe::Coupon).to receive(:retrieve).and_raise(error)
                get :show, params:  { id: coupon.code }
                expect(response).to have_http_status(:bad_request)
            end
            it "responds with card error" do
                error = Stripe::AuthenticationError.new("test error")
                allow(Stripe::Coupon).to receive(:retrieve).and_raise(error)
                get :show, params:  { id: coupon.code }
                expect(response).to have_http_status(:bad_request)
            end
            it "responds with card error" do
                error = Stripe::APIConnectionError.new("test error")
                allow(Stripe::Coupon).to receive(:retrieve).and_raise(error)
                get :show, params:  { id: coupon.code }
                expect(response).to have_http_status(:bad_request)
            end
            it "responds with card error" do
                error = Stripe::StripeError.new("test error")
                allow(Stripe::Coupon).to receive(:retrieve).and_raise(error)
                get :show, params:  { id: coupon.code }
                expect(response).to have_http_status(:bad_request)
            end
        end
    end
end