require 'rails_helper'

RSpec.describe Api::V1::RecallsController, :type => :controller do
    
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:admin) { FactoryBot.create(:user, :role => "admin") }
    let(:jwt) { UserTokenService.create_jwt(admin) }
    let(:binder) { FactoryBot.create(:binder)}
    let(:recall) { FactoryBot.create(:recall)}
    let(:subscription) {FactoryBot.create(:subscription, binder_id: binder.id, customer_id: "1", plan_id: "free")}
    let(:appliance) {FactoryBot.create(:appliance, binder_id: binder.id, created_by: admin.id)}
    let(:file) { {version: "0.0.0"} }
  
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        binder.subscription = subscription
        UserBinder.create(:binder_id => binder.id, :user_id => admin.id, :role => "owner")
        request.headers.merge!(valid_session) # Add to request headers
    end
    
    describe "GET #index" do
        context "gets the recalls" do
            before do
                get :index, params: {}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "PUT #update" do
        context "updates an recall" do
            before do
                put :update, params: {id: recall.id, recall: recall.attributes.symbolize_keys.except(:id)}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "DELETE #destroy" do
        context "deletes a recall" do
            before do
                delete :destroy, params: {id: recall.id}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
end
