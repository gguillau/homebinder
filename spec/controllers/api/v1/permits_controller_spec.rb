require 'rails_helper'

RSpec.describe Api::V1::PermitsController, :type => :controller do

  let(:user) { FactoryBot.create(:user) }
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

  before :each do
    
    request.headers.merge!(valid_session) # Add to request headers
    
    @binder = create(:binder, name: "permit tests", active: true, created_by: user.id)
    create(:subscription, binder_id: @binder.id, plan_id: 'free')
    UserBinder.create(:binder_id => @binder.id, :user_id => user.id, :role => "owner")
  end
  
  describe "GET #index" do
    context "get all permits for a binder" do
      before do
        create(:permit, binder_id: @binder.id)
        create(:permit, binder_id: create(:binder).id)
        get :index, params:  { binderId: @binder.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the list of permits" do
        response_body = JSON.parse(response.body)
        expect(response_body["total"]).to eq 1
      end
    end
  end
  
  describe "GET #show" do
    context "get a single permit" do
      before do
        @permit = create(:permit, binder_id: @binder.id)
        get :show, params:  { id: @permit.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the permit" do
        permit = JSON.parse(response.body)
        expect(permit["id"]).to eq(@permit.id)
      end
    end
  end
  
  describe "POST #create" do
    context "create a permit" do
      before do
        post :create, params: { permit: { :binder_id => @binder.id, :permit_number =>"03592" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "creates a permit" do
        permit = JSON.parse(response.body)
        expect(permit["permit_number"]).to eq("03592")
      end
    end
  end
  
  describe "PUT #update" do
    context "update a permit" do
      before do
        permit = create(:permit, binder_id: @binder.id)
        put :update, params:  { id: permit.id, permit: { binder_id: @binder.id, permit_number: "updated", binder_contractors: [] }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "updates a permit" do
        permit = JSON.parse(response.body)
        expect(permit["permit_number"]).to eq("updated")
      end
    end
  end
  
  describe "DELETE #destroy" do
    context "delete a permit" do
      before do
        permit = create(:permit, binder_id: @binder.id)
        delete :destroy, params:  { id: permit.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
    end
  end
  
end