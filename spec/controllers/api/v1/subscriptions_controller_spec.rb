require 'rails_helper'

RSpec.describe Api::V1::SubscriptionsController, :type => :controller do
    
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:admin) { FactoryBot.create(:user, :role => "admin") }
    let(:jwt) { UserTokenService.create_jwt(admin) }
    let(:binder) { FactoryBot.create(:binder) }
    let(:binder_2) { FactoryBot.create(:binder) }
    let(:subscription) {FactoryBot.create(:subscription, binder_id: binder.id)}
    let(:subscription_two) {FactoryBot.create(:subscription, binder_id: binder_2.id)}
    let!(:user_binder){FactoryBot.create(:user_binder, :binder => binder, :user => admin, :role => "owner")}
    let!(:user_binder){FactoryBot.create(:user_binder, :binder => binder_2, :user => admin, :role => "owner")}
    let(:file) { {version: "0.0.0"} }
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        binder.subscription = subscription
        binder.save!
        binder_2.subscription = subscription_two
        binder_2.save!
        allow(File).to receive(:read).and_call_original
        request.headers.merge!(valid_session) # Add to request headers
    end

    describe "GET #index" do
        context "get all subscriptions" do
            before do
                get :index, params: {binder_id: binder.id}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
        context "get all subscriptions for current user" do
            before do
                get :index, params: {user: true}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end

    describe "GET #show" do
        context "get a single subscription" do
            before do
                get :show, params:  { id: subscription.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end

    describe "PUT #update" do
        context "update a subscription" do
            it "responds with success" do
                put :update, params: { id: subscription.id, action: "update"}
                expect(response).to have_http_status(:success)
            end
                it "responds with card error" do
                error = Stripe::CardError.new("test error", {}, 404, http_status: nil, http_body: nil, json_body: {:error => {:message => "error"}})
                allow(SubscriptionService).to receive(:update_subscription).and_raise(error)
                put :update, params: { id: subscription.id, action: "update"}
                expect(response).to have_http_status(:unprocessable_entity)
            end
            it "responds with card error" do
                error = Stripe::InvalidRequestError.new("test error", {}, http_status: nil, http_body: nil, json_body: {:error => {:message => "error"}})
                allow(SubscriptionService).to receive(:update_subscription).and_raise(error)
                put :update, params: { id: subscription.id, action: "update"}
                expect(response).to have_http_status(:unprocessable_entity)
            end
            it "responds with card error" do
                error = Stripe::AuthenticationError.new("test error")
                allow(SubscriptionService).to receive(:update_subscription).and_raise(error)
                put :update, params: { id: subscription.id, action: "update"}
                expect(response).to have_http_status(:unprocessable_entity)
            end
            it "responds with card error" do
                error = Stripe::APIConnectionError.new("test error")
                allow(SubscriptionService).to receive(:update_subscription).and_raise(error)
                put :update, params: { id: subscription.id, action: "update"}
                expect(response).to have_http_status(:ok)
            end
            it "responds with card error" do
                error = Stripe::StripeError.new("test error")
                allow(SubscriptionService).to receive(:update_subscription).and_raise(error)
                put :update, params: { id: subscription.id, action: "update"}
                expect(response).to have_http_status(:ok)
            end
            it "responds with card error" do
                allow(SubscriptionService).to receive(:update_subscription).and_raise(CardRequiredException)
                put :update, params: { id: subscription.id, action: "update"}
                expect(response).to have_http_status(:unprocessable_entity)
            end
            it "responds with error" do
                allow(SubscriptionService).to receive(:update_subscription).and_raise(UnprocessableException, {})
                put :update, params: { id: subscription.id, action: "update"}
                expect(response).to have_http_status(:unprocessable_entity)
            end
        end
    end

    describe "DELETE #destroy" do
        context "delete a subscription" do
            before do
                delete :destroy, params:  { id: subscription_two.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
end
