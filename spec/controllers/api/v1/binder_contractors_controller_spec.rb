require 'rails_helper'

RSpec.describe Api::V1::BinderContractorsController, :type => :controller do

  let(:user) { FactoryBot.create(:user) }
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

  before :each do
    request.headers.merge!(valid_session) # Add to request headers
    
    @binder = create(:binder, name: "contractor tests", active: true, created_by: user.id)
    create(:subscription, binder_id: @binder.id, plan_id: 'free')
    UserBinder.create(:binder_id => @binder.id, :user_id => user.id, :role => "owner")
  end
  
  describe "GET #index" do
    context "get all contractors for a binder" do
      before do
        con1 = create(:contractor, name: "contractor 1", created_by: 12345)
        con2 = create(:contractor, name: "contractor 2", created_by: 54321)
        create(:binder_contractor, binder_id: @binder.id, contractor_id: con1.id)
        create(:binder_contractor, binder_id: create(:binder).id, contractor_id: con2.id)
        get :index, params:  { binderId: @binder.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the list of contractors" do
        response_body = JSON.parse(response.body)
        expect(response_body["total"]).to eq 1
      end
    end
  end
  
  describe "GET #show" do
    context "get a single contractor" do
      before do
        con1 = create(:contractor, name: "contractor 1", created_by: 12345)
        @bc = create(:binder_contractor, binder_id: @binder.id, contractor_id: con1.id)
        get :show, params:  { id: @bc.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the contractor" do
        con = JSON.parse(response.body)
        expect(con["id"]).to eq(@bc.id)
      end
    end
  end
  
  describe "POST #create" do
    context "create a contractor" do
      before do
        post :create, params:  { binder_contractor: { binder_id: @binder.id, types_attributes: [{name: SecureRandom.hex(4)}], contractor_attributes: { name: "new contractor", created_by: user.id, types_attributes: [{name: SecureRandom.hex(4)}] }}}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "creates an area" do
        con = JSON.parse(response.body)
        expect(con["contractor"]["name"]).to eq("New Contractor")
      end
    end
  end
  
  describe "PUT #update" do
    context "update a contractor" do
      before do
        @con1 = create(:contractor, name: "contractor 1", created_by: 12345)
        @bc = create(:binder_contractor, binder_id: @binder.id, contractor_id: @con1.id)
        create(:binder_contractor_type, :binder_contractor => @bc)
        put :update, params:  { id: @bc.id, binder_contractor: { binder_id: @binder.id}}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "updates a contractor" do
        con = JSON.parse(response.body)
        expect(con["contractor"]["name"]).to eq(@con1.name.titleize)
      end
    end
  end
  
  describe "DELETE #destroy" do
    context "delete an contractor" do
      before do
        con1 = create(:contractor, name: "contractor 1", created_by: 12345)
        @bc = create(:binder_contractor, binder_id: @binder.id, contractor_id: con1.id)
        delete :destroy, params:  { id: @bc.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
    end
  end
  
  describe "POST #contact" do
    context "sends a message" do
      before do
        con1 = create(:contractor, name: "contractor 1", created_by: 12345)
        bc = create(:binder_contractor, binder_id: @binder.id, contractor_id: con1.id)
        post :contact, params:  {:id => bc.id, :message => "Hello sir"}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
    end
  end
  
end