require 'rails_helper'

RSpec.describe Api::V1::KpiController, :type => :controller do

    let(:user) { FactoryBot.create(:user, :role => "admin") }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
  
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        request.headers.merge!(valid_session) # Add to request headers
    end
    
    describe "GET #get_binders" do
        context "Gets KPIs for binders" do
            before do
                get :get_binders, params: {}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "GET #get_summary" do
        context "gets KPIs for users" do
            before do
                get :get_summary, params: {}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "GET #get_engagement" do
        context "gets KPIs for users" do
            before do
                get :get_engagement, params: {:partner_type => "inspector"}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "GET #get_users" do
        context "gets KPIs for users" do
            before do
                get :get_users, params: {}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "GET #download" do
        context "downloads KPI report" do
            before do
                allow_any_instance_of(Kpi::Report).to receive(:get_report).and_return ""
                get :download, params: {}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "GET #account_summaries" do
        context "sends email with summary of partner accounts" do
            before do
                get :account_summaries, params: {}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
end