require 'rails_helper'

RSpec.describe Api::V1::BinderBrandingsController, :type => :controller do
    
    let(:model) {controller.controller_name.classify.constantize}
    let(:symbol) {controller.controller_name.singularize.to_sym}
    let(:user) { FactoryBot.create(:user, :role => "admin") }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    let!(:item) {create(symbol)}
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        allow(File).to receive(:read).and_call_original
        request.headers.merge!(valid_session) # Add to request headers
    end

    describe "GET #index" do
        context "get all brandings" do
            before do
                get :index, params: {}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "returns the list of accounts" do
                response_body = JSON.parse(response.body)
                expect(response_body["total"]).to eq 1
                expect(response_body["items"].length).to eq(1)
            end
        end
    end

    describe "GET #show" do
        context "get a single branding" do
            before do
                get :show, params:  { id: item.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "returns the account" do
                account = JSON.parse(response.body)
                expect(account["id"]).to eq(item.id)
            end
        end
    end

    describe "POST #create" do
        context "create a branding" do
            before do
                binder = create(:binder)
                user = create(:user, :role => "inspector")
                create(:user_profile, :user => user)
                post :create, params:  { binder_branding: { scope: "transfer_email", binder_id: binder.id, user_branding_id: user.user_profile.id }}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "creates a branding" do
                binder_branding = JSON.parse(response.body)
                expect(binder_branding["scope"]).to eq("transfer_email")
            end
        end
    end

    describe "DELETE #destroy" do
        context "delete an account" do
            before do
                delete :destroy, params:  { id: item.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
end
