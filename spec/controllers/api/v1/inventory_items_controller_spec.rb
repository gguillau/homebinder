require 'rails_helper'

RSpec.describe Api::V1::InventoryItemsController, :type => :controller do

  let(:user) { FactoryBot.create(:user) }
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

  before :each do
    
    request.headers.merge!(valid_session) # Add to request headers
    
    @binder = create(:binder, name: "inventory_item tests", active: true, created_by: user.id)
    create(:subscription, binder_id: @binder.id, plan_id: 'free')
    UserBinder.create(:binder_id => @binder.id, :user_id => user.id, :role => "owner")
  end
  
  describe "GET #index" do
    context "get all inventory_items for a binder" do
      before do
        create(:inventory_item, name: "inventory_item a", binder_id: @binder.id)
        create(:inventory_item, name: "inventory_item b", binder_id: create(:binder).id)
        get :index, params: { binderId: @binder.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the list of inventory_items" do
        response_body = JSON.parse(response.body)
        expect(response_body["total"]).to eq 1
      end
    end
  end
  
  describe "GET #show" do
    context "get a single inventory_item" do
      before do
        @inventory_item = create(:inventory_item, name: "get me", binder_id: @binder.id)
        get :show, params: { id: @inventory_item.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the inventory_item" do
        inventory_item = JSON.parse(response.body)
        expect(inventory_item["id"]).to eq(@inventory_item.id)
      end
    end
  end
  
  describe "POST #create" do
    context "create an inventory_item" do
      before do
        post :create, params: { inventory_item: { binder_id: @binder.id, name: "new inventory_item" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "creates an inventory_item" do
        inventory_item = JSON.parse(response.body)
        expect(inventory_item["name"]).to eq("new inventory_item")
      end
    end
  end
  
  describe "PUT #update" do
    context "update an inventory_item" do
      before do
        inventory_item = create(:inventory_item, name: "created", binder_id: @binder.id)
        put :update, params: { id: inventory_item.id, inventory_item: { binder_id: @binder.id, name: "updated" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "updates an inventory_item" do
        inventory_item = JSON.parse(response.body)
        expect(inventory_item["name"]).to eq("updated")
      end
    end
  end
  
  describe "DELETE #destroy" do
    context "delete an inventory_item" do
      before do
        inventory_item = create(:inventory_item, name: "created", binder_id: @binder.id)
        delete :destroy, params: { id: inventory_item.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
    end
  end
  
end