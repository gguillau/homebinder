require 'rails_helper'

RSpec.describe Api::V1::PartnersController, :type => :controller do
    let(:user) { FactoryBot.create(:user, :role => "inspector") }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    let(:partner){FactoryBot.create(:partner, :partner_key => "test")}
    let(:api_key) { FactoryBot.create(:api_key) }
    let!(:partner_api_key) { FactoryBot.create(:api_key, :key => "FAKE", :partner_id => partner.id) }
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        PartnerUser.create!(:role => "admin", :user_id => user.id, :partner_id => partner.id)
        allow(File).to receive(:read).and_call_original
        request.headers.merge!(valid_session) # Add to request headers
    end

    describe "GET #index" do
        context "get all partners" do
            before do
                get :index, params: {}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "returns the list of partners" do
                response_body = JSON.parse(response.body)
                expect(response_body["total"]).to eq 1
                expect(response_body["items"].length).to eq(1)
            end
        end
    end

    describe "GET #show" do
        context "get a single partner" do
            before do
                get :show, params:  { id: partner.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "returns the partner" do
                item = JSON.parse(response.body)
                expect(item["id"]).to eq(partner.id)
            end
        end
    end

    describe "POST #create" do
        context "create a partner and user" do
            before do
                new_user = {:email => "partner@gmail.com", :password => "password", :role => "inspector"}
                new_partner = {:name => "Test Company", phone: "+18002939293", email: "partner@gmail.com", contact: "John Smith", partner_type: "inspector", email_display_name: "Sample Home Inspector", code: "SAMPLE"}
                post :create, params:  { partner: new_partner, user: new_user}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end

    describe "PUT #update" do
        context "update a partner" do
            before do
                put :update, params: { id: partner.id, partner: { id: partner.id, email: "updated_email@test.com"}}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "updates a partner" do
                partner = JSON.parse(response.body)
                expect(partner["email"]).to eq("updated_email@test.com")
            end
        end
    end

    describe "DELETE #destroy" do
        context "delete a partner" do
            before do
                delete :destroy, params:  { id: partner.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "PUT #complete_onboarding" do
        context "update a partner" do
            before do
                object = {
                    email: partner.email
                }
                data = {:employees => {:range => "50+"}, :inspections => {:range => "1100+"},:source => {"Social Media"=> true}, :software => {"Other" => true, "Horizon" => true, "ISN" => true}}
                put :complete_onboarding, params: { id: partner.id, partner: {partner: object, data: data}}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "GET #automation" do
        context "gets a partner" do
            before do
                get :automation, params: { id: "test"}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "gets a partner" do
                item = JSON.parse(response.body)
                expect(item["name"]).to eq(partner.name)
            end
        end
    end
    
    describe "GET #show_apr_config" do
        context "gets a partner" do
            before do
                get :show_apr_config, params: { id: "test"}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "gets a partner" do
                item = JSON.parse(response.body)
                expect(item["name"]).to eq(partner.name)
            end
        end
    end

    describe "GET #enable_warranty_account" do
        it "returns success" do
            post :enable_warranty_account, params: {:id => partner.id}
            expect(response).to have_http_status(:no_content)
        end
    end

    describe "GET #footprints" do
        it "returns success" do
            get :footprints, params: {:id => partner.id}
            expect(response).to have_http_status(:no_content)
        end
    end

    describe "GET #analytics_overview" do
        it "returns success" do
            get :analytics_overview, params: {:partner_id => partner.id}
            expect(response).to have_http_status(:ok)
        end
    end
    
    describe "GET #analytics_logins" do
        it "returns success" do
            get :analytics_logins, params: {:partner_id => partner.id}
            expect(response).to have_http_status(:ok)
        end
    end
    
    describe "GET #analytics_reminders" do
        it "returns success" do
            get :analytics_reminders, params: {:partner_id => partner.id}
            expect(response).to have_http_status(:ok)
        end
    end
    
    describe "GET #analytics_agents" do
        it "returns success" do
            get :analytics_agents, params: {:partner_id => partner.id}
            expect(response).to have_http_status(:ok)
        end
    end
    
    describe "GET #download_reminders" do
        it "returns success" do
            get :download_reminders, params: {:partner_id => partner.id}
            expect(response).to have_http_status(:ok)
        end
    end
    
    describe "GET #download_logins" do
        it "returns success" do
            get :download_logins, params: {:partner_id => partner.id}
            expect(response).to have_http_status(:ok)
        end
    end
    
    describe "GET #widget_exclusions" do
        it "gets widget_exclusions" do
            get :widget_exclusions, params: {:id => partner.id}
            expect(response).to have_http_status(:ok)
        end
    end
    
    describe "GET #warranty_plans" do
        it "returns success" do
            get :warranty_plans, params: {:id => partner.id}
            expect(response).to have_http_status(:ok)
        end
    end
    
    describe "POST #import_users" do
        it "imports users" do
            post :import_users, params: {:id => partner.id, :users => [{"first_name" => "Mike"}], :role => "agent"}
            expect(response).to have_http_status(:ok)
        end
    end
    
    describe "PUT #update_role" do
        it "updates the user role" do
            put :update_role, params: {:id => partner.id, :user_id => user.id,:user => {:id => user.id, :role => "member"}}
            expect(response).to have_http_status(:ok)
        end
    end
end