require 'rails_helper'

RSpec.describe Api::V1::PartnerConfigurationsController, :type => :controller do
    
    let(:model) {controller.controller_name.classify.constantize}
    let(:symbol) {controller.controller_name.singularize.to_sym}
    let(:user) { FactoryBot.create(:user, :role => "admin") }
    let!(:partner) { FactoryBot.create(:partner) }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        allow(File).to receive(:read).and_call_original
        request.headers.merge!(valid_session) # Add to request headers
    end

    describe "GET #index" do
        context "get all configs" do
            before do
                get :index, params: {}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "returns the list of configs" do
                response_body = JSON.parse(response.body)
                expect(response_body["total"]).to eq 1
                expect(response_body["items"].length).to eq(1)
            end
        end
    end

    describe "GET #show" do
        context "get a single config" do
            before do
                get :show, params:  { id: partner.partner_configuration.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "returns the config" do
                config = JSON.parse(response.body)
                expect(config["id"]).to eq(partner.partner_configuration.id)
            end
        end
    end

    describe "PUT #update" do
        context "update a config" do
            before do
                put :update, params: { id: partner.partner_configuration.id, partner_configuration: {maintenance_note: "updated config"}}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "updates a config" do
                config = JSON.parse(response.body)
                expect(config["maintenance_note"]).to eq("updated config")
            end
        end
    end
end
