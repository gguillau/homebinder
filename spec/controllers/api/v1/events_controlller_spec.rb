require 'rails_helper'

RSpec.describe Api::V1::EventsController, :type => :controller do
    
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:user) { FactoryBot.create(:user) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
  
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        request.headers.merge!(valid_session) # Add to request headers
    end
    
    describe "POST #create" do
        context "posts an event" do
            before do
                post :create, params: {:event => {:event_name => "test"}}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end

end
