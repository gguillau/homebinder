require 'rails_helper'

RSpec.describe Api::V1::AutomationErrorsController, :type => :controller do
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:file) { {version: "0.0.0"} }
    let(:partner) { FactoryBot.create(:partner, name: "Test Inspections", contact: "Test Inspector", partner_key: "axium") }
    let(:admin) { FactoryBot.create(:user, :role => "admin") }
    let(:user) { FactoryBot.create(:user, email: partner.email, :role => "inspector") }
    let(:jwt) { UserTokenService.create_jwt(admin) }
    let(:partner_key) { FactoryBot.create(:api_key, partner_id: partner.id, contact_email: "test@gmail.com") }
    let(:config) {partner.partner_configuration}
    let(:template) { FactoryBot.create(:binder_template, partner_configuration_id: config.id) }
    let(:file) { {version: "0.0.0"} }
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        PartnerUser.create(:user_id => user.id, :partner_id => partner.id, :role => "admin")
        allow(File).to receive(:read).and_call_original
        request.headers.merge!(valid_session)
        allow(Partner::Automation::Error).to receive(:create)
    end

    describe "GET #index" do
        it "responds with success" do
            get :index, params: {:orderBy => "id"}
            expect(response).to have_http_status(:success)
        end
    end

    describe "PUT #update" do
        it "updates the request and creates the binder" do
            parameters = {
                :partner_name => "axium",
                :binder_template_id => template.id,
                :client_first => "Bob",
                :client_last => "Smith",
                :client_email => "bobsmith@gmail.com",
                :property_address => "321 Test Street",
                :property_address2 => "",
                :property_city => "Boston",
                :property_state => "MA",
                :property_postalcode => "02210",
                :property_country => "US",
                :agent_name => "Keller Williams",
                :agent_contact => "Carmen Smith",
                :agent_phone => "516-064-0219",
                :agent_email => "carmensmith@gmail.com"
            }
            error = Partner::Automation::Error.new(parameters)
            error.save

            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return({:key=>"axium", :partner_name=>nil, :binder_template_id=>template.id, :client=>{:first=>"Bob", :last=>"Smith", :email=>"bobsmith@gmail.com"}, :property=>{:address=>"321 Test Street", :address2=>"", :city=>"Boston", :state=>"MA", :country=>"US", :postalcode=>"02210"}, :agent=>{:name=>"Keller Williams", :contact=>"Carmen Smith", :phone=>"516-064-0219", :email=>"carmensmith@gmail.com"}, :method=>"api"})

            put :update, params:  {id: error.id, error: error.attributes}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "PUT #destroy" do
        it "destroys the error" do
            parameters = {
                :partner_name => "axium",
                :binder_template_id => template.id,
                :client_first => "Bob",
                :client_last => "Smith",
                :client_email => "bobsmith@gmail.com",
                :property_address => "321 Test Street",
                :property_address2 => "",
                :property_city => "Boston",
                :property_state => "MA",
                :property_postalcode => "02210",
                :property_country => "US",
                :agent_name => "Keller Williams",
                :agent_contact => "Carmen Smith",
                :agent_phone => "516-064-0219",
                :agent_email => "carmensmith@gmail.com"
            }
            error = Partner::Automation::Error.new(parameters)
            error.save
            delete :destroy, params:  {id: error.id}
            expect(response).to have_http_status(:no_content)
        end
    end
end
