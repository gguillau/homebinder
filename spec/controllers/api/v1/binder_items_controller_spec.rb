require 'rails_helper'

RSpec.describe Api::V1::BinderItemsController, type: :controller do

    let(:user) { FactoryBot.create(:user, role: :admin) }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }

    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        request.headers.merge!(valid_session) # Add to request headers
    end

    describe "GET #index" do
        context "gets all binder_items" do
            before do
                get :index, params: {}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
        context "gets all binder_items" do
            before do
                user.regenerate_access_token
                get :index, params: {:token => user.access_token}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end

    describe "POST #create" do
        context "create a binder_item" do
            before do
                appliance = create(:appliance)
                binder = create(:binder)
                post :create, params: {binder_item: {item_type: "Binder::Appliance", :appliance_id => appliance.id, :binder_id => binder.id, :user_id => user.id}}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end

    describe "PUT #update" do
        context "update the binder_item" do
            before do
                appliance = create(:appliance)
                binder_item = create(:binder_item, :appliance => appliance)
                put :update, params: {id: binder_item.id , binder_item: {verified: true}}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end

    describe "DELETE #destroy" do
        context "deletes the binder_item" do
            before do
                delete :destroy, params: {id: 1}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
end
