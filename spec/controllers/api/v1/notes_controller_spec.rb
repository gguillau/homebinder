require 'rails_helper'

RSpec.describe Api::V1::NotesController, :type => :controller do
    let(:model) {controller.controller_name.classify.constantize}
    let(:symbol) {controller.controller_name.singularize.to_sym}
    let(:user) { FactoryBot.create(:user, :role => "admin") }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    let!(:item) {create(symbol)}
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        allow(File).to receive(:read).and_call_original
        request.headers.merge!(valid_session) # Add to request headers
    end

    describe "GET #index" do
        context "get all notes" do
            before do
                get :index, params: {}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "returns the list of notes" do
                response_body = JSON.parse(response.body)
                expect(response_body["total"]).to eq 1
                expect(response_body["items"].length).to eq(1)
            end
        end
        context "when error is raised" do
            before do
                allow(model).to receive(:index).and_raise(BadRequestException)
                get :index, params: {}
            end

            it "responds with success" do
                expect(response).to have_http_status(400)
            end
        end
    end

    describe "GET #show" do
        context "get a single note" do
            before do
                get :show, params:  { id: item.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "returns the note" do
                note = JSON.parse(response.body)
                expect(note["id"]).to eq(item.id)
            end
        end
        context "when error is raised" do
            before do
                allow(model).to receive(:show).and_raise(BadRequestException)
                get :show, params:  { id: item.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(400)
            end
        end
    end

    describe "POST #create" do
        context "create a note" do
            before do
                partner = create(:partner)
                post :create, params:  { note: { content: "test note", notable_id: partner.id, notable_type: "Partner" }}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "creates a note" do
                note = JSON.parse(response.body)
                expect(note["content"]).to eq("test note")
            end
        end
        context "when error is raised when error is UnprocessableException" do
            before do
                allow(model).to receive(:build).and_raise(UnprocessableException.new(user))
                partner = create(:partner)
                post :create, params:  { note: { content: "test note", notable_id: partner.id, notable_type: "Partner" }}
            end

            it "responds with success" do
                expect(response).to have_http_status(422)
            end
        end
        context "when error is raised and error is BadRequestException" do
            before do
                allow(model).to receive(:build).and_raise(BadRequestException)
                partner = create(:partner)
                post :create, params:  { note: { content: "test note", notable_id: partner.id, notable_type: "Partner" }}
            end

            it "responds with success" do
                expect(response).to have_http_status(400)
            end
        end
    end

    describe "PUT #update" do
        context "update a note" do
            before do
                put :update, params: { id: item.id, note: { id: item.id, content: "updated note"}}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "updates a note" do
                note = JSON.parse(response.body)
                expect(note["content"]).to eq("updated note")
            end
        end
        context "when error is raised when error is UnprocessableException" do
            before do
                allow(model).to receive(:update).and_raise(UnprocessableException.new(user))
                put :update, params: { id: item.id, note: { id: item.id, content: "updated note"}}
            end

            it "responds with success" do
                expect(response).to have_http_status(422)
            end
        end
        context "when error is raised when is BadRequestException" do
            before do
                allow(model).to receive(:update).and_raise(BadRequestException)
                put :update, params: { id: item.id, note: { id: item.id, content: "updated note"}}
            end

            it "responds with success" do
                expect(response).to have_http_status(400)
            end
        end
    end

    describe "DELETE #destroy" do
        context "delete a note" do
            before do
                delete :destroy, params:  { id: item.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
        context "when error is raised" do
            before do
                allow(model).to receive(:destroy).and_raise(BadRequestException)
                delete :destroy, params:  { id: item.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(400)
            end
        end
    end
end
