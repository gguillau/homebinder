require 'rails_helper'

RSpec.describe Api::V1::InspectionReportJobsController, :type => :controller do
    
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:admin) { FactoryBot.create(:user, :role => "admin") }
    let(:jwt) { UserTokenService.create_jwt(admin) }
    let(:file) { {version: "0.0.0"} }
    let(:job) {create(:inspection_report_job)}
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        request.headers.merge!(valid_session) # Add to request headers
    end
    
    describe "GET #index" do
        context "gets the reports" do
            before do
                get :index, params: {}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "POST #create" do
        context "creates a report" do
            before do
                payload = {
                    job_id: SecureRandom.hex(4),
                }
                post :create, params: {inspection_report_job: payload}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "PUT #update" do
        context "updates a job" do
            before do
                put :update, params: {id: job.id, inspection_report_job: {status: "complete"}}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "DELETE #destroy" do
        context "deletes a job" do
            before do
                delete :destroy, params: {id: job.id}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
end
