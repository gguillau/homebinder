require 'rails_helper'
require 'mandrill'

RSpec.describe Api::V1::SharesController, :type => :controller do

    let!(:user) { FactoryBot.create(:user, :role => "admin") }
    let!(:api_key) { FactoryBot.create(:api_key) }
    let!(:jwt) { UserTokenService.create_jwt(user) }
    let!(:file) { {version: "0.0.0"} }
    let!(:with) {FactoryBot.create(:user)}
    let!(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }
    
    before :each do
        allow(File).to receive(:read).and_call_original
	    
        request.headers.merge!(valid_session) # Add to request headers
        @binder = create(:binder, name: "appliance tests", active: true, created_by: user.id)
        create(:subscription, binder_id: @binder.id, plan_id: 'free')
        UserBinder.create(:binder_id => @binder.id, :user_id => user.id, :role => "owner")
    end
  
    describe "GET #index" do
        context "get all shares for a binder" do
            before do
                create(:share, status: "sent", shared_by_id: user.id, shared_with_id: with.id, role_name: "co_owner", binder_id: @binder.id)
                create(:share, status: "accepted", shared_by_id: user.id, shared_with_id: with.id, role_name: "co_owner", binder_id: create(:binder).id)
                get :index, params:  { binderId: @binder.id }
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
            it "returns the list of shares" do
                response_body = JSON.parse(response.body)
                expect(response_body["total"]).to eq 1
            end
        end
    end
  
    describe "GET #show" do
        context "get a single share" do
            before do
                @share = create(:share, status: "sent", shared_by_id: user.id, shared_with_id: with.id, role_name: "co_owner", binder_id: @binder.id)
                get :show, params: { id: @share.id }
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
                it "returns the share" do
                share = JSON.parse(response.body)
                expect(share["id"]).to eq(@share.id)
            end
        end
    end
  
    describe "POST #create" do
        context "create an share" do
            before do
                share = {
                    user: {
            			first: "first",
            			last: "first",
            			email: "someuser@homebinder.com"
            		},
            	    binder_id: @binder.id,
            	    role_name: "co_owner"
                }
                post :create, params:  { share: share}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
                it "creates an share" do
                share = JSON.parse(response.body)
                expect(share["role_name"]).to eq("co_owner")
            end
        end
    end
  
    describe "PUT #update" do
        context "update an share" do
            before do
                share = create(:share, status: "sent", shared_by_id: user.id, shared_with_id: with.id, role_name: "co_owner", binder_id: @binder.id)
                put :update, params:  { id: share.id, share: { binder_id: @binder.id, status: "accepted" }}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
                it "updates an share" do
                share = JSON.parse(response.body)
                expect(share["status"]).to eq("accepted")
            end
        end
    end
  
    describe "DELETE #destroy" do
        context "delete an share" do
            before do
                share = create(:share, status: "sent", shared_by_id: user.id, shared_with_id: with.id, role_name: "co_owner", binder_id: @binder.id)
                delete :destroy, params:  { id: share.id }
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "PUT #resend" do
        context "update an share" do
            before do
                share = create(:share, status: "sent", shared_by_id: user.id, shared_with_id: with.id, role_name: "co_owner", binder_id: @binder.id)
                put :resend, params:  { id: share.id}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
  
end