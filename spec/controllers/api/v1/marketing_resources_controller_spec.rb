require 'rails_helper'

RSpec.describe Api::V1::MarketingResourcesController, :type => :controller do
    
    let(:user) { FactoryBot.create(:user, role: :admin) }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }
    
    before :each do
        request.headers.merge!(valid_session) # Add to request headers
    end
  
    describe "POST #create" do
        context "create resource" do
            before do
                post :create, params: { marketing_resource: {name: "TEST"}}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
                it "returns the organization" do
                response_body = JSON.parse(response.body)
                expect(response_body["name"]).to eq "TEST"
            end
        end
    end
    
    describe "PUT #update" do
        context "update resource" do
            before do
                resource = create(:marketing_resource)
                put :update, params: { id: resource.id, marketing_resource: {name: "org", description: "desc" }}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
                it "updates the resource" do
                response_body = JSON.parse(response.body)
                expect(response_body["name"]).to eq "org"
            end
        end
    end
    
    describe "DELETE #destroy" do
        context "delete marketing_resource" do
            before do
                marketing_resource = create(:marketing_resource)
                put :destroy, params: { id: marketing_resource.id }
            end
                it "responds with no content" do
                expect(response).to have_http_status(:no_content)
            end
        end
    end

  
end