require 'rails_helper'

RSpec.describe Api::V1::ContractorSubTypesController, :type => :controller do
    
    let(:model) {controller.klass.classify.constantize}
    let(:symbol) {controller.klass.table_name.singularize.to_sym}
    let(:user) { FactoryBot.create(:user, :role => "admin") }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let!(:item) {create(symbol)}
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        request.headers.merge!(valid_session) # Add to request headers
    end

    describe "GET #index" do
        context "get all sub types" do
            before do
                get :index, params: {}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "returns the list of notes" do
                response_body = JSON.parse(response.body)
                expect(response_body["total"]).to eq 1
                expect(response_body["items"].length).to eq(1)
            end
        end
    end

    describe "GET #show" do
        context "get a single type" do
            before do
                get :show, params:  { id: item.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "returns the type" do
                type = JSON.parse(response.body)
                expect(type["id"]).to eq(item.id)
            end
        end
    end

    describe "POST #create" do
        context "create a type" do
            before do
                post :create, params:  { contractor_sub_category: { name: "test type" }}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "creates a type" do
                type = JSON.parse(response.body)
                expect(type["name"]).to eq("test type")
            end
        end
    end

    describe "PUT #update" do
        context "update a type" do
            before do
                put :update, params: { id: item.id, contractor_sub_category: { name: "updated name"}}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "updates a type" do
                type = JSON.parse(response.body)
                expect(type["name"]).to eq("updated name")
            end
        end
    end

    describe "DELETE #destroy" do
        context "delete a type" do
            before do
                delete :destroy, params:  { id: item.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
end
