require 'rails_helper'
require 'webmock/rspec'

RSpec.describe Api::V1::IsnUsersController, :type => :controller do

  let(:user) { FactoryBot.create(:user, :role => "inspector") }
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

  before :each do
    
    request.headers.merge!(valid_session) # Add to request headers
    @partner = create(:partner)
    @config = @partner.partner_configuration
    PartnerUser.create(:partner_id => @partner.id, :user_id => user.id, :role => "admin")
  end
  
  it "gets all isn users for a partner" do
    create(:isn_user, partner_configuration_id: @config.id)
    create(:isn_user, partner_configuration_id: @config.id)
    create(:isn_user, partner_configuration_id: 9999999)
    
    get :index, params: { :partner_configuration_id => @partner.partner_configuration.id }
    
    p = JSON.parse(response.body)
    expect(response).to have_http_status(:ok)
    expect(p.length).to eq(2)
  end
  
  it "gets an isn user" do
    u = create(:isn_user, partner_configuration_id: @config.id)
    
    get :show, params: { :id => u.id }
    
    p = JSON.parse(response.body)
    expect(response).to have_http_status(:ok)
    expect(p["id"]).to eq(u.id)
  end
  
  it "creates an isn user" do
    isn_rake = Isn::RakeJob.new
    allow(Isn::RakeJob).to receive(:new).and_return(isn_rake)
    allow(isn_rake).to receive(:get_footprints)
        
    post :create, params: {:isn_user => {
      :name => "test user",
      :company_key => "test",
      :username => "bsmith",
      :password => "password",
      :partner_configuration_id => @partner.partner_configuration.id,
      :api_endpoint => "test.com"
    }}
    
    p = JSON.parse(response.body)

    expect(response).to have_http_status(:ok)
    expect(p["partner_configuration_id"]).to eq(@config.id)
    expect(p["name"]).to eq("test user")
    expect(p["company_key"]).to eq("test")
    expect(p["username"]).to eq("bsmith")
    expect(p["password"]).to eq("password")
  end
  
  it "updates an isn user" do
    u = create(:isn_user, partner_configuration_id: @config.id)
    
    put :update, params: { :id => u.id, :isn_user => {
      :company_key => "test",
      :username => "bsmith",
      :password => "password"
    }}
    
    p = JSON.parse(response.body)
    expect(response).to have_http_status(:ok)
    expect(p["company_key"]).to eq("test")
    expect(p["username"]).to eq("bsmith")
    expect(p["password"]).to eq("password")
  end
  
  it "deletes an isn user" do
    u = create(:isn_user, partner_configuration_id: @config.id)
    
    delete :destroy, params: { :id => u.id }
    
    expect(response).to have_http_status(:no_content)
  end

end