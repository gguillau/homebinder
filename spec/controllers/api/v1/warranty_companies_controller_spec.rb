require 'rails_helper'

RSpec.describe Api::V1::WarrantyCompaniesController, type: :controller do
    
    let(:user) { FactoryBot.create(:user, role: :admin) }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }
    
    before :each do
        request.headers.merge!(valid_session) # Add to request headers
    end
  
    describe "GET #index" do
        context "gets all warranty_companies" do
            before do
                get :index, params: nil
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "POST #create" do
        context "create a warranty_company" do
            before do
                post :create, params: {warranty_company: {:name => "TEST", :contact => "TEST", :email => "email@gmail.com"}}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "PUT #update" do
        context "update the warranty_company" do
            before do
                warranty_company = create(:warranty_company)
                put :update, params: {id: warranty_company.id , warranty_company: {:name => "OK"}}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "DELETE #destroy" do
        context "deletes the warranty_company" do
            before do
                warranty_company = create(:warranty_company)
                delete :destroy, params: {id: warranty_company.id}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end

end
