require 'rails_helper'

RSpec.describe Api::V1::BindersController, :type => :controller do
    let(:model) {controller.controller_name.classify.constantize}
    let(:symbol) {controller.controller_name.singularize.to_sym}
    let(:user) { FactoryBot.create(:user, :role => "admin") }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    let!(:item) {create(symbol)}
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        allow(File).to receive(:read).and_call_original
        request.headers.merge!(valid_session) # Add to request headers
    end

    describe "GET #index" do
        context "get all binders" do
            before do
                get :index, params: {}
            end
            it "assigns all binders as @response" do
                binders = JSON.parse(response.body)
                expect(binders["items"].length).to eq(1)
            end

            it "returns http success" do
                expect(response).to have_http_status(:success)
            end

            it "renders the :index view" do
                binders = JSON.parse(response.body)
                expect(binders["total"]).to eq 1
            end
        end
    end

    describe "GET #show" do
        context "get a single binder" do
            before do
                get :show, params:  { id: item.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "returns the binder" do
                binder = JSON.parse(response.body)
                expect(binder["id"]).to eq(item.id)
            end
        end
    end

    describe "POST #create" do
        context "when creating a binder" do
            before do
                property = {
                    property_type: "single",
                    address1: "123 main street",
                    address2: "",
                    city: "Boston",
                    state: "MA",
                    country: "US"
                }
                post :create, params:  {binder: { binder: { name: "test", property_attributes: property}}}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "creates a binder" do
                binder = JSON.parse(response.body)
                expect(binder["name"]).to eq("test")
            end
        end
    end

    describe "PUT #update" do
        context "update a binder" do
            before do
                put :update, params:  { id: item.id, binder: { name: "updated" }}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "updates an binder" do
                binder = JSON.parse(response.body)
                expect(binder["name"]).to eq("updated")
            end
        end
    end

    describe "DELETE #destroy" do
        context "delete a binder" do
            before do
                delete :destroy, params:  { id: item.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end

    describe "PUT #orphan" do
        it "returns http success" do
            put :orphan, params: {:id => item.id}
            expect(response).to have_http_status(:success)
        end
    end

    describe "PUT #sendFeedback" do
        it "returns http success" do
            put :sendFeedback, params: {:id => item.id}
            expect(response).to have_http_status(:success)
        end
    end

    describe "PUT #requestServices" do
        it "returns http success" do
            Sidekiq::Testing.disable!
            put :requestServices, params: {:id => item.id, :services => ["Help with stuff"]}
            expect(response).to have_http_status(:success)
        end
    end

    describe "PUT #requestSolarServices" do
        it "returns http success" do
            create(:user_binder, :user => user, :binder => item, :role => "owner")
            put :requestSolarServices, params: {:id => item.id}
            expect(response).to have_http_status(:success)
        end
    end

    describe "DELETE #declineServices" do
        it "returns http success" do
            Sidekiq::Testing.disable!
            put :declineServices, params: {:id => item.id, :email => "support@homebinder.com"}
            expect(response).to have_http_status(:success)
        end
    end

    describe "PUT #learn_more_apr" do
        it "returns http success" do
            put :learn_more_apr, params: {:id => item.id}
            expect(response).to have_http_status(:success)
        end
    end

    describe "PUT #secure24" do
        it "returns http success" do
            put :secure24, params: {:id => item.id}
            expect(response).to have_http_status(:success)
        end
    end

    describe "GET #inventory_report" do
        it "returns http success" do
            allow_any_instance_of(InventoryReportPdfService).to receive(:create).and_return ""
            get :inventory_report, params: {:id => item.id}
            expect(response).to have_http_status(:success)
        end
    end

    describe "POST #add_user" do
        it "returns http success" do
            add = create(:user)
            post :add_user, params: {:id => item.id, :user_id => add.id, :role => "co_owner"}
            expect(response).to have_http_status(:success)
        end
    end

    describe "POST #get_acl" do
        it "returns http success" do
            post :get_acl, params: {:id => item.id}
            expect(response).to have_http_status(:success)
        end
    end

    describe "POST #delete_acl" do
        it "returns http success" do
            post :delete_acl, params: {:id => item.id, :user_id => -1}
            expect(response).to have_http_status(:success)
        end
    end
    
    describe "POST #batch" do
        it "returns http success" do
            binder = { first_name: "Bob", last_name: "Smith", email: "test@gmail.com", address1: "123 Main Street", city: "Boston", state: "MA", country: "US" }
            partner = create(:partner)
            user = create(:user, :role => "inspector")
            create(:partner_user, :user => user, :partner => partner, :role => "admin")
            template = create(:binder_template, :partner_configuration => partner.partner_configuration)
            partner.partner_configuration.default_binder_template_id = template.id
            partner.partner_configuration.save!
            
            params = {
                :partnerId => partner.id,
                :binders => [binder]
            }
            post :batch, params: params
            expect(response).to have_http_status(:success)
        end
    end

    describe "POST #add_partner" do
        it "returns http success" do
            partner = create(:partner)
            create(:partner_user, :partner => partner)
            homeowner = create(:user, :role => "homeowner")
            post :add_partner, params: {:id => item.id, :data => {:partner_id => partner.id, :role => "co_owner", :client_id => homeowner.id}}
            expect(response).to have_http_status(:success)
        end
    end

    describe "GET #get_partner" do
        it "returns http success" do
            create(:partner_binder, :binder => item)
            get :get_partner, params: {:id => item.id}
            expect(response).to have_http_status(:success)
        end
    end

    describe "GET #capital_expense_report" do
        it "returns http success" do
            allow_any_instance_of(CapitalExpenseReportPdfService).to receive(:create).and_return ""
            get :capital_expense_report, params: {:id => item.id}
            expect(response).to have_http_status(:success)
        end
    end

    describe "POST #find_binder_by_property" do
        it "returns http success" do
            post :find_binder_by_property, params: {:property => {:address => "123 Main Street"}, :client => {:email => "test@gmail.com"}}
            expect(response).to have_http_status(:success)
        end
    end

    describe "DELETE #delete_partner" do
        it "returns http success" do
            partner_binder = create(:partner_binder, :binder => item)
            delete :delete_partner, params: {:id => item.id, :partner_binder_id => partner_binder.id}
            expect(response).to have_http_status(:success)
        end
    end
end
