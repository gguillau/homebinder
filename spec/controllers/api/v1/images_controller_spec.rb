require 'rails_helper'

# these tests require AWS access. Not including them for now
RSpec.describe Api::V1::ImagesController, :type => :controller do

  let(:user) { FactoryBot.create(:user) }
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

  before :each do
    
    request.headers.merge!(valid_session) # Add to request headers
    
    @binder = create(:binder, name: "image tests", active: true, created_by: user.id)
    create(:subscription, binder_id: @binder.id, plan_id: 'free')
    UserBinder.create(:binder_id => @binder.id, :user_id => user.id, :role => "owner")
  end
  
  describe "GET #index" do
    context "get all images for a binder" do
      before do
        create(:image, binder_id: @binder.id, file: File.new("spec/assets/SampleImage.png"))
        create(:image, binder_id: create(:binder).id, file: File.new("spec/assets/SampleImage.png"))
        get :index, params: { binderId: @binder.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the list of images" do
        list = JSON.parse(response.body)
        expect(list["total"]).to eq 1
      end
    end
  end
  
  describe "GET #show" do
    context "get a single image" do
      before do
        @doc = create(:image, binder_id: @binder.id, file: File.new("spec/assets/SampleImage.png"))
        get :show, params: { id: @doc.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the image" do
        doc = JSON.parse(response.body)
        expect(doc["id"]).to eq(@doc.id)
      end
    end
  end
  
  describe "POST #create" do
    context "create a image" do
      before do
        post :create, params: { binder_id: @binder.id, file: fixture_file_upload("spec/assets/SampleImage.png", "image/png")}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "creates a image" do
      end
    end
  end
  
  describe "DELETE #destroy" do
    context "delete an image" do
      before do
        doc = create(:image, binder_id: @binder.id, file: File.new("spec/assets/SampleImage.png"))
        delete :destroy, params: { id: doc.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
    end
  end
  
end