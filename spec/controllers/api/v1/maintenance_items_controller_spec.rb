require 'rails_helper'

RSpec.describe Api::V1::MaintenanceItemsController, :type => :controller do

  let(:user) { FactoryBot.create(:user) }
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

  before :each do
    allow(File).to receive(:read).and_call_original
    
    request.headers.merge!(valid_session) # Add to request headers
    
    @binder = create(:binder, name: "maintenance_item tests", active: true, created_by: user.id)
    create(:subscription, binder_id: @binder.id, plan_id: 'free')
    UserBinder.create(:binder_id => @binder.id, :user_id => user.id, :role => "owner")
  end
  
  describe "GET #index" do
    context "get all maintenance_items for a binder" do
      before do
        create(:maintenance_item, name: "maintenance_item a", binder_id: @binder.id)
        create(:maintenance_item, name: "maintenance_item b", binder_id: create(:binder).id)
        get :index, params: { binderId: @binder.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the list of maintenance_items" do
        response_body = JSON.parse(response.body)
        expect(response_body["total"]).to eq 1
      end
    end
  end
  
  describe "GET #show" do
    context "get a single maintenance_item" do
      before do
        @maintenance_item = create(:maintenance_item, name: "get me", binder_id: @binder.id)
        get :show, params: { id: @maintenance_item.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the maintenance_item" do
        maintenance_item = JSON.parse(response.body)
        expect(maintenance_item["id"]).to eq(@maintenance_item.id)
      end
    end
  end
  
  describe "POST #send_test_email" do
      before do
        @maintenance_item = create(:maintenance_item, name: "get me", binder_id: @binder.id)
        post :send_test_email, params: { id: @maintenance_item.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
  end
  
  describe "POST #create" do
    context "create an maintenance_item" do
      before do
        post :create, params: { maintenance_item: { binder_id: @binder.id, name: "new maintenance_item", maintenance_cycle: "none" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "creates an maintenance_item" do
        maintenance_item = JSON.parse(response.body)
        expect(maintenance_item["name"]).to eq("new maintenance_item")
      end
    end
  end
  
  describe "PUT #update" do
    context "update an maintenance_item" do
      before do
        maintenance_item = create(:maintenance_item, name: "created", binder_id: @binder.id, maintenance_cycle: "none")
        put :update, params: { id: maintenance_item.id, maintenance_item: { binder_id: @binder.id, name: "updated" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "updates an maintenance_item" do
        maintenance_item = JSON.parse(response.body)
        expect(maintenance_item["name"]).to eq("updated")
      end
    end
  end
  
  describe "DELETE #destroy" do
    context "delete an maintenance_item" do
      before do
        maintenance_item = create(:maintenance_item, name: "created", binder_id: @binder.id)
        delete :destroy, params: { id: maintenance_item.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
    end
  end
  
end