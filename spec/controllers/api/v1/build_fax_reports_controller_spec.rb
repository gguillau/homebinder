require 'rails_helper'

RSpec.describe Api::V1::BuildFaxReportsController, type: :controller do
    
    let(:user) { FactoryBot.create(:user, role: :admin) }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }
    
    before :each do
        request.headers.merge!(valid_session) # Add to request headers
    end
  
    describe "GET #index" do
        context "gets all reports" do
            before do
                get :index, params: {:orderBy => "id"}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "GET #show" do
        context "gets a report" do
            before do
                build_fax_report = create(:build_fax_report)
                get :show, params: {id: build_fax_report.id}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "POST #create" do
        context "create a report" do
            before do
                buildfax = Buildfax::Request.new
                allow(Buildfax::Request).to receive(:new).and_return(buildfax)

                file = File.open("#{Rails.root}/spec/assets/SampleDoc.pdf", "rb")
                contents = file.read
                        allow(buildfax).to receive(:download_report).and_return(contents)
                build_fax_report = {
                    :address => "12 Main Street",
                    :city => "Boston",
                    :state => "MA",
                    :zip => "02210"
                }
                post :create, params: {build_fax_report: build_fax_report}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "POST #search" do
        before do
            allow_any_instance_of(Buildfax::Client).to receive(:send_request).and_return({})
            post :search, params: {search: {address: "123 State Street", city: "Boston", state: "MA", zip: "02210"}}
        end
        it "responds with success" do
            expect(response).to have_http_status(:success)
        end
    end
    
    describe "PUT #update" do
        context "update the report" do
            before do
                build_fax_report = create(:build_fax_report)
                put :update, params: {id: build_fax_report.id, build_fax_report: {:status => "paid"}}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "DELETE #destroy" do
        context "deletes the ep" do
            before do
                build_fax_report = create(:build_fax_report)
                delete :destroy, params: {id: build_fax_report.id}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
end
