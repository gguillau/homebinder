require 'rails_helper'

RSpec.describe Api::V1::ProjectsController, :type => :controller do

  let(:user) { FactoryBot.create(:user) }
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

  before :each do
    
    request.headers.merge!(valid_session) # Add to request headers
    
    @binder = create(:binder, name: "project tests", active: true, created_by: user.id)
    create(:subscription, binder_id: @binder.id, plan_id: 'free')
    UserBinder.create(:binder_id => @binder.id, :user_id => user.id, :role => "owner")
  end
  
  describe "GET #index" do
    context "get all projects for a binder" do
      before do
        create(:project, name: "project a", binder_id: @binder.id)
        create(:project, name: "project b", binder_id: create(:binder).id)
        get :index, params: { binderId: @binder.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the list of projects" do
        response_body = JSON.parse(response.body)
        expect(response_body["total"]).to eq 1
      end
    end
  end
  
  describe "GET #show" do
    context "get a single project" do
      before do
        @project = create(:project, name: "get me", binder_id: @binder.id)
        get :show, params: { id: @project.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the project" do
        project = JSON.parse(response.body)
        expect(project["id"]).to eq(@project.id)
      end
    end
  end
  
  describe "POST #create" do
    context "create an project" do
      before do
        post :create, params: { project: { binder_id: @binder.id, name: "new project" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "creates an project" do
        project = JSON.parse(response.body)
        expect(project["name"]).to eq("new project")
      end
    end
  end
  
  describe "PUT #update" do
    context "update an project" do
      before do
        project = create(:project, name: "created", binder_id: @binder.id)
        put :update, params: { id: project.id, project: { binder_id: @binder.id, name: "updated" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "updates an project" do
        project = JSON.parse(response.body)
        expect(project["name"]).to eq("updated")
      end
    end
  end
  
  describe "DELETE #destroy" do
    context "delete an project" do
      before do
        project = create(:project, name: "created", binder_id: @binder.id)
        delete :destroy, params:  { id: project.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
    end
  end
  
end