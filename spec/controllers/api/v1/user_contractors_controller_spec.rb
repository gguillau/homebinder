require 'rails_helper'

RSpec.describe Api::V1::UserContractorsController, :type => :controller do
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:admin) { FactoryBot.create(:user, :role => "admin") }
    let(:jwt) { UserTokenService.create_jwt(admin) }
    let(:file) { {version: "0.0.0"} }
    let(:item) {create(:user_contractor)}
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        request.headers.merge!(valid_session) # Add to request headers
    end

    describe "GET #index" do
        context "gets the items" do
            before do
                get :index, params: {}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end

    describe "POST #create" do
        context "creates an item" do
            before do
                contractor = create(:contractor)
                payload = {
                    name: "test",
                    user_id: admin.id,
                    contractor_id: contractor.id,
                    types_attributes: [{:name => SecureRandom.hex(4)}]
                }
                post :create, params: {user_contractor: payload}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end

    describe "PUT #update" do
        context "updates an item" do
            before do
                put :update, params: {id: item.id, user_contractor: {name: "updated"}}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end

    describe "DELETE #destroy" do
        context "deletes an item" do
            before do
                delete :destroy, params: {id: item.id}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
end
