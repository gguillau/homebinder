require 'rails_helper'
require 'mandrill'

RSpec.describe Api::V1::WarrantyConfigurationsController, type: :controller do

    let(:user) { FactoryBot.create(:user, role: :admin) }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }

    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        allow(File).to receive(:read)
        request.headers.merge!(valid_session) # Add to request headers
    end

    describe "GET #index" do
        it "gets all configs" do
            get :index, params: nil
            expect(response).to have_http_status(:ok)
        end
    end

    describe "POST #create" do
        it "creates a partners warranty configuration" do
            warranty_company = create(:warranty_company)
            warranty_plan = create(:warranty_plan)
            partner = create(:partner)
            post :create, params: {:partner_warranty_plans => [{:id => warranty_plan.id, :price => 2}], warranty_configuration: {account_number: "TET", :warranty_company_id => warranty_company.id, :warranty_plan_id => warranty_plan.id, :partner_id => partner.id}}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "GET #show" do
        it "gets a partners warranty configuration" do
            warranty_configuration = create(:warranty_configuration)
            get :show, params: {:id => warranty_configuration.id}
            expect(response).to have_http_status(:ok)
        end
    end

    describe "PUT #update" do
        it "updates a partners configuration" do
            warranty_configuration = create(:warranty_configuration)
            warranty_plan = create(:warranty_plan)
            params = {
                :id => warranty_configuration.id,
                :warranty_configuration => {
                    :account_number => "1AS1D32A"
                },
                :partner_warranty_plans => [{:id => warranty_plan.id, :price => 2}]
            }

            put :update, params: params

            expect(response).to have_http_status(:ok)
        end
    end

    describe "POST #orders" do
        it "sends an order to a warranty company" do
            binder = create(:binder)
            user = create(:user)

            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")

            warranty_company = create(:warranty_company)
            warranty_plan = create(:warranty_plan)
            warranty_configuration = create(:warranty_configuration)
            homeowner = create(:user)

            homeowner = {
                id: homeowner.id,
                firstName: homeowner.user_profile.first_name,
                lastName: homeowner.user_profile.last_name,
                email: homeowner.email,
                phone: homeowner.user_profile.mobile_phone
            }

            property = {
                address1: "123 Main Street",
                address2: "",
                city: "Boston",
                state: "MA",
                country: "US",
                zip: "02210"
            }

            params = {
                :id => warranty_configuration.id,
                :order => {
                    binder_id: binder.id,
                    property: property,
                    warranty_company_id: warranty_company.id,
                    warranty_plan_id: warranty_plan.id,
                    account_number: warranty_configuration.account_number,
                    homeowner: homeowner,
                    inspection_date: 5.days.ago,
                    price: 25
                }
            }

            post :orders, params: params

            expect(response).to have_http_status(:ok)
        end
    end

    describe "DELETE #delete" do
        it "deletes a partners warranty configuration" do
            warranty_configuration = create(:warranty_configuration)

            params = {:id => warranty_configuration.id }

            delete :destroy, params: params

            expect(response).to have_http_status(:no_content)
        end
    end
end