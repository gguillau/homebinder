require 'rails_helper'

RSpec.describe Api::V1::RegistrationsController, type: :controller do
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:file) { { version: '0.0.0' } }
    let(:valid_session) { { 'HB-APIKey' => api_key.key } }

    before :each do
        allow(File).to receive(:read).and_call_original
        request.headers.merge!(valid_session) # Add to request headers
    end

    describe 'POST #create' do
        context 'create a user' do
            before do
                user = { email: 'test@gmail.com', password: 'password1' }
                post :create, params: { user: user }
            end
            it 'responds with success' do
                expect(response).to have_http_status(:success)
            end
        end
    end
end
