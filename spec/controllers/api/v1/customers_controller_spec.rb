require 'rails_helper'

RSpec.describe Api::V1::CustomersController, type: :controller do
    
    let(:user) { FactoryBot.create(:user, role: :admin) }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }
    
    before :each do
        allow(File).to receive(:read).and_call_original
        request.headers.merge!(valid_session) # Add to request headers
        allow(Stripe::Customer).to receive(:retrieve).and_return(OpenStruct.new({id: 1, email: "test@gmail.com", source: nil, delete: true}))
        allow(Stripe::Customer).to receive(:create).and_return(OpenStruct.new({id: 1, email: "test@gmail.com", source: nil, delete: true}))
        allow(Stripe::Plan).to receive(:retrieve).and_return({id: 1})
        allow(Stripe::Subscription).to receive(:create).and_return({id: 1})
    end
  
    describe "GET #show" do
        context "gets a customer in Stripe" do
            before do
                get :show, params: {:id => 1}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "POST #create" do
        context "create a customer in stripe" do
            before do
                post :create, params: {customer: {email: "test@gmail.com", address: {address1: "123 Main Street"}}, payment: {card: "card"}}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "PUT #update" do
        context "update the customer" do
            before do
                put :update, params: {id: 1, customer: {email: "fake@gmail.com"}, payment: {card: "card"}}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "DELETE #destroy" do
        context "deletes a customer in Stripe" do
            before do
                delete :destroy, params: {:id => 1}
            end
                it "responds with no_content" do
                expect(response).to have_http_status(:no_content)
            end
        end
    end
end
