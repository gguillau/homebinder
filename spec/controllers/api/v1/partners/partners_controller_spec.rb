require 'rails_helper'

RSpec.describe Api::V1::Partners::PartnersController, :type => :controller do
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:partner) { FactoryBot.create(:partner, name: "Test Inspections", contact: "Test Inspector", partner_key: "axium") }
    let(:user) { FactoryBot.create(:user, email: partner.email, :role => "inspector") }
    let(:member) {FactoryBot.create(:user, :role => "inspector")}
    let(:partner_two) { FactoryBot.create(:partner, partner_key: "test") }
    let(:partner_key) { FactoryBot.create(:api_key, partner_id: partner.id, contact_email: "test@gmail.com") }
    let(:config) {partner_two.partner_configuration}
    let(:template) { FactoryBot.create(:binder_template, partner_configuration_id: config.id) }
    let(:config_two) { partner.partner_configuration }
    let(:template_two) { FactoryBot.create(:binder_template, partner_configuration_id: config_two.id) }

    # This should return the minimal set of values that should be in the session
    # in order to pass any filters (e.g. authentication)
    let(:invalid_partner_api_session) { { 'HB-APIKey' => api_key.key} }
    let(:valid_partner_api_session) { { 'HB-APIKey' => partner_key.key, 'HTTP_HB_APIKEY' => partner_key.key} }

    before :each do
        allow(ErrorService).to receive(:perform_async)

        partner.partner_configuration.default_user_branding_id = user.user_profile.id
        partner.partner_configuration.save
        allow(Partner::Automation::Error).to receive(:build).and_call_original
        PartnerUser.create(:user_id => user.id, :partner_id => partner.id, :role => "admin")
        PartnerUser.create(:user_id => member.id, :partner_id => partner.id, :role => "member")
    end

    describe "GET #verify" do
        it "Returns a 403 error because of missing API Key" do
            get :verify, params: {:key => ""}

            expect(response).to have_http_status(403)
            expect(JSON.parse(response.body)["message"]).to eq("Api key is required")
        end
        it "Returns a 400 error because of invalid partner key" do
            request.headers.merge!(invalid_partner_api_session) # Add to request headers
            get :verify, params:{:key => ""}
            expect(response).to have_http_status(400)
            expect(JSON.parse(response.body)["message"]).to eq("Invalid Partner Key")
        end
        it "Returns a 200" do
            request.headers.merge!(valid_partner_api_session) # Add to request headers
            get :verify, params:{:key => "axium"}

            expect(response).to have_http_status(200)
            expect(JSON.parse(response.body)["message"]).to eq("Test succeeded")
        end
    end

    describe "POST #create_binder" do
        it "Returns a 403 error because of missing API Key" do
            post :create, params: {:key => ""}
            expect(response).to have_http_status(403)
            expect(JSON.parse(response.body)["message"]).to eq("Api key is required")
        end
        it "Returns a 400 error because of invalid partner key" do
            request.headers.merge!(invalid_partner_api_session) # Add to request headers
            post :create, params:{:key => ""}
            expect(response).to have_http_status(400)
            expect(JSON.parse(response.body)["message"]).to eq("Invalid Partner Key")
        end
        it "Returns a 400 error because of client information not included" do
            request.headers.merge!(valid_partner_api_session) # Add to request headers
            post :create, params:{:key => "axium"}

            expect(response).to have_http_status(400)
            expect(JSON.parse(response.body)["message"]).to eq("Client information not included")
            expect(ErrorService).to have_received(:perform_async).at_least(:once)
            expect(Partner::Automation::Error).to have_received(:build)
            expect(Partner::Automation::Error.last.error_message).to eq("Client information not included")
        end
        it "Returns a 400 error because property information not included" do
            request.headers.merge!(valid_partner_api_session) # Add to request headers
            post :create, params: {:key => "axium", :client => {first: "Steve"}}

            expect(response).to have_http_status(400)
            expect(JSON.parse(response.body)["message"]).to eq("Property information not included")
            expect(ErrorService).to have_received(:perform_async).at_least(:once)
            expect(Partner::Automation::Error).to have_received(:build)
            expect(Partner::Automation::Error.last.error_message).to eq("Property information not included")
        end
        it "Returns a 400 error because property information not included" do
            request.headers.merge!(valid_partner_api_session) # Add to request headers
            post :create, params: {:key => "axium", :client => {first: "Steve"}}

            expect(response).to have_http_status(400)
            expect(JSON.parse(response.body)["message"]).to eq("Property information not included")
            expect(ErrorService).to have_received(:perform_async).at_least(:once)
            expect(Partner::Automation::Error).to have_received(:build)
            expect(Partner::Automation::Error.last.error_message).to eq("Property information not included")
        end
        it "Returns a 400 error because client first name not included" do
            request.headers.merge!(valid_partner_api_session) # Add to request headers
            post :create, params: {:key => "axium", :client => {first: ""}, :property => {:address => ""}}

            expect(response).to have_http_status(400)
            expect(JSON.parse(response.body)["message"]).to eq("Client First Name required")
            expect(ErrorService).to have_received(:perform_async).at_least(:once)
            expect(Partner::Automation::Error).to have_received(:build)
            expect(Partner::Automation::Error.last.error_message).to eq("Client First Name required")
        end
        it "Returns a 400 error because property address not included" do
            request.headers.merge!(valid_partner_api_session) # Add to request headers
            post :create, params: {:key => "axium", :client => {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}, :property => {address: ""}}

            expect(response).to have_http_status(400)
            expect(JSON.parse(response.body)["message"]).to eq("Property Address required")
            expect(ErrorService).to have_received(:perform_async).at_least(:once)
            expect(Partner::Automation::Error).to have_received(:build)
            expect(Partner::Automation::Error.last.error_message).to eq("Property Address required")
        end
        it "Returns a 400 error because property city not included" do
            request.headers.merge!(valid_partner_api_session) # Add to request headers
            parameters = {:key => "axium", :client => {first: "Mike", last: "Breen", email: "transfers@homebinder.com"},
                          :property => {address: "321 Summer Street", address2: ""}}
            post :create, params: parameters
            expect(response).to have_http_status(400)
            expect(JSON.parse(response.body)["message"]).to eq("Property City required")
            expect(ErrorService).to have_received(:perform_async).at_least(:once)
            expect(Partner::Automation::Error).to have_received(:build)
            expect(Partner::Automation::Error.last.error_message).to eq("Property City required")
        end
        it "Returns a 400 error because property state not included" do
            request.headers.merge!(valid_partner_api_session) # Add to request headers
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: ""}
            parameters = {:key => "axium", :client => client, :property => property}
            post :create, params: parameters
            expect(response).to have_http_status(400)
            expect(JSON.parse(response.body)["message"]).to eq("Property State required")
            expect(ErrorService).to have_received(:perform_async).at_least(:once)
            expect(Partner::Automation::Error).to have_received(:build)
            expect(Partner::Automation::Error.last.error_message).to eq("Property State required")
        end
        it "Returns a 400 error because property country not included" do
            request.headers.merge!(valid_partner_api_session) # Add to request headers
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: ""}
            parameters = {:key => "axium", :client => client, :property => property}
            post :create, params: parameters
            expect(response).to have_http_status(400)
            expect(JSON.parse(response.body)["message"]).to eq("Property Country required")
            expect(ErrorService).to have_received(:perform_async).at_least(:once)
            expect(Partner::Automation::Error).to have_received(:build)
            expect(Partner::Automation::Error.last.error_message).to eq("Property Country required")
        end
        it "Returns a 400 error because binder template id not included" do
            request.headers.merge!(valid_partner_api_session) # Add to request headers
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            parameters = {:key => "axium", :client => client, :property => property, :binder_template_id => "", :transfer => false}
            post :create, params: parameters
            expect(response).to have_http_status(400)
            expect(JSON.parse(response.body)["message"]).to eq("Binder Template ID required")
            expect(ErrorService).to have_received(:perform_async).at_least(:once)
            expect(Partner::Automation::Error).to have_received(:build)
            expect(Partner::Automation::Error.last.error_message).to eq("Binder Template ID required")
        end
        it "Returns a 400 error because incorrect binder template id is included" do
            request.headers.merge!(valid_partner_api_session) # Add to request headers
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            parameters = {:key => "axium", :client => client, :property => property, :binder_template_id => template.id, :transfer => true}
            post :create, params: parameters
            expect(response).to have_http_status(400)
            expect(JSON.parse(response.body)["message"]).to eq("You are not authorized to use template ID # #{template.id}")
            expect(ErrorService).to have_received(:perform_async).at_least(:once)
            expect(Partner::Automation::Error).to have_received(:build)
            expect(Partner::Automation::Error.last.error_message).to eq("You are not authorized to use template ID # #{template.id}")
        end
        it "Returns a 400 error because incorrect binder template id is included" do
            file = Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleDoc.pdf'), 'application/pdf')
            request.headers.merge!(valid_partner_api_session) # Add to request headers
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            inspection_report = {file: file, name: "SampleDoc.pdf"}
            parameters = {:key => "axium", :client => client, :property => property, :binder_template_id => template.id, :documents => [inspection_report]}
            post :create, params: parameters
            expect(response).to have_http_status(400)
            expect(JSON.parse(response.body)["message"]).to eq("You are not authorized to use template ID # #{template.id}")
            expect(ErrorService).to have_received(:perform_async).at_least(:once)
            expect(Partner::Automation::Error).to have_received(:build)
            expect(Partner::Automation::Error.last.error_message).to eq("You are not authorized to use template ID # #{template.id}")
        end
        it "Returns a 400 error because incorrect binder template id is included" do
            file = File.open('spec/assets/SampleDoc.pdf') #Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleDoc.pdf'), 'application/pdf')
            image = File.open('spec/assets/SampleImage.png') #Rack::Test::UploadedFile.new(Rails.root.join('spec/assets/SampleImage.png'), 'image/png')
            request.headers.merge!(valid_partner_api_session) # Add to request headers
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            inspection_report = {file: file, name: "SampleDoc.pdf"}
            property_photo = {file: image, name: "SampleImage.png"}
            parameters = {:key => "axium", :client => client, :property => property, :binder_template_id => template.id, :documents => [inspection_report], :property_photo => property_photo}
            post :create, params: parameters
            expect(response).to have_http_status(400)
            expect(JSON.parse(response.body)["message"]).to eq("You are not authorized to use template ID # #{template.id}")
            expect(ErrorService).to have_received(:perform_async).at_least(:once)
            expect(Partner::Automation::Error).to have_received(:build)
            expect(Partner::Automation::Error.last.error_message).to eq("You are not authorized to use template ID # #{template.id}")
        end
        it "Creates a binder and returns a 200 status" do
            request.headers.merge!(valid_partner_api_session) # Add to request headers
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            parameters = {:key => "axium", :client => client, :property => property, :binder_template_id => template_two.id, :transfer => true}

            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

            post :create, params: parameters
            expect(response).to have_http_status(201)
        end
        it "Creates a binder, adds an agent and returns a 200 status" do
            request.headers.merge!(valid_partner_api_session) # Add to request headers
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}
            parameters = {:key => "axium", :client => client, :property => property, :binder_template_id => template_two.id, :agent => agent, :transfer => true}

            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

            post :create, params: parameters
            expect(response).to have_http_status(201)
            agent = User.where(:role => "agent").last
            expect(agent.email).to eq("carmensmith@gmail.com")
        end
        it "Creates a binder, adds an agent, uploads an property_photo and returns a 200 status" do
            file = "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read)
            request.headers.merge!(valid_partner_api_session) # Add to request headers
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}
            property_photo = {file: file, name: "SampleImage.png"}
            parameters = {
                :key => "axium",
                :client => client,
                :property => property,
                :binder_template_id => template_two.id,
                :agent => agent,
                :transfer => true,
                :property_photo => property_photo,
                :method => "api"
            }

            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

            post :create, params: parameters
            expect(response).to have_http_status(201)
            agent = User.where(:role => "agent").last
            expect(agent.email).to eq("carmensmith@gmail.com")
            expect(Binder.first.images.length).to eq(1)
        end
        it "Creates a binder, adds an agent, uploads an inspection_report and returns a 200 status" do
            file = "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read)
            request.headers.merge!(valid_partner_api_session) # Add to request headers
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}
            inspection_report = {file: file, name: "SampleImage.png"}
            parameters = {
                :key => "axium",
                :client => client,
                :property => property,
                :binder_template_id => template_two.id,
                :agent => agent,
                :transfer => true,
                :documents => [inspection_report],
                :method => "api"
            }

            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

            post :create, params: parameters

            expect(response).to have_http_status(201)
            agent = User.where(:role => "agent").last
            expect(agent.email).to eq("carmensmith@gmail.com")
            expect(Binder.first.documents.length).to eq(1)
        end
        it "Creates a binder, adds an agent, uploads a property_photo, an inspection_report and returns a 200 status" do
            report = "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read)
            image = "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read)
            request.headers.merge!(valid_partner_api_session) # Add to request headers
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com"}
            property = {address: "321 Summer Street", address2: "", city: "Boston", state: "MA", postalcode: "02210", country: "US"}
            agent = {name: "Keller Williams", contact: "Carmen Smith", phone: "781-234-0991", email: "carmensmith@gmail.com"}
            inspection_report = {file: report, name: "SampleDoc.pdf"}
            property_photo = {file: image, name: "SampleImage.png"}
            parameters = {
                :key => "axium",
                :client => client,
                :property => property,
                :binder_template_id => template_two.id,
                :agent => agent,
                :transfer => true,
                :property_photo => property_photo,
                :documents => [inspection_report],
                :method => "api"
            }

            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

            post :create, params: parameters

            expect(response).to have_http_status(201)
            agent = User.where(:role => "agent").last
            expect(agent.email).to eq("carmensmith@gmail.com")
            expect(Binder.first.create_method).to eq("api")
            expect(Binder.first.images.length).to eq(1)
            expect(Binder.first.documents.length).to eq(1)
        end
        it "Creates a binder with additional fields to the API" do
            report = "data:application/pdf;base64," + Base64.encode64(File.open("spec/assets/SampleDoc.pdf", "rb").read)
            image = "data:image/png;base64," + Base64.encode64(File.open("spec/assets/SampleImage.png", "rb").read)
            request.headers.merge!(valid_partner_api_session) # Add to request headers
            client =  {first: "Mike", last: "Breen", email: "transfers@homebinder.com", phone: "1-230-000-2000"}
            property = {
                address: "321 Summer Street",
                address2: "",
                city: "Boston",
                state: "MA",
                postalcode: "02210",
                country: "US",
                county: "Suffolk",
                apn: "086-0-083-015",
                acres: 10,
                year_built: 1940,
                sq_ft: 4000
            }

            buyer_agent = {
                name: "Keller Williams",
                first: "Carmen",
                last: "Smith",
                phone: "781-234-0991",
                email: "carmensmith@gmail.com"
            }
            seller_agent = {
                name: "ERA",
                first: "Steve",
                last: "Smith",
                phone: "777-454-2132",
                email: "stevesmith@gmail.com"
            }
            inspection_report = {file: report, name: "SampleDoc.pdf"}
            property_photo = {file: image, name: "SampleImage.png"}
            parameters = {
                :key => "axium",
                :partner_email => member.email,
                :client => client,
                :property => property,
                :binder_template_id => template_two.id,
                :property_photo => property_photo,
                :documents => [inspection_report],
                :method => "api",
                :additional_images => [
                    {file: image, name: "SampleImage1.png"},
                    {file: image, name: "SampleImage2.png"},
                    {file: image, name: "SampleImage3.png"},
                    {file: image, name: "SampleImage4.png"}
                ],
                :buyer_agent => buyer_agent,
                :seller_agent => seller_agent,
                :maintenance_items => [
                    {
                        name: "Smoke Alarm",
                        do_date: Date.today + 5.weeks,
                        maintenance_cycle: "Years",
                        interval: 1,
                        details: "details"
                    }
                ],
                :projects => [
                    {
                        name: "Replace doors",
                        status: "Planning",
                        project_type: "replacement",
                        start_date: 3.weeks.ago,
                        end_date: Date.today,
                        cost: 0,
                        details: "details",
                    }
                ],
                :home_professionals => [
                    {
                        contact: "Joe Smith",
                        contractor_attributes: {
                            name: "Joe Smith Company",
                            contractor_type: "plumber",
                            phone: "781-423-4305",
                            email: "joesmith@gmail.com",
                            url: "joesmith.com",
                            address_attributes: {
                                address1: "123 Main Street",
                                address2: "Apt. 2",
                                city: "Boston",
                                state: "MA",
                                zip: "01906"
                            }
                        },
                        details: "Details"
                    }
                ],
                :appliances => [
                    {
                        name: "refrigerator",
                        manufacturer: "GE",
                        model: "R300SX",
                        serial_no: "ZL844934B",
                        install_date: 3.days.ago,
                        warranty: "Yes",
                        # purchase_attributes: {
                        #     store: nil,
                        #     price: nil,
                        #     date: nil
                        # },
                        details: "Yes",
                    }
                ]
            }

            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(parameters)

            post :create, params: parameters
            expect(response).to have_http_status(201)
            payload = JSON.parse(response.body)

            # verify property fields were saved
            binder = Binder.find payload["id"]
            expect(binder.create_method).to eq("api")
            expect(binder.property.county).to eq("Suffolk")
            expect(binder.property.apn).to eq("086-0-083-015")
            expect(binder.property.acres).to eq(10)
            # get the homeowner
            homeowner = User.where(:role => "homeowner").last
            # verify tag contains client information
            expect(homeowner.user_profile.first_name).to eq("Mike")
            expect(homeowner.user_profile.last_name).to eq("Breen")
            expect(homeowner.user_profile.mobile_phone).to eq("+12300002000")
            expect(homeowner.email).to eq("transfers@homebinder.com")
            # get binder owner/agents
            owner = User.with_role(:owner, binder).first
            b_agent = User.with_role(:buyer_agent, binder).first
            s_agent = User.with_role(:seller_agent, binder).first
            # verify brandings/ownership/roles
            expect(owner.email).to eq(member.email)
            expect(b_agent.email).to eq(buyer_agent[:email])
            expect(s_agent.email).to eq(seller_agent[:email])
            # verify images/documents
            expect(binder.images.count).to eq(5)
            expect(binder.documents.count).to eq(1)
            # verify transfer
            expect(binder.transfers.count).to eq(1)
            # verify binder items
            expect(binder.appliances.count).to eq(1)
            expect(binder.maintenance_items.count).to eq(1)
            expect(binder.projects.count).to eq(1)
            #expect(binder.binder_contractors.count).to eq(4) # includes 1 home pro, 1 partner, 2 agents
        end
        it "Creates a binder with large payload" do
            request.headers.merge!(valid_partner_api_session) # Add to request headers
            file = File.read("#{Rails.root}/spec/assets/large_abs_payload.json")
            payload = JSON.parse(file)

            parameters = {
                :key => "axium",
                :partner_email => member.email,
                :binder_template_id => template_two.id
            }

            payload.merge! parameters

            allow(JSON).to receive(:parse).and_call_original
            allow(JSON).to receive(:parse).with("", symbolize_names: true).and_return(payload)

            post :create, params: payload
            expect(response).to have_http_status(201)
        end
    end
end
