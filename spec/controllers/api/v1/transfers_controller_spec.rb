require 'rails_helper'
require 'mandrill'

RSpec.describe Api::V1::TransfersController, :type => :controller do

    let!(:user) { FactoryBot.create(:user, :role => "admin") }
    let!(:api_key) { FactoryBot.create(:api_key) }
    let!(:jwt) { UserTokenService.create_jwt(user) }
    let!(:file) { {version: "0.0.0"} }
    let!(:with) {FactoryBot.create(:user)}
    let!(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }
    
    before :each do
        allow(File).to receive(:read).with("/home/ubuntu/workspace/homebinder/app/views/transfer_mailer/notify_unregistered_email.html.erb")
        request.headers.merge!(valid_session) # Add to request headers
        @binder = create(:binder, name: "appliance tests", active: true, created_by: user.id)
        create(:subscription, binder_id: @binder.id, plan_id: 'free')
        UserBinder.create(:binder_id => @binder.id, :user_id => user.id, :role => "owner")
    end
  
    describe "GET #index" do
        context "get all transfers for a binder" do
            before do
                create(:transfer, status: "sent", sender_id: user.id, receiver_id: with.id, transfer_type: "ownership", binder_id: @binder.id)
                create(:transfer, status: "created", sender_id: user.id, receiver_id: with.id, transfer_type: "ownership", binder_id: create(:binder).id)
                get :index, params:  { binderId: @binder.id }
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
            it "returns the list of transfers" do
                response_body = JSON.parse(response.body)
                expect(response_body["total"]).to eq 1
            end
        end
    end
  
    describe "GET #show" do
        context "get a single transfer" do
            before do
                @transfer = create(:transfer, status: "sent", sender_id: user.id, receiver_id: with.id, transfer_type: "ownership", binder_id: @binder.id)
                get :show, params: { id: @transfer.id }
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
                it "returns the transfer" do
                transfer = JSON.parse(response.body)
                expect(transfer["id"]).to eq(@transfer.id)
            end
        end
    end
  
    describe "POST #create" do
        context "create a transfer" do
            before do
                transfer = {
                    transfer_type: "ownership",
                    binder_id: @binder.id,
                    status: "created",
                    sender_id: user.id,
                    receiver_id: with.id
                }
                post :create, params:  { transfer: transfer}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
                it "creates an transfer" do
                transfer = JSON.parse(response.body)
                expect(transfer["status"]).to eq("created")
            end
        end
    end
    
    describe "POST #execute" do
        context "executes a transfer" do
            before do
                transfer = create(:transfer)
                receiver = {
                    email: transfer.receiver.email,
                    first: "First",
                    last: "Last",
                    phone: nil
                }
                post :execute, params:  { id: transfer.id, user: receiver, transfer_type: "ownership"}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
  
    describe "PUT #update" do
        context "update a transfer" do
            before do
                transfer = create(:transfer, status: "sent", sender_id: user.id, receiver_id: with.id, transfer_type: "ownership", binder_id: @binder.id)
                put :update, params:  { id: transfer.id, transfer: { binder_id: @binder.id, status: "accepted" }}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
                it "updates a transfer" do
                transfer = JSON.parse(response.body)
                expect(transfer["status"]).to eq("accepted")
            end
        end
    end
    
    describe "PUT #cancel" do
        context "cancels a transfer" do
            before do
                transfer = create(:transfer, status: "sent", sender_id: user.id, receiver_id: with.id, transfer_type: "ownership", binder_id: @binder.id)
                put :cancel, params:  { id: transfer.id}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "PUT #resend" do
        context "resend a transfer" do
            before do
                transfer = create(:transfer, status: "sent", sender_id: user.id, receiver_id: with.id, transfer_type: "ownership", binder_id: @binder.id)
                put :resend, params:  { id: transfer.id, user: {first_name: "test", last_name: "name", email: "test@gmail.com", phone: "16175002600"}}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
  
    describe "DELETE #destroy" do
        context "delete a transfer" do
            before do
                transfer = create(:transfer, status: "sent", sender_id: user.id, receiver_id: with.id, transfer_type: "ownership", binder_id: @binder.id)
                delete :destroy, params:  { id: transfer.id }
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
  
end