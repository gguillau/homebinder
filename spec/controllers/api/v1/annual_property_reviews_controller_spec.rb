require 'rails_helper'

RSpec.describe Api::V1::AnnualPropertyReviewsController, :type => :controller do
    let(:model) {controller.controller_name.classify.constantize}
    let(:symbol) {controller.controller_name.singularize.to_sym}
    let(:user) { FactoryBot.create(:user, :role => "admin") }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    let!(:item) {create(symbol)}
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }
    let(:second_session) { { 'HB-APIKey' => api_key.key} }
    
    context "when token is in request headers" do
        before :each do
            allow(File).to receive(:read).and_call_original
                request.headers.merge!(valid_session) # Add to request headers
        end
    
        describe "GET #index" do
            context "get all annual_property_reviews" do
                before do
                    get :index, params: {}
                end
    
                it "responds with success" do
                    expect(response).to have_http_status(:success)
                end
    
                it "returns the list of annual_property_reviews" do
                    response_body = JSON.parse(response.body)
                    expect(response_body["total"]).to eq 1
                    expect(response_body["items"].length).to eq(1)
                end
            end
        end
    
        describe "GET #show" do
            context "get a single annual_property_review" do
                before do
                    get :show, params:  { id: item.id }
                end
    
                it "responds with success" do
                    expect(response).to have_http_status(:success)
                end
    
                it "returns the annual_property_review" do
                    annual_property_review = JSON.parse(response.body)
                    expect(annual_property_review["id"]).to eq(item.id)
                end
            end
                context "get a single annual_property_review by uuid" do
                before do
                    get :show, params:  { id: item.uuid }
                end
    
                it "responds with success" do
                    expect(response).to have_http_status(:success)
                end
    
                it "returns the annual_property_review" do
                    annual_property_review = JSON.parse(response.body)
                    expect(annual_property_review["id"]).to eq(item.id)
                end
            end
        end
    
        describe "POST #create" do
            context "create an annual_property_review" do
                before do
                    post :create, params:  { annual_property_review: FactoryBot.attributes_for(:annual_property_review, :state => "MA") }
                end
    
                it "responds with success" do
                    expect(response).to have_http_status(:success)
                end
    
                it "creates an annual_property_review" do
                    annual_property_review = JSON.parse(response.body)
                    expect(annual_property_review["state"]).to eq("MA")
                end
            end
        end
    
        describe "PUT #update" do
            context "update an annual_property_review" do
                before do
                    put :update, params: { id: item.id, annual_property_review: { id: item.id, client_email: "test@gmail.com"}}
                end
    
                it "responds with success" do
                    expect(response).to have_http_status(:success)
                end
    
                it "updates an annual_property_review" do
                    annual_property_review = JSON.parse(response.body)
                    expect(annual_property_review["client_email"]).to eq("test@gmail.com")
                end
            end
        end
    
        describe "DELETE #destroy" do
            context "delete an annual_property_review" do
                before do
                    delete :destroy, params:  { id: item.id }
                end
    
                it "responds with success" do
                    expect(response).to have_http_status(:success)
                end
            end
        end
        describe "GET #download" do
            context "downloads PDF report" do
                before do
                    allow_any_instance_of(AnnualPropertyReviews::AprPdfGenerator).to receive(:generate).and_return("")
                    get :download, params:  { id: item.id }
                end
    
                it "responds with success" do
                    expect(response).to have_http_status(:success)
                end
            end
        end
        describe "GET #messages" do
            context "sends message" do
                before do
                    get :messages, params:  { id: item.id }
                end
    
                it "responds with success" do
                    expect(response).to have_http_status(:success)
                end
            end
        end
        describe "POST #request_quote" do
            context "request quote" do
                before do
                    home_pro = create(:contractor_template)
                    post :request_quote, params:  { id: item.id, recommended_homepro_id: home_pro.id }
                end
    
                it "responds with success" do
                    expect(response).to have_http_status(:success)
                end
            end
        end
    end
    
    context "when token is in param headers" do
        before :each do
            allow(File).to receive(:read).and_call_original
                request.headers.merge!(second_session) # Add to request headers
        end
    
        describe "POST #request_quote" do
            context "request quote" do
                before do
                    home_pro = create(:contractor_template)
                    post :request_quote, params:  { id: item.id, recommended_homepro_id: home_pro.id }
                end
    
                it "responds with success" do
                    expect(response).to have_http_status(:success)
                end
            end
        end
    end
end
