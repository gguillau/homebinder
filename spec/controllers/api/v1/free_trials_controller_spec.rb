require 'rails_helper'

RSpec.describe Api::V1::FreeTrialsController, :type => :controller do

    let(:user) { FactoryBot.create(:user) }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
  
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        request.headers.merge!(valid_session) # Add to request headers
    end
    
    describe "PUT #create" do
        context "sends an email" do
            before do
                post :create, params: {email: "test", name: "test", phone: "+17810234234"}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end

end