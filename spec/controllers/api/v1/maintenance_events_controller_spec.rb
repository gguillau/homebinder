require 'rails_helper'

RSpec.describe Api::V1::MaintenanceEventsController, :type => :controller do
    
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:user) { FactoryBot.create(:user, email: "user@homebinder.com") }
    let(:binder) { FactoryBot.create(:binder) }
    let(:maintenance_item) {FactoryBot.create(:maintenance_item, binder_id: binder.id, created_by: user.id)}
    let(:item) {FactoryBot.create(:maintenance_item, binder_id: binder.id, created_by: user.id)}
    let(:maintenance_event) {FactoryBot.create(:maintenance_event, maintenance_item_id: maintenance_item.id)}
    let(:binder_contractor) {FactoryBot.create(:binder_contractor, binder_id: binder.id)}
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
  
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        UserBinder.create(:binder_id => binder.id, :user_id => user.id, :role => "owner")
        request.headers.merge!(valid_session) # Add to request headers
    end
    
    describe "GET #index" do
        context "gets all maintenance_events" do
            before do
                get :index, params: {maintenance_item_id: maintenance_item.id}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "GET #show" do
        context "gets the maintenance_event" do
            before do
                get :show, params: {:id => maintenance_event.id}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "POST #create" do
        context "creates a maintenance_event" do
            before do
                post :create, params: {maintenance_event: {maintenance_item_id: maintenance_item.id, created_by: user.id, do_date: Date.today + 5.months}}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "PUT #update" do
        context "updates a maintenance_event" do
            before do
                put :update, params: {id: maintenance_event.id, maintenance_event: maintenance_event.attributes.symbolize_keys.except(:id)}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "DELETE #destroy" do
        context "updates a maintenance_event" do
            before do
                delete :destroy, params: {id: maintenance_event.id}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
end
