require 'rails_helper'

RSpec.describe Api::V1::AppliancesController, :type => :controller do

  let(:user) { FactoryBot.create(:user) }
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

  before :each do
    
    request.headers.merge!(valid_session) # Add to request headers
    
    @binder = create(:binder, name: "appliance tests", active: true, created_by: user.id)
    create(:subscription, binder_id: @binder.id, plan_id: 'free')
    UserBinder.create(:binder_id => @binder.id, :user_id => user.id, :role => "owner")
  end
  
  describe "GET #index" do
    context "get all appliances for a binder" do
      before do
        create(:appliance, name: "appliance a", binder_id: @binder.id)
        create(:appliance, name: "appliance b", binder_id: create(:binder).id)
        get :index, params:  { binderId: @binder.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the list of appliances" do
        response_body = JSON.parse(response.body)
        expect(response_body["total"]).to eq 1
      end
    end
  end
  
  describe "GET #show" do
    context "get a single appliance" do
      before do
        @appliance = create(:appliance, name: "get me", binder_id: @binder.id)
        get :show, params: { id: @appliance.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the appliance" do
        appliance = JSON.parse(response.body)
        expect(appliance["id"]).to eq(@appliance.id)
      end
    end
  end
  
  describe "POST #send_test_email" do
    before do
      @appliance = create(:appliance, name: "get me", binder_id: @binder.id)
      post :send_test_email, params: { id: @appliance.id }
    end
    
    it "responds with success" do
      expect(response).to have_http_status(:success)
    end
  end
  
  describe "POST #check_for_recalls" do
    before do
      @appliance = create(:appliance, name: "get me", binder_id: @binder.id)
      post :check_for_recalls, params: { id: @appliance.id }
    end
    
    it "responds with success" do
      expect(response).to have_http_status(:success)
    end
  end
  
  describe "POST #create" do
    context "create an appliance" do
      before do
        post :create, params:  { appliance: { binder_id: @binder.id, name: "new appliance" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "creates an appliance" do
        appliance = JSON.parse(response.body)
        expect(appliance["name"]).to eq("new appliance")
      end
    end
  end
  
  describe "PUT #update" do
    context "update an appliance" do
      before do
        appliance = create(:appliance, name: "created", binder_id: @binder.id)
        put :update, params:  { id: appliance.id, appliance: { binder_id: @binder.id, name: "updated" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "updates an appliance" do
        appliance = JSON.parse(response.body)
        expect(appliance["name"]).to eq("updated")
      end
    end
  end
  
  describe "DELETE #destroy" do
    context "delete an appliance" do
      before do
        appliance = create(:appliance, name: "created", binder_id: @binder.id)
        delete :destroy, params:  { id: appliance.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
    end
  end
  
end