require 'rails_helper'

RSpec.describe Api::V1::WidgetExclusionsController, type: :controller do
    let(:user) { FactoryBot.create(:user, role: UserGlobalRoles::ADMIN) }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    let(:widget) {FactoryBot.create(:widget)}
    let(:partner) {FactoryBot.create(:partner)}

    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        request.headers.merge!(valid_session) # Add to request headers
    end

    describe "POST #create" do
        context "create a widget exclusion" do
            before do
                post :create, params: { widget_exclusion: { partner_id: partner.id, widget_id: widget.id }}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "creates a widget_exclusion" do
                widget_exclusion = JSON.parse(response.body)
                expect(widget_exclusion["partner_id"]).to eq(partner.id)
            end
        end
    end

    describe "DELETE #destroy" do
        context "delete a widget_exclusion" do
            before do
                widget_exclusion = create(:widget_exclusion)
                delete :destroy, params: { id: widget_exclusion.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:no_content)
            end
        end
    end
end
