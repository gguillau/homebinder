require 'rails_helper'

RSpec.describe Api::V1::SellerReportsController, :type => :controller do
    
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:user) { FactoryBot.create(:user, :role => "admin") }
    let(:binder) { FactoryBot.create(:binder) }
    let(:subscription) {FactoryBot.create(:subscription, binder_id: binder.id, plan_id: "free")}
    let(:seller_report) { FactoryBot.create(:seller_report, :binder_id => binder.id)}
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
  
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        binder.subscription = subscription
        binder.save
        allow(File).to receive(:read)
        UserBinder.create(:binder_id => binder.id, :user_id => user.id, :role => "owner")
        request.headers.merge!(valid_session) # Add to request headers
    end
    
    describe "GET #index" do
        context "gets the report" do
            before do
                get :index, params: {}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "POST #create" do
        context "creates the report" do
            before do
                post :create, params: {seller_report: {binder_id: binder.id}}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "GET #show" do
        context "gets the report" do
            before do
                get :show, params: {id: seller_report.code}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "PUT #update" do
        context "updates the report" do
            before do
                        create_list(:appliance, 2, :binder => binder)
                create_list(:maintenance_item, 2, :binder => binder)
                create_list(:binder_contractor, 2, :binder => binder)
                create_list(:project, 2, :binder => binder, :status => "Completed")
                create_list(:paint, 2, :binder => binder)
                create_list(:finish, 2, :binder => binder)
                create_list(:document, 2, :binder => binder)
                create_list(:image, 2, :binder => binder)
                create_list(:permit, 2, :binder => binder)
                        binder.appliances.each do |app|
                    create(:seller_report_item, :appliance_id => app.id, :include => true)
                end
                        binder.maintenance_items.each do |item|
                    create(:seller_report_item, :maintenance_item_id => item.id, :include => true)
                end
                        binder.projects.each do |project|
                    create(:seller_report_item, :project_id => project.id, :include => true)
                end
                        binder.binder_contractors.each do |binder_contractor|
                    create(:seller_report_item, :binder_contractor_id => binder_contractor.id, :include => true)
                end
                        binder.paints.each do |paint|
                    create(:seller_report_item, :paint_id => paint.id, :include => true)
                end
                        binder.finishes.each do |finish|
                    create(:seller_report_item, :finish_id => finish.id, :include => true)
                end
                        binder.documents.each do |doc|
                    create(:seller_report_item, :document_id => doc.id, :include => true)
                end
                        binder.images.each do |img|
                    create(:seller_report_item, :image_id => img.id, :include => true)
                end
                        binder.permits.each do |pt|
                    create(:seller_report_item, :permit_id => pt.id, :include => true)
                end
                        create_list(:appliance, 2, :binder => binder)
                create_list(:maintenance_item, 2, :binder => binder)
                create_list(:binder_contractor, 2, :binder => binder)
                create_list(:project, 2, :binder => binder, :status => "Completed")
                create_list(:paint, 2, :binder => binder)
                create_list(:finish, 2, :binder => binder)
                create_list(:document, 2, :binder => binder)
                create_list(:image, 2, :binder => binder)
                create_list(:permit, 2, :binder => binder)
                        report = seller_report.attributes.symbolize_keys.except(:id)
                        params = {
                    id: seller_report.id, 
                    seller_report: report, 
                    images: [binder.images.pluck(:id)],
                    documents: [binder.documents.pluck(:id)],
                    appliances: [binder.appliances.pluck(:id)],
                    maintenance: [binder.maintenance_items.pluck(:id)],
                    improvements: [binder.projects.pluck(:id)],
                    contractors: [binder.binder_contractors.pluck(:id)],
                    finishes: [binder.finishes.pluck(:id)]
                }
                put :update, params: params
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
end
