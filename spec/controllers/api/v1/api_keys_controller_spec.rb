require 'rails_helper'

RSpec.describe Api::V1::ApiKeysController, :type => :controller do

  let(:user) { FactoryBot.create(:user, :role => "admin") }
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

  before :each do
    
    request.headers.merge!(valid_session) # Add to request headers
  end
  
  describe "POST #create" do
    context "create an apikey" do
      before do
        partner = create(:partner)
        post :create, params:  {partner: {company_name: partner.name, partner_id: partner.id, application_name: "Website", contact_email: partner.email}}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
    end
  end
  
end