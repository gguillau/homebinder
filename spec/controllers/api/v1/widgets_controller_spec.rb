require 'rails_helper'

RSpec.describe Api::V1::WidgetsController, :type => :controller do

  let(:user) { FactoryBot.create(:user, role: UserGlobalRoles::ADMIN) }
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

  before :each do
    
    request.headers.merge!(valid_session) # Add to request headers
  end
  
  describe "GET #index" do
    context "get all widgets" do
      before do
        create(:widget)
        create(:widget)
        get :index
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the list of widgets" do
        response_body = JSON.parse(response.body)
        expect(response_body["total"]).to eq 2
      end
    end
  end
  
  describe "GET #show" do
    context "get a single widget" do
      before do
        @widget = create(:widget)
        get :show, params: { id: @widget.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the widget" do
        widget = JSON.parse(response.body)
        expect(widget["id"]).to eq(@widget.id)
      end
    end
  end
  
  describe "POST #create" do
    context "create an widget" do
      before do
        post :create, params: { widget: { name: "widget", key: "widget", category: "General" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "creates an widget" do
        widget = JSON.parse(response.body)
        expect(widget["name"]).to eq("widget")
      end
    end
  end
  
  describe "PUT #update" do
    context "update an widget" do
      before do
        widget = create(:widget)
        put :update, params: { id: widget.id, widget: { name: "widget", key: "widget", category: "General" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "updates an widget" do
        widget = JSON.parse(response.body)
        expect(widget["name"]).to eq("widget")
      end
    end
  end
  
  describe "DELETE #destroy" do
    context "delete an widget" do
      before do
        widget = create(:widget)
        delete :destroy, params: { id: widget.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:no_content)
      end
    end
  end
  
end