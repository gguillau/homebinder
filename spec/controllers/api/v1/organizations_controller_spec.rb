require 'rails_helper'

RSpec.describe Api::V1::OrganizationsController, :type => :controller do
  let(:user) { FactoryBot.create(:user, role: :admin) }
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

  before :each do
    
    request.headers.merge!(valid_session) # Add to request headers
  end
  
  describe "GET #index" do
    context "get organizations" do
      before do
        create_list(:organization, 10)
        get :index
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the list of organizations" do
        response_body = JSON.parse(response.body)
        expect(response_body["items"].length).to eq 10
      end
    end
  end
  
  describe "POST #create" do
    context "create organization" do
      before do
        post :create, params: { organization: { name: "org", description: "desc" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the organization" do
        response_body = JSON.parse(response.body)
        expect(response_body["name"]).to eq "org"
      end
    end
  end
  
  describe "PUT #update" do
    context "update organization" do
      before do
        org = create(:organization)
        put :update, params: { id: org.id, organization: { id: org.id, name: "org", description: "desc" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "updates the organization" do
        response_body = JSON.parse(response.body)
        expect(response_body["name"]).to eq "org"
      end
    end
  end
  
  describe "DELETE #destroy" do
    context "delete organization" do
      before do
        org = create(:organization)
        put :destroy, params: { id: org.id }
      end
      
      it "responds with no content" do
        expect(response).to have_http_status(:no_content)
      end
    end
  end
  
  describe "POST #add_partners" do
    context "add partners" do
      before do
        org = create(:organization)
        partner = create(:partner)
        post :add_partners, params: { id: org.id, partners: [ partner.id ]}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:no_content)
      end
    end
  end
  
  describe "DELETE #remove_partners" do
    context "remove partners" do
      before do
        org = create(:organization)
        partner = create(:partner)
        create(:organization_partner, organization_id: org.id, partner_id: partner.id)
        put :remove_partner, params: { id: org.id, partner_id: partner.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:no_content)
      end
    end
  end
  
  describe "POST #add_users" do
    context "add users" do
      before do
        org = create(:organization)
        user = create(:user)
        post :add_users, params: { id: org.id, users: [ user.id ]}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:no_content)
      end
    end
  end
  
  describe "DELETE #remove_users" do
    context "remove partners" do
      before do
        org = create(:organization)
        user = create(:user)
        create(:organization_user, organization_id: org.id, user_id: user.id)
        put :remove_user, params: { id: org.id, user_id: user.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:no_content)
      end
    end
  end
  
end