require 'rails_helper'

RSpec.describe Api::V1::AccountsController, :type => :controller do

    let(:model) {controller.controller_name.classify.constantize}
    let(:symbol) {controller.controller_name.singularize.to_sym}
    let(:user) { FactoryBot.create(:user, :role => "admin") }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    let!(:item) {create(symbol)}
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        allow(File).to receive(:read).and_call_original
        request.headers.merge!(valid_session) # Add to request headers
    end

    describe "GET #index" do
        context "get all accounts" do
            before do
                get :index, params: {}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "returns the list of accounts" do
                response_body = JSON.parse(response.body)
                expect(response_body["total"]).to eq 1
                expect(response_body["items"].length).to eq(1)
            end
        end
    end

    describe "GET #show" do
        context "get a single account" do
            before do
                get :show, params:  { id: item.manager.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "returns the account" do
                account = JSON.parse(response.body)
                expect(account["id"]).to eq(item.id)
            end
        end
    end

    describe "POST #create" do
        context "create an account" do
            before do
                partner = create(:partner)
                post :create, params:  { account: { name: "test account", manager_id: partner.id, account_type: "single", account_sub_type: "free_trial", account_status: "active", trial_expiration: Time.now.to_s }}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "creates an account" do
                account = JSON.parse(response.body)
                expect(account["name"]).to eq("test account")
            end
        end
    end

    describe "PUT #update" do
        context "update an account" do
            before do
                put :update, params: { id: item.id, account: { id: item.id, name: "updated account", trial_expiration: Date.today}}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "updates an account" do
                account = JSON.parse(response.body)
                expect(account["name"]).to eq("updated account")
            end
        end
    end

    describe "DELETE #destroy" do
        context "delete an account" do
            before do
                delete :destroy, params:  { id: item.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
end
