require 'rails_helper'

RSpec.describe Api::V1::AreasController, :type => :controller do

  let(:user) { FactoryBot.create(:user) }
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

  before :each do
    
    request.headers.merge!(valid_session) # Add to request headers
    
    @binder = create(:binder, name: "area tests", active: true, created_by: user.id)
    create(:subscription, binder_id: @binder.id, plan_id: 'free')
    UserBinder.create(:binder_id => @binder.id, :user_id => user.id, :role => "owner")
  end
  
  describe "GET #index" do
    context "get all areas for a binder" do
      before do
        create(:area, name: "area a", binder_id: @binder.id)
        create(:area, name: "area b", binder_id: create(:binder).id)
        get :index, params:  { binderId: @binder.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the list of appliances" do
        response_body = JSON.parse(response.body)
        expect(response_body["total"]).to eq 1
      end
    end
  end
  
  describe "GET #show" do
    context "get a single area" do
      before do
        @area = create(:area, name: "get me", binder_id: @binder.id)
        get :show, params:  { id: @area.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the area" do
        area = JSON.parse(response.body)
        expect(area["id"]).to eq(@area.id)
      end
    end
  end
  
  describe "POST #create" do
    context "create an area" do
      before do
        post :create, params: { area: { binder_id: @binder.id, name: "new area" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "creates an area" do
        area = JSON.parse(response.body)
        expect(area["name"]).to eq("new area")
      end
    end
  end
  
  describe "PUT #update" do
    context "update an area" do
      before do
        area = create(:area, name: "created", binder_id: @binder.id)
        put :update, params:  { id: area.id, area: { binder_id: @binder.id, name: "updated" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "updates an area" do
        area = JSON.parse(response.body)
        expect(area["name"]).to eq("updated")
      end
    end
  end
  
  describe "DELETE #destroy" do
    context "delete an area" do
      before do
        area = create(:area, name: "created", binder_id: @binder.id)
        delete :destroy, params:  { id: area.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
    end
  end
  
end