require 'rails_helper'

RSpec.describe Api::V1::SellerReportPdfsController, :type => :controller do
    
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:user) { FactoryBot.create(:user) }
    let(:binder) { FactoryBot.create(:binder) }
    let(:partner) {FactoryBot.create(:partner)}
    let(:seller_report) { FactoryBot.create(:seller_report, :binder_id => binder.id)}
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
  
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        service = SellerReportPdfService.new(seller_report)
        service.init
    
        allow_any_instance_of(FullSellerReportPdfService).to receive(:create).and_return(service.pdf)
        allow_any_instance_of(SellerReportPdfService).to receive(:create).and_return(service.pdf)
    
        Tags::SetResourcesTagsJob.perform_async "Binder", binder.id, [{ tag: "partner_#{partner.id}"}]
        binder.save
        UserBinder.create(:binder_id => binder.id, :user_id => user.id, :role => "owner")
        request.headers.merge!(valid_session) # Add to request headers
    end
    
    describe "GET #show" do
        context "gets the report" do
            before do
                get :show, params: {id: seller_report.code}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
        context "gets the full report" do
            before do
                get :show, params: {id: seller_report.code, full: true}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
end
