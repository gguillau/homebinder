require 'rails_helper'

RSpec.describe Api::V1::RepairPricerReportsController, :type => :controller do
    
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:admin) { FactoryBot.create(:user, :role => "admin") }
    let(:jwt) { UserTokenService.create_jwt(admin) }
    let(:file) { {version: "0.0.0"} }
    let(:report) {create(:repair_pricer_report, :repair_report => File.new("spec/assets/SampleDoc.pdf"), :inspection_report => File.new("spec/assets/SampleDoc.pdf"), :pool_report => File.new("spec/assets/SampleDoc.pdf"), :home_history_report => File.new("spec/assets/SampleDoc.pdf"))}
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        request.headers.merge!(valid_session) # Add to request headers
    end
    
    describe "GET #index" do
        context "gets the reports" do
            before do
                get :index, params: {}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "POST #create" do
        context "creates a report" do
            before do
                Sidekiq::Testing.disable!
                payload = {
                    client_first: "John",
                    client_last: "Smith",
                    client_email: "jsmith@gmail.com",
                    client_phone: "+13444444444",
                    buyer_agent_first: "Mary",
                    buyer_agent_last: "Smith",
                    buyer_agent_email: "msmith@gmail.com",
                    order_pool_report: true,
                    order_home_history_report: true,
                    rush_report: true
                }
                address = {
                    address1: "123 Main Street",
                    address2: "Apt. 2",
                    city: "Boston",
                    state: "MA",
                    country: "US",
                    zip: "02210"
                }
                post :create, params: {repair_pricer_report: payload, address: address, payment: {}}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "PUT #update" do
        context "updates a report" do
            before do
                put :update, params: {id: report.id, repair_pricer_report: {payment_status: "paid"}}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "DELETE #destroy" do
        context "deletes a report" do
            before do
                delete :destroy, params: {id: report.id}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
end
