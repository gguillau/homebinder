require 'rails_helper'

RSpec.describe Api::V1::AnnualPropertyReviewPhotosController, :type => :controller do
    let(:model) {controller.controller_name.classify.constantize}
    let(:symbol) {controller.controller_name.singularize.to_sym}
    let(:user) { FactoryBot.create(:user, :role => "admin") }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    let!(:item) {create(symbol)}
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        allow(File).to receive(:read).and_call_original
        request.headers.merge!(valid_session) # Add to request headers
    end

    describe "GET #index" do
        context "get all annual_property_review_photos" do
            before do
                get :index, params: {}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "returns the list of annual_property_review_photos" do
                response_body = JSON.parse(response.body)
                expect(response_body["total"]).to eq 1
                expect(response_body["items"].length).to eq(1)
            end
        end
    end

    describe "GET #show" do
        context "get a single annual_property_review_photo" do
            before do
                get :show, params:  { id: item.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "returns the annual_property_review_photo" do
                annual_property_review_photo = JSON.parse(response.body)
                expect(annual_property_review_photo["id"]).to eq(item.id)
            end
        end
    end

    describe "POST #create" do
        context "create an annual_property_review_photo" do
            before do
                apr = create(:annual_property_review)
                post :create, params:  {  annual_property_review_id: apr.id, description: "test", file: fixture_file_upload("spec/assets/SampleImage.png", "image/png") }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "creates an annual_property_review_photo" do
                annual_property_review_photo = JSON.parse(response.body)
                expect(annual_property_review_photo["description"]).to eq("test")
            end
        end
    end

    describe "PUT #update" do
        context "update an annual_property_review_photo" do
            before do
                put :update, params: { id: item.id, annual_property_review_photo: { id: item.id, description: "updated"}}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "updates an annual_property_review_photo" do
                annual_property_review_photo = JSON.parse(response.body)
                expect(annual_property_review_photo["description"]).to eq("updated")
            end
        end
    end

    describe "DELETE #destroy" do
        context "delete an annual_property_review_photo" do
            before do
                delete :destroy, params:  { id: item.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
end
