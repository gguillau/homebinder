require 'rails_helper'

RSpec.describe Api::V1::UsersController, :type => :controller do
    
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:user) { FactoryBot.create(:user, :role => "admin") }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        request.headers.merge!(valid_session) # Add to request headers
    end
    
    describe "GET #index" do
        context "gets all users" do
            before do
                get :index, params: {}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "GET #show" do
        context "gets all users" do
            before do
                get :show, params: {id: user.id}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "GET #welcome" do
        before do
            user.invite!
            get :welcome, params: {id: user.access_token}
        end
    
        it "responds with success" do
            expect(response).to have_http_status(:success)
        end
    end
    
    describe "PUT #invite" do
        before do
            put :invite, params: {id: user.id}
        end
    
        it "responds with success" do
            expect(response).to have_http_status(:success)
        end
    end
    
    describe "PUT #change_email" do
        context "updates the email for the user" do

            it "responds with success" do
                put :change_email, params: {:id => user.id, :user => {:email => "new_email@test.com"}, :password => user.password, :email => user.email}
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "PUT #change_password" do
        context "updates the password for the user" do

            it "responds with success" do
                put :change_password, params: {:id => user.id, :user => {:password => "newpassword"}, :password => user.password}
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "PUT #confirm_password" do
        context "confirms the password for the user" do
            it "responds with success" do
                token = user.send_reset_password_instructions
                put :confirm_password, params: {:id => user.id, :token => token}
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "POST #reset_password" do
        context "reset the password for the user" do
            before do
                post :reset_password, params: {user: {email: user.email}}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end

    describe "PUT #update_password" do
        context "updates the password for the user" do

            it "responds with success" do

                reset = User.find(user.id)
                token = reset.send_reset_password_instructions
                password = Faker::Internet.password
                data = {
                    email: reset.email,
                    reset_password_token: token,
                    password: password,
                    password_confirmation: password
                }
                put :update_password, params: {id: 99, user: data}
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "GET #find_user_by_email" do
        context "finds users by email" do

            it "responds with success" do
                get :find_user_by_email, params: {email: "test@gmail.com"}
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "DELETE #destroy" do
        context "deletes the user" do
            before do
                delete :destroy, params: {id: user.id}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
end
