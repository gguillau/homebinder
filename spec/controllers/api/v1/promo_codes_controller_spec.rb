require 'rails_helper'

RSpec.describe Api::V1::PromoCodesController, :type => :controller do
    
    let(:model) {controller.controller_name.classify.constantize}
    let(:symbol) {controller.controller_name.singularize.to_sym}
    let(:user) { FactoryBot.create(:user, :role => "admin") }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let!(:item) {create(symbol)}
    let!(:item_2) {create(symbol, :amount_off => 10)}
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        request.headers.merge!(valid_session) # Add to request headers
    end

    describe "GET #index" do
        context "get all items" do
            before do
                get :index, params: {}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "returns the list of items" do
                response_body = JSON.parse(response.body)
                expect(response_body["total"]).to eq 2
                expect(response_body["items"].length).to eq(2)
            end
        end
    end

    describe "GET #show" do
        context "get a single item" do
            before do
                get :show, params:  { id: item.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "returns the item" do
                returned_item = JSON.parse(response.body)
                expect(returned_item["id"]).to eq(item.id)
            end
        end
    end
    
    describe "POST #verify" do
        context "verify code exists with percent_off" do
            before do
                post :verify, params:  {:amount => 10, :promoCode => item.name}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
        
        context "verify code exists with amount_off" do
            before do
                post :verify, params:  {:amount => 10, :promoCode => item_2.name}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end

    describe "POST #create" do
        context "create a type" do
            before do
                post :create, params:  { "#{symbol}": { name: "test item" }}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "creates a item" do
                created_item = JSON.parse(response.body)
                expect(created_item["name"]).to eq("test item")
            end
        end
    end

    describe "PUT #update" do
        context "update a item" do
            before do
                put :update, params: { id: item.id, "#{symbol}": { name: "updated item"}}
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end

            it "updates a item" do
                updated_item = JSON.parse(response.body)
                expect(updated_item["name"]).to eq("updated item")
            end
        end
    end

    describe "DELETE #destroy" do
        context "delete a item" do
            before do
                delete :destroy, params:  { id: item.id }
            end

            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
end
