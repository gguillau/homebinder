require 'rails_helper'

RSpec.describe Api::V1::ApplianceTemplatesController, :type => :controller do

  let(:user) { FactoryBot.create(:user, :role => "inspector") }
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

  before :each do
    
    request.headers.merge!(valid_session) # Add to request headers
    @partner = create(:partner)
    @config = @partner.partner_configuration
    @template = create(:binder_template, partner_configuration_id: @config.id)
  end
  
  describe "GET #index" do
    it "partners appliance templates" do
      user.add_role :partner_admin, @partner
      
      create(:appliance_template, binder_template_id: @template.id)
      
      get :index, params:  {:binder_template_id => @template.id}
      
      expect(response).to have_http_status(:ok)
    end
  end
  
  describe "GET #show" do
    it "gets a template" do
      user.add_role :partner_admin, @partner
      
      a = create(:appliance_template, binder_template_id: @template.id)
      
      get :show, params:  {:id => a.id}
      
      expect(response).to have_http_status(:ok)
    end
  end
  
  describe "POST #create" do
    it "creates a template" do
      user.add_role :partner_admin, @partner
      
      post :create, params:  {:binder_template_id => @template.id, :appliance_template => { :name => "appliance", :binder_template_id => @template.id }}
      
      expect(response).to have_http_status(:ok)
    end
  end
  
  describe "PUT #update" do
    it "updates a template" do
      user.add_role :partner_admin, @partner
      
      a = create(:appliance_template, binder_template_id: @template.id)
      
      put :update, params:  {:id => a.id, :binder_template => { :id => a.id, :name => "appliances" }, :appliance_template => { :name => "appliance" }}
      expect(response).to have_http_status(:ok)
    end
  end
  
  describe "DELETE #destroy" do
    it "deletes a template" do
      user.add_role :partner_admin, @partner
      
      a = create(:appliance_template, binder_template_id: @template.id)
      
      delete :destroy, params:  {:id => a.id}
      
      expect(response).to have_http_status(:no_content)
    end
  end
end