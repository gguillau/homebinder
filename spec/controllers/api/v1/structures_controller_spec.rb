require 'rails_helper'

RSpec.describe Api::V1::StructuresController, :type => :controller do

  let(:user) { FactoryBot.create(:user) }
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

  before :each do
    
    request.headers.merge!(valid_session) # Add to request headers
    
    @binder = create(:binder, name: "structure tests", active: true, created_by: user.id)
    create(:subscription, binder_id: @binder.id, plan_id: 'free')
    UserBinder.create(:binder_id => @binder.id, :user_id => user.id, :role => "owner")
  end
  
  describe "GET #index" do
    context "get all structures for a binder" do
      before do
        create(:structure, name: "structure a", binder_id: @binder.id)
        create(:structure, name: "structure b", binder_id: create(:binder).id)
        get :index, params: { binderId: @binder.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the list of structures" do
        response_body = JSON.parse(response.body)
        expect(response_body["total"]).to eq 1
      end
    end
  end
  
  describe "GET #show" do
    context "get a single structure" do
      before do
        @structure = create(:structure, name: "get me", binder_id: @binder.id)
        get :show, params: { id: @structure.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the structure" do
        structure = JSON.parse(response.body)
        expect(structure["id"]).to eq(@structure.id)
      end
    end
  end
  
  describe "POST #create" do
    context "create an structure" do
      before do
        post :create, params: { structure: { binder_id: @binder.id, name: "new structure" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "creates an structure" do
        structure = JSON.parse(response.body)
        expect(structure["name"]).to eq("new structure")
      end
    end
  end
  
  describe "PUT #update" do
    context "update an structure" do
      before do
        structure = create(:structure, name: "created", binder_id: @binder.id)
        put :update, params: { id: structure.id, structure: { binder_id: @binder.id, name: "updated" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "updates an structure" do
        structure = JSON.parse(response.body)
        expect(structure["name"]).to eq("updated")
      end
    end
  end
  
  describe "DELETE #destroy" do
    context "delete an structure" do
      before do
        structure = create(:structure, name: "created", binder_id: @binder.id)
        delete :destroy, params: { id: structure.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
    end
  end
  
end