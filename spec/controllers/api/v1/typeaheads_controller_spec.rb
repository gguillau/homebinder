require 'rails_helper'

RSpec.describe Api::V1::TypeaheadsController, :type => :controller do

  let(:user) { FactoryBot.create(:user) }
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

  before :each do
    
    request.headers.merge!(valid_session) # Add to request headers

  end
  
  describe "GET #get_typeaheads" do
    it "get typeaheads for respective class" do
      
      get 'get_typeaheads', params: {klass: Binder::Appliance::Manufacturer}
      
      expect(response).to have_http_status(:ok)
      
    end
  end
  
  describe "GET #get_unverified_typeaheads" do
    it "get unverified typeaheads for respective class" do
      
      get 'get_unverified_typeaheads', params: {klass: Binder::Appliance::Manufacturer}
      
      expect(response).to have_http_status(:ok)
      
    end
  end
  
  describe "PUT #save_unverified_typeahead" do
    it "save unverified typeahead for respective class" do
      
      app = create(:appliance_manufacturer, verified: false, name: "Kenmore")
      
      put 'save_unverified_typeahead', params: {klass: Binder::Appliance::Manufacturer, id: app.id, typeahead: {name: "Kenmore", verified: true}}
      
      expect(response).to have_http_status(:ok)
      
    end
  end
  
  describe "DELETE #remove_unverified_typeahead" do
    it "removes an unverified typeahead" do
      
      app_manu = create(:appliance_manufacturer, verified: false, name: "Test")
      
      delete 'remove_unverified_typeahead', params: {klass: Binder::Appliance::Manufacturer, :id => app_manu.id}
      
      expect(response).to have_http_status(:no_content)
    end
  end
  
end