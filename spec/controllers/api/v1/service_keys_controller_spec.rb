require 'rails_helper'

RSpec.describe Api::V1::ServiceKeysController, :type => :controller do
    
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:file) { {version: "0.0.0"} }
    let(:valid_session) { { 'HB-APIKey' => api_key.key} }
    
    before :each do
        request.headers.merge!(valid_session) # Add to request headers
    end
    
    describe "GET #index" do
        context "gets the keys" do
            before do
                get :index, params: {}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
end
