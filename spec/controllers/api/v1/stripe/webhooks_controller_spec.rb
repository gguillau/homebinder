require 'rails_helper'

RSpec.describe Api::V1::Stripe::WebhooksController, :type => :controller do
    describe "PUT #update" do
        context "does the thing" do
            it "responds with success" do
                allow(Stripe::ParseWebhookJob).to receive(:perform_async)
                put :update, params: {"mandrill_events" => "[{\"event\":\"hard_bounce\",\"msg\":{\"ts\":1365109999,\"subject\":\"This an example webhook message\",\"email\":\"example.webhook@mandrillapp.com\",\"sender\":\"example.sender@mandrillapp.com\",\"tags\":[\"webhook-example\"],\"state\":\"bounced\",\"metadata\":{\"user_id\":111},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"_version\":\"exampleaaaaaaaaaaaaaaa\",\"bounce_description\":\"bad_mailbox\",\"bgtools_code\":10,\"diag\":\"smtp;550 5.1.1 The email account that you tried to reach does not exist. Please try double-checking the recipient's email address for typos or unnecessary spaces.\"},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"ts\":1477310208},{\"event\":\"hard_bounce\",\"msg\":{\"ts\":1365109999,\"subject\":\"This an example webhook message\",\"email\":\"example.webhook@mandrillapp.com\",\"sender\":\"example.sender@mandrillapp.com\",\"tags\":[\"webhook-example\"],\"state\":\"bounced\",\"metadata\":{\"user_id\":111},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa1\",\"_version\":\"exampleaaaaaaaaaaaaaaa\",\"bounce_description\":\"bad_mailbox\",\"bgtools_code\":10,\"diag\":\"smtp;550 5.1.1 The email account that you tried to reach does not exist. Please try double-checking the recipient's email address for typos or unnecessary spaces.\"},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa1\",\"ts\":1477310208}]"}
                expect(Stripe::ParseWebhookJob).to have_received(:perform_async)
                expect(response).to have_http_status(:no_content)
            end
        end
    end

    context "handling rescue_from" do
        controller do
            def index
            end
        end
        errors = [
            {name: JSON::ParserError, status: 400},
            {name: Stripe::SignatureVerificationError.new("", ""), status: 400}
        ]
        errors.each do |error|
            it "raises an #{error[:name]} and returns #{error[:status]}" do
                allow(controller).to receive(:index).and_raise(error[:name], error[:message])
                get :index, params: nil
                expect(response).to have_http_status(error[:status])
            end
        end
    end
end
