require 'rails_helper'

RSpec.describe Api::V1::Mandrill::WebhooksController, :type => :controller do
    
    describe "POST #index" do
        context "does the thing" do
            it "responds with success" do
                post :update, params: {"mandrill_events" => "[{\"event\":\"hard_bounce\",\"msg\":{\"ts\":1365109999,\"subject\":\"This an example webhook message\",\"email\":\"example.webhook@mandrillapp.com\",\"sender\":\"example.sender@mandrillapp.com\",\"tags\":[\"webhook-example\"],\"state\":\"bounced\",\"metadata\":{\"user_id\":111},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"_version\":\"exampleaaaaaaaaaaaaaaa\",\"bounce_description\":\"bad_mailbox\",\"bgtools_code\":10,\"diag\":\"smtp;550 5.1.1 The email account that you tried to reach does not exist. Please try double-checking the recipient's email address for typos or unnecessary spaces.\"},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa\",\"ts\":1477310208},{\"event\":\"hard_bounce\",\"msg\":{\"ts\":1365109999,\"subject\":\"This an example webhook message\",\"email\":\"example.webhook@mandrillapp.com\",\"sender\":\"example.sender@mandrillapp.com\",\"tags\":[\"webhook-example\"],\"state\":\"bounced\",\"metadata\":{\"user_id\":111},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa1\",\"_version\":\"exampleaaaaaaaaaaaaaaa\",\"bounce_description\":\"bad_mailbox\",\"bgtools_code\":10,\"diag\":\"smtp;550 5.1.1 The email account that you tried to reach does not exist. Please try double-checking the recipient's email address for typos or unnecessary spaces.\"},\"_id\":\"exampleaaaaaaaaaaaaaaaaaaaaaaaaa1\",\"ts\":1477310208}]"}
                expect(response).to have_http_status(:success)
            end
        end
    end
  
end