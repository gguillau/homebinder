require 'rails_helper'

RSpec.describe Api::V1::RecallKeywordsController, :type => :controller do
    
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:user) { FactoryBot.create(:user, :role => "admin") }
    let(:recall_keyword) { FactoryBot.create(:recall_keyword) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
  
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        request.headers.merge!(valid_session) # Add to request headers
    end
    
    describe "GET #index" do
        context "gets all recall keywords" do
            before do
                get :index, params: {}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "POST #create" do
        context "creates a new keyword" do
            before do
                post :create, params: {recall_keyword: {keyword: "test"}}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "DELETE #destroy" do
        context "deletes a keyword" do
            before do
                delete :destroy, params: {id: recall_keyword.id}
            end
            it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
end
