require 'rails_helper'

RSpec.describe Api::V1::ApplianceRecallsController, :type => :controller do

    let(:user) { FactoryBot.create(:user, :role => "admin") }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:binder) { FactoryBot.create(:binder)}
    let(:subscription) {FactoryBot.create(:subscription, binder_id: binder.id, customer_id: "1", plan_id: "free")}
    let(:appliance) {FactoryBot.create(:appliance, binder_id: binder.id, created_by: user.id)}
    let(:appliance_recall) {FactoryBot.create(:appliance_recalls, appliance_id: appliance.id)}
    let(:file) { {version: "0.0.0"} }
  
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

    before :each do
        request.headers.merge!(valid_session) # Add to request headers
    end
    
    describe "PUT #update" do
        context "update an appliance recall" do
            it "responds with success" do
                put :update,  params:  {id: appliance_recall.id, appliance_recall: appliance_recall.attributes.symbolize_keys.except(:id)}
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "POST #send_email" do
        it "responds with success" do
            post :send_email,  params:  {id: appliance_recall.id}
            expect(response).to have_http_status(:no_content)
        end
    end
  
end