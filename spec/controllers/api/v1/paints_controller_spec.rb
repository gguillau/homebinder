require 'rails_helper'

RSpec.describe Api::V1::PaintsController, :type => :controller do

  let(:user) { FactoryBot.create(:user) }
  let(:api_key) { FactoryBot.create(:api_key) }
  let(:jwt) { UserTokenService.create_jwt(user) }
  let(:file) { {version: "0.0.0"} }
  
  let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }

  before :each do
    
    request.headers.merge!(valid_session) # Add to request headers
    
    @binder = create(:binder, name: "paint tests", active: true, created_by: user.id)
    create(:subscription, binder_id: @binder.id, plan_id: 'free')
    UserBinder.create(:binder_id => @binder.id, :user_id => user.id, :role => "owner")
  end
  
  describe "GET #index" do
    context "get all paints for a binder" do
      before do
        create(:paint, name: "paint a", binder_id: @binder.id)
        create(:paint, name: "paint b", binder_id: create(:binder).id)
        get :index, params: { binderId: @binder.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the list of appliances" do
        response_body = JSON.parse(response.body)
        expect(response_body["total"]).to eq 1
      end
    end
  end
  
  describe "GET #show" do
    context "get a single paint" do
      before do
        @paint = create(:paint, name: "get me", binder_id: @binder.id)
        get :show, params: { id: @paint.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "returns the paint" do
        paint = JSON.parse(response.body)
        expect(paint["id"]).to eq(@paint.id)
      end
    end
  end
  
  describe "POST #create" do
    context "create an paint" do
      before do
        post :create, params: { paint: { binder_id: @binder.id, name: "new paint" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "creates an paint" do
        paint = JSON.parse(response.body)
        expect(paint["name"]).to eq("new paint")
      end
    end
  end
  
  describe "PUT #update" do
    context "update an paint" do
      before do
        paint = create(:paint, name: "created", binder_id: @binder.id)
        put :update, params: { id: paint.id, paint: { binder_id: @binder.id, name: "updated" }}
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
      
      it "updates an paint" do
        paint = JSON.parse(response.body)
        expect(paint["name"]).to eq("updated")
      end
    end
  end
  
  describe "DELETE #destroy" do
    context "delete an paint" do
      before do
        paint = create(:paint, name: "created", binder_id: @binder.id)
        delete :destroy, params: { id: paint.id }
      end
      
      it "responds with success" do
        expect(response).to have_http_status(:success)
      end
    end
  end
  
end