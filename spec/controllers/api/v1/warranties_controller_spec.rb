require 'rails_helper'

RSpec.describe Api::V1::WarrantiesController, type: :controller do
    
    let(:user) { FactoryBot.create(:user, role: :admin) }
    let(:binder) { FactoryBot.create(:binder) }
    let(:warranty_plan) { FactoryBot.create(:warranty_plan) }
    let(:api_key) { FactoryBot.create(:api_key) }
    let(:jwt) { UserTokenService.create_jwt(user) }
    let(:file) { {version: "0.0.0"} }
    
    let(:valid_session) { { 'HB-APIKey' => api_key.key, 'HB-UserToken' => jwt} }
    
    before :each do
        request.headers.merge!(valid_session) # Add to request headers
    end
  
    describe "GET #index" do
        context "gets all warranties" do
            before do
                get :index, params: {:orderBy => "id"}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "POST #create" do
        context "create a warranty" do
            before do
                        homeowner = create(:user)
                        homeowner = {
                    id: homeowner.id,
                    firstName: homeowner.user_profile.first_name,
                    lastName: homeowner.user_profile.last_name,
                    email: homeowner.email,
                    phone: homeowner.user_profile.mobile_phone
                }
                        property = {
                    address1: "123 Main Street",
                    address2: "",
                    city: "Boston",
                    state: "MA",
                    country: "US",
                    zip: "02210"
                }
                    warranty = {
                    :binder_id => binder.id,
                    :status => "pending",
                    :warranty_plan_id => warranty_plan.id,
                    # set warranty expiration date to warranty_plan duration + inspection date
                    :expiration_date => Date.today,
                    :client_first => homeowner[:firstName],
                    :client_last => homeowner[:lastName],
                    :client_email => homeowner[:email],
                    :client_phone => homeowner[:phone],
                    :address_attributes => property
                }
                post :create, params: {warranty: warranty}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "PUT #update" do
        context "update the warranty" do
            before do
                homeowner = create(:user)
                        homeowner = {
                    id: homeowner.id,
                    firstName: homeowner.user_profile.first_name,
                    lastName: homeowner.user_profile.last_name,
                    email: homeowner.email,
                    phone: homeowner.user_profile.mobile_phone
                }
                        property = {
                    address1: "123 Main Street",
                    address2: "",
                    city: "Boston",
                    state: "MA",
                    country: "US",
                    zip: "02210"
                }
                    warranty = {
                    :binder_id => binder.id,
                    :status => "pending",
                    :warranty_plan_id => warranty_plan.id,
                    # set warranty expiration date to warranty_plan duration + inspection date
                    :expiration_date => Date.today,
                    :client_first => homeowner[:firstName],
                    :client_last => homeowner[:lastName],
                    :client_email => homeowner[:email],
                    :client_phone => homeowner[:phone],
                    :address_attributes => property
                }
                post :create, params: {warranty: warranty}
                warranty = JSON.parse(response.body)
                        Warranties::Service.new(warranty["id"]).create_inspection_transaction(5.days.ago)
                Warranties::Service.new(warranty["id"]).create_warranty_transaction(15)
                        put :update, params: {id: warranty["id"] , warranty: {binder_id: binder.id, warranty_plan_id: warranty_plan.id, status: "pending"}}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "DELETE #destroy" do
        context "deletes the warranty" do
            before do
                warranty = create(:warranty)
                delete :destroy, params: {id: warranty.id}
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
    
    describe "POST #download" do
        context "download all warranties" do
            before do
                post :download, params: nil
            end
                it "responds with success" do
                expect(response).to have_http_status(:success)
            end
        end
    end
end
