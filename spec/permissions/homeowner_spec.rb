require 'rails_helper'
require "cancan/matchers"

RSpec.describe Ability, :type => :model do

    describe "homeowner" do
        context "read permissions" do
            let(:user) { FactoryBot.create(:user) }
            subject(:ability){ Ability.new(user) }

            context "reading binder items" do
                let(:homeowner) { FactoryBot.create(:user) }
                let(:co_owner) { FactoryBot.create(:user) }
                let(:binder) { FactoryBot.create(:binder) }
                let(:subscription) { FactoryBot.create(:subscription, :binder => binder) }
                
                let(:appliance) { FactoryBot.create(:appliance, binder_id: binder.id) }
                let(:area) { FactoryBot.create(:area, binder_id: binder.id) }
                let(:binder_contractor) { FactoryBot.create(:binder_contractor, binder_id: binder.id) }
                let(:document) { FactoryBot.create(:document, binder_id: binder.id) }
                let(:finish) { FactoryBot.create(:finish, binder_id: binder.id) }
                let(:image) { FactoryBot.create(:image, binder_id: binder.id) }
                let(:maintenance_item) { FactoryBot.create(:maintenance_item, binder_id: binder.id) }
                let(:event) { FactoryBot.create(:maintenance_event, maintenance_item_id: maintenance_item.id) }
                let(:paint) { FactoryBot.create(:paint, binder_id: binder.id) }
                let(:permit) { FactoryBot.create(:permit, binder_id: binder.id) }
                let(:inventory_item) { FactoryBot.create(:inventory_item, binder_id: binder.id) }
                let(:project) { FactoryBot.create(:project, binder_id: binder.id) }
                let(:receipt) { FactoryBot.create(:receipt, binder_id: binder.id) }
                let(:structure) { FactoryBot.create(:structure, binder_id: binder.id) }
                let(:binder_branding) { FactoryBot.create(:binder_branding, binder_id: binder.id) }
                let(:seller_report) { FactoryBot.create(:seller_report, binder_id: binder.id)}
                let(:binder_item) { FactoryBot.create(:binder_item, binder_id: binder.id)}

                before :each do
                    homeowner.add_role :owner, binder
                    co_owner.add_role  :co_owner, binder
                end

                # another user should not be able to read another user's binder items
                context "non-homeowner user" do
                    it { should_not be_able_to(:read, appliance) }
                    it { should_not be_able_to(:read, area) }
                    it { should_not be_able_to(:read, binder_contractor) }
                    it { should_not be_able_to(:read, document) }
                    it { should_not be_able_to(:read, finish) }
                    it { should_not be_able_to(:read, image) }
                    it { should_not be_able_to(:read, maintenance_item) }
                    it { should_not be_able_to(:read, event) }
                    it { should_not be_able_to(:read, paint) }
                    it { should_not be_able_to(:read, permit) }
                    it { should_not be_able_to(:read, inventory_item) }
                    it { should_not be_able_to(:read, project) }
                    it { should_not be_able_to(:read, receipt) }
                    it { should_not be_able_to(:read, structure) }
                    it { should_not be_able_to(:read, seller_report) }
                    it { should_not be_able_to(:read, binder) }
                    it { should_not be_able_to(:read, subscription) }
                end

                # homeowner user should be able to read binder items
                context "homeowner" do
                    subject(:ability){ Ability.new(homeowner) }

                    it { should be_able_to(:read, appliance) }
                    it { should be_able_to(:read, area) }
                    it { should be_able_to(:read, binder_contractor) }
                    it { should be_able_to(:read, document) }
                    it { should be_able_to(:read, finish) }
                    it { should be_able_to(:read, image) }
                    it { should be_able_to(:read, maintenance_item) }
                    it { should be_able_to(:read, event) }
                    it { should be_able_to(:read, paint) }
                    it { should be_able_to(:read, inventory_item) }
                    it { should be_able_to(:read, project) }
                    it { should be_able_to(:read, receipt) }
                    it { should be_able_to(:read, structure) }
                    it { should be_able_to(:read, binder_branding) }
                    it { should be_able_to(:read, seller_report) }
                    it { should be_able_to(:read, binder) }
                    it { should be_able_to(:read, binder_item)}
                    it { should be_able_to(:read, subscription) }
                end

                # co_owner user should be able to read binder items
                context "co_owner" do
                    subject(:ability){ Ability.new(co_owner) }

                    it { should be_able_to(:read, appliance) }
                    it { should be_able_to(:read, area) }
                    it { should be_able_to(:read, binder_contractor) }
                    it { should be_able_to(:read, document) }
                    it { should be_able_to(:read, finish) }
                    it { should be_able_to(:read, image) }
                    it { should be_able_to(:read, maintenance_item) }
                    it { should be_able_to(:read, event) }
                    it { should be_able_to(:read, paint) }
                    it { should be_able_to(:read, permit) }
                    it { should be_able_to(:read, inventory_item) }
                    it { should be_able_to(:read, project) }
                    it { should be_able_to(:read, receipt) }
                    it { should be_able_to(:read, structure) }
                    it { should be_able_to(:read, seller_report) }
                    it { should be_able_to(:read, binder) }
                    it { should be_able_to(:read, subscription) }
                end
            end

            # non binder item reading
            context "reading non binder items" do
                let(:random_user) { FactoryBot.create(:user) }
                let(:partner) { FactoryBot.create(:partner) }
                let(:partner_configuration) { partner.partner_configuration }
                let(:template) { FactoryBot.create(:binder_template, partner_configuration_id: partner.partner_configuration.id) }
                let(:appliance_template) {FactoryBot.create(:appliance_template, binder_template_id: template.id)}
                let(:contractor_template) {FactoryBot.create(:contractor_template, binder_template_id: template.id)}
                let(:document_template) {FactoryBot.create(:document_template, binder_template_id: template.id)}
                let(:maintenance_template) {FactoryBot.create(:maintenance_template, binder_template_id: template.id)}
                let(:isn_user) {FactoryBot.create(:isn_user, partner_configuration_id: partner_configuration.id)}

                # what a regular user can't do
                it { should_not be_able_to(:read, random_user) }
                it { should_not be_able_to(:read, partner) }
                it { should_not be_able_to(:read, partner_configuration) }
                it { should_not be_able_to(:read, isn_user) }
                it { should_not be_able_to(:read, template) }
                it { should be_able_to(:read, appliance_template) }
                it { should_not be_able_to(:read, contractor_template) }
                it { should_not be_able_to(:read, document_template) }
                it { should be_able_to(:read, maintenance_template) }
            end
        end

        context "write permissions" do
            let(:user) { FactoryBot.create(:user) }
            subject(:ability){ Ability.new(user) }

            context "writing binder items" do
                let(:homeowner) { FactoryBot.create(:user) }
                let(:co_owner) { FactoryBot.create(:user) }
                let(:binder) { FactoryBot.create(:binder) }

                let(:appliance) { FactoryBot.create(:appliance, binder_id: binder.id) }
                let(:appliance_recall){FactoryBot.create(:appliance_recalls, :appliance => appliance)}
                let(:area) { FactoryBot.create(:area, binder_id: binder.id) }
                let(:binder_contractor) { FactoryBot.create(:binder_contractor, binder_id: binder.id) }
                let(:document) { FactoryBot.create(:document, binder_id: binder.id) }
                let(:finish) { FactoryBot.create(:finish, binder_id: binder.id) }
                let(:image) { FactoryBot.create(:image, binder_id: binder.id) }
                let(:maintenance_item) { FactoryBot.create(:maintenance_item, binder_id: binder.id) }
                let(:event) { FactoryBot.create(:maintenance_event, maintenance_item_id: maintenance_item.id) }
                let(:paint) { FactoryBot.create(:paint, binder_id: binder.id) }
                let(:permit) { FactoryBot.create(:permit, binder_id: binder.id) }
                let(:inventory_item) { FactoryBot.create(:inventory_item, binder_id: binder.id) }
                let(:project) { FactoryBot.create(:project, binder_id: binder.id) }
                let(:receipt) { FactoryBot.create(:receipt, binder_id: binder.id) }
                let(:structure) { FactoryBot.create(:structure, binder_id: binder.id) }
                let(:binder_branding) { FactoryBot.create(:binder_branding, binder_id: binder.id) }
                let(:seller_report) { FactoryBot.create(:seller_report, binder_id: binder.id)}
                let(:binder_item) { FactoryBot.create(:binder_item, binder_id: binder.id)}

                before :each do
                    homeowner.add_role :owner, binder
                    co_owner.add_role  :co_owner, binder
                end

                # another user should not be able to write another user's binder items
                context "non-homeowner user" do
                    it { should_not be_able_to(:write, appliance) }
                    it { should_not be_able_to(:write, area) }
                    it { should_not be_able_to(:write, binder_contractor) }
                    it { should_not be_able_to(:write, document) }
                    it { should_not be_able_to(:write, finish) }
                    it { should_not be_able_to(:write, image) }
                    it { should_not be_able_to(:write, maintenance_item) }
                    it { should_not be_able_to(:write, event) }
                    it { should_not be_able_to(:write, paint) }
                    it { should_not be_able_to(:write, permit) }
                    it { should_not be_able_to(:write, inventory_item) }
                    it { should_not be_able_to(:write, project) }
                    it { should_not be_able_to(:write, receipt) }
                    it { should_not be_able_to(:write, structure) }
                    it { should_not be_able_to(:write, seller_report) }
                    it { should_not be_able_to(:write, binder) }
                end

                # homeowner user should be able to write binder items
                context "homeowner" do
                    subject(:ability){ Ability.new(homeowner) }

                    it { should be_able_to(:write, appliance) }
                    it { should be_able_to(:write, area) }
                    it { should be_able_to(:write, binder_contractor) }
                    it { should be_able_to(:write, document) }
                    it { should be_able_to(:write, finish) }
                    it { should be_able_to(:write, image) }
                    it { should be_able_to(:write, maintenance_item) }
                    it { should be_able_to(:write, event) }
                    it { should be_able_to(:write, paint) }
                    it { should be_able_to(:write, permit) }
                    it { should be_able_to(:write, inventory_item) }
                    it { should be_able_to(:write, project) }
                    it { should be_able_to(:write, receipt) }
                    it { should be_able_to(:write, structure) }
                    it { should be_able_to(:write, binder_branding) }
                    it { should be_able_to(:write, seller_report) }
                    it { should be_able_to(:write, binder) }
                    it { should be_able_to(:write, binder_item) }

                    context "appliance_recall" do
                        it { should be_able_to(:read, appliance_recall) }
                        it { should be_able_to(:write, appliance_recall) }
                        it { should be_able_to(:destroy, appliance_recall) }
                    end
                end

                # co_owner user should be able to write binder items
                context "co_owner" do
                    subject(:ability){ Ability.new(co_owner) }

                    it { should be_able_to(:write, appliance) }
                    it { should be_able_to(:write, area) }
                    it { should be_able_to(:write, binder_contractor) }
                    it { should be_able_to(:write, document) }
                    it { should be_able_to(:write, finish) }
                    it { should be_able_to(:write, image) }
                    it { should be_able_to(:write, maintenance_item) }
                    it { should be_able_to(:write, event) }
                    it { should be_able_to(:write, paint) }
                    it { should be_able_to(:write, permit) }
                    it { should be_able_to(:write, inventory_item) }
                    it { should be_able_to(:write, project) }
                    it { should be_able_to(:write, receipt) }
                    it { should be_able_to(:write, structure) }
                    it { should be_able_to(:write, seller_report) }
                    it { should be_able_to(:write, binder) }
                end
            end

            # non binder item writing
            context "writing non binder items" do
                let(:random_user) { FactoryBot.create(:user) }
                let(:partner) { FactoryBot.create(:partner) }
                let(:partner_configuration) { partner.partner_configuration }
                let(:template) { FactoryBot.create(:binder_template, partner_configuration_id: partner_configuration.id) }
                let(:appliance_template) {FactoryBot.create(:appliance_template, binder_template_id: template.id)}
                let(:contractor_template) {FactoryBot.create(:contractor_template, binder_template_id: template.id)}
                let(:document_template) {FactoryBot.create(:document_template, binder_template_id: template.id)}
                let(:maintenance_template) {FactoryBot.create(:maintenance_template, binder_template_id: template.id)}
                let(:isn_user) {FactoryBot.create(:isn_user, partner_configuration_id: partner_configuration.id)}
                # what a regular user can't do
                it { should_not be_able_to(:write, random_user) }
                it { should_not be_able_to(:write, partner) }
                it { should_not be_able_to(:write, partner_configuration) }
                it { should_not be_able_to(:write, isn_user) }
                it { should_not be_able_to(:write, template) }
                it { should_not be_able_to(:write, appliance_template) }
                it { should_not be_able_to(:write, contractor_template) }
                it { should_not be_able_to(:write, document_template) }
                it { should_not be_able_to(:write, maintenance_template) }
            end
        end

        context "destroy permissions" do
            let(:user) { FactoryBot.create(:user) }
            subject(:ability){ Ability.new(user) }

            context "destroying binder items" do
                let(:homeowner) { FactoryBot.create(:user) }
                let(:co_owner) { FactoryBot.create(:user) }
                let(:binder) { FactoryBot.create(:binder) }

                let(:appliance) { FactoryBot.create(:appliance, binder_id: binder.id) }
                let(:area) { FactoryBot.create(:area, binder_id: binder.id) }
                let(:binder_contractor) { FactoryBot.create(:binder_contractor, binder_id: binder.id) }
                let(:document) { FactoryBot.create(:document, binder_id: binder.id) }
                let(:finish) { FactoryBot.create(:finish, binder_id: binder.id) }
                let(:image) { FactoryBot.create(:image, binder_id: binder.id) }
                let(:maintenance_item) { FactoryBot.create(:maintenance_item, binder_id: binder.id) }
                let(:event) { FactoryBot.create(:maintenance_event, maintenance_item_id: maintenance_item.id) }
                let(:paint) { FactoryBot.create(:paint, binder_id: binder.id) }
                let(:permit) { FactoryBot.create(:permit, binder_id: binder.id) }
                let(:inventory_item) { FactoryBot.create(:inventory_item, binder_id: binder.id) }
                let(:project) { FactoryBot.create(:project, binder_id: binder.id) }
                let(:receipt) { FactoryBot.create(:receipt, binder_id: binder.id) }
                let(:structure) { FactoryBot.create(:structure, binder_id: binder.id) }
                let(:binder_branding) { FactoryBot.create(:binder_branding, binder_id: binder.id) }
                let(:seller_report) { FactoryBot.create(:seller_report, binder_id: binder.id)}

                before :each do
                    homeowner.add_role :owner, binder
                    co_owner.add_role  :co_owner, binder
                end

                # another user should not be able to destroy another user's binder items
                context "non-homeowner user" do
                    it { should_not be_able_to(:destroy, appliance) }
                    it { should_not be_able_to(:destroy, area) }
                    it { should_not be_able_to(:destroy, binder_contractor) }
                    it { should_not be_able_to(:destroy, document) }
                    it { should_not be_able_to(:destroy, finish) }
                    it { should_not be_able_to(:destroy, image) }
                    it { should_not be_able_to(:destroy, maintenance_item) }
                    it { should_not be_able_to(:destroy, event) }
                    it { should_not be_able_to(:destroy, paint) }
                    it { should_not be_able_to(:destroy, permit) }
                    it { should_not be_able_to(:destroy, inventory_item) }
                    it { should_not be_able_to(:destroy, project) }
                    it { should_not be_able_to(:destroy, receipt) }
                    it { should_not be_able_to(:destroy, structure) }
                    it { should_not be_able_to(:destroy, seller_report) }
                    it { should_not be_able_to(:destroy, binder) }
                end

                # homeowner user should be able to destroy binder items
                context "homeowner" do
                    subject(:ability){ Ability.new(homeowner) }

                    it { should be_able_to(:destroy, appliance) }
                    it { should be_able_to(:destroy, area) }
                    it { should be_able_to(:destroy, binder_contractor) }
                    it { should be_able_to(:destroy, document) }
                    it { should be_able_to(:destroy, finish) }
                    it { should be_able_to(:destroy, image) }
                    it { should be_able_to(:destroy, maintenance_item) }
                    it { should be_able_to(:destroy, event) }
                    it { should be_able_to(:destroy, paint) }
                    it { should be_able_to(:destroy, permit) }
                    it { should be_able_to(:destroy, inventory_item) }
                    it { should be_able_to(:destroy, project) }
                    it { should be_able_to(:destroy, receipt) }
                    it { should be_able_to(:destroy, structure) }
                    it { should be_able_to(:destroy, binder_branding) }
                    it { should be_able_to(:destroy, seller_report) }
                    it { should be_able_to(:destroy, binder) }
                end

                # co_owner user should be able to destroy binder items
                context "co_owner" do
                    subject(:ability){ Ability.new(co_owner) }

                    it { should be_able_to(:destroy, appliance) }
                    it { should be_able_to(:destroy, area) }
                    it { should be_able_to(:destroy, binder_contractor) }
                    it { should be_able_to(:destroy, document) }
                    it { should be_able_to(:destroy, finish) }
                    it { should be_able_to(:destroy, image) }
                    it { should be_able_to(:destroy, maintenance_item) }
                    it { should be_able_to(:destroy, event) }
                    it { should be_able_to(:destroy, paint) }
                    it { should be_able_to(:destroy, permit) }
                    it { should be_able_to(:destroy, inventory_item) }
                    it { should be_able_to(:destroy, project) }
                    it { should be_able_to(:destroy, receipt) }
                    it { should be_able_to(:destroy, structure) }
                    it { should be_able_to(:destroy, seller_report) }
                    it { should_not be_able_to(:destroy, binder) }
                end
            end

            # non binder item destruction
            context "destroying non binder items" do
                let(:random_user) { FactoryBot.create(:user) }
                let(:partner) { FactoryBot.create(:partner) }
                let(:partner_configuration) { partner.partner_configuration }
                let(:template) { FactoryBot.create(:binder_template, partner_configuration_id: partner.partner_configuration.id) }
                let(:appliance_template) {FactoryBot.create(:appliance_template, binder_template_id: template.id)}
                let(:contractor_template) {FactoryBot.create(:contractor_template, binder_template_id: template.id)}
                let(:document_template) {FactoryBot.create(:document_template, binder_template_id: template.id)}
                let(:maintenance_template) {FactoryBot.create(:maintenance_template, binder_template_id: template.id)}
                let(:isn_user) {FactoryBot.create(:isn_user, partner_configuration_id: partner_configuration.id)}

                # what a regular user can't do
                it { should_not be_able_to(:destroy, random_user) }
                it { should_not be_able_to(:destroy, partner) }
                it { should_not be_able_to(:destroy, partner_configuration) }
                it { should_not be_able_to(:destroy, isn_user) }
                it { should_not be_able_to(:destroy, template) }
                it { should_not be_able_to(:destroy, appliance_template) }
                it { should_not be_able_to(:destroy, contractor_template) }
                it { should_not be_able_to(:destroy, document_template) }
                it { should_not be_able_to(:destroy, maintenance_template) }
            end
        end
    end
end