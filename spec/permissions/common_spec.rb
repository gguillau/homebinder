require 'rails_helper'
require "cancan/matchers"

RSpec.describe Ability, :type => :model do
    
    describe "common" do
        context "when an admin" do
            let(:admin) { FactoryBot.create(:user, :role => "admin") }
            let(:user) {FactoryBot.create(:user)}
            subject(:ability){ Ability.new(admin) }
                it "can manage itself" do
                assert ability.can?(:read, admin)
                assert ability.can?(:write, admin)
                assert ability.can?(:destroy, admin)
            end
                it "can manage others" do
                assert ability.can?(:read, user)
                assert ability.can?(:write, user)
                assert ability.can?(:destroy, user)
            end
        end
        context "when an inspector" do
            let(:inspector) { FactoryBot.create(:user, :role => "inspector") }
            let(:partner) {FactoryBot.create(:partner)}
            let(:partner_user){FactoryBot.create(:partner_user, :user_id => inspector.id, :partner_id => partner.id)}
            let(:user) {FactoryBot.create(:user)}
            subject(:ability){ Ability.new(inspector) }
                it "can manage itself" do
                assert ability.can?(:read, inspector)
                assert ability.can?(:write, inspector)
                assert ability.cannot?(:destroy, inspector)
            end
        end
        context "when a broker" do
            let(:broker) { FactoryBot.create(:user, :role => "broker") }
            let(:partner) {FactoryBot.create(:partner)}
            let(:partner_user){FactoryBot.create(:partner_user, :user_id => broker.id, :partner_id => partner.id)}
            let(:user) {FactoryBot.create(:user)}
            subject(:ability){ Ability.new(broker) }
                it "can manage itself" do
                assert ability.can?(:read, broker)
                assert ability.can?(:write, broker)
                assert ability.cannot?(:destroy, broker)
            end
        end
        context "when a homeowner" do
            let(:homeowner) { FactoryBot.create(:user, :role => "homeowner") }
            let(:user) {FactoryBot.create(:user)}
            let(:user_contractor) {FactoryBot.create(:user_contractor, :user => homeowner)}
            subject(:ability){ Ability.new(homeowner) }
            it "can manage itself" do
                assert ability.can?(:read, homeowner)
                assert ability.can?(:write, homeowner)
                assert ability.can?(:destroy, homeowner)
            end
            it "cannot manage others" do
                assert ability.cannot?(:read, user)
                assert ability.cannot?(:write, user)
                assert ability.cannot?(:destroy, user)
            end
            it "can manage user_contractors" do
                assert ability.can?(:read, user_contractor)
                assert ability.can?(:write, user_contractor)
                assert ability.can?(:destroy, user_contractor)
            end
        end
    end
end
