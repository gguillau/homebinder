require 'rails_helper'
require "cancan/matchers"

RSpec.describe Ability, :type => :model do
    
    describe "agent" do
        context "permissions" do
            let(:user) { FactoryBot.create(:user, :role => "agent") }
            let(:homeowner) { FactoryBot.create(:user, :role => "homeowner") }
            subject(:ability){ Ability.new(user) }
                it { should be_able_to(:read, user) }
            it { should be_able_to(:write, user) }
            it { should be_able_to(:destroy, user) }
                it { should be_able_to(:read, homeowner) }
            it { should be_able_to(:write, homeowner) }
            it { should_not be_able_to(:destroy, homeowner) }
        end
    end
end