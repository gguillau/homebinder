require 'rails_helper'
require "cancan/matchers"

RSpec.describe Ability, :type => :model do
    
    describe "admin" do
    
        context "when an admin" do
            let(:user) { FactoryBot.create(:user, :role => "admin") }
            subject(:ability){ Ability.new(user) }
                it { should be_able_to(:manage, :all) }
        end
        context "when an inspector" do
            let(:user) { FactoryBot.create(:user, :role => "inspector") }
            let(:partner) {FactoryBot.create(:partner)}
            let(:partner_user){FactoryBot.create(:partner_user, :user_id => user.id, :partner_id => partner.id)}
            subject(:ability){ Ability.new(user) }
                it { should_not be_able_to(:manage, :all) }
        end
        context "when a broker" do
            let(:user) { FactoryBot.create(:user, :role => "broker") }
            let(:partner) {FactoryBot.create(:partner)}
            let(:partner_user){FactoryBot.create(:partner_user, :user_id => user.id, :partner_id => partner.id)}
            subject(:ability){ Ability.new(user) }
                it { should_not be_able_to(:manage, :all) }
        end
        context "when an agent" do
            let(:user) { FactoryBot.create(:user, :role => "agent") }
            subject(:ability){ Ability.new(user) }
                it { should_not be_able_to(:manage, :all) }
        end
        context "when a homeowner" do
            let(:user) { FactoryBot.create(:user, :role => "homeowners") }
            subject(:ability){ Ability.new(user) }
                it { should_not be_able_to(:manage, :all) }
        end
    end
end
