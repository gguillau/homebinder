require 'rails_helper'
require "cancan/matchers"

RSpec.describe Ability, :type => :model do
    describe "partner" do
        context "non-binder permissions" do
            let(:partner) { FactoryBot.create(:partner) }

            context "when inspector" do
                let(:inspector) { FactoryBot.create(:user, :role => "inspector") }
                let(:agent) { FactoryBot.create(:user, :role => "agent") }
                let(:homeowner) { FactoryBot.create(:user, :role => "homeowner") }
                let(:second_partner) { FactoryBot.create(:partner) }
                let!(:partner_user){ FactoryBot.create(:partner_user, :partner => partner, :user => inspector, :role => "admin") }
                let!(:partner_agent){ FactoryBot.create(:partner_user, :partner => partner, :user => agent, :role => "agent") }
                let(:isn_user) {FactoryBot.create(:isn_user, partner_configuration_id: partner_configuration.id)}
                let(:partner_configuration) { partner.partner_configuration }
                let(:template) { FactoryBot.create(:binder_template, partner_configuration_id: partner_configuration.id) }
                let(:appliance_template) {FactoryBot.create(:appliance_template, binder_template_id: template.id)}
                let(:second_appliance_template) {FactoryBot.create(:appliance_template, :library_template => true)}
                let(:contractor_template) {FactoryBot.create(:contractor_template, binder_template_id: template.id)}
                let(:document_template) {FactoryBot.create(:document_template, binder_template_id: template.id)}
                let(:maintenance_template) {FactoryBot.create(:maintenance_template, binder_template_id: template.id)}
                let(:isn_user) {FactoryBot.create(:isn_user, partner_configuration_id: partner_configuration.id)}
                let(:account) {FactoryBot.create(:account, :manager_id => partner.id)}
                let(:warranty_configuration) {FactoryBot.create(:warranty_configuration, :partner => partner)}
                let(:apr) {FactoryBot.create(:annual_property_review, :reviewer_company => partner)}
                let(:apr2) {FactoryBot.create(:annual_property_review, :reviewer_company => partner, :reviewer_id => inspector.id)}
                let(:apr3) {FactoryBot.create(:annual_property_review, :reviewer_company => partner, :status => "completed")}
                let(:apr4) {FactoryBot.create(:annual_property_review, :reviewer_company => partner, :reviewer_id => inspector.id)}
                let(:finding) {FactoryBot.create(:annual_property_review_finding, :annual_property_review => apr)}
                let(:capital_item) {FactoryBot.create(:annual_property_review_capital_item, :annual_property_review => apr)}
                let(:organization) {FactoryBot.create(:organization)}
                let(:dashboard) {FactoryBot.create(:dashboard, :partner => partner, :system => false)}
                let(:widget) {FactoryBot.create(:widget, :partner_restriction_id => partner.id)}
                let(:partner_widget) {FactoryBot.create(:partner_widget, :partner => partner)}
                let(:partner_contractor) {FactoryBot.create(:partner_contractor, :partner => partner)}
                let(:organization) {FactoryBot.create(:organization)}
                let!(:organization_user) {FactoryBot.create(:organization_user, :organization => organization, :user => inspector)}
                subject(:ability){ Ability.new(inspector) }
                
                context "organization" do
                    it { should be_able_to(:read, organization) }
                    it { should be_able_to(:write, organization) }
                end

                context "partner" do
                    it { should be_able_to(:create, partner) }
                    it { should be_able_to(:read, partner) }
                    it { should be_able_to(:write, partner) }
                    it { should be_able_to(:destroy, partner) }
                    # second partner
                    it { should_not be_able_to(:read, second_partner) }
                    it { should_not be_able_to(:write, second_partner) }
                    it { should_not be_able_to(:destroy, second_partner) }
                end
                
                context "partner_user" do
                    it { should be_able_to(:read, partner_user) }
                    it { should be_able_to(:write, partner_user) }
                    it { should_not be_able_to(:destroy, partner_user) }
                end
                
                context "partner_contractor" do
                    it { should be_able_to(:read, partner_contractor) }
                    it { should be_able_to(:write, partner_contractor) }
                    it { should be_able_to(:destroy, partner_contractor) }
                end
                
                context "partner_widget" do
                    it { should be_able_to(:read, partner_widget) }
                    it { should be_able_to(:write, partner_widget) }
                    it { should be_able_to(:destroy, partner_widget) }
                end

                context "isn_user" do
                    it { should be_able_to(:read, isn_user) }
                    it { should be_able_to(:write, isn_user) }
                    it { should be_able_to(:destroy, isn_user) }
                end

                context "partner_configuration" do
                    it { should be_able_to(:read, partner_configuration) }
                    it { should be_able_to(:write, partner_configuration) }
                    it { should be_able_to(:destroy, partner_configuration) }
                end
                
                context "user" do
                    it { should be_able_to(:read, inspector) }
                    it { should be_able_to(:write, inspector) }
                end
                
                context "homeowner" do
                    it { should be_able_to(:read, homeowner) }
                    it { should be_able_to(:write, homeowner) }
                end

                context "agent" do
                    it { should be_able_to(:read, agent) }
                    it { should be_able_to(:write, agent) }
                    it { should be_able_to(:destroy, agent) }
                end

                context "template" do
                    it { should be_able_to(:read, template) }
                    it { should be_able_to(:write, template) }
                    it { should be_able_to(:destroy, template) }
                end

                context "appliance_template" do
                    it { should be_able_to(:read, appliance_template) }
                    it { should be_able_to(:write, appliance_template) }
                    it { should be_able_to(:destroy, appliance_template) }
                    it { should be_able_to(:read, second_appliance_template) }
                    it { should_not be_able_to(:write, second_appliance_template) }
                    it { should_not be_able_to(:destroy, second_appliance_template) }
                end

                context "contractor_template" do
                    it { should be_able_to(:read, contractor_template) }
                    it { should be_able_to(:write, contractor_template) }
                    it { should be_able_to(:destroy, contractor_template) }
                end

                context "document_template" do
                    it { should be_able_to(:read, document_template) }
                    it { should be_able_to(:write, document_template) }
                    it { should be_able_to(:destroy, document_template) }
                end

                context "maintenance_template" do
                    it { should be_able_to(:read, maintenance_template) }
                    it { should be_able_to(:write, maintenance_template) }
                    it { should be_able_to(:destroy, maintenance_template) }
                end

                context "account" do
                    it { should be_able_to(:read, account) }
                end
                context "warranty_configuration" do
                    it { should be_able_to(:read, warranty_configuration) }
                    it { should be_able_to(:write, warranty_configuration) }
                end
                context "apr" do
                    it { should be_able_to(:read, apr) }
                    it { should be_able_to(:write, apr) }
                    it { should be_able_to(:read, apr2) }
                    it { should be_able_to(:write, apr4) }
                    it { should_not be_able_to(:write, apr3) }
                end
                context "finding" do
                    it { should be_able_to(:read, finding) }
                    it { should be_able_to(:write, finding) }
                    it { should be_able_to(:write, finding) }
                    it { should be_able_to(:destroy, finding) }
                end
                context "capital_item" do
                    it { should be_able_to(:read, capital_item) }
                    it { should be_able_to(:write, capital_item) }
                    it { should be_able_to(:destroy, capital_item) }
                end
                context "dashboard" do
                    it { should be_able_to(:read, dashboard) }
                    it { should be_able_to(:write, dashboard) }
                    it { should be_able_to(:destroy, dashboard) }
                end
                context "widget" do
                    it { should be_able_to(:create, widget) }
                    it { should be_able_to(:read, widget) }
                    it { should be_able_to(:write, widget) }
                    it { should be_able_to(:destroy, widget) }
                end
            end

            context "when broker" do
                let(:broker) { FactoryBot.create(:user, :role => "broker") }

                subject(:ability){ Ability.new(broker) }

                # non binder item creating
                context "creating non binder items" do

                    # what a regular user can't do
                    it { should be_able_to(:create, partner) }
                end
            end
        end

        context "binder permissions" do
            let!(:inspector) { FactoryBot.create(:user, :role => "inspector") }
            let!(:partner) { FactoryBot.create(:partner)}
            let!(:partner_user) {FactoryBot.create(:partner_user, :partner => partner, :user => inspector, :role => "admin")}
            let!(:binder) { FactoryBot.create(:binder)}
            let!(:second_binder) { FactoryBot.create(:binder)}
            let!(:third_binder) { FactoryBot.create(:binder)}
            let!(:user_binder) {FactoryBot.create(:user_binder, :binder => binder, :user => inspector, :role => "owner")}
            let!(:second_user_binder) {FactoryBot.create(:user_binder, :binder => third_binder, :user => inspector, :role => "co_owner")}
            let!(:partner_binder) {FactoryBot.create(:partner_binder, :binder => binder, :partner => partner, :role => "binder")}

            subject(:ability){ Ability.new(inspector) }

            # what a regular user can't do
            it { should be_able_to(:read, binder) }
            it { should be_able_to(:access_repair_pricer, binder) }
            it { should be_able_to(:write, user_binder) }
            it { should be_able_to(:destroy, user_binder) }
            it { should be_able_to(:write, binder) }
            it { should be_able_to(:destroy, binder) }
            it { should be_able_to(:transfer, binder) }
            it { should be_able_to(:share, binder) }
            it { should_not be_able_to(:read, second_binder) }
            it { should_not be_able_to(:write, second_binder) }
            it { should_not be_able_to(:destroy, second_binder) }
            it { should_not be_able_to(:transfer, second_binder) }
            it { should_not be_able_to(:share, second_binder) }
            it { should be_able_to(:read, third_binder) }
            it { should be_able_to(:write, third_binder) }
            it { should_not be_able_to(:destroy, third_binder) }
            it { should be_able_to(:transfer, third_binder) }
            it { should be_able_to(:share, third_binder) }

            context "binder items" do
                let(:binder_branding){FactoryBot.create(:binder_branding, :binder => binder)}
                let(:structure){FactoryBot.create(:structure, :binder => binder)}
                let(:area){FactoryBot.create(:area, :binder => binder)}
                let(:appliance){FactoryBot.create(:appliance, :binder => binder)}
                let(:appliance_recall){FactoryBot.create(:appliance_recalls, :appliance => appliance)}
                let(:binder_contractor){FactoryBot.create(:binder_contractor, :binder => binder)}
                let(:finish){FactoryBot.create(:finish, :binder => binder)}
                let(:paint){FactoryBot.create(:paint, :binder => binder)}
                let(:permit){FactoryBot.create(:permit, :binder => binder)}
                let(:project){FactoryBot.create(:project, :binder => binder)}
                let(:maintenance_item){FactoryBot.create(:maintenance_item, :binder => binder)}
                let(:maintenance_event){FactoryBot.create(:maintenance_event, :maintenance_item => maintenance_item)}
                let(:inventory_item){FactoryBot.create(:inventory_item, :binder => binder)}
                let(:receipt){FactoryBot.create(:receipt, :binder => binder)}
                let(:document){FactoryBot.create(:document, :binder => binder)}
                let(:image){FactoryBot.create(:image, :binder => binder)}
                let(:share){FactoryBot.create(:share, :binder => binder)}
                let(:transfer){FactoryBot.create(:transfer, :binder => binder)}
                let(:seller_report){FactoryBot.create(:seller_report, :binder => binder)}

                context "branding" do
                    it { should be_able_to(:read, binder_branding) }
                    it { should be_able_to(:write, binder_branding) }
                    it { should be_able_to(:destroy, binder_branding) }
                end

                context "structure" do
                    it { should be_able_to(:read, structure) }
                    it { should be_able_to(:write, structure) }
                    it { should be_able_to(:destroy, structure) }
                end

                context "area" do
                    it { should be_able_to(:read, area) }
                    it { should be_able_to(:write, area) }
                    it { should be_able_to(:destroy, area) }
                end

                context "appliance" do
                    it { should be_able_to(:read, appliance) }
                    it { should be_able_to(:write, appliance) }
                    it { should be_able_to(:destroy, appliance) }
                end
                context "appliance_recall" do
                    it { should be_able_to(:read, appliance_recall) }
                    it { should be_able_to(:write, appliance_recall) }
                    it { should be_able_to(:destroy, appliance_recall) }
                end

                context "binder_contractor" do
                    it { should be_able_to(:read, binder_contractor) }
                    it { should be_able_to(:write, binder_contractor) }
                    it { should be_able_to(:destroy, binder_contractor) }
                end

                context "finish" do
                    it { should be_able_to(:read, finish) }
                    it { should be_able_to(:write, finish) }
                    it { should be_able_to(:destroy, finish) }
                end

                context "paint" do
                    it { should be_able_to(:read, paint) }
                    it { should be_able_to(:write, paint) }
                    it { should be_able_to(:destroy, paint) }
                end

                context "permit" do
                    it { should be_able_to(:read, permit) }
                    it { should be_able_to(:write, permit) }
                    it { should be_able_to(:destroy, permit) }
                end

                context "project" do
                    it { should be_able_to(:read, project) }
                    it { should be_able_to(:write, project) }
                    it { should be_able_to(:destroy, project) }
                end

                context "maintenance_item" do
                    it { should be_able_to(:read, maintenance_item) }
                    it { should be_able_to(:write, maintenance_item) }
                    it { should be_able_to(:destroy, maintenance_item) }
                end

                context "maintenance_event" do
                    it { should be_able_to(:read, maintenance_event) }
                    it { should be_able_to(:write, maintenance_event) }
                    it { should be_able_to(:destroy, maintenance_event) }
                end

                context "inventory_item" do
                    it { should be_able_to(:read, inventory_item) }
                    it { should be_able_to(:write, inventory_item) }
                    it { should be_able_to(:destroy, inventory_item) }
                end

                context "receipt" do
                    it { should be_able_to(:read, receipt) }
                    it { should be_able_to(:write, receipt) }
                    it { should be_able_to(:destroy, receipt) }
                end

                context "document" do
                    it { should be_able_to(:read, document) }
                    it { should be_able_to(:write, document) }
                    it { should be_able_to(:destroy, document) }
                end

                context "image" do
                    it { should be_able_to(:read, image) }
                    it { should be_able_to(:write, image) }
                    it { should be_able_to(:destroy, image) }
                end

                context "share" do
                    it { should be_able_to(:read, share) }
                    it { should be_able_to(:write, share) }
                    it { should be_able_to(:destroy, share) }
                end

                context "transfer" do
                    it { should be_able_to(:read, transfer) }
                    it { should be_able_to(:write, transfer) }
                    it { should be_able_to(:destroy, transfer) }
                end

                context "seller_report" do
                    it { should be_able_to(:read, seller_report) }
                    it { should be_able_to(:write, seller_report) }
                    it { should be_able_to(:destroy, seller_report) }
                end
            end
        end
    end
end
