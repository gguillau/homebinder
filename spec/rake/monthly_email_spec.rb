require "rails_helper"

RSpec.describe "rake homebinder_monthly_email", type: :task do

    it "preloads the Rails environment" do
        expect(task.prerequisites).to include "environment"
    end
    
    it "runs gracefully with no users" do
        expect { task.execute }.not_to raise_error
    end
    
    it "does not send an email" do
        allow(UserMailer).to receive(:delay).and_return(UserMailer)
        allow(UserMailer).to receive(:monthly_email)
        allow(Date).to receive(:today).and_return(Date.today.beginning_of_month)
      
        user = create :user, :sign_in_count => 0
        binders = create_list(:binder, 10, created_by: user.id)
        binders.each do |binder|
            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")
        end
    
        user.user_profile.monthly_email = true
        user.user_profile.save
        task.execute
        expect(UserMailer).to_not have_received(:monthly_email)
    end
    
    it "sends an email" do
        double = double("UserMailer", :deliver_later => true)
        allow(UserMailer).to receive(:monthly_email).and_return(double)
        allow(Date).to receive(:today).and_return(Date.today.beginning_of_month)
      
        user = create :user, :sign_in_count => 1
        binders = create_list(:binder, 10, created_by: user.id)
        binders.each do |binder|
            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "owner")
        end
    
        user.user_profile.monthly_email = true
        user.user_profile.save
        task.execute
        expect(UserMailer).to have_received(:monthly_email).at_least(:once)
    end
    
    it "rescues from error" do
        message = "#{Rails.env}: #{task.name.titleize} Error: Fake error"
        allow(Date).to receive(:today).and_raise BadRequestException.new "Fake error"
        allow(ErrorService).to receive(:perform_async)
        allow(Slack::MessagingJob).to receive(:perform_async)
        task.execute
        expect(Slack::MessagingJob).to have_received(:perform_async).with("#bugs", message).once
        expect(ErrorService).to have_received(:perform_async)
    end
end