require "rails_helper"

RSpec.describe "rake homebinder_update_partner_billing_accounts", type: :task do

    it "preloads the Rails environment" do
        expect(task.prerequisites).to include "environment"
    end
    
    it "runs gracefully with no items" do
        expect { task.execute }.not_to raise_error
    end
    
    it "rescues from error" do
        message = "#{Rails.env}: #{task.name.titleize} Error: Fake error"
        allow(Partners::UpdateBillingAccountsJob).to receive(:perform_async).and_raise BadRequestException.new "Fake error"
        allow(Slack::MessagingJob).to receive(:perform_async)
        allow(ErrorService).to receive(:perform_async)
        
        task.execute
        expect(Slack::MessagingJob).to have_received(:perform_async).with("#bugs", message).once
        expect(ErrorService).to have_received(:perform_async)
    end
end