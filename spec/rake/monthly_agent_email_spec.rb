require "rails_helper"

RSpec.describe "rake homebinder_monthly_agent_email", type: :task do

    it "preloads the Rails environment" do
        expect(task.prerequisites).to include "environment"
    end
    
    it "runs gracefully with no users" do
        expect { task.execute }.not_to raise_error
    end
    
    it "does not send an email" do
        double = double("KpiMailer", :deliver_later => true)
        allow(KpiMailer).to receive(:send_monthly_agent_metrics).and_return(double)
        allow(Date).to receive(:today).and_return(Date.today.beginning_of_month)
      
        user = create :user, :sign_in_count => 0, role: "agent"
        binders = create_list(:binder, 10, created_by: user.id)
        binders.each do |binder|
            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "buyer_agent")
        end
    
        user.user_profile.monthly_email = true
        user.user_profile.save
        task.execute
        expect(KpiMailer).to_not have_received(:send_monthly_agent_metrics)
    end
    
    it "sends an email" do
        search = OpenStruct.new(
            results: OpenStruct.new({
                total: 1,
                response: OpenStruct.new({
                    response: OpenStruct.new({
                        hits: OpenStruct.new({
                            total: 0
                        })
                    })
                })
            })
        )

        allow(Event.__elasticsearch__).to receive(:search).and_return(search)
        
        double = double("KpiMailer", :deliver_later => true)
        allow(KpiMailer).to receive(:send_monthly_agent_metrics).and_return(double)
        allow(Date).to receive(:today).and_return(Date.today.beginning_of_month)
      
        user = create :user, :sign_in_count => 1, :role => "agent"
        binders = create_list(:binder, 10, created_by: user.id)
        binders.each do |binder|
            create(:user_binder, :user_id => user.id, :binder_id => binder.id, :role => "buyer_agent")
        end
    
        user.user_profile.monthly_email = true
        user.user_profile.save
        task.execute
        expect(KpiMailer).to have_received(:send_monthly_agent_metrics).at_least(:once)
    end
    
    it "rescues from error" do
        message = "#{Rails.env}: #{task.name.titleize} Error: Fake error"
        allow(Date).to receive(:today).and_raise BadRequestException.new "Fake error"
        allow(ErrorService).to receive(:perform_async)
        allow(Slack::MessagingJob).to receive(:perform_async)
        task.execute
        expect(Slack::MessagingJob).to have_received(:perform_async).with("#bugs", message).once
        expect(ErrorService).to have_received(:perform_async)
    end
end