require "rails_helper"

RSpec.describe "rake homebinder_automatic_transfers", type: :task do

    it "preloads the Rails environment" do
        expect(task.prerequisites).to include "environment"
    end
    
    it "runs gracefully with no items" do
        expect { task.execute }.not_to raise_error
    end
    
    it "rescues from BadRequestException" do
        message = "#{Rails.env}: #{task.name.titleize} Error: Fake error"
        allow(Partner::Automation::Service::Access).to receive(:new).and_raise BadRequestException.new "Fake error"
        allow(ErrorService).to receive(:perform_async)
        allow(Slack::MessagingJob).to receive(:perform_async)
        
        task.execute
        expect(Slack::MessagingJob).to have_received(:perform_async).with("#bugs", message).once
        expect(ErrorService).to have_received(:perform_async)
    end
    
    it "rescues from UnprocessableException" do
        message = "#{Rails.env}: #{task.name.titleize} Error: "
        user = create(:user)
        allow(Partner::Automation::Service::Access).to receive(:new).and_raise UnprocessableException.new user
        allow(ErrorService).to receive(:perform_async)
        allow(Slack::MessagingJob).to receive(:perform_async)
        
        task.execute
        expect(Slack::MessagingJob).to have_received(:perform_async).with("#bugs", message).once
        expect(ErrorService).to have_received(:perform_async)
    end
end