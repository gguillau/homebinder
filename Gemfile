source 'https://rubygems.org'
ruby '2.3.8'

# gems required to boot the app
gem 'actionpack', '5.0.6'
gem 'active_model_serializers', '0.9.7'
gem 'activerecord', '5.0.6'
gem 'elasticsearch-dsl', '0.1.8' 
gem 'elasticsearch-model', '5.0.2'
gem 'elasticsearch-persistence', '5.0.2', require: 'elasticsearch/persistence/model'
gem 'elasticsearch-rails', '5.0.2'
gem 'geocoder', '1.4.7'
gem 'money-rails', '1.9.0'
gem 'money', '6.9.0'
gem 'paperclip', '5.1.0'
gem 'pg', '0.19.0'
gem 'rack-cors', '0.4.1', :require => 'rack/cors' # will be used to support mobile
gem 'rack-rewrite', '1.5.1' # A rack middleware for enforcing rewrite rules. In many cases you can get away with rack-rewrite instead of writing Apache mod_rewrite rules.
gem 'rack-timeout', '0.4.2'
gem 'rails', '5.0.6'
gem 'railties', '5.0.6'
gem 'sidekiq', '5.2.7'

# gems not required to boot the app
gem 'attr_encrypted', '1.4.0', :require => false # need to upgrade gem to remove this warning: DEPRECATION WARNING: alias_method_chain is deprecated. Please, use Module#prepend instead.
gem 'aws-sdk', '2.6.50', :require => false
gem 'bcrypt', '3.1.11', :require => false
gem 'bitly', '1.1.2', :require => false
gem 'bootsnap', '1.3.1', require: false # speeds up booting of application
gem 'cancancan', '3.0.1', :require => false
gem 'countries', '2.1.2', :require => 'countries/global'
gem 'httparty', '0.15.6', :require => false
gem 'intercom', '3.5.19', :require => true
gem 'jwt', '1.5.6', :require => false
gem 'mandrill-api', '1.0.53', :require => false
gem 'phony_rails', '0.14.6', :require => false
gem 'prawn', '1.3.0', :require => false
gem 'prawn-table', '0.2.2', :require => false
gem 'recurrence', '1.3.0', :require => false
gem 'ruby-gmail', '0.3.1', :require => false
gem 'sentry-raven', '2.9.0', :require => false
gem 'slack-ruby-client', '0.10.0', :require => false
gem 'soapforce', '0.8.0', :require => false
gem 'stripe', '4.18.1', :require => false
gem 'twilio-ruby', '5.20.1', :require => false # twilio
gem 'unicode-emoji', '2.0.0', :require => false
gem 'whenever', '1.0.0', require: false
gem 'daemons', '1.3.1'
gem 'delayed_job_active_record', '4.1.2'

group :development, :production, :staging do
  gem 'puma', '3.6.2'
end

group :development do
  gem 'annotate', '2.7.2'
  gem 'bullet', '5.6.1'
  gem 'derailed_benchmarks', '1.3.2'
  gem 'fasterer', '0.3.2'
  gem 'foreman', '0.84.0'
  gem 'memory_profiler', '0.9.8'
  gem 'rails_best_practices', '1.19.0'
  gem 'require_all', '1.4.0'
  gem 'reek', '4.7.2'
  gem 'ruby-prof', '0.16.2'
  gem 'rubycritic', '3.3.0', require: false
  gem 'rufo', '0.1'
end

group :test do
  gem 'database_cleaner', '1.6.1'
  gem 'rails-controller-testing', '1.0.2'
  gem 'rspec-benchmark', '0.3.0'
  gem 'rspec-rails', '3.6.1'
  gem 'shoulda-matchers', '3.1.2'
  gem 'simplecov', '0.15.1'
  gem 'hashdiff', '0.3.7'
  gem 'webmock', '2.3.2'
  gem 'rspec-sidekiq', '3.0.3'
end

group :test, :development, :staging do
  gem 'dotenv-rails', '2.2.1'
  gem 'factory_bot_rails', '4.11.1'
  gem 'faker', '1.8.4'
  gem 'thor', '0.19.4', :require => false # to get rid of this warning -> Expected string default value for '--serializer'; got true (boolean)
end

group :production, :master do
  gem 'newrelic_rpm', '3.18.1.330'
end