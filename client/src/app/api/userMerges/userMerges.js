angular
	.module("hb.api.userMerge", [])
	.factory("UserMerge", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/user_merges";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(id) {
					return $http.get("/api/v1/user_merges/" + id);
				},
				update: function(id, user_merge) {
					return $http.put("/api/v1/user_merges/" + id, { user_merge: user_merge });
				},
				revert: function(id) {
					return $http.put("/api/v1/user_merges/" + id + "/revert");
				}
			};
		}
	]);
