angular
    .module("hb.api.warrantyPlan", [])
    .factory("WarrantyPlan", [
        "$http",
        "hb.utils",
        function($http, utils) {
            return {
                all: function(opts) {
                    var url = "/api/v1/warranty_plans";
                    if (opts) {
                        url += utils.utils.createQueryString(opts);
                    }
                    return $http.get(url);
                },
                get: function(id) {
                    return $http.get("/api/v1/warranty_plans/" + id);
                },
                create: function(warrantyPlan) {
                    return $http.post("/api/v1/warranty_plans/", warrantyPlan);
                },
                update: function(id, warrantyPlan) {
                    return $http.put("/api/v1/warranty_plans/" + id, warrantyPlan);
                },
                destroy: function(id) {
                    /*jshint -W024*/
                    return $http.delete("/api/v1/warranty_plans/" + id);
                }
            };
        }
    ]);
