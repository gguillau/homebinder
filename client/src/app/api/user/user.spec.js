describe("hb.api.user", function() {
    'use strict';

    var user, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.user"));
    beforeEach(module(function($provide) {
        $provide.factory("Session", function() {
            return {
                setJwt: function() {},
                getJwt: function() {
                    return "teasd";
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getUser: function() {
                    return { id: 1 };
                }
            };
        });
        $provide.factory("hb.utils", function() {
            return {
                utils: {
                    create: function() {},
                    createQueryString: function() {}
                }
            };
        });
    }));
    beforeEach(function() {
        inject(function($injector) {
            user = $injector.get("User");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('ALL', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/users")
                .respond(response);

            user.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('GET', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/users/1")
                .respond(response);

            user.get(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('create', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/users")
                .respond(response);

            user.create({ name: "test" }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('update', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/users/1")
                .respond(response);

            user.update(1, { id: 1, name: "test" }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('destroy', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectDELETE("/api/v1/users/1")
                .respond(response);

            user.destroy(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('init', function() {
        it("returns user", function() {
            var item = user.init();
            expect(item.id).toEqual(1);
        });
    });

    describe('logon', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/user_tokens")
                .respond(response);

            user.logon({}).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('logoff', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectDELETE("/api/v1/user_tokens/token")
                .respond(response);

            user.logoff("token").then(function(response) {
                expect(response).toBe(undefined);
            });

            $httpBackend.flush();
        });
    });

    describe('register', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/registrations")
                .respond(response);

            user.register({}).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('changeEmail', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/users/1/email")
                .respond(response);

            user.changeEmail(1, { id: 1 }, "", "").then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('changePassword', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/users/1/password")
                .respond(response);

            user.changePassword(1, { id: 1 }, "").then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('resetPassword', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/users/passwords")
                .respond(response);

            user.resetPassword("").then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('updatePassword', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/users/passwords/update")
                .respond(response);

            user.updatePassword("").then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('current', function() {
        it("returns user", function() {
            var item = user.current();
            expect(item.id).toEqual(1);
        });
    });

    describe('isLoggedOn', function() {
        it("returns user", function() {
            var item = user.isLoggedOn();
            expect(item.id).toEqual(1);
        });
    });

    describe('find_by_email', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/users/email")
                .respond(response);

            user.find_by_email("").then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('welcome', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/users/1/welcome")
                .respond(response);

            user.welcome(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('optOut', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectDELETE("/api/v1/pending_users/1")
                .respond(response);

            user.optOut(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('bindersWaitingByEmail', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/pending_users/binders")
                .respond(response);

            user.bindersWaitingByEmail("").then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('bindersWaitingById', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/pending_users/1/binders")
                .respond(response);

            user.bindersWaitingById(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('invite', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/users/1/invite")
                .respond(response);

            user.invite(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});