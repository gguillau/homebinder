angular
	.module("hb.api.user", [])
	.factory("User", [
		"$http",
		"$rootScope",
		"$injector",
		"hb.utils",
		function($http, $rootScope, $injector, util) {
			var Session = $injector.get("Session");

			function setCurrentUser(jwt, rememberMe) {
				Session.setJwt(jwt, rememberMe);
				$http.defaults.headers.common["HB-UserToken"] = jwt;
				$rootScope.$broadcast("user.logon");
			}
			return {
				all: function(opts) {
					var url = "/api/v1/users";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}

					return $http.get(url);
				},
				get: function(id) {
					return $http.get('/api/v1/users/' + id);
				},
				create: function(data) {
					return $http.post("/api/v1/users", data);
				},
				update: function(id, user) {
					return $http.put("/api/v1/users/" + id, {
						user: user
					});
				},
				destroy: function(id) {
					/*jshint -W024*/
					return $http.delete('/api/v1/users/' + id);
				},
				init: function() {
					// Try and get the JWT token from storage. If it doesn"t exist
					// we"re done. The user must logon again.
					var jwt = Session.getJwt();
					if (jwt) {
						setCurrentUser(jwt);
						return Session.getUser();
					}
				},
				logon: function(logonData) {
					return $http.post("/api/v1/user_tokens", logonData).success(
						function(result) {
							setCurrentUser(result.token, logonData.rememberMe);
						});
				},
				logoff: function(utoken) {
					// Delete the session
					/*jshint -W024*/
					return $http.delete("/api/v1/user_tokens/" + utoken).then(
						function(result) {
							delete $http.defaults.headers.common["HB-UserToken"];
							$rootScope.$broadcast("user.logoff");
						}
					);
				},
				register: function(data) {
					return $http.post("/api/v1/registrations", data).success(
						function(result) {
							setCurrentUser(result.token);
						});
				},
				changeEmail: function(id, user, email, password) {
					return $http.put("/api/v1/users/" + id + "/email", {
						user: user,
						email: email,
						password: password
					});
				},
				changePassword: function(id, user, password) {
					return $http.put("/api/v1/users/" + id + "/password", {
						user: user,
						password: password
					});
				},
				resetPassword: function(email) {
					return $http.post("/api/v1/users/passwords", {
						user: {
							email: email
						}
					});
				},
				updatePassword: function(data) {
					return $http.put("/api/v1/users/passwords/update", {
						user: data
					});
				},
				current: function() {
					return Session.getUser();
				},
				isLoggedOn: function() {
					return Session.getUser();
				},
				find_by_email: function(email) {
					return $http.post("/api/v1/users/email", { email: email });
				},
				welcome: function(id, opts) {
					var url = "/api/v1/users/" + id + "/welcome";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				optOut: function(userId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/pending_users/" + userId);
				},
				bindersWaitingByEmail: function(email) {
					return $http.post("/api/v1/pending_users/binders", {
						email: email
					});
				},
				bindersWaitingById: function(id) {
					return $http.get("/api/v1/pending_users/" + id + "/binders");
				},
				invite: function(id) {
					return $http.put('/api/v1/users/' + id + '/invite');
				},
				confirm_password: function(id, token) {
					return $http.put('/api/v1/users/' + id + '/confirm_password', {token: token});
				},
				mergeAgents: function(opts) {
					return $http.post('/api/v1/users/merge_agents', opts);
				}
			};
		}
	]);
