angular
	.module("hb.api.maintenanceItem", [])
	.factory("MaintenanceItem",[
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/maintenance_items";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(itemId) {
					return $http.get("/api/v1/maintenance_items/" + itemId);
				},
				create: function(item) {
					return $http.post("/api/v1/maintenance_items/", {maintenance_item: item});
				},
				update: function(itemId, item) {
					return $http.put("/api/v1/maintenance_items/" + itemId, {maintenance_item: item});
				},
				/* sends a test maintenance reminder email to the current user*/
				sendEmail: function(itemId){
					return $http.post("/api/v1/maintenance_items/" + itemId + "/email");
				},
				destroy: function(itemId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/maintenance_items/" + itemId);
				}
			};
		}]);
	

	