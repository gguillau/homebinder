angular
	.module("hb.api.automationError", [])
	.factory("AutomationError", ["$http", "hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/automation_errors";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}

					return $http.get(url);
				},
				get: function(id) {
					return $http.get("/api/v1/automation_errors/" + id);
				},
				update: function(id, error) {
					return $http.put("/api/v1/automation_errors/" + id, {
						error: error
					});
				},
				destroy: function(id) {
					/*jshint -W024*/
					return $http.delete("/api/v1/automation_errors/" + id);
				}
			};
		}
	]);