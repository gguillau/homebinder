angular
	.module("hb.api.userContractor", [])
	.factory("UserContractor", [
		"$http",
		"hb.utils",
		function ($http, util) {
			return {
				all: function (opts) {
					var url = "/api/v1/user_contractors";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function (id) {
					return $http.get("/api/v1/user_contractors/" + id);
				},
				create: function (user_contractor) {
					return $http.post("/api/v1/user_contractors/", { user_contractor: user_contractor });
				},
				update: function (id, user_contractor) {
					return $http.put("/api/v1/user_contractors/" + id, { user_contractor: user_contractor });
				},
				destroy: function (id) {
					/*jshint -W024*/
					return $http.delete("/api/v1/user_contractors/" + id);
				}
			};
		}
	]);
