angular
	.module("hb.api.widgetExclusion", [])
	.factory("WidgetExclusion", [
		"hbApiBase",
		"$http",
		"$interpolate",
		"hb.utils",
		function(ApiBase, $http, $interpolate, utils) {
			var WidgetExclusion = function() {
				ApiBase.call(this);
				this.controller = "widget_exclusions";
			};

			WidgetExclusion.prototype = Object.create(ApiBase.prototype);

			return new WidgetExclusion();
		}
	]);