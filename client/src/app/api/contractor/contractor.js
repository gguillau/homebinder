angular
	.module("hb.api.contractor", [])
	.factory("Contractor", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/contractors";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				create: function(contractor) {
					return $http.post("/api/v1/contractors/", { contractor: contractor });
				},
				update: function(id, contractor) {
					return $http.put("/api/v1/contractors/" + id, { contractor: contractor });
				},
				get: function(id) {
					return $http.get("/api/v1/contractors/" + id);
				},
				destroy: function(id) {
					/*jshint -W024*/
					return $http.delete("/api/v1/contractors/" + id);
				}
			};
		}
	]);