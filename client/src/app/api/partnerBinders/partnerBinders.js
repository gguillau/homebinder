angular
	.module("hb.api.partnerBinder", [])
	.factory("PartnerBinder", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/partner_binders";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(id) {
					return $http.get("/api/v1/partner_binders/" + id);
				},
				create: function(partner_binder) {
					return $http.post("/api/v1/partner_binders/", { partner_binder: partner_binder });
				},
				update: function(id, partnerbinder) {
					return $http.put("/api/v1/partner_binders/" + id, { partner_binder: partnerbinder });
				},
				destroy: function(id) {
					/*jshint -W024*/
					return $http.delete("/api/v1/partner_binders/" + id);
				}
			};
		}
	]);
