angular
	.module("hb.api.structure", [])
	.factory("Structure",[
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/structures";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(structureId) {
					return $http.get("/api/v1/structures/" + structureId);
				},
				create: function(structure) {
					return $http.post("/api/v1/structures/", {structure: structure});
				},
				update: function(structureId, structure) {
					return $http.put("/api/v1/structures/" + structureId, {structure: structure});
				},
				destroy: function(structureId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/structures/" + structureId);
				}
			};
		}]);
