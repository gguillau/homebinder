angular
    .module("hb.api.applianceModel", [])
    .factory("ApplianceModel",[
        "$http",
        function($http) {
            return {
                all: function() {
                    return $http.get("/api/v1/appliance_models/");
                }
            };
        }]);