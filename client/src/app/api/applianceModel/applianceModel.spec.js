describe("hb.api.applianceModel", function() {
    'use strict';

    var applianceModel, $httpBackend, response;

    // load the applianceModel module
    beforeEach(module("hb.api.applianceModel"));

    beforeEach(function() {
        inject(function($injector) {
            applianceModel = $injector.get("ApplianceModel");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('ALL', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/appliance_models/")
                .respond(response);

            applianceModel.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });
});