angular
	.module("hb.api.receipt", [])
	.factory("Receipt",[
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/receipts";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(id) {
					return $http.get("/api/v1/receipts/" + id);
				},
				create: function(receipt) {
					return $http.post("/api/v1/receipts/", {receipt: receipt});
				},
				update: function(id, receipt) {
					return $http.put("/api/v1/receipts/" + id, {receipt: receipt});
				},
				destroy: function(id) {
					/*jshint -W024*/
					return $http.delete("/api/v1/receipts/" + id);
				}
			};
		}]);