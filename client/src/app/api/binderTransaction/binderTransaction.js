angular
	.module("hb.api.binderTransaction", [])
	.factory("BinderTransaction",[
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/binder_transactions";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(binderTransactionId) {
					return $http.get("/api/v1/binder_transactions/" + binderTransactionId);
				},
				create: function(binderTransaction) {
					return $http.post("/api/v1/binder_transactions/", {binder_transaction: binderTransaction});
				},
				update: function(binderTransactionId, binderTransaction) {
					return $http.put("/api/v1/binder_transactions/" + binderTransactionId, {binder_transaction: binderTransaction});
				},
				destroy: function(binderTransactionId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/binder_transactions/" + binderTransactionId);
				}
			};
		}]);

