angular
	.module("hb.api.freeTrial", [])
	.factory("FreeTrial",[
		"$http",
		function($http) {
			return {
				create: function(user) {
					return $http.post("/api/v1/free_trials", {user: user});
				}
			};
		}]);