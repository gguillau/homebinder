describe("hb.api.freeTrial", function() {
    'use strict';

    var freeTrial, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.freeTrial"));
    beforeEach(function() {
        inject(function($injector) {
            freeTrial = $injector.get("FreeTrial");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('create', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/free_trials")
                .respond(response);

            freeTrial.create().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});