angular
    .module("hb.api.marketingResource", [])
    .factory("MarketingResource", [
        "$http",
        "hb.utils",
        function($http, utils) {
            return {
                all: function(opts) {
                    var url = "/api/v1/marketing_resources";
                    if (opts) {
                        url += utils.utils.createQueryString(opts);
                    }
                    return $http.get(url);
                },
                get: function(id) {
                    return $http.get("/api/v1/marketing_resources/" + id);
                },
                create: function(marketingResource) {
                    return $http.post("/api/v1/marketing_resources/", marketingResource);
                },
                update: function(id, marketingResource) {
                    return $http.put("/api/v1/marketing_resources/" + id, marketingResource);
                },
                destroy: function(id) {
                    /*jshint -W024*/
                    return $http.delete("/api/v1/marketing_resources/" + id);
                }
            };
        }
    ]);
