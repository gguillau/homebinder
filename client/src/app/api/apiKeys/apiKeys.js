angular
    .module("hb.api.apiKey", [])
    .factory("ApiKey",[
        "$http",
        function($http) {
            return {
                create: function(partner) {
                    return $http.post("/api/v1/api_keys/", {partner: partner});
                }
            };
        }]);