describe("hb.api.apiKey", function() {
    'use strict';

    var apiKey, $httpBackend, response;

    beforeEach(function() {
        module("hb.api.apiKey");

        inject(function($injector) {
            apiKey = $injector.get("ApiKey");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('create', function() {
        it("returns promise", function() {
            response = { data: { items: [] } };

            $httpBackend.expectPOST("/api/v1/api_keys/")
                .respond(response);

            apiKey.create({}).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});