angular
	.module("hb.api.binderContractor", [])
	.factory("BinderContractor", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/binder_contractors";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(binderContractorId) {
					return $http.get("/api/v1/binder_contractors/" + binderContractorId);
				},
				create: function(binderContractor) {
					return $http.post("/api/v1/binder_contractors/", { binder_contractor: binderContractor });
				},
				update: function(binderContractorId, binderContractor) {
					return $http.put("/api/v1/binder_contractors/" + binderContractorId, { binder_contractor: binderContractor });
				},
				destroy: function(binderContractorId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/binder_contractors/" + binderContractorId);
				},
				contact: function(id, payload) {
					return $http.post("/api/v1/binder_contractors/" + id + "/contact", payload);
				}
			};
		}
	]);
