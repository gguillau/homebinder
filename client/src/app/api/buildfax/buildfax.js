angular
    .module("hb.api.buildfax", [])
    .factory("BuildFax", [
        "$http",
        function($http) {
            return {
                getPermits: function(binderId) {
                    return $http.get("/api/v1/buildfax/" + binderId);
                },
                createPermits: function(binderId, permits) {
                    return $http.post("/api/v1/buildfax/" + binderId, {permits: permits});
                }
            };
        }
    ]);
