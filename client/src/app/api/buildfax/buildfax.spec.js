describe("hb.api.buildfax", function() {
    'use strict';

    var buildfax, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.buildfax"));
    beforeEach(function() {
        inject(function($injector) {
            buildfax = $injector.get("BuildFax");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('getPermits', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/buildfax/1")
                .respond(response);

            buildfax.getPermits(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('createPermits', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/buildfax/1")
                .respond(response);

            buildfax.createPermits(1, { name: "test" }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});