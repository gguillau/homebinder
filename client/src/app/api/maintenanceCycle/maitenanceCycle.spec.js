describe("hb.api.maintenanceCycle", function() {
    'use strict';

    var maintenanceCycle, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.maintenanceCycle"));
    beforeEach(function() {
        inject(function($injector) {
            maintenanceCycle = $injector.get("MaintenanceCycle");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('all', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/maintenance_cycles")
                .respond(response);

            maintenanceCycle.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});