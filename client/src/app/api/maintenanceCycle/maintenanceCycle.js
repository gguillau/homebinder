angular
	.module("hb.api.maintenanceCycle", [])
	.factory("MaintenanceCycle",[
		"$http",
		function($http) {
			return {
				all: function() {
					return $http.get("/api/v1/maintenance_cycles");
				}
			};
		}]);