describe("hb.api.propertyType", function() {
    'use strict';

    var propertyType, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.propertyType"));
    beforeEach(function() {
        inject(function($injector) {
            propertyType = $injector.get("PropertyType");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('all', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/property_types/")
                .respond(response);

            propertyType.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});