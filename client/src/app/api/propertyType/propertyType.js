angular
	.module("hb.api.propertyType", [])
	.factory("PropertyType", ["$http", 
		function($http) {
			return {
				all: function() {
					return $http.get("/api/v1/property_types/");
				}
			};
		}]);