describe("hb.api.applianceRecall", function() {
    'use strict';

    var applianceRecall, $httpBackend, response;

    // load the applianceRecall module
    beforeEach(module("hb.api.applianceRecall"));
    beforeEach(module(function($provide) {
        $provide.factory("hb.utils", function() {
            return {
                utils: {
                    create: function() {},
                    createQueryString: function() {}
                }
            };
        });
    }));
    beforeEach(function() {
        inject(function($injector) {
            applianceRecall = $injector.get("ApplianceRecall");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('ALL', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/appliance_recalls")
                .respond(response);

            applianceRecall.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('update', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/appliance_recalls/1")
                .respond(response);

            applianceRecall.update({id: 1}).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('sendEmail', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/appliance_recalls/1/email")
                .respond(response);

            applianceRecall.sendEmail(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });
});