angular
	.module("hb.api.applianceRecall", [])
	.factory("ApplianceRecall", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/appliance_recalls";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				update: function(applianceRecall) {
					return $http.put("/api/v1/appliance_recalls/" + applianceRecall.id, { appliance_recall: applianceRecall });
				},
				/* sends a recall email*/
				sendEmail: function(itemId) {
					return $http.post("/api/v1/appliance_recalls/" + itemId + "/email");
				}
			};
		}
	]);