angular
	.module("hb.api.binderItem", [])
	.factory("BinderItem", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/binder_items";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(binderItemId) {
					return $http.get("/api/v1/binder_items/" + binderItemId);
				},
				create: function(binder_item) {
					return $http.post("/api/v1/binder_items/", { binder_item: binder_item });
				},
				update: function(binderItemId, binder_item, token) {
					return $http.put("/api/v1/binder_items/" + binderItemId, { binder_item: binder_item, token: token });
				},
				destroy: function(binderItemId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/binder_items/" + binderItemId);
				}
			};
		}
	]);
