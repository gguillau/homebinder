describe("hb.api.binder", function() {
    'use strict';

    var binder, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.binder"));
    beforeEach(module(function($provide) {
        $provide.factory("hb.utils", function() {
            return {
                utils: {
                    create: function() {},
                    createQueryString: function() {}
                }
            };
        });

        $provide.factory("Event", function() {
            return {
                create: {}
            };
        });
    }));
    beforeEach(function() {
        inject(function($injector) {
            binder = $injector.get("Binder");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('ALL', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/bindersundefined")
                .respond(response);

            binder.all({ id: 1 }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('GET', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/binders/1")
                .respond(response);

            binder.get(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('create', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/binders")
                .respond(response);

            binder.create({ name: "test" }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('batch', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/binders/batch")
                .respond(response);

            binder.batch({ name: "test" }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('update', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/binders/1")
                .respond(response);

            binder.update(1, { name: "test" }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('destroy', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectDELETE("/api/v1/binders/1")
                .respond(response);

            binder.destroy(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('binders', function() {
        it("returns array", function() {
            expect(binder.binders()).toEqual([]);
        });
    });

    describe('add_user', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/binders/1/acl")
                .respond(response);

            binder.add_user(1, {}).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('get_acl', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/binders/1/acl")
                .respond(response);

            binder.get_acl(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('remove_acl', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectDELETE("/api/v1/binders/1/acl/1")
                .respond(response);

            binder.remove_acl(1, 1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('add_partner', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/binders/1/partner")
                .respond(response);

            binder.add_partner(1, {}).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('getPartner', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/binders/1/partner")
                .respond(response);

            binder.getPartner(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('remove_partner', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectDELETE("/api/v1/binders/1/partner/1")
                .respond(response);

            binder.remove_partner(1, 1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('orphan', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/binders/1/orphan")
                .respond(response);

            binder.orphan(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('sendFeedback', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/binders/1/feedback")
                .respond(response);

            binder.sendFeedback(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('requestServices', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/binders/1/services")
                .respond(response);

            binder.requestServices(1, {}, "").then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('requestSolarServices', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };
            $httpBackend.expectPUT("/api/v1/binders/1/solar_services")
                .respond(response);

            binder.requestSolarServices(1, "").then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('secure24', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/binders/1/secure24")
                .respond(response);

            binder.secure24(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('declineServices', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectDELETE("/api/v1/binders/1/services")
                .respond(response);

            binder.declineServices(1, "").then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('learnMoreApr', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/binders/1/aprWidget")
                .respond(response);

            binder.learnMoreApr(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('inventoryReport', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/binders/1/inventory_report")
                .respond(response);

            binder.inventoryReport(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });
});