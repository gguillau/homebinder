angular
	.module("hb.api.binder", [])
	.factory("Binder", ["$http", "$rootScope", "hb.utils", "Event",
		function($http, $rootScope, util, Event) {
			var binders = [];

			return {
				all: function(opts) {
					var url = "/api/v1/binders";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}

					return $http.get(url).success(
						function(result) {
							binders = result.items;
							$rootScope.$broadcast("binder.all");
						});
				},
				get: function(binderId) {
					return $http.get("/api/v1/binders/" + binderId);
				},
				create: function(binder) {
					return $http.post("/api/v1/binders", {
						binder: binder
					});
				},
				batch: function(payload) {
					return $http.post("/api/v1/binders/batch", payload);
				},
				update: function(binderId, binder) {
					return $http.put("/api/v1/binders/" + binderId, {
						binder: binder
					});
				},
				destroy: function(binderId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/binders/" + binderId);
				},
				binders: function() {
					return binders;
				},
				add_user: function(id, data) {
					return $http.post("/api/v1/binders/" + id + "/acl", data);
				},
				get_acl: function(id) {
					return $http.get("/api/v1/binders/" + id + "/acl");
				},
				remove_acl: function(id, user_id) {
					/*jshint -W024*/
					return $http.delete("/api/v1/binders/" + id + "/acl/" + user_id);
				},
				add_partner: function(id, data) {
					return $http.post("/api/v1/binders/" + id + "/partner", {
						data: data
					});
				},
				getPartner: function(id) {
					return $http.get("/api/v1/binders/" + id + "/partner");
				},
				remove_partner: function(id, partner_id) {
					/*jshint -W024*/
					return $http.delete("/api/v1/binders/" + id + "/partner/" + partner_id);
				},
				orphan: function(id) {
					return $http.put("/api/v1/binders/" + id + "/orphan");
				},
				sendFeedback: function(id) {
					return $http.put("/api/v1/binders/" + id + "/feedback");
				},
				requestServices: function(id, services, email) {
					return $http.put("/api/v1/binders/" + id + "/services", { services: services, email: email });
				},
				requestSolarServices: function(id, email) {
					return $http.put("/api/v1/binders/" + id + "/solar_services", { email: email });
				},
				secure24: function(id) {
					return $http.put("/api/v1/binders/" + id + "/secure24");
				},
				declineServices: function(id, email) {
					return $http.delete("/api/v1/binders/" + id + "/services", { email: email });
				},
				learnMoreApr: function(id) {
					return $http.put("/api/v1/binders/" + id + "/aprWidget");
				},
				inventoryReport: function(id) {
					return $http.get("/api/v1/binders/" + id + "/inventory_report");
				},
				capitalExpenseReport: function(id) {
					return $http.get("/api/v1/binders/" + id + "/capital_expense_report");
				},
				checkExisting: function(data) {
					return $http.post("/api/v1/binders/property", data);
				}
			};
		}
	]);