describe("hb.api.note", function() {
    'use strict';

    var note, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.note"));
    beforeEach(function() {
        inject(function($injector) {
            note = $injector.get("Note");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('ALL', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/tests/1/notes")
                .respond(response);

            note.all("test", 1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('GET', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/tests/1/notes/1")
                .respond(response);

            note.get("test", 1, 1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('create', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/tests/1/notes/")
                .respond(response);

            note.create("test", 1, { name: "test" }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('update', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/tests/1/notes/1")
                .respond(response);

            note.update("test", 1, { note: { id: 1 } }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('destroy', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectDELETE("/api/v1/tests/1/notes/1")
                .respond(response);

            note.destroy("test", 1, 1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});