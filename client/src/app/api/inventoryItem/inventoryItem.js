angular
	.module("hb.api.inventoryItem", [])
	.factory("InventoryItem",[
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/inventory_items";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(inventoryItemId) {
					return $http.get("/api/v1/inventory_items/" + inventoryItemId);
				},
				create: function(inventoryItem) {
					return $http.post("/api/v1/inventory_items/", {inventory_item: inventoryItem});
				},
				update: function(inventoryItemId, inventoryItem) {
					return $http.put("/api/v1/inventory_items/" + inventoryItemId, {inventory_item: inventoryItem});
				},
				destroy: function(inventoryItemId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/inventory_items/" + inventoryItemId);
				}
			};
		}]);