angular
    .module("hb.api.contractorSubType", [])
    .factory("ContractorSubType", [
        "$http",
        "hb.utils",
        function($http, util) {
            return {
                all: function(opts) {
                    var url = "/api/v1/contractor_sub_types";
                    if (opts) {
                        url += util.utils.createQueryString(opts);
                    }
                    return $http.get(url);
                },
                get: function(id) {
                    return $http.get("/api/v1/contractor_sub_types/" + id);
                },
                create: function(contractor_sub_type) {
                    return $http.post("/api/v1/contractor_sub_types/", { contractor_sub_type: contractor_sub_type });
                },
                update: function(id, contractor_sub_type) {
                    return $http.put("/api/v1/contractor_sub_types/" + id, { contractor_sub_type: contractor_sub_type });
                },
                destroy: function(id) {
                    /*jshint -W024*/
                    return $http.delete("/api/v1/contractor_sub_types/" + id);
                }
            };
        }
    ]);