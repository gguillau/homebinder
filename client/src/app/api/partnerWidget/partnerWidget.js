angular
	.module("hb.api.partnerWidget", [])
	.factory("PartnerWidget", [
		"$http",
		"hb.utils",
		function($http, utils) {
			return {
				all: function(opts) {
					var url = "/api/v1/partner_widgets";
					if (opts) {
						url += utils.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(id) {
					return $http.get("/api/v1/partner_widgets/" + id);
				},
				create: function(partner_widget) {
					return $http.post("/api/v1/partner_widgets/", {
						partner_widget: partner_widget
					});
				},
				update: function(partnerWidgetId, partnerWidget) {
					return $http.put("/api/v1/partner_widgets/" + partnerWidgetId, { partner_widget: partnerWidget });
				},
				destroy: function(id) {
					/*jshint -W024*/
					return $http.delete("/api/v1/partner_widgets/" + id);
				}
			};
		}
	]);