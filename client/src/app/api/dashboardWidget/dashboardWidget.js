angular
    .module("hb.api.dashboardWidget", [])
    .factory("DashboardWidget", [
        "$http",
        "hb.utils",
        function($http, utils) {
            return {
                all: function(opts) {
                    var url = "/api/v1/dashboard_widgets";
                    if (opts) {
                        url += utils.utils.createQueryString(opts);
                    }
                    return $http.get(url);
                },
                get: function(id) {
                    return $http.get("/api/v1/dashboard_widgets/" + id);
                },
                create: function(data) {
                    return $http.post("/api/v1/dashboard_widgets", data);
                },
                update: function(id, data) {
                    return $http.put("/api/v1/dashboard_widgets/" + id, data);
                },
                destroy: function(id) {
                    /*jshint -W024*/
                    return $http.delete("/api/v1/dashboard_widgets/" + id);
                }
            };
        }
    ]);