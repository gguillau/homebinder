angular
	.module("hb.api.heatSource", [])
	.factory("HeatSource",[
		"$http",
		function($http) {
			return {
				all: function() {
					return $http.get("/api/v1/heat_sources/");
				}
			};
		}]);