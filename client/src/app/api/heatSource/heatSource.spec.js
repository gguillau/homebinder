describe("hb.api.heatSource", function() {
    'use strict';

    var heatSource, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.heatSource"));
    beforeEach(function() {
        inject(function($injector) {
            heatSource = $injector.get("HeatSource");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('all', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/heat_sources/")
                .respond(response);

            heatSource.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});