angular
    .module("hb.api.event", [])
    .factory("Event", [
        "$http",
        "hb.utils",
        function($http, util) {
            return {
                all: function(opts) {
                    var url = "/api/v1/events";
                    if (opts) {
                        url += util.utils.createQueryString(opts);
                    }
                    return $http.get(url);
                },
                create: function(event) {
                    return $http.post("/api/v1/events/", { event: event });
                }
            };
        }
    ]);