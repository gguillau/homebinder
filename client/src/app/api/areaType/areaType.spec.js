describe("hb.api.areaType", function() {
    'use strict';

    var areaType, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.areaType"));
    beforeEach(function() {
        inject(function($injector) {
            areaType = $injector.get("AreaType");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('ALL', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/area_types/")
                .respond(response);

            areaType.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});