angular
    .module("hb.api.areaType", [])
    .factory("AreaType",[
        "$http",
        function($http) {
            return {
                all: function() {
                    return $http.get("/api/v1/area_types/");
                }
            };
        }]);