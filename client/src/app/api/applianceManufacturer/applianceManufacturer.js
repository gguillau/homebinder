angular
    .module("hb.api.applianceManufacturer", [])
    .factory("ApplianceManufacturer",[
        "$http",
        function($http) {
            return {
                all: function() {
                    return $http.get("/api/v1/appliance_manufacturers/");
                },
                unverified: function(){
                    return $http.get("/api/v1/appliance_manufacturers/unverified");
                },
                update: function(manufacturer, id) {
					return $http.put("/api/v1/appliance_manufacturers/" + id, {typeahead: manufacturer});
				},
                destroy: function(id){
                    /*jshint -W024*/
                    return $http.delete("/api/v1/appliance_manufacturers/" + id);
                }
            };
        }]);