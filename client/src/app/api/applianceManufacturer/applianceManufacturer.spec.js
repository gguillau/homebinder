describe("hb.api.applianceManufacturer", function() {
    'use strict';

    var applianceManufacturer, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.applianceManufacturer"));

    beforeEach(function() {
        inject(function($injector) {
            applianceManufacturer = $injector.get("ApplianceManufacturer");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('ALL', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/appliance_manufacturers/")
                .respond(response);

            applianceManufacturer.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('unverified', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/appliance_manufacturers/unverified")
                .respond(response);

            applianceManufacturer.unverified().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('update', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/appliance_manufacturers/1")
                .respond(response);

            applianceManufacturer.update({}, 1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('destroy', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectDELETE("/api/v1/appliance_manufacturers/1")
                .respond(response);

            applianceManufacturer.destroy(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});