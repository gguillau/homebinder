describe("PromoCode", function() {
    var response,
        PromoCode,
        $httpBackend;

    // load the homebinder module
    beforeEach(module("homebinder"));

    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        PromoCode = $injector.get("PromoCode");
        $httpBackend = $injector.get("$httpBackend");
    }));

    describe("verify", function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/promo_codes/verify")
                .respond(response);

            PromoCode.verify().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});