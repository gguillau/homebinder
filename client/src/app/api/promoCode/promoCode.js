angular
	.module("hb.api.promoCode", [])
	.factory("PromoCode", [
		"hbApiBase",
		"$http",
		"$interpolate",
		"hb.utils",
		function(ApiBase, $http, $interpolate, utils) {
			var PromoCode = function() {
				ApiBase.call(this);
				this.controller = "promo_codes";
			};

			PromoCode.prototype = Object.create(ApiBase.prototype);

			angular.extend(PromoCode.prototype, {
				verify: function(data) {
					var url = this.baseUrl + this.controller;
					url += "/verify";
					return $http.post(url, data);
				}
			});

			return new PromoCode();
		}
	]);