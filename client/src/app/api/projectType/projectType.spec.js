describe("hb.api.projectType", function() {
    'use strict';

    var projectType, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.projectType"));
    beforeEach(function() {
        inject(function($injector) {
            projectType = $injector.get("ProjectType");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('all', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/project_types/")
                .respond(response);

            projectType.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});