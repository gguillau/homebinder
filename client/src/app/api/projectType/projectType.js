angular
	.module("hb.api.projectType", [])
    .factory("ProjectType",[
        "$http",
        function($http) {
            return {
                all: function() {
                    return $http.get("/api/v1/project_types/");
                }
            };
        }]);