angular
	.module("hb.api.finishModel", [])
    .factory("FinishModel",["$http",
        function($http) {
            return {
                all: function() {
                    return $http.get("/api/v1/finish_models");
                }
            };
        }]);