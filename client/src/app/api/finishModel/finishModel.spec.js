describe("hb.api.finishModel", function() {
    'use strict';

    var finishModel, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.finishModel"));
    beforeEach(function() {
        inject(function($injector) {
            finishModel = $injector.get("FinishModel");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('ALL', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/finish_models")
                .respond(response);

            finishModel.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});