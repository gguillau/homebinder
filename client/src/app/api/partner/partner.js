angular
	.module("hb.api.partner", [])
	.factory("Partner", [
		"$http",
		"hb.utils",
		"$rootScope",
		"Event",
		function($http, utils, $rootScope, Event) {
			function getAgentSettings() {
				return {
					navigation: {
						binders: true,
						users: true,
						buildfax: true,
						apr: true,
						analytics: true,
						resources: true,
						warranties: true,
						billing: true,
						homepros: true,
						agents: true
					}
				};
			}

			function getInspectorSettings() {
				return {
					navigation: {
						binders: true,
						users: true,
						isn: true,
						settings: true,
						agents: true,
						buildfax: true,
						apr: true,
						analytics: true,
						resources: true,
						warranties: true,
						billing: true,
						homepros: true
					}
				};
			}

			function getLenderSettings() {
				return {
					navigation: {
						binders: true,
						agents: true,
						homepros: true,
						users: true,
						settings: true,
						buildfax: false,
						apr: false,
						analytics: true,
						resources: true,
						warranties: false,
						billing: false
					}
				};
			}

			function getDefaultSettings() {
				return {
					navigation: {
						binders: true,
						agents: false,
						users: true,
						homepros: true,
						settings: true,
						buildfax: false,
						apr: true,
						analytics: false,
						resources: true,
						warranties: false,
						billing: false
					}
				};
			}

			return {
				all: function(opts) {
					var url = "/api/v1/partners";
					if (opts) {
						url += utils.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(partnerId) {
					return $http.get("/api/v1/partners/" + partnerId).then(
						//////////////////// This is temporary code until we build a UI for partners to set their own config ////////////
						function(response) {
							var partner = response.data;
							switch (partner.partner_type) {
								case "broker":
									response.data.settings = getAgentSettings();
									break;
								case "inspector":
									response.data.settings = getInspectorSettings();
									break;
								case "lender":
									response.data.settings = getLenderSettings();
									break;
								case "builder":
									response.data.settings = getLenderSettings();
									break;
								default:
									response.data.settings = getDefaultSettings();
									break;
							}
							return response;
						}
					);
					///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				},
				create: function(registration) {
					return $http.post("/api/v1/partners/", registration);
				},
				update: function(partnerId, partner) {
					return $http.put("/api/v1/partners/" + partnerId, {
						partner: partner
					});
				},
				destroy: function(partnerId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/partners/" + partnerId);
				},
				get_apr_config: function(partnerId) {
					return $http.get("/api/v1/partners/" + partnerId + "/apr_config");
				},
				onboard: function(partnerId, partner) {
					return $http.put("/api/v1/partners/" + partnerId + "/onboarding", {
						partner: partner
					});
				},
				removeUser: function(partnerId, userId) {
					/*jshint -W024*/
					return $http.delete('/api/v1/partners/' + partnerId + '/users/' + userId);
				},
				getFootprints: function(partnerId) {
					return $http.get('/api/v1/partners/' + partnerId + '/footprints');
				},
				createTemplate: function(partnerId, data) {
					return $http.post('/api/v1/partners/' + partnerId + '/binder_templates', data);
				},
				dashboards: function(id) {
					return $http.get("/api/v1/partners/" + id + "/dashboards");
				},
				import: function(partnerId, options) {
					return $http.post('/api/v1/partners/' + partnerId + '/import', options);
				},
				analyticsOverview: function(partnerId) {
					return $http.get("/api/v1/partners/" + partnerId + "/analytics/overview");
				},
				analyticsLogins: function(partnerId) {
					return $http.get("/api/v1/partners/" + partnerId + "/analytics/logins");
				},
				analyticsAgents: function(partnerId) {
					return $http.get("/api/v1/partners/" + partnerId + "/analytics/agents");
				},
				analyticsReminders: function(partnerId) {
					return $http.get("/api/v1/partners/" + partnerId + "/analytics/maintenance_reminders");
				},
				downloadReminders: function(partnerId) {
					return $http.get("/api/v1/partners/" + partnerId + "/analytics/maintenance_reminders/download");
				},
				downloadClients: function(partnerId) {
					return $http.get("/api/v1/partners/" + partnerId + "/analytics/logins/download");
				},
				resources: function(partnerId) {
					return $http.get("/api/v1/partners/" + partnerId + "/resources");
				},
				updateRole: function(id, user) {
					return $http.put("/api/v1/partners/" + id + "/users/" + user.id + "/role/", {
						user: user
					});
				},
				automation: function(key) {
					return $http.get("/api/v1/partners/" + key + "/automation");
				},
				abs: function(key, data, api_key) {
					$http.defaults.headers.common['HB-ApiKey'] = api_key;
					return $http.post("/api/v1/partners/" + key + "/binders", data);
				},
				enableWarrantyAccount: function(partnerId) {
					return $http.post("/api/v1/partners/" + partnerId + "/warranties/enable");
				},
				warrantyPlans: function(partnerId) {
					return $http.get("/api/v1/partners/" + partnerId + "/warranty_plans");
				},
				excludedWidgets: function(partnerId) {
					return $http.get("/api/v1/partners/" + partnerId + "/widget_exclusions");
				}
			};
		}
	]);
