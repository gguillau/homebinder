describe("hb.api.paintSheen", function() {
    'use strict';

    var paintSheen, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.paintSheen"));
    beforeEach(function() {
        inject(function($injector) {
            paintSheen = $injector.get("PaintSheen");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('all', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/paint_sheens/")
                .respond(response);

            paintSheen.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});