angular
	.module("hb.api.paintSheen", [])
    .factory("PaintSheen",[
        "$http",
        function($http) {
            return {
                all: function() {
                    return $http.get("/api/v1/paint_sheens/");
                }
            };
        }]);