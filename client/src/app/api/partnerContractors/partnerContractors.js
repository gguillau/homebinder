angular
	.module("hb.api.partnerContractor", [])
	.factory("PartnerContractor", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/partner_contractors";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(id) {
					return $http.get("/api/v1/partner_contractors/" + id);
				},
				create: function(partner_contractor) {
					return $http.post("/api/v1/partner_contractors/", { partner_contractor: partner_contractor });
				},
				update: function(id, partner_contractor) {
					return $http.put("/api/v1/partner_contractors/" + id, { partner_contractor: partner_contractor });
				},
				destroy: function(id) {
					/*jshint -W024*/
					return $http.delete("/api/v1/partner_contractors/" + id);
				}
			};
		}
	]);
