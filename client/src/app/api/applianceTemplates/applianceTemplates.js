angular
	.module("hb.api.applianceTemplates", [])
	.factory("ApplianceTemplates", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/appliance_templates";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				create: function(templateId, app) {
					return $http.post('/api/v1/binder_templates/' + templateId + '/appliance_templates/', { appliance_template: app });
				},
				get: function(itemId) {
					return $http.get("/api/v1/appliance_templates/" + itemId);
				},
				update: function(appId, appliance) {
					return $http.put('/api/v1/appliance_templates/' + appId, { appliance_template: appliance });
				},
				destroy: function(appId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/appliance_templates/" + appId);
				}
			};
		}
	]);
