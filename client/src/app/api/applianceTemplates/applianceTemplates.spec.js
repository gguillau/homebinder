describe("hb.api.applianceTemplates", function() {
    'use strict';

    var applianceTemplates, $httpBackend, response;

    // load the applianceTemplates module
    beforeEach(module("hb.api.applianceTemplates"));
    beforeEach(module(function($provide) {
        $provide.factory("hb.utils", function() {
            return {
                utils: {
                    create: function() {},
                    createQueryString: function() {}
                }
            };
        });
    }));
    beforeEach(function() {
        inject(function($injector) {
            applianceTemplates = $injector.get("ApplianceTemplates");
            $httpBackend = $injector.get("$httpBackend");
        });
    });
    
    describe('ALL', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/appliance_templates")
                .respond(response);

            applianceTemplates.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('create', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/binder_templates/1/appliance_templates/")
                .respond(response);

            applianceTemplates.create(1, {}).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('update', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/appliance_templates/1")
                .respond(response);

            applianceTemplates.update(1, {id: 1}).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('destroy', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectDELETE("/api/v1/appliance_templates/1")
                .respond(response);

            applianceTemplates.destroy(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });
});