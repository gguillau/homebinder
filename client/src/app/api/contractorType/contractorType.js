angular
    .module("hb.api.contractorType", [])
    .factory("ContractorType", [
        "$http",
        "hb.utils",
        function($http, util) {
            return {
                all: function(opts) {
                    var url = "/api/v1/contractor_types";
                    if (opts) {
                        url += util.utils.createQueryString(opts);
                    }
                    return $http.get(url);
                },
                get: function(id) {
                    return $http.get("/api/v1/contractor_types/" + id);
                },
                create: function(contractor_type) {
                    return $http.post("/api/v1/contractor_types/", { contractor_type: contractor_type });
                },
                update: function(id, contractor_type) {
                    return $http.put("/api/v1/contractor_types/" + id, { contractor_type: contractor_type });
                },
                destroy: function(id) {
                    /*jshint -W024*/
                    return $http.delete("/api/v1/contractor_types/" + id);
                }
            };
        }
    ]);