angular
	.module("hb.api.paint", [])
	.factory("Paint",[
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/paints";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(paintId) {
					return $http.get("/api/v1/paints/" + paintId);
				},
				create: function(paint) {
					return $http.post("/api/v1/paints/", {paint: paint});
				},
				update: function(paintId, paint) {
					return $http.put("/api/v1/paints/" + paintId, {paint: paint});
				},
				destroy: function(paintId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/paints/" + paintId);
				}
			};
		}]);
	
