angular
	.module("hb.api.sellerReport", [])
	.factory("SellerReport", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/seller_reports";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}

					return $http.get(url);
				},
				get: function(reportCode, edit) {
					var url = "/api/v1/seller_reports/" + reportCode;
					if (edit) {
						url += "?edit=yes";
					}
					return $http.get(url);
				},
				create: function(report) {
					return $http.post("/api/v1/seller_reports/", { seller_report: report });
				},
				update: function(report) {
					return $http.put("/api/v1/seller_reports/" + report.seller_report.id, report);
				},
				updateDocsAndImages: function(id, data) {
					return $http.put("/api/v1/seller_reports/" + id, data);
				},
				destroy: function(reportCode) {
					/*jshint -W024*/
					return $http.delete("/api/v1/seller_reports/" + reportCode);
				}
			};
		}
	]);