angular
	.module("hb.api.coupon", [])
	.factory("Coupon",[
		"$http",
		function($http) {
			return {
				get: function(couponId) {
					return $http.get("/api/v1/coupons/" + couponId);
				}
			};
		}]);