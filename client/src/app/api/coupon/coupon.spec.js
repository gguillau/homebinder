describe("hb.api.coupon", function() {
    'use strict';

    var coupon, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.coupon"));
    beforeEach(function() {
        inject(function($injector) {
            coupon = $injector.get("Coupon");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('get', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/coupons/1")
                .respond(response);

            coupon.get(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});