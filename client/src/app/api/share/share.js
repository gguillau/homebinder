angular
	.module("hb.api.share", [])
	.factory("Share", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/shares";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(shareId) {
					return $http.get("/api/v1/shares/" + shareId);
				},
				create: function(share) {
					return $http.post("/api/v1/shares/", {
						share: share
					});
				},
				update: function(shareId, share) {
					return $http.put("/api/v1/shares/" + shareId, {
						share: share
					});
				},
				destroy: function(shareId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/shares/" + shareId);
				},
				resend: function(shareId) {
					return $http.put("/api/v1/shares/" + shareId + "/resend");
				}
			};
		}
	]);