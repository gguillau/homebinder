describe("hb.api.inventoryItemType", function() {
    'use strict';

    var inventoryItemType, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.inventoryItemType"));
    beforeEach(function() {
        inject(function($injector) {
            inventoryItemType = $injector.get("InventoryItemType");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('all', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/inventory_item_types/")
                .respond(response);

            inventoryItemType.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});