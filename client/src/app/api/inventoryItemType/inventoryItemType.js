angular
	.module("hb.api.inventoryItemType", [])
    .factory("InventoryItemType",[
        "$http",
        function($http) {
            return {
                all: function() {
                    return $http.get("/api/v1/inventory_item_types/");
                }
            };
        }]);