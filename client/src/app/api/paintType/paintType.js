angular
	.module("hb.api.paintType", [])
    .factory("PaintType",[
        "$http",
        function($http) {
            return {
                all: function() {
                    return $http.get("/api/v1/paint_types/");
                }
            };
        }]);