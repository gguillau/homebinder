describe("hb.api.paintType", function() {
    'use strict';

    var paintType, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.paintType"));
    beforeEach(function() {
        inject(function($injector) {
            paintType = $injector.get("PaintType");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('all', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/paint_types/")
                .respond(response);

            paintType.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});