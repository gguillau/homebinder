angular
	.module("hb.api.maintenanceEvent", [])
	.factory("MaintenanceEvent", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				index: function(opts) {
					var url = "/api/v1/maintenance_events";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				all: function(forMaintenanceItem) {
					return $http.get("/api/v1/maintenance_events?maintenance_item_id=" + forMaintenanceItem);
				},
				get: function(eventId) {
					return $http.get("/api/v1/maintenance_events/" + eventId);
				},
				create: function(maintenanceEvent) {
					return $http.post("/api/v1/maintenance_events/", { maintenance_event: maintenanceEvent });
				},
				update: function(maintenanceEventId, maintenanceEvent) {
					return $http.put("/api/v1/maintenance_events/" + maintenanceEventId, { maintenance_event: maintenanceEvent });
				},
				destroy: function(eventId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/maintenance_events/" + eventId);
				}
			};
		}
	]);