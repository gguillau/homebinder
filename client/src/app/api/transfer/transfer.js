angular
	.module("hb.api.transfer", [])
	.factory("Transfer", [
		"$http",
		"hb.utils",
		function($http, utils) {
			return {
				all: function(opts) {
					var url = "/api/v1/transfers";
					if (opts) {
						url += utils.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(id) {
					return $http.get("/api/v1/transfers/" + id);
				},
				execute: function(id, transfer) {
					return $http.put("/api/v1/transfers/" + id + "/execute", transfer);
				},
				create: function(transfer) {
					return $http.post("/api/v1/transfers/", {
						transfer: transfer
					});
				},
				update: function(transferId, transfer) {
					return $http.put("/api/v1/transfers/" + transferId, {
						transfer: transfer
					});
				},
				resend: function(transferId, transfer) {
					return $http.put("/api/v1/transfers/" + transferId + "/resend", transfer);
				},
				cancel: function(transferId) {
					return $http.put("/api/v1/transfers/" + transferId + "/cancel");
				},
				resume: function(transfer) {
					return $http.put("/api/v1/transfers/" + transfer.id + "/resume", {
						transfer: transfer
					});
				},
				destroy: function(transferId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/transfers/" + transferId);
				},
				decline: function(transferId) {
					return $http.put("/api/v1/transfers/" + transferId + "/decline");
				}
			};
		}
	]);