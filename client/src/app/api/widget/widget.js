angular
    .module("hb.api.widget", [])
    .factory("Widget", [
        "$http",
        "hb.utils",
        function($http, utils) {
            return {
                all: function(opts) {
                    var url = "/api/v1/widgets";
                    if (opts) {
                        url += utils.utils.createQueryString(opts);
                    }
                    return $http.get(url);
                },
                get: function(id) {
                    return $http.get("/api/v1/widgets/" + id);
                },
                create: function(widget) {
                    return $http.post("/api/v1/widgets", { widget: widget });
                },
                update: function(id, widget) {
                    return $http.put("/api/v1/widgets/" + id, { widget: widget });
                },
                destroy: function(id) {
                    /*jshint -W024*/
                    return $http.delete("/api/v1/widgets/" + id);
                },
                byOrg: function(opts) {
                    var orgId = opts.orgId,
                        url = "/api/v1/organizations/" + orgId + "/widgets";

                    if (orgId) {
                        delete opts.orgId;
                    }

                    if (opts) {
                        url += utils.utils.createQueryString(opts);
                    }
                    return $http.get(url);
                },
                byPartner: function(opts) {
                    var partnerId = opts.partnerId,
                        url = "/api/v1/partners/" + partnerId + "/widgets";

                    if (partnerId) {
                        delete opts.partnerId;
                    }

                    if (opts) {
                        url += utils.utils.createQueryString(opts);
                    }
                    return $http.get(url);
                }
            };
        }
    ]);