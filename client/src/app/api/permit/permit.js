angular
	.module("hb.api.permit", [])
	.factory("Permit",[
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/permits";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(permitId) {
					return $http.get("/api/v1/permits/" + permitId);
				},
				create: function(permit) {
					return $http.post("/api/v1/permits/", {permit: permit});
				},
				update: function(permitId, permit) {
					return $http.put("/api/v1/permits/" + permitId, {permit: permit});
				},
				destroy: function(permitId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/permits/" + permitId);
				}
			};
		}]);

