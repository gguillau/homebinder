describe("hb.api.serviceKey", function() {
    'use strict';

    var serviceKey, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.serviceKey"));
    beforeEach(function() {
        inject(function($injector) {
            serviceKey = $injector.get("ServiceKey");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('index', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/service_keys/")
                .respond(response);

            serviceKey.index().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});