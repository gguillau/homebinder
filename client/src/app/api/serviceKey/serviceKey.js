angular
.module("hb.api.serviceKey", [])
.factory("ServiceKey", [
    "$http",
    function($http) {
        return {
            index: function() {
                return $http.get("/api/v1/service_keys/");
            }
        };
    }]);