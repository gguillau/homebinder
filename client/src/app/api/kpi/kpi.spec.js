describe("hb.api.kpi", function() {
    'use strict';

    var kpi, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.kpi"));
    beforeEach(function() {
        inject(function($injector) {
            kpi = $injector.get("Kpi");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('getBinders', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/kpi/binders")
                .respond(response);

            kpi.getBinders().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('getSummary', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/kpi/summary")
                .respond(response);

            kpi.getSummary().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('getUsers', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/kpi/users")
                .respond(response);

            kpi.getUsers().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('getHomeowners', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/kpi/homeowners")
                .respond(response);

            kpi.getHomeowners().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('getEngagement', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/kpi/engagement/fake")
                .respond(response);

            kpi.getEngagement("fake").then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('downloadReport', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/kpi/download")
                .respond(response);

            kpi.downloadReport("fake").then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });
    
    describe('account_summaries', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/kpi/account_summaries")
                .respond(response);

            kpi.account_summaries().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });
    
    describe('downloadHomeownerReport', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/kpi/homeowner_report")
                .respond(response);

            kpi.downloadHomeownerReport().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});