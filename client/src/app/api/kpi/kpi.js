angular
	.module("hb.api.kpi", [])
	.factory("Kpi", [
		"$http",
		function($http) {
			return {
				getBinders: function(boolean_value) {
					return $http.get("/api/v1/kpi/binders");
				},
				getSummary: function() {
					return $http.get("/api/v1/kpi/summary");
				},
				getUsers: function() {
					return $http.get("/api/v1/kpi/users");
				},
				getHomeowners: function() {
					return $http.get("/api/v1/kpi/homeowners");
				},
				getEngagement: function(type) {
					return $http.get("/api/v1/kpi/engagement/" + type);
				},
				downloadReport: function() {
					return $http.get("/api/v1/kpi/download");
				},
				account_summaries: function() {
					return $http.get("/api/v1/kpi/account_summaries", {
						responseType: 'arraybuffer'
					});
				},
				downloadHomeownerReport: function() {
					return $http.post("/api/v1/kpi/homeowner_report");
				}
			};
		}
	]);