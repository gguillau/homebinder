angular
	.module("hb.api.recall", [])
	.factory("Recall", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/recalls";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(id) {
					return $http.get("/api/v1/recalls/" + id);
				},
				create: function(recall) {
					return $http.post("/api/v1/recalls/", { recall: recall });
				},
				update: function(id, recall) {
					return $http.put("/api/v1/recalls/" + id, { recall: recall });
				},
				destroy: function(id) {
					/*jshint -W024*/
					return $http.delete("/api/v1/recalls/" + id);
				},
				download: function() {
					return $http.post("/api/v1/recalls/download");
				},
				runService: function() {
					return $http.post("/api/v1/recalls/run_service");
				}
			};
		}
	]);