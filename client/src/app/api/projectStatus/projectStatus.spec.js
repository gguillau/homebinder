describe("hb.api.projectStatus", function() {
    'use strict';

    var projectStatus, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.projectStatus"));
    beforeEach(function() {
        inject(function($injector) {
            projectStatus = $injector.get("ProjectStatus");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('all', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/project_statuses/")
                .respond(response);

            projectStatus.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});