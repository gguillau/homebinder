angular
	.module("hb.api.projectStatus", [])
    .factory("ProjectStatus",[
        "$http",
        function($http) {
            return {
                all: function() {
                    return $http.get("/api/v1/project_statuses/");
                }
            };
        }]);