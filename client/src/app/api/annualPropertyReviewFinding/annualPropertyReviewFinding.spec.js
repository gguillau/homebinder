describe("AnnualPropertyReviewFinding", function() {
    var AnnualPropertyReviewFinding;

    // load the homebinder module
    beforeEach(module("homebinder"));

    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        AnnualPropertyReviewFinding = $injector.get("AnnualPropertyReviewFinding");
    }));

    describe("index", function() {
        it("throws an error", function() {
            expect(AnnualPropertyReviewFinding.index).toThrow();
        });
    });
});