angular
.module("hb.api.annualPropertyReviewFinding", [])
.factory("AnnualPropertyReviewFinding", [
    "hbApiBase",
    function(ApiBase) {
        var AnnualPropertyReviewFinding = function() {
            ApiBase.call(this);
            this.controller = "annual_property_review_findings";
        };

        AnnualPropertyReviewFinding.prototype = Object.create(ApiBase.prototype);

        angular.extend(AnnualPropertyReviewFinding.prototype, {
            index: function() {
                throw "findings are downloaded as part of annual property review";
            }
        });

        return new AnnualPropertyReviewFinding();
    }
]);