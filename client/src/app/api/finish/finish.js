angular
	.module("hb.api.finish", [])
	.factory("Finish",[
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/finishes";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(finishId) {
					return $http.get("/api/v1/finishes/" + finishId);
				},
				create: function(finish) {
					return $http.post("/api/v1/finishes/", {finish: finish});
				},
				update: function(finishId, finish) {
					return $http.put("/api/v1/finishes/" + finishId, {finish: finish});
				},
				destroy: function(finishId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/finishes/" + finishId);
				}
			};
		}]);