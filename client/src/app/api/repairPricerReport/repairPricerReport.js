angular
	.module("hb.api.repairPricerReport", [])
	.factory("RepairPricerReport", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/repair_pricer_reports";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(id) {
					return $http.get("/api/v1/repair_pricer_reports/" + id);
				},
				create: function(payload) {
					return $http.post("/api/v1/repair_pricer_reports/", payload);
				},
				update: function(id, payload) {
					return $http.put("/api/v1/repair_pricer_reports/" + id, payload);
				},
				destroy: function(id) {
					/*jshint -W024*/
					return $http.delete("/api/v1/repair_pricer_reports/" + id);
				}
			};
		}
	]);