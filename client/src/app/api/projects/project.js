angular
	.module("hb.api.project", [])
	.factory("Project",[
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/projects";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(projectId) {
					return $http.get("/api/v1/projects/" + projectId);
				},
				create: function(project) {
					return $http.post("/api/v1/projects/", {project: project});
				},
				update: function(projectId, project) {
					return $http.put("/api/v1/projects/" + projectId, {project: project});
				},
				destroy: function(projectId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/projects/" + projectId);
				}
			};
		}]);
	