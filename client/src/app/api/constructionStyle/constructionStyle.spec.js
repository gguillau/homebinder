describe("hb.api.constructionStyle", function() {
    'use strict';

    var constructionStyle, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.constructionStyle"));
    beforeEach(function() {
        inject(function($injector) {
            constructionStyle = $injector.get("ConstructionStyle");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('ALL', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/construction_styles/")
                .respond(response);

            constructionStyle.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});