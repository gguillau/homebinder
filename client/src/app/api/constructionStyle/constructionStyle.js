angular
	.module("hb.api.constructionStyle", [])
	.factory("ConstructionStyle",[
		"$http",
		function($http) {
			return {
				all: function() {
					return $http.get("/api/v1/construction_styles/");
				}
			};
		}]);