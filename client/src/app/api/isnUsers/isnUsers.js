angular
	.module("hb.api.isnUsers", [])
	.factory("IsnUsers", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/isn_users";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(id) {
					return $http.get("/api/v1/isn_users/" + id);
				},
				create: function(isn_user) {
					return $http.post("/api/v1/isn_users/", { isn_user: isn_user });
				},
				update: function(id, isn_user) {
					return $http.put("/api/v1/isn_users/" + id, { isn_user: isn_user });
				},
				destroy: function(id) {
					/*jshint -W024*/
					return $http.delete("/api/v1/isn_users/" + id);
				}
			};
		}
	]);