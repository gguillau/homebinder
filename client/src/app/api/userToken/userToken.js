angular
	.module("hb.api.userToken", [])
	.factory("UserToken",[
		"$http",
		function($http) {
			return {
				create: function(credentials) {
					return $http.post("/api/v1/user_tokens", credentials);
				},
				destroy: function(token) {
					/*jshint -W024*/
					return $http.delete("/api/v1/user_tokens/" + token);
				}
			};
		}]);
