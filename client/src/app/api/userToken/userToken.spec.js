describe("hb.api.userToken", function() {
    'use strict';

    var userToken, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.userToken"));
    beforeEach(function() {
        inject(function($injector) {
            userToken = $injector.get("UserToken");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('create', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/user_tokens")
                .respond(response);

            userToken.create({}).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('destroy', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectDELETE("/api/v1/user_tokens/")
                .respond(response);

            userToken.destroy("").then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});