angular
	.module("hb.api.buildFaxReport", [])
	.factory("BuildFaxReport", [
		"$http",
		"hb.utils",
		function($http, utils) {
			return {
				all: function(opts) {
					var url = "/api/v1/build_fax_reports";
					if (opts) {
						url += utils.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(id) {
					return $http.get("/api/v1/build_fax_reports/" + id);
				},
				update: function(id, buildFaxReport) {
					return $http.put("/api/v1/build_fax_reports/" + id, {
						build_fax_report: buildFaxReport
					});
				},
				create: function(data) {
					return $http.post("/api/v1/build_fax_reports/", {
						build_fax_report: data
					});
				},
				search: function(data) {
					return $http.post("/api/v1/build_fax_reports/search", {
						search: data
					});
				},
				destroy: function(id) {
					/*jshint -W024*/
					return $http.delete("/api/v1/build_fax_reports/" + id);
				}
			};
		}
	]);
