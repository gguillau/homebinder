angular
	.module("hb.api.inspectionReportJob", [])
	.factory("InspectionReportJob", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/inspection_report_jobs";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(id) {
					return $http.get("/api/v1/inspection_report_jobs/" + id);
				},
				create: function(payload) {
					return $http.post("/api/v1/inspection_report_jobs/", payload);
				},
				update: function(id, payload) {
					return $http.put("/api/v1/inspection_report_jobs/" + id, payload);
				},
				destroy: function(id) {
					/*jshint -W024*/
					return $http.delete("/api/v1/inspection_report_jobs/" + id);
				}
			};
		}
	]);