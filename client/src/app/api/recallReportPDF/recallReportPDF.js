angular
	.module("hb.api.recallReportPDF", [])
	.factory("RecallReportPDF",[
		"$http",
		function($http) {
			return {
				get: function(binder_id) {
					return $http.get("/api/v1/recall_report_pdf?binder_id=" + binder_id, { responseType: 'arraybuffer' });
				}
			};
		}]);