describe("hb.api.recallReportPDF", function() {
    'use strict';

    var recallReportPDF, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.recallReportPDF"));
    beforeEach(function() {
        inject(function($injector) {
            recallReportPDF = $injector.get("RecallReportPDF");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('get', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/recall_report_pdf?binder_id=1")
                .respond(response);

            recallReportPDF.get(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});