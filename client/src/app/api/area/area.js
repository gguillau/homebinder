angular
	.module("hb.api.area", [])
	.factory("Area", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/areas";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(areaId) {
					return $http.get("/api/v1/areas/" + areaId);
				},
				create: function(area) {
					return $http.post("/api/v1/areas/", { area: area });
				},
				update: function(areaId, area) {
					return $http.put("/api/v1/areas/" + areaId, { area: area });
				},
				destroy: function(areaId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/areas/" + areaId);
				}
			};
		}
	]);
