angular
	.module("hb.api.binderTemplate", [])
	.factory("BinderTemplate", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/binder_templates";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(templateId) {
					return $http.get('/api/v1/binder_templates/' + templateId);
				},
				update: function(templateId, template) {
					return $http.put("/api/v1/binder_templates/" + templateId, template);
				},
				copy: function(templateId) {
					return $http.get("/api/v1/binder_templates/" + templateId + "/copy");
				},
				destroy: function(templateId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/binder_templates/" + templateId);
				}
			};
		}
	]);
