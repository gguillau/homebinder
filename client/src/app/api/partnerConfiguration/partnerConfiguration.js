angular
	.module("hb.api.partnerConfiguration", [])
	.factory("PartnerConfiguration", [
		"hbApiBase",
		"$http",
		"$interpolate",
		"hb.utils",
		function(ApiBase, $http, $interpolate, utils) {
			var PartnerConfiguration = function() {
				ApiBase.call(this);
				this.controller = "partner_configurations";
			};

			PartnerConfiguration.prototype = Object.create(ApiBase.prototype);

			angular.extend(PartnerConfiguration.prototype);

			return new PartnerConfiguration();
		}
	]);