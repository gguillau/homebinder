angular
	.module("hb.api.subscription", [])
	.factory("Subscription", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/subscriptions";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(subscriptionId) {
					return $http.get("/api/v1/subscriptions/" + subscriptionId);
				},
				create: function(subscription) {
					return $http.post("/api/v1/subscriptions/", { subscription: subscription });
				},
				update: function(subscriptionId, subscription) {
					return $http.put("/api/v1/subscriptions/" + subscriptionId, { subscription: subscription });
				},
				destroy: function(subscriptionId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/subscriptions/" + subscriptionId);
				},
				allForUser: function() {
					return $http.get("/api/v1/subscriptions?user=x");
				},
				save: function(action, subscription, card, coupon) {
					var data = {
						subscription_action: action,
						subscription: subscription,
						card: card,
						coupon: coupon
					};

					return $http.put("/api/v1/subscriptions/" + subscription.id, data);
				}
			};
		}
	]);