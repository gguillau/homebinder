describe("hb.api.userProfile", function() {
    'use strict';

    var userProfile, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.userProfile"));
    beforeEach(function() {
        inject(function($injector) {
            userProfile = $injector.get("UserProfile");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('get', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/user_profiles/")
                .respond(response);

            userProfile.get().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('update', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/user_profiles/1")
                .respond(response);

            userProfile.update(1, {}).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});