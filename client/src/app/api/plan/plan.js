angular
	.module("hb.api.plan", [])
	.factory("Plan",[
		"$http",
		function($http) {
			return {
				get: function(planId) {
					return $http.get("/api/v1/plans/" + planId);
				}
			};
		}]);