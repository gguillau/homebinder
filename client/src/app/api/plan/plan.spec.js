describe("hb.api.plan", function() {
    'use strict';

    var plan, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.plan"));
    beforeEach(function() {
        inject(function($injector) {
            plan = $injector.get("Plan");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('all', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/plans/1")
                .respond(response);

            plan.get(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});