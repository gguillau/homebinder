describe("hb.api.organization", function() {
    'use strict';

    var organization, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.organization"));
    beforeEach(module(function($provide) {
        $provide.factory("hb.utils", function() {
            return {
                utils: {
                    create: function() {},
                    createQueryString: function() {}
                }
            };
        });
    }));
    beforeEach(function() {
        inject(function($injector) {
            organization = $injector.get("Organization");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('ALL', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/organizationsundefined")
                .respond(response);

            organization.all({ id: 1 }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('GET', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/organizations/1")
                .respond(response);

            organization.get(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('create', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/organizations")
                .respond(response);

            organization.create({ name: "test" }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('update', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/organizations/1")
                .respond(response);

            organization.update({ id: 1, name: "test" }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('destroy', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectDELETE("/api/v1/organizations/1")
                .respond(response);

            organization.destroy(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('partners', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/organizations/1/partners")
                .respond(response);

            organization.partners(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('addPartners', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/organizations/1/partners")
                .respond(response);

            organization.addPartners(1, {}).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('removePartner', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectDELETE("/api/v1/organizations/1/partners/1")
                .respond(response);

            organization.removePartner(1, 1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('users', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/organizations/1/users")
                .respond(response);

            organization.users(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('addUsers', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/organizations/1/users")
                .respond(response);

            organization.addUsers(1, {}).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('removeUser', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectDELETE("/api/v1/organizations/1/users/1")
                .respond(response);

            organization.removeUser(1, 1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('resources', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/organizations/1/resources")
                .respond(response);

            organization.resources(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});