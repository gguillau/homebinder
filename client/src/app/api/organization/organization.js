angular
    .module("hb.api.organization", [])
    .factory("Organization", [
        "$http",
        "hb.utils",
        function($http, utils) {
            return {
                all: function(opts) {
                    var url = "/api/v1/organizations";
                    if (opts) {
                        url += utils.utils.createQueryString(opts);
                    }
                    return $http.get(url);
                },
                get: function(id) {
                    return $http.get("/api/v1/organizations/" + id);
                },
                create: function(org) {
                    return $http.post("/api/v1/organizations", {
                        organization: org
                    });
                },
                update: function(org) {
                    var id = org.id;
                    delete org.id;
                    return $http.put("/api/v1/organizations/" + id, {
                        organization: org
                    });
                },
                destroy: function(id) {
                    /*jshint -W024*/
                    return $http.delete("/api/v1/organizations/" + id);
                },
                partners: function(id) {
                    return $http.get("/api/v1/organizations/" + id + "/partners");
                },
                addPartners: function(id, partners) {
                    return $http.post("/api/v1/organizations/" + id + "/partners", {
                        partners: partners
                    });
                },
                removePartner: function(id, partnerId) {
                    /*jshint -W024*/
                    return $http.delete("/api/v1/organizations/" + id + "/partners/" + partnerId);
                },
                users: function(id) {
                    return $http.get("/api/v1/organizations/" + id + "/users");
                },
                addUsers: function(id, users) {
                    return $http.post("/api/v1/organizations/" + id + "/users", {
                        users: users
                    });
                },
                removeUser: function(id, userId) {
                    /*jshint -W024*/
                    return $http.delete("/api/v1/organizations/" + id + "/users/" + userId);
                },
                resources: function(id) {
                    return $http.get("/api/v1/organizations/" + id + "/resources");
                }
            };
        }
    ]);