angular
	.module("hb.api.documentTemplates", [])
	.factory("DocumentTemplates", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/document_templates";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				create: function(templateId, form) {
					return $http.post('/api/v1/binder_templates/' + templateId + '/document_templates/', form, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } });
				},
				update: function(docId, form) {
					return $http.put('/api/v1/document_templates/' + docId, form, { transformRequest: angular.identity, headers: { 'Content-Type': undefined } });
				},
				destroy: function(docId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/document_templates/" + docId);
				}
			};
		}
	]);
