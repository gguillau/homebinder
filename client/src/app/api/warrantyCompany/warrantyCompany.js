angular
    .module("hb.api.warrantyCompany", [])
    .factory("WarrantyCompany", [
        "$http",
        "hb.utils",
        function($http, utils) {
            return {
                all: function(opts) {
                    var url = "/api/v1/warranty_companies";
                    if (opts) {
                        url += utils.utils.createQueryString(opts);
                    }
                    return $http.get(url);
                },
                get: function(id) {
                    return $http.get("/api/v1/warranty_companies/" + id);
                },
                create: function(warrantyCompany) {
                    return $http.post("/api/v1/warranty_companies/", warrantyCompany);
                },
                update: function(id, warrantyCompany) {
                    return $http.put("/api/v1/warranty_companies/" + id, warrantyCompany);
                },
                destroy: function(id) {
                    /*jshint -W024*/
                    return $http.delete("/api/v1/warranty_companies/" + id);
                }
            };
        }
    ]);
