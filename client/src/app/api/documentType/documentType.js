angular
    .module("hb.api.documentType", [])
    .factory("DocumentType", [
        "$http",
        "hb.utils",
        function($http, util) {
            return {
                all: function(opts) {
                    var url = "/api/v1/document_types";
                    if (opts) {
                        url += util.utils.createQueryString(opts);
                    }
                    return $http.get(url);
                },
                get: function(id) {
                    return $http.get("/api/v1/document_types/" + id);
                },
                create: function(document_type) {
                    return $http.post("/api/v1/document_types/", { document_type: document_type });
                },
                update: function(id, document_type) {
                    return $http.put("/api/v1/document_types/" + id, { document_type: document_type });
                },
                destroy: function(id) {
                    /*jshint -W024*/
                    return $http.delete("/api/v1/document_types/" + id);
                }
            };
        }
    ]);