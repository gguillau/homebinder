angular
    .module("hb.api.base", [])
    .factory("hbApiBase", [
        '$http',
        '$interpolate',
        'hb.utils',
        function($http, $interpolate, hbUtils) {
            var ApiBase = function() {
                this.baseUrl = "/api/v1/";
            };

            angular.extend(ApiBase.prototype, {
                all: function(opts) {
                    this.index(opts);
                },
                index: function(opts) {
                    var url = this.baseUrl + this.controller;

                    if (opts) {
                        url += hbUtils.utils.createQueryString(opts);
                    }
                    return $http.get(url);
                },
                get: function(id) {
                    return $http.get(this.baseUrl + this.controller + "/" + id);
                },
                create: function(data) {
                    return $http.post(this.baseUrl + this.controller, data);
                },
                update: function(id, data) {
                    return $http.put(this.baseUrl + this.controller + "/" + id, data);
                },
                destroy: function(id) {
                    /*jshint -W024*/
                    return $http.delete(this.baseUrl + this.controller + "/" + id);
                }
            });

            return ApiBase;
        }
    ]);