describe("hbApiBase", function() {
    'use strict';

    var hbApiBase, $httpBackend, response, TestClass;

    // load the hb.api.base module
    beforeEach(module("hb.api.base"));
    beforeEach(module(function($provide) {
        $provide.factory("hb.utils", function() {
            return {
                utils: {
                    create: function() {},
                    createQueryString: function() {}
                }
            };
        });
    }));
    beforeEach(function() {
        inject(function($injector) {
            hbApiBase = $injector.get("hbApiBase");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    function createTestClass() {
        TestClass = function() {
            hbApiBase.call(this);
        };

        TestClass.prototype = Object.create(hbApiBase.prototype);

        return new TestClass();
    }

    describe('init', function() {
        it("sets the baseUrl", function() {
            var model = createTestClass();
            expect(model.baseUrl).toEqual("/api/v1/");
        });
    });

    describe('ALL', function() {
        it("calls index", function() {
            var model = createTestClass();
            spyOn(model, "index");

            model.all();

            expect(model.index).toHaveBeenCalled();
        });
    });

    describe('INDEX', function() {
        it("returns promise", function() {
            var model = createTestClass();
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/undefinedundefined")
                .respond(response);

            model.index({ name: "test" }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('GET', function() {
        it("returns promise", function() {
            var model = createTestClass();
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/undefined/1")
                .respond(response);

            model.get(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('create', function() {
        it("returns promise", function() {
            var model = createTestClass();
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/undefined")
                .respond(response);

            model.create({ name: "test" }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('update', function() {
        it("returns promise", function() {
            var model = createTestClass();
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/undefined/1")
                .respond(response);

            model.update(1, { name: "test" }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('destroy', function() {
        it("returns promise", function() {
            var model = createTestClass();
            response = { data: { name: "test" } };

            $httpBackend.expectDELETE("/api/v1/undefined/1")
                .respond(response);

            model.destroy(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });
});