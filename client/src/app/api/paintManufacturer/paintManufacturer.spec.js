describe("hb.api.paintManufacturer", function() {
    'use strict';

    var paintManufacturer, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.paintManufacturer"));
    beforeEach(function() {
        inject(function($injector) {
            paintManufacturer = $injector.get("PaintManufacturer");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('all', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/paint_manufacturers/")
                .respond(response);

            paintManufacturer.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});