angular
	.module("hb.api.paintManufacturer", [])
    .factory("PaintManufacturer",[
    "$http",
        function($http) {
            return {
                all: function() {
                    return $http.get("/api/v1/paint_manufacturers/");
                }
            };
        }]);