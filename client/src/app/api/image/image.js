angular
	.module("hb.api.image", [])
	.factory("Img",[
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/images";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(imageId) {
					return $http.get("/api/v1/images/" + imageId);
				},
				create: function(img) {
					return $http.post("/api/v1/images/", {image: img});
				},
				update: function(imgId, img) {
					return $http.put("/api/v1/images/" + imgId, {image: img});
				},
				destroy: function(imageId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/images/" + imageId);
				}
			};
		}]);