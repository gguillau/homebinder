describe("hb.api.sellerReportPDF", function() {
    'use strict';

    var sellerReportPDF, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.sellerReportPDF"));
    beforeEach(function() {
        inject(function($injector) {
            sellerReportPDF = $injector.get("SellerReportPDF");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('download', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/seller_report_pdfs/code?full=1")
                .respond(response);

            sellerReportPDF.download("code", true).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });

        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/seller_report_pdfs/code")
                .respond(response);

            sellerReportPDF.download("code", false).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});