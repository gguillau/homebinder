angular
	.module("hb.api.sellerReportPDF", [])
	.factory("SellerReportPDF", [
		"$http",
		function($http) {
			return {
				download: function(code, full) {
					if (full) {
						return $http.get("/api/v1/seller_report_pdfs/" + code + "?full=1", {
							responseType: 'arraybuffer'
						});
					}
					return $http.get("/api/v1/seller_report_pdfs/" + code, {
						responseType: 'arraybuffer'
					});
				}
			};
		}
	]);