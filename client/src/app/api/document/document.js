angular
	.module("hb.api.document", [])
	.factory("Document",[
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/documents";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(documentId) {
					return $http.get("/api/v1/documents/" + documentId);
				},
				create: function(doc) {
					return $http.post("/api/v1/documents", doc);
				},
				update: function(docId, doc) {
					return $http.put("/api/v1/documents/" + docId, {document: doc});
				},
				destroy: function(documentId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/documents/" + documentId);
				}
			};
		}]);