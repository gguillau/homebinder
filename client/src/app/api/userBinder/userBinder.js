angular
	.module("hb.api.userBinder", [])
	.factory("UserBinder", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/user_binders";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(id) {
					return $http.get("/api/v1/user_binders/" + id);
				},
				create: function(user_binder) {
					return $http.post("/api/v1/user_binders/", { user_binder: user_binder });
				},
				update: function(id, user_binder) {
					return $http.put("/api/v1/user_binders/" + id, { user_binder: user_binder });
				},
				destroy: function(id) {
					/*jshint -W024*/
					return $http.delete("/api/v1/user_binders/" + id);
				},
				find_by_repair_pricer_token: function(id) {
					return $http.get("/api/v1/user_binders/" + id + "/repair_pricer_token");
				},
				find_by_property: function(property) {
					var url = "/api/v1/user_binders/property";
					if (property) {
						url += util.utils.createQueryString(property);
					}
					return $http.get(url);
				}
			};
		}
	]);
