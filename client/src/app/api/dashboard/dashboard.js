angular
    .module("hb.api.dashboard", [])
    .factory("Dashboard", [
        "$http",
        "hb.utils",
        function($http, utils) {
            return {
                all: function(opts) {
                    var url = "/api/v1/dashboards";
					if (opts) {
						url += utils.utils.createQueryString(opts);
					}
                    return $http.get(url);
                },
                get: function(id) {
                    return $http.get("/api/v1/dashboards/" + id);
                },
                create: function(data) {
                    return $http.post("/api/v1/dashboards", data);
                },
                update: function(id, data) {
                    return $http.put("/api/v1/dashboards/" + id, data);
                },
                destroy: function(id) {
                    /*jshint -W024*/
                    return $http.delete("/api/v1/dashboards/" + id);
                },
				forBinder: function(id) {
					return $http.get("/api/v1/binders/" + id + "/dashboard");
				},
				byPartner: function(partnerId, opts) {
					return $http.get("/api/v1/partners/" + partnerId + "/dashboards");
				},
				byOrganization: function(organizationId, opts) {
					return $http.get("/api/v1/organizations/" + organizationId + "/dashboards");
				}
            };
    }]);