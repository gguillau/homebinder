describe("hb.api.appliance", function() {
    'use strict';

    var appliance, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.appliance"));
    beforeEach(module(function($provide) {
        $provide.factory("hb.utils", function() {
            return {
                utils: {
                    create: function() {},
                    createQueryString: function() {}
                }
            };
        });
    }));
    beforeEach(function() {
        inject(function($injector) {
            appliance = $injector.get("Appliance");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('ALL', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/appliances")
                .respond(response);

            appliance.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('GET', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/appliances/1")
                .respond(response);

            appliance.get(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('create', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/appliances/")
                .respond(response);

            appliance.create({ name: "test" }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('update', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/appliances/1")
                .respond(response);

            appliance.update(1, { name: "test" }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('sendEmail', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/appliances/1/email")
                .respond(response);

            appliance.sendEmail(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });
    
    describe('checkForRecalls', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/appliances/1/recalls")
                .respond(response);

            appliance.checkForRecalls(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });
    
    describe('destroy', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectDELETE("/api/v1/appliances/1")
                .respond(response);

            appliance.destroy(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});