angular
	.module("hb.api.appliance", [])
	.factory("Appliance", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/appliances";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(applianceId) {
					return $http.get("/api/v1/appliances/" + applianceId);
				},
				create: function(appliance) {
					return $http.post("/api/v1/appliances/", { appliance: appliance });
				},
				update: function(applianceId, appliance) {
					return $http.put("/api/v1/appliances/" + applianceId, { appliance: appliance });
				},
				/* sends a test maintenance reminder email to the current user*/
				sendEmail: function(itemId) {
					return $http.post("/api/v1/appliances/" + itemId + "/email");
				},
				checkForRecalls: function(itemId) {
					return $http.post("/api/v1/appliances/" + itemId + "/recalls");
				},
				destroy: function(applianceId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/appliances/" + applianceId);
				}
			};
		}
	]);