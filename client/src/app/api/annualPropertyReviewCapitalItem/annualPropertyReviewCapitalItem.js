angular
.module("hb.api.annualPropertyReviewCapitalItem", [])
.factory("AnnualPropertyReviewCapitalItem", [
    "hbApiBase",
    function(ApiBase) {
        var AnnualPropertyReviewCapitalItem = function() {
            ApiBase.call(this);
            this.controller = "annual_property_review_capital_items";
        };

        AnnualPropertyReviewCapitalItem.prototype = Object.create(ApiBase.prototype);

        angular.extend(AnnualPropertyReviewCapitalItem.prototype, {
            index: function() {
                throw "findings are downloaded as part of annual proeprty review";
            }
        });

        return new AnnualPropertyReviewCapitalItem();
    }
]);