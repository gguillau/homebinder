describe("AnnualPropertyReviewCapitalItem", function() {
    var AnnualPropertyReviewCapitalItem;

    // load the homebinder module
    beforeEach(module("homebinder"));

    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        AnnualPropertyReviewCapitalItem = $injector.get("AnnualPropertyReviewCapitalItem");
    }));

    describe("index", function() {
        it("throws an error", function() {
            expect(AnnualPropertyReviewCapitalItem.index).toThrow();
        });
    });
});