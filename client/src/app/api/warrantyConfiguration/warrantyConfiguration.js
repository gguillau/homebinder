angular
    .module("hb.api.warrantyConfiguration", [])
    .factory("WarrantyConfiguration", [
        "$http",
        "hb.utils",
        function($http, utils) {
            return {
                all: function(opts) {
                    var url = "/api/v1/warranty_configurations";
                    if (opts) {
                        url += utils.utils.createQueryString(opts);
                    }
                    return $http.get(url);
                },
                get: function(id) {
                    return $http.get("/api/v1/warranty_configurations/" + id);
                },
                create: function(warrantyConfiguration) {
                    return $http.post("/api/v1/warranty_configurations/", warrantyConfiguration);
                },
                order: function(id, order) {
                    return $http.post("/api/v1/warranty_configurations/" + id + "/orders", {
                        order: order
                    });
                },
                update: function(id, warrantyConfiguration) {
                    return $http.put("/api/v1/warranty_configurations/" + id, warrantyConfiguration);
                },
                destroy: function(id) {
                    /*jshint -W024*/
                    return $http.delete("/api/v1/warranty_configurations/" + id);
                }
            };
        }
    ]);
