describe("hb.api.contractorTemplates", function() {
    'use strict';

    var contractorTemplates, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.contractorTemplates"));
    beforeEach(function() {
        inject(function($injector) {
            contractorTemplates = $injector.get("ContractorTemplates");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('create', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/binder_templates/1/contractor_templates/")
                .respond(response);

            contractorTemplates.create(1, { name: "test" }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('update', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/contractor_templates/1")
                .respond(response);

            contractorTemplates.update(1, { name: "test" }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });
    
    describe('destroy', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectDELETE("/api/v1/contractor_templates/1")
                .respond(response);

            contractorTemplates.destroy(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});