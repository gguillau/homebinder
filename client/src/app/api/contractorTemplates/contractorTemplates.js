angular
	.module("hb.api.contractorTemplates", [])
	.factory("ContractorTemplates", [
		"$http",
		function($http) {
			return {
				create: function(templateId, contr){
					return $http.post('/api/v1/binder_templates/' + templateId + '/contractor_templates/', contr);
				},
				update: function(contrId, contractor){
					return $http.put('/api/v1/contractor_templates/' + contrId, contractor);
				},
				destroy: function(contrId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/contractor_templates/" + contrId);
				}
			};
		}
	]);
