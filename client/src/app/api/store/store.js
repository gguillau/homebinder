angular
	.module("hb.api.store", [])
	.factory("Store",[
		"$http",
		function($http) {
			return {
				all: function() {
					return $http.get("/api/v1/stores/");
				}
			};
		}]);