describe("hb.api.store", function() {
    'use strict';

    var store, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.store"));
    beforeEach(function() {
        inject(function($injector) {
            store = $injector.get("Store");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('all', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/stores/")
                .respond(response);

            store.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});