angular
	.module("hb.api.maintenanceTemplates", [])
	.factory("MaintenanceTemplates", [
		"$http",
		"hb.utils",
		function($http, util) {
			return {
				all: function(opts) {
					var url = "/api/v1/maintenance_templates";
					if (opts) {
						url += util.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(itemId) {
					return $http.get("/api/v1/maintenance_templates/" + itemId);
				},
				create: function(templateId, mi) {
					return $http.post('/api/v1/binder_templates/' + templateId + '/maintenance_templates/', mi);
				},
				update: function(itemId, maintenanceItem) {
					return $http.put('/api/v1/maintenance_templates/' + itemId, maintenanceItem);
				},
				destroy: function(itemId) {
					/*jshint -W024*/
					return $http.delete("/api/v1/maintenance_templates/" + itemId);
				},
				/* sends a test maintenance reminder email to the current user*/
				sendEmail: function(itemId) {
					return $http.post("/api/v1/maintenance_templates/" + itemId + "/email");
				}
			};
		}
	]);
