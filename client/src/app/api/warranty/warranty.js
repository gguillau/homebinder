angular
    .module("hb.api.warranty", [])
    .factory("Warranty", [
        "$http",
        "hb.utils",
        function($http, utils) {
            return {
                all: function(opts) {
                    var url = "/api/v1/warranties";
                    if (opts) {
                        url += utils.utils.createQueryString(opts);
                    }
                    return $http.get(url);
                },
                get: function(id) {
                    return $http.get("/api/v1/warranties/" + id);
                },
                create: function(warranty) {
                    return $http.post("/api/v1/warranties/", warranty);
                },
                update: function(id, warranty) {
                    return $http.put("/api/v1/warranties/" + id, {
                        warranty: warranty
                    });
                },
                destroy: function(id) {
                    /*jshint -W024*/
                    return $http.delete("/api/v1/warranties/" + id);
                },
                downloadSummaries: function(){
                    return $http.post("/api/v1/warranties/download");
                }
            };
        }
    ]);
