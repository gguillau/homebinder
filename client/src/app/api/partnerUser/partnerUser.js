angular
	.module("hb.api.partnerUser", [])
	.factory("PartnerUser", [
		"$http",
		"hb.utils",
		function($http, utils) {
			return {
				all: function(opts) {
					var url = "/api/v1/partner_users";
					if (opts) {
						url += utils.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(id) {
					return $http.get("/api/v1/partner_users/" + id);
				},
				create: function(partner_user) {
					return $http.post("/api/v1/partner_users/", {
						partner_user: partner_user
					});
				},
				update: function(partner_user) {
					var url = "/api/v1/partner_users/" + partner_user.id;
					return $http.put(url, {
						partner_user: partner_user
					});
				},
				destroy: function(id) {
					/*jshint -W024*/
					return $http.delete("/api/v1/partner_users/" + id);
				}
			};
		}
	]);