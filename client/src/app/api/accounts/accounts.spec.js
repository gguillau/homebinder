describe("hb.api.account", function() {
    'use strict';

    var account, $httpBackend, response;

    beforeEach(function() {
        module("hb.api.account");

        inject(function($injector) {
            account = $injector.get("Account");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('create', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/accounts")
                .respond(response);

            account.create({ name: "test" }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('GET', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/accounts/1")
                .respond(response);

            account.get(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('update', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/accounts/1")
                .respond(response);

            account.update(1, { name: "test" }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});