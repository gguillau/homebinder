angular
    .module("hb.api.account", [])
    .factory("Account", [
        "$http",
        function($http) {
            return {
                create: function(account) {
                    return $http.post("/api/v1/accounts", {
                        account: account
                    });
                },
                get: function(id) {
                    return $http.get("/api/v1/accounts/" + id);
                },
                update: function(id, account) {
                    return $http.put("/api/v1/accounts/" + id, {
                        account: account
                    });
                }
            };
        }
    ]);