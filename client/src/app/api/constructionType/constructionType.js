angular
	.module("hb.api.constructionType", [])
	.factory("ConstructionType",[
		"$http",
		function($http) {
			return {
				all: function() {
					return $http.get("/api/v1/construction_types/");
				}
			};
		}]);