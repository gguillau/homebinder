describe("hb.api.constructionType", function() {
    'use strict';

    var constructionType, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.constructionType"));
    beforeEach(function() {
        inject(function($injector) {
            constructionType = $injector.get("ConstructionType");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('ALL', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/construction_types/")
                .respond(response);

            constructionType.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});