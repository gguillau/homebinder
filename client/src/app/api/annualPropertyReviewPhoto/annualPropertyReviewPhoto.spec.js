describe("AnnualPropertyReviewPhoto", function() {
    var AnnualPropertyReviewPhoto;

    // load the homebinder module
    beforeEach(module("homebinder"));

    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        AnnualPropertyReviewPhoto = $injector.get("AnnualPropertyReviewPhoto");
    }));

    describe("index", function() {
        it("throws an error", function() {
            expect(AnnualPropertyReviewPhoto.index).toThrow();
        });
    });
    
    describe("create", function() {
        it("throws an error", function() {
            expect(AnnualPropertyReviewPhoto.create).toThrow();
        });
    });
    
    describe("update", function() {
        it("throws an error", function() {
            expect(AnnualPropertyReviewPhoto.update).toThrow();
        });
    });
});