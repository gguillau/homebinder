angular
.module("hb.api.annualPropertyReviewPhoto", [])
.factory("AnnualPropertyReviewPhoto", [
    "hbApiBase",
    function(ApiBase) {
        var AnnualPropertyReviewFinding = function() {
            ApiBase.call(this);
            this.controller = "annual_property_review_photos";
        };

        AnnualPropertyReviewFinding.prototype = Object.create(ApiBase.prototype);

        angular.extend(AnnualPropertyReviewFinding.prototype, {
            index: function() {
                throw "photos are downloaded as part of annual property review";
            },
            create: function() {
                throw "implemeted using hb uploader";
            },
            update: function() {
                throw "not implemented";
            }
        });

        return new AnnualPropertyReviewFinding();
    }
]);