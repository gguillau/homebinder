describe("AnnualPropertyReview", function() {
    var response,
        AnnualPropertyReview,
        $httpBackend;

    // load the homebinder module
    beforeEach(module("homebinder"));

    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        AnnualPropertyReview = $injector.get("AnnualPropertyReview");
        $httpBackend = $injector.get("$httpBackend");
    }));

    describe("index", function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/annual_property_reviews")
                .respond(response);

            AnnualPropertyReview.index().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe("download", function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/annual_property_reviews/1/download")
                .respond(response);

            AnnualPropertyReview.download(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe("message", function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/annual_property_reviews/1/messages")
                .respond(response);

            AnnualPropertyReview.message(1, {}).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });
    
    describe("requestQuote", function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/annual_property_reviews/1/request_quote")
                .respond(response);

            AnnualPropertyReview.requestQuote(1, {}).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });
});