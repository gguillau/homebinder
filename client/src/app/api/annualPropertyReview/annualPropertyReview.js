angular
	.module("hb.api.annualPropertyReview", [])
	.factory("AnnualPropertyReview", [
		"hbApiBase",
		"$http",
		"hb.utils",
		function(ApiBase, $http, utils) {
			var AnnualPropertyReview = function() {
				ApiBase.call(this);
				this.controller = "annual_property_reviews";
			};

			AnnualPropertyReview.prototype = Object.create(ApiBase.prototype);

			angular.extend(AnnualPropertyReview.prototype, {
				index: function(opts) {
					var url = this.baseUrl;
					url += this.controller;
					
					// add opts for search and paginng
					url += utils.utils.createQueryString(opts);

					return $http.get(url);
				},
				download: function(id) {
					var url = this.baseUrl + this.controller;
					url += "/" + id + "/download";
					return $http.get(url, {
						responseType: 'arraybuffer'
					});
				},
				message: function(id, data) {
					var url = this.baseUrl + this.controller;
					url += "/" + id + "/messages";
					return $http.post(url, data);
				},
				requestQuote: function(id, data) {
					var url = this.baseUrl + this.controller;
					url += "/" + id + "/request_quote";
					return $http.post(url, data);
				}
			});

			return new AnnualPropertyReview();
		}
	]);