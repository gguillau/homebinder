angular
	.module("hb.api.sessions", [])
	.factory("Sessions", [
		"$http",
		"hb.utils",
		function($http, utils) {
			return {
				all: function(opts) {
					var url = "/api/v1/sessions";
					if (opts) {
						url += utils.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(id) {
					return $http.get("/api/v1/sessions/" + id);
				},
				create: function(session) {
					return $http.post("/api/v1/sessions/", {
						session: session
					});
				},
				update: function(session) {
					var url = "/api/v1/sessions/" + session.id;
					return $http.put(url, {
						session: session
					});
				},
				destroy: function(id) {
					/*jshint -W024*/
					return $http.delete("/api/v1/sessions/" + id);
				}
			};
		}
	]);