describe("hb.api.binderBranding", function() {
    'use strict';

    var binderBranding, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.binderBranding"));
    beforeEach(module(function($provide) {
        $provide.factory("hb.utils", function() {
            return {
                utils: {
                    create: function() {},
                    createQueryString: function() {}
                }
            };
        });
    }));
    beforeEach(function() {
        inject(function($injector) {
            binderBranding = $injector.get("BinderBranding");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('ALL', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/binder_brandings")
                .respond(response);

            binderBranding.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('create', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPOST("/api/v1/binder_brandings/")
                .respond(response);

            binderBranding.create({ name: "test" }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('update', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectPUT("/api/v1/binder_brandings/1")
                .respond(response);

            binderBranding.update(1, { name: "test" }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });
    
    describe('destroy', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectDELETE("/api/v1/binder_brandings/1")
                .respond(response);

            binderBranding.destroy(1).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});