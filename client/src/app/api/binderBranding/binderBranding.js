angular
    .module("hb.api.binderBranding", [])
    .factory("BinderBranding", [
        "$http",
        "hb.utils",
        function($http, util) {
            return {
                all: function(opts) {
                    var url = "/api/v1/binder_brandings";
                    if (opts) {
                        url += util.utils.createQueryString(opts);
                    }
                    return $http.get(url);
                },
                create: function(branding) {
                    return $http.post("/api/v1/binder_brandings/", {
                        binder_branding: branding
                    });
                },
                update: function(id, binder_branding) {
                    return $http.put("/api/v1/binder_brandings/" + id, { binder_branding: binder_branding });
                },
                /*jshint -W024*/
                destroy: function(id) {
                    return $http.delete("/api/v1/binder_brandings/" + id);
                }
            };
        }
    ]);
