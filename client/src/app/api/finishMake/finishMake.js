angular
	.module("hb.api.finishMake", [])
    .factory("FinishMake",[
        "$http",
        function($http) {
            return {
                all: function() {
                    return $http.get("/api/v1/finish_makes");
                }
            };
        }]);