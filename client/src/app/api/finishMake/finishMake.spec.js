describe("hb.api.finishMake", function() {
    'use strict';

    var finishMake, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.finishMake"));
    beforeEach(function() {
        inject(function($injector) {
            finishMake = $injector.get("FinishMake");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('ALL', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/finish_makes")
                .respond(response);

            finishMake.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});