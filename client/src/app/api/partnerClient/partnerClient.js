angular
	.module("hb.api.partnerClient", [])
	.factory("PartnerClient", [
		"$http",
		"hb.utils",
		function($http, utils) {
			return {
				all: function(opts) {
					var url = "/api/v1/partner_clients";
					if (opts) {
						url += utils.utils.createQueryString(opts);
					}
					return $http.get(url);
				},
				get: function(id) {
					return $http.get("/api/v1/partner_clients/" + id);
				},
				create: function(partner_client) {
					return $http.post("/api/v1/partner_clients/", {
						partner_client: partner_client
					});
				},
				update: function(partner_client) {
					var url = "/api/v1/partner_clients/" + partner_client.id;
					return $http.put(url, {
						partner_client: partner_client
					});
				},
				destroy: function(id) {
					/*jshint -W024*/
					return $http.delete("/api/v1/partner_clients/" + id);
				}
			};
		}
	]);