angular
	.module("hb.api.customer", [])
	.factory("Customer", [
		"hbApiBase",
		function(ApiBase) {
			var Customer = function() {
				ApiBase.call(this);
				this.controller = "customers";
			};

			Customer.prototype = Object.create(ApiBase.prototype);

			return new Customer();
		}
	]);