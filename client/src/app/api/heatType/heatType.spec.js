describe("hb.api.heatType", function() {
    'use strict';

    var heatType, $httpBackend, response;

    // load the homebinder module
    beforeEach(module("hb.api.heatType"));
    beforeEach(function() {
        inject(function($injector) {
            heatType = $injector.get("HeatType");
            $httpBackend = $injector.get("$httpBackend");
        });
    });

    describe('all', function() {
        it("returns promise", function() {
            response = { data: { name: "test" } };

            $httpBackend.expectGET("/api/v1/heat_types/")
                .respond(response);

            heatType.all().then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

});