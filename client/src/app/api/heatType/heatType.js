angular
	.module("hb.api.heatType", [])
	.factory("HeatType",[
		"$http",
		function($http) {
			return {
				all: function() {
					return $http.get("/api/v1/heat_types/");
				}
			};
		}]);