angular
	.module("hb.session", [])
	.factory('Session', [
		"$window",
		function($window) {
			var JWT_TOKEN = "_hb.twj";
			var LOCALE = "_hb.locale";
			var USER = "_hb.user";
			var BINDER = "_hb.binder";
			var currentJwt,
				currentUser,
				currentLocale;

			function setValue(key, value, remember) {
				// Set the value to session storage
				$window.sessionStorage[key] = JSON.stringify(value);

				// If we want to remember it set it to local storage as well
				if (remember) {
					$window.localStorage[key] = JSON.stringify(value);
				}
			}

			function getValue(key) {
				var value = $window.sessionStorage[key];
				if (typeof value !== "undefined" && value !== "undefined") {
					return JSON.parse(value);
				}
				value = $window.localStorage[key];

				if (typeof value !== "undefined" && value !== "undefined") {
					return JSON.parse(value);
				}
				else {
					return undefined;
				}
			}

			function clearAllStorage() {
				$window.sessionStorage.clear();
				$window.localStorage.clear();
			}

			function parseUser() {
				var jwt = currentJwt || getValue(JWT_TOKEN);
				return jwt ? angular.fromJson($window.atob(jwt.split('.')[1])) : undefined;
			}

			return {
				setJwt: function(jwt, remember) {
					currentJwt = jwt;
					currentUser = undefined;
					setValue(JWT_TOKEN, jwt, remember);
				},
				getJwt: function(jwt) {
					return currentJwt || getValue(JWT_TOKEN);
				},
				setLocale: function(locale, reload) {
					currentLocale = locale;
					setValue(LOCALE, locale, true);
					if (reload) {
						window.location.reload();
					}
				},
				getLocale: function() {
					return currentLocale || getValue(LOCALE);
				},
				getUser: function() {
					if (currentUser) {
						return currentUser;
					}
					currentUser = parseUser();
					return currentUser;
				},
				clearAll: function() {
					currentJwt = undefined;
					currentUser = undefined;
					currentLocale = undefined;
					clearAllStorage();
				}
			};
		}
	]);