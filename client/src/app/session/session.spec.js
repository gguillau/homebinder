describe("Session", function() {
    'use strict';

    var Session, $window;

    beforeEach(function() {
        module("homebinder");

        inject(function(_$injector_) {
            Session = _$injector_.get("Session");
            $window = _$injector_.get("$window");
        });
    });

    it("sets the jwt", function() {
        var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MywiZW1haWwiOiJkZXZAaG9tZWJpbmRlci5jb20iLCJ1c2VyX3Rva2VuIjoic001TGtqc1J2Z21aXzVOZlNqLU4iLCJyb2xlIjoiYWRtaW4iLCJjcmVhdGVkX2F0IjoiMjAxOC0wMy0wNiAxNzoyNjo0NSAtMDUwMCIsImludGVyY29tX2tleSI6IjJiZjJiMTA2YTExMjhmOWQ3YjExNDUzNTYxNTJlZTY1ZDZiZTA0YzgxYmU0ZGNiOWRmMTljMzVlZTgzMmYxMDgifQ.x46fq31HiOY3zkmEhsDYjJaYRs5CscIlhM3YX6bIJus";
        Session.setJwt(token, true);
        var user = Session.getUser();
        
        expect(user.email).toEqual("dev@homebinder.com");
        $window.sessionStorage.clear();
		$window.localStorage.clear();
    });
});