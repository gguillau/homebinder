describe("hb.resources", function() {
    'use strict';

    var resources;

    beforeEach(function() {
        module("hb.resources");

        inject(function($injector) {
            resources = $injector.get("hb.resources.english");
        });
    });

    it("returns correct resources for verifiedRecalls", function() {
        expect(resources.verifiedRecalls.title).toEqual("Verified");
        expect(resources.verifiedRecalls.dateColumn).toEqual("Date");
        expect(resources.verifiedRecalls.binderColumn).toEqual("Binder ID");
        expect(resources.verifiedRecalls.applianceId).toEqual("Appliance ID");
        expect(resources.verifiedRecalls.ownerColumn).toEqual("Owner");
        expect(resources.verifiedRecalls.nameColumn).toEqual("Appliance Name");
        expect(resources.verifiedRecalls.makeColumn).toEqual("Appliance Make");
        expect(resources.verifiedRecalls.modelColumn).toEqual("Appliance Model");
        expect(resources.verifiedRecalls.serialColumn).toEqual("Appliance Serial No.");
        expect(resources.verifiedRecalls.statusColumn).toEqual("Status");
        expect(resources.verifiedRecalls.category).toEqual("Category");
        expect(resources.verifiedRecalls.subCategory).toEqual("Sub-Category");
        expect(resources.verifiedRecalls.resendEmail).toEqual("Re-send Email");
        expect(resources.verifiedRecalls.ignore).toEqual("Ignore");
        expect(resources.verifiedRecalls.repaired).toEqual("Repaired");
        expect(resources.verifiedRecalls.replaced).toEqual("Replaced");
        expect(resources.verifiedRecalls.linkColumn).toEqual("CPSC Link");
        expect(resources.verifiedRecalls.options).toEqual("Options");
        expect(resources.verifiedRecalls.does_not_apply).toEqual("Does Not Apply");
        expect(resources.verifiedRecalls.false_positive).toEqual("False Positive");
        expect(resources.verifiedRecalls.potential_recall).toEqual("Potential Recall");
        expect(resources.verifiedRecalls.confirmed_recall).toEqual("Confirmed Recall");
        expect(resources.verifiedRecalls.emptyTableText).toEqual("No Recalls Found");
        expect(resources.verifiedRecalls.searchText).toEqual("Search by appliance recall ID, appliance name...");
    });

});