angular
    .module("hb.resources", [
        "hb.resources.english",
        "hb.resources.french",
        "hb.resources.spanish"
    ])
    .factory("hb.resources", [
        "hb.resources.english",
        "hb.resources.french",
        "hb.resources.spanish",
        "Session",
        function(english, french, spanish, session) {
            var locale = session.getLocale();

            if (!locale || locale.length < 2) {
                return english;
            }
            switch (locale.substring(0, 2)) {
                case "sp":
                    return angular.extend({}, english, spanish);
                case "fr":
                    return angular.extend({}, english, french);
                default:
                    return english;
            }
        }
    ]);