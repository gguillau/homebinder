describe("hb.resources", function() {
    'use strict';

    var resources, session;

    beforeEach(function() {
        module("hb.resources");
        module("hb.session");
    });

    it("returns correct resources for verifiedRecalls for spanish", inject(function($injector) {
        session = $injector.get("Session");
        session.setLocale("sp", false);
        resources = $injector.get("hb.resources");
        expect(resources.verifiedRecalls.title).toEqual("Verificado");
    }));

    it("returns correct resources for verifiedRecalls for french", inject(function($injector) {
        session = $injector.get("Session");
        session.setLocale("fr", false);
        resources = $injector.get("hb.resources");
        expect(resources.verifiedRecalls.title).toEqual("Vérifié");
    }));

    it("returns correct resources for verifiedRecalls for english", inject(function($injector) {
        session = $injector.get("Session");
        session.setLocale("en-us", false);
        resources = $injector.get("hb.resources");
        expect(resources.verifiedRecalls.title).toEqual("Verified");
    }));

});