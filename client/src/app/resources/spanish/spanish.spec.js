describe("hb.resources.spanish", function() {
    'use strict';

    var resources;

    beforeEach(function() {
        module("hb.resources");

        inject(function($injector) {
            resources = $injector.get("hb.resources.spanish");
        });
    });

    it("returns correct resources for verifiedRecalls", function() {
        expect(resources.verifiedRecalls.title).toEqual("Verificado");
    });

});