describe("hb.resources.french", function() {
    'use strict';

    var resources;

    beforeEach(function() {
        module("hb.resources");

        inject(function($injector) {
            resources = $injector.get("hb.resources.french");
        });
    });

    it("returns correct resources for verifiedRecalls", function() {
        expect(resources.verifiedRecalls.title).toEqual("Vérifié");
    });

});