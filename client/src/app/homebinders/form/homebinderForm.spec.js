describe("BinderFormCtrl", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $state,
        session,
        $stateParams,
        address,
        notify,
        api,
        loading,
        $log,
        uploader,
        $window,
        $compile,
        $templateCache,
        propertyForm,
        template;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$templateCache_, _$compile_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        address = _$injector_.get("Address");
        $compile = _$compile_;
        $templateCache = _$templateCache_;
        $stateParams = {};

        $window = {
            URL: {
                createObjectURL: function() {},
                revokeObjectURL: function() {}
            }
        };

        $log = {
            error: function(msg) {}
        };

        loading = {
            show: function(msg) {},
            setMessage: function(msg) {},
            isLoading: function() {},
            close: function() {}
        };

        $state = {
            go: function(state, args) {}
        };

        session = {
            currentBinder: function() {},
            getJwt: function() {},
            getUser: function() {}
        };

        notify = {
            success: function(msg) {},
            error: function(msg) {}
        };

        uploader = {
            api: {
                uploadFiles: function() {},
                updateParams: function(params) {},
                pendingUploads: function() {},
                remove: function() {},
                setJwt: function(jwt) {}
            }
        };

    }));

    function createController() {

        $scope = $rootScope.$new();
        ctrl = $controller("BinderFormCtrl", {
            "$state": $state,
            "hb.api": api,
            "Session": session,
            "Notify": notify,
            "$stateParams": $stateParams,
            "Address": address,
            "Loading": loading,
            "$log": $log,
            "$window": $window
        });

        ctrl.uploader = uploader;
        $scope.ctrl = ctrl;
        template = $templateCache.get("homebinders/form/homebinderForm.tpl.html");
        $compile(template)($scope);
        propertyForm = $scope.ctrl.propertyForm;
        $scope.$apply();
        $scope.$digest();

    }

    describe('propertyForm', function() {

        it('should expect propertyForm to be defined and form validation to be false', function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            createController();

            ctrl.data.form.name = "";
            ctrl.data.form.property_attributes.address1 = "";
            ctrl.data.form.property_attributes.address2 = "";
            ctrl.data.form.property_attributes.city = "";
            ctrl.data.form.property_attributes.state = "";
            $rootScope.$apply();

            expect(propertyForm).toBeDefined();
            expect(propertyForm.$valid).toBe(false);

        });

        it('should expect propertyForm to be defined and form validation to be false', function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            createController();

            ctrl.data.form.name = "Binder Name";
            ctrl.data.form.property_attributes.address1 = "";
            ctrl.data.form.property_attributes.address2 = "";
            ctrl.data.form.property_attributes.city = "";
            ctrl.data.form.property_attributes.state = "";
            $rootScope.$apply();

            expect(propertyForm).toBeDefined();
            expect(propertyForm.$valid).toBe(false);

        });

        it('should expect propertyForm to be defined and form validation to be false', function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            createController();

            ctrl.data.form.name = "Binder Name";
            ctrl.data.form.property_attributes.address1 = "32 Main Street";
            ctrl.data.form.property_attributes.address2 = "";
            ctrl.data.form.property_attributes.city = "";
            ctrl.data.form.property_attributes.state = "";
            $rootScope.$apply();

            expect(propertyForm).toBeDefined();
            expect(propertyForm.$valid).toBe(false);

        });

        it('should expect propertyForm to be defined and form validation to be false', function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            createController();

            ctrl.data.form.name = "Binder Name";
            ctrl.data.form.property_attributes.address1 = "32 Main Street";
            ctrl.data.form.property_attributes.address2 = "";
            ctrl.data.form.property_attributes.city = "New Orleans";
            ctrl.data.form.property_attributes.state = "";
            $rootScope.$apply();

            expect(propertyForm).toBeDefined();
            expect(propertyForm.$valid).toBe(false);

        });

        it('should expect propertyForm to be defined and form validation to be false', function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            createController();

            ctrl.data.form.name = "6m9jq21riqsb40mtq1u3ow87ehhf0j0g7bfqktnea5v86nwrvnk";
            ctrl.data.form.property_attributes.address1 = "32 Main Street";
            ctrl.data.form.property_attributes.address2 = "";
            ctrl.data.form.property_attributes.city = "New Orleans";
            ctrl.data.form.property_attributes.state = "LA";
            $rootScope.$apply();

            expect(propertyForm).toBeDefined();
            expect(propertyForm.$valid).toBe(false);

        });

        it('should expect propertyForm to be defined and form validation to be false', function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            createController();

            ctrl.data.form.name = "Binder Name";
            ctrl.data.form.property_attributes.address1 = "6m9jq21riqsb40mtq1u3ow87ehhf0j0g7bfqktnea5v86nwrvnk";
            ctrl.data.form.property_attributes.address2 = "";
            ctrl.data.form.property_attributes.city = "New Orleans";
            ctrl.data.form.property_attributes.state = "LA";
            $rootScope.$apply();

            expect(propertyForm).toBeDefined();
            expect(propertyForm.$valid).toBe(false);

        });

        it('should expect propertyForm to be defined and form validation to be false', function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            createController();

            ctrl.data.form.name = "Binder Name";
            ctrl.data.form.property_attributes.address1 = "40 Lane street";
            ctrl.data.form.property_attributes.address2 = "6m9jq21riqsb40mtq1u3ow87ehhf0j0g7bfqktnea5v86nwrvnk";
            ctrl.data.form.property_attributes.city = "New Orleans";
            ctrl.data.form.property_attributes.state = "LA";
            $rootScope.$apply();

            expect(propertyForm).toBeDefined();
            expect(propertyForm.$valid).toBe(false);

        });

        it('should expect propertyForm to be defined and form validation to be false', function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            createController();

            ctrl.data.form.name = "Binder Name";
            ctrl.data.form.property_attributes.address1 = "40 Lane street";
            ctrl.data.form.property_attributes.address2 = "Apt. 2";
            ctrl.data.form.property_attributes.city = "6m9jq21riqsb40mtq1u3ow87ehhf0j0g7bfqktnea5v86nwrvnk";
            ctrl.data.form.property_attributes.state = "LA";
            $rootScope.$apply();

            expect(propertyForm).toBeDefined();
            expect(propertyForm.$valid).toBe(false);

        });

        it('should expect propertyForm to be defined and form validation to be true', function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            createController();

            ctrl.data.form.name = "Binder Name";
            ctrl.data.form.property_attributes.address1 = "32 Main Street";
            ctrl.data.form.property_attributes.address2 = "";
            ctrl.data.form.property_attributes.city = "New Orleans";
            ctrl.data.form.property_attributes.state = "LA";
            ctrl.data.form.property_attributes.country = "US";
            $rootScope.$apply();

            expect(propertyForm).toBeDefined();
            expect(propertyForm.$invalid).toBe(false);

        });

    });

    describe('ctrl.refresh', function() {

        it("should initiate a new binder", function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            spyOn(api.binder, "binders").and.callThrough();

            createController();
            $rootScope.$apply();

            expect(ctrl.id).toBe(undefined);
            expect(api.user.current).toHaveBeenCalled();
            expect(api.binder.binders).toHaveBeenCalled();
            expect(ctrl.title).toEqual("New Binder");
            expect(ctrl.moreInfo).toEqual("Fill out the form below to get started with your new Binder!");
            expect(ctrl.uploadShow).toBe(true);
        });

        it("should send the user to the edit binder view", function() {

            $stateParams.binderId = 100;
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 100,
                name: "This is a fake home",
                primary: true,
                details: "this is a home",
                last_recall: null,
                partner: {
                    address: null,
                    binder_count: 0,
                    binder_logo_id: null,
                    code: "TESTGROUP",
                    contact: "Test Partner 1",
                    coupons: [],
                    created_at: "2015-06-11T02:45:39Z",
                    email: "testpartner@thetestgroup.com",
                    id: 154,
                    name: "The Tet Group",
                    partner_type: "broker",
                    phone: "+19786219784",
                    sellers_logo_id: null,
                    tags: [],
                    vendors: []
                },
                vendor: null,
                recalls: 0,
                documents: null,
                maintenance_items: null,
                binder_contractors: null,
                property: {
                    state: "MA",
                    country: "US"
                },
                subscription: null,
                seller_report: null,
                transfer: null,
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                },
                tags: [{
                    id: 698,
                    metadata: null,
                    tag: "partner_120",
                    tag_scope: null
                }]
            };
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            spyOn(api.binder, "binders").and.callThrough();
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: binder
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();

            expect(api.binder.get).toHaveBeenCalledWith(100);
            expect(loading.show).toHaveBeenCalledWith("Loading...");
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.id).toEqual(100);
            expect(ctrl.data.form.id).toEqual(100);
            expect(ctrl.data.form.name).toEqual("This is a fake home");
            expect(ctrl.data.form.primary).toBe(true);
            expect(ctrl.data.form.property_attributes).toEqual({
                state: 'MA',
                country: 'US'
            });
            expect(ctrl.data.form.details).toEqual("this is a home");
            expect(ctrl.binder).toEqual(binder);
        });

    });

    describe('ctrl.onBinderError', function() {
        it("calls notify error", function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));

            createController();
            spyOn(notify, "error");
            ctrl.onBinderError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe('ctrl.initNewBinder', function() {

        it("should initiate a new binder", function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            spyOn(api.binder, "binders").and.callThrough();

            createController();
            spyOn(ctrl, "init").and.callThrough();
            ctrl.initNewBinder();
            $rootScope.$apply();

            expect(ctrl.init).toHaveBeenCalled();
            expect(ctrl.data.form.property_attributes.property_type).toEqual("Single Family");

        });
    });
    
    describe('ctrl.getUserError', function() {
        it("calls notify", function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            spyOn(notify, "error");
            
            createController();
            ctrl.getUserError({data: "error"});

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe('ctrl.submitForm', function() {

        it("should submit the form and call the create binder function", function() {

            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 222,
                name: "This is a fake home",
                primary: true,
                details: "this is a home",
                last_recall: null,
                partner: {
                    address: null,
                    binder_count: 0,
                    binder_logo_id: null,
                    code: "TESTGROUP",
                    contact: "Test Partner 1",
                    coupons: [],
                    created_at: "2015-06-11T02:45:39Z",
                    email: "testpartner@thetestgroup.com",
                    id: 154,
                    name: "The Tet Group",
                    partner_type: "broker",
                    phone: "+19786219784",
                    sellers_logo_id: null,
                    tags: [],
                    vendors: []
                },
                vendor: null,
                recalls: 0,
                documents: null,
                maintenance_items: null,
                binder_contractors: null,
                property: {
                    state: "MA",
                    country: "US"
                },
                subscription: null,
                seller_report: null,
                transfer: null,
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            spyOn(api.binder, "binders").and.callThrough();
            spyOn(api.binder, "create").and.returnValue($q.when({
                data: binder
            }));
            spyOn(loading, "show");

            var data_binder = {
                id: binder.id,
                name: binder.name,
                primary: binder.primary,
                property_attributes: binder.property,
                details: binder.details,
                user_binders_attributes: [{
                    user_id: undefined,
                    role: "owner"
                }],
                create_method: "manual"
            };

            createController();
            $rootScope.$apply();
            spyOn(ctrl, "createMaintenance");
            ctrl.data.form = data_binder;
            ctrl.data.form.errors = "errors";
            ctrl.submitForm();
            $rootScope.$apply();

            expect(api.binder.create).toHaveBeenCalled();
            expect(loading.show).toHaveBeenCalledWith("Creating binder...");
            expect(ctrl.binder).toEqual(binder);
            expect(ctrl.createMaintenance).toHaveBeenCalled();

        });

        it("should submit the form and call the create binder function and return an error", function() {

            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 222,
                name: "This is a fake home",
                primary: true,
                details: "this is a home",
                last_recall: null,
                partner: {
                    address: null,
                    binder_count: 0,
                    binder_logo_id: null,
                    code: "TESTGROUP",
                    contact: "Test Partner 1",
                    coupons: [],
                    created_at: "2015-06-11T02:45:39Z",
                    email: "testpartner@thetestgroup.com",
                    id: 154,
                    name: "The Tet Group",
                    partner_type: "broker",
                    phone: "+19786219784",
                    sellers_logo_id: null,
                    tags: [],
                    vendors: []
                },
                vendor: null,
                recalls: 0,
                documents: null,
                maintenance_items: null,
                binder_contractors: null,
                property: {
                    state: "MA",
                    country: "US"
                },
                subscription: null,
                seller_report: null,
                transfer: null,
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            spyOn(api.binder, "binders").and.callThrough();
            spyOn(api.binder, "create").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(loading, "show");
            spyOn($log, "error");
            spyOn(notify, "error");

            var data_binder = {
                id: binder.id,
                name: binder.name,
                primary: binder.primary,
                property_attributes: binder.property,
                details: binder.details,
                user_binders_attributes: [{
                    user_id: undefined,
                    role: "owner"
                }],
                create_method: "manual"
            };

            createController();
            $rootScope.$apply();
            spyOn(ctrl, "createMaintenance");
            ctrl.data.form = angular.copy(data_binder);
            ctrl.submitForm();
            $rootScope.$apply();

            expect(api.binder.create).toHaveBeenCalled();
            expect(loading.show).toHaveBeenCalledWith("Creating binder...");
            expect(ctrl.data.form.errors).toBe("error");
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");

        });

        it("should submit the form and call the update binder function", function() {

            $stateParams.binderId = 100;
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 100,
                name: "This is a fake home",
                primary: true,
                details: "this is a home",
                last_recall: null,
                partner: {
                    address: null,
                    binder_count: 0,
                    binder_logo_id: null,
                    code: "TESTGROUP",
                    contact: "Test Partner 1",
                    coupons: [],
                    created_at: "2015-06-11T02:45:39Z",
                    email: "testpartner@thetestgroup.com",
                    id: 154,
                    name: "The Tet Group",
                    partner_type: "broker",
                    phone: "+19786219784",
                    sellers_logo_id: null,
                    tags: [],
                    vendors: []
                },
                vendor: null,
                recalls: 0,
                documents: null,
                maintenance_items: null,
                binder_contractors: null,
                property: {
                    state: "MA",
                    country: "US"
                },
                subscription: null,
                seller_report: null,
                transfer: null,
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                },
                tags: [{
                    id: 698,
                    metadata: null,
                    tag: "partner_120",
                    tag_scope: null
                }]
            };
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            spyOn(api.binder, "binders").and.callThrough();
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: binder
            }));
            spyOn(api.binder, "update").and.returnValue($q.when({
                data: binder
            }));
            spyOn(loading, "show");

            var data_binder = {
                id: binder.id,
                name: binder.name,
                primary: binder.primary,
                property_attributes: binder.property,
                user_binders_attributes: [{
                    user_id: undefined,
                    role: "owner"
                }],
                details: binder.details
            };

            createController();
            $rootScope.$apply();
            spyOn(ctrl, "goToOverview");
            ctrl.data.form = data_binder;
            ctrl.submitForm();
            $rootScope.$apply();

            expect(api.binder.update).toHaveBeenCalledWith(100, data_binder);
            expect(loading.show).toHaveBeenCalledWith("Updating binder...");
            expect(ctrl.binder).toEqual(binder);
            expect(ctrl.goToOverview).toHaveBeenCalled();

        });

    });

    describe('ctrl.createMaintenance', function() {
        it("should call createMaintenance function successfully", function() {

            spyOn(api.maintenanceItem, "create").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));

            var items = [{
                name: "Smoke Detector Battery",
                interval: "Annual",
                do_date: "",
                details: ""
            }, {
                name: "Sweep Chimney1",
                interval: "Semi-Annual",
                do_date: "",
                details: ""
            }, {
                name: "Sweep Chimney2",
                interval: "Quarterly",
                do_date: "",
                details: ""
            }, {
                name: "Sweep Chimney3",
                interval: "Monthly",
                do_date: "",
                details: ""
            }, {
                name: "Sweep Chimney4",
                interval: "Every-Other-Year",
                do_date: "",
                details: ""
            }, {
                name: "Sweep Chimney5",
                interval: "Every-3-Years",
                do_date: "",
                details: ""
            }, {
                name: "Sweep Chimney6",
                interval: "Every-4-Years",
                do_date: "",
                details: ""
            }, {
                name: "Sweep Chimney7",
                interval: "Every-5-Years",
                do_date: "",
                details: ""
            }, {
                name: "Sweep Chimney8",
                interval: "Every-10-Years",
                do_date: "",
                details: ""
            }, {
                name: "Sweep Chimney9",
                interval: "Every-40-Years",
                do_date: "",
                details: ""
            }];

            createController();
            ctrl.binder = { id: 121 };
            ctrl.maintenanceItems = items;
            spyOn(ctrl, "uploadImage");
            ctrl.createMaintenance();
            $rootScope.$apply();

            expect(api.maintenanceItem.create.calls.count()).toBe(10);
        });

        it("should call createMaintenance function successfully and also return an error", function() {

            spyOn(api.maintenanceItem, "create").and.callFake(function(opts) {
                if (opts.name === "Smoke Detector Battery") {
                    return $q.when({
                        data: "success"
                    });
                }
                else if (opts.name === "Sweep Chimney") {
                    return $q.reject({
                        data: "error"
                    });
                }
            });
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            spyOn(loading, "setMessage");
            spyOn($log, "error");
            spyOn(notify, "error");

            var items = [{
                name: "Smoke Detector Battery",
                interval: 1,
                do_date: "",
                details: ""
            }, {
                name: "Sweep Chimney",
                interval: 3,
                do_date: "",
                details: ""
            }];

            createController();
            $rootScope.$apply();
            ctrl.binderId = 121;
            ctrl.maintenanceItems = items;
            spyOn(ctrl.maintenanceItems, "splice").and.callThrough();
            spyOn(ctrl, "uploadImage");
            spyOn(ctrl, "createMaintenance").and.callThrough();
            ctrl.createMaintenance();
            $rootScope.$apply();

            expect(api.maintenanceItem.create.calls.count()).toBe(2);
            expect(ctrl.maintenanceItems.splice).toHaveBeenCalledWith(0, 1);
            expect(loading.setMessage).toHaveBeenCalledWith("Adding maintenance items to binder...");
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(ctrl.uploadImage).toHaveBeenCalled();
            expect(ctrl.createMaintenance).toHaveBeenCalled();

        });
    });

    describe('ctrl.uploadImage', function() {

        it("should call uploadFiles successfully", function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));

            var images = [{}];
            spyOn(loading, "setMessage");

            createController();
            $rootScope.$apply();
            spyOn(ctrl.uploader.api, "pendingUploads").and.returnValue(images);
            spyOn(ctrl.uploader.api, "updateParams");
            ctrl.binder.id = 121;
            ctrl.uploadImage();
            $rootScope.$apply();

            expect(ctrl.uploader.api.pendingUploads).toHaveBeenCalled();
            expect(loading.setMessage).toHaveBeenCalledWith("Uploading photo...");
            expect(ctrl.uploader.api.updateParams).toHaveBeenCalledWith({
                binder_id: 121,
                hero_image_id: true
            });

        });

        it("should call goToOverview", function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));

            var images = [];

            createController();
            $rootScope.$apply();
            spyOn(ctrl.uploader.api, "pendingUploads").and.returnValue(images);
            spyOn(ctrl, "goToOverview");
            ctrl.uploadImage();
            $rootScope.$apply();

            expect(ctrl.uploader.api.pendingUploads).toHaveBeenCalled();
            expect(ctrl.goToOverview).toHaveBeenCalled();

        });

    });

    describe('ctrl.fileUploaded', function() {
        it("should call goToOverview", function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            spyOn(notify, "success");

            createController();
            $rootScope.$apply();
            spyOn(ctrl, "goToOverview");
            ctrl.fileUploaded({
                name: "uploaded_image.png"
            });
            $rootScope.$apply();

            expect(notify.success).toHaveBeenCalledWith("Image uploaded!");
            expect(ctrl.goToOverview).toHaveBeenCalled();

        });
    });

    describe('ctrl.fileUploadError', function() {
        it("should call goToOverview", function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));

            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            $rootScope.$apply();
            spyOn(ctrl, "goToOverview");
            ctrl.fileUploadError({
                data: "error"
            });
            $rootScope.$apply();

            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(ctrl.goToOverview).toHaveBeenCalled();

        });
    });

    describe('ctrl.goToOverview', function() {
        it("should send user to binder overview", function() {

            $stateParams.binderId = 100;
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 100,
                name: "This is a fake home",
                primary: true,
                details: "this is a home",
                last_recall: null,
                partner: {
                    address: null,
                    binder_count: 0,
                    binder_logo_id: null,
                    code: "TESTGROUP",
                    contact: "Test Partner 1",
                    coupons: [],
                    created_at: "2015-06-11T02:45:39Z",
                    email: "testpartner@thetestgroup.com",
                    id: 154,
                    name: "The Tet Group",
                    partner_type: "broker",
                    phone: "+19786219784",
                    sellers_logo_id: null,
                    tags: [],
                    vendors: []
                },
                vendor: null,
                recalls: 0,
                documents: null,
                maintenance_items: null,
                binder_contractors: null,
                property: {
                    state: "MA",
                    country: "US"
                },
                subscription: null,
                seller_report: null,
                transfer: null,
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            spyOn(api.binder, "binders").and.callThrough();
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: binder
            }));
            spyOn($state, "go");
            spyOn(loading, "close");
            spyOn(session, "currentBinder");
            spyOn(notify, "success");

            createController();
            $rootScope.$apply();
            ctrl.goToOverview();
            $rootScope.$apply();

            expect($state.go).toHaveBeenCalledWith('binder.overview', {
                binderId: 100
            });
            expect(loading.close).toHaveBeenCalled();
            expect(notify.success).toHaveBeenCalledWith("Binder null.");
        });
    });

    describe('ctrl.cancelForm', function() {
        it("should cancel the form and send user to binder overview", function() {

            $stateParams.binderId = 100;
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 100,
                name: "This is a fake home",
                primary: true,
                details: "this is a home",
                last_recall: null,
                partner: {
                    address: null,
                    binder_count: 0,
                    binder_logo_id: null,
                    code: "TESTGROUP",
                    contact: "Test Partner 1",
                    coupons: [],
                    created_at: "2015-06-11T02:45:39Z",
                    email: "testpartner@thetestgroup.com",
                    id: 154,
                    name: "The Tet Group",
                    partner_type: "broker",
                    phone: "+19786219784",
                    sellers_logo_id: null,
                    tags: [],
                    vendors: []
                },
                vendor: null,
                recalls: 0,
                documents: null,
                maintenance_items: null,
                binder_contractors: null,
                property: {
                    state: "MA",
                    country: "US"
                },
                subscription: null,
                seller_report: null,
                transfer: null,
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            spyOn(api.binder, "binders").and.callThrough();
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: binder
            }));
            spyOn($state, "go");

            createController();
            ctrl.cancelForm();
            $rootScope.$apply();

            expect($state.go).toHaveBeenCalledWith('binder.overview', {
                binderId: 100
            });
        });

        it("should cancel the form and send the user to binders state", function() {

            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            spyOn(api.binder, "binders").and.callThrough();
            spyOn($state, "go");

            createController();
            ctrl.cancelForm();
            $rootScope.$apply();

            expect($state.go).toHaveBeenCalledWith('binders');

        });
    });

    describe('ctrl.nextStep', function() {
        it("should increment current step and set the current step", function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));

            createController();
            $rootScope.$apply();
            ctrl.nextStep();
            $rootScope.$apply();

            expect(ctrl.current_step).toBe(1);
            expect(ctrl.step).toBe("stepTwo");
        });
    });

    describe('ctrl.previousStep', function() {
        it("should decrement current step and set the current step", function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));

            createController();
            $rootScope.$apply();
            ctrl.nextStep();
            $rootScope.$apply();
            ctrl.previousStep();
            $rootScope.$apply();

            expect(ctrl.current_step).toBe(0);
            expect(ctrl.step).toBe("stepOne");
        });
    });

    describe('ctrl.fileAdded', function() {
        it('should call $window object createObjectURL function', function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            spyOn($window.URL, "createObjectURL").and.returnValue("url");

            var images = ["native_file"];

            createController();
            spyOn(ctrl.uploader.api, "pendingUploads").and.returnValue(images);
            $rootScope.$apply();
            ctrl.fileAdded();
            $rootScope.$apply();

            expect($window.URL.createObjectURL).toHaveBeenCalledWith("native_file");
            expect(ctrl.uploader.api.pendingUploads).toHaveBeenCalled();
            expect(ctrl.image.src).toBe("url");
            expect(ctrl.url).toBe("url");

        });
    });

    describe('ctrl.remove_image', function() {
        it('should call $window object revokeObjectURL function', function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            spyOn($window.URL, "revokeObjectURL");
            spyOn($window.URL, "createObjectURL").and.returnValue("url");

            var images = [{
            }];

            createController();
            spyOn(ctrl.uploader.api, "pendingUploads").and.returnValue(images);
            spyOn(ctrl.uploader.api, "remove");
            $rootScope.$apply();
            ctrl.fileAdded();
            $rootScope.$apply();
            ctrl.remove_image();
            $rootScope.$apply();

            expect($window.URL.revokeObjectURL).toHaveBeenCalledWith("url");
            expect(ctrl.uploader.api.remove).toHaveBeenCalledWith(ctrl.images[0]);

        });
    });

    describe('ctrl.onSelect', function() {
        it("sets the country", function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));

            createController();
            var item = { country: { value: "US" } };
            ctrl.data = {
                form: {
                    property_attributes: {
                        country: null
                    }
                }
            };
            ctrl.onSelect(item);

            expect(ctrl.data.form.property_attributes.country).toEqual("US");
        });
    });

    describe('ctrl.showPrimary', function() {
        it('should return true', function() {
            var homeowner = {
                email: "homeowner@gmail.com",
                user_role: {
                    role: "homeowner"
                }
            };

            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            spyOn(api.user, "current").and.returnValue(homeowner);

            createController();
            $rootScope.$apply();
            var value = ctrl.showPrimary();

            expect(value).toBe(true);

        });

        it('should return true', function() {
            var admin = {
                email: "somerandomuseremail@gmail.com",
                user_role: {
                    role: "admin"
                }
            };
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));
            spyOn(api.user, "current").and.returnValue(admin);

            createController();
            $rootScope.$apply();
            var value = ctrl.showPrimary();

            expect(value).toBe(true);

        });

        it('should return false', function() {
            var partner = {
                email: "somerandomuseremail@gmail.com",
                role: "inspector"
            };
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, role: "inspector", user_profile_attributes: { address_attributes: {} } } }));
            spyOn(api.user, "current").and.returnValue(partner);

            createController();
            $rootScope.$apply();
            var value = ctrl.showPrimary();

            expect(value).toBe(false);

        });

        it('should return false', function() {
            var partner = {
                email: "somerandomuseremail@gmail.com",
                role: "partner"
            };
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, role: "partner", user_profile_attributes: { address_attributes: {} } } }));
            spyOn(api.user, "current").and.returnValue(partner);

            createController();
            $rootScope.$apply();
            var value = ctrl.showPrimary();

            expect(value).toBe(false);

        });

        it('should return false', function() {
            var partner = {
                email: "somerandomuseremail@gmail.com",
                role: "broker"
            };
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, role: "broker", user_profile_attributes: { address_attributes: {} } } }));
            spyOn(api.user, "current").and.returnValue(partner);

            createController();
            $rootScope.$apply();
            var value = ctrl.showPrimary();

            expect(value).toBe(false);

        });
    });
    
    describe('ctrl.onSelectCountry', function() {
        it("sets the country", function() {
            spyOn(api.user, "current").and.returnValue({ id: 1 });
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1, user_profile_attributes: { address_attributes: {} } } }));

            createController();
            var item = { country: { value: "US" } };
            ctrl.data = {
                form: {
                    property_attributes: {
                        country: null
                    }
                }
            };
            ctrl.onSelectCountry(item.country);

            expect(ctrl.country).toEqual(item.country);
        });
    });

});