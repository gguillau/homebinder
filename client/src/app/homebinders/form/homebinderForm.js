(function() {
    "use strict";

    angular
        .module("hb.homebinders.form", [
            "ui.router",
            "ui.bootstrap",
            "hb.components"
        ])
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state("binders.new", {
                    url: "^/binder/new",
                    views: {
                        '@': {
                            templateUrl: "homebinders/form/homebinderForm.tpl.html",
                            controller: "BinderFormCtrl",
                            controllerAs: "ctrl"
                        }
                    },
                    access: {
                        requiresLogin: true
                    }
                })
                .state("binder.edit", {
                    url: "^/binders/:binderId/edit",
                    templateUrl: "homebinders/form/homebinderFormEdit.tpl.html",
                    controller: "BinderFormCtrl",
                    controllerAs: "ctrl",
                    resolve: {
                        binder: function(binder) {
                            return binder;
                        }
                    },
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .controller("BinderFormCtrl", [
            "$state",
            "$stateParams",
            "Address",
            "hb.api",
            "Session",
            "Notify",
            "Loading",
            "$log",
            "$window",
            "Context",
            "hb.resources",
            "$timeout",
            "hb.userRole",
            "HomeBinderFactory",
            BinderFormCtrl
        ]);

    function BinderFormCtrl($state, $stateParams, Address, api, Session, Notify, loading, $log, $window, Context, resources, $timeout, userRole, homebinderFactory) {
        this.$state = $state;
        this.$stateParams = $stateParams;
        this.Address = Address;
        this.api = api;
        this.Session = Session;
        this.notify = Notify;
        this.loading = loading;
        this.$log = $log;
        this.user = this.Session.getUser();
        this.id = this.$stateParams["binderId"];
        this.steps = ["stepOne", "stepTwo"];
        this.uploadShow = false;
        this.roles = userRole;
        this.images = [];
        this.image = {};
        this.step = "stepThree";
        this.uploader = {
            fileTypes: "image",
            hideUploadButton: true,
            showTable: false,
            selectButtonLabel: "Upload property photo",
            fileLimit: true,
            url: "/api/v1/images",
            jwt: this.Session.getJwt(),
            multiSelect: false
        };
        this.current_step = 0;
        this.step = this.steps[this.current_step];
        this.$window = $window;
        this.maintenanceItems = [];
        this.uploadShow = false;
        this.binder = {};
        this.options = {};
        this.notify_text = null;
        this.context = Context;
        this.resources = resources.homebinderForm;
        this.toolbarCfg = {
            title: this.resources.title,
            subtitle: this.resources.subtitle
        };
        this.homebinderFactory = homebinderFactory;
        this.validationErrors = resources.validationErrors;
        this.$timeout = $timeout;
        this.datepicker = {
            format: "MMMM dd, yyyy",
            options: {
                "show-button-bar": false
            }
        };
        this.date = [{
            opened: false
        }];
        this.refresh();
    }

    BinderFormCtrl.prototype = {

        refresh: function() {
            if (this.id) {
                this.loading.show("Loading...");
                this.loadBinder();
            }
            else {
                this.uploadShow = true;
                this.initNewBinder();
            }
        },

        initNewBinder: function() {
            this.init();
            this.title = this.resources.newBinderTitle;
            this.moreInfo = this.resources.newBinderInfo;
            this.data = {};
            this.data.form = {};
            this.data.form.primary = this.api.binder.binders() === undefined || this.api.binder.binders().length === 0 ? true : false;
            this.data.form.property_attributes = {};
            this.data.form.property_attributes.property_type = "Single Family";
        },

        init: function() {
            this.user = this.api.user.current();
            this.getUser();
            this.options = {};
            this.options.countries = this.Address.countries();
            this.options.propertyTypes = ["Single Family", "Multi Family", "Condo"];
        },

        getUser: function() {
            this.api.user.get(this.user.id).then(
                angular.bind(this, this.getUserSuccess),
                angular.bind(this, this.getUserError));
        },

        getUserSuccess: function(response) {
            this.user = response.data;
            this.country = this.Address.findCountryByCode(this.user.user_profile_attributes.address_attributes.country);
            this.options.states = this.Address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                return state.country.value === this.country.value;
            }));
        },

        getUserError: function(response) {
            this.notify.error(response.data);
        },

        loadBinder: function() {
            this.init();
            this.homebinderFactory.setToolbar({
                buttons: []
            });
            /*
            this.toolbarCfg.button = {
                title: this.resources.save,
                click: angular.bind(this, this.submitForm)
            };
            */
            this.toolbarCfg.button = null;
            this.itemType = "edit_binder";
            this.api.binder.get(this.id).then(
                angular.bind(this, this.onBinderLoaded),
                angular.bind(this, this.onBinderError)
            );
        },

        onBinderLoaded: function(response) {
            var binder = {
                id: response.data.id,
                name: response.data.name,
                primary: response.data.primary,
                partner: response.data.partner ? response.data.partner : undefined,
                property_attributes: response.data.property,
                details: response.data.details
            };
            
            if(binder.property_attributes.purchase_date){
                binder.property_attributes.purchase_date = new Date(binder.property_attributes.purchase_date);    
            }
            
            this.data = {
                form: binder
            };
            this.binder = response.data;
            this.country = this.Address.findCountryByCode(this.binder.property.country);
            this.options.states = this.Address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                return state.country.value === this.country.value;
            }));
            this.loading.close();
        },

        onBinderError: function(response) {
            this.notify.error(response.data);
            this.loading.close();
        },

        submitForm: function() {

            if (this.data.form.errors) {
                delete this.data.form.errors;
            }

            var data = angular.copy(this.data.form);

            if (this.id) {
                this.loading.show("Updating binder...");
                //this.loading.close();
                this.api.binder
                    .update(this.id, data)
                    .then(
                        angular.bind(this, this.binderUpdated, "updated"),
                        angular.bind(this, this.binderSaveError, "updated")
                    );
            }
            else {
                this.loading.show("Creating binder...");
                // add the user
                this.user_binders = [{
                    user_id: this.user.id,
                    role: "owner"
                }];
                data.create_method = "manual";

                var request = {
                    binder: data,
                    user_binders: this.user_binders
                };
                this.api.binder
                    .create(request)
                    .then(
                        angular.bind(this, this.binderCreated, "created"),
                        angular.bind(this, this.binderSaveError, "created")
                    );
            }
        },

        binderCreated: function(notify_text, response) {
            this.notify_text = notify_text;
            this.binder = response.data;
            this.createMaintenance();
        },

        binderUpdated: function(notify_text, response) {
            this.notify_text = notify_text;
            this.binder = response.data;
            this.goToOverview();
        },

        binderSaveError: function(text, response) {
            this.data.form.errors = response.data;
            this.$log.error(response);
            this.notify.error(response.data);
            this.loading.close();
        },

        createMaintenance: function() {
            // If there are maintenance items to create start creating them.
            if (this.maintenanceItems && this.maintenanceItems.length > 0) {
                var item = this.maintenanceItems[0];
                this.maintenanceItems.splice(0, 1);
                var mi = {
                    binder_id: this.binder.id,
                    name: item.name,
                    do_date: item.do_date,
                    details: item.details
                };

                // convert interval dropdown for maintenance_cycle
                switch (item.interval) {
                    case "Annual":
                        mi.interval = 1;
                        mi.maintenance_cycle = "Years";
                        break;
                    case "Semi-Annual":
                        mi.interval = 6;
                        mi.maintenance_cycle = "Months";
                        break;
                    case "Quarterly":
                        mi.interval = 3;
                        mi.maintenance_cycle = "Months";
                        break;
                    case "Monthly":
                        mi.interval = 1;
                        mi.maintenance_cycle = "Months";
                        break;
                    case "Every-Other-Year":
                        mi.interval = 2;
                        mi.maintenance_cycle = "Years";
                        break;
                    case "Every-3-Years":
                        mi.interval = 3;
                        mi.maintenance_cycle = "Years";
                        break;
                    case "Every-4-Years":
                        mi.interval = 4;
                        mi.maintenance_cycle = "Years";
                        break;
                    case "Every-5-Years":
                        mi.interval = 5;
                        mi.maintenance_cycle = "Years";
                        break;
                    case "Every-10-Years":
                        mi.interval = 10;
                        mi.maintenance_cycle = "Years";
                        break;
                    case "Every-40-Years":
                        mi.interval = 40;
                        mi.maintenance_cycle = "Years";
                        break;
                }

                this.loading.setMessage("Adding maintenance items to binder...");
                this.api.maintenanceItem.create(mi).then(
                    angular.bind(this, this.onCreateMaintenanceItemSuccess),
                    angular.bind(this, this.onCreateMaintenanceItemError)
                );
            }
            else {
                this.uploadImage();
            }
        },

        onCreateMaintenanceItemSuccess: function(response) {
            this.createMaintenance();
        },

        onCreateMaintenanceItemError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.createMaintenance();
        },

        uploadImage: function() {
            var uploadApi = this.uploader.api;
            if (uploadApi && uploadApi.pendingUploads().length > 0) {
                uploadApi.updateParams({
                    binder_id: this.binder.id,
                    hero_image_id: true
                });
                this.loading.setMessage("Uploading photo...");
                // TODO: Hack
                setTimeout(function() {
                    uploadApi.uploadFiles();
                }, 0);
            }
            else {
                this.goToOverview();
            }
        },

        fileUploaded: function(file) {
            this.notify.success("Image uploaded!");
            this.goToOverview();
        },

        fileUploadError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.goToOverview();

        },

        goToOverview: function() {
            this.context.setBinder(this.binder);
            this.loading.close();
            this.notify.success("Binder " + this.notify_text + ".");
            this.$state.go("binder.overview", {
                binderId: this.binder.id
            });
        },

        cancelForm: function() {
            if (this.id) {
                this.$state.go("binder.overview", {
                    binderId: this.id
                });
            }
            else {
                this.$state.go("binders");
            }
        },

        nextStep: function() {
            this.current_step++;
            this.step = this.steps[this.current_step];
        },

        previousStep: function() {
            this.current_step--;
            this.step = this.steps[this.current_step];
        },

        fileAdded: function() {
            this.images = this.uploader.api.pendingUploads();
            var native_file = this.images[0];
            this.image.src = this.url = this.$window.URL.createObjectURL(native_file);
        },

        remove_image: function() {
            this.uploader.api.remove(this.images[0]);
            this.$window.URL.revokeObjectURL(this.image.src);
        },

        onSelect: function(item) {
            if (item) {
                this.data.form.property_attributes.country = item.country.value;
            }
        },

        showPrimary: function() {
            var role = this.user.role;
            switch (role) {
                case this.roles.PARTNER:
                case this.roles.INSPECTOR:
                case this.roles.BROKER:
                    return false;
                default:
                    return true;
            }
        },

        onSelectCountry: function(item) {
            this.country = item;
            this.options.states = this.Address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                return state.country.value === this.country.value;
            }));
        },

        open: function(index) {
            this.date[index].opened = true;
        }

    };

})();