describe("HomeBindersController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $state,
        loading,
        modals,
        notify,
        $log,
        api,
        Session;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");

        $state = {
            go: function(state, args) {}
        };

        api = {
            binder: {
                all: function(opts) {},
                destroy: function(id) {}
            }
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        $log = {
            error: function(msg) {}
        };

        notify = {
            error: function(msg) {},
            success: function(msg) {}
        };

        Session = {
            currentBinder: function() {},
            getJwt: function() {},
            getUser: function() {
                return { id: 1 };
            },
            getBinder: function() {}
        };

        modals = {
            confirm: function(confirmOptions) {},
            preview: function(previewOptions) {},
            show: function(showOptions) {},
            upload: function(uploadOptions) {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("HomeBindersController", {
            "$state": $state,
            "hb.api": api,
            "$log": $log,
            "Session": Session,
            "Notify": notify,
            "ModalService": modals,
            "Loading": loading
        });
        $scope.ctrl = ctrl;

    }

    describe('ctrl.refresh', function() {

        it('should call api binder all function', function() {

            var binders = {
                total: 3,
                items: [{
                    id: 1
                }, {
                    id: 2
                }, {
                    id: 3
                }]
            };

            spyOn(api.binder, "all").and.returnValue($q.when({
                data: binders
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();

            expect(api.binder.all).toHaveBeenCalled();
            expect(ctrl.binders.length).toBe(3);
            expect(loading.show).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();

        });

        it('should call api binder all function and set primary binder', function() {

            var binders = {
                total: 3,
                items: [{
                    id: 1
                }, {
                    id: 2
                }, {
                    id: 3,
                    primary: true
                }]
            };

            spyOn(api.binder, "all").and.returnValue($q.when({
                data: binders
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();

            expect(api.binder.all).toHaveBeenCalled();
            expect(ctrl.binders.length).toBe(3);
            expect(ctrl.binders[0].id).toBe(3);
            expect(loading.show).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();

        });

        it('should call api binder all function and set primary binder', function() {

            spyOn(api.binder, "all").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            $rootScope.$apply();

            expect(api.binder.all).toHaveBeenCalled();
            expect(loading.show).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();

        });
    });

    describe('ctrl.clickBinder', function() {

        it('should call $state go function', function() {

            var binders = {
                total: 3,
                items: [{
                    id: 1
                }, {
                    id: 2
                }, {
                    id: 3
                }]
            };

            spyOn(api.binder, "all").and.returnValue($q.when({
                data: binders
            }));
            spyOn($state, "go");

            createController();
            $rootScope.$apply();
            ctrl.clickBinder(ctrl.binders[2]);
            $rootScope.$apply();

            expect($state.go).toHaveBeenCalledWith("binder.overview", {
                binderId: 3
            });

        });

    });

    describe('ctrl.deleteBinder', function() {

        it('should call modals confirm function', function() {

            var binders = {
                total: 3,
                items: [{
                    id: 1,
                    name: "Delete me!"
                }, {
                    id: 2
                }, {
                    id: 3
                }]
            };

            spyOn(api.binder, "all").and.returnValue($q.when({
                data: binders
            }));

            spyOn(modals, "confirm");

            createController();
            $rootScope.$apply();
            ctrl.deleteBinder(ctrl.binders[0]);
            $rootScope.$apply();

            expect(modals.confirm).toHaveBeenCalled();

        });

    });

    describe('ctrl.deleteBinderConfirmed', function() {

        it('should call api binder delete', function() {

            var binders = {
                total: 3,
                items: [{
                    id: 1,
                    name: "Delete me!"
                }, {
                    id: 2
                }, {
                    id: 3
                }]
            };

            spyOn(api.binder, "all").and.returnValue($q.when({
                data: binders
            }));
            spyOn(api.binder, "destroy").and.returnValue($q.when({
                data: "success"
            }));

            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn(notify, "success");

            createController();
            $rootScope.$apply();
            spyOn(ctrl.binders, "indexOf").and.callThrough();
            spyOn(ctrl.binders, "splice").and.callThrough();
            ctrl.deleteBinderConfirmed(ctrl.binders[0]);
            $rootScope.$apply();

            expect(api.binder.destroy).toHaveBeenCalledWith(1);
            expect(loading.show).toHaveBeenCalled();
            expect(ctrl.binders.indexOf).toHaveBeenCalled();
            expect(ctrl.binders.splice).toHaveBeenCalledWith(0, 1);
            expect(loading.close).toHaveBeenCalled();
            expect(notify.success).toHaveBeenCalled();

        });

        it('should call api binder delete', function() {

            var binders = {
                total: 3,
                items: [{
                    id: 1,
                    name: "Delete me!"
                }, {
                    id: 2
                }, {
                    id: 3
                }]
            };

            spyOn(api.binder, "all").and.returnValue($q.when({
                data: binders
            }));
            spyOn(api.binder, "destroy").and.returnValue($q.reject({
                data: "error"
            }));

            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            $rootScope.$apply();
            ctrl.deleteBinderConfirmed(ctrl.binders[0]);
            $rootScope.$apply();

            expect(api.binder.destroy).toHaveBeenCalledWith(1);
            expect(loading.show).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();

        });

    });

});

describe('homebinders', function() {
    var $scope, $compile, api, session, $q, $state, $stateParams, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        session = _$injector_.get("Session");
        $q = _$injector_.get("$q");
        $state = _$injector_.get("$state");
        $stateParams = _$injector_.get("$stateParams");

        $scope = $rootScope.$new(), $compile = _$compile_;

        $stateParams.binderId = 1;
        $state.go("binders");

        spyOn(session, "getUser").and.returnValue({ id: 1 });
        spyOn(api.binder, "all").and.returnValue($q.when({ data: { items: [] } }));

        element = $compile('<homebinders></homebinders>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the elements', function() {
            expect(element.html()).toContain("My Binders");
        });
    });
});