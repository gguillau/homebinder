(function() {
    "use strict";

    angular
        .module("hb.homebinders.homebinder.transfer.transferModal", [])
        .factory("TransferModal", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "homebinders/homebinder/transfer/transferModal/transferModal.tpl.html",
                            controller: "TransferModalController as ctrl",
                            resolveData: opts,
                            closed: opts.closed
                        });
                    },
                    resend: function(opts) {
                        Modals.show({
                            templateUrl: "homebinders/homebinder/transfer/transferModal/resendTransferModal.tpl.html",
                            controller: "TransferModalController as ctrl",
                            resolveData: opts,
                            closed: opts.closed
                        });
                    }
                };
            }
        ])
        .controller("TransferModalController", [
            "$modalInstance",
            "data",
            "Notify",
            "Loading",
            "TransferController",
            TransferModalController
        ]);

    function TransferModalController($modal, opts, notify, loading, TransferController) {
        var Model = function() {
            // Call parent
            TransferController.call(this);

            this.opts = opts;
            this.transfer = opts.transfer;
            this.binder = opts.binder;
            this.resend = opts.resend;
            this.onCancelSuccess = opts.cancelTransfer;

            this.notify = notify;
            this.loading = loading;
            this.$modal = $modal;
            this.init();
        };

        Model.prototype = Object.create(TransferController.prototype);

        angular.extend(Model.prototype, {
            init: function() {
                if (!this.resend) {
                    // set the default transfer type
                    this.transferType = this.opts.defaultTransferType || "ownership";

                    // show the transfer options?
                    this.showTransferTypes = this.opts.showTransferTypes === undefined ? true : this.opts.showTransferTypes;
                    this.getBinder();
                }
                else {
                    if (this.transfer) {
                        this.transfer.receiver_id = this.transfer.receiver.id;
                        this.transfer.sender_id = this.transfer.sender.id;
                        this.getUser();
                    }
                    else if (this.binder) {
                        this.getBinder();
                    }
                }
            },

            transferCompleted: function(response) {
                this.saving = false;
                this.loading.close();
                this.notify.success(this.resources.success);
                this.$modal.close({
                    binder: this.binder,
                    transferType: this.transferType
                });
            },
            
            resendTransferCompleted: function(response) {
                this.saving = false;
                this.loading.close();
                this.notify.success(this.resources.success);
                this.closeModal();
            },

            cancelTransferSuccess: function(response) {
                this.notify.success("Cancelled!");
                this.$modal.close();
                this.loading.close();
            },

            closeModal: function() {
                this.$modal.dismiss("cancel");
            }
        });

        this.model = new Model();
    }
})();