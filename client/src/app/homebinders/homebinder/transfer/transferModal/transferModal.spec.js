describe("TransferModalController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        modal,
        binder,
        opts,
        $modalInstance,
        loading,
        notify,
        api,
        TransferModal,
        TransferController,
        ModalService;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        TransferModal = _$injector_.get("TransferModal");
        ModalService = _$injector_.get("ModalService");
        TransferController = _$injector_.get("TransferController");
        api = _$injector_.get("hb.api");
        
        loading = {
            show: function(msg) {},
            isLoading: function() {},
            close: function() {}
        };

        modal = {
            show: function(arg) {}
        };

        $modalInstance = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };
        
        notify = {
            error: function(msg) {},
            success: function(msg) {},
            info: function() {}
        };

        binder = {
            appliances: null,
            created_at: "2015-09-06T04:29:36Z",
            id: 1,
            name: "This is a fake home",
            primary: null,
            details: "this is a home",
            last_recall: null,
            partner: {
                address: null,
                binder_count: 7,
                binder_logo_id: null,
                code: "FAKE",
                contact: "Test Inspector",
                coupons: null,
                created_at: "2015-04-25T15:56:15Z",
                email: "girgalicious@gmail.com",
                id: 115,
                name: "Test Inspector",
                partner_type: "inspector",
                phone: "+17814928454",
                sellers_logo_id: null,
                tags: [],
                vendors: []
            },
            vendor: null,
            recalls: 0,
            documents: null,
            maintenance_items: null,
            binder_contractors: null,
            property: {
                address1: "",
                address2: "",
                city: "",
                state: "",
                country: "",
                zip: ""

            },
            subscription: null,
            seller_report: null,
            permissions: {
                can_create: true,
                can_create_seller_report: true,
                can_destroy: true,
                can_edit_seller_report: true,
                can_read: true,
                can_share: true,
                can_subscribe: true,
                can_transfer: true,
                can_view_master_report: false,
                can_write: true
            },
            tags: [{
                tag: "client",
                metadata: "{\"email\":\"faketransferemail@gmail.com\"}"
            }]
        };

        opts = {
            binder: binder
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("TransferModalController", {
            "$modalInstance": $modalInstance,
            "data": opts,
            "ModalService": modal,
            "Notify": notify,
            "Loading": loading,
            "TransferController": TransferController
        });

        $scope.ctrl = ctrl;
    }

    describe("ctrl.init", function() {
        it("loads options with only a binder", function() {

            spyOn(api.binder, "get").and.returnValue($q.when({
                data: binder
            }));

            createController();
            $rootScope.$apply();

            expect(ctrl.model.binder).toEqual(binder);
        });

        it("loads default transfer type from options", function() {
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: binder
            }));

            opts.defaultTransferType = "copy";

            createController();
            $rootScope.$apply();

            expect(ctrl.model.transferType).toEqual("copy");
        });

        it("hides transfer types", function() {
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: binder
            }));

            opts.showTransferTypes = false;
            createController();
            $rootScope.$apply();

            expect(ctrl.model.showTransferTypes).toEqual(false);
        });

        it("calls get User", function() {
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: binder
            }));

            opts.showTransferTypes = false;
            createController();
            spyOn(ctrl.model, "getUser");
            ctrl.model.resend = true;
            ctrl.model.transfer = {
                receiver: {
                    id: 1
                },
                sender: {
                    id: 2
                }
            };
            ctrl.model.init();

            expect(ctrl.model.getUser).toHaveBeenCalled();
        });
    });
    
    describe("ctrl.transferCompleted", function() {
        it("should call $modalInstance dismiss function", function() {
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: binder
            }));
            spyOn(loading, "close");
            spyOn(notify, "success");
            spyOn($modalInstance, "close");

            createController();
            $rootScope.$apply();
            ctrl.model.transferCompleted();
            $rootScope.$apply();

            expect(ctrl.model.saving).toEqual(false);
            expect(loading.close).toHaveBeenCalled();
            expect(notify.success).toHaveBeenCalled();
            expect($modalInstance.close).toHaveBeenCalled();
        });

    });
    
    describe("ctrl.resendTransferCompleted", function() {
        it("should call closeModal()", function() {
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: binder
            }));
            spyOn(loading, "close");
            spyOn(notify, "success");

            createController();
            $rootScope.$apply();
            
            spyOn(ctrl.model, "closeModal");
            
            ctrl.model.resendTransferCompleted();
            $rootScope.$apply();

            expect(ctrl.model.saving).toEqual(false);
            expect(loading.close).toHaveBeenCalled();
            expect(notify.success).toHaveBeenCalled();
            expect(ctrl.model.closeModal).toHaveBeenCalled();
        });

    });

    describe("ctrl.cancelTransferSuccess", function() {
        it("calls notify", function() {
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: binder
            }));

            spyOn(notify, "success");

            createController();
            ctrl.model.cancelTransferSuccess();

            expect(notify.success).toHaveBeenCalled();
        });
    });

    describe("ctrl.closeModal", function() {
        it("should call $modalInstance dismiss function", function() {
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: binder
            }));

            spyOn($modalInstance, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.model.closeModal();
            $rootScope.$apply();

            expect($modalInstance.dismiss).toHaveBeenCalledWith('cancel');
        });

    });

    describe('TransferModal', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            TransferModal.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });

        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            TransferModal.resend({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});