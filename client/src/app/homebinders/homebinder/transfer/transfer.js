(function() {
    "use strict";

    angular
        .module("hb.homebinders.homebinder.transfer", [
            "hb.homebinders.homebinder.transfer.transferModal",
            "hb.homebinders.homebinder.transfer.index"
        ])
        .factory("TransferController", [
            "$log",
            "hb.api",
            "Session",
            "Notify",
            "Loading",
            "hb.resources",
            "Address",
            function TransferController($log, api, session, notify, loading, resources, Address) {
                var Model = function() {
                    this.$log = $log;
                    this.api = api;
                    this.session = session;
                    this.notify = notify;
                    this.transferType = "ownership";
                    this.showTransferTypes = true;
                    this.loading = loading;
                    this.resources = resources.transfer;
                    this.address = Address;
                    this.resources = angular.extend({}, this.resources, resources.usersForm);
                    this.currentUser = session.getUser();
                    this.homeowner = {
                        firstName: null,
                        lastName: null,
                        email: null,
                        phone: {},
                        readonly: false
                    };
                    this.saving = false;
                };

                angular.extend(Model.prototype, {
                    init: function() {
                        if (!this.resend) {
                            this.getBinder();
                        }
                        else {
                            if (this.transfer) {
                                this.transfer.receiver_id = this.transfer.receiver.id;
                                this.transfer.sender_id = this.transfer.sender.id;
                                this.getUser();
                            }
                            else if (this.binder) {
                                this.getBinder();
                            }
                        }
                    },

                    getBinder: function() {
                        this.api.binder.get(this.binder.id).then(
                            angular.bind(this, this.getBinderSuccess),
                            angular.bind(this, this.getBinderError));
                    },

                    getBinderSuccess: function(response) {
                        this.binder = response.data;
                        this.transfer = response.data.transfer;
                        this.country = this.address.findCountryByCode(this.binder.property.country);
                        if (this.transfer) {
                            this.getUser();
                        }
                    },

                    getBinderError: function(response) {
                        this.$log.error(response);
                    },

                    getUser: function() {
                        this.api.user.get(this.transfer.receiver_id).then(
                            angular.bind(this, this.getUserSuccess),
                            angular.bind(this, this.onError));
                    },

                    getUserSuccess: function(response) {
                        this.homeowner = {
                            firstName: response.data.user_profile_attributes.first_name,
                            lastName: response.data.user_profile_attributes.last_name,
                            email: response.data.email,
                            phone: response.data.user_profile_attributes.mobile_phone,
                            readonly: false
                        };
                        this.country = this.address.findCountryByCode(response.data.user_profile_attributes.address_attributes.country);
                        if (response.data.sign_in_count > 0 || response.data.role != "homeowner") {
                            this.homeowner.readonly = true;
                        }
                    },

                    onError: function(response) {
                        this.$log.error(response);
                        this.notify.error(response.data);
                    },

                    preSubmit: function() {
                        // if a pending transfer exists then transfer the binder
                        if (this.transfer) {
                            this.submit();
                        }
                        // create the user, the transfer and then transfer it
                        else {
                            this.confirmEmail();
                        }
                    },

                    confirmEmail: function() {
                        this.api.user.find_by_email(this.homeowner.email).then(
                            angular.bind(this, this.onConfirmEmail),
                            angular.bind(this, this.onError));
                    },

                    onConfirmEmail: function(response) {
                        if (response.data && response.data.id) {
                            this.onCreateHomeownerSuccess(response);
                        }
                        else {
                            this.createHomeowner();
                        }
                    },

                    createHomeowner: function() {
                        var homeowner = {
                            email: this.homeowner.email,
                            password: "password",
                            role: "homeowner",
                            create_method: "transfer",
                            created_by: this.currentUser.id,
                            user_profile_attributes: {
                                first_name: this.homeowner.firstName,
                                last_name: this.homeowner.lastName,
                                mobile_phone: "+" + this.country.code + this.homeowner.phone.national,
                                address_attributes: {
                                    address1: this.binder.property.address1,
                                    address2: this.binder.property.address2,
                                    city: this.binder.property.city,
                                    state: this.binder.property.state,
                                    zip: this.binder.property.zip,
                                    country: this.binder.property.country
                                }
                            }
                        };

                        this.loading.show(this.resources.saving + "...");
                        this.api.user.create({ user: homeowner }).then(
                            angular.bind(this, this.onCreateHomeownerSuccess),
                            angular.bind(this, this.transferError)
                        );

                    },

                    onCreateHomeownerSuccess: function(response) {
                        this.homeowner.id = response.data.id;
                        this.createTransfer();
                    },

                    createTransfer: function() {
                        var transfer = {
                            transfer_type: this.transferType,
                            binder_id: this.binder.id,
                            status: "created",
                            sender_id: this.currentUser.id,
                            receiver_id: this.homeowner.id
                        };

                        this.api.transfer.create(transfer).then(
                            angular.bind(this, this.onCreateTransfer),
                            angular.bind(this, this.transferError)
                        );
                    },

                    onCreateTransfer: function(response) {
                        this.transfer = response.data;
                        this.submit();
                    },

                    submit: function() {
                        if (this.homeowner.email.toLowerCase() == this.currentUser.email.toLowerCase()) {
                            this.notify.info(this.resources.warning);
                            return;
                        }

                        this.saving = true;
                        this.loading.show(this.resources.saving + "...");
                        this.api.transfer.execute(this.transfer.id, {
                                user: {
                                    first: this.homeowner.firstName,
                                    last: this.homeowner.lastName,
                                    email: this.homeowner.email,
                                    phone: "+" + this.country.code + this.homeowner.phone.national
                                },
                                transfer_type: this.transferType,
                                cc_transfer_by: this.cc_transfer_by
                            })
                            .then(
                                angular.bind(this, this.transferCompleted),
                                angular.bind(this, this.transferError)
                            );
                    },

                    transferCompleted: function(response) {
                        this.saving = false;
                        this.loading.close();
                        this.notify.success(this.resources.success);
                    },

                    transferError: function(response) {
                        this.$log.error(response);
                        this.notify.error(response.data);
                        this.saving = false;
                        this.loading.close();
                    },

                    resendTransfer: function() {
                        if (this.homeowner.email.toLowerCase() == this.currentUser.email.toLowerCase()) {
                            this.notify.info(this.resources.warning);
                            return;
                        }

                        this.saving = true;
                        this.loading.show(this.resources.saving + "...");
                        this.api.transfer.resend(this.transfer.id, {
                                user: {
                                    first: this.homeowner.firstName,
                                    last: this.homeowner.lastName,
                                    email: this.homeowner.email,
                                    phone: "+" + this.country.code + this.homeowner.phone.national
                                }
                            })
                            .then(
                                angular.bind(this, this.resendTransferCompleted),
                                angular.bind(this, this.transferError)
                            );
                    },

                    cancelTransfer: function() {
                        this.saving = true;
                        this.loading.show(this.resources.saving + "...");
                        this.api.transfer.cancel(this.transfer.id)
                            .then(
                                angular.bind(this, this.cancelTransferSuccess),
                                angular.bind(this, this.transferError)
                            );
                    },

                    cancelTransferSuccess: function(response) {
                        this.notify.success("Cancelled!");
                        this.loading.close();
                    }
                });

                return Model;
            }
        ]);

})();