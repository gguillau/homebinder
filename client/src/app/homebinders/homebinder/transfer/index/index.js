(function() {
    "use strict";

    angular.module('hb.homebinders.homebinder.transfer.index', [])
        .factory("TransfersController", [
            "hb.framework.indexBase",
            "TransferModal",
            "hb.api",
            "hb.resources",
            "$stateParams",
            "Notify",
            "$log",
            "Session",
            function(IndexBase, TransferModal, api, resources, $stateParams, notify, $log, session) {
                var Model = function() {
                    // call the parent class
                    IndexBase.call(this);
                    this.currentUser = session.getUser();
                    this.api = api;
                    // apply sellerReport specific resource strings
                    this.resources = angular.extend({}, this.resources, resources.transfer);
                    // set the refresh API call
                    this.refreshCall = api.transfer.all;
                    // set the delete API call
                    this.deleteCall = api.transfer.destroy;
                    // delete prop
                    this.nameProperty = "created_at";
                    this.maxSize = 10;
                    this.sortOptions = [{
                        orderBy: "transfers.created_at",
                        order: "desc"
                    }, {
                        orderBy: "transfer.created_at",
                        order: "asc"
                    }];
                    // "sent", "created", "pending", "declined", "accepted", "delivery_failed"
                    this.statusOptions = [{
                        name: undefined,
                        value: "All Statuses"
                    }, {
                        name: "sent",
                        value: "Sent"
                    }, {
                        name: "created",
                        value: "Created"
                    }, {
                        name: "pending",
                        value: "Pending"
                    }, {
                        name: "declined",
                        value: "Declined"
                    }, {
                        name: "accepted",
                        value: "Accepted"
                    }, {
                        name: "delivery_failed",
                        value: "Delivery Failed"
                    }];
                    this.sortOption = this.sortOptions[0];
                    this.orderBy = this.sortOption.orderBy;
                    this.order = this.sortOption.order;
                    this.transferArgs = {
                        receiver_id: $stateParams.userId,
                        binder_id: $stateParams.binderId,
                        transfer_type: undefined,
                        status: undefined,
                        sender_id: undefined
                    };
                    this.settings = {
                        canSearch: false
                    };
                    this.setHeaders();
                };

                Model.prototype = Object.create(IndexBase.prototype);

                angular.extend(Model.prototype, {

                    setHeaders: function() {
                        this.headers = this.resources.headers.map(angular.bind(this, function(attribute) {
                            var sortable = false,
                                orderBy = null,
                                sorted = false,
                                show = true;
                            if (attribute === "Binder ID") {
                                sorted = true;
                                sortable = true;
                                orderBy = "transfers.binder_id";
                            }
                            else if (attribute === "Sender") {
                                sortable = true;
                                orderBy = "transfers.sender_id";
                            }
                            else if (attribute === "Receiver") {
                                sortable = true;
                                orderBy = "transfers.receiver_id";
                            }
                            else if (attribute === "Status") {
                                sortable = true;
                                orderBy = "transfers.status";
                            }
                            return {
                                name: attribute,
                                sortable: sortable,
                                sorted: sorted,
                                orderBy: orderBy,
                                show: show,
                                order: "desc"
                            };
                        }));
                        this.sortOption = this.headers[0];
                    },

                    addQueryArgs: function() {
                        this.addArgs("binder_id");
                        this.addArgs("receiver_id");
                        this.addArgs("sender_id");
                        this.addArgs("status");
                    },

                    addArgs: function(arg) {
                        if (this.transferArgs[arg]) {
                            this.queryArgs[arg] = this.transferArgs[arg];
                        }
                        else {
                            delete this.queryArgs[arg];
                        }
                    },
                    
                    send: function(transfer) {
                        TransferModal.show({
                            transfer: transfer,
                            binder: transfer.binder,
							showTransferTypes: false,
							defaultTransferType: "ownership",
                            closed: angular.bind(this, this.removeItem)
                        });
                    },

                    resend: function(transfer) {
                        TransferModal.resend({
                            transfer: transfer,
                            binder: transfer.binder,
							showTransferTypes: false,
							defaultTransferType: "ownership",
							resend: true
                        });
                    },

                    onError: function(response) {
                        $log.error(response);
                        notify.error(response);
                    },

                    canResend: function(transfer) {
                        switch (transfer.status) {
                            case "sent":
                                return true;
                            default:
                                return false;
                        }
                    }
                });

                return Model;
            }
        ]);

})();