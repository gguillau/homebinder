describe("TransfersController", function() {
    var TestClass,
        TransfersController,
        $q,
        notify,
        TransferModal;

    // load the homebinder module
    beforeEach(module("homebinder"));
    
    // get the base class under test
    beforeEach(inject(function($injector) {
        TransfersController = $injector.get("TransfersController");
        $q = $injector.get("$q");
        notify = $injector.get("Notify");
        TransferModal = $injector.get("TransferModal");
    }));

    function createTestClass() {
        TestClass = function() {
            TransfersController.call(this);
        };

        TestClass.prototype = Object.create(TransfersController.prototype);

        return new TestClass();
    }
    
    describe("setHeaders", function() {
        it("creates the headers", inject(function() {
            var model = createTestClass();
            model.setHeaders();
            
            expect(model.headers.length).toEqual(5);
        }));
    });
    
    describe("addQueryArgs", function() {
        it("adds the query arguments", inject(function() {
            var model = createTestClass();

            model.transferArgs["binder_id"] = 1;
            model.transferArgs["receiver_id"] = 2;
            model.transferArgs["sender_id"] = 3;
            model.transferArgs["status"] = "sent";
            model.addQueryArgs();

            expect(model.queryArgs["binder_id"]).toEqual(1);
            expect(model.queryArgs["receiver_id"]).toEqual(2);
            expect(model.queryArgs["sender_id"]).toEqual(3);
            expect(model.queryArgs["status"]).toEqual("sent");
        }));

    });

    describe("addArgs", function() {
        it("adds the query argument", inject(function() {
            var model = createTestClass();
            model.transferArgs["status"] = "created";
            model.addArgs("status");

            expect(model.queryArgs["status"]).toEqual("created");
        }));

        it("deletes the query argument", inject(function() {
            var model = createTestClass();

            model.transferArgs["status"] = "accepted";
            model.addArgs("status");
            model.transferArgs["status"] = undefined;
            model.addArgs("status");

            expect(model.queryArgs["status"]).toBe(undefined);
        }));
    });

    describe("send", function() {
        it("calls TransferModal.show", function() {
            var model = createTestClass();
            spyOn(TransferModal, "show").and.returnValue($q.when({
                data: "success"
            }));

            model.send({ id: 1 });

            expect(TransferModal.show).toHaveBeenCalled();
        });
    });
    
    describe("resend", function() {
        it("calls TransferModal.resend", function() {
            var model = createTestClass();
            spyOn(TransferModal, "resend").and.returnValue($q.when({
                data: "success"
            }));

            model.resend({ id: 1 });

            expect(TransferModal.resend).toHaveBeenCalled();
        });
    });

    describe("onError", function() {
        it("calls notify error", function() {
            var model = createTestClass();
            
            spyOn(notify, "error");

            model.onError({ data: "" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("canResend", function() {
        it("returns true", function() {
            var model = createTestClass();
            
            spyOn(notify, "error");

            var result = model.canResend({ status: "sent" });

            expect(result).toBe(true);
        });
        it("returns false", function() {
            var model = createTestClass();
            
            spyOn(notify, "error");

            var result = model.canResend({ status: "accepted" });

            expect(result).toBe(false);
        });
    });

});