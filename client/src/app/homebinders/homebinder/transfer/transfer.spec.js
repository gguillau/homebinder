describe("TransferController", function() {
    var binder,
        TransferController,
        TestClass,
        $log;

    // load the homebinder module
    beforeEach(module("homebinder"));
    
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
        
        // mock the notify service
        $provide.factory("Notify", function() {
            return {
                success: function() {},
                error: function() {},
                info: function() {}
            };
        });

        // mock the Session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin", email: "test-email@gmail.com" };
                },
                getJwt: function() {},
                getLocale: function() {
                    return null;
                },
                setLocale: function() {}
            };
        });

    }));
    
    // get the base class under test
    beforeEach(inject(function($injector) {
        TransferController = $injector.get("TransferController");
        
        binder = {
            appliances: null,
            created_at: "2015-09-06T04:29:36Z",
            id: 1,
            name: "This is a fake home",
            primary: null,
            details: "this is a home",
            last_recall: null,
            partner: {
                address: null,
                binder_count: 7,
                binder_logo_id: null,
                code: "FAKE",
                contact: "Test Inspector",
                coupons: null,
                created_at: "2015-04-25T15:56:15Z",
                email: "girgalicious@gmail.com",
                id: 115,
                name: "Test Inspector",
                partner_type: "inspector",
                phone: "+17814928454",
                sellers_logo_id: null,
                tags: [],
                vendors: []
            },
            vendor: null,
            recalls: 0,
            documents: null,
            maintenance_items: null,
            binder_contractors: null,
            property: {
                address1: "",
                address2: "",
                city: "",
                state: "",
                country: "",
                zip: ""

            },
            subscription: null,
            seller_report: null,
            permissions: {
                can_create: true,
                can_create_seller_report: true,
                can_destroy: true,
                can_edit_seller_report: true,
                can_read: true,
                can_share: true,
                can_subscribe: true,
                can_transfer: true,
                can_view_master_report: false,
                can_write: true
            },
            tags: [{
                tag: "client",
                metadata: "{\"email\":\"faketransferemail@gmail.com\"}"
            }]
        };
        
        $log = $injector.get("$log");
        
    }));

    function createTestClass() {
        TestClass = function() {
            TransferController.call(this);
            this.binder = {
                id: 1
            };
        };

        TestClass.prototype = Object.create(TransferController.prototype);

        return new TestClass();
    }

    describe("ctrl.init", function() {

        it("loads options with only a binder", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            spyOn(model.api.binder, "get").and.returnValue($q.when({
                data: binder
            }));
            
            model.init();
            $rootScope.$apply();
            
            expect(model.binder).toEqual(binder);
        }));

        it("calls get User", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            spyOn(model.api.binder, "get").and.returnValue($q.when({
                data: binder
            }));
            spyOn(model.api.user, "get").and.returnValue($q.when({
                data: {
                    user_profile_attributes: {
                        first_name: "name",
                        last_name: "last",
                        mobile_phone: "phone"
                    },
                    email: "email",
                    sign_in_count: 0,
                    role: "homeowner"
                }
            }));
            spyOn(model, "getUser");
            model.resend = true;
            model.transfer = {
                receiver: {
                    id: 1
                },
                sender: {
                    id: 2
                }
            };
            model.init();
            $rootScope.$apply();

            expect(model.getUser).toHaveBeenCalled();
        }));
    });

    describe("ctrl.getBinderError", function() {
        it("calls $log error", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            spyOn(model.api.binder, "get").and.returnValue($q.when({
                data: binder
            }));

            spyOn($log, "error");
            model.getBinderError({ data: "error" });
            $rootScope.$apply();

            expect($log.error).toHaveBeenCalled();
        }));
    });

    describe("ctrl.getUserSuccess", function() {
        it("sets homeowner to read only", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            spyOn(model.api.binder, "get").and.returnValue($q.when({
                data: binder
            }));

            model.currentUser = { role: "homeowner" };
            model.getUserSuccess({ data: { id: 1, sign_in_count: 1, user_profile_attributes: { address_attributes: {} } } });
            $rootScope.$apply();

            expect(model.homeowner.readonly).toBe(true);
        }));
    });

    describe("ctrl.onError", function() {
        it("calls $log error", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            spyOn(model.api.binder, "get").and.returnValue($q.when({
                data: binder
            }));

            spyOn($log, "error");
            model.onError({ data: "error" });
            $rootScope.$apply();

            expect($log.error).toHaveBeenCalled();
        }));
    });

    describe("ctrl.onConfirmEmail", function() {
        it("calls createHomeowner", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            spyOn(model, "createHomeowner");
            model.onConfirmEmail({});
            $rootScope.$apply();

            expect(model.createHomeowner).toHaveBeenCalled();
        }));
    });

    describe("ctrl.preSubmit", function() {
        it("calls submit", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            spyOn(model.api.binder, "get").and.returnValue($q.when({
                data: binder
            }));

            spyOn(model, "submit");
            model.transfer = { id: 1 };
            
            model.init();
            model.preSubmit();
            $rootScope.$apply();

            expect(model.submit).toHaveBeenCalled();
        }));

        it("creates the user and transfer", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            spyOn(model.api.binder, "get").and.returnValue($q.when({
                data: binder
            }));
            spyOn(model.api.transfer, "create").and.returnValue($q.when({
                data: {
                    id: 1
                }
            }));
            spyOn(model.api.user, "find_by_email").and.returnValue($q.when({
                data: {
                    id: 1
                }
            }));
            spyOn(model.api.user, "create").and.returnValue($q.when({
                data: {
                    id: 1,
                    email: "test@gmail.com",
                    user_profile_attributes: {
                        first_name: "test",
                        last_name: "name",
                        phone: ""
                    }
                }
            }));

            var user = {
                email: "user@homebinder.com"
            };

            spyOn(model.session, "getUser").and.returnValue(user);
            spyOn(model.api.transfer, "execute").and.returnValue($q.when({
                data: {
                    transfer_to: "transfer@homebinder.com"
                }
            }));
            spyOn(model.notify, "success");

            model.homeowner.firstName = "roger";
            model.homeowner.lastName = "smith";
            model.homeowner.email = "transfer@homebinder.com";
            model.homeowner.id = 1;
            model.homeowner.phone = {};
            
            model.init();
            model.preSubmit();
            $rootScope.$apply();

            expect(model.notify.success).toHaveBeenCalled();
            expect(model.api.transfer.execute).toHaveBeenCalledWith(1, {
                user: {
                    first: "roger",
                    last: "smith",
                    email: 'transfer@homebinder.com',
                    phone: "+1undefined"
                },
                transfer_type: 'ownership',
                cc_transfer_by: undefined
            });

        }));
    });

    describe("ctrl.createHomeowner", function() {
        it("calls user create", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            spyOn(model.api.user, "create").and.returnValue($q.when({
                data: {}
            }));

            model.country = { code: "US" };
            model.currentUser = { id: 1, email: "test@gmai.com" };
            model.homeowner = { email: "to@homebinder.com", phone: {} };
            model.binder = { property: {} };
            model.createHomeowner();
            $rootScope.$apply();

            expect(model.api.user.create).toHaveBeenCalled();
        }));
    });

    describe("ctrl.submit", function() {
        it("does not allow transferring to the owner", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            spyOn(model.api.binder, "get").and.returnValue($q.when({
                data: binder
            }));

            var user = {
                email: "to@homebinder.com"
            };

            spyOn(model.session, "getUser").and.returnValue(user);
            spyOn(model.notify, "info");

            model.init();

            model.homeowner.email = "test-email@gmail.com";
            model.submit();
            $rootScope.$apply();

            expect(model.notify.info).toHaveBeenCalled();
        }));

        it("transfers the binder", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            binder.transfer = {
                id: 1,
                receiver_id: 2
            };
            spyOn(model.api.binder, "get").and.returnValue($q.when({
                data: binder
            }));
            spyOn(model.api.user, "get").and.returnValue($q.when({
                data: {
                    id: 1,
                    email: "test@gmail.com",
                    user_profile_attributes: {
                        address_attributes: {},
                        first_name: "test",
                        last_name: "name",
                        phone: ""
                    }
                }
            }));

            var user = {
                email: "user@homebinder.com"
            };

            spyOn(model.session, "getUser").and.returnValue(user);
            spyOn(model.api.transfer, "execute").and.returnValue($q.when({
                data: {
                    transfer_to: "transfer@homebinder.com"
                }
            }));
            spyOn(model.notify, "success");

            model.transfer = {
                id: 1
            };
            model.homeowner.firstName = "roger";
            model.homeowner.lastName = "smith";
            model.homeowner.email = "transfer@homebinder.com";
            model.homeowner.phone = {};
            model.country = {
                code: 1
            };
    
            model.init();
            model.submit();
            $rootScope.$apply();

            expect(model.notify.success).toHaveBeenCalled();
            expect(model.api.transfer.execute).toHaveBeenCalledWith(1, {
                user: {
                    first: "roger",
                    last: "smith",
                    email: 'transfer@homebinder.com',
                    phone: "+1undefined"
                },
                transfer_type: 'ownership',
                cc_transfer_by: undefined
            });

        }));

        it("responds to a transfer error", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            binder.transfer = {
                id: 1,
                receiver_id: 2
            };
            spyOn(model.api.binder, "get").and.returnValue($q.when({
                data: binder
            }));
            spyOn(model.api.user, "get").and.returnValue($q.when({
                data: {
                    id: 1,
                    email: "test@gmail.com",
                    user_profile_attributes: {
                        address_attributes: {},
                        first_name: "test",
                        last_name: "name",
                        phone: ""
                    }
                }
            }));

            var user = {
                email: "user@homebinder.com"
            };

            spyOn(model.session, "getUser").and.returnValue(user);
            spyOn(model.api.transfer, "execute").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(model.notify, "error");
            spyOn($log, "error");

            model.transfer = {
                id: 1
            };
            model.homeowner.firstName = "roger";
            model.homeowner.lastName = "smith";
            model.homeowner.email = "transfer@homebinder.com";
            model.homeowner.phone = {};
            model.country = {
                code: 1
            };

            model.submit();
            $rootScope.$apply();

            expect(model.notify.error).toHaveBeenCalledWith("error");
            expect(model.api.transfer.execute).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
        }));
    });

    describe("ctrl.resendTransfer", function() {
        it("does not allow transferring to the owner", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            spyOn(model.api.binder, "get").and.returnValue($q.when({
                data: binder
            }));

            var user = {
                email: "to@homebinder.com"
            };

            spyOn(model.session, "getUser").and.returnValue(user);
            spyOn(model.notify, "info");

            model.homeowner.email = "test-email@gmail.com";
            model.resendTransfer();
            $rootScope.$apply();

            expect(model.notify.info).toHaveBeenCalled();
        }));

        it("transfers the binder", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            binder.transfer = {
                id: 1,
                receiver_id: 2
            };
            spyOn(model.api.binder, "get").and.returnValue($q.when({
                data: binder
            }));
            spyOn(model.api.user, "get").and.returnValue($q.when({
                data: {
                    id: 1,
                    email: "test@gmail.com",
                    user_profile_attributes: {
                        address_attributes: {},
                        first_name: "test",
                        last_name: "name",
                        phone: ""
                    }
                }
            }));

            var user = {
                email: "user@homebinder.com"
            };

            spyOn(model.session, "getUser").and.returnValue(user);
            spyOn(model.api.transfer, "resend").and.returnValue($q.when({
                data: {
                    transfer_to: "transfer@homebinder.com"
                }
            }));

            model.transfer = {};
            model.country = {};
            model.homeowner.firstName = "roger";
            model.homeowner.lastName = "smith";
            model.homeowner.email = "transfer@homebinder.com";
            model.homeowner.phone = {};
            model.country = {
                code: 1
            };
            
            model.resendTransfer();
            $rootScope.$apply();

            expect(model.api.transfer.resend).toHaveBeenCalled();

        }));
    });

    describe("ctrl.cancelTransfer", function() {
        it("cancels the transfer", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            binder.transfer = {
                id: 1,
                receiver_id: 2
            };
            spyOn(model.api.binder, "get").and.returnValue($q.when({
                data: binder
            }));
            spyOn(model.api.user, "get").and.returnValue($q.when({
                data: {
                    id: 1,
                    email: "test@gmail.com",
                    user_profile_attributes: {
                        address_attributes: {},
                        first_name: "test",
                        last_name: "name",
                        phone: ""
                    }
                }
            }));

            var user = {
                email: "user@homebinder.com"
            };

            spyOn(model.session, "getUser").and.returnValue(user);
            spyOn(model.api.transfer, "cancel").and.returnValue($q.when({
                data: {
                    transfer_to: "transfer@homebinder.com"
                }
            }));

            model.transfer = {};
            model.cancelTransfer();
            $rootScope.$apply();

            expect(model.api.transfer.cancel).toHaveBeenCalled();

        }));
    });

    describe("ctrl.cancelTransferSuccess", function() {
        it("calls notify", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            spyOn(model.api.binder, "get").and.returnValue($q.when({
                data: binder
            }));

            spyOn(model.notify, "success");

            model.cancelTransferSuccess();
            $rootScope.$apply();

            expect(model.notify.success).toHaveBeenCalled();
        }));
    });

});