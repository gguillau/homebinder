(function() {
    "use strict";

    angular
        .module("hb.homebinders.finishes", [
            "hb.homebinders.finishes.routes",
            "hb.homebinders.finishes.details",
            "hb.homebinders.finishes.form",
            "hb.homebinders.finishes.modal"
        ])
        .directive("binderFinishes", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/finishes/finishes.tpl.html",
                controller: "BinderFinishesController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("BinderFinishesController", [
            "BinderItemsIndexController",
            "hb.resources",
            Controller
        ]);

    function Controller(BinderItemsIndexController, resources) {
        var Model = function() {
            // call the parent class
            BinderItemsIndexController.call(this);
            this.resources = angular.extend({}, this.resources, resources.binderFinishesIndex);
            this.refreshCall = this.api.finish.all;
            this.itemType = "finishes";
            this.modalController = "FinishModalController";
            this.itemNewState = "binder.finishesNew";
            this.itemDetailsState = "binder.finishesDetails";
            this.init();
        };

        Model.prototype = Object.create(BinderItemsIndexController.prototype);

        this.model = new Model();
    }
})();