(function() {
    "use strict";

    angular
        .module("hb.homebinders.finishes.form", [])
        .directive("hbFinishForm", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/finishes/form/form.tpl.html",
                controller: "FinishFormController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        // Model backing the finish modal form
        .controller("FinishFormController", [
            "hb.resources",
            "BinderItemsFormController",
            "hb.api",
            "Address",
            "$stateParams",
            Controller
        ]);

    function Controller(resources, BinderItemsFormController, api, Address, $stateParams) {
        var that = this;
        var Model = function() {
            this.cfg = that.cfg;
            this.onSaved = that.onSaved;
            this.onCancel = that.onCancel;
            this.resources = angular.extend({}, this.resources, resources.binderFinishesForm);
            this.taggerCfg = {
                finishTags: false
            };
            this.tagName = "finish_";
            // call the parent class
            this.saveCall = api.finish.update;
            this.createCall = api.finish.create;
            this.deleteCall = api.finish.destroy;
            this.refreshCall = api.finish.get;
            
            // If $stateParams has an id, we are editing an exisiting item and back should send us to that item's details
            if ($stateParams.id) {
                this.indexState = "binder.finishesDetails";
                this.indexParams = { binderId: $stateParams.binderId, id: $stateParams.id };
            }
            // Else, we were creating a new item and back should send us to the list of items
            else {
                this.indexState = "binder.finishes";
                this.indexParams = { binderId: $stateParams.binderId };
            }
            this.editState = "binder.finishesEdit";
            
            BinderItemsFormController.call(this);
            this.loadFinishMakes();
            this.loadFinishModels();
            this.loadStores();
            this.init();
        };

        Model.prototype = Object.create(BinderItemsFormController.prototype);

        angular.extend(Model.prototype, {

            populate: function() {
                // if a item was passed in store the values
                if (this.item) {
                    this.name = this.item.name;
                    this.make = this.item.make;
                    this.model = this.item.model;
                    this.style_color = this.item.style_color;
                    if (this.item.purchase) {
                        this.store = this.item.purchase.store;
                        this.price = this.item.purchase.number;
                        this.purchase_date = this.item.purchase.date;
                    }
                    this.details = this.item.details;
                    this.tags = this.item.tags;
                    this.setUploadParams();
                }
            },

            loadFinishMakes: function() {
                api.finishMake.all().then(
                    angular.bind(this, this.onLoadFinishMakes),
                    angular.bind(this, this.onLoadFinishMakesError)
                );
            },

            onLoadFinishMakes: function(response) {
                this.finishMakes = response.data;
            },

            onLoadFinishMakesError: function(response) {
                this.$log.error(response);
            },

            loadFinishModels: function() {
                api.finishModel.all().then(
                    angular.bind(this, this.onLoadFinishModels),
                    angular.bind(this, this.onLoadFinishModelsError)
                );
            },

            onLoadFinishModels: function(response) {
                this.finishModels = response.data;
            },

            onLoadFinishModelsError: function(response) {
                this.$log.error(response);
            },

            loadStores: function() {
                api.store.all().then(
                    angular.bind(this, this.onLoadStores),
                    angular.bind(this, this.onLoadStoresError)
                );
            },

            onLoadStores: function(response) {
                this.stores = response.data.map(function(store){
                    return store.name;
                });
            },

            onLoadStoresError: function(response) {
                this.$log.error(response);
            },

            setPayload: function() {
                // create payload
                return {
                    binder_id: this.binderId,
                    name: this.name,
                    make: this.make,
                    model: this.model,
                    style_color: this.style_color,
                    purchase_attributes: {
                        store: this.store,
                        price: this.price ? this.price : 0,
                        date: this.purchase_date
                    },
                    details: this.details,
                    tags_attributes: this.tags
                };
            },

            setUploadParams: function() {
                this.uploadParams = {
                    binder_id: this.item.binder_id,
                    finish_id: this.item.id
                };
            }
        });

        this.model = new Model();
    }

})();