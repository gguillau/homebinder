describe("FinishFormController", function() {
    var $rootScope,
        $controller,
        $stateParams,
        resources,
        ctrl,
        BinderItemsIndexController,
        api,
        $q,
        $log;


    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q_.defer();
                    defer.resolve({ permissions: { can_read: true } });
                    return defer.promise;
                }
            };
        });
    }));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $stateParams = _$injector_.get("$stateParams");
        BinderItemsIndexController = _$injector_.get("BinderItemsIndexController");
        resources = _$injector_.get("hb.resources");
        BinderRecallModal = _$injector_.get("BinderRecallModal");
        $log = _$injector_.get("$log");
    }));

    function createController() {
        ctrl = $controller("FinishFormController", {
            "BinderItemsIndexController": BinderItemsIndexController,
            "hb.resources": resources,
            "hb.api": api,
            "$log": $log,
            "$stateParams": $stateParams
        });
        $rootScope.$apply();
    }

    beforeEach(function() {
        spyOn(api.finishMake, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(api.finishModel, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(api.store, "all").and.returnValue($q.when({ data: [] }));
    });

    describe('#init', function() {
        it("calls finishMake.all and finishModel.all", function() {
            $stateParams.binderId = 99;
            createController();

            expect(api.finishMake.all).toHaveBeenCalled();
            expect(api.finishModel.all).toHaveBeenCalled();
            expect(api.store.all).toHaveBeenCalled();
        });

        it("sets the state data when editing an existing item", function() {
            $stateParams = {
                binderId: 1,
                id: 1
            };
            createController();

            expect(ctrl.model.indexState).toEqual("binder.finishesDetails");
            expect(ctrl.model.indexParams).toEqual({ binderId: 1, id: 1 });
            expect(ctrl.model.editState).toEqual("binder.finishesEdit");
        });
        
        it("sets the state data when creating a new item", function() {
            $stateParams = {
                binderId: 1
            };
            createController();

            expect(ctrl.model.indexState).toEqual("binder.finishes");
            expect(ctrl.model.indexParams).toEqual({ binderId: 1 });
            expect(ctrl.model.editState).toEqual("binder.finishesEdit");
        });
    });

    describe('#populate', function() {
        it("doesnt set the item", function() {
            createController();
            ctrl.model.populate();

            expect(ctrl.model.item).toEqual({});
        });

        it("sets the item", function() {
            createController();
            ctrl.model.cfg = { item: { id: 1, binder_id: 1, name: "test", purchase: {} } };
            ctrl.model.item = ctrl.model.cfg.item;
            ctrl.model.populate();

            expect(ctrl.model.name).toEqual(ctrl.model.cfg.item.name);
        });
    });

    describe('#onLoadFinishMakes', function() {
        it("sets the finishMakes", function() {

            createController();
            ctrl.model.onLoadFinishMakes({ data: [{ id: 1 }] });

            expect(ctrl.model.finishMakes.length).toEqual(1);
        });
    });

    describe('#onLoadFinishMakesError', function() {
        it("calls $log error", function() {
            spyOn($log, "error");

            createController();
            ctrl.model.onLoadFinishMakesError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
        });
    });

    describe('#onLoadFinishModels', function() {
        it("sets the finishModels", function() {

            createController();
            ctrl.model.onLoadFinishModels({ data: [{ id: 1 }] });

            expect(ctrl.model.finishModels.length).toEqual(1);
        });
    });

    describe('#onLoadFinishModelsError', function() {
        it("calls $log error", function() {
            spyOn($log, "error");

            createController();
            ctrl.model.onLoadFinishModelsError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
        });
    });

    describe('#onLoadStores', function() {
        it("sets the stores", function() {

            createController();
            ctrl.model.onLoadStores({ data: [{ id: 1 }] });

            expect(ctrl.model.stores.length).toEqual(1);
        });
    });

    describe('#onLoadStoresError', function() {
        it("calls $log error", function() {
            spyOn($log, "error");

            createController();
            ctrl.model.onLoadStoresError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
        });
    });

    describe('#setPayload', function() {
        it("returns the payload", function() {
            createController();
            ctrl.model.name = "test";
            var data = ctrl.model.setPayload();

            expect(data.name).toEqual("test");
        });
    });

});

describe('hbFinishForm', function() {
    var $scope, $compile, api, $q, context, element, session;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        session = _$injector_.get("Session");
        context = _$injector_.get("Context");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.finishMake, "all").and.returnValue($q.when({ data: [] }));
        spyOn(api.finishModel, "all").and.returnValue($q.when({ data: [] }));
        spyOn(api.store, "all").and.returnValue($q.when({ data: [] }));
        spyOn(context, "getBinder").and.returnValue({ data: { permissions: { can_read: false }, property: {} } });
        spyOn(session, "getUser").and.returnValue({ id: 1, role: "admin" });

        $scope.cfg = { binderId: 1, item: null };
        $scope.onSaved = function() {};
        $scope.onCancel = function() {};

        element = $compile('<hb-finish-form cfg="cfg" on-saved="onSaved" on-cancel="onCancel"></hb-finish-form>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {
            expect(element.html()).toContain("Molding");
        });
    });
});