(function() {
	"use strict";

	angular
		.module("hb.homebinders.finishes.details", [])
		.directive("hbFinishDetails", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/finishes/details/details.tpl.html",
				controller: "FinishDetailsController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("FinishDetailsController", [
			"hb.resources",
			"BinderItemsDetailsController",
			"hb.api",
			"$stateParams",
			Controller
		]);

	function Controller(resources, BinderItemsDetailsController, api, $stateParams) {
		var that = this;
		var Model = function() {
			this.cfg = that.cfg;
			this.onDestroyed = that.onDestroyed;
			this.onClose = that.onClose;
			this.resources = angular.extend({}, this.resources, resources.binderFinishesDetails);
			this.nameProperty = "name";
			this.deleteCall = api.finish.destroy;
			this.refreshCall = api.finish.get;
			this.indexState = "binder.finishes";
			this.indexParams = { binderId: $stateParams.binderId };
			this.editState = "binder.finishesEdit";
			// call the parent class
			BinderItemsDetailsController.call(this);
		};

		Model.prototype = Object.create(BinderItemsDetailsController.prototype);

		angular.extend(Model.prototype, {
			populate: function() {
				this.editParams = { binderId: $stateParams.binderId, id: this.item.id };
				this.tagId = "finish_" + this.item.id;
				this.uploadParams = {
					binder_id: this.item.binder_id,
					finish_id: this.item.id
				};
			}
		});

		this.model = new Model();
	}
})();