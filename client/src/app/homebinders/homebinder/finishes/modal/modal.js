(function() {
    angular
        .module("hb.homebinders.finishes.modal", [])
        .controller("FinishModalController", [
            "hb.resources",
            "hb.api",
            "BinderItemsModalBaseController",
            "$modalInstance",
            "args",
            Controller
        ]);

    function Controller(resources, api, BinderItemsModalBaseController, $modalInstance, args) {
        var Model = function() {
            this.refreshCall = api.finish.get;
            this.resources = resources.binderFinishesModal;
            this.$modalInstance = $modalInstance;
            this.args = args;
            this.nameProperty = "name";
            BinderItemsModalBaseController.call(this);

        };

        Model.prototype = Object.create(BinderItemsModalBaseController.prototype);

        this.model = new Model();
    }
})();