angular
    .module("hb.homebinders.finishes.routes", [])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            // finish index state
            .state("binder.finishes", {
                url: "^/binders/:binderId/finishes",
                template: "<binder-finishes></binder-finishes>",
                access: {
                    requiresLogin: true
                }
            })
            // finish details state
            .state("binder.finishesDetails", {
                url: "^/binders/:binderId/finishes/{id:int}",
                template: "<hb-finish-details></hb-finish-details>",
                access: {
                    requiresLogin: true
                }
            })
            // state for new finish
            .state("binder.finishesNew", {
                url: "^/binders/:binderId/finishes/new",
                template: "<hb-finish-form></hb-finish-form>",
                access: {
                    requiresLogin: true
                }
            })
            // state for edit finish
            .state("binder.finishesEdit", {
                url: "^/binders/:binderId/finishes/{id:int}/edit",
                template: "<hb-finish-form></hb-finish-form>",
                access: {
                    requiresLogin: true
                }
            });
    }]);