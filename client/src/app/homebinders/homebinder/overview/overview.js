(function() {
    "use strict";

    angular
        .module("hb.homebinders.overview", [
            "ui.router",
            "hb.api",
            "hb.components"
        ])
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state("binder.overview", {
                    url: "^/binders/:binderId/overview",
                    templateUrl: "homebinders/homebinder/overview/overview.tpl.html",
                    controller: "BinderCtrl",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .controller("BinderCtrl", [
            "hb.api",
            "Session",
            "Notify",
            "HomeBinderFactory",
            "$state",
            "$stateParams",
            "Loading",
            "$log",
            "BinderRecallModal",
            "binder",
            "Context",
            "$q",
            "hb.resources",
            BinderCtrl
        ]);

    function BinderCtrl(api, Session, Notify, homebinderFactory, $state, $stateParams, loading, $log, PartnerRecallModal, binder, Context, $q, resources) {
        this.api = api;
        this.session = Session;
        this.notify = Notify;
        this.binderId = $stateParams.binderId;
        this.dashboard = null;
        this.widgets = [];
        this.selectedTab = "overview";
        this.busyMessage = null;
        this.homebinderFactory = homebinderFactory;
        this.loading = loading;
        this.$log = $log;
        this.recallModal = PartnerRecallModal;
        this.$state = $state;
        this.resources = resources.overview;
        this.homebinderFactory.setToolbar({
            buttons: [{
                id: "new",
                text: this.resources.edit,
                class: "btn btn-primary",
                show: true,
                icon: "glyphicon glyphicon-edit",
                click: angular.bind(this, this.editBinder)
            }]
        });
        this.widgetHostCfg = {
            previewMode: false,
            editMode: false,
            onRemoveWidget: angular.bind(this, this.onRemoveWidget)
        };
        this.homebinderFactory.setButtons(false);
        this.refresh();
    }

    angular.extend(BinderCtrl.prototype, {
        refresh: function() {
            this.api.dashboard.forBinder(this.binderId).then(
                angular.bind(this, this.gotDashboard),
                angular.bind(this, this.getDashboardError)
            );
        },
        gotDashboard: function(response) {
            this.dashboard = response.data;
            // returning null for some reason
            if (this.dashboard === null) {
                this.widgets = [];
            }
            else {
                this.widgets = this.dashboard.dashboard_widgets.sort(function(a, b) {
                    return (a.index > b.index) ? 1 : (a.index < b.index) ? -1 : 0;
                });
            }
        },
        getDashboardError: function(response) {
            this.notify.error(response.data);
            this.$log.error(response.data);
        },
        onRemoveWidget: function(id) {
            this.api.dashboardWidget.destroy(id);
        },
        editBinder: function() {
            this.$state.go("binder.edit", {
                binderId: this.binderId
            });
        }
    });
})();