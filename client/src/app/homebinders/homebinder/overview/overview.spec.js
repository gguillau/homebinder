describe("BinderCtrl", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $state,
        $stateParams,
        session,
        homeBinderFactory,
        notify,
        api,
        loading,
        $log,
        current_binder,
        binder;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");

        $log = {
            error: function(msg) {}
        };

        $stateParams = {
            binderId: 1
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        $state = {
            go: function(state, args) {}
        };

        session = {
            currentBinder: function() {}
        };

        homeBinderFactory = {
            setToolbar: function(tb) {},
            setButtons: function(bool) {}
        };

        notify = {
            success: function(msg) {},
            error: function(msg) {}
        };

        current_binder = {
            appliances: [],
            created_at: "2015-10-04T01:39:37Z",
            details: null,
            hero_image_id: 617,
            hero_photo: null,
            id: 1438,
            last_recall: null,
            name: "Test Home Binder",
            partner: null,
            permissions: null,
            primary: false,
            property: null,
            recalls: 0,
            role: {
                role: "user"
            },
            seller_report: null,
            subscription: null,
            vendor: null
        };

        binder = {
            data: current_binder,
            load_binder_successful: true
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("BinderCtrl", {
            "$state": $state,
            "$stateParams": $stateParams,
            "hb.api": api,
            "Session": session,
            "Notify": notify,
            "HomeBinderFactory": homeBinderFactory,
            "Loading": loading,
            "$log": $log,
            "binder": binder
        });

        $scope.ctrl = ctrl;
    }

    it("gets the dashboard", function() {
        spyOn(api.dashboard, "forBinder").and.returnValue($q.when({
            data: {
                id: 1,
                name: "my dashboard",
                dashboard_widgets: [
                    { id: 1, widget_id: 1, dashboard_id: 1, index: 1 },
                    { id: 2, widget_id: 2, dashboard_id: 1, index: 0 }
                ]
            }
        }));

        createController();
        $rootScope.$apply();

        expect(api.dashboard.forBinder).toHaveBeenCalledWith(1);
        expect(ctrl.dashboard).not.toBeNull();
        expect(ctrl.widgets.length).toEqual(2);
        expect(ctrl.widgets[0].id).toEqual(2);
        expect(ctrl.widgets[1].id).toEqual(1);
    });

    it("gets the dashboard and sets widgets to empty array", function() {
        spyOn(api.dashboard, "forBinder").and.returnValue($q.when({
            data: null
        }));

        createController();
        $rootScope.$apply();

        expect(api.dashboard.forBinder).toHaveBeenCalledWith(1);
        expect(ctrl.widgets.length).toEqual(0);
    });

    it("notifies the user of a failure getting the dashboard", function() {
        spyOn(api.dashboard, "forBinder").and.returnValue($q.reject({
            data: "error"
        }));
        spyOn(notify, "error");
        spyOn($log, "error");

        createController();
        $rootScope.$apply();

        expect(api.dashboard.forBinder).toHaveBeenCalledWith(1);
        expect(notify.error).toHaveBeenCalled();
        expect($log.error).toHaveBeenCalledWith("error");
    });

    it("calls api dashboardWidget destroy", function() {
        spyOn(api.dashboardWidget, "destroy");
        createController();
        ctrl.onRemoveWidget(1);

        expect(api.dashboardWidget.destroy).toHaveBeenCalledWith(1);
    });
    
    it("calls $state go", function() {
        spyOn($state, "go");
        createController();
        ctrl.editBinder();

        expect($state.go).toHaveBeenCalled();
    });
});