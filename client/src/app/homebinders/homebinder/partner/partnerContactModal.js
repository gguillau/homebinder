(function() {
    "use strict";

    angular
        .module("hb.homebinders.homebinder.partnerContact", [])
        .factory("PartnerContactModal", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(partner) {
                        Modals.show({
                            templateUrl: "homebinders/homebinder/partner/partnerContactModal.tpl.html",
                            controller: "PartnerContactModalController as ctrl",
                            backdrop: true,
                            resolveData: partner
                        });
                    }
                };
            }
        ])
        .controller("PartnerContactModalController", [
            "$modalInstance",
            "data",
            "hb.api",
            "$log",
            "hb.resources",
            PartnerContactModalController
        ]);

    function PartnerContactModalController($modal, data, api, $log, resources) {
        this.$modal = $modal;
        this.user = data.user;
        this.api = api;
        this.$log = $log;
        this.resources = resources.partnersAccount;
    }

    PartnerContactModalController.prototype = {

        cancel: function() {
            this.$modal.dismiss("cancel");
        }
    };

})();