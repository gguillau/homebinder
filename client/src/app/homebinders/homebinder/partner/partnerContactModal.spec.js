describe("PartnerContactModalController", function() {
    var $rootScope,
        $scope,
        $controller,
        $location,
        ctrl,
        data,
        api,
        $modalInstance,
        $log,
        PartnerContactModal,
        ModalService;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        PartnerContactModal = _$injector_.get("PartnerContactModal");
        ModalService = _$injector_.get("ModalService");

        $log = {
            error: function(msg) {}
        };

        $modalInstance = {
            dismiss: function(msg) {}
        };

        data = {
            address: null,
            coupons: [],
            tags: [],
            id: 100,
            name: "John Smith Inspection Services",
            partner_type: "inspector",
            code: "JohnFREEFORLIFE",
            contact: "",
            phone: "",
            email: "johnsmith@gmail.com",
            binder_logo_id: null,
            sellers_logo_id: null,
            binder_count: 0,
            vendors: [],
            created_at: "2015-09-21 19:09:37.484248",
            settings: {
                navigation: {
                    binders: true,
                    users: true,
                    vendors: false
                }
            }
        };

        api = {
            logo: {
                get: function(id) {}
            }
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("PartnerContactModalController", {
            "$location": $location,
            "$modalInstance": $modalInstance,
            "data": data,
            "hb.api": api,
            "$log": $log
        });

        $scope.ctrl = ctrl;
    }

    describe('ctrl.cancel', function() {

        it('should call $modalInstance dismiss function', function() {

            spyOn($modalInstance, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.cancel();
            $rootScope.$apply();

            expect($modalInstance.dismiss).toHaveBeenCalledWith('cancel');
        });
    });

    describe('PartnerContactModal', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            PartnerContactModal.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});