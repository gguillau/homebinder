describe("UpgradeFormController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $modalInstance,
        data,
        binder,
        context;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        context = _$injector_.get("Context");

        $modalInstance = {
            dismiss: function(button) {}
        };

        data = {
            upgradeText: "Upgrade text"
        };

        binder = {
            subscription: {
                id: 1,
                url: null
            }
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("UpgradeFormController", {
            "$modalInstance": $modalInstance,
            "data": data,
            "Context": context
        });

        $scope.ctrl = ctrl;

    }

    describe('ctrl.init', function() {
        it('calls context.getBinder() and sets binder', inject(function($q) {
            spyOn(context, "getBinder").and.returnValue($q.when(binder));

            createController();
            ctrl.init();
            $rootScope.$apply();
            
            expect(ctrl.upgradeText).toEqual("Upgrade text");
            expect(ctrl.binder).toEqual(binder);
            expect(ctrl.binder.subscription.url).toEqual("/settings/subscriptions/1/upgrade");
            expect(context.getBinder).toHaveBeenCalled();

        }));
    });
    
    describe('ctrl.close', function() {
        it('calls context.getBinder() and sets binder', inject(function($q) {
            spyOn($modalInstance, "dismiss");

            createController();
            ctrl.close();
            $rootScope.$apply();
            
            expect($modalInstance.dismiss).toHaveBeenCalled();

        }));
    });

});