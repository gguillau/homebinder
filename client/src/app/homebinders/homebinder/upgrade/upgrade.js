(function() {
	"use strict";

	angular
		.module("hb.homebinders.upgrade", [])
		.factory("UpgradeForm", [
			"ModalService",
			function(Modals) {
				return {
					show: function(opts) {
						Modals.show({
							templateUrl: "homebinders/homebinder/upgrade/upgrade.tpl.html",
							controller: "UpgradeFormController as ctrl",
							resolveData: opts,
							backdrop: true
						});
					}
				};
			}
		])
		.controller("UpgradeFormController", [
			"$modalInstance",
			"data",
			"Context",
			"hb.resources",
			UpgradeFormController
		]);

	function UpgradeFormController($modalInstance, opts, context, resources) {
		this.$modal = $modalInstance;
		this.upgradeText = opts.upgradeText;
		this.context = context;
		this.resources = resources.upgradeForm;
		this.init();
	}

	UpgradeFormController.prototype = {
		init: function() {
			this.context.getBinder().then(angular.bind(this, function(binder) {
				this.binder = binder;
				this.binder.subscription.url = "/settings/subscriptions/" + this.binder.subscription.id + "/upgrade";
			}));
		},

		close: function() {
			this.$modal.dismiss();
		}
	};

})();