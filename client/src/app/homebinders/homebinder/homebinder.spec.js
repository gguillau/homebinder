describe("HomeBinderController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $state,
        constants,
        HomeBinderFactory,
        modals,
        notify,
        $log,
        api,
        Session,
        transferModal,
        partnerContact,
        context,
        binder,
        current_binder,
        resources,
        BinderItemsVerify,
        HomeownerOnboard,
        appDelegate,
        Appcues;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        resources = _$injector_.get("hb.resources");
        BinderItemsVerify = _$injector_.get("BinderItemsVerify");
        HomeBinderFactory = _$injector_.get("HomeBinderFactory");
        api = _$injector_.get("hb.api");
        context = _$injector_.get("Context");
        $state = _$injector_.get("$state");
        notify = _$injector_.get("Notify");
        appDelegate = _$injector_.get("AppDelegate");

        current_binder = {
            appliances: null,
            created_at: "2015-09-06T04:29:36Z",
            id: 1,
            name: "This is a fake home",
            primary: null,
            details: "this is a home",
            last_recall: null,
            partner: null,
            vendor: null,
            recalls: 0,
            documents: [{
                file_size: 506986,
                id: 950,
                location: "https://homebinder.com",
                name: "document_name.pdf",
                seller_report_item: null,
                tags: []
            }],
            maintenance_items: null,
            binder_contractors: null,
            property: null,
            subscription: null,
            seller_report: null,
            permissions: {
                can_create: false,
                can_create_seller_report: false,
                can_destroy: false,
                can_edit_seller_report: false,
                can_read: false,
                can_share: false,
                can_subscribe: false,
                can_transfer: false,
                can_view_master_report: false,
                can_write: false
            }
        };

        binder = current_binder;

        constants = {
            ApiKey: "faksd029i92"
        };

        transferModal = {
            show: function() {}
        };

        $log = {
            error: function(msg) {}
        };

        Session = {
            currentBinder: function() {},
            getJwt: function() {},
            getUser: function() {},
            getBinder: function() {}
        };

        modals = {
            confirm: function(confirmOptions) {},
            preview: function(previewOptions) {},
            show: function(showOptions) {},
            upload: function(uploadOptions) {}
        };

        partnerContact = {
            show: function(partner) {}
        };

        Appcues = {
            populate: function(user, binderId) {}
        };

    }));

    function createController(another_binder) {
        binder = another_binder ? another_binder : current_binder;
        $scope = $rootScope.$new();
        ctrl = $controller("HomeBinderController", {
            "$state": $state,
            "hb.api": api,
            "constants": constants,
            "$log": $log,
            "Session": Session,
            "Notify": notify,
            "ModalService": modals,
            "HomeBinderFactory": HomeBinderFactory,
            "TransferModal": transferModal,
            "PartnerContactModal": partnerContact,
            "BinderItemsVerify": BinderItemsVerify,
            "binder": binder,
            "Context": context,
            "hb.resources": resources,
            "AppDelegate": appDelegate,
            "Appcues": Appcues,
            "$q": $q
        });
        $scope.ctrl = ctrl;

        spyOn(api.user, "get").and.returnValue($q.when({
            data: { id: 1, role: "agent" }
        }));

        // stub all binder items count calls
        spyOn(api.maintenanceItem, "all").and.returnValue($q.when({ data: 1 }));
        spyOn(api.appliance, "all").and.returnValue($q.when({ data: 2 }));
        spyOn(api.project, "all").and.returnValue($q.when({ data: 3 }));
        spyOn(api.binderContractor, "all").and.returnValue($q.when({ data: 4 }));
        spyOn(api.inventoryItem, "all").and.returnValue($q.when({ data: 5 }));
        spyOn(api.document, "all").and.returnValue($q.when({ data: 6 }));
        spyOn(api.image, "all").and.returnValue($q.when({ data: 7 }));
        spyOn(api.permit, "all").and.returnValue($q.when({ data: 8 }));
        spyOn(api.receipt, "all").and.returnValue($q.when({ data: 9 }));
        spyOn(api.area, "all").and.returnValue($q.when({ data: 10 }));
        spyOn(api.structure, "all").and.returnValue($q.when({ data: 11 }));
        spyOn(api.paint, "all").and.returnValue($q.when({ data: 12 }));
        spyOn(api.finish, "all").and.returnValue($q.when({ data: 13 }));
    }

    describe('ctrl.binderLoadError', function() {
        it('should calls notify error', function() {
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(binder));
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "agent" });

            createController();
            ctrl.binderLoadError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();

        });
    });

    describe('ctrl.setMobileButtons', function() {
        it('adds button to mobileTabs', function() {
            var spec_binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(spec_binder));
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "agent" });
            binder.load_binder_successful = false;
            createController();
            ctrl.showHeader = true;
            expect(ctrl.mobileTabs.length).toEqual(8);
            ctrl.setMobileButtons();

            expect(ctrl.mobileTabs.length).toEqual(9);
        });
    });

    describe('ctrl.getBinderItemsCounts', function() {
        it('makes all required api calls and sets the values', function() {
            var spec_binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };
            spyOn(Session, "currentBinder").and.returnValue($q.when(spec_binder));
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "agent" });
            createController();

            ctrl.getBinderItemsCounts();

            expect(api.maintenanceItem.all).toHaveBeenCalled();
            expect(api.appliance.all).toHaveBeenCalled();
            expect(api.project.all).toHaveBeenCalled();
            expect(api.binderContractor.all).toHaveBeenCalled();
            expect(api.inventoryItem.all).toHaveBeenCalled();
            expect(api.document.all).toHaveBeenCalled();
            expect(api.image.all).toHaveBeenCalled();
            expect(api.permit.all).toHaveBeenCalled();
            expect(api.receipt.all).toHaveBeenCalled();
            expect(api.area.all).toHaveBeenCalled();
            expect(api.structure.all).toHaveBeenCalled();
            expect(api.paint.all).toHaveBeenCalled();
            expect(api.finish.all).toHaveBeenCalled();
        });
    });

    describe('ctrl.binderItemsCountSuccess', function() {
        it('sets the tab counts', function() {
            var spec_binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };
            spyOn(Session, "currentBinder").and.returnValue($q.when(spec_binder));
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "agent" });
            createController();

            ctrl.maintenanceItemsCount = 1;
            ctrl.appliancesCount = 2;
            ctrl.projectsCount = 3;
            ctrl.homeProsCount = 4;
            ctrl.inventoriesCount = 5;
            ctrl.documentsCount = 6;
            ctrl.imagesCount = 7;
            ctrl.permitsCount = 8;
            ctrl.receiptsCount = 9;
            ctrl.roomsCount = 10;
            ctrl.structuresCount = 11;
            ctrl.paintsCount = 12;
            ctrl.finishesCount = 13;
            ctrl.binderItemsCountSuccess();

            expect(ctrl.tabs[1].count).toEqual(1);
            expect(ctrl.tabs[2].count).toEqual(2);
            expect(ctrl.tabs[3].count).toEqual(3);
            expect(ctrl.tabs[4].count).toEqual(4);
            expect(ctrl.tabs[5].count).toEqual(5);
            expect(ctrl.tabs[6].count).toEqual(30);
            expect(ctrl.tabs[7].count).toEqual(46);
        });
    });

    describe('ctrl.getUser', function() {
        it('should call api user get', function() {
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(binder));
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "homeowner" });

            createController();
            ctrl.currentUser = { id: 1, role: "homeowner" };
            ctrl.getUser();

            expect(api.user.get).toHaveBeenCalled();
        });
    });

    describe('ctrl.getUserSuccess', function() {
        it('should set ctrl.user and call Appcues.populate', function() {
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(binder));
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "homeowner" });
            spyOn(Appcues, "populate");

            createController();
            ctrl.$stateParams.binderId = 1;
            ctrl.currentUser = { id: 1, role: "homeowner" };
            ctrl.getUserSuccess({ data: { id: 1 } });

            expect(ctrl.user).toEqual({ id: 1 });
            expect(Appcues.populate).toHaveBeenCalledWith({ id: 1 }, 1);
        });
    });

    describe('ctrl.getUserError', function() {
        it('should call $log error', function() {
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(binder));
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "homeowner" });
            spyOn($log, "error");

            createController();
            ctrl.currentUser = { id: 1, role: "homeowner" };
            ctrl.getUserError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
        });
    });

    describe('ctrl.getBinders', function() {
        it('calls api.binder.all', function() {
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(binder));
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "homeowner" });
            spyOn(api.binder, "all").and.returnValue($q.when({}));

            createController();
            ctrl.currentUser = { id: 1, role: "homeowner" };
            ctrl.getBinders();

            expect(ctrl.showMyBinders).toEqual(true);
            expect(api.binder.all).toHaveBeenCalledWith({ searchMethod: "for_user", userId: 1 });

        });
    });

    describe('ctrl.getBindersSuccess', function() {
        it('sets this.binders', function() {
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(binder));
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "homeowner" });

            createController();
            ctrl.currentUser = { id: 1, role: "homeowner" };

            var response = {
                data: {
                    items: [{ id: 1 }]
                }
            };
            ctrl.getBindersSuccess(response);

            expect(ctrl.binders).toEqual([{ id: 1 }]);

        });
    });

    describe('ctrl.getBindersError', function() {
        it('logs the error', function() {
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(binder));
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "homeowner" });
            spyOn($log, "error");

            createController();
            ctrl.currentUser = { id: 1, role: "homeowner" };

            var response = { data: "error" };
            ctrl.getBindersError(response);

            expect($log.error).toHaveBeenCalledWith("error");

        });
    });

    describe('ctrl.selectBinder', function() {
        it('calls $state.go', function() {
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(binder));
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "homeowner" });
            spyOn($state, "go");

            createController();
            ctrl.currentUser = { id: 1, role: "homeowner" };
            ctrl.selectBinder(binder);

            expect($state.go).toHaveBeenCalledWith("binder.overview", { binderId: 1 });

        });
    });

    describe('ctrl.backToDashboard', function() {
        it('calls $state.go for a partner', function() {
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(binder));
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "homeowner" });
            spyOn($state, "go");

            createController();
            ctrl.currentUser = { id: 2, role: "partner", partner_id: 1 };
            ctrl.backToDashboard();

            expect($state.go).toHaveBeenCalledWith("partner.binders", { partnerId: 1 });

        });

        it('calls $state.go for an inspector', function() {
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(binder));
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "homeowner" });
            spyOn($state, "go");

            createController();
            ctrl.currentUser = { id: 2, role: "inspector", partner_id: 1 };
            ctrl.backToDashboard();

            expect($state.go).toHaveBeenCalledWith("partner.binders", { partnerId: 1 });

        });

        it('calls $state.go for an hoa', function() {
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(binder));
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "homeowner" });
            spyOn($state, "go");

            createController();
            ctrl.currentUser = { id: 2, role: "hoa", partner_id: 1 };
            ctrl.backToDashboard();

            expect($state.go).toHaveBeenCalledWith("partner.binders", { partnerId: 1 });

        });

        it('calls $state.go for an lender', function() {
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(binder));
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "homeowner" });
            spyOn($state, "go");

            createController();
            ctrl.currentUser = { id: 2, role: "lender", partner_id: 1 };
            ctrl.backToDashboard();

            expect($state.go).toHaveBeenCalledWith("partner.binders", { partnerId: 1 });

        });

        it('calls $state.go for an homepro', function() {
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(binder));
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "homeowner" });
            spyOn($state, "go");

            createController();
            ctrl.currentUser = { id: 2, role: "homepro", partner_id: 1 };
            ctrl.backToDashboard();

            expect($state.go).toHaveBeenCalledWith("partner.binders", { partnerId: 1 });

        });

        it('calls $state.go for a broker', function() {
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(binder));
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "homeowner" });
            spyOn($state, "go");

            createController();
            ctrl.currentUser = { id: 2, role: "broker", partner_id: 1 };
            ctrl.backToDashboard();

            expect($state.go).toHaveBeenCalledWith("partner.binders", { partnerId: 1 });

        });

        it('calls $state.go for an agent', function() {
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(binder));
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "agent" });
            spyOn($state, "go");

            createController();
            ctrl.currentUser = { id: 2, role: "agent", partner_id: 1 };
            ctrl.backToDashboard();

            expect($state.go).toHaveBeenCalledWith("agent.binders", { userId: 2 });

        });

        it('calls $state.go for an admin', function() {
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(binder));
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "homeowner" });
            spyOn($state, "go");

            createController();
            ctrl.currentUser = { id: 2, role: "admin" };
            ctrl.backToDashboard();

            expect($state.go).toHaveBeenCalledWith("admin.binders");

        });
    });

    describe('ctrl.goToSubscription', function() {
        it('calls $state.go', function() {
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(binder));
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "homeowner" });
            spyOn($state, "go");

            createController();
            ctrl.currentUser = { id: 1, role: "homeowner" };
            ctrl.subscription = {
                id: 1
            };
            ctrl.goToSubscription();

            expect($state.go).toHaveBeenCalledWith("binder.userSettingsUpgrade", { subscriptionId: 1 });

        });
    });

    describe('ctrl.verify', function() {
        it('calls BinderItemsVerify show', function() {

            current_binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "agent" });
            spyOn(BinderItemsVerify, "show");

            createController(current_binder);
            ctrl.verify();

            expect(BinderItemsVerify.show).toHaveBeenCalled();

        });
    });

    describe("ctrl.logout", function() {
        it("calls appDelegate logOff", function() {
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(binder));
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "homeowner" });
            spyOn(appDelegate, "logOff");

            createController();
            ctrl.currentUser = { id: 1, role: "homeowner" };
            ctrl.logout();

            expect(appDelegate.logOff).toHaveBeenCalled();
        });
    });

    describe('HomeBinderFactory', function() {
        it('returns empty hash', function() {
            var toolbar = HomeBinderFactory.toolbar();
            expect(toolbar).toEqual({});
        });

        it('returns true', function() {
            HomeBinderFactory.setButtons(true);
            var result = HomeBinderFactory.hideButtons(true);
            expect(result).toBe(true);
        });

        it('calls the callback', function() {
            var object = {
                callback: function() {}
            };
            spyOn(object, "callback");

            HomeBinderFactory.setCallback(object.callback);
            HomeBinderFactory.setToolbar({});

            expect(object.callback).toHaveBeenCalled();
        });
    });

});
