describe("DocumentsCtrl", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        session,
        modal,
        homebinderFactory,
        $window,
        notify,
        api,
        loading,
        $log,
        context,
        binder,
        $stateParams;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        $stateParams = _$injector_.get("$stateParams");
        loading = _$injector_.get("Loading");
        $log = _$injector_.get("$log");
        api = _$injector_.get("hb.api");
        notify = _$injector_.get("Notify");
        modal = _$injector_.get("ModalService");
        session = _$injector_.get("Session");
        context = _$injector_.get("Context");
        $window = _$injector_.get("$window");

        binder = {
            id: 100,
            permissions: {
                can_read: true
            }
        };
    }));

    function createController() {
        $stateParams.binderId = 100;
        $scope = $rootScope.$new();
        ctrl = $controller("HomeBinderDocumentsController", {
            "$scope": $scope,
            "hb.api": api,
            "$q": $q,
            "$window": $window,
            "Session": session,
            "Notify": notify,
            "HomeBinderFactory": homebinderFactory,
            "ModalService": modal,
            "Loading": loading,
            "$log": $log,
            "Context": context,
            "$stateParams": $stateParams
        });
        $scope.ctrl = ctrl;

    }

    describe('ctrl.model.onRefreshSuccess', function() {
        it("should load all documents for binder/resource", function() {
            spyOn(context, "getBinder").and.returnValue($q.when(binder));
            var documents = [{
                binder: null,
                file_size: 439531,
                id: 562,
                location: "https://somerandomlink.com/documents.doc",
                name: "fakedoc1.doc",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null
            }, {
                binder: null,
                file_size: 439531,
                id: 563,
                location: "https://somerandomlink.com/documents.doc",
                name: "fakedoc2.doc",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null
            }];

            createController();
            ctrl.model.onRefreshSuccess({ data: { items: documents } });

            expect(ctrl.model.items.length).toBe(2);
            expect(ctrl.model.items[0].name).toBe("fakedoc1");
            expect(ctrl.model.items[1].name).toBe("fakedoc2");
        });

        it("should load 0 documents for binder/resource", function() {
            spyOn(context, "getBinder").and.returnValue($q.when(binder));

            createController();
            ctrl.model.onRefreshSuccess({ data: { items: [] } });

            expect(ctrl.model.items.length).toBe(0);
        });
    });

    describe('ctrl.model.setDocumentName', function() {
        it("set name to empty string", function() {
            spyOn(context, "getBinder").and.returnValue($q.when(binder));
            var documents = [{
                binder: null,
                file_size: 439531,
                id: 562,
                location: "https://somerandomlink.com/documents.doc",
                name: "fakedoc.doc",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null
            }, {
                binder: null,
                file_size: 439531,
                id: 563,
                location: "https://somerandomlink.com/documents.doc",
                name: "fakedoc.doc",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null
            }];

            spyOn(api.document, "all").and.returnValue($q.when({
                data: { items: documents }
            }));

            var user = {
                email: "somerandomuseremail@gmail.com",
                role: "homeowner",
                user_token: "abunchofrandomletters"
            };

            spyOn(session, "getUser").and.returnValue($q.when(user));

            var doc = {
                binder: null,
                file_size: 439531,
                id: 562,
                location: "https://somerandomlink.com/fake.doc",
                name: null,
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null
            };

            createController();
            $rootScope.$apply();

            var name = ctrl.model.setDocumentName(doc);

            expect(name).toBe("");
        });

        it("set name to empty string", function() {
            spyOn(context, "getBinder").and.returnValue($q.when(binder));
            var documents = [{
                binder: null,
                file_size: 439531,
                id: 562,
                location: "https://somerandomlink.com/documents.doc",
                name: "fakedoc.doc",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null
            }, {
                binder: null,
                file_size: 439531,
                id: 563,
                location: "https://somerandomlink.com/documents.doc",
                name: "fakedoc.doc",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null
            }];

            spyOn(api.document, "all").and.returnValue($q.when({
                data: { items: documents }
            }));

            var user = {
                email: "somerandomuseremail@gmail.com",
                role: "homeowner",
                user_token: "abunchofrandomletters"
            };

            spyOn(session, "getUser").and.returnValue($q.when(user));

            var doc = {
                binder: null,
                file_size: 439531,
                id: 562,
                location: "https://somerandomlink.com/fake.doc",
                name: undefined,
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null
            };

            createController();
            $rootScope.$apply();

            var name = ctrl.model.setDocumentName(doc);

            expect(name).toBe("");
        });

        it("set name to empty string", function() {
            spyOn(context, "getBinder").and.returnValue($q.when(binder));
            var documents = [{
                binder: null,
                file_size: 439531,
                id: 562,
                location: "https://somerandomlink.com/documents.doc",
                name: "fakedoc.doc",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null
            }, {
                binder: null,
                file_size: 439531,
                id: 563,
                location: "https://somerandomlink.com/documents.doc",
                name: "fakedoc.doc",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null
            }];

            spyOn(api.document, "all").and.returnValue($q.when({
                data: { items: documents }
            }));

            var user = {
                email: "somerandomuseremail@gmail.com",
                role: "homeowner",
                user_token: "abunchofrandomletters"
            };

            spyOn(session, "getUser").and.returnValue($q.when(user));

            var doc = {
                binder: null,
                file_size: 439531,
                id: 562,
                location: "https://somerandomlink.com/fake.doc",
                name: "fake.doc",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null
            };

            createController();
            $rootScope.$apply();

            var name = ctrl.model.setDocumentName(doc);

            expect(name).toBe("fake");
        });

    });

    describe('ctrl.model.uploadDocuments', function() {
        it("opens the file window", function() {
            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1,
                subscription: {
                    plan: "paid"
                },
                permissions: {
                    can_read: true
                }
            }));

            spyOn(api.document, "all").and.returnValue($q.reject({
                data: "error"
            }));

            createController();
            ctrl.model.uploadDocuments();
        });
    });

    describe('ctrl.model.viewDocument', function() {
        it("calls $window open", function() {
            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1,
                subscription: {
                    plan: "paid"
                }
            }));

            spyOn(session, "getUser").and.returnValue($q.when({ id: 1 }));

            spyOn(api.document, "all").and.returnValue($q.reject({
                data: "error"
            }));

            spyOn($window, "open");

            createController();
            ctrl.model.viewDocument({ accessible: true });

            expect($window.open).toHaveBeenCalled();
        });
    });

    describe('ctrl.model.getIcon', function() {
        it("returns file", function() {
            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1
            }));

            spyOn(api.document, "all").and.returnValue($q.reject({
                data: "error"
            }));

            createController();
            var icon = ctrl.model.getIcon();

            expect(icon).toEqual("fa fa-file");
        });

        it("returns file", function() {
            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1
            }));

            spyOn(api.document, "all").and.returnValue($q.reject({
                data: "error"
            }));

            createController();
            var icon = ctrl.model.getIcon("name.local");

            expect(icon).toEqual("fa fa-file");
        });

        it("returns pdf", function() {
            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1
            }));

            spyOn(api.document, "all").and.returnValue($q.reject({
                data: "error"
            }));

            createController();
            var icon = ctrl.model.getIcon("name.pdf");

            expect(icon).toEqual("fa fa-file-pdf-o");
        });

        it("returns word", function() {
            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1
            }));

            spyOn(api.document, "all").and.returnValue($q.reject({
                data: "error"
            }));

            createController();
            var icon = ctrl.model.getIcon("name.doc");

            expect(icon).toEqual("fa fa-file-word-o");
        });
    });

    describe('ctrl.model.edit', function() {
        it("sets read_only to true", function() {
            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1
            }));

            spyOn(api.document, "all").and.returnValue($q.reject({
                data: "error"
            }));

            createController();
            var doc = { read_only: false };
            ctrl.model.edit(doc);

            expect(doc.read_only).toBe(true);
        });
    });

    describe('ctrl.model.fileAdded', function() {
        it("calls startUpload", function() {
            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1
            }));

            spyOn(api.document, "all").and.returnValue($q.reject({
                data: "error"
            }));

            createController();
            spyOn(ctrl.model, "startUpload");
            ctrl.model.fileAdded();

            expect(ctrl.model.startUpload).toHaveBeenCalled();
        });
    });

    describe('ctrl.model.startUpload', function() {
        it("calls uploadFiles", function() {
            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1,
                permissions: {
                    can_read: false
                }
            }));

            spyOn(api.document, "all").and.returnValue($q.reject({
                data: "error"
            }));

            createController();
            ctrl.model.uploadCfg = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            spyOn(ctrl.model.uploadCfg.api, "pendingUploads").and.returnValue([{ id: 1 }]);
            spyOn(ctrl.model.uploadCfg.api, "updateParams");
            spyOn(ctrl.model.uploadCfg.api, "uploadFiles");
            ctrl.model.startUpload();
            $rootScope.$apply();
            
            expect(ctrl.model.uploadCfg.api.pendingUploads).toHaveBeenCalled();
            expect(ctrl.model.uploadCfg.api.updateParams).toHaveBeenCalled();
            expect(ctrl.model.uploadCfg.api.uploadFiles).toHaveBeenCalled();
        });

        it("calls uploadFiles", function() {
            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1,
                permissions: {
                    can_read: false
                }
            }));

            spyOn(api.document, "all").and.returnValue($q.reject({
                data: "error"
            }));

            createController();
            ctrl.model.uploadCfg = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            spyOn(ctrl.model.uploadCfg.api, "pendingUploads").and.returnValue([]);
            spyOn(ctrl.model.uploadCfg.api, "updateParams");
            spyOn(ctrl.model.uploadCfg.api, "uploadFiles");
            ctrl.model.startUpload();

            expect(ctrl.model.uploadCfg.api.pendingUploads).toHaveBeenCalled();
            expect(ctrl.model.uploadCfg.api.updateParams).not.toHaveBeenCalled();
            expect(ctrl.model.uploadCfg.api.uploadFiles).not.toHaveBeenCalled();
        });
    });

    describe('ctrl.model.onFileUploadedSuccess', function() {
        it("should add new doc to array", function() {
            spyOn(context, "getBinder").and.returnValue($q.when(binder));

            spyOn(api.document, "all").and.returnValue($q.when({
                data: { items: [] }
            }));
            var file = {
                binder: null,
                file_size: 439531,
                id: 564,
                location: "https://somerandomlink.com/fake.doc",
                name: "fake.doc",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null
            };
            createController();
            ctrl.model.binder = {
                permissions: {
                    can_read: true
                }
            };
            ctrl.model.uploadCfg = {
                api: {
                    pendingUploads: function() { return []; }
                }
            };
            ctrl.model.onFileUploadedSuccess(file);
            expect(ctrl.model.items.length).toEqual(1);
        });

        it("calls startUpload", function() {
            spyOn(context, "getBinder").and.returnValue($q.when(binder));

            spyOn(api.document, "all").and.returnValue($q.when({
                data: { items: [] }
            }));
            var file = {
                binder: null,
                file_size: 439531,
                id: 564,
                location: "https://somerandomlink.com/fake.doc",
                name: "fake.doc",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null
            };
            createController();
            ctrl.model.binder = {
                permissions: {
                    can_read: true
                }
            };
            ctrl.model.uploadCfg = {
                api: {
                    pendingUploads: function() { return [{}]; }
                }
            };
            spyOn(ctrl.model, "startUpload");
            ctrl.model.onFileUploadedSuccess(file);

            expect(ctrl.model.items.length).toEqual(1);
            expect(ctrl.model.startUpload).toHaveBeenCalled();
        });
    });

    describe('ctrl.model.onFileUploadError', function() {
        it("should call notify and log error functions", function() {
            spyOn(context, "getBinder").and.returnValue($q.when(binder));
            var documents = [{
                binder: null,
                file_size: 439531,
                id: 562,
                location: "https://somerandomlink.com/documents.doc",
                name: "fakedoc.doc",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null
            }, {
                binder: null,
                file_size: 439531,
                id: 563,
                location: "https://somerandomlink.com/documents.doc",
                name: "fakedoc.doc",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null
            }];

            spyOn(api.document, "all").and.returnValue($q.when({
                data: { items: documents }
            }));

            var user = {
                email: "somerandomuseremail@gmail.com",
                role: "homeowner",
                user_token: "abunchofrandomletters"
            };

            spyOn(session, "getUser").and.returnValue($q.when(user));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            $rootScope.$apply();
            ctrl.model.onFileUploadError({
                data: "error"
            });
            $rootScope.$apply();
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");

        });
    });

    describe('ctrl.model.update', function() {
        it("calls doc update", function() {
            spyOn(context, "getBinder").and.returnValue($q.when(binder));

            spyOn(api.document, "all").and.returnValue($q.when({
                data: { items: [] }
            }));

            spyOn(api.document, "update").and.returnValue($q.when({
                data: {}
            }));

            var user = {
                email: "somerandomuseremail@gmail.com",
                role: "homeowner",
                user_token: "abunchofrandomletters"
            };

            spyOn(session, "getUser").and.returnValue($q.when(user));

            var doc = {
                binder: null,
                file_size: 439531,
                id: 562,
                location: "https://somerandomlink.com/fake.doc",
                name: undefined,
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null
            };

            createController();
            $rootScope.$apply();

            ctrl.model.update(doc);

            expect(api.document.update).toHaveBeenCalled();
        });
    });

    describe('ctrl.model.onUpdateSuccess', function() {
        it("calls doc update", function() {
            spyOn(context, "getBinder").and.returnValue($q.when(binder));

            spyOn(api.document, "all").and.returnValue($q.when({
                data: { items: [] }
            }));

            var user = {
                email: "somerandomuseremail@gmail.com",
                role: "homeowner",
                user_token: "abunchofrandomletters"
            };

            spyOn(session, "getUser").and.returnValue($q.when(user));

            var doc = {
                binder: null,
                file_size: 439531,
                id: 562,
                location: "https://somerandomlink.com/fake.doc",
                name: "updated",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null
            };

            createController();
            $rootScope.$apply();
            ctrl.model.items = [{ id: 1, name: 'test' }];
            ctrl.model.onUpdateSuccess(ctrl.model.items[0], { data: doc });

            expect(ctrl.model.items[0].name).toEqual("updated");
        });
    });

    describe('ctrl.model.onUpdateError', function() {
        it("should call notify and log error functions", function() {
            spyOn(context, "getBinder").and.returnValue($q.when(binder));
            var documents = [{
                binder: null,
                file_size: 439531,
                id: 562,
                location: "https://somerandomlink.com/documents.doc",
                name: "fakedoc.doc",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null
            }, {
                binder: null,
                file_size: 439531,
                id: 563,
                location: "https://somerandomlink.com/documents.doc",
                name: "fakedoc.doc",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null
            }];

            spyOn(api.document, "all").and.returnValue($q.when({
                data: { items: documents }
            }));

            var user = {
                email: "somerandomuseremail@gmail.com",
                role: "homeowner",
                user_token: "abunchofrandomletters"
            };

            spyOn(session, "getUser").and.returnValue($q.when(user));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            $rootScope.$apply();
            ctrl.model.onUpdateError({
                data: "error"
            });

            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");

        });
    });

});