(function() {
    "use strict";

    angular
        .module("hb.homebinders.documents", [
            "ui.router",
            "ui.bootstrap",
            "hb.api",
            "hb.components"
        ])
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state("binder.documents", {
                    url: "^/binders/:binderId/documents",
                    templateUrl: "homebinders/homebinder/documents/documents.tpl.html",
                    controller: "HomeBinderDocumentsController as ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .factory("BinderDocumentsController", [
            "BinderItemsIndexController",
            "hb.resources",
            "HomeBinderFactory",
            "$window",
            "Context",
            "UpgradeForm",
            "Notify",
            "Loading",
            "Event",
            function(BinderItemsIndexController, resources, homebinderFactory, $window, Context, UpgradeForm, notify, loading, event) {
                var Model = function() {
                    // call the parent class
                    BinderItemsIndexController.call(this);
                    this.homebinderFactory = homebinderFactory;
                    this.homebinderFactory.setButtons(true);
                    this.resources = angular.extend({}, this.resources, resources.documents);
                    this.refreshCall = this.api.document.all;
                    this.deleteCall = this.api.document.destroy;
                    this.uploadCfg = {
                        id: "uploadDocuments",
                        fileTypes: "documents",
                        url: "/api/v1/documents",
                        jwt: this.session.getJwt(),
                        params: {
                            binder_id: this.binderId
                        },
                        multiSelect: true,
                        hide_upload: true,
                        showTable: true
                    };
                    this.itemType = "documents";
                    this.nameProperty = "name";
                    // doesn't exist
                    this.modalController = "DocumentsModalController";
                    this.uploadLimit = true;
                };

                Model.prototype = Object.create(BinderItemsIndexController.prototype);

                angular.extend(Model.prototype, {

                    onRefreshSuccess: function(response) {
                        this.items = response.data.items.map(angular.bind(this, function(doc) {
                            var document = {
                                id: doc.id,
                                name: this.setDocumentName(doc),
                                location: doc.location,
                                icon: this.getIcon(doc.file_file_name),
                                details: doc.details,
                                read_only: true,
                                accessible: doc.accessible,
                                file_size: doc.file_size
                            };
                            return document;
                        }));

                        this.total = response.data.total;
                        this.toolbarCfg.total = this.total;
                        this.toolbarCfg.button = {
                            title: this.resources.upload,
                            click: angular.bind(this, this.uploadDocuments)
                        };

                        this.loading.close();
                    },

                    setDocumentName: function(doc) {
                        if (!doc.name) {
                            return "";
                        }
                        else {
                            return doc.name.replace(/\.[^/.]+$/, "");
                        }
                    },

                    onFileUploadedSuccess: function(file) {
                        Context.getBinder().then(function(binder) {
                            binder.total_storage += file.file_size;
                            Context.setBinder(binder);
                        });
                        if (this.items.length < 1) {
                            this.homebinderFactory.setToolbar({
                                buttons: [{
                                    id: "new",
                                    text: this.resources.upload,
                                    class: "btn btn-primary",
                                    show: true,
                                    icon: "glyphicon glyphicon-plus",
                                    click: angular.bind(this, this.uploadDocuments)
                                }]
                            });
                        }

                        var document = {
                            id: file.id,
                            name: this.setDocumentName(file),
                            location: file.location,
                            icon: this.getIcon(file.file_file_name),
                            details: file.details,
                            read_only: true,
                            accessible: file.accessible,
                            file_size: file.file_size
                        };

                        this.onAdded(document);

                        if (this.uploadCfg.api.pendingUploads().length < 1) {
                            this.loading.close();
                        }
                        else {
                            this.startUpload();
                        }
                    },

                    onFileUploadError: function(response) {
                        this.$log.error(response);
                        this.notify.error(response.data);
                        this.startUpload();
                    },

                    viewDocument: function(doc) {
                        if (!doc.accessible) {
                            UpgradeForm.show({
                                upgradeText: "Please upgrade to get access to The HomeBinder New Homeowner Guide (140-Page eBook)"
                            });
                            return;
                        }
                        this.currentUser = this.session.getUser();
                        event.create({ event_name: "view_document", event_type: "engagement", user_id: this.currentUser.id, user_role: this.currentUser.role, user_created_at: this.currentUser.created_at, user_create_method: this.currentUser.create_method });
                        $window.open(doc.location);
                    },

                    uploadDocuments: function() {
                        Context.getBinder().then(function(binder) {
                            var number = Math.floor(Math.log(binder.total_storage) / Math.log(1024));
                            var bytes = binder.total_storage / Math.pow(1024, Math.floor(number));
                            if (binder.permissions.can_read && binder.subscription.plan === "free" && bytes > 10) {
                                UpgradeForm.show({
                                    upgradeText: "You have used all 10MB of storage in Renter edition. Please upgrade to increase your storage to 10GB."
                                });
                            }
                            else {
                                angular.element('#uploadDocuments').trigger('click');
                            }
                        });
                    },

                    fileAdded: function() {
                        this.startUpload();
                    },

                    startUpload: function() {
                        var uploadApi = this.uploadCfg.api;
                        if (uploadApi && uploadApi.pendingUploads().length > 0) {
                            var verified = true;
                            Context.getBinder().then(angular.bind(this, function(binder) {
                                if (!binder.permissions.can_read) {
                                    verified = false;
                                }
                                uploadApi.updateParams({
                                    binder_id: this.binderId,
                                    verified: verified
                                });
                                this.loading.show("Saving...");

                                uploadApi.uploadFiles();
                            }));
                        }
                        else {
                            this.loading.close();
                        }
                    },

                    getIcon: function(name) {
                        if (!name) {
                            return "fa fa-file";
                        }
                        var file_extension = name.substring(name.length - 3);

                        switch (file_extension) {
                            case "pdf":
                                return "fa fa-file-pdf-o";
                            case "doc":
                            case "ocx":
                                return "fa fa-file-word-o";
                            default:
                                return "fa fa-file";
                        }

                    },

                    edit: function(document) {
                        document.read_only = !document.read_only;
                    },

                    update: function(doc) {
                        this.api.document.update(doc.id, { name: doc.name }).then(
                            angular.bind(this, this.onUpdateSuccess, doc),
                            angular.bind(this, this.onUpdateError)
                        );
                    },

                    onUpdateSuccess: function(doc, response) {
                        var index = this.items.indexOf(doc);
                        this.items[index] = {
                            id: response.data.id,
                            name: this.setDocumentName(response.data),
                            location: response.data.location,
                            icon: this.getIcon(response.data.file_file_name),
                            details: response.data.details,
                            read_only: true,
                            accessible: response.data.accessible,
                            file_size: response.data.file_size
                        };
                    },

                    onUpdateError: function(response) {
                        this.$log.error(response);
                        this.notify.error(response.data);
                    },

                    // updates the view after a delete
                    onDeleteItemSuccess: function(item, response) {
                        Context.getBinder().then(function(binder) {
                            binder.total_storage -= item.file_size;
                            Context.setBinder(binder);
                        });

                        var index = this.items.indexOf(item);
                        this.items.splice(index, 1);
                        this.totalItems = this.toolbarCfg.total -= 1;
                        notify.success(this.resources.deleteSuccess);
                        loading.close();
                    }

                });

                return Model;

            }
        ])
        .controller("HomeBinderDocumentsController", [
            "BinderDocumentsController",
            HomeBinderDocumentsController
        ]);

    function HomeBinderDocumentsController(BinderDocumentsController) {
        var Model = function() {
            // call the parent class
            BinderDocumentsController.call(this);
            this.init();
            this.toolbarCfg.searchBox = false;
            this.toolbarCfg.subtitle = this.resources.subtitle;
        };

        Model.prototype = Object.create(BinderDocumentsController.prototype);

        this.model = new Model();
    }
})();