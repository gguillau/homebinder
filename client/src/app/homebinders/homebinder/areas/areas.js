(function() {
    "use strict";

    angular
        .module("hb.homebinders.areas", [
            "hb.homebinders.areas.routes",
            "hb.homebinders.areas.details",
            "hb.homebinders.areas.form",
            "hb.homebinders.areas.modal"
        ])
        .directive("binderAreas", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/areas/areas.tpl.html",
                controller: "BinderAreasController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("BinderAreasController", [
            "BinderItemsIndexController",
            "hb.resources",
            Controller
        ]);

    function Controller(BinderItemsIndexController, resources) {
        var Model = function() {
            // call the parent class
            BinderItemsIndexController.call(this);
            this.resources = angular.extend({}, this.resources, resources.binderAreasIndex);
            this.refreshCall = this.api.area.all;
            this.itemType = "areas";
            this.modalController = "AreaModalController";
            this.init();
            this.itemNewState = "binder.areasNew";
            this.itemDetailsState = "binder.areasDetails";
        };

        Model.prototype = Object.create(BinderItemsIndexController.prototype);

        this.model = new Model();
    }
})();