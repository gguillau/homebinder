(function() {
    "use strict";

    angular
        .module("hb.homebinders.areas.form", [])
        .directive("hbAreaForm", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/areas/form/form.tpl.html",
                controller: "AreaFormController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        // Model backing the area modal form
        .controller("AreaFormController", [
            "hb.resources",
            "BinderItemsFormController",
            "hb.api",
            "$stateParams",
            Controller
        ]);

    function Controller(resources, BinderItemsFormController, api, $stateParams) {
        var that = this;
        var Model = function() {
            this.cfg = that.cfg;
            this.onSaved = that.onSaved;
            this.onCancel = that.onCancel;
            this.resources = angular.extend({}, this.resources, resources.binderAreasForm);
            this.receiptTypes = [];
            this.taggerCfg = {
                areaTags: false
            };
            this.tagName = "area_";
            // call the parent class;
            this.saveCall = api.area.update;
            this.createCall = api.area.create;
            this.deleteCall = api.area.destroy;
            this.refreshCall = api.area.get;
            
            // If $stateParams has an id, we are editing an exisiting item and back should send us to that item's details
            if ($stateParams.id) {
                this.indexState = "binder.areasDetails";
                this.indexParams = { binderId: $stateParams.binderId, id: $stateParams.id };
            }
            // Else, we were creating a new item and back should send us to the list of items
            else {
                this.indexState = "binder.areas";
                this.indexParams = { binderId: $stateParams.binderId };
            }
            this.editState = "binder.areasEdit";
            
            BinderItemsFormController.call(this);
            this.areaTypes = [];
            this.loadAreaTypes();
            this.init();
        };

        Model.prototype = Object.create(BinderItemsFormController.prototype);

        angular.extend(Model.prototype, {

            populate: function() {
                // if a item was passed in store the values
                if (this.item && this.item.id) {
                    this.name = this.item.name;
                    this.areaType = this.item.area_type;
                    this.dimensions = this.item.dimensions;
                    this.details = this.item.details;
                    this.tags = this.item.tags;
                    this.setUploadParams();
                }
            },

            loadAreaTypes: function() {
                api.areaType.all().then(
                    angular.bind(this, this.onLoadAreaTypes),
                    angular.bind(this, this.onLoadAreaTypesError)
                );
            },

            onLoadAreaTypes: function(response) {
                this.areaTypes = response.data;
            },

            onLoadAreaTypesError: function(response) {
                this.$log.error(response);
            },

            setPayload: function() {
                // create payload
                return {
                    binder_id: this.binderId,
                    name: this.name,
                    area_type: this.areaType,
                    dimensions: this.dimensions,
                    details: this.details,
                    tags_attributes: this.tags
                };
            },

            setUploadParams: function() {
                this.uploadParams = {
                    binder_id: this.item.binder_id,
                    area_id: this.item.id
                };
            }
        });

        this.model = new Model();
    }

})();