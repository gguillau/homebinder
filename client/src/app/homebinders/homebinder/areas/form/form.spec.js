describe("AreaFormController", function() {
    var $rootScope,
        $controller,
        $stateParams,
        resources,
        ctrl,
        BinderItemsIndexController,
        api,
        $q,
        $log;


    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q_.defer();
                    defer.resolve({ permissions: { can_read: true } });
                    return defer.promise;
                }
            };
        });
    }));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $stateParams = _$injector_.get("$stateParams");
        BinderItemsIndexController = _$injector_.get("BinderItemsIndexController");
        resources = _$injector_.get("hb.resources");
        $log = _$injector_.get("$log");
    }));

    function createController() {
        ctrl = $controller("AreaFormController", {
            "BinderItemsIndexController": BinderItemsIndexController,
            "hb.resources": resources,
            "hb.api": api,
            "$log": $log,
            "$stateParams": $stateParams
        });
        $rootScope.$apply();
    }

    beforeEach(function() {
        spyOn(api.areaType, "all").and.returnValue($q.when({ data: { items: [] } }));
    });

    describe('#init', function() {
        it("calls areaType.all", function() {
            $stateParams.binderId = 99;
            createController();

            expect(api.areaType.all).toHaveBeenCalled();
        });

        it("sets the state data when editing an existing item", function() {
            $stateParams = {
                binderId: 1,
                id: 1
            };
            createController();

            expect(ctrl.model.indexState).toEqual("binder.areasDetails");
            expect(ctrl.model.indexParams).toEqual({ binderId: 1, id: 1 });
            expect(ctrl.model.editState).toEqual("binder.areasEdit");
        });
        
        it("sets the state data when creating a new item", function() {
            $stateParams = {
                binderId: 1
            };
            createController();

            expect(ctrl.model.indexState).toEqual("binder.areas");
            expect(ctrl.model.indexParams).toEqual({ binderId: 1 });
            expect(ctrl.model.editState).toEqual("binder.areasEdit");
        });
    });

    describe('#populate', function() {
        it("doesnt set the item", function() {
            createController();
            ctrl.model.populate();

            expect(ctrl.model.item).toEqual({});
        });

        it("sets the item", function() {
            createController();
            ctrl.model.cfg = { item: { id: 1, binder_id: 1, name: "test" } };
            ctrl.model.item = ctrl.model.cfg.item;
            ctrl.model.populate();

            expect(ctrl.model.name).toEqual(ctrl.model.cfg.item.name);
        });
    });

    describe('#onLoadAreaTypes', function() {
        it("sets the areaTypes", function() {

            createController();
            ctrl.model.onLoadAreaTypes({ data: [{ id: 1 }] });

            expect(ctrl.model.areaTypes.length).toEqual(1);
        });
    });

    describe('#onLoadAreaTypesError', function() {
        it("calls $log error", function() {
            spyOn($log, "error");

            createController();
            ctrl.model.onLoadAreaTypesError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
        });
    });

    describe('#setPayload', function() {
        it("returns the payload", function() {
            createController();
            ctrl.model.name = "test";
            var data = ctrl.model.setPayload();

            expect(data.name).toEqual("test");
        });
    });

});

describe('hbAreaForm', function() {
    var $scope, $compile, api, $q, context, element, session;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        session = _$injector_.get("Session");
        context = _$injector_.get("Context");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.areaType, "all").and.returnValue($q.when({ data: [] }));
        spyOn(context, "getBinder").and.returnValue({ data: { permissions: { can_read: false }, property: {} } });
        spyOn(session, "getUser").and.returnValue({ id: 1, role: "admin" });

        $scope.cfg = { binderId: 1, item: null };
        $scope.onSaved = function() {};
        $scope.onCancel = function() {};

        element = $compile('<hb-area-form cfg="cfg" on-saved="onSaved" on-cancel="onCancel"></hb-area-form>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {
            expect(element.html()).toContain("Master Bedroom");
        });
    });
});