(function() {
	"use strict";

	angular
		.module("hb.homebinders.areas.details", [])
		.directive("hbAreaDetails", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/areas/details/details.tpl.html",
				controller: "AreaDetailsController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("AreaDetailsController", [
			"hb.resources",
			"BinderItemsDetailsController",
			"hb.api",
			"$stateParams",
			Controller
		]);

	function Controller(resources, BinderItemsDetailsController, api, $stateParams) {
		var that = this;
		var Model = function() {
			this.cfg = that.cfg;
			this.onDestroyed = that.onDestroyed;
			this.onClose = that.onClose;
			this.resources = angular.extend({}, this.resources, resources.binderAreasDetails);
			this.nameProperty = "name";
			this.refreshCall = api.area.get;
			this.indexState = "binder.areas";
			this.indexParams = { binderId: $stateParams.binderId };
			this.editState = "binder.areasEdit";
			// call the parent class
			BinderItemsDetailsController.call(this);
		};

		Model.prototype = Object.create(BinderItemsDetailsController.prototype);

		angular.extend(Model.prototype, {
			populate: function() {
				this.editParams = { binderId: $stateParams.binderId, id: this.item.id };
				this.tagId = "area_" + this.item.id;
				this.uploadParams = {
					binder_id: this.item.binder_id,
					area_id: this.item.id
				};
			},
			
			deleteCall: function() {
				this.api.area.destroy(this.item.id).then(
					angular.bind(this, this.onDeleteItemSuccess)
                );
            },
                    
            onDeleteItemSuccess: function(response) {
                this.notify.success(this.resources.deleteSuccess);
                this.back();
            }
		});

		this.model = new Model();
	}
})();