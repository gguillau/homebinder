angular
    .module("hb.homebinders.areas.routes", [])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            // area index state
            .state("binder.areas", {
                url: "^/binders/:binderId/areas",
                template: "<binder-areas></binder-areas>",
                access: {
                    requiresLogin: true
                }
            })
            // area details state
            .state("binder.areasDetails", {
                url: "^/binders/:binderId/areas/{id:int}",
                template: "<hb-area-details></hb-area-details>",
                access: {
                    requiresLogin: true
                }
            })
            // state for new area
            .state("binder.areasNew", {
                url: "^/binders/:binderId/areas/new",
                template: "<hb-area-form></hb-area-form>",
                access: {
                    requiresLogin: true
                }
            })
            // state for edit area
            .state("binder.areasEdit", {
                url: "^/binders/:binderId/areas/{id:int}/edit",
                template: "<hb-area-form></hb-area-form>",
                access: {
                    requiresLogin: true
                }
            });
    }]);