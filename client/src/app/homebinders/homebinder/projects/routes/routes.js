angular
    .module("hb.homebinders.projects.routes", [])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            // project index state
            .state("binder.projects", {
                url: "^/binders/:binderId/projects",
                template: "<binder-projects></binder-projects>",
                access: {
                    requiresLogin: true
                }
            })
            // project details state
            .state("binder.projectsDetails", {
                url: "^/binders/:binderId/projects/{id:int}",
                template: "<hb-project-details></hb-project-details>",
                access: {
                    requiresLogin: true
                }
            })
            // state for new project
            .state("binder.projectsNew", {
                url: "^/binders/:binderId/projects/new",
                template: "<hb-project-form></hb-project-form>",
                access: {
                    requiresLogin: true
                }
            })
            // state for edit project
            .state("binder.projectsEdit", {
                url: "^/binders/:binderId/projects/{id:int}/edit",
                template: "<hb-project-form></hb-project-form>",
                access: {
                    requiresLogin: true
                }
            });
    }]);