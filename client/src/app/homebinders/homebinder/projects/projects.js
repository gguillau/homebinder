(function() {
    "use strict";

    angular
        .module("hb.homebinders.projects", [
            "hb.homebinders.projects.routes",
            "hb.homebinders.projects.details",
            "hb.homebinders.projects.form",
            "hb.homebinders.projects.modal"
        ])
        .directive("binderProjects", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/projects/projects.tpl.html",
                controller: "BinderProjectsController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("BinderProjectsController", [
            "BinderItemsIndexController",
            "hb.resources",
            Controller
        ]);

    function Controller(BinderItemsIndexController, resources) {
        var Model = function() {
            // call the parent class
            BinderItemsIndexController.call(this);
            this.resources = angular.extend({}, this.resources, resources.binderProjectsIndex);
            this.refreshCall = this.api.project.all;
            this.itemType = "projects";
            this.modalController = "ProjectModalController";
            this.itemNewState = "binder.projectsNew";
            this.itemDetailsState = "binder.projectsDetails";
            this.init();
        };

        Model.prototype = Object.create(BinderItemsIndexController.prototype);

        this.model = new Model();
    }
})();