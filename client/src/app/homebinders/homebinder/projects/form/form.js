(function() {
    "use strict";

    angular
        .module("hb.homebinders.projects.form", [])
        .directive("hbProjectForm", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/projects/form/form.tpl.html",
                controller: "ProjectFormController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        // Model backing the project modal form
        .controller("ProjectFormController", [
            "hb.resources",
            "BinderItemsFormController",
            "hb.api",
            "$stateParams",
            Controller
        ]);

    function Controller(resources, BinderItemsFormController, api, $stateParams) {
        var that = this;
        var Model = function() {
            this.cfg = that.cfg;
            this.onSaved = that.onSaved;
            this.onCancel = that.onCancel;
            this.resources = angular.extend({}, this.resources, resources.binderProjectsForm);
            this.receiptTypes = [];
            this.taggerCfg = {
                areaTags: false
            };
            this.tagName = "project_";
            // call the parent class;
            this.saveCall = api.project.update;
            this.createCall = api.project.create;
            this.deleteCall = api.project.destroy;
            this.refreshCall = api.project.get;
            
            // If $stateParams has an id, we are editing an exisiting item and back should send us to that item's details
            if ($stateParams.id) {
                this.indexState = "binder.projectsDetails";
                this.indexParams = { binderId: $stateParams.binderId, id: $stateParams.id };
            }
            // Else, we were creating a new item and back should send us to the list of items
            else {
                this.indexState = "binder.projects";
                this.indexParams = { binderId: $stateParams.binderId };
            }
            
            this.editState = "binder.projectsEdit";
            
            BinderItemsFormController.call(this);
            this.show_select = false;
            this.loadProjectTypes();
            this.loadProjectStatuses();
            this.date = [{
                opened: false
            }, {
                opened: false
            }];
            this.init();
        };

        Model.prototype = Object.create(BinderItemsFormController.prototype);

        angular.extend(Model.prototype, {

            open: function(index) {
                this.date[index].opened = true;
            },

            populate: function() {
                // if a item was passed in store the values
                if (this.item && this.item.id) {
                    this.name = this.item.name;
                    this.project_type = this.item.project_type;
                    this.status = this.item.status;
                    this.start_date = this.item.start_date ? new Date(this.item.start_date) : this.item.start_date;
                    this.end_date = this.item.end_date ? new Date(this.item.end_date) : this.item.end_date;
                    this.cost = this.item.number;
                    this.details = this.item.details;
                    this.tags = this.item.tags;
                    this.setUploadParams();
                }
            },

            loadProjectTypes: function() {
                api.projectType.all().then(
                    angular.bind(this, this.onLoadProjectTypes),
                    angular.bind(this, this.onLoadProjectTypesError)
                );
            },

            onLoadProjectTypes: function(response) {
                this.projectTypes = response.data;
            },

            onLoadProjectTypesError: function(response) {
                this.$log.error(response);
            },

            loadProjectStatuses: function() {
                api.projectStatus.all().then(
                    angular.bind(this, this.onLoadProjectStatuses),
                    angular.bind(this, this.onLoadProjectStatusesError)
                );
            },

            onLoadProjectStatuses: function(response) {
                this.projectStatuses = response.data;
                this.show_select = true;
            },

            onLoadProjectStatusesError: function(response) {
                this.$log.error(response);
            },

            setPayload: function() {
                // create payload
                return {
                    binder_id: this.binderId,
                    name: this.name,
                    status: this.status,
                    project_type: this.project_type,
                    start_date: this.start_date,
                    end_date: this.end_date,
                    cost: this.cost ? this.cost : 0,
                    dimensions: this.dimensions,
                    details: this.details,
                    tags_attributes: this.tags
                };
            },

            setUploadParams: function() {
                this.uploadParams = {
                    binder_id: this.item.binder_id,
                    project_id: this.item.id
                };
            }
        });

        this.model = new Model();
    }

})();