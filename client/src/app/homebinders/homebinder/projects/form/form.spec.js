describe("ProjectFormController", function() {
    var $rootScope,
        $controller,
        $stateParams,
        resources,
        ctrl,
        BinderItemsIndexController,
        api,
        $q,
        BinderRecallModal,
        $log,
        defer;


    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q.defer();
                    defer.resolve({ permissions: { can_read: true } });
                    return defer.promise;
                }
            };
        });
    }));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $stateParams = _$injector_.get("$stateParams");
        BinderItemsIndexController = _$injector_.get("BinderItemsIndexController");
        resources = _$injector_.get("hb.resources");
        BinderRecallModal = _$injector_.get("BinderRecallModal");
        $log = _$injector_.get("$log");
    }));

    function createController() {
        ctrl = $controller("ProjectFormController", {
            "BinderItemsIndexController": BinderItemsIndexController,
            "hb.resources": resources,
            "hb.api": api,
            "BinderRecallModal": BinderRecallModal,
            "$log": $log,
            "$stateParams": $stateParams
        });
        $rootScope.$apply();
    }

    beforeEach(function() {
        spyOn(api.projectType, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(api.projectStatus, "all").and.returnValue($q.when({ data: { items: [] } }));
    });

    describe('#init', function() {
        it("calls projectType.all and projectStatus.all", function() {
            $stateParams.binderId = 99;
            createController();

            expect(api.projectType.all).toHaveBeenCalled();
            expect(api.projectStatus.all).toHaveBeenCalled();
        });
        
        it("sets the state data when editing an existing item", function() {
            $stateParams = {
                binderId: 1,
                id: 1
            };
            createController();

            expect(ctrl.model.indexState).toEqual("binder.projectsDetails");
            expect(ctrl.model.indexParams).toEqual({ binderId: 1, id: 1 });
            expect(ctrl.model.editState).toEqual("binder.projectsEdit");
        });
        
        it("sets the state data when creating a new item", function() {
            $stateParams = {
                binderId: 1
            };
            createController();

            expect(ctrl.model.indexState).toEqual("binder.projects");
            expect(ctrl.model.indexParams).toEqual({ binderId: 1 });
            expect(ctrl.model.editState).toEqual("binder.projectsEdit");
        });
    });

    describe('#open', function() {
        it("sets to true", function() {

            createController();
            ctrl.model.open(0);

            expect(ctrl.model.date[0].opened).toBe(true);
        });
    });

    describe('#populate', function() {
        it("doesnt set the item", function() {
            createController();
            ctrl.model.populate();

            expect(ctrl.model.item).toEqual({});
        });

        it("sets the item", function() {
            createController();
            ctrl.model.cfg = { item: { id: 1, binder_id: 1, name: "test" } };
            ctrl.model.item = ctrl.model.cfg.item;
            ctrl.model.populate();

            expect(ctrl.model.name).toEqual(ctrl.model.cfg.item.name);
        });
    });

    describe('#onLoadProjectTypes', function() {
        it("sets the projectTypes", function() {

            createController();
            ctrl.model.onLoadProjectTypes({ data: [{ id: 1 }] });

            expect(ctrl.model.projectTypes.length).toEqual(1);
        });
    });

    describe('#onLoadProjectTypesError', function() {
        it("calls $log error", function() {
            spyOn($log, "error");

            createController();
            ctrl.model.onLoadProjectTypesError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
        });
    });

    describe('#onLoadProjectStatuses', function() {
        it("sets the projectStatuses", function() {

            createController();
            ctrl.model.onLoadProjectStatuses({ data: [{ id: 1 }] });

            expect(ctrl.model.projectStatuses.length).toEqual(1);
        });
    });

    describe('#onLoadProjectStatusesError', function() {
        it("calls $log error", function() {
            spyOn($log, "error");

            createController();
            ctrl.model.onLoadProjectStatusesError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
        });
    });

    describe('#setPayload', function() {
        it("returns the payload", function() {
            createController();
            ctrl.model.name = "test";
            var data = ctrl.model.setPayload();

            expect(data.name).toEqual("test");
        });
    });

});

describe('hbProjectForm', function() {
    var $scope, $compile, api, $q, context, element, session;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        session = _$injector_.get("Session");
        context = _$injector_.get("Context");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.projectType, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(api.projectStatus, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(context, "getBinder").and.returnValue({ data: { permissions: { can_read: false }, property: {} } });
        spyOn(session, "getUser").and.returnValue({ id: 1, role: "admin" });

        $scope.cfg = { binderId: 1, item: null };
        $scope.onSaved = function() {};
        $scope.onCancel = function() {};

        element = $compile('<hb-project-form cfg="cfg" on-saved="onSaved" on-cancel="onCancel"></hb-project-form>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {
            expect(element.html()).toContain("Project Type");
        });
    });
});