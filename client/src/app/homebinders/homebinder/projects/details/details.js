(function() {
	"use strict";

	angular
		.module("hb.homebinders.projects.details", [])
		.directive("hbProjectDetails", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/projects/details/details.tpl.html",
				controller: "ProjectDetailsController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("ProjectDetailsController", [
			"hb.resources",
			"BinderItemsDetailsController",
			"hb.api",
			"$stateParams",
			Controller
		]);

	function Controller(resources, BinderItemsDetailsController, api, $stateParams) {
		var that = this;
		var Model = function() {
			this.cfg = that.cfg;
			this.onDestroyed = that.onDestroyed;
			this.onClose = that.onClose;
			this.resources = angular.extend({}, this.resources, resources.binderProjectsDetails);
			this.nameProperty = "name";
			this.refreshCall = api.project.get;
			this.indexState = "binder.projects";
			this.indexParams = { binderId: $stateParams.binderId };
			this.editState = "binder.projectsEdit";
			
			// call the parent class
			BinderItemsDetailsController.call(this);
		};

		Model.prototype = Object.create(BinderItemsDetailsController.prototype);

		angular.extend(Model.prototype, {
			populate: function() {
				this.editParams = { binderId: $stateParams.binderId, id: this.item.id };
				this.tagId = "project_" + this.item.id;
				this.uploadParams = {
					binder_id: this.item.binder_id,
					project_id: this.item.id
				};
			},
			
			deleteCall: function() {
                this.api.project.destroy(this.item.id).then(
                    angular.bind(this, this.onDeleteItemSuccess)
                    );
            },
                    
            onDeleteItemSuccess: function(response) {
                this.notify.success(this.resources.deleteSuccess);
                this.back();
            }
            
		});

		this.model = new Model();
	}

})();