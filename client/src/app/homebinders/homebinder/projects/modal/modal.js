(function() {
    angular
        .module("hb.homebinders.projects.modal", [])
        .controller("ProjectModalController", [
            "hb.resources",
            "hb.api",
            "BinderItemsModalBaseController",
            "$modalInstance",
            "args",
            Controller
        ]);

    function Controller(resources, api, BinderItemsModalBaseController, $modalInstance, args) {
        var Model = function() {
            this.refreshCall = api.project.get;
            this.resources = resources.binderProjectsModal;
            this.$modalInstance = $modalInstance;
            this.args = args;
            this.nameProperty = "name";
            BinderItemsModalBaseController.call(this);

        };

        Model.prototype = Object.create(BinderItemsModalBaseController.prototype);

        this.model = new Model();
    }

})();