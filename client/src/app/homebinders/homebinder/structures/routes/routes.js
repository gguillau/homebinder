angular
    .module("hb.homebinders.structures.routes", [])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            // structure index state
            .state("binder.structures", {
                url: "^/binders/:binderId/structures",
                template: "<binder-structures></binder-structures>",
                access: {
                    requiresLogin: true
                }
            })
            // structures details state
            .state("binder.structuresDetails", {
                url: "^/binders/:binderId/structures/{id:int}",
                template: "<hb-structure-details></hb-structure-details>",
                access: {
                    requiresLogin: true
                }
            })
            // state for new structures
            .state("binder.structuresNew", {
                url: "^/binders/:binderId/structures/new",
                template: "<hb-structure-form></hb-structure-form>",
                access: {
                    requiresLogin: true
                }
            })
            // state for edit structure
            .state("binder.structuresEdit", {
                url: "^/binders/:binderId/structures/{id:int}/edit",
                template: "<hb-structure-form></hb-structure-form>",
                access: {
                    requiresLogin: true
                }
            });
    }]);