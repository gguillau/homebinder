(function() {
    angular
        .module("hb.homebinders.structures.modal", [])
        .controller("StructureModalController", [
            "hb.resources",
            "hb.api",
            "BinderItemsModalBaseController",
            "$modalInstance",
            "args",
            Controller
        ]);

    function Controller(resources, api, BinderItemsModalBaseController, $modalInstance, args) {
        var Model = function() {
            this.refreshCall = api.structure.get;
            this.resources = resources.binderStructuresModal;
            this.$modalInstance = $modalInstance;
            this.args = args;
            this.nameProperty = "name";
            BinderItemsModalBaseController.call(this);

        };

        Model.prototype = Object.create(BinderItemsModalBaseController.prototype);

        this.model = new Model();
    }

})();