(function() {
	"use strict";

	angular
		.module("hb.homebinders.structures.details", [])
		.directive("hbStructureDetails", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/structures/details/details.tpl.html",
				controller: "StructureDetailsController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("StructureDetailsController", [
			"hb.resources",
			"BinderItemsDetailsController",
			"hb.api",
			"$stateParams",
			Controller
		]);

	function Controller(resources, BinderItemsDetailsController, api, $stateParams) {
		var that = this;
		var Model = function() {
			this.cfg = that.cfg;
			this.onDestroyed = that.onDestroyed;
			this.onClose = that.onClose;
			this.resources = angular.extend({}, this.resources, resources.binderStructuresDetails);
			this.nameProperty = "name";
			this.deleteCall = api.structure.destroy;
			this.refreshCall = api.structure.get;
			this.indexState = "binder.structures";
			this.indexParams = { binderId: $stateParams.binderId };
			this.editState = "binder.structuresEdit";
			// call the parent class
			BinderItemsDetailsController.call(this);
		};

		Model.prototype = Object.create(BinderItemsDetailsController.prototype);

		angular.extend(Model.prototype, {
			populate: function() {
				this.editParams = { binderId: $stateParams.binderId, id: this.item.id };
				this.tagId = "structure_" + this.item.id;
				this.uploadParams = {
					binder_id: this.item.binder_id,
					structure_id: this.item.id
				};
			}
		});

		this.model = new Model();
	}

})();