describe("StructureFormController", function() {
    var $rootScope,
        $controller,
        $stateParams,
        resources,
        ctrl,
        BinderItemsIndexController,
        api,
        $q,
        $log,
        defer;

    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q.defer();
                    defer.resolve({ permissions: { can_read: true } });
                    return defer.promise;
                }
            };
        });
    }));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $stateParams = _$injector_.get("$stateParams");
        BinderItemsIndexController = _$injector_.get("BinderItemsIndexController");
        resources = _$injector_.get("hb.resources");
        $log = _$injector_.get("$log");
    }));

    function createController() {
        ctrl = $controller("StructureFormController", {
            "BinderItemsIndexController": BinderItemsIndexController,
            "hb.resources": resources,
            "hb.api": api,
            "$log": $log,
            "$stateParams": $stateParams
        });
        $rootScope.$apply();
    }

    beforeEach(function() {
        spyOn(api.constructionType, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(api.constructionStyle, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(api.heatType, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(api.heatSource, "all").and.returnValue($q.when({ data: { items: [] } }));
    });

    describe('#init', function() {
        it("calls constructionType.all and constructionStyle.all", function() {
            $stateParams.binderId = 99;
            createController();

            expect(api.constructionType.all).toHaveBeenCalled();
            expect(api.constructionStyle.all).toHaveBeenCalled();
            expect(api.heatType.all).toHaveBeenCalled();
            expect(api.heatSource.all).toHaveBeenCalled();
        });

        it("sets the state data when editing an existing item", function() {
            $stateParams = {
                binderId: 1,
                id: 1
            };
            createController();

            expect(ctrl.model.indexState).toEqual("binder.structuresDetails");
            expect(ctrl.model.indexParams).toEqual({ binderId: 1, id: 1 });
            expect(ctrl.model.editState).toEqual("binder.structuresEdit");
        });
        
        it("sets the state data when creating a new item", function() {
            $stateParams = {
                binderId: 1
            };
            createController();

            expect(ctrl.model.indexState).toEqual("binder.structures");
            expect(ctrl.model.indexParams).toEqual({ binderId: 1 });
            expect(ctrl.model.editState).toEqual("binder.structuresEdit");
        });
    });

    describe('#populate', function() {
        it("doesnt set the item", function() {
            createController();
            ctrl.model.populate();

            expect(ctrl.model.item).toEqual({});
        });

        it("sets the item", function() {
            createController();
            ctrl.model.cfg = { item: { id: 1, binder_id: 1, name: "test" } };
            ctrl.model.item = ctrl.model.cfg.item;
            ctrl.model.populate();

            expect(ctrl.model.name).toEqual(ctrl.model.cfg.item.name);
        });
    });

    describe('#onLoadConstructionTypes', function() {
        it("sets the construction_types", function() {

            createController();
            ctrl.model.onLoadConstructionTypes({ data: [{ id: 1 }] });

            expect(ctrl.model.construction_types.length).toEqual(1);
        });
    });

    describe('#onLoadConstructionTypesError', function() {
        it("calls $log error", function() {
            spyOn($log, "error");

            createController();
            ctrl.model.onLoadConstructionTypesError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
        });
    });

    describe('#onLoadConstructionStyles', function() {
        it("sets the construction_styles", function() {

            createController();
            ctrl.model.onLoadConstructionStyles({ data: [{ id: 1 }] });

            expect(ctrl.model.construction_styles.length).toEqual(1);
        });
    });

    describe('#onLoadConstructionStylesError', function() {
        it("calls $log error", function() {
            spyOn($log, "error");

            createController();
            ctrl.model.onLoadConstructionStylesError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
        });
    });

    describe('#onLoadHeatTypes', function() {
        it("sets the heat_types", function() {

            createController();
            ctrl.model.onLoadHeatTypes({ data: [{ id: 1 }] });

            expect(ctrl.model.heat_types.length).toEqual(1);
        });
    });

    describe('#onLoadHeatTypesError', function() {
        it("calls $log error", function() {
            spyOn($log, "error");

            createController();
            ctrl.model.onLoadHeatTypesError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
        });
    });

    describe('#onLoadHeatSources', function() {
        it("sets the heat_sources", function() {

            createController();
            ctrl.model.onLoadHeatSources({ data: [{ id: 1 }] });

            expect(ctrl.model.heat_sources.length).toEqual(1);
        });
    });

    describe('#onLoadHeatSourcesError', function() {
        it("calls $log error", function() {
            spyOn($log, "error");

            createController();
            ctrl.model.onLoadHeatSourcesError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
        });
    });

    describe('#setPayload', function() {
        it("returns the payload", function() {
            createController();
            ctrl.model.name = "test";
            var data = ctrl.model.setPayload();

            expect(data.name).toEqual("test");
        });
    });

});

describe('hbStructureForm', function() {
    var $scope, $compile, api, $q, context;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        context = _$injector_.get("Context");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.constructionType, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(api.constructionStyle, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(api.heatType, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(api.heatSource, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(context, "getBinder").and.returnValue({ data: { id: 1, permissions: { can_read: false } } });

        $scope.cfg = { binderId: 1 };
        $scope.onSaved = function() {};
        $scope.onCancel = function() {};

        $compile('<hb-structure-form cfg="cfg" on-saved="onSaved" on-cancel="onCancel"></hb-structure-form>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls constructionType all', function() {
            expect(api.constructionType.all).toHaveBeenCalled();
        });
    });
});