(function() {
    "use strict";

    angular
        .module("hb.homebinders.structures.form", [])
        .directive("hbStructureForm", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/structures/form/form.tpl.html",
                controller: "StructureFormController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        // Model backing the structure modal form
        .controller("StructureFormController", [
            "hb.resources",
            "BinderItemsFormController",
            "hb.api",
            "$stateParams",
            Controller
        ]);

    function Controller(resources, BinderItemsFormController, api, $stateParams) {
        var that = this;
        var Model = function() {
            this.cfg = that.cfg;
            this.onSaved = that.onSaved;
            this.onCancel = that.onCancel;
            this.resources = angular.extend({}, this.resources, resources.binderStructuresForm);
            this.taggerCfg = {
                structureTags: false
            };
            this.tagName = "structures_";
            // call the parent class;
            this.saveCall = api.structure.update;
            this.createCall = api.structure.create;
            this.deleteCall = api.structure.destroy;
            this.refreshCall = api.structure.get;
            
            // If $stateParams has an id, we are editing an exisiting item and back should send us to that item's details
            if ($stateParams.id) {
                this.indexState = "binder.structuresDetails";
                this.indexParams = { binderId: $stateParams.binderId, id: $stateParams.id };
            }
            // Else, we were creating a new item and back should send us to the list of items
            else {
                this.indexState = "binder.structures";
                this.indexParams = { binderId: $stateParams.binderId };
            }
            this.editState = "binder.structuresEdit";
            
            BinderItemsFormController.call(this);
            this.loadConstructionTypes();
            this.loadConstructionStyles();
            this.loadHeatTypes();
            this.loadHeatSources();
            this.init();
        };

        Model.prototype = Object.create(BinderItemsFormController.prototype);

        angular.extend(Model.prototype, {

            populate: function() {
                // if a item was passed in store the values
                if (this.item && this.item.id) {
                    this.name = this.item.name;
                    this.construction_style = this.item.construction_style;
                    this.construction_type = this.item.construction_type;
                    this.sq_ft = this.item.sq_ft;
                    this.beds = this.item.beds;
                    this.baths = this.item.baths;
                    this.heat_type = this.item.heat_type;
                    this.heat_source = this.item.heat_source;
                    this.year_built = this.item.year_built;
                    this.details = this.item.details;
                    this.tags = this.item.tags;
                    this.setUploadParams();
                }
            },

            loadConstructionTypes: function() {
                api.constructionType.all().then(
                    angular.bind(this, this.onLoadConstructionTypes),
                    angular.bind(this, this.onLoadConstructionTypesError)
                );
            },

            onLoadConstructionTypes: function(response) {
                this.construction_types = response.data;
            },

            onLoadConstructionTypesError: function(response) {
                this.$log.error("failed to load construction types. " + response.data);
            },

            loadConstructionStyles: function() {
                api.constructionStyle.all().then(
                    angular.bind(this, this.onLoadConstructionStyles),
                    angular.bind(this, this.onLoadConstructionStylesError)
                );
            },

            onLoadConstructionStyles: function(response) {
                this.construction_styles = response.data;
            },

            onLoadConstructionStylesError: function(response) {
                this.$log.error("failed to load construction styles. " + response.data);
            },

            loadHeatTypes: function() {
                api.heatType.all().then(
                    angular.bind(this, this.onLoadHeatTypes),
                    angular.bind(this, this.onLoadHeatTypesError)
                );
            },

            onLoadHeatTypes: function(response) {
                this.heat_types = response.data;
            },

            onLoadHeatTypesError: function(response) {
                this.$log.error(response.data);
            },

            loadHeatSources: function() {
                api.heatSource.all().then(
                    angular.bind(this, this.onLoadHeatSources),
                    angular.bind(this, this.onLoadHeatSourcesError)
                );
            },

            onLoadHeatSources: function(response) {
                this.heat_sources = response.data;
            },

            onLoadHeatSourcesError: function(response) {
                this.$log.error("failed to load heat sources. " + response.data);
            },

            setPayload: function() {
                // create payload
                return {
                    binder_id: this.binderId,
                    name: this.name,
                    construction_style: this.construction_style,
                    construction_type: this.construction_type,
                    sq_ft: this.sq_ft,
                    beds: this.beds,
                    baths: this.baths,
                    heat_type: this.heat_type,
                    heat_source: this.heat_source,
                    year_built: this.year_built,
                    details: this.details,
                    tags_attributes: this.tags
                };
            },

            setUploadParams: function() {
                this.uploadParams = {
                    binder_id: this.item.binder_id,
                    structure_id: this.item.id
                };
            }
        });

        this.model = new Model();
    }
})();