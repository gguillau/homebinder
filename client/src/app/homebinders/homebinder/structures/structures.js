(function() {
    "use strict";

    angular
        .module("hb.homebinders.structures", [
            "hb.homebinders.structures.routes",
            "hb.homebinders.structures.details",
            "hb.homebinders.structures.form",
            "hb.homebinders.structures.modal"
        ])
        .directive("binderStructures", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/structures/structures.tpl.html",
                controller: "BinderStructuresController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("BinderStructuresController", [
            "BinderItemsIndexController",
            "hb.resources",
            Controller
        ]);

    function Controller(BinderItemsIndexController, resources) {
        var Model = function() {
            // call the parent class
            BinderItemsIndexController.call(this);
            this.resources = angular.extend({}, this.resources, resources.binderStructuresIndex);
            this.refreshCall = this.api.structure.all;
            this.itemType = "structures";
            this.modalController = "StructureModalController";
            this.itemNewState = "binder.structuresNew";
            this.itemDetailsState = "binder.structuresDetails";
            this.init();
        };

        Model.prototype = Object.create(BinderItemsIndexController.prototype);

        this.model = new Model();
    }

})();
