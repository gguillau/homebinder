(function() {
    "use strict";

    angular
        .module("hb.homebinders.receipts", [
            "hb.homebinders.receipts.routes",
            "hb.homebinders.receipts.details",
            "hb.homebinders.receipts.form",
            "hb.homebinders.receipts.modal",
            "hb.homebinders.receipts.tax"
        ])
        .directive("binderReceipts", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/receipts/receipts.tpl.html",
                controller: "BinderReceiptsController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("BinderReceiptsController", [
            "hb.resources",
            "BinderItemsIndexController",
            Controller
        ]);

    function Controller(resources, BinderItemsIndexController) {
        var Model = function() {
            // call the parent class
            BinderItemsIndexController.call(this);
            this.resources = angular.extend({}, this.resources, resources.binderReceiptsIndex);
            this.refreshCall = this.api.receipt.all;
            this.itemType = "receipts";
            this.modalController = "ReceiptModalController";
            this.itemNewState = "binder.receiptsNew";
            this.itemDetailsState = "binder.receiptsDetails";
            this.init();
        };

        Model.prototype = Object.create(BinderItemsIndexController.prototype);

        this.model = new Model();
    }
})();