(function() {
    angular
        .module("hb.homebinders.receipts.modal", [])
        .controller("ReceiptModalController", [
            "hb.resources",
            "hb.api",
            "BinderItemsModalBaseController",
            "$modalInstance",
            "args",
            Controller
        ]);

    function Controller(resources, api, BinderItemsModalBaseController, $modalInstance, args) {
        var Model = function() {
            this.refreshCall = api.receipt.get;
            this.resources = resources.binderReceiptsModal;
            this.$modalInstance = $modalInstance;
            this.args = args;
            this.nameProperty = "name";
            BinderItemsModalBaseController.call(this);

        };

        Model.prototype = Object.create(BinderItemsModalBaseController.prototype);

        this.model = new Model();
    }
})();