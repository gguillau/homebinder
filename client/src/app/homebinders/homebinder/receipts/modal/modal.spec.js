describe("ReceiptModalController", function() {
    var controller,
        base,
        api,
        resources;

    // load the homebinder module
    beforeEach(module("homebinder"));
    
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("BinderItemsModalBaseController");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");

        controller = $controller("ReceiptModalController", {
            "BinderItemsModalBaseController": base,
            "hb.api": api,
            "hb.resources": resources,
            "$modalInstance": {},
            "args": {}
        });

        $rootScope.$apply();
    }));

    describe("init", function() {
        it("sets the controller api call", function() {
            expect(controller.model.refreshCall).toEqual(api.receipt.get);
        });
    });

});