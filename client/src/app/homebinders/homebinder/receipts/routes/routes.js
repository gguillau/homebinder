angular
    .module("hb.homebinders.receipts.routes", [])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            // receipt index state
            .state("binder.receipts", {
                url: "^/binders/:binderId/receipts",
                template: "<binder-receipts></binder-receipts>",
                access: {
                    requiresLogin: true
                }
            })
            // receipt details state
            .state("binder.receiptsDetails", {
                url: "^/binders/:binderId/receipts/{id:int}",
                template: "<hb-receipt-details></hb-receipt-details>",
                access: {
                    requiresLogin: true
                }
            })
            // state for new receipt
            .state("binder.receiptsNew", {
                url: "^/binders/:binderId/receipts/new",
                template: "<hb-receipt-form></hb-receipt-form>",
                access: {
                    requiresLogin: true
                }
            })
            // state for edit receipt
            .state("binder.receiptsEdit", {
                url: "^/binders/:binderId/receipts/{id:int}/edit",
                template: "<hb-receipt-form></hb-receipt-form>",
                access: {
                    requiresLogin: true
                }
            });
    }]);