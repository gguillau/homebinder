describe("BinderReceiptsController", function() {
    var controller,
        base,
        resources,
        $q_,
        defer;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q_.defer();
                    defer.resolve({ permissions: { can_read: true } });
                    return defer.promise;
                }
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("BinderItemsIndexController");
        resources = $injector.get("hb.resources");
        $q_ = $q;

        controller = $controller("BinderReceiptsController", {
            "BinderItemsIndexController": base,
            "hb.resources": resources
        });
    }));

    describe("init", function() {
        it("sets the controller api call", function() {
            expect(controller.model.refreshCall).toEqual(controller.model.api.receipt.all);
        });
    });

});

describe('binderReceipts', function() {
    var $scope, $compile, $q, api;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        context = _$injector_.get("Context");
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.receipt, "all").and.returnValue($q.when({ data: { items: [] } }));

        $compile('<binder-receipts></binder-receipts>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls api receipt all', function() {
            expect(api.receipt.all).toHaveBeenCalled();
        });
    });
});