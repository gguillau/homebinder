(function() {
    "use strict";

    angular
        .module("hb.homebinders.receipts.form", [])
        .directive("hbReceiptForm", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/receipts/form/form.tpl.html",
                controller: "ReceiptFormController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        // Model backing the receipt modal form
        .controller("ReceiptFormController", [
            "hb.resources",
            "TaxInfo",
            "BinderItemsFormController",
            "hb.api",
            "$stateParams",
            Controller
        ]);

    function Controller(resources, TaxInfo, BinderItemsFormController, api, $stateParams) {
        var that = this;
        var Model = function() {
            this.cfg = that.cfg;
            this.onSaved = that.onSaved;
            this.onCancel = that.onCancel;
            this.resources = angular.extend({}, this.resources, resources.binderReceiptsForm);
            this.TaxInfo = TaxInfo;
            this.receiptTypes = [];
            this.taggerCfg = {
                areaTags: false
            };
            this.tagName = "receipt_";
            // call the parent class;
            this.saveCall = api.receipt.update;
            this.createCall = api.receipt.create;
            this.deleteCall = api.receipt.destroy;
            this.refreshCall = api.receipt.get;
            
            // If $stateParams has an id, we are editing an exisiting item and back should send us to that item's details
            if ($stateParams.id) {
                this.indexState = "binder.receiptsDetails";
                this.indexParams = { binderId: $stateParams.binderId, id: $stateParams.id };
            }
            // Else, we were creating a new item and back should send us to the list of items
            else {
                this.indexState = "binder.receipts";
                this.indexParams = { binderId: $stateParams.binderId };
            }
            this.editState = "binder.receiptsEdit";
            
            BinderItemsFormController.call(this);
            this.loadStores();
            this.init();
        };

        Model.prototype = Object.create(BinderItemsFormController.prototype);

        angular.extend(Model.prototype, {

            populate: function() {
                // if an item was passed in store the values
                if (this.item && this.item.id) {
                    this.name = this.item.name;
                    this.deductable = this.item.deductable;
                    if (this.item.purchase) {
                        this.store = this.item.purchase.store;
                        this.price = this.item.purchase.number;
                        this.purchase_date = this.item.purchase.date;
                    }
                    this.details = this.item.details;
                    this.tags = this.item.tags;
                    this.setUploadParams();
                }
            },

            loadStores: function() {
                api.store.all().then(
                    angular.bind(this, this.onLoadStores),
                    angular.bind(this, this.onLoadStoresError)
                );
            },

            onLoadStores: function(response) {
                this.stores = response.data;
            },

            onLoadStoresError: function(response) {
                this.$log.error(response);
            },

            setPayload: function() {
                // create payload
                return {
                    binder_id: this.binderId,
                    name: this.name,
                    deductable: this.deductable,
                    purchase_attributes: {
                        store: this.store,
                        price: this.price ? this.price : 0,
                        date: this.purchase_date
                    },
                    details: this.details,
                    tags_attributes: this.tags
                };
            },

            setUploadParams: function() {
                this.uploadParams = {
                    binder_id: this.item.binder_id,
                    receipt_id: this.item.id
                };
            },

            showTaxInfo: function() {
                this.TaxInfo.show();
            }
        });

        this.model = new Model();
    }

})();