(function() {
	"use strict";

	angular
		.module("hb.homebinders.receipts.details", [])
		.directive("hbReceiptDetails", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/receipts/details/details.tpl.html",
				controller: "ReceiptDetailsController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("ReceiptDetailsController", [
			"hb.resources",
			"TaxInfo",
			"BinderItemsDetailsController",
			"hb.api",
			"$stateParams",
			Controller
		]);

	function Controller(resources, TaxInfo, BinderItemsDetailsController, api, $stateParams) {
		var that = this;
		var Model = function() {
			this.cfg = that.cfg;
			this.onDestroyed = that.onDestroyed;
			this.onClose = that.onClose;
			this.resources = angular.extend({}, this.resources, resources.binderReceiptsDetails);
			this.TaxInfo = TaxInfo;
			this.nameProperty = "name";
			this.deleteCall = api.receipt.destroy;
			this.refreshCall = api.receipt.get;
			this.indexState = "binder.receipts";
			this.indexParams = { binderId: $stateParams.binderId };
			this.editState = "binder.receiptsEdit";
			// call the parent class
			BinderItemsDetailsController.call(this);
		};

		Model.prototype = Object.create(BinderItemsDetailsController.prototype);

		angular.extend(Model.prototype, {
			populate: function() {
				if (this.item.purchase) {
					this.item.purchase.price = this.item.purchase.number;
				}
				this.editParams = { binderId: $stateParams.binderId, id: this.item.id };
				this.tagId = "receipt_" + this.item.id;
				this.uploadParams = {
					binder_id: this.item.binder_id,
					receipt_id: this.item.id
				};
			},

			showTaxInfo: function() {
				this.TaxInfo.show();
			}
		});

		this.model = new Model();
	}
})();