describe("ReceiptDetailsController", function() {
    var $rootScope,
        $controller,
        resources,
        ctrl,
        BinderItemsIndexController,
        api,
        $log,
        context,
        TaxInfo,
        $q_,
        defer;

    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q_.defer();
                    defer.resolve({ permissions: { can_read: true } });
                    return defer.promise;
                }
            };
        });
    }));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        api = _$injector_.get("hb.api");
        BinderItemsIndexController = _$injector_.get("BinderItemsIndexController");
        resources = _$injector_.get("hb.resources");
        $log = _$injector_.get("$log");
        context = _$injector_.get("Context");
        TaxInfo = _$injector_.get("TaxInfo");
        $q_ = _$injector_.get("$q");
    }));

    function createController() {
        ctrl = $controller("ReceiptDetailsController", {
            "BinderItemsIndexController": BinderItemsIndexController,
            "hb.resources": resources,
            "hb.api": api,
            "$log": $log,
            "Context": context,
            "TaxInfo": TaxInfo
        });
        $rootScope.$apply();
    }

    describe('#populate', function() {
        it("sets the item", function() {
            createController();
            var item = { id: 1, binder_id: 1, purchase: { price: 1 } };
            ctrl.model.item = item;
            ctrl.model.populate();

            expect(ctrl.model.item).toEqual(item);
        });
    });

    describe('showTaxInfo', function() {
        it("calls TaxInfo.show", function() {
            spyOn(TaxInfo, "show");

            createController();
            ctrl.model.showTaxInfo();

            expect(TaxInfo.show).toHaveBeenCalled();
        });
    });
});

describe('hbReceiptDetails', function() {
    var $scope, $compile, api, $q, context, element, session;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        session = _$injector_.get("Session");
        context = _$injector_.get("Context");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.image, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(api.document, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(context, "getBinder").and.returnValue({ data: { permissions: { can_read: false }, property: {} } });
        spyOn(session, "getUser").and.returnValue({ id: 1, role: "admin" });

        $scope.cfg = null;
        $scope.onDestroyed = function() {};
        $scope.onClose = function() {};

        element = $compile('<hb-receipt-details cfg="cfg" on-destroyed="onDestroyed" on-close="onClose"></hb-receipt-details>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {
            expect(element.html()).toContain("Name");
        });
    });
});