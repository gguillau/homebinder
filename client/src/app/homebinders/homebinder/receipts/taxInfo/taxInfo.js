(function() {
    "use strict";

    angular
        .module("hb.homebinders.receipts.tax", [])
        .factory("TaxInfo", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "homebinders/homebinder/receipts/taxInfo/taxInfo.tpl.html",
                            controller: "TaxInfoController as ctrl"
                        });
                    }
                };
            }
        ])
        .controller("TaxInfoController", [
            "$modalInstance",
            "hb.resources",
            TaxInfoController
        ]);

    function TaxInfoController($modal, resources) {
        this.$modal = $modal;
        this.resources = resources.taxInfo;
    }

    TaxInfoController.prototype = {

        cancel: function() {
            this.$modal.close();
        }
    };
})();