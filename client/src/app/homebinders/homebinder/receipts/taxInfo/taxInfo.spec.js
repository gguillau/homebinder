describe("TaxInfoController", function() {
    var $rootScope,
        $controller,
        resources,
        ctrl,
        $modalInstance,
        TaxInfo,
        ModalService;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        resources = _$injector_.get("hb.resources");
        ModalService = _$injector_.get("ModalService");
        TaxInfo = _$injector_.get("TaxInfo");

        $modalInstance = {
            close: function() {}
        };
    }));

    function createController() {
        ctrl = $controller("TaxInfoController", {
            "$modalInstance": $modalInstance,
            "hb.resources": resources
        });
        $rootScope.$apply();
    }

    describe('cancel', function() {
        it("calls close", function() {
            spyOn($modalInstance, "close");

            createController();
            ctrl.cancel();

            expect($modalInstance.close).toHaveBeenCalled();
        });
    });

    describe('TaxInfo', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            TaxInfo.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });

});