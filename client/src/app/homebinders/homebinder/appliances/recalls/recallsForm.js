(function() {
    "use strict";

    angular
        .module("hb.homebinders.appliances.recalls.form", [])
        .controller("ApplianceRecallsFormController", [
            "$modalInstance",
            "hb.resources",
            ApplianceRecallsFormController
        ]);

    function ApplianceRecallsFormController($modal, resources) {
        this.$modal = $modal;
        this.resources = resources.recallsForm;
    }

    ApplianceRecallsFormController.prototype = {

        ok: function() {
            this.$modal.dismiss("cancel");
        }
    };
})();