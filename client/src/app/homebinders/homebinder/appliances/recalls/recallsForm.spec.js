describe("ApplianceRecallsFormController", function() {
    var $rootScope,
        $controller,
        resources,
        ctrl,
        $modalInstance;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        resources = _$injector_.get("hb.resources");
        
        $modalInstance = {
            dismiss: function(){}
        };
    }));

    function createController() {
        ctrl = $controller("ApplianceRecallsFormController", {
            "$modalInstance": $modalInstance,
            "hb.resources": resources
        });
        $rootScope.$apply();
    }

    describe('ok', function() {
        it("calls dismiss", function() {
            spyOn($modalInstance, "dismiss");
            
            createController();
            ctrl.ok();
            
            expect($modalInstance.dismiss).toHaveBeenCalled();
        });
    });

});