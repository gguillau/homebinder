(function() {
    angular
        .module("hb.homebinders.appliances.modal", [])
        .controller("ApplianceModalController", [
            "hb.resources",
            "hb.api",
            "BinderItemsModalBaseController",
            "$modalInstance",
            "args",
            Controller
        ]);

    function Controller(resources, api, BinderItemsModalBaseController, $modalInstance, args) {
        var Model = function() {
            this.refreshCall = api.appliance.get;
            this.resources = resources.binderAppliancesModal;
            this.$modalInstance = $modalInstance;
            this.args = args;
            this.nameProperty = "name";
            BinderItemsModalBaseController.call(this);

        };

        Model.prototype = Object.create(BinderItemsModalBaseController.prototype);

        this.model = new Model();
    }
})();