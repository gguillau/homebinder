(function() {
    "use strict";

    angular
        .module("hb.homebinders.appliances", [
            "hb.homebinders.appliances.routes",
            "hb.homebinders.appliances.details",
            "hb.homebinders.appliances.form",
            "hb.homebinders.appliances.modal"
        ])
        .directive("binderAppliances", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/appliances/appliances.tpl.html",
                controller: "HomeBinderAppliancesController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .service("ApplianceService", [ApplianceService])
        .factory("BinderAppliancesController", [
            "BinderItemsIndexController",
            "hb.resources",
            function(BinderItemsIndexController, resources) {
                var Model = function() {
                    // call the parent class
                    BinderItemsIndexController.call(this);
                    this.resources = angular.extend({}, this.resources, resources.binderAppliancesIndex);
                    this.refreshCall = this.api.appliance.all;
                    this.deleteCall = this.api.appliance.destroy;
                    this.itemType = "appliances";
                    this.itemDetailsState = "binder.appliancesDetails";
                    this.nameProperty = "name";
                    this.modalController = "ApplianceModalController";
                    this.templateConfig = {
                        templateUrl: "homebinders/homebinder/appliances/template/template.tpl.html",
                        resources: this.resources,
                        refreshCall: this.api.applianceTemplates.all
                    };
                    this.hasTemplates = true;
                    this.requiresSubscription = true;
                    this.upgradeText = "Please upgrade to add a new appliance and enroll in recall alerts.";
                };

                Model.prototype = Object.create(BinderItemsIndexController.prototype);

                angular.extend(Model.prototype, {
                    showRecallMessage: function(appliance) {
                        this.new_recalls = 0;
                        appliance.appliance_recalls.forEach(angular.bind(this, function(ar) {
                            if (ar.status === "new") {
                                this.new_recalls += 1;
                            }
                        }));
                        if (this.new_recalls > 0) {
                            return true;
                        }
                        return false;
                    }
                });

                return Model;


            }
        ])
        .controller("HomeBinderAppliancesController", [
            "BinderAppliancesController",
            HomeBinderAppliancesController
        ]);

    function HomeBinderAppliancesController(BinderAppliancesController) {
        var Model = function() {
            // call the parent class
            BinderAppliancesController.call(this);
            this.itemDetailsState = "binder.appliancesDetails";
            this.itemNewState = "binder.appliancesNew";
            this.itemNewTemplateState = "binder.appliancesNewTemplate";
            this.init();
        };

        Model.prototype = Object.create(BinderAppliancesController.prototype);

        this.model = new Model();
    }

    function ApplianceService() {}

    ApplianceService.prototype = {
        makeAndModelCheck: function(appliances) {
            if (appliances.length < 1) {
                return {
                    check: false,
                    message: "No appliances have been created for this binder. Do you want to add appliances along with their make and model at this time?"
                };
            }
            for (var i = 0; i < appliances.length; i++) {
                var app = appliances[i];
                if (app.make === null || app.manufacturer === null) {
                    return {
                        check: false,
                        message: "Make and Model information have not been entered for all appliances for this binder. Do you want to enter that information at this time?"
                    };
                }
            }
            return {
                check: true
            };
        }
    };
})();