(function() {
	"use strict";

	angular
		.module("hb.homebinders.appliances.details", [])
		.directive("hbApplianceDetails", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/appliances/details/details.tpl.html",
				controller: "ApplianceDetailsController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("ApplianceDetailsController", [
			"hb.resources",
			"BinderItemsDetailsController",
			"hb.api",
			"BinderRecallModal",
			"Context",
			"$stateParams",
			Controller
		]);

	function Controller(resources, BinderItemsDetailsController, api, BinderRecallModal, Context, $stateParams) {
		var Model = function() {
			this.resources = angular.extend({}, this.resources, resources.binderAppliancesDetails);
			this.nameProperty = "name";
			this.refreshCall = api.appliance.get;
			this.indexState = "binder.appliances";
			this.indexParams = { binderId: $stateParams.binderId };
			this.editState = "binder.appliancesEdit";
			// call the parent class
			BinderItemsDetailsController.call(this);
		};

		Model.prototype = Object.create(BinderItemsDetailsController.prototype);

		angular.extend(Model.prototype, {
			populate: function() {
				this.editParams = { binderId: $stateParams.binderId, id: this.item.id };
				this.tagId = "appliance_" + this.item.id;
				this.recalls = {};
				this.recalls.url = "/api/v1/recalls?appliance_id=" + this.item.binder_id;
				var binder = Context.getBinder(this.item.binder_id).data;
				this.recalls.canCheckRecall = binder ? binder.subscription ? binder.subscription.plan_id != "free" && this.item.binder_id : false : false;
				this.uploadParams = {
					binder_id: this.item.binder_id,
					appliance_id: this.item.id
				};
			},

			checkForRecalls: function() {
				this.loading.show(this.resources.checkingForRecalls);
				this.api.appliance.checkForRecalls(this.item.id).then(
					angular.bind(this, this.recallCheckSuccess),
					angular.bind(this, this.recallCheckError));
			},

			recallCheckSuccess: function(response) {
				if (response.data.length > 0) {
					BinderRecallModal.display({
						recalls: response.data
					});
				}
				response.data.forEach(this.convertRecallDetails);
				this.item.appliance_recalls = response.data;
				this.loading.close();
			},

			recallCheckError: function(response) {
				this.$log.error(response);
				this.notify.error(response.data);
				this.loading.close();
			},

			setRecallStatus: function(applianceRecall, status) {
				this.api.applianceRecall.update({
					id: applianceRecall.id,
					status: status
				}).then(
					function() {
						applianceRecall.status = status;
					}
				);
			},

			showRecall: function(ar) {
				return ar.status != 'ignore';
			},

			showRecallStatus: function(ar) {
				return ar.status && ar.status != "new";
			},

			convertRecallDetails: function(ar) {
				if (!angular.isObject(ar.recall.details)) {
					ar.recall.details = JSON.parse(ar.recall.details);
				}
			},
			
			deleteCall: function() {
                this.api.appliance.destroy(this.item.id).then(
                    angular.bind(this, this.onDeleteItemSuccess)
                    );
            },
                    
            onDeleteItemSuccess: function(response) {
                this.notify.success(this.resources.deleteSuccess);
                this.back();
            }
		});

		this.model = new Model();
	}
})();