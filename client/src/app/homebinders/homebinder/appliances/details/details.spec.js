describe("ApplianceDetailsController", function() {
    var $rootScope,
        $controller,
        resources,
        ctrl,
        BinderItemsIndexController,
        api,
        $q,
        BinderRecallModal,
        $log,
        context,
        notify,
        defer;

    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q.defer();
                    defer.resolve({ permissions: { can_read: true } });
                    return defer.promise;
                }
            };
        });
    }));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        BinderItemsIndexController = _$injector_.get("BinderItemsIndexController");
        resources = _$injector_.get("hb.resources");
        BinderRecallModal = _$injector_.get("BinderRecallModal");
        $log = _$injector_.get("$log");
        context = _$injector_.get("Context");
        notify = _$injector_.get("Notify");
    }));

    function createController() {
        ctrl = $controller("ApplianceDetailsController", {
            "BinderItemsIndexController": BinderItemsIndexController,
            "hb.resources": resources,
            "hb.api": api,
            "BinderRecallModal": BinderRecallModal,
            "$log": $log,
            "Context": context
        });
        $rootScope.$apply();
    }

    describe('#populate', function() {
        it("sets the item", function() {
            spyOn(context, "getBinder").and.returnValue($q.when({ subscription: { plan_id: "paid" } }));

            createController();
            var item = { id: 1, binder_id: 1 };
            ctrl.model.item = item;
            ctrl.model.populate();

            expect(ctrl.model.item).toEqual(item);
        });
    });

    describe('#checkForRecalls', function() {
        it("calls api appliance checkForRecalls", function() {
            spyOn(api.appliance, "checkForRecalls").and.returnValue($q.when({ data: {} }));

            createController();
            ctrl.model.item = { id: 1 };
            ctrl.model.checkForRecalls();

            expect(api.appliance.checkForRecalls).toHaveBeenCalled();
        });
    });

    describe('#recallCheckSuccess', function() {
        it("calls BinderRecallModal", function() {
            spyOn(BinderRecallModal, "display");

            createController();
            spyOn(ctrl.model, "convertRecallDetails");
            ctrl.model.item = { id: 1 };
            ctrl.model.recallCheckSuccess({ data: [{ id: 1 }] });

            expect(BinderRecallModal.display).toHaveBeenCalled();
        });
    });

    describe('#recallCheckError', function() {
        it("calls notify", function() {
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            ctrl.model.recallCheckError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe('#setRecallStatus', function() {
        it("calls api appliance checkForRecalls", function() {
            spyOn(api.applianceRecall, "update").and.returnValue($q.when({ data: {} }));

            createController();
            ctrl.model.item = { id: 1 };
            var applianceRecall = { id: 1, status: "new" };
            ctrl.model.setRecallStatus(applianceRecall, "verified");
            $rootScope.$apply();

            expect(api.applianceRecall.update).toHaveBeenCalled();
            expect(applianceRecall.status).toEqual("verified");
        });
    });

    describe('#recallCheckError', function() {
        it("calls notify", function() {
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            ctrl.model.recallCheckError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe('#showRecall', function() {
        it("returns true", function() {
            createController();
            var value = ctrl.model.showRecall({ status: "ignore" });

            expect(value).toBe(false);
        });
    });

    describe('#showRecallStatus', function() {
        it("returns false", function() {
            createController();
            var value = ctrl.model.showRecallStatus({ status: "new" });

            expect(value).toBe(false);
        });
    });

    describe('#convertRecallDetails', function() {
        it("parses the JSON", function() {
            createController();
            var recall = { recall: { details: "{\"RecallID\":234234}" } };
            ctrl.model.convertRecallDetails(recall);
            expect(recall.recall.details.RecallID).toEqual(234234);
        });
    });
});

describe('hbApplianceDetails', function() {
    var $scope, $compile, api, $q, context, element, session;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        session = _$injector_.get("Session");
        context = _$injector_.get("Context");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.image, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(api.document, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(context, "getBinder").and.returnValue({ data: { permissions: { can_read: false }, property: {} } });
        spyOn(session, "getUser").and.returnValue({ id: 1, role: "admin" });

        $scope.cfg = null;
        $scope.onDestroyed = function() {};
        $scope.onClose = function() {};

        element = $compile('<hb-appliance-details cfg="cfg" on-destroyed="onDestroyed" on-close="onClose"></hb-appliance-details>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {
            expect(element.html()).toContain("Name");
        });
    });
});