describe("BinderAppliancesController", function() {
    var $rootScope,
        $controller,
        $stateParams,
        resources,
        ctrl,
        BinderItemsIndexController,
        api,
        $q;


    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        $provide.value("binder", {});
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    var defer = $q.defer();
                    defer.resolve({});
                    return defer.promise;
                }
            };
        });
    }));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $stateParams = _$injector_.get("$stateParams");
        BinderItemsIndexController = _$injector_.get("BinderItemsIndexController");
        resources = _$injector_.get("hb.resources");
    }));

    function createController() {
        ctrl = $controller("HomeBinderAppliancesController", {
            "BinderItemsIndexController": BinderItemsIndexController,
            "hb.resources": resources,
            "hb.api": api
        });
        $rootScope.$apply();
    }

    describe('showRecallMessage', function() {
        it("returns false", function() {
            $stateParams.binderId = 99;
            spyOn(api.appliance, "all").and.returnValue($q.when({ data: { items: [] } }));
            createController();
            var result = ctrl.model.showRecallMessage({ appliance_recalls: [] });
            expect(result).toBe(false);
        });

        it("returns true", function() {
            $stateParams.binderId = 99;
            spyOn(api.appliance, "all").and.returnValue($q.when({ data: { items: [] } }));
            createController();
            var result = ctrl.model.showRecallMessage({ appliance_recalls: [{ status: "new" }] });
            expect(result).toBe(true);
        });
    });

});

describe('binderAppliances', function() {
    var $scope, $compile, $q, api;

    beforeEach(module('homebinder'));
    beforeEach(module(function($provide) { $provide.value("binder", {}); }));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        context = _$injector_.get("Context");
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.appliance, "all").and.returnValue($q.when({ data: { items: [] } }));

        $compile('<binder-appliances></binder-appliances>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls api appliance all', function() {
            expect(api.appliance.all).toHaveBeenCalled();
        });
    });
});