(function() {
    "use strict";

    angular
        .module("hb.homebinders.appliances.form", [
            "hb.homebinders.appliances.recalls.form"
        ])
        .directive("hbApplianceForm", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/appliances/form/form.tpl.html",
                controller: "HomeBinderApplianceFormController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .factory("ApplianceFormController", [
            "hb.resources",
            "BinderItemsFormController",
            "hb.api",
            "BinderRecallModal",
            "Session",
            "Context",
            "$state",
            "$stateParams",
            "$log",
            function(resources, BinderItemsFormController, api, BinderRecallModal, session, context, $state, $stateParams, $log) {
                var that = this;
                var Model = function() {
                    this.currentUser = session.getUser();
                    context.getBinder().then(angular.bind(this, function(binder) {
                        this.binder = binder;
                    }));
                    this.cfg = that.cfg;
                    this.onSaved = that.onSaved;
                    this.onCancel = that.onCancel;
                    this.resources = angular.extend({}, this.resources, resources.binderAppliancesForm);
                    this.receiptTypes = [];
                    this.taggerCfg = {
                        applianceTags: false
                    };
                    this.tagName = "appliance_";
                    // call the parent class
                    this.saveCall = api.appliance.update;
                    this.createCall = api.appliance.create;
                    this.deleteCall = api.appliance.destroy;
                    this.refreshCall = api.appliance.get;
                    this.getTemplateItem = api.applianceTemplates.get;
                    this.appliance_manufacturers = [];
                    this.applianceManufacturers();
                    this.loadStores();
                    this.BinderRecallModal = BinderRecallModal;
                    this.date = [{
                        opened: false
                    }, {
                        opened: false
                    }];
                    this.show_template = true;
                    BinderItemsFormController.call(this);
                    this.init();
                };

                Model.prototype = Object.create(BinderItemsFormController.prototype);

                angular.extend(Model.prototype, {

                    open: function(index) {
                        this.date[index].opened = true;
                    },

                    onRefreshTemplateSuccess: function(response) {
                        this.template = response.data;
                        this.name = response.data.name;
                        this.details = response.data.notes;
                        this.library_source_id = response.data.id;
                        this.setFrequency(this.item);
                        this.populate();
                        this.loading.close();
                    },

                    setFrequency: function(item) {
                        // convert interval dropdown for maintenance_cycle
                        switch (this.template.frequency) {
                            case "As Needed":
                                item.interval = 1;
                                item.maintenance_cycle = "As Needed";
                                break;
                            case "Once":
                                item.interval = 1;
                                item.maintenance_cycle = "Once";
                                break;
                            case "Annual":
                                item.interval = 1;
                                item.maintenance_cycle = "Years";
                                break;
                            case "Every 6 months":
                                item.interval = 6;
                                item.maintenance_cycle = "Months";
                                break;
                            case "Quarterly":
                                item.interval = 3;
                                item.maintenance_cycle = "Months";
                                break;
                            case "Monthly":
                                item.interval = 1;
                                item.maintenance_cycle = "Months";
                                break;
                            case "Every other year":
                                item.interval = 2;
                                item.maintenance_cycle = "Years";
                                break;
                            case "Every 3 years":
                                item.interval = 3;
                                item.maintenance_cycle = "Years";
                                break;
                            case "Every 4 years":
                                item.interval = 4;
                                item.maintenance_cycle = "Years";
                                break;
                            case "Every 5 years":
                                item.interval = 5;
                                item.maintenance_cycle = "Years";
                                break;
                            case "Every 10 years":
                                item.interval = 10;
                                item.maintenance_cycle = "Years";
                                break;
                            case "Every 40 years":
                                item.interval = 40;
                                item.maintenance_cycle = "Years";
                                break;
                        }
                    },

                    populate: function() {
                        // if a item was passed in store the values
                        if (this.item && this.item.id) {
                            this.name = this.item.name;
                            this.editParams = { binderId: $stateParams.binderId, id: this.item.id };
                            this.manufacturer = this.item.manufacturer;
                            this.model = this.item.model;
                            this.upc = this.item.upc;
                            this.serial_no = this.item.serial_no;
                            this.library_source_id = this.item.library_source_id;
                            this.warranty = this.item.warranty;
                            this.user_guide_url = this.item.user_guide_url;
                            if (this.item.install_date) {
                                this.install_date = new Date(this.item.install_date);
                            }
                            if (this.item.purchase) {
                                this.store = this.item.purchase.store;
                                this.price = this.item.purchase.number;
                                if (this.item.purchase.date) {
                                    this.purchase_date = new Date(this.item.purchase.date);
                                }
                            }
                            this.tagId = "appliance_" + this.item.id;
                            this.details = this.item.details;
                            this.tags = this.item.tags;
                            this.tagsCfg = {
                                binderId: $stateParams.binderId
                            };
                            this.docUploader.jwt = this.jwt;
                            this.docUploader.params = {
                                binder_id: $stateParams.binderId
                            };
                            this.imgUploader.jwt = this.jwt;
                            this.imgUploader.params = {
                                binder_id: $stateParams.binderId
                            };
                            this.setUploadParams();
                        }
                    },

                    applianceManufacturers: function() {
                        api.applianceManufacturer.all().then(
                            angular.bind(this, this.onLoadApplianceManufacturers),
                            angular.bind(this, this.onLoadApplianceManufacturersError)
                        );
                    },

                    onLoadApplianceManufacturers: function(response) {
                        this.appliance_manufacturers = response.data;
                    },

                    onLoadApplianceManufacturersError: function(response) {
                        $log.error(response);
                    },

                    loadStores: function() {
                        api.store.all().then(
                            angular.bind(this, this.onLoadStores),
                            angular.bind(this, this.onLoadStoresError)
                        );
                    },

                    onLoadStores: function(response) {
                        this.stores = response.data.map(function(store) {
                            return store.name;
                        });
                    },

                    onLoadStoresError: function(response) {
                        $log.error(response);
                    },

                    setPayload: function() {
                        // create payload
                        return {
                            binder_id: this.binderId,
                            name: this.name,
                            manufacturer: this.manufacturer,
                            model: this.model,
                            upc: this.upc,
                            serial_no: this.serial_no,
                            library_source_id: this.library_source_id,
                            warranty: this.warranty,
                            user_guide_url: this.user_guide_url,
                            install_date: this.install_date,
                            purchase_attributes: {
                                store: this.store,
                                price: this.price ? this.price : 0,
                                date: this.purchase_date
                            },
                            details: this.details,
                            tags_attributes: this.tags
                        };
                    },

                    onItemSaved: function(response) {
                        if (response.data.appliance_recalls.length > 0) {
                            this.BinderRecallModal.display({
                                recalls: response.data.appliance_recalls
                            });
                        }
                        this.item = response.data;
                        this.tagId = this.tagName + response.data.id;
                        this.setUploadParams();
                        this.uploadDocuments();
                    },

                    hideExtraFields: function() {
                        return this.currentUser.role === "admin" && $state.current.name === "admin.bindersItems";
                    },

                    setUploadParams: function() {
                        this.uploadParams = {
                            binder_id: this.item.binder_id,
                            appliance_id: this.item.id
                        };
                    }
                });

                return Model;


            }
        ])
        .controller("HomeBinderApplianceFormController", [
            "ApplianceFormController",
            "$stateParams",
            HomeBinderApplianceFormController
        ]);

    function HomeBinderApplianceFormController(ApplianceFormController, $stateParams) {
        var Model = function() {
            // If $stateParams has an id, we are editing an exisiting item and back should send us to that item's details
            if ($stateParams.id) {
                this.indexState = "binder.appliancesDetails";
                this.indexParams = { binderId: $stateParams.binderId, id: $stateParams.id };
            }
            // Else, we were creating a new item and back should send us to the list of items
            else {
                this.indexState = "binder.appliances";
                this.indexParams = { binderId: $stateParams.binderId };
            }
            this.editState = "binder.appliancesEdit";
            ApplianceFormController.call(this);
        };

        Model.prototype = Object.create(ApplianceFormController.prototype);

        angular.extend(Model.prototype, {});

        this.model = new Model();
    }
})();