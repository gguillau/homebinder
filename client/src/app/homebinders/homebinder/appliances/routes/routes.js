angular
    .module("hb.homebinders.appliances.routes", [])
    .config(['$stateProvider', function($stateProvider) {

        $stateProvider
            // appliance index state
            .state("binder.appliances", {
                url: "^/binders/:binderId/appliances",
                template: "<binder-appliances></binder-appliances>",
                access: {
                    requiresLogin: true
                }
            })
            // appliance details state
            .state("binder.appliancesDetails", {
                url: "^/binders/:binderId/appliances/{id:int}",
                template: "<hb-appliance-details></hb-appliance-details>",
                access: {
                    requiresLogin: true
                }
            })
            // state for new appliance
            .state("binder.appliancesNew", {
                url: "^/binders/:binderId/appliances/new",
                template: "<hb-appliance-form></hb-appliance-form>",
                access: {
                    requiresLogin: true
                }
            })
            // state for new maintenanceItem
            .state("binder.appliancesNewTemplate", {
                url: "^/binders/:binderId/appliances/new/{templateId:int}",
                template: "<hb-appliance-form></hb-appliance-form>",
                access: {
                     requiresLogin: true
                }
             })
            // state for edit appliance
            .state("binder.appliancesEdit", {
                url: "^/binders/:binderId/appliances/{id:int}/edit",
                template: "<hb-appliance-form></hb-appliance-form>",
                access: {
                    requiresLogin: true
                }
            });
    }]);
