describe("BuildFaxDisclaimerFormController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $modal,
        api,
        notify,
        $log,
        loading,
        $q,
        $stateParams,
        $state,
        data,
        BuildFaxDisclaimerForm,
        ModalService;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        BuildFaxDisclaimerForm = _$injector_.get("BuildFaxDisclaimerForm");
        ModalService = _$injector_.get("ModalService");

        $stateParams = {};

        data = {
            newPermit: function() {}
        };

        $state = {
            go: function() {}
        };

        api = {
            buildfax: {
                getPermits: function() {},
                createPermits: function() {}
            }
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        $modal = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };

        notify = {
            success: function() {},
            error: function() {},
            info: function() {}
        };

        $log = {
            error: function() {}
        };

        uploader = {
            api: {
                pendingUploads: function() {}
            }
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        $stateParams.binderId = 100;
        ctrl = $controller("BuildFaxDisclaimerFormController", {
            "$modalInstance": $modal,
            "hb.api": api,
            "Notify": notify,
            "$log": $log,
            "Loading": loading,
            "$state": $state,
            "$stateParams": $stateParams,
            "data": data
        });
        $scope.ctrl = ctrl;
    }

    describe("ctrl.import", function() {

        it("should call the buildfax getPermits function", function() {

            spyOn(api.buildfax, "getPermits").and.returnValue($q.when({
                data: {
                    code: 0,
                    message: "This is a message.",
                    permits: [{
                        permit_number: "9jsad-9asds"
                    }]
                }
            }));

            createController();
            $rootScope.$apply();
            ctrl.import();
            $rootScope.$apply();

            expect(api.buildfax.getPermits).toHaveBeenCalled();
            expect(ctrl.step).toBe("two");
            expect(ctrl.response.code).toBe(0);
            expect(ctrl.response.message).toBe("This is a message.");
            expect(ctrl.response.permits).toEqual([{
                permit_number: "9jsad-9asds"
            }]);
            expect(ctrl.selected_permits).toEqual([{
                permit_number: "9jsad-9asds"
            }]);

        });

        it("should return an error", function() {

            spyOn(api.buildfax, "getPermits").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(notify, "error");


            createController();
            $rootScope.$apply();
            ctrl.import();
            $rootScope.$apply();

            expect(api.buildfax.getPermits).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");
        });

    });

    describe("ctrl.createPermits", function() {
        it("should call buildfax createPermits function", function() {
            spyOn(api.buildfax, "createPermits").and.returnValue($q.when({
                data: [{}]
            }));
            spyOn($modal, "close");
            spyOn(notify, "success");

            createController();
            $rootScope.$apply();
            ctrl.createPermits();
            $rootScope.$apply();

            expect(api.buildfax.createPermits).toHaveBeenCalled();
            expect($modal.close).toHaveBeenCalledWith({
                permits: [{}]
            });
            expect(notify.success).toHaveBeenCalledWith("Permit(s) created.");
        });

        it("should call buildfax createPermits function and call notify info", function() {
            spyOn(api.buildfax, "createPermits").and.returnValue($q.when({
                data: []
            }));
            spyOn($modal, "close");
            spyOn(notify, "info");

            createController();
            $rootScope.$apply();
            ctrl.createPermits();
            $rootScope.$apply();

            expect(api.buildfax.createPermits).toHaveBeenCalled();
            expect($modal.close).toHaveBeenCalledWith({
                permits: []
            });
            expect(notify.info).toHaveBeenCalled();
        });

        it("should call buildfax createPermits function and call notify error", function() {
            spyOn(api.buildfax, "createPermits").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            $rootScope.$apply();
            ctrl.createPermits();
            $rootScope.$apply();

            expect(api.buildfax.createPermits).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(ctrl.step).toBe("three");
        });
    });

    describe("ctrl.cancel", function() {

        it("should call $modalInstance dismiss function", function() {

            spyOn($modal, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.cancel();
            $rootScope.$apply();

            expect($modal.dismiss).toHaveBeenCalledWith('cancel');
        });

        it("should call $modalInstance close function", function() {

            spyOn($modal, "close");

            createController();
            $rootScope.$apply();
            ctrl.step = "five";
            ctrl.cancel();
            $rootScope.$apply();

            expect($modal.close).toHaveBeenCalled();
        });

    });

    describe("ctrl.showButtons", function() {

        it("should return false", function() {

            createController();
            $rootScope.$apply();
            ctrl.hide = "true";
            ctrl.step = "one";
            var boolean_value = ctrl.showButtons();
            $rootScope.$apply();

            expect(boolean_value).toBe(false);
        });

        it("should return true", function() {

            createController();
            $rootScope.$apply();
            ctrl.hide = "true";
            ctrl.step = "zero";
            var boolean_value = ctrl.showButtons();
            $rootScope.$apply();

            expect(boolean_value).toBe(true);
        });

    });

    describe("ctrl.showCreate", function() {

        it("should return true", function() {

            createController();
            $rootScope.$apply();
            ctrl.selected_permits = [{}];
            ctrl.step = "two";
            var boolean_value = ctrl.showCreate();
            $rootScope.$apply();

            expect(boolean_value).toBe(true);
        });

        it("should return false", function() {

            createController();
            $rootScope.$apply();
            ctrl.step = "zero";
            var boolean_value = ctrl.showCreate();
            $rootScope.$apply();

            expect(boolean_value).toBe(false);
        });

    });

    describe("ctrl.checkBox", function() {
        it("should add a permit to the selected_permits array", function() {

            createController();
            $rootScope.$apply();
            ctrl.checkBox({ import: true });
            $rootScope.$apply();

            expect(ctrl.selected_permits.length).toBe(1);
        });

        it("should remove a permit from the selected_permits array", function() {

            createController();
            ctrl.selected_permits = [{ id: 1, import: true }];
            $rootScope.$apply();
            ctrl.checkBox({ id: 1, import: false });
            $rootScope.$apply();

            expect(ctrl.selected_permits.length).toBe(0);
        });
    });

    describe('BuildFaxDisclaimerForm', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            BuildFaxDisclaimerForm.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });

});