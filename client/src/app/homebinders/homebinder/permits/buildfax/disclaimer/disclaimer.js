(function() {
    "use strict";

    angular
        .module("hb.homebinders.permits.buildfax.disclaimer", [])
        .factory("BuildFaxDisclaimerForm", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "homebinders/homebinder/permits/buildfax/disclaimer/disclaimer.tpl.html",
                            controller: "BuildFaxDisclaimerFormController as ctrl",
                            backdrop: "static",
                            keyboard: false,
                            resolveData: opts,
                            closed: opts.closed
                        });
                    }
                };
            }
        ])
        .controller("BuildFaxDisclaimerFormController", [
            "$modalInstance",
            "hb.api",
            "Notify",
            "$log",
            "Loading",
            "$state",
            "$stateParams",
            "data",
            "hb.resources",
            BuildFaxDisclaimerFormController
        ]);

    function BuildFaxDisclaimerFormController($modal, api, notify, $log, loading, $state, $stateParams, opts, resources) {
        this.$modal = $modal;
        this.api = api;
        this.notify = notify;
        this.$log = $log;
        this.loading = loading;
        this.$state = $state;
        this.binderId = $stateParams.binderId;
        this.newPermit = opts.newPermit;
        this.response = {};
        this.selected_permits = [];
        this.hide = "false";
        this.cancelText = "Decline";
        this.step = "zero";
        this.resources = resources.buildFaxDisclaimer;
    }

    BuildFaxDisclaimerFormController.prototype = {

        import: function() {
            this.step = "one";
            this.hide = "true";
            this.api.buildfax.getPermits(this.binderId).then(
                angular.bind(this, this.getPermitsSuccess),
                angular.bind(this, this.getPermitsError));
        },

        getPermitsSuccess: function(response) {
            this.step = "two";
            this.cancelText = "Close";
            this.response = response.data;
            angular.copy(response.data.permits, this.selected_permits);
        },

        getPermitsError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.cancelText = "Close";
            this.step = "three";
        },

        createPermits: function() {
            this.step = "four";
            this.api.buildfax.createPermits(this.binderId, this.selected_permits).then(
                angular.bind(this, this.createPermitsSuccess),
                angular.bind(this, this.createPermitsError));
        },

        createPermitsSuccess: function(response) {
            if (response.data.length > 0) {
                this.notify.success("Permit(s) created.");
            }
            else {
                this.notify.info("Duplicate permits were not created.");
            }
            this.$modal.close({
                permits: response.data
            });
        },

        createPermitsError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.step = "three";
        },

        cancel: function() {
            if (this.step === "five") {
                this.$modal.close();
            }
            else {
                this.$modal.dismiss("cancel");
            }
        },

        showButtons: function() {
            if (this.step === "one" || this.step === "four") {
                return false;
            }
            return true;
        },

        showCreate: function() {
            if (this.step === "two" && this.selected_permits.length > 0) {
                return true;
            }
            return false;
        },

        checkBox: function(permit) {
            if (permit.import === true) {
                this.selected_permits.push(permit);
            }
            else {
                var index = this.selected_permits.indexOf(permit);
                this.selected_permits.splice(index, 1);
            }
        }

    };
})();