(function() {
    "use strict";

    angular
        .module("hb.homebinders.permits.buildfax", [
            "hb.homebinders.permits.buildfax.disclaimer"
        ])
        .factory("BuildFaxImportForm", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "homebinders/homebinder/permits/buildfax/buildfax.tpl.html",
                            controller: "BuildFaxImportFormController as ctrl",
                            backdrop: "static",
                            keyboard: false,
                            resolveData: opts,
                            closed: opts.closed
                        });
                    }
                };
            }
        ])
        .controller("BuildFaxImportFormController", [
            "$modalInstance",
            "hb.api",
            "Notify",
            "$log",
            "Loading",
            "$state",
            "$stateParams",
            "data",
            "BuildFaxDisclaimerForm",
            "hb.resources",
            "Context",
            "UpgradeForm",
            BuildFaxImportFormController
        ]);

    function BuildFaxImportFormController($modal, api, notify, $log, loading, $state, $stateParams, opts, BuildFaxDisclaimerForm, resources, context, UpgradeForm) {
        this.$modal = $modal;
        this.api = api;
        this.notify = notify;
        this.$log = $log;
        this.loading = loading;
        this.$state = $state;
        this.binderId = $stateParams.binderId;
        this.newPermit = opts.newPermit;
        this.closed = opts.closed;
        this.BuildFaxDisclaimerForm = BuildFaxDisclaimerForm;
        this.hide = "false";
        this.step = "zero";
        this.resources = resources.binderPermitsBuildfax;
        context.getBinder().then(angular.bind(this, function(binder) { this.binder = binder; }));
        this.UpgradeForm = UpgradeForm;
        this.upgradeText = "Please upgrade to search the BuildFax Building Permit database for any permits associated with your home.";
    }

    BuildFaxImportFormController.prototype = {

        import: function() {
            this.$modal.dismiss("cancel");
            if (this.binder.subscription.plan == "free") {
                this.UpgradeForm.show({
                    upgradeText: this.upgradeText
                });
            }
            else {
                this.BuildFaxDisclaimerForm.show({
                    closed: angular.bind(this, this.closed)
                });
            }
        },

        create: function() {
            this.$modal.dismiss("cancel");
            this.newPermit();
        },

        cancel: function() {
            if (this.step === "five") {
                this.$modal.close();
            }
            else {
                this.$modal.dismiss("cancel");
            }
        }

    };
})();