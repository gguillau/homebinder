describe("BuildFaxImportFormController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $modal,
        api,
        notify,
        $log,
        loading,
        $q,
        $stateParams,
        $state,
        data,
        BuildFaxDisclaimerForm,
        BuildFaxImportForm,
        ModalService,
        context,
        defer;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        BuildFaxImportForm = _$injector_.get("BuildFaxImportForm");
        ModalService = _$injector_.get("ModalService");

        context = {
            getBinder: function() {
                defer = $q.defer();
                defer.resolve({ subscription: {} });
                return defer.promise;
            }
        };

        $stateParams = {};

        data = {
            newPermit: function() {}
        };

        $state = {
            go: function() {}
        };

        api = {
            buildfax: {
                getPermits: function() {},
                createPermits: function() {}
            }
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        $modal = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };

        notify = {
            success: function() {},
            error: function() {},
            info: function() {}
        };

        $log = {
            error: function() {}
        };

        BuildFaxDisclaimerForm = {
            show: function() {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        $stateParams.binderId = 100;
        ctrl = $controller("BuildFaxImportFormController", {
            "$modalInstance": $modal,
            "hb.api": api,
            "Notify": notify,
            "$log": $log,
            "Loading": loading,
            "$state": $state,
            "$stateParams": $stateParams,
            "data": data,
            "BuildFaxDisclaimerForm": BuildFaxDisclaimerForm,
            "Context": context
        });
        $scope.ctrl = ctrl;
    }

    describe("ctrl.import", function() {

        it("should call the BuildFaxDisclaimerForm show function", function() {

            spyOn(BuildFaxDisclaimerForm, "show");

            createController();
            $rootScope.$apply();
            ctrl.import();

            expect(BuildFaxDisclaimerForm.show).toHaveBeenCalled();
        });
    });

    describe("ctrl.create", function() {

        it("should call new permit controller", function() {

            spyOn($modal, "dismiss");

            createController();
            spyOn(ctrl, "newPermit");
            $rootScope.$apply();
            ctrl.create();
            $rootScope.$apply();

            expect($modal.dismiss).toHaveBeenCalledWith('cancel');
            expect(ctrl.newPermit).toHaveBeenCalled();
        });

    });

    describe("ctrl.cancel", function() {

        it("should call $modalInstance dismiss function", function() {

            spyOn($modal, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.cancel();
            $rootScope.$apply();

            expect($modal.dismiss).toHaveBeenCalledWith('cancel');
        });

        it("should call $modalInstance close function", function() {

            spyOn($modal, "close");

            createController();
            $rootScope.$apply();
            ctrl.step = "five";
            ctrl.cancel();
            $rootScope.$apply();

            expect($modal.close).toHaveBeenCalled();
        });

    });

    describe('BuildFaxImportForm', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            BuildFaxImportForm.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });

});