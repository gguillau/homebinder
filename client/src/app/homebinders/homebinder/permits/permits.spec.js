describe("BinderPermitsController", function() {
    var $rootScope,
        $controller,
        $stateParams,
        resources,
        ctrl,
        BinderItemsIndexController,
        api,
        $q,
        BuildFaxImportForm,
        defer;


    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q.defer();
                    defer.resolve({});
                    return defer.promise;
                }
            };
        });
    }));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $stateParams = _$injector_.get("$stateParams");
        BinderItemsIndexController = _$injector_.get("BinderItemsIndexController");
        resources = _$injector_.get("hb.resources");
        BuildFaxImportForm = _$injector_.get("BuildFaxImportForm");
    }));

    function createController() {
        ctrl = $controller("BinderPermitsController", {
            "BinderItemsIndexController": BinderItemsIndexController,
            "hb.resources": resources,
            "hb.api": api,
            "BuildFaxImportForm": BuildFaxImportForm
        });
        $rootScope.$apply();
    }

    describe('addItem', function() {
        it("calls BuildFaxImportForm", function() {
            $stateParams.binderId = 99;
            spyOn(api.permit, "all").and.returnValue($q.when({ data: { items: [] } }));
            spyOn(BuildFaxImportForm, "show");

            createController();
            ctrl.model.addItem();

            expect(BuildFaxImportForm.show).toHaveBeenCalled();
        });
    });

    describe('showModal', function() {
        it("calls addModal", function() {
            $stateParams.binderId = 99;
            spyOn(api.permit, "all").and.returnValue($q.when({ data: { items: [] } }));
            spyOn(BuildFaxImportForm, "show");

            createController();
            spyOn(ctrl.model, "addModal");
            ctrl.model.showModal();

            expect(ctrl.model.addModal).toHaveBeenCalled();
        });
    });

    describe('showModal', function() {
        it("calls addModal", function() {
            $stateParams.binderId = 99;
            spyOn(api.permit, "all").and.returnValue($q.when({ data: { items: [] } }));

            createController();
            ctrl.model.onAddedPermits({ permits: [{ id: 1 }] });

            expect(ctrl.model.items.length).toEqual(1);
        });
    });

});

describe('binderPermits', function() {
    var $scope, $compile, $q, api;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        context = _$injector_.get("Context");
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.permit, "all").and.returnValue($q.when({ data: { items: [] } }));

        $compile('<binder-permits></binder-permits>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls api permit all', function() {
            expect(api.permit.all).toHaveBeenCalled();
        });
    });
});