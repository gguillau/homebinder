(function() {
	"use strict";

	angular
		.module("hb.homebinders.permits.details", [])
		.directive("hbPermitDetails", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/permits/details/details.tpl.html",
				controller: "PermitDetailsController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("PermitDetailsController", [
			"hb.resources",
			"BinderItemsDetailsController",
			"hb.api",
			"$stateParams",
			Controller
		]);

	function Controller(resources, BinderItemsDetailsController, api, $stateParams) {
		var that = this;
		var Model = function() {
			this.cfg = that.cfg;
			this.onDestroyed = that.onDestroyed;
			this.onClose = that.onClose;
			this.resources = angular.extend({}, this.resources, resources.binderPermitsDetails);
			this.nameProperty = "permit_number";
			this.deleteCall = api.permit.destroy;
			this.refreshCall = api.permit.get;
			this.indexState = "binder.permits";
			this.indexParams = { binderId: $stateParams.binderId };
			this.editState = "binder.permitsEdit";
			
			// call the parent class
			BinderItemsDetailsController.call(this);
		};

		Model.prototype = Object.create(BinderItemsDetailsController.prototype);

		angular.extend(Model.prototype, {
			populate: function() {
				this.editParams = { binderId: $stateParams.binderId, id: this.item.id };
				this.tagId = "permit_" + this.item.id;
				this.uploadParams = {
					binder_id: this.item.binder_id,
					permit_id: this.item.id
				};
			}
		});

		this.model = new Model();
	}
})();