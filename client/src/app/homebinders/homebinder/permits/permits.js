(function() {
    "use strict";

    angular
        .module("hb.homebinders.permits", [
            "hb.homebinders.permits.routes",
            "hb.homebinders.permits.details",
            "hb.homebinders.permits.form",
            "hb.homebinders.permits.modal",
            "hb.homebinders.permits.buildfax"
        ])
        .directive("binderPermits", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/permits/permits.tpl.html",
                controller: "BinderPermitsController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("BinderPermitsController", [
            "BinderItemsIndexController",
            "hb.resources",
            "BuildFaxImportForm",
            Controller
        ]);

    function Controller(BinderItemsIndexController, resources, BuildFaxImportForm) {
        var Model = function() {
            // call the parent class
            this.orderBy = "permit_number";
            this.sortBy = "permit_number";
            this.reverseSort = false;
            BinderItemsIndexController.call(this);
            this.resources = angular.extend({}, this.resources, resources.binderPermitsIndex);
            this.refreshCall = this.api.permit.all;
            this.itemType = "permits";
            this.modalController = "PermitModalController";
            this.BuildFaxImportForm = BuildFaxImportForm;
            this.itemNewState = "binder.permitsNew";
            this.itemDetailsState = "binder.permitsDetails";
            this.init();
        };

        Model.prototype = Object.create(BinderItemsIndexController.prototype);

        angular.extend(Model.prototype, {
            addItem: function() {
                this.BuildFaxImportForm.show({
                    closed: angular.bind(this, this.onAddedPermits),
                    newPermit: angular.bind(this, this.showModal)
                });
            },

            showModal: function() {
                this.addModal();
            },

            onAddedPermits: function(response) {
                if (response.permits.length > 0) {
                    var array_one = [];
                    angular.copy(this.permits, array_one);
                    this.items = array_one.concat(response.permits);
                    this.toolbarCfg.total = this.items.length;
                }
            }
        });

        this.model = new Model();
    }

})();