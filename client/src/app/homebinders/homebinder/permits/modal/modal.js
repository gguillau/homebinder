(function() {
    angular
        .module("hb.homebinders.permits.modal", [])
        .controller("PermitModalController", [
            "hb.resources",
            "hb.api",
            "BinderItemsModalBaseController",
            "$modalInstance",
            "args",
            Controller
        ]);

    function Controller(resources, api, BinderItemsModalBaseController, $modalInstance, args) {
        var Model = function() {
            this.refreshCall = api.permit.get;
            this.resources = resources.binderPermitsModal;
            this.$modalInstance = $modalInstance;
            this.args = args;
            this.nameProperty = "permit_number";
            BinderItemsModalBaseController.call(this);
        };

        Model.prototype = Object.create(BinderItemsModalBaseController.prototype);

        this.model = new Model();
    }

})();