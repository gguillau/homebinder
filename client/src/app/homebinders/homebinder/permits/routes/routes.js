angular
    .module("hb.homebinders.permits.routes", [])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            // permit index state
            .state("binder.permits", {
                url: "^/binders/:binderId/permits",
                template: "<binder-permits></binder-permits>",
                access: {
                    requiresLogin: true
                }
            })
            // permit details state
            .state("binder.permitsDetails", {
                url: "^/binders/:binderId/permits/{id:int}",
                template: "<hb-permit-details></hb-permit-details>",
                access: {
                    requiresLogin: true
                }
            })
            // state for new permit
            .state("binder.permitsNew", {
                url: "^/binders/:binderId/permits/new",
                template: "<hb-permit-form></hb-permit-form>",
                access: {
                    requiresLogin: true
                }
            })
            // state for edit permit
            .state("binder.permitsEdit", {
                url: "^/binders/:binderId/permits/{id:int}/edit",
                template: "<hb-permit-form></hb-permit-form>",
                access: {
                    requiresLogin: true
                }
            });
    }]);