describe("PermitFormController", function() {
    var $rootScope,
        $controller,
        $stateParams,
        resources,
        ctrl,
        BinderItemsIndexController,
        api,
        $q,
        BinderRecallModal,
        $log,
        defer;


    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q.defer();
                    defer.resolve({ permissions: { can_read: true } });
                    return defer.promise;
                }
            };
        });
    }));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $stateParams = _$injector_.get("$stateParams");
        BinderItemsIndexController = _$injector_.get("BinderItemsIndexController");
        resources = _$injector_.get("hb.resources");
        BinderRecallModal = _$injector_.get("BinderRecallModal");
        $log = _$injector_.get("$log");
    }));

    function createController() {
        ctrl = $controller("PermitFormController", {
            "BinderItemsIndexController": BinderItemsIndexController,
            "hb.resources": resources,
            "hb.api": api,
            "BinderRecallModal": BinderRecallModal,
            "$log": $log,
            "$stateParams": $stateParams
        });
        $rootScope.$apply();
    }

    beforeEach(function() {
        spyOn(api.binderContractor, "all").and.returnValue($q.when({ data: { items: [] } }));
    });

    describe('#init', function() {
        it("calls binderContractor.all", function() {
            $stateParams.binderId = 99;
            createController();

            expect(api.binderContractor.all).toHaveBeenCalled();
        });

        it("sets the state data when editing an existing item", function() {
            $stateParams = {
                binderId: 1,
                id: 1
            };
            createController();

            expect(ctrl.model.indexState).toEqual("binder.permitsDetails");
            expect(ctrl.model.indexParams).toEqual({ binderId: 1, id: 1 });
            expect(ctrl.model.editState).toEqual("binder.permitsEdit");
        });
        
        it("sets the state data when creating a new item", function() {
            $stateParams = {
                binderId: 1
            };
            createController();

            expect(ctrl.model.indexState).toEqual("binder.permits");
            expect(ctrl.model.indexParams).toEqual({ binderId: 1 });
            expect(ctrl.model.editState).toEqual("binder.permitsEdit");
        });
    });

    describe('#populate', function() {
        it("doesnt set the item", function() {
            createController();
            ctrl.model.populate();

            expect(ctrl.model.item).toEqual({});
        });

        it("sets the item", function() {
            createController();
            ctrl.model.cfg = { item: { id: 1, binder_id: 1, status: "test" } };
            ctrl.model.item = ctrl.model.cfg.item;
            ctrl.model.populate();

            expect(ctrl.model.status).toEqual(ctrl.model.cfg.item.status);
        });
    });

    describe('#getContractorsSuccess', function() {
        it("sets the contractors", function() {

            createController();
            ctrl.model.getContractorsSuccess({ data: { items: [{ id: 1 }] } });

            expect(ctrl.model.options.contractors.length).toEqual(1);
        });
    });

    describe('#getContractorsError', function() {
        it("calls $log error", function() {
            spyOn($log, "error");

            createController();
            ctrl.model.getContractorsError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
        });
    });

    describe('#onSelect', function() {
        it("sets the binder_contractor", function() {
            createController();
            ctrl.model.onSelect({ id: 1 });

            expect(ctrl.model.binder_contractor).toEqual({ id: 1 });
        });
    });

    describe('#setPayload', function() {
        it("returns the payload", function() {
            createController();
            ctrl.model.status = "test";
            var data = ctrl.model.setPayload();

            expect(data.status).toEqual("test");
        });
    });

});

describe('hbPermitForm', function() {
    var $scope, $compile, api, $q, context, element, session;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        session = _$injector_.get("Session");
        context = _$injector_.get("Context");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.binderContractor, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(context, "getBinder").and.returnValue({ data: { permissions: { can_read: false }, property: {} } });
        spyOn(session, "getUser").and.returnValue({ id: 1, role: "admin" });

        $scope.cfg = { binderId: 1, item: null };
        $scope.onSaved = function() {};
        $scope.onCancel = function() {};

        element = $compile('<hb-permit-form cfg="cfg" on-saved="onSaved" on-cancel="onCancel"></hb-permit-form>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {
            expect(element.html()).toContain("Mechanical");
        });
    });
});