(function() {
    "use strict";

    angular
        .module("hb.homebinders.permits.form", [])
        .directive("hbPermitForm", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/permits/form/form.tpl.html",
                controller: "PermitFormController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        // Model backing the permit modal form
        .controller("PermitFormController", [
            "hb.resources",
            "BinderItemsFormController",
            "hb.api",
            "$stateParams",
            Controller
        ]);

    function Controller(resources, BinderItemsFormController, api, $stateParams) {
        var that = this;
        var Model = function() {
            this.cfg = that.cfg;
            this.onSaved = that.onSaved;
            this.onCancel = that.onCancel;
            this.resources = resources.binderPermitsForm;
            this.permitTypes = [];
            this.taggerCfg = {
                permitTags: false
            };
            this.tagName = "permit_";
            // call the parent class
            this.saveCall = api.permit.update;
            this.createCall = api.permit.create;
            this.deleteCall = api.permit.destroy;
            this.refreshCall = api.permit.get;
            
            // If $stateParams has an id, we are editing an exisiting item and back should send us to that item's details
            if ($stateParams.id) {
                this.indexState = "binder.permitsDetails";
                this.indexParams = { binderId: $stateParams.binderId, id: $stateParams.id };
            }
            // Else, we were creating a new item and back should send us to the list of items
            else {
                this.indexState = "binder.permits";
                this.indexParams = { binderId: $stateParams.binderId };
            }
            this.editState = "binder.permitsEdit";
            
            BinderItemsFormController.call(this);
            this.contractors = [];
            this.options = {};
            this.getContractors();
            this.init();
        };

        Model.prototype = Object.create(BinderItemsFormController.prototype);

        angular.extend(Model.prototype, {

            populate: function() {
                // if a item was passed in store the values
                if (this.item) {
                    this.status = this.item.status;
                    this.proposed_use = this.item.proposed_use;
                    this.work_class = this.item.work_class;
                    this.permit_type = this.item.permit_type;
                    this.permit_number = this.item.permit_number;
                    this.valuation_amount = this.item.valuation_amount;
                    this.permit_date = this.item.permit_date ? new Date(this.item.permit_date) : this.item.permit_date;
                    this.details = this.item.details;
                    this.binder_contractor = this.item.binder_contractor;
                    this.tags = this.item.tags;
                    this.setUploadParams();
                }
            },

            getContractors: function() {
                api.binderContractor.all({ binderId: $stateParams.binderId }).then(
                    angular.bind(this, this.getContractorsSuccess),
                    angular.bind(this, this.getContractorsError));
            },

            getContractorsSuccess: function(response) {
                this.options.contractors = response.data.items;
            },

            getContractorsError: function(response) {
                this.$log.error(response);
                this.notify.error(response.data);
            },

            onSelect: function(item) {
                this.binder_contractor = item;
            },

            setPayload: function() {
                // create payload
                return {
                    binder_id: this.binderId,
                    status: this.status,
                    proposed_use: this.proposed_use,
                    permit_type: this.permit_type,
                    permit_number: this.permit_number,
                    valuation_amount: this.valuation_amount,
                    permit_date: this.permit_date,
                    work_class: this.work_class,
                    details: this.details,
                    tags_attributes: this.tags,
                    binder_contractor_id: this.binder_contractor ? this.binder_contractor.id : null
                };
            },

            setUploadParams: function() {
                this.uploadParams = {
                    binder_id: this.item.binder_id,
                    permit_id: this.item.id
                };
            }
        });

        this.model = new Model();
    }
})();