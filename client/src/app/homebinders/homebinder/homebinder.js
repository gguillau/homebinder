(function() {
    "use strict";

    angular
        .module("hb.homebinders.homebinder", [
            "ui.router",
            "hb.homebinders.appliances",
            "hb.homebinders.areas",
            "hb.homebinders.binderContractors",
            "hb.homebinders.documents",
            "hb.homebinders.finishes",
            "hb.homebinders.framework",
            "hb.homebinders.images",
            "hb.homebinders.inventoryItems",
            "hb.homebinders.maintenanceItems",
            "hb.homebinders.overview",
            "hb.homebinders.paints",
            "hb.homebinders.permits",
            "hb.homebinders.projects",
            "hb.homebinders.receipts",
            "hb.homebinders.sellerReports",
            "hb.homebinders.share.form",
            "hb.homebinders.structures",
            "hb.homebinders.upgrade",
            "hb.homebinders.userSettings",
            "hb.homebinders.homebinder.transfer",
            "hb.homebinders.homebinder.partnerContact",
            "hb.homebinders.homebinder.binderItemsVerify"
        ])
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state("binder", {
                    url: "^/binders/:binderId",
                    templateUrl: "homebinders/homebinder/homebinder.tpl.html",
                    controller: "HomeBinderController",
                    controllerAs: "ctrl",
                    resolve: {
                        binder: function(Context, $stateParams) {
                            return Context.getBinder($stateParams.binderId);
                        }
                    },
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .factory("HomeBinderFactory", [
            function() {
                var toolbar = {},
                    updateCallback = null,
                    hideButtons = false;
                return {
                    toolbar: function() {
                        return toolbar;
                    },
                    hideButtons: function() {
                        return hideButtons;
                    },
                    setButtons: function(bool) {
                        hideButtons = bool;
                    },
                    setCallback: function(cb) {
                        updateCallback = cb;
                    },
                    setToolbar: function(tb) {
                        toolbar = tb;
                        if (updateCallback) {
                            updateCallback();
                        }
                    }
                };
            }
        ])
        .controller("HomeBinderController", [
            "$state",
            "ModalService",
            "hb.api",
            "constants",
            "Session",
            "Notify",
            "HomeBinderFactory",
            "$log",
            "TransferModal",
            "Loading",
            "$window",
            "hb.utils",
            "Context",
            "ShareForm",
            "hb.resources",
            "BinderItemsVerify",
            "UpgradeForm",
            "$stateParams",
            "binder",
            "AppDelegate",
            "Appcues",
            "$q",
            HomeBinderController
        ]);

    function HomeBinderController($state, ModalService, api, constants, Session, notify, homebinderFactory, $log, TransferModal, Loading, $window, utils, context, ShareForm, resources, BinderItemsVerify, UpgradeForm, $stateParams, binder, appDelegate, Appcues, $q) {
        this.resources = resources.homebinder;
        this.tabs = [{
            id: "overview",
            name: "Home Dashboard",
            state: "binder.overview",
            image: "/img/newInspector/hbWhiteLogo.png",
            imageClass: "logo",
            nameClass: "logo-text"
        }, {
            id: "maintenance",
            name: this.resources.tabMaintenance,
            resourceType: "maintenance_item",
            state: "binder.maintenanceItems",
            image: "/img/maintenanceIcon.png",
            count: 0
        }, {
            id: "appliances",
            name: this.resources.tabRecalls,
            resourceType: "appliance",
            state: "binder.appliances",
            image: "/img/recallsIcon.png",
            count: 0
        }, {
            id: "projects",
            name: this.resources.tabProjects,
            resourceType: "project",
            state: "binder.projects",
            image: "/img/projectsIcon.png",
            count: 0
        }, {
            id: "contractors",
            name: this.resources.tabContractors,
            resourceType: "binder_contractor",
            state: "binder.contractors",
            image: "/img/homeProIcon.png",
            count: 0
        }, {
            id: "inventory",
            name: this.resources.tabInventory,
            resourceType: "inventory_item",
            state: "binder.inventory",
            image: "/img/inventoryIcon.png",
            count: 0
        }, {
            id: "documents_category",
            name: this.resources.tabDocumentsCategory,
            image: "/img/docsIcon.png",
            state: "binder.documents",
            count: 0
        }, {
            id: "details_category",
            name: this.resources.tabDetailsCategory,
            image: "/img/homeDetailsIcon.png",
            state: "binder.areas",
            count: 0
        }];
        this.mobileTabs = angular.copy(this.tabs);
        this.flatLogo = "/img/flatlogo.png";
        this.constants = constants;
        this.api = api;
        this.modals = ModalService;
        this.Session = Session;
        this.notify = notify;
        this.$log = $log;
        this.transferModal = TransferModal;
        this.homebinderFactory = homebinderFactory;
        this.loading = Loading;
        this.$window = $window;
        this.homebinderFactory.setCallback(angular.bind(this, this.refreshToolbar));
        this.$state = $state;
        this.utils = utils;
        this.context = context;
        this.ShareForm = ShareForm;
        this.today = this.utils.utils.getToday(true);
        this.currentUser = this.Session.getUser();
        this.BinderItemsVerify = BinderItemsVerify;
        this.UpgradeForm = UpgradeForm;
        this.$stateParams = $stateParams;
        this.appDelegate = appDelegate;
        this.showHeaderClass = "hide-header";
        this.Appcues = Appcues;
        this.$q = $q;

        this.setBinderInfo(binder);
    }

    HomeBinderController.prototype = {
        setBinderInfo: function(binder) {
            this.data = binder;
            this.context.setBinder(binder);
            this.permissions = this.data.permissions;
            if (this.data.subscription) {
                this.subscription = {
                    id: this.data.subscription.id,
                    text: this.data.plan_id == "free" ? "Upgrade" : "Standard",
                    url: this.data.plan_id == "free" ?
                        "settings/subscriptions/" + this.data.subscription.id + "/upgrade" : "/settings/subscriptions/" + this.data.subscription.id + "/upgrade",
                    failed: this.data.subscription.payment_status == "failed",
                    visible: this.data.subscription.plan === "free" && this.permissions.can_subscribe
                };
                // Set application and header classes depending on subscription
                if (this.data.subscription.plan === 'free') {
                    this.showHeader = true;
                    this.showHeaderClass = "show-header";
                }
            }
            this.reports = {
                visible: this.data.plan_id != "free",
                fullReportUrl: "/api/v1/reports?binder_id=" + this.data.id +
                    "&api_key=" + this.constants.ApiKey +
                    "&HB-UserToken=" + this.Session.getJwt(),
                sellerReport: {
                    exists: angular.isObject(this.data.seller_report),
                    canCreate: this.permissions.can_create_seller_report && !angular.isObject(this.data.seller_report),
                    canEdit: this.permissions.can_edit_seller_report && angular.isObject(this.data.seller_report),
                    canView: angular.isObject(this.data.seller_report),
                    reportUrl: this.data.seller_report ? "/SellerReport/" + this.data.seller_report.code : undefined,
                    editUrl: this.data.seller_report ? "/SellerReport/" + this.data.seller_report.code + "/edit" : undefined
                }
            };
            this.access_list = binder.access_list;
            this.setMobileButtons();
            this.getBinderItemsCounts();
            if (this.currentUser.role === "homeowner") {
                this.getUser();
                this.getBinders();
            }
        },

        binderLoadError: function(response) {
            this.logo_url = this.flatLogo;
            this.$log.error(response);
            this.notify.error(response.data);
        },

        setMobileButtons: function() {
            if (this.showHeader) {
                this.mobileTabs.push({
                    id: "upgrade",
                    name: this.resources.upgrade,
                    resourceType: "subscription",
                    icon: "glyphicon glyphicon-hand-up"
                });
            }
        },

        getBinderItemsCounts: function() {
            this.maintenanceItemsCount = 0;
            this.appliancesCount = 0;
            this.projectsCount = 0;
            this.homeProsCount = 0;
            this.inventoriesCount = 0;
            this.documentsCount = 0;
            this.imagesCount = 0;
            this.permitsCount = 0;
            this.receiptsCount = 0;
            this.roomsCount = 0;
            this.structuresCount = 0;
            this.paintsCount = 0;
            this.finishesCount = 0;

            var promises = [];
            promises.push(
                this.api.maintenanceItem.all({
                    searchType: "aggregation",
                    query: "binder_id:" + this.$stateParams.binderId,
                    search: true,
                    binderId: this.$stateParams.binderId
                }).then(
                    angular.bind(this, function(response) { this.maintenanceItemsCount = response.data; })),

                this.api.appliance.all({
                    searchType: "aggregation",
                    query: "binder_id:" + this.$stateParams.binderId,
                    search: true,
                    binderId: this.$stateParams.binderId
                }).then(
                    angular.bind(this, function(response) { this.appliancesCount = response.data; })),

                this.api.project.all({
                    searchType: "aggregation",
                    query: "binder_id:" + this.$stateParams.binderId,
                    search: true,
                    binderId: this.$stateParams.binderId
                }).then(
                    angular.bind(this, function(response) { this.projectsCount = response.data; })),

                this.api.binderContractor.all({
                    searchType: "aggregation",
                    query: "binder_id:" + this.$stateParams.binderId,
                    search: true,
                    binderId: this.$stateParams.binderId
                }).then(
                    angular.bind(this, function(response) { this.homeProsCount = response.data; })),

                this.api.inventoryItem.all({
                    searchType: "aggregation",
                    query: "binder_id:" + this.$stateParams.binderId,
                    search: true,
                    binderId: this.$stateParams.binderId
                }).then(
                    angular.bind(this, function(response) { this.inventoriesCount = response.data; })),

                // Docs & Pics

                this.api.document.all({
                    searchType: "aggregation",
                    query: "binder_id:" + this.$stateParams.binderId,
                    search: true,
                    binderId: this.$stateParams.binderId
                }).then(
                    angular.bind(this, function(response) { this.documentsCount = response.data; })),

                this.api.image.all({
                    searchType: "aggregation",
                    query: "binder_id:" + this.$stateParams.binderId,
                    search: true,
                    binderId: this.$stateParams.binderId
                }).then(
                    angular.bind(this, function(response) { this.imagesCount = response.data; })),

                this.api.permit.all({
                    searchType: "aggregation",
                    query: "binder_id:" + this.$stateParams.binderId,
                    search: true,
                    binderId: this.$stateParams.binderId
                }).then(
                    angular.bind(this, function(response) { this.permitsCount = response.data; })),

                this.api.receipt.all({
                    searchType: "aggregation",
                    query: "binder_id:" + this.$stateParams.binderId,
                    search: true,
                    binderId: this.$stateParams.binderId
                }).then(
                    angular.bind(this, function(response) { this.receiptsCount = response.data; })),

                // Home Details

                this.api.area.all({
                    searchType: "aggregation",
                    query: "binder_id:" + this.$stateParams.binderId,
                    search: true,
                    binderId: this.$stateParams.binderId
                }).then(
                    angular.bind(this, function(response) { this.roomsCount = response.data; })),

                this.api.structure.all({
                    searchType: "aggregation",
                    query: "binder_id:" + this.$stateParams.binderId,
                    search: true,
                    binderId: this.$stateParams.binderId
                }).then(
                    angular.bind(this, function(response) { this.structuresCount = response.data; })),

                this.api.paint.all({
                    searchType: "aggregation",
                    query: "binder_id:" + this.$stateParams.binderId,
                    search: true,
                    binderId: this.$stateParams.binderId
                }).then(
                    angular.bind(this, function(response) { this.paintsCount = response.data; })),

                this.api.finish.all({
                    searchType: "aggregation",
                    query: "binder_id:" + this.$stateParams.binderId,
                    search: true,
                    binderId: this.$stateParams.binderId
                }).then(
                    angular.bind(this, function(response) { this.finishesCount = response.data; }))

            );

            this.$q.all(promises).then(
                angular.bind(this, this.binderItemsCountSuccess)
            );
        },

        binderItemsCountSuccess: function(response) {
            this.tabs[1].count = this.maintenanceItemsCount;
            this.tabs[2].count = this.appliancesCount;
            this.tabs[3].count = this.projectsCount;
            this.tabs[4].count = this.homeProsCount;
            this.tabs[5].count = this.inventoriesCount;
            this.tabs[6].count = this.documentsCount + this.imagesCount + this.permitsCount + this.receiptsCount;
            this.tabs[7].count = this.roomsCount + this.structuresCount + this.paintsCount + this.finishesCount;
        },

        getUser: function() {
            this.api.user.get(this.currentUser.id).then(
                angular.bind(this, this.getUserSuccess),
                angular.bind(this, this.getUserError));
        },

        getUserSuccess: function(response) {
            this.user = response.data;
            this.Appcues.populate(this.user, this.$stateParams.binderId);
        },

        getUserError: function(response) {
            this.$log.error(response.data);
        },

        getBinders: function() {
            this.showMyBinders = true;
            this.api.binder.all({ searchMethod: "for_user", userId: this.currentUser.id }).then(
                angular.bind(this, this.getBindersSuccess),
                angular.bind(this, this.getBindersError));
        },

        getBindersSuccess: function(response) {
            this.binders = response.data.items;
        },

        getBindersError: function(response) {
            this.$log.error(response.data);
        },

        selectBinder: function(binder) {
            this.$state.go("binder.overview", { binderId: binder.id });
        },

        backToDashboard: function() {
            var user = this.currentUser;
            switch (user.role) {
                case "agent":
                    this.$state.go("agent.binders", {
                        userId: user.id
                    });
                    break;
                case "limited_admin":
                case "admin":
                    this.$state.go("admin.binders");
                    break;
                default:
                    this.$state.go("partner.binders", {
                        partnerId: user.partner_id
                    });
                    break;
            }
        },

        goToSubscription: function() {
            this.$state.go("binder.userSettingsUpgrade", { subscriptionId: this.subscription.id });
        },

        verify: function() {
            this.BinderItemsVerify.show();
        },

        logout: function() {
            this.appDelegate.logOff();
        }

    };

})();
