angular
    .module("hb.homebinders.framework", [
        "hb.homebinders.framework.indexBase",
        "hb.homebinders.framework.detailsBase",
        "hb.homebinders.framework.formBase",
        "hb.homebinders.framework.modalBase",
        "hb.homebinders.framework.template"
    ]);