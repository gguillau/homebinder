(function() {
    "use strict";

    angular
        .module("hb.homebinders.framework.indexBase", [])
        .factory("BinderItemsIndexController", [
            'hb.framework.indexBase',
            '$state',
            '$log',
            'hb.api',
            'Notify',
            'Loading',
            'Session',
            '$stateParams',
            'HomeBinderFactory',
            'BinderItemsModal',
            'Context',
            'TemplatesFormModal',
            'UpgradeForm',
            function(IndexBase, $state, $log, api, notify, loading, session, $stateParams, HomeBinderFactory, BinderItemsModal, context, TemplatesFormModal, UpgradeForm) {
                var Model = function() {
                    context.getBinder().then(angular.bind(this, function(binder) { this.binder = binder; }));
                    // call the parent class
                    IndexBase.call(this);
                    // sub-classes need to set itemsModal
                    this.$state = $state;
                    this.$log = $log;
                    this.api = api;
                    this.notify = notify;
                    this.loading = loading;
                    this.session = session;
                    this.currentUser = session.getUser();
                    this.binderId = $stateParams.binderId;
                    this.sortBy = "name";
                    this.reverseSort = false;
                    this.currentPage = 1;
                    this.maxSize = 10;
                    this.itemsPerPage = 40;
                    this.hasTemplates = false;
                    this.queryArgs = {
                        binderId: this.binderId,
                        verified: true
                    };
                };

                Model.prototype = Object.create(IndexBase.prototype);

                angular.extend(Model.prototype, {

                    init: function() {
                        // make sure the itemType call is set
                        if (!this.itemType) {
                            throw "itemType not set";
                        }
                        // make sure the modalController is set
                        if (!this.modalController) {
                            throw "modalController not set";
                        }

                        this.toolbarCfg = {
                            searchBox: true,
                            title: this.resources.title,
                            subtitle: this.resources.subTitle,
                            total: null,
                            button: {
                                title: this.resources.addButton,
                                click: angular.bind(this, this.addItem)
                            }
                        };
                        HomeBinderFactory.setToolbar({
                            buttons: [{
                                id: "new",
                                text: this.resources.addButton,
                                class: "btn btn-primary",
                                show: true,
                                icon: "glyphicon glyphicon-plus",
                                click: angular.bind(this, this.addItem)
                            }]
                        });
                        this.refresh();
                    },

                    viewDetails: function(item) {
                        if (item.binder) {
                            context.setBinder(item.binder);
                        }
                        $state.go(this.itemDetailsState, { binderId: item.binder_id, id: item.id });
                    },

                    viewform: function(item) {
                        BinderItemsModal.showForm({
                            itemType: this.itemType,
                            controller: this.modalController,
                            binderId: this.binderId,
                            itemId: item.id,
                            jwt: this.session.getJwt(),
                            onSaved: angular.bind(this, this.onUpdated),
                            onDeleted: angular.bind(this, this.onDeleted)
                        });
                    },

                    onUpdated: function(item) {
                        // remove the item
                        this.onDeleted(item);
                        // add it again
                        this.onAdded(item);
                    },

                    onDeleted: function(item) {
                        var k,
                            len;

                        for (k = 0, len = this.items.length; k < len; k++) {
                            if (this.items[k].id == item.id) {
                                this.items.splice(k, 1);
                                this.toolbarCfg.total = this.items.length;
                                break;
                            }
                        }
                    },

                    addItem: function() {
                        this.addModal();
                    },

                    addModal: function() {
                        var conditions = this.binder && this.binder.permissions.can_read && this.requiresSubscription && this.binder.subscription && this.binder.subscription.plan == "free";
                        if (conditions && this.currentUser.role === "homeowner") {
                            UpgradeForm.show({
                                upgradeText: this.upgradeText
                            });
                        }
                        else if (this.hasTemplates) {
                            TemplatesFormModal.show({
                                config: this.templateConfig,
                                closed: angular.bind(this, this.showForm)
                            });
                        }
                        else {
                            this.showForm();
                        }
                    },

                    showForm: function(item) {
                        if (item) {
                            $state.go(this.itemNewTemplateState, { binderId: this.binder.id, templateId: item.id });
                        }
                        else {
                            $state.go(this.itemNewState, { binderId: this.binder.id });
                        }
                    },

                    onAdded: function(item) {
                        if (this.binder.permissions.can_read) {
                            this.items.push(item);
                            this.toolbarCfg.total = this.items.length;
                            notify.success("Added!");
                        }
                        else {
                            notify.success("Successfully added to binder! An email will be sent to the binder owner within the next 2 hours.");
                        }
                    }
                });

                return Model;
            }

        ]);
})();