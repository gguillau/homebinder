describe("BinderItemsIndexController", function() {
    var BinderItemsIndexController,
        TestClass,
        TemplatesFormModal,
        $q,
        defer,
        $rootScope,
        $state;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the notify service
        $provide.factory("Notify", function() {
            return {
                success: function() {}
            };
        });
        // mock the Session service
        $provide.factory("Session", function() {
            return {
                getJwt: function() {},
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getUser: function() {}
            };
        });

        // mock the Context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q.defer();
                    defer.resolve({ permissions: { can_read: true }, subscription: {} });
                    return defer.promise;
                }
            };
        });

    }));
    // get the base class under test
    beforeEach(inject(function($injector) {
        BinderItemsIndexController = $injector.get("BinderItemsIndexController");
        TemplatesFormModal = $injector.get("TemplatesFormModal");
        $q = $injector.get("$q");
        $rootScope = $injector.get("$rootScope");
        $state = $injector.get("$state");
    }));

    function createTestClass() {
        TestClass = function() {
            BinderItemsIndexController.call(this);
        };

        TestClass.prototype = Object.create(BinderItemsIndexController.prototype);

        return new TestClass();
    }

    describe("init", function() {
        it("calls refresh", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            spyOn(model, "refresh");

            model.itemType = "area";
            model.modalController = "AreaModalController";
            model.init();

            expect(model.refresh).toHaveBeenCalled();
        }));
    });

    describe("viewDetails", function() {
        it("calls BinderItemsModal showDetails", inject(function($q, Loading, $rootScope, BinderItemsModal) {
            var model = createTestClass();
            spyOn($state, "go");

            model.viewDetails({ id: 1 });

            expect($state.go).toHaveBeenCalled();
        }));
    });

    describe("viewform", function() {
        it("calls BinderItemsModal showForm", inject(function($q, Loading, $rootScope, BinderItemsModal) {
            var model = createTestClass();
            spyOn(BinderItemsModal, "showForm");

            model.viewform({ id: 1 });

            expect(BinderItemsModal.showForm).toHaveBeenCalled();
        }));
    });

    describe("onUpdated", function() {
        it("calls onDeleted and onAdded", inject(function($q, Loading, $rootScope, BinderItemsModal) {
            var model = createTestClass();

            spyOn(model, "onDeleted");
            spyOn(model, "onAdded");

            model.onUpdated({ id: 1 });

            expect(model.onDeleted).toHaveBeenCalled();
            expect(model.onAdded).toHaveBeenCalled();
        }));
    });

    describe("onDeleted", function() {
        it("removes the item", inject(function($q, Loading, $rootScope, BinderItemsModal) {
            var model = createTestClass();

            model.items = [{ id: 1 }];
            expect(model.items.length).toEqual(1);
            model.onDeleted({ id: 1 });

            expect(model.items.length).toEqual(0);
        }));
    });

    describe("addItem", function() {
        it("calls addModal", inject(function($q, Loading, $rootScope, BinderItemsModal) {
            var model = createTestClass();
            spyOn(model, "addModal");

            model.addItem();

            expect(model.addModal).toHaveBeenCalled();
        }));
    });

    describe("addModal", function() {
        it("calls BinderItemsModal showForm", inject(function($q, Loading, $rootScope, BinderItemsModal) {
            var model = createTestClass();
            spyOn(model, "showForm");

            $rootScope.$apply();
            model.addModal();

            expect(model.showForm).toHaveBeenCalled();
        }));

        it("calls TemplatesFormModal show", inject(function($q, Loading, $rootScope, BinderItemsModal) {
            var model = createTestClass();
            spyOn(TemplatesFormModal, "show");

            model.hasTemplates = true;
            $rootScope.$apply();
            model.addModal();

            expect(TemplatesFormModal.show).toHaveBeenCalled();
        }));
    });

    describe("showForm", function() {
        it("calls BinderItemsModal showForm", inject(function($q, Loading, $rootScope, BinderItemsModal) {
            var model = createTestClass();
            spyOn($state, "go");
            model.itemNewState = "binder.contractorsNew";
            model.binder = { id: 1 };
            model.showForm();

            expect($state.go).toHaveBeenCalled();
        }));
    });

    describe("onAdded", function() {
        it("adds the item", inject(function($q, Loading, $rootScope, BinderItemsModal) {
            var model = createTestClass();
            model.binder = {
                permissions: {
                    can_read: true
                }
            };
            model.onAdded({ id: 1 });

            expect(model.items.length).toEqual(1);
        }));

        it("does not add the item", inject(function($q, Loading, $rootScope, BinderItemsModal) {
            var model = createTestClass();
            model.binder = {
                permissions: {
                    can_read: false
                }
            };
            model.onAdded({ id: 1 });

            expect(model.items.length).toEqual(0);
        }));
    });

});