describe("BinderItemsModalBaseController", function() {
    var ModalControllerBase, BinderItemsModal, $modal, $rootScope, $q;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
        // mock the notify service
        $provide.factory("Notify", function() {
            return {
                success: function() {},
                error: function() {}
            };
        });
        // mock the $log service

        $provide.factory("$log", function() {
            return {
                error: function() {},
                warn: function() {}
            };
        });

        // mock the modals service
        $provide.factory("$modal", function() {
            return {
                open: function() {}
            };
        });
    }));
    // get the base class under test
    beforeEach(inject(function($injector) {
        $rootScope = $injector.get("$rootScope");
        ModalControllerBase = $injector.get("BinderItemsModalBaseController");
        BinderItemsModal = $injector.get("BinderItemsModal");
        $modal = $injector.get("$modal");
        $q = $injector.get("$q");
    }));

    /*
     * Test BinderItemsModalBaseController class
     */
    function createControllerTestClass() {
        var Controller = function() {
            this.args = {};
            ModalControllerBase.call(this);
        };

        Controller.prototype = Object.create(ModalControllerBase.prototype);

        return new Controller();
    }

    /*
     * BinderItemsModalBaseController tests
     */

    describe("ToggleModalControllerBase", function() {
        describe("init", function() {
            it("shows the details view and populates", function() {
                var controller = createControllerTestClass();
                expect(controller.objectProperty).toEqual("item");
            });
        });
    });

    describe('BinderItemsModal', function() {
        it('should call show and then $modal open', function() {
            var args = { onClosed: function() {} },
                deferred = $q.defer(),
                modalInstance = {
                    result: deferred.promise
                };

            spyOn($modal, "open").and.returnValue(modalInstance);

            BinderItemsModal.showDetails(args);

            deferred.resolve({});
            $rootScope.$apply();
            expect($modal.open).toHaveBeenCalled();
        });

        it('should call showForm and then $modal open', function() {
            var args = { onClosed: function() {} },
                deferred = $q.defer(),
                modalInstance = {
                    result: deferred.promise
                };

            spyOn($modal, "open").and.returnValue(modalInstance);

            BinderItemsModal.showForm(args);

            deferred.resolve({});
            $rootScope.$apply();
            expect($modal.open).toHaveBeenCalled();
        });
    });

});