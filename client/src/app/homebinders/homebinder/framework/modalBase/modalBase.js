(function() {
    "use strict";

    angular.module("hb.homebinders.framework.modalBase", [])
        .factory("BinderItemsModal", [
            "$modal",
            function($modal) {
                var $modalInstance;

                function showModal(args) {
                    // open the modal
                    $modalInstance = $modal.open({
                        templateUrl: "homebinders/homebinder/" + args.itemType + "/modal/modal.tpl.html",
                        controller: args.controller + " as ctrl",
                        size: "md",
                        resolve: {
                            args: function() {
                                return args;
                            }
                        },
                        backdrop: "static"
                    });

                    /*jshint -W024*/
                    $modalInstance
                        .result
                        //.then(args.onClosed)
                        .finally(function() {
                            $modalInstance = null;
                            args.onClosed();
                        });
                }

                return {
                    showDetails: function(args) {
                        // set the view type on the args before showing
                        args.view = "details";
                        // show the modal
                        showModal(args);
                    },
                    showForm: function(args) {
                        // set the view type on the args before showing
                        args.view = args.hasOwnProperty("item") || args.hasOwnProperty("itemId") ? "edit" : "new";
                        // show the modal
                        showModal(args);
                    },
                    close: function() {
                        if ($modalInstance) {
                            $modalInstance.dismiss();
                            $modalInstance = null;
                        }
                    }
                };
            }
        ])
        .factory("BinderItemsModalBaseController", [
            "$log",
            "hb.api",
            "Notify",
            "Loading",
            "hb.framework.toggleModalControllerBase",
            function($log, api, notify, loading, toggleModalBase) {
                var Model = function() {
                    toggleModalBase.call(this);
                    this.$log = $log;
                    this.api = api;
                    this.notify = notify;
                    this.loading = loading;
                    this.detailsCfg = {
                        binderId: this.args.binderId,
                        jwt: this.args.jwt,
                        hide: false
                    };
                    this.formCfg = {
                        binderId: this.args.binderId,
                        jwt: this.args.jwt,
                        hide: false
                    };
                    this.initialView = null;
                    this.currentView = null;
                    this.isLoading = true;
                    this.objectProperty = "item";
                    this.objectIdProperty = "itemId";
                    this.populateCall = this.refreshCall;
                    this.init();
                };

                Model.prototype = Object.create(toggleModalBase.prototype);

                return Model;
            }

        ]);
})();