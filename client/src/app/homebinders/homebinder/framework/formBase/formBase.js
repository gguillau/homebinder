(function() {
    "use strict";

    angular.module("hb.homebinders.framework.formBase", [])
        .factory("BinderItemsFormController", [
            '$state',
            '$log',
            'hb.api',
            'Notify',
            'Loading',
            'Context',
            'hb.framework.editBase',
            '$stateParams',
            'ModalService',
            function($state, $log, api, notify, loading, context, EditBase, $stateParams, ModalService) {
                var Model = function() {
                    EditBase.call(this);
                    this.modals = ModalService;
                };

                Model.prototype = Object.create(EditBase.prototype);

                angular.extend(Model.prototype, {

                    init: function() {
                        this.$log = $log;
                        this.api = api;
                        this.notify = notify;
                        this.loading = loading;
                        this.tags = [];
                        this.images = [];
                        this.documents = [];
                        this.itemId = this.itemId || $stateParams.id;
                        this.binderId = $stateParams.binderId;
                        this.templateId = $stateParams.templateId;
                        this.docUploader = {
                            fileTypes: "document",
                            hideUploadButton: true,
                            showTable: false,
                            selectButtonLabel: this.resources.docUpload,
                            url: "/api/v1/documents/",
                            multiSelect: true,
                            id: "uploadDocuments",
                            hide_upload: false
                        };
                        this.imgUploader = {
                            fileTypes: "image",
                            hideUploadButton: true,
                            showTable: false,
                            selectButtonLabel: this.resources.imageUpload,
                            fileLimit: false,
                            url: "/api/v1/images",
                            multiSelect: true,
                            id: "uploadImages",
                            hide_upload: false
                        };
                        if (this.itemId) {
                            this.refresh();
                        }
                        else if (this.templateId) {
                            this.getTemplate();
                        }
                        else {
                            this.populate();
                        }
                    },

                    getTemplate: function() {
                        this.getTemplateItem(this.templateId).then(
                            angular.bind(this, this.onRefreshTemplateSuccess),
                            angular.bind(this, this.onRefreshError)
                        );
                    },

                    // Refresh callback. Updates the item list and totals
                    onRefreshSuccess: function(response) {
                        this.item = response.data;
                        this.populate();
                        loading.close();
                    },

                    submit: function() {
                        this.form.$submitted = true;
                        if (this.form.$invalid) {
                            return;
                        }

                        // retrieve the payload and
                        // determine whether the item is
                        // verified
                        var data = this.setPayload();
                        this.verify(data);

                        this.loading.show(this.resources.saving);
                        if (this.item && this.item.id) {
                            // add item id and binder id for update
                            data.id = this.item.id;
                            // do the update
                            this.saveCall(this.item.id, data).then(
                                angular.bind(this, this.onItemSaved),
                                angular.bind(this, this.onItemSaveError)
                            );
                        }
                        else {
                            // do the create
                            this.createCall(data).then(
                                angular.bind(this, this.onItemSaved),
                                angular.bind(this, this.onItemSaveError)
                            );
                        }
                    },

                    onItemSaved: function(response) {
                        this.item = response.data;
                        this.tagId = this.tagName + response.data.id;
                        this.setUploadParams();
                        this.uploadDocuments();
                    },

                    onItemSaveError: function(response) {
                        this.$log.error(response);
                        this.notify.error(response.data);
                        this.loading.close();
                    },

                    uploadDocuments: function() {
                        var uploadApi = this.docUploader.api;
                        if (uploadApi && uploadApi.pendingUploads().length > 0) {
                            uploadApi.updateParams(this.uploadParams);
                            this.loading.setMessage(this.resources.uploadingDocuments);
                            // TODO: Hack
                            //setTimeout(function() {
                            uploadApi.uploadFiles();
                            //}, 0);
                        }
                        else {
                            this.uploadImages();
                        }
                    },

                    docUploadSuccess: function(file) {
                        this.uploadDocuments();
                    },

                    docUploadError: function(response) {
                        this.$log.error(response);
                        this.notify.error(response.data);
                        var files = this.docUploader.api.pendingUploads();
                        this.docUploader.api.remove(files[0]);
                        this.uploadDocuments();
                    },

                    uploadImages: function() {
                        var uploadApi = this.imgUploader.api;
                        if (uploadApi && uploadApi.pendingUploads().length > 0) {
                            uploadApi.updateParams(this.uploadParams);
                            this.loading.setMessage(this.resources.uploadingImages);
                            // TODO: Hack
                            //setTimeout(function() {
                            uploadApi.uploadFiles();
                            //}, 0);
                        }
                        else {
                            this.imgUploadSuccess();
                        }
                    },

                    imgUploadSuccess: function(file) {
                        this.images.push(file);
                        if (this.imgUploader.api.pendingUploads().length === 0) {
                            this.loading.close();
                            this.back();
                        }
                        else {
                            this.uploadImages();
                        }
                    },

                    imgUploadError: function(response) {
                        this.$log.error(response);
                        this.notify.error(response.data);
                        var files = this.imgUploader.api.pendingUploads();
                        this.imgUploader.api.remove(files[0]);
                        this.imgUploadSuccess();
                    },

                    cancel: function() {
                        this.hide = true;
                        this.onCancel();
                    },

                    verify: function(data) {
                        context.getBinder().then(function(binder) {
                            if (!binder.permissions.can_read) {
                                data.verified = false;
                            }
                        });
                    },

                    setUploadParams: function() {},
                    
                    onBackClicked: function() {
                        if (!this.form.$pristine) {
                            this.modals.confirm({
                                glyphicon: "glyphicon glyphicon-trash",
                                title: this.resources.confirmBack,
                                message: this.resources.confirmBackMessage,
                                confirm: angular.bind(this, this.back)
                            });
                        }
                        else {
                            this.back();
                        }
                    }
                });

                return Model;
            }

        ]);
})();