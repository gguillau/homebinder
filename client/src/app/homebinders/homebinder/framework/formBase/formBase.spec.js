describe("BinderItemsFormController", function() {
    var BinderItemsFormController,
        TestClass,
        api,
        $q,
        defer,
        ModalService;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {},
                setMessage: function() {}
            };
        });
        // mock the notify service
        $provide.factory("Notify", function() {
            return {
                error: function() {}
            };
        });
        // mock the Session service
        $provide.factory("Session", function() {
            return {
                getJwt: function() {},
                getLocale: function() {
                    return null;
                },
                setLocale: function() {}
            };
        });

        // mock the Context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q.defer();
                    defer.resolve({ id: 1, permissions: { can_read: true }, subscription: {} });
                    return defer.promise;
                }
            };
        });

    }));
    // get the base class under test
    beforeEach(inject(function($injector) {
        BinderItemsFormController = $injector.get("BinderItemsFormController");
        api = $injector.get("hb.api");
        $q = $injector.get("$q");
        ModalService = $injector.get("ModalService");
    }));

    function createTestClass() {
        TestClass = function() {
            this.saveCall = api.appliance.update;
            this.createCall = api.appliance.create;
            this.resources = {};
            this.taggerCfg = {};
            this.item = {};
            this.itemId = 2;
            this.refreshCall = api.appliance.get;
            BinderItemsFormController.call(this);
            this.init();
        };

        TestClass.prototype = Object.create(BinderItemsFormController.prototype);

        angular.extend(TestClass.prototype, {
            refresh: function() {},
            setPayload: function() { return {}; }
        });

        return new TestClass();
    }

    describe("init", function() {
        it("calls refresh", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            spyOn(model, "refresh");
            model.itemId = 2;
            model.init();

            expect(model.refresh).toHaveBeenCalled();
        }));
    });

    describe("submit", function() {
        it("does not call createCall or saveCall", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            spyOn(model, "createCall").and.returnValue($q.when({ data: {} }));
            spyOn(model, "saveCall").and.returnValue($q.when({ data: {} }));
            model.form = { $invalid: true };
            model.item = undefined;
            model.submit();

            expect(model.createCall).not.toHaveBeenCalled();
            expect(model.saveCall).not.toHaveBeenCalled();
        }));

        it("calls createCall", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            spyOn(model, "createCall").and.returnValue($q.when({ data: {} }));
            model.form = { $invalid: false };
            model.item = undefined;
            model.submit();

            expect(model.createCall).toHaveBeenCalled();
        }));

        it("calls saveCall", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            spyOn(model, "saveCall").and.returnValue($q.when({ data: {} }));
            model.form = { $invalid: false };
            model.item = { id: 1 };
            model.submit();

            expect(model.saveCall).toHaveBeenCalled();
        }));
    });

    describe("onItemSaved", function() {
        it("calls uploadDocuments", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();

            spyOn(model, "uploadDocuments");
            model.onItemSaved({ data: { id: 1 } });

            expect(model.uploadDocuments).toHaveBeenCalled();
        }));
    });

    describe("onItemSaveError", function() {
        it("calls notify error", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();

            spyOn(model.notify, "error");
            model.onItemSaveError({ data: "error" });

            expect(model.notify.error).toHaveBeenCalled();
        }));
    });

    describe("uploadDocuments", function() {
        it("calls uploadFiles", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            var uploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            spyOn(uploader.api, "uploadFiles");
            spyOn(uploader.api, "updateParams");
            spyOn(uploader.api, "pendingUploads").and.returnValue([{ id: 1 }]);
            model.docUploader = uploader;
            model.uploadDocuments();

            expect(uploader.api.uploadFiles).toHaveBeenCalled();
        }));

        it("calls uploadImages", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            var uploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            spyOn(uploader.api, "uploadFiles");
            spyOn(uploader.api, "updateParams");
            spyOn(uploader.api, "pendingUploads").and.returnValue([]);
            model.docUploader = uploader;
            spyOn(model, "uploadImages");
            model.uploadDocuments();

            expect(model.uploadImages).toHaveBeenCalled();
        }));
    });

    describe("docUploadSuccess", function() {
        it("calls uploadDocuments", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();

            spyOn(model, "uploadDocuments");
            model.docUploadSuccess({ id: 1 });

            expect(model.uploadDocuments).toHaveBeenCalled();
        }));
    });

    describe("docUploadError", function() {
        it("calls notify error", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            var uploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    remove: function() {}
                }
            };
            spyOn(uploader.api, "remove");
            spyOn(uploader.api, "updateParams");
            spyOn(uploader.api, "pendingUploads").and.returnValue([{ id: 1 }]);
            model.docUploader = uploader;
            spyOn(model, "uploadDocuments");
            model.docUploadError({ data: "error" });

            expect(uploader.api.remove).toHaveBeenCalled();
            expect(model.uploadDocuments).toHaveBeenCalled();
        }));
    });

    describe("uploadImages", function() {
        it("calls uploadFiles", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            var uploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            spyOn(uploader.api, "uploadFiles");
            spyOn(uploader.api, "updateParams");
            spyOn(uploader.api, "pendingUploads").and.returnValue([{ id: 1 }]);
            model.imgUploader = uploader;
            model.uploadImages();

            expect(uploader.api.uploadFiles).toHaveBeenCalled();
        }));

        it("calls imgUploadSuccess", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            var uploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            spyOn(uploader.api, "uploadFiles");
            spyOn(uploader.api, "updateParams");
            spyOn(uploader.api, "pendingUploads").and.returnValue([]);
            model.imgUploader = uploader;
            spyOn(model, "imgUploadSuccess");
            model.uploadImages();

            expect(model.imgUploadSuccess).toHaveBeenCalled();
        }));
    });

    describe("imgUploadSuccess", function() {
        it("calls back", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();

            model.back = function() {};
            spyOn(model, "back");
            var uploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            spyOn(uploader.api, "pendingUploads").and.returnValue([]);
            model.imgUploader = uploader;
            model.indexBase = "binder.maintenanceItems";
            model.indexParams = { binderId: 1 };
            model.imgUploadSuccess({ id: 1 });

            expect(model.back).toHaveBeenCalled();
        }));

        it("calls uploadImages", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();

            spyOn(model, "uploadImages");
            var uploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            spyOn(uploader.api, "pendingUploads").and.returnValue([{ id: 1 }]);
            model.imgUploader = uploader;
            model.imgUploadSuccess({ id: 1 });

            expect(model.uploadImages).toHaveBeenCalled();
        }));
    });

    describe("imgUploadError", function() {
        it("calls notify error", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            var uploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    remove: function() {}
                }
            };
            spyOn(uploader.api, "remove");
            spyOn(uploader.api, "updateParams");
            spyOn(uploader.api, "pendingUploads").and.returnValue([{ id: 1 }]);
            model.imgUploader = uploader;
            spyOn(model, "imgUploadSuccess");
            model.imgUploadError({ data: "error" });

            expect(uploader.api.remove).toHaveBeenCalled();
            expect(model.imgUploadSuccess).toHaveBeenCalled();
        }));
    });

    describe("cancel", function() {
        it("calls onCancel", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();

            model.onCancel = function() {};
            spyOn(model, "onCancel");
            model.cancel();

            expect(model.onCancel).toHaveBeenCalled();
        }));
    });

    describe("verify", function() {
        it("sets verified to false", inject(function($q, Loading, $rootScope, Context) {
            var model = createTestClass();
            spyOn(Context, "getBinder").and.returnValue($q.when({ permissions: { can_read: false } }));

            var data = { verified: true };
            expect(data.verified).toBe(true);
            $rootScope.$apply();
            model.verify(data);
            $rootScope.$apply();
            
            expect(data.verified).toBe(false);
        }));
    });

    describe('onBackClicked', function() {
        it("immediately calls back if the form is pristine", function() {
            var model = createTestClass();
            spyOn(model, "back");
            
            model.form = {
                $pristine: true
            };
            model.onBackClicked();

            expect(model.back).toHaveBeenCalled();

        });
        
        it("calls the confirmation modal if the form is not pristine", function() {
            var model = createTestClass();
            spyOn(ModalService, "confirm");
            
            model.form = {
                $pristine: false
            };
            model.onBackClicked();

            expect(ModalService.confirm).toHaveBeenCalled();

        });
    });
});