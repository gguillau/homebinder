(function() {
    "use strict";

    angular
        .module("hb.homebinders.framework.template.form", [])
        .factory("TemplatesFormModal", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: opts.config.templateUrl,
                            controller: "TemplatesFormModalController as ctrl",
                            backdrop: "static",
                            keyboard: false,
                            rejected: opts.dismiss,
                            closed: opts.closed,
                            resolveData: opts
                        });
                    }
                };
            }
        ])
        .controller("TemplatesFormModalController", [
            "$modalInstance",
            "data",
            "$log",
            TemplatesFormModalController
        ]);

    function TemplatesFormModalController($modal, opts, $log) {
        this.$modal = $modal;
        this.refreshCall = opts.config.refreshCall;
        this.resources = opts.config.resources;
        this.$log = $log;
        this.loadTemplates();
    }

    TemplatesFormModalController.prototype = {

        loadTemplates: function() {
            this.refreshCall({ library_template: true, system: true, "default": true, count: 100 }).then(
                angular.bind(this, this.onLoadTemplateSuccess),
                angular.bind(this, this.onLoadTemplatesError)
            );
        },

        onLoadTemplateSuccess: function(response) {
            this.templates = response.data.items;
            this.template = this.templates[0];
        },

        onLoadTemplatesError: function(response) {
            this.$log.error(response);
        },

        dismiss: function() {
            this.$modal.dismiss();
        },

        createOwn: function() {
            this.$modal.close();
        },

        createFromTemplate: function() {
            this.$modal.close(this.template);
        }
    };
})();