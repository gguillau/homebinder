describe("TemplatesFormModalController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $modal,
        opts,
        $q,
        $log,
        TemplatesFormModal,
        ModalService;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $q = _$injector_.get("$q");
        $log = _$injector_.get("$log");
        TemplatesFormModal = _$injector_.get("TemplatesFormModal");
        ModalService = _$injector_.get("ModalService");

        $controller = _$controller_;

        $modal = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };

        opts = {
            config: {
                refreshCall: function() {}
            }
        };

    }));

    function createController() {
        spyOn(opts.config, "refreshCall").and.returnValue($q.when({ data: {} }));
        $scope = $rootScope.$new();
        ctrl = $controller("TemplatesFormModalController", {
            "$modalInstance": $modal,
            "data": opts
        });
        $scope.ctrl = ctrl;
    }

    describe("ctrl.loadTemplates", function() {
        it("calls the refreshCall", function() {
            createController();
            ctrl.loadTemplates();

            expect(opts.config.refreshCall).toHaveBeenCalled();
        });
    });

    describe("ctrl.onLoadTemplateSuccess", function() {
        it("sets the templates and the template", function() {
            createController();
            ctrl.onLoadTemplateSuccess({ data: { items: [{ id: 1 }] } });

            expect(ctrl.templates.length).toEqual(1);
        });
    });

    describe("ctrl.onLoadTemplatesError", function() {
        it("should call $log error function", function() {
            spyOn($log, "error");

            createController();
            ctrl.onLoadTemplatesError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
        });
    });

    describe("ctrl.dismiss", function() {
        it("should call $modalInstance close function", function() {
            spyOn($modal, "dismiss");

            createController();
            ctrl.template = {};
            ctrl.dismiss();

            expect($modal.dismiss).toHaveBeenCalled();
        });
    });

    describe("ctrl.createOwn", function() {
        it("should call $modalInstance close function", function() {
            spyOn($modal, "close");

            createController();
            ctrl.template = {};
            ctrl.createOwn();

            expect($modal.close).toHaveBeenCalled();
        });
    });

    describe("ctrl.createFromTemplate", function() {
        it("should call $modalInstance close function", function() {
            spyOn($modal, "close");

            createController();
            ctrl.template = {frequency: "Once"};
            ctrl.createFromTemplate();

            expect($modal.close).toHaveBeenCalled();
        });
    });

    describe('TemplatesFormModal', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            TemplatesFormModal.show({ config: {} });

            expect(ModalService.show).toHaveBeenCalled();
        });
    });

});