(function() {
    "use strict";

    angular.module("hb.homebinders.framework.detailsBase", [])
        .factory("BinderItemsDetailsController", [
            "$log",
            "hb.api",
            "Loading",
            "Notify",
            "ModalService",
            "hb.framework.editBase",
            "$stateParams",
            function($log, api, loading, notify, modals, EditBase, $stateParams) {
                var Model = function() {
                    EditBase.call(this);
                    this.$log = $log;
                    this.api = api;
                    this.notify = notify;
                    this.loading = loading;
                    this.tagsCfg = {
                        linkToItems: true
                    };
                    this.docUploader = {
                        fileTypes: "document",
                        hideUploadButton: true,
                        showTable: false,
                        selectButtonLabel: this.resources.docUpload,
                        url: "/api/v1/documents/",
                        multiSelect: true,
                        id: "uploadDocuments",
                        hide_upload: false,
                        uploadOnAdd: true
                    };
                    this.imgUploader = {
                        fileTypes: "image",
                        hideUploadButton: true,
                        showTable: false,
                        selectButtonLabel: this.resources.imageUpload,
                        fileLimit: false,
                        url: "/api/v1/images",
                        multiSelect: true,
                        id: "uploadImages",
                        hide_upload: false,
                        uploadOnAdd: true
                    };
                    this.images = [];
                    this.documents = [];
                    this.itemId = $stateParams.id;
                    // base init for class
                    this.init();
                };

                Model.prototype = Object.create(EditBase.prototype);

                angular.extend(Model.prototype, {
                    init: function() {
                        // make sure the deleteCall is set
                        if (!this.deleteCall) {
                            throw "deleteCall not set";
                        }
                        this.refresh();
                    },

                    // Refresh callback. Updates the item list and totals
                    onRefreshSuccess: function(response) {
                        this.item = response.data;
                        this.populate();
                        this.tagsCfg.binderId = this.item.binder_id;
                        this.docUploader.jwt = this.jwt;
                        this.docUploader.params = {
                            binder_id: this.item.binder_id
                        };
                        this.imgUploader.jwt = this.jwt;
                        this.imgUploader.params = {
                            binder_id: this.item.binder_id
                        };
                        loading.close();
                    },

                    destroy: function() {
                        // confirm the delete with the user
                        modals.confirm({
                            message: this.resources.confirmDelete + this.item[this.nameProperty] + "?",
                            confirm: angular.bind(this, this.destroyConfirmed)
                        });
                    },

                    destroyConfirmed: function() {
                        // delete the item
                        this.deleteCall(this.item.id).then(
                            angular.bind(this, this.onDestroy),
                            angular.bind(this, this.onDestroyError)
                        );
                    },

                    onDestroy: function(response) {
                        this.back();
                    },

                    onDestroyError: function(response) {
                        // couldn't delete. let the user know
                        this.$log.error(response);
                        this.notify.error(response.data);
                    },

                    uploadDocuments: function() {
                        var uploadApi = this.docUploader.api;
                        if (uploadApi && uploadApi.pendingUploads().length > 0) {
                            uploadApi.updateParams(this.uploadParams);
                            this.loading.show(this.resources.uploadingDocuments);
                            // TODO: Hack
                            //setTimeout(function() {
                            uploadApi.uploadFiles();
                            //}, 0);
                        }
                        else {
                            this.loading.close();
                        }
                    },

                    docUploadSuccess: function() {
                        var files = this.docUploader.api.addedFiles();
                        var file = files[files.length - 1];
                        var document = {
                            name: file.name,
                            location: file.location,
                            icon: "fa fa-file"
                        };
                        this.documents.push(document);
                        this.uploadDocuments();
                    },

                    docUploadError: function(response) {
                        this.$log.error(response);
                        this.notify.error(response.data);
                        var files = this.docUploader.api.pendingUploads();
                        this.docUploader.api.remove(files[0]);
                        this.uploadDocuments();
                    },

                    uploadImages: function() {
                        var uploadApi = this.imgUploader.api;
                        if (uploadApi && uploadApi.pendingUploads().length > 0) {
                            uploadApi.updateParams(this.uploadParams);
                            this.loading.show(this.resources.uploadingImages);
                            // TODO: Hack
                            //setTimeout(function() {
                            uploadApi.uploadFiles();
                            //}, 0);
                        }
                        else {
                            this.loading.close();
                        }
                    },

                    imgUploadSuccess: function() {
                        var files = this.imgUploader.api.addedFiles();
                        var file = files[files.length - 1];
                        var image = {
                            src: file.location
                        };
                        this.images.push(image);
                        this.uploadImages();
                    },

                    imgUploadError: function(response) {
                        this.$log.error(response);
                        this.notify.error(response.data);
                        var files = this.imgUploader.api.pendingUploads();
                        this.imgUploader.api.remove(files[0]);
                        this.uploadImages();
                    }
                });

                return Model;
            }

        ]);
})();