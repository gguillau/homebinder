describe("BinderItemsDetailsController", function() {
    var BinderItemsDetailsController,
        TestClass,
        api,
        ModalService;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
        // mock the notify service
        $provide.factory("Notify", function() {
            return {
                error: function() {}
            };
        });
        // mock the Session service
        $provide.factory("Session", function() {
            return {
                getJwt: function() {},
                getLocale: function() {
                    return null;
                },
                setLocale: function() {}
            };
        });

    }));
    // get the base class under test
    beforeEach(inject(function($injector) {
        BinderItemsDetailsController = $injector.get("BinderItemsDetailsController");
        api = $injector.get("hb.api");
        ModalService = $injector.get("ModalService");
    }));

    function createTestClass() {
        TestClass = function() {
            this.nameProperty = "name";
            this.cfg = { item: {} };
            this.resources = {};
            this.tagsCfg = {};
            this.item = {};
            this.itemId = 2;
            this.refreshCall = api.appliance.get;
            BinderItemsDetailsController.call(this);
        };

        TestClass.prototype = Object.create(BinderItemsDetailsController.prototype);

        angular.extend(TestClass.prototype, {
            populate: function() {},
            deleteCall: function() {}
        });

        return new TestClass();
    }

    describe("init", function() {
        it("calls refresh", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            spyOn(model, "refresh");
            model.init();

            expect(model.refresh).toHaveBeenCalled();
        }));
    });

    describe("destroy", function() {
        it("calls modal confirm", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            spyOn(ModalService, "confirm");
            model.destroy();

            expect(ModalService.confirm).toHaveBeenCalled();
        }));
    });

    describe("destroyConfirmed", function() {
        it("calls deleteCall", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            spyOn(model, "deleteCall").and.returnValue($q.when({ data: {} }));
            model.destroyConfirmed();

            expect(model.deleteCall).toHaveBeenCalled();
        }));
    });

    describe("onDestroy", function() {
        it("calls onDestroyed", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();

            model.back = function() {};
            spyOn(model, "back");
            model.onDestroy({ data: {} });

            expect(model.back).toHaveBeenCalled();
        }));
    });

    describe("onDestroyError", function() {
        it("calls notify error", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();

            spyOn(model.notify, "error");
            model.onDestroyError({ data: "error" });

            expect(model.notify.error).toHaveBeenCalled();
        }));
    });

    describe("uploadDocuments", function() {
        it("calls uploadFiles", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            var uploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            spyOn(uploader.api, "uploadFiles");
            spyOn(uploader.api, "updateParams");
            spyOn(uploader.api, "pendingUploads").and.returnValue([{ id: 1 }]);
            model.docUploader = uploader;
            model.uploadDocuments();

            expect(uploader.api.uploadFiles).toHaveBeenCalled();
        }));

        it("calls loading close", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            var uploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            spyOn(uploader.api, "uploadFiles");
            spyOn(uploader.api, "updateParams");
            spyOn(uploader.api, "pendingUploads").and.returnValue([]);
            spyOn(model.loading, "close");
            model.docUploader = uploader;
            model.uploadDocuments();

            expect(model.loading.close).toHaveBeenCalled();
        }));
    });

    describe("docUploadSuccess", function() {
        it("calls uploadDocuments", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();

            spyOn(model, "uploadDocuments");
            model.docUploader = {
                api: {
                    addedFiles: function() { return [{}]; }
                }
            };
            model.docUploadSuccess({ id: 1 });

            expect(model.uploadDocuments).toHaveBeenCalled();
        }));
    });

    describe("docUploadError", function() {
        it("calls notify error", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            var uploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    remove: function() {}
                }
            };
            spyOn(uploader.api, "remove");
            spyOn(uploader.api, "updateParams");
            spyOn(uploader.api, "pendingUploads").and.returnValue([{ id: 1 }]);
            model.docUploader = uploader;
            spyOn(model, "uploadDocuments");
            model.docUploadError({ data: "error" });

            expect(uploader.api.remove).toHaveBeenCalled();
            expect(model.uploadDocuments).toHaveBeenCalled();
        }));
    });

    describe("uploadImages", function() {
        it("calls uploadFiles", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            var uploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            spyOn(uploader.api, "uploadFiles");
            spyOn(uploader.api, "updateParams");
            spyOn(uploader.api, "pendingUploads").and.returnValue([{ id: 1 }]);
            model.imgUploader = uploader;
            model.uploadImages();

            expect(uploader.api.uploadFiles).toHaveBeenCalled();
        }));

        it("calls loading close", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            var uploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            spyOn(uploader.api, "uploadFiles");
            spyOn(uploader.api, "updateParams");
            spyOn(uploader.api, "pendingUploads").and.returnValue([]);
            spyOn(model.loading, "close");
            model.imgUploader = uploader;
            model.uploadImages();

            expect(model.loading.close).toHaveBeenCalled();
        }));
    });

    describe("imgUploadSuccess", function() {
        it("calls uploadImages", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();

            model.imgUploader = {
                api: {
                    addedFiles: function() { return [{}]; }
                }
            };
            spyOn(model, "uploadImages");
            model.imgUploadSuccess({ id: 1 });

            expect(model.uploadImages).toHaveBeenCalled();
        }));
    });

    describe("imgUploadError", function() {
        it("calls notify error", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            var uploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    remove: function() {}
                }
            };
            spyOn(uploader.api, "remove");
            spyOn(uploader.api, "updateParams");
            spyOn(uploader.api, "pendingUploads").and.returnValue([{ id: 1 }]);
            model.imgUploader = uploader;
            spyOn(model, "uploadImages");
            model.imgUploadError({ data: "error" });

            expect(uploader.api.remove).toHaveBeenCalled();
            expect(model.uploadImages).toHaveBeenCalled();
        }));
    });

});