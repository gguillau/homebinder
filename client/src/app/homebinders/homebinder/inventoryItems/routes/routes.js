angular
    .module("hb.homebinders.inventoryItems.routes", [])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            // area index state
            .state("binder.inventory", {
                url: "^/binders/:binderId/inventory_items",
                template: "<binder-inventory-items></binder-inventory-items>",
                access: {
                    requiresLogin: true
                }
            })
            // area details state
            .state("binder.inventoryDetails", {
                url: "^/binders/:binderId/inventory_items/{id:int}",
                template: "<hb-inventory-item-details></hb-inventory-item-details>",
                access: {
                    requiresLogin: true
                }
            })
            // state for new area
            .state("binder.inventoryNew", {
                url: "^/binders/:binderId/inventory_items/new",
                template: "<hb-inventory-item-form></hb-inventory-item-form>",
                access: {
                    requiresLogin: true
                }
            })
            // state for edit area
            .state("binder.inventoryEdit", {
                url: "^/binders/:binderId/inventory_items/{id:int}/edit",
                template: "<hb-inventory-item-form></hb-inventory-item-form>",
                access: {
                    requiresLogin: true
                }
            });
    }]);