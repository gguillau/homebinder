(function() {
	"use strict";

	angular
		.module("hb.homebinders.inventoryItems.details", [])
		.directive("hbInventoryItemDetails", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/inventoryItems/details/details.tpl.html",
				controller: "InventoryItemDetailsController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("InventoryItemDetailsController", [
			"hb.resources",
			"BinderItemsDetailsController",
			"hb.api",
			"$stateParams",
			Controller
		]);

	function Controller(resources, BinderItemsDetailsController, api, $stateParams) {
		var that = this;
		var Model = function() {
			this.cfg = that.cfg;
			this.onDestroyed = that.onDestroyed;
			this.onClose = that.onClose;
			this.resources = angular.extend({}, this.resources, resources.binderInventoryItemsDetails);
			this.nameProperty = "name";
			this.refreshCall = api.inventoryItem.get;
			this.indexState = "binder.inventory";
			this.indexParams = { binderId: $stateParams.binderId };
			this.editState = "binder.inventoryEdit";
			// call the parent class
			BinderItemsDetailsController.call(this);
		};

		Model.prototype = Object.create(BinderItemsDetailsController.prototype);

		angular.extend(Model.prototype, {
			populate: function() {
				this.editParams = { binderId: $stateParams.binderId, id: this.item.id };
				this.tagId = "inventoryItem_" + this.item.id;
				this.uploadParams = {
					binder_id: this.item.binder_id,
					inventory_item_id: this.item.id
				};
			},
			
			deleteCall: function() {
                this.api.inventoryItem.destroy(this.item.id).then(
                    angular.bind(this, this.onDeleteItemSuccess)
                    );
            },
                    
            onDeleteItemSuccess: function(response) {
                this.notify.success(this.resources.deleteSuccess);
                this.back();
            }
		});

		this.model = new Model();
	}
})();