describe("InventoryItemDetailsController", function() {
    var $rootScope,
        $controller,
        resources,
        ctrl,
        BinderItemsIndexController,
        api,
        $log,
        context;


    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    return { data: {} };
                }
            };
        });
    }));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        api = _$injector_.get("hb.api");
        BinderItemsIndexController = _$injector_.get("BinderItemsIndexController");
        resources = _$injector_.get("hb.resources");
        $log = _$injector_.get("$log");
        context = _$injector_.get("Context");
    }));

    function createController() {
        ctrl = $controller("InventoryItemDetailsController", {
            "BinderItemsIndexController": BinderItemsIndexController,
            "hb.resources": resources,
            "hb.api": api,
            "$log": $log,
            "Context": context
        });
        $rootScope.$apply();
    }

    describe('#populate', function() {
        it("sets the item", function() {
            createController();
            var item = { id: 1, binder_id: 1 };
            ctrl.model.item = item;
            ctrl.model.populate();

            expect(ctrl.model.item).toEqual(item);
        });
    });
});

describe('hbInventoryItemDetails', function() {
    var $scope, $compile, api, $q, context, element, session;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        session = _$injector_.get("Session");
        context = _$injector_.get("Context");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.image, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(api.document, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(context, "getBinder").and.returnValue({ data: { permissions: { can_read: false }, property: {} } });
        spyOn(session, "getUser").and.returnValue({ id: 1, role: "admin" });

        $scope.cfg = null;
        $scope.onDestroyed = function() {};
        $scope.onClose = function() {};

        element = $compile('<hb-inventory-item-details cfg="cfg" on-destroyed="onDestroyed" on-close="onClose"></hb-inventory-item-details>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {
            expect(element.html()).toContain("Name");
        });
    });
});