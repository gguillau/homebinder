(function() {
    "use strict";

    angular
        .module("hb.homebinders.inventoryItems.form", [])
        .directive("hbInventoryItemForm", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/inventoryItems/form/form.tpl.html",
                controller: "InventoryItemFormController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        // Model backing the inventoryItem modal form
        .controller("InventoryItemFormController", [
            "hb.resources",
            "BinderItemsFormController",
            "hb.api",
            "$stateParams",
            Controller
        ]);

    function Controller(resources, BinderItemsFormController, api, $stateParams) {
        var that = this;
        var Model = function() {
            this.cfg = that.cfg;
            this.onSaved = that.onSaved;
            this.onCancel = that.onCancel;
            this.resources = angular.extend({}, this.resources, resources.binderInventoryItemsForm);
            this.inventoryItemTypes = [];
            this.taggerCfg = {
                inventoryItemTags: false
            };
            this.tagName = "inventoryItem_";
            // call the parent class;
            this.saveCall = api.inventoryItem.update;
            this.createCall = api.inventoryItem.create;
            this.deleteCall = api.inventoryItem.destroy;
            this.refreshCall = api.inventoryItem.get;
            
            // If $stateParams has an id, we are editing an exisiting item and back should send us to that item's details
            if ($stateParams.id) {
                this.indexState = "binder.inventoryDetails";
                this.indexParams = { binderId: $stateParams.binderId, id: $stateParams.id };
            }
            // Else, we were creating a new item and back should send us to the list of items
            else {
                this.indexState = "binder.inventory";
                this.indexParams = { binderId: $stateParams.binderId };
            }
            this.editState = "binder.inventoryEdit";
            
            BinderItemsFormController.call(this);
            this.loadInventoryItemTypes();
            this.init();
        };

        Model.prototype = Object.create(BinderItemsFormController.prototype);

        angular.extend(Model.prototype, {

            populate: function() {
                // if a item was passed in store the values
                if (this.item && this.item.id) {
                    this.name = this.item.name;
                    this.inventory_item_type = this.item.inventory_item_type;
                    this.value = this.item.number;
                    this.details = this.item.details;
                    this.tags = this.item.tags;
                    this.setUploadParams();
                }
            },

            loadInventoryItemTypes: function() {
                api.inventoryItemType.all().then(
                    angular.bind(this, this.onLoadInventoryItemTypes),
                    angular.bind(this, this.onLoadInventoryItemTypesError)
                );
            },

            onLoadInventoryItemTypes: function(response) {
                this.inventoryItemTypes = response.data;
            },

            onLoadInventoryItemTypesError: function(response) {
                this.$log.error(response);
            },

            setPayload: function() {
                // create payload
                return {
                    binder_id: this.binderId,
                    name: this.name,
                    inventory_item_type: this.inventory_item_type,
                    value: this.value ? this.value : 0,
                    details: this.details,
                    tags_attributes: this.tags
                };
            },

            setUploadParams: function() {
                this.uploadParams = {
                    binder_id: this.item.binder_id,
                    inventory_item_id: this.item.id
                };
            }
        });

        this.model = new Model();
    }

})();