describe("InventoryItemFormController", function() {
    var $rootScope,
        $controller,
        $stateParams,
        resources,
        ctrl,
        BinderItemsIndexController,
        api,
        $q,
        $log;


    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    return { data: {} };
                }
            };
        });
    }));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $stateParams = _$injector_.get("$stateParams");
        BinderItemsIndexController = _$injector_.get("BinderItemsIndexController");
        resources = _$injector_.get("hb.resources");
        $log = _$injector_.get("$log");
    }));

    function createController() {
        ctrl = $controller("InventoryItemFormController", {
            "BinderItemsIndexController": BinderItemsIndexController,
            "hb.resources": resources,
            "hb.api": api,
            "$log": $log,
            "$stateParams": $stateParams
        });
        $rootScope.$apply();
    }

    beforeEach(function() {
        spyOn(api.inventoryItemType, "all").and.returnValue($q.when({ data: { items: [] } }));
    });

    describe('init', function() {
        it("calls inventoryItemType.all", function() {
            $stateParams.binderId = 99;
            createController();

            expect(api.inventoryItemType.all).toHaveBeenCalled();
        });
        
        it("sets the state data when editing an existing item", function() {
            $stateParams = {
                binderId: 1,
                id: 1
            };
            createController();

            expect(ctrl.model.indexState).toEqual("binder.inventoryDetails");
            expect(ctrl.model.indexParams).toEqual({ binderId: 1, id: 1 });
            expect(ctrl.model.editState).toEqual("binder.inventoryEdit");
        });
        
        it("sets the state data when creating a new item", function() {
            $stateParams = {
                binderId: 1
            };
            createController();

            expect(ctrl.model.indexState).toEqual("binder.inventory");
            expect(ctrl.model.indexParams).toEqual({ binderId: 1 });
            expect(ctrl.model.editState).toEqual("binder.inventoryEdit");
        });
    });

    describe('#populate', function() {
        it("doesnt set the item", function() {
            createController();
            ctrl.model.populate();

            expect(ctrl.model.item).toEqual({});
        });

        it("sets the item", function() {
            createController();
            ctrl.model.cfg = { item: { id: 1, binder_id: 1, name: "test" } };
            ctrl.model.item = ctrl.model.cfg.item;
            ctrl.model.populate();

            expect(ctrl.model.name).toEqual(ctrl.model.cfg.item.name);
        });
    });

    describe('#onLoadInventoryItemTypes', function() {
        it("sets the inventoryItemTypes", function() {

            createController();
            ctrl.model.onLoadInventoryItemTypes({ data: [{ id: 1 }] });

            expect(ctrl.model.inventoryItemTypes.length).toEqual(1);
        });
    });

    describe('#onLoadInventoryItemTypesError', function() {
        it("calls $log error", function() {
            spyOn($log, "error");

            createController();
            ctrl.model.onLoadInventoryItemTypesError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
        });
    });

    describe('#setPayload', function() {
        it("returns the payload", function() {
            createController();
            ctrl.model.name = "test";
            var data = ctrl.model.setPayload();

            expect(data.name).toEqual("test");
        });
    });

});

describe('hbInventoryItemForm', function() {
    var $scope, $compile, api, $q, context;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        context = _$injector_.get("Context");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.inventoryItemType, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(context, "getBinder").and.returnValue({ data: { id: 1, permissions: { can_read: false } } });

        $scope.cfg = { binderId: 1 };
        $scope.onSaved = function() {};
        $scope.onCancel = function() {};

        $compile('<hb-inventory-item-form cfg="cfg" on-saved="onSaved" on-cancel="onCancel"></hb-inventory-item-form>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls inventoryItemType all', function() {
            expect(api.inventoryItemType.all).toHaveBeenCalled();
        });
    });
});