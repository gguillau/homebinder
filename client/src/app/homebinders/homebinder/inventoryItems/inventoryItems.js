(function() {
    "use strict";

    angular
        .module("hb.homebinders.inventoryItems", [
            "hb.homebinders.inventoryItems.routes",
            "hb.homebinders.inventoryItems.details",
            "hb.homebinders.inventoryItems.form",
            "hb.homebinders.inventoryItems.modal"
        ])
        .directive("binderInventoryItems", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/inventoryItems/inventoryItems.tpl.html",
                controller: "BinderInventoryItemsController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("BinderInventoryItemsController", [
            "BinderItemsIndexController",
            "hb.resources",
            Controller
        ]);

    function Controller(BinderItemsIndexController, resources) {
        var Model = function() {
            // call the parent class
            BinderItemsIndexController.call(this);
            this.resources = angular.extend({}, this.resources, resources.binderInventoryItemsIndex);
            this.refreshCall = this.api.inventoryItem.all;
            this.itemType = "inventoryItems";
            this.modalController = "InventoryItemModalController";
            this.itemNewState = "binder.inventoryNew";
            this.itemDetailsState = "binder.inventoryDetails";
            this.init();
        };

        Model.prototype = Object.create(BinderItemsIndexController.prototype);

        this.model = new Model();
    }
})();