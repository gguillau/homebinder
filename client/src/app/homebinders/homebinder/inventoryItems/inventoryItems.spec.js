describe("BinderInventoryItemsController", function() {
    var controller,
        base,
        resources,
        $q_,
        defer;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q_.defer();
                    defer.resolve({});
                    return defer.promise;
                }
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        $q_ = $q;
        base = $injector.get("BinderItemsIndexController");
        resources = $injector.get("hb.resources");

        controller = $controller("BinderInventoryItemsController", {
            "BinderItemsIndexController": base,
            "hb.resources": resources
        });
    }));

    describe("init", function() {
        it("sets the controller api call", function() {
            expect(controller.model.refreshCall).toEqual(controller.model.api.inventoryItem.all);
        });
    });

});

describe('binderInventoryItems', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;

        spyOn(api.inventoryItem, "all").and.returnValue($q.when({ data: { items: [] } }));

        $compile('<binder-inventory-items></binder-inventory-items>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls api inventoryItem all', function() {
            expect(api.inventoryItem.all).toHaveBeenCalled();
        });
    });
});