(function() {
    "use strict";
    
    angular
        .module("hb.homebinders.sellerReports", [
            "ui.router",
            "ui.bootstrap",
            "hb.api",
            "hb.components",
            "hb.homebinders.sellerReports.details"
        ]);
})();