(function() {
    "use strict";

    angular
        .module("hb.homebinders.sellerReports.details", [
            "ui.router",
            "ui.bootstrap",
            "hb.components",
            "hb.homebinders.sellerReports.form"
        ])
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state("seller_report", {
                    url: "^/sellerreport/:code",
                    templateUrl: "homebinders/homebinder/sellerReports/sellerReport/sellerReport.tpl.html",
                    controller: "SellerReportCtrl",
                    controllerAs: "ctrl"
                });
        }])
        .controller("SellerReportCtrl", [
            "$location",
            "$anchorScroll",
            "$stateParams",
            "hb.api",
            "Notify",
            "$log",
            "Loading",
            "$window",
            "hb.utils",
            "hb.resources",
            SellerReportCtrl
        ]);

    function SellerReportCtrl($location, $anchorScroll, $stateParams, api, notify, $log, loading, $window, utils, resources) {
        this.$location = $location;
        this.$anchorScroll = $anchorScroll;
        this.code = $stateParams['code'].toUpperCase();
        this.api = api;
        this.notify = notify;
        this.$log = $log;
        this.loading = loading;
        this.$window = $window;
        this.utils = utils;
        this.resources = resources.sellerReport;
        this.today = this.utils.utils.getToday(true);
        this.data = {};
        this.is_public = false;
        this.response_text = null;
        this.refresh();
    }

    SellerReportCtrl.prototype = {

        refresh: function() {
            this.loading.show(this.resources.loading + "...");
            this.api.sellerReport.get(this.code).then(
                angular.bind(this, this.getReportSuccess),
                angular.bind(this, this.getReportError));
        },

        getReportSuccess: function(response) {
            if (response.data.public) {
                this.is_public = true;
                this.data.show_project_costs = response.data.show_project_costs;
                this.data.binder = response.data.binder;
                this.data.appliances = response.data.appliances;
                this.data.contractors = response.data.binder_contractors;
                this.data.maintenance = response.data.maintenance_items;
                this.data.improvements = response.data.projects;
                this.data.paints = response.data.paints;
                this.data.permits = response.data.permits;
                this.data.finishes = response.data.finishes;
                this.data.documents = response.data.documents;
                this.data.images = response.data.images;
                this.data.binder.logo_url = response.data.seller_report_logo ?
                    response.data.seller_report_logo.user ?
                    response.data.seller_report_logo.user.logo_file : undefined : undefined;
                    
                this.data.improvements.forEach(function(project){
                    project.cost_cents = Number(project.cost_cents / 100).toFixed(2); 
                });
            }
            else {
                this.data.binder = response.data.binder;
                this.response_text = this.resources.responseText1 + " " + this.data.binder.property.address1 + " " + this.data.binder.property.city + ", " + this.data.binder.property.state + " " + this.resources.responseText2;
            }
            this.loading.close();
        },

        getReportError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.loading.close();
        },

        download: function(value) {
            this.loading.show(this.resources.downloading + "...");
            this.api.sellerReportPDF.download(this.code, value).then(
                angular.bind(this, this.downloadSuccess, value),
                angular.bind(this, this.downloadError));
        },

        downloadSuccess: function(value, response) {
            var file = new Blob([response.data], {
                type: "application/pdf"
            }, {
                encoding: "raw"
            });
            var name = value ? "HomeBinder_Full_Sellers_Report_" : "HomeBinder_Sellers_Report_";
            var filename = name + this.data.binder.name + "_" + this.today + ".pdf";
            if (this.$window.navigator.msSaveOrOpenBlob) {
                this.$window.navigator.msSaveOrOpenBlob(file, filename);
            }
            else {
                var fileURL = this.$window.URL.createObjectURL(file);
                var a = document.createElement("a");
                a.href = fileURL;
                a.target = "_blank";
                a.download = filename;
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
                this.$window.URL.revokeObjectURL(file);
            }
            this.loading.close();
        },

        downloadError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.loading.close();
        }

    };

})();