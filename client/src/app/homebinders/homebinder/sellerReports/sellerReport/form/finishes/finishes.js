(function() {
	"use strict";

	angular
		.module("hb.homebinders.sellerReports.form.finishes", [])
		.directive("sellerReportFormFinishes", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/sellerReports/sellerReport/form/finishes/finishes.tpl.html",
				controller: "SellerReportFormFinishesController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {
					finishes: "=",
					selectAll: "&",
					allFinishes: "=",
					binderId: "="
				}
			};
		})
		.controller("SellerReportFormFinishesController", [
			'hb.api',
			'Loading',
			'$log',
			'Notify',
			'hb.resources',
			SellerReportFormFinishesController
		]);

	function SellerReportFormFinishesController(api, loading, $log, notify, resources) {
		this.api = api;
		this.$log = $log;
		this.loading = loading;
		this.notify = notify;
		this.is_finishes_collapsed = false;
		this.resources = resources.sellerReport;
		this.resources = angular.extend({}, this.resources, resources.sellerReportForm);
		this.finishes = [];
	}

	SellerReportFormFinishesController.prototype = {

		destroy: function(finish) {
			var index = this.finishes.indexOf(finish);
			var fin = this.finishes[index];
			this.loading.show(this.resources.removing + "...");
			this.api.finish.destroy(fin.id).then(
				angular.bind(this, this.onRemoveSuccess, index),
				angular.bind(this, this.onRemoveError)
			);
		},

		onRemoveSuccess: function(index, response) {
			this.finishes.splice(index, 1);
			this.notify.success(this.resources.removed);
			this.loading.close();
		},

		onRemoveError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		setInclude: function(finish) {
			finish.seller_report_item.include = !finish.seller_report_item.include;
		},

		change: function() {
			this.allFinishes = !this.allFinishes;
			this.selectAll(this.finishes, this.allFinishes);
		}
	};

})();