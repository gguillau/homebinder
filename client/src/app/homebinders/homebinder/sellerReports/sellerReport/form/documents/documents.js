(function() {
	"use strict";

	angular
		.module("hb.homebinders.sellerReports.form.documents", [])
		.directive("sellerReportFormDocuments", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/sellerReports/sellerReport/form/documents/documents.tpl.html",
				controller: "SellerReportFormDocumentsController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {
					documents: "=",
					selectAll: "&",
					allDocuments: "=",
					binderId: "="
				}
			};
		})
		.controller("SellerReportFormDocumentsController", [
			'hb.api',
			'Loading',
			'$log',
			'Notify',
			'hb.resources',
			SellerReportFormDocumentsController
		]);

	function SellerReportFormDocumentsController(api, loading, $log, notify, resources) {
		this.api = api;
		this.$log = $log;
		this.loading = loading;
		this.notify = notify;
		this.is_documents_collapsed = false;
		this.resources = resources.sellerReport;
		this.resources = angular.extend({}, this.resources, resources.sellerReportForm);
		this.documents = [];
	}

	SellerReportFormDocumentsController.prototype = {
		
		destroy: function(docu) {
			var index = this.documents.indexOf(docu);
			var doc = this.documents[index];
			this.loading.show(this.resources.removing + "...");
			this.api.document.destroy(doc.id).then(
				angular.bind(this, this.onRemoveSuccess, index),
				angular.bind(this, this.onRemoveError)
			);
		},

		onRemoveSuccess: function(index, response) {
			this.documents.splice(index, 1);
			this.notify.success(this.resources.removed);
			this.loading.close();
		},

		onRemoveError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},
		
		setInclude: function(doc){
			doc.seller_report_item.include = !doc.seller_report_item.include;
		},
		
		change: function(){
			this.allDocuments = !this.allDocuments;
			this.selectAll(this.documents, this.allDocuments);
		}
	};

})();