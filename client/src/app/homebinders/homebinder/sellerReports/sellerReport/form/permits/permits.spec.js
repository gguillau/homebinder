describe("SellerReportFormPermitsController", function() {

    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $q,
        api,
        loading,
        notify,
        $log,
        selectAll;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");

        $log = {
            error: function() {}
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        notify = {
            success: function(msg) {},
            error: function(msg) {}
        };

        api = {
            permit: {
                destroy: function(id) {}
            }
        };

        selectAll = function(array, select) {};

    }));


    function createController(boolean_value) {
        $scope = $rootScope.$new();
        ctrl = $controller("SellerReportFormPermitsController", {
            "hb.api": api,
            "Loading": loading,
            "$log": $log,
            "Notify": notify
        }, {
            allPermits: boolean_value,
            selectAll: selectAll
        });

        $scope.ctrl = ctrl;
        $scope.$apply();
        $scope.$digest();

    }

    describe('ctrl.destroy', function() {

        it('should remove permit at index 0 from permits array', function() {

            spyOn(api.permit, "destroy").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(notify, "success");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            var permit = {
                id: 1,
                name: "Red"
            };
            ctrl.permits.push(permit);
            $rootScope.$apply();
            spyOn(ctrl.permits, "indexOf").and.callThrough();
            spyOn(ctrl.permits, "splice").and.callThrough();
            ctrl.destroy(ctrl.permits[0]);
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalledWith("Removing Red from binder...");
            expect(api.permit.destroy).toHaveBeenCalledWith(1);
            expect(notify.success).toHaveBeenCalledWith("Permit deleted.");
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.permits.length).toEqual(0);
            expect(ctrl.permits.indexOf).toHaveBeenCalledWith(permit);
            expect(ctrl.permits.splice).toHaveBeenCalledWith(0, 1);
        });

        it('should attempt to remove permit from index 0 and destroy permit but return an error', function() {

            spyOn(api.permit, "destroy").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            var permit = {
                id: 1,
                name: "Red"
            };
            ctrl.permits.push(permit);
            $rootScope.$apply();
            ctrl.destroy(ctrl.permits[0]);
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalledWith("Removing Red from binder...");
            expect(api.permit.destroy).toHaveBeenCalledWith(1);
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.close).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(ctrl.permits.length).toEqual(1);
        });

    });

    describe('ctrl.setInclude', function() {

        it('should set include to true', function() {

            createController();
            var permit = {
                id: 1,
                name: "Red",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.permits.push(permit);
            $rootScope.$apply();
            ctrl.setInclude(ctrl.permits[0]);
            $rootScope.$apply();

            expect(ctrl.permits[0].seller_report_item.include).toBe(true);

        });

        it('should set include to true', function() {

            createController();
            var permit = {
                id: 1,
                name: "Red",
                seller_report_item: {
                    include: true
                }
            };
            ctrl.permits.push(permit);
            $rootScope.$apply();
            ctrl.setInclude(ctrl.permits[0]);
            $rootScope.$apply();

            expect(ctrl.permits[0].seller_report_item.include).toBe(false);

        });

    });

    describe('ctrl.change', function() {

        it('should set allPermits to false', function() {

            createController(true);
            var permit = {
                id: 1,
                name: "Red",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.permits.push(permit);
            $rootScope.$apply();
            spyOn(ctrl, "selectAll");
            ctrl.change();
            $rootScope.$apply();

            expect(ctrl.allPermits).toBe(false);
            expect(ctrl.selectAll).toHaveBeenCalledWith(ctrl.permits, false);

        });

        it('should set allPermits to true', function() {

            createController(false);
            var permit = {
                id: 1,
                name: "Red",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.permits.push(permit);
            $rootScope.$apply();
            spyOn(ctrl, "selectAll");
            ctrl.change();
            $rootScope.$apply();
            expect(ctrl.allPermits).toBe(true);
            expect(ctrl.selectAll).toHaveBeenCalledWith(ctrl.permits, true);

        });


    });

});

describe('sellerReportFormPermits', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        $scope.permits = [];
        $scope.selectAll = function() {};
        $scope.allPermits = false;
        $scope.binderId = 1;

        element = $compile('<seller-report-form-permits permits="permits" select-all="selectAll" all-permits="allPermits" binder-id="binderId"></seller-report-form-permits>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the elements', function() {
            expect(element.html()).toContain("Permits");
        });
    });
});