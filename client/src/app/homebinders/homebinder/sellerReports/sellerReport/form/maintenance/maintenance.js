(function() {
	"use strict";

	angular
		.module("hb.homebinders.sellerReports.form.maintenance", [])
		.directive("sellerReportFormMaintenance", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/sellerReports/sellerReport/form/maintenance/maintenance.tpl.html",
				controller: "SellerReportFormMaintenanceController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {
					items: "=",
					selectAll: "&",
					allItems: "=",
					binderId: "="
				}
			};
		})
		.controller("SellerReportFormMaintenanceController", [
			'hb.api',
			'Loading',
			'$log',
			'Notify',
			'hb.resources',
			SellerReportFormMaintenanceController
		]);

	function SellerReportFormMaintenanceController(api, loading, $log, notify, resources) {
		this.api = api;
		this.$log = $log;
		this.loading = loading;
		this.notify = notify;
		this.resources = resources.sellerReport;
		this.resources = angular.extend({}, this.resources, resources.sellerReportForm);
		this.is_items_collapsed = false;
		this.items = [];
	}

	SellerReportFormMaintenanceController.prototype = {

		destroy: function(item) {
			var index = this.items.indexOf(item);
			var mi = this.items[index];
			this.loading.show(this.resources.removing + "...");
			this.api.maintenanceItem.destroy(mi.id).then(
				angular.bind(this, this.onRemoveSuccess, index),
				angular.bind(this, this.onRemoveError)
			);
		},

		onRemoveSuccess: function(index, response) {
			this.items.splice(index, 1);
			this.notify.success(this.resources.removed);
			this.loading.close();
		},

		onRemoveError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		setInclude: function(maintenance_item) {
			maintenance_item.seller_report_item.include = !maintenance_item.seller_report_item.include;
		},

		change: function() {
			this.allItems = !this.allItems;
			this.selectAll(this.items, this.allItems);
		}
	};

})();