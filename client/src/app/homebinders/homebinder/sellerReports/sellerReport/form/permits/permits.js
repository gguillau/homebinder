(function() {
	"use strict";

	angular
		.module("hb.homebinders.sellerReports.form.permits", [])
		.directive("sellerReportFormPermits", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/sellerReports/sellerReport/form/permits/permits.tpl.html",
				controller: "SellerReportFormPermitsController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {
					permits: "=",
					selectAll: "&",
					allPermits: "=",
					binderId: "="
				}
			};
		})
		.controller("SellerReportFormPermitsController", [
			'hb.api',
			'Loading',
			'$log',
			'Notify',
			'hb.resources',
			SellerReportFormPermitsController
		]);

	function SellerReportFormPermitsController(api, loading, $log, notify, resources) {
		this.api = api;
		this.$log = $log;
		this.loading = loading;
		this.notify = notify;
		this.resources = resources.sellerReport;
		this.resources = angular.extend({}, this.resources, resources.sellerReportForm);
		this.is_permits_collapsed = false;
		this.permits = [];
	}

	SellerReportFormPermitsController.prototype = {

		destroy: function(permit) {
			var index = this.permits.indexOf(permit);
			var pt = this.permits[index];
			this.loading.show("Removing " + pt.name + " from binder...");
			this.api.permit.destroy(pt.id).then(
				angular.bind(this, this.onRemoveSuccess, index),
				angular.bind(this, this.onRemoveError)
			);
		},

		onRemoveSuccess: function(index, response) {
			this.permits.splice(index, 1);
			this.notify.success("Permit deleted.");
			this.loading.close();
		},

		onRemoveError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		setInclude: function(permit) {
			permit.seller_report_item.include = !permit.seller_report_item.include;
		},

		change: function() {
			this.allPermits = !this.allPermits;
			this.selectAll(this.permits, this.allPermits);
		}
	};

})();