describe("SellerReportFormCtrl", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $state,
        $stateParams,
        $anchorScroll,
        notify,
        $log,
        api,
        Session,
        $window,
        loading;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        $state = _$injector_.get("$state");
        $stateParams = _$injector_.get("$stateParams");
        api = _$injector_.get("hb.api");
        Session = _$injector_.get("Session");
        notify = _$injector_.get("Notify");
        $window = _$injector_.get("$window");
        $anchorScroll = _$injector_.get("$anchorScroll");
        loading = _$injector_.get("Loading");
        $log = _$injector_.get("$log");
        $stateParams = {};

    }));

    function createController() {
        $scope = $rootScope.$new();
        $stateParams.code = "FSA92A";
        spyOn(api.sellerReport, "get").and.returnValue($q.reject({
            data: {}
        }));
        ctrl = $controller("SellerReportFormCtrl", {
            "$state": $state,
            "$stateParams": $stateParams,
            "hb.api": api,
            "$log": $log,
            "Session": Session,
            "Notify": notify,
            "$window": $window,
            "$anchorScroll": $anchorScroll,
            "Loading": loading
        });
        $scope.ctrl = ctrl;

    }

    describe('ctrl.refresh', function() {
        it('should call api sellerReport get', function() {
            createController();

            expect(api.sellerReport.get).toHaveBeenCalledWith("FSA92A", true);
        });
    });

    describe('ctrl.onRefreshSuccess', function() {
        it('should call api binder get', function() {
            spyOn(api.binder, "get").and.returnValue($q.when({ data: {} }));

            createController();
            ctrl.onRefreshSuccess({ data: { binder: { id: 1 } } });

            expect(api.binder.get).toHaveBeenCalled();
        });
    });

    describe('ctrl.onRefreshError', function() {
        it('calls notify error', function() {
            spyOn(notify, "error");

            createController();
            ctrl.onRefreshError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe('ctrl.getBinderSuccess', function() {
        it('sets the binder and calls loadComponents', function() {
            createController();
            spyOn(ctrl, "loadComponents");
            ctrl.data = {
                report: {

                }
            };
            ctrl.getBinderSuccess({ data: { id: 1 } });

            expect(ctrl.loadComponents).toHaveBeenCalled();
        });
    });

    describe('ctrl.getBinderError', function() {
        it('calls notify error', function() {
            spyOn(notify, "error");

            createController();
            ctrl.getBinderError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe('ctrl.loadComponents', function() {
        it('gets binder items for seller report binder', function() {
            spyOn(api.finish, "all").and.returnValue($q.when({
                data: { items: [] }
            }));
            spyOn(api.paint, "all").and.returnValue($q.when({
                data: { items: [] }
            }));
            spyOn(api.permit, "all").and.returnValue($q.when({
                data: { items: [] }
            }));
            spyOn(api.binderContractor, "all").and.returnValue($q.when({
                data: { items: [] }
            }));
            spyOn(api.project, "all").and.returnValue($q.when({
                data: { items: [] }
            }));
            spyOn(api.maintenanceItem, "all").and.returnValue($q.when({
                data: { items: [] }
            }));
            spyOn(api.appliance, "all").and.returnValue($q.when({
                data: { items: [] }
            }));
            spyOn(api.image, "all").and.returnValue($q.when({
                data: { items: [] }
            }));
            spyOn(api.document, "all").and.returnValue($q.when({
                data: { items: [] }
            }));

            createController();
            ctrl.data = {
                report: {
                    binder: {
                        id: 1
                    }
                }
            };
            ctrl.loadComponents();

            expect(api.finish.all).toHaveBeenCalled();
            expect(api.paint.all).toHaveBeenCalled();
            expect(api.permit.all).toHaveBeenCalled();
            expect(api.binderContractor.all).toHaveBeenCalled();
            expect(api.project.all).toHaveBeenCalled();
            expect(api.maintenanceItem.all).toHaveBeenCalled();
            expect(api.appliance.all).toHaveBeenCalled();
            expect(api.image.all).toHaveBeenCalled();
            expect(api.document.all).toHaveBeenCalled();
        });
    });

    describe('ctrl.getDocumentSuccess', function() {
        it('sets the documents', function() {
            createController();
            ctrl.data = {};
            ctrl.getDocumentSuccess({ data: { items: [{ id: 1, seller_report_item: null }] } });

            expect(ctrl.data.documents.length).toEqual(1);
        });
    });

    describe('ctrl.getImageSuccess', function() {
        it('sets the images', function() {
            createController();
            ctrl.data = {};
            ctrl.getImageSuccess({ data: { items: [{ id: 1, seller_report_item: null }] } });

            expect(ctrl.data.images.length).toEqual(1);
        });
    });

    describe('ctrl.getApplianceSuccess', function() {
        it('sets the appliances', function() {
            createController();
            ctrl.data = {};
            ctrl.getApplianceSuccess({ data: { items: [{ id: 1, seller_report_item: null }] } });

            expect(ctrl.data.appliances.length).toEqual(1);
        });
    });

    describe('ctrl.getMaintenanceSuccess', function() {
        it('sets the maintenance items', function() {
            createController();
            ctrl.data = {};
            ctrl.getMaintenanceSuccess({ data: { items: [{ id: 1, seller_report_item: null }] } });

            expect(ctrl.data.maintenance.length).toEqual(1);
        });
    });

    describe('ctrl.getProjectSuccess', function() {
        it('sets the improvements', function() {
            createController();
            ctrl.data = {};
            ctrl.getProjectSuccess({ data: { items: [{ id: 1, seller_report_item: null }] } });

            expect(ctrl.data.improvements.length).toEqual(1);
        });
    });

    describe('ctrl.getContractorSuccess', function() {
        it('sets the contractors', function() {
            createController();
            ctrl.data = {};
            ctrl.getContractorSuccess({ data: { items: [{ id: 1, seller_report_item: null }] } });

            expect(ctrl.data.contractors.length).toEqual(1);
        });
    });

    describe('ctrl.getPaintSuccess', function() {
        it('sets the paints', function() {
            createController();
            ctrl.data = {};
            ctrl.getPaintSuccess({ data: { items: [{ id: 1, seller_report_item: null }] } });

            expect(ctrl.data.paints.length).toEqual(1);
        });
    });

    describe('ctrl.getPermitSuccess', function() {
        it('sets the permits', function() {
            createController();
            ctrl.data = {};
            ctrl.getPermitSuccess({ data: { items: [{ id: 1, seller_report_item: null }] } });

            expect(ctrl.data.permits.length).toEqual(1);
        });
    });

    describe('ctrl.getFinishSuccess', function() {
        it('sets the finishes', function() {
            createController();
            ctrl.data = {};
            ctrl.getFinishSuccess({ data: { items: [{ id: 1, seller_report_item: null }] } });

            expect(ctrl.data.finishes.length).toEqual(1);
        });
    });

    describe('ctrl.getItemError', function() {
        it('calls notify', function() {
            createController();
            spyOn(notify, "error");
            ctrl.getItemError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe('ctrl.closeLoading', function() {
        it('calls loading close', function() {
            createController();
            spyOn(loading, "close");
            ctrl.closeLoading();

            expect(loading.close).toHaveBeenCalled();
        });
    });

    describe('ctrl.save', function() {
        it('calls binder update with cancel option', function() {
            spyOn(api.binder, "update").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.data = {
                binder: {
                    id: 1
                },
                report: {

                },
                documents: [{ seller_report_item: { include: true } }],
                images: [{ seller_report_item: { include: true } }],
                appliances: [{ seller_report_item: { include: true } }],
                maintenance: [{ seller_report_item: { include: true } }],
                improvements: [{ seller_report_item: { include: true } }],
                contractors: [{ seller_report_item: { include: true } }],
                paints: [{ seller_report_item: { include: true } }],
                permits: [{ seller_report_item: { include: true } }],
                finishes: [{ seller_report_item: { include: true } }]
            };
            ctrl.save("cancel");

            expect(api.binder.update).toHaveBeenCalled();
        });

        it('calls binder update with download option', function() {
            spyOn(api.binder, "update").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.data = {
                binder: {
                    id: 1
                },
                report: {

                },
                documents: [{ seller_report_item: { include: true } }],
                images: [{ seller_report_item: { include: true } }],
                appliances: [{ seller_report_item: { include: true } }],
                maintenance: [{ seller_report_item: { include: true } }],
                improvements: [{ seller_report_item: { include: true } }],
                contractors: [{ seller_report_item: { include: true } }],
                paints: [{ seller_report_item: { include: true } }],
                permits: [{ seller_report_item: { include: true } }],
                finishes: [{ seller_report_item: { include: true } }]
            };
            ctrl.save("download");

            expect(api.binder.update).toHaveBeenCalled();
        });

        it('calls binder update with description option', function() {
            spyOn(api.binder, "update").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.data = {
                binder: {
                    id: 1
                },
                report: {

                },
                documents: [{ seller_report_item: { include: true } }],
                images: [{ seller_report_item: { include: true } }],
                appliances: [{ seller_report_item: { include: true } }],
                maintenance: [{ seller_report_item: { include: true } }],
                improvements: [{ seller_report_item: { include: true } }],
                contractors: [{ seller_report_item: { include: true } }],
                paints: [{ seller_report_item: { include: true } }],
                permits: [{ seller_report_item: { include: true } }],
                finishes: [{ seller_report_item: { include: true } }]
            };
            ctrl.save("description");

            expect(api.binder.update).toHaveBeenCalled();
        });

        it('calls binder update with is_view_save option', function() {
            spyOn(api.binder, "update").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.data = {
                binder: {
                    id: 1
                },
                report: {

                },
                documents: [{ seller_report_item: { include: true } }],
                images: [{ seller_report_item: { include: true } }],
                appliances: [{ seller_report_item: { include: true } }],
                maintenance: [{ seller_report_item: { include: true } }],
                improvements: [{ seller_report_item: { include: true } }],
                contractors: [{ seller_report_item: { include: true } }],
                paints: [{ seller_report_item: { include: true } }],
                permits: [{ seller_report_item: { include: true } }],
                finishes: [{ seller_report_item: { include: true } }]
            };
            ctrl.save("is_view_save");

            expect(api.binder.update).toHaveBeenCalled();
        });
    });

    describe('ctrl.updateSuccess', function() {
        it('calls updateImagesandDocsn', function() {
            createController();
            spyOn(ctrl, "updateImagesandDocs");
            ctrl.updateSuccess({}, { data: {} });

            expect(ctrl.updateImagesandDocs).toHaveBeenCalled();
        });
    });

    describe('ctrl.updateError', function() {
        it('calls notify', function() {
            createController();
            spyOn(notify, "error");
            ctrl.updateError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe('ctrl.updateImagesandDocs', function() {
        it('calls updateDocsAndImages', function() {
            spyOn(api.sellerReport, "updateDocsAndImages").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.data = {
                report: {
                    id: 1
                }
            };
            ctrl.updateImagesandDocs({});

            expect(api.sellerReport.updateDocsAndImages).toHaveBeenCalled();
        });
    });

    describe('ctrl.saveComplete', function() {
        it('calls cancel', function() {
            createController();
            spyOn(ctrl, "cancel");
            ctrl.is_cancel_save = true;
            ctrl.saveComplete({}, { data: {} });

            expect(ctrl.cancel).toHaveBeenCalled();
        });

        it('calls download', function() {
            createController();
            spyOn(ctrl, "download");
            ctrl.is_download_save = true;
            ctrl.saveComplete({}, { data: {} });

            expect(ctrl.download).toHaveBeenCalled();
        });

        it('calls $window open', function() {
            createController();
            spyOn(ctrl.$window, "open");
            ctrl.is_view_save = true;
            ctrl.saveComplete({ public: true }, { data: {} });

            expect(ctrl.$window.open).toHaveBeenCalled();
        });
    });

    describe('ctrl.saveError', function() {
        it('calls notify', function() {
            createController();
            spyOn(notify, "error");
            spyOn(ctrl, "saveComplete");
            ctrl.saveError({}, { data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe('ctrl.cancel', function() {
        it('should cancel the form and send user to binder overview page', function() {
            spyOn($state, "go");

            createController();
            ctrl.data = {
                report: {
                    binder: {

                    }
                }
            };
            ctrl.cancel();

            expect($state.go).toHaveBeenCalled();
        });
    });

    describe('ctrl.download', function() {
        it('calls sellerReportPDF download', function() {
            spyOn(api.sellerReportPDF, "download").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.data = {
                report: {
                    id: 1
                }
            };
            ctrl.download(true);

            expect(api.sellerReportPDF.download).toHaveBeenCalled();
        });
    });

    describe('ctrl.downloadSuccess', function() {
        it('calls loading close', function() {
            createController();
            spyOn(loading, "close");
            ctrl.data = {
                binder: {
                    name: "Test"
                }
            };
            ctrl.downloadSuccess(true, { data: {} });

            expect(loading.close).toHaveBeenCalled();
        });

        it('calls msSaveOrOpenBlob', function() {
            createController();
            spyOn(loading, "close");
            ctrl.data = {
                binder: {
                    name: "Test"
                }
            };
            ctrl.$window = {
                navigator: {
                    msSaveOrOpenBlob: function() {}
                }
            };
            ctrl.downloadSuccess(true, { data: {} });

            expect(loading.close).toHaveBeenCalled();
        });
    });

    describe('ctrl.downloadError', function() {
        it('calls notify', function() {
            createController();
            spyOn(notify, "error");
            ctrl.downloadError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe('ctrl.selectAll', function() {
        it('sets the include attribute to true for all items', function() {
            createController();
            ctrl.data = {
                binder: {
                    id: 1
                },
                report: {

                },
                everything: true,
                allAppliances: false,
                allMaintenance: false,
                allImprovements: false,
                allContractors: false,
                allPaints: false,
                allPermits: false,
                allFinishes: false,
                allDocuments: false,
                allImages: false,
                documents: [{ seller_report_item: { include: false } }],
                images: [{ seller_report_item: { include: false } }],
                appliances: [{ seller_report_item: { include: false } }],
                maintenance: [{ seller_report_item: { include: false } }],
                improvements: [{ seller_report_item: { include: false } }],
                contractors: [{ seller_report_item: { include: false } }],
                paints: [{ seller_report_item: { include: false } }],
                permits: [{ seller_report_item: { include: false } }],
                finishes: [{ seller_report_item: { include: false } }]
            };

            expect(ctrl.data.documents[0].seller_report_item.include).toBe(false);
            expect(ctrl.data.images[0].seller_report_item.include).toBe(false);
            expect(ctrl.data.appliances[0].seller_report_item.include).toBe(false);
            expect(ctrl.data.maintenance[0].seller_report_item.include).toBe(false);
            expect(ctrl.data.improvements[0].seller_report_item.include).toBe(false);
            expect(ctrl.data.contractors[0].seller_report_item.include).toBe(false);
            expect(ctrl.data.paints[0].seller_report_item.include).toBe(false);
            expect(ctrl.data.permits[0].seller_report_item.include).toBe(false);
            expect(ctrl.data.finishes[0].seller_report_item.include).toBe(false);

            ctrl.selectAll();

            expect(ctrl.data.documents[0].seller_report_item.include).toBe(true);
            expect(ctrl.data.images[0].seller_report_item.include).toBe(true);
            expect(ctrl.data.appliances[0].seller_report_item.include).toBe(true);
            expect(ctrl.data.maintenance[0].seller_report_item.include).toBe(true);
            expect(ctrl.data.improvements[0].seller_report_item.include).toBe(true);
            expect(ctrl.data.contractors[0].seller_report_item.include).toBe(true);
            expect(ctrl.data.paints[0].seller_report_item.include).toBe(true);
            expect(ctrl.data.permits[0].seller_report_item.include).toBe(true);
            expect(ctrl.data.finishes[0].seller_report_item.include).toBe(true);
        });
    });

    describe('ctrl.uploadImage', function() {
        it('calls uploadFiles', function() {
            createController();
            ctrl.uploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            ctrl.binder = { id: 1 };
            spyOn(ctrl.uploader.api, "pendingUploads").and.returnValue([{ id: 1 }]);
            spyOn(ctrl.uploader.api, "uploadFiles");
            ctrl.uploadImage();

            expect(ctrl.uploader.api.uploadFiles).toHaveBeenCalled();
        });
    });

    describe('ctrl.uploadSuccess', function() {
        it('calls notify success', function() {
            createController();
            spyOn(notify, "success");
            ctrl.binder = {};
            ctrl.uploadSuccess({});

            expect(notify.success).toHaveBeenCalled();
        });
    });

    describe('ctrl.uploadError', function() {
        it('calls notify', function() {
            createController();
            spyOn(notify, "error");
            ctrl.uploadError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });
});