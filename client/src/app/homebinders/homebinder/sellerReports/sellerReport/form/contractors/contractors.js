(function() {
	"use strict";

	angular
		.module("hb.homebinders.sellerReports.form.contractors", [])
		.directive("sellerReportFormContractors", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/sellerReports/sellerReport/form/contractors/contractors.tpl.html",
				controller: "SellerReportFormContractorsController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {
					contractors: "=",
					selectAll: "&",
					allContractors: "=",
					binderId: "="
				}
			};
		})
		.controller("SellerReportFormContractorsController", [
			'hb.api',
			'Loading',
			'$log',
			'Notify',
			'hb.resources',
			SellerReportFormContractorsController
		]);

	function SellerReportFormContractorsController(api, loading, $log, notify, resources) {
		this.api = api;
		this.$log = $log;
		this.loading = loading;
		this.notify = notify;
		this.resources = resources.sellerReport;
		this.resources = angular.extend({}, this.resources, resources.sellerReportForm);
		this.is_contractors_collapsed = false;
		this.contractors = [];
	}

	SellerReportFormContractorsController.prototype = {

		destroy: function(contractor) {
			var index = this.contractors.indexOf(contractor);
			var contr = this.contractors[index];
			this.loading.show(this.resources.removing + "...");
			this.api.binderContractor.destroy(contr.id).then(
				angular.bind(this, this.onRemoveSuccess, index),
				angular.bind(this, this.onRemoveError)
			);
		},

		onRemoveSuccess: function(index, response) {
			this.contractors.splice(index, 1);
			this.notify.success(this.resources.removed);
			this.loading.close();
		},

		onRemoveError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		setInclude: function(binder_contractor) {
			binder_contractor.seller_report_item.include = !binder_contractor.seller_report_item.include;
		},

		change: function() {
			this.allContractors = !this.allContractors;
			this.selectAll(this.contractors, this.allContractors);
		}
	};

})();