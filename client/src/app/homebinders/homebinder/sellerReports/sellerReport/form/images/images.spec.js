describe("SellerReportFormImagesController", function() {

    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $q,
        api,
        loading,
        notify,
        $log,
        selectAll;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");

        $log = {
            error: function() {}
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        notify = {
            success: function(msg) {},
            error: function(msg) {}
        };

        api = {
            image: {
                destroy: function(id) {}
            }
        };

        selectAll = function(array, select) {};

    }));

    function createController(boolean_value) {
        $scope = $rootScope.$new();
        ctrl = $controller("SellerReportFormImagesController", {
            "hb.api": api,
            "Loading": loading,
            "$log": $log,
            "Notify": notify
        }, {
            images: [{ id: 1, seller_report_item: { include: true } }],
            allImages: boolean_value,
            selectAll: selectAll
        });

        $scope.ctrl = ctrl;
        $scope.$apply();
        $scope.$digest();

    }

    describe('ctrl.destroy', function() {

        it('should remove image at index 0 from images array', function() {

            spyOn(api.image, "destroy").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(notify, "success");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            var image = {
                id: 1,
                name: "Home.jpg"
            };
            ctrl.images.push(image);
            $rootScope.$apply();
            spyOn(ctrl.images, "indexOf").and.callThrough();
            spyOn(ctrl.images, "splice").and.callThrough();
            ctrl.destroy(ctrl.images[1]);
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalledWith("Removing Home.jpg from binder...");
            expect(api.image.destroy).toHaveBeenCalledWith(1);
            expect(notify.success).toHaveBeenCalledWith("Image deleted.");
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.images.length).toEqual(1);
            expect(ctrl.images.indexOf).toHaveBeenCalledWith(image);
            expect(ctrl.images.splice).toHaveBeenCalledWith(1, 1);
        });

        it('should attempt to remove image from index 0 and destroy image but return an error', function() {

            spyOn(api.image, "destroy").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            var image = {
                id: 1,
                name: "Home.jpg"
            };
            ctrl.images.push(image);
            $rootScope.$apply();
            ctrl.destroy(ctrl.images[1]);
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalledWith("Removing Home.jpg from binder...");
            expect(api.image.destroy).toHaveBeenCalledWith(1);
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.close).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(ctrl.images.length).toEqual(2);
        });

    });

    describe('ctrl.setInclude', function() {

        it('should set include to true', function() {

            createController();
            var image = {
                id: 1,
                name: "Home.jpg",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.images.push(image);
            $rootScope.$apply();
            ctrl.setInclude(ctrl.images[1]);
            $rootScope.$apply();

            expect(ctrl.images[1].seller_report_item.include).toBe(true);

        });

        it('should set include to true', function() {

            createController();
            var image = {
                id: 1,
                name: "Home.jpg",
                seller_report_item: {
                    include: true
                }
            };
            ctrl.images.push(image);
            $rootScope.$apply();
            ctrl.setInclude(ctrl.images[1]);
            $rootScope.$apply();

            expect(ctrl.images[1].seller_report_item.include).toBe(false);

        });

    });

    describe('ctrl.change', function() {

        it('should set allImages to false', function() {

            createController(true);
            var image = {
                id: 1,
                name: "Home.jpg",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.images.push(image);
            $rootScope.$apply();
            spyOn(ctrl, "selectAll");
            ctrl.change();
            $rootScope.$apply();

            expect(ctrl.allImages).toBe(false);
            expect(ctrl.selectAll).toHaveBeenCalledWith(ctrl.images, false);

        });

        it('should set allImages to true', function() {

            createController(false);
            var image = {
                id: 1,
                name: "Home.jpg",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.images.push(image);
            $rootScope.$apply();
            spyOn(ctrl, "selectAll");
            ctrl.change();
            $rootScope.$apply();
            expect(ctrl.allImages).toBe(true);
            expect(ctrl.selectAll).toHaveBeenCalledWith(ctrl.images, true);

        });


    });

});

describe('sellerReportFormImages', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        $scope.images = [];
        $scope.selectAll = function() {};
        $scope.allImages = true;
        $scope.binderId = 1;
        
        element = $compile('<seller-report-form-images images="images" selectAll="selectAll" allImages="allImages" binderId="binderId"></seller-report-form-images>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {
            expect(element.html()).toContain("Images");
        });
    });
});