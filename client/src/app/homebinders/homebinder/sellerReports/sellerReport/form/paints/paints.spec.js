describe("SellerReportFormPaintsController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $q,
        api,
        loading,
        notify,
        $log,
        selectAll;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");

        $log = {
            error: function() {}
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        notify = {
            success: function(msg) {},
            error: function(msg) {}
        };

        api = {
            paint: {
                destroy: function(id) {}
            }
        };

        selectAll = function(array, select) {};

    }));

    function createController(boolean_value) {
        $scope = $rootScope.$new();
        ctrl = $controller("SellerReportFormPaintsController", {
            "hb.api": api,
            "Loading": loading,
            "$log": $log,
            "Notify": notify
        }, {
            allPaints: boolean_value,
            selectAll: selectAll
        });

        $scope.ctrl = ctrl;
        $scope.$apply();
        $scope.$digest();

    }

    describe('ctrl.destroy', function() {

        it('should remove paint at index 0 from paints array', function() {

            spyOn(api.paint, "destroy").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(notify, "success");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            var paint = {
                id: 1,
                name: "Red"
            };
            ctrl.paints.push(paint);
            $rootScope.$apply();
            spyOn(ctrl.paints, "indexOf").and.callThrough();
            spyOn(ctrl.paints, "splice").and.callThrough();
            ctrl.destroy(ctrl.paints[0]);
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalledWith("Removing Red from binder...");
            expect(api.paint.destroy).toHaveBeenCalledWith(1);
            expect(notify.success).toHaveBeenCalledWith("Paint deleted.");
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.paints.length).toEqual(0);
            expect(ctrl.paints.indexOf).toHaveBeenCalledWith(paint);
            expect(ctrl.paints.splice).toHaveBeenCalledWith(0, 1);
        });

        it('should attempt to remove paint from index 0 and destroy paint but return an error', function() {

            spyOn(api.paint, "destroy").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            var paint = {
                id: 1,
                name: "Red"
            };
            ctrl.paints.push(paint);
            $rootScope.$apply();
            ctrl.destroy(ctrl.paints[0]);
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalledWith("Removing Red from binder...");
            expect(api.paint.destroy).toHaveBeenCalledWith(1);
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.close).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(ctrl.paints.length).toEqual(1);
        });

    });

    describe('ctrl.setInclude', function() {

        it('should set include to true', function() {

            createController();
            var paint = {
                id: 1,
                name: "Red",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.paints.push(paint);
            $rootScope.$apply();
            ctrl.setInclude(ctrl.paints[0]);
            $rootScope.$apply();

            expect(ctrl.paints[0].seller_report_item.include).toBe(true);

        });

        it('should set include to true', function() {

            createController();
            var paint = {
                id: 1,
                name: "Red",
                seller_report_item: {
                    include: true
                }
            };
            ctrl.paints.push(paint);
            $rootScope.$apply();
            ctrl.setInclude(ctrl.paints[0]);
            $rootScope.$apply();

            expect(ctrl.paints[0].seller_report_item.include).toBe(false);

        });

    });

    describe('ctrl.change', function() {

        it('should set allPaints to false', function() {

            createController(true);
            var paint = {
                id: 1,
                name: "Red",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.paints.push(paint);
            $rootScope.$apply();
            spyOn(ctrl, "selectAll");
            ctrl.change();
            $rootScope.$apply();

            expect(ctrl.allPaints).toBe(false);
            expect(ctrl.selectAll).toHaveBeenCalledWith(ctrl.paints, false);

        });

        it('should set allPaints to true', function() {

            createController(false);
            var paint = {
                id: 1,
                name: "Red",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.paints.push(paint);
            $rootScope.$apply();
            spyOn(ctrl, "selectAll");
            ctrl.change();
            $rootScope.$apply();
            expect(ctrl.allPaints).toBe(true);
            expect(ctrl.selectAll).toHaveBeenCalledWith(ctrl.paints, true);

        });


    });

});

describe('sellerReportFormPaints', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        $scope.paints = [];
        $scope.selectAll = function() {};
        $scope.allPaints = false;
        $scope.binderId = 1;

        element = $compile('<seller-report-form-paints paints="paints" select-all="selectAll" all-paints="allPaints" binder-id="binderId"></seller-report-form-paints>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the elements', function() {
            expect(element.html()).toContain("Paints");
        });
    });
});