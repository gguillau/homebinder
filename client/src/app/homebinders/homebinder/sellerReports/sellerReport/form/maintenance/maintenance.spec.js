describe("SellerReportFormMaintenanceController", function() {

    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $q,
        api,
        loading,
        notify,
        $log,
        selectAll;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");

        $log = {
            error: function() {}
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        notify = {
            success: function(msg) {},
            error: function(msg) {}
        };

        api = {
            maintenanceItem: {
                destroy: function(id) {}
            }
        };

        selectAll = function(array, select) {};

    }));


    function createController(boolean_value) {
        $scope = $rootScope.$new();
        ctrl = $controller("SellerReportFormMaintenanceController", {
            "hb.api": api,
            "Loading": loading,
            "$log": $log,
            "Notify": notify
        }, {
            allItems: boolean_value,
            selectAll: selectAll
        });

        $scope.ctrl = ctrl;
        $scope.$apply();
        $scope.$digest();

    }

    describe('ctrl.destroy', function() {

        it('should remove item at index 0 from items array', function() {

            spyOn(api.maintenanceItem, "destroy").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(notify, "success");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            var item = {
                id: 1,
                name: "Smoke Detector"
            };
            ctrl.items.push(item);
            $rootScope.$apply();
            spyOn(ctrl.items, "indexOf").and.callThrough();
            spyOn(ctrl.items, "splice").and.callThrough();
            ctrl.destroy(ctrl.items[0]);
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalled();
            expect(api.maintenanceItem.destroy).toHaveBeenCalledWith(1);
            expect(notify.success).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.items.length).toEqual(0);
            expect(ctrl.items.indexOf).toHaveBeenCalledWith(item);
            expect(ctrl.items.splice).toHaveBeenCalledWith(0, 1);
        });

        it('should attempt to remove item from index 0 and destroy item but return an error', function() {

            spyOn(api.maintenanceItem, "destroy").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            var item = {
                id: 1,
                name: "Smoke Detector"
            };
            ctrl.items.push(item);
            $rootScope.$apply();
            ctrl.destroy(ctrl.items[0]);
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalled();
            expect(api.maintenanceItem.destroy).toHaveBeenCalledWith(1);
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.close).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(ctrl.items.length).toEqual(1);
        });

    });

    describe('ctrl.setInclude', function() {

        it('should set include to true', function() {

            createController();
            var item = {
                id: 1,
                name: "Smoke Detector",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.items.push(item);
            $rootScope.$apply();
            ctrl.setInclude(ctrl.items[0]);
            $rootScope.$apply();

            expect(ctrl.items[0].seller_report_item.include).toBe(true);

        });

        it('should set include to true', function() {

            createController();
            var item = {
                id: 1,
                name: "Smoke Detector",
                seller_report_item: {
                    include: true
                }
            };
            ctrl.items.push(item);
            $rootScope.$apply();
            ctrl.setInclude(ctrl.items[0]);
            $rootScope.$apply();

            expect(ctrl.items[0].seller_report_item.include).toBe(false);

        });

    });

    describe('ctrl.change', function() {

        it('should set allItems to false', function() {

            createController(true);
            var item = {
                id: 1,
                name: "Smoke Detector",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.items.push(item);
            $rootScope.$apply();
            spyOn(ctrl, "selectAll");
            ctrl.change();
            $rootScope.$apply();

            expect(ctrl.allItems).toBe(false);
            expect(ctrl.selectAll).toHaveBeenCalledWith(ctrl.items, false);

        });

        it('should set allItems to true', function() {

            createController(false);
            var item = {
                id: 1,
                name: "Smoke Detector",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.items.push(item);
            $rootScope.$apply();
            spyOn(ctrl, "selectAll");
            ctrl.change();
            $rootScope.$apply();
            expect(ctrl.allItems).toBe(true);
            expect(ctrl.selectAll).toHaveBeenCalledWith(ctrl.items, true);

        });


    });

});

describe('sellerReportFormMaintenance', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        $scope.items = [];
        $scope.selectAll = function() {};
        $scope.allItems = false;
        $scope.binderId = 1;

        element = $compile('<seller-report-form-maintenance items="items" select-all="selectAll" all-items="allItems" binder-id="binderId"></seller-report-form-maintenance>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the elements', function() {
            expect(element.html()).toContain("Maintenance Items");
        });
    });
});