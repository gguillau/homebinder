(function() {
	"use strict";

	angular
		.module("hb.homebinders.sellerReports.form.improvements", [])
		.directive("sellerReportFormImprovements", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/sellerReports/sellerReport/form/improvements/improvements.tpl.html",
				controller: "SellerReportFormImprovementsController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {
					improvements: "=",
					selectAll: "&",
					allImprovements: "=",
					binderId: "="
				}
			};
		})
		.controller("SellerReportFormImprovementsController", [
			'hb.api',
			'Loading',
			'$log',
			'Notify',
			'hb.resources',
			SellerReportFormImprovementsController
		]);

	function SellerReportFormImprovementsController(api, loading, $log, notify, resources) {
		this.api = api;
		this.$log = $log;
		this.loading = loading;
		this.notify = notify;
		this.resources = resources.sellerReport;
		this.resources = angular.extend({}, this.resources, resources.sellerReportForm);
		this.is_improvements_collapsed = false;
		this.improvements = [];
	}

	SellerReportFormImprovementsController.prototype = {

		destroy: function(improvement) {
			var index = this.improvements.indexOf(improvement);
			var project = this.improvements[index];
			this.loading.show(this.resources.removing + "...");
			this.api.project.destroy(project.id).then(
				angular.bind(this, this.onRemoveSuccess, index),
				angular.bind(this, this.onRemoveError)
			);
		},

		onRemoveSuccess: function(index, response) {
			this.improvements.splice(index, 1);
			this.notify.success(this.resources.removed);
			this.loading.close();
		},

		onRemoveError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		setInclude: function(improvement) {
			improvement.seller_report_item.include = !improvement.seller_report_item.include;
		},

		change: function() {
			this.allImprovements = !this.allImprovements;
			this.selectAll(this.improvements, this.allImprovements);
		}
	};

})();