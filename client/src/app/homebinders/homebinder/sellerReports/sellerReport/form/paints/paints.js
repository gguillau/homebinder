(function() {
	"use strict";

	angular
		.module("hb.homebinders.sellerReports.form.paints", [])
		.directive("sellerReportFormPaints", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/sellerReports/sellerReport/form/paints/paints.tpl.html",
				controller: "SellerReportFormPaintsController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {
					paints: "=",
					selectAll: "&",
					allPaints: "=",
					binderId: "="
				}
			};
		})
		.controller("SellerReportFormPaintsController", [
			'hb.api',
			'Loading',
			'$log',
			'Notify',
			'hb.resources',
			SellerReportFormPaintsController
		]);

	function SellerReportFormPaintsController(api, loading, $log, notify, resources) {
		this.api = api;
		this.$log = $log;
		this.loading = loading;
		this.notify = notify;
		this.resources = resources.sellerReport;
		this.resources = angular.extend({}, this.resources, resources.sellerReportForm);
		this.is_paints_collapsed = false;
		this.paints = [];
	}

	SellerReportFormPaintsController.prototype = {

		destroy: function(paint) {
			var index = this.paints.indexOf(paint);
			var pain = this.paints[index];
			this.loading.show("Removing " + pain.name + " from binder...");
			this.api.paint.destroy(pain.id).then(
				angular.bind(this, this.onRemoveSuccess, index),
				angular.bind(this, this.onRemoveError)
			);
		},

		onRemoveSuccess: function(index, response) {
			this.paints.splice(index, 1);
			this.notify.success("Paint deleted.");
			this.loading.close();
		},

		onRemoveError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		setInclude: function(paint) {
			paint.seller_report_item.include = !paint.seller_report_item.include;
		},

		change: function() {
			this.allPaints = !this.allPaints;
			this.selectAll(this.paints, this.allPaints);
		}
	};

})();