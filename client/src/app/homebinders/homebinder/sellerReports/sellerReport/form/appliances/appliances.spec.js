describe("SellerReportFormAppliancesController", function() {

    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $q,
        api,
        loading,
        notify,
        $log,
        selectAll;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");

        $log = {
            error: function() {}
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        notify = {
            success: function(msg) {},
            error: function(msg) {}
        };

        api = {
            appliance: {
                destroy: function(id) {}
            }
        };

        selectAll = function(array, select) {};

    }));


    function createController(boolean_value) {
        $scope = $rootScope.$new();
        ctrl = $controller("SellerReportFormAppliancesController", {
            "hb.api": api,
            "Loading": loading,
            "$log": $log,
            "Notify": notify
        }, {
            allAppliances: boolean_value,
            selectAll: selectAll
        });

        $scope.ctrl = ctrl;
        $scope.$apply();
        $scope.$digest();

    }

    describe('ctrl.destroy', function() {

        it('should remove appliance at index 0 from appliances array', function() {

            spyOn(api.appliance, "destroy").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(notify, "success");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            var appliance = {
                id: 1,
                name: "Fridge"
            };
            ctrl.appliances.push(appliance);
            $rootScope.$apply();
            spyOn(ctrl.appliances, "indexOf").and.callThrough();
            spyOn(ctrl.appliances, "splice").and.callThrough();
            ctrl.destroy(ctrl.appliances[0]);
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalled();
            expect(api.appliance.destroy).toHaveBeenCalledWith(1);
            expect(notify.success).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.appliances.length).toEqual(0);
            expect(ctrl.appliances.indexOf).toHaveBeenCalledWith(appliance);
            expect(ctrl.appliances.splice).toHaveBeenCalledWith(0, 1);
        });

        it('should attempt to remove appliance from index 0 and destroy appliance but return an error', function() {

            spyOn(api.appliance, "destroy").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            var appliance = {
                id: 1,
                name: "Fridge"
            };
            ctrl.appliances.push(appliance);
            $rootScope.$apply();
            ctrl.destroy(ctrl.appliances[0]);
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalled();
            expect(api.appliance.destroy).toHaveBeenCalledWith(1);
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.close).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(ctrl.appliances.length).toEqual(1);
        });

    });

    describe('ctrl.setInclude', function() {

        it('should set include to true', function() {

            createController();
            var appliance = {
                id: 1,
                name: "Fridge",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.appliances.push(appliance);
            $rootScope.$apply();
            ctrl.setInclude(ctrl.appliances[0]);
            $rootScope.$apply();

            expect(ctrl.appliances[0].seller_report_item.include).toBe(true);

        });

        it('should set include to true', function() {

            createController();
            var appliance = {
                id: 1,
                name: "Fridge",
                seller_report_item: {
                    include: true
                }
            };
            ctrl.appliances.push(appliance);
            $rootScope.$apply();
            ctrl.setInclude(ctrl.appliances[0]);
            $rootScope.$apply();

            expect(ctrl.appliances[0].seller_report_item.include).toBe(false);

        });

    });

    describe('ctrl.change', function() {

        it('should set allAppliances to false', function() {

            createController(true);
            var appliance = {
                id: 1,
                name: "Fridge",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.appliances.push(appliance);
            $rootScope.$apply();
            spyOn(ctrl, "selectAll");
            ctrl.change();
            $rootScope.$apply();

            expect(ctrl.allAppliances).toBe(false);
            expect(ctrl.selectAll).toHaveBeenCalledWith(ctrl.appliances, false);

        });

        it('should set allAppliances to true', function() {

            createController(false);
            var appliance = {
                id: 1,
                name: "Fridge",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.appliances.push(appliance);
            $rootScope.$apply();
            spyOn(ctrl, "selectAll");
            ctrl.change();
            $rootScope.$apply();
            expect(ctrl.allAppliances).toBe(true);
            expect(ctrl.selectAll).toHaveBeenCalledWith(ctrl.appliances, true);

        });


    });

});

describe('sellerReportFormAppliances', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        $scope.appliances = [];
        $scope.selectAll = function() {};
        $scope.allAppliances = false;
        $scope.binderId = 1;

        element = $compile('<seller-report-form-appliances appliances="appliances" select-all="selectAll" all-appliances="allAppliances" binder-id="binderId"></seller-report-form-appliances>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the elements', function() {
            expect(element.html()).toContain("Appliances");
        });
    });
});