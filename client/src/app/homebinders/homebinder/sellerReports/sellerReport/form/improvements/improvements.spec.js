describe("SellerReportFormImprovementsController", function() {

    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $q,
        api,
        loading,
        notify,
        $log,
        selectAll;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");

        $log = {
            error: function() {}
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        notify = {
            success: function(msg) {},
            error: function(msg) {}
        };

        api = {
            project: {
                destroy: function(id) {}
            }
        };

        selectAll = function(array, select) {};

    }));


    function createController(boolean_value) {
        $scope = $rootScope.$new();
        ctrl = $controller("SellerReportFormImprovementsController", {
            "hb.api": api,
            "Loading": loading,
            "$log": $log,
            "Notify": notify
        }, {
            allImprovements: boolean_value,
            selectAll: selectAll
        });

        $scope.ctrl = ctrl;
        $scope.$apply();
        $scope.$digest();

    }

    describe('ctrl.destroy', function() {

        it('should remove project at index 0 from improvements array', function() {

            spyOn(api.project, "destroy").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(notify, "success");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            var project = {
                id: 1,
                name: "Kitchen Renovation"
            };
            ctrl.improvements.push(project);
            $rootScope.$apply();
            spyOn(ctrl.improvements, "indexOf").and.callThrough();
            spyOn(ctrl.improvements, "splice").and.callThrough();
            ctrl.destroy(ctrl.improvements[0]);
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalled();
            expect(api.project.destroy).toHaveBeenCalledWith(1);
            expect(notify.success).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.improvements.length).toEqual(0);
            expect(ctrl.improvements.indexOf).toHaveBeenCalledWith(project);
            expect(ctrl.improvements.splice).toHaveBeenCalledWith(0, 1);
        });

        it('should attempt to remove project from index 0 and destroy project but return an error', function() {

            spyOn(api.project, "destroy").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            var project = {
                id: 1,
                name: "Kitchen Renovation"
            };
            ctrl.improvements.push(project);
            $rootScope.$apply();
            ctrl.destroy(ctrl.improvements[0]);
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalled();
            expect(api.project.destroy).toHaveBeenCalledWith(1);
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.close).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(ctrl.improvements.length).toEqual(1);
        });

    });

    describe('ctrl.setInclude', function() {

        it('should set include to true', function() {

            createController();
            var project = {
                id: 1,
                name: "Kitchen Renovation",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.improvements.push(project);
            $rootScope.$apply();
            ctrl.setInclude(ctrl.improvements[0]);
            $rootScope.$apply();

            expect(ctrl.improvements[0].seller_report_item.include).toBe(true);

        });

        it('should set include to true', function() {

            createController();
            var project = {
                id: 1,
                name: "Kitchen Renovation",
                seller_report_item: {
                    include: true
                }
            };
            ctrl.improvements.push(project);
            $rootScope.$apply();
            ctrl.setInclude(ctrl.improvements[0]);
            $rootScope.$apply();

            expect(ctrl.improvements[0].seller_report_item.include).toBe(false);

        });

    });

    describe('ctrl.change', function() {

        it('should set allImprovements to false', function() {

            createController(true);
            var project = {
                id: 1,
                name: "Kitchen Renovation",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.improvements.push(project);
            $rootScope.$apply();
            spyOn(ctrl, "selectAll");
            ctrl.change();
            $rootScope.$apply();

            expect(ctrl.allImprovements).toBe(false);
            expect(ctrl.selectAll).toHaveBeenCalledWith(ctrl.improvements, false);

        });

        it('should set allImprovements to true', function() {

            createController(false);
            var project = {
                id: 1,
                name: "Kitchen Renovation",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.improvements.push(project);
            $rootScope.$apply();
            spyOn(ctrl, "selectAll");
            ctrl.change();
            $rootScope.$apply();
            expect(ctrl.allImprovements).toBe(true);
            expect(ctrl.selectAll).toHaveBeenCalledWith(ctrl.improvements, true);

        });


    });

});

describe('sellerReportFormImprovements', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        $scope.improvements = [];
        $scope.selectAll = function() {};
        $scope.allImprovements = false;
        $scope.binderId = 1;

        element = $compile('<seller-report-form-improvements improvements="improvements" select-all="selectAll" all-improvements="allImprovements" binder-id="binderId"></seller-report-form-improvements>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the elements', function() {
            expect(element.html()).toContain("Home Improvements");
        });
    });
});