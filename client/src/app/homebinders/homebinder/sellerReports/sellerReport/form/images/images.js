(function() {
	"use strict";

	angular
		.module("hb.homebinders.sellerReports.form.images", [])
		.directive("sellerReportFormImages", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/sellerReports/sellerReport/form/images/images.tpl.html",
				controller: "SellerReportFormImagesController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {
					images: "=",
					selectAll: "&",
					allImages: "=",
					binderId: "="
				}
			};
		})
		.controller("SellerReportFormImagesController", [
			'hb.api',
			'Loading',
			'$log',
			'Notify',
			'hb.resources',
			SellerReportFormImagesController
		]);

	function SellerReportFormImagesController(api, loading, $log, notify, resources) {
		this.api = api;
		this.$log = $log;
		this.loading = loading;
		this.notify = notify;
		this.resources = resources.sellerReport;
		this.resources = angular.extend({}, this.resources, resources.sellerReportForm);
		this.is_images_collapsed = false;
		this.init();
	}

	SellerReportFormImagesController.prototype = {

		init: function() {
			if (this.images && this.images.length > 0) {
				this.images_chunk = this.chunk(this.images, 4);
			}
		},

		chunk: function(images, size) {
			var array = [];

			for (var i = 0; i < images.length; i += size) {
				array.push(images.slice(i, i + size));
			}
			return array;
		},

		destroy: function(image) {
			var index = this.images.indexOf(image);
			var img = this.images[index];
			this.loading.show("Removing " + img.name + " from binder...");
			this.api.image.destroy(img.id).then(
				angular.bind(this, this.onRemoveSuccess, index),
				angular.bind(this, this.onRemoveError)
			);
		},

		onRemoveSuccess: function(index, response) {
			this.images.splice(index, 1);
			this.images_chunk = this.chunk(this.images, 4);
			this.notify.success("Image deleted.");
			this.loading.close();
		},

		onRemoveError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		setInclude: function(image) {
			image.seller_report_item.include = !image.seller_report_item.include;
		},

		change: function() {
			this.allImages = !this.allImages;
			this.selectAll(this.images, this.allImages);
		}
	};

})();