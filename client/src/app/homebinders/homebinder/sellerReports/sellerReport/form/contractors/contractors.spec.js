describe("SellerReportFormContractorsController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $q,
        api,
        loading,
        notify,
        $log,
        selectAll;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");

        $log = {
            error: function() {}
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        notify = {
            success: function(msg) {},
            error: function(msg) {}
        };

        api = {
            binderContractor: {
                destroy: function(id) {}
            }
        };

        selectAll = function(array, select) {};

    }));

    function createController(boolean_value) {
        $scope = $rootScope.$new();
        ctrl = $controller("SellerReportFormContractorsController", {
            "hb.api": api,
            "Loading": loading,
            "$log": $log,
            "Notify": notify
        }, {
            allContractors: boolean_value,
            selectAll: selectAll
        });

        $scope.ctrl = ctrl;
        $scope.$apply();
        $scope.$digest();

    }

    describe('ctrl.destroy', function() {
        it('should remove contractor at index 0 from contractors array', function() {

            spyOn(api.binderContractor, "destroy").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(notify, "success");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            var contractor = {
                id: 1,
                contractor: {
                    name: "John Plumber"
                }
            };
            ctrl.contractors.push(contractor);
            $rootScope.$apply();
            spyOn(ctrl.contractors, "indexOf").and.callThrough();
            spyOn(ctrl.contractors, "splice").and.callThrough();
            ctrl.destroy(ctrl.contractors[0]);
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalled();
            expect(api.binderContractor.destroy).toHaveBeenCalledWith(1);
            expect(notify.success).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.contractors.length).toEqual(0);
            expect(ctrl.contractors.indexOf).toHaveBeenCalledWith(contractor);
            expect(ctrl.contractors.splice).toHaveBeenCalledWith(0, 1);
        });

        it('should attempt to remove contractor from index 0 and destroy contractor but return an error', function() {

            spyOn(api.binderContractor, "destroy").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            var contractor = {
                id: 1,
                contractor: {
                    name: "John Plumber"
                }
            };
            ctrl.contractors.push(contractor);
            $rootScope.$apply();
            ctrl.destroy(ctrl.contractors[0]);
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalled();
            expect(api.binderContractor.destroy).toHaveBeenCalledWith(1);
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.close).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(ctrl.contractors.length).toEqual(1);
        });
    });

    describe('ctrl.setInclude', function() {
        it('should set include to true', function() {

            createController();
            var contractor = {
                id: 1,
                contractor: {
                    name: "John Plumber"
                },
                seller_report_item: {
                    include: false
                }
            };
            ctrl.contractors.push(contractor);
            $rootScope.$apply();
            ctrl.setInclude(ctrl.contractors[0]);
            $rootScope.$apply();

            expect(ctrl.contractors[0].seller_report_item.include).toBe(true);

        });

        it('should set include to true', function() {

            createController();
            var contractor = {
                id: 1,
                contractor: {
                    name: "John Plumber"
                },
                seller_report_item: {
                    include: true
                }
            };
            ctrl.contractors.push(contractor);
            $rootScope.$apply();
            ctrl.setInclude(ctrl.contractors[0]);
            $rootScope.$apply();

            expect(ctrl.contractors[0].seller_report_item.include).toBe(false);
        });
    });

    describe('ctrl.change', function() {
        it('should set allContractors to false', function() {

            createController(true);
            var contractor = {
                id: 1,
                contractor: {
                    name: "John Plumber"
                },
                seller_report_item: {
                    include: false
                }
            };
            ctrl.contractors.push(contractor);
            $rootScope.$apply();
            spyOn(ctrl, "selectAll");
            ctrl.change();
            $rootScope.$apply();

            expect(ctrl.allContractors).toBe(false);
            expect(ctrl.selectAll).toHaveBeenCalledWith(ctrl.contractors, false);

        });

        it('should set allContractors to true', function() {

            createController(false);
            var contractor = {
                id: 1,
                contractor: {
                    name: "John Plumber"
                },
                seller_report_item: {
                    include: false
                }
            };
            ctrl.contractors.push(contractor);
            $rootScope.$apply();
            spyOn(ctrl, "selectAll");
            ctrl.change();
            $rootScope.$apply();
            expect(ctrl.allContractors).toBe(true);
            expect(ctrl.selectAll).toHaveBeenCalledWith(ctrl.contractors, true);

        });
    });

});

describe('sellerReportFormContractors', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        $scope.contractors = [];
        $scope.selectAll = function() {};
        $scope.allContractors = false;
        $scope.binderId = 1;

        element = $compile('<seller-report-form-contractors contractors="contractors" select-all="selectAll" all-contractors="allContractors" binder-id="binderId"></seller-report-form-contractors>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the elements', function() {
            expect(element.html()).toContain("Home Pros");
        });
    });
});