(function() {
    "use strict";

    angular
        .module("hb.homebinders.sellerReports.form", [
            "hb.homebinders.sellerReports.form.appliances",
            "hb.homebinders.sellerReports.form.maintenance",
            "hb.homebinders.sellerReports.form.improvements",
            "hb.homebinders.sellerReports.form.contractors",
            "hb.homebinders.sellerReports.form.paints",
            "hb.homebinders.sellerReports.form.finishes",
            "hb.homebinders.sellerReports.form.documents",
            "hb.homebinders.sellerReports.form.images",
            "hb.homebinders.sellerReports.form.permits"
        ])
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state("seller_report_edit", {
                    url: "^/sellerreport/:code/edit",
                    templateUrl: "homebinders/homebinder/sellerReports/sellerReport/form/sellerReportForm.tpl.html",
                    controller: "SellerReportFormCtrl",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .controller("SellerReportFormCtrl", [
            "$state",
            "$anchorScroll",
            "$stateParams",
            "hb.api",
            "Notify",
            "Session",
            "$log",
            "$window",
            "Loading",
            "hb.utils",
            "$q",
            "hb.resources",
            SellerReportFormCtrl
        ]);

    function SellerReportFormCtrl($state, $anchorScroll, $stateParams, api, Notify, Session, $log, $window, loading, utils, $q, resources) {
        this.$state = $state;
        this.api = api;
        this.notify = Notify;
        this.Session = Session;
        this.$log = $log;
        this.$window = $window;
        this.loading = loading;
        this.is_cancel_save = false;
        this.is_download_save = false;
        this.is_view_save = false;
        this.code = $stateParams['code'].toUpperCase();
        this.utils = utils;
        this.$q = $q;
        this.resources = resources.sellerReport;
        this.resources = angular.extend({}, this.resources, resources.sellerReportForm);
        this.today = this.utils.utils.getToday(true);
        this.data = {};
        this.data = {
            everything: false,
            allAppliances: false,
            allMaintenance: false,
            allImprovements: false,
            allContractors: false,
            allPaints: false,
            allPermits: false,
            allFinishes: false,
            allDocuments: false,
            allImages: false
        };
        this.uploader = {
            fileTypes: "image",
            hideUploadButton: true,
            showTable: false,
            selectButtonLabel: this.resources.uploadPhoto,
            fileLimit: true,
            url: "/api/v1/images",
            jwt: this.Session.getJwt(),
            multiSelect: false,
            uploadOnAdd: true
        };
        this.refresh();
    }

    SellerReportFormCtrl.prototype = {

        refresh: function() {
            this.loading.show(this.resources.loading + "...");
            this.api.sellerReport.get(this.code, true).then(
                angular.bind(this, this.onRefreshSuccess),
                angular.bind(this, this.onRefreshError)
            );
        },

        onRefreshSuccess: function(response) {
            this.data.report = response.data;
            this.api.binder.get(this.data.report.binder.id).then(
                angular.bind(this, this.getBinderSuccess),
                angular.bind(this, this.getBinderError));
        },

        onRefreshError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.loading.close();
        },

        getBinderSuccess: function(response) {
            this.binder = response.data;
            var binder = {
                id: response.data.id,
                name: response.data.name,
                primary: response.data.primary,
                property_attributes: response.data.property,
                details: response.data.details
            };
            this.data.binder = binder;
            this.logo_url = this.data.report.seller_report_logo ?
                this.data.report.seller_report_logo.logo ?
                this.data.report.seller_report_logo.logo.location : undefined : undefined;

            this.loadComponents();

        },

        getBinderError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.loading.close();
        },

        loadComponents: function() {

            var promises = [];
            var opts = { binderId: this.data.report.binder.id, count: 200 };

            promises.push(this.api.document.all(opts).then(
                    angular.bind(this, this.getDocumentSuccess),
                    angular.bind(this, this.getItemError)),

                this.api.image.all({
                    binderId: this.data.report.binder.id
                }).then(
                    angular.bind(this, this.getImageSuccess),
                    angular.bind(this, this.getItemError)),


                this.api.appliance.all(opts).then(
                    angular.bind(this, this.getApplianceSuccess),
                    angular.bind(this, this.getItemError)),

                this.api.maintenanceItem.all(opts).then(
                    angular.bind(this, this.getMaintenanceSuccess),
                    angular.bind(this, this.getItemError)),

                this.api.project.all(opts).then(
                    angular.bind(this, this.getProjectSuccess),
                    angular.bind(this, this.getItemError)),

                this.api.binderContractor.all(opts).then(
                    angular.bind(this, this.getContractorSuccess),
                    angular.bind(this, this.getItemError)),

                this.api.paint.all(opts).then(
                    angular.bind(this, this.getPaintSuccess),
                    angular.bind(this, this.getItemError)),

                this.api.permit.all(opts).then(
                    angular.bind(this, this.getPermitSuccess),
                    angular.bind(this, this.getItemError)),

                this.api.finish.all(opts).then(
                    angular.bind(this, this.getFinishSuccess),
                    angular.bind(this, this.getItemError)));

            this.$q.all(promises).then(
                angular.bind(this, this.closeLoading)
            );
        },

        getDocumentSuccess: function(response) {
            this.data.documents = response.data.items;
            this.data.documents.forEach(function(doc) {
                if (doc.seller_report_item === null) {
                    doc.seller_report_item = {
                        include: false
                    };
                }
            });
        },

        getImageSuccess: function(response) {
            this.data.images = response.data.items;
            this.data.images.forEach(function(img) {
                if (img.seller_report_item === null) {
                    img.seller_report_item = {
                        include: false
                    };
                }
            });
        },

        getApplianceSuccess: function(response) {
            this.data.appliances = response.data.items;
            this.data.appliances.forEach(function(app) {
                if (app.seller_report_item === null) {
                    app.seller_report_item = {
                        include: false
                    };
                }
            });
        },

        getMaintenanceSuccess: function(response) {
            this.data.maintenance = response.data.items;
            this.data.maintenance.forEach(function(maint) {
                if (maint.seller_report_item === null) {
                    maint.seller_report_item = {
                        include: false
                    };
                }
            });
        },

        getProjectSuccess: function(response) {
            this.data.improvements = response.data.items;
            this.data.improvements.forEach(function(impr) {
                impr.cost_cents = Number(impr.cost_cents / 100).toFixed(2);
                if (impr.seller_report_item === null) {
                    impr.seller_report_item = {
                        include: false
                    };
                }
            });
        },

        getContractorSuccess: function(response) {
            this.data.contractors = response.data.items;
            this.data.contractors.forEach(function(cont) {
                if (cont.seller_report_item === null) {
                    cont.seller_report_item = {
                        include: false
                    };
                }
            });
        },

        getPaintSuccess: function(response) {
            this.data.paints = response.data.items;
            this.data.paints.forEach(function(pa) {
                if (pa.seller_report_item === null) {
                    pa.seller_report_item = {
                        include: false
                    };
                }
            });
        },

        getPermitSuccess: function(response) {
            this.data.permits = response.data.items;
            this.data.permits.forEach(function(pt) {
                if (pt.seller_report_item === null) {
                    pt.seller_report_item = {
                        include: false
                    };
                }
            });
        },

        getFinishSuccess: function(response) {
            this.data.finishes = response.data.items;
            this.data.finishes.forEach(function(f) {
                if (f.seller_report_item === null) {
                    f.seller_report_item = {
                        include: false
                    };
                }
            });
        },

        getItemError: function(response) {
            this.notify.error(response.data);
        },

        closeLoading: function() {
            this.loading.close();
        },

        save: function(option) {
            if (option === "cancel") {
                this.is_cancel_save = true;
            }
            else if (option === "download") {
                this.is_download_save = true;
            }
            else if (option === "description") {
                this.is_description_save = true;
            }
            else {
                this.is_view_save = true;
            }
            this.loading.show(this.resources.saving + "...");
            var data = {
                show_project_costs: this.data.report.show_project_costs,
                public: this.data.report.public,
                documents: [],
                images: [],
                appliances: [],
                maintenance: [],
                improvements: [],
                contractors: [],
                paints: [],
                permits: [],
                finishes: []
            };

            this.data.documents.forEach(function(doc) {
                if (doc.seller_report_item && doc.seller_report_item.include) {
                    data.documents.push(doc.id);
                }
            });

            this.data.images.forEach(function(img) {
                if (img.seller_report_item && img.seller_report_item.include) {
                    data.images.push(img.id);
                }
            });

            this.data.appliances.forEach(function(app) {
                if (app.seller_report_item && app.seller_report_item.include) {
                    data.appliances.push(app.id);
                }
            });

            this.data.maintenance.forEach(function(mi) {
                if (mi.seller_report_item && mi.seller_report_item.include) {
                    data.maintenance.push(mi.id);
                }
            });

            this.data.improvements.forEach(function(imp) {
                if (imp.seller_report_item && imp.seller_report_item.include) {
                    data.improvements.push(imp.id);
                }
            });

            this.data.contractors.forEach(function(con) {
                if (con.seller_report_item && con.seller_report_item.include) {
                    data.contractors.push(con.id);
                }
            });

            this.data.paints.forEach(function(paint) {
                if (paint.seller_report_item && paint.seller_report_item.include) {
                    data.paints.push(paint.id);
                }
            });

            this.data.permits.forEach(function(permit) {
                if (permit.seller_report_item && permit.seller_report_item.include) {
                    data.permits.push(permit.id);
                }
            });

            this.data.finishes.forEach(function(fin) {
                if (fin.seller_report_item && fin.seller_report_item.include) {
                    data.finishes.push(fin.id);
                }
            });

            this.api.binder.update(this.data.binder.id, this.data.binder).then(
                angular.bind(this, this.updateSuccess, data),
                angular.bind(this, this.updateError));
        },

        updateSuccess: function(data, response) {
            this.binder = response.data;
            this.updateImagesandDocs(data);
        },

        updateError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.loading.close();
        },

        updateImagesandDocs: function(data) {
            this.api.sellerReport.updateDocsAndImages(this.data.report.id, data).then(
                angular.bind(this, this.saveComplete, data),
                angular.bind(this, this.saveError, data));
        },

        saveComplete: function(data, response) {
            if (this.is_cancel_save) {
                this.cancel();
            }
            else if (this.is_download_save) {
                this.download(true);
            }
            else if (this.is_view_save) {
                if (data.public) {
                    var url = this.$state.href("seller_report", {
                        code: this.code
                    }, {
                        absolute: true
                    });
                    this.$window.open(url, "_blank");
                }
            }
            this.notify.success(this.resources.saved);
            this.loading.close();
        },

        saveError: function(data, response) {
            this.$log.error(response.data);
            this.notify.error(response.data);
            this.saveComplete(data);
        },

        cancel: function() {
            this.$state.go("binder.overview", {
                binderId: this.data.report.binder.id
            });
        },

        download: function(value) {
            this.loading.show(this.resources.downloading + "...");
            this.api.sellerReportPDF.download(this.code, value).then(
                angular.bind(this, this.downloadSuccess, value),
                angular.bind(this, this.downloadError));
        },

        downloadSuccess: function(value, response) {
            this.is_download_save = false;
            var file = new Blob([response.data], {
                type: "application/pdf"
            }, {
                encoding: "raw"
            });
            var name = value ? "HomeBinder_Full_Sellers_Report_" : "HomeBinder_Sellers_Report_";
            var filename = name + this.data.binder.name + "_" + this.today + ".pdf";
            if (this.$window.navigator.msSaveOrOpenBlob) {
                this.$window.navigator.msSaveOrOpenBlob(file, filename);
            }
            else {
                var fileURL = this.$window.URL.createObjectURL(file);
                var a = document.createElement("a");
                a.href = fileURL;
                a.target = "_blank";
                a.download = filename;
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
                this.$window.URL.revokeObjectURL(file);
            }
            this.loading.close();
        },

        downloadError: function(response) {
            this.is_download_save = false;
            this.$log.error(response);
            this.notify.error(response.data);
            this.loading.close();
        },

        selectAll: function() {
            this.data.allAppliances = this.data.everything;
            this.data.allMaintenance = this.data.everything;
            this.data.allImprovements = this.data.everything;
            this.data.allContractors = this.data.everything;
            this.data.allPaints = this.data.everything;
            this.data.allPermits = this.data.everything;
            this.data.allFinishes = this.data.everything;
            this.data.allDocuments = this.data.everything;
            this.data.allImages = this.data.everything;
            this.allAppliances(this.data.everything);
            this.allContractors(this.data.everything);
            this.allDocuments(this.data.everything);
            this.allFinishes(this.data.everything);
            this.allImages(this.data.everything);
            this.allImprovements(this.data.everything);
            this.allMaintenance(this.data.everything);
            this.allPaints(this.data.everything);
            this.allPermits(this.data.everything);
        },

        allAppliances: function(boolean_value) {
            this.selectAllItems(this.data.appliances, this.data.allAppliances, boolean_value);
        },

        allMaintenance: function(boolean_value) {
            this.selectAllItems(this.data.maintenance, this.data.allMaintenance, boolean_value);
        },

        allImprovements: function(boolean_value) {
            this.selectAllItems(this.data.improvements, this.data.allImprovements, boolean_value);
        },

        allContractors: function(boolean_value) {
            this.selectAllItems(this.data.contractors, this.data.allContractors, boolean_value);
        },

        allPaints: function(boolean_value) {
            this.selectAllItems(this.data.paints, this.data.allPaints, boolean_value);
        },

        allPermits: function(boolean_value) {
            this.selectAllItems(this.data.permits, this.data.allPermits, boolean_value);
        },

        allFinishes: function(boolean_value) {
            this.selectAllItems(this.data.finishes, this.data.allFinishes, boolean_value);
        },

        allDocuments: function(boolean_value) {
            this.selectAllItems(this.data.documents, this.data.allDocuments, boolean_value);
        },

        allImages: function(boolean_value) {
            this.selectAllItems(this.data.images, this.data.allImages, boolean_value);
        },

        selectAllItems: function(arr, select, boolean_value) {
            if (arr) {
                arr.forEach(function(a) {
                    a.seller_report_item.include = boolean_value !== undefined ? boolean_value : !select;
                });
            }
        },

        uploadImage: function() {
            this.loading.show(this.resources.saving + "...");
            var uploadApi = this.uploader.api;
            if (uploadApi && uploadApi.pendingUploads().length > 0) {
                uploadApi.updateParams({
                    binder_id: this.binder.id,
                    hero_image_id: true
                });

                uploadApi.uploadFiles();
            }
        },

        uploadSuccess: function(response) {
            this.binder.hero_photo = response.location;
            this.notify.success(this.resources.saved);
            this.loading.close();
        },

        uploadError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.loading.close();
        }

    };

})();