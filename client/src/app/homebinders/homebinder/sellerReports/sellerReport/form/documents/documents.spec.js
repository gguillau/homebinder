describe("SellerReportFormDocumentsController", function() {

    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $q,
        api,
        loading,
        notify,
        $log,
        selectAll;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");

        $log = {
            error: function() {}
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        notify = {
            success: function(msg) {},
            error: function(msg) {}
        };

        api = {
            document: {
                destroy: function(id) {}
            }
        };

        selectAll = function(array, select) {};

    }));


    function createController(boolean_value) {
        $scope = $rootScope.$new();
        ctrl = $controller("SellerReportFormDocumentsController", {
            "hb.api": api,
            "Loading": loading,
            "$log": $log,
            "Notify": notify
        }, {
            allDocuments: boolean_value,
            selectAll: selectAll
        });

        $scope.ctrl = ctrl;
        $scope.$apply();
        $scope.$digest();

    }

    describe('ctrl.destroy', function() {

        it('should remove document at index 0 from documents array', function() {

            spyOn(api.document, "destroy").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(notify, "success");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            var document = {
                id: 1,
                name: "Moving.pdf"
            };
            ctrl.documents.push(document);
            $rootScope.$apply();
            spyOn(ctrl.documents, "indexOf").and.callThrough();
            spyOn(ctrl.documents, "splice").and.callThrough();
            ctrl.destroy(ctrl.documents[0]);
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalled();
            expect(api.document.destroy).toHaveBeenCalledWith(1);
            expect(notify.success).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.documents.length).toEqual(0);
            expect(ctrl.documents.indexOf).toHaveBeenCalledWith(document);
            expect(ctrl.documents.splice).toHaveBeenCalledWith(0, 1);
        });

        it('should attempt to remove document from index 0 and destroy document but return an error', function() {

            spyOn(api.document, "destroy").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            var document = {
                id: 1,
                name: "Moving.pdf"
            };
            ctrl.documents.push(document);
            $rootScope.$apply();
            ctrl.destroy(ctrl.documents[0]);
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalled();
            expect(api.document.destroy).toHaveBeenCalledWith(1);
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.close).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(ctrl.documents.length).toEqual(1);
        });

    });

    describe('ctrl.setInclude', function() {

        it('should set include to true', function() {

            createController();
            var document = {
                id: 1,
                name: "Moving.pdf",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.documents.push(document);
            $rootScope.$apply();
            ctrl.setInclude(ctrl.documents[0]);
            $rootScope.$apply();

            expect(ctrl.documents[0].seller_report_item.include).toBe(true);

        });

        it('should set include to true', function() {

            createController();
            var document = {
                id: 1,
                name: "Moving.pdf",
                seller_report_item: {
                    include: true
                }
            };
            ctrl.documents.push(document);
            $rootScope.$apply();
            ctrl.setInclude(ctrl.documents[0]);
            $rootScope.$apply();

            expect(ctrl.documents[0].seller_report_item.include).toBe(false);

        });

    });

    describe('ctrl.change', function() {

        it('should set allDocuments to false', function() {

            createController(true);
            var document = {
                id: 1,
                name: "Moving.pdf",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.documents.push(document);
            $rootScope.$apply();
            spyOn(ctrl, "selectAll");
            ctrl.change();
            $rootScope.$apply();

            expect(ctrl.allDocuments).toBe(false);
            expect(ctrl.selectAll).toHaveBeenCalledWith(ctrl.documents, false);

        });

        it('should set allDocuments to true', function() {

            createController(false);
            var document = {
                id: 1,
                name: "Moving.pdf",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.documents.push(document);
            $rootScope.$apply();
            spyOn(ctrl, "selectAll");
            ctrl.change();
            $rootScope.$apply();
            expect(ctrl.allDocuments).toBe(true);
            expect(ctrl.selectAll).toHaveBeenCalledWith(ctrl.documents, true);

        });


    });

});

describe('sellerReportFormDocuments', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        $scope.documents = [];
        $scope.selectAll = function() {};
        $scope.allDocuments = false;
        $scope.binderId = 1;

        element = $compile('<seller-report-form-documents documents="documents" select-all="selectAll" all-documents="allDocuments" binder-id="binderId"></seller-report-form-documents>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the elements', function() {
            expect(element.html()).toContain("Documents");
        });
    });
});