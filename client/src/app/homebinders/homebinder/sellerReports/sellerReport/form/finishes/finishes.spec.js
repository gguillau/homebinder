describe("SellerReportFormFinishesController", function() {

    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $q,
        api,
        loading,
        notify,
        $log,
        selectAll;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");

        $log = {
            error: function() {}
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        notify = {
            success: function(msg) {},
            error: function(msg) {}
        };

        api = {
            finish: {
                destroy: function(id) {}
            }
        };

        selectAll = function(array, select) {};

    }));


    function createController(boolean_value) {
        $scope = $rootScope.$new();
        ctrl = $controller("SellerReportFormFinishesController", {
            "hb.api": api,
            "Loading": loading,
            "$log": $log,
            "Notify": notify
        }, {
            allFinishes: boolean_value,
            selectAll: selectAll
        });

        $scope.ctrl = ctrl;
        $scope.$apply();
        $scope.$digest();

    }

    describe('ctrl.destroy', function() {

        it('should remove finish at index 0 from finishes array', function() {

            spyOn(api.finish, "destroy").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(notify, "success");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            var finish = {
                id: 1,
                name: "Wood Stain"
            };
            ctrl.finishes.push(finish);
            $rootScope.$apply();
            spyOn(ctrl.finishes, "indexOf").and.callThrough();
            spyOn(ctrl.finishes, "splice").and.callThrough();
            ctrl.destroy(finish);
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalled();
            expect(api.finish.destroy).toHaveBeenCalledWith(1);
            expect(notify.success).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.finishes.length).toEqual(0);
            expect(ctrl.finishes.indexOf).toHaveBeenCalledWith(finish);
            expect(ctrl.finishes.splice).toHaveBeenCalledWith(0, 1);
        });

        it('should attempt to remove finish from index 0 and destroy finish but return an error', function() {

            spyOn(api.finish, "destroy").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            var finish = {
                id: 1,
                name: "Wood Stain"
            };
            ctrl.finishes.push(finish);
            $rootScope.$apply();
            ctrl.destroy(ctrl.finishes[0]);
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalled();
            expect(api.finish.destroy).toHaveBeenCalledWith(1);
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.close).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(ctrl.finishes.length).toEqual(1);
        });

    });

    describe('ctrl.setInclude', function() {

        it('should set include to true', function() {

            createController();
            var finish = {
                id: 1,
                name: "Wood Stain",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.finishes.push(finish);
            $rootScope.$apply();
            ctrl.setInclude(ctrl.finishes[0]);
            $rootScope.$apply();

            expect(ctrl.finishes[0].seller_report_item.include).toBe(true);

        });

        it('should set include to true', function() {

            createController();
            var finish = {
                id: 1,
                name: "Wood Stain",
                seller_report_item: {
                    include: true
                }
            };
            ctrl.finishes.push(finish);
            $rootScope.$apply();
            ctrl.setInclude(ctrl.finishes[0]);
            $rootScope.$apply();

            expect(ctrl.finishes[0].seller_report_item.include).toBe(false);

        });

    });

    describe('ctrl.change', function() {

        it('should set allFinishes to false', function() {

            createController(true);
            var finish = {
                id: 1,
                name: "Moving.pdf",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.finishes.push(finish);
            $rootScope.$apply();
            spyOn(ctrl, "selectAll");
            ctrl.change();
            $rootScope.$apply();

            expect(ctrl.allFinishes).toBe(false);
            expect(ctrl.selectAll).toHaveBeenCalledWith(ctrl.finishes, false);

        });

        it('should set allFinishes to true', function() {

            createController(false);
            var finish = {
                id: 1,
                name: "Wood Stain",
                seller_report_item: {
                    include: false
                }
            };
            ctrl.finishes.push(finish);
            $rootScope.$apply();
            spyOn(ctrl, "selectAll");
            ctrl.change();
            $rootScope.$apply();
            expect(ctrl.allFinishes).toBe(true);
            expect(ctrl.selectAll).toHaveBeenCalledWith(ctrl.finishes, true);

        });


    });

});

describe('sellerReportFormFinishes', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        $scope.finishes = [];
        $scope.selectAll = function() {};
        $scope.allFinishes = false;
        $scope.binderId = 1;

        element = $compile('<seller-report-form-finishes finishes="finishes" select-all="selectAll" all-finishes="allFinishes" binder-id="binderId"></seller-report-form-finishes>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the elements', function() {
            expect(element.html()).toContain("Finishes");
        });
    });
});