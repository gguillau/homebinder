(function() {
	"use strict";

	angular
		.module("hb.homebinders.sellerReports.form.appliances", [])
		.directive("sellerReportFormAppliances", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/sellerReports/sellerReport/form/appliances/appliances.tpl.html",
				controller: "SellerReportFormAppliancesController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {
					appliances: "=",
					selectAll: "&",
					allAppliances: "=",
					binderId: "="
				}
			};
		})
		.controller("SellerReportFormAppliancesController", [
			'hb.api',
			'Loading',
			'$log',
			'Notify',
			'hb.resources',
			SellerReportFormAppliancesController
		]);

	function SellerReportFormAppliancesController(api, loading, $log, notify, resources) {
		this.api = api;
		this.$log = $log;
		this.loading = loading;
		this.notify = notify;
		this.resources = resources.sellerReport;
		this.resources = angular.extend({}, this.resources, resources.sellerReportForm);
		this.is_app_collapsed = false;
		this.appliances = [];
	}

	SellerReportFormAppliancesController.prototype = {

		destroy: function(appliance) {
			var index = this.appliances.indexOf(appliance);
			var app = this.appliances[index];
			this.loading.show(this.resources.removing + "...");
			this.api.appliance.destroy(app.id).then(
				angular.bind(this, this.onRemoveSuccess, index),
				angular.bind(this, this.onRemoveError)
			);
		},

		onRemoveSuccess: function(index, response) {
			this.appliances.splice(index, 1);
			this.notify.success(this.resources.removed);
			this.loading.close();
		},

		onRemoveError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		setInclude: function(appliance) {
			appliance.seller_report_item.include = !appliance.seller_report_item.include;
		},

		change: function() {
			this.allAppliances = !this.allAppliances;
			this.selectAll(this.appliances, this.allAppliances);
		}
	};

})();