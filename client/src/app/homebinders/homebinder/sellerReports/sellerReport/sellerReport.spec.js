describe("SellerReportCtrl", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $stateParams,
        notify,
        $log,
        api,
        $location,
        $anchorScroll,
        loading,
        binder;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");

        binder = {
            appliances: null,
            created_at: "2015-09-06T04:29:36Z",
            id: 1,
            name: "This is a fake home",
            primary: null,
            details: "this is a home",
            last_recall: null,
            partner: {
                address: null,
                binder_count: 7,
                binder_logo_id: null,
                code: "FAKE",
                contact: "Test Inspector",
                coupons: null,
                created_at: "2015-04-25T15:56:15Z",
                email: "girgalicious@gmail.com",
                id: 115,
                name: "Test Inspector",
                partner_type: "inspector",
                phone: "+17814928454",
                sellers_logo_id: null,
                tags: [],
                vendors: []
            },
            vendor: null,
            recalls: 0,
            documents: null,
            maintenance_items: null,
            binder_contractors: null,
            property: {
                country: null,
                address1: "555 50th",
                address2: null,
                city: "Seattle",
                state: "WA",
                zip: "98888"
            },
            subscription: null,
            seller_report: null,
            transfer: {
                binder_id: 1359,
                created_at: "2015-07-05T13:55:10Z",
                id: 498,
                status: "complete",
                transfer_to: "faketransferemail@gmail.com",
                transfer_type: "ownership",
                updated_at: "2015-07-05T18:23:37Z",
                user_id: 278
            },
            permissions: {
                can_create: true,
                can_create_seller_report: true,
                can_destroy: true,
                can_edit_seller_report: true,
                can_read: true,
                can_share: true,
                can_subscribe: true,
                can_transfer: true,
                can_view_master_report: false,
                can_write: true
            }
        };

        $anchorScroll = function() {};

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        $stateParams = {
            code: "5DS91"
        };

        api = {
            sellerReport: {
                get: function(code) {}
            },
            sellerReportPDF: {
                download: function(boolean_value) {}
            }
        };

        $log = {
            error: function(msg) {}
        };

        notify = {
            error: function(msg) {}
        };

        $location = {
            hash: function(element) {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("SellerReportCtrl", {
            "$location": $location,
            "$anchorScroll": $anchorScroll,
            "$stateParams": $stateParams,
            "hb.api": api,
            "$log": $log,
            "Notify": notify,
            "Loading": loading
        });
        $scope.ctrl = ctrl;

    }

    describe('ctrl.refresh', function() {

        it('should load all information for current binder in session', function() {

            var report = {
                appliances: [],
                binder: binder,
                code: "5DS91",
                binder_contractors: [],
                created_at: "2016-02-02T02:54:29Z",
                documents: [],
                finishes: [],
                id: 265,
                images: [],
                maintenance_items: [],
                projects: [{ id: 1, cost_cents: 10000 }],
                public: true
            };

            spyOn(api.sellerReport, "get").and.returnValue($q.when({
                data: report
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();

            expect(api.sellerReport.get).toHaveBeenCalledWith("5DS91");
            expect(ctrl.data.binder).toEqual(report.binder);
            expect(ctrl.is_public).toBe(true);
            expect(loading.show).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();

        });

        it('should load all information for current binder in session and show response_text', function() {

            var report = {
                appliances: [],
                binder: binder,
                code: "5DS91",
                binder_contractors: [],
                created_at: "2016-02-02T02:54:29Z",
                documents: [],
                finishes: [],
                id: 265,
                images: [],
                maintenance_items: [],
                projects: [],
                public: false
            };

            spyOn(api.sellerReport, "get").and.returnValue($q.when({
                data: report
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();

            expect(api.sellerReport.get).toHaveBeenCalledWith("5DS91");
            expect(ctrl.data.binder).toEqual(report.binder);
            expect(ctrl.is_public).toBe(false);
            expect(ctrl.response_text).toBe("The HomeBinder Seller Report for this property is no longer public. If you have any questions about " + binder.property.address1 + " " + binder.property.city + ", " + binder.property.state + " please contact the real estate professional or seller directly.  If you have technical questions about HomeBinder please email support@homebinder.com.");
            expect(loading.show).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();

        });

        it('should attempt to retrieve report and return an error', function() {

            spyOn(api.sellerReport, "get").and.returnValue($q.reject({ data: "error" }));
            spyOn(notify, "error");
            spyOn($log, "error");
            createController();
            $rootScope.$apply();

            expect($log.error).toHaveBeenCalled();
        });

    });

    describe('ctrl.download', function() {
        it('should attempt to download PDF report', function() {

            var report = {
                appliances: [],
                binder: binder,
                code: "5DS91",
                binder_contractors: [],
                created_at: "2016-02-02T02:54:29Z",
                documents: [],
                finishes: [],
                id: 265,
                images: [],
                maintenance_items: [],
                projects: [],
                public: true
            };
            spyOn(loading, "show");
            spyOn(loading, "close");

            spyOn(api.sellerReport, "get").and.returnValue($q.when({
                data: report
            }));
            spyOn(api.sellerReportPDF, "download").and.returnValue($q.when({
                data: "error"
            }));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            ctrl.download(false);

            expect(api.sellerReportPDF.download).toHaveBeenCalledWith("5DS91", false);
        });
    });

    describe('ctrl.downloadSuccess', function() {
        it('should close loading', function() {

            var report = {
                appliances: [],
                binder: binder,
                code: "5DS91",
                binder_contractors: [],
                created_at: "2016-02-02T02:54:29Z",
                documents: [],
                finishes: [],
                id: 265,
                images: [],
                maintenance_items: [],
                projects: [],
                public: true
            };
            spyOn(loading, "show");
            spyOn(loading, "close");

            spyOn(api.sellerReport, "get").and.returnValue($q.when({
                data: report
            }));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            ctrl.data = {
                binder: {
                    name: "test"
                }
            };
            ctrl.$window = {
                navigator: {
                    msSaveOrOpenBlob: function() {}
                }
            };
            ctrl.downloadSuccess(true, {});

            expect(loading.close).toHaveBeenCalled();
        });

        it('should close loading', function() {

            var report = {
                appliances: [],
                binder: binder,
                code: "5DS91",
                binder_contractors: [],
                created_at: "2016-02-02T02:54:29Z",
                documents: [],
                finishes: [],
                id: 265,
                images: [],
                maintenance_items: [],
                projects: [],
                public: true
            };
            spyOn(loading, "show");
            spyOn(loading, "close");

            spyOn(api.sellerReport, "get").and.returnValue($q.when({
                data: report
            }));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            ctrl.data = {
                binder: {
                    name: "test"
                }
            };
            ctrl.$window = {
                navigator: {

                },
                URL: {
                    createObjectURL: function() {},
                    revokeObjectURL: function() {}
                }
            };
            ctrl.downloadSuccess(true, {});

            expect(loading.close).toHaveBeenCalled();
        });
    });

    describe('ctrl.downloadError', function() {
        it('should close loading', function() {

            var report = {
                appliances: [],
                binder: binder,
                code: "5DS91",
                binder_contractors: [],
                created_at: "2016-02-02T02:54:29Z",
                documents: [],
                finishes: [],
                id: 265,
                images: [],
                maintenance_items: [],
                projects: [],
                public: true
            };
            spyOn(loading, "show");
            spyOn(loading, "close");

            spyOn(api.sellerReport, "get").and.returnValue($q.when({
                data: report
            }));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            ctrl.downloadError(true, {});

            expect(notify.error).toHaveBeenCalled();
        });
    });

});