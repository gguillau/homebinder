(function() {
    "use strict";

    angular
        .module("hb.homebinders.paints", [
            "hb.homebinders.paints.routes",
            "hb.homebinders.paints.details",
            "hb.homebinders.paints.form",
            "hb.homebinders.paints.modal"
        ])
        .directive("binderPaints", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/paints/paints.tpl.html",
                controller: "BinderPaintsController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("BinderPaintsController", [
            "BinderItemsIndexController",
            "hb.resources",
            Controller
        ]);

    function Controller(BinderItemsIndexController, resources) {
        var Model = function() {
            // call the parent class
            BinderItemsIndexController.call(this);
            this.resources = angular.extend({}, this.resources, resources.binderPaintsIndex);
            this.refreshCall = this.api.paint.all;
            this.itemType = "paints";
            this.modalController = "PaintModalController";
            this.itemNewState = "binder.paintsNew";
            this.itemDetailsState = "binder.paintsDetails";
            this.init();
        };

        Model.prototype = Object.create(BinderItemsIndexController.prototype);

        this.model = new Model();
    }

})();