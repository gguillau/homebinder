(function() {
    "use strict";

    angular
        .module("hb.homebinders.paints.form", [])
        .directive("hbPaintForm", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/paints/form/form.tpl.html",
                controller: "PaintFormController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        // Model backing the paint modal form
        .controller("PaintFormController", [
            "hb.resources",
            "BinderItemsFormController",
            "hb.api",
            "UpgradeForm",
            "Context",
            "$stateParams",
            Controller
        ]);

    function Controller(resources, BinderItemsFormController, api, UpgradeForm, context, $stateParams) {
        var that = this;
        var Model = function() {
            this.cfg = that.cfg;
            this.onSaved = that.onSaved;
            this.onCancel = that.onCancel;
            this.resources = angular.extend({}, this.resources, resources.binderPaintsForm);
            this.taggerCfg = {
                paintTags: false
            };
            this.tagName = "paint_";
            // call the parent class
            this.saveCall = api.paint.update;
            this.createCall = api.paint.create;
            this.deleteCall = api.paint.destroy;
            this.refreshCall = api.paint.get;
            
            // If $stateParams has an id, we are editing an exisiting item and back should send us to that item's details
            if ($stateParams.id) {
                this.indexState = "binder.paintsDetails";
                this.indexParams = { binderId: $stateParams.binderId, id: $stateParams.id };
            }
            // Else, we were creating a new item and back should send us to the list of items
            else {
                this.indexState = "binder.paints";
                this.indexParams = { binderId: $stateParams.binderId };
            }
            this.editState = "binder.paintsEdit";
            
            BinderItemsFormController.call(this);
            this.init();
            if (this.imgUploader) {
                this.imgUploader.hide_upload = true;
            }
            this.loadPaintTypes();
            this.loadPaintSheens();
            this.paintManufacturers();
            this.loadStores();
        };

        Model.prototype = Object.create(BinderItemsFormController.prototype);

        angular.extend(Model.prototype, {

            populate: function() {
                // if a item was passed in store the values
                if (this.item && this.item.id) {
                    this.name = this.item.name;
                    this.manufacturer = this.item.manufacturer;
                    this.code = this.item.code;
                    this.makeup = this.item.makeup;
                    this.paint_type = this.item.paint_type;
                    this.sheen = this.item.sheen;
                    this.tags = this.item.tags;
                    if (this.item.purchase) {
                        this.store = this.item.purchase.store;
                        this.price = this.item.purchase.number;
                        this.purchase_date = this.item.purchase.date;
                    }
                    this.details = this.item.details;
                    this.setUploadParams();
                }
            },

            loadPaintTypes: function() {
                api.paintType.all().then(
                    angular.bind(this, this.onLoadPaintTypes),
                    angular.bind(this, this.onLoadPaintTypesError)
                );
            },

            onLoadPaintTypes: function(response) {
                this.paint_types = response.data;
            },

            onLoadPaintTypesError: function(response) {
                this.$log.error(response);
            },

            paintManufacturers: function() {
                api.paintManufacturer.all().then(
                    angular.bind(this, this.onLoadPaintManufacturers),
                    angular.bind(this, this.onLoadPaintManufacturersError)
                );
            },

            onLoadPaintManufacturers: function(response) {
                this.manufacturers = response.data;
            },

            onLoadPaintManufacturersError: function(response) {
                this.$log.error(response);
            },

            loadStores: function() {
                api.store.all().then(
                    angular.bind(this, this.onLoadStores),
                    angular.bind(this, this.onLoadStoresError)
                );
            },

            onLoadStores: function(response) {
                this.stores = response.data.map(function(store) {
                    return store.name;
                });
            },

            onLoadStoresError: function(response) {
                this.$log.error(response);
            },

            loadPaintSheens: function() {
                api.paintSheen.all().then(
                    angular.bind(this, this.onLoadPaintSheens),
                    angular.bind(this, this.onLoadPaintSheensError)
                );
            },

            onLoadPaintSheens: function(response) {
                this.sheens = response.data;
            },

            onLoadPaintSheensError: function(response) {
                this.$log.error(response);
            },

            setPayload: function() {
                // create payload
                return {
                    binder_id: this.binderId,
                    name: this.name,
                    manufacturer: this.manufacturer,
                    code: this.code,
                    makeup: this.makeup,
                    sheen: this.sheen,
                    paint_type: this.paint_type,
                    purchase_attributes: {
                        store: this.store,
                        price: this.price ? this.price : 0,
                        date: this.purchase_date
                    },
                    details: this.details,
                    tags_attributes: this.tags
                };
            },

            setUploadParams: function() {
                this.uploadParams = {
                    binder_id: this.item.binder_id,
                    paint_id: this.item.id
                };
            },

            uploadPhotos: function() {
                context.getBinder().then(angular.bind(function(binder) {
                    if (binder.subscription.plan === "free") {
                        UpgradeForm.show({
                            upgradeText: "Please upgrade to enable photo processing of your Paint Can photos."
                        });
                    }
                    else {
                        angular.element('#uploadImages').trigger('click');
                    }
                }));
            }
        });

        this.model = new Model();
    }

})();