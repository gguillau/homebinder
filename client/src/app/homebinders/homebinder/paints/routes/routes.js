angular
    .module("hb.homebinders.paints.routes", [])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            // paint index state
            .state("binder.paints", {
                url: "^/binders/:binderId/paints",
                template: "<binder-paints></binder-paints>",
                access: {
                    requiresLogin: true
                }
            })
            // paint details state
            .state("binder.paintsDetails", {
                url: "^/binders/:binderId/paints/{id:int}",
                template: "<hb-paint-details></hb-paint-details>",
                access: {
                    requiresLogin: true
                }
            })
            // state for new paint
            .state("binder.paintsNew", {
                url: "^/binders/:binderId/paints/new",
                template: "<hb-paint-form></hb-paint-form>",
                access: {
                    requiresLogin: true
                }
            })
            // state for edit paint
            .state("binder.paintsEdit", {
                url: "^/binders/:binderId/paints/{id:int}/edit",
                template: "<hb-paint-form></hb-paint-form>",
                access: {
                    requiresLogin: true
                }
            });
    }]);