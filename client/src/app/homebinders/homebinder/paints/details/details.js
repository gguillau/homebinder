(function() {
	"use strict";

	angular
		.module("hb.homebinders.paints.details", [])
		.directive("hbPaintDetails", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/paints/details/details.tpl.html",
				controller: "PaintDetailsController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("PaintDetailsController", [
			"hb.resources",
			"BinderItemsDetailsController",
			"hb.api",
			"Context",
			"UpgradeForm",
			"$stateParams",
			Controller
		]);

	function Controller(resources, BinderItemsDetailsController, api, context, UpgradeForm, $stateParams) {
		var that = this;
		var Model = function() {
			this.cfg = that.cfg;
			this.onDestroyed = that.onDestroyed;
			this.onClose = that.onClose;
			this.nameProperty = "name";
			this.resources = angular.extend({}, this.resources, resources.binderPaintsDetails);
			this.deleteCall = api.paint.destroy;
			this.refreshCall = api.paint.get;
			this.indexState = "binder.paints";
			this.indexParams = { binderId: $stateParams.binderId };
			this.editState = "binder.paintsEdit";
			
			// call the parent class
			BinderItemsDetailsController.call(this);
			this.imgUploader.hide_upload = true;
		};

		Model.prototype = Object.create(BinderItemsDetailsController.prototype);

		angular.extend(Model.prototype, {
			populate: function() {
				this.editParams = { binderId: $stateParams.binderId, id: this.item.id };
				this.tagId = "paint_" + this.item.id;
				this.uploadParams = {
					binder_id: this.item.binder_id,
					paint_id: this.item.id
				};
			},

			uploadPhotos: function() {
				context.getBinder().then(function(binder) {
					if (binder.subscription.plan === "free") {
						UpgradeForm.show({
							upgradeText: "Please upgrade to enable photo processing of your Paint Can photos."
						});
					}
					else {
						angular.element('#uploadImages').trigger('click');
					}
				});
			}
		});

		this.model = new Model();
	}
})();