(function() {
	"use strict";

	angular
		.module("hb.homebinders.share.form", [
			"hb.homebinders.share.form.index"
		])
		.factory("ShareForm", [
			"ModalService",
			function(Modals) {
				return {
					show: function(opts) {
						Modals.show({
							templateUrl: "homebinders/homebinder/share/share.tpl.html",
							controller: "ShareFormController as ctrl",
							resolveData: opts,
							backdrop: true,
							closed: opts.closed
						});
					}
				};
			}
		])
		.factory("ShareBinderController", [
			"hb.api",
			"Notify",
			"hb.resources",
			"Address",
			"$stateParams",
			function ShareBinderController(api, notify, resources, address, $stateParams) {
				var Model = function() {
					this.model = { phone: {} };
					this.model.role_name = "reader";
					this.api = api;
					this.notify = notify;
					this.resources = resources.share;
					this.resources = angular.extend({}, this.resources, resources.usersForm);
					this.address = address;
					this.$stateParams = $stateParams;
					this.binder = {
						id: this.$stateParams.binderId
					};
				};

				angular.extend(Model.prototype, {
					init: function() {
						this.api.binder.get(this.binder.id).then(
							angular.bind(this, this.onBinderSuccess),
							angular.bind(this, this.onError)
						);
					},

					onBinderSuccess: function(response) {
						this.binder = response.data;
						this.transfer = response.data.transfer;
						this.country = this.address.findCountryByCode(this.binder.property.country);
						if (this.transfer) {
							this.getUser();
						}
					},

					getUser: function() {
						this.api.user.get(this.transfer.receiver_id).then(
							angular.bind(this, this.getUserSuccess),
							angular.bind(this, this.getUserError));
					},

					getUserSuccess: function(response) {
						this.model = {
							first: response.data.user_profile_attributes.first_name,
							last: response.data.user_profile_attributes.last_name,
							shared_with_email: response.data.email,
							phone: response.data.user_profile_attributes.mobile_phone,
							role_name: "co_owner",
							readonly: false
						};
						this.country = this.address.findCountryByCode(response.data.user_profile_attributes.address_attributes.country);
						if (response.data.sign_in_count > 0 || response.data.role != "homeowner") {
							this.model.readonly = true;
						}
					},

					submit: function() {
						this.processing = true;
						this.api.share.create({
							user: {
								first: this.model.first,
								last: this.model.last,
								email: this.model.shared_with_email,
								phone: this.model.phone.national
							},
							binder_id: this.binder.id,
							role_name: this.model.role_name
						}).then(
							angular.bind(this, this.onShareSuccess),
							angular.bind(this, this.onError)
						);
					},

					onShareSuccess: function(response) {
						this.processing = false;
						this.notify.info(this.resources.shared);
					},

					onError: function(response) {
						this.processing = false;
						this.error = response.data;
						this.notify.error(response.data);
					}
				});

				return Model;
			}
		])
		.controller("ShareFormController", [
			"ShareBinderController",
			"hb.api",
			"$modalInstance",
			"data",
			ShareFormController
		]);

	function ShareFormController(ShareBinderController, api, $modalInstance, opts) {
		var Model = function() {
			// Call parent
			ShareBinderController.call(this);
			this.api = api;
			this.$modal = $modalInstance;
			if(opts.binder){
				this.binder.id = opts.binder.id;
			}
			this.init();
		};

		Model.prototype = Object.create(ShareBinderController.prototype);

		angular.extend(Model.prototype, {
			onShareSuccess: function(response) {
				this.processing = false;
				this.notify.info(this.resources.shared);
				this.$modal.close();
			},

			cancel: function() {
				this.$modal.dismiss("cancel");
			}
		});

		this.model = new Model();
	}
})();