describe("ShareFormController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        api,
        $modalInstance,
        data,
        ShareForm,
        ModalService,
        ShareBinderController;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        ShareForm = _$injector_.get("ShareForm");
        ModalService = _$injector_.get("ModalService");
        ShareBinderController = _$injector_.get("ShareBinderController");

        data = {
            binder: {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                property: {},
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                subscription: null,
                seller_report: null,
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                },
                tags: [{
                    id: 56,
                    metadata: '{"firstName":"Share","lastName":"Test","email":"sharetest@gmail.com"}',
                    tag: "client",
                    tag_scope: null
                }]
            }
        };

        api = {
            binder: {
                share: function() {},
                get: function() {}
            },
            user: {
                get: function() {}
            },
            share: {
                create: function() {}
            }
        };

        $modalInstance = {
            close: function(button) {},
            dismiss: function(button) {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("ShareFormController", {
            "ShareBinderController": ShareBinderController,
            "hb.api": api,
            "data": data,
            "$modalInstance": $modalInstance
        });

        $scope.ctrl = ctrl;

    }

    describe('ctrl.init', function() {
        it('should set shared_with_email and role name as co_owner', function() {
            data.binder.transfer = {
                id: 1,
                receiver_id: 2
            };
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: data.binder
            }));
            spyOn(api.user, "get").and.returnValue($q.when({
                data: {
                    id: 1,
                    email: "sharetest@gmail.com",
                    sign_in_count: 1,
                    user_profile_attributes: {
                        address_attributes: {},
                        first_name: "Share",
                        last_name: "Test",
                        mobile_phone: {}
                    }
                }
            }));

            createController();
            $rootScope.$apply();

            expect(ctrl.model.model.first).toBe("Share");
            expect(ctrl.model.model.last).toBe("Test");
            expect(ctrl.model.model.shared_with_email).toBe("sharetest@gmail.com");
            expect(ctrl.model.model.role_name).toBe("co_owner");

        });
    });

    describe('ctrl.submit', function() {
        it('should create a new share object, close the modal', function() {
            data.binder.transfer = {
                id: 1,
                receiver_id: 2
            };
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: data.binder
            }));
            spyOn(api.user, "get").and.returnValue($q.when({
                data: {
                    id: 1,
                    email: "sharetest@gmail.com",
                    user_profile_attributes: {
                        address_attributes: {},
                        first_name: "Share",
                        last_name: "Test",
                        mobile_phone: {}
                    }
                }
            }));
            spyOn(api.share, "create").and.returnValue($q.when({
                data: {}
            }));
            spyOn($modalInstance, "close");

            createController();
            $rootScope.$apply();

            ctrl.model.submit();
            $rootScope.$apply();

            expect(api.share.create).toHaveBeenCalledWith({
                user: {
                    first: "Share",
                    last: "Test",
                    email: 'sharetest@gmail.com',
                    phone: undefined
                },
                binder_id: 1,
                role_name: "co_owner"
            });
            expect($modalInstance.close).toHaveBeenCalled();
        });

        it('should try to create a new share object and return an error', function() {
            data.binder.transfer = {
                id: 1,
                receiver_id: 2
            };
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: data.binder
            }));
            spyOn(api.user, "get").and.returnValue($q.when({
                data: {
                    id: 1,
                    email: "sharetest@gmail.com",
                    user_profile_attributes: {
                        address_attributes: {},
                        first_name: "Share",
                        last_name: "Test",
                        mobile_phone: {}
                    }
                }
            }));
            spyOn(api.share, "create").and.returnValue($q.reject({
                data: "error"
            }));

            createController();
            $rootScope.$apply();

            ctrl.model.submit();
            $rootScope.$apply();

            expect(api.share.create).toHaveBeenCalledWith({
                user: {
                    first: "Share",
                    last: "Test",
                    email: 'sharetest@gmail.com',
                    phone: undefined
                },
                binder_id: 1,
                role_name: "co_owner"
            });
            expect(ctrl.model.error).toEqual("error");
        });

    });

    describe('ctrl.cancel', function() {
        it('should dismiss the modal', function() {
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: data.binder
            }));
            spyOn($modalInstance, "dismiss");
            createController();
            ctrl.model.cancel();
            $rootScope.$apply();

            expect($modalInstance.dismiss).toHaveBeenCalledWith("cancel");
        });
    });

    describe('ShareForm', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            ShareForm.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });

});