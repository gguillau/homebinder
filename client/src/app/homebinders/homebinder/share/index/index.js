(function() {
    "use strict";

    angular.module('hb.homebinders.share.form.index', [])
        .factory("SharesController", [
            "hb.framework.indexBase",
            "hb.api",
            "hb.resources",
            "$stateParams",
            "Notify",
            "$log",
            "Session",
            function(IndexBase, api, resources, $stateParams, notify, $log, session) {
                var Model = function() {
                    // call the parent class
                    IndexBase.call(this);
                    this.currentUser = session.getUser();
                    this.api = api;
                    // apply sellerReport specific resource strings
                    this.resources = angular.extend({}, this.resources, resources.share);
                    // set the refresh API call
                    this.refreshCall = api.share.all;
                    // set the delete API call
                    this.deleteCall = api.share.destroy;
                    // delete prop
                    this.nameProperty = "created_at";
                    this.maxSize = 10;
                    this.sortOptions = [{
                        orderBy: "shares.created_at",
                        order: "desc"
                    }, {
                        orderBy: "shares.created_at",
                        order: "asc"
                    }];
                    // "sent", "shared", "declined", "delivery_failed"
                    this.statusOptions = [{
                        name: undefined,
                        value: "All Statuses"
                    }, {
                        name: "sent",
                        value: "Sent"
                    }, {
                        name: "shared",
                        value: "Shared"
                    }, {
                        name: "declined",
                        value: "Declined"
                    }, {
                        name: "delivery_failed",
                        value: "Delivery Failed"
                    }];
                    this.sortOption = this.sortOptions[0];
                    this.orderBy = this.sortOption.orderBy;
                    this.order = this.sortOption.order;
                    this.shareArgs = {
                        shared_with_id: $stateParams.userId,
                        binder_id: $stateParams.binderId,
                        status: undefined,
                        shared_by_id: undefined
                    };
                    this.settings = {
                        canSearch: false
                    };
                    this.setHeaders();
                };

                Model.prototype = Object.create(IndexBase.prototype);

                angular.extend(Model.prototype, {

                    setHeaders: function() {
                        this.headers = this.resources.headers.map(angular.bind(this, function(attribute) {
                            var sortable = false,
                                orderBy = null,
                                sorted = false,
                                show = true;
                            if (attribute === "Binder ID") {
                                sorted = true;
                                sortable = true;
                                orderBy = "shares.binder_id";
                            }
                            else if (attribute === "Sender") {
                                sortable = true;
                                orderBy = "shares.shared_by_id";
                            }
                            else if (attribute === "Receiver") {
                                sortable = true;
                                orderBy = "shares.shared_with_id";
                            }
                            else if (attribute === "Status") {
                                sortable = true;
                                orderBy = "shares.status";
                            }
                            return {
                                name: attribute,
                                sortable: sortable,
                                sorted: sorted,
                                orderBy: orderBy,
                                show: show,
                                order: "desc"
                            };
                        }));
                        this.sortOption = this.headers[0];
                    },

                    addQueryArgs: function() {
                        this.addArgs("binder_id");
                        this.addArgs("shared_with_id");
                        this.addArgs("shared_by_id");
                        this.addArgs("status");
                    },

                    addArgs: function(arg) {
                        if (this.shareArgs[arg]) {
                            this.queryArgs[arg] = this.shareArgs[arg];
                        }
                        else {
                            delete this.queryArgs[arg];
                        }
                    },

                    resend: function(share) {
                        api.share.resend(share.id).then(
                            angular.bind(this, this.resendSuccess),
                            angular.bind(this, this.onError)
                        );
                    },

                    resendSuccess: function(response) {
                        notify.info("Resent!");
                    },

                    onError: function(response) {
                        $log.error(response);
                        notify.error(response);
                    },

                    canResend: function(share) {
                        switch (share.status) {
                            case "sent":
                                return true;
                            default:
                                return false;
                        }
                    }
                });

                return Model;
            }
        ]);

})();