describe("SharesController", function() {
    var TestClass,
        SharesController,
        api,
        $q,
        notify;

    // load the homebinder module
    beforeEach(module("homebinder"));

    // get the base class under test
    beforeEach(inject(function($injector) {
        SharesController = $injector.get("SharesController");
        api = $injector.get("hb.api");
        $q = $injector.get("$q");
        notify = $injector.get("Notify");
    }));

    function createTestClass() {
        TestClass = function() {
            SharesController.call(this);
        };

        TestClass.prototype = Object.create(SharesController.prototype);

        return new TestClass();
    }

    describe("setHeaders", function() {
        it("creates the headers", inject(function() {
            var model = createTestClass();
            model.setHeaders();

            expect(model.headers.length).toEqual(5);
        }));
    });

    describe("addQueryArgs", function() {
        it("adds the query arguments", inject(function() {
            var model = createTestClass();

            model.shareArgs["binder_id"] = 1;
            model.shareArgs["shared_with_id"] = 2;
            model.shareArgs["shared_by_id"] = 3;
            model.shareArgs["status"] = "sent";
            model.addQueryArgs();

            expect(model.queryArgs["binder_id"]).toEqual(1);
            expect(model.queryArgs["shared_with_id"]).toEqual(2);
            expect(model.queryArgs["shared_by_id"]).toEqual(3);
            expect(model.queryArgs["status"]).toEqual("sent");
        }));

    });

    describe("addArgs", function() {
        it("adds the query argument", inject(function() {
            var model = createTestClass();
            model.shareArgs["status"] = "created";
            model.addArgs("status");

            expect(model.queryArgs["status"]).toEqual("created");
        }));

        it("deletes the query argument", inject(function() {
            var model = createTestClass();

            model.shareArgs["status"] = "accepted";
            model.addArgs("status");
            model.shareArgs["status"] = undefined;
            model.addArgs("status");

            expect(model.queryArgs["status"]).toBe(undefined);
        }));
    });

    describe("resend", function() {
        it("calls api.share.resend", function() {
            var model = createTestClass();
            spyOn(api.share, "resend").and.returnValue($q.when({
                data: "success"
            }));

            model.resend({ id: 1 });

            expect(api.share.resend).toHaveBeenCalled();
        });
    });

    describe("resendSuccess", function() {
        it("calls notify info", function() {
            var model = createTestClass();

            spyOn(notify, "info");

            model.resendSuccess({ data: "" });

            expect(notify.info).toHaveBeenCalled();
        });
    });

    describe("onError", function() {
        it("calls notify error", function() {
            var model = createTestClass();

            spyOn(notify, "error");

            model.onError({ data: "" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("canResend", function() {
        it("returns true", function() {
            var model = createTestClass();

            spyOn(notify, "error");

            var result = model.canResend({ status: "sent" });

            expect(result).toBe(true);
        });
        it("returns false", function() {
            var model = createTestClass();

            spyOn(notify, "error");

            var result = model.canResend({ status: "accepted" });

            expect(result).toBe(false);
        });
    });

});