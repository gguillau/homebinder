describe("Permissions", function() {
    'use strict';

    var Permissions, session, $stateParams, $state, context, $scope, $q, api;

    beforeEach(function() {
        module("homebinder");

        inject(function(_$injector_, _$rootScope_) {
            $scope = _$rootScope_;
            Permissions = _$injector_.get("Permissions");
            session = _$injector_.get("Session");
            $stateParams = _$injector_.get("$stateParams");
            $state = _$injector_.get("$state");
            context = _$injector_.get("Context");
            $q = _$injector_.get("$q");
            api = _$injector_.get("hb.api");

            $scope.$apply();
        });
    });

    it("returns true", function() {
        var result;
        spyOn(session, "getUser");

        // call the function
        result = Permissions.canWrite();

        // verify results
        expect(result).toBe(true);
    });

    it("returns true", function() {
        var result;
        spyOn(session, "getUser").and.returnValue({ role: "admin" });
        var config = {
            url: ""
        };
        // call the function
        result = Permissions.canWrite(config);

        // verify results
        expect(result).toBe(true);
    });

    it("returns true", function() {
        var result;
        spyOn(session, "getUser").and.returnValue({ role: "admin" });
        var config = {
            url: function() { return "/api/v1"; },
            method: "GET"
        };
        // call the function
        result = Permissions.canWrite(config);

        // verify results
        expect(result).toBe(true);
    });

    it("returns true", function() {
        var result;
        spyOn(session, "getUser").and.returnValue({ role: "admin" });
        var config = {
            url: function() { return "/api/v1"; },
            method: "POST"
        };
        // call the function
        result = Permissions.canWrite(config);

        // verify results
        expect(result).toBe(true);
    });

    it("returns true", function() {
        var result;
        spyOn(session, "getUser").and.returnValue({ role: "admin" });
        var config = {
            url: function() { return "/api/v1"; },
            method: "POST"
        };
        // call the function
        result = Permissions.canWrite(config);

        // verify results
        expect(result).toBe(true);
    });

    it("returns true", function() {
        var result;
        spyOn(session, "getUser").and.returnValue({ role: "admin" });
        $state.go("admin.users");
        $scope.$apply();
        $stateParams.binderId = 1;
        var config = {
            url: function() { return "/api/v1"; },
            method: "POST"
        };
        // call the function
        result = Permissions.canWrite(config);

        // verify results
        expect(result).toBe(true);
    });

    it("returns true", function() {
        var result;
        spyOn(session, "getUser").and.returnValue({ role: "admin" });
        spyOn(api.binder, "get").and.returnValue($q.when({ data: {} }));
        $state.go("binder.appliances", { binderId: 1 });
        $scope.$apply();
        spyOn(context, "getBinder").and.returnValue($q.when({ permissions: { can_write: true } }));
        var config = {
            url: function() { return "/api/v1"; },
            method: "POST"
        };
        // call the function
        result = Permissions.canWrite(config);

        // verify results
        expect(result).toBe(false);
    });

    it("returns false", function() {
        var result;
        spyOn(session, "getUser").and.returnValue({ role: "admin" });
        spyOn(api.binder, "get").and.returnValue($q.when({ data: {} }));
        $state.go("binder.appliances", { binderId: 1 });
        $scope.$apply();

        spyOn(context, "getBinder").and.returnValue($q.when({}));
        var config = {
            url: function() { return "/api/v1"; },
            method: "POST"
        };
        // call the function
        result = Permissions.canWrite(config);

        // verify results
        expect(result).toBe(false);
    });

    it("returns false", function() {
        var result;
        spyOn(session, "getUser").and.returnValue({ role: "admin" });
        spyOn(api.binder, "get").and.returnValue($q.when({ data: {} }));
        $state.go("binder.appliances", { binderId: 1 });
        $scope.$apply();

        spyOn(context, "getBinder").and.returnValue($q.when({}));
        var config = {
            url: function() { return "/api/v1"; },
            method: "DELETE"
        };
        // call the function
        result = Permissions.canWrite(config);

        // verify results
        expect(result).toBe(false);
    });

    it("returns false", function() {
        var result;
        spyOn(session, "getUser").and.returnValue({ role: "admin" });
        spyOn(api.binder, "get").and.returnValue($q.when({ data: {} }));
        spyOn(context, "getBinder").and.returnValue($q.when({}));
        $state.go("binder.appliances", { binderId: 1 });
        $scope.$apply();

        var config = {
            url: function() { return "/api/v1"; },
            method: "PUT"
        };
        // call the function
        result = Permissions.canWrite(config);

        // verify results
        expect(result).toBe(false);
    });
});