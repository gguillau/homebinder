angular
    .module("hb.userSettings.permissions", [])
    .factory('Permissions', [
        "$stateParams",
        "Context",
        "Session",
        "$state",
        function($stateParams, Context, Session, $state) {
            function canEdit() {
                if ($state.current.name.indexOf("binder") === 0) {
                    Context.getBinder().then(function(binder) {
                        if (!binder) {
                            return false;
                        }
                        return binder.data.permissions.can_write;
                    });
                }
                else {
                    return true;
                }
            }

            function checkState() {
                if ($stateParams.binderId) {
                    if (!canEdit()) {
                        return false;
                    }
                }
                return true;
            }

            function checkMethod(method) {

                switch (method) {
                    case "DELETE":
                        return true;
                    case "POST":
                        return true;
                    case "PUT":
                        return true;
                    default:
                        return false;
                }
            }

            function checkUrl(url) {
                if (typeof url === "function") {
                    var url_link = url();
                    var type = typeof url;
                    var result = type === "function";
                    if (result === true) {
                        if (url_link.indexOf("/api/v1") === 0) {
                            return true;
                        }
                    }
                }
                return false;
            }

            return {
                canWrite: function(config) {
                    var user = Session.getUser();
                    if (!user) {
                        return true;
                    }
                    if (!$stateParams.partnerId) {
                        if (checkUrl(config.url)) {
                            if (checkMethod(config.method)) {
                                return checkState();
                            }
                        }
                    }
                    return true;
                }
            };
        }
    ]);