angular
	.module("hb.userSettings.shareBinder", [])
	.directive("userShareBinder", function() {
		return {
			restrict: "E",
			templateUrl: "homebinders/homebinder/userSettings/shareBinder/shareBinder.tpl.html",
			controller: "UserShareBinderController",
			controllerAs: "ctrl",
			bindToController: true,
			scope: {}
		};
	})
	.controller("UserShareBinderController", [
		"ShareBinderController",
		"$state",
		"$stateParams",
		"ModalService",
		"hb.api",
		"$log",
		"Notify",
		"Context",
		UserShareBinderController
	]);

function UserShareBinderController(ShareBinderController, $state, $stateParams, ModalService, api, $log, notify, Context) {
	var Model = function() {
		// Call parent
		ShareBinderController.call(this);
		this.$state = $state;
		this.$stateParams = $stateParams;
		this.modals = ModalService;
		this.api = api;
		this.$log = $log;
		this.notify = notify;
		this.context = Context;

		var response = this.context.getBinder(this.$stateParams.binderId);
		this.getAccessList(response);
		this.init();
	};

	Model.prototype = Object.create(ShareBinderController.prototype);

	angular.extend(Model.prototype, {
		// Get list of people who have access to this binder
		getAccessList: function(response) {
			this.data = response.$$state.value;
			this.access_list = this.data.access_list;
		},

		onShareSuccess: function(response) {
			this.processing = false;
			this.notify.info(this.resources.shared);
			this.$state.reload();
		},

		removeAccess: function(user) {
			if (user.role != "owner") {
				this.modals.confirm({
					title: this.resources.removeAccess,
					minorMessage: this.resources.removeAccessText + " " + user.user.email + "?",
					confirm: angular.bind(this, this.onRemoveAccess, user)
				});
			}
		},

		onRemoveAccess: function(acl) {
			this.api.binder.remove_acl(this.data.id, acl.user.id).then(
				angular.bind(this, this.refreshAcl),
				angular.bind(this, this.onRemoveAccessError)
			);
		},

		onRemoveAccessError: function(response) {
			this.$log.error(response.data);
			this.notify.error(response.data);
		},

		refreshAcl: function() {
			this.api.binder.get_acl(this.data.id).then(
				angular.bind(this, this.onRefreshAcl),
				angular.bind(this, this.onRefreshAclError)
			);
		},

		onRefreshAcl: function(response) {
			this.notify.success(this.resources.accessRemoved);
			this.access_list = response.data;
		},

		onRefreshAclError: function(response) {
			this.$log.error(response.data);
		}
	});

	this.model = new Model();

}


