describe("UserShareBinderController", function() {
    var $rootScope,
        $controller,
        $q,
        ShareBinderController,
        ModalService,
        api,
        $state,
        $stateParams,
        notify,
        $scope,
        $log,
        response,
        Context,
        ctrl;

    // load the homebinder module
    beforeEach(module("homebinder"));

    beforeEach(inject(function(_$controller_, _$injector_, _$templateCache_, _$compile_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        ShareBinderController = _$injector_.get("ShareBinderController");
        ModalService = _$injector_.get("ModalService");
        api = _$injector_.get("hb.api");

        $state = {
            go: function() {},
            reload: function() {}
        };

        $stateParams = {
            binderId: 1
        };

        notify = {
            success: function() {},
            info: function() {},
            error: function() {}
        };

        $log = {
            error: function() {}
        };

        response = {
            $$state: {
                value: {
                    access_list: []
                }
            }
        };

        Context = {
            getBinder: function() {
                return response;
            }
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("UserShareBinderController", {
            "ShareBinderController": ShareBinderController,
            "$state": $state,
            "$stateParams": $stateParams,
            "ModalService": ModalService,
            "hb.api": api,
            "$log": $log,
            "Notify": notify,
            "Context": Context
        });
        $scope.ctrl = ctrl;
    }

    describe("getAccessList", function() {
        it("sets data and access_list", function() {
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: {}
            }));
            spyOn(api.user, "get").and.returnValue($q.when({
                data: {}
            }));
            
            createController();

            var value = {
                access_list: [
                    {id: 1}, {id: 2}, {id: 3}
                ]
            };
            
            response = {
                $$state: {
                    value: value
                }
            };

            ctrl.model.getAccessList(response);
            $rootScope.$apply();

            expect(ctrl.model.data).toEqual(value);
            expect(ctrl.model.access_list).toEqual([{id: 1}, {id: 2}, {id: 3}]);
        });
    });
    
    describe("onShareSuccess", function() {
        it("calls notify and $state.reload", function() {
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: {}
            }));
            spyOn(api.user, "get").and.returnValue($q.when({
                data: {}
            }));
            
            spyOn(notify, "info");
            spyOn($state, "reload");
            
            createController();

            ctrl.model.onShareSuccess();
            $rootScope.$apply();

            expect(ctrl.model.processing).toEqual(false);
            expect(notify.info).toHaveBeenCalled();
            expect($state.reload).toHaveBeenCalled();
        });
    });
    
    describe("removeAccess", function() {
        it("does not call modal when user.role is owner", function() {
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: {}
            }));
            spyOn(api.user, "get").and.returnValue($q.when({
                data: {}
            }));
            spyOn(ModalService, "confirm");

            createController();
            
            var user = {role: "owner"};

            ctrl.model.removeAccess(user);
            $rootScope.$apply();
            
            expect(ModalService.confirm).not.toHaveBeenCalled();
        });
        
        it("calls modal when user.role is not owner", function() {
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: {}
            }));
            spyOn(api.user, "get").and.returnValue($q.when({
                data: {}
            }));
            spyOn(ModalService, "confirm");

            createController();
            
            var user = {role: "co-owner", user: {email: "email"}};

            ctrl.model.removeAccess(user);
            $rootScope.$apply();
            
            expect(ModalService.confirm).toHaveBeenCalled();
        });
    });
    
    describe("onRemoveAccess", function() {
        it("calls api.binder.remove_acl", function() {
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: {}
            }));
            spyOn(api.user, "get").and.returnValue($q.when({
                data: {}
            }));
            spyOn(api.binder, "remove_acl").and.returnValue($q.when({
                data: {}
            }));

            createController();

            ctrl.model.onRemoveAccess({user: {id: 1}});
            $rootScope.$apply();
            
            expect(api.binder.remove_acl).toHaveBeenCalled();
        });
    });
    
    describe("onRemoveAccessError", function() {
        it("calls $log and notify.error", function() {
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: {}
            }));
            spyOn(api.user, "get").and.returnValue($q.when({
                data: {}
            }));
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            
            var response = {
                data: "error"
            };

            ctrl.model.onRemoveAccessError(response);
            $rootScope.$apply();
            
            expect($log.error).toHaveBeenCalledWith("error");
            expect(notify.error).toHaveBeenCalledWith("error");
        });
    });
    
    describe("refreshAcl", function() {
        it("calls api.binder.get_acl", function() {
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: {}
            }));
            spyOn(api.user, "get").and.returnValue($q.when({
                data: {}
            }));
            spyOn(api.binder, "get_acl").and.returnValue($q.when({
                data: {}
            }));

            createController();

            ctrl.model.refreshAcl();
            $rootScope.$apply();
            
            expect(api.binder.get_acl).toHaveBeenCalled();
        });
    });
    
    describe("onRefreshAcl", function() {
        it("calls notify.success and sets access_list", function() {
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: {}
            }));
            spyOn(api.user, "get").and.returnValue($q.when({
                data: {}
            }));
            spyOn(notify, "success");

            createController();
            
            var response = {
                data: "data"
            };

            ctrl.model.onRefreshAcl(response);
            $rootScope.$apply();
            
            expect(notify.success).toHaveBeenCalled();
            expect(ctrl.model.access_list).toEqual("data");
        });
    });
    
    describe("onRefreshAclError", function() {
        it("calls $log.error", function() {
            spyOn(api.binder, "get").and.returnValue($q.when({
                data: {}
            }));
            spyOn(api.user, "get").and.returnValue($q.when({
                data: {}
            }));
            spyOn($log, "error");

            createController();
            
            var response = {
                data: "error"
            };

            ctrl.model.onRefreshAclError(response);
            $rootScope.$apply();
            
            expect($log.error).toHaveBeenCalledWith("error");
        });
    });
});