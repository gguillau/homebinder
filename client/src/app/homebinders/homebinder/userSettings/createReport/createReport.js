angular
	.module("hb.userSettings.createReport", [])
	.directive("userCreateReport", function() {
		return {
			restrict: "E",
			templateUrl: "homebinders/homebinder/userSettings/createReport/createReport.tpl.html",
			controller: "UserCreateReportController as ctrl",
			bindToController: true,
			scope: {}
		};
	})
	.controller("UserCreateReportController", [
		"hb.api",
		"Context",
		"$state",
		"$stateParams",
		"hb.resources",
		"Notify",
		"$log",
		"Session",
		"constants",
		"UpgradeForm",
		UserCreateReportController
	]);

function UserCreateReportController(api, Context, $state, $stateParams, resources, notify, $log, Session, constants, UpgradeForm) {
	this.api = api;
	this.context = Context;
	this.$state = $state;
	this.$stateParams = $stateParams;
	this.resources = resources.createReport;
	this.notify = notify;
	this.$log = $log;
	this.Session = Session;
	this.currentUser = this.Session.getUser();
	this.constants = constants;
	this.UpgradeForm = UpgradeForm;

	var response = this.context.getBinder(this.$stateParams.binderId);
	this.getData(response);
	this.setReportInfo();
}

UserCreateReportController.prototype = {
	getData: function(response) {
		this.data = response.$$state.value;
		this.permissions = this.data.permissions;
	},
	
	setReportInfo: function() {
		this.permissions = this.data.permissions;
		this.reports = {
			visible: this.data.plan_id != "free",
			fullReportUrl: "/api/v1/reports?binder_id=" + this.data.id +
				"&api_key=" + this.constants.ApiKey +
				"&HB-UserToken=" + this.Session.getJwt(),
			sellerReport: {
				exists: angular.isObject(this.data.seller_report),
				canCreate: this.permissions.can_create_seller_report && !angular.isObject(this.data.seller_report),
				canEdit: this.permissions.can_edit_seller_report && angular.isObject(this.data.seller_report),
				canView: angular.isObject(this.data.seller_report),
				reportUrl: this.data.seller_report ? "/SellerReport/" + this.data.seller_report.code : undefined,
				editUrl: this.data.seller_report ? "/SellerReport/" + this.data.seller_report.code + "/edit" : undefined
			}
		};
	},

	createInventoryReport: function() {
		if (this.data.subscription.plan == "free") {
			this.UpgradeForm.show({
				upgradeText: "Please upgrade to run the Inventory Report and export to PDF."
			});
		}
		else {
			this.api.binder.inventoryReport(this.data.id).then(
				angular.bind(this, this.onCreateInventoryReportSuccess),
				angular.bind(this, this.onCreateReportError)
			);
		}
	},

	createCapitalReport: function() {
		if (this.data.subscription.plan == "free") {
			this.UpgradeForm.show({
				upgradeText: "Please upgrade to generate and download a Capital Expense Report from your receipts."
			});
		}
		else {
			this.api.binder.capitalExpenseReport(this.data.id).then(
				angular.bind(this, this.onCreateInventoryReportSuccess),
				angular.bind(this, this.onCreateReportError)
			);
		}
	},

	createSellerReport: function() {
		this.api.sellerReport.create({
			"binder_id": this.data.id,
			"public": true
		}).then(
			angular.bind(this, this.onCreateReportSuccess),
			angular.bind(this, this.onCreateReportError)
		);
	},

	onCreateInventoryReportSuccess: function(response) {
		this.notify.info("Please check your email");
	},

	onCreateReportSuccess: function(response) {
		this.data.seller_report = response.data;
		this.data.can_create_seller_report = false;
		this.data.can_edit_seller_report = true;
		this.setReportInfo();
		this.$state.go("seller_report_edit", {
			code: response.data.code
		});
	},

	onCreateReportError: function(response) {
		this.$log.error(response);
		this.notify.error(response.data);
	}
};