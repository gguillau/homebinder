describe("UserCreateReportController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $state,
        $stateParams,
        constants,
        notify,
        $log,
        api,
        Session,
        context,
        response,
        UpgradeForm;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $state = _$injector_.get("$state");
        notify = _$injector_.get("Notify");
        UpgradeForm = _$injector_.get("UpgradeForm");

        constants = {
            ApiKey: "faksd029i92"
        };

        $log = {
            error: function(msg) {}
        };

        Session = {
            currentBinder: function() {},
            getUser: function() {},
            getJwt: function() {}
        };
        
        $stateParams = {
            binderId: 1
        };
        
        response = {
            $$state: {
                value: {
                    id: 1,
                    permissions: {}
                }
            }
        };

        context = {
            getBinder: function(id) {
                return response;
            }
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("UserCreateReportController", {
            "hb.api": api,
            "Context": context,
            "$state": $state,
            "$stateParams": $stateParams,
            "Notify": notify,
            "$log": $log,
            "Session": Session,
            "constants": constants,
            "UpgradeForm": UpgradeForm
        });
        $scope.ctrl = ctrl;

        spyOn(api.user, "get").and.returnValue($q.when({
            data: { id: 1, role: "agent" }
        }));
    }

    describe('ctrl.createInventoryReport', function() {
        it('should call api binder inventoryReport', function() {
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(binder));
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "homeowner" });
            spyOn(api.binder, "inventoryReport").and.returnValue($q.when({ data: {} }));
            createController();
            binder.subscription = { plan: "" };
            ctrl.data = binder;
            ctrl.currentUser = { id: 1, role: "homeowner" };
            ctrl.createInventoryReport();

            expect(api.binder.inventoryReport).toHaveBeenCalled();
        });
    });

    describe('ctrl.onCreateInventoryReportSuccess', function() {
        it('should calls notify info', function() {
            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(binder));
            spyOn(notify, "info");
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "agent" });

            createController();
            ctrl.onCreateInventoryReportSuccess();

            expect(notify.info).toHaveBeenCalled();

        });
    });

    describe('ctrl.createSellerReport', function() {

        it('should successfully create a seller report', function() {

            var report = {
                appliances: [],
                binder: {},
                code: "randomcode",
                contractors: [],
                documents: [],
                finishes: [],
                id: 258,
                images: [],
                maintenance_items: [],
                paints: [],
                projects: [],
                public: true
            };

            spyOn(api.sellerReport, "create").and.returnValue($q.when({
                data: report
            }));
            spyOn($state, "go");
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "agent" });

            createController();
            $rootScope.$apply();
            ctrl.createSellerReport();
            $rootScope.$apply();

            expect(api.sellerReport.create).toHaveBeenCalledWith({
                "binder_id": 1,
                "public": true
            });
            expect(ctrl.data.seller_report).toEqual(report);
            expect(ctrl.data.can_create_seller_report).toBe(false);
            expect(ctrl.data.can_edit_seller_report).toBe(true);
            expect($state.go).toHaveBeenCalledWith("seller_report_edit", {
                code: "randomcode"
            });
        });

        it('should attempt to create a seller report and return an error', function() {

            var binder = {
                appliances: null,
                created_at: "2015-09-06T04:29:36Z",
                id: 1,
                name: "This is a fake home",
                primary: null,
                details: "this is a home",
                last_recall: null,
                partner: null,
                vendor: null,
                recalls: 0,
                documents: [{
                    file_size: 506986,
                    id: 950,
                    location: "https://homebinder.com",
                    name: "document_name.pdf",
                    seller_report_item: null,
                    tags: []
                }],
                maintenance_items: null,
                binder_contractors: null,
                property: null,
                subscription: null,
                seller_report: null,
                transfer: {
                    binder_id: 1359,
                    created_at: "2015-07-05T13:55:10Z",
                    id: 498,
                    status: "hold",
                    transfer_to: "faketransferemail@gmail.com",
                    transfer_type: "ownership",
                    updated_at: "2015-07-05T18:23:37Z",
                    user_id: 278
                },
                permissions: {
                    can_create: false,
                    can_create_seller_report: false,
                    can_destroy: false,
                    can_edit_seller_report: false,
                    can_read: false,
                    can_share: false,
                    can_subscribe: false,
                    can_transfer: false,
                    can_view_master_report: false,
                    can_write: false
                }
            };

            spyOn(Session, "currentBinder").and.returnValue($q.when(binder));
            spyOn(api.sellerReport, "create").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(Session, "getUser").and.returnValue({ id: 1, role: "agent" });
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            $rootScope.$apply();
            ctrl.createSellerReport();
            $rootScope.$apply();

            expect(api.sellerReport.create).toHaveBeenCalledWith({
                "binder_id": 1,
                "public": true
            });
            expect(notify.error).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();

        });

    });
});