angular
    .module("hb.homebinders.userSettings", [
        "ui.router",
        "ui.bootstrap",
        "hb.api",
        "hb.components",
        "hb.userSettings.createReport",
        "hb.userSettings.permissions",
        "hb.userSettings.shareBinder",
        "hb.userSettings.subscriptions",
        "hb.userSettings.transferBinder",
        "hb.userSettings.userProfile"
    ])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('binder.userSettings', {
                url: "^/binders/:binderId/settings",
                templateUrl: "homebinders/homebinder/userSettings/userSettings.tpl.html",
                controller: "UserSettingsController as ctrl",
                access: {
                    requiresLogin: true
                }
            });
    }])
    .controller("UserSettingsController", [
        "Session",
        "hb.resources",
        "$stateParams",
        "Context",
        UserSettingsController
    ]);

function UserSettingsController(Session, resources, $stateParams, Context) {
    this.Session = Session;
    this.resources = resources.userSettings;
    this.currentUser = this.Session.getUser();
    this.$stateParams = $stateParams;
    this.context = Context;

    var response = this.context.getBinder(this.$stateParams.binderId);
    this.getPermissions(response);
    this.setNavigationLinks();
}

UserSettingsController.prototype = {
    // Get permissions to determine what links can appear in settings
    getPermissions: function(response) {
        var binder = response.$$state.value;
        this.permissions = binder.permissions;
    },

    // Set the navigation links
    setNavigationLinks: function() {
        this.navigation_links = [];
        
        // Homeowner user settings
        if (this.currentUser.role === "homeowner") {
            this.navigation_links.push({
                value: "account",
                name: this.resources.editUser,
                active: false
            });
            
            this.navigation_links.push({
                value: "password",
                name: this.resources.password,
                active: false
            });
            
            this.navigation_links.push({
                value: "email",
                name: this.resources.email,
                active: false
            });
            
            this.navigation_links.push({
                value: "language",
                name: this.resources.language,
                active: false
            });
            
            this.navigation_links.push({
                value: "subscriptions",
                name: this.resources.subscriptions,
                active: false
            });
        }

        this.navigation_links.push({
            value: "create_report",
            name: this.resources.createReport,
            active: false
        });

        if (this.permissions.can_share) {
            this.navigation_links.push({
                value: "share_binder",
                name: this.resources.shareBinder,
                active: false
            });
        }

        if (this.permissions.can_transfer) {
            this.navigation_links.push({
                value: "transfer_binder",
                name: this.resources.transferBinder,
                active: false
            });
        }
        
        // Set the first link current and active
        this.currentLink = this.navigation_links[0];
        this.currentLink.active = true;
    },

    // On link changed
    changeLink: function(link) {
        this.currentLink.active = false;
        link.active = true;
        this.currentLink = link;
    }
};