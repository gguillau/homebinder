describe("UserSubscriptionsController", function() {
    
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $stateParams,
        api,
        $log,
        modals,
        $state,
        notify,
        loading;

    
    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        $stateParams = {};
        
        loading = {
            show: function(msg){},
            close: function(){}
        };
        
        api = {
            subscription: {
                allForUser: function() {},
                save: function(string, data) {}
            }
        };
        
        notify = {
            error: function(msg) {},
            success: function(msg) {}
        };
        
        $log = {
            error: function(msg){}
        };
        
        modals = {
            confirm: function(confirmOptions) {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("UserSubscriptionsController",
            {
                "hb.api": api,
                "Notify": notify,
                "$log": $log,
                "ModalService": modals,
                "$stateParams": $stateParams,
                "$state": $state,
                "Loading": loading
            });
            
        $scope.ctrl = ctrl;

    }
    
    describe('ctrl.refresh', function(){

        it('should retrieve the subscription with success', function(){
            
            var subscription = {
                binder: null,
                card_type: null,
                discount: null,
                end_date: null,
                id: 1359,
                last4: null,
                payment_status: null,
                plan: "free",
                subtotal: null
            };
            
            var list = [
                subscription
            ];

            spyOn(api.subscription, "allForUser").and.returnValue($q.when({
                data: list
            })); 
            spyOn(loading, "show");
            spyOn(loading, "close");
            
            createController();
            $rootScope.$apply();
            
            expect(ctrl.list).toEqual(jasmine.objectContaining(list));
            expect(api.subscription.allForUser).toHaveBeenCalled();
            expect(loading.show).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();
        });
        
        it('should retrieve the subscription and return error', function(){
            
            spyOn(api.subscription, "allForUser").and.returnValue($q.reject({
                data: "error"
            })); 
            
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "show");
            spyOn(loading, "close");
            
            createController();
            $rootScope.$apply();

            expect(notify.error).toHaveBeenCalledWith("error");
            expect($log.error).toHaveBeenCalledWith({data: "error"});
            expect(loading.show).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();

        });

    });
    
    describe('ctrl.cancel', function(){

        it('should call the modal confirm function when cancel is called', function(){
            
            var subscription = {
                binder: null,
                card_type: null,
                discount: null,
                end_date: null,
                id: 1359,
                last4: null,
                payment_status: null,
                plan: "free",
                subtotal: null
            };
            
            var list = [
                subscription
            ];

            spyOn(api.subscription, "allForUser").and.returnValue($q.when({
                data: list
            })); 
            
            spyOn(modals, "confirm");
            
            createController();
            $rootScope.$apply();
            ctrl.cancel(ctrl.list[0]);
            $rootScope.$apply();
            expect(modals.confirm).toHaveBeenCalled();


        });

    });

    describe('ctrl.confirmCancel', function(){

        it('should save subscription with success', function(){
            
            var subscription = {
                binder: null,
                card_type: null,
                discount: null,
                end_date: null,
                id: 1359,
                last4: null,
                payment_status: null,
                plan: "free",
                subtotal: null
            };
            
            var list = [
                subscription
            ];

            spyOn(api.subscription, "allForUser").and.returnValue($q.when({
                data: list
            })); 
            
            spyOn(api.subscription, "save").and.returnValue($q.when({
                data: "success"
            })); 
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn(notify, "success");
            
            createController();
            spyOn(ctrl, "refresh");
            $rootScope.$apply();
            ctrl.confirmCancel(ctrl.list[0]);
            $rootScope.$apply();
            
            expect(api.subscription.save).toHaveBeenCalledWith("cancel", {id: 1359});
            expect(notify.success).toHaveBeenCalled();
            expect(ctrl.refresh).toHaveBeenCalled();
            expect(loading.show).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();


        });
        
        it('should attempt to save subscription and return an error', function(){
            
            var subscription = {
                binder: null,
                card_type: null,
                discount: null,
                end_date: null,
                id: 1359,
                last4: null,
                payment_status: null,
                plan: "free",
                subtotal: null
            };
            
            var list = [
                subscription
            ];

            spyOn(api.subscription, "allForUser").and.returnValue($q.when({
                data: list
            })); 
            
            spyOn(api.subscription, "save").and.returnValue($q.reject({
                data: "error"
            })); 
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn($log, "error");
            spyOn(notify, "error");
            
            createController();
            $rootScope.$apply();
            ctrl.confirmCancel(ctrl.list[0]);
            $rootScope.$apply();
            
            expect(notify.error).toHaveBeenCalledWith("error");
            expect($log.error).toHaveBeenCalledWith({data: "error"});
            expect(loading.show).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();

        });

    });

});