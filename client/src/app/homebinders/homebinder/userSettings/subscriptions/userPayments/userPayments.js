(function() {
	"use strict";

	angular.module("hb.userSettings.subscriptions.userPayments", [])
		.factory("UserPayments", [
			"$stateParams",
			"hb.api",
			"Session",
			"Notify",
			"$log",
			"Loading",
			"$state",
			"hb.resources",
			"Context",
			function UserPayments($stateParams, api, Session, notify, $log, loading, $state, resources, context) {
				var Model = function() {
					this.$stateParams = $stateParams;
					this.api = api;
					this.Session = Session;
					this.notify = notify;
					this.$log = $log;
					this.loading = loading;
					this.$state = $state;
					this.subscriptionId = this.$stateParams.subscriptionId;
					// Flags to show hide parts of the page
					this.showPlans = false;
					this.showCouponForm = false;
					this.showCCForm = false;
					this.showTotals = false;
					// Storage for the coupon form
					this.coupon = {};
					// Storgae for the CC form
					this.cc = {};
					// Storage for the totals
					this.totals = {};
					// The subscription we're working with
					this.subscription = {};
					// Storage for the status of the page
					this.status = null;
					// Options for the Month/Year on the CC form
					this.options = {};
					this.options.months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
					this.options.years = [];
					// Current year
					var currentYear = new Date().getFullYear();
					for (var k = 0; k < 10; k++) {
						this.options.years.push(currentYear + k);
					}
					this.resources = resources.subscriptionPayment;
					this.creditCardConfig = {
						onReceiveToken: angular.bind(this, this.submitCCForm),
						onError: angular.bind(this, this.onError)
					};
					this.context = context;
					this.getPlan();
					this.getSubscription();
				};

				angular.extend(Model.prototype, {

					getPlan: function() {
						this.api.plan.get("standard").then(
							angular.bind(this, this.getPlanSuccess),
							angular.bind(this, this.getPlanError));
					},

					getPlanSuccess: function(response) {
						var result = response.data;
						this.totals.subtotal = result.amount;
						this.totals.discount = 0;
						this.totals.total = result.amount;
					},

					getPlanError: function(response) {
						this.$log.error(response);
						this.notify.error(response.data);
					},

					getSubscription: function() {
						this.loading.show(this.resources.loading);
						this.api.subscription.get(this.subscriptionId).then(
							angular.bind(this, this.subscriptionSuccess),
							angular.bind(this, this.subscriptionError));
					},

					subscriptionSuccess: function(response) {
						this.subscription = response.data;
						this.isUpgrade = this.subscription.plan == "free";
						this.showPlans = this.isUpgrade ? true : false;
						this.showCouponForm = this.isUpgrade ? true : false;
						this.showCCForm = this.isUpgrade ? false : true;
						this.showTotals = this.isUpgrade ? true : false;
						// Store the totals for dislay
						this.totals.subtotal = this.subscription.subtotal;
						this.totals.discount = this.subscription.discount;
						this.totals.total = this.totals.subtotal;
						// Format the totals
						if (!this.isUpgrade) {
							this.updateTotals();
						}
						this.loading.close();
					},

					subscriptionError: function(response) {
						this.error = response;
						this.$log.error(response);
						this.notify.error(response.data);
						this.loading.close();
					},

					updateTotals: function() {
						this.totals.subtotal = (this.totals.subtotal / 100).toFixed(2);
						this.totals.discount = (this.totals.discount / 100).toFixed(2);
						this.totals.total = (this.totals.total / 100).toFixed(2);
					},

					updateSubscription: function() {
						this.loading.show(this.resources.updating);
						var action = this.subscription.plan == "free" ? "upgrade" : "update";
						this.api.subscription.save(action, this.subscription, this.cc, this.coupon.value).then(
							angular.bind(this, this.updateSubscriptionSuccess),
							angular.bind(this, this.updateSubscriptionError));
					},

					updateSubscriptionSuccess: function(response) {
						this.status = this.isUpgrade ?
							this.resources.upgraded :
							this.resources.updated;
						this.context.getBinder().then(angular.bind(this, function(binder) {
							if (response.data.binder.id == binder.id) {
								binder.subscription = response.data;
								this.context.setBinder(binder);
							}
						}));
						this.loading.close();
						this.$state.go("binder.userSettings");
					},

					updateSubscriptionError: function(response) {
						this.$log.error(response);
						this.notify.error(response.data);
						this.loading.close();
					},

					submitCouponForm: function() {
						this.value = this.coupon.value;
						// Get the coupon
						this.api.coupon.get(this.value).then(
							angular.bind(this, this.getCouponSuccess),
							angular.bind(this, this.getCouponError));
					},

					getCouponSuccess: function(response) {
						var result = response.data;

						// Notify the user their coupon worked, depending on its type
						if (result.duration == "forever") {
							this.notify.success("Valid coupon!");
						}
						else {
							this.notify.success("Valid coupon! Please enter your credit card credentials and click upgrade to continue!");
						}

						// Update the totals
						// If the amount off is null it is percent off.
						if (result.amount_off == null) {
							this.totals.discount = this.totals.subtotal * (100 / 100);
						}
						else {
							this.totals.discount = result.amount_off;
						}
						this.totals.total = this.totals.subtotal - this.totals.discount;

						// If the total is a forever upgrade
						if (this.totals.total <= 0 && result.duration == "forever") {
							this.updateSubscription();
						}
					},

					getCouponError: function(response) {
						this.$log.error(response);
						this.notify.error(response.data);
					},

					tokenize: function() {
						this.loading.show(this.resources.updating);
						this.creditCardConfig.tokenize();
					},

					submitCCForm: function(token) {
						this.cc = token;
						// Update the subscription
						this.updateSubscription();
					},

					onError: function(error) {
						this.notify.info(error);
						this.loading.close();
					},
					
					back: function() {
						$state.go("binder.userSettings");
					}
				});

				return Model;
			}
		]);

})();