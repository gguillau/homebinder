describe("UserPayments", function() {
    var TestClass,
        UserPayments,
        $rootScope,
        $q;
        
    // load the homebinder module
    beforeEach(module("homebinder"));
    
    // get the base class under test
    beforeEach(inject(function($injector) {
        UserPayments = $injector.get("UserPayments");
        $rootScope = $injector.get("$rootScope");
        $q = $injector.get("$q");
    }));
    
    // define test class
    function createTestClass() {
        TestClass = function() {
            UserPayments.call(this);
            this.subscriptionId = 1359;
        };
        
        TestClass.prototype = Object.create(UserPayments.prototype);
        
        return new TestClass();
    }

    describe('getSubscription', function() {

        it('should retrieve subscription with success', inject(function() {
            var model = createTestClass();

            var subscription = {
                card_type: null,
                discount: 5,
                end_date: null,
                id: 1359,
                last4: null,
                payment_status: null,
                plan: "free",
                subtotal: 100
            };

            spyOn(model.api.plan, "get").and.returnValue($q.when({
                data: {
                    amount: 1000
                }
            }));
            spyOn(model.api.subscription, "get").and.returnValue($q.when({
                data: subscription
            }));
            spyOn(model.loading, "show");
            spyOn(model.loading, "close");

            $rootScope.$apply();
            model.getSubscription();
            $rootScope.$apply();

            expect(model.api.subscription.get).toHaveBeenCalledWith(1359);
            expect(model.subscription).toEqual(jasmine.objectContaining(subscription));
            expect(model.loading.show).toHaveBeenCalled();
            expect(model.totals.subtotal).toBe(100);
            expect(model.totals.discount).toBe(5);
            expect(model.totals.total).toBe(100);
            expect(model.loading.close).toHaveBeenCalled();

        }));

        it('should retrieve subscription with success and call updateTotals', inject(function() {
            var model = createTestClass();

            var subscription = {
                card_type: null,
                discount: 5,
                end_date: null,
                id: 1359,
                last4: null,
                payment_status: null,
                plan: "renter",
                subtotal: 100
            };

            spyOn(model.api.subscription, "get").and.returnValue($q.when({
                data: subscription
            }));
            spyOn(model.loading, "show");
            spyOn(model.loading, "close");

            spyOn(model, "updateTotals").and.callThrough();
            
            model.getSubscription();
            $rootScope.$apply();

            expect(model.api.subscription.get).toHaveBeenCalledWith(1359);
            expect(model.subscription).toEqual(jasmine.objectContaining(subscription));
            expect(model.loading.show).toHaveBeenCalled();
            expect(model.totals.subtotal).toBe("1.00");
            expect(model.totals.discount).toBe("0.05");
            expect(model.totals.total).toBe("1.00");
            expect(model.loading.close).toHaveBeenCalled();
            expect(model.updateTotals).toHaveBeenCalled();

        }));

        it('should call subscription get function and return error', inject(function() {
            var model = createTestClass();

            spyOn(model.api.subscription, "get").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(model.loading, "show");
            spyOn(model.loading, "close");
            spyOn(model.$log, "error");
            spyOn(model.notify, "error");

            model.getSubscription();
            $rootScope.$apply();

            expect(model.api.subscription.get).toHaveBeenCalledWith(1359);
            expect(model.notify.error).toHaveBeenCalledWith("error");
            expect(model.$log.error).toHaveBeenCalledWith({ data: "error" });
            expect(model.loading.close).toHaveBeenCalled();
            expect(model.loading.show).toHaveBeenCalled();

        }));

    });

    describe('getPlanError', function() {
        it('calls notify error', inject(function() {
            var model = createTestClass();

            spyOn(model.$log, "error");
            spyOn(model.notify, "error");

            model.getPlanError({ data: "error" });

            expect(model.$log.error).toHaveBeenCalledWith({ data: "error" });
            expect(model.notify.error).toHaveBeenCalledWith("error");
        }));
    });

    describe('updateTotals', function() {

        it('should format subscription totals', inject(function() {

            var model = createTestClass();
            
            model.totals.subtotal = 12000;
            model.totals.discount = 500;
            model.totals.total = 12000;

            model.updateTotals();
            $rootScope.$apply();

            expect(model.totals.subtotal).toBe("120.00");
            expect(model.totals.discount).toBe("5.00");
            expect(model.totals.total).toBe("120.00");

        }));

    });

    describe('updateSubscription', function() {

        it('should upgrade subscription with success on free plan', inject(function() {

            var model = createTestClass();

            var subscription = {
                binder: { id: 1, name: "test" },
                card_type: null,
                discount: 5,
                end_date: null,
                id: 1359,
                last4: null,
                payment_status: null,
                plan: "free",
                subtotal: 100
            };

            var cc = {
                number: "5555-5555-5555-5555",
                cvc: 304,
                exp_month: 02,
                exp_year: 2018
            };

            spyOn(model.api.subscription, "save").and.returnValue($q.when({
                data: subscription
            }));
            spyOn(model.loading, "show");
            spyOn(model.loading, "close");

            model.subscription = subscription;
            model.cc = cc;
            model.coupon.value = "1000";
            model.updateSubscription();
            $rootScope.$apply();

            expect(model.api.subscription.save).toHaveBeenCalledWith("upgrade", subscription, cc, "1000");
            expect(model.loading.show).toHaveBeenCalled();
            expect(model.loading.close).toHaveBeenCalled();

        }));

        it('should update subscription with success on renter plan', inject(function() {

            var model = createTestClass();

            var subscription = {
                binder: { id: 1, name: "test" },
                card_type: null,
                discount: 5,
                end_date: null,
                id: 1359,
                last4: null,
                payment_status: null,
                plan: "renter",
                subtotal: 100
            };

            var cc = {
                number: "5555-5555-5555-5555",
                cvc: 304,
                exp_month: 02,
                exp_year: 2018
            };

            spyOn(model.api.subscription, "save").and.returnValue($q.when({
                data: subscription
            }));
            spyOn(model.loading, "show");
            spyOn(model.loading, "close");

            model.subscription = subscription;
            model.cc = cc;
            model.coupon.value = "1000";
            model.updateSubscription();
            $rootScope.$apply();

            expect(model.api.subscription.save).toHaveBeenCalledWith("update", subscription, cc, "1000");
            expect(model.loading.show).toHaveBeenCalled();
            expect(model.loading.close).toHaveBeenCalled();

        }));
    });

    describe('submitCouponForm', function() {

        it('should update subscription with success', inject(function() {

            var model = createTestClass();

            var subscription = {
                binder: { id: 1, name: "test" },
                card_type: null,
                discount: 5,
                end_date: null,
                id: 1359,
                last4: null,
                payment_status: null,
                plan: "free",
                subtotal: 100
            };

            var cc = {
                number: "5555-5555-5555-5555",
                cvc: 304,
                exp_month: 02,
                exp_year: 2018
            };

            spyOn(model.api.subscription, "get").and.returnValue($q.when({
                data: subscription
            }));
            spyOn(model.api.plan, "get").and.returnValue($q.when({
                data: {
                    amount: 1000
                }
            }));
            spyOn(model.api.coupon, "get").and.returnValue($q.when({
                data: {
                    amount_off: 1000,
                    percent_off: 5,
                    duration: "forever"
                }
            }));
            spyOn(model.api.subscription, "save").and.returnValue($q.when({
                data: subscription
            }));
            
            spyOn(model.notify, "success");
            spyOn(model, "updateSubscription");
        
            model.getPlan();
			model.getSubscription();
            $rootScope.$apply();
            model.subscription = subscription;
            model.cc = cc;
            model.coupon.value = "1000";
            model.submitCouponForm();
            $rootScope.$apply();

            expect(model.api.coupon.get).toHaveBeenCalledWith("1000");
            expect(model.notify.success).toHaveBeenCalledWith("Valid coupon!");
            expect(model.totals.subtotal).toBe(1000);
            expect(model.totals.discount).toBe(1000);
            expect(model.totals.total).toBe(0);
            expect(model.updateSubscription).toHaveBeenCalled();

        }));
        
        it('should not call updateSubscription() when coupon duration is "once"', inject(function() {

            var model = createTestClass();

            var subscription = {
                binder: { id: 1, name: "test" },
                card_type: null,
                discount: 5,
                end_date: null,
                id: 1359,
                last4: null,
                payment_status: null,
                plan: "free",
                subtotal: 100
            };

            var cc = {
                number: "5555-5555-5555-5555",
                cvc: 304,
                exp_month: 02,
                exp_year: 2018
            };

            spyOn(model.api.subscription, "get").and.returnValue($q.when({
                data: subscription
            }));
            spyOn(model.api.plan, "get").and.returnValue($q.when({
                data: {
                    amount: 1000
                }
            }));
            spyOn(model.api.coupon, "get").and.returnValue($q.when({
                data: {
                    amount_off: 1000,
                    percent_off: 5,
                    duration: "once"
                }
            }));
            spyOn(model.api.subscription, "save").and.returnValue($q.when({
                data: subscription
            }));
            
            spyOn(model.notify, "success");
            spyOn(model, "updateSubscription");
        
            model.getPlan();
			model.getSubscription();
            $rootScope.$apply();
            model.subscription = subscription;
            model.cc = cc;
            model.coupon.value = "1000";
            model.submitCouponForm();
            $rootScope.$apply();

            expect(model.notify.success).toHaveBeenCalledWith("Valid coupon! Please enter your credit card credentials and click upgrade to continue!");
            expect(model.updateSubscription).not.toHaveBeenCalled();

        }));
        
        it('should not call updateSubscription() when coupon duration is "repeating"', inject(function() {

            var model = createTestClass();

            var subscription = {
                binder: { id: 1, name: "test" },
                card_type: null,
                discount: 5,
                end_date: null,
                id: 1359,
                last4: null,
                payment_status: null,
                plan: "free",
                subtotal: 100
            };

            var cc = {
                number: "5555-5555-5555-5555",
                cvc: 304,
                exp_month: 02,
                exp_year: 2018
            };

            spyOn(model.api.subscription, "get").and.returnValue($q.when({
                data: subscription
            }));
            spyOn(model.api.plan, "get").and.returnValue($q.when({
                data: {
                    amount: 1000
                }
            }));
            spyOn(model.api.coupon, "get").and.returnValue($q.when({
                data: {
                    amount_off: 1000,
                    percent_off: 5,
                    duration: "repeating"
                }
            }));
            spyOn(model.api.subscription, "save").and.returnValue($q.when({
                data: subscription
            }));
            
            spyOn(model.notify, "success");
            spyOn(model, "updateSubscription");
        
            model.getPlan();
			model.getSubscription();
            $rootScope.$apply();
            model.subscription = subscription;
            model.cc = cc;
            model.coupon.value = "1000";
            model.submitCouponForm();
            $rootScope.$apply();

            expect(model.notify.success).toHaveBeenCalledWith("Valid coupon! Please enter your credit card credentials and click upgrade to continue!");
            expect(model.updateSubscription).not.toHaveBeenCalled();

        }));
        
        it('should not call updateSubscription() when amount_off is less than amount', inject(function() {

            var model = createTestClass();

            var subscription = {
                binder: { id: 1, name: "test" },
                card_type: null,
                discount: 5,
                end_date: null,
                id: 1359,
                last4: null,
                payment_status: null,
                plan: "free",
                subtotal: 100
            };

            var cc = {
                number: "5555-5555-5555-5555",
                cvc: 304,
                exp_month: 02,
                exp_year: 2018
            };

            spyOn(model.api.subscription, "get").and.returnValue($q.when({
                data: subscription
            }));
            spyOn(model.api.plan, "get").and.returnValue($q.when({
                data: {
                    amount: 1000
                }
            }));
            spyOn(model.api.coupon, "get").and.returnValue($q.when({
                data: {
                    amount_off: 999,
                    percent_off: 5,
                    duration: "repeating"
                }
            }));
            spyOn(model.api.subscription, "save").and.returnValue($q.when({
                data: subscription
            }));
            
            spyOn(model.notify, "success");
            spyOn(model, "updateSubscription");
        
            model.getPlan();
			model.getSubscription();
            $rootScope.$apply();
            model.subscription = subscription;
            model.cc = cc;
            model.coupon.value = "1000";
            model.submitCouponForm();
            $rootScope.$apply();

            expect(model.notify.success).toHaveBeenCalledWith("Valid coupon! Please enter your credit card credentials and click upgrade to continue!");
            expect(model.updateSubscription).not.toHaveBeenCalled();

        }));
        
        it('should update subscription with success when amount_off is null', inject(function() {

            var model = createTestClass();

            var subscription = {
                binder: { id: 1, name: "test" },
                card_type: null,
                discount: 5,
                end_date: null,
                id: 1359,
                last4: null,
                payment_status: null,
                plan: "free",
                subtotal: 100
            };

            var cc = {
                number: "5555-5555-5555-5555",
                cvc: 304,
                exp_month: 02,
                exp_year: 2018
            };

            spyOn(model.api.subscription, "get").and.returnValue($q.when({
                data: subscription
            }));
            spyOn(model.api.plan, "get").and.returnValue($q.when({
                data: {
                    amount: 1000
                }
            }));
            spyOn(model.api.coupon, "get").and.returnValue($q.when({
                data: {
                    amount_off: null,
                    percent_off: 5,
                    duration: "forever"
                }
            }));
            spyOn(model.api.subscription, "save").and.returnValue($q.when({
                data: subscription
            }));
            spyOn(model.notify, "success");
            spyOn(model, "updateSubscription");
            
            model.getPlan();
			model.getSubscription();
            $rootScope.$apply();
            model.subscription = subscription;
            model.cc = cc;
            model.coupon.value = "1000";
            model.submitCouponForm();
            $rootScope.$apply();

            expect(model.api.plan.get).toHaveBeenCalledWith("standard");
            expect(model.api.coupon.get).toHaveBeenCalledWith("1000");
            expect(model.notify.success).toHaveBeenCalledWith("Valid coupon!");
            expect(model.totals.subtotal).toBe(1000);
            expect(model.totals.discount).toBe(1000);
            expect(model.totals.total).toBe(0);
            expect(model.updateSubscription).toHaveBeenCalled();

        }));

        it('should try to update subscription and return an error', inject(function() {

            var model = createTestClass();

            var subscription = {
                binder: { id: 1, name: "test" },
                card_type: null,
                discount: 5,
                end_date: null,
                id: 1359,
                last4: null,
                payment_status: null,
                plan: "free",
                subtotal: 100
            };

            spyOn(model.api.subscription, "get").and.returnValue($q.when({
                data: subscription
            }));
            spyOn(model.api.plan, "get").and.returnValue($q.when({
                data: {
                    amount: 1000
                }
            }));
            spyOn(model.api.coupon, "get").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(model.notify, "error");
            spyOn(model.$log, "error");

            model.getPlan();
			model.getSubscription();
            $rootScope.$apply();
            model.subscription = subscription;
            model.coupon.value = "1000";
            model.submitCouponForm();
            $rootScope.$apply();

            expect(model.api.plan.get).toHaveBeenCalledWith("standard");
            expect(model.api.coupon.get).toHaveBeenCalledWith("1000");
            expect(model.totals.subtotal).toBe(1000);
            expect(model.totals.discount).toBe(0);
            expect(model.totals.total).toBe(1000);
            expect(model.$log.error).toHaveBeenCalled();
            expect(model.notify.error).toHaveBeenCalled();

        }));

        it('should try to update subscription and return an error', inject(function() {

            var model = createTestClass();

            var subscription = {
                binder: { id: 1, name: "test" },
                card_type: null,
                discount: 5,
                end_date: null,
                id: 1359,
                last4: null,
                payment_status: null,
                plan: "free",
                subtotal: 100
            };

            var cc = {
                number: "5555-5555-5555-5555",
                cvc: 304,
                exp_month: 02,
                exp_year: 2018
            };

            spyOn(model.api.subscription, "get").and.returnValue($q.when({
                data: subscription
            }));
            spyOn(model.api.plan, "get").and.returnValue($q.when({
                data: {
                    amount: 1000
                }
            }));
            spyOn(model.api.coupon, "get").and.returnValue($q.when({
                data: {
                    amount_off: 1000,
                    percent_off: 5,
                    duration: "forever"
                }
            }));
            spyOn(model.api.subscription, "save").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(model.notify, "error");
            spyOn(model.$log, "error");
            spyOn(model, "updateSubscription");
            
            model.getPlan();
			model.getSubscription();
            $rootScope.$apply();
            model.subscription = subscription;
            model.cc = cc;
            model.coupon.value = "1000";
            model.submitCouponForm();
            $rootScope.$apply();

            expect(model.api.plan.get).toHaveBeenCalledWith("standard");
            expect(model.api.coupon.get).toHaveBeenCalledWith("1000");
            expect(model.totals.subtotal).toBe(1000);
            expect(model.totals.discount).toBe(1000);
            expect(model.totals.total).toBe(0);
            expect(model.updateSubscription).toHaveBeenCalled();
            expect(model.$log.error).toHaveBeenCalled();
            expect(model.notify.error).toHaveBeenCalled();

        }));
    });

    describe('submitCCForm', function() {

        it('should call setStatus and updateSubscription', inject(function() {

            var model = createTestClass();

            var subscription = {
                binder: { id: 1, name: "test" },
                card_type: null,
                discount: 5,
                end_date: null,
                id: 1359,
                last4: null,
                payment_status: null,
                plan: "free",
                subtotal: 100
            };

            spyOn(model.api.plan, "get").and.returnValue($q.when({
                data: {
                    amount: 1000
                }
            }));

            spyOn(model.api.subscription, "get").and.returnValue($q.when({
                data: subscription
            }));

            spyOn(model, "updateSubscription");
            
            model.submitCCForm();
            $rootScope.$apply();

            expect(model.updateSubscription).toHaveBeenCalled();

        }));

        it('should call setStatus and updateSubscription', inject(function() {

            var model = createTestClass();

            var subscription = {
                binder: { id: 1, name: "test" },
                card_type: null,
                discount: 5,
                end_date: null,
                id: 1359,
                last4: null,
                payment_status: null,
                plan: "renter",
                subtotal: 100
            };

            spyOn(model.api.plan, "get").and.returnValue($q.when({
                data: {
                    amount: 1000
                }
            }));

            spyOn(model.api.subscription, "get").and.returnValue($q.when({
                data: subscription
            }));

            spyOn(model, "updateSubscription");
            
            model.submitCCForm();
            $rootScope.$apply();

            expect(model.updateSubscription).toHaveBeenCalled();

        }));

    });

    describe('tokenize', function() {
        it('calls notify error', inject(function() {

            var model = createTestClass();

            var subscription = {
                card_type: null,
                discount: 5,
                end_date: null,
                id: 1359,
                last4: null,
                payment_status: null,
                plan: "renter",
                subtotal: 100
            };

            spyOn(model.api.plan, "get").and.returnValue($q.when({
                data: {
                    amount: 1000
                }
            }));

            spyOn(model.api.subscription, "get").and.returnValue($q.when({
                data: subscription
            }));
            spyOn(model.notify, "info");
            spyOn(model.loading, "show");
            spyOn(model.loading, "close");

            model.creditCardConfig = {
                tokenize: function() {}
            };
            spyOn(model.creditCardConfig, "tokenize");
            model.tokenize();
            $rootScope.$apply();

            expect(model.creditCardConfig.tokenize).toHaveBeenCalled();
        }));
    });

    describe('onError', function() {
        it('calls notify error', inject(function() {

            var model = createTestClass();

            spyOn(model.notify, "info");
            spyOn(model.loading, "close");

            model.onError("error");
            $rootScope.$apply();

            expect(model.notify.info).toHaveBeenCalledWith("error");
            expect(model.loading.close).toHaveBeenCalled();
        }));
    });

});