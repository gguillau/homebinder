angular
	.module("hb.userSettings.subscriptions", [
		"ui.router",
		"ui.bootstrap",
		"hb.api",
		"hb.components",
		"hb.userSettings.subscriptions.payment",
		"hb.userSettings.subscriptions.userPayments"
	])
	.directive("userEditSubscriptions", function() {
		return {
			restrict: "E",
			templateUrl: "homebinders/homebinder/userSettings/subscriptions/subscriptions.tpl.html",
			controller: "UserSubscriptionsController as ctrl",
			bindToController: true,
			scope: {}
		};
	})
	.controller("UserSubscriptionsController", [
		"ModalService",
		"hb.api",
		"$stateParams",
		"$state",
		"Notify",
		"$log",
		"Loading",
		"hb.resources",
		UserSubscriptionsController
	]);

function UserSubscriptionsController(modals, api, $stateParams, $state, notify, $log, loading, resources) {
	this.modals = modals;
	this.api = api;
	this.$state = $state;
	this.notify = notify;
	this.$log = $log;
	this.loading = loading;
	this.resources = resources.userSubscriptions;
	this.list = [];
	this.refresh();
}

UserSubscriptionsController.prototype = {

	refresh: function() {
		this.loading.show(this.resources.loading);
		this.api.subscription.allForUser().then(
			angular.bind(this, this.onRefreshSuccess),
			angular.bind(this, this.onRefreshError));
	},

	onRefreshSuccess: function(response) {
		this.list = response.data;
		this.loading.close();
	},

	onRefreshError: function(response) {
		this.$log.error(response);
		this.notify.error(response.data);
		this.loading.close();
	},

	cancel: function(subscription) {
		this.modals.confirm({
			message: this.resources.cancelMessage,
			glyphicon: "glyphicon glyphicon-remove",
			confirm: angular.bind(this, this.confirmCancel, subscription)
		});
	},

	confirmCancel: function(subscription) {
		this.loading.show(this.resources.updating);
		this.api.subscription.save("cancel", { id: subscription.id }).then(
			angular.bind(this, this.cancelSuccess),
			angular.bind(this, this.cancelError));
	},

	cancelSuccess: function(response) {
		this.notify.success(this.resources.updated);
		this.loading.close();
		this.refresh();
	},

	cancelError: function(response) {
		this.$log.error(response);
		this.notify.error(response.data);
		this.loading.close();
	}
};