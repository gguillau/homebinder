describe("UserPaymentController", function() {
    var ctrl,
        controller;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
    }));

    function createController() {
        ctrl = controller("UserPaymentController", {
        });
    }

    describe("init", function() {
        it("sets the settings", function() {
            createController();
            expect(ctrl.model.status).toBe(null);
        });
    });
});
