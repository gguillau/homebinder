(function() {
	"use strict";

	angular
		.module("hb.userSettings.subscriptions.payment", [
			"ui.router",
			"ui.bootstrap",
			"hb.api",
			"hb.components"
		])
		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("binder.userSettings.payment", {
					abstract: true,
					template: "<div ui-view></div>"
				})
				.state("binder.userSettingsUpgrade", {
					url: "^/settings/subscriptions/:subscriptionId/upgrade",
					templateUrl: "homebinders/homebinder/userSettings/subscriptions/payment/subscriptionPayment.tpl.html",
					controller: "UserPaymentController",
					controllerAs: "payment"
				})
				.state("binder.userSettingsUpdate", {
					url: "^/settings/subscriptions/:subscriptionId/update",
					templateUrl: "homebinders/homebinder/userSettings/subscriptions/payment/subscriptionPayment.tpl.html",
					controller: "UserPaymentController",
					controllerAs: "payment"
				});
		}])
		.controller("UserPaymentController", [
			"UserPayments",
			UserPaymentController
		]);

	function UserPaymentController(UserPayments) {
		var Model = function() {
			// Call parent
			UserPayments.call(this);
		};
	
		Model.prototype = Object.create(UserPayments.prototype);

		this.model = new Model();
	
	}
})();
