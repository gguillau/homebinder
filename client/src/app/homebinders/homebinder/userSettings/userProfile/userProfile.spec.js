describe("UserProfileController", function() {
    var UserProfileController,
        TestClass,
        $stateParams;

    // load the homebinder module
    beforeEach(module("homebinder"));
    
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
        
        // mock the notify service
        $provide.factory("Notify", function() {
            return {
                success: function() {},
                error: function() {}
            };
        });
        
        // mock the AfterLogin service
        $provide.factory("AfterLogin", function() {
            return {
                go: function() {}
            };
        });

        // mock the Session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin" };
                },
                getJwt: function() {},
                getLocale: function() {
                    return null;
                },
                setLocale: function() {}
            };
        });

    }));
    
    // get the base class under test
    beforeEach(inject(function($injector) {
        UserProfileController = $injector.get("UserProfileController");
        $stateParams = $injector.get("$stateParams");
        $stateParams.userId = 1;
    }));

    function createTestClass() {
        TestClass = function() {
            UserProfileController.call(this);
            this.refresh();
        };

        TestClass.prototype = Object.create(UserProfileController.prototype);

        return new TestClass();
    }

    describe("onSaveSuccess", function() {
        it("updates the item", inject(function($q, Loading, $rootScope, $state) {
            $stateParams.userId = undefined;
            var model = createTestClass();
            
            spyOn(model.notify, "success");
            spyOn(model.loading, "close");
            
            model.onSaveSuccess({ data: { id: 1, user_profile_attributes: { default_locale: "en-us" } } });

            expect(model.item).toEqual({ id: 1, user_profile_attributes: { default_locale: "en-us" } });
            expect(model.settings.readOnly).toEqual(true);
            expect(model.notify.success).toHaveBeenCalled();
            expect(model.loading.close).toHaveBeenCalled();
        }));
    });

    describe("showPasswordField", function() {
        it("returns false", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            var result = model.showPasswordField();

            expect(result).toBe(false);
        }));
    });

    describe("updateEmail", function() {
        it("does not call api.user.changeEmail", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();

            spyOn(model.loading, "show");
            spyOn(model.api.user, "changeEmail").and.returnValue($q.when({ data: {} }));
            
            model.emailForm = { $invalid: true };
            model.item = {};
            model.updateEmail();

            expect(model.emailForm.$submitted).toEqual(true);
            expect(model.loading.show).not.toHaveBeenCalled();
            expect(model.api.user.changeEmail).not.toHaveBeenCalled();
        }));

        it("calls api.user.changeEmail", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();

            spyOn(model.loading, "show");
            spyOn(model.api.user, "changeEmail").and.returnValue($q.when({ data: {} }));
            
            model.emailForm = { $invalid: false };
            model.item = {};
            model.updateEmail();

            expect(model.emailForm.$submitted).toEqual(true);
            expect(model.loading.show).toHaveBeenCalled();
            expect(model.api.user.changeEmail).toHaveBeenCalled();
        }));
    });

    describe("emailUpdateSuccess", function() {
        it("updates item email", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            
            spyOn(model.notify, "success");
            spyOn(model.loading, "close");
            
            model.item = {};
            model.emailForm = {
                $setPristine: function() {}
            };
            model.new_email = "new";
            model.confirm_password = "confirm";
            
            spyOn(model.emailForm, "$setPristine");
            
            model.emailUpdateSuccess({ data: { email: "updated@gmail.com" } });

            expect(model.item.email).toEqual("updated@gmail.com");
            expect(model.notify.success).toHaveBeenCalled();
            expect(model.new_email).toEqual(null);
            expect(model.confirm_password).toEqual(null);
            expect(model.emailForm.$setPristine).toHaveBeenCalled();
            expect(model.loading.close).toHaveBeenCalled();
        }));
    });

    describe("emailUpdateError", function() {
        it("calls notify error", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            
            spyOn(model.$log, "error").and.returnValue("log");
            spyOn(model.notify, "error");
            spyOn(model.loading, "close");
            
            model.emailUpdateError({ data: "error" });

            expect(model.$log.error).toHaveBeenCalledWith({ data: "error" });
            expect(model.notify.error).toHaveBeenCalledWith("error");
            expect(model.loading.close).toHaveBeenCalled();
        }));
    });

    describe("updatePassword", function() {
        it("does not call api.user.changePassword", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();

            spyOn(model.loading, "show");
            spyOn(model.api.user, "changePassword").and.returnValue($q.when({ data: {} }));
            
            model.passwordForm = { $invalid: true };
            model.item = {};
            model.updatePassword();

            expect(model.passwordForm.$submitted).toEqual(true);
            expect(model.loading.show).not.toHaveBeenCalled();
            expect(model.api.user.changePassword).not.toHaveBeenCalled();
        }));

        it("calls api.user.changePassword", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();

            spyOn(model.loading, "show");
            spyOn(model.api.user, "changePassword").and.returnValue($q.when({ data: {} }));
            
            model.passwordForm = { $invalid: false };
            model.item = {};
            model.updatePassword();

            expect(model.passwordForm.$submitted).toEqual(true);
            expect(model.loading.show).toHaveBeenCalled();
            expect(model.api.user.changePassword).toHaveBeenCalled();
        }));
    });

    describe("passwordUpdateSuccess", function() {
        it("sets current_password to null", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            
            spyOn(model.notify, "success");
            spyOn(model.loading, "close");
            
            model.passwordForm = {
                $setPristine: function() {}
            };
            model.current_password = "current";
            model.first_password = "first";
            model.second_password = "second";
            
            spyOn(model.passwordForm, "$setPristine");
            model.passwordUpdateSuccess({ data: { email: "updated@gmail.com" } });

            expect(model.notify.success).toHaveBeenCalled();
            expect(model.current_password).toBe(null);
            expect(model.first_password).toBe(null);
            expect(model.second_password).toBe(null);
            expect(model.passwordForm.$setPristine).toHaveBeenCalled();
            expect(model.loading.close).toHaveBeenCalled();
        }));
    });

    describe("passwordUpdateError", function() {
        it("calls notify error", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            
            spyOn(model.$log, "error").and.returnValue("log");
            spyOn(model.notify, "error");
            spyOn(model.loading, "close");
            
            model.passwordUpdateError({ data: "error" });

            expect(model.$log.error).toHaveBeenCalledWith({ data: "error" });
            expect(model.notify.error).toHaveBeenCalledWith("error");
            expect(model.loading.close).toHaveBeenCalled();
        }));
    });

    describe("UserSettingsProfileController", function() {
        var ctrl,
            controller;

        beforeEach(inject(function($controller, $injector) {
            controller = $controller;
        }));

        function createController() {
            ctrl = controller("UserSettingsProfileController", {
                "UserProfileController": UserProfileController
            });
        }

        describe("refresh", function() {
            it("sets the navigation_links", function() {
                createController();
                expect(ctrl.model.navigation_links.length).toEqual(1);
            });
        });
        
        
    });

});