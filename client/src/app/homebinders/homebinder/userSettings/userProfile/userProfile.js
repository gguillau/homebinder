angular
	.module("hb.userSettings.userProfile", [
		"ui.router",
		"ui.bootstrap",
		"hb.components"
	])
	.directive("userEditProfileBinder", function() {
		return {
			restrict: "E",
			templateUrl: "components/users/form/profile/profile.tpl.html",
			controller: "UserSettingsProfileController as ctrl",
			bindToController: true,
			scope: {}
		};
	})
	.directive("userEditEmailBinder", function() {
		return {
			restrict: "E",
			templateUrl: "components/users/edit/email/email.tpl.html",
			controller: "UserSettingsProfileController as ctrl",
			bindToController: true,
			scope: {}
		};
	})
	.directive("userEditPasswordBinder", function() {
		return {
			restrict: "E",
			templateUrl: "components/users/edit/password/password.tpl.html",
			controller: "UserSettingsProfileController as ctrl",
			bindToController: true,
			scope: {}
		};
	})
	.directive("userEditLocaleBinder", function() {
		return {
			restrict: "E",
			templateUrl: "components/users/edit/locale/locale.tpl.html",
			controller: "UserSettingsProfileController as ctrl",
			bindToController: true,
			scope: {}
		};
	})
	.factory("UserProfileController", [
		"hb.api",
		"Address",
		"Notify",
		"$log",
		"Session",
		"Loading",
		"hb.resources",
		"UsersEditController",
		"$stateParams",
		function(api, Address, notify, $log, Session, loading, resources, UsersEditController, $stateParams) {
			var Model = function() {
				// call the parent class
				UsersEditController.call(this);
				this.api = api;
				this.notify = notify;
				this.$log = $log;
				this.resources = angular.extend({}, this.resources, resources.userProfile);
				this.user = Session.getUser();
				if ($stateParams.userId) {
					this.itemId = $stateParams.userId;
				}
				else {
					this.itemId = this.user.id;
				}
			};

			Model.prototype = Object.create(UsersEditController.prototype);

			angular.extend(Model.prototype, {

				onSaveSuccess: function(response) {
					Session.setLocale(response.data.user_profile_attributes.default_locale, false);
					this.item = response.data;
					this.settings.readOnly = true;
					this.notify.success(this.resources.saveSuccess);
					this.loading.close();
				},

				showPasswordField: function() {
					return false;
				},

				updateEmail: function() {
					this.emailForm.$submitted = true;
					if (this.emailForm.$invalid) {
						return;
					}
					var user = {
						email: this.new_email
					};
					this.loading.show(this.resources.updating);
					this.api.user.changeEmail(this.item.id, user, this.item.email, this.confirm_password).then(
						angular.bind(this, this.emailUpdateSuccess),
						angular.bind(this, this.emailUpdateError));
				},

				emailUpdateSuccess: function(response) {
					this.item.email = response.data.email;
					this.notify.success(this.resources.updated);
					this.new_email = null;
					this.confirm_password = null;
					this.emailForm.$setPristine();
					this.loading.close();
				},

				emailUpdateError: function(response) {
					this.$log.error(response);
					this.notify.error(response.data);
					this.loading.close();
				},

				updatePassword: function() {
					this.passwordForm.$submitted = true;
					if (this.passwordForm.$invalid) {
						return;
					}
					var user = {
						password: this.first_password
					};
					this.loading.show(this.resources.updating);
					this.api.user.changePassword(this.item.id, user, this.current_password).then(
						angular.bind(this, this.passwordUpdateSuccess),
						angular.bind(this, this.passwordUpdateError));
				},

				passwordUpdateSuccess: function(response) {
					this.notify.success(this.resources.updated);
					this.current_password = null;
					this.first_password = null;
					this.second_password = null;
					this.passwordForm.$setPristine();
					this.loading.close();
				},

				passwordUpdateError: function(response) {
					this.$log.error(response);
					this.notify.error(response.data);
					this.loading.close();
				}
			});

			return Model;
		}

	])
	.controller("UserSettingsProfileController", [
		"UserProfileController",
		UserSettingsProfileController
	]);

function UserSettingsProfileController(UserProfileController) {
	var Model = function() {
		// call the parent class
		UserProfileController.call(this);
		this.refresh();
	};

	Model.prototype = Object.create(UserProfileController.prototype);

	angular.extend(Model.prototype);

	this.model = new Model();
}
