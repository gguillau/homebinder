describe("UserSettingsController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $state,
        $stateParams,
        Session,
        Context;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $stateParams = {
            binderId: 1
        };

        Session = {
            getUser: function() {
                return { role: "admin" };
            }
        };

        Context = {
            getBinder: function(id) {
                return { $$state: { value: { permissions: "permissions" } } };
            }
        };
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("UserSettingsController", {
            "$state": $state,
            "$stateParams": $stateParams,
            "Session": Session,
            "Context": Context
        });
        $scope.ctrl = ctrl;
    }

    describe('ctrl.init', function() {
        it('inits the controller', function() {
            createController();
            expect(ctrl.currentUser.role).toEqual("admin");
        });

        it('inits the controller and adds subscriptions', function() {
            spyOn(Session, "getUser").and.returnValue({ role: "homeowner" });
            createController();
            expect(ctrl.currentUser.role).toEqual("homeowner");
        });
    });

    describe('ctrl.getPermissions', function() {
        it('sets permissions', function() {
            createController();
            expect(ctrl.permissions).toEqual("permissions");
        });
    });

    describe('ctrl.setNavigationLinks', function() {
        it('sets navigation links when a user can share the binder', function() {
            createController();

            ctrl.permissions = {
                can_share: false,
                can_transfer: false
            };

            var navigation_links = [];
            navigation_links.push({
                value: "create_report",
                name: ctrl.resources.createReport,
                active: true
            });
            
            ctrl.setNavigationLinks();

            expect(ctrl.navigation_links).toEqual(navigation_links);
            expect(ctrl.navigation_links.length).toEqual(1);
            expect(ctrl.currentLink).toEqual(navigation_links[0]);
        });
        
        it('sets navigation links for a homeowner', function() {
            spyOn(Session, "getUser").and.returnValue({ role: "homeowner" });

            createController();

            ctrl.permissions = {
                can_share: false,
                can_transfer: false
            };

            var navigation_links = [];
            navigation_links.push({
                value: "account",
                name: ctrl.resources.editUser,
                active: true
            });
            navigation_links.push({
                value: "password",
                name: ctrl.resources.password,
                active: false
            });
            navigation_links.push({
                value: "email",
                name: ctrl.resources.email,
                active: false
            });
            navigation_links.push({
                value: "language",
                name: ctrl.resources.language,
                active: false
            });
            navigation_links.push({
                value: "subscriptions",
                name: ctrl.resources.subscriptions,
                active: false
            });
            navigation_links.push({
                value: "create_report",
                name: ctrl.resources.createReport,
                active: false
            });
            
            ctrl.setNavigationLinks();

            expect(ctrl.navigation_links).toEqual(navigation_links);
            expect(ctrl.navigation_links.length).toEqual(6);
            expect(ctrl.currentLink).toEqual(navigation_links[0]);
        });
        
        it('sets navigation links when a user can share the binder', function() {
            createController();

            ctrl.permissions = {
                can_share: true,
                can_transfer: false
            };

            var navigation_links = [];
            navigation_links.push({
                value: "create_report",
                name: ctrl.resources.createReport,
                active: true
            });
            navigation_links.push({
                value: "share_binder",
                name: ctrl.resources.shareBinder,
                active: false
            });
            
            ctrl.setNavigationLinks();

            expect(ctrl.navigation_links).toEqual(navigation_links);
            expect(ctrl.navigation_links.length).toEqual(2);
            expect(ctrl.currentLink).toEqual(navigation_links[0]);
        });
        
        it('sets navigation links when a user can transfer the binder', function() {
            createController();

            ctrl.permissions = {
                can_share: false,
                can_transfer: true
            };

            var navigation_links = [];
            navigation_links.push({
                value: "create_report",
                name: ctrl.resources.createReport,
                active: true
            });
            navigation_links.push({
                value: "transfer_binder",
                name: ctrl.resources.transferBinder,
                active: false
            });
            
            ctrl.setNavigationLinks();

            expect(ctrl.navigation_links).toEqual(navigation_links);
            expect(ctrl.navigation_links.length).toEqual(2);
            expect(ctrl.currentLink).toEqual(navigation_links[0]);
        });
        
        it('sets navigation links when a user can share and transfer the binder', function() {
            createController();

            ctrl.permissions = {
                can_share: true,
                can_transfer: true
            };

            var navigation_links = [];
            navigation_links.push({
                value: "create_report",
                name: ctrl.resources.createReport,
                active: true
            });
            navigation_links.push({
                value: "share_binder",
                name: ctrl.resources.shareBinder,
                active: false
            });
            navigation_links.push({
                value: "transfer_binder",
                name: ctrl.resources.transferBinder,
                active: false
            });
            
            ctrl.setNavigationLinks();

            expect(ctrl.navigation_links).toEqual(navigation_links);
            expect(ctrl.navigation_links.length).toEqual(3);
            expect(ctrl.currentLink).toEqual(navigation_links[0]);
        });
        
        it('sets navigation links when a homeowner can share and transfer the binder', function() {
            spyOn(Session, "getUser").and.returnValue({ role: "homeowner" });
            createController();

            ctrl.permissions = {
                can_share: true,
                can_transfer: true
            };

            var navigation_links = [];
            navigation_links.push({
                value: "account",
                name: ctrl.resources.editUser,
                active: true
            });
            navigation_links.push({
                value: "password",
                name: ctrl.resources.password,
                active: false
            });
            navigation_links.push({
                value: "email",
                name: ctrl.resources.email,
                active: false
            });
            navigation_links.push({
                value: "language",
                name: ctrl.resources.language,
                active: false
            });
            navigation_links.push({
                value: "subscriptions",
                name: ctrl.resources.subscriptions,
                active: false
            });
            navigation_links.push({
                value: "create_report",
                name: ctrl.resources.createReport,
                active: false
            });
            navigation_links.push({
                value: "share_binder",
                name: ctrl.resources.shareBinder,
                active: false
            });
            navigation_links.push({
                value: "transfer_binder",
                name: ctrl.resources.transferBinder,
                active: false
            });
            
            ctrl.setNavigationLinks();

            expect(ctrl.navigation_links).toEqual(navigation_links);
            expect(ctrl.navigation_links.length).toEqual(8);
            expect(ctrl.currentLink).toEqual(navigation_links[0]);
        });
    });
    
    describe('ctrl.changeLink', function() {
        it('sets changes current link', function() {
            createController();
            
            var link = {
                value: "value",
                name: "name",
                active: true
            };
            
            ctrl.changeLink(link);
            
            expect(ctrl.currentLink).toEqual(link);
        });
    });
});