angular
	.module("hb.userSettings.transferBinder", [])
	.directive("userTransferBinder", function() {
		return {
			restrict: "E",
			templateUrl: "homebinders/homebinder/userSettings/transferBinder/transferBinder.tpl.html",
			controller: "UserTransferBinderController",
			controllerAs: "ctrl",
			bindToController: true,
			scope: {}
		};
	})
	.controller("UserTransferBinderController", [
		"TransferController",
		"$state",
		"$stateParams",
		"Loading",
		"Notify",
		UserTransferBinderController
	]);

function UserTransferBinderController(TransferController, $state, $stateParams, loading, notify) {
	var Model = function() {
		// Call parent
		TransferController.call(this);
		this.$state = $state;
		this.$stateParams = $stateParams;
		this.loading = loading;
		this.notify = notify;
		this.binder = {
			id: $stateParams.binderId
		};
		this.init();
	};

	Model.prototype = Object.create(TransferController.prototype);

	angular.extend(Model.prototype, {
		transferCompleted: function(response) {
			this.saving = false;
			this.loading.close();
			this.notify.success(this.resources.success);
			this.onBinderTransferred();
		},
		
		onBinderTransferred: function() {
            var transferType = this.transferType;
            var user = this.currentUser;
            switch (user.role) {
                case "partner":
                case "inspector":
                case "broker":
                    this.$state.go("partner.binders", {
                        partnerId: user.partner_id
                    });
                    break;
                case "admin":
                    this.$state.go("admin.binders");
                    break;
                default:
                    if (transferType == "ownership") {
                        this.$state.go("binders");
                    }
                    else {
                        this.$state.go("binder.overview", {
                            binderId: this.data.id
                        });
                    }
                    break;
            }
        }
	});

	this.model = new Model();

}


