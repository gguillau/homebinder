describe("UserTransferBinderController", function() {
    var $rootScope,
        $controller,
        TransferController,
        $state,
        $stateParams,
        notify,
        loading,
        $scope,
        ctrl;

    // load the homebinder module
    beforeEach(module("homebinder"));
    
    beforeEach(inject(function(_$controller_, _$injector_, _$templateCache_, _$compile_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        TransferController = _$injector_.get("TransferController");

        $state = {
            go: function() {}
        };
        
        $stateParams = {
            binderId: 1
        };

        notify = {
            success: function() {}
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

    }));
    
    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("UserTransferBinderController", {
            "TransferController": TransferController,
            "$state": $state,
            "$stateParams": $stateParams,
            "Loading": loading,
            "Notify": notify
        });
        $scope.ctrl = ctrl;
    }

    describe("transferCompleted", function() {
        it("calls loading, notify, and onBinderTransferred()", function() {
            createController();
            
            spyOn(ctrl.model.loading, "close");
            spyOn(ctrl.model.notify, "success");
            spyOn(ctrl.model, "onBinderTransferred");
            
            ctrl.model.transferCompleted("response");
            $rootScope.$apply();

            expect(ctrl.model.saving).toEqual(false);
            expect(ctrl.model.loading.close).toHaveBeenCalled();
            expect(ctrl.model.notify.success).toHaveBeenCalled();
            expect(ctrl.model.onBinderTransferred).toHaveBeenCalled();
        });
    });
    
    describe("onBinderTransferred", function() {
        it("changes state for a partner", function() {
            createController();
            
            spyOn(ctrl.model.$state, "go");
            
            ctrl.model.currentUser = {
                role: "partner",
                partner_id: 1
            };
            ctrl.model.onBinderTransferred();
            $rootScope.$apply();

            expect(ctrl.model.$state.go).toHaveBeenCalledWith("partner.binders", {partnerId: 1});
        });
        
        it("changes state for an inspector", function() {
            createController();
            
            spyOn(ctrl.model.$state, "go");
            
            ctrl.model.currentUser = {
                role: "inspector",
                partner_id: 1
            };
            ctrl.model.onBinderTransferred();
            $rootScope.$apply();

            expect(ctrl.model.$state.go).toHaveBeenCalledWith("partner.binders", {partnerId: 1});
        });
        
        it("changes state for a broker", function() {
            createController();
            
            spyOn(ctrl.model.$state, "go");
            
            ctrl.model.currentUser = {
                role: "broker",
                partner_id: 1
            };
            ctrl.model.onBinderTransferred();
            $rootScope.$apply();

            expect(ctrl.model.$state.go).toHaveBeenCalledWith("partner.binders", {partnerId: 1});
        });
        
        it("changes state for an admin", function() {
            createController();
            
            spyOn(ctrl.model.$state, "go");
            
            ctrl.model.currentUser = {
                role: "admin"
            };
            ctrl.model.onBinderTransferred();
            $rootScope.$apply();

            expect(ctrl.model.$state.go).toHaveBeenCalledWith("admin.binders");
        });
        
        it("changes state for a homeowner when transferType is ownership", function() {
            createController();
            
            spyOn(ctrl.model.$state, "go");
            
            ctrl.model.currentUser = {
                role: "homeowner"
            };
            ctrl.model.transferType = "ownership";
            ctrl.model.onBinderTransferred();
            $rootScope.$apply();

            expect(ctrl.model.$state.go).toHaveBeenCalledWith("binders");
        });
        
        it("changes state for a homeowner when transferType is ownership", function() {
            createController();
            
            spyOn(ctrl.model.$state, "go");
            
            ctrl.model.currentUser = {
                role: "homeowner"
            };
            ctrl.model.transferType = "view";
            ctrl.model.data = {
                id: 1
            };
            ctrl.model.onBinderTransferred();
            $rootScope.$apply();

            expect(ctrl.model.$state.go).toHaveBeenCalledWith("binder.overview", {binderId: 1});
        });
    });

});