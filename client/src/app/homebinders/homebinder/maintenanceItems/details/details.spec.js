describe("MaintenanceItemDetailsController", function() {
    var $rootScope,
        $controller,
        $stateParams,
        resources,
        ctrl,
        BinderItemsIndexController,
        api,
        $q,
        BinderRecallModal,
        $log,
        context,
        notify;


    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q_.defer();
                    defer.resolve({ permissions: { can_read: true } });
                    return defer.promise;
                }
            };
        });
    }));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $stateParams = _$injector_.get("$stateParams");
        BinderItemsIndexController = _$injector_.get("BinderItemsIndexController");
        resources = _$injector_.get("hb.resources");
        $log = _$injector_.get("$log");
        context = _$injector_.get("Context");
        notify = _$injector_.get("Notify");
    }));

    function createController() {
        ctrl = $controller("MaintenanceItemDetailsController", {
            "BinderItemsIndexController": BinderItemsIndexController,
            "hb.resources": resources,
            "hb.api": api,
            "BinderRecallModal": BinderRecallModal,
            "$log": $log,
            "Context": context
        });
        $rootScope.$apply();
    }

    describe('#populate', function() {
        it("sets the item and interval to Once", function() {
            createController();
            var item = { id: 1, binder_id: 1, maintenance_cycle: "Once" };
            ctrl.model.item = item;
            ctrl.model.populate();

            expect(ctrl.model.item).toEqual(item);
            expect(ctrl.model.interval).toEqual("Once");
        });

        it("sets the item and interval to As Needed", function() {
            createController();
            var item = { id: 1, binder_id: 1, maintenance_cycle: "As Needed" };
            ctrl.model.item = item;
            ctrl.model.populate();

            expect(ctrl.model.item).toEqual(item);
            expect(ctrl.model.interval).toEqual("As Needed");
        });

        it("sets the item and frequency to empty string", function() {
            createController();
            var item = { id: 1, binder_id: 1, maintenance_cycle: "Annual", interval: 1 };
            ctrl.model.item = { id: 1, binder_id: 1, maintenance_cycle: "Annual", interval: 1 };
            ctrl.model.populate();

            expect(ctrl.model.item).toEqual(item);
            expect(ctrl.model.frequency).toEqual("");
        });

        it("sets the item and frequency to empty string", function() {
            createController();
            var item = { id: 1, binder_id: 1, maintenance_cycle: "Annual", interval: 2 };
            ctrl.model.item = item;
            ctrl.model.populate();

            expect(ctrl.model.item).toEqual(item);
            expect(ctrl.model.cycle).toEqual(ctrl.model.item.maintenance_cycle);
        });
    });

    describe('#loadHistory', function() {
        it("calls api maintenanceEvent all", function() {
            spyOn(api.maintenanceEvent, "all").and.returnValue($q.when({ data: {} }));

            createController();
            ctrl.model.item = { id: 1 };
            ctrl.model.loadHistory();

            expect(api.maintenanceEvent.all).toHaveBeenCalled();
        });
    });

    describe('#onLoadHistory', function() {
        it("sets the events", function() {

            createController();
            ctrl.model.item = { id: 1 };
            ctrl.model.onLoadHistory({ data: [{ id: 1 }] });

            expect(ctrl.model.item.events.length).toEqual(1);
        });
    });

    describe('#onLoadHistoryError', function() {
        it("calls $log error", function() {
            spyOn($log, "error");

            createController();
            ctrl.model.onLoadHistoryError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
        });
    });
});

describe('hbMaintenanceItemDetails', function() {
    var $scope, $compile, api, $q, context, element, session;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        session = _$injector_.get("Session");
        context = _$injector_.get("Context");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.image, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(api.document, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(context, "getBinder").and.returnValue({ data: { permissions: { can_read: false }, property: {} } });
        spyOn(session, "getUser").and.returnValue({ id: 1, role: "admin" });

        $scope.cfg = null;
        $scope.onDestroyed = function() {};
        $scope.onClose = function() {};

        element = $compile('<hb-maintenance-item-details cfg="cfg" on-destroyed="onDestroyed" on-close="onClose"></hb-maintenance-item-details>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {
            expect(element.html()).toContain("Name");
        });
    });
});