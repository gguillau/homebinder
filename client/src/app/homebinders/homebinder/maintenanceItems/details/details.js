(function() {
	"use strict";

	angular
		.module("hb.homebinders.maintenanceItems.details", [])
		.directive("hbMaintenanceItemDetails", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/maintenanceItems/details/details.tpl.html",
				controller: "MaintenanceItemDetailsController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("MaintenanceItemDetailsController", [
			"hb.resources",
			"BinderItemsDetailsController",
			"hb.api",
			"$stateParams",
			Controller
		]);

	function Controller(resources, BinderItemsDetailsController, api, $stateParams) {
		var Model = function() {
			this.resources = angular.extend({}, this.resources, resources.binderMaintenanceItemsDetails);
			this.nameProperty = "name";
			this.refreshCall = api.maintenanceItem.get;
			this.indexState = "binder.maintenanceItems";
			this.indexParams = { binderId: $stateParams.binderId };
			this.editState = "binder.maintenanceItemsEdit";
			// call the parent class
			BinderItemsDetailsController.call(this);
		};

		Model.prototype = Object.create(BinderItemsDetailsController.prototype);

		angular.extend(Model.prototype, {

			populate: function() {
				this.tagId = "maintenanceItem_" + this.item.id;
				this.editParams = { binderId: $stateParams.binderId, id: this.item.id };
				switch (this.item.maintenance_cycle) {
					case "Once":
						this.interval = "Once";
						break;
					case "As Needed":
						this.interval = "As Needed";
						break;
					default:
						if (this.item.interval == 1) {
							this.frequency = "";
							this.cycle = this.item.maintenance_cycle.slice(0, -1);
						}
						else {
							this.cycle = this.item.maintenance_cycle;
							this.frequency = this.item.interval + " ";
						}
						this.interval = "Every " + this.frequency + this.cycle;
						break;
				}
				this.uploadParams = {
					binder_id: this.item.binder_id,
					maintenance_item_id: this.item.id
				};
				this.loadHistory();
			},

			loadHistory: function() {
				this.api.maintenanceEvent.all(this.item.id).then(
					angular.bind(this, this.onLoadHistory),
					angular.bind(this, this.onLoadHistoryError));
			},

			onLoadHistory: function(response) {
				this.item.events = response.data;
				this.item.events.forEach(function(event) {
					event.can_edit = false;
				});
				this.has_events = true;
			},

			onLoadHistoryError: function(response) {
				this.$log.error(response);
			},
			
			deleteCall: function() {
				this.api.maintenanceItem.destroy(this.item.id).then(
					angular.bind(this, this.onDeleteItemSuccess));
			},
			
			onDeleteItemSuccess: function(response) {
				this.notify.success(this.resources.deleteSuccess);
				this.back();
			}
		});

		this.model = new Model();
	}

})();