describe("HomeBinderMaintenanceItemFormController", function() {
    var $rootScope,
        $controller,
        $stateParams,
        resources,
        ctrl,
        BinderItemsIndexController,
        api,
        $q,
        BinderRecallModal,
        $log;


    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q_.defer();
                    defer.resolve({ permissions: { can_read: true } });
                    return defer.promise;
                }
            };
        });
    }));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $stateParams = _$injector_.get("$stateParams");
        BinderItemsIndexController = _$injector_.get("BinderItemsIndexController");
        resources = _$injector_.get("hb.resources");
        BinderRecallModal = _$injector_.get("BinderRecallModal");
        $log = _$injector_.get("$log");
    }));

    function createController() {
        ctrl = $controller("HomeBinderMaintenanceItemFormController", {
            "BinderItemsIndexController": BinderItemsIndexController,
            "hb.resources": resources,
            "hb.api": api,
            "BinderRecallModal": BinderRecallModal,
            "$log": $log,
            "$stateParams": $stateParams
        });
        $rootScope.$apply();
    }
    
    describe('#init', function() {
        it("sets the state data when editing an existing item", function() {
            $stateParams = {
                binderId: 1,
                id: 1
            };
            createController();

            expect(ctrl.model.indexState).toEqual("binder.maintenanceItemsDetails");
            expect(ctrl.model.indexParams).toEqual({ binderId: 1, id: 1 });
            expect(ctrl.model.editState).toEqual("binder.maintenanceItemsEdit");
        });
        
        it("sets the state data when creating a new item", function() {
            $stateParams = {
                binderId: 1
            };
            createController();

            expect(ctrl.model.indexState).toEqual("binder.maintenanceItems");
            expect(ctrl.model.indexParams).toEqual({ binderId: 1 });
            expect(ctrl.model.editState).toEqual("binder.maintenanceItemsEdit");
        });
    });

    describe('#populate', function() {
        it("doesnt set the item", function() {
            createController();
            ctrl.model.populate();

            expect(ctrl.model.item).toEqual({});
        });

        it("sets the item", function() {
            createController();
            var item = { id: 1, binder_id: 1, name: "test" };
            ctrl.model.item = item;
            ctrl.model.populate();

            expect(ctrl.model.name).toEqual(item.name);
        });

        it("sets the interval", function() {
            createController();
            ctrl.model.item = null;
            ctrl.model.populate();

            expect(ctrl.model.interval).toEqual(1);
        });
    });

    describe("#setFrequency", function() {
        it("should set interval to 1", function() {
            createController();
            ctrl.model.template = { frequency: "As Needed" };
            var item = {};
            ctrl.model.setFrequency(item);

            expect(item.interval).toEqual(1);
        });
        it("should set interval to 1", function() {
            createController();
            ctrl.model.template = { frequency: "Once" };
            var item = {};
            ctrl.model.setFrequency(item);

            expect(item.interval).toEqual(1);
        });
        it("should set interval to 1", function() {
            createController();
            ctrl.model.template = { frequency: "Annual" };
            var item = {};
            ctrl.model.setFrequency(item);

            expect(item.interval).toEqual(1);
        });
        it("should set interval to 6", function() {
            createController();
            ctrl.model.template = { frequency: "Every 6 months" };
            var item = {};
            ctrl.model.setFrequency(item);

            expect(item.interval).toEqual(6);
        });
        it("should set interval to 3", function() {
            createController();
            ctrl.model.template = { frequency: "Quarterly" };
            var item = {};
            ctrl.model.setFrequency(item);

            expect(item.interval).toEqual(3);
        });
        it("should set interval to 1", function() {
            createController();
            ctrl.model.template = { frequency: "Monthly" };
            var item = {};
            ctrl.model.setFrequency(item);

            expect(item.interval).toEqual(1);
        });
        it("should set interval to 2", function() {
            createController();
            ctrl.model.template = { frequency: "Every other year" };
            var item = {};
            ctrl.model.setFrequency(item);

            expect(item.interval).toEqual(2);
        });
        it("should set interval to 3", function() {
            createController();
            ctrl.model.template = { frequency: "Every 3 years" };
            var item = {};
            ctrl.model.setFrequency(item);

            expect(item.interval).toEqual(3);
        });
        it("should set interval to 4", function() {
            createController();
            ctrl.model.template = { frequency: "Every 4 years" };
            var item = {};
            ctrl.model.setFrequency(item);

            expect(item.interval).toEqual(4);
        });
        it("should set interval to 5", function() {
            createController();
            ctrl.model.template = { frequency: "Every 5 years" };
            var item = {};
            ctrl.model.setFrequency(item);

            expect(item.interval).toEqual(5);
        });
        it("should set interval to 10", function() {
            createController();
            ctrl.model.template = { frequency: "Every 10 years" };
            var item = {};
            ctrl.model.setFrequency(item);

            expect(item.interval).toEqual(10);
        });
        it("should set interval to 40", function() {
            createController();
            ctrl.model.template = { frequency: "Every 40 years" };
            var item = {};
            ctrl.model.setFrequency(item);

            expect(item.interval).toEqual(40);
        });
    });

    describe('#setPayload', function() {
        it("returns the payload", function() {
            createController();
            ctrl.model.name = "test";
            var data = ctrl.model.setPayload();

            expect(data.name).toEqual("test");
        });
    });

});

describe('hbMaintenanceItemForm', function() {
    var $scope, $compile, api, $q, context, element, session;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        session = _$injector_.get("Session");
        context = _$injector_.get("Context");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(context, "getBinder").and.returnValue({ data: { permissions: { can_read: false }, property: {} } });
        spyOn(session, "getUser").and.returnValue({ id: 1, role: "admin" });

        $scope.cfg = { binderId: 1, item: null };
        $scope.onSaved = function() {};
        $scope.onCancel = function() {};

        element = $compile('<hb-maintenance-item-form cfg="cfg" on-saved="onSaved" on-cancel="onCancel"></hb-maintenance-item-form>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {
            expect(element.html()).toContain("Due Next On");
        });
    });
});