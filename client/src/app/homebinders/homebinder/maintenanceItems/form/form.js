(function() {
    "use strict";

    angular
        .module("hb.homebinders.maintenanceItems.form", [])
        .directive("hbMaintenanceItemForm", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/maintenanceItems/form/form.tpl.html",
                controller: "HomeBinderMaintenanceItemFormController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .factory("MaintenanceItemFormController", [
            "hb.resources",
            "BinderItemsFormController",
            "hb.api",
            "$stateParams",
            function(resources, BinderItemsFormController, api, $stateParams) {
                var that = this;
                var Model = function() {
                    this.cfg = that.cfg;
                    this.onSaved = that.onSaved;
                    this.onCancel = that.onCancel;
                    this.resources = angular.extend({}, this.resources, resources.binderMaintenanceItemsForm);
                    this.cycles = [{
                        id: 1,
                        name: "Days"
                    }, {
                        id: 2,
                        name: "Weeks"
                    }, {
                        id: 3,
                        name: "Months"
                    }, {
                        id: 4,
                        name: "Years"
                    }];
                    this.maintenanceItemTypes = [];
                    this.taggerCfg = {
                        maintenanceItemTags: false
                    };
                    this.email_notifications = true;
                    this.tagName = "maintenanceItem_";
                    // call the parent class
                    this.saveCall = api.maintenanceItem.update;
                    this.createCall = api.maintenanceItem.create;
                    this.deleteCall = api.maintenanceItem.destroy;
                    this.refreshCall = api.maintenanceItem.get;
                    this.getTemplateItem = api.maintenanceTemplates.get;
                    BinderItemsFormController.call(this);
                    this.init();
                };

                Model.prototype = Object.create(BinderItemsFormController.prototype);

                angular.extend(Model.prototype, {

                    onRefreshTemplateSuccess: function(response) {
                        this.template = response.data;
                        this.name = response.data.name;
                        this.details = response.data.notes;
                        this.do_date = response.data.due_date;
                        this.setFrequency(this.item);
                        this.populate();
                        this.loading.close();
                    },

                    setFrequency: function(item) {
                        // convert interval dropdown for maintenance_cycle
                        switch (this.template.frequency) {
                            case "As Needed":
                                item.interval = 1;
                                item.maintenance_cycle = "As Needed";
                                break;
                            case "Once":
                                item.interval = 1;
                                item.maintenance_cycle = "Once";
                                break;
                            case "Annual":
                                item.interval = 1;
                                item.maintenance_cycle = "Years";
                                break;
                            case "Every 6 months":
                                item.interval = 6;
                                item.maintenance_cycle = "Months";
                                break;
                            case "Quarterly":
                                item.interval = 3;
                                item.maintenance_cycle = "Months";
                                break;
                            case "Monthly":
                                item.interval = 1;
                                item.maintenance_cycle = "Months";
                                break;
                            case "Every other year":
                                item.interval = 2;
                                item.maintenance_cycle = "Years";
                                break;
                            case "Every 3 years":
                                item.interval = 3;
                                item.maintenance_cycle = "Years";
                                break;
                            case "Every 4 years":
                                item.interval = 4;
                                item.maintenance_cycle = "Years";
                                break;
                            case "Every 5 years":
                                item.interval = 5;
                                item.maintenance_cycle = "Years";
                                break;
                            case "Every 10 years":
                                item.interval = 10;
                                item.maintenance_cycle = "Years";
                                break;
                            case "Every 40 years":
                                item.interval = 40;
                                item.maintenance_cycle = "Years";
                                break;
                        }
                    },

                    populate: function() {
                        // if a item was passed in store the values
                        if (this.item && this.item.id) {
                            this.name = this.item.name;
                            this.editParams = { binderId: $stateParams.binderId, id: this.item.id };
                            this.interval = this.item.interval;
                            this.maintenance_cycle = this.item.maintenance_cycle;
                            this.completed_date = this.item.last_event_date ? new Date(this.item.last_event_date) : this.item.last_event_date;
                            this.do_date = this.item.do_date ? new Date(this.item.do_date) : this.item.do_date;
                            this.email_notifications = this.item.email_notifications;
                            this.details = this.item.details;
                            this.library_source_id = this.item.library_source_id;
                            this.tags = this.item.tags;
                            this.tagsCfg = {
                                binderId: $stateParams.binderId
                            };
                            this.docUploader.jwt = this.jwt;
                            this.docUploader.params = {
                                binder_id: $stateParams.binderId
                            };
                            this.imgUploader.jwt = this.jwt;
                            this.imgUploader.params = {
                                binder_id: $stateParams.binderId
                            };
                            this.setUploadParams();
                        }
                        else {
                            this.interval = 1;
                            this.maintenance_cycle = this.cycles[3].name;
                        }
                    },

                    setPayload: function() {
                        // create payload
                        return {
                            binder_id: this.binderId,
                            name: this.name,
                            interval: this.interval,
                            maintenance_cycle: this.maintenance_cycle,
                            do_date: this.do_date,
                            library_source_id: this.library_source_id,
                            email_notifications: this.email_notifications,
                            details: this.details,
                            tags_attributes: this.tags
                        };
                    },

                    setUploadParams: function() {
                        this.uploadParams = {
                            binder_id: this.item.binder_id,
                            maintenance_item_id: this.item.id
                        };
                    }
                });

                return Model;


            }
        ])
        // Model backing the maintenanceItem modal form
        .controller("HomeBinderMaintenanceItemFormController", [
            "MaintenanceItemFormController",
            "$stateParams",
            Controller
        ]);

    function Controller(MaintenanceItemFormController, $stateParams) {
        var Model = function() {
            MaintenanceItemFormController.call(this);
            
            // If $stateParams has an id, we are editing an exisiting item and back should send us to that item's details
            if ($stateParams.id) {
                this.indexState = "binder.maintenanceItemsDetails";
                this.indexParams = { binderId: $stateParams.binderId, id: $stateParams.id };
            }
            // Else, we were creating a new item and back should send us to the list of items
            else {
                this.indexState = "binder.maintenanceItems";
                this.indexParams = { binderId: $stateParams.binderId };
            }
    
            this.editState = "binder.maintenanceItemsEdit";
        };

        Model.prototype = Object.create(MaintenanceItemFormController.prototype);

        angular.extend(Model.prototype, {});

        this.model = new Model();
    }

})();