describe("hb.homebinders.maintenanceItems.routes", function() {
    var $rootScope,
        BinderItemsModal,
        $state,
        User,
        api,
        $q;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $q = _$injector_.get("$q");
        $state = _$injector_.get("$state");
        User = _$injector_.get("User");
        api = _$injector_.get("hb.api");
        BinderItemsModal = _$injector_.get("BinderItemsModal");
    }));

    describe('binder.maintenanceItemsDetails', function() {
        it('calls modal', function() {
            // set up the spies
            spyOn(api.binder, "get").and.returnValue($q.when({ data: { id: 1, permissions: { can_read: false } } }));
            spyOn(api.store, "all").and.returnValue($q.when({ data: [] }));
            spyOn(api.image, "all").and.returnValue($q.when({ data: { items: [] } }));
            spyOn(api.document, "all").and.returnValue($q.when({ data: { items: [] } }));
            spyOn(api.maintenanceItem, "get").and.returnValue($q.when({ data: { id: 1 } }));
            spyOn(User, "current").and.returnValue({ id: 1, email: "test@gmail.com" });
            spyOn(BinderItemsModal, "showDetails");

            // go the state
            $state.go("binder.maintenanceItemsDetails", { binderId: 1, id: 1 });
            $rootScope.$digest();

            //expect(BinderItemsModal.showDetails).toHaveBeenCalled();

        });
    });

    describe('binder.maintenanceItemsNew', function() {
        it('calls modal', function() {
            // set up the spies
            spyOn(api.binder, "get").and.returnValue($q.when({ data: { id: 1, permissions: { can_read: false } } }));
            spyOn(api.store, "all").and.returnValue($q.when({ data: [] }));
            spyOn(User, "current").and.returnValue({ id: 1, email: "test@gmail.com" });
            spyOn(BinderItemsModal, "showForm");

            // go the state
            $state.go("binder.maintenanceItemsNew", { binderId: 1 });
            $rootScope.$digest();

            //expect(BinderItemsModal.showForm).toHaveBeenCalled();

        });
    });

    describe('binder.maintenanceItemsEdit', function() {
        it('calls modal', function() {
            // set up the spies
            spyOn(api.binder, "get").and.returnValue($q.when({ data: { id: 1, permissions: { can_read: false } } }));
            spyOn(api.store, "all").and.returnValue($q.when({ data: [] }));
            spyOn(api.maintenanceItem, "get").and.returnValue($q.when({ data: { id: 1 } }));
            spyOn(User, "current").and.returnValue({ id: 1, email: "test@gmail.com" });
            spyOn(BinderItemsModal, "showForm");
            spyOn(BinderItemsModal, "close");

            // go the state
            $state.go("binder.maintenanceItemsEdit", { binderId: 1, id: 1 });
            $rootScope.$digest();

            //expect(BinderItemsModal.showForm).toHaveBeenCalled();

            // go the state
            $state.go("binder.maintenanceItems", { binderId: 1 });
            $rootScope.$apply();

            //expect(BinderItemsModal.close).toHaveBeenCalled();

        });
    });

});