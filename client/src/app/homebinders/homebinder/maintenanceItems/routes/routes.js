angular
    .module("hb.homebinders.maintenanceItems.routes", [])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            // maintenanceItem index state
            .state("binder.maintenanceItems", {
                url: "^/binders/:binderId/maintenanceItems",
                template: "<binder-maintenance-items></binder-maintenance-items>",
                access: {
                    requiresLogin: true
                }
            })
            // maintenanceItem details state
            .state("binder.maintenanceItemsDetails", {
                url: "^/binders/:binderId/maintenanceItems/{id:int}",
                template: "<hb-maintenance-item-details></hb-maintenance-item-details>",
                access: {
                    requiresLogin: true
                }
            })
            // state for new maintenanceItem
            .state("binder.maintenanceItemsNew", {
                url: "^/binders/:binderId/maintenanceItems/new",
                template: "<hb-maintenance-item-form></hb-maintenance-item-form>",
                access: {
                    requiresLogin: true
                }
            })
            // state for new maintenanceItem
            .state("binder.maintenanceItemsNewTemplate", {
                url: "^/binders/:binderId/maintenanceItems/new/{templateId:int}",
                template: "<hb-maintenance-item-form></hb-maintenance-item-form>",
                access: {
                    requiresLogin: true
                }
            })
            // state for edit maintenanceItem
            .state("binder.maintenanceItemsEdit", {
                url: "^/binders/:binderId/maintenanceItems/{id:int}/edit",
                template: "<hb-maintenance-item-form></hb-maintenance-item-form>",
                access: {
                    requiresLogin: true
                }
            });
    }]);