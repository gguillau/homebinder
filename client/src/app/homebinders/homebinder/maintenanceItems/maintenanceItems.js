(function() {
    "use strict";

    angular
        .module("hb.homebinders.maintenanceItems", [
            "hb.homebinders.maintenanceItems.routes",
            "hb.homebinders.maintenanceItems.details",
            "hb.homebinders.maintenanceItems.form",
            "hb.homebinders.maintenanceItems.modal"
        ])
        .directive("binderMaintenanceItems", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/maintenanceItems/maintenanceItems.tpl.html",
                controller: "HomeBinderMaintenanceItemsController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .factory("BinderMaintenanceItemsController", [
            "BinderItemsIndexController",
            "hb.resources",
            function(BinderItemsIndexController, resources) {
                var Model = function() {
                    // call the parent class
                    BinderItemsIndexController.call(this);
                    this.resources = angular.extend({}, this.resources, resources.binderMaintenanceItemsIndex);
                    this.refreshCall = this.api.maintenanceItem.all;
                    this.deleteCall = this.api.maintenanceItem.destroy;
                    this.itemType = "maintenanceItems";
                    this.nameProperty = "name";
                    this.modalController = "MaintenanceItemModalController";
                    this.sortBy = "do_date";
                    this.orderBy = null;
                    this.order = null;
                    this.templateConfig = {
                        templateUrl: "homebinders/homebinder/maintenanceItems/template/template.tpl.html",
                        resources: this.resources,
                        refreshCall: this.api.maintenanceTemplates.all
                    };
                    this.itemNewState = "binder.maintenanceItemsNew";
                    this.itemNewTemplateState = "binder.maintenanceItemsNewTemplate";
                    this.itemDetailsState = "binder.maintenanceItemsDetails";
                    this.hasTemplates = true;
                };

                Model.prototype = Object.create(BinderItemsIndexController.prototype);

                return Model;

            }
        ])
        .controller("HomeBinderMaintenanceItemsController", [
            "BinderMaintenanceItemsController",
            HomeBinderMaintenanceItemsController
        ]);

    function HomeBinderMaintenanceItemsController(BinderMaintenanceItemsController) {
        var Model = function() {
            // call the parent class
            BinderMaintenanceItemsController.call(this);
            this.itemDetailsState = "binder.maintenanceItemsDetails";
            this.init();
        };

        Model.prototype = Object.create(BinderMaintenanceItemsController.prototype);

        this.model = new Model();
    }
})();