(function() {
    "use strict";

    angular
        .module("hb.homebinders.binderContractors", [
            "hb.homebinders.binderContractors.routes",
            "hb.homebinders.binderContractors.details",
            "hb.homebinders.binderContractors.form",
            "hb.homebinders.binderContractors.modal"
        ])
        .directive("binderBinderContractors", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/binderContractors/binderContractors.tpl.html",
                controller: "HomeBinderContractorsController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .factory("BinderContractorsController", [
            "BinderItemsIndexController",
            "hb.resources",
            function(BinderItemsIndexController, resources) {
                var Model = function() {
                    // call the parent class
                    BinderItemsIndexController.call(this);
                    this.resources = angular.extend({}, this.resources, resources.binderContractorsIndex);
                    this.refreshCall = this.api.binderContractor.all;
                    this.deleteCall = this.api.binderContractor.destroy;
                    this.itemType = "binderContractors";
                    this.itemNewState = "binder.contractorsNew";
                    this.itemDetailsState = "binder.contractorsDetails";
                    this.editState = "binder.contractorsEdit";
                    this.sortBy = "contact";
                    this.nameProperty = "contact";
                    this.modalController = "BinderContractorModalController";
                };

                Model.prototype = Object.create(BinderItemsIndexController.prototype);

                return Model;


            }
        ])
        .controller("HomeBinderContractorsController", [
            "BinderContractorsController",
            "AprHomeProRequestForm",
            HomeBinderContractorsController
        ]);

    function HomeBinderContractorsController(BinderContractorsController, AprHomeProRequestForm) {
        var Model = function() {
            // call the parent class
            BinderContractorsController.call(this);
            this.init();
        };

        Model.prototype = Object.create(BinderContractorsController.prototype);

        angular.extend(Model.prototype, {
            contact: function(pro) {
                AprHomeProRequestForm.display({
                    pro: {
                        id: pro.id,
                        displayName: pro.contractor.name,
                        phone: pro.contractor.phone,
                        email: pro.contractor.email
                    },
                    apr: null,
                    requestType: "contact"
                });
            }
        });

        this.model = new Model();
    }
})();