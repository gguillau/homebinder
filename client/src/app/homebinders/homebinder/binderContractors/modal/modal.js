(function() {
    angular
        .module("hb.homebinders.binderContractors.modal", [])
        .controller("BinderContractorModalController", [
            "hb.resources",
            "hb.api",
            "BinderItemsModalBaseController",
            "$modalInstance",
            "args",
            Controller
        ]);

    function Controller(resources, api, BinderItemsModalBaseController, $modalInstance, args) {
        var Model = function() {
            this.refreshCall = api.binderContractor.get;
            this.resources = resources.binderContractorsModal;
            this.$modalInstance = $modalInstance;
            this.args = args;
            this.nameProperty = "contact";
            BinderItemsModalBaseController.call(this);

        };

        Model.prototype = Object.create(BinderItemsModalBaseController.prototype);

        this.model = new Model();
    }
})();