angular
    .module("hb.homebinders.binderContractors.routes", [])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            // binderContractor index state
            .state("binder.contractors", {
                url: "^/binders/:binderId/binder_contractors",
                template: "<binder-binder-contractors></binder-binder-contractors>",
                access: {
                    requiresLogin: true
                }
            })
            // binderContractor details state
            .state("binder.contractorsDetails", {
                url: "^/binders/:binderId/binder_contractors/{id:int}",
                template: "<hb-binder-contractor-details></<hb-binder-contractor-details>",
                access: {
                    requiresLogin: true
                }
            })
            // state for new binderContractor
            .state("binder.contractorsNew", {
                url: "^/binders/:binderId/binder_contractors/new",
                template: "<hb-binder-contractor-form></<hb-binder-contractor-form>",
                access: {
                    requiresLogin: true
                }
            })
            // state for edit binderContractor
            .state("binder.contractorsEdit", {
                url: "^/binders/:binderId/binder_contractors/{id:int}/edit",
                template: "<hb-binder-contractor-form></<hb-binder-contractor-form>",
                access: {
                    requiresLogin: true
                }
            });
    }]);