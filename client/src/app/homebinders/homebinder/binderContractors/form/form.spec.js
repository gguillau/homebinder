describe("HomeBinderBinderContractorFormController", function() {
    var $rootScope,
        $controller,
        resources,
        ctrl,
        BinderItemsIndexController,
        api,
        $q,
        $log,
        notify,
        HomeProSelect,
        defer,
        $stateParams;

    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { id: 1, role: "admin" };
                },
                getLocale: function() {
                    return "en";
                },
                getJwt: function() {
                    return null;
                }
            };
        });
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q.defer();
                    defer.resolve({ permissions: { can_read: true }, property: {} });
                    return defer.promise;
                }
            };
        });
    }));

    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        notify = _$injector_.get("Notify");
        BinderItemsIndexController = _$injector_.get("BinderItemsIndexController");
        resources = _$injector_.get("hb.resources");
        HomeProSelect = _$injector_.get("HomeProSelect");
        $log = _$injector_.get("$log");
        $stateParams = _$injector_.get("$stateParams");
    }));

    function createController() {
        ctrl = $controller("HomeBinderBinderContractorFormController", {
            "BinderItemsIndexController": BinderItemsIndexController,
            "hb.resources": resources,
            "hb.api": api,
            "$log": $log,
            "$stateParams": $stateParams
        });
        $rootScope.$apply();
    }

    beforeEach(function() {
        spyOn(api.contractorType, "all").and.returnValue($q.when({ data: { items: [] } }));
    });

    describe('#init', function() {
        it("sets the state data when editing an existing item", function() {
            $stateParams = {
                binderId: 1,
                id: 1
            };
            createController();

            expect(ctrl.model.indexState).toEqual("binder.contractorsDetails");
            expect(ctrl.model.indexParams).toEqual({ binderId: 1, id: 1 });
            expect(ctrl.model.editState).toEqual("binder.contractorsEdit");
        });

        it("sets the state data when creating a new item", function() {
            $stateParams = {
                binderId: 1
            };
            createController();

            expect(ctrl.model.indexState).toEqual("binder.contractors");
            expect(ctrl.model.indexParams).toEqual({ binderId: 1 });
            expect(ctrl.model.editState).toEqual("binder.contractorsEdit");
        });
    });

    describe('#homeProSelected', function() {
        it("sets the pro and calls populate", function() {
            createController();
            ctrl.model.item = {
                id: 1,
                contractor: {
                    id: 1,
                    name: "test",
                    user: {
                        id: 1,
                        sign_in_count: 1
                    }
                }
            };
            spyOn(ctrl.model, "populate");
            ctrl.model.homeProSelected({ id: 1, name: "selected" });

            expect(ctrl.model.populate).toHaveBeenCalled();
            expect(ctrl.model.item.contractor.name).toEqual("selected");
        });
    });

    describe('#populate', function() {
        it("doesnt set the item", function() {
            createController();
            ctrl.model.populate();

            expect(ctrl.model.item).toEqual({});
        });

        it("sets empty fields", function() {
            createController();
            ctrl.model.item = null;
            ctrl.model.currentUser = {};
            ctrl.model.populate();

            expect(ctrl.model.secondary_phone).toEqual({});
        });

        it("sets the item", function() {
            createController();
            var item = { id: 1, binder_id: 1, contractor: { name: "test", user: { id: 1, sign_in_count: 1 }, address: { country: "US" } } };
            ctrl.model.item = item;
            ctrl.model.populate();

            expect(ctrl.model.name).toEqual(item.contractor.name);
        });
    });

    describe("#searchTypes", function() {
        it("calls api contractorType all when value exists and its length is greater than 3", function() {
            createController();
            ctrl.model.searchTypes("search");

            expect(api.contractorType.all).toHaveBeenCalled();
        });

        it("does not call api contractorType all when value is undefined", function() {
            createController();
            ctrl.model.searchTypes(undefined);

            expect(api.contractorType.all).not.toHaveBeenCalled();
        });

        it("does not call api contractorType all when value is null", function() {
            createController();
            ctrl.model.searchTypes(null);

            expect(api.contractorType.all).not.toHaveBeenCalled();
        });

        it("does not call api contractorType all when value length is less than 3", function() {
            createController();
            ctrl.model.searchTypes("se");

            expect(api.contractorType.all).not.toHaveBeenCalled();
        });
    });

    describe("#searchSubTypes", function() {
        it("calls api contractorSubType all", function() {
            spyOn(api.contractorSubType, "all").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.model.searchSubTypes("search");

            expect(api.contractorSubType.all).toHaveBeenCalled();
        });

        it("does not call api contractorSubType all", function() {
            spyOn(api.contractorSubType, "all").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.model.searchSubTypes(undefined);

            expect(api.contractorSubType.all).not.toHaveBeenCalled();
        });
    });

    describe("searchComplete", function() {
        it("sets the options", function() {
            createController();
            ctrl.model.types = [];
            ctrl.model.item = {
                types: []
            };
            ctrl.model.searchComplete({ data: { items: [{ id: 1, name: "test" }] } });

            expect(ctrl.model.typeOptions.length).toEqual(1);
        });
    });

    describe("searchError", function() {
        it("calls notify error", function() {
            spyOn(notify, "error");

            createController();
            ctrl.model.searchError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("searchSubComplete", function() {
        it("sets the options", function() {
            createController();
            ctrl.model.sub_types = [];
            ctrl.model.item = {
                sub_types: []
            };
            ctrl.model.searchSubComplete({ data: { items: [{ id: 1 }] } });

            expect(ctrl.model.subTypeOptions.length).toEqual(1);
        });
    });

    describe("onTypeSelect", function() {
        it("calls findExisting and updates the sub_types", function() {
            createController();
            var type = { id: 1, name: "test" };
            ctrl.model.types = [type];
            ctrl.model.item = { types: [type] };
            spyOn(ctrl.model, "findExisting").and.returnValue({ index: -1, item: { id: 1, name: "test", sub_types: [{ id: 1, name: "cpa" }] } });
            spyOn(ctrl.model, "onSubTypeSelect");
            ctrl.model.onTypeSelect({}, type);

            expect(ctrl.model.item.types.length).toEqual(1);
            expect(ctrl.model.onSubTypeSelect).toHaveBeenCalled();
        });
    });

    describe("onSubTypeSelect", function() {
        it("calls findExistingSubTypes and updates the sub_types", function() {
            createController();
            var type = { id: 1, name: "test" };
            ctrl.model.sub_types = [type];
            ctrl.model.item = { sub_types: [type] };
            spyOn(ctrl.model, "findExistingSubTypes").and.returnValue({ index: -1, item: { name: "test" } });
            ctrl.model.onSubTypeSelect({}, type);

            expect(ctrl.model.item.sub_types.length).toEqual(1);
            expect(ctrl.model.findExistingSubTypes).toHaveBeenCalled();
        });
    });

    describe("onTypeRemove", function() {
        it("calls findExisting and updates the item's types", function() {
            createController();
            var type = { id: 1, name: "test", sub_types: ["test"] };
            ctrl.model.binder_contractor_types = [type];
            ctrl.model.item = { types: [type] };
            expect(ctrl.model.item.types.length).toEqual(1);
            spyOn(ctrl.model, "findExisting").and.returnValue({ index: 0, name: "test" });
            ctrl.model.onTypeRemove(type);

            expect(ctrl.model.binder_contractor_types.length).toEqual(0);
        });

        it("calls findExisting and updates the item's types", function() {
            createController();
            var type = { id: 1, name: "test", sub_types: ["test"] };
            ctrl.model.binder_contractor_types = undefined;
            ctrl.model.item = { types: [type] };
            expect(ctrl.model.item.types.length).toEqual(1);
            spyOn(ctrl.model, "findExisting").and.returnValue({ index: 0, name: "test" });
            ctrl.model.onTypeRemove(type);

            expect(ctrl.model.binder_contractor_types.length).toEqual(0);
        });
    });

    describe("onSubTypeRemove", function() {
        it("calls findExisting and updates the item's sub_types", function() {
            createController();
            var type = { id: 1, name: "test" };
            ctrl.model.binder_contractor_sub_types = [type];
            ctrl.model.item = { binder_contractor_sub_types: [type] };
            expect(ctrl.model.item.binder_contractor_sub_types.length).toEqual(1);
            spyOn(ctrl.model, "findExistingSubTypes").and.returnValue({ index: 0, name: "test" });
            ctrl.model.onSubTypeRemove(type);

            expect(ctrl.model.binder_contractor_sub_types.length).toEqual(0);
        });
    });

    describe("findExisting", function() {
        it("returns an index of -1", function() {
            createController();
            ctrl.model.binder_contractor_types = [];
            ctrl.model.item = { binder_contractor_types: [] };
            var hash = ctrl.model.findExisting({ type: { name: "test" } });

            expect(hash.index).toEqual(-1);
        });
    });

    describe("tagTransform", function() {
        it("returns the item", function() {
            createController();
            var expected = {
                name: "newTag",
                sub_types: []
            };
            var item = ctrl.model.tagTransform("newTag");

            expect(item).toEqual(expected);
        });
    });

    describe("findExistingSubTypes", function() {
        it("returns an index of -1", function() {
            createController();
            ctrl.model.binder_contractor_sub_types = [];
            ctrl.model.item = { binder_contractor_sub_types: [] };
            var hash = ctrl.model.findExistingSubTypes({ sub_type: { name: "test" } });

            expect(hash.index).toEqual(-1);
        });
    });

    describe('#setPayload', function() {
        it("returns the payload", function() {
            createController();
            ctrl.model.item = { types: [], sub_types: [] };
            ctrl.model.account_number = "test";
            ctrl.model.phone = {};
            ctrl.model.secondary_phone = {};
            var data = ctrl.model.setPayload();
            expect(data.account_number).toEqual("test");
        });
    });

    describe('#onSelect', function() {
        it("calls address setCountry", function() {
            createController();
            spyOn(ctrl.model.address, "setCountry");
            ctrl.model.onSelect({ country: { value: "US" } });

            expect(ctrl.model.country).toEqual({ value: "US" });
        });
    });

    describe('#checkForExisting', function() {
        it("calls contractor all", function() {
            createController();
            spyOn(api.contractor, "all").and.returnValue($q.when({ data: { items: [] } }));

            ctrl.model.name = "";
            ctrl.model.email = "";
            ctrl.model.phone = {};
            ctrl.model.secondary_name = "";
            ctrl.model.secondary_email = "";
            ctrl.model.secondary_phone = {};
            ctrl.model.checkForExisting();

            expect(api.contractor.all).toHaveBeenCalled();
        });
    });

    describe('#existingSuccess', function() {
        it("calls Home Pro Select modal", function() {
            createController();
            spyOn(HomeProSelect, "show");

            ctrl.model.existingSuccess({ data: { items: [{ id: 1 }] } });

            expect(HomeProSelect.show).toHaveBeenCalled();
        });

        it("calls createOrUpdate", function() {
            createController();
            spyOn(ctrl.model, "createOrUpdate");

            ctrl.model.existingSuccess({ data: { items: [] } });

            expect(ctrl.model.createOrUpdate).toHaveBeenCalled();
        });
    });

    describe('#existingError', function() {
        it("calls notify error", function() {
            createController();
            spyOn(notify, "error");

            ctrl.model.existingError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe('#setHomePro', function() {
        it("sets the correct fields and calls createOrUpdate", function() {
            createController();
            spyOn(ctrl.model, "createOrUpdate");

            ctrl.model.secondary_phone = {};
            ctrl.model.setHomePro({ name: "test" });

            expect(ctrl.model.name).toEqual("test");
            expect(ctrl.model.createOrUpdate).toHaveBeenCalled();
        });

        it("calls createOrUpdate", function() {
            createController();
            spyOn(ctrl.model, "createOrUpdate");

            ctrl.model.setHomePro();

            expect(ctrl.model.createOrUpdate).toHaveBeenCalled();
        });
    });

    describe('#submit', function() {
        it("returns and does not call createOrUpdate or checkForExisting due to invalid form", function() {
            createController();
            spyOn(ctrl.model, "createOrUpdate");
            spyOn(ctrl.model, "checkForExisting");
            ctrl.model.form = { $invalid: true };

            ctrl.model.submit();

            expect(ctrl.model.createOrUpdate).not.toHaveBeenCalled();
            expect(ctrl.model.checkForExisting).not.toHaveBeenCalled();
        });

        it("returns and does not call createOrUpdate or checkForExisting due to homeowner submitting their own email", function() {
            createController();
            spyOn(ctrl.model, "createOrUpdate");
            spyOn(ctrl.model, "checkForExisting");

            var testEmail = "test@homebinder.com";
            ctrl.model.form = { $invalid: false };
            ctrl.model.currentUser = { email: testEmail };
            ctrl.model.email = testEmail;

            ctrl.model.submit();

            expect(ctrl.model.createOrUpdate).not.toHaveBeenCalled();
            expect(ctrl.model.checkForExisting).not.toHaveBeenCalled();
        });

        it("calls createOrUpdate but not checkForExisting", function() {
            createController();
            spyOn(ctrl.model, "createOrUpdate");
            spyOn(ctrl.model, "checkForExisting");
            ctrl.model.form = { $invalid: false };
            ctrl.model.currentUser = { email: "test@homebinder.com" };
            ctrl.model.item = { id: 1 };
            ctrl.model.submit();

            expect(ctrl.model.createOrUpdate).toHaveBeenCalled();
            expect(ctrl.model.checkForExisting).not.toHaveBeenCalled();
        });

        it("calls checkForExisting but not createOrUpdate", function() {
            createController();
            spyOn(ctrl.model, "createOrUpdate");
            spyOn(ctrl.model, "checkForExisting");
            ctrl.model.form = { $invalid: false };
            ctrl.model.currentUser = { email: "test@homebinder.com" };
            ctrl.model.item = {};
            ctrl.model.submit();

            expect(ctrl.model.createOrUpdate).not.toHaveBeenCalled();
            expect(ctrl.model.checkForExisting).toHaveBeenCalled();
        });
    });

    describe('#createOrUpdate', function() {
        it("calls saveCall", function() {
            createController();
            spyOn(ctrl.model, "saveCall").and.returnValue($q.when({ data: {} }));
            ctrl.model.item = { id: 1 };
            ctrl.model.secondary_phone = {};
            ctrl.model.createOrUpdate();

            expect(ctrl.model.saveCall).toHaveBeenCalled();
        });

        it("calls createCall", function() {
            createController();
            spyOn(ctrl.model, "createCall").and.returnValue($q.when({ data: {} }));
            spyOn(ctrl.model, "checkForExisting");
            ctrl.model.form = { $invalid: false };
            ctrl.model.currentUser = {};
            ctrl.model.secondary_phone = {};
            ctrl.model.phone = {};
            ctrl.model.types = [];
            ctrl.model.createOrUpdate();

            expect(ctrl.model.createCall).toHaveBeenCalled();
        });
    });
});

describe('hbBinderContractorForm', function() {
    var $scope, $compile, api, $q, context, element, session;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        context = _$injector_.get("Context");
        session = _$injector_.get("Session");

        $scope = $rootScope.$new(), $compile = _$compile_;

        spyOn(session, "getUser").and.returnValue({ id: 1 });
        spyOn(api.contractorType, "all").and.returnValue($q.when({ data: [] }));
        spyOn(context, "getBinder").and.returnValue({ data: { permissions: { can_read: false }, property: {} } });

        $scope.cfg = { binderId: 1, item: {} };
        $scope.onSaved = function() {};
        $scope.onCancel = function() {};

        element = $compile('<hb-binder-contractor-form cfg="cfg" on-saved="onSaved" on-cancel="onCancel"></hb-binder-contractor-form>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the hb-binder-contractor-form directive', function() {
        it('sets the html elements', function() {
            expect(element.html()).toContain("Types");
        });
    });
});