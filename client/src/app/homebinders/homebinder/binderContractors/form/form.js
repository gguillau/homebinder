(function() {
    "use strict";

    angular
        .module("hb.homebinders.binderContractors.form", [])
        .directive("hbBinderContractorForm", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/binderContractors/form/form.tpl.html",
                controller: "HomeBinderBinderContractorFormController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .factory("BinderContractorFormController", [
            "hb.resources",
            "BinderItemsFormController",
            "hb.api",
            "Address",
            "$filter",
            "Notify",
            "Context",
            "$log",
            "HomeProSelect",
            "Session",
            "$stateParams",
            function(resources, BinderItemsFormController, api, address, $filter, notify, context, $log, HomeProSelect, session, $stateParams) {
                var that = this;
                var Model = function() {
                    context.getBinder().then(angular.bind(this, function(binder) {
                        this.binder = binder;
                        this.country = address.findCountryByCode(this.binder.property.country);
                        this.states = address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                            return state.country.value === this.country.value;
                        }));
                    }));
                    this.currentUser = session.getUser();
                    this.cfg = that.cfg;
                    this.onSaved = that.onSaved;
                    this.onCancel = that.onCancel;
                    this.resources = angular.extend({}, this.resources, resources.binderContractorsForm);
                    this.taggerCfg = {
                        binderContractorTags: false
                    };
                    this.tagName = "binderContractor_";
                    // call the parent class
                    this.saveCall = api.binderContractor.update;
                    this.createCall = api.binderContractor.create;
                    this.deleteCall = api.binderContractor.destroy;
                    this.refreshCall = api.binderContractor.get;
                    BinderItemsFormController.call(this);
                    this.address = address;
                    this.countries = address.countries();
                    this.states = address.getStatesAndProvinces();
                    this.typeOptions = [];
                    this.subTypeOptions = [];
                    this.homeProLookupCfg = {
                        homeProSelected: angular.bind(this, this.homeProSelected)
                    };
                    this.url_regex = /^((?:http|ftp)s?:\/\/)(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|localhost|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(?::\d+)?(?:\/?|[\/?]\S+)$/i;
                    this.init();
                };

                Model.prototype = Object.create(BinderItemsFormController.prototype);

                angular.extend(Model.prototype, {


                    homeProSelected: function(contractor) {
                        this.item.contractor = contractor;
                        this.populate();
                    },

                    populate: function() {
                        // if a item was passed in store the values
                        if (this.item && this.item.id) {
                            this.account_number = this.item.account_number;
                            this.contact = this.item.contact;
                            this.types = this.item.types;
                            this.binder_contractor_types = this.item.binder_contractor_types;
                            this.binder_contractor_sub_types = this.item.binder_contractor_sub_types;
                            this.sub_types = this.item.sub_types;
                            this.worked_on_property = this.item.worked_on_property;
                            this.secondary_name = this.item.secondary_name;
                            this.secondary_email = this.item.secondary_email;
                            this.secondary_phone = this.item.secondary_phone;
                            this.secondary_details = this.item.secondary_details;
                            this.contractor_id = this.item.contractor_id;
                            if (this.item.contractor) {
                                this.name = this.item.contractor.name;
                                this.phone = this.item.contractor.phone;
                                this.email = this.item.contractor.email;
                                this.url = this.item.contractor.url;
                            }
                            if (this.item.contractor && this.item.contractor.address) {
                                this.address_id = this.item.contractor.address.id;
                                this.address1 = this.item.contractor.address.address1;
                                this.address2 = this.item.contractor.address.address2;
                                this.city = this.item.contractor.address.city;
                                this.state = this.item.contractor.address.state;
                                this.zip = this.item.contractor.address.zip;
                                if (this.item.contractor.address.country) {
                                    this.country = address.findCountryByCode(this.item.contractor.address.country);
                                    this.states = address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                                        return state.country.value === this.country.value;
                                    }));
                                }
                            }
                            if (this.item.contractor && this.item.contractor.user) {
                                this.user_contractor = this.item.contractor.user;
                            }
                            this.details = this.item.details;
                            this.tags = this.item.tags;
                            this.setUploadParams();
                        }
                        else {
                            this.name = "";
                            this.email = "";
                            this.types = [];
                            this.sub_types = [];
                            this.binder_contractor_types = [];
                            this.binder_contractor_sub_types = [];
                            this.contractor_types = [];
                            this.contractor_sub_types = [];
                            this.send_email_invitation = true;
                            this.phone = {};
                            this.filtered_binder_contractor_sub_types = [];
                            this.filtered_binder_contractor_types = [];
                            this.filtered_sub_types = [];
                            this.filtered_types = [];
                            this.secondary_name = "";
                            this.secondary_email = "";
                            this.secondary_phone = {};
                            this.worked_on_property = this.currentUser.role === "homeowner" ? true : false;
                        }
                    },

                    onSelect: function(item) {
                        if (item) {
                            this.country = item.country;
                        }
                    },

                    searchTypes: function(value) {
                        // don't do the search when no search value is provided just clear the list
                        if (!value || value.length < 3) {
                            this.typeOptions = [];
                            return;
                        }

                        var opts = {
                            page: 1,
                            count: 50,
                            search: value,
                            verified: true
                        };

                        api.contractorType.all(opts).then(
                            angular.bind(this, this.searchComplete),
                            angular.bind(this, this.searchError)
                        );
                    },

                    searchComplete: function(response) {
                        this.typeOptions = response.data.items.filter(angular.bind(this, function(item) {
                            var array = $filter("filter")(this.binder_contractor_types, {
                                type: {
                                    name: item.name
                                }
                            });
                            return array.length < 1;
                        }));
                    },

                    searchError: function(response) {
                        notify.error(response.data);
                    },

                    onTypeSelect: function($select, $item) {
                        // remove the item first and then add back again
                        var index = this.binder_contractor_types.indexOf($item);
                        if (index > -1) {
                            this.binder_contractor_types.splice(index, 1);
                        }

                        var hash = this.findExisting($item);
                        if (hash.index < 0) {
                            if (hash.item.id) {
                                this.binder_contractor_types.push({ type: { id: hash.item.id, name: hash.item.name } });
                                hash.item.sub_types.forEach(angular.bind(this, function(sub_type) {
                                    this.onSubTypeSelect({}, sub_type);
                                }));
                            }
                            else {
                                this.binder_contractor_types.push({ type: { name: hash.item.name } });
                            }
                        }
                    },

                    onTypeRemove: function($item) {
                        if (!this.binder_contractor_types) {
                            this.binder_contractor_types = [];
                        }
                        var hash = this.findExisting($item);
                        if (hash.index > -1) {
                            this.binder_contractor_types.splice(hash.index, 1);
                        }
                    },

                    searchSubTypes: function(value) {
                        // don't do the search when no search value is provided just clear the list
                        if (value === null ||
                            value === undefined ||
                            value.length < 3) {
                            this.subTypeOptions = [];
                            return;
                        }

                        var opts = {
                            page: 1,
                            count: 50,
                            search: value,
                            verified: true
                        };

                        api.contractorSubType.all(opts).then(
                            angular.bind(this, this.searchSubComplete),
                            angular.bind(this, this.searchError)
                        );
                    },

                    searchSubComplete: function(response) {
                        this.subTypeOptions = response.data.items.filter(angular.bind(this, function(item) {
                            var array = $filter("filter")(this.binder_contractor_sub_types, {
                                sub_type: {
                                    name: item.name
                                }
                            });
                            return array.length < 1;
                        }));
                    },

                    onSubTypeSelect: function($select, $item) {
                        //remove the item first and then add back again
                        var index = this.binder_contractor_sub_types.indexOf($item);
                        if (index > -1) {
                            this.binder_contractor_sub_types.splice(index, 1);
                        }

                        var hash = this.findExistingSubTypes($item);
                        if (hash.index < 0) {

                            if (hash.item.id) {
                                this.binder_contractor_sub_types.push({ sub_type: { id: hash.item.id, name: hash.item.name } });
                            }
                            else {
                                this.binder_contractor_sub_types.push({ sub_type: { name: hash.item.name } });
                            }
                        }


                    },

                    onSubTypeRemove: function($item) {
                        var hash = this.findExistingSubTypes($item);
                        if (hash.index > -1) {
                            this.binder_contractor_sub_types.splice(hash.index, 1);
                        }
                    },

                    findExisting: function(item) {
                        var name = null;
                        if (!item.id && !item.isTag) {
                            name = item.type.name.toLowerCase();
                            item = { type: { name: name } };
                        }
                        return { index: this.binder_contractor_types.indexOf(item), item: item };
                    },

                    tagTransform: function(newTag) {
                        var item = {
                            name: newTag,
                            sub_types: []
                        };

                        return item;
                    },

                    findExistingSubTypes: function(item) {
                        var name = null,
                            index = null;
                        if (!item.id && !item.isTag) {
                            name = item.sub_type.name.toLowerCase();
                            item = { sub_type: { name: name } };
                        }

                        var array = $filter("filter")(this.binder_contractor_sub_types, {
                            sub_type: {
                                name: item.name
                            }
                        }, true);

                        if (array.length > 0) {
                            index = 0;
                        }
                        else {
                            index = -1;
                        }

                        return { index: index, item: item };
                    },

                    setPayload: function() {
                        this.filtered_types = this.binder_contractor_types.filter(function(binder_contractor_type) {
                            return !binder_contractor_type.id && !binder_contractor_type.type.id;
                        });

                        this.filtered_binder_contractor_types = this.binder_contractor_types.filter(function(binder_contractor_type) {
                            return binder_contractor_type.id || binder_contractor_type.type.id;
                        });

                        this.filtered_sub_types = this.binder_contractor_sub_types.filter(function(binder_contractor_sub_type) {
                            return !binder_contractor_sub_type.id && !binder_contractor_sub_type.sub_type.id;
                        });

                        this.filtered_binder_contractor_sub_types = this.binder_contractor_sub_types.filter(function(binder_contractor_sub_type) {
                            return binder_contractor_sub_type.id || binder_contractor_sub_type.sub_type.id;
                        });

                        // create payload
                        return {
                            binder_id: this.binderId,
                            account_number: this.account_number,
                            contact: this.contact,
                            secondary_name: this.secondary_name,
                            secondary_email: this.secondary_email,
                            secondary_details: this.secondary_details,
                            secondary_phone: "+" + this.country.code + this.secondary_phone.national,
                            details: this.details,
                            tags_attributes: this.tags,
                            binder_contractor_types_attributes: this.filtered_binder_contractor_types.map(function(binder_contractor_type) {
                                if (binder_contractor_type.id) {
                                    return { id: binder_contractor_type.id };
                                }
                                else if (binder_contractor_type.type.id) {
                                    return { contractor_category_id: binder_contractor_type.type.id };
                                }
                            }),
                            types_attributes: this.filtered_types.map(function(binder_contractor_type) {
                                if (!binder_contractor_type.id && !binder_contractor_type.type.id) {
                                    return { name: binder_contractor_type.type.name };
                                }
                            }),
                            binder_contractor_sub_types_attributes: this.filtered_binder_contractor_sub_types.map(function(binder_contractor_sub_type) {
                                if (binder_contractor_sub_type.id) {
                                    return { id: binder_contractor_sub_type.id };
                                }
                                else if (binder_contractor_sub_type.sub_type.id) {
                                    return { contractor_sub_category_id: binder_contractor_sub_type.sub_type.id };
                                }
                            }),
                            sub_types_attributes: this.filtered_sub_types.map(function(binder_contractor_sub_type) {
                                if (!binder_contractor_sub_type.id && !binder_contractor_sub_type.sub_type.id) {
                                    return { name: binder_contractor_sub_type.sub_type.name };
                                }
                            }),
                            contractor_id: this.contractor_id,
                            worked_on_property: this.worked_on_property,
                            send_email_invitation: this.send_email_invitation
                        };
                    },

                    setUploadParams: function() {
                        this.uploadParams = {
                            binder_id: this.item.binder_id,
                            binder_contractor_id: this.item.id
                        };
                    },

                    checkForExisting: function() {
                        api.contractor.all({
                            count: 5,
                            searchType: "fuzzy",
                            name: this.name.toLowerCase(),
                            email: this.email.toLowerCase(),
                            phone: "+" + this.country.code + this.phone.national
                        }).then(
                            angular.bind(this, this.existingSuccess),
                            angular.bind(this, this.existingError));
                    },

                    existingSuccess: function(response) {
                        var email = this.email ? this.email.toLowerCase() : "";
                        var contractors = response.data.items;
                        if (contractors.length > 0) {
                            HomeProSelect.show({
                                contractors: contractors,
                                email: email,
                                closed: angular.bind(this, this.setHomePro)
                            });
                        }
                        else {
                            this.createOrUpdate();
                        }
                    },

                    existingError: function(response) {
                        $log.error(response.data);
                        notify.error(response.data);
                    },

                    setHomePro: function(pro) {
                        if (pro) {
                            this.name = pro.name;
                            this.email = pro.email;
                            this.phone = pro.phone;
                            this.secondary_name = pro.name;
                            this.secondary_phone = pro.phone;
                            this.secondary_email = pro.email;
                            this.contractor_id = pro.id;
                            this.createOrUpdate();
                        }
                        else {
                            this.createOrUpdate();
                        }
                    },

                    submit: function() {
                        this.form.$submitted = true;
                        if (this.form.$invalid) {
                            return;
                        }

                        if (this.currentUser.email === this.email) {
                            this.notify.info(this.resources.homeownerError);
                            return;
                        }

                        if (this.item && this.item.id) {
                            this.createOrUpdate();
                        }
                        else {
                            // check for existing home pro
                            this.checkForExisting();
                        }
                    },

                    createOrUpdate: function() {
                        // retrieve the payload and
                        // determine whether the item is
                        // verified
                        var data = this.setPayload();
                        this.verify(data);

                        if (!this.contractor_id) {
                            data.contractor_attributes = {
                                name: this.name,
                                phone: "+" + this.country.code + this.phone.national,
                                email: this.email,
                                created_by: this.currentUser.id,
                                url: this.url,
                                contractor_types_attributes: this.filtered_binder_contractor_types.map(function(binder_contractor_type) {
                                    if (binder_contractor_type.id) {
                                        return { id: binder_contractor_type.id };
                                    }
                                    else if (binder_contractor_type.type.id) {
                                        return { contractor_category_id: binder_contractor_type.type.id };
                                    }
                                }),
                                types_attributes: this.filtered_types.map(function(binder_contractor_type) {
                                    if (!binder_contractor_type.id && !binder_contractor_type.type.id) {
                                        return { name: binder_contractor_type.type.name };
                                    }
                                }),
                                contractor_sub_types_attributes: this.filtered_binder_contractor_sub_types.map(function(binder_contractor_sub_type) {
                                    if (binder_contractor_sub_type.id) {
                                        return { id: binder_contractor_sub_type.id };
                                    }
                                    else if (binder_contractor_sub_type.sub_type.id) {
                                        return { contractor_sub_category_id: binder_contractor_sub_type.sub_type.id };
                                    }
                                }),
                                sub_types_attributes: this.filtered_sub_types.map(function(binder_contractor_sub_type) {
                                    if (!binder_contractor_sub_type.id && !binder_contractor_sub_type.sub_type.id) {
                                        return { name: binder_contractor_sub_type.sub_type.name };
                                    }
                                }),
                                address_attributes: {
                                    address1: this.address1,
                                    address2: this.address2,
                                    city: this.city,
                                    state: this.binder.property.state,
                                    country: this.binder.property.country,
                                    zip: this.zip
                                }
                            };
                        }

                        this.loading.show(this.resources.saving);
                        if (this.item && this.item.id) {
                            // add item id and binder id for update
                            data.id = this.item.id;
                            // do the update
                            this.saveCall(this.item.id, data).then(
                                angular.bind(this, this.onItemSaved),
                                angular.bind(this, this.onItemSaveError)
                            );
                        }
                        else {
                            // do the create
                            this.createCall(data).then(
                                angular.bind(this, this.onItemSaved),
                                angular.bind(this, this.onItemSaveError)
                            );
                        }
                    }
                });

                return Model;

            }
        ])
        .controller("HomeBinderBinderContractorFormController", [
            "BinderContractorFormController",
            "$stateParams",
            HomeBinderBinderContractorFormController
        ]);

    function HomeBinderBinderContractorFormController(BinderContractorFormController, $stateParams) {
        var Model = function() {
            BinderContractorFormController.call(this);

            // If $stateParams has an id, we are editing an exisiting item and back should send us to that item's details
            if ($stateParams.id) {
                this.indexState = "binder.contractorsDetails";
                this.indexParams = { binderId: $stateParams.binderId, id: $stateParams.id };
            }
            // Else, we were creating a new item and back should send us to the list of items
            else {
                this.indexState = "binder.contractors";
                this.indexParams = { binderId: $stateParams.binderId };
            }

            this.editState = "binder.contractorsEdit";
        };

        Model.prototype = Object.create(BinderContractorFormController.prototype);

        angular.extend(Model.prototype, {});

        this.model = new Model();
    }

})();