(function() {
	"use strict";

	angular
		.module("hb.homebinders.binderContractors.details", [])
		.directive("hbBinderContractorDetails", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/binderContractors/details/details.tpl.html",
				controller: "BinderContractorsDetailsController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("BinderContractorsDetailsController", [
			"hb.resources",
			"BinderItemsDetailsController",
			"hb.api",
			"$stateParams",
			"AprHomeProRequestForm",
			Controller
		]);

	function Controller(resources, BinderItemsDetailsController, api, $stateParams, AprHomeProRequestForm) {
		var that = this;
		var Model = function() {
			this.cfg = that.cfg;
			this.onDestroyed = that.onDestroyed;
			this.onClose = that.onClose;
			this.resources = angular.extend({}, this.resources, resources.binderContractorsDetails);
			this.nameProperty = "contact";
			this.refreshCall = api.binderContractor.get;
			this.indexState = "binder.contractors";
			this.indexParams = { binderId: $stateParams.binderId };
			this.editState = "binder.contractorsEdit";
			// call the parent class
			BinderItemsDetailsController.call(this);
		};

		Model.prototype = Object.create(BinderItemsDetailsController.prototype);

		angular.extend(Model.prototype, {
			populate: function() {
				this.editParams = { binderId: $stateParams.binderId, id: this.item.id };
				this.tagId = "binder_contractor" + this.item.id;
				this.uploadParams = {
					binder_id: this.item.binder_id,
					binder_contractor_id: this.item.id
				};
			},

			contact: function() {
				AprHomeProRequestForm.display({
					pro: {
						id: this.item.id,
						displayName: this.item.contractor.name,
						phone: this.item.contractor.phone,
						email: this.item.contractor.email
					},
					apr: null,
					requestType: "contact"
				});
			},
			
			deleteCall: function() {
                this.api.binderContractor.destroy(this.item.id).then(
                    angular.bind(this, this.onDeleteItemSuccess)
                    );
            },
                    
            onDeleteItemSuccess: function(response) {
                this.notify.success(this.resources.deleteSuccess);
                this.back();
            }
		});

		this.model = new Model();
	}
})();