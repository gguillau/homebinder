describe("BinderItemsVerifyModalController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        api,
        $log,
        notify,
        $stateParams,
        resources,
        $q,
        $modalInstance,
        BinderItemsVerify,
        ModalService,
        $state;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $q = _$injector_.get("$q");
        $controller = _$controller_;
        api = _$injector_.get("hb.api");
        $log = _$injector_.get("$log");
        notify = _$injector_.get("Notify");
        $stateParams = _$injector_.get("$stateParams");
        resources = _$injector_.get("hb.resources");
        BinderItemsVerify = _$injector_.get("BinderItemsVerify");
        ModalService = _$injector_.get("ModalService");
        $state = _$injector_.get("$state");

        $modalInstance = {
            dismiss: function() {}
        };
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("BinderItemsVerifyModalController", {
            "hb.api": api,
            "$log": $log,
            "Notify": notify,
            "$stateParams": $stateParams,
            "hb.resources": resources,
            "$modalInstance": $modalInstance,
            "$state": $state
        });

        $scope.ctrl = ctrl;
    }

    describe("ctrl.model.refresh", function() {
        it("calls api contractor all", function() {

            spyOn(api.binderItem, "all").and.returnValue($q.when({ data: {} }));

            createController();
            ctrl.model.refresh();

            expect(api.binderItem.all).toHaveBeenCalled();
        });
    });

    describe("ctrl.model.onRefreshSuccess", function() {
        it("sets the items and calls acceptAll", function() {
            $state.go("verify");
            $rootScope.$apply();
            createController();
            $stateParams.token = "token";
            spyOn(ctrl.model, "acceptAll");

            ctrl.model.onRefreshSuccess({ data: { items: [{ id: 1 }] } });

            expect(ctrl.model.items.length).toEqual(1);
            expect(ctrl.model.acceptAll).toHaveBeenCalled();
        });

        it("sets the items and calls declineAll", function() {
            $state.go("decline");
            $rootScope.$apply();
            createController();
            $stateParams.token = "token";
            spyOn(ctrl.model, "declineAll");

            ctrl.model.onRefreshSuccess({ data: { items: [{ id: 1 }] } });

            expect(ctrl.model.items.length).toEqual(1);
            expect(ctrl.model.declineAll).toHaveBeenCalled();
        });
    });

    describe("ctrl.model.onRefreshError", function() {
        it("calls notify error", function() {

            spyOn(notify, "error");

            createController();
            ctrl.model.onRefreshError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("ctrl.model.acceptAll", function() {
        it("calls api binderItem update", function() {
            createController();
            spyOn(ctrl.model, "update");
            ctrl.model.items = [{ id: 1 }];
            ctrl.model.acceptAll();

            expect(ctrl.model.update).toHaveBeenCalled();
        });
    });

    describe("ctrl.model.declineAll", function() {
        it("calls api binderItem update", function() {
            createController();
            spyOn(ctrl.model, "update");
            ctrl.model.items = [{ id: 1 }];
            ctrl.model.declineAll();

            expect(ctrl.model.update).toHaveBeenCalled();
        });
    });

    describe("ctrl.model.update", function() {
        it("calls api binderItem update", function() {

            spyOn(api.binderItem, "update").and.returnValue($q.when({ data: {} }));

            createController();
            ctrl.model.update({ id: 1 }, true, true);

            expect(api.binderItem.update).toHaveBeenCalled();
        });
    });

    describe("ctrl.model.updateSuccess", function() {
        it("removes the item", function() {

            createController();
            ctrl.model.items = [{ id: 1 }];
            ctrl.model.updateSuccess(ctrl.model.items[0], { data: {} });

            expect(ctrl.model.items.length).toEqual(0);
        });
    });

    describe("ctrl.model.updateError", function() {
        it("calls notify error", function() {

            spyOn(notify, "error");

            createController();
            ctrl.model.updateError({ id: 1 }, { data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe('BinderItemsVerify', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            BinderItemsVerify.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });

    function createSecondController() {
        $scope = $rootScope.$new();
        ctrl = $controller("HomeBinderItemsVerifyController", {
            "hb.api": api,
            "$log": $log,
            "Notify": notify,
            "$stateParams": $stateParams,
            "hb.resources": resources,
            "$modalInstance": $modalInstance,
            "$state": $state
        });

        $scope.ctrl = ctrl;
    }

    describe("ctrl.model.refresh", function() {
        it("calls api contractor all", function() {

            spyOn(api.binderItem, "all").and.returnValue($q.when({ data: {} }));

            createSecondController();
            ctrl.model.refresh();

            expect(api.binderItem.all).toHaveBeenCalled();
        });
    });
});