(function() {
    "use strict";

    angular
        .module("hb.homebinders.homebinder.binderItemsVerify", [])
        .factory("BinderItemsVerify", [
            "ModalService",
            function(Modals) {
                return {
                    show: function() {
                        Modals.show({
                            templateUrl: "homebinders/homebinder/binderItemsVerify/binderItemsVerify.tpl.html",
                            controller: "BinderItemsVerifyModalController as ctrl",
                            backdrop: true
                        });
                    }
                };
            }
        ])
        .factory("BinderItemsVerifyController", [
            "hb.framework.indexBase",
            "hb.resources",
            "$stateParams",
            "hb.api",
            "Notify",
            "$state",
            "Loading",
            function(IndexBase, resources, $stateParams, api, Notify, $state, loading) {
                var Model = function() {
                    // call the parent class
                    IndexBase.call(this);
                    this.api = api;
                    this.$stateParams = $stateParams;
                    this.refreshCall = this.api.binderItem.all;
                    this.deleteCall = this.api.binderItem.destroy;
                    this.resources = angular.extend({}, this.resources, resources.binderItemsVerify);
                    this.itemsPerPage = 100;
                    this.queryArgs = {
                        binderId: $stateParams.binderId,
                        verified: false,
                        token: undefined
                    };
                    this.removeItems = true;
                    this.verified = $state.current.name === "verify";
                };

                Model.prototype = Object.create(IndexBase.prototype);

                angular.extend(Model.prototype, {
                    onRefreshSuccess: function(response) {
                        this.items = response.data.items;
                        loading.close();

                        if ($stateParams.token && $state.current.name === "verify") {
                            this.removeItems = false;
                            this.acceptAll();
                        }
                        else if ($stateParams.token && $state.current.name === "decline") {
                            this.removeItems = false;
                            this.declineAll();
                        }
                    },

                    acceptAll: function() {
                        this.items.forEach(angular.bind(this, function(item) {
                            this.update(item, true, false);
                        }));
                    },

                    declineAll: function() {
                        this.items.forEach(angular.bind(this, function(item) {
                            this.update(item, true, true);
                        }));
                    },

                    update: function(item, verified, dismissed) {
                        item.processing = true;
                        var binder_item = {
                            verified: verified,
                            dismissed: dismissed
                        };
                        this.api.binderItem.update(item.id, binder_item, this.queryArgs.token).then(
                            angular.bind(this, this.updateSuccess, item),
                            angular.bind(this, this.updateError, item));
                    },

                    updateSuccess: function(item, response) {
                        if (this.removeItems) {
                            var index = this.items.indexOf(item);
                            this.items.splice(index, 1);
                        }
                    },

                    updateError: function(item, response) {
                        item.processing = false;
                        Notify.error(response.data);
                    }
                });

                return Model;


            }
        ])
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state("verify", {
                    url: "^/binders/:binderId/verify/:token",
                    views: {
                        '@': {
                            templateUrl: "homebinders/homebinder/binderItemsVerify/verify.tpl.html",
                            controller: "HomeBinderItemsVerifyController",
                            controllerAs: "ctrl"
                        }
                    },
                    access: {
                        requiresLogin: false
                    }
                })
                .state("decline", {
                    url: "^/binders/:binderId/decline/:token",
                    views: {
                        '@': {
                            templateUrl: "homebinders/homebinder/binderItemsVerify/verify.tpl.html",
                            controller: "HomeBinderItemsVerifyController",
                            controllerAs: "ctrl"
                        }
                    },
                    access: {
                        requiresLogin: false
                    }
                });
        }])
        .controller("HomeBinderItemsVerifyController", [
            "BinderItemsVerifyController",
            HomeBinderItemsVerifyController
        ])
        .controller("BinderItemsVerifyModalController", [
            "$modalInstance",
            "BinderItemsVerifyController",
            BinderItemsVerifyModalController
        ]);

    function HomeBinderItemsVerifyController(BinderItemsVerifyController) {
        var Model = function() {
            // call the parent class
            BinderItemsVerifyController.call(this);
            this.queryArgs.token = this.$stateParams.token;
            this.refresh();
        };

        Model.prototype = Object.create(BinderItemsVerifyController.prototype);

        this.model = new Model();

    }

    function BinderItemsVerifyModalController($modal, BinderItemsVerifyController) {
        var Model = function() {
            // call the parent class
            BinderItemsVerifyController.call(this);
            this.modal = $modal;
            this.refresh();
        };

        Model.prototype = Object.create(BinderItemsVerifyController.prototype);

        this.model = new Model();
    }

})();