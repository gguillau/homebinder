(function() {
    "use strict";

    angular
        .module("hb.homebinders.images", [
            "ui.router",
            "ui.bootstrap",
            "hb.api",
            "hb.components",
            "hb.homebinders.images"
        ])
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state('binder.images', {
                    url: '^/binders/:binderId/images',
                    controller: "HomeBinderImagesController",
                    controllerAs: "ctrl",
                    templateUrl: 'homebinders/homebinder/images/images.tpl.html',
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .factory("BinderImagesController", [
            "BinderItemsIndexController",
            "hb.resources",
            "HomeBinderFactory",
            "Context",
            "ModalService",
            "$timeout",
            "UpgradeForm",
            function(BinderItemsIndexController, resources, homebinderFactory, Context, modal, $timeout, UpgradeForm) {
                var Model = function() {
                    // call the parent class
                    BinderItemsIndexController.call(this);
                    this.homebinderFactory = homebinderFactory;
                    this.homebinderFactory.setButtons(true);
                    this.resources = angular.extend({}, this.resources, resources.images);
                    this.refreshCall = this.api.image.all;
                    this.deleteCall = this.api.image.destroy;
                    this.uploadCfg = {
                        id: "uploadImages",
                        fileTypes: "image",
                        url: "/api/v1/images",
                        jwt: this.session.getJwt(),
                        params: {
                            binder_id: this.binderId
                        },
                        multiSelect: true,
                        hide_upload: true,
                        showTable: true
                    };
                    this.itemType = "images";
                    this.nameProperty = "name";
                    // doesn't exist
                    this.modalController = "ImagesModalController";
                    this.uploadLimit = true;
                };

                Model.prototype = Object.create(BinderItemsIndexController.prototype);

                angular.extend(Model.prototype, {

                    onRefreshSuccess: function(response) {
                        this.items = response.data.items;
                        this.total = response.data.total;
                        this.toolbarCfg.total = this.total;
                        
                        angular.forEach(this.items, function(i) {
                            if (i.is_hero) {
                                this.hero_photo = i;
                            }
                        }, this);
                        this.toolbarCfg.button = {
                            title: this.resources.upload,
                            click: angular.bind(this, this.uploadImages)
                        };
                        
                        this.loading.close();
                    },

                    onRefreshError: function(response) {
                        this.$log.error(response);
                        this.notify.error(response.data);
                        this.loading.close();
                    },

                    uploadImages: function() {
                        Context.getBinder().then(function(binder) {
                            var number = Math.floor(Math.log(binder.total_storage) / Math.log(1024));
                            var bytes = binder.total_storage / Math.pow(1024, Math.floor(number));
                            if (binder.permissions.can_read && binder.subscription.plan === "free" && bytes > 10) {
                                UpgradeForm.show({
                                    upgradeText: "You have used all 10MB of storage in Renter edition. Please upgrade to increase your storage to 10GB."
                                });
                            }
                            else {
                                angular.element('#uploadImages').trigger('click');
                            }
                        });
                    },

                    fileAdded: function() {
                        this.startUpload();
                    },

                    startUpload: function() {
                        var uploadApi = this.uploadCfg.api;
                        if (uploadApi && uploadApi.pendingUploads().length > 0) {
                            var verified = true;
                            Context.getBinder().then(angular.bind(this, function(binder) {
                                if (!binder.permissions.can_read) {
                                    verified = false;
                                }
                                uploadApi.updateParams({
                                    binder_id: this.binderId,
                                    verified: verified
                                });
                                this.loading.show("Saving...");

                                uploadApi.uploadFiles();
                            }));

                        }
                        else {
                            this.loading.close();
                        }
                    },

                    onFileUploadSuccess: function(file) {
                        Context.getBinder().then(function(binder) {
                            binder.total_storage += file.file_size;
                            Context.setBinder(binder);
                        });

                        if (this.uploadCfg.api.pendingUploads().length < 1) {
                            this.loading.close();
                        }
                        else {
                            this.startUpload();
                        }

                        this.onAdded(file);
                    },

                    onFileUploadError: function(response) {
                        this.$log.error(response);
                        this.notify.error(response.data);
                        this.startUpload();
                    },

                    update: function(img) {
                        modal.confirm({
                            title: this.resources.defaultTitle,
                            message: this.resources.defaultConfirm,
                            glyphicon: "glyphicon glyphicon-picture",
                            confirm: angular.bind(this, this.updateConfirmed, img)
                        });
                    },

                    updateConfirmed: function(img) {
                        img.processing = true;
                        this.api.image.update(img.id, img).then(
                            angular.bind(this, this.onImageUpdateSuccess, img),
                            angular.bind(this, this.onImageUpdateError, img)
                        );
                    },

                    onImageUpdateSuccess: function(item, response) {
                        this.items.forEach(function(item) {
                            if (item.is_hero) {
                                item.is_hero = false;
                            }
                        });
                        var index = this.items.indexOf(item);
                        this.items[index] = response.data;
                        this.notify.success(this.resources.saved);
                    },

                    onImageUpdateError: function(img, response) {
                        img.processing = false;
                        this.$log.error(response);
                        this.notify.error(response.data);

                    },

                    onDeleteItemSuccess: function(item, response) {
                        Context.getBinder().then(function(binder) {
                            binder.total_storage -= item.file_size;
                            Context.setBinder(binder);
                        });
                        var index = this.items.indexOf(item);
                        this.items.splice(index, 1);
                        this.totalItems = this.toolbarCfg.total -= 1;
                        this.notify.success(this.resources.removed);
                        this.loading.close();
                    }

                });

                return Model;

            }
        ])
        .controller("HomeBinderImagesController", [
            "BinderImagesController",
            HomeBinderImagesController
        ]);

    function HomeBinderImagesController(BinderImagesController) {
        var Model = function() {
            // call the parent class
            BinderImagesController.call(this);
            this.init();
            this.toolbarCfg.searchBox = false;
            this.toolbarCfg.subtitle = this.resources.subtitle;
        };

        Model.prototype = Object.create(BinderImagesController.prototype);

        this.model = new Model();
    }
})();