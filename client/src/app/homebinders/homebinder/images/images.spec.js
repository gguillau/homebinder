describe("ImagesController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        session,
        modal,
        homebinderFactory,
        notify,
        api,
        loading,
        $log,
        context,
        $stateParams;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        $stateParams = _$injector_.get("$stateParams");
        loading = _$injector_.get("Loading");
        $log = _$injector_.get("$log");
        api = _$injector_.get("hb.api");
        notify = _$injector_.get("Notify");
        context = _$injector_.get("Context");
        session = _$injector_.get("Session");
        modal = _$injector_.get("ModalService");
    }));

    function createController() {
        $stateParams.binderId = 100;

        ctrl = $rootScope.$new();
        ctrl = $controller("HomeBinderImagesController", {
            "$scope": $scope,
            "hb.api": api,
            "Session": session,
            "Notify": notify,
            "HomeBinderFactory": homebinderFactory,
            "ModalService": modal,
            "Loading": loading,
            "$log": $log,
            "Context": context
        });
    }

    describe('ctrl.model.refresh', function() {
        it("should load all images for binder/resource", function() {
            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1
            }));

            var images = [{
                binder: null,
                file_size: 439531,
                is_hero: false,
                id: 562,
                location: "https://somerandomlink.com/image.jpg",
                name: "Screen_Shot_2015-08-01_at_8.01.45_PM.png",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null,
                type: "image"
            }, {
                binder: null,
                file_size: 439531,
                is_hero: true,
                id: 562,
                location: "https://somerandomlink.com/image.jpg",
                name: "Screen_Shot_2015-08-01_at_8.01.45_PM.png",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null,
                type: "image"
            }];

            spyOn(api.image, "all").and.returnValue($q.when({
                data: { items: images }
            }));

            var image = {
                binder: null,
                file_size: 439531,
                is_hero: true,
                id: 562,
                location: "https://somerandomlink.com/image.jpg",
                name: "Screen_Shot_2015-08-01_at_8.01.45_PM.png",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null,
                type: "image"
            };
            createController();
            $rootScope.$apply();
            expect(ctrl.model.items.length).toBe(2);
            expect(ctrl.model.hero_photo).toEqual(jasmine.objectContaining(image));
        });
        it("should load all images for binder/resource", function() {
            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1
            }));

            spyOn(api.image, "all").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            $rootScope.$apply();
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");
        });
    });

    describe('ctrl.model.onRefreshSuccess', function() {
        it("sets empty array of images", function() {
            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1
            }));

            spyOn(api.image, "all").and.returnValue($q.reject({
                data: "error"
            }));

            createController();
            ctrl.model.onRefreshSuccess({ data: { items: [] } });
            expect(ctrl.model.items.length).toBe(0);
        });
    });

    describe('ctrl.model.uploadImages', function() {
        it("opens the file window", function() {

            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1,
                subscription: {
                    plan: "paid"
                },
                permissions: {
                    can_read: true
                }
            }));

            spyOn(api.image, "all").and.returnValue($q.reject({
                data: "error"
            }));

            createController();
            ctrl.model.uploadImages();
        });
    });

    describe('ctrl.model.fileAdded', function() {
        it("calls startUpload", function() {
            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1,
                subscription: {
                    plan: "paid"
                },
                permissions: {
                    can_read: true
                }
            }));

            spyOn(api.image, "all").and.returnValue($q.reject({
                data: "error"
            }));

            createController();
            spyOn(ctrl.model, "startUpload");
            ctrl.model.fileAdded();

            expect(ctrl.model.startUpload).toHaveBeenCalled();
        });
    });

    describe('ctrl.model.startUpload', function() {
        it("calls uploadFiles", function() {
            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1,
                subscription: {
                    plan: "paid"
                },
                permissions: {
                    can_read: false
                }
            }));

            spyOn(api.image, "all").and.returnValue($q.reject({
                data: "error"
            }));

            createController();
            ctrl.model.uploadCfg = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            spyOn(ctrl.model.uploadCfg.api, "pendingUploads").and.returnValue([{ id: 1 }]);
            spyOn(ctrl.model.uploadCfg.api, "updateParams");
            spyOn(ctrl.model.uploadCfg.api, "uploadFiles");
            ctrl.model.startUpload();
            $rootScope.$apply();

            expect(ctrl.model.uploadCfg.api.pendingUploads).toHaveBeenCalled();
            expect(ctrl.model.uploadCfg.api.updateParams).toHaveBeenCalled();
            expect(ctrl.model.uploadCfg.api.uploadFiles).toHaveBeenCalled();
        });

        it("calls uploadFiles", function() {
            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1,
                subscription: {
                    plan: "paid"
                },
                permissions: {
                    can_read: false
                }
            }));

            spyOn(api.image, "all").and.returnValue($q.reject({
                data: "error"
            }));

            createController();
            ctrl.model.uploadCfg = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            spyOn(ctrl.model.uploadCfg.api, "pendingUploads").and.returnValue([]);
            spyOn(ctrl.model.uploadCfg.api, "updateParams");
            spyOn(ctrl.model.uploadCfg.api, "uploadFiles");
            ctrl.model.startUpload();

            expect(ctrl.model.uploadCfg.api.pendingUploads).toHaveBeenCalled();
            expect(ctrl.model.uploadCfg.api.updateParams).not.toHaveBeenCalled();
            expect(ctrl.model.uploadCfg.api.uploadFiles).not.toHaveBeenCalled();
        });
    });

    describe('ctrl.model.onFileUploadSuccess', function() {
        it("should add new image to array", function() {
            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1,
                subscription: {
                    plan: "paid"
                },
                permissions: {
                    can_read: true
                }
            }));

            spyOn(api.image, "all").and.returnValue($q.reject({
                data: { items: "error" }
            }));

            var file = {
                binder: null,
                file_size: 439531,
                is_hero: null,
                id: 562,
                location: "https://somerandomlink.com/image.jpg",
                name: "genericfilename.png",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null,
                type: "image"
            };
            createController();
            ctrl.model.binder = {
                permissions: {
                    can_read: true
                }
            };
            ctrl.model.uploadCfg = {
                api: {
                    pendingUploads: function() { return []; }
                }
            };
            ctrl.model.onFileUploadSuccess(file);
            expect(ctrl.model.items.length).toEqual(1);
        });

        it("should add new image to array and call startUpload", function() {
            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1,
                subscription: {
                    plan: "paid"
                },
                permissions: {
                    can_read: true
                }
            }));

            spyOn(api.image, "all").and.returnValue($q.reject({
                data: { items: "error" }
            }));

            var file = {
                binder: null,
                file_size: 439531,
                is_hero: null,
                id: 562,
                location: "https://somerandomlink.com/image.jpg",
                name: "genericfilename.png",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null,
                type: "image"
            };
            createController();
            ctrl.model.binder = {
                permissions: {
                    can_read: true
                }
            };
            spyOn(ctrl.model, "startUpload");
            ctrl.model.uploadCfg = {
                api: {
                    pendingUploads: function() { return [{}]; }
                }
            };
            ctrl.model.onFileUploadSuccess(file);

            expect(ctrl.model.items.length).toEqual(1);
            expect(ctrl.model.startUpload).toHaveBeenCalled();
        });

    });

    describe('ctrl.model.onFileUploadError', function() {
        it("should call notify and log error functions", function() {

            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1,
                subscription: {
                    plan: "paid"
                },
                permissions: {
                    can_read: true
                }
            }));

            var images = [{
                binder: null,
                file_size: 439531,
                is_hero: false,
                id: 562,
                location: "https://somerandomlink.com/image.jpg",
                name: "Screen_Shot_2015-08-01_at_8.01.45_PM.png",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null,
                type: "image"
            }, {
                binder: null,
                file_size: 439531,
                is_hero: true,
                id: 562,
                location: "https://somerandomlink.com/image.jpg",
                name: "Screen_Shot_2015-08-01_at_8.01.45_PM.png",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null,
                type: "image"
            }];

            spyOn(api.image, "all").and.returnValue($q.when({
                data: { items: images }
            }));

            var user = {
                email: "somerandomuseremail@gmail.com",
                role: "homeowner",
                user_token: "abunchofrandomletters"
            };

            spyOn(session, "getUser").and.returnValue($q.when(user));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            $rootScope.$apply();
            ctrl.model.onFileUploadError({
                data: "error"
            });
            $rootScope.$apply();
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");

        });
    });

    describe('ctrl.model.update', function() {
        it("should update call modal confirm", inject(function(ModalService) {

            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1,
                subscription: {
                    plan: "paid"
                },
                permissions: {
                    can_read: true
                }
            }));

            var images = [{
                binder: null,
                file_size: 439531,
                is_hero: false,
                id: 562,
                location: "https://somerandomlink.com/image.jpg",
                name: "Screen_Shot_2015-08-01_at_8.01.45_PM.png",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null,
                type: "image"
            }, {
                binder: null,
                file_size: 439531,
                is_hero: true,
                id: 562,
                location: "https://somerandomlink.com/image.jpg",
                name: "Screen_Shot_2015-08-01_at_8.01.45_PM.png",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null,
                type: "image"
            }];

            spyOn(api.image, "all").and.returnValue($q.when({
                data: { items: images }
            }));

            var user = {
                email: "somerandomuseremail@gmail.com",
                role: "homeowner",
                user_token: "abunchofrandomletters"
            };

            spyOn(session, "getUser").and.returnValue($q.when(user));

            var image = {
                binder: null,
                file_size: 439531,
                is_hero: null,
                id: 562,
                location: "https://somerandomlink.com/image.jpg",
                name: "genericfilename.png",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null,
                type: "image"
            };
            createController();
            spyOn(ModalService, "confirm");
            ctrl.model.update(image);
            expect(ModalService.confirm).toHaveBeenCalled();
        }));

    });

    describe('ctrl.model.updateConfirmed', function() {
        it("should succesfully call img update function", function() {

            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1,
                subscription: {
                    plan: "paid"
                },
                permissions: {
                    can_read: true
                }
            }));

            var images = [{
                binder: null,
                file_size: 439531,
                is_hero: false,
                id: 562,
                location: "https://somerandomlink.com/image.jpg",
                name: "Screen_Shot_2015-08-01_at_8.01.45_PM.png",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null,
                type: "image"
            }, {
                binder: null,
                file_size: 439531,
                is_hero: true,
                id: 562,
                location: "https://somerandomlink.com/image.jpg",
                name: "Screen_Shot_2015-08-01_at_8.01.45_PM.png",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null,
                type: "image"
            }];

            spyOn(api.image, "all").and.returnValue($q.when({
                data: { items: images }
            }));

            var user = {
                email: "somerandomuseremail@gmail.com",
                role: "homeowner",
                user_token: "abunchofrandomletters"
            };

            spyOn(session, "getUser").and.returnValue($q.when(user));

            var image = {
                binder: null,
                file_size: 439531,
                is_hero: null,
                id: 562,
                location: "https://somerandomlink.com/image.jpg",
                name: "genericfilename.png",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null,
                type: "image"
            };

            spyOn(api.image, "update").and.returnValue($q.when({
                data: image
            }));
            spyOn(notify, "success");
            createController();
            ctrl.model.updateConfirmed(image);
            spyOn(ctrl.model, "refresh");
            $rootScope.$apply();
            expect(notify.success).toHaveBeenCalled();
        });

        it("should call img update function and return an error", function() {

            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1,
                subscription: {
                    plan: "paid"
                },
                permissions: {
                    can_read: true
                }
            }));

            var images = [{
                binder: null,
                file_size: 439531,
                is_hero: false,
                id: 562,
                location: "https://somerandomlink.com/image.jpg",
                name: "Screen_Shot_2015-08-01_at_8.01.45_PM.png",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null,
                type: "image"
            }, {
                binder: null,
                file_size: 439531,
                is_hero: true,
                id: 562,
                location: "https://somerandomlink.com/image.jpg",
                name: "Screen_Shot_2015-08-01_at_8.01.45_PM.png",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null,
                type: "image"
            }];

            spyOn(api.image, "all").and.returnValue($q.when({
                data: { items: images }
            }));

            var user = {
                email: "somerandomuseremail@gmail.com",
                role: "homeowner",
                user_token: "abunchofrandomletters"
            };

            spyOn(session, "getUser").and.returnValue($q.when(user));

            var image = {
                binder: null,
                file_size: 439531,
                is_hero: null,
                id: 562,
                location: "https://somerandomlink.com/image.jpg",
                name: "genericfilename.png",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null,
                type: "image"
            };

            spyOn(api.image, "update").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            ctrl.model.updateConfirmed(image);
            $rootScope.$apply();
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");
        });

    });

    describe('ctrl.model.onDeleteItemSuccess', function() {
        it("should remove the image from the array", function() {
            spyOn(context, "getBinder").and.returnValue($q.when({
                id: 1,
                subscription: {
                    plan: "paid"
                },
                permissions: {
                    can_read: true
                }
            }));

            spyOn(api.image, "all").and.returnValue($q.reject({
                data: { items: "error" }
            }));

            var file = {
                binder: null,
                file_size: 439531,
                is_hero: null,
                id: 562,
                location: "https://somerandomlink.com/image.jpg",
                name: "genericfilename.png",
                permissions: {
                    can_read: true,
                    can_write: true,
                    can_create: false,
                    can_destroy: true
                },
                seller_report_item: null,
                tags: null,
                type: "image"
            };
            createController();
            ctrl.model.items = [file];
            ctrl.model.onDeleteItemSuccess(file, {});
            expect(ctrl.model.items.length).toEqual(0);
        });
    });

});