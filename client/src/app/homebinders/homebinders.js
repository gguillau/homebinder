(function() {
    "use strict";

    angular
        .module("hb.homebinders", [
            "ui.bootstrap",
            "hb.components",
            "hb.homebinders.homebinder",
            "hb.homebinders.form"
        ])
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state("binders", {
                    url: "^/binders",
                    template: "<homebinders></homebinders>",
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .directive("homebinders", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinders.tpl.html",
                controller: "HomeBindersController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("HomeBindersController", [
            "$state",
            "$log",
            "hb.api",
            "Session",
            "ModalService",
            "Notify",
            "Loading",
            "hb.resources",
            HomeBindersController
        ]);

    function HomeBindersController($state, $log, api, session, modals, notify, loading, resources) {
        this.$state = $state;
        this.$log = $log;
        this.api = api;
        this.session = session;
        this.user = session.getUser();
        this.modals = modals;
        this.notify = notify;
        this.loading = loading;
        this.resources = resources.homebinders;
        this.binders = null;
        this.refresh();
    }

    HomeBindersController.prototype = {

        refresh: function() {
            this.loading.show(this.resources.loading + "...");
            this.api.binder.all({ searchMethod: "for_user", userId: this.user.id }).then(
                angular.bind(this, this.onRefresh),
                angular.bind(this, this.onRefreshError));
        },

        onRefresh: function(response) {
            this.binders = response.data.items;
            for (var i = 0; i < this.binders.length; i++) {
                if (this.binders[i].primary === true) {
                    this.binders.move(i, 0);
                }
            }
            this.loading.close();
        },

        onRefreshError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.loading.close();
        },

        clickBinder: function(binder) {
            this.$state.go("binder.overview", {
                binderId: binder.id
            });
        },

        deleteBinder: function(binder) {
            this.modals.confirm({
                glyphicon: "glyphicon glyphicon-trash",
                message: this.resources.deleteConfirm,
                confirm: angular.bind(this, this.deleteBinderConfirmed, binder)
            });
        },

        deleteBinderConfirmed: function(binder) {
            this.loading.show(this.resources.removing + "...");
            this.api.binder.destroy(binder.id).then(
                angular.bind(this, this.onBinderDeleted, binder),
                angular.bind(this, this.onDeleteError));
        },

        onBinderDeleted: function(binder, response) {
            var index = this.binders.indexOf(binder);
            this.binders.splice(index, 1);
            this.notify.success(this.resources.removed);
            this.loading.close();
        },

        onDeleteError: function(response) {
            this.notify.error(response.data);
            this.$log.error(response);
            this.loading.close();
        }

    };

})();