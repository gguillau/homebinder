(function() {
	"use strict";

	angular
		.module("hb.utils.exceptions", [])
		.factory("$exceptionHandler", ["$log", function($log) {
			return function myExceptionHandler(exception, cause) {
				$log.warn(exception, cause);
				if (window.Sentry) {
					window.Sentry.captureException(exception);
				}
			};
		}]);
})();