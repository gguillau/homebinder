describe("ServiceKeyList", function() {
    'use strict';

    var ServiceKeyList, $httpBackend, $rootScope;

    beforeEach(function() {
        module("homebinder");

        inject(function(_$injector_) {
            ServiceKeyList = _$injector_.get("ServiceKeyList");
            $httpBackend = _$injector_.get("$httpBackend");
            $rootScope = _$injector_.get("$rootScope");
        });
    });

    it("gets the keys and then gets the keys again after they've been loaded", function() {
        var respond;
        
        respond = { data: {stripe: "Fake_Publishable_key" }};

        $httpBackend.expectGET("/api/v1/service_keys/").respond(respond);
        
        // call the function
        ServiceKeyList.keys().then(function(response){
            expect(response).toEqual(respond);
        });
        
        $httpBackend.flush();
        
        // call the function
        ServiceKeyList.keys().then(function(response){
            expect(response).toEqual(respond);
        });
        
        $rootScope.$apply();
    });
});