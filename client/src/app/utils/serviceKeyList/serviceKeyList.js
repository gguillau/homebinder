angular
    .module("hb.utils.serviceKeyList", [])
    .factory("ServiceKeyList", [
        '$q',
        function($q) {
            var ServiceKeyList = function() {
                this.apiFunction = null;
                this.serviceKeys = {};
                this.loaded = false;
            };

            angular.extend(ServiceKeyList.prototype, {
                init: function(apiFunction) {
                    this.apiFunction = apiFunction;
                },
                keys: function() {
                    if (this.loaded) {
                        // if we already have the keys return them wrapped in a promise
                        var d = $q.defer();
                        d.resolve(this.serviceKeys);
                        return d.promise;
                    }
                    else {
                        var that = this;
                        // make the api call to get the keys
                        return this.apiFunction().then(
                            function(response) {
                                var prop;
                                for (prop in response.data) {
                                    that.serviceKeys[prop] = response.data[prop];
                                }
                                that.loaded = true;
                                return that.serviceKeys;
                            });
                    }
                }
            });
            return new ServiceKeyList();
        }
    ]);
