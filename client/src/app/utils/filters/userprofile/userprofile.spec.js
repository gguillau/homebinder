describe("hb.utils.filters.userprofile", function() {
    'use strict';

    var $filter;

    beforeEach(function() {
        module("hb.utils.filters.userprofile");

        inject(function(_$filter_) {
            $filter = _$filter_;
        });
    });

    it("should return empty string", function() {
        var result;

        // call the filter
        result = $filter("userprofile")(null);

        // verify filter changes
        expect(result).toEqual("");
    });

    it("should return Mike - mike@gmail.com", function() {
        var user = { email: "mike@gmail.com", user_profile_attributes: { first_name: "Mike" } },
            result;

        // call the filter
        result = $filter("userprofile")(user);

        // verify filter changes
        expect(result).toEqual("Mike - mike@gmail.com");
    });

    it("should return Mike Smith", function() {
        var user = { email: "mike@gmail.com", user_profile_attributes: { first_name: "Mike", last_name: "Smith" } },
            result;

        // call the filter
        result = $filter("userprofile")(user);

        // verify filter changes
        expect(result).toEqual("Mike Smith");
    });

});