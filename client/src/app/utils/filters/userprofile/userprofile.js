(function() {
    "use strict";

    angular
        .module("hb.utils.filters.userprofile", [])
        .filter("userprofile", function() {
            return function(user) {
                if (user && user.user_profile_attributes) {
                    var name = "";
                    if (user.user_profile_attributes.first_name) {
                        name = name + user.user_profile_attributes.first_name;
                    }
                    if (user.user_profile_attributes.last_name) {
                        name = name + " " + user.user_profile_attributes.last_name;
                    }
                    if (!user.user_profile_attributes.last_name) {
                        name = name + " - " + user.email;
                    }
                    return name;
                }else{
                    return "";
                }
            };
        });
})();