describe("hb.utils.filters.priority", function() {
    'use strict';

    var $filter;

    beforeEach(function() {
        module("hb.utils.filters.priority");

        inject(function(_$filter_) {
            $filter = _$filter_;
        });
    });

    it("should return To Be Determined", function() {
        var value = "tbd",
            result;

        // call the filter
        result = $filter("priority")(value);

        // verify filter changes
        expect(result).toEqual("To Be Determined");
    });

    it("should return Cosmetic", function() {
        var value = "cosmetic",
            result;

        // call the filter
        result = $filter("priority")(value);

        // verify filter changes
        expect(result).toEqual("Cosmetic");
    });

    it("should return Low", function() {
        var value = "low",
            result;

        // call the filter
        result = $filter("priority")(value);

        // verify filter changes
        expect(result).toEqual("Low");
    });

    it("should return High", function() {
        var value = "high",
            result;

        // call the filter
        result = $filter("priority")(value);

        // verify filter changes
        expect(result).toEqual("High");
    });

    it("should return Critical", function() {
        var value = "critical",
            result;

        // call the filter
        result = $filter("priority")(value);

        // verify filter changes
        expect(result).toEqual("Critical");
    });

});