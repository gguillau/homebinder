(function() {
    "use strict";

    angular
        .module("hb.utils.filters.priority", [])
        .filter("priority", function() {
            return function(value) {
                switch (value) {
                    case "tbd":
                        return "To Be Determined";
                    case "cosmetic":
                        return "Cosmetic";
                    case "low":
                        return "Low";
                    case "high":
                        return "High";
                    case "critical":
                        return "Critical";
                }
            };
        });
})();