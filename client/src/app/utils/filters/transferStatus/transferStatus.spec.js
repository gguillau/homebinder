describe("hb.utils.filters.transferStatus", function() {
    'use strict';

    var $filter;

    beforeEach(function() {
        module("hb.utils.filters.transferStatus");

        inject(function(_$filter_) {
            $filter = _$filter_;
        });
    });

    it("should return not sent", function() {

        var status = "created",
            result;

        // call the filter
        result = $filter("transferStatus")(status);

        // verify filter changes
        expect(result).toEqual("Not Sent");
    });

    it("should return not sent", function() {

        var status = null,
            result;

        // call the filter
        result = $filter("transferStatus")(status);

        // verify filter changes
        expect(result).toEqual("Not Sent");
    });

    it("should return Transferred", function() {

        var status = "sent",
            result;

        // call the filter
        result = $filter("transferStatus")(status);

        // verify filter changes
        expect(result).toEqual("Transferred");
    });

    it("should return Transferred", function() {

        var status = "delivery_failed",
            result;

        // call the filter
        result = $filter("transferStatus")(status);

        // verify filter changes
        expect(result).toEqual("Transferred");
    });

    it("should return Transferred", function() {

        var status = "declined",
            result;

        // call the filter
        result = $filter("transferStatus")(status);

        // verify filter changes
        expect(result).toEqual("Transferred");
    });

    it("should return Transferred", function() {

        var status = "didnt_buy",
            result;

        // call the filter
        result = $filter("transferStatus")(status);

        // verify filter changes
        expect(result).toEqual("Transferred");
    });

    it("should return Transferred", function() {

        var status = "accepted",
            result;

        // call the filter
        result = $filter("transferStatus")(status);

        // verify filter changes
        expect(result).toEqual("Transferred");
    });

    it("should return shared", function() {

        var status = "shared",
            result;

        // call the filter
        result = $filter("transferStatus")(status);

        // verify filter changes
        expect(result).toEqual("Shared");
    });
});