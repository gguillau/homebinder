(function() {
    "use strict";
    
	angular
        .module("hb.utils.filters.transferStatus", [])
        .filter("transferStatus", function(){
            return function(status) {
                switch (status) {
                    case "created":
                        return "Not Sent";
                    case "sent":
                    case "delivery_failed":
                    case "declined":
                    case "didnt_buy":
                    case "accepted":
                    case "pending":
                        return "Transferred";
                    case "shared":
                        return "Shared";
                    default:
                        return "Not Sent";
                }
            };
        });
})();