(function(){
    "use strict";
    
    angular
        .module("hb.utils.filters", [
            "hb.utils.filters.branding",
            "hb.utils.filters.bytes",
            "hb.utils.filters.icon",
            "hb.utils.filters.role",
            "hb.utils.filters.recallStatus",
            "hb.utils.filters.subscriptionLabel",
            "hb.utils.filters.tel",
            "hb.utils.filters.titleize",
            "hb.utils.filters.transferStatus",
            "hb.utils.filters.url",
            "hb.utils.filters.yesNo",
            "hb.utils.filters.userprofile",
            "hb.utils.filters.partnerWarrantyPlan",
            "hb.utils.filters.enum",
            "hb.utils.filters.priority",
            "hb.utils.filters.agentNotifications"
        ]);
})();