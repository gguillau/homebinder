describe("hb.utils.filters.recallStatus", function() {
    'use strict';

    var $filter;

    beforeEach(function() {
        module("hb.utils.filters.recallStatus");

        inject(function(_$filter_) {
            $filter = _$filter_;
        });
    });

    it("should return Viewed", function() {
        var recall = { status: "viewed" },
            result;

        // call the filter
        result = $filter("recallStatus")(recall);

        // verify filter changes
        expect(result).toEqual("Viewed");
    });

    it("should return Replaced", function() {
        var recall = { status: "replaced" },
            result;

        // call the filter
        result = $filter("recallStatus")(recall);

        // verify filter changes
        expect(result).toEqual("Replaced");
    });

    it("should return Repaired", function() {
        var recall = { status: "repaired" },
            result;

        // call the filter
        result = $filter("recallStatus")(recall);

        // verify filter changes
        expect(result).toEqual("Repaired");
    });

    it("should return ''", function() {
        var recall = { status: "nope" },
            result;

        // call the filter
        result = $filter("recallStatus")(recall);

        // verify filter changes
        expect(result).toEqual("");
    });

});