(function() {
    "use strict";
    
	angular
        .module("hb.utils.filters.recallStatus", [])
        .filter("recallStatus", function(){
            return function(recall) {
                switch(recall.status) {
                    case "viewed":
                        return "Viewed";
                    case "replaced":
                        return "Replaced";
                    case "repaired":
                        return "Repaired";
                    default:
                        return "";
                }
            };
        });
})();