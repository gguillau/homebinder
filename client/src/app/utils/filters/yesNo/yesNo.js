(function() {
    "use strict";
    
    angular
        .module("hb.utils.filters.yesNo", [])
        .filter("yesNo", function(){
            return function(value) {
                return value === true ? "\u2713" : "No";
            };
         });
})();