describe("hb.utils.filters.yesNo", function() {
    'use strict';

    var $filter;

    beforeEach(function() {
        module("hb.utils.filters.yesNo");

        inject(function(_$filter_) {
            $filter = _$filter_;
        });
    });

    it("should return '✓'", function() {
        var result;

        // call the filter
        result = $filter("yesNo")(true);

        // verify filter changes
        expect(result).toEqual("✓");
    });

    it("should return No", function() {
        var result;

        // call the filter
        result = $filter("yesNo")(false);

        // verify filter changes
        expect(result).toEqual("No");
    });

});