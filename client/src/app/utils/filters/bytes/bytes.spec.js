describe("hb.utils.filters.bytes", function() {
    'use strict';

    var $filter;

    beforeEach(function() {
        module("hb.utils.filters.bytes");
        
        inject(function(_$filter_) {
            $filter = _$filter_;
        });
    });

    it("should return 1.0 KB", function() {
        var size = 1024,
            result;

        // call the filter
        result = $filter("bytes")(size);

        // verify filter changes
        expect(result).toEqual("1.0 KB");
    });

    it("should return -", function() {
        var size = "",
            result;

        // call the filter
        result = $filter("bytes")(size);

        // verify filter changes
        expect(result).toEqual("-");
    });

});