(function() {
    "use strict";

    angular
        .module("hb.utils.filters.tel", [])
        .filter("tel", function() {
            return function(phoneNumber) {
                if (!phoneNumber) {
                    return phoneNumber;
                }
                else if (phoneNumber.indexOf("+") !== -1) {
                    return formatLocal("US", phoneNumber.substring(2, phoneNumber.length));
                }
                return formatLocal("US", phoneNumber);
            };
        });
})();