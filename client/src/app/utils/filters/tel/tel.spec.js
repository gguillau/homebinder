describe("hb.utils.filters.tel", function() {
    'use strict';

    var $filter;

    beforeEach(function() {
        module("hb.utils.filters.tel");

        inject(function(_$filter_) {
            $filter = _$filter_;
        });
    });

    it("should return empty string", function() {
        var result;

        // call the filter
        result = $filter("tel")(null);

        // verify filter changes
        expect(result).toEqual(null);
    });

    it("should return (617) 500-2500", function() {
        var result;

        // call the filter
        result = $filter("tel")("+16175002500");

        // verify filter changes
        expect(result).toEqual("(617) 500-2500");
    });
    
    it("should return (617) 500-2500", function() {
        var result;

        // call the filter
        result = $filter("tel")("6175002500");

        // verify filter changes
        expect(result).toEqual("(617) 500-2500");
    });
    
});