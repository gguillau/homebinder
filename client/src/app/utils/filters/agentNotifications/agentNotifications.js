(function() {
    "use strict";

    angular
        .module("hb.utils.filters.agentNotifications", [])
        .filter("agentNotifications", ["$filter", function($filter) {
            return function(agentNotification) {
                switch (agentNotification) {
                    case "always":
                        return "All binders";
                    case "once":
                        return "Only the first binder";
                    default:
                        return "Never";
                }
            };
        }]);
})();