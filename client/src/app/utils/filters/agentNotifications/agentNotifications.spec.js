describe("hb.utils.filters.agentNotifications", function() {
    'use strict';

    var $filter;

    beforeEach(function() {
        module("hb.utils.filters.agentNotifications");

        inject(function(_$filter_) {
            $filter = _$filter_;
        });
    });

    it("should return Never", function() {
        var status = "never",
            result;

        // call the filter
        result = $filter("agentNotifications")(status);

        // verify filter changes
        expect(result).toEqual("Never");
    });

    it("should return Only for the first binder", function() {
        var status = "once",
            result;

        // call the filter
        result = $filter("agentNotifications")(status);

        // verify filter changes
        expect(result).toEqual("Only the first binder");
    });

    it("should return All binders", function() {
        var status = "always",
            result;

        // call the filter
        result = $filter("agentNotifications")(status);

        // verify filter changes
        expect(result).toEqual("All binders");
    });

});