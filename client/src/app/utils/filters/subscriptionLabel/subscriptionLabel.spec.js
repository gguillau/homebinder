describe("hb.utils.filters.subscriptionLabel", function() {
    'use strict';

    var $filter;

    beforeEach(function() {
        module("hb.utils.filters.subscriptionLabel");

        inject(function(_$filter_) {
            $filter = _$filter_;
        });
    });

    it("should return Renter", function() {
        var result;

        // call the filter
        result = $filter("subscriptionLabel")("free", "home");

        // verify filter changes
        expect(result).toEqual("Renter");
    });

    it("should return free", function() {
        var result;

        // call the filter
        result = $filter("subscriptionLabel")("free", "partners");

        // verify filter changes
        expect(result).toEqual("free");
    });
    
});