(function() {
    "use strict";
    
    angular
        .module("hb.utils.filters.subscriptionLabel", [])
        .filter("subscriptionLabel", function(){
            return function(id, site) {
                switch(site){
                    case "home":
                        return id == "free" ? "Renter" : "Homeowner";
                    //case "auto":
                    //case "boat":
                    default:
                        return id;
                }
            };
        });
})();