(function() {
    "use strict";
    
    // checks if url begins with https:// or http:// and appends it if it doesn't
	angular
        .module("hb.utils.filters.url", [])
        .filter("url", function(){
            return function(url) {
                var formattedUrl = "";
                if (!/^http[s]?:\/\//.test(url)) {
                    formattedUrl += 'http://';
                }
                formattedUrl += url;
                return formattedUrl;
            };
        });
})();