describe("hb.utils.filters.url", function() {
    'use strict';

    var $filter;

    beforeEach(function() {
        module("hb.utils.filters.url");

        inject(function(_$filter_) {
            $filter = _$filter_;
        });
    });

    it("should return the url with http:// appended", function() {
        var url = "www.homebinder.com";
        var result = $filter("url")(url);
        expect(result).toEqual("http://www.homebinder.com");
    });
    
    it("should return the url unchanged (starts with http://)", function() {
        var url = "http://www.homebinder.com";
        var result = $filter("url")(url);
        expect(result).toEqual("http://www.homebinder.com");
    });
    
    it("should return the url unchanged (starts with https://)", function() {
        var url = "https://www.homebinder.com";
        var result = $filter("url")(url);
        expect(result).toEqual("https://www.homebinder.com");
    });
});