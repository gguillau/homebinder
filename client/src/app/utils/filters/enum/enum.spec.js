describe("hb.utils.filters.enum", function() {
    'use strict';

    var $filter;

    beforeEach(function() {
        module("hb.utils.filters.enum");

        inject(function(_$filter_) {
            $filter = _$filter_;
        });
    });

    it("should return Less than 1 year", function() {
        var status = "less_one",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("Less than 1 year");
    });

    it("should return 1 to 2 years", function() {
        var status = "one_to_two",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("1 to 2 years");
    });

    it("should return 2 to 3 years", function() {
        var status = "two_to_three",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("2 to 3 years");
    });

    it("should return 3 to 5 years", function() {
        var status = "three_to_five",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("3 to 5 years");
    });

    it("should return 5 to 10 years", function() {
        var status = "five_to_ten",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("5 to 10 years");
    });

    it("should return 10 to 15 years", function() {
        var status = "ten_to_fifteen",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("10 to 15 years");
    });

    it("should return 15 to 20 years", function() {
        var status = "fifteen_to_twenty",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("15 to 20 years");
    });

    it("should return 20+ years", function() {
        var status = "twenty_plus",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("20+ years");
    });

    it("should return Unknown", function() {
        var status = "unknown",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("Unknown");
    });

    it("should return TBD by Home Professional", function() {
        var status = "tbd",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("TBD by Home Professional");
    });

    it("should return None", function() {
        var status = "none",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("None");
    });

    it("should return N/A", function() {
        var status = "not_applicable",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("N/A");
    });

    it("should return Under $100", function() {
        var status = "less_hun",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("Under $100");
    });

    it("should return $100 to $250", function() {
        var status = "one_hun_to_two_fity",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("$100 to $250");
    });

    it("should return $100 to $250", function() {
        var status = "one_hun_to_two_fity",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("$100 to $250");
    });

    it("should return $250 to $500", function() {
        var status = "two_fifty_to_five_hun",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("$250 to $500");
    });

    it("should return Under $500", function() {
        var status = "less_five_hun",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("Under $500");
    });

    it("should return $500 - $1,000", function() {
        var status = "five_hun_to_one_thou",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("$500 - $1,000");
    });

    it("should return $1,000 to $2,000", function() {
        var status = "one_thou_to_twenty_hun",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("$1,000 to $2,000");
    });

    it("should return $1,000 to $2,500", function() {
        var status = "one_thou_to_twentyfive_hun",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("$1,000 to $2,500");
    });

    it("should return $2,000 - $5,000", function() {
        var status = "twenty_hun_to_five_thou",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("$2,000 - $5,000");
    });

    it("should return $2,500 - $5,000", function() {
        var status = "twentyfive_hun_to_five_thou",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("$2,500 - $5,000");
    });

    it("should return $5,000+", function() {
        var status = "five_thou_plus",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("$5,000+");
    });

    it("should return $5,000 to $10,000", function() {
        var status = "five_thou_to_ten_thou",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("$5,000 to $10,000");
    });

    it("should return $10,000 - $15,000", function() {
        var status = "ten_thou_to_fifteen_thou",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("$10,000 - $15,000");
    });

    it("should return $15,000 - $20,000", function() {
        var status = "fifteen_thou_to_twenty_thou",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("$15,000 - $20,000");
    });

    it("should return $20,000+", function() {
        var status = "twenty_thou_plus",
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("$20,000+");
    });

    it("should return None", function() {
        var status = null,
            result;

        // call the filter
        result = $filter("enum")(status);

        // verify filter changes
        expect(result).toEqual("None");
    });
});