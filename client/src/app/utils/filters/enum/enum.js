(function() {
    "use strict";

    angular
        .module("hb.utils.filters.enum", [])
        .filter("enum", function() {
            return function(value) {
                switch (value) {
                    case "less_one":
                        return "Less than 1 year";
                    case "one_to_two":
                        return "1 to 2 years";
                    case "two_to_three":
                        return "2 to 3 years";
                    case "three_to_five":
                        return "3 to 5 years";
                    case "five_to_ten":
                        return "5 to 10 years";
                    case "ten_to_fifteen":
                        return "10 to 15 years";
                    case "fifteen_to_twenty":
                        return "15 to 20 years";
                    case "twenty_plus":
                        return "20+ years";
                    case "unknown":
                        return "Unknown";
                    case "tbd":
                        return "TBD by Home Professional";
                    case "none":
                        return "None";
                    case "not_applicable":
                        return "N/A";
                    case "less_hun":
                        return "Under $100";
                    case "one_hun_to_two_fity":
                        return "$100 to $250";
                    case "two_fifty_to_five_hun":
                        return "$250 to $500";
                    case "less_five_hun":
                        return "Under $500";
                    case "five_hun_to_one_thou":
                        return "$500 - $1,000";
                    case "one_thou_to_twenty_hun":
                        return "$1,000 to $2,000";
                    case "one_thou_to_twentyfive_hun":
                        return "$1,000 to $2,500";
                    case "twenty_hun_to_five_thou":
                        return "$2,000 - $5,000";
                    case "twentyfive_hun_to_five_thou":
                        return "$2,500 - $5,000";
                    case "five_thou_plus":
                        return "$5,000+";
                    case "five_thou_to_ten_thou":
                        return "$5,000 to $10,000";
                    case "ten_thou_to_fifteen_thou":
                        return "$10,000 - $15,000";
                    case "fifteen_thou_to_twenty_thou":
                        return "$15,000 - $20,000";
                    case "twenty_thou_plus":
                        return "$20,000+";
                    default:
                        return "None";
                }
            };
        });
})();