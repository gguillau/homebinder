describe("hb.utils.filters.role", function() {
    'use strict';

    var $filter;

    beforeEach(function() {
        module("hb.utils.filters.role");

        inject(function(_$filter_) {
            $filter = _$filter_;
        });
    });

    it("should return Owner", function() {
        var role = "owner",
            result;

        // call the filter
        result = $filter("role")(role);

        // verify filter changes
        expect(result).toEqual("Owner");
    });

    it("should return Co-Owner", function() {
        var role = "co_owner",
            result;

        // call the filter
        result = $filter("role")(role);

        // verify filter changes
        expect(result).toEqual("Co-Owner");
    });

    it("should return Viewer", function() {
        var role = "reader",
            result;

        // call the filter
        result = $filter("role")(role);

        // verify filter changes
        expect(result).toEqual("Viewer");
    });

    it("should return Writer", function() {
        var role = "writer",
            result;

        // call the filter
        result = $filter("role")(role);

        // verify filter changes
        expect(result).toEqual("Writer");
    });

    it("should return role", function() {
        var role = "role",
            result;

        // call the filter
        result = $filter("role")(role);

        // verify filter changes
        expect(result).toEqual("role");
    });

});