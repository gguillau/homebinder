(function() {
    "use strict";
    
    angular
        .module("hb.utils.filters.role", [])
        .filter("role", function() {
            return function(role_name) {
                switch(role_name) {
                    case "owner":
                        return "Owner";
                    case "co_owner":
                        return "Co-Owner";
                    case "reader":
                        return "Viewer";
                    case "writer":
                        return "Writer";
                    default:
                        return role_name;
                }
            };
        });
})();