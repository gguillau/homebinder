describe("hb.utils.filters.branding", function() {
    'use strict';

    var $filter;

    beforeEach(function() {
        module("hb.utils.filters.branding");
        module("hb.utils.filters.titleize");

        inject(function(_$filter_) {
            $filter = _$filter_;
        });
    });

    it("should return No Branding", function() {
        var status = "none",
            result;

        // call the filter
        result = $filter("branding")(status);

        // verify filter changes
        expect(result).toEqual("No Branding");
    });

    it("should return Co-Branding", function() {
        var status = "co_brand",
            result;

        // call the filter
        result = $filter("branding")(status);

        // verify filter changes
        expect(result).toEqual("Co-Branding");
    });

    it("should return Broker", function() {
        var status = "user",
            result;

        // call the filter
        result = $filter("branding")(status, "broker");

        // verify filter changes
        expect(result).toEqual("Broker");
    });

    it("should return Agent", function() {
        var status = "agent",
            result;

        // call the filter
        result = $filter("branding")(status);

        // verify filter changes
        expect(result).toEqual("Agent");
    });

    it("should return No Branding", function() {
        var status = null,
            result;

        // call the filter
        result = $filter("branding")(status);

        // verify filter changes
        expect(result).toEqual("No Branding");
    });

});