(function() {
    "use strict";

    angular
        .module("hb.utils.filters.branding", [])
        .filter("branding", ["$filter", function($filter) {
            return function(branding, type) {
                var partner_type = $filter("titleize")(type);
                switch (branding) {
                    case "none":
                        return "No Branding";
                    case "co_brand":
                        return "Co-Branding";
                    case "user":
                        return partner_type;
                    case "agent":
                        return "Agent";
                    default:
                        return "No Branding";
                }
            };
        }]);
})();