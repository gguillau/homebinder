describe("hb.utils.filters.partnerWarrantyPlan", function() {
    'use strict';

    var $filter;

    beforeEach(function() {
        module("hb.utils.filters.partnerWarrantyPlan");
        
        inject(function(_$filter_) {
            $filter = _$filter_;
        });
    });

    it("should return 30 Day Warranty - $33.00", function() {
        var plan = {plan: {name: "30 Day Warranty"}, price: "33"},
            result;

        // call the filter
        result = $filter("partnerWarrantyPlan")(plan);

        // verify filter changes
        expect(result).toEqual("30 Day Warranty - $33.00");
    });

});