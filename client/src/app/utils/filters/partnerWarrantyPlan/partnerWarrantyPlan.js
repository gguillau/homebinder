(function() {
    "use strict";

    angular
        .module("hb.utils.filters.partnerWarrantyPlan", [])
        .filter("partnerWarrantyPlan", ["$filter", function($filter) {
            return function(plan) {
                return plan.plan.name + " - " + $filter("currency")(plan.price);
            };
        }]);
})();