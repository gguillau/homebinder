describe("hb.utils.filters.titleize", function() {
    'use strict';

    var $filter;

    beforeEach(function() {
        module("hb.utils.filters.titleize");

        inject(function(_$filter_) {
            $filter = _$filter_;
        });
    });

    it("should return Owner", function() {
        var string = "owner",
            result;

        // call the filter
        result = $filter("titleize")(string);

        // verify filter changes
        expect(result).toEqual("Owner");
    });
    
    it("should return Test String", function() {
        var string = "test string",
            result;

        // call the filter
        result = $filter("titleize")(string);

        // verify filter changes
        expect(result).toEqual("Test String");
    });
    
    it("should return Test String", function() {
        var string = ["test string"],
            result;

        // call the filter
        result = $filter("titleize")(string);

        // verify filter changes
        expect(result).toEqual(["Test String"]);
    });
    
    it("should return 1", function() {
        var value = 1,
            result;

        // call the filter
        result = $filter("titleize")(value);

        // verify filter changes
        expect(result).toEqual(1);
    });

});