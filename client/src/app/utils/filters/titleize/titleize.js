(function() {
    "use strict";

    angular
        .module("hb.utils.filters.titleize", [])
        .filter("titleize", function() {
            return function(value) {

                function titleize(value) {
                    var result = "";
                    for (var k = 0; k < value.length; k++) {
                        if (k === 0) {
                            result += value.charAt(k).toUpperCase();
                        }
                        else if (value[k - 1] === " ") {
                            result += value.charAt(k).toUpperCase();
                        }
                        else {
                            result += value.charAt(k);
                        }
                    }
                    return result;
                }

                if (angular.isString(value)) {
                    return titleize(value);
                }
                else if (angular.isArray(value)) {
                    for (var k = 0; k < value.length; k++) {
                        value[k] = titleize(value[k]);
                    }
                    return value;
                }
                else {
                    return value;
                }
            };
        });
})();