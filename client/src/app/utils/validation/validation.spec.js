describe("Validation", function() {
    'use strict';

    var Validation;

    beforeEach(function() {
        module("hb.utils.validation");

        inject(function(_$injector_) {
            Validation = _$injector_.get("Validation");
        });
    });

    it("returns true", function() {
        var result;

        // call the function
        result = Validation.isEmpty();

        // verify results
        expect(result).toBe(true);
    });

    it("returns true", function() {
        var result;

        // call the function
        result = Validation.isEmpty("");

        // verify results
        expect(result).toBe(true);
    });

    it("returns false", function() {
        var result;

        // call the function
        result = Validation.isEmpty("test");

        // verify results
        expect(result).toBe(false);
    });
});