(function() {
	"use strict";

	angular
		.module("hb.utils.validation", [])
		.factory("Validation", [
			function() {
				return {
					isEmpty: function(value) {
						if (value === undefined || value === null) {
							return true;
						}

						if (angular.isString(value) && value.length === 0) {
							return true;
						}

						return false;
					}
				};
			}
		]);
})();