(function() {
    "use strict";

    angular
        .module("hb.utils.filters.icon", [])
        .filter("icon", function() {
            return function(name) {
                var file_extension = name.substring(name.length - 3);

                switch (file_extension) {
                    case "pdf":
                        return "fa fa-file-pdf-o text-primary";
                    case "doc":
                    case "ocx":
                        return "fa fa-file-word-o text-primary";
                    case "png":
                    case "jpg":
                    case "gif":
                    case "peg":
                        return "fa fa-file-image-o text-primary";
                    default:
                        return "fa fa-file text-primary";
                }

            };
        });
})();