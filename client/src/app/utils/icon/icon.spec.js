describe("hb.utils.filters.icon", function() {
    'use strict';

    var $filter;

    beforeEach(function() {
        module("hb.utils.filters.icon");

        inject(function(_$filter_) {
            $filter = _$filter_;
        });
    });

    it("should return fa fa-file-pdf-o text-primary", function() {
        var name = "test.pdf",
            result;

        // call the filter
        result = $filter("icon")(name);

        // verify filter changes
        expect(result).toEqual("fa fa-file-pdf-o text-primary");
    });

    it("should return fa fa-file-word-o text-primary", function() {
        var name = "test.doc",
            result;

        // call the filter
        result = $filter("icon")(name);

        // verify filter changes
        expect(result).toEqual("fa fa-file-word-o text-primary");
    });

    it("should return fa fa-file-image-o text-primary", function() {
        var name = "test.png",
            result;

        // call the filter
        result = $filter("icon")(name);

        // verify filter changes
        expect(result).toEqual("fa fa-file-image-o text-primary");
    });

    it("should return fa fa-file text-primary", function() {
        var name = "test.zip",
            result;

        // call the filter
        result = $filter("icon")(name);

        // verify filter changes
        expect(result).toEqual("fa fa-file text-primary");
    });

});