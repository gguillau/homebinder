angular.module("hb.utils.enums.userRole", [])
.factory('hb.userRole', [
    function() {
        return {
          ADMIN: "admin",
          HOMEOWNER: "homeowner",
          PARTNER: "partner",
          INSPECTOR: "inspector",
          BROKER: "broker",
          AGENT: "agent",
          REVIEWER: "apr_reviewer"
        };
}]);