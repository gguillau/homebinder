describe("hb.userRole", function() {
    'use strict';

    var userRole;

    beforeEach(function() {
        module("hb.utils.enums.userRole");

        inject(function(_$injector_) {
            userRole = _$injector_.get("hb.userRole");
        });
    });

    it("returns correct roles", function() {
        expect(userRole.ADMIN).toEqual("admin");
        expect(userRole.HOMEOWNER).toEqual("homeowner");
        expect(userRole.PARTNER).toEqual("partner");
        expect(userRole.INSPECTOR).toEqual("inspector");
        expect(userRole.BROKER).toEqual("broker");
        expect(userRole.AGENT).toEqual("agent");
        expect(userRole.REVIEWER).toEqual("apr_reviewer");
    });
    
});