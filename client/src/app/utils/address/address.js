(function() {
	"use strict";

	angular
		.module("hb.utils.address", [])
		.factory('Address', [
			"hb.resources",
			function(resources) {
				var countries = [{
					name: resources.countries.usa,
					value: 'US',
					code: 1,
					phone_format: "999-999-9999"
				}, {
					name: resources.countries.ca,
					value: "CA",
					code: 1,
					phone_format: "999-999-9999"
				}, {
					name: resources.countries.aus,
					value: "AS",
					code: 61,
					phone_format: "99-9999-9999"
				}, {
					name: resources.countries.uk,
					value: "GB",
					code: 44,
					phone_format: "9999-999999"
				}, {
					name: resources.countries.ir,
					value: "IE",
					code: 353,
					phone_format: "999-999-9999"
				}];
				var provinces = [{
					name: "Alberta",
					value: "AB",
					country: countries[1]
				}, {
					name: "British Columbia",
					value: "BC",
					country: countries[1]
				}, {
					name: "Manitoba",
					value: "MB",
					country: countries[1]
				}, {
					name: "New Brunswick",
					value: "NB",
					country: countries[1]
				}, {
					name: "Newfoundland and Labrador",
					value: "NL",
					country: countries[1]
				}, {
					name: "Northwest Territories",
					value: "NT",
					country: countries[1]
				}, {
					name: "Nova Scotia",
					value: "NS",
					country: countries[1]
				}, {
					name: "Nunavut",
					value: "NU",
					country: countries[1]
				}, {
					name: "Ontario",
					value: "ON",
					country: countries[1]
				}, {
					name: "Prince Edward Island",
					value: "PE",
					country: countries[1]
				}, {
					name: "Quebec",
					value: "QC",
					country: countries[1]
				}, {
					name: "Saskatchewan",
					value: "SK",
					country: countries[1]
				}, {
					name: "Yukon",
					value: "YT",
					country: countries[1]
				}];
				var us_states = [{
					name: 'Alabama',
					value: 'AL',
					country: countries[0]
				}, {
					name: 'Alaska',
					value: 'AK',
					country: countries[0]
				}, {
					name: 'Arizona',
					value: 'AZ',
					country: countries[0]
				}, {
					name: 'Arkansas',
					value: 'AR',
					country: countries[0]
				}, {
					name: 'California',
					value: 'CA',
					country: countries[0]
				}, {
					name: 'Colorado',
					value: 'CO',
					country: countries[0]
				}, {
					name: 'Connecticut',
					value: 'CT',
					country: countries[0]
				}, {
					name: 'Delaware',
					value: 'DE',
					country: countries[0]
				}, {
					name: 'District of Columbia',
					value: 'DC',
					country: countries[0]
				}, {
					name: 'Florida',
					value: 'FL',
					country: countries[0]
				}, {
					name: 'Georgia',
					value: 'GA',
					country: countries[0]
				}, {
					name: 'Hawaii',
					value: 'HI',
					country: countries[0]
				}, {
					name: 'Idaho',
					value: 'ID',
					country: countries[0]
				}, {
					name: 'Illinois',
					value: 'IL',
					country: countries[0]
				}, {
					name: 'Indiana',
					value: 'IN',
					country: countries[0]
				}, {
					name: 'Iowa',
					value: 'IA',
					country: countries[0]
				}, {
					name: 'Kansas',
					value: 'KS',
					country: countries[0]
				}, {
					name: 'Kentucky',
					value: 'KY',
					country: countries[0]
				}, {
					name: 'Louisiana',
					value: 'LA',
					country: countries[0]
				}, {
					name: 'Maine',
					value: 'ME',
					country: countries[0]
				}, {
					name: 'Maryland',
					value: 'MD',
					country: countries[0]
				}, {
					name: 'Massachusetts',
					value: 'MA',
					country: countries[0]
				}, {
					name: 'Michigan',
					value: 'MI',
					country: countries[0]
				}, {
					name: 'Minnesota',
					value: 'MN',
					country: countries[0]
				}, {
					name: 'Mississippi',
					value: 'MS',
					country: countries[0]
				}, {
					name: 'Missouri',
					value: 'MO',
					country: countries[0]
				}, {
					name: 'Montana',
					value: 'MT',
					country: countries[0]
				}, {
					name: 'Nebraska',
					value: 'NE',
					country: countries[0]
				}, {
					name: 'Nevada',
					value: 'NV',
					country: countries[0]
				}, {
					name: 'New Hampshire',
					value: 'NH',
					country: countries[0]
				}, {
					name: 'New Jersey',
					value: 'NJ',
					country: countries[0]
				}, {
					name: 'New Mexico',
					value: 'NM',
					country: countries[0]
				}, {
					name: 'New York',
					value: 'NY',
					country: countries[0]
				}, {
					name: 'North Carolina',
					value: 'NC',
					country: countries[0]
				}, {
					name: 'North Dakota',
					value: 'ND',
					country: countries[0]
				}, {
					name: 'Ohio',
					value: 'OH',
					country: countries[0]
				}, {
					name: 'Oklahoma',
					value: 'OK',
					country: countries[0]
				}, {
					name: 'Oregon',
					value: 'OR',
					country: countries[0]
				}, {
					name: 'Pennsylvania',
					value: 'PA',
					country: countries[0]
				}, {
					name: 'Puerto Rico',
					value: 'PR',
					country: countries[0]
				}, {
					name: 'Rhode Island',
					value: 'RI',
					country: countries[0]
				}, {
					name: 'South Carolina',
					value: 'SC',
					country: countries[0]
				}, {
					name: 'South Dakota',
					value: 'SD',
					country: countries[0]
				}, {
					name: 'Tennessee',
					value: 'TN',
					country: countries[0]
				}, {
					name: 'Texas',
					value: 'TX',
					country: countries[0]
				}, {
					name: 'Utah',
					value: 'UT',
					country: countries[0]
				}, {
					name: 'Vermont',
					value: 'VT',
					country: countries[0]
				}, {
					name: 'Virginia',
					value: 'VA',
					country: countries[0]
				}, {
					name: "Virgin Islands, U.S.",
					value: "VI",
					country: countries[0]
				}, {
					name: 'Washington',
					value: 'WA',
					country: countries[0]
				}, {
					name: 'West Virginia',
					value: 'WV',
					country: countries[0]
				}, {
					name: 'Wisconsin',
					value: 'WI',
					country: countries[0]
				}, {
					name: 'Wyoming',
					value: 'WY',
					country: countries[0]
				}];
				var australian_states = [
					{ name: "Australian Capital Territory", value: "ACT", country: countries[2] },
					{ name: "New South Wales", value: "NSW", country: countries[2] },
					{ name: "Northern Territory", value: "NT", country: countries[2] },
					{ name: "Queensland", value: "QLD", country: countries[2] },
					{ name: "South Australia", value: "SA", country: countries[2] },
					{ name: "Tasmania", value: "TAS", country: countries[2] },
					{ name: "Victoria", value: "VIC", country: countries[2] },
					{ name: "Western Australia", value: "WAS", country: countries[2] }
				];
				var uk_states = [
					{ value: "ABD", name: "Aberdeenshire", country: countries[3] },
					{ value: "ABE", name: "Aberdeen City", country: countries[3] },
					{ value: "AGB", name: "Argyll and Bute", country: countries[3] },
					{ value: "AGY", name: "Isle of Anglesey", country: countries[3] },
					{ value: "ANS", name: "Angus", country: countries[3] },
					{ value: "ANT", name: "Antrim", country: countries[3] },
					{ value: "ARD", name: "Ards", country: countries[3] },
					{ value: "ARM", name: "Armagh", country: countries[3] },
					{ value: "BAS", name: "Bath and North East Somerset", country: countries[3] },
					{ value: "BBD", name: "Blackburn with Darwen", country: countries[3] },
					{ value: "BDF", name: "Bedfordshire", country: countries[3] },
					{ value: "BDG", name: "Barking and Dagenham", country: countries[3] },
					{ value: "BEN", name: "Brent", country: countries[3] },
					{ value: "BEX", name: "Bexley", country: countries[3] },
					{ value: "BFS", name: "Belfast", country: countries[3] },
					{ value: "BGE", name: "Bridgend", country: countries[3] },
					{ value: "BGW", name: "Blaenau Gwent", country: countries[3] },
					{ value: "BIR", name: "Birmingham", country: countries[3] },
					{ value: "BKM", name: "Buckinghamshire", country: countries[3] },
					{ value: "BLA", name: "Ballymena", country: countries[3] },
					{ value: "BLY", name: "Ballymoney", country: countries[3] },
					{ value: "BMH", name: "Bournemouth", country: countries[3] },
					{ value: "BNB", name: "Banbridge", country: countries[3] },
					{ value: "BNE", name: "Barnet", country: countries[3] },
					{ value: "BNH", name: "Brighton and Hove", country: countries[3] },
					{ value: "BNS", name: "Barnsley", country: countries[3] },
					{ value: "BOL", name: "Bolton", country: countries[3] },
					{ value: "BPL", name: "Blackpool", country: countries[3] },
					{ value: "BRC", name: "Bracknell Forest", country: countries[3] },
					{ value: "BRD", name: "Bradford", country: countries[3] },
					{ value: "BRY", name: "Bromley", country: countries[3] },
					{ value: "BST", name: "Bristol, City of", country: countries[3] },
					{ value: "BUR", name: "Bury", country: countries[3] },
					{ value: "CAM", name: "Cambridgeshire", country: countries[3] },
					{ value: "CAY", name: "Caerphilly", country: countries[3] },
					{ value: "CGN", name: "Ceredigion", country: countries[3] },
					{ value: "CGV", name: "Craigavon", country: countries[3] },
					{ value: "CHS", name: "Cheshire", country: countries[3] },
					{ value: "CKF", name: "Carrickfergus", country: countries[3] },
					{ value: "CKT", name: "Cookstown", country: countries[3] },
					{ value: "CLD", name: "Calderdale", country: countries[3] },
					{ value: "CLK", name: "Clackmannanshire", country: countries[3] },
					{ value: "CLR", name: "Coleraine", country: countries[3] },
					{ value: "CMA", name: "Cumbria", country: countries[3] },
					{ value: "CMD", name: "Camden", country: countries[3] },
					{ value: "CMN", name: "Carmarthenshire", country: countries[3] },
					{ value: "CON", name: "Cornwall", country: countries[3] },
					{ value: "COV", name: "Coventry", country: countries[3] },
					{ value: "CRF", name: "Cardiff", country: countries[3] },
					{ value: "CRY", name: "Croydon", country: countries[3] },
					{ value: "CSR", name: "Castlereagh", country: countries[3] },
					{ value: "CWY", name: "Conwy", country: countries[3] },
					{ value: "DAL", name: "Darlington", country: countries[3] },
					{ value: "DBY", name: "Derbyshire", country: countries[3] },
					{ value: "DEN", name: "Denbighshire", country: countries[3] },
					{ value: "DER", name: "Derby", country: countries[3] },
					{ value: "DEV", name: "Devon", country: countries[3] },
					{ value: "DGN", name: "Dungannon", country: countries[3] },
					{ value: "DGY", name: "Dumfries and Galloway", country: countries[3] },
					{ value: "DNC", name: "Doncaster", country: countries[3] },
					{ value: "DND", name: "Dundee City", country: countries[3] },
					{ value: "DOR", name: "Dorset", country: countries[3] },
					{ value: "DOW", name: "Down", country: countries[3] },
					{ value: "DRY", name: "Derry", country: countries[3] },
					{ value: "DUD", name: "Dudley", country: countries[3] },
					{ value: "DUR", name: "Durham", country: countries[3] },
					{ value: "EAL", name: "Ealing", country: countries[3] },
					{ value: "EAY", name: "East Ayrshire", country: countries[3] },
					{ value: "EDH", name: "Edinburgh, City of", country: countries[3] },
					{ value: "EDU", name: "East Dunbartonshire", country: countries[3] },
					{ value: "ELN", name: "East Lothian", country: countries[3] },
					{ value: "ELS", name: "Eilean Siar", country: countries[3] },
					{ value: "ENF", name: "Enfield", country: countries[3] },
					{ value: "ERW", name: "East Renfrewshire", country: countries[3] },
					{ value: "ERY", name: "East Riding of Yorkshire", country: countries[3] },
					{ value: "ESS", name: "Essex", country: countries[3] },
					{ value: "ESX", name: "East Sussex", country: countries[3] },
					{ value: "FAL", name: "Falkirk", country: countries[3] },
					{ value: "FER", name: "Fermanagh", country: countries[3] },
					{ value: "FIF", name: "Fife", country: countries[3] },
					{ value: "FLN", name: "Flintshire", country: countries[3] },
					{ value: "GAT", name: "Gateshead", country: countries[3] },
					{ value: "GLG", name: "Glasgow City", country: countries[3] },
					{ value: "GLS", name: "Gloucestershire", country: countries[3] },
					{ value: "GRE", name: "Greenwich", country: countries[3] },
					{ value: "GWN", name: "Gwynedd", country: countries[3] },
					{ value: "HAL", name: "Halton", country: countries[3] },
					{ value: "HAM", name: "Hampshire", country: countries[3] },
					{ value: "HAV", name: "Havering", country: countries[3] },
					{ value: "HCK", name: "Hackney", country: countries[3] },
					{ value: "HEF", name: "Herefordshire, County of", country: countries[3] },
					{ value: "HIL", name: "Hillingdon", country: countries[3] },
					{ value: "HLD", name: "Highland", country: countries[3] },
					{ value: "HMF", name: "Hammersmith and Fulham", country: countries[3] },
					{ value: "HNS", name: "Hounslow", country: countries[3] },
					{ value: "HPL", name: "Hartlepool", country: countries[3] },
					{ value: "HRT", name: "Hertfordshire", country: countries[3] },
					{ value: "HRW", name: "Harrow", country: countries[3] },
					{ value: "HRY", name: "Haringey", country: countries[3] },
					{ value: "IOS", name: "Isles of Scilly", country: countries[3] },
					{ value: "IOW", name: "Isle of Wight", country: countries[3] },
					{ value: "ISL", name: "Islington", country: countries[3] },
					{ value: "IVC", name: "Inverclyde", country: countries[3] },
					{ value: "KEC", name: "Kensington and Chelsea", country: countries[3] },
					{ value: "KEN", name: "Kent", country: countries[3] },
					{ value: "KHL", name: "Kingston upon Hull, City of", country: countries[3] },
					{ value: "KIR", name: "Kirklees", country: countries[3] },
					{ value: "KTT", name: "Kingston upon Thames", country: countries[3] },
					{ value: "KWL", name: "Knowsley", country: countries[3] },
					{ value: "LAN", name: "Lancashire", country: countries[3] },
					{ value: "LBH", name: "Lambeth", country: countries[3] },
					{ value: "LCE", name: "Leicester", country: countries[3] },
					{ value: "LDS", name: "Leeds", country: countries[3] },
					{ value: "LEC", name: "Leicestershire", country: countries[3] },
					{ value: "LEW", name: "Lewisham", country: countries[3] },
					{ value: "LIN", name: "Lincolnshire", country: countries[3] },
					{ value: "LIV", name: "Liverpool", country: countries[3] },
					{ value: "LMV", name: "Limavady", country: countries[3] },
					{ value: "LND", name: "London, City of", country: countries[3] },
					{ value: "LRN", name: "Larne", country: countries[3] },
					{ value: "LSB", name: "Lisburn", country: countries[3] },
					{ value: "LUT", name: "Luton", country: countries[3] },
					{ value: "MAN", name: "Manchester", country: countries[3] },
					{ value: "MDB", name: "Middlesbrough", country: countries[3] },
					{ value: "MDW", name: "Medway", country: countries[3] },
					{ value: "MFT", name: "Magherafelt", country: countries[3] },
					{ value: "MIK", name: "Milton Keynes", country: countries[3] },
					{ value: "MLN", name: "Midlothian", country: countries[3] },
					{ value: "MON", name: "Monmouthshire", country: countries[3] },
					{ value: "MRT", name: "Merton", country: countries[3] },
					{ value: "MRY", name: "Moray", country: countries[3] },
					{ value: "MTY", name: "Merthyr Tydfil", country: countries[3] },
					{ value: "MYL", name: "Moyle", country: countries[3] },
					{ value: "NAY", name: "North Ayrshire", country: countries[3] },
					{ value: "NBL", name: "Northumberland", country: countries[3] },
					{ value: "NDN", name: "North Down", country: countries[3] },
					{ value: "NEL", name: "North East Lincolnshire", country: countries[3] },
					{ value: "NET", name: "Newcastle upon Tyne", country: countries[3] },
					{ value: "NFK", name: "Norfolk", country: countries[3] },
					{ value: "NGM", name: "Nottingham", country: countries[3] },
					{ value: "NLK", name: "North Lanarkshire", country: countries[3] },
					{ value: "NLN", name: "North Lincolnshire", country: countries[3] },
					{ value: "NSM", name: "North Somerset", country: countries[3] },
					{ value: "NTA", name: "Newtownabbey", country: countries[3] },
					{ value: "NTH", name: "Northamptonshire", country: countries[3] },
					{ value: "NTL", name: "Neath Port Talbot", country: countries[3] },
					{ value: "NTT", name: "Nottinghamshire", country: countries[3] },
					{ value: "NTY", name: "North Tyneside", country: countries[3] },
					{ value: "NWM", name: "Newham", country: countries[3] },
					{ value: "NWP", name: "Newport [Casnewydd GB-CNW]", country: countries[3] },
					{ value: "NYK", name: "North Yorkshire", country: countries[3] },
					{ value: "NYM", name: "Newry and Mourne", country: countries[3] },
					{ value: "OLD", name: "Oldham", country: countries[3] },
					{ value: "OMH", name: "Omagh", country: countries[3] },
					{ value: "ORK", name: "Orkney Islands", country: countries[3] },
					{ value: "OXF", name: "Oxfordshire", country: countries[3] },
					{ value: "PEM", name: "Pembrokeshire", country: countries[3] },
					{ value: "PKN", name: "Perth and Kinross", country: countries[3] },
					{ value: "PLY", name: "Plymouth", country: countries[3] },
					{ value: "POL", name: "Poole", country: countries[3] },
					{ value: "POR", name: "Portsmouth", country: countries[3] },
					{ value: "POW", name: "Powys", country: countries[3] },
					{ value: "PTE", name: "Peterborough", country: countries[3] },
					{ value: "RCC", name: "Redcar and Cleveland", country: countries[3] },
					{ value: "RCH", name: "Rochdale", country: countries[3] },
					{ value: "RCT", name: "Rhondda, Cynon, Taff", country: countries[3] },
					{ value: "RDB", name: "Redbridge", country: countries[3] },
					{ value: "RDG", name: "Reading", country: countries[3] },
					{ value: "RFW", name: "Renfrewshire", country: countries[3] },
					{ value: "RIC", name: "Richmond upon Thames", country: countries[3] },
					{ value: "ROT", name: "Rotherham", country: countries[3] },
					{ value: "RUT", name: "Rutland", country: countries[3] },
					{ value: "SAW", name: "Sandwell", country: countries[3] },
					{ value: "SAY", name: "South Ayrshire", country: countries[3] },
					{ value: "SCB", name: "Scottish Borders, The", country: countries[3] },
					{ value: "SFK", name: "Suffolk", country: countries[3] },
					{ value: "SFT", name: "Sefton", country: countries[3] },
					{ value: "SGC", name: "South Gloucestershire", country: countries[3] },
					{ value: "SHF", name: "Sheffield", country: countries[3] },
					{ value: "SHN", name: "St. Helens", country: countries[3] },
					{ value: "SHR", name: "Shropshire", country: countries[3] },
					{ value: "SKP", name: "Stockport", country: countries[3] },
					{ value: "SLF", name: "Salford", country: countries[3] },
					{ value: "SLG", name: "Slough", country: countries[3] },
					{ value: "SLK", name: "South Lanarkshire", country: countries[3] },
					{ value: "SND", name: "Sunderland", country: countries[3] },
					{ value: "SOL", name: "Solihull", country: countries[3] },
					{ value: "SOM", name: "Somerset", country: countries[3] },
					{ value: "SOS", name: "Southend-on-Sea", country: countries[3] },
					{ value: "SRY", name: "Surrey", country: countries[3] },
					{ value: "STB", name: "Strabane", country: countries[3] },
					{ value: "STE", name: "Stoke-on-Trent", country: countries[3] },
					{ value: "STG", name: "Stirling", country: countries[3] },
					{ value: "STH", name: "Southampton", country: countries[3] },
					{ value: "STN", name: "Sutton", country: countries[3] },
					{ value: "STS", name: "Staffordshire", country: countries[3] },
					{ value: "STT", name: "Stockton-on-Tees", country: countries[3] },
					{ value: "STY", name: "South Tyneside", country: countries[3] },
					{ value: "SWA", name: "Swansea", country: countries[3] },
					{ value: "SWD", name: "Swindon", country: countries[3] },
					{ value: "SWK", name: "Southwark", country: countries[3] },
					{ value: "TAM", name: "Tameside", country: countries[3] },
					{ value: "TFW", name: "Telford and Wrekin", country: countries[3] },
					{ value: "THR", name: "Thurrock", country: countries[3] },
					{ value: "TOB", name: "Torbay", country: countries[3] },
					{ value: "TOF", name: "Torfaen", country: countries[3] },
					{ value: "TRF", name: "Trafford", country: countries[3] },
					{ value: "TWH", name: "Tower Hamlets", country: countries[3] },
					{ value: "VGL", name: "Vale of Glamorgan, The", country: countries[3] },
					{ value: "WAR", name: "Warwickshire", country: countries[3] },
					{ value: "WBK", name: "West Berkshire", country: countries[3] },
					{ value: "WDU", name: "West Dunbartonshire", country: countries[3] },
					{ value: "WFT", name: "Waltham Forest", country: countries[3] },
					{ value: "WGN", name: "Wigan", country: countries[3] },
					{ value: "WIL", name: "Wiltshire", country: countries[3] },
					{ value: "WKF", name: "Wakefield", country: countries[3] },
					{ value: "WLL", name: "Walsall", country: countries[3] },
					{ value: "WLN", name: "West Lothian", country: countries[3] },
					{ value: "WLV", name: "Wolverhampton", country: countries[3] },
					{ value: "WND", name: "Wandsworth", country: countries[3] },
					{ value: "WNM", name: "Windsor and Maidenhead", country: countries[3] },
					{ value: "WOK", name: "Wokingham", country: countries[3] },
					{ value: "WOR", name: "Worcestershire", country: countries[3] },
					{ value: "WRL", name: "Wirral", country: countries[3] },
					{ value: "WRT", name: "Warrington", country: countries[3] },
					{ value: "WRX", name: "Wrexham", country: countries[3] },
					{ value: "WSM", name: "Westminster", country: countries[3] },
					{ value: "WSX", name: "West Sussex", country: countries[3] },
					{ value: "YOR", name: "York", country: countries[3] },
					{ value: "ZET", name: "Shetland Islands", country: countries[3] }
				];
				var ie_states = [
					{ value: "C", name: "Cork", country: countries[4] },
					{ value: "CE", name: "Clare", country: countries[4] },
					{ value: "CN", name: "Cavan", country: countries[4] },
					{ value: "CW", name: "Carlow", country: countries[4] },
					{ value: "D", name: "Dublin", country: countries[4] },
					{ value: "DL", name: "Donegal", country: countries[4] },
					{ value: "G", name: "Galway", country: countries[4] },
					{ value: "KE", name: "Kildare", country: countries[4] },
					{ value: "KK", name: "Kilkenny", country: countries[4] },
					{ value: "KY", name: "Kerry", country: countries[4] },
					{ value: "LD", name: "Longford", country: countries[4] },
					{ value: "LH", name: "Louth", country: countries[4] },
					{ value: "LK", name: "Limerick", country: countries[4] },
					{ value: "LM", name: "Leitrim", country: countries[4] },
					{ value: "LS", name: "Laois", country: countries[4] },
					{ value: "MH", name: "Meath", country: countries[4] },
					{ value: "MN", name: "Monaghan", country: countries[4] },
					{ value: "MO", name: "Mayo", country: countries[4] },
					{ value: "OY", name: "Offaly", country: countries[4] },
					{ value: "RN", name: "Roscommon", country: countries[4] },
					{ value: "SO", name: "Sligo", country: countries[4] },
					{ value: "TA", name: "Tipperary", country: countries[4] },
					{ value: "WD", name: "Waterford", country: countries[4] },
					{ value: "WH", name: "Westmeath", country: countries[4] },
					{ value: "WW", name: "Wicklow", country: countries[4] },
					{ value: "WX", name: "Wexford", country: countries[4] }
				];

				return {
					getStatesAndProvinces: function() {
						return us_states.concat(provinces, australian_states, uk_states, ie_states);
					},
					countries: function() {
						return countries;
					},
					setState: function(states, binder) {
						states.every(function(state) {
							if (state.value.toLowerCase() === binder.property_attributes.state.toLowerCase()) {
								binder.property_attributes.state = state.value;
								return false;
							}
						});
						return binder.property_attributes.state;
					},
					getState: function(states, state) {
						var new_state = null;
						states.forEach(function(ste) {
							if (ste.name.toLowerCase() === state.toLowerCase()) {
								new_state = ste;
							}
						});
						return new_state;
					},
					setCountry: function(states, propState) {
						var country = null;
						states.forEach(function(state) {
							if (state.value.toLowerCase() === propState.toLowerCase()) {
								country = state.country.value;
							}
						});
						return country;
					},
					findCountry: function(item) {
						return item.country.name;
					},
					findCountryByCode: function(countryCode) {

						if (!countryCode) {
							return countries[0];
						}
						var country = null;
						countries.forEach(function(ctry) {
							if (ctry.value === countryCode) {
								country = ctry;
							}
						});

						if (!country) {
							return countries[0];
						}

						return country;
					}
				};
			}
		]);
})();