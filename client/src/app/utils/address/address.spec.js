describe("Address", function() {
    'use strict';

    var Address;

    beforeEach(function() {
        module("homebinder");

        inject(function(_$injector_) {
            Address = _$injector_.get("Address");
        });
    });

    it("returns all states and provinces", function() {
        var states;

        // call the function
        states = Address.getStatesAndProvinces();

        // verify results
        expect(states.length).toEqual(330);
    });
    
    it("returns all countries", function() {
        var countries;

        // call the function
        countries = Address.countries();

        // verify results
        expect(countries.length).toEqual(5);
    });

    it("returns the state", function() {
        var state;

        // call the function
        state = Address.setState([{value: "MA"}], {property_attributes: {state: "MA"}});

        // verify results
        expect(state).toEqual("MA");
    });

    it("returns the state", function() {
        var state;

        // call the function
        state = Address.getState([{name: "MA"}], "MA");

        // verify results
        expect(state).toEqual({name: "MA"});
    });
    
    it("returns the country", function() {
        var country;

        // call the function
        country = Address.setCountry([{value: "MA", country: {value: "US"}}], "MA");

        // verify results
        expect(country).toEqual("US");
    });
    
    it("returns the country", function() {
        var country;

        // call the function
        country = Address.findCountry({country: {name: "US"}});

        // verify results
        expect(country).toEqual("US");
    });
    
});