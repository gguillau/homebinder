(function() {
    "use strict";
    
    angular
        .module("hb.utils", [
            "hb.utils.address",
            "hb.utils.enums.userRole",
            "hb.utils.filters",
            "hb.utils.exceptions",
            "hb.utils.hbInterceptor",
            "hb.utils.utils",
            "hb.utils.validation",
            "hb.utils.serviceKeyList"
        ])
        .factory("hb.utils", [
            "Address",
            "Utils",
            "Validation",
            "ServiceKeyList",
            function (
                Address,
                Utils,
                Validation,
                ServiceKeyList
            ) {
                return {
                    address: Address,
                    utils: Utils,
                    validation: Validation,
                    serviceKeyList: ServiceKeyList
                };
            }
        ]);
})();