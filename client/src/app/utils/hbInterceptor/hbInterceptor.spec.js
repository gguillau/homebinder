describe("hbInterceptor", function() {
    'use strict';

    var hbInterceptor, Permissions, notify, $window, $state, AfterLogin, Session,
    $location, $timeout;

    // load the homebinder module
    beforeEach(module("homebinder"));
    
    beforeEach(function() {
        inject(function($injector) {
            hbInterceptor = $injector.get("hbInterceptor");
            Permissions = $injector.get("Permissions");
            $window = $injector.get("$window");
            notify = $injector.get("Notify");
            $state = $injector.get("$state");
            AfterLogin = $injector.get("AfterLogin");
            Session = $injector.get("Session");
            $location = $injector.get("$location");
            $timeout = $injector.get("$timeout");
        });
    });

    describe('request', function() {
        it("returns empty hash", function() {
            spyOn(Permissions, "canWrite").and.returnValue(true);
            var config = hbInterceptor.request({});
            expect(config).toEqual({});
        });
        
        it("returns rejection", function() {
            spyOn(Permissions, "canWrite").and.returnValue(false);
            var config = hbInterceptor.request({});
            expect(config.$$state.status).toEqual(2);
        });
    });
    
    describe('responseError', function() {
        it("calls notify", function() {
            spyOn(notify, "info");
            hbInterceptor.responseError({data: {message: "Hello"}, status: -1, config: {timeout: true}});
            $timeout.flush();
            
            expect(notify.info).toHaveBeenCalledWith("You don't have permission to make any changes.");
        });
        
        xit("calls notify", function() {
            spyOn(notify, "info");
            spyOn($window.location, "reload");
            hbInterceptor.responseError({data: {message: "Hello"}, status: 412, config: {timeout: true}});
            expect(notify.info).toHaveBeenCalledWith("Your browser has been refreshed to include the latest version of HomeBinder.");
        });
        
        it("calls $state go", function() {
            spyOn($state, "go");
            hbInterceptor.responseError({data: {message: "Hello"}, status: 401, config: {timeout: true}});
            expect($state.go).toHaveBeenCalledWith("login");
        });
        
        it("returns Test", function() {
            spyOn($state, "go");
            var response = hbInterceptor.responseError({data: ["Test"], status: 422, config: {timeout: true}});
            expect(response.$$state.value.data).toEqual("Test");
        });
        
        it("calls $state go", function() {
            spyOn($state, "go");
            hbInterceptor.responseError({data: {message: "Hello"}, status: 503, config: {timeout: true}});
            expect($state.go).toHaveBeenCalledWith("unavailable");
        });
        
        it("calls AfterLogin goToDashboard", function() {
            spyOn(AfterLogin, "goToDashboard");
            spyOn(Session, "getUser").and.returnValue(true);
            hbInterceptor.responseError({data: {message: "Hello"}, status: 403, config: {timeout: true}});
            
            expect(AfterLogin.goToDashboard).toHaveBeenCalled();
        });
        
        it("calls $location url", function() {
            spyOn($location, "url");
            spyOn(Session, "getUser").and.returnValue(false);
            hbInterceptor.responseError({data: {message: "Hello"}, status: 403, config: {timeout: true}});
            
            expect($location.url).toHaveBeenCalled();
        });
        
        it("returns Internal Server error message", function() {
            var response = hbInterceptor.responseError({data: ["Test"], status: 500, config: {timeout: true}});
            expect(response.$$state.value.data).toEqual("Internal Server Error. Please contact support@homebinder.com");
        });
    });
});