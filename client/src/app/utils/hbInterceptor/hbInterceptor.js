(function() {
	"use strict";

	angular
		.module("hb.utils.hbInterceptor", [])
		.factory("hbInterceptor", [
			"$injector",
			function($injector) {
				var $q = $injector.get("$q");
				var $location = $injector.get("$location");
				return {
					"request": function(config) {
						var permissions = $injector.get("Permissions");
						if (!permissions.canWrite(config)) {
							var canceler = $q.defer();
							config.timeout = canceler.promise;
							canceler.resolve();
							var rejection = {
								status: -1,
								config: config
							};
							return $q.reject(rejection);
						}
						return config;
					},
					"responseError": function(rejection) {
						var $state = $injector.get("$state");
						var $window = $injector.get("$window");
						var notify = $injector.get("Notify");
						var loading = $injector.get("Loading");
						var $timeout = $injector.get("$timeout");
						var session = $injector.get("Session");
						var context = $injector.get("Context");

						if (rejection.data && rejection.data.message) {
							rejection.data = rejection.data.message;
						}
						if (rejection.status == -1 && rejection.config.timeout) {
							notify.info("You don't have permission to make any changes.");
							$timeout(function() {
								loading.close();
							});
							return $q.defer().promise;
						}
						if (rejection.status == 412) {
							$window.location.reload();
							notify.info("Your browser has been refreshed to include the latest version of HomeBinder.");
						}
						if (rejection.status == 401) {
							if (!$state.is("login")) {
								Intercom('shutdown');
								session.clearAll();
								context.reset();
								$state.go("login");
							}
						}
						else if (rejection.status == 422) {
							rejection.data = rejection.data[0];
							return $q.reject(rejection);
						}
						else if (rejection.status == 503) {
							$state.go("unavailable");
							return $q.reject(rejection);
						}
						else if (rejection.status == 403) {
							var afterLogin = $injector.get("AfterLogin");
							if (session.getUser()) {
								afterLogin.goToDashboard();
							}
							else {
								$location.url("/");
							}
							notify.info("Sorry you don't have permission to do that.");
							return $q.reject(rejection);
						}
						else if (rejection.status == 500) {
							rejection.data = "Internal Server Error. Please contact support@homebinder.com";
						}
						return $q.reject(rejection);
					}
				};
			}
		]);
})();