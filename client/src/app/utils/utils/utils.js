(function() {
    "use strict";

    angular
        .module("hb.utils.utils", [])
        .factory("Utils", [
            '$location',
            'hb.userRole',
            function($location, userRole) {

                var date = new Date();
                var dd = date.getDate();
                var month = date.getMonth() + 2;
                var yyyy = date.getFullYear();
                var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                if (dd < 10) {
                    dd = '0' + dd;
                }

                if (month < 10) {
                    month = '0' + month;
                }

                return {
                    createQueryString: function(params) {
                        var prop,
                            string = "";

                        for (prop in params) {
                            if (string.length === 0) {
                                string = "?";
                            }
                            else {
                                string += "&";
                            }
                            string += prop + "=" + params[prop];
                        }
                        return string;
                    },
                    getToday: function(add_undescore) {
                        if (add_undescore) {
                            return months[month - 2] + '_' + dd + '_' + yyyy;
                        }
                        else {
                            return months[month - 2] + ' ' + dd + ' ' + yyyy;
                        }
                    },
                    getNextMonth: function() {
                        return months[month - 2] + ' ' + dd + ', ' + yyyy;
                    },
                    getYear: function() {
                        return yyyy;
                    },
                    getDeviceInfo: function() {
                        var ua = window.navigator.userAgent,
                            tem,
                            M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
                        if (/trident/i.test(M[1])) {
                            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
                            return 'IE ' + (tem[1] || '');
                        }
                        if (M[1] === 'Chrome') {
                            tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
                            if (tem != null) { return tem.slice(1).join(' ').replace('OPR', 'Opera'); }
                        }
                        M = M[2] ? [M[1], M[2]] : [window.navigator.appName, window.navigator.appVersion, '-?'];
                        if ((tem = ua.match(/version\/(\d+)/i)) != null) { M.splice(1, 1, tem[1]); }
                        return M;
                    },
                    getOperatingSystem: function() {
                        var OSName = "Unknown OS";
                        if (window.navigator.appVersion.indexOf("Win") != -1) { OSName = "Windows"; }
                        if (window.navigator.appVersion.indexOf("Mac") != -1) { OSName = "MacOS"; }
                        if (window.navigator.appVersion.indexOf("X11") != -1) { OSName = "UNIX"; }
                        if (window.navigator.appVersion.indexOf("Linux") != -1) { OSName = "Linux"; }
                        return OSName;
                    }
                };
            }
        ]);

})();