# The `src/app` Directory

## Overview

```
src/
  |- app/
  |  |- app.js
  |  |- app.spec.js
```

The `src/app` directory contains all code specific to this application. Apart
from `app.js` and its accompanying tests (discussed below), this directory is
filled with subdirectories corresponding to high-level sections of the
application, often corresponding to top-level routes. Each directory has as
many subdirectories as it needs, and the build system will understand what to
do. For example, one top-level route is "partners", which would be a folder
within the `src/app` directory that conceptually corresponds to the top-level
route `/partners`. Partners have subdirectories for "settings", "binders", 
"homeowners", etc.

## `app.js`

This is our main app configuration file. It kickstarts the whole process by
requiring all the modules from `src/app` that we need. We must load these now to
ensure the routes are loaded. If as in our "partners" example there are
subroutes, we only require the top-level module, and allow the submodules to
require their own submodules.

As a matter of course, we also require the template modules that are generated
during the build.


```js
angular.module( 'homebinder', [
  'templates-app',
  'templates-common',
  'homebinder.admins',
  'homebinder.partners'
  'ui.router',
  'ui.route'
])
```

With app modules broken down in this way, all routing is performed by the
submodules we include, as that is where our app's functionality is really
defined.  So all we need to do in `app.js` is specify a default route to follow,
which route of course is defined in a submodule. In this case, our `admins` module
is where we want to start, which has a defined route for `/admins` in
`src/app/admins/admins.js`.


### Testing

One of the design philosophies of `homebinder` is that tests should exist
alongside the code they test and that the build system should be smart enough to
know the difference and react accordingly. As such, the unit test for `app.js`
is `app.spec.js`, though it is quite minimal.