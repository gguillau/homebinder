describe("HBAdminDashboardsController", function() {
    var controller,
        DashboardController,
        api;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });

        // mock the Notify service
        $provide.factory("Notify", function() {
            return {
                info: function() {},
                error: function() {}
            };
        });

        // mock the Session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return {
                        role: "admin"
                    };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        DashboardController = $injector.get("DashboardController");
        api = $injector.get("hb.api");

        spyOn(api.dashboard, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));

        controller = $controller("HBAdminDashboardsController", {
            "DashboardController": DashboardController,
            "hb.api": api
        });

        $rootScope.$apply();
    }));

    describe("init", function() {
        it("sets attributes", function() {
            expect(controller.model.cfg.admin).toBe(true);
        });
    });

});