(function() {
    "use strict";

    angular.module('hb.admin.dashboards', [
        ])
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state('admin.dashboards', {
                    url: '^/admin/dashboards',
                    templateUrl: "dashboards/dashboards.tpl.html",
                    controller: "HBAdminDashboardsController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .controller("HBAdminDashboardsController", [
            'DashboardController',
            Controller
        ]);

    function Controller(DashboardController) {
        var Model = function() {
            DashboardController.call(this);
            this.cfg = {
                admin: true
            };
            this.refresh();
        };

        Model.prototype = Object.create(DashboardController.prototype);

        this.model = new Model();
    }
})();