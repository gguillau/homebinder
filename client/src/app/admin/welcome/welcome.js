(function() {
	"use strict";

	angular
		.module("hb.admin.welcome", [
			"hb.components"
		])
		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("adminsWelcome", {
					url: "^/admin/:accessToken/welcome",
					views: {
						'@': {
							templateUrl: "components/users/welcome/welcome.tpl.html",
							controller: "AdminWelcomeController",
							controllerAs: "ctrl"
						}
					}
				});
		}])
		.controller("AdminWelcomeController", [
			"UsersWelcomeController",
			AdminWelcomeController
		]);

	function AdminWelcomeController(UsersWelcomeController) {
		var Model = function() {
			// call the parent class
			UsersWelcomeController.call(this);
			this.transferText = this.resources.choosePassword;
			this.submitLabel = this.resources.getStarted;
			this.refresh();
		};

		Model.prototype = Object.create(UsersWelcomeController.prototype);

		angular.extend(Model.prototype);

		this.model = new Model();
	}
})();