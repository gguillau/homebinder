(function() {
	"use strict";

	angular.module('hb.admin.users', [
			'hb.admin.users.edit',
			"hb.admin.users.new"
		])
		.config(['$stateProvider', adminUsersConfig])
		.controller("AdminUsersIndexController", [
			"UsersIndexController",
			AdminUsersIndexController
		]);

	function AdminUsersIndexController(UsersIndexController) {

		var Model = function() {
			// call the parent class
			UsersIndexController.call(this);
			this.settings.canImpersonate = true;
			this.refresh();
		};

		Model.prototype = Object.create(UsersIndexController.prototype);

		angular.extend(Model.prototype);

		this.model = new Model();
	}

	function adminUsersConfig($stateProvider) {
		$stateProvider
			.state('admin.users', {
				url: '^/admin/users',
				templateUrl: 'components/users/index/index.tpl.html',
				controller: "AdminUsersIndexController",
				controllerAs: "ctrl",
				access: {
					requiresLogin: true
				}
			});
	}

})();