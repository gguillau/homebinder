(function() {
    "use strict";

    angular.module('hb.admin.users.new', [])
        .config(['$stateProvider', adminUsersNewConfig])
        .controller("AdminUsersNewController", [
            "UsersNewController",
            AdminUsersNewController
        ]);

    function AdminUsersNewController(UsersNewController) {

        var Model = function() {
            // call the parent class
            UsersNewController.call(this);
            this.indexState = "admin.users";
            this.indexParams = {};
        };

        Model.prototype = Object.create(UsersNewController.prototype);

        angular.extend(Model.prototype);

        this.model = new Model();
    }

    function adminUsersNewConfig($stateProvider) {
        $stateProvider
            .state('admin.users_new', {
                url: '^/admin/users/new',
                templateUrl: 'components/users/form/form.tpl.html',
                controller: "AdminUsersNewController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
    }

})();