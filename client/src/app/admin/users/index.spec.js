describe("AdminUsersIndexController", function() {
    var controller,
        base,
        $q_;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        $q_ = $q;
        base = $injector.get("UsersIndexController");

        controller = $controller("AdminUsersIndexController", {
            "UsersIndexController": base
        });
    }));

    describe("init", function() {
        it("sets the settings canImpersonate", function() {
            expect(controller.model.settings.canImpersonate).toEqual(true);
        });
    });

});