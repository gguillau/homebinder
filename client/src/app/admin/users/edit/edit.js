(function() {
    "use strict";

    angular.module('hb.admin.users.edit', [])
        .config(['$stateProvider', adminUsersEditConfig])
        .controller("AdminUsersEditController", [
            'UsersEditController',
            AdminUsersEditController
        ]);

    function AdminUsersEditController(UsersEditController) {
        var Model = function() {
            UsersEditController.call(this);
            this.indexState = "admin.users";
            this.indexParams = {};
            // binders
            this.navigation_links[0].links.push({
                active: false,
                name: "Binders",
                value: "binders"
            });
            // partners
            this.navigation_links[0].links.push({
                active: false,
                name: "Partners",
                value: "partners"
            });
            // companies that have created a binder for this user
            this.navigation_links[0].links.push({
                active: false,
                name: "Companies",
                value: "companies"
            });
            // sessions in the last 30 days
            this.navigation_links[0].links.push({
                active: false,
                name: "Sessions",
                value: "sessions"
            });
            // shares
            this.navigation_links[0].links.push({
                active: false,
                name: "Transfers",
                value: "transfers"
            });
            // shares
            this.navigation_links[0].links.push({
                active: false,
                name: "Shares",
                value: "shares"
            });
            // refresh
            this.refresh();
        };

        Model.prototype = Object.create(UsersEditController.prototype);

        angular.extend(Model.prototype);

        this.model = new Model();
    }

    function adminUsersEditConfig($stateProvider) {
        $stateProvider
            .state('admin.users_edit', {
                url: '^/admin/users/:userId/edit',
                templateUrl: 'components/users/form/form.tpl.html',
                controller: "AdminUsersEditController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
    }
})();
