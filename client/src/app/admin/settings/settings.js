(function() {
	"use strict";

	angular.module('hb.admin.settings', [
			'hb.admin.settings.typeaheads'
		])
		.config(['$stateProvider', adminSettingsConfig])
		.controller("AdminSettingsCtrl", [
			AdminSettingsCtrl
		]);

	function AdminSettingsCtrl() {
		this.navigation_links = [{
			name: "Typeaheads",
			links: [
				{ name: "Appliance Makes", value: "appliance_manufacturers", active: true }
			]
		}];
		this.currentLink = this.navigation_links[0].links[0];
	}

	AdminSettingsCtrl.prototype = {
		changeLink: function(link) {
			this.currentLink.active = false;
			link.active = true;
			this.currentLink = link;
		}
	};

	function adminSettingsConfig($stateProvider) {
		$stateProvider
			.state('admin.settings', {
				url: '^/admin/settings',
				templateUrl: 'admin/settings/settings.tpl.html',
				controller: 'AdminSettingsCtrl',
				controllerAs: "ctrl",
				access: {
					requiresLogin: true
				}
			});
	}

})();