(function() {
	"use strict";

	angular.module('hb.admin.settings.typeaheads.applianceManufacturers', [])
		.directive("applianceManufacturers", function() {
			return {
				restrict: "E",
				templateUrl: "admin/settings/typeaheads/index/index.tpl.html",
				controller: "ApplianceManufacturersController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("ApplianceManufacturersController", [
			'TypeaheadsController',
			'hb.api',
			ApplianceManufacturersController
		]);

	function ApplianceManufacturersController(TypeaheadsController, api) {
		var Model = function() {
			// call the parent class
			TypeaheadsController.call(this);
			// set the refresh API call
			this.refreshCall = api.applianceManufacturer.unverified;
			// set the delete API call
			this.deleteCall = api.applianceManufacturer.destroy;
			// set the update API call
			this.updateCall = api.applianceManufacturer.update;
			this.title = "Appliance Typeaheads";
			this.refresh();
		};

		Model.prototype = Object.create(TypeaheadsController.prototype);

		this.model = new Model();
	}

})();