describe("ApplianceManufacturersController", function() {
    var controller,
        base;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("TypeaheadsController");

        controller = $controller("ApplianceManufacturersController", {
            "TypeaheadsController": base
        });
    }));

    describe("init", function() {
        it("sets the title", function() {
            expect(controller.model.title).toEqual("Appliance Typeaheads");
        });
    });

});

describe('applianceManufacturers', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;

        spyOn(api.applianceManufacturer, "unverified").and.returnValue($q.when({ data: {items: [] }}));

        $compile('<appliance-manufacturers></appliance-manufacturers>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the appliance-manufacturers directive', function() {
        it('calls api applianceManufacturer unverified', function() {
            expect(api.applianceManufacturer.unverified).toHaveBeenCalled();
        });
    });
});