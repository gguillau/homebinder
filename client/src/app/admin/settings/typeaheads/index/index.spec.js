describe("TypeaheadsController", function() {
    var TestClass,
        TypeaheadsController,
        api,
        $q,
        notify;

    // load the homebinder module
    beforeEach(module("homebinder"));

    // get the base class under test
    beforeEach(inject(function($injector) {
        TypeaheadsController = $injector.get("TypeaheadsController");
        api = $injector.get("hb.api");
        $q = $injector.get("$q");
        notify = $injector.get("Notify");
    }));

    function createTestClass() {
        TestClass = function() {
            TypeaheadsController.call(this);
        };

        TestClass.prototype = Object.create(TypeaheadsController.prototype);

        return new TestClass();
    }

    describe("onRefreshSuccess", function() {
        it("sets the items", inject(function() {
            var model = createTestClass();
            model.onRefreshSuccess({ data: [{ id: 1 }] });

            expect(model.items.length).toEqual(1);
        }));
    });

    describe("saveAll", function() {
        it("calls updateCall", inject(function() {
            var model = createTestClass();
            model.updateCall = api.applianceManufacturer.update;
            spyOn(model, "updateCall").and.returnValue($q.when({ data: {} }));

            model.items = [{ id: 1, name: "test", verified: false }];
            model.saveAll();

            expect(model.updateCall).toHaveBeenCalled();
        }));

        it("calls loading close", inject(function() {
            var model = createTestClass();
            spyOn(model.loading, "close");

            model.items = [];
            model.saveAll();

            expect(model.loading.close).toHaveBeenCalled();
        }));
    });

    describe("updateSuccess", function() {
        it("calls saveAll", inject(function() {
            var model = createTestClass();
            spyOn(model, "saveAll");

            model.updateSuccess();

            expect(model.saveAll).toHaveBeenCalled();
        }));
    });
    
    describe("updateError", function() {
        it("calls saveAll", inject(function() {
            var model = createTestClass();
            spyOn(model, "saveAll");

            model.updateError({data: "error"});

            expect(model.saveAll).toHaveBeenCalled();
        }));
    });

});