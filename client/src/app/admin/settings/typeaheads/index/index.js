(function() {
	"use strict";

	angular.module('hb.admin.settings.typeaheads.index', [])
		.factory("TypeaheadsController", [
			"hb.framework.indexBase",
			"hb.api",
			"hb.resources",
			"$stateParams",
			"Notify",
			"$log",
			"Session",
			"Loading",
			function(IndexBase, api, resources, $stateParams, notify, $log, session, loading) {
				var Model = function() {
					// call the parent class
					IndexBase.call(this);
				};

				Model.prototype = Object.create(IndexBase.prototype);

				angular.extend(Model.prototype, {

					// Refresh callback. Updates the item list and totals
					onRefreshSuccess: function(response) {
						this.items = response.data;
						this.totalItems = this.toolbarCfg.total = this.items.length;
						loading.close();
					},

					saveAll: function() {
						this.loading.show("Updating typeaheads...");
						// If there are typeheads to save start updating them.
						if (this.items && this.items.length > 0) {
							var item = this.items[0];
							this.items.splice(0, 1);
							item.verified = true;
							this.updateCall(item, item.id).then(
								angular.bind(this, this.updateSuccess),
								angular.bind(this, this.updateError));
						}
						else {
							this.loading.close();
						}
					},

					updateSuccess: function(response) {
						this.saveAll();
					},

					updateError: function(response) {
						this.$log.error(response);
						this.notify.error(response.data);
						this.saveAll();
					}
				});

				return Model;
			}
		]);
})();