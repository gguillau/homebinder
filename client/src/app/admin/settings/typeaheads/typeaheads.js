(function() {
    "use strict";
    
    angular.module("hb.admin.settings.typeaheads", [
        "hb.admin.settings.typeaheads.index",
        "hb.admin.settings.typeaheads.applianceManufacturers"
    ]);
})();