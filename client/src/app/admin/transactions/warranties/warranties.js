(function() {
	"use strict";

	angular
		.module("hb.admin.transactions.warranties", [])
		.directive("adminWarrantiesTransactions", function() {
			return {
				restrict: "E",
				templateUrl: "admin/transactions/warranties/warranties.tpl.html",
				controller: "AdminWarrantiesTransactionsCtrl",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};

		})
		.controller("AdminWarrantiesTransactionsCtrl", [
			"TransactionalBillingIndex",
			"hb.api",
			"Notify",
			"$log",
			"Loading",
			"hb.resources",
			"$q",
			AdminWarrantiesTransactionsCtrl
		]);

	function AdminWarrantiesTransactionsCtrl(TransactionalBillingIndex, api, Notify, $log, loading, resources, $q) {
		var Model = function() {
			// call parent
			TransactionalBillingIndex.call(this);
			this.api = api;
			this.resources = angular.extend({}, this.resources, resources.warrantyIndex);
			this.$log = $log;
			this.notify = Notify;
			this.loading = loading;
			this.$q = $q;

			// set costs
			this.planOneCost = 18;
			this.planTwoCost = 15;

			this.setHeaders();
			this.refresh();
		};

		Model.prototype = Object.create(TransactionalBillingIndex.prototype);

		angular.extend(Model.prototype, {
			setHeaders: function() {
				this.columnspan = 7;
				var planOneHeader = "Plan One Count ($" + this.planOneCost + ")";
				var planTwoHeader = "Plan Two Count ($" + this.planTwoCost + ")";

				this.headerTitles = ["Partner ID", "Partner Name", "Partner Email", "Stripe ID", planOneHeader, planTwoHeader, "Total Cost"];
				this.headers = this.headerTitles.map(angular.bind(this, function(attribute) {
					var value = null;
					if (attribute === "Partner ID") {
						value = "id";
					}
					if (attribute === "Partner Name") {
						value = "contact";
					}
					if (attribute === "Partner Email") {
						value = "email";
					}
					if (attribute === "Stripe ID") {
						value = "stripe";
					}
					if (attribute === planOneHeader) {
						value = "planOneCount";
					}
					if (attribute === planTwoHeader) {
						value = "planTwoCount";
					}
					if (attribute === "Total Cost") {
						value = "totalCost";
					}
					return {
						name: attribute,
						sorted: false,
						sortable: true,
						order: "desc",
						value: value
					};
				}));
			},


			refresh: function() {
				this.loading.show("Loading...");
				this.partners = [];
				this.warranties = [];
				this.retrievedPartners = [];

				// reset sort and get first and last days of the month
				this.resetSort();
				this.getDays();

				var promises = [];
				promises.push(
					// warranty_plan_id: 1
					// 90 Day Warranty + Water/Sewer, Mold and Roof Protection, worth $18
					this.api.warranty.all({
						searchType: "aggregation_field",
						query: "warranty_plan_id: 1",
						field: "partner_id",
						range: "created_at",
						gte: this.firstDay,
						lte: this.lastDay,
						count: 100,
						search: true
					}).then(
						angular.bind(this, this.onWarrantySuccess, 1),
						angular.bind(this, this.onError)),

					// warranty_plan_id: 2
					// 90 Day Warranty, worth $15
					this.api.warranty.all({
						searchType: "aggregation_field",
						query: "warranty_plan_id: 2",
						field: "partner_id",
						range: "created_at",
						gte: this.firstDay,
						lte: this.lastDay,
						count: 100,
						search: true
					}).then(
						angular.bind(this, this.onWarrantySuccess, 2),
						angular.bind(this, this.onError))
				);

				this.$q.all(promises).then(
					angular.bind(this, this.onRefreshSuccess)
				);
			},

			onWarrantySuccess: function(type, response) {
				var warranties = response.data;
				warranties.forEach(angular.bind(this, function(warranty) {
					if (!this.retrievedPartners.includes(warranty.key)) {
						this.retrievedPartners.push(warranty.key);
					}
					warranty.type = type;
					this.warranties.push(warranty);
				}));
			},

			onRefreshSuccess: function(response) {
				var promises = [];

				this.retrievedPartners.forEach(angular.bind(this, function(partnerKey) {
					promises.push(
						this.api.partner.get(partnerKey).then(
							angular.bind(this, this.getPartnerSuccess),
							angular.bind(this, this.onError))
					);
				}));

				this.$q.all(promises).then(
					angular.bind(this, this.allPartnersSuccess)
				);
			},

			getPartnerSuccess: function(response) {
				var partner = response.data;
				partner.stripe = partner.account.stripe_customer_id;
				partner.planOneCost = 0;
				partner.planTwoCost = 0;
				partner.planOneCount = 0;
				partner.planTwoCount = 0;
				partner.totalCost = 0;
				this.partners.push(partner);
			},

			allPartnersSuccess: function(response) {
				this.partners.forEach(angular.bind(this, function(partner, partnerIndex) {
					this.warranties.forEach(angular.bind(this, function(warranty, warrantyIndex) {
						if (partner.id === warranty.key) {
							if (warranty.type === 1) {
								this.setCountAndCost(partnerIndex, "planOneCount", "planOneCost", warranty.doc_count, this.planOneCost);
							}
							else if (warranty.type === 2) {
								this.setCountAndCost(partnerIndex, "planTwoCount", "planTwoCost", warranty.doc_count, this.planTwoCost);
							}
						}
					}));
				}));

				// calculate total cost
				this.calculateTotalCost();

				this.loading.close();
			},

			setCountAndCost: function(index, countAttribute, costAttribute, count, cost) {
				this.partners[index][countAttribute] = count;
				this.partners[index][costAttribute] = count * cost;
			},

			calculateTotalCost: function() {
				this.partners.forEach(angular.bind(this, function(partner) {
					partner.totalCost = partner.planOneCost + partner.planTwoCost;
				}));
			}
		});

		this.model = new Model();
	}

})();