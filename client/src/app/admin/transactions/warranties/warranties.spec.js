describe("AdminWarrantiesTransactionsCtrl", function() {
    var controller,
        ctrl,
        TransactionalBillingIndex,
        api,
        resources,
        $log,
        notify,
        loading,
        $q;

    // load the homebinder module
    beforeEach(module("homebinder"));

    // create the controller
    beforeEach(inject(function($controller, $injector, _$q_, $rootScope) {
        controller = $controller;
        TransactionalBillingIndex = $injector.get("TransactionalBillingIndex");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        $log = $injector.get("$log");
        notify = $injector.get("Notify");
        loading = $injector.get("Loading");
        $q = $injector.get("$q");
    }));

    function createController() {
        ctrl = controller("AdminWarrantiesTransactionsCtrl", {
            "TransactionalBillingIndex": TransactionalBillingIndex,
            "hb.api": api,
            "hb.resources": resources,
            "$log": $log,
            "Notify": notify,
            "Loading": loading
        });

        spyOn(api.partner, "get").and.returnValue($q.when({
            data: { id: 1 }
        }));

        spyOn(api.warranty, "all").and.returnValue($q.when({
            data: [{ key: 1, doc_counts: 1 }]
        }));
    }

    describe("init", function() {
        it("sets the costs", function() {
            createController();
            expect(ctrl.model.planOneCost).toEqual(18);
            expect(ctrl.model.planTwoCost).toEqual(15);
        });
    });

    describe("setHeaders", function() {
        it("sets the headers", function() {
            createController();

            var planOneHeader = "Plan One Count ($" + ctrl.model.planOneCost + ")";
            var planTwoHeader = "Plan Two Count ($" + ctrl.model.planTwoCost + ")";
            var headerTitles = ["Partner ID", "Partner Name", "Partner Email", "Stripe ID", planOneHeader, planTwoHeader, "Total Cost"];
            var headers = [{
                name: "Partner ID",
                sorted: false,
                sortable: true,
                order: "desc",
                value: "id"
            }, {
                name: "Partner Name",
                sorted: false,
                sortable: true,
                order: "desc",
                value: "contact"
            }, {
                name: "Partner Email",
                sorted: false,
                sortable: true,
                order: "desc",
                value: "email"
            }, {
                name: "Stripe ID",
                sorted: false,
                sortable: true,
                order: "desc",
                value: "stripe"
            }, {
                name: planOneHeader,
                sorted: false,
                sortable: true,
                order: "desc",
                value: "planOneCount"
            }, {
                name: planTwoHeader,
                sorted: false,
                sortable: true,
                order: "desc",
                value: "planTwoCount"
            }, {
                name: "Total Cost",
                sorted: false,
                sortable: true,
                order: "desc",
                value: "totalCost"
            }];
            ctrl.model.setHeaders();

            expect(ctrl.model.columnspan).toEqual(7);
            expect(ctrl.model.headerTitles).toEqual(headerTitles);
            expect(ctrl.model.headers).toEqual(headers);
        });
    });

    describe("refresh", function() {
        it("calls warranty api", function() {
            createController();
            spyOn(loading, "show");
            spyOn(ctrl.model, "resetSort");
            spyOn(ctrl.model, "getDays");

            ctrl.model.refresh();

            expect(loading.show).toHaveBeenCalled();
            expect(ctrl.model.resetSort).toHaveBeenCalled();
            expect(ctrl.model.getDays).toHaveBeenCalled();
            expect(api.warranty.all.calls.count()).toEqual(2);
        });
    });

    describe("onWarrantySuccess", function() {
        it("sets the warranties and creates a list of unique partners", function() {
            createController();

            var responseOne = { data: [{ key: 1, doc_count: 1 }, { key: 2, doc_count: 2 }] };
            var responseTwo = { data: [{ key: 1, doc_count: 1 }, { key: 2, doc_count: 2 }] };
            ctrl.model.onWarrantySuccess(1, responseOne);
            ctrl.model.onWarrantySuccess(2, responseTwo);

            expect(ctrl.model.warranties.length).toEqual(4);
            expect(ctrl.model.retrievedPartners).toEqual([1, 2]);
        });
    });

    describe("onRefreshSuccess", function() {
        it("calls api.partner.get for each partner", function() {
            createController();

            ctrl.model.retrievedPartners = [1, 2, 3];
            ctrl.model.onRefreshSuccess();

            expect(api.partner.get.calls.count()).toEqual(3);
        });
    });

    describe("getPartnerSuccess", function() {
        it("sets the partner data", function() {
            createController();

            var response = { data: { account: { stripe_customer_id: 1 } } };
            var partner = {
                account: { stripe_customer_id: 1 },
                stripe: 1,
                planOneCost: 0,
                planTwoCost: 0,
                planOneCount: 0,
                planTwoCount: 0,
                totalCost: 0
            };
            ctrl.model.getPartnerSuccess(response);

            expect(ctrl.model.partners.length).toEqual(1);
            expect(ctrl.model.partners[0]).toEqual(partner);
        });
    });

    describe("allPartnersSuccess", function() {
        it("calls countAndCost for each partner/warranty pair", function() {
            createController();
            spyOn(loading, "close");
            spyOn(ctrl.model, "setCountAndCost");
            spyOn(ctrl.model, "calculateTotalCost");

            ctrl.model.partners = [{ id: 1 }, { id: 2 }];
            ctrl.model.warranties = [{ key: 1, type: 1, doc_count: 1 }, { key: 1, type: 2, doc_count: 1 }, { key: 2, type: 1, doc_count: 2 }, { key: 2, type: 2, doc_count: 2 }];
            ctrl.model.allPartnersSuccess();

            expect(ctrl.model.setCountAndCost.calls.count()).toEqual(4);
            expect(ctrl.model.calculateTotalCost).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();
        });
    });

    describe("setCountAndCost", function() {
        it("sets the count and cost", function() {
            createController();

            ctrl.model.partners = [{ id: 1 }, { id: 2 }];
            ctrl.model.setCountAndCost(0, "planOneCount", "planOneCost", 10, 10);

            expect(ctrl.model.partners[0].planOneCount).toEqual(10);
            expect(ctrl.model.partners[0].planOneCost).toEqual(100);
        });
    });

    describe("calculateTotalCost", function() {
        it("calculates the total cost for each partner", function() {
            createController();

            ctrl.model.partners = [{ id: 1, planOneCost: 5, planTwoCost: 5 }, { id: 2, planOneCost: 1, planTwoCost: 1 }];
            ctrl.model.calculateTotalCost();

            expect(ctrl.model.partners[0].totalCost).toEqual(10);
            expect(ctrl.model.partners[1].totalCost).toEqual(2);
        });
    });

});

describe('adminWarrantyTransactions', function() {
    var $scope, $compile, element, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_, _$q_) {
        api = _$injector_.get("hb.api");
        $q = _$q_;
        spyOn(api.partner, "all").and.returnValue($q.when({}));
        spyOn(api.warranty, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));
        $scope = $rootScope.$new(), $compile = _$compile_;

        element = $compile('<admin-warranties-transactions></admin-warranties-transactions>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the admin-warranties-transactions directive', function() {
        it('displays the title', function() {
            expect(element.html()).toContain("Warranties");
        });
    });
});