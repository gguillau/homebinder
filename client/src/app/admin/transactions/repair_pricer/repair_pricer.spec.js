describe("AdminRepairPricerTransactionsCtrl", function() {
    var controller,
        ctrl,
        TransactionalBillingIndex,
        api,
        resources,
        loading,
        $q;

    // load the homebinder module
    beforeEach(module("homebinder"));

    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        controller = $controller;
        TransactionalBillingIndex = $injector.get("TransactionalBillingIndex");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
        $q = $injector.get("$q");

        spyOn(api.partner, "all").and.returnValue($q.when({
            data: {items: []}
        }));
        
        spyOn(api.partnerBinder, "all").and.returnValue($q.when({
            data: 0
        }));
    }));

    function createController() {
        ctrl = controller("AdminRepairPricerTransactionsCtrl", {
            "TransactionalBillingIndex": TransactionalBillingIndex,
            "hb.api": api,
            "hb.resources": resources,
            "Loading": loading
        });
    }

    describe("init", function() {
        it("sets the cost", function() {
            createController();
            expect(ctrl.model.cost).toEqual(14);
        });
    });

    describe("refresh", function() {
        it("calls partner api", function() {
            createController();
            spyOn(loading, "show");

            ctrl.model.refresh();

            expect(loading.show).toHaveBeenCalled();
            expect(api.partner.all).toHaveBeenCalled();
        });
    });
    
    describe("onRefreshSuccess", function() {
        it("calls partner api for each partner", function() {
            createController();
            spyOn(ctrl.model, "resetSort");
            spyOn(ctrl.model, "getDays");

            var partners = [{
                id: 1,
                account: {}
            }, {
                id: 2,
                account: {}
            }, {
                id: 3,
                account: {}
            }];
            ctrl.model.onRefreshSuccess({data: {items: partners}});

            expect(ctrl.model.resetSort).toHaveBeenCalled();
            expect(ctrl.model.getDays).toHaveBeenCalled();
            expect(api.partnerBinder.all.calls.count()).toEqual(3);
        });
    });
    
    describe("getPartnerBinderSuccess", function() {
        it("calls partner api for each partner", function() {
            createController();
            spyOn(ctrl.model, "setCountAndCost");

            var index = 1;
            var response = {data: 2};
            ctrl.model.getPartnerBinderSuccess(index, response);

            expect(ctrl.model.setCountAndCost).toHaveBeenCalledWith(1, 2, ctrl.model.cost);
        });
    });
    
    describe("removeZeros", function() {
        it("remove all partners with 0 count from the array of partners", function() {
            createController();
            spyOn(loading, "close");

            ctrl.model.partners = [{
                count: 0
            }, {
                count: 1
            }, {
                count: 0
            }, {
                count: 2
            }, {
                count: 0
            }];
            ctrl.model.removeZeros();

            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.model.partners).toEqual([{count: 1}, {count: 2}]);
        });
    });
});

describe('adminRepairPricerTransactions', function() {
    var $scope, $compile, element, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_, _$q_) {
        api = _$injector_.get("hb.api");
        $q = _$q_;
        spyOn(api.partner, "all").and.returnValue($q.when({}));
        spyOn(api.repairPricerReport, "all").and.returnValue($q.when({}));
        $scope = $rootScope.$new(), $compile = _$compile_;

        element = $compile('<admin-repair-pricer-transactions></admin-repair-pricer-transactions>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the admin-repair-pricer-transactions directive', function() {
        it('displays the title', function() {
            expect(element.html()).toContain("Repair Pricer");
        });
    });
});