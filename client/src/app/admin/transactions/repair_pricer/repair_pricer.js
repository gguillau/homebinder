(function() {
	"use strict";

	angular.module('hb.admin.transactions.repair_pricer', [])
		.directive("adminRepairPricerTransactions", function() {
			return {
				restrict: "E",
				templateUrl: "admin/transactions/repair_pricer/repair_pricer.tpl.html",
				controller: "AdminRepairPricerTransactionsCtrl",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("AdminRepairPricerTransactionsCtrl", [
			"TransactionalBillingIndex",
			"hb.api",
			"hb.resources",
			"Loading",
			"$log",
			"Notify",
			"$q",
			AdminRepairPricerTransactionsCtrl
		]);

	function AdminRepairPricerTransactionsCtrl(TransactionalBillingIndex, api, resources, loading, $log, notify, $q) {
		var Model = function() {
			// call parent
			TransactionalBillingIndex.call(this);
			this.api = api;
			this.resources = angular.extend({}, this.resources, resources.repairPricerIndex);
			this.loading = loading;
			this.$log = $log;
			this.notify = notify;
			this.$q = $q;

			// set cost
			this.cost = 14;

			this.setHeaders();
			this.refresh();
		};

		Model.prototype = Object.create(TransactionalBillingIndex.prototype);

		angular.extend(Model.prototype, {
			refresh: function() {
				this.loading.show("Loading...");
				var opts = {
					repair_pricer_pre_paid_reports: true,
					repair_pricer_enabled: true,
					count: 10000
				};
				this.api.partner.all(opts).then(
					angular.bind(this, this.onRefreshSuccess),
					angular.bind(this, this.onError));
			},
			
			onRefreshSuccess: function(response) {
				this.partners = response.data.items;
				var promises = [];
				
				// reset sort and get first and last days of the month
				this.resetSort();
				this.getDays();
				
				this.partners.forEach(angular.bind(this, function(partner, index) {
					partner.stripe = partner.account.stripe_customer_id;
					var opts = {
						searchType: "aggregation",
						query: "partner_id: " + partner.id + " AND role: binder AND repair_pricer_enabled: true",
						range: "created_at",
						gte: this.firstDay,
						lte: this.lastDay,
						count: 10000,
						search: true
					};
					promises.push(
						this.api.partnerBinder.all(opts).then(
							angular.bind(this, this.getPartnerBinderSuccess, index),
							angular.bind(this, this.onError))
					);
				}));
				
				this.$q.all(promises).then(
					angular.bind(this, this.removeZeros));
			},
			
			getPartnerBinderSuccess: function(index, response) {
				this.setCountAndCost(index, response.data, this.cost);
			},
			
			removeZeros: function() {
				for(var i = 0; i < this.partners.length; i++) {
					if (this.partners[i].count === 0) {
						this.partners.splice(i, 1);
						i--;
					}
				}
				
				this.loading.close();
			}
		});

		this.model = new Model();
	}

})();