(function() {
	"use strict";

	angular.module('hb.admin.transactions', [
			'hb.admin.transactions.buildfax',
			'hb.admin.transactions.repair_pricer',
			'hb.admin.transactions.warranties'
		])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
				.state("admin.transactions", {
					url: "^/admin/transactions",
					templateUrl: "admin/transactions/transactions.tpl.html",
					controller: "AdminTransactionsCtrl",
					controllerAs: "ctrl",
					access: {
						requiresLogin: true
					}
				});
		}])
		.factory("TransactionalBillingIndex", [
			"hb.api",
			"$log",
			"Notify",
			"Loading",
			function(resources) {
				var Model = function(api, $log, notify, loading, $q) {
					this.api = api;
					this.$log = $log;
					this.notify = notify;
					this.loading = loading;
					this.$q = $q;

					this.headers = null;
					this.currentMonth = new Date();
				};

				angular.extend(Model.prototype, {
					setHeaders: function() {
						this.columnspan = 6;
						this.headerTitles = ["Partner ID", "Partner Name", "Partner Email", "Stripe ID", "Count", "Total Cost"];
						this.headers = this.headerTitles.map(angular.bind(this, function(attribute) {
							var value = null;
							if (attribute === "Partner ID") {
								value = "id";
							}
							if (attribute === "Partner Name") {
								value = "contact";
							}
							if (attribute === "Partner Email") {
								value = "email";
							}
							if (attribute === "Stripe ID") {
								value = "stripe";
							}
							if (attribute === "Count") {
								value = "count";
							}
							if (attribute === "Total Cost") {
								value = "cost";
							}
							return {
								name: attribute,
								sorted: false,
								sortable: true,
								order: "desc",
								value: value
							};
						}));
					},

					getDays: function() {
						if (!this.currentMonth) {
							this.currentMonth = new Date();
						}
						this.firstDay = new Date(this.currentMonth.getFullYear(), this.currentMonth.getMonth(), 1).toLocaleDateString("en-us");
						this.lastDay = new Date(this.currentMonth.getFullYear(), this.currentMonth.getMonth() + 1, 0).toLocaleDateString("en-us");
					},

					resetSort: function() {
						if (this.sortOption) {
							this.sortOption.sorted = false;
							this.sortOption = null;
						}
					},

					onRefreshSuccess: function(response) {
						this.buckets = response.data;

						var promises = [];
						this.buckets.forEach(angular.bind(this, function(bucket) {
							promises.push(
								this.api.partner.get(bucket.key).then(
									angular.bind(this, this.getPartnerSuccess),
									angular.bind(this, this.onError))
							);
						}));

						this.$q.all(promises).then(
							angular.bind(this, this.allPartnersSuccess)
						);

					},

					getPartnerSuccess: function(response) {
						var partner = response.data;
						partner.stripe = partner.account.stripe_customer_id;
						this.partners.push(partner);
					},

					allPartnersSuccess: function(response) {
						this.partners.forEach(angular.bind(this, function(partner, partnerIndex) {
							this.buckets.forEach(angular.bind(this, function(bucket, reportIndex) {
								if (partner.id === bucket.key) {
									this.setCountAndCost(partnerIndex, bucket.doc_count, this.cost);
								}
							}));
						}));
						this.loading.close();
					},

					setCountAndCost: function(index, count, cost) {
						this.partners[index].count = count;
						this.partners[index].cost = count * cost;
					},

					// sorts table by header attribute that was clicked in the DOM
					sortTable: function(attribute) {
						if (!this.sortOption) {
							this.sortOption = attribute;
							this.sortOption.sorted = true;
						}
						if (this.sortOption.name === attribute.name) {
							attribute.order = attribute.order === "asc" ? "desc" : "asc";
							this.sortOption = attribute;
						}
						else {
							this.sortOption.sorted = false;
							attribute.sorted = true;
							this.sortOption = attribute;
						}
						this.sort();
					},

					sort: function() {
						if (this.sortOption.order === "desc") {
							this.partners.sort(angular.bind(this, this.descending));
						}
						else if (this.sortOption.order === "asc") {
							this.partners.sort(angular.bind(this, this.ascending));
						}
					},

					ascending: function(a, b) {
						if (a[this.sortOption.value] < b[this.sortOption.value]) {
							return -1;
						}
						if (a[this.sortOption.value] > b[this.sortOption.value]) {
							return 1;
						}
						return 0;
					},

					descending: function(a, b) {
						if (a[this.sortOption.value] < b[this.sortOption.value]) {
							return 1;
						}
						if (a[this.sortOption.value] > b[this.sortOption.value]) {
							return -1;
						}
						return 0;
					},

					onError: function(response) {
						this.$log.error(response);
						this.notify.error(response.data);
						this.loading.close();
					}
				});
				return Model;
			}
		])
		.controller("AdminTransactionsCtrl", [
			AdminTransactionsCtrl
		]);

	function AdminTransactionsCtrl() {
		this.navigation_links = [{
			name: "Transactions",
			links: [
				{ name: "Repair Pricer", value: "repair_pricer", active: true },
				{ name: "Warranties", value: "warranties", active: false },
				{ name: "BuildFax", value: "buildfax", active: false }
			]
		}];
		this.currentLink = this.navigation_links[0].links[0];
	}

	AdminTransactionsCtrl.prototype = {
		changeLink: function(link) {
			this.currentLink.active = false;
			link.active = true;
			this.currentLink = link;
		}
	};
})();