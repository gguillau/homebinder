(function() {
	"use strict";

	angular
		.module("hb.admin.transactions.buildfax", [])
		.directive("adminBuildfaxTransactions", function() {
			return {
				restrict: "E",
				templateUrl: "admin/transactions/buildfax/buildfax.tpl.html",
				controller: "AdminBuildFaxTransactionsController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("AdminBuildFaxTransactionsController", [
			"TransactionalBillingIndex",
			"hb.api",
			"Notify",
			"$log",
			"Notify",
			"Loading",
			"hb.resources",
			"$q",
			AdminBuildFaxTransactionsController
		]);

	function AdminBuildFaxTransactionsController(TransactionalBillingIndex, api, Notify, $log, notify, loading, resources, $q) {
		var Model = function() {
			// call parent
			TransactionalBillingIndex.call(this);
			this.api = api;
			this.resources = angular.extend({}, this.resources, resources.buildFaxReportIndex);
			this.loading = loading;
			this.$log = $log;
			this.notify = notify;
			this.$q = $q;

			// set cost
			this.cost = 4.50;

			this.setHeaders();
			this.refresh();
		};

		Model.prototype = Object.create(TransactionalBillingIndex.prototype);

		angular.extend(Model.prototype, {
			// Refresh callback. Updates the item list and totals
			refresh: function() {
				this.loading.show("Loading...");
				this.partners = [];

				// reset sort and get first and last days of the month
				this.resetSort();
				this.getDays();

				this.api.buildFaxReport.all({
					searchType: "aggregation_field",
					range: "created_at",
					gte: this.firstDay,
					lte: this.lastDay,
					count: 100,
					field: "partner_id",
					search: true
				}).then(
					angular.bind(this, this.onRefreshSuccess),
					angular.bind(this, this.onError));
			}
		});

		this.model = new Model();
	}

})();