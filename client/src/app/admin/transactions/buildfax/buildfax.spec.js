describe("AdminBuildFaxTransactionsController", function() {
    var controller,
        ctrl,
        TransactionalBillingIndex,
        api,
        resources,
        loading,
        $q;

    // load the homebinder module
    beforeEach(module("homebinder"));

    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        controller = $controller;
        TransactionalBillingIndex = $injector.get("TransactionalBillingIndex");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
        $q = $injector.get("$q");

        spyOn(api.buildFaxReport, "all").and.returnValue($q.when({
            data: [{ key: 1, doc_counts: 1 }]
        }));
    }));

    function createController() {
        ctrl = controller("AdminBuildFaxTransactionsController", {
            "TransactionalBillingIndex": TransactionalBillingIndex,
            "hb.api": api,
            "hb.resources": resources,
            "Loading": loading,
            "$q": $q
        });
    }

    describe("init", function() {
        it("sets the cost", function() {
            createController();
            expect(ctrl.model.cost).toEqual(4.50);
        });
    });

    describe("refresh", function() {
        it("calls buildfax api", function() {
            createController();
            spyOn(loading, "show");
            spyOn(ctrl.model, "resetSort");
            spyOn(ctrl.model, "getDays");

            ctrl.model.refresh();

            expect(loading.show).toHaveBeenCalled();
            expect(ctrl.model.resetSort).toHaveBeenCalled();
            expect(ctrl.model.getDays).toHaveBeenCalled();
            expect(api.buildFaxReport.all).toHaveBeenCalled();
        });
    });
});

describe('adminBuildfaxTransactions', function() {
    var $scope, $compile, element, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_, _$q_) {
        api = _$injector_.get("hb.api");
        $q = _$q_;
        spyOn(api.partner, "all").and.returnValue($q.when({}));
        spyOn(api.buildFaxReport, "all").and.returnValue($q.when({}));
        $scope = $rootScope.$new(), $compile = _$compile_;

        element = $compile('<admin-buildfax-transactions></admin-buildfax-transactions>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the admin-buildfax-transactions directive', function() {
        it('displays the title', function() {
            expect(element.html()).toContain("BuildFax Building Permit Reports");
        });
    });
});