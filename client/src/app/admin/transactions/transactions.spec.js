describe("TransactionalBillingIndex", function() {
    var TransactionalBillingIndex,
        TestClass,
        api,
        $log,
        notify,
        loading,
        $q;

    // load the homebinder module
    beforeEach(module("homebinder"));

    // get the base class under test
    beforeEach(inject(function($injector) {
        TransactionalBillingIndex = $injector.get("TransactionalBillingIndex");
        api = $injector.get("hb.api");
        $log = $injector.get("$log");
        notify = $injector.get("Notify");
        loading = $injector.get("Loading");
        $q = $injector.get("$q");
    }));

    function createTestClass() {
        TestClass = function() {
            TransactionalBillingIndex.call(this);
            this.api = api;
            this.$log = $log;
            this.notify = notify;
            this.loading = loading;
            this.$q = $q;
        };

        TestClass.prototype = Object.create(TransactionalBillingIndex.prototype);

        return new TestClass();
    }

    describe("setHeaders", function() {
        it("sets the headers", function() {
            var model = createTestClass();

            var headerTitles = ["Partner ID", "Partner Name", "Partner Email", "Stripe ID", "Count", "Total Cost"];
            var headers = [{
                name: "Partner ID",
                sorted: false,
                sortable: true,
                order: "desc",
                value: "id"
            }, {
                name: "Partner Name",
                sorted: false,
                sortable: true,
                order: "desc",
                value: "contact"
            }, {
                name: "Partner Email",
                sorted: false,
                sortable: true,
                order: "desc",
                value: "email"
            }, {
                name: "Stripe ID",
                sorted: false,
                sortable: true,
                order: "desc",
                value: "stripe"
            }, {
                name: "Count",
                sorted: false,
                sortable: true,
                order: "desc",
                value: "count"
            }, {
                name: "Total Cost",
                sorted: false,
                sortable: true,
                order: "desc",
                value: "cost"
            }];
            model.setHeaders();

            expect(model.columnspan).toEqual(6);
            expect(model.headerTitles).toEqual(headerTitles);
            expect(model.headers).toEqual(headers);
        });
    });

    describe("getDays", function() {
        it("gets the first and last days of the month", function() {
            var model = createTestClass();

            var currentMonth = new Date();
            var firstDay = new Date(currentMonth.getFullYear(), currentMonth.getMonth(), 1).toLocaleDateString("en-us");
            var lastDay = new Date(currentMonth.getFullYear(), currentMonth.getMonth() + 1, 0).toLocaleDateString("en-us");
            model.getDays();

            expect(model.currentMonth).toEqual(currentMonth);
            expect(model.firstDay).toEqual(firstDay);
            expect(model.lastDay).toEqual(lastDay);
        });
    });

    describe("resetSort", function() {
        it("resets sortOptions", function() {
            var model = createTestClass();

            model.sortOption = {
                sorted: true
            };
            model.resetSort();

            expect(model.sortOption).toEqual(null);
        });
    });

    describe("onRefreshSuccess", function() {
        it("calls api.partner.get for each partner key", function() {
            var model = createTestClass();
            spyOn(api.partner, "get").and.returnValue($q.when({ data: { id: 1, account: { stripe_customer_id: 1 } } }));

            var response = { data: [{ key: 1 }, { key: 2 }] };
            model.onRefreshSuccess(response);

            expect(api.partner.get.calls.count()).toEqual(2);
        });
    });

    describe("getPartnerSuccess", function() {
        it("adds the partner to the array of partners", function() {
            var model = createTestClass();

            var response = { data: { account: { stripe_customer_id: 1 } } };
            model.partners = [];
            model.getPartnerSuccess(response);

            expect(model.partners.length).toEqual(1);
            expect(model.partners[0].stripe).toEqual(1);
        });
    });

    describe("allPartnersSuccess", function() {
        it("calls count and cost for each partner", function() {
            var model = createTestClass();
            spyOn(loading, "close");
            spyOn(model, "setCountAndCost");

            model.partners = [{ id: 1 }, { id: 2 }, { id: 3 }];
            model.buckets = [{ key: 1, doc_count: 1 }, { key: 2, doc_count: 2 }, { key: 3, doc_count: 3 }];
            model.allPartnersSuccess();

            expect(model.setCountAndCost.calls.count()).toEqual(3);
            expect(loading.close).toHaveBeenCalled();
        });
    });

    describe("setCountAndCost", function() {
        it("sets the count and cost for the given partner", function() {
            var model = createTestClass();

            model.partners = [{ id: 1 }, { id: 2 }, { id: 3 }];
            model.setCountAndCost(0, 10, 10);

            expect(model.partners[0]).toEqual({ id: 1, count: 10, cost: 100 });
        });
    });

    describe("sortTable", function() {
        it("sets the sortOption to the attribute and sorts the table using the given attribute", function() {
            var model = createTestClass();
            spyOn(model, "sort");

            var attribute = { name: "name", sortable: true, order: "desc" };
            model.sortTable(attribute);

            expect(model.sortOption.sorted).toEqual(true);
            expect(attribute.order).toEqual("asc");
            expect(model.sort).toHaveBeenCalled();
        });

        it("sorts the table using the given attribute", function() {
            var model = createTestClass();
            spyOn(model, "sort");

            var attribute = { name: "name", sortable: true, order: "desc" };
            model.sortOption = { name: "name" };
            model.sortTable(attribute);

            expect(attribute.order).toEqual("asc");
            expect(model.sort).toHaveBeenCalled();
        });

        it("sorts the table using the given attribute and sets the attribute as the sort option", function() {
            var model = createTestClass();
            spyOn(model, "sort");

            var attribute = { name: "id", sortable: true, order: "desc" };
            model.sortOption = { name: "name" };
            model.sortTable(attribute);

            expect(attribute.order).toEqual("desc");
            expect(model.sortOption).toEqual(attribute);
            expect(model.sort).toHaveBeenCalled();
        });
    });

    describe("sort", function() {
        it("calls descending when the order is desc", function() {
            var model = createTestClass();
            model.partners = [];
            spyOn(model.partners, "sort");

            model.sortOption = { order: "desc" };
            model.sort();

            expect(model.partners.sort).toHaveBeenCalled();
        });

        it("calls ascending when the order is asc", function() {
            var model = createTestClass();
            model.partners = [];
            spyOn(model.partners, "sort");

            model.sortOption = { order: "asc" };
            model.sort();

            expect(model.partners.sort).toHaveBeenCalled();
        });
    });

    describe("ascending", function() {
        it("returns -1 when a < b", function() {
            var model = createTestClass();

            var a = { value: 0 };
            var b = { value: 1 };
            model.sortOption = { value: "value" };
            var result = model.ascending(a, b);

            expect(result).toEqual(-1);
        });

        it("returns 1 when a > b", function() {
            var model = createTestClass();

            var a = { value: 1 };
            var b = { value: 0 };
            model.sortOption = { value: "value" };
            var result = model.ascending(a, b);

            expect(result).toEqual(1);
        });

        it("returns 0 when a = b", function() {
            var model = createTestClass();

            var a = { value: 0 };
            var b = { value: 0 };
            model.sortOption = { value: "value" };
            var result = model.ascending(a, b);

            expect(result).toEqual(0);
        });
    });

    describe("descending", function() {
        it("returns -1 when a < b", function() {
            var model = createTestClass();

            var a = { value: 0 };
            var b = { value: 1 };
            model.sortOption = { value: "value" };
            var result = model.descending(a, b);

            expect(result).toEqual(1);
        });

        it("returns -1 when a > b", function() {
            var model = createTestClass();

            var a = { value: 1 };
            var b = { value: 0 };
            model.sortOption = { value: "value" };
            var result = model.descending(a, b);

            expect(result).toEqual(-1);
        });

        it("returns -1 when a < b", function() {
            var model = createTestClass();

            var a = { value: 0 };
            var b = { value: 0 };
            model.sortOption = { value: "value" };
            var result = model.descending(a, b);

            expect(result).toEqual(0);
        });
    });

    describe("onError", function() {
        it("calls $log.error, notify.error, and loading.close", function() {
            var model = createTestClass();
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "close");

            var response = {
                data: "error"
            };
            model.onError(response);

            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.close).toHaveBeenCalled();
        });
    });

});

describe("AdminTransactionsCtrl", function() {
    var ctrl,
        controller;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
    }));

    function createController() {
        ctrl = controller("AdminTransactionsCtrl", {});
    }

    describe("init", function() {
        it("sets the title and creates 3 links", function() {
            createController();
            expect(ctrl.navigation_links[0].name).toEqual("Transactions");
            expect(ctrl.navigation_links[0].links.length).toEqual(3);
        });
    });

    describe("changeLink", function() {
        it("sets the current link", function() {
            createController();
            var link = { name: "test", value: "test", active: false };
            ctrl.changeLink(link);

            expect(link.active).toEqual(true);
        });
    });
});
