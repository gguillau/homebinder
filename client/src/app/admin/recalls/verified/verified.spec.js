describe("AdminVerifiedRecallsCtrl", function() {
    var ctrl,
        controller,
        $log,
        api,
        notify,
        resources,
        loading,
        $modal,
        $q;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");

        $modal = {
            show: function() {}
        };
    }));

    function createController() {
        ctrl = controller("AdminVerifiedRecallsCtrl", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "hb.resources": resources,
            "Loading": loading,
            "ModalService": $modal
        });
    }

    describe("onRefreshSuccess", function() {
        it("sets processed appliance recalls", function() {
            createController();
            ctrl.model.onRefreshSuccess({ data: { items: [{ recall: { details: "{\"RecallID\":234234}" } }], total: 1 } });

            expect(ctrl.model.items.length).toEqual(1);
        });
    });

    describe("setStatus", function() {
        it("calls api applianceRecall update", function() {
            spyOn(api.applianceRecall, "update").and.returnValue($q.when({ data: {} }));

            createController();
            ctrl.model.setStatus({ id: 1 }, "verified", "", "");

            expect(api.applianceRecall.update).toHaveBeenCalled();
        });
    });

    describe("sendEmail", function() {
        it("calls api applianceRecall sendEmail", function() {
            spyOn(api.applianceRecall, "sendEmail").and.returnValue($q.when({ data: {} }));

            createController();
            ctrl.model.sendEmail({ id: 1 }, "sub_category");

            expect(api.applianceRecall.sendEmail).toHaveBeenCalled();
        });
    });

    describe("updateSuccess", function() {
        it("calls notify", function() {
            spyOn(notify, "info");

            createController();
            ctrl.model.updateSuccess("", { data: {} });

            expect(notify.info).toHaveBeenCalled();
        });
    });

    describe("updateError", function() {
        it("calls notify", function() {
            spyOn(notify, "error");

            createController();
            ctrl.model.updateError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("addQueryArgs", function() {
        it("calls addArgs", function() {
            createController();
            spyOn(ctrl.model, "addArgs");
            ctrl.model.addQueryArgs();

            expect(ctrl.model.addArgs).toHaveBeenCalled();
        });
    });

    describe("addArgs", function() {
        it("adds the arg", function() {
            createController();
            ctrl.model.recallArgs["binderId"] = 1;
            ctrl.model.addArgs("binderId");

            expect(ctrl.model.queryArgs["binderId"]).toEqual(1);
        });

        it("removes the arg", function() {
            createController();
            ctrl.model.queryArgs["binderId"] = 1;
            ctrl.model.addArgs("binderId");

            expect(ctrl.model.queryArgs["binderId"]).toBe(undefined);
        });
    });
});

describe('verifiedRecalls', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.applianceRecall, "all").and.returnValue($q.when({ data: { items: [] } }));

        $compile('<verified-recalls></verified-recalls>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls api applianceRecall all', function() {
            expect(api.applianceRecall.all).toHaveBeenCalled();
        });
    });
});