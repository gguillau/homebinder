(function() {
	"use strict";

	angular.module("hb.admin.recalls.unverified", [])
		.directive("unverifiedRecalls", function() {
			return {
				restrict: "E",
				templateUrl: 'admin/recalls/unverified/unverified.tpl.html',
				controller: 'AdminUnverifiedRecallsCtrl as ctrl',
				bindToController: true,
				scope: {}
			};
		})
		.controller("AdminUnverifiedRecallsCtrl", [
			"hb.framework.indexBase",
			"hb.api",
			"hb.resources",
			AdminUnverifiedRecallsCtrl
		]);

	function AdminUnverifiedRecallsCtrl(IndexBase, api, resources) {
		var Model = function() {
			// call the parent class
			IndexBase.call(this);
			this.resources = angular.extend({}, this.resources, resources.unverifiedRecalls);
			this.refreshCall = api.applianceRecall.all;
			this.queryArgs = {
				status: "unverified"
			};
			this.recallArgs = {
				binderId: undefined,
				applianceId: undefined
			};
			// refresh the list
			this.refresh();
		};

		Model.prototype = Object.create(IndexBase.prototype);

		angular.extend(Model.prototype, {
			// Refresh callback. Updates the item list and totals
			onRefreshSuccess: function(response) {
				this.items = response.data.items;
				this.items.forEach(function(i) {
					i.recall.details = JSON.parse(i.recall.details);
				});
				this.totalItems = this.toolbarCfg.total = response.data.total;
				this.loading.close();
			},

			setStatus: function(appliance_recall, status, category, sub_category) {
				this.loading.show("Updating...");
				var recall = {
					id: appliance_recall.id,
					status: status,
					category: category,
					sub_category: sub_category
				};
				api.applianceRecall.update(recall).then(
					angular.bind(this, this.updateSuccess, appliance_recall, sub_category, "Updated"),
					angular.bind(this, this.updateError));
			},

			sendEmail: function(appliance_recall, sub_category) {
				api.applianceRecall.sendEmail(appliance_recall.id).then(
					angular.bind(this, this.updateSuccess, appliance_recall, null, "Email sent."),
					angular.bind(this, this.updateError));
			},

			updateSuccess: function(appliance_recall, sub_category, msg, response) {
				if (sub_category === "pending") {
					this.sendEmail(appliance_recall, sub_category);
				}
				else {
					var index = this.items.indexOf(appliance_recall);
					this.items.splice(index, 1);
					this.notify.info(msg);
					this.loading.close();
				}
			},

			updateError: function(response) {
				this.$log.error(response);
				this.notify.error(response.data);
				this.loading.close();
			},

			addQueryArgs: function() {
				this.addArgs("binderId");
				this.addArgs("applianceId");
			},

			addArgs: function(arg) {
				if (this.recallArgs[arg]) {
					this.queryArgs[arg] = this.recallArgs[arg];
				}
				else {
					delete this.queryArgs[arg];
				}
			}
		});
		this.model = new Model();
	}
})();