describe("AdminRecallsFormCtrl", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        notify,
        $log,
        AdminRecallsFormService,
        $modalInstance,
        data,
        api,
        $httpBackend;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $log = _$injector_.get("$log");
        notify = _$injector_.get("Notify");
        AdminRecallsFormService = _$injector_.get("AdminRecallsFormService");
        $httpBackend = _$injector_.get("$httpBackend");

        $modalInstance = {
            dismiss: function() {},
            close: function() {}
        };

        data = {
            id: 500,
            models: []
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("AdminRecallsFormCtrl", {
            "data": data,
            "$log": $log,
            "Notify": notify,
            "AdminRecallsFormService": AdminRecallsFormService,
            "$modalInstance": $modalInstance,
            "hb.api": api
        });
        $scope.ctrl = ctrl;
    }

    beforeEach(function() {
        spyOn(api.recall, "get").and.returnValue($q.when({ data: { id: 1, details: "{\"RecallID\":234234}" } }));
    });

    describe('ctrl.getError', function() {
        it('calls notify', function() {

            spyOn(notify, "error");

            createController();
            ctrl.getError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();

        });
    });

    describe('ctrl.ignore', function() {
        it('should call updateRecall function', function() {
            createController();
            spyOn(ctrl, "updateRecall");
            $rootScope.$apply();
            ctrl.ignore();
            $rootScope.$apply();

            expect(ctrl.updateRecall).toHaveBeenCalled();
        });
    });

    describe('ctrl.saveModels', function() {
        it('should call updateRecall function', function() {
            createController();
            spyOn(ctrl, "updateRecall");
            $rootScope.$apply();
            ctrl.saveModels();
            $rootScope.$apply();

            expect(ctrl.updateRecall).toHaveBeenCalled();
        });
    });

    describe('ctrl.cancel', function() {
        it('should call $modalInstance dismiss function', function() {
            spyOn($modalInstance, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.cancel();
            $rootScope.$apply();

            expect($modalInstance.dismiss).toHaveBeenCalled();
        });
    });

    describe('ctrl.updateRecall', function() {
        it('should successfully update recall', function() {
            spyOn(AdminRecallsFormService, "updateRecall").and.returnValue($q.when("success"));
            spyOn($modalInstance, "close");

            createController();
            $rootScope.$apply();
            ctrl.updateRecall(ctrl.recall);
            $rootScope.$apply();

            expect(AdminRecallsFormService.updateRecall).toHaveBeenCalledWith(ctrl.recall);
            expect($modalInstance.close).toHaveBeenCalled();
        });

        it('should attempt to update recall and return an error', function() {
            spyOn(AdminRecallsFormService, "updateRecall").and.returnValue($q.reject("error"));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            $rootScope.$apply();
            ctrl.updateRecall(ctrl.recall);
            $rootScope.$apply();

            expect(AdminRecallsFormService.updateRecall).toHaveBeenCalledWith(ctrl.recall);
            expect(notify.error).toHaveBeenCalledWith("Error updating recall");
            expect($log.error).toHaveBeenCalledWith("error");
        });
    });

    describe("AdminRecallsFormService", function() {
        describe('updateRecall', function() {
            it('should call updateRecall function', function() {
                var response = { data: { name: "test" } };

                $httpBackend.expectPUT("/api/v1/recalls/1")
                    .respond(response);

                AdminRecallsFormService.updateRecall({ id: 1 }).then(function(response) {
                    expect(response.data).toEqual(response.data);
                });

                $httpBackend.flush();

            });
        });

        describe('onUpdateRecallSuccess', function() {
            it('returns the response', function() {
                var response = AdminRecallsFormService.onUpdateRecallSuccess({ data: { id: 1 } });
                expect(response).toEqual({ id: 1 });
            });
        });

        describe('onUpdateRecallError', function() {
            it('calls $log', function() {
                spyOn($log, "error");

                AdminRecallsFormService.onUpdateRecallError({ data: "error" });

                expect($log.error).toHaveBeenCalled();
            });
        });
    });

});