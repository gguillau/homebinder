(function() {
	"use strict";

	angular.module("hb.admin.recalls.form", [
			"ui.bootstrap",
			"hb.components"
		])
		.controller("AdminRecallsFormCtrl", [
			"AdminRecallsFormService",
			"Notify",
			'$modalInstance',
			'data',
			'$log',
			"hb.resources",
			"hb.api",
			AdminRecallsFormCtrl
		])
		.service("AdminRecallsFormService", [
			"$http",
			"$log",
			AdminRecallsFormService
		]);

	function AdminRecallsFormService($http, $log) {
		this.$http = $http;
		this.$log = $log;
	}

	AdminRecallsFormService.prototype = {

		updateRecall: function(recall) {
			return this.$http
				.put("/api/v1/recalls/" + recall.id, {
					recall: recall
				})
				.then(
					angular.bind(this, this.onUpdateRecallSuccess),
					angular.bind(this, this.onUpdateRecallError)
				);
		},

		onUpdateRecallSuccess: function(response) {
			return response.data;
		},

		onUpdateRecallError: function(error) {
			this.$log.error(error.data);
		}
	};

	function AdminRecallsFormCtrl(Service, Notify, $modalInstance, data, $log, resources, api) {
		this.service = Service;
		this.notify = Notify;
		this.$modalInstance = $modalInstance;
		this.recall = data;
		this.$log = $log;
		this.resources = resources.recallForm;
		this.api = api;
		this.url_regex = /^((?:http|ftp)s?:\/\/)(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|localhost|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(?::\d+)?(?:\/?|[\/?]\S+)$/i;
		this.refresh();
	}

	AdminRecallsFormCtrl.prototype = {

		refresh: function() {
			this.api.recall.get(this.recall.id).then(
				angular.bind(this, this.getSuccess),
				angular.bind(this, this.getError));
		},

		getSuccess: function(response) {
			this.recall = response.data;
			this.recall.details = JSON.parse(response.data.details);
		},

		getError: function(response) {
			this.notify.error(response.data);
		},

		ignore: function() {
			var r = {
				id: this.recall.id,
				ignore: true,
				verified: true
			};
			this.updateRecall(r);
		},

		saveModels: function() {
			var r = {
				id: this.recall.id,
				models: this.recall.models,
				verified: true,
				details: JSON.stringify(this.recall.details)
			};

			this.updateRecall(r);
		},

		cancel: function() {
			this.$modalInstance.dismiss();
		},

		updateRecall: function(recall) {
			this.service
				.updateRecall(recall)
				.then(
					angular.bind(this, this.onUpdateRecallSuccess),
					angular.bind(this, this.onUpdateRecallError)
				);
		},

		onUpdateRecallSuccess: function(data) {
			this.$modalInstance.close();
		},

		onUpdateRecallError: function(data) {
			this.$log.error(data);
			this.notify.error(this.resources.updateError);
		}
	};
})();