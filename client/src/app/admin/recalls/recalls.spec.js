describe("RecallsController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        notify,
        $log,
        modal,
        loading,
        api;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        loading = _$injector_.get("Loading");
        $log = _$injector_.get("$log");
        notify = _$injector_.get("Notify");

        modal = {
            show: function() {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("RecallsController", {
            "$log": $log,
            "Notify": notify,
            "ModalService": modal,
            "Loading": loading
        });
        $scope.ctrl = ctrl;

    }

    describe('ctrl.changeLink', function() {
        it('sets the current link', function() {
            createController();
            var link = { active: false };
            ctrl.changeLink(link);

            expect(link.active).toBe(true);

        });

    });

    describe('ctrl.showKeywords', function() {
        it('should show recall keywords', function() {
            spyOn(modal, "show");

            createController();
            ctrl.showKeywords();

            expect(modal.show).toHaveBeenCalled();
        });

    });

    describe('ctrl.downloadRecalls', function() {
        it('should successfully downloadRecalls', function() {
            spyOn(api.recall, "download").and.returnValue($q.when({ data: null }));

            createController();
            ctrl.downloadRecalls();

            expect(api.recall.download).toHaveBeenCalled();

        });

    });

    describe('ctrl.runService', function() {
        it('should run recall check', function() {
            spyOn(api.recall, "runService").and.returnValue($q.when({ data: null }));

            createController();
            ctrl.runService();

            expect(api.recall.runService).toHaveBeenCalled();
        });
    });

    describe('ctrl.onDownloadSuccess', function() {
        it('calls notify', function() {
            spyOn(notify, "info");

            createController();
            ctrl.onDownloadSuccess();

            expect(notify.info).toHaveBeenCalled();
        });
    });

    describe('ctrl.onRunServiceSuccess', function() {
        it('calls notify', function() {
            spyOn(notify, "info");

            createController();
            ctrl.onRunServiceSuccess();

            expect(notify.info).toHaveBeenCalled();
        });
    });

    describe('ctrl.onError', function() {
        it('calls notify', function() {
            spyOn(notify, "error");

            createController();
            ctrl.onError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

});