(function() {
	"use strict";

	angular.module("hb.admin.recalls.keywords", [
			"ui.router",
			"ui.bootstrap",
			"hb.components"
		])
		.controller("KeywordsCtrl", [
			"KeywordsService",
			"Notify",
			"ModalService",
			"$modalInstance",
			"$log",
			"hb.resources",
			KeywordsCtrl
		])
		.service("KeywordsService", [
			"$http",
			"$log",
			KeywordsService
		]);

	function KeywordsService($http, $log) {
		this.$http = $http;
		this.$log = $log;
	}

	KeywordsService.prototype = {

		getKeywords: function() {
			return this.$http
				.get("/api/v1/recall_keywords")
				.then(
					angular.bind(this, this.onGetKeywordsSuccess),
					angular.bind(this, this.onGetKeywordsError)
				);
		},

		onGetKeywordsSuccess: function(response) {
			return response.data;
		},

		onGetKeywordsError: function(error) {
			this.$log.error(error.data);
		},

		addKeyword: function(value) {
			return this.$http
				.post("/api/v1/recall_keywords", {
					recall_keyword: {
						keyword: value
					}
				})
				.then(
					angular.bind(this, this.onAddKeywordSuccess),
					angular.bind(this, this.onAddKeywordError)
				);
		},

		onAddKeywordSuccess: function(response) {
			return response.data;
		},

		onAddKeywordError: function(error) {
			this.$log.error(error.data);
		},

		deleteKeyword: function(id) {
			return this.$http
				/*jshint -W024*/
				.delete("/api/v1/recall_keywords/" + id)
				.then(
					undefined,
					angular.bind(this, this.onAddKeywordError)
				);
		}
	};

	function KeywordsCtrl(KeywordService, Notify, ModalService, $modalInstance, $log, resources) {
		this.service = KeywordService;
		this.notify = Notify;
		this.modals = ModalService;
		this.$modalInstance = $modalInstance;
		this.$log = $log;
		this.resources = resources.recallKeyword;
		this.form = {};
		this.keywords = [];
		this.refresh();
	}

	KeywordsCtrl.prototype = {

		refresh: function() {
			this.service
				.getKeywords()
				.then(
					angular.bind(this, this.onRefreshSuccess),
					angular.bind(this, this.onRefreshError)
				);
		},

		onRefreshSuccess: function(data) {
			this.keywords = data.items;
		},

		onRefreshError: function(data) {
			this.$log.error(data);
			this.notify.error(data);
		},

		findKeyword: function(keyword) {
			var word;
			for (var k = 0, len = this.keywords.length; k < len; k++) {
				word = this.keywords[k];
				if (word.keyword.toLowerCase() == keyword.toLowerCase()) {
					return k;
				}
			}
			return -1;
		},

		add: function() {
			var new_word = this.form.keyword;
			if (this.findKeyword(new_word) == -1) {
				this.service
					.addKeyword(new_word)
					.then(
						angular.bind(this, this.onAddSuccess),
						angular.bind(this, this.onAddError)
					);
			}
			else {
				this.notify.info(this.resources.keywordExists);
			}
		},

		onAddSuccess: function(data) {
			this.keywords.push(data);
			this.form.keyword = undefined;
		},

		onAddError: function(data) {
			this.notify.error(this.resources.addError);
			this.$log.error(data);
		},

		remove: function(keyword) {
			this.$modalInstance.dismiss();
			this.modals.confirm({
				glyphicon: "glyphicon glyphicon-trash",
				message: this.resources.removeText + " " + keyword.keyword + "?",
				confirm: angular.bind(this, this.removeKeyword, keyword)
			});
		},

		removeKeyword: function(keyword) {
			this.service
				.deleteKeyword(keyword.id)
				.then(
					angular.bind(this, this.onRemoveSuccess, keyword),
					angular.bind(this, this.onRemoveError)
				);
		},

		onRemoveSuccess: function(keyword) {
			var index = this.findKeyword(keyword.keyword);
			if (index != -1) {
				this.keywords.splice(index, 1);
			}
		},

		onRemoveError: function(data) {
			this.notify.error(this.resources.deleteError);
			this.$log.error(data);
		},

		close: function() {
			this.$modalInstance.dismiss();
		}
	};
})();