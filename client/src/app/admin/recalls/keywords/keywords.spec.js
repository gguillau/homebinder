describe("KeywordsCtrl", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        notify,
        $log,
        modal,
        KeywordsService,
        $modalInstance,
        $httpBackend;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        $httpBackend = _$injector_.get("$httpBackend");
        KeywordsService = _$injector_.get("KeywordsService");
        $log = _$injector_.get("$log");

        $modalInstance = {
            dismiss: function() {}
        };

        modal = {
            confirm: function(args) {}
        };

        notify = {
            error: function(msg) {},
            info: function(msg) {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("KeywordsCtrl", {
            "$log": $log,
            "Notify": notify,
            "ModalService": modal,
            "KeywordsService": KeywordsService,
            "$modalInstance": $modalInstance
        });
        $scope.ctrl = ctrl;

    }

    describe('ctrl.refresh', function() {

        it('should successfully get recall keywords', function() {

            var keywords = [
                { name: "appliance" },
                { name: "maintenance" },
                { name: "fridge" },
                { name: "heater" },
                { name: "chimney" }
            ];

            spyOn(KeywordsService, "getKeywords").and.returnValue($q.when({ items: keywords }));

            createController();
            $rootScope.$apply();
            expect(KeywordsService.getKeywords).toHaveBeenCalled();
            expect(ctrl.keywords.length).toEqual(5);

        });

        it('should attempt to get recall keywords and return an error', function() {

            spyOn(KeywordsService, "getKeywords").and.returnValue($q.reject("error"));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            $rootScope.$apply();
            expect(KeywordsService.getKeywords).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalledWith("error");
            expect($log.error).toHaveBeenCalledWith("error");

        });

    });

    describe('ctrl.remove', function() {

        it('should call modal confirm function', function() {

            var keywords = [
                { name: "appliance", keyword: "appliance" },
                { name: "maintenance" },
                { name: "fridge" },
                { name: "heater" },
                { name: "chimney" }
            ];

            spyOn(KeywordsService, "getKeywords").and.returnValue($q.when({ items: keywords }));
            spyOn(modal, "confirm");
            spyOn($modalInstance, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.remove(keywords[0]);
            $rootScope.$apply();

            expect($modalInstance.dismiss).toHaveBeenCalled();
            expect(modal.confirm).toHaveBeenCalledWith({
                glyphicon: "glyphicon glyphicon-trash",
                message: "Remove keyword appliance?",
                confirm: jasmine.any(Function)
            });

        });

    });

    describe('ctrl.add', function() {

        it('should add new word to list of recall keywords', function() {

            var keywords = [
                { keyword: "appliance" },
                { keyword: "maintenance" },
                { keyword: "fridge" },
                { keyword: "heater" },
                { keyword: "chimney" }
            ];
            var keyword = "dryer";

            spyOn(KeywordsService, "getKeywords").and.returnValue($q.when({ items: keywords }));
            spyOn(KeywordsService, "addKeyword").and.returnValue($q.when(keyword));

            createController();
            spyOn(ctrl, "findKeyword").and.callThrough();
            $rootScope.$apply();
            ctrl.form.keyword = keyword;
            ctrl.add();
            $rootScope.$apply();
            expect(ctrl.findKeyword).toHaveBeenCalledWith(keyword);
            expect(KeywordsService.addKeyword).toHaveBeenCalledWith(keyword);
            expect(ctrl.keywords.length).toBe(6);

        });

        it('should attempt to add new word to list of recall keywords but return an error', function() {

            var keywords = [
                { keyword: "appliance" },
                { keyword: "maintenance" },
                { keyword: "fridge" },
                { keyword: "heater" },
                { keyword: "chimney" }
            ];
            var keyword = "dryer";

            spyOn(KeywordsService, "getKeywords").and.returnValue($q.when({ items: keywords }));
            spyOn(KeywordsService, "addKeyword").and.returnValue($q.reject("error"));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            spyOn(ctrl, "findKeyword").and.callThrough();
            $rootScope.$apply();
            ctrl.form.keyword = keyword;
            ctrl.add();
            $rootScope.$apply();
            expect(ctrl.findKeyword).toHaveBeenCalledWith(keyword);
            expect(KeywordsService.addKeyword).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalledWith("Unable to add keyword");
            expect($log.error).toHaveBeenCalledWith("error");

        });

        it('should notify user that keyword already exists', function() {

            var keywords = [
                { keyword: "appliance" },
                { keyword: "maintenance" },
                { keyword: "fridge" },
                { keyword: "heater" },
                { keyword: "chimney" }
            ];
            var keyword = "appliance";

            spyOn(KeywordsService, "getKeywords").and.returnValue($q.when({ items: keywords }));
            spyOn(notify, "info");

            createController();
            spyOn(ctrl, "findKeyword").and.callThrough();
            $rootScope.$apply();
            ctrl.form.keyword = keyword;
            ctrl.add();
            $rootScope.$apply();
            expect(ctrl.findKeyword).toHaveBeenCalledWith(keyword);
            expect(notify.info).toHaveBeenCalledWith("Keyword already exists.");

        });

    });

    describe('ctrl.removeKeyword', function() {

        it('should succesfully remove keyword', function() {

            var keywords = [
                { id: 1, keyword: "appliance" },
                { id: 2, keyword: "maintenance" },
                { id: 3, keyword: "fridge" },
                { id: 4, keyword: "heater" },
                { id: 5, keyword: "chimney" }
            ];

            spyOn(KeywordsService, "getKeywords").and.returnValue($q.when({ items: keywords }));
            spyOn(KeywordsService, "deleteKeyword").and.returnValue($q.when("chimney"));

            createController();
            spyOn(ctrl, "findKeyword").and.callThrough();
            $rootScope.$apply();
            spyOn(ctrl.keywords, "splice").and.callThrough();
            ctrl.removeKeyword(ctrl.keywords[4]);
            $rootScope.$apply();

            expect(KeywordsService.deleteKeyword).toHaveBeenCalledWith(5);
            expect(ctrl.findKeyword).toHaveBeenCalledWith("chimney");
            expect(ctrl.keywords.length).toBe(4);
            expect(ctrl.keywords.splice).toHaveBeenCalledWith(4, 1);

        });

        it('should attempt to remove keyword and return an error', function() {

            var keywords = [
                { id: 1, keyword: "appliance" },
                { id: 2, keyword: "maintenance" },
                { id: 3, keyword: "fridge" },
                { id: 4, keyword: "heater" },
                { id: 5, keyword: "chimney" }
            ];

            spyOn(KeywordsService, "getKeywords").and.returnValue($q.when({ items: keywords }));
            spyOn(KeywordsService, "deleteKeyword").and.returnValue($q.reject("error"));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            $rootScope.$apply();
            ctrl.removeKeyword(ctrl.keywords[4]);
            $rootScope.$apply();

            expect(KeywordsService.deleteKeyword).toHaveBeenCalledWith(5);
            expect(notify.error).toHaveBeenCalledWith("Unable to delete keyword");
            expect($log.error).toHaveBeenCalledWith("error");

        });

    });

    describe('ctrl.findKeyword', function() {

        it('should find keyword and return index', function() {

            var keywords = [
                { keyword: "appliance" },
                { keyword: "maintenance" },
                { keyword: "fridge" },
                { keyword: "heater" },
                { keyword: "chimney" }
            ];
            var keyword = "chimney";

            spyOn(KeywordsService, "getKeywords").and.returnValue($q.when({ items: keywords }));

            createController();
            $rootScope.$apply();
            var index = ctrl.findKeyword(keyword);
            $rootScope.$apply();

            expect(index).toBe(4);

        });

    });

    describe('ctrl.close', function() {

        it('should call modalinstance close method', function() {

            var keywords = [
                { keyword: "appliance" },
                { keyword: "maintenance" },
                { keyword: "fridge" },
                { keyword: "heater" },
                { keyword: "chimney" }
            ];

            spyOn(KeywordsService, "getKeywords").and.returnValue($q.when({ items: keywords }));
            spyOn($modalInstance, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.close();
            $rootScope.$apply();

            expect($modalInstance.dismiss).toHaveBeenCalled();

        });

    });

    describe("KeywordsService", function() {
        describe('getKeywords', function() {
            it('should call getKeywords function', function() {
                var response = { data: [] };

                $httpBackend.expectGET("/api/v1/recall_keywords")
                    .respond(response);

                KeywordsService.getKeywords().then(function(response) {
                    expect(response.data).toEqual(response.data);
                });

                $httpBackend.flush();

            });
        });

        describe('onGetKeywordsSuccess', function() {
            it('returns the response', function() {
                var response = KeywordsService.onGetKeywordsSuccess({ data: { id: 1 } });
                expect(response).toEqual({ id: 1 });
            });
        });

        describe('onGetKeywordsError', function() {
            it('calls $log', function() {
                spyOn($log, "error");

                KeywordsService.onGetKeywordsError({ data: "error" });

                expect($log.error).toHaveBeenCalled();
            });
        });

        describe('addKeyword', function() {
            it('should call getKeywords function', function() {
                var response = { data: [] };

                $httpBackend.expectPOST("/api/v1/recall_keywords")
                    .respond(response);

                KeywordsService.addKeyword("word").then(function(response) {
                    expect(response.data).toEqual(response.data);
                });

                $httpBackend.flush();

            });
        });

        describe('onAddKeywordSuccess', function() {
            it('returns the response', function() {
                var response = KeywordsService.onAddKeywordSuccess({ data: { id: 1 } });
                expect(response).toEqual({ id: 1 });
            });
        });

        describe('onAddKeywordError', function() {
            it('calls $log', function() {
                spyOn($log, "error");

                KeywordsService.onAddKeywordError({ data: "error" });

                expect($log.error).toHaveBeenCalled();
            });
        });

        describe('deleteKeyword', function() {
            it('should call deleteKeyword function', function() {
                var response = { data: [] };

                $httpBackend.expectDELETE("/api/v1/recall_keywords/1")
                    .respond(response);

                KeywordsService.deleteKeyword(1).then(function(response) {
                    expect(response.data).toEqual(response.data);
                });

                $httpBackend.flush();

            });
        });
    });

});