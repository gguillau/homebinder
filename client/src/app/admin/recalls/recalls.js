(function() {
	"use strict";

	angular.module("hb.admin.recalls", [
			"hb.admin.recalls.unprocessed",
			"hb.admin.recalls.processed",
			"hb.admin.recalls.unverified",
			"hb.admin.recalls.verified",
			"hb.admin.recalls.form",
			"hb.admin.recalls.keywords"
		])
		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("admin.recalls", {
					url: "^/recalls",
					templateUrl: "admin/recalls/recalls.tpl.html",
					controller: "RecallsController",
					controllerAs: "ctrl",
					access: {
						requiresLogin: true
					}

				});
		}])
		.controller("RecallsController", [
			"ModalService",
			"hb.resources",
			"hb.api",
			"Notify",
			"$log",
			RecallsController
		]);

	function RecallsController(modals, resources, api, notify, $log) {
		this.modals = modals;
		this.resources = resources.recallIndex;
		this.api = api;
		this.notify = notify;
		this.$log = $log;
		this.navigation_links = [{
			name: "Recalls",
			links: [
				{ name: "Unprocessed", value: "unprocessed", active: false },
				{ name: "Processed", value: "processed", active: false },
				{ name: "Unverified", value: "unverified", active: true },
				{ name: "Verified", value: "verified", active: false }
			]
		}];
		this.currentLink = this.navigation_links[0].links[2];
	}

	RecallsController.prototype = {

		changeLink: function(link) {
			this.currentLink.active = false;
			link.active = true;
			this.currentLink = link;
		},

		showKeywords: function() {
			this.modals.show({
				templateUrl: "admin/recalls/keywords/keywords.tpl.html",
				controller: "KeywordsCtrl as keywordForm",
				backdrop: "static"
			});
		},

		downloadRecalls: function() {
			this.api.recall
				.download()
				.then(
					angular.bind(this, this.onDownloadSuccess),
					angular.bind(this, this.onError)
				);
		},

		runService: function() {
			this.api.recall
				.runService()
				.then(
					angular.bind(this, this.onRunServiceSuccess),
					angular.bind(this, this.onError)
				);
		},

		onDownloadSuccess: function() {
			this.notify.info(this.resources.downloadText);
		},

		onRunServiceSuccess: function(response) {
			this.notify.info(this.resources.runCheckText);
		},

		onError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
		}
	};
})();
