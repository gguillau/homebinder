describe("AdminUnprocessedRecallsCtrl", function() {
    var ctrl,
        controller,
        $log,
        api,
        notify,
        resources,
        loading,
        $modal;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");

        $modal = {
            show: function() {}
        };
    }));

    function createController() {
        ctrl = controller("AdminUnprocessedRecallsCtrl", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "hb.resources": resources,
            "Loading": loading,
            "ModalService": $modal
        });
    }

    describe("onRefreshSuccess", function() {
        it("sets processed recalls", function() {
            createController();
            ctrl.model.onRefreshSuccess({ data: { items: [{ details: "{\"RecallID\":234234}" }], total: 1 } });

            expect(ctrl.model.items.length).toEqual(1);
        });
    });

    describe("showRecall", function() {
        it("calls modal show", function() {
            spyOn($modal, "show");

            createController();
            ctrl.model.showRecall({ id: 1 });

            expect($modal.show).toHaveBeenCalled();
        });
    });

});

describe('unprocessedRecalls', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.recall, "all").and.returnValue($q.when({ data: { items: [] } }));

        $compile('<unprocessed-recalls></unprocessed-recalls>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls api recall all', function() {
            expect(api.recall.all).toHaveBeenCalled();
        });
    });
});