(function() {
	"use strict";

	angular.module("hb.admin.recalls.unprocessed", [])
		.directive("unprocessedRecalls", function() {
			return {
				restrict: "E",
				templateUrl: 'admin/recalls/unprocessed/unprocessed.tpl.html',
				controller: 'AdminUnprocessedRecallsCtrl as ctrl',
				bindToController: true,
				scope: {}
			};
		})
		.controller("AdminUnprocessedRecallsCtrl", [
			"hb.framework.indexBase",
			"hb.api",
			"ModalService",
			"hb.resources",
			AdminUnprocessedRecallsCtrl
		]);

	function AdminUnprocessedRecallsCtrl(IndexBase, api, modals, resources) {
		var Model = function() {
			// call the parent class
			IndexBase.call(this);
			this.resources = angular.extend({}, this.resources, resources.unprocessedRecalls);
			this.refreshCall = api.recall.all;
			this.queryArgs = {
				verified: false
			};
			this.orderBy = "recall_date";
			this.order = "desc";
			// refresh the list
			this.refresh();
		};

		Model.prototype = Object.create(IndexBase.prototype);

		angular.extend(Model.prototype, {
			// Refresh callback. Updates the item list and totals
			onRefreshSuccess: function(response) {
				this.items = response.data.items;
				this.items.forEach(function(i) {
					i.details = JSON.parse(i.details);
				});
				this.totalItems = this.toolbarCfg.total = response.data.total;
				this.loading.close();
			},

			showRecall: function(recall) {
				modals.show({
					templateUrl: "admin/recalls/form/form.tpl.html",
					controller: "AdminRecallsFormCtrl as recallForm",
					resolveData: recall,
					keyboard: false,
					backdrop: "static",
					closed: angular.bind(this, this.refresh)
				});
			}
		});

		this.model = new Model();
	}
})();