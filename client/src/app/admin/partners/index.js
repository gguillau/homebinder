(function() {
	"use strict";

	angular.module('hb.admin.partners', [
			"hb.admin.partners.edit"
		])
		.config(['$stateProvider', adminPartnersConfig])
		.controller("AdminPartnersIndexController", [
			"PartnersIndexController",
			AdminPartnersIndexController
		]);

	function AdminPartnersIndexController(PartnersIndexController) {

		var Model = function() {
			// call the parent class
			PartnersIndexController.call(this);
			this.refresh();
		};

		Model.prototype = Object.create(PartnersIndexController.prototype);

		angular.extend(Model.prototype);

		this.model = new Model();
	}

	function adminPartnersConfig($stateProvider) {
		$stateProvider
			.state('admin.partners', {
				url: '^/admin/partners',
				templateUrl: 'components/partners/index/index.tpl.html',
				controller: "AdminPartnersIndexController",
				controllerAs: "ctrl",
				access: {
					requiresLogin: true
				}
			});
	}

})();