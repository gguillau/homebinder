(function() {
    "use strict";

    angular.module('hb.admin.partners.edit', [])
        .config(['$stateProvider', adminPartnersEditConfig])
        .controller("AdminPartnersEditController", [
            'PartnersEditController',
            AdminPartnersEditController
        ]);

    function AdminPartnersEditController(PartnersEditController) {
        var Model = function() {
            // call the parent class
            PartnersEditController.call(this);
        };

        Model.prototype = Object.create(PartnersEditController.prototype);

        angular.extend(Model.prototype);

        this.model = new Model();
    }

    function adminPartnersEditConfig($stateProvider) {
        $stateProvider
            .state('admin.partners_edit', {
                url: '^/admin/partners/:partnerId/edit',
                templateUrl: 'components/partners/edit/edit.tpl.html',
                controller: "AdminPartnersEditController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
    }
})();
