describe("AdminPartnersEditController", function() {
    var controller,
        base,
        resources,
        $q_,
        defer,
        $scope;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getPartner: function() {
                    defer = $q_.defer();
                    defer.resolve({ settings: { navigation: {} } });
                    return defer.promise;
                }
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        $scope = $rootScope.$new();
        $q_ = $q;
        base = $injector.get("PartnersEditController");
        resources = $injector.get("hb.resources");

        controller = $controller("AdminPartnersEditController", {
            "PartnersEditController": base,
            "hb.resources": resources
        });
        $scope.$digest();

    }));

    describe("init", function() {
        it("sets the last link value", function() {
            var length = controller.model.navigation_links[0].links.length;
            expect(controller.model.navigation_links[0].links[length - 1].value).toEqual("customWidget");
        });
    });

});