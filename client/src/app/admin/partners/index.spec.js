describe("AdminPartnersIndexController", function() {
    var controller,
        base,
        resources,
        $q_;

    // load the homebinder module
    beforeEach(module("homebinder"));

    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        $q_ = $q;
        base = $injector.get("PartnersIndexController");
        resources = $injector.get("hb.resources");

        controller = $controller("AdminPartnersIndexController", {
            "PartnersIndexController": base,
            "hb.resources": resources
        });
    }));

    describe("refresh", function() {
        it("sets the controller api call", function() {
            spyOn(controller.model, "refreshCall").and.returnValue($q_.when({ data: { items: [] } }));

            controller.model.refresh();

            expect(controller.model.refreshCall).toHaveBeenCalled();
        });
    });

});