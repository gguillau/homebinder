(function() {
    "use strict";

    angular
        .module("hb.admin.contractors.form", [])
        .directive("hbContractorForm", function() {
            return {
                restrict: "E",
                templateUrl: "admin/contractors/form/form.tpl.html",
                controller: "ContractorFormController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    cfg: "=",
                    onSaved: "&",
                    onCancel: "&"
                }
            };
        })
        // Model backing the area modal form
        .controller("ContractorFormController", [
            "$log",
            "hb.api",
            "Notify",
            "Loading",
            "hb.resources",
            "Address",
            Controller
        ]);

    function Controller($log, api, notify, loading, resources, Address) {
        var that = this;
        var Model = function() {
            this.$log = $log;
            this.api = api;
            this.notify = notify;
            this.resources = resources.contractorForm;
            this.loading = loading;
            this.name = null;
            this.contact = null;
            this.email = null;
            this.url_regex = /^((?:http|ftp)s?:\/\/)(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|localhost|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(?::\d+)?(?:\/?|[\/?]\S+)$/i;
            this.populate();
        };

        angular.extend(Model.prototype, {
            populate: function() {
                this.country = Address.findCountryByCode();
                this.states = Address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                    return state.country.value === this.country.value;
                }));

                if (!that.cfg) {
                    return;
                }

                // setup the save callback
                that.cfg.api = {
                    save: angular.bind(this, this.submit)
                };

                // load the resource
                if (that.cfg.item) {
                    // name, phone, email, url, contact, details, verified
                    this.resource = that.cfg.item;
                    this.title = this.resource.name;
                    this.name = this.resource.name;
                    this.contact = this.resource.contact;
                    this.email = this.resource.email;
                    this.phone = this.resource.phone;
                    this.url = this.resource.url;
                    this.details = this.resource.details;
                    this.verified = this.resource.verified;
                }
            },

            submit: function() {
                that.form.$submitted = true;

                if (that.form.$invalid) {
                    return;
                }

                var data = {
                        name: this.name,
                        contact: this.contact,
                        email: this.email,
                        phone: "+" + this.country.code + this.phone.national,
                        url: this.url,
                        details: this.details
                    },
                    promise;

                this.error422 = false;
                this.loading.show(this.resources.saving);

                if (this.resource) {
                    promise = api.contractor.update(this.resource.id, data);
                }
                else {
                    promise = api.contractor.create(data);
                }

                promise.then(angular.bind(this, this.onResourceSaved),
                        angular.bind(this, this.onResourceSaveError))
                    /*jshint -W024*/
                    .finally(angular.bind(this, this.onDoneSaving));
            },

            onResourceSaved: function(response) {
                this.resource = response.data;
                that.cfg.resource = this.resource;
                that.onSaved({
                    item: this.resource
                });
            },

            onResourceSaveError: function(response) {
                this.notify.error(response.data);
            },

            onDoneSaving: function() {
                this.loading.close();
            }
        });

        this.model = new Model();
    }
})();