angular
    .module("hb.admin.contractors.modal", [])
    .factory("ContractorModal", [
        "hb.framework.toggleModalBase",
        "hb.resources",
        function(ModalBase, resources) {
            var instance = null;
            var Modal = function() {
                ModalBase.call(this);
                this.templateUrl = "admin/contractors/modal/modal.tpl.html";
                this.controller = "ContractorModalController as ctrl";
                this.windowClass = "widget-modal";
            };

            Modal.prototype = Object.create(ModalBase.prototype);

            if (!instance) {
                instance = new Modal();
            }

            return instance;
        }
    ])
    .controller("ContractorModalController", [
        "hb.framework.toggleModalControllerBase",
        "$modalInstance",
        "args",
        "hb.api",
        "hb.resources",
        "Notify",
        "$log",
        function(ToggleModalControllerBase, $modalInstance, args, api, resources, notify, $log) {
            var Model = function() {
                ToggleModalControllerBase.call(this);
                this.args = args;
                this.populateCall = api.contractor.get;
                this.$modalInstance = $modalInstance;
                this.objectProperty = "item";
                this.objectIdProperty = "itemId";
                this.nameProperty = "name";
                this.resources = resources.contractorForm;
                this.init();
            };

            Model.prototype = Object.create(ToggleModalControllerBase.prototype);

            this.model = new Model();
        }
    ]);