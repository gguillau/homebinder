describe("AdminContractorsCtrl", function() {
    var controller,
        base,
        api,
        resources;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");

        spyOn(api.contractor, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));
        spyOn(api.contractor, "destroy").and.returnValue($q.when({}));

        controller = $controller("AdminContractorsCtrl", {
            "hb.framework.indexBase": base,
            "hb.api": api,
            "hb.resources": resources
        });

        $rootScope.$apply();
    }));

    describe("headers", function() {
        it("sets the headers", function() {
            expect(controller.model.headers.length).toEqual(6);
        });
    });
});