(function() {
	"use strict";

	angular.module('hb.admin.contractors', [
		"hb.admin.contractors.modal",
		"hb.admin.contractors.form"
		])
		.config(['$stateProvider', adminContractorsConfig])
		.controller("AdminContractorsCtrl", [
			"hb.framework.indexBase",
			"hb.api",
			"hb.resources",
			"ContractorModal",
			AdminContractorsCtrl
		]);

	function AdminContractorsCtrl(IndexBase, api, resources, ContractorModal) {

		var Model = function() {
			// call the parent class
			IndexBase.call(this);
			this.api = api;
			// apply abs error specific resource strings
			this.resources = angular.extend({}, this.resources, resources.contractorsIndex);
			// set the refresh API call
			this.refreshCall = api.contractor.all;
			// set the delete API call
			this.deleteCall = api.contractor.destroy;
			// delete prop
			this.nameProperty = "name";
			this.maxSize = 10;
			this.sortOptions = [{
				orderBy: "contractors.id",
				order: "desc",
				desc: this.resources.creationDate + " - " + this.resources.descending
			}, {
				orderBy: "contractors.id",
				order: "asc",
				desc: this.resources.creationDate + " - " + this.resources.ascending
			}];
			this.sortOption = this.sortOptions[0];
			this.orderBy = this.sortOption.orderBy;
			this.order = this.sortOption.order;
			this.headers = null;
			this.setHeaders();
			this.refresh();
		};

		Model.prototype = Object.create(IndexBase.prototype);

		angular.extend(Model.prototype, {
			setHeaders: function() {
				this.columnspan = 6;
				this.headers = this.resources.attributes.map(angular.bind(this, function(attribute) {
					var sortable = false,
						orderBy = null,
						sorted = false,
						show = true;
					if (attribute === "Name") {
						sorted = true;
						sortable = true;
						orderBy = "contractors.name";
					}
					else if (attribute === "Email") {
						sortable = true;
						orderBy = "contractors.email";
					}
					else if (attribute === "Contact") {
						sortable = true;
						orderBy = "contractors.contact";
					}
					else if (attribute === "Phone") {
						sortable = true;
						orderBy = "contractors.phone";
					}
					else if (attribute === "Website") {
						sortable = true;
						orderBy = "contractors.url";
					}
					return {
						name: attribute,
						sortable: sortable,
						sorted: sorted,
						orderBy: orderBy,
						show: show,
						order: "desc"
					};
				}));
			},

			editItem: function(item) {
				var formOpts = {};
				formOpts.item = item;
				var index = this.items.indexOf(item);
				formOpts.onSaved = angular.bind(this, this.onEdited, index);
				ContractorModal.showForm(formOpts);
			},

			onEdited: function(index, item) {
				this.items[index] = item;
			}
		});

		this.model = new Model();
	}

	function adminContractorsConfig($stateProvider) {
		$stateProvider
			.state('admin.contractors', {
				url: '^/admin/contractors',
				templateUrl: 'admin/contractors/contractors.tpl.html',
				controller: "AdminContractorsCtrl",
				controllerAs: "ctrl",
				access: {
					requiresLogin: true
				}
			});
	}

})();