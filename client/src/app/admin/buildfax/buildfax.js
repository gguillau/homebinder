(function() {
	"use strict";

	angular
		.module("hb.admin.buildfax", [])
		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("admin.buildfax", {
					url: "^/admin/buildfax",
					templateUrl: 'partners/buildfax/buildfax.tpl.html',
					controller: 'AdminBuildFaxController',
					controllerAs: "ctrl",
					access: {
						requiresLogin: true
					}
				});
		}])
		.directive("buildFaxIndex", function() {
			return {
				restrict: "E",
				templateUrl: "partners/buildfax/buildfax.tpl.html",
				controller: "AdminBuildFaxController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("AdminBuildFaxController", [
			"$stateParams",
			"$log",
			"hb.api",
			"Notify",
			"Loading",
			"$state",
			"ModalService",
			"BuildFaxController",
			"hb.resources",
			"$window",
			"Address",
			"ModalService",
			AdminBuildFaxController
		]);

	function AdminBuildFaxController($stateParams, $log, api, notify, loading, $state, modals, IndexBase, resources, $window, Address, ModalService) {
		var Model = function() {
			// call the parent class
			IndexBase.call(this);
			this.setHeaders();
			this.refresh();
		};

		Model.prototype = Object.create(IndexBase.prototype);

		angular.extend(Model.prototype);

		this.model = new Model();
	}
})();