describe("AdminBuildFaxController", function() {
    var controller,
        base,
        resources,
        $q_,
        defer;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getPartner: function() {
                    defer = $q_.defer();
                    defer.resolve({ settings: { navigation: {} } });
                    return defer.promise;
                }
            };
        });

        // mock the session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { id: 1, role: "admin" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        $q_ = $q;
        base = $injector.get("BuildFaxController");
        resources = $injector.get("hb.resources");

        controller = $controller("AdminBuildFaxController", {
            "BuildFaxController": base,
            "hb.resources": resources
        });
    }));

    describe("refresh", function() {
        it("sets the controller api call", function() {
            spyOn(controller.model, "refreshCall").and.returnValue($q_.when({ data: {} }));

            controller.model.refresh();

            expect(controller.model.refreshCall).toHaveBeenCalled();
        });
    });

});