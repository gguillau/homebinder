describe("AdminReportsCtrl", function() {
    var ctrl,
        controller,
        $log,
        api,
        notify,
        resources,
        loading,
        $modal,
        $q;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");

        $modal = {
            show: function() {}
        };
    }));

    function createController() {
        ctrl = controller("AdminReportsCtrl", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "hb.resources": resources,
            "Loading": loading,
            "ModalService": $modal
        });
    }

    describe("changeLink", function() {
        it("sets the current link", function() {
            createController();
            var link = {name: "test", value: "test", active: false};
            ctrl.changeLink(link);

            expect(link.active).toBe(true);
        });
    });
});
