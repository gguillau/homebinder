describe("AdminUserMergesCtrl", function() {
    var controller,
        base,
        api,
        resources,
        $window,
        modal,
        notify,
        $q_;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        $window = $injector.get("$window");
        modal = $injector.get("ModalService");
        notify = $injector.get("Notify");
        $q_ = $q;

        controller = $controller("AdminUserMergesCtrl", {
            "hb.framework.indexBase": base,
            "hb.api": api,
            "hb.resources": resources,
            "$window": $window,
            "ModalService": modal,
            "Notify": notify
        });

        $rootScope.$apply();
    }));

    describe("revertConfirm", function() {
        it("calls modal confirm", inject(function($window) {
            spyOn(modal, "confirm");

            controller.model.revertConfirm({});

            expect(modal.confirm).toHaveBeenCalled();
        }));
    });

    describe("revert", function() {
        it("calls api revert", inject(function($window) {
            spyOn(api.userMerge, "revert").and.returnValue($q_.when({ data: {} }));

            controller.model.revert({ id: 1 });

            expect(api.userMerge.revert).toHaveBeenCalled();
        }));
    });

    describe("onRevertSuccess", function() {
        it("updates the merge and calls notify", inject(function($window) {
            spyOn(notify, "info");

            var merge = { id: 1, status: "merged" };
            controller.model.onRevertSuccess(merge, {});

            expect(merge.status).toEqual("reverting");
            expect(notify.info).toHaveBeenCalled();
        }));
    });

    describe("onRevertError", function() {
        it("calls api revert", inject(function($window) {
            spyOn(notify, "error");

            controller.model.onRevertError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        }));
    });
});