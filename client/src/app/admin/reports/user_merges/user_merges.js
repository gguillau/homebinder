(function() {
	"use strict";

	angular.module('hb.admin.reports.user_merges', [])
		.directive("hbUserMerges", function() {
			return {
				restrict: "E",
				templateUrl: "admin/reports/user_merges/user_merges.tpl.html",
				controller: "AdminUserMergesCtrl",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("AdminUserMergesCtrl", [
			"hb.framework.indexBase",
			"hb.api",
			"hb.resources",
			"$window",
			"ModalService",
			"Notify",
			AdminUserMergesCtrl
		]);

	function AdminUserMergesCtrl(IndexBase, api, resources, $window, modal, notify) {

		var Model = function() {
			// call the parent class
			IndexBase.call(this);
			this.api = api;
			// apply abs error specific resource strings
			this.resources = angular.extend({}, this.resources, resources.userMergeIndex);
			// set the refresh API call
			this.refreshCall = api.userMerge.all;
			// set the delete API call
			this.deleteCall = api.userMerge.destroy;
			// delete prop
			this.nameProperty = "old_agent_email";
			this.maxSize = 10;
			this.sortOptions = [{
				orderBy: "user_merges.created_at",
				order: "desc",
				desc: this.resources.creationDate + " - " + this.resources.descending
			}, {
				orderBy: "user_merges.created_at",
				order: "asc",
				desc: this.resources.creationDate + " - " + this.resources.ascending
			}];
			this.sortOption = this.sortOptions[0];
			this.orderBy = this.sortOption.orderBy;
			this.order = this.sortOption.order;
			this.headers = null;
			this.setHeaders();
			this.refresh();
		};

		Model.prototype = Object.create(IndexBase.prototype);

		angular.extend(Model.prototype, {
			setHeaders: function() {
				this.columnspan = 10;
				this.headers = this.resources.attributes.map(angular.bind(this, function(attribute) {
					var sortable = false,
						orderBy = null,
						sorted = false,
						show = true;
					if (attribute === "Date") {
						sorted = true;
						sortable = true;
						orderBy = "user_merges.created_at";
					}
					else if (attribute === "User") {
						sortable = true;
						orderBy = "user_merges.user_id";
					}
					else if (attribute === "Old Agent") {
						sortable = true;
						orderBy = "user_merges.old_agent_email";
					}
					else if (attribute === "Status") {
						sortable = true;
						orderBy = "user_merges.status";
					}
					return {
						name: attribute,
						sortable: sortable,
						sorted: sorted,
						orderBy: orderBy,
						show: show,
						order: "desc"
					};
				}));
			},

			revertConfirm: function(merge) {
				modal.confirm({
					message: this.resources.confirmRevert,
					glyphicon: "glyphicon glyphicon-question-sign",
					title: "Confirm Creation",
					confirm: angular.bind(this, this.revert, merge)
				});
			},
			revert: function(merge) {
				api.userMerge.revert(merge.id).then(
					angular.bind(this, this.onRevertSuccess, merge),
					angular.bind(this, this.onRevertError));
			},

			onRevertSuccess: function(merge, response) {
				notify.info(this.resources.success);
				merge.status = "reverting";
			},

			onRevertError: function(response) {
				notify.error(response.data);
			}
		});

		this.model = new Model();
	}

})();