describe("AdminRepairPricerEditFormController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        opts,
        $modal,
        api,
        notify,
        $log,
        loading,
        address,
        $q,
        AdminRepairPricerEditForm,
        ModalService;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$templateCache_, _$compile_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        address = _$injector_.get("Address");
        AdminRepairPricerEditForm = _$injector_.get("AdminRepairPricerEditForm");
        ModalService = _$injector_.get("ModalService");
        loading = _$injector_.get("Loading");
        api = _$injector_.get("hb.api");
        notify = _$injector_.get("Notify");
        loading = _$injector_.get("Loading");
        ModalService = _$injector_.get("ModalService");
        $log = _$injector_.get("$log");
        
        opts = {
            report: {
                id: 1,
                client_phone: {
                    
                },
                address: {
                    id: 1,
                    country: "US"
                }
            }
        };

        $modal = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("AdminRepairPricerEditFormController", {
            "$modalInstance": $modal,
            "Address": address,
            "data": opts,
            "hb.api": api,
            "Notify": notify,
            "$log": $log,
            "Loading": loading
        });

        $scope.ctrl = ctrl;
    }

    describe('ctrl.submitForm', function() {
        it("should submit the form and call the update binder function", function() {

            spyOn(api.repairPricerReport, "update").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(notify, "success");
            spyOn($modal, "close");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();
            ctrl.submitForm();
            $rootScope.$apply();

            expect(api.repairPricerReport.update).toHaveBeenCalled();
            expect(loading.show).toHaveBeenCalledWith("Updating report...");
            expect(loading.close).toHaveBeenCalled();
            expect(notify.success).toHaveBeenCalledWith("Report updated.");
            expect($modal.close).toHaveBeenCalled();

        });

        it("should submit the form but return an error", function() {
            spyOn(api.repairPricerReport, "update").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();
            ctrl.submitForm();
            $rootScope.$apply();

            expect(api.repairPricerReport.update).toHaveBeenCalled();
            expect(loading.show).toHaveBeenCalledWith("Updating report...");
            expect(loading.close).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");

        });
    });

    describe("ctrl.cancel", function() {

        it("should call $modalInstance dismiss function", function() {

            spyOn($modal, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.cancel();
            $rootScope.$apply();

            expect($modal.dismiss).toHaveBeenCalledWith('cancel');
        });

    });

    describe('ctrl.onSelectState', function() {
        it('sets the error property country', function() {
            createController();

            ctrl.onSelectState({ country: { value: "MA" } });

            expect(ctrl.country.value).toEqual("MA");
        });
    });

    describe('ctrl.onSelectCountry', function() {
        it('sets the error property country', function() {
            createController();

            ctrl.onSelectCountry({ value: "MA" });

            expect(ctrl.country.value).toEqual("MA");
        });
    });

    describe('AdminRepairPricerEditForm', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            AdminRepairPricerEditForm.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });

});