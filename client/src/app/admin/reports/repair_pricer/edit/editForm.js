(function() {
    "use strict";

    angular
        .module("hb.admin.reports.repair_pricer.edit", [])
        .factory("AdminRepairPricerEditForm", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "admin/reports/repair_pricer/edit/editForm.tpl.html",
                            controller: "AdminRepairPricerEditFormController as ctrl",
                            resolveData: opts,
                            closed: opts.closed
                        });
                    }
                };
            }
        ])
        .controller("AdminRepairPricerEditFormController", [
            "$modalInstance",
            "data",
            "Address",
            "hb.api",
            "Notify",
            "$log",
            "Loading",
            "hb.resources",
            AdminRepairPricerEditFormController
        ]);

    function AdminRepairPricerEditFormController($modal, opts, Address, api, notify, $log, loading, resources) {
        this.$modal = $modal;
        this.Address = Address;
        this.api = api;
        this.notify = notify;
        this.$log = $log;
        this.loading = loading;
        this.report = opts.report;
        this.country = Address.findCountryByCode(this.report.address.country);
        this.countries = this.Address.countries();
        this.options = {};
        this.options.countries = this.countries;
        this.options.states = this.Address.getStatesAndProvinces();
        this.temp_state = null;
        this.resources = resources.repairPricerForm;
        this.init();
    }

    AdminRepairPricerEditFormController.prototype = {

        init: function() {
            if (this.report.address.country) {
                this.options.states = this.Address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                    return state.country.value === this.report.address.country;
                }));
            }
        },

        submitForm: function() {
            var payload = {
                repair_pricer_report: {
                    client_first: this.report.client_first,
                    client_last: this.report.client_last,
                    client_email: this.report.client_email,
                    client_phone: "+" + this.country.code + this.report.client_phone.number,
                    buyer_agent_first: this.report.buyer_agent_first,
                    buyer_agent_last: this.report.buyer_agent_last,
                    buyer_agent_email: this.report.buyer_agent_email
                },
                address: {
                    address1: this.report.address.address1,
                    address2: this.report.address.address2,
                    city: this.report.address.city,
                    state: this.report.address.state,
                    country: this.report.address.country,
                    zip: this.report.address.zip
                }
            };
            
            this.loading.show(this.resources.loading);
            this.api.repairPricerReport
                .update(this.report.id, payload)
                .then(
                    angular.bind(this, this.reportUpdated),
                    angular.bind(this, this.reportError)
                );
        },

        reportUpdated: function(response) {
            this.notify.success(this.resources.updatedText);
            this.loading.close();
            this.$modal.close();
        },

        reportError: function(response) {
            angular.copy(this.report.address.state, this.temp_state);
            this.$log.error(response);
            this.notify.error(response.data);
            this.loading.close();
        },

        cancel: function() {
            this.$modal.dismiss("cancel");
        },

        onSelectState: function(item) {
            if (item) {
                this.country = item.country;
                this.report.address.country = item.country.value;
            }
        },

        onSelectCountry: function(item) {
            if (item) {
                this.country = item;
                this.report.address.country = item.value;
                this.options.states = this.Address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                    return state.country.value === this.country.value;
                }));
            }
        }

    };
})();