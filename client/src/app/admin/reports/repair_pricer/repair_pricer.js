(function() {
	"use strict";

	angular.module('hb.admin.reports.repair_pricer', [
			"hb.admin.reports.repair_pricer.edit"
		])
		.directive("hbRepairPricerReports", function() {
			return {
				restrict: "E",
				templateUrl: "admin/reports/repair_pricer/repair_pricer.tpl.html",
				controller: "AdminRepairPricerReportsCtrl",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("AdminRepairPricerReportsCtrl", [
			"hb.framework.indexBase",
			"AdminRepairPricerEditForm",
			"hb.api",
			"hb.resources",
			"$window",
			"ModalService",
			AdminRepairPricerReportsCtrl
		]);

	function AdminRepairPricerReportsCtrl(IndexBase, AdminRepairPricerEditForm, api, resources, $window, modal) {

		var Model = function() {
			// call the parent class
			IndexBase.call(this);
			this.api = api;
			// apply abs error specific resource strings
			this.resources = angular.extend({}, this.resources, resources.repairPricerIndex);
			// set the refresh API call
			this.refreshCall = api.repairPricerReport.all;
			// set the delete API call
			this.deleteCall = api.repairPricerReport.destroy;
			// delete prop
			this.nameProperty = "client_first";
			this.maxSize = 10;
			this.sortOptions = [{
				orderBy: "repair_pricer_reports.created_at",
				order: "desc",
				desc: this.resources.creationDate + " - " + this.resources.descending
			}, {
				orderBy: "repair_pricer_reports.created_at",
				order: "asc",
				desc: this.resources.creationDate + " - " + this.resources.ascending
			}];
			this.sortOption = this.sortOptions[0];
			this.orderBy = this.sortOption.orderBy;
			this.order = this.sortOption.order;
			this.headers = null;
			this.setHeaders();
			this.refresh();
		};

		Model.prototype = Object.create(IndexBase.prototype);

		angular.extend(Model.prototype, {
			setHeaders: function() {
				this.columnspan = 10;
				this.headers = this.resources.errorAttributes.map(angular.bind(this, function(attribute) {
					var sortable = false,
						orderBy = null,
						sorted = false,
						show = true;
					if (attribute === "Date") {
						sorted = true;
						sortable = true;
						orderBy = "repair_pricer_reports.created_at";
					}
					else if (attribute === "Delivery Date") {
						sortable = true;
						orderBy = "repair_pricer_reports.delivery_date";
					}
					else if (attribute === "Partner ID") {
						sortable = true;
						orderBy = "repair_pricer_reports.partner_id";
					}
					else if (attribute === "Payment") {
						sortable = true;
						orderBy = "repair_pricer_reports.payment_status";
					}
					else if (attribute === "Status") {
						sortable = true;
						orderBy = "repair_pricer_reports.status";
					}
					return {
						name: attribute,
						sortable: sortable,
						sorted: sorted,
						orderBy: orderBy,
						show: show,
						order: "desc"
					};
				}));
			},

			editItem: function(report) {
				AdminRepairPricerEditForm.show({
					report: report
				});
			},

			confirmRefund: function(report, index) {
				modal.confirm({
					message: this.resources.confirmRefund,
					glyphicon: "glyphicon glyphicon-question-sign",
					title: "Confirm Creation",
					confirm: angular.bind(this, this.refund, report, index)
				});
			},

			refund: function(report, index) {
				this.loading.show(this.resources.refunding);
				var payload = {
					action_type: "refund",
					repair_pricer_report: {
						client_first: report.client_first
					}
				};
				api.repairPricerReport.update(report.id, payload).then(
					angular.bind(this, this.onUpdateSuccess, index),
					angular.bind(this, this.onError));
			},

			onUpdateSuccess: function(index, response) {
				this.items[index] = response.data;
				this.loading.close();
			},

			onError: function(response) {
				this.loading.close();
				this.notify.error(response.data);
			},

			openLink: function(link) {
				$window.open(link);
			}
		});

		this.model = new Model();
	}

})();