describe("AdminRepairPricerReportsCtrl", function() {
    var controller,
        base,
        api,
        resources,
        AdminRepairPricerEditForm,
        $window,
        modal,
        notify,
        $q;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        AdminRepairPricerEditForm = $injector.get("AdminRepairPricerEditForm");
        $window = $injector.get("$window");
        modal = $injector.get("ModalService");
        notify = $injector.get("Notify");
        $q = $injector.get("$q");

        controller = $controller("AdminRepairPricerReportsCtrl", {
            "hb.framework.indexBase": base,
            "hb.api": api,
            "hb.resources": resources,
            "AdminRepairPricerEditForm": AdminRepairPricerEditForm,
            "$window": $window,
            "ModalService": modal,
            "Notify": notify
        });

        $rootScope.$apply();
    }));

    describe("editItem", function() {
        it("calls modal", function() {
            spyOn(AdminRepairPricerEditForm, "show");
            controller.model.editItem({
                id: 1,
                name: "name"
            });
            expect(AdminRepairPricerEditForm.show).toHaveBeenCalledWith({
                report: {
                    id: 1,
                    name: 'name'
                }
            });
        });
    });

    describe("confirmRefund", function() {
        it("calls modal confirm", inject(function($window) {
            spyOn(modal, "confirm");

            controller.model.confirmRefund({}, 0);

            expect(modal.confirm).toHaveBeenCalled();
        }));
    });

    describe("openLink", function() {
        it("calls window open", inject(function($window) {
            spyOn($window, "open");

            controller.model.openLink("link");

            expect($window.open).toHaveBeenCalled();
        }));
    });

    describe("refund", function() {
        it("calls api", inject(function($window, $q) {
            spyOn(api.repairPricerReport, "update").and.returnValue($q.when({ data: {} }));

            controller.model.items = [{ id: 1, status: "created" }];
            controller.model.refund({ id: 1 }, 1);

            expect(api.repairPricerReport.update).toHaveBeenCalled();
        }));
    });

    describe("onUpdateSuccess", function() {
        it("updates item", inject(function($window) {
            controller.model.items = [{ id: 1, status: "created" }];
            controller.model.onUpdateSuccess(0, { data: { id: 1, status: "received" } });

            expect(controller.model.items[0].status).toEqual("received");
        }));
    });

    describe("onError", function() {
        it("calls notify error", inject(function($window) {
            spyOn(notify, "error");

            controller.model.onError({ data: "message" });

            expect(notify.error).toHaveBeenCalled();
        }));
    });
});