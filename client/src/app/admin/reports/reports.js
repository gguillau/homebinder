(function() {
	"use strict";

	angular.module('hb.admin.reports', [
			"hb.admin.reports.repair_pricer",
			"hb.admin.reports.inspection_report_jobs",
			"hb.admin.reports.user_merges"
		])
		.config(['$stateProvider', adminReportsConfig])
		.controller("AdminReportsCtrl", [
			AdminReportsCtrl
		]);

	function AdminReportsCtrl() {
		this.navigation_links = [{
			name: "Reports",
			links: [
				{ name: "Annual Property Reviews", value: "apr", active: true },
				{ name: "BuildFax Reports", value: "buildfax", active: false },
				{ name: "Repair Pricer", value: "repair_pricer", active: false },
				{ name: "Inspection Report Jobs", value: "inspection_report_jobs", active: false },
				{ name: "User Merges", value: "user_merges", active: false }
			]
		}];
		this.currentLink = this.navigation_links[0].links[0];
	}

	AdminReportsCtrl.prototype = {
		changeLink: function(link) {
			this.currentLink.active = false;
			link.active = true;
			this.currentLink = link;
		}
	};

	function adminReportsConfig($stateProvider) {
		$stateProvider
			.state('admin.reports', {
				url: '^/admin/reports',
				templateUrl: 'admin/reports/reports.tpl.html',
				controller: 'AdminReportsCtrl',
				controllerAs: "ctrl",
				access: {
					requiresLogin: true
				}
			});
	}

})();