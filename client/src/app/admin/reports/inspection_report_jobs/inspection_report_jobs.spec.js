describe("AdminInspectionReportsCtrl", function() {
    var controller,
        base,
        api,
        resources,
        $window,
        modal,
        notify;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        $window = $injector.get("$window");
        modal = $injector.get("ModalService");
        notify = $injector.get("Notify");
        $q = $injector.get("$q");

        controller = $controller("AdminInspectionReportsCtrl", {
            "hb.framework.indexBase": base,
            "hb.api": api,
            "hb.resources": resources,
            "$window": $window,
            "ModalService": modal,
            "Notify": notify
        });

        $rootScope.$apply();
    }));

    describe("openLink", function() {
        it("calls window open", inject(function($window) {
            spyOn($window, "open");

            controller.model.openLink("link");

            expect($window.open).toHaveBeenCalled();
        }));
    });
});