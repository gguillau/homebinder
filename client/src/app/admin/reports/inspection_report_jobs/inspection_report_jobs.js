(function() {
	"use strict";

	angular.module('hb.admin.reports.inspection_report_jobs', [])
		.directive("hbInspectionReports", function() {
			return {
				restrict: "E",
				templateUrl: "admin/reports/inspection_report_jobs/inspection_report_jobs.tpl.html",
				controller: "AdminInspectionReportsCtrl",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("AdminInspectionReportsCtrl", [
			"hb.framework.indexBase",
			"hb.api",
			"hb.resources",
			"$window",
			"ModalService",
			AdminInspectionReportsCtrl
		]);

	function AdminInspectionReportsCtrl(IndexBase, api, resources, $window, modal) {

		var Model = function() {
			// call the parent class
			IndexBase.call(this);
			this.api = api;
			// apply abs error specific resource strings
			this.resources = angular.extend({}, this.resources, resources.inspectionReportJobIndex);
			// set the refresh API call
			this.refreshCall = api.inspectionReportJob.all;
			// set the delete API call
			this.deleteCall = api.inspectionReportJob.destroy;
			// delete prop
			this.nameProperty = "job_id";
			this.maxSize = 10;
			this.sortOptions = [{
				orderBy: "inspection_report_jobs.created_at",
				order: "desc",
				desc: this.resources.creationDate + " - " + this.resources.descending
			}, {
				orderBy: "inspection_report_jobs.created_at",
				order: "asc",
				desc: this.resources.creationDate + " - " + this.resources.ascending
			}];
			this.sortOption = this.sortOptions[0];
			this.orderBy = this.sortOption.orderBy;
			this.order = this.sortOption.order;
			this.headers = null;
			this.setHeaders();
			this.refresh();
		};

		Model.prototype = Object.create(IndexBase.prototype);

		angular.extend(Model.prototype, {
			setHeaders: function() {
				this.columnspan = 10;
				this.headers = this.resources.errorAttributes.map(angular.bind(this, function(attribute) {
					var sortable = false,
						orderBy = null,
						sorted = false,
						show = true;
					if (attribute === "Date") {
						sorted = true;
						sortable = true;
						orderBy = "inspection_report_jobs.created_at";
					}
					else if (attribute === "Binder ID") {
						sortable = true;
						orderBy = "inspection_report_jobs.binder_id";
					}
					else if (attribute === "Partner ID") {
						sortable = true;
						orderBy = "inspection_report_jobs.partner_id";
					}
					else if (attribute === "Workaround Job ID") {
						sortable = true;
						orderBy = "inspection_report_jobs.job_id";
					}
					else if (attribute === "Status") {
						sortable = true;
						orderBy = "inspection_report_jobs.status";
					}
					return {
						name: attribute,
						sortable: sortable,
						sorted: sorted,
						orderBy: orderBy,
						show: show,
						order: "desc"
					};
				}));
			},
			openLink: function(link) {
				$window.open(link);
			}
		});

		this.model = new Model();
	}

})();