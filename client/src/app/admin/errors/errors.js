(function() {
	"use strict";

	angular.module('hb.admin.errors', [
			"hb.admin.errors.edit"
		])
		.config(['$stateProvider', adminErrorsConfig])
		.controller("AdminAbsErrorsCtrl", [
			"hb.framework.indexBase",
			"AdminErrorsEditForm",
			"hb.api",
			"hb.resources",
			AdminAbsErrorsCtrl
		]);

	function AdminAbsErrorsCtrl(IndexBase, AdminErrorsEditForm, api, resources) {

		var Model = function() {
			// call the parent class
			IndexBase.call(this);
			this.api = api;
			// apply abs error specific resource strings
			this.resources = angular.extend({}, this.resources, resources.errorsIndex);
			// set the refresh API call
			this.refreshCall = api.automationError.all;
			// set the delete API call
			this.deleteCall = api.automationError.destroy;
			// delete prop
			this.nameProperty = "error_message";
			this.maxSize = 10;
			this.sortOptions = [{
				orderBy: "automation_errors.created_at",
				order: "desc",
				desc: this.resources.creationDate + " - " + this.resources.descending
			}, {
				orderBy: "automation_errors.created_at",
				order: "asc",
				desc: this.resources.creationDate + " - " + this.resources.ascending
			}];
			this.sortOption = this.sortOptions[0];
			this.orderBy = this.sortOption.orderBy;
			this.order = this.sortOption.order;
			this.headers = null;
			this.setHeaders();
			this.refresh();
		};

		Model.prototype = Object.create(IndexBase.prototype);

		angular.extend(Model.prototype, {
			setHeaders: function() {
				this.columnspan = 6;
				this.headers = this.resources.errorAttributes.map(angular.bind(this, function(attribute) {
					var sortable = false,
						orderBy = null,
						sorted = false,
						show = true;
					if (attribute === "Date") {
						sorted = true;
						sortable = true;
						orderBy = "automation_errors.created_at";
					}
					else if (attribute === "Reason for Failure") {
						sortable = true;
						orderBy = "automation_errors.error_message";
					}
					else if (attribute === "Partner Route Name") {
						sortable = true;
						orderBy = "automation_errors.partner_name";
					}
					else if (attribute === "Template ID") {
						sortable = true;
						orderBy = "automation_errors.binder_template_id";
					}
					else if (attribute === "Client Name") {
						sortable = true;
						orderBy = "automation_errors.client_first";
					}
					return {
						name: attribute,
						sortable: sortable,
						sorted: sorted,
						orderBy: orderBy,
						show: show,
						order: "desc"
					};
				}));
			},

			editItem: function(error) {
				AdminErrorsEditForm.show({
					error: error,
					errors: this.items,
					closed: angular.bind(this, this.removeItem)
				});
			}
		});

		this.model = new Model();
	}

	function adminErrorsConfig($stateProvider) {
		$stateProvider
			.state('admin.errors', {
				url: '^/admin/errors',
				templateUrl: 'admin/errors/errors.tpl.html',
				controller: "AdminAbsErrorsCtrl",
				controllerAs: "ctrl",
				access: {
					requiresLogin: true
				}
			});
	}

})();