(function() {
    "use strict";

    angular
        .module("hb.admin.errors.edit", [])
        .factory("AdminErrorsEditForm", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "admin/errors/edit/editForm.tpl.html",
                            controller: "AdminErrorsEditFormController as ctrl",
                            resolveData: opts,
                            closed: opts.closed
                        });
                    }
                };
            }
        ])
        .controller("AdminErrorsEditFormController", [
            "$modalInstance",
            "data",
            "Address",
            "hb.api",
            "Notify",
            "$log",
            "Loading",
            "hb.resources",
            AdminErrorsEditFormController
        ]);

    function AdminErrorsEditFormController($modal, opts, Address, api, notify, $log, loading, resources) {
        this.$modal = $modal;
        this.Address = Address;
        this.api = api;
        this.notify = notify;
        this.$log = $log;
        this.loading = loading;
        this.error = opts.error;
        this.errors = opts.errors;
        this.country = Address.findCountryByCode(this.error.property_country);
        this.countries = this.Address.countries();
        this.options = {};
        this.options.countries = this.countries;
        this.options.states = this.Address.getStatesAndProvinces();
        this.temp_state = null;
        this.resources = resources.errorsForm;
        this.init();
    }

    AdminErrorsEditFormController.prototype = {

        init: function() {
            if (this.error.property_country) {
                this.options.states = this.Address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                    return state.country.value === this.error.property_country;
                }));
            }
        },

        submitForm: function() {
            var data = angular.copy(this.error);
            data.agent_phone = "+" + this.country.code + this.error.agent_phone.national;
            this.loading.show(this.resources.loading);
            this.api.automationError
                .update(this.error.id, data)
                .then(
                    angular.bind(this, this.requestUpdated),
                    angular.bind(this, this.requestError)
                );
        },

        requestUpdated: function(response) {
            this.notify.success(this.resources.updatedText);
            this.loading.close();
            this.$modal.close(this.error);
        },

        requestError: function(response) {
            angular.copy(this.error.property_state, this.temp_state);
            this.$log.error(response);
            this.notify.error(response.data);
            this.loading.close();
        },

        cancel: function() {
            this.$modal.dismiss("cancel");
        },

        onSelectState: function(item) {
            if (item) {
                this.country = item.country;
                this.error.property_country = item.country.value;
            }
        },

        onSelectCountry: function(item) {
            if (item) {
                this.country = item;
                this.error.property_country = item.value;
                this.options.states = this.Address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                    return state.country.value === this.country.value;
                }));
            }
        }

    };
})();