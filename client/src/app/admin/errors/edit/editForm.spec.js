describe("AdminErrorsEditFormController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        opts,
        $modal,
        api,
        notify,
        $log,
        loading,
        address,
        $q,
        $templateCache,
        $compile,
        template,
        requestForm,
        AdminErrorsEditForm,
        ModalService;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$templateCache_, _$compile_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        address = _$injector_.get("Address");
        AdminErrorsEditForm = _$injector_.get("AdminErrorsEditForm");
        ModalService = _$injector_.get("ModalService");
        $compile = _$compile_;
        $templateCache = _$templateCache_;

        api = {
            automationError: {
                update: function(error) {}
            }

        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        $modal = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };

        binder = {
            appliances: null,
            created_at: "2015-09-06T04:29:36Z",
            id: 1,
            name: "This is a fake home",
            primary: null,
            details: "this is a home",
            last_recall: null,
            partner: {
                address: null,
                binder_count: 7,
                binder_logo_id: null,
                code: "FAKE",
                contact: "Test Inspector",
                coupons: null,
                created_at: "2015-04-25T15:56:15Z",
                email: "gerlininspector@gmail.com",
                id: 115,
                name: "Test Inspector",
                partner_type: "inspector",
                phone: "+17814928454",
                sellers_logo_id: null,
                tags: [],
                vendors: []
            },
            vendor: null,
            recalls: 0,
            documents: null,
            maintenance_items: null,
            binder_contractors: null,
            property: {
                property_type: ""
            },
            subscription: null,
            seller_report: null,
            permissions: {
                can_create: true,
                can_create_seller_report: true,
                can_destroy: true,
                can_edit_seller_report: true,
                can_read: true,
                can_share: true,
                can_subscribe: true,
                can_transfer: true,
                can_view_master_report: false,
                can_write: true
            },
            tags: [{
                id: 1042,
                tag: "client",
                metadata: "{\"firstName\":\"John\",\"lastName\":\"Smith\",\"email\":\"johnsmith@gmail.com\"}",
                tag_scope: null
            }]
        };

        opts = {
            error: {
                agent_phone: {

                },
                property_state: "",
                property_country: "US"
            }
        };

        notify = {
            success: function() {},
            error: function() {}
        };

        $log = {
            error: function() {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("AdminErrorsEditFormController", {
            "$modalInstance": $modal,
            "Address": address,
            "data": opts,
            "hb.api": api,
            "Notify": notify,
            "$log": $log,
            "Loading": loading
        });

        $scope.ctrl = ctrl;
        template = $templateCache.get("admin/errors/edit/editForm.tpl.html");
        $compile(template)($scope);
        requestForm = $scope.ctrl.requestForm;
    }

    describe('requestForm', function() {

        it('should expect requestForm to be defined and form validation to be false', function() {

            createController();

            ctrl.error.partner_name = "";
            ctrl.error.api_key = "";
            ctrl.error.client_first = "";
            ctrl.error.client_last = "";
            ctrl.error.client_email = "";
            ctrl.error.binder_template_id = "";
            ctrl.error.property_address = "";
            ctrl.error.property_address2 = "";
            ctrl.error.property_city = "";
            ctrl.error.property_state = "";
            ctrl.error.property_country = "";
            ctrl.error.property_postalcode = "";
            ctrl.error.agent_name = "";
            ctrl.error.agent_contact = "";
            ctrl.error.agent_phone = "";
            ctrl.error.agent_email = "";
            $rootScope.$apply();

            expect(requestForm).toBeDefined();
            expect(requestForm.$valid).toBe(false);
            expect(requestForm.partner_name.$error.required).toBe(true);
            expect(requestForm.api_key.$error.required).toBe(true);
            expect(requestForm.client_first.$error.required).toBe(true);
            expect(requestForm.client_last.$error.required).toBe(true);
            expect(requestForm.template.$error.required).toBe(true);
            expect(requestForm.address.$error.required).toBe(true);
            expect(requestForm.city.$error.required).toBe(true);
            expect(requestForm.property_state.$error.required).toBe(true);
        });

        it('should expect requestForm to be defined and form validation to be false', function() {

            createController();

            ctrl.error.partner_name = "test";
            ctrl.error.api_key = "apikey";
            ctrl.error.client_first = "yo9xinjll3niw1lh3o9x6xojq7gfypr5zvp2f2s6pr94o40z604";
            ctrl.error.client_last = "yo9xinjll3niw1lh3o9x6xojq7gfypr5zvp2f2s6pr94o40z604";
            ctrl.error.client_email = "jmtmmsyy8uhyy3knq2yhj5rgtc0ol3njw94q8qq6xsjmykawa591xeufktarzq505j6qttekpsngpi1t1rgbzozg0wiovnomwzm4xmngxuf4uq6a7prf52czlhoe3rnrhou1jupxtkznt5yfsos8ksmv3liitcekytivk7vvdmebfn5aim5b5yc7dv5pbwb638czddh6ey1dama9ajswkldeyw25nz2gvxo428oihxy8uo6ce24p0b@gmail.com";
            ctrl.error.binder_template_id = "583";
            ctrl.error.property_address = "yo9xinjll3niw1lh3o9x6xojq7gfypr5zvp2f2s6pr94o40z604";
            ctrl.error.property_address2 = "yo9xinjll3niw1lh3o9x6xojq7gfypr5zvp2f2s6pr94o40z604";
            ctrl.error.property_city = "yo9xinjll3niw1lh3o9x6xojq7gfypr5zvp2f2s6pr94o40z604";
            ctrl.error.property_state = "MA";
            ctrl.error.property_country = "US";
            ctrl.error.property_postalcode = "nb4fiholjwo323ucgqsfi";
            ctrl.error.agent_name = "48h009zopjem7nj30owr2sgy9iqvc8ow3ps9gmj8wvcbq6onzpi2jznmpbtf5cnjuy9texo3xqer4sa8fpplt28v9lbng46v0tev";
            ctrl.error.agent_contact = "hma9um1hem8hr681tvpn5quhmwmme44y21ztf8tare56gixjtn60jotmxnnkcgivkr78zae580ld6zhd87lobssxz2k7t73548uq3oow2eghkn9svpkrhn88ij8kdpfcvhcvi3l9c651e5h7g2whsf80rn0xf0rh1jg4gh7ud6rn432t220p784s0ug73truwuboksk3f5e8bdp2up2vwnewv6i3ai0de3p2eq1d4rcp9zswuhpy47p9va7ikus3";
            ctrl.error.agent_phone = "";
            ctrl.error.agent_email = "hma9um1hem8hr681tvpn5quhmwmme44y21ztf8tare56gixjtn60jotmxnnkcgivkr78zae580ld6zhd87lobssxz2k7t73548uq3oow2eghkn9svpkrhn88ij8kdpfcvhcvi3l9c651e5h7g2whsf80rn0xf0rh1jg4gh7ud6rn432t220p784s0ug73truwuboksk3f5e8bdp2up2vwnewv6i3ai0de3p2eq1d4rcp9zswuhpy47p9va7ikus3";
            $rootScope.$apply();

            expect(requestForm).toBeDefined();
            expect(requestForm.$valid).toBe(false);
            expect(requestForm.client_first.$error.maxlength).toBe(true);
            expect(requestForm.client_last.$error.maxlength).toBe(true);
            expect(requestForm.client_email.$error.maxlength).toBe(true);
            expect(requestForm.address.$error.maxlength).toBe(true);
            expect(requestForm.unit.$error.maxlength).toBe(true);
            expect(requestForm.city.$error.maxlength).toBe(true);
            expect(requestForm.property_postalcode.$error.maxlength).toBe(true);
        });

        it('should expect requestForm to be defined and form validation to be true', function() {

            createController();

            ctrl.error.partner_name = "test";
            ctrl.error.api_key = "apikey";
            ctrl.error.client_first = "Bob";
            ctrl.error.client_last = "Smith";
            ctrl.error.client_email = "bobsmith@gmail.com";
            ctrl.error.binder_template_id = "583";
            ctrl.error.property_address = "32 Harding Road";
            ctrl.error.property_address2 = "";
            ctrl.error.property_city = "Dorchester";
            ctrl.error.property_state = "MA";
            ctrl.error.property_country = "US";
            ctrl.error.property_postalcode = "";
            ctrl.error.agent_name = "";
            ctrl.error.agent_contact = "";
            ctrl.error.agent_phone = "";
            ctrl.error.agent_email = "";
            $rootScope.$apply();

            expect(requestForm).toBeDefined();
            expect(requestForm.$valid).toBe(true);
        });

    });

    describe('ctrl.submitForm', function() {

        it("should submit the form and call the update binder function", function() {

            spyOn(api.automationError, "update").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(notify, "success");
            spyOn($modal, "close");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();
            ctrl.submitForm();
            $rootScope.$apply();

            expect(api.automationError.update).toHaveBeenCalled();
            expect(loading.show).toHaveBeenCalledWith("Re-Sending request...");
            expect(loading.close).toHaveBeenCalled();
            expect(notify.success).toHaveBeenCalledWith("Binder created.");
            expect($modal.close).toHaveBeenCalled();

        });

        it("should submit the form and call the update binder function but return an error", function() {

            spyOn(api.automationError, "update").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();
            ctrl.submitForm();
            $rootScope.$apply();

            expect(api.automationError.update).toHaveBeenCalled();
            expect(loading.show).toHaveBeenCalledWith("Re-Sending request...");
            expect(loading.close).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");

        });
    });

    describe("ctrl.cancel", function() {

        it("should call $modalInstance dismiss function", function() {

            spyOn($modal, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.cancel();
            $rootScope.$apply();

            expect($modal.dismiss).toHaveBeenCalledWith('cancel');
        });

    });

    describe('ctrl.onSelectState', function() {
        it('sets the error property country', function() {
            createController();

            ctrl.onSelectState({ country: { value: "MA" } });

            expect(ctrl.country.value).toEqual("MA");
        });
    });

    describe('ctrl.onSelectCountry', function() {
        it('sets the error property country', function() {
            createController();

            ctrl.onSelectCountry({ value: "MA" });

            expect(ctrl.country.value).toEqual("MA");
        });
    });

    describe('AprScheduleModal', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            AdminErrorsEditForm.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });

});