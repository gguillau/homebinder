describe("AdminAbsErrorsCtrl", function() {
    var controller,
        base,
        api,
        resources,
        AdminErrorsEditForm;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        AdminErrorsEditForm = $injector.get("AdminErrorsEditForm");

        spyOn(api.automationError, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));
        spyOn(api.automationError, "destroy").and.returnValue($q.when({}));

        controller = $controller("AdminAbsErrorsCtrl", {
            "hb.framework.indexBase": base,
            "hb.api": api,
            "hb.resources": resources,
            "AdminErrorsEditForm": AdminErrorsEditForm
        });

        $rootScope.$apply();
    }));

    describe("editItem", function() {
        it("calls modal", function() {
            spyOn(AdminErrorsEditForm, "show");
            controller.model.editItem({
                id: 1,
                name: "name"
            });
            expect(AdminErrorsEditForm.show).toHaveBeenCalledWith({
                error: {
                    id: 1,
                    name: 'name'
                },
                errors: controller.model.items,
                closed: jasmine.any(Function)
            });
        });
    });
});