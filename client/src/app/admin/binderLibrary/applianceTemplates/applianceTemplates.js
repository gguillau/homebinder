(function() {
    "use strict";

    angular
        .module("hb.admin.binderLibrary.applianceTemplates", [])
        .directive("hbApplianceTemplates", function() {
            return {
                restrict: "E",
                templateUrl: "admin/binderLibrary/applianceTemplates/applianceTemplates.tpl.html",
                controller: "AdminApplianceTemplatesController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("AdminApplianceTemplatesController", [
            "hb.framework.indexBase",
            "hb.resources",
            "hb.api",
            "PartnerTemplatesItemsEditModal",
            AdminApplianceTemplatesController
        ]);

    function AdminApplianceTemplatesController(IndexBase, resources, api, PartnerTemplatesItemsEditModal) {
        var Model = function() {
            // call the parent class
            IndexBase.call(this);
            // apply binderTemplates specific resource strings
            this.resources = angular.extend({}, this.resources, resources.applianceTemplates);
            this.refreshCall = api.applianceTemplates.all;
            this.deleteCall = api.applianceTemplates.destroy;
            this.itemsPerPage = 10;
            // delete prop
            this.nameProperty = "name";
            this.queryArgs = {
                library_template: true
            };
            this.setHeaders();
            this.refresh();
        };

        Model.prototype = Object.create(IndexBase.prototype);

        angular.extend(Model.prototype, {

            addApp: function() {
                var appliance = {
                    name: "Untitled",
                    library_template: true,
                    notes: "",
                    description: ""
                };

                PartnerTemplatesItemsEditModal.show({
                    templateUrl: "partners/templates/form/appliances/edit/edit.tpl.html",
                    controller: "PartnerTemplatesAppliancesEditController as ctrl",
                    item: appliance,
                    cfg: { showTypeaheads: false },
                    index: -1,
                    onSave: angular.bind(this, this.insertAppliance)
                });
            },

            editItem: function(template) {
                var index = this.items.indexOf(template);
                PartnerTemplatesItemsEditModal.show({
                    templateUrl: "partners/templates/form/appliances/edit/edit.tpl.html",
                    controller: "PartnerTemplatesAppliancesEditController as ctrl",
                    item: template,
                    cfg: { showTypeaheads: false },
                    index: index,
                    onSave: angular.bind(this, this.insertAppliance)
                });
            },

            insertAppliance: function(item, index) {
                if (index > -1) {
                    this.items[index] = item;
                }
                else {
                    this.items.unshift(item);
                }
            },

            setHeaders: function() {
                this.headers = this.resources.headers.map(angular.bind(this, function(attribute) {
                    var sortable = false,
                        orderBy = null,
                        sorted = false;
                    if (attribute === "ID") {
                        sorted = true;
                        sortable = true;
                        orderBy = "appliance_templates.id";
                    }
                    else if (attribute === "Name") {
                        sortable = true;
                        orderBy = "appliance_templates.name";
                    }
                    else if (attribute === "Binder Template") {
                        sortable = true;
                        orderBy = "appliance_templates.binder_template_id";
                    }
                    else if (attribute === "Description") {
                        sortable = true;
                        orderBy = "appliance_templates.description";
                    }
                    else if (attribute === "Library Template") {
                        sortable = true;
                        orderBy = "appliance_templates.library_template";
                    }
                    return {
                        name: attribute,
                        sortable: sortable,
                        sorted: sorted,
                        orderBy: orderBy,
                        order: "desc"
                    };
                }));
                this.sortOption = this.headers[0];
            }
        });

        this.model = new Model();
    }

})();