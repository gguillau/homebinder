describe("AdminApplianceTemplatesController", function() {
    var ctrl,
        controller,
        $log,
        api,
        notify,
        resources,
        loading,
        $modal,
        PartnerTemplatesItemsEditModal;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
        PartnerTemplatesItemsEditModal = $injector.get("PartnerTemplatesItemsEditModal");

        $modal = {
            show: function() {}
        };
    }));

    function createController() {
        ctrl = controller("AdminApplianceTemplatesController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "hb.resources": resources,
            "Loading": loading,
            "ModalService": $modal,
            "PartnerTemplatesItemsEditModal": PartnerTemplatesItemsEditModal
        });
    }

    describe("setHeaders", function() {
        it("sets the headers", function() {
            createController();
            ctrl.model.setHeaders();

            expect(ctrl.model.headers.length).toEqual(5);
        });
    });

    describe('insertAppliance', function() {
        it('updates the item to the list', function() {
            createController();
            ctrl.model.items = [{
                id: 1,
                "name": "Appliance"
            }];
            expect(ctrl.model.items[0].name).toEqual("Appliance");
            ctrl.model.insertAppliance({
                id: 1,
                "name": "Appliance 2"
            }, 0);

            expect(ctrl.model.items[0].name).toEqual("Appliance 2");
        });

        it('adds the item to the list', function() {
            createController();
            ctrl.model.items = [{
                id: 1,
                "name": "Appliance 1"
            }];
            var length = ctrl.model.items.length;
            ctrl.model.insertAppliance({
                id: length,
                "name": "Appliance 2"
            }, -1);

            expect(ctrl.model.items[length].id).toEqual(length);
        });

    });

    describe('addApp', function() {
        it('should call modal', function() {

            createController();
            spyOn(PartnerTemplatesItemsEditModal, "show");
            ctrl.model.addApp();

            expect(PartnerTemplatesItemsEditModal.show).toHaveBeenCalled();
        });

    });

    describe('editItem', function() {
        it('should call modal', function() {

            createController();
            spyOn(PartnerTemplatesItemsEditModal, "show");
            ctrl.model.items = [{ id: 1 }];
            ctrl.model.editItem(ctrl.model.items[0]);

            expect(PartnerTemplatesItemsEditModal.show).toHaveBeenCalled();
        });
    });
});

describe('hbApplianceTemplates', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_, _$q_) {
        api = _$injector_.get("hb.api");
        $q = _$q_;
        spyOn(api.applianceTemplates, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));
        $scope = $rootScope.$new(), $compile = _$compile_;

        $compile('<hb-appliance-templates></hb-appliance-templates>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the title', function() {
            expect(api.applianceTemplates.all).toHaveBeenCalled();
        });
    });
});