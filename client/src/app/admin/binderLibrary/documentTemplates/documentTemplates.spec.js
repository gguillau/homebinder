describe("AdminDocumentTemplatesController", function() {
    var ctrl,
        controller,
        $log,
        api,
        notify,
        resources,
        loading,
        $modal,
        PartnerTemplatesItemsEditModal;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
        PartnerTemplatesItemsEditModal = $injector.get("PartnerTemplatesItemsEditModal");

        $modal = {
            show: function() {}
        };
    }));

    function createController() {
        ctrl = controller("AdminDocumentTemplatesController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "hb.resources": resources,
            "Loading": loading,
            "ModalService": $modal,
            "PartnerTemplatesItemsEditModal": PartnerTemplatesItemsEditModal
        });
    }

    describe("setHeaders", function() {
        it("sets the headers", function() {
            createController();
            ctrl.model.setHeaders();

            expect(ctrl.model.headers.length).toEqual(5);
        });
    });

    describe('insertDocument', function() {
        it('updates the item to the list', function() {
            createController();
            ctrl.model.items = [{
                id: 1,
                "file_file_name": "document"
            }];
            expect(ctrl.model.items[0].file_file_name).toEqual("document");
            ctrl.model.insertDocument({
                id: 1,
                "file_file_name": "Document 2"
            }, 0);

            expect(ctrl.model.items[0].file_file_name).toEqual("Document 2");
        });

        it('adds the item to the list', function() {
            createController();
            ctrl.model.items = [{
                id: 1,
                "file_file_name": "Document 1"
            }];
            var length = ctrl.model.items.length;
            ctrl.model.insertDocument({
                id: length,
                "file_file_name": "Document 2"
            }, -1);

            expect(ctrl.model.items[length].id).toEqual(length);
        });
    });

    describe('addItem', function() {
        it('should call modal', function() {

            createController();
            spyOn(PartnerTemplatesItemsEditModal, "show");
            ctrl.model.addItem();

            expect(PartnerTemplatesItemsEditModal.show).toHaveBeenCalled();
        });

    });

    describe('editItem', function() {
        it('should call modal', function() {

            createController();
            spyOn(PartnerTemplatesItemsEditModal, "show");
            ctrl.model.items = [{ id: 1 }];
            ctrl.model.editItem(ctrl.model.items[0]);

            expect(PartnerTemplatesItemsEditModal.show).toHaveBeenCalled();
        });
    });
});

describe('hbDocumentTemplates', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_, _$q_) {
        api = _$injector_.get("hb.api");
        $q = _$q_;
        spyOn(api.documentTemplates, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));
        $scope = $rootScope.$new(), $compile = _$compile_;

        $compile('<hb-document-templates></hb-document-templates>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the title', function() {
            expect(api.documentTemplates.all).toHaveBeenCalled();
        });
    });
});