(function() {
    "use strict";

    angular
        .module("hb.admin.binderLibrary.documentTemplates", [])
        .directive("hbDocumentTemplates", function() {
            return {
                restrict: "E",
                templateUrl: "admin/binderLibrary/documentTemplates/documentTemplates.tpl.html",
                controller: "AdminDocumentTemplatesController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("AdminDocumentTemplatesController", [
            "hb.framework.indexBase",
            "hb.resources",
            "hb.api",
            "PartnerTemplatesItemsEditModal",
            AdminDocumentTemplatesController
        ]);

    function AdminDocumentTemplatesController(IndexBase, resources, api, PartnerTemplatesItemsEditModal) {
        var Model = function() {
            // call the parent class
            IndexBase.call(this);
            // apply binderTemplates specific resource strings
            this.resources = angular.extend({}, this.resources, resources.documentTemplates);
            this.refreshCall = api.documentTemplates.all;
            this.deleteCall = api.documentTemplates.destroy;
            this.itemsPerPage = 10;
            // delete prop
            this.nameProperty = "file_file_name";
            this.queryArgs = {
                library_template: true
            };
            this.setHeaders();
            this.refresh();
        };

        Model.prototype = Object.create(IndexBase.prototype);

        angular.extend(Model.prototype, {

            addItem: function() {
                var document = {
                    name: "Untitled",
                    library_template: true,
                    notes: "",
                    description: ""
                };

                PartnerTemplatesItemsEditModal.show({
                    templateUrl: "partners/templates/form/documents/edit/edit.tpl.html",
                    controller: "PartnerTemplatesDocumentsEditController as ctrl",
                    item: document,
                    cfg: {},
                    index: -1,
                    onSave: angular.bind(this, this.insertDocument)
                });
            },

            editItem: function(template) {
                var index = this.items.indexOf(template);
                PartnerTemplatesItemsEditModal.show({
                    templateUrl: "partners/templates/form/documents/edit/edit.tpl.html",
                    controller: "PartnerTemplatesDocumentsEditController as ctrl",
                    item: template,
                    cfg: { showTypeaheads: false },
                    index: index,
                    onSave: angular.bind(this, this.insertDocument)
                });
            },

            insertDocument: function(item, index) {
                if (index > -1) {
                    this.items[index] = item;
                }
                else {
                    this.items.unshift(item);
                }
            },

            setHeaders: function() {
                this.headers = this.resources.headers.map(angular.bind(this, function(attribute) {
                    var sortable = false,
                        orderBy = null,
                        sorted = false;
                    if (attribute === "ID") {
                        sorted = true;
                        sortable = true;
                        orderBy = "document_templates.id";
                    }
                    else if (attribute === "Name") {
                        sortable = true;
                        orderBy = "document_templates.name";
                    }
                    else if (attribute === "Binder Template") {
                        sortable = true;
                        orderBy = "document_templates.binder_template_id";
                    }
                    else if (attribute === "Description") {
                        sortable = true;
                        orderBy = "document_templates.description";
                    }
                    else if (attribute === "Library Template") {
                        sortable = true;
                        orderBy = "document_templates.library_template";
                    }
                    return {
                        name: attribute,
                        sortable: sortable,
                        sorted: sorted,
                        orderBy: orderBy,
                        order: "desc"
                    };
                }));
                this.sortOption = this.headers[0];
            }
        });

        this.model = new Model();
    }

})();