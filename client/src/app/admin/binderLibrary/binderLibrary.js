(function() {
	"use strict";

	angular.module("hb.admin.binderLibrary", [
		"hb.admin.binderLibrary.templates",
		"hb.admin.binderLibrary.applianceTemplates",
		"hb.admin.binderLibrary.maintenanceTemplates",
		"hb.admin.binderLibrary.contractorTypes",
		"hb.admin.binderLibrary.documentTemplates"
		])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
				.state("admin.binderLibrary", {
					url: "^/admin/binderLibrary",
					templateUrl: "admin/binderLibrary/binderLibrary.tpl.html",
					controller: "AdminBinderLibraryCtrl",
					controllerAs: "ctrl",
					access: {
						requiresLogin: true
					}
				});
		}])
		.controller("AdminBinderLibraryCtrl", [
			AdminBinderLibraryCtrl
		]);

	function AdminBinderLibraryCtrl() {
		this.navigation_links = [{
			name: "Binder Library",
			links: [
				{ name: "Library Templates", value: "templates", active: true },
				{ name: "Appliance Library Items", value: "appliance_templates", active: false },
				{ name: "Maintenance Library Items", value: "maintenance_templates", active: false },
				{ name: "Contractor Types", value: "contractor_types", active: false },
				{ name: "Document Library Items", value: "document_templates", active: false }
			]
		}];
		this.currentLink = this.navigation_links[0].links[0];
	}

	AdminBinderLibraryCtrl.prototype = {
		changeLink: function(link) {
			this.currentLink.active = false;
			link.active = true;
			this.currentLink = link;
		}
	};
})();
