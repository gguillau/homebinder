describe("AdminContractorTypesController", function() {
    var ctrl,
        controller,
        $log,
        api,
        notify,
        resources,
        loading,
        $q;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
        $q = $injector.get("$q");
    }));

    function createController() {
        ctrl = controller("AdminContractorTypesController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "hb.resources": resources,
            "Loading": loading
        });
    }

    describe("setHeaders", function() {
        it("sets the headers", function() {
            createController();
            ctrl.model.setHeaders();

            expect(ctrl.model.headers.length).toEqual(3);
        });
    });

    describe("searchSubTypes", function() {
        it("calls api contractorSubType all", function() {
            spyOn(api.contractorSubType, "all").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.model.searchSubTypes("search");

            expect(api.contractorSubType.all).toHaveBeenCalled();
        });

        it("does not call api contractorSubType all", function() {
            spyOn(api.contractorSubType, "all").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.model.searchSubTypes(undefined);

            expect(api.contractorSubType.all).not.toHaveBeenCalled();
        });
    });

    describe("searchComplete", function() {
        it("sets the options", function() {
            createController();
            ctrl.model.searchComplete({ sub_types: [] }, { data: { items: [{ id: 1 }] } });

            expect(ctrl.model.options.length).toEqual(1);
        });
    });

    describe("searchError", function() {
        it("calls notify error", function() {
            spyOn(notify, "error");

            createController();
            ctrl.model.searchError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("addItem", function() {
        it("adds an item to the items list", function() {
            spyOn(notify, "error");

            createController();
            ctrl.model.addItem();

            expect(ctrl.model.items.length).toEqual(1);
        });
    });

    describe("editItem", function() {
        it("sets editMode for item to true", function() {
            spyOn(notify, "error");

            createController();
            ctrl.model.items = [{ id: 1, editMode: false }, { id: 2, editMode: true }];
            expect(ctrl.model.items[0].editMode).toBe(false);
            expect(ctrl.model.items[1].editMode).toBe(true);
            ctrl.model.editItem(ctrl.model.items[0]);

            expect(ctrl.model.items[0].editMode).toBe(true);
            expect(ctrl.model.items[1].editMode).toBe(false);
        });
    });

    describe("cancel", function() {
        it("removes the item from the list", function() {
            createController();
            ctrl.model.items = [{ name: "test", sub_types: [{ name: "test" }] }];
            ctrl.model.cancel(ctrl.model.items[0], 0);

            expect(ctrl.model.items.length).toEqual(0);
        });

        it("sets editMode to false", function() {
            createController();
            ctrl.model.items = [{ id: 1, name: "test", editMode: true, sub_types: [{ name: "test" }] }];
            ctrl.model.cancel(ctrl.model.items[0], 0);

            expect(ctrl.model.items[0].editMode).toBe(false);
        });
    });

    describe("save", function() {
        it("calls createCall", function() {
            createController();
            spyOn(ctrl.model, "createCall").and.returnValue($q.when({ data: {} }));
            ctrl.model.save({ name: "test", sub_types: [] }, 0);

            expect(ctrl.model.createCall).toHaveBeenCalled();
        });

        it("calls updateCall", function() {
            createController();
            spyOn(ctrl.model, "updateCall").and.returnValue($q.when({ data: {} }));
            ctrl.model.save({ id: 1, name: "test", sub_types: [] }, 0);

            expect(ctrl.model.updateCall).toHaveBeenCalled();
        });
    });

    describe("saveSuccess", function() {
        it("updates the item and calls notify", function() {
            spyOn(notify, "success");

            createController();
            ctrl.model.items = [{ name: "test" }];
            ctrl.model.saveSuccess(0, { data: { id: 1, name: "test", sub_types: [] } });

            expect(notify.success).toHaveBeenCalled();
        });
    });

    describe("saveError", function() {
        it("calls notify error", function() {
            spyOn(notify, "error");

            createController();
            ctrl.model.items = [{ id: 1 }];
            ctrl.model.saveError(0, { data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("onSelect", function() {
        it("calls findExisting and updates the sub_types", function() {
            createController();
            ctrl.model.items = [{ id: 1 }];
            var type = { sub_types: [] };
            spyOn(ctrl.model, "findExisting").and.returnValue({ index: -1, name: "test" });
            ctrl.model.onSelect({}, {}, type);

            expect(type.sub_types.length).toEqual(1);
        });
    });

    describe("onRemove", function() {
        it("calls findExisting and updates the sub_types", function() {
            createController();
            ctrl.model.items = [{ id: 1 }];
            var type = { sub_types: ["test"] };
            spyOn(ctrl.model, "findExisting").and.returnValue({ index: 0, name: "test" });
            ctrl.model.onRemove({}, type);

            expect(type.sub_types.length).toEqual(0);
        });
    });

    describe("findExisting", function() {
        it("returns an index of -1", function() {
            createController();
            var hash = ctrl.model.findExisting("test", { sub_types: [] });

            expect(hash.index).toEqual(-1);
        });

        it("returns an index of 0", function() {
            createController();
            var item = { id: 1, name: "test" };
            var hash = ctrl.model.findExisting(item, { sub_types: [item] });

            expect(hash.index).toEqual(0);
        });
    });
});

describe('hbContractorTypes', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        spyOn(api.contractorType, "all").and.returnValue($q.when({ data: {} }));

        $compile('<hb-contractor-types></hb-contractor-types>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls api contractorType all', function() {
            expect(api.contractorType.all).toHaveBeenCalled();
        });
    });
});