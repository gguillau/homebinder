(function() {
    "use strict";

    angular
        .module("hb.admin.binderLibrary.contractorTypes", [])
        .directive("hbContractorTypes", function() {
            return {
                restrict: "E",
                templateUrl: "admin/binderLibrary/contractorTypes/contractorTypes.tpl.html",
                controller: "AdminContractorTypesController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("AdminContractorTypesController", [
            "hb.framework.indexBase",
            "hb.resources",
            "hb.api",
            "Loading",
            "Notify",
            "$filter",
            AdminContractorTypesController
        ]);

    function AdminContractorTypesController(IndexBase, resources, api, loading, notify, $filter) {
        var Model = function() {
            // call the parent class
            IndexBase.call(this);
            // apply binderTemplates specific resource strings
            this.resources = angular.extend({}, this.resources, resources.contractorTypes);
            this.refreshCall = api.contractorType.all;
            this.deleteCall = api.contractorType.destroy;
            this.createCall = api.contractorType.create;
            this.updateCall = api.contractorType.update;
            // delete prop
            this.nameProperty = "name";
            this.itemsPerPage = 10;
            this.setHeaders();
            this.refresh();
            this.types = [];
        };

        Model.prototype = Object.create(IndexBase.prototype);

        angular.extend(Model.prototype, {

            searchSubTypes: function(value, type) {
                // don't do the search when no search value is provided just clear the list
                if (value === null ||
                    value === undefined ||
                    value.length < 3) {
                    this.options = [];
                    return;
                }

                var opts = {
                    page: 1,
                    count: 50,
                    search: value
                };

                api.contractorSubType.all(opts).then(
                    angular.bind(this, this.searchComplete, type),
                    angular.bind(this, this.searchError)
                );
            },

            searchComplete: function(type, response) {
                this.options = response.data.items.filter(angular.bind(this, function(item) {
                    var array = $filter("filter")(type.sub_types, {
                        name: item.name
                    });
                    return array.length < 1;
                }));
            },

            searchError: function(response) {
                notify.error(response.data);
            },

            addItem: function() {
                var item = {
                    name: "Untitled",
                    sub_types: [],
                    editMode: true
                };
                this.items.unshift(item);
            },

            setHeaders: function() {
                this.headers = this.resources.headers.map(angular.bind(this, function(attribute) {
                    var sortable = false,
                        orderBy = null,
                        sorted = false,
                        style = "col-xs-2";
                    if (attribute === "ID") {
                        sorted = true;
                        sortable = true;
                        orderBy = "contractor_types.id";
                    }
                    else if (attribute === "Type") {
                        sortable = true;
                        orderBy = "contractor_types.name";
                    }
                    else if (attribute === "Sub-Types") {
                        sortable = true;
                        style = "col-xs-5";
                    }
                    return {
                        name: attribute,
                        sortable: sortable,
                        sorted: sorted,
                        orderBy: orderBy,
                        style: style,
                        order: "desc"
                    };
                }));
                this.sortOption = this.headers[0];
            },

            editItem: function(type) {
                this.items.forEach(function(item) {
                    item.editMode = false;
                });
                this.types = type.sub_types;
                type.editMode = true;
            },

            cancel: function(type, index) {
                if (!type.id) {
                    this.items.splice(index, 1);
                }
                else {
                    type.editMode = false;
                    type.sub_types.forEach(function(item, index, object) {
                        if (!item.id) {
                            object.splice(index, 1);
                        }
                    });
                }
                this.types = [];
            },

            save: function(type, index) {
                type.processing = true;
                if (!type.id) {
                    this.createCall({ contractor_type: { name: type.name, verified: true }, sub_types: type.sub_types }).then(
                        angular.bind(this, this.saveSuccess, index),
                        angular.bind(this, this.saveError, index));
                }
                else {
                    this.updateCall(type.id, { contractor_type: { name: type.name, verified: true }, sub_types: type.sub_types }).then(
                        angular.bind(this, this.saveSuccess, index),
                        angular.bind(this, this.saveError, index));
                }
            },

            saveSuccess: function(index, response) {
                this.items[index] = response.data;
                notify.success("Saved!");
            },

            saveError: function(index, response) {
                this.items[index].processing = false;
                notify.error(response.data);
            },

            onSelect: function($select, $item, type) {
                // remove the item first and then add back again
                var index = this.types.indexOf($item);
                this.types.splice(index, 1);

                var hash = this.findExisting($item, type);
                if (hash.index < 0) {
                    type.sub_types.push(hash.item);
                    this.types.push(hash.item);
                }
            },

            onRemove: function($item, type) {
                var hash = this.findExisting($item, type);
                if (hash.index > -1) {
                    type.sub_types.splice(hash.index, 1);
                }
            },

            findExisting: function(item, type) {
                var name = null;
                if (!item.id) {
                    name = item.toLowerCase();
                    item = { name: name };
                }
                return { index: type.sub_types.indexOf(item), item: item };
            }
        });

        this.model = new Model();
    }

})();