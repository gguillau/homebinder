(function() {
    "use strict";

    angular
        .module("hb.admin.binderLibrary.maintenanceTemplates", [])
        .directive("hbMaintenanceTemplates", function() {
            return {
                restrict: "E",
                templateUrl: "admin/binderLibrary/maintenanceTemplates/maintenanceTemplates.tpl.html",
                controller: "AdminMaintenanceTemplatesController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("AdminMaintenanceTemplatesController", [
            "hb.framework.indexBase",
            "hb.resources",
            "hb.api",
            "PartnerTemplatesItemsEditModal",
            "Loading",
            "hb.utils",
            AdminMaintenanceTemplatesController
        ]);

    function AdminMaintenanceTemplatesController(IndexBase, resources, api, PartnerTemplatesItemsEditModal, loading, utils) {
        var Model = function() {
            // call the parent class
            IndexBase.call(this);
            // apply binderTemplates specific resource strings
            this.resources = angular.extend({}, this.resources, resources.maintenanceTemplates);
            this.refreshCall = api.maintenanceTemplates.all;
            this.deleteCall = api.maintenanceTemplates.destroy;
            this.itemsPerPage = 10;
            this.queryArgs = {
                library_template: true
            };
            this.today = utils.utils.getToday(false);
            this.setHeaders();
            this.refresh();
        };

        Model.prototype = Object.create(IndexBase.prototype);

        angular.extend(Model.prototype, {

            // Refresh callback. Updates the item list and totals
            onRefreshSuccess: function(response) {
                this.items = response.data.items;
                this.totalItems = response.data.total;
                this.validateDates();
                loading.close();
            },

            validateDates: function() {
                var x;
                for (x = 0; x < this.items.length; x++) {
                    var item = this.items[x];
                    this.validateItem(item);
                }
            },

            validateItem: function(item) {
                if (item.due_date) {
                    item.due_date = new Date(item.due_date);
                    item.due_date_read_only = true;
                    var first = Date.parse(item.due_date);
                    var second = Date.parse(this.today);
                    if (first < second) {
                        return true;
                    }
                    return false;
                }
                else if (item.frequency === "As Needed") {
                    item.due_date_read_only = false;
                    return false;
                }
                else {
                    item.due_date_read_only = false;
                    return false;
                }
            },

            validateDate: function(item) {
                var first = Date.parse(item.due_date);
                var second = Date.parse(this.today);
                if (first < second) {
                    return true;
                }
                return false;
            },

            addItem: function() {
                var item = {
                    name: "",
                    frequency: "Annual",
                    library_template: true,
                    due_date_read_only: true
                };
                PartnerTemplatesItemsEditModal.show({
                    templateUrl: "partners/templates/form/maintenanceItems/edit/edit.tpl.html",
                    controller: "PartnerTemplatesMaintenanceItemsEditController as ctrl",
                    item: item,
                    cfg: { showTypeaheads: false },
                    index: -1,
                    onSave: angular.bind(this, this.insertItem)
                });
            },

            editItem: function(item) {
                var index = this.items.indexOf(item);
                PartnerTemplatesItemsEditModal.show({
                    templateUrl: "partners/templates/form/maintenanceItems/edit/edit.tpl.html",
                    controller: "PartnerTemplatesMaintenanceItemsEditController as ctrl",
                    item: item,
                    cfg: { showTypeaheads: false },
                    index: index,
                    onSave: angular.bind(this, this.insertItem)
                });
            },

            insertItem: function(item, index) {
                if (index > -1) {
                    this.items[index] = item;
                }
                else {
                    this.items.unshift(item);
                }
            },

            setHeaders: function() {
                this.headers = this.resources.headers.map(angular.bind(this, function(attribute) {
                    var sortable = false,
                        orderBy = null,
                        sorted = false;
                    if (attribute === "ID") {
                        sorted = true;
                        sortable = true;
                        orderBy = "maintenance_templates.id";
                    }
                    else if (attribute === "Name") {
                        sortable = true;
                        orderBy = "maintenance_templates.name";
                    }
                    else if (attribute === "Binder Template") {
                        sortable = true;
                        orderBy = "maintenance_templates.binder_template_id";
                    }
                    else if (attribute === "Description") {
                        sortable = true;
                        orderBy = "maintenance_templates.description";
                    }
                    else if (attribute === "Library Template") {
                        sortable = true;
                        orderBy = "maintenance_templates.library_template";
                    }
                    return {
                        name: attribute,
                        sortable: sortable,
                        sorted: sorted,
                        orderBy: orderBy,
                        order: "desc"
                    };
                }));
                this.sortOption = this.headers[0];
            }
        });

        this.model = new Model();
    }

})();