describe("AdminMaintenanceTemplatesController", function() {
    var ctrl,
        controller,
        $log,
        api,
        notify,
        resources,
        loading,
        $modal,
        PartnerTemplatesItemsEditModal;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
        PartnerTemplatesItemsEditModal = $injector.get("PartnerTemplatesItemsEditModal");

        $modal = {
            show: function() {}
        };
    }));

    function createController() {
        ctrl = controller("AdminMaintenanceTemplatesController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "hb.resources": resources,
            "Loading": loading,
            "ModalService": $modal,
            "PartnerTemplatesItemsEditModal": PartnerTemplatesItemsEditModal
        });
    }

    describe("onRefreshSuccess", function() {
        it("sets the items and calls validateDates", function() {
            createController();
            spyOn(ctrl.model, "validateDates");
            ctrl.model.onRefreshSuccess({ data: { items: [] } });

            expect(ctrl.model.validateDates).toHaveBeenCalled();
        });
    });

    describe("validateDates", function() {
        it("calls validateItem", function() {
            createController();
            spyOn(ctrl.model, "validateItem");
            ctrl.model.items = [{ id: 1 }];
            ctrl.model.validateDates();

            expect(ctrl.model.validateItem).toHaveBeenCalled();
        });
    });

    describe('validateItem', function() {

        it('should call validateItem and return true', function() {
            createController();
            ctrl.model.items = [{
                id: 1,
                name: "Smoke Detector Battery",
                frequency: "Annual",
                due_date: "",
                due_date_read_only: false,
                interval_read_only: true
            }];
            var today = new Date();
            var yesterday = new Date(today.getTime());
            yesterday.setDate(today.getDate() - 1);
            ctrl.model.items[0].due_date = yesterday;
            var boolean_value = ctrl.model.validateItem(ctrl.model.items[0]);

            expect(boolean_value).toEqual(true);
            expect(ctrl.model.items[0].interval_read_only).toBe(true);
        });

        it('should call validateItem and return false', function() {

            createController();
            ctrl.model.items = [{
                id: 1,
                name: "Smoke Detector Battery",
                frequency: "Annual",
                due_date: "",
                due_date_read_only: false,
                interval_read_only: true
            }];
            var today = new Date();
            var tomorrow = new Date(today.getTime());
            tomorrow.setDate(today.getDate() + 1);
            ctrl.model.items[0].due_date = tomorrow;
            var boolean_value = ctrl.model.validateItem(ctrl.model.items[0]);

            expect(boolean_value).toEqual(false);
            expect(ctrl.model.items[0].interval_read_only).toBe(true);
        });

        it('should call validateItem and return false', function() {

            createController();
            ctrl.model.items = [{
                "name": "Change Water Filter",
                "frequency": "Annual",
                "due_date": ""
            }];
            ctrl.model.items[0].due_date = null;
            var boolean_value = ctrl.model.validateItem(ctrl.model.items[0]);

            expect(boolean_value).toEqual(false);
            expect(ctrl.model.items[0].due_date_read_only).toBe(false);
        });

        it('should call validateItem and return false', function() {

            createController();
            ctrl.model.items = [{
                "name": "Change Water Filter",
                "frequency": "As Needed",
                "due_date": ""
            }];
            ctrl.model.items[0].due_date = null;
            var boolean_value = ctrl.model.validateItem(ctrl.model.items[0]);

            expect(boolean_value).toEqual(false);
            expect(ctrl.model.items[0].due_date_read_only).toBe(false);
        });

    });

    describe('validateDate', function() {

        it('should call validateDate and return true', function() {

            createController();
            var today = new Date();
            var yesterday = new Date(today.getTime());
            yesterday.setDate(today.getDate() - 1);
            var boolean_value = ctrl.model.validateDate({
                due_date: yesterday
            });

            expect(boolean_value).toEqual(true);
        });

        it('should call validateDate and return false', function() {

            createController();
            var today = new Date();
            var tomorrow = new Date(today.getTime());
            tomorrow.setDate(today.getDate() + 1);
            var boolean_value = ctrl.model.validateDate(tomorrow);

            expect(boolean_value).toEqual(false);
        });
    });

    describe("setHeaders", function() {
        it("sets the headers", function() {
            createController();
            ctrl.model.setHeaders();

            expect(ctrl.model.headers.length).toEqual(7);
        });
    });

    describe('insertItem', function() {
        it('updates the item to the list', function() {
            createController();
            ctrl.model.items = [{
                id: 1,
                "name": "Smoke Detector Battery",
                "frequency": "Annual",
                "due_date": "",
                "due_date_read_only": false,
                "interval_read_only": true
            }];
            expect(ctrl.model.items[0].name).toEqual("Smoke Detector Battery");
            ctrl.model.insertItem({
                id: 1,
                "name": "Smoke Detector Battery 2",
                "frequency": "Annual",
                "due_date": "",
                "due_date_read_only": false,
                "interval_read_only": true
            }, 0);

            expect(ctrl.model.items[0].name).toEqual("Smoke Detector Battery 2");
        });

        it('adds the item to the list', function() {
            createController();
            ctrl.model.items = [{
                id: 1,
                "name": "Smoke Detector Battery",
                "frequency": "Annual",
                "due_date": "",
                "due_date_read_only": false,
                "interval_read_only": true
            }];
            var length = ctrl.model.items.length;
            ctrl.model.insertItem({
                id: length,
                "name": "Smoke Detector Battery 2",
                "frequency": "Annual",
                "due_date": "",
                "due_date_read_only": false,
                "interval_read_only": true
            }, -1);

            expect(ctrl.model.items[length].id).toEqual(length);
        });

    });

    describe('addItem', function() {
        it('should call modal', function() {

            createController();
            spyOn(PartnerTemplatesItemsEditModal, "show");
            ctrl.model.addItem();

            expect(PartnerTemplatesItemsEditModal.show).toHaveBeenCalled();
        });

    });

    describe('editItem', function() {
        it('should call modal', function() {

            createController();
            spyOn(PartnerTemplatesItemsEditModal, "show");
            ctrl.model.items = [{ id: 1 }];
            ctrl.model.editItem(ctrl.model.items[0]);

            expect(PartnerTemplatesItemsEditModal.show).toHaveBeenCalled();
        });
    });
});

describe('hbMaintenanceTemplates', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_, _$q_) {
        api = _$injector_.get("hb.api");
        $q = _$q_;
        spyOn(api.maintenanceTemplates, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));
        $scope = $rootScope.$new(), $compile = _$compile_;

        $compile('<hb-maintenance-templates></hb-maintenance-templates>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the title', function() {
            expect(api.maintenanceTemplates.all).toHaveBeenCalled();
        });
    });
});
