(function() {
    "use strict";

    angular
        .module("hb.admin.binderLibrary.templates", [])
        .directive("hbBinderTemplates", function() {
            return {
                restrict: "E",
                templateUrl: "admin/binderLibrary/templates/templates.tpl.html",
                controller: "AdminBinderTemplatesController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("AdminBinderTemplatesController", [
            "hb.framework.indexBase",
            "hb.resources",
            "hb.api",
            AdminBinderTemplatesController
        ]);

    function AdminBinderTemplatesController(IndexBase, resources, api) {
        var Model = function() {
            // call the parent class
            IndexBase.call(this);
            // apply binderTemplates specific resource strings
            this.resources = angular.extend({}, this.resources, resources.binderTemplates);
            this.refreshCall = api.binderTemplate.all;
            this.deleteCall = api.binderTemplate.destroy;
            this.itemsPerPage = 10;
            this.queryArgs = {
                system: true
            };
            // delete prop
            this.nameProperty = "name";
            this.setHeaders();
            this.refresh();
        };

        Model.prototype = Object.create(IndexBase.prototype);

        angular.extend(Model.prototype, {

            setHeaders: function() {
                this.headers = this.resources.headers.map(angular.bind(this, function(attribute) {
                    var sortable = false,
                        orderBy = null,
                        sorted = false;
                    if (attribute === "ID") {
                        sorted = true;
                        sortable = true;
                        orderBy = "binder_templates.id";
                    }
                    else if (attribute === "Name") {
                        sortable = true;
                        orderBy = "binder_templates.name";
                    }
                    else if (attribute === "Default") {
                        sortable = true;
                        orderBy = "binder_templates.default";
                    }
                    else if (attribute === "System") {
                        sortable = true;
                        orderBy = "binder_templates.system";
                    }
                    else if (attribute === "Partner") {
                        sortable = true;
                        orderBy = "binder_templates.partner";
                    }
                    return {
                        name: attribute,
                        sortable: sortable,
                        sorted: sorted,
                        orderBy: orderBy,
                        order: "desc"
                    };
                }));
                this.sortOption = this.headers[0];
            }
        });

        this.model = new Model();
    }

})();