angular.module('hb.admin.widgets', [
	])
	.config(['$stateProvider', function($stateProvider) {
		$stateProvider
			.state('admin.widgets', {
				url: '^/admin/widgets',
				template: '<hb-admin-widgets></hb-admin-widgets>',
                access: {
                    requiresLogin: true
                }
			});
	}])
	.directive("hbAdminWidgets", function() {
		return {
			restrict: "E",
			template: "<hb-widget-index cfg=\"ctrl.indexConfig\"></hb-widget-index>",
			controller: "AdminWidgetsController",
			controllerAs: "ctrl",
			bindToController: true,
			scope: {}
		};
	})
	.controller("AdminWidgetsController", [
		function() {
			this.indexConfig = {
				byAdmin: true
			};
		}
	]);
