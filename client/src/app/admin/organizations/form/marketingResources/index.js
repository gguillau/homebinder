(function() {
    "use strict";

    angular
        .module("hb.admin.organizations.form.marketingResources", [
            "ui.router",
            "ui.bootstrap"
        ])
        .directive("hbOrganizationResources", function() {
            return {
                restrict: "E",
                templateUrl: "admin/organizations/form/marketingResources/index.tpl.html",
                controller: "HBOrganizationResourcesController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    organizationId: "="
                }
            };
        })
        .controller("HBOrganizationResourcesController", [
            "MarketingResourcesIndexController",
            "$stateParams",
            HBOrganizationResourcesController
        ]);

    function HBOrganizationResourcesController(MarketingResourcesIndexController, $stateParams) {
        var that = this;
        var Model = function() {
            // call the parent class
            MarketingResourcesIndexController.call(this);
            // set the refresh API call
            this.refreshCall = this.api.marketingResource.all;
            this.queryArgs = {
                organizationId: this.organizationId
            };
            this.refresh();
        };

        Model.prototype = Object.create(MarketingResourcesIndexController.prototype);

        angular.extend(Model.prototype, {
            // Refresh callback. Updates the item list and totals
            onRefreshSuccess: function(response) {
                this.items = response.data.items.map(angular.bind(this, function(item) {
                    if (item.resource_type === "document") {
                        item.document_file = this.$sce.trustAsResourceUrl(item.document_file);
                    }
                    return item;
                }));
                this.totalItems = this.toolbarCfg.total = response.data.items.length;
            },

            newItem: function() {
                var formOpts = {};
                formOpts.organizationId = that.organizationId;
                formOpts.onSaved = angular.bind(this, this.onAdded);
                this.MarketingResourcesModal.showForm(formOpts);
            },

            editItem: function(item) {
                var formOpts = {};
                formOpts.marketingResource = item;
                formOpts.organizationId = that.organizationId;
                var index = this.items.indexOf(item);
                formOpts.onSaved = angular.bind(this, this.onEdited, index);
                this.MarketingResourcesModal.showForm(formOpts);
            }
        });

        this.model = new Model();
    }
})();