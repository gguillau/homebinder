angular
    .module("hb.admin.organizations.form", [
        "hb.admin.organizations.form.marketingResources"
    ])
    .directive("hbOrganizationForm", function() {
        return {
            restrict: "E",
            templateUrl: "admin/organizations/form/form.tpl.html",
            controller: "HBOrganizationFormController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {
                cfg: "=",
                onSaved: "&",
                onCancel: "&"
            }
        };
    })
    // Model backing the area modal form
    .controller("HBOrganizationFormController", [
        "$log",
        "hb.api",
        "Notify",
        "hb.resources",
        "Loading",
        function Controller($log, api, notify, resources, loading) {
            var that = this;
            var Model = function(cfg) {
                this.cfg = cfg;
                this.error422 = false;
                this.resources = resources.organizationForm;
                this.userLookupCfg = {
                    userSelected: angular.bind(this, this.userSelected),
                    clearOnSelect: true
                };
                this.partnerLookupCfg = {
                    partnerSelected: angular.bind(this, this.partnerSelected),
                    clearOnSelect: true
                };
                this.users = [];
                this.partners = [];
                this.hasOrg = false;
                this.populate();
            };

            angular.extend(Model.prototype, {
                populate: function() {
                    if (!this.cfg) {
                        return;
                    }

                    this.cfg.api = {
                        save: angular.bind(this, this.submit)
                    };
                    this.organization = this.cfg.item;
                    // if an org was passed in store the values
                    if (this.organization) {
                        this.name = this.organization.name;
                        this.description = this.organization.description;
                        this.hasOrg = true;

                        // get the users
                        api.user.all({ organizationId: this.organization.id, searchMethod: "for_organization" }).then(
                            angular.bind(this, this.populateArray, this.users),
                            angular.bind(this, this.handleError, this.resources.loadUsersError)
                        );
                        // get the partners
                        api.partner.all({ organizationId: this.organization.id, searchMethod: "for_organization" }).then(
                            angular.bind(this, this.populateArray, this.partners),
                            angular.bind(this, this.handleError, this.resources.loadPartnersError)
                        );
                    }
                },
                populateArray: function(arr, response) {
                    response.data.items.forEach(function(item) {
                        arr.push(item);
                    });
                },
                handleError: function(msg, response) {
                    $log.error(response.data);
                    notify.error(response.data);
                },
                submit: function() {
                    that.form.$submitted = true;

                    if (that.form.$invalid) {
                        return;
                    }

                    // create payload
                    var data = {
                            name: this.name,
                            description: this.description
                        },
                        promise;

                    this.error422 = false;
                    loading.show(this.resources.saving);
                    if (this.organization) {
                        // add area id and binder id for update
                        data.id = this.organization.id;
                        // do the update
                        promise = api.organization.update(data);
                    }
                    else {
                        // do the create
                        promise = api.organization.create(data);
                    }

                    /*jshint -W024*/
                    promise.then(angular.bind(this, this.onOrganizationSaved),
                            angular.bind(this, this.onOrganizationSaveError))
                        .finally(angular.bind(this, this.onDoneSaving));
                },
                onOrganizationSaved: function(response) {
                    this.organization = response.data;
                    this.cfg.item = this.organization;
                    that.onSaved({
                        item: this.organization
                    });
                },
                onOrganizationSaveError: function(response) {
                    if (response.status == 422) {
                        this.error422 = true;
                    }
                    else {
                        notify.error(this.resources.saveError);
                    }
                },
                onDoneSaving: function() {
                    loading.close();
                },
                userSelected: function(user) {
                    api.organization.addUsers(this.organization.id, [user.id]).then(
                        angular.bind(this, this.addToArray, this.users, user),
                        angular.bind(this, this.handleError, this.resources.addUserError)
                    );
                },
                partnerSelected: function(partner) {
                    api.organization.addPartners(this.organization.id, [partner.id]).then(
                        angular.bind(this, this.addToArray, this.partners, partner),
                        angular.bind(this, this.handleError, this.resources.addPartnerError)
                    );
                },
                addToArray: function(arr, item, response) {
                    arr.push(item);
                },
                removeUser: function(user) {
                    api.organization.removeUser(this.organization.id, user.id).then(
                        angular.bind(this, this.removeFromArray, this.users, user),
                        angular.bind(this, this.handleError, this.resources.removeUserError)
                    );
                },
                removePartner: function(partner) {
                    api.organization.removePartner(this.organization.id, partner.id).then(
                        angular.bind(this, this.removeFromArray, this.partners, partner),
                        angular.bind(this, this.handleError, this.resources.removePartnerError)
                    );
                },
                removeFromArray: function(arr, item, response) {
                    var k, len;
                    for (k = 0, len = arr.length; k < len; k++) {
                        if (arr[k].id == item.id) {
                            arr.splice(k, 1);
                            break;
                        }
                    }
                }
            });

            this.model = new Model(this.cfg);
        }
    ]);