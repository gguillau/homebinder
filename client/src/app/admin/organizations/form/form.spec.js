describe("HBOrganizationFormController", function() {
    var ctrl,
        controller,
        $q,
        $log,
        api,
        notify,
        resources,
        loading;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
    }));

    function createController() {
        ctrl = controller("HBOrganizationFormController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "hb.resources": resources,
            "Loading": loading
        });
    }

    describe("populate", function() {
        it("returns when no cfg is provided", function() {
            createController();
            ctrl.model.cfg = null;
            ctrl.model.populate();
        });

        it("populates the organization", function() {
            createController();
            ctrl.model.cfg = {
                item: {
                    name: "org",
                    description: "org description"
                }
            };
            ctrl.model.populate();

            expect(ctrl.model.organization).not.toBeNull();
            expect(ctrl.model.organization.name).toEqual("org");
            expect(ctrl.model.organization.description).toEqual("org description");
            expect(ctrl.model.cfg.api.save).not.toBeNull();
        });

        it("does not populate when no organization is provided", function() {
            createController();
            ctrl.model.cfg = {};
            ctrl.model.populate();

            expect(ctrl.model.cfg.api.save).not.toBeNull();
        });
    });

    describe("populateArray", function() {
        it("populates the array", function() {
            createController();
            ctrl.model.cfg = null;
            var response = {
                data: {
                    items: [{ id: 1 }]
                }
            };
            var array = [];
            ctrl.model.populateArray(array, response);

            expect(array.length).toEqual(1);
        });
    });

    describe("handleError", function() {
        it("calls notify", function() {
            createController();
            ctrl.model.cfg = null;
            spyOn(notify, "error");
            ctrl.model.handleError("test", { data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("submit", function() {
        it("does not submit invalid data", function() {
            spyOn(api.organization, "create").and.returnValue($q.when({}));
            createController();
            ctrl.model.cfg = {};
            ctrl.form = {
                $invalid: true
            };
            ctrl.model.submit();
            expect(ctrl.form.$submitted).toEqual(true);
            expect(api.organization.create).not.toHaveBeenCalled();
        });

        it("creates a new organization", function() {
            spyOn(api.organization, "create").and.returnValue($q.when({}));
            createController();
            ctrl.model.cfg = {};
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.name = "org";
            ctrl.model.description = "description";
            ctrl.model.submit();
            expect(api.organization.create).toHaveBeenCalledWith(jasmine.objectContaining({
                name: "org",
                description: "description"
            }));
        });

        it("updates an existing organization", function() {
            spyOn(api.organization, "update").and.returnValue($q.when({}));
            createController();
            ctrl.model.cfg = {};
            ctrl.model.organization = {
                id: 1
            };
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.name = "org";
            ctrl.model.description = "description";
            ctrl.model.submit();
            expect(api.organization.update).toHaveBeenCalledWith(jasmine.objectContaining({
                id: 1,
                name: "org",
                description: "description"
            }));
        });
    });

    describe("onOrganizationSaved", function() {
        it("saves the response and calls onSaved", function() {
            var org = {
                id: 1,
                name: "org",
                description: "description"
            };
            createController();
            ctrl.model.cfg = {};
            ctrl.onSaved = function() {};
            spyOn(ctrl, "onSaved");
            ctrl.model.onOrganizationSaved({ data: org });
            expect(ctrl.model.organization).toEqual(org);
            expect(ctrl.model.cfg.item).toEqual(org);
            expect(ctrl.onSaved).toHaveBeenCalledWith(jasmine.objectContaining({ item: org }));
        });
    });

    describe("onOrganizationSaveError", function() {
        it("handles a 422 error", function() {
            createController();
            ctrl.model.cfg = {};
            ctrl.model.onOrganizationSaveError({ status: 422, data: "error" });
            expect(ctrl.model.error422).toEqual(true);
        });

        it("handles a general error", function() {
            spyOn(notify, "error");
            createController();
            ctrl.model.cfg = {};
            ctrl.model.onOrganizationSaveError({ status: 500, data: "error" });
            expect(ctrl.model.error422).toEqual(false);
            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("onDoneSaving", function() {
        it("calls loading close", function() {
            createController();
            ctrl.model.cfg = null;
            spyOn(loading, "close");
            ctrl.model.onDoneSaving();

            expect(loading.close).toHaveBeenCalled();
        });
    });

    describe("userSelected", function() {
        it("calls api organization addUsers", function() {
            createController();
            ctrl.model.cfg = null;
            spyOn(api.organization, "addUsers").and.returnValue($q.when({ data: {} }));
            ctrl.model.organization = { id: 1 };
            ctrl.model.userSelected({ id: 1 });

            expect(api.organization.addUsers).toHaveBeenCalled();
        });
    });

    describe("partnerSelected", function() {
        it("calls api organization addPartners", function() {
            createController();
            ctrl.model.cfg = null;
            spyOn(api.organization, "addPartners").and.returnValue($q.when({ data: {} }));
            ctrl.model.organization = { id: 1 };
            ctrl.model.partnerSelected({ id: 1 });

            expect(api.organization.addPartners).toHaveBeenCalled();
        });
    });

    describe("addToArray", function() {
        it("populates the array", function() {
            createController();
            ctrl.model.cfg = null;
            var response = {
                data: {
                    items: [{ id: 1 }]
                }
            };
            var array = [];
            ctrl.model.addToArray(array, { id: 1 }, response);

            expect(array.length).toEqual(1);
        });
    });
    
    describe("removeUser", function() {
        it("calls api organization removeUser", function() {
            createController();
            ctrl.model.cfg = null;
            spyOn(api.organization, "removeUser").and.returnValue($q.when({ data: {} }));
            ctrl.model.organization = { id: 1 };
            ctrl.model.removeUser({ id: 1 });

            expect(api.organization.removeUser).toHaveBeenCalled();
        });
    });

    describe("removePartner", function() {
        it("calls api organization removePartner", function() {
            createController();
            ctrl.model.cfg = null;
            spyOn(api.organization, "removePartner").and.returnValue($q.when({ data: {} }));
            ctrl.model.organization = { id: 1 };
            ctrl.model.removePartner({ id: 1 });

            expect(api.organization.removePartner).toHaveBeenCalled();
        });
    });

    describe("removeFromArray", function() {
        it("removes from the array", function() {
            createController();
            ctrl.model.cfg = null;
            var response = {
                data: {
                    items: [{ id: 1 }]
                }
            };
            var array = [{ id: 1 }];
            expect(array.length).toEqual(1);
            ctrl.model.removeFromArray(array, { id: 1 }, response);

            expect(array.length).toEqual(0);
        });
    });
});

describe('hbOrganizationForm', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = {};
        $scope.save = function() {};
        $scope.cancel = function() {};

        element = $compile('<hb-organization-form cfg="cfg" on-saved="save" on-cancel="cancel"></hb-organization-form>>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the hb-organization-from directive', function() {
        it('displays the title', function() {
            expect(element.html()).toContain("Details");
        });
    });
});