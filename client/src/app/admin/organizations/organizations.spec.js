describe("HBOrganizationsController", function() {
    var controller,
        base,
        api,
        resources,
        modal;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        modal = $injector.get("OrganizationModal");

        spyOn(api.organization, "all").and.returnValue($q.when({ data: { total: 1, items: [{ id: 1, name: "name" }] } }));
        spyOn(api.organization, "destroy").and.returnValue($q.when({}));

        controller = $controller("HBOrganizationsController", {
            "hb.framework.indexBase": base,
            "hb.api": api,
            "hb.resources": resources,
            "OrganizationModal": modal
        });

        $rootScope.$apply();
    }));

    describe("refresh", function() {
        it("calls hb.api.organization.all", function() {
            controller.model.refresh();
            expect(api.organization.all).toHaveBeenCalled();
        });
    });

    describe("destroy", function() {
        it("calls hb.api.organization.destroy", function() {
            controller.model.onConfirmDeleteItem({ id: 1, name: "name" }, 0);
            expect(api.organization.destroy).toHaveBeenCalled();
        });
    });

    describe("add organization", function() {
        it("calls organizationModal.showForm", inject(function(OrganizationModal) {
            spyOn(OrganizationModal, "showForm");
            controller.model.addOrg();
            expect(OrganizationModal.showForm).toHaveBeenCalled();
        }));
    });

    describe("edit widget", function() {
        it("calls organizationModal.editForm", inject(function(OrganizationModal) {
            spyOn(OrganizationModal, "showForm");
            controller.model.editOrg({ id: 1, name: "org" });
            expect(OrganizationModal.showForm).toHaveBeenCalledWith(jasmine.objectContaining({ organization: { id: 1, name: "org" } }));
        }));
    });
});

describe('hbAdminOrganizations', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_, _$q_) {
        api = _$injector_.get("hb.api");
        $q = _$q_;
        spyOn(api.organization, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));
        $scope = $rootScope.$new(), $compile = _$compile_;

        $compile('<hb-admin-organizations></hb-admin-organizations>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the title', function() {
            expect(api.organization.all).toHaveBeenCalled();
        });
    });
});