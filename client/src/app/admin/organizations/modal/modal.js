angular
    .module("hb.admin.organizations.modal", [
    ])
    .factory("OrganizationModal", [
        "hb.framework.toggleModalBase",
        function(ModalBase) {
            var instance = null;
            var Modal = function() {
                ModalBase.call(this);

                this.templateUrl = "admin/organizations/modal/modal.tpl.html";
                this.controller = "OrganizationModalController as ctrl";
                this.windowClass = "organization-modal";
                this.objectProperty = "organization";
                this.objectIdProperty = "organizationId";
                this.nameProperty = "name";
            };
            
            Modal.prototype = Object.create(ModalBase.prototype);
            
            if (!instance) {
                instance = new Modal();
            }
            
            return instance;
        }
    ])
    .controller("OrganizationModalController", [
        "hb.framework.toggleModalControllerBase",
        "$modalInstance",
        "args",
        "hb.api",
        "hb.resources",
        function(ToggleModalControllerBase, $modalInstance, args, api, resources) {
            var Model = function() {
                this.$modalInstance = $modalInstance;
                this.args = args;                
                this.objectProperty = "organization";
                this.objectIdProperty = "organizationId";
                this.nameProperty = "name";
                this.populateCall = api.organization.get;
                
                this.resources = resources.organizationModal;
                
                ToggleModalControllerBase.call(this);
                
                this.init();
            };
            
            Model.prototype = Object.create(ToggleModalControllerBase.prototype);

            this.model = new Model();
        }
    ]);