describe("OrganizationModalController", function() {
    var controller,
        base,
        api,
        $modal;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });

        // mock the Notify service
        $provide.factory("Notify", function() {
            return {
                info: function() {},
                error: function() {}
            };
        });

        // mock the Session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return {
                        role: "admin"
                    };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("hb.framework.toggleModalControllerBase");
        api = $injector.get("hb.api");

        $modal = {};

        controller = $controller("OrganizationModalController", {
            "hb.framework.toggleModalControllerBase": base,
            "hb.api": api,
            "$modalInstance": $modal,
            "args": {}
        });

        $rootScope.$apply();
    }));

    describe("init", function() {
        it("sets attributes", function() {
            expect(controller.model.objectProperty).toBe("organization");
        });
    });
});