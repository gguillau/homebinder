angular
    .module('hb.admin.organizations', [
        "hb.admin.organizations.modal",
        "hb.admin.organizations.form"
    ])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('admin.organizations', {
                url: '^/admin/organizations',
                template: '<hb-admin-organizations></hb-admin-organizations>',
                access: {
                    requiresLogin: true
                }
            });
    }])
    .directive("hbAdminOrganizations", function() {
        return {
            restrict: "E",
            templateUrl: "admin/organizations/organizations.tpl.html",
            controller: "HBOrganizationsController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {}
        };
    })
    .controller("HBOrganizationsController", [
        'hb.framework.indexBase',
        'hb.api',
        'hb.resources',
        'OrganizationModal',
        function(IndexBase, api, resources, organizationModal) {
            var Model = function() {
                // call the parent class
                IndexBase.call(this);

                // apply org specific resource strings
                this.resources = angular.extend({}, this.resources, resources.organizations);

                // set the refresh API call
                this.refreshCall = api.organization.all;
                // set the delete API call
                this.deleteCall = api.organization.destroy;
                // set the name property used in the delete message
                this.nameProperty = "name";

                // set page specific values on toolbar
                angular.extend(this.toolbarCfg, {
                    title: this.resources.title,
                    button: {
                        title: this.resources.newButton,
                        click: angular.bind(this, this.addOrg)
                    },
                    sort: {
                        title: this.resources.sortTitle,
                        sortOptions: [
                            { desc: this.resources.sortById, orderBy: "organizations.id", order: "asc" },
                            { desc: this.resources.sortByIdDesc, orderBy: "organizations.id", order: "desc" },
                            { desc: this.resources.sortByName, orderBy: "organizations.name", order: "asc" },
                            { desc: this.resources.sortByNameDesc, orderBy: "organizations.name", order: "desc" }
                        ],
                        execute: angular.bind(this, this.sort)
                    }
                });
                this.toolbarCfg.sort.sortOption = this.toolbarCfg.sort.sortOptions[2];

                // refresh the list
                this.refresh();
            };

            Model.prototype = Object.create(IndexBase.prototype);

            angular.extend(Model.prototype, {
                addOrg: function() {
                    organizationModal.showForm({
                        onSaved: angular.bind(this, this.onAdded)
                    });
                },
                editOrg: function(org) {
                    organizationModal.showForm({
                        organization: org,
                        onSaved: angular.bind(this, this.onUpdated)
                    });
                }
            });

            this.model = new Model();
        }
    ]);