describe("AdminSharesController", function() {
    var ctrl,
        controller;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
    }));

    function createController() {
        ctrl = controller("AdminSharesController", {});
    }

    describe("init", function() {
        it("sets the settings", function() {
            createController();
            expect(ctrl.model.settings.canSearch).toBe(true);
        });
    });
});

describe('hbShares', function() {
    var $scope, $compile, element, $q, api;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.share, "all").and.returnValue($q.when({ data: { items: [] } }));

        element = $compile('<hb-shares></hb-shares>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("Share");
        });
    });
});
