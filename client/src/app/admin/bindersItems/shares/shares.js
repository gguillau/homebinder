(function() {
    "use strict";

    angular
        .module("hb.admin.bindersItems.shares", [])
        .directive("hbShares", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/share/index/index.tpl.html",
                controller: "AdminSharesController as ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("AdminSharesController", [
            "SharesController",
            AdminSharesController
        ]);

    function AdminSharesController(SharesController) {
        var Model = function() {
            // call the parent class
            SharesController.call(this);
            this.settings.canSearch = true;
            this.refresh();
        };

        Model.prototype = Object.create(SharesController.prototype);

        this.model = new Model();
    }

})();