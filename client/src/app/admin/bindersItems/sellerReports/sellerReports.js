(function() {
	"use strict";

	angular.module("hb.admin.bindersItems.sellerReports", [])
		.directive("hbSellerReports", function() {
			return {
				restrict: "E",
				templateUrl: "admin/bindersItems/sellerReports/sellerReports.tpl.html",
				controller: "AdminSellerReportsController as ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("AdminSellerReportsController", [
			"hb.framework.indexBase",
			"hb.api",
			"hb.resources",
			"$location",
			"$window",
			AdminSellerReportsController
		]);

	function AdminSellerReportsController(IndexBase, api, resources, $location, $window) {

		var Model = function() {
			// call the parent class
			IndexBase.call(this);
			this.api = api;
			// apply sellerReport specific resource strings
			this.resources = angular.extend({}, this.resources, resources.sellerReportIndex);
			// set the refresh API call
			this.refreshCall = api.sellerReport.all;
			// set the delete API call
			this.deleteCall = api.sellerReport.destroy;
			// delete prop
			this.nameProperty = "binder_id";
			this.maxSize = 10;
			this.sortOptions = [{
				orderBy: "seller_reports.created_at",
				order: "desc",
				desc: this.resources.creationDate + " - " + this.resources.descending
			}, {
				orderBy: "seller_reports.created_at",
				order: "asc",
				desc: this.resources.creationDate + " - " + this.resources.ascending
			}];
			this.sortOption = this.sortOptions[0];
			this.orderBy = this.sortOption.orderBy;
			this.order = this.sortOption.order;
			this.headers = null;
			this.setHeaders();
			this.refresh();
		};

		Model.prototype = Object.create(IndexBase.prototype);

		angular.extend(Model.prototype, {
			setHeaders: function() {
				this.columnspan = 6;
				this.headers = this.resources.reportAttributes.map(angular.bind(this, function(attribute) {
					var sortable = false,
						orderBy = null,
						sorted = false,
						show = true;
					if (attribute === "Binder ID") {
						sorted = true;
						sortable = true;
						orderBy = "seller_reports.binder_id";
					}
					else if (attribute === "Code") {
						sortable = true;
						orderBy = "seller_reports.code";
					}
					else if (attribute === "Created On") {
						sortable = true;
						orderBy = "seller_reports.created_at";
					}
					else if (attribute === "Public") {
						sortable = true;
						orderBy = "seller_reports.public";
					}
					return {
						name: attribute,
						sortable: sortable,
						sorted: sorted,
						orderBy: orderBy,
						show: show,
						order: "desc"
					};
				}));

				this.sortOption = this.headers[4];
			},

			viewReport: function(report) {
				var url = 'http://' + $location.host();
				if ($location.port() != '80') {
					url += ":" + $location.port();
				}
				url += '/';
				$window.open(url + 'sellerreport/' + report.code);
			}

		});

		this.model = new Model();
	}

})();