describe("AdminSellerReportsController", function() {
    var controller,
        base,
        $location;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getJwt: function() {},
                getLocale: function() {
                    return null;
                },
                setLocale: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("hb.framework.indexBase");
        $location = $injector.get("$location");

        controller = $controller("AdminSellerReportsController", {
            "hb.framework.indexBase": base
        });
    }));

    describe("setHeaders", function() {
        it("sets the headers", function() {
            controller.model.setHeaders();

            expect(controller.model.headers.length).toEqual(5);
        });
    });

    describe("viewReport", function() {
        it("calls window open", inject(function($window) {
            spyOn($window, "open");
            spyOn($location, "port").and.returnValue("3000");

            controller.model.viewReport({ code: 1 });

            expect($window.open).toHaveBeenCalled();
        }));
    });

});

describe('hbSellerReports', function() {
    var $scope, $compile, element, $q, api;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.sellerReport, "all").and.returnValue($q.when({ data: { items: [] } }));

        element = $compile('<hb-seller-reports></hb-seller-reports>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {
            expect(element.html()).toContain("Seller Report");
        });
    });
});