(function() {
    "use strict";

    angular
        .module("hb.admin.bindersItems.maintenanceItems", [])
        .directive("hbMaintenanceItems", function() {
            return {
                restrict: "E",
                templateUrl: "admin/bindersItems/maintenanceItems/maintenanceItems.tpl.html",
                controller: "AdminMaintenanceItemsController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("AdminMaintenanceItemsController", [
            "BinderMaintenanceItemsController",
            "hb.resources",
            AdminMaintenanceItemsController
        ]);

    function AdminMaintenanceItemsController(BinderMaintenanceItemsController, resources) {
        var Model = function() {
            // call the parent class
            BinderMaintenanceItemsController.call(this);
            this.resources = angular.extend({}, this.resources, resources.maintenanceItemsIndex);
            this.itemsPerPage = 10;
            this.maintenanceItemsArgs = {
                binderId: undefined,
                name: undefined,
                do_date: undefined
            };
            // delete prop
            this.nameProperty = "name";
            this.current_filter = null;
            this.datepicker = {
                format: "MMMM dd, yyyy",
                options: {
                    "show-button-bar": false
                }
            };
            this.sortOptions = [{
                orderBy: "maintenance_items.created_at",
                order: "desc",
                desc: this.resources.creationDate + " - " + this.resources.descending
            }, {
                orderBy: "maintenance_items.created_at",
                order: "asc",
                desc: this.resources.creationDate + " - " + this.resources.ascending
            }, {
                orderBy: "maintenance_items.name",
                order: "desc",
                desc: this.resources.name + " - " + this.resources.descending
            }, {
                orderBy: "maintenance_items.name",
                order: "asc",
                desc: this.resources.name + " - " + this.resources.ascending
            }];
            this.sortOption = this.sortOptions[0];
            this.orderBy = this.sortOption.orderBy;
            this.order = this.sortOption.order;
            this.date_opened = false;
            this.setHeaders();
            this.itemDetailsState = "binder.maintenanceItemsDetails";
            this.init();
        };

        Model.prototype = Object.create(BinderMaintenanceItemsController.prototype);

        angular.extend(Model.prototype, {

            setHeaders: function() {
                this.headers = this.resources.attributes.map(angular.bind(this, function(attribute) {
                    var sortable = false,
                        orderBy = null,
                        sorted = false;
                    if (attribute === "ID") {
                        sorted = true;
                        sortable = true;
                        orderBy = "maintenance_items.id";
                    }
                    else if (attribute === "Name") {
                        sortable = true;
                        orderBy = "maintenance_items.name";
                    }
                    else if (attribute === "Binder ID") {
                        sortable = true;
                        orderBy = "maintenance_items.binder_id";
                    }
                    return {
                        name: attribute,
                        sortable: sortable,
                        sorted: sorted,
                        orderBy: orderBy,
                        order: "desc"
                    };
                }));
                this.headerOptions = angular.copy(this.headers);
                this.sortOption = this.headers[0];
            },

            addQueryArgs: function() {
                this.addArgs("binderId");
                this.addArgs("name");
                this.addArgs("do_date");
            },

            addArgs: function(arg) {
                if (this.maintenanceItemsArgs[arg]) {
                    this.queryArgs[arg] = this.maintenanceItemsArgs[arg];
                }
                else {
                    delete this.queryArgs[arg];
                }
            }
        });

        this.model = new Model();
    }

})();