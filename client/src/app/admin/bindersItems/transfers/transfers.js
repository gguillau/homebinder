(function() {
    "use strict";

    angular
        .module("hb.admin.bindersItems.transfers", [
            "hb.admin.bindersItems.transfers.adminModal"
        ])
        .directive("hbTransfers", function() {
            return {
                restrict: "E",
                templateUrl: "homebinders/homebinder/transfer/index/index.tpl.html",
                controller: "AdminTransfersController as ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("AdminTransfersController", [
            "TransfersController",
            "AdminTransferModal",
            AdminTransfersController
        ]);

    function AdminTransfersController(TransfersController, AdminTransferModal) {
        var Model = function() {
            // call the parent class
            TransfersController.call(this);
            this.settings.canSearch = true;
            this.refresh();
        };

        Model.prototype = Object.create(TransfersController.prototype);
        
        angular.extend(Model.prototype, {
			edit: function(transfer) {
				AdminTransferModal.show({
					transfer: transfer,
					closed: angular.bind(this, this.removeItem)
				});
			}
        });

        this.model = new Model();
    }

})();