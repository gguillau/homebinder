(function() {
    "use strict";

    angular
        .module("hb.admin.bindersItems.transfers.adminModal", [])
        .factory("AdminTransferModal", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "admin/bindersItems/transfers/adminModal/adminModal.tpl.html",
                            controller: "AdminTransferModalController as ctrl",
                            resolveData: opts,
                            closed: opts.closed
                        });
                    }
                };
            }
        ])
        .controller("AdminTransferModalController", [
            "$modalInstance",
            "data",
            "hb.api",
            "Notify",
            "hb.resources",
            AdminTransferModalController
        ]);

    function AdminTransferModalController($modal, opts, api, notify, resources) {
        this.$modal = $modal;
        this.transfer = opts.transfer;
        this.api = api;
        this.notify = notify;
        this.resources = resources.adminTransferModal;
        this.binderLookupCfg = {
            allowClear: true,
            placeholder: "Search for binder",
            refresh: angular.bind(this, this.binderLookup),
            itemTemplate: "components/lookup/partner/partnerDropDownItem.tpl.html",
            selectionTemplate: "components/lookup/partner/partnerSelection.tpl.html"
        };
        // "sent", "created", "pending", "declined", "accepted", "delivery_failed"
        this.statusOptions = [{
            name: "sent",
            value: "Sent"
        }, {
            name: "created",
            value: "Created"
        }, {
            name: "pending",
            value: "Pending"
        }, {
            name: "declined",
            value: "Declined"
        }, {
            name: "accepted",
            value: "Accepted"
        }, {
            name: "delivery_failed",
            value: "Delivery Failed"
        }];
        this.populateForm();
    }

    AdminTransferModalController.prototype = {
        
        // Updates model to display strings in form
        populateForm: function() {
            this.binder = this.transfer.binder;
            this.sender = this.transfer.sender;
            this.receiver = this.transfer.receiver;
            this.status = this.transfer.status;
        },
        
        // Executes when form is submitted
        submitForm: function() {
            // Update members of the transfer object
            var updatedTransfer = {
                binder_id: this.binder.id,
                sender_id: this.sender.id,
                receiver_id: this.receiver.id
            };
            
            // Update transfer object in api
            this.api.transfer
                .update(this.transfer.id, updatedTransfer)
                .then(
                    angular.bind(this, this.onSave),
                    angular.bind(this, this.onError)
                );
        },
        
        // Successful update
        onSave: function(response) {
            this.notify.success(this.resources.updatedText);
            this.updateUI();
            this.$modal.close(response);
        },

        // Failed update
        onError: function(response) {
            this.notify.error(response.data);
        },
        
        // Updates strings in UI to reflect update
        updateUI: function() {
            this.transfer.binder_id = this.binder.id;
            this.transfer.sender = this.sender;
            this.transfer.receiver = this.receiver;
        },
        
        // Close modal
        closeModal: function() {
            this.$modal.dismiss("cancel");
        },
        
        // Binder Lookup field controller
        binderLookup: function(value) {
            var opts = {
                page: 1,
                count: 50,
                search: value,
                searchMethod: "for_admin"
            };
            return this.api.binder.all(opts).then(
                angular.bind(this, function(response) {
                    return {
                        data: response.data.items
                    };
                })
            );
        }
    };
})();