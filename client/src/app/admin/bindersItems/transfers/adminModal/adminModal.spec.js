describe("AdminTransferModalController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        opts,
        $modal,
        api,
        notify,
        $q,
        $templateCache,
        $compile,
        template;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$templateCache_, _$compile_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        $compile = _$compile_;
        $templateCache = _$templateCache_;
        api = _$injector_.get("hb.api");

        $modal = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };

        opts = {
            transfer: {
                id: 1,
                binder_id: 1,
                sender: {
                    id: 1
                },
                receiver: {
                    id: 2
                },
                status: "sent"
            }
        };

        notify = {
            success: function() {},
            error: function() {}
        };
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("AdminTransferModalController", {
            "$modalInstance": $modal,
            "data": opts,
            "hb.api": api,
            "Notify": notify
        });

        $scope.ctrl = ctrl;
        template = $templateCache.get("admin/bindersItems/transfers/adminModal/adminModal.tpl.html");
        $compile(template)($scope);
    }

    describe('form', function() {
        it('should expect form to be defined and error required to be true', function() {

            createController();

            ctrl.binder = undefined;
            ctrl.sender = undefined;
            ctrl.receiver = undefined;

            $rootScope.$apply();

            expect(ctrl.form).toBeDefined();
            expect(ctrl.form.$valid).toBe(false);
            expect(ctrl.form.binder.$error.required).toBe(true);
            expect(ctrl.form.sender.$error.required).toBe(true);
            expect(ctrl.form.receiver.$error.required).toBe(true);
        });

        it('should expect form to be defined and form validation to be true', function() {

            createController();

            ctrl.binder = {
                id: 1
            },
            ctrl.sender = {
                email: "dev@homebinder.com"
            };
            ctrl.receiver = {
                email: "dev@homebinder.com"
            };
            
            $rootScope.$apply();

            expect(ctrl.form).toBeDefined();
            expect(ctrl.form.$valid).toBe(true);
        });

    });
    
    describe('ctrl.populateForm', function() {
        it("should populate the fields of the form with data from ctrl.transfer", function() {

            createController();
            
            ctrl.transfer = {
                binder: {
                    id: 1
                },
                sender: {
                    id: 1
                },
                receiver: {
                    id: 2
                },
                status: "pending"
            };

            $rootScope.$apply();
            ctrl.populateForm();
            $rootScope.$apply();

            expect(ctrl.binder).toEqual(ctrl.transfer.binder);
            expect(ctrl.sender).toEqual(ctrl.transfer.sender);
            expect(ctrl.receiver).toEqual(ctrl.transfer.receiver);
            expect(ctrl.status).toEqual(ctrl.transfer.status);

        });
    });

    describe('ctrl.submitForm', function() {
        it("should submit the form and call the update transfer function", function() {

            spyOn(api.transfer, "update").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(notify, "success");
            spyOn($modal, "close");

            createController();
            ctrl.binder = {
                id: 1
            };
            ctrl.sender = {
                id: 2
            };
            ctrl.receiver = {
                id: 3
            };
            $rootScope.$apply();
            ctrl.submitForm();
            $rootScope.$apply();

            expect(api.transfer.update).toHaveBeenCalled();
            expect(notify.success).toHaveBeenCalledWith("Transfer updated!");
            expect($modal.close).toHaveBeenCalled();

        });

        it("should submit the form and call the update transfer function but return an error", function() {

            spyOn(api.transfer, "update").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(notify, "error");

            createController();
            ctrl.binder = {
                id: 1
            };
            ctrl.sender = {
                id: 2
            };
            ctrl.receiver = {
                id: 3
            };
            $rootScope.$apply();
            ctrl.submitForm();
            $rootScope.$apply();

            expect(api.transfer.update).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();

        });
    });

    describe('ctrl.onSave', function() {
        it('calls notify, updateUI, and $modal.close', function() {
            
            spyOn(notify, "success");
            spyOn($modal, "close");
            
            createController();
            
            spyOn(ctrl, "updateUI");
            ctrl.binder = {
                id: 1
            };
            ctrl.reciever = 1;
            ctrl.sender = 1;
            $rootScope.$apply();
            ctrl.onSave();
            $rootScope.$apply();

            expect(notify.success).toHaveBeenCalledWith("Transfer updated!");
            expect(ctrl.updateUI).toHaveBeenCalled();
            expect($modal.close).toHaveBeenCalled();
        });
    });
    
    describe('ctrl.onError', function() {
        it('calls notify on error', function() {
            
            spyOn(notify, "error");
            
            createController();
            var response = {
                data: "error"
            };
            ctrl.onError(response);
            $rootScope.$apply();

            expect(notify.error).toHaveBeenCalled();
        });
    });
    
    describe('ctrl.updateUI', function() {
        it('sets binder_id, sender, and receiver from transfer', function() {

            createController();
            ctrl.binder = {
                id: 1
            };
            ctrl.reciever = 1;
            ctrl.sender = 1;
            $rootScope.$apply();
            ctrl.updateUI();
            $rootScope.$apply();

            expect(ctrl.transfer.binder_id).toEqual(ctrl.binder.id);
            expect(ctrl.transfer.sender).toEqual(ctrl.sender);
            expect(ctrl.transfer.receiver).toEqual(ctrl.receiver);
        });
    });
    
    describe("ctrl.closeModal", function() {
        it("should call $modal dismiss function", function() {

            spyOn($modal, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.closeModal();
            $rootScope.$apply();

            expect($modal.dismiss).toHaveBeenCalledWith('cancel');
        });
    });
    
    describe("ctrl.binderLookup", function() {
        it("returns a list of binders", function() {
            spyOn(api.binder, "all").and.returnValue($q.when({ data: { items: [{ id: 1, name: "test" }] } }));
            createController();
            var promise = ctrl.binderLookup("test");
            $scope.$apply();

            expect(promise.$$state.value.data.length).toEqual(1);
        });
    });
});
