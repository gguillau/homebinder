describe("AdminTransfersController", function() {
    var ctrl,
        controller;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
    }));

    function createController() {
        ctrl = controller("AdminTransfersController", {
        });
    }

    describe("init", function() {
        it("sets the settings", function() {
            createController();
            expect(ctrl.model.settings.canSearch).toBe(true);
        });
    });
});

describe('hbTransfers', function() {
    var $scope, $compile, element, $q, api;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.transfer, "all").and.returnValue($q.when({ data: { items: [] } }));

        element = $compile('<hb-transfers></hb-transfers>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("Transfers");
        });
    });
});
