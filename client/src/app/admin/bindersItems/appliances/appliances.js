(function() {
    "use strict";

    angular
        .module("hb.admin.bindersItems.appliances", [])
        .directive("hbAppliances", function() {
            return {
                restrict: "E",
                templateUrl: "admin/bindersItems/appliances/appliances.tpl.html",
                controller: "AdminAppliancesController as ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("AdminAppliancesController", [
            "BinderAppliancesController",
            "hb.resources",
            AdminAppliancesController
        ]);

    function AdminAppliancesController(BinderAppliancesController, resources) {
        var Model = function() {
            // call the parent class
            BinderAppliancesController.call(this);
            this.resources = angular.extend({}, this.resources, resources.appliancesIndex);
            this.itemsPerPage = 10;
            this.applianceArgs = {
                verified: undefined,
                binderId: undefined,
                name: undefined,
                images: true,
                no_model: true,
                no_serial: false,
                no_make: true
            };
            this.current_filter = null;
            this.sortOptions = [{
                orderBy: "appliances.created_at",
                order: "desc",
                desc: this.resources.creationDate + " - " + this.resources.descending
            }, {
                orderBy: "appliances.created_at",
                order: "asc",
                desc: this.resources.creationDate + " - " + this.resources.ascending
            }, {
                orderBy: "appliances.name",
                order: "desc",
                desc: this.resources.name + " - " + this.resources.descending
            }, {
                orderBy: "appliances.name",
                order: "asc",
                desc: this.resources.name + " - " + this.resources.ascending
            }];
            this.sortOption = this.sortOptions[0];
            this.orderBy = this.sortOption.orderBy;
            this.order = this.sortOption.order;
            this.setHeaders();
            this.itemDetailsState = "binder.appliancesDetails";
            this.init();
        };

        Model.prototype = Object.create(BinderAppliancesController.prototype);

        angular.extend(Model.prototype, {

            setHeaders: function() {
                this.headers = this.resources.attributes.map(angular.bind(this, function(attribute) {
                    var sortable = false,
                        orderBy = null,
                        sorted = false;
                    if (attribute === "ID") {
                        sorted = true;
                        sortable = true;
                        orderBy = "appliances.id";
                    }
                    else if (attribute === "Name") {
                        sortable = true;
                        orderBy = "appliances.name";
                    }
                    else if (attribute === "Binder ID") {
                        sortable = true;
                        orderBy = "appliances.binder_id";
                    }
                    return {
                        name: attribute,
                        sortable: sortable,
                        sorted: sorted,
                        orderBy: orderBy,
                        order: "desc"
                    };
                }));
                this.headerOptions = angular.copy(this.headers);
                this.sortOption = this.headers[0];
            },

            addQueryArgs: function() {
                this.addArgs("binderId");
                this.addArgs("name");
                this.addArgs("verified");
                this.addArgs("name");
                this.addArgs("images");
                this.addArgs("no_make");
                this.addArgs("no_model");
                this.addArgs("no_serial");
            },

            addArgs: function(arg) {
                if (this.applianceArgs[arg]) {
                    this.queryArgs[arg] = this.applianceArgs[arg];
                }
                else {
                    delete this.queryArgs[arg];
                }
            }
        });

        this.model = new Model();
    }

})();