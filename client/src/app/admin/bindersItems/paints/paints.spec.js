describe("AdminPaintsController", function() {
    var ctrl,
        controller,
        $log,
        api,
        notify,
        resources,
        loading,
        $modal,
        $q;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");

        $modal = {
            show: function() {}
        };
    }));

    function createController() {
        ctrl = controller("AdminPaintsController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "hb.resources": resources,
            "Loading": loading,
            "ModalService": $modal
        });
    }

    describe("setHeaders", function() {
        it("sets the headers", function() {
            createController();
            ctrl.model.setHeaders();

            expect(ctrl.model.headers.length).toEqual(7);
        });
    });

    describe("addQueryArgs", function() {
        it("calls addArgs", function() {
            createController();
            spyOn(ctrl.model, "addArgs");
            ctrl.model.addQueryArgs();

            expect(ctrl.model.addArgs).toHaveBeenCalled();
        });
    });

    describe("addArgs", function() {
        it("adds the arg", function() {
            createController();
            ctrl.model.paintArgs["binderId"] = 1;
            ctrl.model.addArgs("binderId");

            expect(ctrl.model.queryArgs["binderId"]).toEqual(1);
        });

        it("removes the arg", function() {
            createController();
            ctrl.model.paintArgs["binderId"] = 1;
            ctrl.model.addArgs("binderId");
            expect(ctrl.model.queryArgs["binderId"]).toBe(1);

            ctrl.model.paintArgs["binderId"] = undefined;
            ctrl.model.addArgs("binderId");
            expect(ctrl.model.queryArgs["binderId"]).toBe(undefined);
        });
    });
});

describe('hbPaints', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_, _$q_) {
        api = _$injector_.get("hb.api");
        $q = _$q_;
        spyOn(api.paint, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));
        $scope = $rootScope.$new(), $compile = _$compile_;

        $compile('<hb-paints></hb-paints>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the title', function() {
            expect(api.paint.all).toHaveBeenCalled();
        });
    });
});