(function() {
    "use strict";

    angular
        .module("hb.admin.bindersItems.paints", [])
        .directive("hbPaints", function() {
            return {
                restrict: "E",
                templateUrl: "admin/bindersItems/paints/paints.tpl.html",
                controller: "AdminPaintsController as ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("AdminPaintsController", [
            "BinderItemsIndexController",
            "hb.resources",
            AdminPaintsController
        ]);

    function AdminPaintsController(BinderItemsIndexController, resources) {
        var Model = function() {
            // call the parent class
            BinderItemsIndexController.call(this);
            this.resources = angular.extend({}, this.resources, resources.binderPaintsIndex);
            this.itemsPerPage = 10;
            this.paintArgs = {
                verified: undefined,
                binderId: undefined,
                name: undefined,
                images: true
            };
            this.current_filter = null;
            this.sortOptions = [{
                orderBy: "paints.created_at",
                order: "desc",
                desc: this.resources.creationDate + " - " + this.resources.descending
            }, {
                orderBy: "paints.created_at",
                order: "asc",
                desc: this.resources.creationDate + " - " + this.resources.ascending
            }, {
                orderBy: "paints.name",
                order: "desc",
                desc: this.resources.name + " - " + this.resources.descending
            }, {
                orderBy: "paints.name",
                order: "asc",
                desc: this.resources.name + " - " + this.resources.ascending
            }];
            this.sortOption = this.sortOptions[0];
            this.orderBy = this.sortOption.orderBy;
            this.order = this.sortOption.order;
            this.refreshCall = this.api.paint.all;
            this.itemType = "paints";
            this.modalController = "PaintModalController";
            this.itemDetailsState = "binder.paintsDetails";
            this.setHeaders();
            this.itemDetailsState = "binder.paintsDetails";
            this.init();
        };

        Model.prototype = Object.create(BinderItemsIndexController.prototype);

        angular.extend(Model.prototype, {

            setHeaders: function() {
                this.headers = this.resources.attributes.map(angular.bind(this, function(attribute) {
                    var sortable = false,
                        orderBy = null,
                        sorted = false;
                    if (attribute === "ID") {
                        sorted = true;
                        sortable = true;
                        orderBy = "paints.id";
                    }
                    else if (attribute === "Name") {
                        sortable = true;
                        orderBy = "paints.name";
                    }
                    else if (attribute === "Binder ID") {
                        sortable = true;
                        orderBy = "paints.binder_id";
                    }
                    return {
                        name: attribute,
                        sortable: sortable,
                        sorted: sorted,
                        orderBy: orderBy,
                        order: "desc"
                    };
                }));
                this.headerOptions = angular.copy(this.headers);
                this.sortOption = this.headers[0];
            },

            addQueryArgs: function() {
                this.addArgs("binderId");
                this.addArgs("name");
                this.addArgs("verified");
                this.addArgs("name");
                this.addArgs("images");
            },

            addArgs: function(arg) {
                if (this.paintArgs[arg]) {
                    this.queryArgs[arg] = this.paintArgs[arg];
                }
                else {
                    delete this.queryArgs[arg];
                }
            }
        });

        this.model = new Model();
    }

})();