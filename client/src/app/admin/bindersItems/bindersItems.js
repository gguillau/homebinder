(function() {
	"use strict";

	angular.module("hb.admin.bindersItems", [
			"hb.admin.bindersItems.appliances",
			"hb.admin.bindersItems.maintenanceItems",
			"hb.admin.bindersItems.paints",
			"hb.admin.bindersItems.transfers",
			"hb.admin.bindersItems.shares",
			"hb.admin.bindersItems.sellerReports"
		])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
				.state("admin.bindersItems", {
					url: "^/admin/bindersItems",
					templateUrl: "admin/bindersItems/bindersItems.tpl.html",
					controller: "AdminBindersItemsCtrl",
					controllerAs: "ctrl",
					access: {
						requiresLogin: true
					}
				});
		}])
		.controller("AdminBindersItemsCtrl", [
			AdminBindersItemsCtrl
		]);

	function AdminBindersItemsCtrl() {
		this.navigation_links = [{
			name: "Binder Items",
			links: [
				{ name: "Maintenance Items", value: "maintenance_items", active: false },
				{ name: "Appliances", value: "appliances", active: true },
				{ name: "Transfers", value: "transfers", active: false },
				{ name: "Shares", value: "shares", active: false },
				{ name: "Paints", value: "paints", active: false },
				{ name: "Seller Reports", value: "seller_reports", active: false }
			]
		}];
		this.currentLink = this.navigation_links[0].links[1];
	}

	AdminBindersItemsCtrl.prototype = {
		changeLink: function(link) {
			this.currentLink.active = false;
			link.active = true;
			this.currentLink = link;
		}
	};
})();
