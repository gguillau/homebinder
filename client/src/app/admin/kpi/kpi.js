(function() {
	"use strict";

	angular.module("hb.admin.kpi", [
			"ui.router",
			"ui.bootstrap",
			"hb.components"
		])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
				.state("admin.kpi", {
					url: "^/admin/kpi",
					templateUrl: "admin/kpi/kpi.tpl.html",
					controller: "AdminKpiCtrl",
					controllerAs: "ctrl",
					access: {
						requiresLogin: true
					}
				});
		}])
		.filter("percentage", ["$filter", function($filter) {
			return function(input, decimals) {
				return $filter("number")(input * 100, decimals) + "%";
			};
		}])
		.controller("AdminKpiCtrl", [
			"$state",
			"$log",
			"hb.api",
			"Notify",
			"Loading",
			"$window",
			"hb.utils",
			AdminKpiCtrl
		]);

	function AdminKpiCtrl($state, $log, api, notify, Loading, $window, utils) {
		this.$state = $state;
		this.$log = $log;
		this.api = api;
		this.notify = notify;
		this.loading = Loading;
		this.$window = $window;
		this.utils = utils;
		this.tabs = [{
			name: "KPI Summary"
		}, {
			name: "Binder Totals"
		}, {
			name: "User Totals"
		}, {
			name: "Inspector Engagement"
		}];
		this.current_tab = this.tabs[0].name;
		this.title = "Key Performance Indicators";
		this.toolbarCfg = {
			searchBox: false,
			title: this.title,
			total: null,
			button: {
				title: "Download KPI PDF",
				click: angular.bind(this, this.downloadKpi)
			}
		};
		this.today = this.utils.utils.getToday(true);
		this.data = {};
		this.homeowner = {};
		this.inspectors = {};
		this.getSummary();
	}

	AdminKpiCtrl.prototype = {

		getSummary: function() {
			this.loading.show("Loading KPI Summary...");
			this.api.kpi.getSummary().then(
				angular.bind(this, this.summarySuccess),
				angular.bind(this, this.summaryError));
		},

		summarySuccess: function(response) {
			this.data.total_acv = response.data.total_acv;
			this.homeowners = response.data.homeowners;
			this.inspectors = response.data.inspectors;
			this.loading.close();
		},

		summaryError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		getBinders: function() {
			if (this.data.binders == null) {
				this.loading.show("Loading Binder KPIs...");
				this.api.kpi.getBinders().then(
					angular.bind(this, this.bindersSuccess),
					angular.bind(this, this.bindersError));
			}
		},

		bindersSuccess: function(response) {
			this.data.binders = response.data;
			this.loading.close();
		},

		bindersError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		getUsers: function() {
			if (this.data.users == null) {
				this.loading.show("Loading User KPIs...");
				this.api.kpi.getUsers().then(
					angular.bind(this, this.usersSuccess),
					angular.bind(this, this.usersError));
			}
		},

		usersSuccess: function(response) {
			this.data.users = response.data.count;
			this.data.logins = response.data.logins;
			this.data.nologins = response.data.nologins;
			this.loading.close();
		},

		usersError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		downloadKpi: function() {
			this.loading.show("Downloading report...");
			this.api.kpi.downloadReport().then(
				angular.bind(this, this.downloadReportSuccess),
				angular.bind(this, this.downloadReportError));
		},

		downloadReportSuccess: function(response) {
			this.notify.info("Please check your email in a couple minutes.");
			this.loading.close();
		},

		downloadReportError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		setTabName: function(tab) {
			this.current_tab = tab.name;
			if (tab.name === "Binder Totals") {
				this.getBinders();
			}
			else if (tab.name === "User Totals") {
				this.getUsers();
			}
			else if (tab.name === "Inspector Engagement") {
				this.getInspectorEngagement();
			}
		},

		getInspectorEngagement: function() {
			if (this.inspectors.binders == null) {
				this.loading.show("Loading KPIs...");
				this.api.kpi.getEngagement("inspector").then(
					angular.bind(this, this.inspectorSuccess),
					angular.bind(this, this.inspectorError));
			}
		},

		inspectorSuccess: function(response) {
			this.inspectors.binders = response.data.binders;
			this.inspectors.logins = response.data.logins;
			this.loading.close();
		},

		inspectorError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		}

	};
})();
