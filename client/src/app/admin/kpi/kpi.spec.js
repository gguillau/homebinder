describe("AdminKpiCtrl", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $state,
        notify,
        $log,
        api,
        loading,
        $filter;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$filter_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        $filter = _$filter_;

        $state = {
            go: function(state, args) {}
        };

        api = {
            kpi: {
                getBinders: function() {},
                getUsers: function() {},
                getSummary: function() {},
                downloadReport: function() {},
                getEngagement: function() {}
            }
        };

        $log = {
            error: function(msg) {}
        };

        notify = {
            info: function(msg) {},
            error: function(msg) {}
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("AdminKpiCtrl", {
            "$state": $state,
            "hb.api": api,
            "$log": $log,
            "Notify": notify,
            "Loading": loading
        });
        $scope.ctrl = ctrl;

    }

    describe('ctrl.getSummary', function() {

        it('should successfully retrieve all kpi summary', function() {

            spyOn(api.kpi, "getSummary").and.returnValue($q.when({
                data: {
                    total_acv: 8000,
                    homeowners: {},
                    inspectors: {}
                }
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();

            expect(api.kpi.getSummary).toHaveBeenCalled();
            expect(loading.show).toHaveBeenCalledWith("Loading KPI Summary...");
            expect(loading.close).toHaveBeenCalled();
        });

        it('should unsuccessfully retrieve all kpi summary', function() {

            spyOn(api.kpi, "getSummary").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            $rootScope.$apply();

            expect(api.kpi.getSummary).toHaveBeenCalled();
            expect(loading.show).toHaveBeenCalledWith("Loading KPI Summary...");
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.close).toHaveBeenCalled();
        });

    });

    describe('ctrl.getBinders', function() {

        it('should successfully retrieve kpi binders', function() {

            spyOn(api.kpi, "getSummary").and.returnValue($q.when({
                data: {
                    total_acv: 8000,
                    homeowners: {},
                    inspectors: {}
                }
            }));
            spyOn(api.kpi, "getBinders").and.returnValue($q.when({
                data: {}
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();
            ctrl.getBinders();
            $rootScope.$apply();

            expect(api.kpi.getBinders).toHaveBeenCalled();
            expect(loading.show.calls.count()).toEqual(2);
            expect(loading.close.calls.count()).toEqual(2);
        });

        it('should unsuccessfully retrieve kpi binders', function() {

            spyOn(api.kpi, "getSummary").and.returnValue($q.when({
                data: {
                    total_acv: 8000,
                    homeowners: {},
                    inspectors: {}
                }
            }));
            spyOn(api.kpi, "getBinders").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            $rootScope.$apply();
            ctrl.getBinders();
            $rootScope.$apply();

            expect(api.kpi.getBinders).toHaveBeenCalled();
            expect(loading.show.calls.count()).toEqual(2);
            expect(loading.close.calls.count()).toEqual(2);
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(notify.error).toHaveBeenCalledWith("error");
        });

    });

    describe('ctrl.getUsers', function() {

        it('should successfully retrieve kpi users', function() {

            spyOn(api.kpi, "getSummary").and.returnValue($q.when({
                data: {
                    total_acv: 8000,
                    homeowners: {},
                    inspectors: {}
                }
            }));
            spyOn(api.kpi, "getUsers").and.returnValue($q.when({
                data: {
                    count: 0,
                    logins: 0,
                    nologins: 0
                }
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();
            ctrl.getUsers();
            $rootScope.$apply();

            expect(api.kpi.getUsers).toHaveBeenCalled();
            expect(loading.show.calls.count()).toEqual(2);
            expect(loading.close.calls.count()).toEqual(2);
        });

        it('should unsuccessfully retrieve kpi binders', function() {

            spyOn(api.kpi, "getSummary").and.returnValue($q.when({
                data: {
                    total_acv: 8000,
                    homeowners: {},
                    inspectors: {}
                }
            }));
            spyOn(api.kpi, "getUsers").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            $rootScope.$apply();
            ctrl.getUsers();
            $rootScope.$apply();

            expect(api.kpi.getUsers).toHaveBeenCalled();
            expect(loading.show.calls.count()).toEqual(2);
            expect(loading.close.calls.count()).toEqual(2);
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(notify.error).toHaveBeenCalledWith("error");
        });

    });

    describe('ctrl.downloadKpi', function() {

        it('should successfully call downloadKpi function', function() {

            spyOn(api.kpi, "getSummary").and.returnValue($q.when({
                data: {
                    total_acv: 8000,
                    homeowners: {},
                    inspectors: {}
                }
            }));
            spyOn(api.kpi, "downloadReport").and.returnValue($q.when({
                data: "Success"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn(notify, "info");

            createController();
            $rootScope.$apply();
            ctrl.downloadKpi();
            $rootScope.$apply();

            expect(notify.info).toHaveBeenCalledWith("Please check your email in a couple minutes.");
            expect(api.kpi.downloadReport).toHaveBeenCalled();
            expect(loading.show.calls.count()).toEqual(2);
            expect(loading.close.calls.count()).toEqual(2);
        });

        it('should unsuccessfully downloadReport', function() {

            spyOn(api.kpi, "getSummary").and.returnValue($q.when({
                data: {
                    total_acv: 8000,
                    homeowners: {},
                    inspectors: {}
                }
            }));
            spyOn(api.kpi, "downloadReport").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            $rootScope.$apply();
            ctrl.downloadKpi();
            $rootScope.$apply();

            expect(api.kpi.downloadReport).toHaveBeenCalled();
            expect(loading.show.calls.count()).toEqual(2);
            expect(loading.close.calls.count()).toEqual(2);
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(notify.error).toHaveBeenCalledWith("error");
        });

    });

    describe('ctrl.setTabName', function() {

        it('should set tab name to binders total', function() {

            spyOn(api.kpi, "getSummary").and.returnValue($q.when({
                data: {
                    total_acv: 8000,
                    homeowners: {},
                    inspectors: {}
                }
            }));
            spyOn(api.kpi, "getBinders").and.returnValue($q.when({
                data: {}
            }));
            createController();
            $rootScope.$apply();
            ctrl.setTabName({
                name: "Binder Totals"
            });
            $rootScope.$apply();

            expect(api.kpi.getBinders).toHaveBeenCalled();
            expect(ctrl.current_tab).toBe("Binder Totals");

        });

        it('should set tab name to user total', function() {

            spyOn(api.kpi, "getSummary").and.returnValue($q.when({
                data: {
                    total_acv: 8000,
                    homeowners: {},
                    inspectors: {}
                }
            }));
            spyOn(api.kpi, "getUsers").and.returnValue($q.when({
                data: {}
            }));
            createController();
            $rootScope.$apply();
            ctrl.setTabName({
                name: "User Totals"
            });
            $rootScope.$apply();

            expect(api.kpi.getUsers).toHaveBeenCalled();
            expect(ctrl.current_tab).toBe("User Totals");

        });

        it('should set tab name to user total', function() {

            spyOn(api.kpi, "getSummary").and.returnValue($q.when({
                data: {
                    total_acv: 8000,
                    homeowners: {},
                    inspectors: {}
                }
            }));
            spyOn(api.kpi, "getEngagement").and.returnValue($q.when({
                data: {}
            }));
            createController();
            $rootScope.$apply();
            ctrl.setTabName({
                name: "Inspector Engagement"
            });
            $rootScope.$apply();

            expect(api.kpi.getEngagement).toHaveBeenCalledWith("inspector");
            expect(ctrl.current_tab).toBe("Inspector Engagement");

        });

    });

    describe('ctrl.getInspectorEngagement', function() {

        it('should successfully call getInspectorEngagement function', function() {

            spyOn(api.kpi, "getSummary").and.returnValue($q.when({
                data: {
                    total_acv: 8000,
                    homeowners: {},
                    inspectors: {}
                }
            }));
            spyOn(api.kpi, "getEngagement").and.returnValue($q.when({
                data: "Success"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();
            ctrl.getInspectorEngagement();
            $rootScope.$apply();

            expect(api.kpi.getEngagement).toHaveBeenCalled();
            expect(loading.show.calls.count()).toEqual(2);
            expect(loading.close.calls.count()).toEqual(2);
        });

        it('should unsuccessfully getInspectorEngagement', function() {

            spyOn(api.kpi, "getSummary").and.returnValue($q.when({
                data: {
                    total_acv: 8000,
                    homeowners: {},
                    inspectors: {}
                }
            }));
            spyOn(api.kpi, "getEngagement").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            $rootScope.$apply();
            ctrl.getInspectorEngagement();
            $rootScope.$apply();

            expect(api.kpi.getEngagement).toHaveBeenCalledWith("inspector");
            expect(loading.show.calls.count()).toEqual(2);
            expect(loading.close.calls.count()).toEqual(2);
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(notify.error).toHaveBeenCalledWith("error");
        });

    });

    it("should return 25%", function() {
        var number = 0.25,
            result;

        // call the filter
        result = $filter("percentage")(number, 2);

        // verify filter changes
        expect(result).toEqual("25.00%");
    });

});