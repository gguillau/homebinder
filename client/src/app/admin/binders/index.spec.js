describe("AdminBindersIndexController", function() {
    var controller,
        BindersIndexController,
        api;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });

        // mock the Notify service
        $provide.factory("Notify", function() {
            return {
                info: function() {},
                error: function() {}
            };
        });

        // mock the Session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return {
                        role: "admin"
                    };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        BindersIndexController = $injector.get("BindersIndexController");
        api = $injector.get("hb.api");

        spyOn(api.binder, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));
        spyOn(api.binder, "destroy").and.returnValue($q.when({}));

        controller = $controller("AdminBindersIndexController", {
            "BindersIndexController": BindersIndexController,
            "hb.api": api
        });

        $rootScope.$apply();
    }));

    describe("init", function() {
        it("sets attributes", function() {
            expect(controller.model.editState).toBe("admin.binders_edit.client");
        });
    });

    describe("downloadHomeownerReport", function() {
        it("returns success", inject(function($q, Notify, $log, $rootScope) {

            spyOn(api.kpi, "downloadHomeownerReport").and.returnValue($q.when({
                data: "success"
            }));

            spyOn(Notify, "info");
            spyOn($log, "error");

            controller.model.downloadHomeownerReport();
            $rootScope.$apply();

            expect(api.kpi.downloadHomeownerReport).toHaveBeenCalled();
            expect(Notify.info).toHaveBeenCalled();
            expect($log.error).not.toHaveBeenCalled();
        }));

        it("returns error", inject(function($q, Notify, $log, $rootScope) {

            spyOn(api.kpi, "downloadHomeownerReport").and.returnValue($q.reject({
                data: "error"
            }));

            spyOn(Notify, "error");
            spyOn($log, "error");

            controller.model.downloadHomeownerReport();
            $rootScope.$apply();

            expect(api.kpi.downloadHomeownerReport).toHaveBeenCalled();
            expect(Notify.error).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
        }));
    });
});