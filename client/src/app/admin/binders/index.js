(function() {
	"use strict";

	angular.module('hb.admin.binders', [
			'hb.admin.binders.new',
			'hb.admin.binders.edit'
		])
		.config(['$stateProvider', adminBindersConfig])
		.controller("AdminBindersIndexController", [
			"BindersIndexController",
			AdminBindersIndexController
		]);

	function AdminBindersIndexController(BindersIndexController) {

		var Model = function() {
			// call the parent class
			BindersIndexController.call(this);
			this.editState = "admin.binders_edit.client";
			this.newState = "admin.binders_new.client";
			this.queryArgs = {
				searchMethod: "for_admin"
			};
			this.refresh();
		};

		Model.prototype = Object.create(BindersIndexController.prototype);

		angular.extend(Model.prototype, {
			downloadHomeownerReport: function() {
				this.api.kpi.downloadHomeownerReport().then(
					angular.bind(this, this.onDownloadSuccess),
					angular.bind(this, this.onDownloadError)
				);
			},

			onDownloadSuccess: function(response) {
				this.notify.info(this.resources.homeownerReportSuccess);
			},

			onDownloadError: function(response) {
				this.notify.error(response.data);
				this.$log.error(response);
			}
		});

		this.model = new Model();
	}

	function adminBindersConfig($stateProvider) {
		$stateProvider
			.state('admin.binders', {
				url: '^/admin/binders',
				templateUrl: 'components/binders/index/index.tpl.html',
				controller: "AdminBindersIndexController",
				controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
			});
	}

})();