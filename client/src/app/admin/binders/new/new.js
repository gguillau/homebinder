(function() {
    "use strict";

    angular.module('hb.admin.binders.new', [
        "hb.admin.binders.new.client"
        ])
        .config(['$stateProvider', adminBindersNewConfig])
        .controller("AdminBindersNewController", [
            "BindersNewController",
            AdminBindersNewController
        ]);

    function AdminBindersNewController(BindersNewController) {

        var Model = function() {
            // call the parent class
            BindersNewController.call(this);
            this.indexState = "admin.binders";
            this.indexParams = {};
            this.editState = "admin.binders_edit";
            this.formCfg.settings.showPartner = true;
        };

        Model.prototype = Object.create(BindersNewController.prototype);

        angular.extend(Model.prototype, {
            initiate: function() {
                this.navigation_links = [{
                    name: "Binder Info",
                    links: [
                        { name: "Client/Property Info", value: "client", state: "admin.binders_new.client", allowed: true }
                    ]
                }];
            }
        });

        this.model = new Model();
    }

    function adminBindersNewConfig($stateProvider) {
        $stateProvider
            .state('admin.binders_new', {
                url: '^/admin/binders/new',
                abstract: true,
                templateUrl: 'components/binders/form/form.tpl.html',
                controller: "AdminBindersNewController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
    }

})();