(function() {
    "use strict";

    angular.module('hb.admin.binders.new.client', [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('admin.binders_new.client', {
                    url: '^/admin/binders/new/client',
                    templateUrl: 'components/binders/form/clientInformation/clientInformation.tpl.html',
                    controller: "AdminBindersNewController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();