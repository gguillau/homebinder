(function() {
    "use strict";

    angular.module('hb.admin.binders.edit', [
            "hb.admin.binders.edit.client",
            "hb.admin.binders.edit.agents",
            "hb.admin.binders.edit.images",
            "hb.admin.binders.edit.appliances",
            "hb.admin.binders.edit.maintenanceItems",
            "hb.admin.binders.edit.contractors",
            "hb.admin.binders.edit.documents",
            "hb.admin.binders.edit.transactions",
            "hb.admin.binders.edit.subscriptions",
            "hb.admin.binders.edit.transfers",
            "hb.admin.binders.edit.users",
            "hb.admin.binders.edit.brandings",
            "hb.admin.binders.edit.partners",
            "hb.admin.binders.edit.emails",
            "hb.admin.binders.edit.warranties"
        ])
        .config(['$stateProvider', adminBindersNewConfig])
        .controller("AdminBindersEditController", [
            "BindersEditController",
            AdminBindersEditController
        ]);

    function AdminBindersEditController(BindersEditController) {

        var Model = function() {
            // call the parent class
            BindersEditController.call(this);
            this.indexState = "admin.binders";
            this.indexParams = {};
            this.formCfg.settings.showBinderAgents = false;
            this.formCfg.settings.showBinderEmails = true;
            this.formCfg.settings.showBinderUsers = true;
            this.formCfg.settings.showBinderPartners = true;
            this.formCfg.settings.showBinderBrandings = true;
            this.formCfg.settings.showBinderWarranties = true;
            this.formCfg.settings.showBinderTransactions = true;
            this.formCfg.settings.showBinderSubscriptions = true;
            this.formCfg.settings.showBinderTransfers = true;
            this.initiate();
            this.refresh();
        };

        Model.prototype = Object.create(BindersEditController.prototype);

        angular.extend(Model.prototype);

        this.model = new Model();
    }

    function adminBindersNewConfig($stateProvider) {
        $stateProvider
            .state('admin.binders_edit', {
                url: '^/admin/binders/:binderId',
                abstract: true,
                templateUrl: 'components/binders/form/form.tpl.html',
                controller: "AdminBindersEditController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                },
                resolve: {
                    binder: ["Context", "$stateParams", function(Context, $stateParams) {
                        return Context.getBinder($stateParams.binderId);
                    }]
                }
            });
    }

})();