(function() {
    "use strict";

    angular.module("hb.admin.binders.edit.brandings", [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('admin.binders_edit.brandings', {
                url: '^/admin/binders/:binderId/brandings',
                templateUrl: 'components/binders/edit/branding/branding.tpl.html',
                controller: "BindersFormBrandingListController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();