(function() {
    "use strict";

    angular.module('hb.admin.binders.edit.client', [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('admin.binders_edit.client', {
                url: '^/admin/binders/:binderId/client',
                templateUrl: 'components/binders/form/clientInformation/clientInformation.tpl.html',
                controller: "AdminBindersEditController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();