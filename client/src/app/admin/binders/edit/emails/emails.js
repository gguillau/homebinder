(function() {
    "use strict";

    angular.module("hb.admin.binders.edit.emails", [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('admin.binders_edit.emails', {
                url: '^/admin/binders/:binderId/emails',
                templateUrl: 'components/binders/edit/emails/emails.tpl.html',
                controller: "BindersFormEmailsController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();