(function() {
    "use strict";

    angular.module("hb.admin.binders.edit.warranties", [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('admin.binders_edit.warranties', {
                url: '^/admin/binders/:binderId/warranties',
                templateUrl: 'components/binders/edit/warranties/warranties.tpl.html',
                controller: "BindersEditWarrantiesController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();