describe("AdminBindersEditController", function() {
    var controller,
        base,
        $q_;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    var defer = $q_.defer();
                    defer.resolve({});
                    return defer.promise;
                }
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope, $stateParams) {
        $q_ = $q;
        base = $injector.get("BindersEditController");
        $stateParams.binderId = 1;

        controller = $controller("AdminBindersEditController", {
            "BindersEditController": base
        });
    }));

    describe("init", function() {
        it("sets the indexState", function() {
            expect(controller.model.indexState).toEqual("admin.binders");
        });
    });

});