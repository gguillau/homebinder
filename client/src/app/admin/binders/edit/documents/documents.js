(function() {
    "use strict";

    angular.module("hb.admin.binders.edit.documents", [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('admin.binders_edit.documents', {
                url: '^/admin/binders/:binderId/documents',
                templateUrl: 'components/binders/form/documents/documents.tpl.html',
                controller: "BinderFormDocumentsController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();