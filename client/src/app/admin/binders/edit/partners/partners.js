(function() {
    "use strict";

    angular.module("hb.admin.binders.edit.partners", [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('admin.binders_edit.partners', {
                url: '^/admin/binders/:binderId/partners',
                templateUrl: 'components/binders/edit/partners/partners.tpl.html',
                controller: "BindersFormPartnersListController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();