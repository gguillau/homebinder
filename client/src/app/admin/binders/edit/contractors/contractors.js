(function() {
    "use strict";

    angular
        .module("hb.admin.binders.edit.contractors", [
            "hb.admin.binders.edit.contractors.index",
            "hb.admin.binders.edit.contractors.edit",
            "hb.admin.binders.edit.contractors.new",
            "hb.admin.binders.edit.contractors.newTemplate"
        ]);
})();