(function() {
    "use strict";

    angular.module("hb.admin.binders.edit.contractors.newTemplate", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state("admin.binders_edit.contractorsNewTemplate", {
                    url: "^/admin/binders/:binderId/contractors/new/{templateId:int}",
                    templateUrl: "homebinders/homebinder/binderContractors/form/form.tpl.html",
                    controller: "AdminBindersContractorFormController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();