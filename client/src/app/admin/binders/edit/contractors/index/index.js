(function() {
    "use strict";

    angular.module("hb.admin.binders.edit.contractors.index", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('admin.binders_edit.contractors', {
                    url: '^/admin/binders/:binderId/contractors',
                    templateUrl: 'components/binders/form/contractors/contractors.tpl.html',
                    controller: "AdminBindersContractorsController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .controller("AdminBindersContractorsController", [
            "BinderFormBinderContractorsController",
            "$state",
            "$stateParams",
            AdminBindersContractorsController
        ]);

    function AdminBindersContractorsController(BinderFormBinderContractorsController, $state, $stateParams) {
        var Model = function() {
            // call the parent class
            BinderFormBinderContractorsController.call(this);
            this.itemNewState = "admin.binders_edit.contractorsNew";
            this.itemNewTemplateState = "admin.binders_edit.contactorsNewTemplate";
        };

        Model.prototype = Object.create(BinderFormBinderContractorsController.prototype);

        angular.extend(Model.prototype, {
            // send user back to editState
            edit: function(item) {
                $state.go("admin.binders_edit.contractorsEdit", { id: item.id, binderId: $stateParams.binderId });
            },

            showForm: function(item) {
                if (item) {
                    $state.go(this.itemNewTemplateState, { binderId: this.binder.id, templateId: item.id });
                }
                else {
                    $state.go(this.itemNewState, { binderId: $stateParams.binderId });
                }
            }
        });

        this.model = new Model();
    }
})();