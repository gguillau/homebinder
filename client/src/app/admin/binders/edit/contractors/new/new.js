(function() {
    "use strict";

    angular.module("hb.admin.binders.edit.contractors.new", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('admin.binders_edit.contractorsNew', {
                    url: '^/admin/binders/:binderId/contractors/new',
                    templateUrl: "homebinders/homebinder/binderContractors/form/form.tpl.html",
                    controller: "AdminBindersContractorFormController",
                    controllerAs: "ctrl",
                    bindToController: true,
                    scope: {},
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .controller("AdminBindersContractorFormController", [
            "BinderContractorFormController",
            "$stateParams",
            AdminBindersContractorFormController
        ]);

    function AdminBindersContractorFormController(BinderContractorFormController, $stateParams) {
        var Model = function() {
            this.indexState = "admin.binders_edit.contractors";
            this.indexParams = { binderId: $stateParams.binderId };
            this.editState = "admin.binders_edit.contractorsEdit";
            BinderContractorFormController.call(this);
        };

        Model.prototype = Object.create(BinderContractorFormController.prototype);

        angular.extend(Model.prototype, {});

        this.model = new Model();
    }
})();