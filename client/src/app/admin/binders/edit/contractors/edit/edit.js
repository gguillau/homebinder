(function() {
    "use strict";

    angular.module("hb.admin.binders.edit.contractors.edit", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('admin.binders_edit.contractorsEdit', {
                    url: '^/admin/binders/:binderId/contractors/{id:int}/edit',
                    templateUrl: "homebinders/homebinder/binderContractors/form/form.tpl.html",
                    controller: "AdminBindersContractorFormController",
                    controllerAs: "ctrl",
                    bindToController: true,
                    scope: {},
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();