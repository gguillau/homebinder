(function() {
    "use strict";

    angular
        .module("hb.admin.binders.edit.maintenanceItems", [
            "hb.admin.binders.edit.maintenanceItems.index",
            "hb.admin.binders.edit.maintenanceItems.edit",
            "hb.admin.binders.edit.maintenanceItems.new",
            "hb.admin.binders.edit.maintenanceItems.newTemplate"
        ]);
})();