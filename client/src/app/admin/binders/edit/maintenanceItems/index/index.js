(function() {
    "use strict";

    angular.module("hb.admin.binders.edit.maintenanceItems.index", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('admin.binders_edit.maintenanceItems', {
                    url: '^/admin/binders/:binderId/maintenanceItems',
                    templateUrl: 'components/binders/form/maintenanceItems/maintenanceItems.tpl.html',
                    controller: "AdminBindersMaintenanceItemsController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .controller("AdminBindersMaintenanceItemsController", [
            "BinderFormMaintenanceItemsController",
            "$state",
            "$stateParams",
            AdminBindersMaintenanceItemsController
        ]);

    function AdminBindersMaintenanceItemsController(BinderFormMaintenanceItemsController, $state, $stateParams) {
        var Model = function() {
            // call the parent class
            BinderFormMaintenanceItemsController.call(this);
            this.itemNewState = "admin.binders_edit.maintenanceItemsNew";
            this.itemNewTemplateState = "admin.binders_edit.maintenanceItemsNewTemplate";
        };

        Model.prototype = Object.create(BinderFormMaintenanceItemsController.prototype);

        angular.extend(Model.prototype, {
            // send user back to editState
            edit: function(item) {
                $state.go("admin.binders_edit.maintenanceItemsEdit", { id: item.id, binderId: $stateParams.binderId });
            },

            showForm: function(item) {
                if (item) {
                    $state.go(this.itemNewTemplateState, { binderId: this.binder.id, templateId: item.id });
                }
                else {
                    $state.go(this.itemNewState, { binderId: $stateParams.binderId });
                }
            }
        });

        this.model = new Model();
    }
})();