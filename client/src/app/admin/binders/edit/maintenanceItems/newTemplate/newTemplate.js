(function() {
    "use strict";

    angular.module("hb.admin.binders.edit.maintenanceItems.newTemplate", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state("admin.binders_edit.maintenanceItemsNewTemplate", {
                    url: "^/admin/binders/:binderId/maintenanceItems/new/{templateId:int}",
                    templateUrl: "homebinders/homebinder/maintenanceItems/form/form.tpl.html",
                    controller: "AdminBindersMaintenanceItemsFormController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();