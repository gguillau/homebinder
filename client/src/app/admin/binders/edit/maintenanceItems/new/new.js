(function() {
    "use strict";

    angular.module("hb.admin.binders.edit.maintenanceItems.new", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('admin.binders_edit.maintenanceItemsNew', {
                    url: '^/admin/binders/:binderId/maintenanceItems/new',
                    templateUrl: "homebinders/homebinder/maintenanceItems/form/form.tpl.html",
                    controller: "AdminBindersMaintenanceItemsFormController",
                    controllerAs: "ctrl",
                    bindToController: true,
                    scope: {},
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        // Model backing the appliance modal form
        .controller("AdminBindersMaintenanceItemsFormController", [
            "MaintenanceItemFormController",
            "$stateParams",
            AdminBindersMaintenanceItemsFormController
        ]);

    function AdminBindersMaintenanceItemsFormController(MaintenanceItemFormController, $stateParams) {
        var Model = function() {
            this.indexState = "admin.binders_edit.maintenanceItems";
            this.indexParams = { binderId: $stateParams.binderId };
            this.editState = "admin.binders_edit.maintenanceItemsEdit";
            MaintenanceItemFormController.call(this);
        };

        Model.prototype = Object.create(MaintenanceItemFormController.prototype);

        angular.extend(Model.prototype, {});

        this.model = new Model();
    }
})();