(function() {
    "use strict";

    angular.module("hb.admin.binders.edit.maintenanceItems.edit", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('admin.binders_edit.maintenanceItemsEdit', {
                    url: '^/admin/binders/:binderId/maintenanceItems/{id:int}/edit',
                    templateUrl: "homebinders/homebinder/maintenanceItems/form/form.tpl.html",
                    controller: "AdminBindersMaintenanceItemsFormController",
                    controllerAs: "ctrl",
                    bindToController: true,
                    scope: {},
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();