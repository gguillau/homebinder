(function() {
    "use strict";

    angular.module('hb.admin.binders.edit.agents', [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('admin.binders_edit.agents', {
                url: '^/admin/binders/:binderId/agents',
                templateUrl: 'components/binders/form/agents/agents.tpl.html',
                controller: "BinderFormAgentController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();