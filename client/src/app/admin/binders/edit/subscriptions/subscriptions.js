(function() {
    "use strict";

    angular.module("hb.admin.binders.edit.subscriptions", [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('admin.binders_edit.subscriptions', {
                url: '^/admin/binders/:binderId/subscriptions',
                templateUrl: 'components/binders/edit/subscriptions/subscriptions.tpl.html',
                controller: "BinderFormTransfersController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();