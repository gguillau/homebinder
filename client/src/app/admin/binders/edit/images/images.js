(function() {
    "use strict";

    angular.module('hb.admin.binders.edit.images', [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('admin.binders_edit.images', {
                url: '^/admin/binders/:binderId/images',
                templateUrl: 'components/binders/form/images/images.tpl.html',
                controller: "BinderFormImagesController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();