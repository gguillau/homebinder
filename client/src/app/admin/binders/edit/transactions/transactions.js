(function() {
    "use strict";

    angular.module("hb.admin.binders.edit.transactions", [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('admin.binders_edit.transactions', {
                url: '^/admin/binders/:binderId/transactions',
                templateUrl: 'components/binders/edit/transactions/transactions.tpl.html',
                controller: "BinderFormTransfersController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();