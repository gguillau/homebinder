(function() {
    "use strict";

    angular.module("hb.admin.binders.edit.transfers", [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('admin.binders_edit.transfers', {
                url: '^/admin/binders/:binderId/transfers',
                templateUrl: 'components/binders/form/transfers/transfers.tpl.html',
                controller: "BinderFormTransfersController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();