(function() {
    "use strict";

    angular.module("hb.admin.binders.edit.users", [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('admin.binders_edit.users', {
                url: '^/admin/binders/:binderId/users',
                templateUrl: 'components/binders/edit/users/users.tpl.html',
                controller: "BindersFormUsersController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();