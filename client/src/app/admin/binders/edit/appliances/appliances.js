(function() {
    "use strict";

    angular
        .module("hb.admin.binders.edit.appliances", [
            "hb.admin.binders.edit.appliances.index",
            "hb.admin.binders.edit.appliances.edit",
            "hb.admin.binders.edit.appliances.new",
            "hb.admin.binders.edit.appliances.newTemplate"
        ]);
})();