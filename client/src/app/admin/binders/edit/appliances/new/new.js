(function() {
    "use strict";

    angular.module("hb.admin.binders.edit.appliances.new", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('admin.binders_edit.appliancesNew', {
                    url: '^/admin/binders/:binderId/appliances/new',
                    templateUrl: "homebinders/homebinder/appliances/form/form.tpl.html",
                    controller: "AdminBindersApplianceFormController",
                    controllerAs: "ctrl",
                    bindToController: true,
                    scope: {},
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        // Model backing the appliance modal form
        .controller("AdminBindersApplianceFormController", [
            "ApplianceFormController",
            "$stateParams",
            AdminBindersApplianceFormController
        ]);

    function AdminBindersApplianceFormController(ApplianceFormController, $stateParams) {
        var Model = function() {
            this.indexState = "admin.binders_edit.appliances";
            this.indexParams = { binderId: $stateParams.binderId };
            this.editState = "admin.binders_edit.appliancesEdit";
            ApplianceFormController.call(this);
        };

        Model.prototype = Object.create(ApplianceFormController.prototype);

        angular.extend(Model.prototype, {});

        this.model = new Model();
    }
})();