(function() {
    "use strict";

    angular.module("hb.admin.binders.edit.appliances.edit", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('admin.binders_edit.appliancesEdit', {
                    url: '^/admin/binders/:binderId/appliances/{id:int}/edit',
                    templateUrl: "homebinders/homebinder/appliances/form/form.tpl.html",
                    controller: "AdminBindersApplianceFormController",
                    controllerAs: "ctrl",
                    bindToController: true,
                    scope: {},
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();