(function() {
    "use strict";

    angular.module("hb.admin.binders.edit.appliances.newTemplate", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state("admin.binders_edit.appliancesNewTemplate", {
                    url: "^/admin/binders/:binderId/appliances/new/{templateId:int}",
                    templateUrl: "homebinders/homebinder/appliances/form/form.tpl.html",
                    controller: "AdminBindersApplianceFormController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();