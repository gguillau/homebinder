(function() {
    "use strict";

    angular.module("hb.admin.binders.edit.appliances.index", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('admin.binders_edit.appliances', {
                    url: '^/admin/binders/:binderId/appliances',
                    templateUrl: 'components/binders/form/appliances/appliances.tpl.html',
                    controller: "AdminBindersAppliancesController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .controller("AdminBindersAppliancesController", [
            "BinderFormAppliancesController",
            "$state",
            "$stateParams",
            AdminBindersAppliancesController
        ]);

    function AdminBindersAppliancesController(BinderFormAppliancesController, $state, $stateParams) {
        var Model = function() {
            // call the parent class
            BinderFormAppliancesController.call(this);
            this.itemNewState = "admin.binders_edit.appliancesNew";
            this.itemNewTemplateState = "admin.binders_edit.appliancesNewTemplate";
        };

        Model.prototype = Object.create(BinderFormAppliancesController.prototype);

        angular.extend(Model.prototype, {
            // send user back to editState
            edit: function(item) {
                $state.go("admin.binders_edit.appliancesEdit", { id: item.id, binderId: $stateParams.binderId });
            },

            showForm: function(item) {
                if (item) {
                    $state.go(this.itemNewTemplateState, { binderId: this.binder.id, templateId: item.id });
                }
                else {
                    $state.go(this.itemNewState, { binderId: $stateParams.binderId });
                }
            }
        });

        this.model = new Model();
    }
})();