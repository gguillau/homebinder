(function() {
	"use strict";

	angular
		.module("hb.admin.warranties.plans.index", [
			"ui.router",
			"ui.bootstrap",
			"hb.components"
		])
		.directive("warrantyPlans", function() {
			return {
				restrict: "E",
				templateUrl: "admin/warranties/plans/index/index.tpl.html",
				controller: "WarrantiesPlansIndexCtrl",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};

		})
		.controller("WarrantiesPlansIndexCtrl", [
			"hb.api",
			"ModalService",
			"Notify",
			"$log",
			"Loading",
			"hb.framework.indexBase",
			"hb.resources",
			"WarrantyPlanModal",
			WarrantiesPlansIndexCtrl
		]);

	function WarrantiesPlansIndexCtrl(api, ModalService, Notify, $log, loading, IndexBase, resources, WarrantyPlanModal) {
		var Model = function() {
			// call the parent class
			IndexBase.call(this);
			// apply warranty company specific resource strings
			this.resources = angular.extend({}, this.resources, resources.warrantyPlanIndex);
			this.WarrantyPlanModal = WarrantyPlanModal;
			// set the refresh API call
			this.refreshCall = api.warrantyPlan.all;
			// set the delete API call
			this.deleteCall = api.warrantyPlan.destroy;
			// set the name property used in the delete message
			this.nameProperty = "name";
			this.maxSize = 10;
			this.sortOptions = [{
				orderBy: "warranty_plans.created_at",
				order: "desc",
				desc: this.resources.creationDate + " - " + this.resources.descending
			}, {
				orderBy: "warranty_plans.created_at",
				order: "asc",
				desc: this.resources.creationDate + " - " + this.resources.ascending
			}];
			this.sortOption = this.sortOptions[0];
			this.orderBy = this.sortOption.orderBy;
			this.order = this.sortOption.order;
			this.headers = null;
			this.setHeaders();
			this.refresh();
		};

		Model.prototype = Object.create(IndexBase.prototype);

		angular.extend(Model.prototype, {
			setHeaders: function() {
				this.columnspan = 8;
				this.headers = this.resources.warrantyPlanAttributes.map(angular.bind(this, function(attribute) {
					var sortable = false,
						orderBy = null,
						sorted = false,
						show = true;
					if (attribute === "ID") {
						sorted = true;
						sortable = true;
						orderBy = "warranty_plans.id";
					}
					if (attribute === "Name") {
						sortable = true;
						orderBy = "warranty_plans.name";
					}
					else if (attribute === "Duration") {
						sortable = true;
						orderBy = "warranty_plans.duration";
					}
					else if (attribute === "Cycle") {
						sortable = true;
						orderBy = "warranty_plans.cycle";
					}
					else if (attribute === "Description") {
						sortable = true;
						orderBy = "warranty_plans.description";
					}
					else if (attribute === "Price") {
						sortable = true;
						orderBy = "warranty_plans.price";
					}
					return {
						name: attribute,
						sortable: sortable,
						sorted: sorted,
						orderBy: orderBy,
						show: show,
						order: "desc"
					};
				}));
				this.sortOption = this.headers[0];
			},

			newItem: function() {
				var formOpts = {};
				formOpts.onSaved = angular.bind(this, this.onAdded);
				this.WarrantyPlanModal.showForm(formOpts);
			},

			editItem: function(item) {
				var formOpts = {};
				formOpts.warrantyPlan = item;
				var index = this.items.indexOf(item);
				formOpts.onSaved = angular.bind(this, this.onEdited, index);
				this.WarrantyPlanModal.showForm(formOpts);
			},

			onEdited: function(index, item) {
				this.items[index] = item;
			}
		});

		this.model = new Model();
	}

})();