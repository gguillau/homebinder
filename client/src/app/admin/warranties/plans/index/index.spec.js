describe("WarrantiesPlansIndexCtrl", function() {
    var controller,
        base,
        api,
        resources,
        WarrantyPlanModal;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        WarrantyPlanModal = $injector.get("WarrantyPlanModal");

        spyOn(api.warrantyPlan, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));
        spyOn(api.warrantyPlan, "destroy").and.returnValue($q.when({}));

        controller = $controller("WarrantiesPlansIndexCtrl", {
            "hb.framework.indexBase": base,
            "hb.api": api,
            "hb.resources": resources,
            "WarrantyPlanModal": WarrantyPlanModal
        });

        $rootScope.$apply();
    }));

    describe("init", function() {
        it("sets attributes", function() {
            expect(controller.model.maxSize).toBe(10);
            expect(controller.model.sortOptions.length).toBe(2);
        });
    });

    describe("refresh", function() {
        it("gets the warranties", function() {
            expect(api.warrantyPlan.all).toHaveBeenCalled();
            expect(controller.model.columnspan).toBe(8);
            expect(controller.model.headers.length).toBe(7);
        });
    });

    describe("newItem", function() {
        it("calls modal", function() {
            spyOn(WarrantyPlanModal, "showForm");
            controller.model.newItem();
            expect(WarrantyPlanModal.showForm).toHaveBeenCalledWith({
                onSaved: jasmine.any(Function)
            });
        });
    });


    describe("editItem", function() {
        it("calls modal", function() {
            spyOn(WarrantyPlanModal, "showForm");
            controller.model.editItem({
                id: 1,
                name: "name"
            });
            expect(WarrantyPlanModal.showForm).toHaveBeenCalledWith({
                warrantyPlan: {
                    id: 1,
                    name: 'name'
                },
                onSaved: jasmine.any(Function)
            });
        });
    });

    describe("onEdited", function() {
        it("inserts item into array", function() {
            expect(controller.model.items[0].name).toBe("name");
            spyOn(WarrantyPlanModal, "showForm");
            controller.model.onEdited(0, {
                name: "test"
            });
            expect(controller.model.items[0].name).toBe("test");
        });
    });
});

describe('warrantyPlans', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.warrantyPlan, "all").and.returnValue($q.when({ data: { id: 1 } }));

        $compile('<warranty-plans></warranty-plans>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls api warrantyPlan all', function() {
            expect(api.warrantyPlan.all).toHaveBeenCalled();
        });
    });
});