(function() {
    "use strict";

    angular
        .module("hb.admin.warranties.plans", [
            "hb.admin.warranties.plans.index",
            "hb.admin.warranties.plans.modal",
            "hb.admin.warranties.plans.form"
        ]);
})();