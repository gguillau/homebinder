(function() {
    "use strict";

    angular
        .module("hb.admin.warranties.plans.form", [])
        .directive("hbWarrantyPlanForm", function() {
            return {
                restrict: "E",
                templateUrl: "admin/warranties/plans/form/form.tpl.html",
                controller: "WarrantyPlanFormController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    cfg: "=",
                    onSaved: "&",
                    onCancel: "&"
                }
            };
        })
        // Model backing the area modal form
        .controller("WarrantyPlanFormController", [
            "$log",
            "hb.api",
            "Notify",
            "Loading",
            "hb.resources",
            Controller
        ]);

    function Controller($log, api, notify, loading, resources) {
        var that = this;
        var Model = function() {
            this.$log = $log;
            this.api = api;
            this.notify = notify;
            this.resources = resources.warrantyPlanForm;
            this.loading = loading;
            this.name = null;
            this.duration = null;
            this.cycle = null;
            this.description = null;
            this.price = null;
            this.partnerLookupCfg = {
                allowClear: true,
                placeholder: "Search for partner",
                refresh: angular.bind(this, this.partnerLookup),
                itemTemplate: "components/lookup/partner/partnerDropDownItem.tpl.html",
                selectionTemplate: "components/lookup/partner/partnerSelection.tpl.html"
            };
            this.companyLookupCfg = {
                allowClear: true,
                placeholder: "Search for company",
                refresh: angular.bind(this, this.companyLookup),
                itemTemplate: "components/lookup/partner/partnerDropDownItem.tpl.html",
                selectionTemplate: "components/lookup/partner/partnerSelection.tpl.html"
            };
            this.cycles = [{
                value: "days",
                name: "Days"
            }, {
                value: "weeks",
                name: "Weeks"
            }, {
                value: "months",
                name: "Months"
            }, {
                value: "years",
                name: "Years"
            }];
            this.populate();
            this.loadWarrantyCompanies();
        };

        angular.extend(Model.prototype, {
            populate: function() {
                if (!that.cfg) {
                    return;
                }

                // setup the save callback
                that.cfg.api = {
                    save: angular.bind(this, this.submit)
                };

                // load the resource
                if (that.cfg.item) {
                    this.resource = that.cfg.item;
                    this.title = this.resource.name;
                    this.name = this.resource.name;
                    this.description = this.resource.description;
                    this.duration = this.resource.duration;
                    this.price = this.resource.price;
                    this.cycle = this.resource.cycle;
                    this.warranty_company_id = this.resource.warranty_company_id;
                    this.partner_id = this.resource.partner_id;
                    this.partner = this.resource.partner;
                    this.warranty_company = this.resource.warranty_company;
                }
            },

            loadWarrantyCompanies: function() {
                var opts = {
                    page: 1,
                    count: 50
                };
                this.api.warrantyCompany.all(opts).then(
                    angular.bind(this, this.onLoadWarrantyCompaniesSuccess),
                    angular.bind(this, this.onLoadWarrantyCompaniesError)
                );
            },

            onLoadWarrantyCompaniesSuccess: function(response) {
                this.warranty_companies = response.data.items;
            },

            onLoadWarrantyCompaniesError: function(response) {
                this.$log.error(response);
            },

            partnerLookup: function(value) {
                var opts = {
                    page: 1,
                    count: 50,
                    search: value
                };
                return this.api.partner.all(opts).then(
                    angular.bind(this, function(response) {
                        return {
                            data: response.data.items
                        };
                    })
                );
            },

            submit: function() {
                that.form.$submitted = true;

                if (that.form.$invalid) {
                    return;
                }

                var data = {
                        warranty_plan: {
                            name: this.name,
                            duration: this.duration,
                            description: this.description,
                            price: this.price,
                            cycle: this.cycle,
                            warranty_company_id: this.warranty_company.id
                        }
                    },
                    promise;

                if (this.partner) {
                    data.partner_id = this.partner.id;
                }

                this.error422 = false;
                this.loading.show(this.resources.saving);

                if (this.resource) {
                    promise = api.warrantyPlan.update(this.resource.id, data);
                }
                else {
                    promise = api.warrantyPlan.create(data);
                }

                promise.then(angular.bind(this, this.onResourceSaved),
                        angular.bind(this, this.onResourceSaveError))
                    /*jshint -W024*/
                    .finally(angular.bind(this, this.onDoneSaving));
            },

            onResourceSaved: function(response) {
                this.resource = response.data;
                that.cfg.resource = this.resource;
                that.onSaved({
                    item: this.resource
                });
            },

            onResourceSaveError: function(response) {
                this.notify.error(response.data);
            },

            onDoneSaving: function() {
                this.loading.close();
            }
        });

        this.model = new Model();
    }
})();