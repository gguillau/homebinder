describe("WarrantyPlanFormController", function() {
    var ctrl,
        controller,
        $q,
        $log,
        api,
        notify,
        resources,
        loading,
        warranty_company,
        $rootScope;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
        $rootScope = $injector.get("$rootScope");
    }));

    function createController() {
        ctrl = controller("WarrantyPlanFormController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "Loading": loading,
            "hb.resources": resources
        });
    }

    describe("populate", function() {
        it("returns when no cfg is provided", function() {
            createController();
            ctrl.cfg = null;
            ctrl.model.populate();
        });

        it("populates resource", function() {
            warranty_company = {
                id: 1,
                name: "CP1",
                contact: "Test Contact",
                email: "test+warrantyco2@homebinder.com"
            };
            createController();
            ctrl.cfg = {
                item: {
                    created_at: "2017-07-21T10:34:09.555-04:00",
                    cycle: "days",
                    description: "90-Day Warranty + WSM + RWD",
                    duration: 90,
                    id: 4,
                    name: "90-Day Warranty + WSM + RWD",
                    price: 33,
                    updated_at: "2017-07-21T10:34:09.555-04:00",
                    warranty_company: warranty_company,
                    warranty_company_id: 1
                }
            };

            ctrl.model.populate();

            expect(ctrl.model.resource).not.toBeNull();
            expect(ctrl.model.name).toEqual("90-Day Warranty + WSM + RWD");
            expect(ctrl.model.title).toEqual("90-Day Warranty + WSM + RWD");
            expect(ctrl.model.cycle).toEqual("days");
            expect(ctrl.model.duration).toEqual(90);
            expect(ctrl.model.price).toEqual(33);
            expect(ctrl.model.warranty_company_id).toEqual(1);
            expect(ctrl.model.warranty_company).toEqual(warranty_company);
            expect(ctrl.model.description).toEqual("90-Day Warranty + WSM + RWD");
            expect(ctrl.cfg.api.save).not.toBeNull();
        });

        it("does not populate when no resource is provided", function() {
            createController();
            ctrl.cfg = {};
            ctrl.model.populate();

            expect(ctrl.model.resource).toBeUndefined();
            expect(ctrl.cfg.api.save).not.toBeNull();
        });
    });

    describe("loadWarrantyCompanies", function() {

        it("loads the companies", function() {
            spyOn(api.warrantyCompany, "all").and.returnValue($q.when({
                data: {
                    items: []
                }
            }));
            createController();
            ctrl.model.loadWarrantyCompanies();
            expect(api.warrantyCompany.all).toHaveBeenCalledWith({
                page: 1,
                count: 50
            });
        });

        it("doesn't load the companies", function() {
            spyOn(api.warrantyCompany, "all").and.returnValue($q.reject({
                data: "error"
            }));

            createController();
            spyOn($log, "error");
            ctrl.model.loadWarrantyCompanies();
            $rootScope.$apply();

            expect(api.warrantyCompany.all).toHaveBeenCalledWith({
                page: 1,
                count: 50
            });
            expect($log.error).toHaveBeenCalled();
        });

    });

    describe("onLoadWarrantyCompaniesSuccess", function() {
        it("sets the companies", function() {
            createController();
            ctrl.model.onLoadWarrantyCompaniesSuccess({
                data: { items: [] }
            });
            expect(ctrl.model.warranty_companies.length).toEqual(0);
        });
    });

    describe("partnerLookup", function() {
        it("loads the partners", function() {
            spyOn(api.warrantyCompany, "all").and.returnValue($q.when({
                data: {
                    items: []
                }
            }));
            spyOn(api.partner, "all").and.returnValue($q.when({
                data: {
                    items: []
                }
            }));
            createController();
            ctrl.model.partnerLookup();
            $rootScope.$apply();

            expect(api.partner.all).toHaveBeenCalled();
        });
    });

    describe("submit", function() {
        it("does not submit invalid data", function() {
            spyOn(api.warrantyPlan, "create").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = {};
            ctrl.form = {
                $invalid: true
            };
            ctrl.model.submit();
            expect(ctrl.form.$submitted).toEqual(true);
            expect(api.warrantyPlan.create).not.toHaveBeenCalled();
        });

        it("creates a new resource", function() {
            spyOn(api.warrantyPlan, "create").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = {};
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.partner = {
                id: 1
            };
            ctrl.model.name = "resource";
            ctrl.model.description = "resource description";
            ctrl.model.warranty_company = {
                id: 1
            };
            ctrl.model.submit();
            expect(api.warrantyPlan.create).toHaveBeenCalled();
        });

        it("updates an existing resource", function() {
            spyOn(api.warrantyPlan, "update").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = {};
            ctrl.model.resource = {
                id: 1
            };
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.name = "resource";
            ctrl.model.description = "resource description";
            ctrl.model.warranty_company = {
                id: 1
            };
            ctrl.model.submit();
            expect(api.warrantyPlan.update).toHaveBeenCalledWith(1, {
                warranty_plan: {
                    name: 'resource',
                    duration: null,
                    description: 'resource description',
                    price: null,
                    cycle: null,
                    warranty_company_id: 1
                }
            });
        });
    });

    describe("onResourceSaved", function() {
        it("saves the response and calls onSaved", function() {
            var resource = {
                id: 1,
                name: "resource",
                description: "description"
            };
            createController();
            ctrl.cfg = {};
            ctrl.onSaved = function() {};
            spyOn(ctrl, "onSaved");
            ctrl.model.onResourceSaved({
                data: resource
            });
            expect(ctrl.model.resource).toEqual(resource);
            expect(ctrl.cfg.resource).toEqual(resource);
            expect(ctrl.onSaved).toHaveBeenCalledWith(jasmine.objectContaining({
                item: resource
            }));
        });
    });

    describe("onResourceSaveError", function() {
        it("handles a general error", function() {
            spyOn(notify, "error");
            createController();
            ctrl.cfg = {};
            ctrl.model.onResourceSaveError({
                status: 500,
                data: "error"
            });
            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("onDoneSaving", function() {
        it("calls loading close", function() {
            spyOn(loading, "close");
            createController();
            ctrl.cfg = {};
            ctrl.model.onDoneSaving();
            expect(loading.close).toHaveBeenCalled();
        });
    });

});

describe('hbWarrantyPlanForm', function() {
    var $scope, $compile, element, $q, api;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        spyOn(api.warrantyCompany, "all").and.returnValue($q.when({ data: { items: [] } }));

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = {};
        $scope.onSaved = function() {};
        $scope.onCancel = function() {};

        element = $compile('<hb-warranty-plan-form cfg="cfg" onSaved="onSaved" onCancel="onCancel"></hb-warranty-plan-form>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {
            expect(element.html()).toContain("Company");
        });
    });
});