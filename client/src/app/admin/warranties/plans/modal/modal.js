angular
    .module("hb.admin.warranties.plans.modal", [])
    .factory("WarrantyPlanModal", [
        "hb.framework.toggleModalBase",
        "hb.resources",
        function(ModalBase, resources) {
            var instance = null;
            var Modal = function() {
                ModalBase.call(this);
                this.templateUrl = "admin/warranties/plans/modal/modal.tpl.html";
                this.controller = "WarrantyPlansModalController as ctrl";
                this.windowClass = "widget-modal";
            };

            Modal.prototype = Object.create(ModalBase.prototype);

            if (!instance) {
                instance = new Modal();
            }

            return instance;
        }
    ])
    .controller("WarrantyPlansModalController", [
        "hb.framework.toggleModalControllerBase",
        "$modalInstance",
        "args",
        "hb.api",
        "hb.resources",
        "Notify",
        "$log",
        function(ToggleModalControllerBase, $modalInstance, args, api, resources, notify, $log) {
            var Model = function() {
                ToggleModalControllerBase.call(this);
                this.args = args;
                this.populateCall = api.warrantyPlan.get;
                this.$modalInstance = $modalInstance;
                this.objectProperty = "warrantyPlan";
                this.objectIdProperty = "warrantyPlanId";
                this.nameProperty = "name";
                this.resources = resources.warrantyPlanForm;
                this.init();
            };

            Model.prototype = Object.create(ToggleModalControllerBase.prototype);

            this.model = new Model();
        }
    ]);