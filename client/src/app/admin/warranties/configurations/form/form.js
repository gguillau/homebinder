(function() {
    "use strict";

    angular
        .module("hb.admin.warranties.configurations.form", [])
        .directive("hbWarrantyConfigurationForm", function() {
            return {
                restrict: "E",
                templateUrl: "admin/warranties/configurations/form/form.tpl.html",
                controller: "WarrantyConfigurationFormController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    cfg: "=",
                    onSaved: "&",
                    onCancel: "&"
                }
            };
        })
        // Model backing the area modal form
        .controller("WarrantyConfigurationFormController", [
            "$log",
            "hb.api",
            "Notify",
            "Loading",
            "hb.resources",
            Controller
        ]);

    function Controller($log, api, notify, loading, resources) {
        var that = this;
        var Model = function() {
            this.$log = $log;
            this.api = api;
            this.notify = notify;
            this.resources = resources.warrantyConfigurationForm;
            this.loading = loading;
            this.name = null;
            this.contact = null;
            this.email = null;
            this.partnerLookupCfg = {
                allowClear: true,
                placeholder: "Search for partner",
                refresh: angular.bind(this, this.partnerLookup),
                itemTemplate: "components/lookup/partner/partnerDropDownItem.tpl.html",
                selectionTemplate: "components/lookup/partner/partnerSelection.tpl.html"
            };
            this.companyLookupCfg = {
                allowClear: true,
                placeholder: "Search for company",
                refresh: angular.bind(this, this.companyLookup),
                itemTemplate: "components/lookup/partner/partnerDropDownItem.tpl.html",
                selectionTemplate: "components/lookup/partner/partnerSelection.tpl.html"
            };
            this.planLookupCfg = {
                allowClear: true,
                placeholder: "Search for plan",
                refresh: angular.bind(this, this.planLookup),
                itemTemplate: "components/lookup/partner/partnerDropDownItem.tpl.html",
                selectionTemplate: "components/lookup/partner/partnerSelection.tpl.html"
            };
            this.populate();
            this.loadWarrantyCompanies();
        };

        angular.extend(Model.prototype, {
            populate: function() {
                if (!that.cfg) {
                    return;
                }

                // setup the save callback
                that.cfg.api = {
                    save: angular.bind(this, this.submit)
                };

                // load the resource
                if (that.cfg.item) {
                    this.resource = that.cfg.item;
                    this.title = this.resource.name;
                    this.name = this.resource.name;
                    this.automatic_purchases = this.resource.automatic_purchases;
                    this.account_number = this.resource.account_number;
                    this.warranty_plan_id = this.resource.warranty_plan_id;
                    this.warranty_company_id = this.resource.warranty_company_id;
                    this.partner_id = this.resource.partner_id;
                    this.partner = this.resource.partner;
                    this.warranty_company = this.resource.warranty_company;
                    this.warranty_plan = this.resource.warranty_plan;
                    this.partner_warranty_plans = this.resource.warranty_plans;
                    this.loadWarrantyPlans();
                }
            },

            partnerLookup: function(value) {
                var opts = {
                    page: 1,
                    count: 50,
                    search: value
                };
                return this.api.partner.all(opts).then(
                    angular.bind(this, function(response) {
                        return {
                            data: response.data.items
                        };
                    })
                );
            },

            loadWarrantyCompanies: function() {
                var opts = {
                    page: 1,
                    count: 50
                };
                this.api.warrantyCompany.all(opts).then(
                    angular.bind(this, this.onLoadWarrantyCompaniesSuccess),
                    angular.bind(this, this.onLoadWarrantyCompaniesError)
                );
            },

            onLoadWarrantyCompaniesSuccess: function(response) {
                this.warranty_companies = response.data.items;
            },

            onLoadWarrantyCompaniesError: function(response) {
                this.$log.error(response);
            },

            onCompanyChange: function() {
                this.warranty_plan = null;
                this.partner_warranty_plans = null;
                this.warranty_plans = null;
                this.loadWarrantyPlans();
            },

            loadWarrantyPlans: function() {
                var opts = {
                    page: 1,
                    count: 50,
                    warranty_company_id: this.warranty_company.id
                };
                this.api.warrantyPlan.all(opts).then(
                    angular.bind(this, this.onLoadWarrantyPlansSuccess),
                    angular.bind(this, this.onLoadWarrantyPlansError)
                );
            },

            onLoadWarrantyPlansSuccess: function(response) {
                this.warranty_plans = response.data.items;
                if (!that.cfg.item) {
                    this.partner_warranty_plans = this.warranty_plans;
                    this.partner_warranty_plans.forEach(function(plan) {
                        plan.warranty_plan_id = plan.id;
                    });
                }
                else {
                    this.partner_warranty_plans = this.warranty_plans;
                    this.partner_warranty_plans.forEach(angular.bind(this, function(plan) {
                        if (plan.warranty_plan_id === this.warranty_plan_id) {
                            this.warranty_plan = plan;
                        }
                    }));
                }
            },

            onLoadWarrantyPlansError: function(response) {
                this.$log.error(response);
            },

            submit: function() {
                that.form.$submitted = true;

                if (that.form.$invalid) {
                    return;
                }

                var data = {
                        warranty_configuration: {
                            automatic_purchases: this.automatic_purchases,
                            account_number: this.account_number,
                            warranty_plan_id: this.warranty_plan.id,
                            warranty_company_id: this.warranty_company.id,
                            partner_id: this.partner.id
                        },
                        partner_warranty_plans: this.partner_warranty_plans
                    },
                    promise;

                this.error422 = false;
                this.loading.show(this.resources.saving);

                if (this.resource) {
                    promise = api.warrantyConfiguration.update(this.resource.id, data);
                }
                else {
                    promise = api.warrantyConfiguration.create(data);
                }

                promise.then(angular.bind(this, this.onResourceSaved),
                        angular.bind(this, this.onResourceSaveError))
                    /*jshint -W024*/
                    .finally(angular.bind(this, this.onDoneSaving));
            },

            onResourceSaved: function(response) {
                this.resource = response.data;
                that.cfg.resource = this.resource;
                that.onSaved({
                    item: this.resource
                });
            },

            onResourceSaveError: function(response) {
                this.notify.error(response.data);
            },

            onDoneSaving: function() {
                this.loading.close();
            }
        });

        this.model = new Model();
    }
})();