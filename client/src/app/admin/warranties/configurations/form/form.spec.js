describe("WarrantyConfigurationFormController", function() {
    var ctrl,
        controller,
        $q,
        $log,
        api,
        notify,
        resources,
        loading,
        warranty_partner,
        $rootScope;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
        $rootScope = $injector.get("$rootScope");

        warranty_partner = {
            account_number: "MA8372",
            automatic_purchases: false,
            created_at: "2017-07-21T10:40:35.302-04:00",
            id: 7,
            name: "Complete Home Inspections",
            partner: {
                account_id: 427,
                affiliate_code: null,
                binder_logo_id: 834,
                code: "partner163",
                completed_onboarding: true,
                contact: "John Doe",
                created_at: "2015-06-23T18:59:04.429-04:00",
                email: "greg+inspector@homebinder.com",
                email_display_name: "Complete Home Inspections",
                id: 163,
                logo_content_type: "image/png",
                logo_file_name: "Complete Home Inspections Logo.png",
                logo_file_size: 14298,
                logo_updated_at: "2017-05-09T14:09:51.254-04:00",
                name: "Complete Home Inspections",
                partner_key: "completehomeinspections",
                partner_type: "inspector",
                phone: "+18599858654",
                sellers_logo_id: null,
                updated_at: "2017-05-09T14:09:52.354-04:00",
                website: "http://www.completehomeinspections.com"
            },
            partner_id: 163,
            price: 16,
            updated_at: "2017-07-21T10:40:35.302-04:00",
            warranty_company: {
                contact: "Greg Moretti",
                created_at: "2017-07-21T10:30:37.046-04:00",
                email: "greg+warrantyco2@homebinder.com",
                id: 2,
                name: "CP2",
                updated_at: "2017-07-21T10:30:37.046-04:00"
            },
            warranty_company_id: 2,
            warranty_plan: {
                created_at: "2017-07-21T10:31:02.771-04:00",
                cycle: "days",
                description: "90-Day Warranty",
                duration: 90,
                id: 2,
                name: "90-Day Warranty",
                price: "15.0",
                updated_at: "2017-07-21T10:31:02.771-04:00",
                warranty_company_id: 2
            },
            warranty_plan_id: 2,
            warranty_plans: [{
                id: 7,
                name: "90-Day Warranty",
                price: 16,
                warranty_plan_id: 2
            }, {
                id: 8,
                name: "90-Day Warranty + Water/Sewer, Mold and Roof Protection",
                price: 19,
                warranty_plan_id: 3
            }, {
                id: 9,
                name: "90-Day Warranty + WSM + RWD",
                price: 34,
                warranty_plan_id: 4
            }]
        };
    }));

    function createController() {
        ctrl = controller("WarrantyConfigurationFormController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "Loading": loading,
            "hb.resources": resources
        });
    }

    describe("populate", function() {
        it("returns when no cfg is provided", function() {
            createController();
            ctrl.cfg = null;
            ctrl.model.populate();
        });

        it("populates resource", function() {

            createController();
            ctrl.cfg = {
                item: warranty_partner
            };

            ctrl.model.populate();

            expect(ctrl.model.resource).not.toBeNull();
            expect(ctrl.model.name).toEqual(warranty_partner.name);
            expect(ctrl.model.automatic_purchases).toEqual(warranty_partner.automatic_purchases);
            expect(ctrl.model.account_number).toEqual(warranty_partner.account_number);
            expect(ctrl.model.warranty_plan_id).toEqual(warranty_partner.warranty_plan_id);
            expect(ctrl.model.warranty_company_id).toEqual(warranty_partner.warranty_company_id);
            expect(ctrl.model.partner_id).toEqual(warranty_partner.partner_id);
            expect(ctrl.model.partner).toEqual(warranty_partner.partner);
            expect(ctrl.model.warranty_company).toEqual(warranty_partner.warranty_company);
            expect(ctrl.model.warranty_plan).toEqual(warranty_partner.warranty_plan);
            expect(ctrl.model.partner_warranty_plans).toEqual(warranty_partner.warranty_plans);
            expect(ctrl.cfg.api.save).not.toBeNull();
        });

        it("does not populate when no resource is provided", function() {
            createController();
            ctrl.cfg = {};
            ctrl.model.populate();

            expect(ctrl.model.resource).toBeUndefined();
            expect(ctrl.cfg.api.save).not.toBeNull();
        });
    });

    describe("partnerLookup", function() {
        it("loads the partners", function() {
            spyOn(api.warrantyCompany, "all").and.returnValue($q.when({
                data: {
                    items: []
                }
            }));
            spyOn(api.partner, "all").and.returnValue($q.when({
                data: {
                    items: []
                }
            }));
            createController();
            ctrl.model.partnerLookup();
            $rootScope.$apply();

            expect(api.partner.all).toHaveBeenCalled();
        });
    });

    describe("loadWarrantyCompanies", function() {

        it("loads the companies", function() {
            spyOn(api.warrantyCompany, "all").and.returnValue($q.when({
                data: {
                    items: []
                }
            }));
            createController();
            ctrl.model.loadWarrantyCompanies();
            expect(api.warrantyCompany.all).toHaveBeenCalledWith({
                page: 1,
                count: 50
            });
        });

        it("doesn't load the companies", function() {
            spyOn(api.warrantyCompany, "all").and.returnValue($q.reject({
                data: "error"
            }));

            createController();
            spyOn($log, "error");
            ctrl.model.loadWarrantyCompanies();
            $rootScope.$apply();

            expect(api.warrantyCompany.all).toHaveBeenCalledWith({
                page: 1,
                count: 50
            });
            expect($log.error).toHaveBeenCalled();
        });

    });

    describe("onCompanyChange", function() {
        it("loads the loadWarrantyPlans", function() {
            createController();
            spyOn(ctrl.model, "loadWarrantyPlans");
            ctrl.model.onCompanyChange();

            expect(ctrl.model.loadWarrantyPlans).toHaveBeenCalled();
        });
    });

    describe("loadWarrantyPlans", function() {

        it("loads the plans", function() {
            spyOn(api.warrantyCompany, "all").and.returnValue($q.when({
                data: {
                    items: []
                }
            }));
            spyOn(api.warrantyPlan, "all").and.returnValue($q.when({
                data: {
                    items: []
                }
            }));
            createController();
            ctrl.model.warranty_company = {
                id: 1
            };
            ctrl.model.loadWarrantyPlans();
            expect(api.warrantyPlan.all).toHaveBeenCalledWith({
                page: 1,
                count: 50,
                warranty_company_id: 1
            });
        });

        it("doesn't load the plans", function() {
            spyOn(api.warrantyCompany, "all").and.returnValue($q.when({
                data: {
                    items: []
                }
            }));
            spyOn(api.warrantyPlan, "all").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");

            createController();
            ctrl.model.warranty_company = {
                id: 1
            };
            ctrl.model.loadWarrantyPlans();
            $rootScope.$apply();

            expect(api.warrantyPlan.all).toHaveBeenCalledWith({
                page: 1,
                count: 50,
                warranty_company_id: 1
            });
            expect($log.error).toHaveBeenCalled();
        });

    });

    describe("onLoadWarrantyPlansSuccess", function() {
        it("sets the warranty plan ids when there is no item", function() {
            createController();
            ctrl.cfg = {};
            var item = { id: 1 };
            ctrl.model.onLoadWarrantyPlansSuccess({ data: { items: [item] } });

            expect(item.warranty_plan_id).toEqual(1);
        });

        it("sets the warranty plan when there is an item", function() {
            createController();
            ctrl.cfg = { item: { id: 1 } };
            var item = { id: 1, warranty_plan_id: 1 };
            ctrl.model.warranty_plan_id = 1;
            ctrl.model.onLoadWarrantyPlansSuccess({ data: { items: [item] } });

            expect(ctrl.model.warranty_plan).toEqual(item);
        });
    });

    describe("submit", function() {
        it("does not submit invalid data", function() {
            spyOn(api.warrantyConfiguration, "create").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = {};
            ctrl.form = {
                $invalid: true
            };
            ctrl.model.submit();
            expect(ctrl.form.$submitted).toEqual(true);
            expect(api.warrantyConfiguration.create).not.toHaveBeenCalled();
        });

        it("creates a new resource", function() {
            spyOn(api.warrantyConfiguration, "create").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = {};
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.name = "resource";
            ctrl.model.description = "resource description";
            ctrl.model.warranty_company = {
                id: 1
            };
            ctrl.model.warranty_plan = {
                id: 1
            };
            ctrl.model.partner = {
                id: 1
            };
            ctrl.model.submit();
            expect(api.warrantyConfiguration.create).toHaveBeenCalledWith({
                warranty_configuration: {
                    automatic_purchases: undefined,
                    account_number: undefined,
                    warranty_plan_id: 1,
                    warranty_company_id: 1,
                    partner_id: 1
                },
                partner_warranty_plans: undefined
            });
        });

        it("updates an existing resource", function() {
            spyOn(api.warrantyConfiguration, "update").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = {
                item: warranty_partner
            };
            ctrl.model.populate();
            ctrl.model.warranty_company = {
                id: 1
            };
            ctrl.model.warranty_plan = {
                id: 1
            };
            ctrl.model.partner = {
                id: 1
            };
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.submit();
            expect(api.warrantyConfiguration.update).toHaveBeenCalledWith(7, {
                warranty_configuration: {
                    automatic_purchases: false,
                    account_number: "MA8372",
                    warranty_plan_id: 1,
                    warranty_company_id: 1,
                    partner_id: 1
                },
                partner_warranty_plans: [{
                    id: 7,
                    name: '90-Day Warranty',
                    price: 16,
                    warranty_plan_id: 2
                }, {
                    id: 8,
                    name: '90-Day Warranty + Water/Sewer, Mold and Roof Protection',
                    price: 19,
                    warranty_plan_id: 3
                }, {
                    id: 9,
                    name: '90-Day Warranty + WSM + RWD',
                    price: 34,
                    warranty_plan_id: 4
                }]
            });
        });
    });

    describe("onResourceSaved", function() {
        it("saves the response and calls onSaved", function() {
            var resource = {
                id: 1,
                name: "resource",
                description: "description"
            };
            createController();
            ctrl.cfg = {};
            ctrl.onSaved = function() {};
            spyOn(ctrl, "onSaved");
            ctrl.model.onResourceSaved({
                data: resource
            });
            expect(ctrl.model.resource).toEqual(resource);
            expect(ctrl.cfg.resource).toEqual(resource);
            expect(ctrl.onSaved).toHaveBeenCalledWith(jasmine.objectContaining({
                item: resource
            }));
        });
    });

    describe("onResourceSaveError", function() {
        it("handles a general error", function() {
            spyOn(notify, "error");
            createController();
            ctrl.cfg = {};
            ctrl.model.onResourceSaveError({
                status: 500,
                data: "error"
            });
            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("onDoneSaving", function() {
        it("calls loading close", function() {
            spyOn(loading, "close");
            createController();
            ctrl.cfg = {};
            ctrl.model.onDoneSaving();
            expect(loading.close).toHaveBeenCalled();
        });
    });

});

describe('hbWarrantyConfigurationForm', function() {
    var $scope, $compile, element, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        $scope = $rootScope.$new(), $compile = _$compile_;
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        spyOn(api.warrantyCompany, "all").and.returnValue($q.when({ data: { items: [] } }));

        $scope.cfg = {};
        $scope.saved = function() {};
        $scope.cancel = function() {};

        element = $compile('<hb-warranty-configuration-form cfg="cfg" on-saved="saved" on-cancel="cancel"></hb-warranty-configuration-form>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the hb-warranty-configuration-form directive', function() {
        it('sets the html', function() {
            expect(element.html()).toContain("Partner");
        });
    });
});