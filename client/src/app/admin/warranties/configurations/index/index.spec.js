describe("WarrantiesConfigurationIndexCtrl", function() {
    var controller,
        base,
        api,
        resources,
        WarrantyConfigurationModal;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        WarrantyConfigurationModal = $injector.get("WarrantyConfigurationModal");

        spyOn(api.warrantyConfiguration, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));
        spyOn(api.warrantyPlan, "destroy").and.returnValue($q.when({}));

        controller = $controller("WarrantiesConfigurationIndexCtrl", {
            "hb.framework.indexBase": base,
            "hb.api": api,
            "hb.resources": resources,
            "WarrantyConfigurationModal": WarrantyConfigurationModal
        });

        $rootScope.$apply();
    }));

    describe("init", function() {
        it("sets attributes", function() {
            expect(controller.model.maxSize).toBe(10);
            expect(controller.model.sortOptions.length).toBe(2);
        });
    });

    describe("refresh", function() {
        it("gets the warranties", function() {
            expect(api.warrantyConfiguration.all).toHaveBeenCalled();
            expect(controller.model.columnspan).toBe(7);
            expect(controller.model.headers.length).toBe(6);
        });
    });

    describe("newItem", function() {
        it("calls modal", function() {
            spyOn(WarrantyConfigurationModal, "showForm");
            controller.model.newItem();
            expect(WarrantyConfigurationModal.showForm).toHaveBeenCalledWith({
                onSaved: jasmine.any(Function)
            });
        });
    });


    describe("editItem", function() {
        it("calls modal", function() {
            spyOn(WarrantyConfigurationModal, "showForm");
            controller.model.editItem({
                id: 1,
                name: "name"
            });
            expect(WarrantyConfigurationModal.showForm).toHaveBeenCalledWith({
                warrantyConfiguration: {
                    id: 1,
                    name: 'name'
                },
                onSaved: jasmine.any(Function)
            });
        });
    });

    describe("onEdited", function() {
        it("inserts item into array", function() {
            expect(controller.model.items[0].name).toBe("name");
            spyOn(WarrantyConfigurationModal, "showForm");
            controller.model.onEdited(0, {
                name: "test"
            });
            expect(controller.model.items[0].name).toBe("test");
        });
    });
});

describe('warrantyConfiguration', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;

        spyOn(api.warrantyConfiguration, "all").and.returnValue($q.when({ data: { items: [] } }));

        $compile('<warranty-configuration></warranty-configuration>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the warranty configuration directive', function() {
        it('calls api warrantyConfiguration all', function() {
            expect(api.warrantyConfiguration.all).toHaveBeenCalled();
        });
    });
});