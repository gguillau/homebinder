(function() {
	"use strict";

	angular
		.module("hb.admin.warranties.configurations.index", [
			"ui.router",
			"ui.bootstrap",
			"hb.components"
		])
		.directive("warrantyConfiguration", function() {
			return {
				restrict: "E",
				templateUrl: "admin/warranties/configurations/index/index.tpl.html",
				controller: "WarrantiesConfigurationIndexCtrl",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};

		})
		.controller("WarrantiesConfigurationIndexCtrl", [
			"hb.api",
			"ModalService",
			"Notify",
			"$log",
			"Loading",
			"hb.framework.indexBase",
			"hb.resources",
			"WarrantyConfigurationModal",
			WarrantiesConfigurationIndexCtrl
		]);

	function WarrantiesConfigurationIndexCtrl(api, ModalService, Notify, $log, loading, IndexBase, resources, WarrantyConfigurationModal) {
		var Model = function() {
			// call the parent class
			IndexBase.call(this);
			// apply warranty company specific resource strings
			this.resources = angular.extend({}, this.resources, resources.warrantyConfigurationIndex);
			this.WarrantyConfigurationModal = WarrantyConfigurationModal;
			// set the refresh API call
			this.refreshCall = api.warrantyConfiguration.all;
			// set the delete API call
			this.deleteCall = api.warrantyConfiguration.destroy;
			// set the name property used in the delete message
			this.nameProperty = "name";
			this.maxSize = 10;
			this.sortOptions = [{
				orderBy: "warranty_configurations.created_at",
				order: "desc",
				desc: this.resources.creationDate + " - " + this.resources.descending
			}, {
				orderBy: "warranty_configurations.created_at",
				order: "asc",
				desc: this.resources.creationDate + " - " + this.resources.ascending
			}];
			this.sortOption = this.sortOptions[0];
			this.orderBy = this.sortOption.orderBy;
			this.order = this.sortOption.order;
			this.headers = null;
			this.setHeaders();
			this.refresh();
		};

		Model.prototype = Object.create(IndexBase.prototype);

		angular.extend(Model.prototype, {
			setHeaders: function() {
				this.columnspan = 7;
				this.headers = this.resources.warrantyConfigurationAttributes.map(angular.bind(this, function(attribute) {
					var sortable = false,
						orderBy = null,
						sorted = false,
						show = true;
					if (attribute === "ID") {
						sorted = true;
						sortable = true;
						orderBy = "warranty_configurations.id";
					}
					else if (attribute === "Partner ID") {
						sortable = true;
						orderBy = "warranty_configurations.partner_id";
					}
					return {
						name: attribute,
						sortable: sortable,
						sorted: sorted,
						orderBy: orderBy,
						show: show,
						order: "desc"
					};
				}));
				this.sortOption = this.headers[0];
			},

			newItem: function() {
				var formOpts = {};
				formOpts.onSaved = angular.bind(this, this.onAdded);
				this.WarrantyConfigurationModal.showForm(formOpts);
			},

			editItem: function(item) {
				var formOpts = {};
				formOpts.warrantyConfiguration = item;
				var index = this.items.indexOf(item);
				formOpts.onSaved = angular.bind(this, this.onEdited, index);
				this.WarrantyConfigurationModal.showForm(formOpts);
			},

			onEdited: function(index, item) {
				this.items[index] = item;
			}
		});

		this.model = new Model();
	}

})();