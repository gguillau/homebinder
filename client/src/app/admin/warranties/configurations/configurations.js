(function() {
    "use strict";

    angular
        .module("hb.admin.warranties.configurations", [
            "hb.admin.warranties.configurations.index",
            "hb.admin.warranties.configurations.modal",
            "hb.admin.warranties.configurations.form"
        ]);
})();