angular
    .module("hb.admin.warranties.configurations.modal", [])
    .factory("WarrantyConfigurationModal", [
        "hb.framework.toggleModalBase",
        "hb.resources",
        function(ModalBase, resources) {
            var instance = null;
            var Modal = function() {
                ModalBase.call(this);
                this.templateUrl = "admin/warranties/configurations/modal/modal.tpl.html";
                this.controller = "WarrantyConfigurationModalController as ctrl";
                this.windowClass = "widget-modal";
            };

            Modal.prototype = Object.create(ModalBase.prototype);

            if (!instance) {
                instance = new Modal();
            }

            return instance;
        }
    ])
    .controller("WarrantyConfigurationModalController", [
        "hb.framework.toggleModalControllerBase",
        "$modalInstance",
        "args",
        "hb.api",
        "hb.resources",
        "Notify",
        "$log",
        function(ToggleModalControllerBase, $modalInstance, args, api, resources, notify, $log) {
            var Model = function() {
                ToggleModalControllerBase.call(this);
                this.args = args;
                this.populateCall = api.warrantyConfiguration.get;
                this.$modalInstance = $modalInstance;
                this.objectProperty = "warrantyConfiguration";
                this.objectIdProperty = "warrantyConfigurationId";
                this.nameProperty = "name";
                this.resources = resources.warrantyConfigurationForm;
                this.init();
            };

            Model.prototype = Object.create(ToggleModalControllerBase.prototype);

            this.model = new Model();
        }
    ]);