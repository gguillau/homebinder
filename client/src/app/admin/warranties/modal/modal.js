angular
    .module("hb.admin.warranties.modal", [])
    .factory("WarrantyModal", [
        "hb.framework.toggleModalBase",
        "hb.resources",
        function(ModalBase, resources) {
            var instance = null;
            var Modal = function() {
                ModalBase.call(this);
                this.templateUrl = "admin/warranties/modal/modal.tpl.html";
                this.controller = "WarrantyModalController as ctrl";
                this.windowClass = "widget-modal";
            };

            Modal.prototype = Object.create(ModalBase.prototype);

            if (!instance) {
                instance = new Modal();
            }

            return instance;
        }
    ])
    .controller("WarrantyModalController", [
        "hb.framework.toggleModalControllerBase",
        "$modalInstance",
        "args",
        "hb.api",
        "hb.resources",
        "Notify",
        "$log",
        function(ToggleModalControllerBase, $modalInstance, args, api, resources, notify, $log) {
            var Model = function() {
                ToggleModalControllerBase.call(this);
                this.args = args;
                this.populateCall = api.warranty.get;
                this.$modalInstance = $modalInstance;
                this.objectProperty = "warranty";
                this.objectIdProperty = "warrantyId";
                this.nameProperty = "name";
                this.resources = resources.warrantyForm;
                this.init();
            };

            Model.prototype = Object.create(ToggleModalControllerBase.prototype);

            this.model = new Model();
        }
    ]);