describe("WarrantiesIndexCtrl", function() {
    var controller,
        base,
        api,
        resources,
        WarrantyModal,
        BindersWarrantiesOrderModalForm,
        $q,
        notify;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, _$q_, $rootScope) {
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        notify = $injector.get("Notify");
        WarrantyModal = $injector.get("WarrantyModal");
        BindersWarrantiesOrderModalForm = $injector.get("BindersWarrantiesOrderModalForm");
        $q = _$q_;

        spyOn(api.warranty, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));
        spyOn(api.warranty, "destroy").and.returnValue($q.when({}));

        controller = $controller("WarrantiesIndexCtrl", {
            "hb.framework.indexBase": base,
            "hb.api": api,
            "hb.resources": resources,
            "WarrantyModal": WarrantyModal,
            "Notify": notify,
            "BindersWarrantiesOrderModalForm": BindersWarrantiesOrderModalForm
        });

        $rootScope.$apply();
    }));

    describe("init", function() {
        it("sets attributes", function() {
            expect(controller.model.maxSize).toBe(10);
            expect(controller.model.sortOptions.length).toBe(2);
        });
    });

    describe("refresh", function() {
        it("gets the warranties", function() {
            expect(api.warranty.all).toHaveBeenCalled();
            expect(controller.model.columnspan).toBe(8);
            expect(controller.model.headers.length).toBe(8);
        });
    });

    describe("newItem", function() {
        it("calls modal", function() {
            spyOn(BindersWarrantiesOrderModalForm, "show");
            controller.model.newItem();
            expect(BindersWarrantiesOrderModalForm.show).toHaveBeenCalledWith({
                binder: null,
                closed: jasmine.any(Function)
            });
        });
    });

    describe("editItem", function() {
        it("calls modal", function() {
            spyOn(WarrantyModal, "showForm");
            controller.model.editItem({
                id: 1,
                name: "name"
            });
            expect(WarrantyModal.showForm).toHaveBeenCalledWith({
                warranty: {
                    id: 1,
                    name: 'name'
                },
                onSaved: jasmine.any(Function)
            });
        });
    });

    describe("onEdited", function() {
        it("inserts item into array", function() {
            expect(controller.model.items[0].name).toBe("name");
            spyOn(WarrantyModal, "showForm");
            controller.model.onEdited(0, {
                name: "test"
            });
            expect(controller.model.items[0].name).toBe("test");
        });
    });

    describe("downloadSummaries", function() {
        it("calls api downloadSummaries", function() {
            spyOn(api.warranty, "downloadSummaries").and.returnValue($q.when({ data: {} }));

            controller.model.downloadSummaries();

            expect(api.warranty.downloadSummaries).toHaveBeenCalled();
        });
    });

    describe("downloadSuccess", function() {
        it("calls notify info", function() {
            spyOn(notify, "info");

            controller.model.downloadSuccess();

            expect(notify.info).toHaveBeenCalled();
        });
    });

    describe("downloadError", function() {
        it("calls notify error", function() {
            spyOn(notify, "error");

            controller.model.downloadError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("addArgs", function() {
        it("sets the query arg for partnerId to 1", function() {
            controller.model.warrantyArgs["partnerId"] = 1;
            controller.model.addArgs("partnerId");

            expect(controller.model.queryArgs["partnerId"]).toEqual(1);
        });
    });
});

describe('warrantyIndex', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_, _$q_) {
        api = _$injector_.get("hb.api");
        $q = _$q_;
        spyOn(api.warranty, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));
        $scope = $rootScope.$new(), $compile = _$compile_;

        $compile('<warranty-index></warranty-index>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the hb-organization-from directive', function() {
        it('displays the title', function() {
            expect(api.warranty.all).toHaveBeenCalled();
        });
    });
});