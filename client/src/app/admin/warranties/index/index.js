(function() {
	"use strict";

	angular
		.module("hb.admin.warranties.index", [
			"ui.router",
			"ui.bootstrap",
			"hb.components"
		])
		.directive("warrantyIndex", function() {
			return {
				restrict: "E",
				templateUrl: "admin/warranties/index/index.tpl.html",
				controller: "WarrantiesIndexCtrl",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};

		})
		.controller("WarrantiesIndexCtrl", [
			"hb.api",
			"ModalService",
			"Notify",
			"$log",
			"Loading",
			"hb.framework.indexBase",
			"hb.resources",
			"WarrantyModal",
			"BindersWarrantiesOrderModalForm",
			WarrantiesIndexCtrl
		]);

	function WarrantiesIndexCtrl(api, ModalService, Notify, $log, loading, IndexBase, resources, WarrantyModal, BindersWarrantiesOrderModalForm) {
		var Model = function() {
			// call the parent class
			IndexBase.call(this);
			// apply warranty company specific resource strings
			this.resources = angular.extend({}, this.resources, resources.warrantyIndex);
			this.WarrantyModal = WarrantyModal;
			this.BindersWarrantiesOrderModalForm = BindersWarrantiesOrderModalForm;
			// set the refresh API call
			this.refreshCall = api.warranty.all;
			// set the delete API call
			this.deleteCall = api.warranty.destroy;
			// set the binder_id property used in the delete message
			this.nameProperty = "binder_id";
			this.maxSize = 10;
			this.sortOptions = [{
				orderBy: "warranties.created_at",
				order: "desc",
				desc: this.resources.creationDate + " - " + this.resources.descending
			}, {
				orderBy: "warranties.created_at",
				order: "asc",
				desc: this.resources.creationDate + " - " + this.resources.ascending
			}];
			this.sortOption = this.sortOptions[0];
			this.orderBy = this.sortOption.orderBy;
			this.order = this.sortOption.order;
			this.headers = null;
			this.warrantyArgs = {
				partnerId: undefined
			};
			this.setHeaders();
			this.refresh();
		};

		Model.prototype = Object.create(IndexBase.prototype);

		angular.extend(Model.prototype, {
			setHeaders: function() {
				this.columnspan = 8;
				this.headers = this.resources.warrantyAttributes.map(angular.bind(this, function(attribute) {
					var sortable = false,
						orderBy = null,
						sorted = false,
						show = true;
					if (attribute === "ID") {
						sorted = true;
						sortable = true;
						orderBy = "warranties.id";
					}
					else if (attribute === "Binder ID") {
						sortable = true;
						orderBy = "warranties.binder_id";
					}
					else if (attribute === "Request Date") {
						sortable = true;
						orderBy = "warranties.created_at";
					}
					else if (attribute === "Status") {
						sortable = true;
						orderBy = "warranties.status";
					}
					return {
						name: attribute,
						sortable: sortable,
						sorted: sorted,
						orderBy: orderBy,
						show: show,
						order: "desc"
					};
				}));
				this.sortOption = this.headers[0];
			},

			newItem: function() {
				this.BindersWarrantiesOrderModalForm.show({
					binder: null,
					closed: angular.bind(this, this.onAdded)
				});
			},

			editItem: function(item) {
				var formOpts = {};
				formOpts.warranty = item;
				var index = this.items.indexOf(item);
				formOpts.onSaved = angular.bind(this, this.onEdited, index);
				this.WarrantyModal.showForm(formOpts);
			},

			onEdited: function(index, item) {
				this.items[index] = item;
			},

			downloadSummaries: function() {
				api.warranty.downloadSummaries().then(
					angular.bind(this, this.downloadSuccess),
					angular.bind(this, this.downloadError));
			},

			downloadSuccess: function(response) {
				Notify.info("Please check your email");
			},

			downloadError: function(response) {
				Notify.error(response.data);
			},

			addQueryArgs: function() {
				this.addArgs("partnerId");
			},

			addArgs: function(arg) {
				if (this.warrantyArgs[arg]) {
					this.queryArgs[arg] = this.warrantyArgs[arg];
				}
				else {
					delete this.queryArgs[arg];
				}
			}
		});

		this.model = new Model();
	}

})();