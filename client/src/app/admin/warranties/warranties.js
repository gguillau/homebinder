(function() {
	"use strict";

	angular.module('hb.admin.warranties', [
			'ui.router',
			'ui.bootstrap',
			'hb.components',
			"hb.admin.warranties.companies",
			"hb.admin.warranties.configurations",
			"hb.admin.warranties.index",
			"hb.admin.warranties.modal",
			"hb.admin.warranties.form",
			"hb.admin.warranties.plans"
		])
		.config(['$stateProvider', adminWarrantiesConfig])
		.controller("WarrantiesCtrl", [
			"hb.resources",
			WarrantiesCtrl
		]);

	function WarrantiesCtrl(resources) {
		this.resources = resources.warranties;
		this.navigation_links = [{
			name: "Menu",
			links: [{
				value: "companies",
				name: this.resources.companies,
				active: true
			}, {
				value: "plans",
				name: this.resources.plans,
				active: false
			}, {
				value: "configurations",
				name: this.resources.configurations,
				active: false
			}, {
				value: "warranties",
				name: this.resources.warranties,
				active: false
			}]
		}];
		this.currentLink = this.navigation_links[0].links[0];
	}

	WarrantiesCtrl.prototype = {

		changeLink: function(link) {
			this.currentLink.active = false;
			link.active = true;
			this.currentLink = link;
		}
	};

	function adminWarrantiesConfig($stateProvider) {
		$stateProvider
			.state('admin.warranties', {
				url: '^/admin/warranties',
				templateUrl: 'admin/warranties/warranties.tpl.html',
				controller: 'WarrantiesCtrl',
				controllerAs: "ctrl",
				access: {
					requiresLogin: true
				}
			});
	}

})();