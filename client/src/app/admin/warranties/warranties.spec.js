describe("WarrantiesCtrl", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("WarrantiesCtrl",
            {
            });

        $scope.ctrl = ctrl;

    }

    describe('init', function() {

        it('init the controller', function() {

            createController();
            $rootScope.$apply();

            expect(ctrl.navigation_links[0].links.length).toBe(4);
            expect(ctrl.currentLink.active).toEqual(true);
            expect(ctrl.currentLink.value).toBe("companies");
            expect(ctrl.currentLink.name).toEqual("Companies");

        });

    });

    describe('changeLink', function() {

        it('init the controller', function() {

            createController();
            $rootScope.$apply();
            ctrl.changeLink(ctrl.navigation_links[0].links[2]);
            $rootScope.$apply();

            expect(ctrl.navigation_links[0].links[0].active).toEqual(false);
            expect(ctrl.currentLink.active).toEqual(true);
            expect(ctrl.currentLink.value).toBe("configurations");
            expect(ctrl.currentLink.name).toEqual("Partners");

        });

    });


});