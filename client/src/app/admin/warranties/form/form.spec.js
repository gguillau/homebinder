describe("WarrantyFormController", function() {
    var ctrl,
        controller,
        $q,
        $log,
        api,
        notify,
        resources,
        loading,
        warranty,
        $rootScope;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
        $rootScope = $injector.get("$rootScope");

        warranty = {
            name: "90-Day Warranty",
            address: {
                address1: "234 Test St",
                address2: null,
                addressable_id: 279,
                addressable_type: "Warranty",
                city: "Boston",
                country: "US",
                id: 246641,
                lat: null,
                long: null,
                name: null,
                state: "MA",
                zip: "02128"
            },
            binder: {
                active: true,
                create_method: "manual",
                created_at: "2017-07-21T10:58:01.096-04:00",
                created_by: 1284,
                details: "",
                hero_image_id: null,
                id: 72023,
                name: "Test Test Home",
                primary: null,
                updated_at: "2017-07-21T10:58:01.096-04:00"
            },
            binder_id: 72023,
            client_email: "greg+testtes0293@homebinder.com",
            client_first: "Test",
            client_last: "Test",
            client_phone: "+12384787323",
            created_at: "2017-07-21T11:29:55.118-04:00",
            expiration_date: "2017-10-18T00:00:00.000-04:00",
            id: 279,
            last_inspection: {
                binder_id: 72023,
                created_at: "2017-07-21T10:58:01.169-04:00",
                description: null,
                id: 560,
                tracking_number: null,
                transaction_cost: null,
                transaction_date: "2017-07-20T00:00:00.000-04:00",
                transaction_type: "buy_side_inspection",
                updated_at: "2017-07-21T10:58:01.169-04:00"
            },
            partner: {
                account_id: 427,
                affiliate_code: null,
                binder_logo_id: 834,
                code: "partner163",
                completed_onboarding: true,
                contact: "John Doe",
                created_at: "2015-06-23T18:59:04.429-04:00",
                email: "greg+inspector@homebinder.com",
                email_display_name: "Complete Home Inspections",
                id: 163,
                logo_content_type: "image/png",
                logo_file_name: "Complete Home Inspections Logo.png",
                logo_file_size: 14298,
                logo_updated_at: "2017-05-09T14:09:51.254-04:00",
                name: "Complete Home Inspections",
                partner_key: "completehomeinspections",
                partner_type: "inspector",
                phone: "+18599858654",
                sellers_logo_id: null,
                updated_at: "2017-05-09T14:09:52.354-04:00",
                website: "http://www.completehomeinspections.com"
            },
            price: 16,
            status: "pending",
            updated_at: "2017-07-21T11:29:55.118-04:00",
            warranty_company: {
                contact: "Greg Moretti",
                created_at: "2017-07-21T10:30:37.046-04:00",
                email: "greg+warrantyco2@homebinder.com",
                id: 2,
                name: "CP2",
                updated_at: "2017-07-21T10:30:37.046-04:00"
            },
            warranty_plan: {
                created_at: "2017-07-21T10:31:02.771-04:00",
                cycle: "days",
                description: "90-Day Warranty",
                duration: 90,
                id: 2,
                name: "90-Day Warranty",
                price: "15.0",
                updated_at: "2017-07-21T10:31:02.771-04:00"
            },
            warranty_plan_id: 2
        };
    }));

    function createController() {
        ctrl = controller("WarrantyFormController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "Loading": loading,
            "hb.resources": resources
        });
    }

    describe("populate", function() {
        it("returns when no cfg is provided", function() {
            createController();
            ctrl.cfg = null;
            ctrl.model.populate();
        });

        it("populates resource", function() {
            createController();
            ctrl.cfg = {
                item: warranty
            };

            ctrl.model.populate();

            expect(ctrl.model.resource).not.toBeNull();
            expect(ctrl.model.name).toEqual("90-Day Warranty");
            expect(ctrl.model.title).toEqual("90-Day Warranty");
            expect(ctrl.model.status).toEqual("pending");
            expect(ctrl.model.expiration_date).toEqual(new Date(warranty.expiration_date));
            expect(ctrl.model.binder_id).toEqual(warranty.binder_id);
            expect(ctrl.model.warranty_plan_id).toEqual(warranty.warranty_plan.id);
            expect(ctrl.model.binder).toEqual(warranty.binder);
            expect(ctrl.model.warranty_plan).toEqual(warranty.warranty_plan);
            expect(ctrl.model.warranty_company).toEqual(warranty.warranty_company);
            expect(ctrl.model.price).toEqual(warranty.price);
            expect(ctrl.model.client_first).toEqual(warranty.client_first);
            expect(ctrl.model.client_last).toEqual(warranty.client_last);
            expect(ctrl.model.client_email).toEqual(warranty.client_email);
            expect(ctrl.model.client_phone).toEqual(warranty.client_phone);
            expect(ctrl.model.inspection_date).toEqual(warranty.last_inspection.transaction_date);
            expect(ctrl.model.property).toEqual(warranty.address);
            expect(ctrl.cfg.api.save).not.toBeNull();
        });

        it("does not populate when no resource is provided", function() {
            createController();
            ctrl.cfg = {};
            ctrl.model.populate();

            expect(ctrl.model.resource).toBeUndefined();
            expect(ctrl.cfg.api.save).not.toBeNull();
        });
    });

    describe("loadWarrantyCompanies", function() {
        it("loads the companies", function() {
            spyOn(api.warrantyCompany, "all").and.returnValue($q.when({
                data: {
                    items: []
                }
            }));
            createController();
            ctrl.model.loadWarrantyCompanies();
            expect(api.warrantyCompany.all).toHaveBeenCalledWith({
                page: 1,
                count: 50
            });
        });

        it("doesn't load the companies", function() {
            spyOn(api.warrantyCompany, "all").and.returnValue($q.reject({
                data: "error"
            }));

            createController();
            spyOn($log, "error");
            ctrl.model.loadWarrantyCompanies();
            $rootScope.$apply();

            expect(api.warrantyCompany.all).toHaveBeenCalledWith({
                page: 1,
                count: 50
            });
            expect($log.error).toHaveBeenCalled();
        });
    });

    describe("onCompanyChange", function() {
        it("calls loadWarrantyPlans", function() {
            createController();
            spyOn(ctrl.model, "loadWarrantyPlans");
            ctrl.model.onCompanyChange();

            expect(ctrl.model.loadWarrantyPlans).toHaveBeenCalled();
        });
    });

    describe("loadWarrantyPlans", function() {
        it("loads the plans", function() {
            spyOn(api.warrantyCompany, "all").and.returnValue($q.when({
                data: {
                    items: []
                }
            }));
            spyOn(api.warrantyPlan, "all").and.returnValue($q.when({
                data: {
                    items: []
                }
            }));
            createController();
            ctrl.model.warranty_company = {
                id: 1
            };
            ctrl.model.loadWarrantyPlans();
            expect(api.warrantyPlan.all).toHaveBeenCalledWith({
                page: 1,
                count: 50,
                warranty_company_id: 1
            });
        });

        it("doesn't load the plans", function() {
            spyOn(api.warrantyCompany, "all").and.returnValue($q.when({
                data: {
                    items: []
                }
            }));
            spyOn(api.warrantyPlan, "all").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");

            createController();
            ctrl.model.warranty_company = {
                id: 1
            };
            ctrl.model.loadWarrantyPlans();
            $rootScope.$apply();

            expect(api.warrantyPlan.all).toHaveBeenCalledWith({
                page: 1,
                count: 50,
                warranty_company_id: 1
            });
            expect($log.error).toHaveBeenCalled();
        });

    });

    describe("onLoadWarrantyPlansSuccess", function() {
        it("sets the warranty_plans", function() {
            createController();
            ctrl.model.onLoadWarrantyPlansSuccess({ data: { items: [{ id: 1 }] } });

            expect(ctrl.model.warranty_plans.length).toEqual(1);
        });
    });

    describe("binderLookup", function() {
        it("calls api binder all", function() {
            spyOn(api.warrantyCompany, "all").and.returnValue($q.when({ data: {} }));
            spyOn(api.binder, "all").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.model.binderLookup("search");
            $rootScope.$apply();

            expect(api.binder.all).toHaveBeenCalled();
        });
    });

    describe("planLookup", function() {
        it("calls api warrantyPlan all", function() {
            spyOn(api.warrantyCompany, "all").and.returnValue($q.when({ data: {} }));
            spyOn(api.warrantyPlan, "all").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.model.planLookup("search");
            $rootScope.$apply();

            expect(api.warrantyPlan.all).toHaveBeenCalled();
        });
    });

    describe("onPlanChange", function() {
        it("sets the price", function() {
            createController();
            ctrl.model.warranty_plan = { price: 1 };
            ctrl.model.onPlanChange();

            expect(ctrl.model.price).toEqual(1);
        });
    });

    describe("submit", function() {
        it("does not submit invalid data", function() {
            spyOn(api.warrantyPlan, "create").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = {};
            ctrl.form = {
                $invalid: true
            };
            ctrl.model.submit();
            expect(ctrl.form.$submitted).toEqual(true);
            expect(api.warrantyPlan.create).not.toHaveBeenCalled();
        });

        it("creates a new resource", function() {
            spyOn(api.warranty, "create").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = {};
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.binder = warranty.binder;
            ctrl.model.warranty_plan = warranty.warranty_plan;
            ctrl.model.submit();
            expect(api.warranty.create).toHaveBeenCalledWith({
                warranty: {
                    status: ctrl.model.status,
                    expiration_date: ctrl.model.expiration_date,
                    binder_id: ctrl.model.binder.id,
                    warranty_plan_id: ctrl.model.warranty_plan.id,
                    client_first: ctrl.model.client_first,
                    client_last: ctrl.model.client_last,
                    client_email: ctrl.model.client_email,
                    client_phone: ctrl.model.client_phone,
                    address_attributes: ctrl.model.property
                },
                price: ctrl.model.price
            });
        });

        it("updates an existing resource", function() {
            spyOn(api.warranty, "update").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = {
                item: warranty
            };
            ctrl.model.populate();
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.submit();
            expect(api.warranty.update).toHaveBeenCalledWith(warranty.id, {
                warranty: {
                    status: warranty.status,
                    expiration_date: ctrl.model.expiration_date,
                    binder_id: ctrl.model.binder.id,
                    warranty_plan_id: ctrl.model.warranty_plan.id,
                    client_first: ctrl.model.client_first,
                    client_last: ctrl.model.client_last,
                    client_email: ctrl.model.client_email,
                    client_phone: ctrl.model.client_phone,
                    address_attributes: ctrl.model.property
                },
                price: ctrl.model.price
            });
        });
    });

    describe("onResourceSaved", function() {
        it("saves the response and calls onSaved", function() {
            var resource = {
                id: 1,
                name: "resource",
                description: "description"
            };
            createController();
            ctrl.cfg = {};
            ctrl.onSaved = function() {};
            spyOn(ctrl, "onSaved");
            ctrl.model.onResourceSaved({
                data: resource
            });
            expect(ctrl.model.resource).toEqual(resource);
            expect(ctrl.cfg.resource).toEqual(resource);
            expect(ctrl.onSaved).toHaveBeenCalledWith(jasmine.objectContaining({
                item: resource
            }));
        });
    });

    describe("onResourceSaveError", function() {
        it("handles a general error", function() {
            spyOn(notify, "error");
            createController();
            ctrl.cfg = {};
            ctrl.model.onResourceSaveError({
                status: 500,
                data: "error"
            });
            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("onDoneSaving", function() {
        it("calls loading close", function() {
            spyOn(loading, "close");
            createController();
            ctrl.cfg = {};
            ctrl.model.onDoneSaving();
            expect(loading.close).toHaveBeenCalled();
        });
    });

    describe("open", function() {
        it("sets date to be opened", function() {
            createController();
            ctrl.model.open(0);

            expect(ctrl.model.date[0].opened).toBe(true);
        });
    });

    describe("onSelect", function() {
        it("sets the country", function() {
            createController();
            var item = { country: {} };
            ctrl.model.property = {};
            ctrl.model.onSelect(item);

            expect(ctrl.model.country).toEqual(item.country);
        });
    });

    describe("onSelectCountry", function() {
        it("sets the country", function() {
            createController();
            ctrl.model.onSelectCountry({ value: "MA" });

            expect(ctrl.model.country).toEqual({ value: "MA" });
        });
    });

});

describe('hbWarrantyForm', function() {
    var $scope, $compile, element, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        $scope = $rootScope.$new(), $compile = _$compile_;
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        spyOn(api.warrantyCompany, "all").and.returnValue($q.when({ data: { items: [] } }));

        $scope.cfg = {};
        $scope.saved = function() {};
        $scope.cancel = function() {};

        element = $compile('<hb-warranty-form cfg="cfg" on-saved="saved" on-cancel="cancel"></hb-warranty-form>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the hb-warranty-form directive', function() {
        it('sets the html', function() {
            expect(element.html()).toContain("Client First Name");
        });
    });
});