(function() {
    "use strict";

    angular
        .module("hb.admin.warranties.form", [])
        .directive("hbWarrantyForm", function() {
            return {
                restrict: "E",
                templateUrl: "admin/warranties/form/form.tpl.html",
                controller: "WarrantyFormController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    cfg: "=",
                    onSaved: "&",
                    onCancel: "&"
                }
            };
        })
        // Model backing the area modal form
        .controller("WarrantyFormController", [
            "$log",
            "hb.api",
            "Notify",
            "Loading",
            "hb.resources",
            "Address",
            Controller
        ]);

    function Controller($log, api, notify, loading, resources, address) {
        var that = this;
        var Model = function() {
            this.$log = $log;
            this.api = api;
            this.notify = notify;
            this.resources = resources.warrantyForm;
            this.loading = loading;
            this.name = null;
            this.binder_id = null;
            this.warranty_plan_id = null;
            this.status = null;
            this.expiration_date = null;
            this.binderLookupCfg = {
                allowClear: true,
                placeholder: "Search for binder",
                refresh: angular.bind(this, this.binderLookup),
                itemTemplate: "components/lookup/partner/partnerDropDownItem.tpl.html",
                selectionTemplate: "components/lookup/partner/partnerSelection.tpl.html"
            };
            this.planLookupCfg = {
                allowClear: true,
                placeholder: "Search for plan",
                refresh: angular.bind(this, this.planLookup),
                itemTemplate: "components/lookup/partner/partnerDropDownItem.tpl.html",
                selectionTemplate: "components/lookup/partner/partnerSelection.tpl.html"
            };
            this.statuses = [{
                value: "pending",
                name: "Pending"
            }, {
                value: "cancelled",
                name: "Cancelled"
            }, {
                value: "submitted",
                name: "Submitted"
            }];
            this.datepicker = {
                format: "MMMM dd, yyyy",
                options: {
                    "show-button-bar": false
                }
            };
            this.date = [{
                opened: false
            }, {
                opened: false
            }];
            this.address = address;
            this.countries = address.countries();
            this.states = address.getStatesAndProvinces();
            this.populate();
            this.loadWarrantyCompanies();
        };

        angular.extend(Model.prototype, {
            populate: function() {
                if (!that.cfg) {
                    return;
                }

                // setup the save callback
                that.cfg.api = {
                    save: angular.bind(this, this.submit)
                };

                // load the resource
                if (that.cfg.item) {
                    this.resource = that.cfg.item;
                    this.title = this.resource.name;
                    this.name = this.resource.name;
                    this.status = this.resource.status;
                    this.expiration_date = new Date(this.resource.expiration_date);
                    this.binder_id = this.resource.binder_id;
                    this.warranty_plan_id = this.resource.warranty_plan_id;
                    this.binder = this.resource.binder;
                    this.warranty_plan = this.resource.warranty_plan;
                    this.warranty_company = this.resource.warranty_company;
                    this.price = this.resource.price;
                    this.client_first = this.resource.client_first;
                    this.client_last = this.resource.client_last;
                    this.client_email = this.resource.client_email;
                    this.client_phone = this.resource.client_phone;
                    this.inspection_date = this.resource.last_inspection.transaction_date;
                    this.property = this.resource.address;
                    this.loadWarrantyPlans();
                    this.country = address.findCountryByCode(this.property.country);
                    this.states = address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                        return state.country.value === this.country.value;
                    }));
                }
            },

            loadWarrantyCompanies: function() {
                var opts = {
                    page: 1,
                    count: 50
                };
                this.api.warrantyCompany.all(opts).then(
                    angular.bind(this, this.onLoadWarrantyCompaniesSuccess),
                    angular.bind(this, this.onLoadWarrantyCompaniesError)
                );
            },

            onLoadWarrantyCompaniesSuccess: function(response) {
                this.warranty_companies = response.data.items;
            },

            onLoadWarrantyCompaniesError: function(response) {
                this.$log.error(response);
            },

            onCompanyChange: function() {
                this.warranty_plan = null;
                this.warranty_plans = null;
                this.loadWarrantyPlans();
            },

            loadWarrantyPlans: function() {
                var opts = {
                    page: 1,
                    count: 50,
                    warranty_company_id: this.warranty_company.id
                };
                this.api.warrantyPlan.all(opts).then(
                    angular.bind(this, this.onLoadWarrantyPlansSuccess),
                    angular.bind(this, this.onLoadWarrantyPlansError)
                );
            },

            onLoadWarrantyPlansSuccess: function(response) {
                this.warranty_plans = response.data.items;
            },

            onLoadWarrantyPlansError: function(response) {
                this.$log.error(response);
            },

            onPlanChange: function() {
                this.price = this.warranty_plan.price;
            },

            binderLookup: function(value) {
                var opts = {
                    page: 1,
                    count: 50,
                    search: value,
                    searchMethod: "for_admin"
                };
                return this.api.binder.all(opts).then(
                    angular.bind(this, function(response) {
                        return {
                            data: response.data.items
                        };
                    })
                );
            },

            planLookup: function(value) {
                var opts = {
                    page: 1,
                    count: 50,
                    search: value
                };
                return this.api.warrantyPlan.all(opts).then(
                    angular.bind(this, function(response) {
                        return {
                            data: response.data.items
                        };
                    })
                );
            },

            submit: function() {
                that.form.$submitted = true;

                if (that.form.$invalid) {
                    return;
                }

                var data = {
                        warranty: {
                            status: this.status,
                            expiration_date: this.expiration_date,
                            binder_id: this.binder.id,
                            warranty_plan_id: this.warranty_plan.id,
                            client_first: this.client_first,
                            client_last: this.client_last,
                            client_email: this.client_email,
                            client_phone: this.client_phone,
                            address_attributes: this.property
                        },
                        price: this.price
                    },
                    promise;

                this.error422 = false;
                this.loading.show(this.resources.saving);

                if (this.resource) {
                    promise = api.warranty.update(this.resource.id, data);
                }
                else {
                    promise = api.warranty.create(data);
                }

                promise.then(angular.bind(this, this.onResourceSaved),
                        angular.bind(this, this.onResourceSaveError))
                    /*jshint -W024*/
                    .finally(angular.bind(this, this.onDoneSaving));
            },

            onResourceSaved: function(response) {
                this.resource = response.data;
                that.cfg.resource = this.resource;
                that.onSaved({
                    item: this.resource
                });
            },

            onResourceSaveError: function(response) {
                this.notify.error(response.data);
            },

            onDoneSaving: function() {
                this.loading.close();
            },

            open: function(index) {
                this.date[index].opened = true;
            },

            onSelect: function(item) {
                if (item) {
                    this.country = item.country;
                    this.property.country = item.country.value;
                    this.states = address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                        return state.country.value === this.country.value;
                    }));
                }
            },

            onSelectCountry: function(item) {
                this.country = item;
                this.states = address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                    return state.country.value === this.country.value;
                }));
            }
        });

        this.model = new Model();
    }
})();