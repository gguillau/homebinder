(function() {
    "use strict";

    angular
        .module("hb.admin.warranties.companies",[
            "hb.admin.warranties.companies.index",
            "hb.admin.warranties.companies.modal",
            "hb.admin.warranties.companies.form"
        ]);
})();