angular
    .module("hb.admin.warranties.companies.modal", [])
    .factory("WarrantyCompanyModal", [
        "hb.framework.toggleModalBase",
        "hb.resources",
        function(ModalBase, resources) {
            var instance = null;
            var Modal = function() {
                ModalBase.call(this);
                this.templateUrl = "admin/warranties/companies/modal/modal.tpl.html";
                this.controller = "WarrantyCompanyModalController as ctrl";
                this.windowClass = "widget-modal";
            };

            Modal.prototype = Object.create(ModalBase.prototype);

            if (!instance) {
                instance = new Modal();
            }

            return instance;
        }
    ])
    .controller("WarrantyCompanyModalController", [
        "hb.framework.toggleModalControllerBase",
        "$modalInstance",
        "args",
        "hb.api",
        "hb.resources",
        "Notify",
        "$log",
        function(ToggleModalControllerBase, $modalInstance, args, api, resources, notify, $log) {
            var Model = function() {
                ToggleModalControllerBase.call(this);
                this.args = args;
                this.populateCall = api.warrantyCompany.get;
                this.$modalInstance = $modalInstance;
                this.objectProperty = "warrantyCompany";
                this.objectIdProperty = "warrantyCompanyId";
                this.nameProperty = "name";
                this.resources = resources.warrantyCompanyForm;
                this.init();
            };

            Model.prototype = Object.create(ToggleModalControllerBase.prototype);

            this.model = new Model();
        }
    ]);