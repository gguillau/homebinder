(function() {
    "use strict";

    angular
        .module("hb.admin.warranties.companies.form", [])
        .directive("hbWarrantyCompanyForm", function() {
            return {
                restrict: "E",
                templateUrl: "admin/warranties/companies/form/form.tpl.html",
                controller: "WarrantyCompanyFormController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    cfg: "=",
                    onSaved: "&",
                    onCancel: "&"
                }
            };
        })
        // Model backing the area modal form
        .controller("WarrantyCompanyFormController", [
            "$log",
            "hb.api",
            "Notify",
            "Loading",
            "hb.resources",
            Controller
        ]);

    function Controller($log, api, notify, loading, resources) {
        var that = this;
        var Model = function() {
            this.$log = $log;
            this.api = api;
            this.notify = notify;
            this.resources = resources.warrantyCompanyForm;
            this.loading = loading;
            this.name = null;
            this.contact = null;
            this.email = null;
            this.secondary_email = null;
            this.populate();
        };

        angular.extend(Model.prototype, {
            populate: function() {
                if (!that.cfg) {
                    return;
                }

                // setup the save callback
                that.cfg.api = {
                    save: angular.bind(this, this.submit)
                };

                // load the resource
                if (that.cfg.item) {
                    this.resource = that.cfg.item;
                    this.title = this.resource.name;
                    this.name = this.resource.name;
                    this.contact = this.resource.contact;
                    this.email = this.resource.email;
                    this.secondary_email = this.resource.secondary_email;
                }
            },

            submit: function() {
                that.form.$submitted = true;

                if (that.form.$invalid) {
                    return;
                }

                var data = {
                        warranty_company: {
                            name: this.name,
                            contact: this.contact,
                            email: this.email,
                            secondary_email: this.secondary_email
                        }
                    },
                    promise;

                this.error422 = false;
                this.loading.show(this.resources.saving);

                if (this.resource) {
                    promise = api.warrantyCompany.update(this.resource.id, data);
                }
                else {
                    promise = api.warrantyCompany.create(data);
                }

                promise.then(angular.bind(this, this.onResourceSaved),
                        angular.bind(this, this.onResourceSaveError))
                    /*jshint -W024*/
                    .finally(angular.bind(this, this.onDoneSaving));
            },

            onResourceSaved: function(response) {
                this.resource = response.data;
                that.cfg.resource = this.resource;
                that.onSaved({
                    item: this.resource
                });
            },

            onResourceSaveError: function(response) {
                this.notify.error(response.data);
            },

            onDoneSaving: function() {
                this.loading.close();
            }
        });

        this.model = new Model();
    }
})();