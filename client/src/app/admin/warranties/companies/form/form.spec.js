describe("WarrantyCompanyFormController", function() {
    var ctrl,
        controller,
        $q,
        $log,
        api,
        notify,
        resources,
        loading,
        warranty_company;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
        $rootScope = $injector.get("$rootScope");

        warranty_company = {
            contact: "Greg Moretti",
            created_at: "2017-07-21T10:30:37.046-04:00",
            email: "greg+warrantyco2@homebinder.com",
            secondary_email: "test@gmail.com",
            id: 2,
            name: "CP2",
            updated_at: "2017-07-21T10:30:37.046-04:00"
        };
    }));

    function createController() {
        ctrl = controller("WarrantyCompanyFormController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "Loading": loading,
            "hb.resources": resources
        });
    }

    describe("populate", function() {
        it("returns when no cfg is provided", function() {
            createController();
            ctrl.cfg = null;
            ctrl.model.populate();
        });

        it("populates resource", function() {

            createController();
            ctrl.cfg = {
                item: warranty_company
            };

            ctrl.model.populate();

            expect(ctrl.model.resource).not.toBeNull();
            expect(ctrl.model.resource).toBe(warranty_company);
            expect(ctrl.model.title).toEqual(warranty_company.name);
            expect(ctrl.model.name).toEqual(warranty_company.name);
            expect(ctrl.model.contact).toEqual(warranty_company.contact);
            expect(ctrl.model.email).toEqual(warranty_company.email);
            expect(ctrl.model.secondary_email).toEqual(warranty_company.secondary_email);
            expect(ctrl.cfg.api.save).not.toBeNull();
        });

        it("does not populate when no resource is provided", function() {
            createController();
            ctrl.cfg = {};
            ctrl.model.populate();

            expect(ctrl.model.resource).toBeUndefined();
            expect(ctrl.cfg.api.save).not.toBeNull();
        });
    });

    describe("submit", function() {
        it("does not submit invalid data", function() {
            spyOn(api.warrantyCompany, "create").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = {};
            ctrl.form = {
                $invalid: true
            };
            ctrl.model.submit();
            expect(ctrl.form.$submitted).toEqual(true);
            expect(api.warrantyCompany.create).not.toHaveBeenCalled();
        });

        it("creates a new resource", function() {
            spyOn(api.warrantyCompany, "create").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = {};
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.name = "resource";
            ctrl.model.contact = "resource description";
            ctrl.model.submit();
            expect(api.warrantyCompany.create).toHaveBeenCalledWith({
                warranty_company: {
                    name: 'resource',
                    contact: 'resource description',
                    email: null,
                    secondary_email: null
                }
            });
        });

        it("updates an existing resource", function() {
            spyOn(api.warrantyCompany, "update").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = {
                item: warranty_company
            };
            ctrl.model.populate();
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.submit();
            expect(api.warrantyCompany.update).toHaveBeenCalledWith(2, {
                warranty_company: {
                    name: "CP2",
                    contact: "Greg Moretti",
                    email: "greg+warrantyco2@homebinder.com",
                    secondary_email: "test@gmail.com"
                }
            });
        });
    });

    describe("onResourceSaved", function() {
        it("saves the response and calls onSaved", function() {
            var resource = {
                id: 1,
                name: "resource",
                description: "description"
            };
            createController();
            ctrl.cfg = {};
            ctrl.onSaved = function() {};
            spyOn(ctrl, "onSaved");
            ctrl.model.onResourceSaved({
                data: resource
            });
            expect(ctrl.model.resource).toEqual(resource);
            expect(ctrl.cfg.resource).toEqual(resource);
            expect(ctrl.onSaved).toHaveBeenCalledWith(jasmine.objectContaining({
                item: resource
            }));
        });
    });

    describe("onResourceSaveError", function() {
        it("handles a general error", function() {
            spyOn(notify, "error");
            createController();
            ctrl.cfg = {};
            ctrl.model.onResourceSaveError({
                status: 500,
                data: "error"
            });
            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("onDoneSaving", function() {
        it("calls loading close", function() {
            spyOn(loading, "close");
            createController();
            ctrl.cfg = {};
            ctrl.model.onDoneSaving();
            expect(loading.close).toHaveBeenCalled();
        });
    });

});

describe('hbWarrantyCompanyForm', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        $scope.cfg = {};
        $scope.onSaved = function() {};
        $scope.onCancel = function() {};

        element = $compile('<hb-warranty-company-form cfg="cfg" on-saved="onSaved" on-cancel="onCancel"></hb-warranty-company-form>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the elements', function() {
            expect(element.html()).toContain("Complete Home Protection");
        });
    });
});