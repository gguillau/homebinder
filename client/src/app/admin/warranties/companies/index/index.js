(function() {
	"use strict";

	angular.module("hb.admin.warranties.companies.index", [
			"ui.router",
			"ui.bootstrap",
			"hb.components"
		])
		.directive("warrantyCompanies", function() {
			return {
				restrict: "E",
				templateUrl: "admin/warranties/companies/index/index.tpl.html",
				controller: "WarrantiesCompaniesIndexCtrl",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};

		})
		.controller("WarrantiesCompaniesIndexCtrl", [
			"hb.api",
			"ModalService",
			"Notify",
			"$log",
			"Loading",
			"hb.framework.indexBase",
			"hb.resources",
			"WarrantyCompanyModal",
			WarrantiesCompaniesIndexCtrl
		]);

	function WarrantiesCompaniesIndexCtrl(api, ModalService, Notify, $log, loading, IndexBase, resources, WarrantyCompanyModal) {
		var Model = function() {
			// call the parent class
			IndexBase.call(this);
			// apply warranty company specific resource strings
			this.resources = angular.extend({}, this.resources, resources.warrantyCompanyIndex);
			this.WarrantyCompanyModal = WarrantyCompanyModal;
			// set the refresh API call
			this.refreshCall = api.warrantyCompany.all;
			// set the delete API call
			this.deleteCall = api.warrantyCompany.destroy;
			// set the name property used in the delete message
			this.nameProperty = "name";
			this.maxSize = 10;
			this.sortOptions = [{
				orderBy: "warranty_companies.created_at",
				order: "desc",
				desc: this.resources.creationDate + " - " + this.resources.descending
			}, {
				orderBy: "warranty_companies.created_at",
				order: "asc",
				desc: this.resources.creationDate + " - " + this.resources.ascending
			}, {
				orderBy: "warranty_companies.name",
				order: "desc",
				desc: this.resources.binderName + " - " + this.resources.descending
			}, {
				orderBy: "warranty_companies.name",
				order: "asc",
				desc: this.resources.binderName + " - " + this.resources.ascending
			}];
			this.sortOption = this.sortOptions[0];
			this.orderBy = this.sortOption.orderBy;
			this.order = this.sortOption.order;
			this.headers = null;
			this.setHeaders();
			this.refresh();
		};

		Model.prototype = Object.create(IndexBase.prototype);

		angular.extend(Model.prototype, {
			setHeaders: function() {
				this.columnspan = 5;
				this.headers = this.resources.warrantyCompanyAttributes.map(angular.bind(this, function(attribute) {
					var sortable = false,
						orderBy = null,
						sorted = false,
						show = true;
					if (attribute === "ID") {
						sorted = true;
						sortable = true;
						orderBy = "warranty_companies.id";
					}
					else if (attribute === "Name") {
						sortable = true;
						orderBy = "warranty_companies.name";
					}
					else if (attribute === "Contact") {
						sortable = true;
						orderBy = "warranty_companies.created_at";
					}
					else if (attribute === "Email") {
						sortable = true;
						orderBy = "warranty_companies.email";
					}
					return {
						name: attribute,
						sortable: sortable,
						sorted: sorted,
						orderBy: orderBy,
						show: show,
						order: "desc"
					};
				}));
				this.sortOption = this.headers[0];
			},

			newItem: function() {
				var formOpts = {};
				formOpts.onSaved = angular.bind(this, this.onAdded);
				this.WarrantyCompanyModal.showForm(formOpts);
			},

			editItem: function(item) {
				var formOpts = {};
				formOpts.warrantyCompany = item;
				var index = this.items.indexOf(item);
				formOpts.onSaved = angular.bind(this, this.onEdited, index);
				this.WarrantyCompanyModal.showForm(formOpts);
			},

			onEdited: function(index, item) {
				this.items[index] = item;
			}
		});

		this.model = new Model();
	}

})();