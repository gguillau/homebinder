describe("WarrantiesCompaniesIndexCtrl", function() {
    var controller,
        base,
        api,
        resources,
        WarrantyCompanyModal;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        WarrantyCompanyModal = $injector.get("WarrantyCompanyModal");

        spyOn(api.warrantyCompany, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));
        spyOn(api.warrantyPlan, "destroy").and.returnValue($q.when({}));

        controller = $controller("WarrantiesCompaniesIndexCtrl", {
            "hb.framework.indexBase": base,
            "hb.api": api,
            "hb.resources": resources,
            "WarrantyCompanyModal": WarrantyCompanyModal
        });

        $rootScope.$apply();
    }));

    describe("init", function() {
        it("sets attributes", function() {
            expect(controller.model.maxSize).toBe(10);
            expect(controller.model.sortOptions.length).toBe(4);
        });
    });

    describe("refresh", function() {
        it("gets the warranties", function() {
            expect(api.warrantyCompany.all).toHaveBeenCalled();
            expect(controller.model.columnspan).toBe(5);
            expect(controller.model.headers.length).toBe(4);
        });
    });

    describe("newItem", function() {
        it("calls modal", function() {
            spyOn(WarrantyCompanyModal, "showForm");
            controller.model.newItem();
            expect(WarrantyCompanyModal.showForm).toHaveBeenCalledWith({
                onSaved: jasmine.any(Function)
            });
        });
    });


    describe("editItem", function() {
        it("calls modal", function() {
            spyOn(WarrantyCompanyModal, "showForm");
            controller.model.editItem({
                id: 1,
                name: "name"
            });
            expect(WarrantyCompanyModal.showForm).toHaveBeenCalledWith({
                warrantyCompany: {
                    id: 1,
                    name: 'name'
                },
                onSaved: jasmine.any(Function)
            });
        });
    });

    describe("onEdited", function() {
        it("inserts item into array", function() {
            expect(controller.model.items[0].name).toBe("name");
            spyOn(WarrantyCompanyModal, "showForm");
            controller.model.onEdited(0, {
                name: "test"
            });
            expect(controller.model.items[0].name).toBe("test");
        });
    });
});

describe('warrantyCompanies', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_, _$q_) {
        api = _$injector_.get("hb.api");
        $q = _$q_;
        spyOn(api.warrantyCompany, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));
        $scope = $rootScope.$new(), $compile = _$compile_;

        $compile('<warranty-companies></warranty-companies>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the title', function() {
            expect(api.warrantyCompany.all).toHaveBeenCalled();
        });
    });
});