(function() {
	"use strict";

	angular
		.module("hb.admin", [
			"ui.router",
			"ui.bootstrap",
			"selectize",
			"hb.components",
			"hb.admin.kpi",
			"hb.admin.users",
			"hb.admin.binders",
			"hb.admin.bindersItems",
			"hb.admin.partners",
			"hb.admin.organizations",
			"hb.admin.recalls",
			"hb.admin.settings",
			"hb.admin.transactions",
			"hb.admin.widgets",
			"hb.admin.dashboards",
			"hb.admin.errors",
			"hb.admin.reports",
			"hb.admin.warranties",
			"hb.admin.buildfax",
			"hb.admin.binderLibrary",
			"hb.admin.welcome",
			"hb.admin.contractors"
		])
		.config(['$stateProvider', adminConfig])
		.controller("AdminCtrl", [
			"$state",
			"$window",
			"User",
			'hb.userRole',
			'Session',
			AdminCtrl
		]);

	function AdminCtrl($state, $window, User, userRole, session) {
		this.headerCfg = {
			title: "Administration",
			canSearch: true,
			toolbar: undefined
		};
		this.currentUser = session.getUser();
		this.init();
	}

	AdminCtrl.prototype = {
		init: function() {
			if (this.currentUser.role === "admin") {
				this.navCfg = {
					visible: true,
					navList: [{
						id: "kpi",
						state: "admin.kpi",
						label: "KPIs",
						icon: "glyphicon glyphicon-stats"
					}, {
						id: "users",
						state: "admin.users",
						label: "Users",
						icon: "glyphicon glyphicon-user"
					}, {
						id: "binders",
						state: "admin.binders",
						label: "Binders",
						icon: "glyphicon glyphicon-home"
					}, {
						id: "partners",
						state: "admin.partners",
						label: "Partners",
						icon: "glyphicon glyphicon-user"
					}, {
						id: "recalls",
						state: "admin.recalls",
						label: "Recalls",
						icon: "glyphicon glyphicon-bullhorn"
					}, {
						id: "settings",
						state: "admin.settings",
						label: "Settings",
						icon: "glyphicon glyphicon-wrench"
					}, {
						id: "organizations",
						state: "admin.organizations",
						label: "Organizations",
						icon: "glyphicon glyphicon-briefcase"
					}, {
						id: "widgets",
						state: "admin.widgets",
						label: "Widgets",
						icon: "glyphicon glyphicon-th-large"
					}, {
						id: "dashboards",
						state: "admin.dashboards",
						label: "Dashboards",
						icon: "glyphicon glyphicon-th"
					}, {
						id: "errors",
						state: "admin.errors",
						label: "ABS Errors",
						icon: "glyphicon glyphicon-list"
					}, {
						id: "bindersItems",
						state: "admin.bindersItems",
						label: "BindersItems",
						icon: "glyphicon glyphicon-list"
					}, {
						id: "reports",
						state: "admin.reports",
						label: "Reports",
						icon: "glyphicon glyphicon-time"
					}, {
						id: "warranties",
						state: "admin.warranties",
						label: "Warranties",
						faIcon: "fa fa-shield"
					}, {
						id: "library",
						state: "admin.binderLibrary",
						label: "Binder Library",
						faIcon: "glyphicon glyphicon-book"
					}, {
						id: "contractors",
						state: "admin.contractors",
						label: "Home Pros",
						faIcon: "glyphicon glyphicon-header"
					}, {
						id: "transactions",
						state: "admin.transactions",
						label: "Transactional Billing",
						faIcon: "glyphicon glyphicon-usd"
					}]
				};
			}
			else {
				this.navCfg = {
					visible: true,
					navList: [{
						id: "users",
						state: "admin.users",
						label: "Users",
						icon: "glyphicon glyphicon-user"
					}, {
						id: "binders",
						state: "admin.binders",
						label: "Binders",
						icon: "glyphicon glyphicon-home"
					}, {
						id: "partners",
						state: "admin.partners",
						label: "Partners",
						icon: "glyphicon glyphicon-user"
					}]
				};
			}
		}
	};

	function adminConfig($stateProvider) {
		$stateProvider
			.state("admin", {
				url: "/admin/",
				templateUrl: "admin/admin.tpl.html",
				controller: "AdminCtrl",
				controllerAs: "admin",
				data: {
					title: "Not set",
					toolbar: {
						buttonGroups: [],
						searchBox: true
					}
				}
			});
	}
})();
