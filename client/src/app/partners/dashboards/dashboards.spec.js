describe("HBPartnerDashboardsController", function() {
    var controller,
        DashboardController,
        api,
        $stateParams;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });

        // mock the Notify service
        $provide.factory("Notify", function() {
            return {
                info: function() {},
                error: function() {}
            };
        });

        // mock the Session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return {
                        role: "admin"
                    };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        DashboardController = $injector.get("DashboardController");
        api = $injector.get("hb.api");
        $stateParams = $injector.get("$stateParams");
        $stateParams.partnerId = 100;

        spyOn(api.dashboard, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));

        controller = $controller("HBPartnerDashboardsController", {
            "DashboardController": DashboardController,
            "hb.api": api,
            "$stateParams": $stateParams
        });

        $rootScope.$apply();
    }));

    describe("init", function() {
        it("sets attributes", function() {
            expect(controller.model.cfg.partnerId).toEqual(100);
        });
    });

});