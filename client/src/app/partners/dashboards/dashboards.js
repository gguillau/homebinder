(function() {
    "use strict";

    angular.module('hb.partner.dashboards', [])
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state('partner.dashboards', {
                    url: '^/partners/:partnerId/dashboards',
                    templateUrl: "dashboards/dashboards.tpl.html",
                    controller: "HBPartnerDashboardsController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .controller("HBPartnerDashboardsController", [
            'DashboardController',
            '$stateParams',
            Controller
        ]);

    function Controller(DashboardController, $stateParams) {
        var Model = function() {
            DashboardController.call(this);
            this.cfg = {
                partnerId: $stateParams.partnerId
            };
            this.queryArgs = {
                partnerId: $stateParams.partnerId
            };
            this.refresh();
        };

        Model.prototype = Object.create(DashboardController.prototype);

        this.model = new Model();
    }
})();