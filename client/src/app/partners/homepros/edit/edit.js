(function() {
    "use strict";

    angular
        .module("hb.partner.homepros.edit", [])
        .controller("PartnerHomeProsEditController", [
            'PartnerTemplatesItemsEditModalController',
            "hb.resources",
            'data',
            "$modalInstance",
            "hb.api",
            "Notify",
            "$filter",
            "Context",
            "Address",
            "$log",
            "HomeProSelect",
            PartnerHomeProsEditController
        ]);

    function PartnerHomeProsEditController(PartnerTemplatesItemsEditModalController, resources, opts, $modal, api, notify, $filter, context, Address, $log, HomeProSelect) {

        var Model = function() {
            // call the parent class
            PartnerTemplatesItemsEditModalController.call(this);
            // set the resources
            this.resources = resources.partnerTemplatesContractorsEdit;
            // the item being updated/created
            this.item = opts.item;
            this.partner_contractor_types = [];
            this.partner_contractor_sub_types = [];
            this.filtered_types = [];
            this.filtered_sub_types = [];
            this.filtered_partner_contractor_types = [];
            this.filtered_partner_contractor_sub_types = [];
            // the call that updates the item in the list view
            this.onSave = opts.onSave;
            // the index of the item in the items list
            this.index = opts.index;
            // the createCall
            this.createCall = api.partnerContractor.create;
            // the updateCall
            this.updateCall = api.partnerContractor.update;
            // set the modalInstance
            this.$modal = $modal;
            this.homeProLookupCfg = {
                homeProSelected: angular.bind(this, this.homeProSelected)
            };
            this.url_regex = /^((?:http|ftp)s?:\/\/)(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|localhost|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(?::\d+)?(?:\/?|[\/?]\S+)$/i;
            // get the current partner
            this.init();
        };

        Model.prototype = Object.create(PartnerTemplatesItemsEditModalController.prototype);

        angular.extend(Model.prototype, {
            homeProSelected: function(pro) {
                this.item.name = pro.name;
                this.item.email = pro.email;
                this.item.contractor_id = pro.id;
            },

            searchTypes: function(value) {
                // don't do the search when no search value is provided just clear the list
                if (value === null ||
                    value === undefined ||
                    value.length < 3) {
                    this.typeOptions = [];
                    return;
                }

                var opts = {
                    page: 1,
                    count: 50,
                    search: value,
                    verified: true
                };

                api.contractorType.all(opts).then(
                    angular.bind(this, this.searchComplete),
                    angular.bind(this, this.searchError)
                );
            },

            searchComplete: function(response) {
                this.typeOptions = response.data.items.filter(angular.bind(this, function(item) {
                    var array = $filter("filter")(this.item.partner_contractor_types, {
                        type: {
                            name: item.name
                        }
                    });
                    return array.length < 1;
                }));
            },

            searchError: function(response) {
                notify.error(response.data);
            },

            onTypeSelect: function($select, $item) {
                // remove the item first and then add back again
                var index = this.item.partner_contractor_types.indexOf($item);
                if (index > -1) {
                    this.item.partner_contractor_types.splice(index, 1);
                }

                var hash = this.findExisting($item);
                if (hash.index < 0) {
                    if (hash.item.id) {
                        this.item.partner_contractor_types.push({ type: { id: hash.item.id, name: hash.item.name } });
                        hash.item.sub_types.forEach(angular.bind(this, function(sub_type) {
                            this.onSubTypeSelect({}, sub_type);
                        }));
                    }
                    else {
                        this.item.partner_contractor_types.push({ type: { name: hash.item.name } });
                    }
                }
            },

            onTypeRemove: function($item) {
                if (!this.item.partner_contractor_types) {
                    this.item.partner_contractor_types = [];
                }
                var hash = this.findExisting($item);
                if (hash.index > -1) {
                    this.item.partner_contractor_types.splice(hash.index, 1);
                }
            },

            tagTransform: function(newTag) {
                var item = {
                    name: newTag,
                    sub_types: []
                };

                return item;
            },

            searchSubTypes: function(value) {
                // don't do the search when no search value is provided just clear the list
                if (value === null ||
                    value === undefined ||
                    value.length < 3) {
                    this.subTypeOptions = [];
                    return;
                }

                var opts = {
                    page: 1,
                    count: 50,
                    search: value
                };

                api.contractorSubType.all(opts).then(
                    angular.bind(this, this.searchSubComplete),
                    angular.bind(this, this.searchError)
                );
            },

            searchSubComplete: function(response) {
                this.subTypeOptions = response.data.items.filter(angular.bind(this, function(item) {
                    var array = $filter("filter")(this.item.partner_contractor_sub_types, {
                        sub_type: {
                            name: item.name
                        }
                    });
                    return array.length < 1;
                }));
            },

            onSubTypeSelect: function($select, $item) {
                //remove the item first and then add back again
                var index = this.item.partner_contractor_sub_types.indexOf($item);
                if (index > -1) {
                    this.item.partner_contractor_sub_types.splice(index, 1);
                }

                var hash = this.findExistingSubTypes($item);
                if (hash.index < 0) {
                    if (hash.item.id) {
                        this.item.partner_contractor_sub_types.push({ sub_type: { id: hash.item.id, name: hash.item.name } });
                    }
                    else {
                        this.item.partner_contractor_sub_types.push({ sub_type: { name: hash.item.name } });
                    }
                }
            },

            onSubTypeRemove: function($item) {
                var hash = this.findExistingSubTypes($item);
                if (hash.index > -1) {
                    this.item.partner_contractor_sub_types.splice(hash.index, 1);
                }
            },

            findExisting: function(item) {
                var name = null;
                if (!item.id && !item.isTag) {
                    name = item.type.name.toLowerCase();
                    item = { name: name };
                }
                return { index: this.item.partner_contractor_types.indexOf(item), item: item };
            },

            findExistingSubTypes: function(item) {
                var name = null,
                    index = null;
                if (!item.id && !item.isTag) {
                    name = item.sub_type.name.toLowerCase();
                    item = { name: name };
                }

                var array = $filter("filter")(this.item.partner_contractor_sub_types, {
                    sub_type: {
                        name: item.name
                    }
                }, true);

                if (array.length > 0) {
                    index = 0;
                }
                else {
                    index = -1;
                }

                return { index: index, item: item };
            },

            init: function() {
                this.address = Address;
                context.getPartner().then(angular.bind(this, function(partner) {
                    this.partner = partner;
                    this.country = Address.findCountryByCode(this.partner.address.country);
                    this.states = Address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                        return state.country.value === this.country.value;
                    }));
                }));
            },

            beforeSave: function() {
                if(!this.item.address){
                    this.item.address = {};
                }
                
                this.filtered_types = this.item.partner_contractor_types.filter(function(partner_contractor_type) {
                    return !partner_contractor_type.id && !partner_contractor_type.type.id;
                });

                this.filtered_partner_contractor_types = this.item.partner_contractor_types.filter(function(partner_contractor_type) {
                    return partner_contractor_type.id || partner_contractor_type.type.id;
                });

                this.filtered_sub_types = this.item.partner_contractor_sub_types.filter(function(partner_contractor_sub_type) {
                    return !partner_contractor_sub_type.id && !partner_contractor_sub_type.sub_type.id;
                });

                this.filtered_partner_contractor_sub_types = this.item.partner_contractor_sub_types.filter(function(partner_contractor_sub_type) {
                    return partner_contractor_sub_type.id || partner_contractor_sub_type.sub_type.id;
                });

                this.data = {
                    name: this.item.name,
                    partner_id: this.partner.id,
                    phone: "+" + this.country.code + this.item.phone.national,
                    website: this.item.website,
                    email: this.item.email,
                    notes: this.item.notes,
                    partner_contractor_types_attributes: this.filtered_partner_contractor_types.map(function(partner_contractor_type) {
                        if (partner_contractor_type.id) {
                            return { id: partner_contractor_type.id };
                        }
                        else if (partner_contractor_type.type.id) {
                            return { contractor_category_id: partner_contractor_type.type.id };
                        }
                    }),
                    types_attributes: this.filtered_types.map(function(partner_contractor_type) {
                        if (!partner_contractor_type.id && !partner_contractor_type.type.id) {
                            return { name: partner_contractor_type.type.name };
                        }
                    }),
                    partner_contractor_sub_types_attributes: this.filtered_partner_contractor_sub_types.map(function(partner_contractor_sub_type) {
                        if (partner_contractor_sub_type.id) {
                            return { id: partner_contractor_sub_type.id };
                        }
                        else if (partner_contractor_sub_type.sub_type.id) {
                            return { contractor_sub_category_id: partner_contractor_sub_type.sub_type.id };
                        }
                    }),
                    sub_types_attributes: this.filtered_sub_types.map(function(partner_contractor_sub_type) {
                        if (!partner_contractor_sub_type.id && !partner_contractor_sub_type.sub_type.id) {
                            return { name: partner_contractor_sub_type.sub_type.name };
                        }
                    }),
                    send_email_invitation: this.item.send_email_invitation,
                    contractor_id: this.item.contractor_id,
                    secondary_name: this.item.secondary_name,
                    secondary_email: this.item.secondary_email,
                    secondary_phone: "+" + this.country.code + this.item.secondary_phone.national,
                    secondary_notes: this.item.secondary_notes,
                    address_attributes: {
                        id: this.item.address.id,
                        address1: this.item.address.address1,
                        address2: this.item.address.address2,
                        city: this.item.address.city,
                        state: this.item.address.state,
                        zip: this.item.address.zip,
                        country: this.country.value
                    }
                };

                if (this.item && !this.item.id && !this.item.contractor_id) {
                    this.data.contractor_attributes = {
                        name: this.item.name,
                        url: this.item.website,
                        phone: "+" + this.country.code + this.item.phone.national,
                        email: this.item.email,
                        created_by: this.currentUser.id,
                        contractor_types_attributes: this.filtered_partner_contractor_types.map(function(partner_contractor_type) {
                            if (partner_contractor_type.id) {
                                return { id: partner_contractor_type.id };
                            }
                            else if (partner_contractor_type.type.id) {
                                return { contractor_category_id: partner_contractor_type.type.id };
                            }
                        }),
                        types_attributes: this.filtered_types.map(function(partner_contractor_type) {
                            if (!partner_contractor_type.id && !partner_contractor_type.type.id) {
                                return { name: partner_contractor_type.type.name };
                            }
                        }),
                        contractor_sub_types_attributes: this.filtered_partner_contractor_sub_types.map(function(partner_contractor_sub_type) {
                            if (partner_contractor_sub_type.id) {
                                return { id: partner_contractor_sub_type.id };
                            }
                            else if (partner_contractor_sub_type.sub_type.id) {
                                return { contractor_sub_category_id: partner_contractor_sub_type.sub_type.id };
                            }
                        }),
                        sub_types_attributes: this.filtered_sub_types.map(function(partner_contractor_sub_type) {
                            if (!partner_contractor_sub_type.id && !partner_contractor_sub_type.sub_type.id) {
                                return { name: partner_contractor_sub_type.sub_type.name };
                            }
                        }),
                        address_attributes: {
                            address1: this.item.address.address1,
                            address2: this.item.address.address2,
                            city: this.item.address.city,
                            state: this.item.address.state,
                            country: this.country.value
                        }
                    };
                }
            },

            checkForExisting: function() {
                api.contractor.all({ count: 5, searchType: "fuzzy", name: this.item.name.toLowerCase(), email: this.item.email.toLowerCase(), phone: "+" + this.country.code + this.item.phone.national }).then(
                    angular.bind(this, this.existingSuccess),
                    angular.bind(this, this.existingError));
            },

            existingSuccess: function(response) {
                var email = this.item.email ? this.item.email.toLowerCase() : "";
                var contractors = response.data.items;
                if (contractors.length > 0) {
                    HomeProSelect.show({
                        contractors: contractors,
                        email: email,
                        closed: angular.bind(this, this.setHomePro)
                    });
                }
                else {
                    this.createOrUpdate();
                }
            },

            existingError: function(response) {
                $log.error(response);
                notify.error(response.data);
            },

            setHomePro: function(pro) {
                if (pro) {
                    this.homeProSelected(pro);
                    this.createOrUpdate();
                }
                else {
                    this.createOrUpdate();
                }
            },

            save: function() {
                this.form.$submitted = true;
                if (this.form.$invalid) {
                    return;
                }

                if (this.item && this.item.id) {
                    this.createOrUpdate();
                }
                else {
                    // check for existing home pro
                    this.checkForExisting();
                }
            },

            createOrUpdate: function() {

                this.beforeSave();

                if (this.item && this.item.id) {
                    this.updateCall(this.item.id, this.data).then(
                        angular.bind(this, this.onSaveSuccess),
                        angular.bind(this, this.onError)
                    );
                }
                else {
                    this.createCall(this.data).then(
                        angular.bind(this, this.onSaveSuccess),
                        angular.bind(this, this.onError)
                    );
                }
            }
        });

        this.model = new Model();
    }

})();