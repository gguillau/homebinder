(function() {
    "use strict";

    angular
        .module("hb.partner.homepros", [
            "hb.partner.homepros.edit"
        ])
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state('partner.homepros', {
                    url: '^/partners/:partnerId/homepros',
                    templateUrl: "partners/homepros/index.tpl.html",
                    controller: "PartnerHomeProsController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .controller("PartnerHomeProsController", [
            "hb.framework.indexBase",
            "hb.resources",
            "Session",
            "$stateParams",
            "hb.api",
            "PartnerTemplatesItemsEditModal",
            PartnerHomeProsController
        ]);

    function PartnerHomeProsController(IndexBase, resources, session, $stateParams, api, PartnerTemplatesItemsEditModal) {
        var Model = function() {
            // call the parent class
            IndexBase.call(this);
            this.currentUser = session.getUser();
            this.resources = angular.extend({}, this.resources, resources.partnerProsIndex);
            this.itemsPerPage = 10;
            this.proArgs = {
                partner_id: $stateParams.partnerId
            };
            // delete prop
            this.nameProperty = "name";
            this.sortOptions = [{
                orderBy: "partner_contractors.created_at",
                order: "desc",
                desc: this.resources.creationDate + " - " + this.resources.descending
            }, {
                orderBy: "partner_contractors.created_at",
                order: "asc",
                desc: this.resources.creationDate + " - " + this.resources.ascending
            }, {
                orderBy: "partner_contractors.name",
                order: "desc",
                desc: this.resources.name + " - " + this.resources.descending
            }, {
                orderBy: "partner_contractors.name",
                order: "asc",
                desc: this.resources.name + " - " + this.resources.ascending
            }];
            this.sortOption = this.sortOptions[0];
            this.orderBy = this.sortOption.orderBy;
            this.order = this.sortOption.order;
            // set the refresh API call
            this.refreshCall = api.partnerContractor.all;
            // set the delete API call
            this.deleteCall = api.partnerContractor.destroy;
            this.setHeaders();
            this.refresh();
        };

        Model.prototype = Object.create(IndexBase.prototype);

        angular.extend(Model.prototype, {

            setHeaders: function() {
                this.headers = this.resources.tableAttributes.map(angular.bind(this, function(attribute) {
                    var sortable = false,
                        orderBy = null,
                        sorted = false;
                    if (attribute === "ID") {
                        sorted = true;
                        sortable = true;
                        orderBy = "partner_contractors.id";
                    }
                    else if (attribute === "Name") {
                        sortable = true;
                        orderBy = "partner_contractors.name";
                    }
                    else if (attribute === "Email") {
                        sortable = true;
                        orderBy = "partner_contractors.email";
                    }
                    return {
                        name: attribute,
                        sortable: sortable,
                        sorted: sorted,
                        orderBy: orderBy,
                        order: "desc"
                    };
                }));
                this.headerOptions = angular.copy(this.headers);
                this.sortOption = this.headers[0];
            },

            addQueryArgs: function() {
                this.addArgs("partner_id");
            },

            addArgs: function(arg) {
                if (this.proArgs[arg]) {
                    this.queryArgs[arg] = this.proArgs[arg];
                }
                else {
                    delete this.queryArgs[arg];
                }
            },

            // send user to new item view
            newItem: function() {
                var item = {
                    name: "",
                    email: "",
                    notes: "",
                    partner_contractor_types: [],
                    partner_contractor_sub_types: [],
                    address: {},
                    phone: {},
                    secondary_name: "",
                    secondary_email: "",
                    secondary_phone: {},
                    send_email_invitation: true
                };

                PartnerTemplatesItemsEditModal.show({
                    templateUrl: "partners/homepros/edit/edit.tpl.html",
                    controller: "PartnerHomeProsEditController as ctrl",
                    item: item,
                    index: -1,
                    onSave: angular.bind(this, this.refresh)
                });
            },

            edit: function(item) {
                var index = this.items.indexOf(item);
                PartnerTemplatesItemsEditModal.show({
                    templateUrl: "partners/homepros/edit/edit.tpl.html",
                    controller: "PartnerHomeProsEditController as ctrl",
                    item: item,
                    index: index,
                    onSave: angular.bind(this, this.refresh)
                });
            }
        });

        this.model = new Model();
    }

})();