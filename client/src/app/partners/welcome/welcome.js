(function() {
	"use strict";

	angular
		.module("hb.partner.welcome", [
			"hb.components"
		])
		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("partnersWelcome", {
					url: "^/partners/:accessToken/welcome",
					views: {
						'@': {
							templateUrl: "components/users/welcome/welcome.tpl.html",
							controller: "PartnersWelcomeController",
							controllerAs: "ctrl"
						}
					}
				});
		}])
		.controller("PartnersWelcomeController", [
			"UsersWelcomeController",
			PartnersWelcomeController
		]);

	function PartnersWelcomeController(UsersWelcomeController) {
		var Model = function() {
			// call the parent class
			UsersWelcomeController.call(this);
			this.transferText = this.resources.choosePassword;
			this.submitLabel = this.resources.getStarted;
			this.refresh();
		};

		Model.prototype = Object.create(UsersWelcomeController.prototype);

		angular.extend(Model.prototype);

		this.model = new Model();
	}
})();