describe("PartnersWelcomeController", function() {
    var controller,
        base,
        resources,
        $q_;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getJwt: function() {},
                getLocale: function() {
                    return null;
                },
                setLocale: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        $q_ = $q;
        base = $injector.get("UsersWelcomeController");
        resources = $injector.get("hb.resources");

        controller = $controller("PartnersWelcomeController", {
            "UsersWelcomeController": base,
            "hb.resources": resources
        });
    }));

    describe("refresh", function() {
        it("sets the controller api call", function() {
            spyOn(controller.model.api.user, "welcome").and.returnValue($q_.when({ data: {} }));

            controller.model.refresh();

            expect(controller.model.api.user.welcome).toHaveBeenCalled();
        });
    });

});