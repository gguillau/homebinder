(function() {
	"use strict";

	angular
		.module("hb.partner.templates.form", [
			"ui.bootstrap",
			"hb.components",
			"hb.partner.templates.form.maintenanceItems",
			"hb.partner.templates.form.appliances",
			"hb.partner.templates.form.contractors",
			"hb.partner.templates.form.documents",
			"hb.partner.templates.form.editModal"
		])
		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("partner.templates_form", {
					url: "^/partners/:partnerId/templates/:templateId",
					template: "<partner-templates-form></partner-templates-form>",
					access: {
						requiresLogin: true
					}
				});
		}])
		.directive("partnerTemplatesForm", function() {
			return {
				restrict: "E",
				templateUrl: "partners/templates/form/form.tpl.html",
				controller: "PartnerTemplatesFormController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("PartnerTemplatesFormController", [
			"$stateParams",
			"$log",
			"hb.api",
			"Notify",
			"Session",
			"Loading",
			"hb.resources",
			PartnerTemplatesFormController
		]);

	function PartnerTemplatesFormController($stateParams, $log, api, notify, Session, loading, resources) {
		this.$stateParams = $stateParams;
		this.$log = $log;
		this.api = api;
		this.notify = notify;
		this.partnerId = this.$stateParams["partnerId"];
		this.templateId = this.$stateParams["templateId"];
		this.loading = loading;
		this.resources = resources.partnerTemplatesForm;
		this.items = [];
		this.temp_items = [];
		this.appliances = [];
		this.temp_appliances = [];
		this.contractors = [];
		this.temp_contractors = [];
		this.documents = [];
		this.temp_documents = [];
		this.docUploadCfg = {};
		this.transfer_note = null;
		this.templateName = this.current_name = null;
		this.maintForm = {};
		this.appForm = {};
		this.contractForm = {};
		this.docForm = {};
		this.note = this.resources.defaultNote;
		this.read_only = true;
		this.refresh();
	}

	PartnerTemplatesFormController.prototype = {

		refresh: function() {
			this.loading.show(this.resources.loading);
			this.api.binderTemplate.get(this.templateId).then(
				angular.bind(this, this.getTemplateSuccess),
				angular.bind(this, this.getTemplateError));
		},

		getTemplateSuccess: function(response) {
			this.templateName = this.current_name = response.data.name;
			this.items = response.data.maintenance_templates;
			this.appliances = response.data.appliance_templates;
			this.contractors = response.data.contractor_templates;
			this.documents = response.data.document_templates;
			this.transfer_note = response.data.transfer_note ? response.data.transfer_note : this.note;
			this.loading.close();
		},

		getTemplateError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		save: function() {
			angular.copy(this.items, this.temp_items);
			angular.copy(this.appliances, this.temp_appliances);
			angular.copy(this.contractors, this.temp_contractors);
			angular.copy(this.documents, this.temp_documents);
			this.update();
		},

		update: function() {
			this.api.binderTemplate.update(this.templateId, { name: this.current_name, transfer_note: this.transfer_note }).then(
				angular.bind(this, this.updateSuccess),
				angular.bind(this, this.updateError));
		},

		updateSuccess: function(response) {
			if (this.read_only === false) {
				this.edit();
			}
			this.templateName = this.current_name = response.data.name;
			this.transfer_note = response.data.transfer_note ? response.data.transfer_note : this.note;
			this.items = response.data.maintenance_templates;
			this.appliances = response.data.appliance_templates;
			this.contractors = response.data.contractor_templates;
			this.documents = response.data.document_templates;
			this.notify.success(this.resources.updated);
			this.loading.close();
		},

		updateError: function(response) {
			if (this.read_only === false) {
				this.edit();
			}
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		edit: function() {
			this.read_only = !this.read_only;
			this.current_name = this.templateName;
		},

		editForm: function() {
			this.read_only = false;
		},

		cancelForm: function() {
			this.read_only = true;
		}

	};
})();