(function() {
	"use strict";

	angular
		.module("hb.partner.templates.form.appliances", [
			"hb.partner.templates.form.appliances.edit"
		])
		.directive("partnerTemplatesAppliances", function() {
			return {
				restrict: "E",
				templateUrl: "partners/templates/form/appliances/appliances.tpl.html",
				controller: "PartnerTemplatesAppliancesController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {
					appliances: "=",
					create: "&",
					appForm: "="
				}
			};
		})
		.controller("PartnerTemplatesAppliancesController", [
			'hb.api',
			'hb.utils',
			'Loading',
			'$log',
			'Notify',
			'$stateParams',
			'hb.resources',
			'PartnerTemplatesItemsEditModal',
			PartnerTemplatesAppliancesController
		]);

	function PartnerTemplatesAppliancesController(api, utils, loading, $log, notify, $stateParams, resources, PartnerTemplatesItemsEditModal) {
		this.templateId = $stateParams["templateId"];
		this.api = api;
		this.utils = utils;
		this.validationErrors = resources.validationErrors;
		this.$log = $log;
		this.notify = notify;
		this.loading = loading;
		this.resources = resources.partnerTemplatesAppliances;
		this.PartnerTemplatesItemsEditModal = PartnerTemplatesItemsEditModal;
		this.appliance_items = [{
			name: "Refrigerator",
			value: "Refrigerator",
			model: "",
			serial_no: ""
		}, {
			name: "Dishwasher",
			value: "Dishwasher",
			model: "",
			serial_no: ""
		}, {
			name: "Microwave",
			value: "Microwave",
			model: "",
			serial_no: ""
		}, {
			name: "Furnace",
			value: "Furnace",
			model: "",
			serial_no: ""
		}, {
			name: "Stove",
			value: "Stove",
			model: "",
			serial_no: ""
		}, {
			name: "Oven/Range",
			value: "OvenRange",
			model: "",
			serial_no: ""
		}, {
			name: "Washer",
			value: "Washer",
			model: "",
			serial_no: ""
		}, {
			name: "Dryer",
			value: "Dryer",
			model: "",
			serial_no: ""
		}, {
			name: "Water Heater",
			value: "WaterHeater",
			model: "",
			serial_no: ""
		}];
		this.is_app_collapsed = false;
	}

	PartnerTemplatesAppliancesController.prototype = {

		remove: function(app) {
			var index = this.appliances.indexOf(app);
			var appliance = this.appliances[index];
			if (appliance.id) {
				this.api.applianceTemplates.destroy(appliance.id).then(
					angular.bind(this, this.onRemoveSuccess, index),
					angular.bind(this, this.onRemoveError)
				);
			}
		},

		onRemoveSuccess: function(index, response) {
			this.appliances.splice(index, 1);
			this.notify.success(this.resources.removed);
		},

		onRemoveError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
		},

		addApp: function() {
			var appliance = {
				name: "Untitled",
				notes: "",
				library_template: false
			};

			this.PartnerTemplatesItemsEditModal.show({
				templateUrl: "partners/templates/form/appliances/edit/edit.tpl.html",
				controller: "PartnerTemplatesAppliancesEditController as ctrl",
				item: appliance,
				cfg: { showTypeaheads: true },
				index: -1,
				onSave: angular.bind(this, this.insertAppliance)
			});
		},

		edit: function(appliance) {
			var index = this.appliances.indexOf(appliance);
			this.PartnerTemplatesItemsEditModal.show({
				templateUrl: "partners/templates/form/appliances/edit/edit.tpl.html",
				controller: "PartnerTemplatesAppliancesEditController as ctrl",
				item: appliance,
				cfg: { showTypeaheads: true },
				index: index,
				onSave: angular.bind(this, this.insertAppliance)
			});
		},

		insertAppliance: function(item, index) {
			if (index > -1) {
				this.appliances[index] = item;
			}
			else {
				this.appliances.push(item);
			}
		}
	};

})();