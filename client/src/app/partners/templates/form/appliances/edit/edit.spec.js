describe("PartnerTemplatesAppliancesEditController", function() {
    var $rootScope,
        $controller,
        PartnerTemplatesItemsEditModalController,
        $modalInstance,
        ctrl,
        api,
        $q,
        $log;


    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the $modalInstance service
        $provide.factory("$modalInstance", function() {
            return {
                close: function() {},
                dismiss: function() {}
            };
        });
    }));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        PartnerTemplatesItemsEditModalController = _$injector_.get("PartnerTemplatesItemsEditModalController");
        $modalInstance = _$injector_.get("$modalInstance");
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        $log = _$injector_.get("$log");
    }));

    function createController() {
        spyOn(api.applianceTemplates, "all").and.returnValue($q.when({ data: { items: [] } }));
        ctrl = $controller("PartnerTemplatesAppliancesEditController", {
            "PartnerTemplatesItemsEditModalController": PartnerTemplatesItemsEditModalController,
            "data": { item: {}, cfg: {} },
            "$modalInstance": $modalInstance
        });
        $rootScope.$apply();
    }

    describe('loadLibrary', function() {
        it("calls api applianceTemplates all", function() {
            createController();
            ctrl.model.loadLibrary();

            expect(api.applianceTemplates.all).toHaveBeenCalled();
        });
    });

    describe('onLoadApplianceTemplates', function() {
        it("sets the templates", function() {
            createController();
            ctrl.model.onLoadApplianceTemplates({ data: { items: [{ id: 1 }] } });

            expect(ctrl.model.appliance_templates.length).toEqual(1);
        });
    });

    describe('onLoadApplianceTemplatesError', function() {
        it("calls $log error", function() {
            createController();
            spyOn($log, "error");
            ctrl.model.onLoadApplianceTemplatesError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
        });
    });

    describe('onSelect', function() {
        it("sets the $item", function() {
            createController();

            ctrl.model.onSelect({ name: "name", notes: "notes", description: "description", library_source_id: 1 }, {}, {});

            expect(ctrl.model.item.name).toEqual("name");
            expect(ctrl.model.item.notes).toEqual("notes");
            expect(ctrl.model.item.description).toEqual("description");
            expect(ctrl.model.item.library_source_id).toEqual(1);
        });
    });

    describe('beforeSave', function() {
        it("sets the data object", function() {
            createController();
            ctrl.model.item = { name: "test" };
            ctrl.model.binder_template = { id: 1, system: true };
            ctrl.model.beforeSave();

            expect(ctrl.model.data.name).toEqual("test");
        });
    });

});