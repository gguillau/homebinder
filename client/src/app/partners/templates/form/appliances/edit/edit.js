(function() {
    "use strict";

    angular
        .module("hb.partner.templates.form.appliances.edit", [])
        .controller("PartnerTemplatesAppliancesEditController", [
            'PartnerTemplatesItemsEditModalController',
            "hb.resources",
            'data',
            "$modalInstance",
            "hb.api",
            "$log",
            PartnerTemplatesAppliancesEditController
        ]);

    function PartnerTemplatesAppliancesEditController(PartnerTemplatesItemsEditModalController, resources, opts, $modal, api, $log) {
        var Model = function() {
            // call the parent class
            PartnerTemplatesItemsEditModalController.call(this);
            // set the resources
            this.resources = resources.partnerTemplatesAppliancesEdit;
            // the item being updated/created
            this.item = opts.item;
            // the call that updates the item in the list view
            this.onSave = opts.onSave;
            // the index of the item in the items list
            this.index = opts.index;
            // whether we should show typeheads
            this.showTypeaheads = opts.cfg.showTypeaheads;
            // the createCall
            this.createCall = api.applianceTemplates.create;
            // the updateCall
            this.updateCall = api.applianceTemplates.update;
            // set the modalInstance
            this.$modal = $modal;
            // load typeheads
            this.loadLibrary();
            // init the controller
            this.init();
        };

        Model.prototype = Object.create(PartnerTemplatesItemsEditModalController.prototype);

        angular.extend(Model.prototype, {

            loadLibrary: function() {
                api.applianceTemplates.all({ library_template: true, system: true, "default": true, count: 1000 }).then(
                    angular.bind(this, this.onLoadApplianceTemplates),
                    angular.bind(this, this.onLoadApplianceTemplatesError)
                );
            },

            onLoadApplianceTemplates: function(response) {
                this.appliance_templates = response.data.items;
            },

            onLoadApplianceTemplatesError: function(response) {
                $log.error(response);
            },

            onSelect: function($item, $model, label) {
                this.item.name = $item.name;
                this.item.notes = $item.notes;
                this.item.description = $item.description;
                this.item.library_source_id = $item.library_source_id;
            },

            beforeSave: function() {
                this.data = {
                    binder_template_id: this.binder_template ? this.binder_template.id : this.templateId,
                    name: this.item.name,
                    notes: this.item.notes,
                    description: this.item.description,
                    library_source_id: this.item.library_source_id,
                    image: this.item.image,
                    image_file_name: this.item.image_file_name
                };

                if (this.binder_template && this.binder_template.system) {
                    this.data.library_template = true;
                }
            }
        });

        this.model = new Model();
    }


})();