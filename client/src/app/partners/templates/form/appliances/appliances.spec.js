describe("PartnerTemplatesAppliancesController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $q,
        api,
        appliance_items,
        loading,
        notify,
        $log,
        $compile,
        $templateCache,
        template,
        PartnerTemplatesItemsEditModal;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$compile_, _$templateCache_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        PartnerTemplatesItemsEditModal = _$injector_.get("PartnerTemplatesItemsEditModal");
        $compile = _$compile_;
        $templateCache = _$templateCache_;

        $log = {
            error: function() {}
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        notify = {
            success: function(msg) {},
            error: function(msg) {}
        };

        api = {
            applianceManufacturer: {
                all: function() {}
            },
            applianceTemplates: {
                destroy: function(appId) {}
            }
        };

        appliance_items = [
            { id: 1, name: "Refrigerator" },
            { id: 2, name: "Dishwasher" },
            { id: 3, name: "Microwave" },
            { name: "Ventilator" },
            { name: "Stove" },
            { name: "Oven/Range" },
            { name: "Disposal" },
            { name: "Washer" },
            { name: "Dryer" },
            { name: "Water Heater" }
        ];

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("PartnerTemplatesAppliancesController", {
            "hb.api": api,
            "Loading": loading,
            "$log": $log,
            "Notify": notify,
            "PartnerTemplatesItemsEditModal": PartnerTemplatesItemsEditModal
        });

        $scope.ctrl = ctrl;
        template = $templateCache.get('partners/templates/form/appliances/appliances.tpl.html');
        $compile(template)($scope);
        appForm = $scope.ctrl.clientForm;
        $scope.$apply();
        $scope.$digest();

    }

    describe('ctrl.remove', function() {

        it('should remove appliance at index 1 from appliances array', function() {

            spyOn(api.applianceTemplates, "destroy").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(notify, "success");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            ctrl.appliances = appliance_items;
            $rootScope.$apply();
            ctrl.remove(ctrl.appliances[0]);
            $rootScope.$apply();

            expect(api.applianceTemplates.destroy).toHaveBeenCalled();
            expect(notify.success).toHaveBeenCalled();
            expect(ctrl.appliances.length).toEqual(9);
        });

        it('should attempt to remove item from index 1 and destroy item but return an error', function() {

            spyOn(api.applianceTemplates, "destroy").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            ctrl.appliances = appliance_items;
            $rootScope.$apply();
            ctrl.remove(ctrl.appliances[0]);
            $rootScope.$apply();

            expect(api.applianceTemplates.destroy).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalledWith("error");
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(ctrl.appliances.length).toEqual(10);
        });

    });

    describe('ctrl.addApp', function() {

        it('should add appliance to appliances array', function() {

            spyOn(PartnerTemplatesItemsEditModal, "show");

            createController();
            ctrl.appliances = appliance_items;
            $rootScope.$apply();
            ctrl.addApp();
            $rootScope.$apply();

            expect(PartnerTemplatesItemsEditModal.show).toHaveBeenCalled();
        });

    });

    describe('ctrl.edit', function() {
        it('should call edit modal', function() {

            spyOn(PartnerTemplatesItemsEditModal, "show");

            createController();
            ctrl.appliances = appliance_items;
            $rootScope.$apply();
            ctrl.edit(ctrl.appliances[0]);
            $rootScope.$apply();

            expect(PartnerTemplatesItemsEditModal.show).toHaveBeenCalled();
        });

    });

    describe('ctrl.insertAppliance', function() {
        it('updates the item to the list', function() {
            createController();
            ctrl.appliances = appliance_items;
            expect(ctrl.appliances[0].name).toEqual("Refrigerator");
            ctrl.insertAppliance({
                id: 1,
                "name": "Refrigerator 2"
            }, 0);

            expect(ctrl.appliances[0].name).toEqual("Refrigerator 2");
        });

        it('adds the item to the list', function() {
            createController();
            ctrl.appliances = appliance_items;
            var length = ctrl.appliances.length;
            ctrl.insertAppliance({
                id: length,
                "name": "Appliance 2"
            }, -1);

            expect(ctrl.appliances[length].id).toEqual(length);
        });
    });

});

describe('partnerTemplatesAppliances', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.appliances = [];
        $scope.create = function() {};

        element = $compile('<partner-templates-appliances appliances="appliances" create="create"></partner-templates-appliances>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the elements html', function() {
            expect(element.html()).toContain("Appliances");
        });
    });
});