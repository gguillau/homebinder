(function() {
    "use strict";

    angular
        .module("hb.partner.templates.form.documents.edit", [])
        .controller("PartnerTemplatesDocumentsEditController", [
            'PartnerTemplatesItemsEditModalController',
            "hb.resources",
            'data',
            "$modalInstance",
            "hb.api",
            "Session",
            "$log",
            PartnerTemplatesDocumentsEditController
        ]);

    function PartnerTemplatesDocumentsEditController(PartnerTemplatesItemsEditModalController, resources, opts, $modal, api, session, $log) {
        var Model = function() {
            // call the parent class
            PartnerTemplatesItemsEditModalController.call(this);
            // set the resources
            this.resources = resources.partnerTemplatesDocumentsEdit;
            // the item being updated/created
            this.item = opts.item;
            // the call that updates the item in the list view
            this.onSave = opts.onSave;
            // the index of the item in the items list
            this.index = opts.index;
            // the createCall
            this.createCall = api.documentTemplates.create;
            // the updateCall
            this.updateCall = api.documentTemplates.update;
            // set the modalInstance
            this.$modal = $modal;
            // uploader
            this.uploader = {
                id: "uploadDocument",
                fileTypes: "document",
                hideUploadButton: true,
                showTable: false,
                url: "/api/v1/binder_templates/" + this.templateId + '/document_templates/',
                jwt: session.getJwt(),
                hide_upload: false,
                multiSelect: false,
                fileLimit: true
            };
            // load document types
            this.loadTypes();
            // init the controller
            this.init();
        };

        Model.prototype = Object.create(PartnerTemplatesItemsEditModalController.prototype);

        angular.extend(Model.prototype, {
            loadTypes: function() {
                api.documentType.all({ count: 1000 }).then(
                    angular.bind(this, this.onLoadTypesSuccess),
                    angular.bind(this, this.onLoadTypesError)
                );
            },

            onLoadTypesSuccess: function(response) {
                this.document_types = response.data.items;
            },

            onLoadTypesError: function(response) {
                $log.error(response);
            },

            beforeSave: function() {
                var form = new FormData();
                if (this.item.id) {
                    form.append("document_template[file_file_name]", this.item.name + this.item.file_extension);
                }
                else {
                    form.append("document_template[file_file_name]", this.item.name + "." + this.item.file_extension);
                }
                form.append("document_template[binder_template_id]", this.binder_template ? this.binder_template.id : this.templateId);
                form.append("document_template[document_type_id]", this.item.document_type_id);
                form.append("document_template[notes]", this.item.notes ? this.item.notes : "");
                form.append("document_template[description]", this.item.description ? this.item.description : "");
                if (this.item.file) {
                    form.append("document_template[file]", this.item.file);
                }

                if (this.binder_template && this.binder_template.system) {
                    form.append("document_template[library_template]", true);
                }

                this.data = form;
            },

            fileAdded: function() {
                var pending = this.uploader.api.pendingUploads();
                this.item.file = pending[0];
                this.item.name = pending[0].name.replace(/\.[^/.]+$/, "");
                this.item.file_extension = pending[0].name.split('.').pop();
            }
        });

        this.model = new Model();
    }

})();