describe("PartnerTemplatesDocumentsEditController", function() {
    var $rootScope,
        $controller,
        PartnerTemplatesItemsEditModalController,
        $modalInstance,
        ctrl,
        api,
        $q,
        $log;


    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the $modalInstance service
        $provide.factory("$modalInstance", function() {
            return {
                close: function() {},
                dismiss: function() {}
            };
        });
    }));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        PartnerTemplatesItemsEditModalController = _$injector_.get("PartnerTemplatesItemsEditModalController");
        $modalInstance = _$injector_.get("$modalInstance");
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        $log = _$injector_.get("$log");
    }));

    function createController() {
        spyOn(api.documentType, "all").and.returnValue($q.when({ data: { items: [] } }));

        ctrl = $controller("PartnerTemplatesDocumentsEditController", {
            "PartnerTemplatesItemsEditModalController": PartnerTemplatesItemsEditModalController,
            "data": { item: {}, cfg: {} },
            "$modalInstance": $modalInstance
        });
        $rootScope.$apply();
    }

    describe('onLoadTypesError', function() {
        it("calls $log error", function() {
            spyOn($log, "error");

            createController();
            ctrl.model.onLoadTypesError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
        });
    });

    describe('beforeSave', function() {
        it("sets the data object", function() {
            createController();
            ctrl.model.item = { file: "test", file_extension: ".pdf", notes: "notes" };
            ctrl.model.beforeSave();

            expect(ctrl.model.data).not.toBeNull();
        });

        it("sets the data object for an existing item", function() {
            createController();
            ctrl.model.binder_template = {
                system: true
            };
            ctrl.model.item = { id: 1, file: "test", file_extension: ".pdf", notes: "notes" };
            ctrl.model.beforeSave();

            expect(ctrl.model.data).not.toBeNull();
        });
    });

    describe('fileAdded', function() {
        it("sets the item", function() {
            createController();
            ctrl.model.uploader = {
                api: {
                    pendingUploads: function() {}
                }
            };
            var item = { file: "file", file_extension: ".pdf", name: "name.pdf" };
            spyOn(ctrl.model.uploader.api, "pendingUploads").and.returnValue([item]);
            ctrl.model.item = {};
            ctrl.model.fileAdded();

            expect(ctrl.model.item.file).toBe(item);
        });
    });

});