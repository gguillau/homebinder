(function() {
    "use strict";

    angular
        .module("hb.partner.templates.form.documents", [
            "hb.partner.templates.form.documents.edit"
        ])
        .directive("partnerTemplatesDocuments", function() {
            return {
                restrict: "E",
                templateUrl: "partners/templates/form/documents/documents.tpl.html",
                controller: "PartnerTemplatesDocumentsController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    documents: "=",
                    templateId: "=",
                    create: "&",
                    uploader: "=",
                    fileUploaded: "&",
                    fileError: "&",
                    docForm: "="
                }
            };
        })
        .controller("PartnerTemplatesDocumentsController", [
            'hb.api',
            'hb.utils',
            'Loading',
            '$log',
            'Notify',
            'Session',
            'hb.resources',
            'PartnerTemplatesItemsEditModal',
            PartnerTemplatesDocumentsController
        ]);

    function PartnerTemplatesDocumentsController(api, utils, loading, $log, notify, session, resources, PartnerTemplatesItemsEditModal) {
        this.api = api;
        this.utils = utils;
        this.validationErrors = resources.validationErrors;
        this.$log = $log;
        this.notify = notify;
        this.loading = loading;
        this.session = session;
        this.resources = resources.partnerTemplatesDocuments;
        this.uploader = {};
        this.is_docs_collapsed = false;
        this.PartnerTemplatesItemsEditModal = PartnerTemplatesItemsEditModal;
    }

    PartnerTemplatesDocumentsController.prototype = {

        remove: function(file, index) {
            this.api.documentTemplates.destroy(file.id).then(
                angular.bind(this, this.onRemoveSuccess, index),
                angular.bind(this, this.onRemoveError)
            );
        },

        onRemoveSuccess: function(index, response) {
            this.documents.splice(index, 1);
            this.notify.success(this.resources.removed);
        },

        onRemoveError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
        },

        addDocument: function() {
            this.PartnerTemplatesItemsEditModal.show({
                templateUrl: "partners/templates/form/documents/edit/edit.tpl.html",
                controller: "PartnerTemplatesDocumentsEditController as ctrl",
                item: {},
                cfg: {},
                index: -1,
                onSave: angular.bind(this, this.insertDocument)
            });
        },

        edit: function(doc) {
            var index = this.documents.indexOf(doc);
            this.PartnerTemplatesItemsEditModal.show({
                templateUrl: "partners/templates/form/documents/edit/edit.tpl.html",
                controller: "PartnerTemplatesDocumentsEditController as ctrl",
                item: doc,
                cfg: {},
                index: index,
                onSave: angular.bind(this, this.insertDocument)
            });
        },

        insertDocument: function(item, index) {
            if (index > -1) {
                this.documents[index] = item;
            }
            else {
                this.documents.push(item);
            }
        }
    };
})();