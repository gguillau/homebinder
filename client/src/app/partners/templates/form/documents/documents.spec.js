describe("PartnerTemplatesDocumentsController", function() {
    var $rootScope,
        $scope,
        $controller,
        session,
        ctrl,
        uploader,
        api,
        $q,
        notify,
        PartnerTemplatesItemsEditModal;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        session = _$injector_.get("Session");
        notify = _$injector_.get("Notify");
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        PartnerTemplatesItemsEditModal = _$injector_.get("PartnerTemplatesItemsEditModal");
        $controller = _$controller_;

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("PartnerTemplatesDocumentsController", {
            "Session": session
        });

        $scope.ctrl = ctrl;
        uploader = ctrl.uploader;
        uploader.api = {
            pendingUploads: function() {},
            remove: function(file) {}
        };
        $scope.$apply();
        $scope.$digest();

    }

    describe('ctrl.remove', function() {
        it('should remove image file from queue', function() {
            spyOn(api.documentTemplates, "destroy").and.returnValue($q.when({ data: {} }));
            var file = { id: 1, name: "file", binder_template_id: 1 };
            createController();
            ctrl.documents = [];
            ctrl.documents.push(file);
            ctrl.remove(file);

            expect(api.documentTemplates.destroy).toHaveBeenCalled();

        });
    });

    describe('ctrl.onRemoveSuccess', function() {
        it('should remove image file from queue', function() {
            var file = { name: "file", getNative: function() { return "file"; } };
            createController();
            ctrl.documents = [];
            ctrl.documents.push(file);
            expect(ctrl.documents.length).toEqual(1);
            ctrl.onRemoveSuccess(0, {});

            expect(ctrl.documents.length).toEqual(0);
        });
    });

    describe('ctrl.onRemoveError', function() {
        it('should call notify', function() {
            createController();
            spyOn(notify, "error");
            ctrl.onRemoveError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();

        });
    });

    describe('ctrl.addDocument', function() {
        it('should call PartnerTemplatesItemsEditModal', function() {
            createController();
            spyOn(PartnerTemplatesItemsEditModal, "show");
            ctrl.addDocument();

            expect(PartnerTemplatesItemsEditModal.show).toHaveBeenCalled();

        });
    });

    describe('ctrl.edit', function() {
        it('should call PartnerTemplatesItemsEditModal', function() {
            createController();
            spyOn(PartnerTemplatesItemsEditModal, "show");
            ctrl.documents = [{ id: 1 }];
            ctrl.edit(ctrl.documents[0]);

            expect(PartnerTemplatesItemsEditModal.show).toHaveBeenCalled();

        });
    });

    describe('ctrl.insertDocument', function() {
        it('updates the item to the list', function() {
            createController();
            ctrl.documents = [{ id: 1, name: "Test" }];
            expect(ctrl.documents[0].name).toEqual("Test");
            ctrl.insertDocument({
                id: 1,
                "name": "Test 2"
            }, 0);

            expect(ctrl.documents[0].name).toEqual("Test 2");
        });

        it('adds the item to the list', function() {
            createController();
            ctrl.documents = [];
            var length = ctrl.documents.length;
            ctrl.insertDocument({
                id: length,
                "name": "Test"
            }, -1);

            expect(ctrl.documents[length].id).toEqual(length);
        });

    });
});

describe('partnerTemplatesDocuments', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        $scope = $rootScope.$new(), $compile = _$compile_;

        $scope.documents = [];
        $scope.create = function() {};
        $scope.uploaded = function() {};
        $scope.error = function() {};
        $scope.uploader = {};
        $scope.form = {};
        element = $compile('<partner-templates-documents documents="documents" template-id="1" create="create" uploader="uploader" file-uploaded="uploaded" file-error="error" doc-form="form"></partner-templates-documents>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the partner-templates-documents> directive', function() {
        it('adds the title to the html', function() {
            expect(element.html()).toContain("Documents");
        });
    });
});