describe("PartnerTemplatesFormController", function() {

    var $rootScope,
        $scope,
        $controller,
        $q,
        $stateParams,
        ctrl,
        api,
        $log,
        Session,
        notify,
        loading;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");

        $stateParams = {};

        loading = {
            show: function(msg) {},
            close: function() {},
            setMessage: function() {}
        };

        api = {

            binderTemplate: {
                get: function(templateId) {},
                update: function(templateId, data) {}
            },
            maintenanceTemplates: {
                create: function(templateId, mi) {},
                update: function(itemId, item) {}
            },
            applianceTemplates: {
                create: function(templateId, app) {},
                update: function(appId, app) {}
            },
            contractorTemplates: {
                create: function(templateId, cont) {},
                update: function(contId, cont) {}
            },
            documentTemplates: {
                create: function(templateId, doc) {},
                update: function(docId, doc) {}
            }
        };

        notify = {
            error: function(msg) {},
            success: function(msg) {}
        };

        $log = {
            error: function(msg) {}
        };

        Session = {
            getJwt: function() {}
        };

        $stateParams.templateId = 11;

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("PartnerTemplatesFormController", {
            "$stateParams": $stateParams,
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "Session": Session,
            "Loading": loading
        });

        $scope.ctrl = ctrl;
    }

    describe('ctrl.refresh', function() {

        it('should retrieve partner template and set note to default note', function() {

            var template = {
                appliance_templates: [{
                    binder_template_id: 79,
                    id: 337,
                    image_url: "/images/original/missing.png",
                    name: "Water Heater",
                    notes: ""
                }],
                contractor_templates: [{
                    binder_template_id: 79,
                    email: "",
                    id: 109,
                    image_url: "/images/original/missing.png",
                    name: "Benjamin Franklin",
                    notes: "",
                    phone: "202-967-1556",
                    type: "Plumber"
                }],
                id: 1,
                maintenance_templates: [{
                    binder_template_id: 79,
                    due_date: "2016-10-15T00:00:00Z",
                    frequency: "Annual",
                    id: 414,
                    image_url: "/images/original/missing.png",
                    name: "Sweep Chimney",
                    notes: null
                }],
                document_templates: [{
                    binder_template_id: 79,
                    file_url: "https://fakeprod.s3.amazonaws.com/binder_templates/doc.pdf",
                    id: 39,
                    name: "doc.pdf",
                    notes: "This Life Expectancy Chart is to be used as a guide only.  This may guide you in budgeting for future replacement of the homes major components.",
                    size: 54645
                }],
                name: "Binder Defaults",
                transfer_note: null
            };
            spyOn(api.binderTemplate, "get").and.returnValue($q.when({
                data: template
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalled();
            expect(api.binderTemplate.get).toHaveBeenCalledWith(11);
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.templateName).toBe("Binder Defaults");
            expect(ctrl.current_name).toBe("Binder Defaults");
            expect(ctrl.items).toEqual(template.maintenance_templates);
            expect(ctrl.appliances).toEqual(template.appliance_templates);
            expect(ctrl.contractors).toEqual(template.contractor_templates);
            expect(ctrl.documents).toEqual(template.document_templates);
            expect(ctrl.transfer_note).toEqual(ctrl.note);

        });

        it('should retrieve partner template and note', function() {

            var template = {
                appliance_templates: [{
                    binder_template_id: 79,
                    id: 337,
                    image_url: "/images/original/missing.png",
                    name: "Water Heater",
                    notes: ""
                }],
                contractor_templates: [{
                    binder_template_id: 79,
                    email: "",
                    id: 109,
                    image_url: "/images/original/missing.png",
                    name: "Benjamin Franklin",
                    notes: "",
                    phone: "202-967-1556",
                    type: "Plumber"
                }],
                id: 1,
                maintenance_templates: [{
                    binder_template_id: 79,
                    due_date: "2016-10-15T00:00:00Z",
                    frequency: "Annual",
                    id: 414,
                    image_url: "/images/original/missing.png",
                    name: "Sweep Chimney",
                    notes: null
                }],
                document_templates: [{
                    binder_template_id: 79,
                    file_url: "https://fakeprod.s3.amazonaws.com/binder_templates/doc.pdf",
                    id: 39,
                    name: "doc.pdf",
                    notes: "This Life Expectancy Chart is to be used as a guide only.  This may guide you in budgeting for future replacement of the homes major components.",
                    size: 54645
                }],
                name: "Binder Defaults",
                transfer_note: "This is a note."
            };
            spyOn(api.binderTemplate, "get").and.returnValue($q.when({
                data: template
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalled();
            expect(api.binderTemplate.get).toHaveBeenCalledWith(11);
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.templateName).toBe("Binder Defaults");
            expect(ctrl.current_name).toBe("Binder Defaults");
            expect(ctrl.items).toEqual(template.maintenance_templates);
            expect(ctrl.appliances).toEqual(template.appliance_templates);
            expect(ctrl.contractors).toEqual(template.contractor_templates);
            expect(ctrl.documents).toEqual(template.document_templates);
            expect(ctrl.transfer_note).toEqual("This is a note.");

        });

        it('should try to retrieve template but return an error', function() {

            spyOn(api.binderTemplate, "get").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalled();
            expect(api.binderTemplate.get).toHaveBeenCalledWith(11);
            expect(notify.error).toHaveBeenCalledWith("error");
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(loading.close).toHaveBeenCalled();

        });
    });

    describe('ctrl.save', function() {
        it('should copy templates and call addAppliance function', function() {

            var template = {
                appliance_templates: [{
                    binder_template_id: 79,
                    id: 337,
                    image_url: "/images/original/missing.png",
                    name: "Water Heater",
                    notes: ""
                }],
                contractor_templates: [{
                    binder_template_id: 79,
                    email: "",
                    id: 109,
                    image_url: "/images/original/missing.png",
                    name: "Benjamin Franklin",
                    notes: "",
                    phone: "202-967-1556",
                    type: "Plumber"
                }],
                id: 1,
                maintenance_templates: [{
                    binder_template_id: 79,
                    due_date: "2016-10-15T00:00:00Z",
                    frequency: "Annual",
                    id: 414,
                    image_url: "/images/original/missing.png",
                    name: "Sweep Chimney",
                    notes: null
                }],
                document_templates: [{
                    binder_template_id: 79,
                    file_url: "https://fakeprod.s3.amazonaws.com/binder_templates/doc.pdf",
                    id: 39,
                    name: "doc.pdf",
                    notes: "This Life Expectancy Chart is to be used as a guide only.  This may guide you in budgeting for future replacement of the homes major components.",
                    size: 54645
                }],
                name: "Binder Defaults",
                transfer_note: null
            };

            spyOn(api.binderTemplate, "get").and.returnValue($q.when({
                data: template
            }));

            spyOn(api.binderTemplate, "update").and.returnValue($q.when({
                data: template
            }));

            createController();
            $rootScope.$apply();
            ctrl.save();
            $rootScope.$apply();

            expect(ctrl.temp_items).toEqual(template.maintenance_templates);
            expect(ctrl.temp_appliances).toEqual(template.appliance_templates);
            expect(ctrl.temp_contractors).toEqual(template.contractor_templates);
            expect(ctrl.temp_documents).toEqual(template.document_templates);

        });

    });

    describe('ctrl.update', function() {
        it('should succesfully call api binderTemplate update and edit method', function() {

            var template = {
                appliance_templates: [],
                contractor_templates: [],
                id: 1,
                maintenance_templates: [],
                document_templates: [{
                    id: 1,
                    name: "Sample.pdf"
                }],
                name: "Binder Defaults"
            };

            spyOn(api.binderTemplate, "get").and.returnValue($q.when({
                data: template
            }));
            spyOn(api.binderTemplate, "update").and.returnValue($q.when({
                data: template
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn(notify, "success");

            createController();
            spyOn(ctrl, "edit").and.callThrough();
            ctrl.read_only = false;
            $rootScope.$apply();
            ctrl.update();
            $rootScope.$apply();

            expect(api.binderTemplate.update).toHaveBeenCalledWith(11, {
                name: "Binder Defaults",
                transfer_note: ctrl.note
            });
            expect(ctrl.items).toEqual(template.maintenance_templates);
            expect(ctrl.appliances).toEqual(template.appliance_templates);
            expect(ctrl.contractors).toEqual(template.contractor_templates);
            expect(ctrl.documents).toEqual(template.document_templates);
            expect(ctrl.transfer_note).toEqual(ctrl.note);
            expect(loading.close).toHaveBeenCalled();
            expect(notify.success).toHaveBeenCalled();
            expect(ctrl.edit).toHaveBeenCalled();
            expect(ctrl.read_only).toBe(true);

        });

        it('should succesfully call api binderTemplate update and but not edit method', function() {

            var template = {
                appliance_templates: [],
                contractor_templates: [],
                id: 1,
                maintenance_templates: [],
                document_templates: [{
                    id: 1,
                    name: "Sample.pdf"
                }],
                name: "Binder Defaults"
            };

            spyOn(api.binderTemplate, "get").and.returnValue($q.when({
                data: template
            }));

            spyOn(api.binderTemplate, "update").and.returnValue($q.when({
                data: template
            }));

            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn(notify, "success");

            createController();
            spyOn(ctrl, "edit");
            ctrl.read_only = true;
            $rootScope.$apply();
            ctrl.update();
            $rootScope.$apply();

            expect(api.binderTemplate.update).toHaveBeenCalledWith(11, {
                name: "Binder Defaults",
                transfer_note: ctrl.note
            });
            expect(ctrl.items).toEqual(template.maintenance_templates);
            expect(ctrl.appliances).toEqual(template.appliance_templates);
            expect(ctrl.contractors).toEqual(template.contractor_templates);
            expect(ctrl.documents).toEqual(template.document_templates);
            expect(ctrl.transfer_note).toEqual(ctrl.note);
            expect(loading.close).toHaveBeenCalled();
            expect(notify.success).toHaveBeenCalled();
            expect(ctrl.edit).not.toHaveBeenCalled();
            expect(ctrl.read_only).toBe(true);

        });

        it('should call api binderTemplate update and edit method and return an error', function() {

            var template = {
                appliance_templates: [],
                contractor_templates: [],
                id: 1,
                maintenance_templates: [],
                document_templates: [{
                    id: 1,
                    name: "Sample.pdf"
                }],
                name: "Binder Defaults"
            };

            spyOn(api.binderTemplate, "get").and.returnValue($q.when({
                data: template
            }));
            spyOn(api.binderTemplate, "update").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            spyOn(ctrl, "edit").and.callThrough();
            ctrl.read_only = false;
            $rootScope.$apply();
            ctrl.update();
            $rootScope.$apply();

            expect(api.binderTemplate.update).toHaveBeenCalledWith(11, {
                name: "Binder Defaults",
                transfer_note: ctrl.note
            });
            expect(loading.close).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalledWith("error");
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(ctrl.edit).toHaveBeenCalled();
            expect(ctrl.read_only).toBe(true);

        });

        it('should call api binderTemplate update and but not edit method and return an error', function() {

            var template = {
                appliance_templates: [],
                contractor_templates: [],
                id: 1,
                maintenance_templates: [],
                document_templates: [{
                    id: 1,
                    name: "Sample.pdf"
                }],
                name: "Binder Defaults"
            };

            spyOn(api.binderTemplate, "get").and.returnValue($q.when({
                data: template
            }));
            spyOn(api.binderTemplate, "update").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            spyOn(ctrl, "edit");
            ctrl.read_only = true;
            $rootScope.$apply();
            ctrl.update();
            $rootScope.$apply();

            expect(api.binderTemplate.update).toHaveBeenCalledWith(11, {
                name: "Binder Defaults",
                transfer_note: ctrl.note
            });
            expect(loading.close).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalledWith("error");
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(ctrl.edit).not.toHaveBeenCalled();
            expect(ctrl.read_only).toBe(true);

        });

    });

    describe('ctrl.edit', function() {
        it('should set read_only to true', function() {

            var template = {
                appliance_templates: [],
                contractor_templates: [],
                id: 1,
                maintenance_templates: [],
                document_templates: [{
                    id: 1,
                    name: "Sample.pdf"
                }],
                name: "Binder Defaults"
            };

            spyOn(api.binderTemplate, "get").and.returnValue($q.when({
                data: template
            }));

            createController();
            ctrl.read_only = false;
            $rootScope.$apply();
            ctrl.edit();
            $rootScope.$apply();

            expect(ctrl.read_only).toBe(true);

        });

        it('should set read_only to false', function() {

            var template = {
                appliance_templates: [],
                contractor_templates: [],
                id: 1,
                maintenance_templates: [],
                document_templates: [{
                    id: 1,
                    name: "Sample.pdf"
                }],
                name: "Binder Defaults"
            };

            spyOn(api.binderTemplate, "get").and.returnValue($q.when({
                data: template
            }));

            createController();
            ctrl.read_only = true;
            $rootScope.$apply();
            ctrl.edit();
            $rootScope.$apply();

            expect(ctrl.read_only).toBe(false);

        });
    });

    describe('ctrl.editForm', function() {
        it('should set read_only to false', function() {

            var template = {
                appliance_templates: [],
                contractor_templates: [],
                id: 1,
                maintenance_templates: [],
                document_templates: [{
                    id: 1,
                    name: "Sample.pdf"
                }],
                name: "Binder Defaults"
            };

            spyOn(api.binderTemplate, "get").and.returnValue($q.when({
                data: template
            }));

            createController();
            ctrl.read_only = true;
            $rootScope.$apply();
            ctrl.editForm();
            $rootScope.$apply();

            expect(ctrl.read_only).toBe(false);

        });
    });

    describe('ctrl.cancelForm', function() {
        it('should set read_only to true', function() {

            var template = {
                appliance_templates: [],
                contractor_templates: [],
                id: 1,
                maintenance_templates: [],
                document_templates: [{
                    id: 1,
                    name: "Sample.pdf"
                }],
                name: "Binder Defaults"
            };

            spyOn(api.binderTemplate, "get").and.returnValue($q.when({
                data: template
            }));

            createController();
            ctrl.read_only = false;
            $rootScope.$apply();
            ctrl.cancelForm();
            $rootScope.$apply();

            expect(ctrl.read_only).toBe(true);

        });

    });

});

describe('partnerTemplatesForm', function() {
    var $scope, $compile, api, $q, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        spyOn(api.binderTemplate, "get").and.returnValue($q.when({ data: {} }));

        element = $compile('<partner-templates-form></partner-templates-form>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("Maintenance Items");
        });
    });
});