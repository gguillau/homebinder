describe("PartnerTemplatesMaintenanceItemsEditController", function() {
    var $rootScope,
        $controller,
        PartnerTemplatesItemsEditModalController,
        $modalInstance,
        ctrl,
        api,
        $q,
        $log,
        utils,
        notify,
        $filter,
        Session;


    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the $modalInstance service
        $provide.factory("$modalInstance", function() {
            return {
                close: function() {},
                dismiss: function() {}
            };
        });
    }));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        PartnerTemplatesItemsEditModalController = _$injector_.get("PartnerTemplatesItemsEditModalController");
        $modalInstance = _$injector_.get("$modalInstance");
        api = _$injector_.get("hb.api");
        utils = _$injector_.get("hb.utils");
        $log = _$injector_.get("$log");
        notify = _$injector_.get("Notify");
        $filter = _$injector_.get("$filter");
        Session = _$injector_.get("Session");
        $q = _$injector_.get("$q");
        
    }));

    function createController() {
        spyOn(api.maintenanceTemplates, "all").and.returnValue($q.when({ data: { items: [] } }));

        ctrl = $controller("PartnerTemplatesMaintenanceItemsEditController", {
            "PartnerTemplatesItemsEditModalController": PartnerTemplatesItemsEditModalController,
            "data": { item: {}, cfg: {} },
            "$modalInstance": $modalInstance,
            "hb.api": api,
            "hb.utils": utils,
            "$log": $log,
            "Notify": notify,
            "$filter": $filter,
            "Session": Session
        });
        $rootScope.$apply();
    }

    describe('loadLibrary', function() {
        it("calls api maintenanceTemplates all", function() {
            createController();
            ctrl.model.loadLibrary();

            expect(api.maintenanceTemplates.all).toHaveBeenCalled();
        });
    });

    describe('onLoadLibraryTemplates', function() {
        it("sets the templates", function() {
            createController();
            ctrl.model.onLoadLibraryTemplates({ data: { items: [{ id: 1 }] } });

            expect(ctrl.model.maintenance_templates.length).toEqual(1);
        });
    });

    describe('onSelect', function() {
        it("sets the $item", function() {
            createController();

            ctrl.model.onSelect({ name: "name", notes: "notes", description: "description", maintenance_template_contractor_types: "maintenance_template_contractor_types", types: "types", library_source_id: 1, frequency: "frequency", due_date: Date.now() }, {}, {});

            expect(ctrl.model.item.name).toEqual("name");
            expect(ctrl.model.item.notes).toEqual("notes");
            expect(ctrl.model.item.description).toEqual("description");
            expect(ctrl.model.item.maintenance_template_contractor_types).toEqual("maintenance_template_contractor_types");
            expect(ctrl.model.item.types).toEqual("types");
            expect(ctrl.model.item.library_source_id).toEqual(1);
            expect(ctrl.model.item.frequency).toEqual("frequency");
            expect(ctrl.model.maintenance_template_contractor_types).toEqual("maintenance_template_contractor_types");
            expect(ctrl.model.types).toEqual("types");
        });
    });

    describe('beforeSave', function() {
        it("sets the data object", function() {
            createController();
            ctrl.model.maintenance_template_contractor_types = [];
            ctrl.model.item = { name: "test" };
            ctrl.model.beforeSave();

            expect(ctrl.model.data.name).toEqual("test");
        });
    });

    describe('frequencyChange', function() {
        it("sets the due_date to null", function() {
            createController();
            ctrl.model.item = { name: "test" };
            ctrl.model.item.due_date = new Date();
            ctrl.model.item.frequency = "As Needed";
            ctrl.model.frequencyChange();

            expect(ctrl.model.item.due_date).toBe(null);
        });

        it("sets the due_date_read_only to false", function() {
            createController();
            ctrl.model.item = { name: "test" };
            ctrl.model.item.due_date = new Date();
            ctrl.model.item.frequency = "Once";
            ctrl.model.item.due_date_read_only = true;
            ctrl.model.item.interval_read_only = true;
            ctrl.model.frequencyChange();

            expect(ctrl.model.item.due_date_read_only).toBe(false);
        });
    });

    describe('changeDate', function() {
        it("sets the frequency to once", function() {
            createController();
            ctrl.model.item = { name: "test" };
            ctrl.model.item.due_date = new Date();
            ctrl.model.item.frequency = "As Needed";
            ctrl.model.changeDate();

            expect(ctrl.model.item.frequency).toEqual("Once");
        });

        it("sets the frequency to once", function() {
            createController();
            ctrl.model.item = { name: "test" };
            ctrl.model.item.due_date = new Date();
            ctrl.model.item.frequency = "As Needed";
            ctrl.model.changeDate();

            expect(ctrl.model.item.frequency).toEqual("Once");
        });

        it("sets the due_date to null", function() {
            createController();
            ctrl.model.item = { name: "test" };
            ctrl.model.item.due_date = new Date();
            ctrl.model.item.frequency = "Once";
            ctrl.model.item.due_date_read_only = false;
            ctrl.model.changeDate();

            expect(ctrl.model.item.due_date).toBe(null);
        });

        it("sets the initial_due_date_interval to null", function() {
            createController();
            ctrl.model.item = { name: "test" };
            ctrl.model.item.due_date = new Date();
            ctrl.model.item.frequency = "Once";
            ctrl.model.item.due_date_read_only = true;
            ctrl.model.changeDate();

            expect(ctrl.model.item.initial_due_date_interval).toBe(null);
        });
    });
    
    describe("searchTypes", function() {
        it("calls api contractorType all when value exists and its length is greater than 3", function() {
            spyOn(api.contractorType, "all").and.returnValue($q.when({ data: { items: [] } }));
            createController();
            ctrl.model.searchTypes("search");

            expect(api.contractorType.all).toHaveBeenCalled();
        });

        it("does not call api contractorType all when value is undefined", function() {
            spyOn(api.contractorType, "all").and.returnValue($q.when({ data: { items: [] } }));
            createController();
            ctrl.model.searchTypes(undefined);

            expect(api.contractorType.all).not.toHaveBeenCalled();
        });
        
        it("does not call api contractorType all when value is null", function() {
            spyOn(api.contractorType, "all").and.returnValue($q.when({ data: { items: [] } }));
            createController();
            ctrl.model.searchTypes(null);

            expect(api.contractorType.all).not.toHaveBeenCalled();
        });
        
        it("does not call api contractorType all when value length is less than 3", function() {
            spyOn(api.contractorType, "all").and.returnValue($q.when({ data: { items: [] } }));
            createController();
            ctrl.model.searchTypes("se");

            expect(api.contractorType.all).not.toHaveBeenCalled();
        });
    });
    
    describe("searchComplete", function() {
        it("sets the options", function() {
            createController();
            ctrl.model.types = [];
            ctrl.model.maintenance_template_contractor_types = [];
            ctrl.model.item = {
                types: []
            };
            ctrl.model.searchComplete({ data: { items: [{ id: 1, name: "test" }] } });

            expect(ctrl.model.typeOptions.length).toEqual(1);
        });
    });
    
    describe("onTypeSelect", function() {
        it("calls findExisting", function() {
            createController();
            var type = { id: 1, name: "test" };
            ctrl.model.types = [type];
            ctrl.model.item = { types: [type] };
            ctrl.model.maintenance_template_contractor_types = [];
            spyOn(ctrl.model, "findExisting").and.returnValue({ index: -1, item: { id: 1, name: "test", sub_types: [{ id: 1, name: "cpa" }] } });
            ctrl.model.onTypeSelect({}, type);

            expect(ctrl.model.item.types.length).toEqual(1);
        });
    });
    
    describe("onTypeRemove", function() {
        it("calls findExisting and updates the item's types", function() {
            createController();
            var type = { id: 1, name: "test", sub_types: ["test"] };
            ctrl.model.maintenance_template_contractor_types = [type];
            ctrl.model.item = { types: [type] };
            expect(ctrl.model.item.types.length).toEqual(1);
            spyOn(ctrl.model, "findExisting").and.returnValue({ index: 0, name: "test" });
            ctrl.model.onTypeRemove(type);

            expect(ctrl.model.maintenance_template_contractor_types.length).toEqual(0);
        });

        it("calls findExisting and updates the item's types", function() {
            createController();
            var type = { id: 1, name: "test", sub_types: ["test"] };
            ctrl.model.maintenance_template_contractor_types = undefined;
            ctrl.model.item = { types: [type] };
            expect(ctrl.model.item.types.length).toEqual(1);
            spyOn(ctrl.model, "findExisting").and.returnValue({ index: 0, name: "test" });
            ctrl.model.onTypeRemove(type);

            expect(ctrl.model.maintenance_template_contractor_types.length).toEqual(0);
        });
    });
    
    describe("findExisting", function() {
        it("returns an index of -1", function() {
            createController();
            ctrl.model.maintenance_template_contractor_types = [];
            ctrl.model.item = { maintenance_template_contractor_types: [] };
            var hash = ctrl.model.findExisting({ type: { name: "test" } });

            expect(hash.index).toEqual(-1);
        });
    });
    
    describe("tagTransform", function() {
        it("returns the item", function() {
            createController();
            var expected = {
                name: "newTag"
            };
            var item = ctrl.model.tagTransform("newTag");

            expect(item).toEqual(expected);
        });
    });
    
    describe('onError', function() {
        it("logs and notifies the error", function() {
            createController();
            spyOn($log, "error");
            spyOn(notify, "error");

            var response = {data: "error"};
            ctrl.model.onError(response);

            expect($log.error).toHaveBeenCalledWith({data: "error"});
            expect(notify.error).toHaveBeenCalledWith("error");
        });
    });

});