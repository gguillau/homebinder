(function() {
    "use strict";

    angular
        .module("hb.partner.templates.form.maintenanceItems.edit", [])
        .controller("PartnerTemplatesMaintenanceItemsEditController", [
            'PartnerTemplatesItemsEditModalController',
            "hb.resources",
            'data',
            "$modalInstance",
            "hb.api",
            "hb.utils",
            "$log",
            "Notify",
            "$filter",
            "Session",
            PartnerTemplatesMaintenanceItemsEditController
        ]);

    function PartnerTemplatesMaintenanceItemsEditController(PartnerTemplatesItemsEditModalController, resources, opts, $modal, api, utils, $log, notify, $filter, Session) {
        var Model = function() {
            // call the parent class
            PartnerTemplatesItemsEditModalController.call(this);
            // set the resources
            this.resources = resources.partnerTemplatesAppliancesEdit;
            // the item being updated/created
            this.item = opts.item;
            this.maintenance_template_contractor_types = this.item.maintenance_template_contractor_types;
            this.types = this.item.types;
            // the call that updates the item in the list view
            this.onSave = opts.onSave;
            // the index of the item in the items list
            this.index = opts.index;
            // whether we should show typeheads
            this.showTypeaheads = opts.cfg.showTypeaheads;
            // the createCall
            this.createCall = api.maintenanceTemplates.create;
            // the updateCall
            this.updateCall = api.maintenanceTemplates.update;
            // set the modalInstance
            this.$modal = $modal;
            this.utils = utils;
            this.$log = $log;
            this.notify = notify;
            this.$filter = $filter;
            this.Session = Session;
            this.currentUser = this.Session.getUser();
            this.today = this.utils.utils.getToday(false);
            this.next_month = this.utils.utils.getNextMonth();
            this.yyyy = this.utils.utils.getYear();
            this.datepicker = {
                format: "MMMM dd, yyyy"
            };
            this.days_after = [{
                name: "In 30 Days",
                value: 30
            }, {
                name: "In 60 Days",
                value: 60
            }, {
                name: "In 90 Days",
                value: 90
            }, {
                name: "In 120 Days",
                value: 120
            }, {
                name: "In 180 Days",
                value: 180
            }];
            this.frequencies = [{
                name: "Once",
                value: "Once"
            }, {
                name: "Annual",
                value: "Annual"
            }, {
                name: "Quarterly",
                value: "Quarterly"
            }, {
                name: "Monthly",
                value: "Monthly"
            }, {
                name: "Every Other Year",
                value: "Every-Other-Year"
            }, {
                name: "Every 3 Years",
                value: "Every-3-Years"
            }, {
                name: "Every 4 Years",
                value: "Every-4-Years"
            }, {
                name: "Every 5 Years",
                value: "Every-5-Years"
            }, {
                name: "Every 10 Years",
                value: "Every-10-Years"
            }, {
                name: "Every 40 Years",
                value: "Every-40-Years"
            }, {
                name: "Semi-Annual",
                value: "Semi-Annual"
            }, {
                name: "As Needed",
                value: "As Needed"
            }];
            this.typeOptions = [];

            // load the library
            this.loadLibrary();
            // init the controller
            this.init();
        };

        Model.prototype = Object.create(PartnerTemplatesItemsEditModalController.prototype);

        angular.extend(Model.prototype, {

            loadLibrary: function() {
                api.maintenanceTemplates.all({ library_template: true, system: true, "default": true, count: 1000 }).then(
                    angular.bind(this, this.onLoadLibraryTemplates),
                    angular.bind(this, this.onError)
                );
            },

            onLoadLibraryTemplates: function(response) {
                this.maintenance_templates = response.data.items;
            },

            onSelect: function($item, $model, label) {
                this.item.name = $item.name;
                this.item.notes = $item.notes;
                this.item.description = $item.description;
                this.item.estimated_cost = $item.estimated_cost;
                this.item.estimated_time = $item.estimated_time;
                this.item.maintenance_template_contractor_types = $item.maintenance_template_contractor_types;
                this.item.types = $item.types;
                this.item.video_url = $item.video_url;
                this.item.library_source_id = $item.library_source_id;
                this.item.frequency = $item.frequency;
                if ($item.due_date) {
                    this.item.due_date = new Date($item.due_date);
                }
                this.item.initial_due_date_interval = $item.initial_due_date_interval;
            
                this.maintenance_template_contractor_types = this.item.maintenance_template_contractor_types;
                this.types = this.item.types;
            },

            beforeSave: function() {
                var filtered_maintenance_template_contractor_types = this.maintenance_template_contractor_types.filter(function(maintenance_template_contractor_type) {
                    return maintenance_template_contractor_type.id || maintenance_template_contractor_type.type.id;
                });
                var filtered_types = this.maintenance_template_contractor_types.filter(function(maintenance_template_contractor_type) {
                    return !maintenance_template_contractor_type.id && !maintenance_template_contractor_type.type.id;
                });
                
                this.data = {
                    binder_template_id: this.binder_template ? this.binder_template.id : this.templateId,
                    frequency: this.item.frequency,
                    name: this.item.name,
                    initial_due_date_interval: this.item.initial_due_date_interval,
                    due_date: this.item.due_date,
                    notes: this.item.notes,
                    description: this.item.description,
                    estimated_cost: this.item.estimated_cost,
                    estimated_time: this.item.estimated_time,
                    maintenance_template_contractor_types_attributes: filtered_maintenance_template_contractor_types.map(function(maintenance_template_contractor_type) {
                        if (maintenance_template_contractor_type.id) {
                            return { id: maintenance_template_contractor_type.id };
                        }
                        else if (maintenance_template_contractor_type.type.id) {
                            return { contractor_category_id: maintenance_template_contractor_type.type.id };
                        }
                    }),
                    types_attributes: filtered_types.map(function(maintenance_template_contractor_type) {
                        if (!maintenance_template_contractor_type.id && !maintenance_template_contractor_type.type.id) {
                            return { name: maintenance_template_contractor_type.type.name };
                        }
                    }),
                    video_url: this.item.video_url,
                    library_template: this.item.library_template,
                    library_source_id: this.item.library_source_id,
                    image: this.item.image,
                    image_file_name: this.item.image_file_name
                };
            },

            frequencyChange: function() {
                if (this.item.frequency === "As Needed") {
                    this.item.due_date = null;
                    this.item.initial_due_date_interval = null;
                    this.item.interval_read_only = true;
                    this.item.due_date_read_only = true;
                }
                else if (this.item.interval_read_only && this.item.due_date_read_only) {
                    this.item.due_date_read_only = false;
                }
            },

            changeDate: function() {
                if (this.item.frequency === "As Needed") {
                    this.item.frequency = "Once";
                }
                else {
                    if (!this.item.due_date_read_only) {
                        this.item.due_date = null;
                    }
                    else {
                        this.item.initial_due_date_interval = null;
                    }
                }
            },

            searchTypes: function(value) {
                // don't do the search when no search value is provided just clear the list
                if (!value || value.length < 3) {
                    this.typeOptions = [];
                    return;
                }

                var opts = {
                    page: 1,
                    count: 50,
                    search: value,
                    verified: true
                };

                api.contractorType.all(opts).then(
                    angular.bind(this, this.searchComplete),
                    angular.bind(this, this.onError)
                );
            },

            searchComplete: function(response) {
                this.typeOptions = response.data.items.filter(angular.bind(this, function(item) {
                    var array = $filter("filter")(this.maintenance_template_contractor_types, {
                        type: {
                            name: item.name
                        }
                    });
                    return array.length < 1;
                }));
            },

            onTypeSelect: function($select, $item) {
                // remove the item first and then add back again
                var index = this.maintenance_template_contractor_types.indexOf($item);
                if (index > -1) {
                    this.maintenance_template_contractor_types.splice(index, 1);
                }

                var hash = this.findExisting($item);
                if (hash.index < 0) {
                    if (hash.item.id) {
                        this.maintenance_template_contractor_types.push({ type: { id: hash.item.id, name: hash.item.name } });
                    }
                    else {
                        this.maintenance_template_contractor_types.push({ type: { name: hash.item.name } });
                    }
                }
            },

            onTypeRemove: function($item) {
                if (!this.maintenance_template_contractor_types) {
                    this.maintenance_template_contractor_types = [];
                }
                var hash = this.findExisting($item);
                if (hash.index > -1) {
                    this.maintenance_template_contractor_types.splice(hash.index, 1);
                }
            },

            findExisting: function(item) {
                var name = null;
                if (!item.id && !item.isTag) {
                    name = item.type.name.toLowerCase();
                    item = { type: { name: name } };
                }
                return { index: this.maintenance_template_contractor_types.indexOf(item), item: item };
            },

            tagTransform: function(newTag) {
                var item = {
                    name: newTag
                };

                return item;
            },

            onError: function(response) {
                this.$log.error(response);
                this.notify.error(response.data);
            }
        });

        this.model = new Model();
    }

})();