(function() {
	"use strict";

	angular
		.module("hb.partner.templates.form.maintenanceItems", [
			"hb.partner.templates.form.maintenanceItems.itemModal",
			"hb.partner.templates.form.maintenanceItems.edit"
		])
		.directive("partnerTemplatesMaintenanceItems", function() {
			return {
				restrict: "E",
				templateUrl: "partners/templates/form/maintenanceItems/maintenanceItems.tpl.html",
				controller: "PartnerTemplatesMaintenanceItemsController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {
					items: "=",
					create: "&",
					maintForm: "="
				}
			};
		})
		.directive('datepickerPopup', function() {
			return {
				restrict: 'EAC',
				require: 'ngModel',
				link: function(scope, element, attr, controller) {
					//remove the default formatter from the input directive to prevent conflict
					controller.$formatters.shift();
				}
			};
		})
		.controller("PartnerTemplatesMaintenanceItemsController", [
			"hb.utils",
			"Loading",
			"$log",
			"Notify",
			"hb.api",
			"$stateParams",
			"ItemModal",
			"$modal",
			"PartnerTemplatesItemsEditModal",
			"hb.resources",
			PartnerTemplatesMaintenanceItemsController
		]);

	function PartnerTemplatesMaintenanceItemsController(utils, loading, $log, notify, api, $stateParams, ItemModal, $modal, PartnerTemplatesItemsEditModal, resources) {
		this.templateId = $stateParams["templateId"];
		this.is_maint_collapsed = false;
		this.utils = utils;
		this.validationErrors = resources.validationErrors;
		this.$log = $log;
		this.notify = notify;
		this.api = api;
		this.ItemModal = ItemModal;
		this.$modal = $modal;
		this.PartnerTemplatesItemsEditModal = PartnerTemplatesItemsEditModal;
		this.resources = resources.partnerTemplatesMaintenanceItems;
		this.today = this.utils.utils.getToday(false);
		this.next_month = this.utils.utils.getNextMonth();
		this.yyyy = this.utils.utils.getYear();
		this.datepicker = {
			format: "MMMM dd, yyyy"
		};
		this.days_after = [{
			name: "In 30 Days",
			value: 30
		}, {
			name: "In 60 Days",
			value: 60
		}, {
			name: "In 90 Days",
			value: 90
		}, {
			name: "In 120 Days",
			value: 120
		}, {
			name: "In 180 Days",
			value: 180
		}];
		this.frequencies = [{
			name: "Once",
			value: "Once"
		}, {
			name: "Annual",
			value: "Annual"
		}, {
			name: "Quarterly",
			value: "Quarterly"
		}, {
			name: "Monthly",
			value: "Monthly"
		}, {
			name: "Every Other Year",
			value: "Every-Other-Year"
		}, {
			name: "Every 3 Years",
			value: "Every-3-Years"
		}, {
			name: "Every 4 Years",
			value: "Every-4-Years"
		}, {
			name: "Every 5 Years",
			value: "Every-5-Years"
		}, {
			name: "Every 10 Years",
			value: "Every-10-Years"
		}, {
			name: "Every 40 Years",
			value: "Every-40-Years"
		}, {
			name: "Semi-Annual",
			value: "Semi-Annual"
		}, {
			name: "As Needed",
			value: "As Needed"
		}];
		this.maintenance_items = [{
			"name": "Smoke Detector Battery",
			"frequency": "Annual",
			"due_date": this.next_month
		}, {
			"name": "Clean Dryer Vent",
			"frequency": "Annual",
			"due_date": this.next_month
		}, {
			"name": "Sweep Chimney",
			"frequency": "Annual",
			"due_date": this.setDate('fall')
		}, {
			"name": "Pump Septic Tank",
			"frequency": "Annual",
			"due_date": this.setDate('summer')
		}, {
			"name": "Change A/C Filter",
			"frequency": "Quarterly",
			"due_date": this.next_month
		}, {
			"name": "Blow Out Sprinkler Lines",
			"frequency": "Annual",
			"due_date": this.setDate('fall')
		}, {
			"name": "Change Water Filter",
			"frequency": "Annual",
			"due_date": this.setDate('fall', 'filter')
		}, {
			"name": "Add Water Softener",
			"frequency": "Semi-Annual",
			"due_date": this.next_month
		}, {
			"name": "Clean Gutters",
			"frequency": "Annual",
			"due_date": this.setDate('fall', 'gutters')
		}, {
			"name": "Inspect Crawlspace",
			"frequency": "Annual",
			"due_date": this.setDate('spring')
		}];
		this.loading = loading;
		this.modal_displayed = false;
		this.call_create = false;
		this.validateDates();
	}

	PartnerTemplatesMaintenanceItemsController.prototype = {

		validateDates: function() {
			if (this.items && this.items.length > 0) {
				var x;
				for (x = 0; x < this.items.length; x++) {
					var item = this.items[x];
					var boolean_value = this.validateItem(item);
					if (boolean_value && !this.modal_displayed) {
						this.ItemModal.show(item);
						this.modal_displayed = true;
					}

				}
			}
		},

		setDate: function(season, item) {

			if (season === 'spring') {
				var spring = 'March 15, ' + this.setYear('March 15, ' + this.yyyy, this.today);
				return spring;
			}
			else if (season === 'summer') {
				var summer = 'June 15, ' + this.setYear('June 15, ' + this.yyyy, this.today);
				return summer;
			}
			else if (season === 'fall') {
				var fall = 'October 15, ' + this.setYear('October 15, ' + this.yyyy, this.today);
				return fall;
			}
			else if (item === 'filter') {
				return 'September 15, ' + this.setYear('September 15, ' + this.yyyy, this.today);
			}
			else if (item === 'gutters') {
				return 'November 1, ' + this.setYear('November 1, ' + this.yyyy, this.today);
			}
		},

		setYear: function(date_one, date_two) {
			var first = Date.parse(date_one);
			var second = Date.parse(date_two);
			if (first < second) {
				return this.yyyy + 1;
			}
			return this.yyyy;
		},

		remove: function(item) {
			var index = this.items.indexOf(item);
			var mi = this.items[index];
			if (mi.id) {
				this.api.maintenanceTemplates.destroy(mi.id).then(
					angular.bind(this, this.onRemoveSuccess, index),
					angular.bind(this, this.onRemoveError)
				);
			}
		},

		onRemoveSuccess: function(index, response) {
			this.items.splice(index, 1);
			this.notify.success("Item removed from template");
		},

		onRemoveError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
		},

		addItem: function() {
			var item = {
				name: "",
				frequency: "Annual",
				due_date_read_only: true,
				maintenance_template_contractor_types: []
			};
			this.PartnerTemplatesItemsEditModal.show({
				templateUrl: "partners/templates/form/maintenanceItems/edit/edit.tpl.html",
				controller: "PartnerTemplatesMaintenanceItemsEditController as ctrl",
				item: item,
				cfg: { showTypeaheads: true },
				index: -1,
				onSave: angular.bind(this, this.insertItem)
			});
		},

		edit: function(item) {
			var index = this.items.indexOf(item);
			this.PartnerTemplatesItemsEditModal.show({
				templateUrl: "partners/templates/form/maintenanceItems/edit/edit.tpl.html",
				controller: "PartnerTemplatesMaintenanceItemsEditController as ctrl",
				item: item,
				cfg: { showTypeaheads: true },
				index: index,
				onSave: angular.bind(this, this.insertItem)
			});
		},

		insertItem: function(item, index) {
			if (index > -1) {
				this.items[index] = item;
			}
			else {
				this.items.push(item);
			}
			this.validateDates();
		},

		validateItem: function(item) {
			if (item.due_date) {
				item.due_date = new Date(item.due_date);
				item.due_date_read_only = true;
				var first = Date.parse(item.due_date);
				var second = Date.parse(this.today);
				if (first < second) {
					return true;
				}
				return false;
			}
			else if (item.frequency === "As Needed") {
				item.due_date_read_only = false;
				return false;
			}
			else {
				item.due_date_read_only = false;
				return false;
			}
		},

		validateDate: function(item) {
			var first = Date.parse(item.due_date);
			var second = Date.parse(this.today);
			if (first < second) {
				return true;
			}
			return false;
		},

		send: function(item) {
			this.api.maintenanceTemplates.sendEmail(item.id).then(
				angular.bind(this, this.onEmailSuccess),
				angular.bind(this, this.onEmailError)
			);
		},

		onEmailSuccess: function(response) {
			this.notify.info("Please check your email");
		},

		onEmailError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
		}

	};

})();