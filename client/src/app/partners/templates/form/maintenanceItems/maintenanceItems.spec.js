describe("PartnerTemplatesMaintenanceItemsController", function() {

    var $rootScope,
        $scope,
        $controller,
        ctrl,
        items,
        api,
        notify,
        $log,
        loading,
        $q,
        ItemModal,
        $modal,
        PartnerTemplatesItemsEditModal;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        notify = _$injector_.get("Notify");
        PartnerTemplatesItemsEditModal = _$injector_.get("PartnerTemplatesItemsEditModal");

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        $log = {
            error: function(msg) {}
        };

        items = [{
            id: 1,
            "name": "Smoke Detector Battery",
            "frequency": "Annual",
            "due_date": "",
            "due_date_read_only": false,
            "interval_read_only": true
        }, {
            id: 2,
            "name": "Sweep Chimney",
            "frequency": "Annual",
            "due_date": ""
        }, {
            "name": "Pump Septic Tank",
            "frequency": "Annual",
            "due_date": ""
        }, {
            "name": "Change A/C Filter",
            "frequency": "Quarterly",
            "due_date": ""
        }, {
            "name": "Blow Out Sprinkler Lines",
            "frequency": "Annual",
            "due_date": ""
        }, {
            "name": "Change Water Filter",
            "frequency": "Annual",
            "due_date": ""
        }, {
            "name": "Add Water Softener",
            "frequency": "Semi-Annual",
            "due_date": "",
            "due_date_read_only": false,
            "interval_read_only": true
        }, {
            "name": "Clean Gutters",
            "frequency": "Annual",
            "due_date": ""
        }, {
            "name": "Inspect Crawlspace",
            "frequency": "Annual",
            "due_date": ""
        }];

        ItemModal = {
            show: function(opts) {}
        };

        $modal = {
            open: function(opts) {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("PartnerTemplatesMaintenanceItemsController", {
            "hb.api": api,
            "Notify": notify,
            "$log": $log,
            "Loading": loading,
            "ItemModal": ItemModal,
            "$modal": $modal,
            "PartnerTemplatesItemsEditModal": PartnerTemplatesItemsEditModal
        }, {
            items: []
        });

        $scope.ctrl = ctrl;
        $scope.$apply();
        $scope.$digest();

    }

    describe('ctrl.validateDates', function() {

        it('should call the itemModal show function', function() {

            spyOn(ItemModal, "show");

            createController();
            ctrl.items = [{
                name: "",
                frequency: "Annual",
                due_date: ctrl.next_month,
                due_date_read_only: false
            }];
            $rootScope.$apply();
            ctrl.items[0].due_date = "March 15, 2016";
            ctrl.validateDates();
            $rootScope.$apply();

            expect(ItemModal.show).toHaveBeenCalledWith(ctrl.items[0]);
        });

        it('should not call the itemModal show function', function() {

            spyOn(ItemModal, "show");

            createController();
            ctrl.items = [{
                name: "",
                frequency: "Annual",
                due_date: ctrl.next_month,
                due_date_read_only: false
            }];
            $rootScope.$apply();
            ctrl.items[0].due_date = ctrl.today;
            ctrl.validateDates();
            $rootScope.$apply();

            expect(ItemModal.show).not.toHaveBeenCalled();
        });
    });

    describe('ctrl.setDate', function() {

        it('should set date for maintenance item', function() {

            createController();
            ctrl.items = items;
            $rootScope.$apply();
            ctrl.today = new Date("January 15, 2007");
            ctrl.yyyy = ctrl.today.getFullYear();
            var date = ctrl.setDate('spring');
            $rootScope.$apply();

            expect(date).toBe("March 15, 2007");
        });

        it('should set date for maintenance item', function() {

            createController();
            ctrl.items = items;
            $rootScope.$apply();
            ctrl.today = new Date("June 14 2007");
            ctrl.yyyy = ctrl.today.getFullYear();
            var date = ctrl.setDate('summer');
            $rootScope.$apply();

            expect(date).toBe("June 15, 2007");
        });

        it('should set date for maintenance item', function() {

            createController();
            ctrl.items = items;
            $rootScope.$apply();
            ctrl.today = new Date("January 15, 2007");
            ctrl.yyyy = ctrl.today.getFullYear();
            var date = ctrl.setDate('fall');
            $rootScope.$apply();

            expect(date).toBe("October 15, 2007");
        });

        it('should set date for maintenance item', function() {

            createController();
            ctrl.items = items;
            $rootScope.$apply();
            ctrl.today = new Date("January 15, 2007");
            ctrl.yyyy = ctrl.today.getFullYear();
            var date = ctrl.setDate('winter', 'filter');
            $rootScope.$apply();

            expect(date).toBe("September 15, 2007");
        });

        it('should set date for maintenance item', function() {

            createController();
            ctrl.items = items;
            $rootScope.$apply();
            ctrl.today = new Date("January 15, 2007");
            ctrl.yyyy = ctrl.today.getFullYear();
            var date = ctrl.setDate('winter', 'gutters');
            $rootScope.$apply();

            expect(date).toBe("November 1, 2007");
        });

    });

    describe('ctrl.setYear', function() {

        it('should return the next year', function() {

            createController();
            $rootScope.$apply();
            var today = new Date();
            var yesterday = new Date(today.getTime());
            yesterday.setDate(today.getDate() - 1);
            var year = ctrl.setYear(yesterday, today);
            $rootScope.$apply();

            expect(year).toEqual(today.getFullYear() + 1);
        });

        it('should return the current year', function() {

            createController();
            $rootScope.$apply();
            var today = new Date();
            var tomorrow = new Date(today.getTime());
            tomorrow.setDate(today.getDate() + 1);
            var year = ctrl.setYear(tomorrow, today);
            $rootScope.$apply();

            expect(year).toEqual(today.getFullYear());
        });
    });

    describe('ctrl.remove', function() {

        it('should remove item at index 1 from items array', function() {

            spyOn(api.maintenanceTemplates, "destroy").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(notify, "success");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            ctrl.items = items;
            $rootScope.$apply();
            ctrl.remove(ctrl.items[0]);
            $rootScope.$apply();

            expect(api.maintenanceTemplates.destroy).toHaveBeenCalled();
            expect(notify.success).toHaveBeenCalled();
            expect(ctrl.items.length).toEqual(8);
        });

        it('should attempt to remove item from index 1 and destroy item but return an error', function() {

            spyOn(api.maintenanceTemplates, "destroy").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            ctrl.items = items;
            $rootScope.$apply();
            ctrl.remove(ctrl.items[0]);
            $rootScope.$apply();

            expect(api.maintenanceTemplates.destroy).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
            expect(ctrl.items.length).toEqual(9);
        });

    });

    describe('ctrl.insertItem', function() {
        it('updates the item to the list', function() {
            createController();
            ctrl.items = items;
            expect(ctrl.items[0].name).toEqual("Smoke Detector Battery");
            ctrl.insertItem({
                id: 1,
                "name": "Smoke Detector Battery 2",
                "frequency": "Annual",
                "due_date": "",
                "due_date_read_only": false,
                "interval_read_only": true
            }, 0);

            expect(ctrl.items[0].name).toEqual("Smoke Detector Battery 2");
        });

        it('adds the item to the list', function() {
            createController();
            ctrl.items = items;
            var length = ctrl.items.length;
            ctrl.insertItem({
                id: length,
                "name": "Smoke Detector Battery 2",
                "frequency": "Annual",
                "due_date": "",
                "due_date_read_only": false,
                "interval_read_only": true
            }, -1);

            expect(ctrl.items[length].id).toEqual(length);
        });

    });

    describe('ctrl.addItem', function() {

        it('should call modal', function() {

            createController();
            ctrl.items = items;
            $rootScope.$apply();
            spyOn(PartnerTemplatesItemsEditModal, "show");
            ctrl.addItem();

            $rootScope.$apply();

            expect(PartnerTemplatesItemsEditModal.show).toHaveBeenCalled();
        });

    });

    describe('ctrl.edit', function() {
        it('should call modal', function() {
            createController();
            ctrl.items = items;
            spyOn(PartnerTemplatesItemsEditModal, "show");
            ctrl.edit(ctrl.items[0]);


            expect(PartnerTemplatesItemsEditModal.show).toHaveBeenCalled();
        });
    });

    describe('ctrl.validateItem', function() {
        it('should call validateItem and return true', function() {

            createController();
            ctrl.items = items;
            $rootScope.$apply();
            var today = new Date();
            var yesterday = new Date(today.getTime());
            yesterday.setDate(today.getDate() - 1);
            ctrl.items[0].due_date = yesterday;
            var boolean_value = ctrl.validateItem(ctrl.items[0]);
            $rootScope.$apply();

            expect(boolean_value).toEqual(true);
            expect(ctrl.items[0].interval_read_only).toBe(true);
        });

        it('should call validateItem and return false', function() {

            createController();
            ctrl.items = items;
            $rootScope.$apply();
            var today = new Date();
            var tomorrow = new Date(today.getTime());
            tomorrow.setDate(today.getDate() + 1);
            ctrl.items[0].due_date = tomorrow;
            var boolean_value = ctrl.validateItem(ctrl.items[0]);
            $rootScope.$apply();

            expect(boolean_value).toEqual(false);
            expect(ctrl.items[0].interval_read_only).toBe(true);
        });

        it('should call validateItem and return false', function() {

            createController();
            ctrl.items = items;
            ctrl.items[6].due_date = null;
            $rootScope.$apply();
            var boolean_value = ctrl.validateItem(ctrl.items[6]);
            $rootScope.$apply();

            expect(boolean_value).toEqual(false);
            expect(ctrl.items[6].due_date_read_only).toBe(false);
        });

        it('should call validateItem and return false', function() {

            createController();
            ctrl.items = items;
            ctrl.items[6].frequency = "As Needed";
            ctrl.items[6].due_date = null;
            var boolean_value = ctrl.validateItem(ctrl.items[6]);

            expect(boolean_value).toEqual(false);
            expect(ctrl.items[6].due_date_read_only).toBe(false);
        });
    });

    describe('ctrl.validateDate', function() {

        it('should call validateDate and return true', function() {

            createController();
            var today = new Date();
            var yesterday = new Date(today.getTime());
            yesterday.setDate(today.getDate() - 1);
            var boolean_value = ctrl.validateDate({
                due_date: yesterday
            });
            $rootScope.$apply();

            expect(boolean_value).toEqual(true);
        });

        it('should call validateDate and return false', function() {

            createController();
            var today = new Date();
            var tomorrow = new Date(today.getTime());
            tomorrow.setDate(today.getDate() + 1);
            var boolean_value = ctrl.validateDate(tomorrow);
            $rootScope.$apply();

            expect(boolean_value).toEqual(false);
        });
    });

    describe('ctrl.send', function() {
        it('calls maintenanceTemplates sendEmail', function() {

            spyOn(api.maintenanceTemplates, "sendEmail").and.returnValue($q.when({
                data: "success"
            }));

            createController();
            ctrl.send({ id: 1 });

            expect(api.maintenanceTemplates.sendEmail).toHaveBeenCalled();
        });
    });

    describe('ctrl.onEmailSuccess', function() {
        it('calls notify info', function() {

            spyOn(notify, "info");

            createController();
            ctrl.onEmailSuccess({ data: {} });

            expect(notify.info).toHaveBeenCalled();
        });
    });

    describe('ctrl.onEmailError', function() {
        it('calls notify error', function() {

            spyOn(notify, "error");

            createController();
            ctrl.onEmailError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

});

describe('partnerTemplatesMaintenanceItems', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_, _$q_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        $scope.items = [];
        $scope.create = function() {};
        $scope.maintForm = {};

        element = $compile('<partner-templates-maintenance-items items="items" create="create" maint-form="maintForm"></partner-templates-maintenance-items>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the title', function() {
            expect(element.html()).toContain("Maintenance Items");
        });
    });
});