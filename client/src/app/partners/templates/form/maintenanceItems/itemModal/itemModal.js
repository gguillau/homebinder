(function() {
    "use strict";

    angular
        .module("hb.partner.templates.form.maintenanceItems.itemModal", [])
        .factory("ItemModal", [
            "ModalService",
            function(Modals) {
                return {
                    show: function() {
                        Modals.show({
                            templateUrl: "partners/templates/form/maintenanceItems/itemModal/itemModal.tpl.html",
                            controller: "ItemModalController as ctrl"
                        });
                    }
                };
            }
        ])
        .controller("ItemModalController", [
            "$location",
            "$modalInstance",
            ItemModalController
        ]);

    function ItemModalController($location, $modal) {
        this.$location = $location;
        this.$modal = $modal;
    }

    ItemModalController.prototype = {
        
        cancel: function() {
            this.$modal.dismiss("cancel");
        }
    };
})();