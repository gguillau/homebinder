describe("ItemModalController", function() {
    var $rootScope,
        $scope,
        $controller,
        $location,
        ctrl,
        data,
        $modalInstance,
        ItemModal,
        ModalService;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        ItemModal = _$injector_.get("ItemModal");
        ModalService = _$injector_.get("ModalService");
        $controller = _$controller_;

        $modalInstance = {
            dismiss: function(msg){}
        };
        
        data = {
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("ItemModalController", {
            "$location": $location,
            "$modalInstance": $modalInstance,
            "data": data
        });

        $scope.ctrl = ctrl;
    }

    describe('ctrl.cancel', function() {
        it('should call $modalInstance dismiss function', function() {

            spyOn($modalInstance, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.cancel();
            $rootScope.$apply();
            
            expect($modalInstance.dismiss).toHaveBeenCalledWith('cancel');
        });
    });

    describe('ItemModal', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            ItemModal.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});