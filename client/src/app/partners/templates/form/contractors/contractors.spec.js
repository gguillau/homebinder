describe("PartnerTemplatesContractorsController", function() {

    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $q,
        $log,
        loading,
        api,
        notify,
        PartnerTemplatesItemsEditModal,
        PartnerHomeProsAddForm;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");

        $log = {
            error: function() {}
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        notify = {
            success: function(msg) {},
            error: function(msg) {}
        };

        api = {
            contractorTemplates: {
                destroy: function(appId) {}
            }
        };

        PartnerTemplatesItemsEditModal = {
            show: function() {}
        };

        PartnerHomeProsAddForm = {
            show: function() {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("PartnerTemplatesContractorsController", {
            "hb.api": api,
            "Notify": notify,
            "Loading": loading,
            "$log": $log,
            "PartnerTemplatesItemsEditModal": PartnerTemplatesItemsEditModal,
            "PartnerHomeProsAddForm": PartnerHomeProsAddForm
        });

        $scope.ctrl = ctrl;
        $scope.$apply();
        $scope.$digest();

    }

    describe('ctrl.remove', function() {

        it('should remove contractor at index 0 from contractors array', function() {
            var contractor = { id: 1, name: "Adam Driver", type: "Plumbing", email: "adamdriver@gmail.com", phone: "6175002500" };

            spyOn(api.contractorTemplates, "destroy").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(notify, "success");

            createController();
            ctrl.contractors = [];
            ctrl.contractors.push(contractor);
            expect(ctrl.contractors.length).toEqual(1);
            $rootScope.$apply();
            ctrl.remove(contractor);
            $rootScope.$apply();

            expect(api.contractorTemplates.destroy).toHaveBeenCalled();
            expect(notify.success).toHaveBeenCalled();
            expect(ctrl.contractors.length).toEqual(0);
        });

        it('should call contractorTemplates destroy but return an error', function() {
            var contractor = { id: 1, name: "Adam Driver", type: "Plumbing", email: "adamdriver@gmail.com", phone: "6175002500" };

            spyOn(api.contractorTemplates, "destroy").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(notify, "error");
            spyOn($log, "error");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            ctrl.contractors = [];
            ctrl.contractors.push(contractor);
            expect(ctrl.contractors.length).toEqual(1);
            $rootScope.$apply();
            ctrl.remove(contractor);
            $rootScope.$apply();

            expect(api.contractorTemplates.destroy).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalledWith("error");
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(ctrl.contractors.length).toEqual(1);
        });

    });

    describe('ctrl.addContractor', function() {
        it('should call modal', function() {
            spyOn(PartnerHomeProsAddForm, "show");
            createController();
            ctrl.contractors = [];
            $rootScope.$apply();
            ctrl.addContractor();
            $rootScope.$apply();

            expect(PartnerHomeProsAddForm.show).toHaveBeenCalled();
        });

    });

    describe('ctrl.edit', function() {
        it('should call edit modal', function() {

            spyOn(PartnerTemplatesItemsEditModal, "show");

            createController();
            ctrl.contractors = [{ id: 1 }];
            ctrl.edit({ id: 1 });

            expect(PartnerTemplatesItemsEditModal.show).toHaveBeenCalled();
        });

    });

    describe('ctrl.insertContractor', function() {
        it('updates the item to the list', function() {
            createController();
            ctrl.contractors = [{ id: 1, name: "Home Pro" }];
            expect(ctrl.contractors[0].name).toEqual("Home Pro");
            ctrl.insertContractor({
                id: 1,
                "name": "Home Pro 2"
            }, 0);

            expect(ctrl.contractors[0].name).toEqual("Home Pro 2");
        });

        it('adds the item to the list', function() {
            createController();
            ctrl.contractors = [];
            var length = ctrl.contractors.length;
            ctrl.insertContractor({
                id: length,
                "name": "Home Pro 2"
            }, -1);

            expect(ctrl.contractors[length].id).toEqual(length);
        });

    });

});

describe('partnerTemplatesContractors', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_, _$q_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        $scope.contractors = [];
        $scope.create = function() {};
        $scope.contractorForm = {};

        element = $compile('<partner-templates-contractors contractors="contractors" create="create" contractor-form="contractorForm"></partner-templates-contractors>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the title', function() {
            expect(element.html()).toContain("Preferred Home Pros");
        });
    });
});