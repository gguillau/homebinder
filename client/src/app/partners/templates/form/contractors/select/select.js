(function() {
    "use strict";

    angular
        .module("hb.partner.templates.form.contractors.select", [])
        .factory("PartnerHomeProsSelectForm", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "partners/templates/form/contractors/select/select.tpl.html",
                            controller: "PartnerHomeProsSelectFormController as ctrl",
                            backdrop: "static",
                            keyboard: false,
                            resolveData: opts
                        });
                    }
                };
            }
        ])
        .controller("PartnerHomeProsSelectFormController", [
            "$modalInstance",
            "$state",
            "$stateParams",
            "hb.resources",
            "PartnerTemplatesItemsEditModal",
            "hb.api",
            "$log",
            "data",
            "$q",
            "hb.framework.indexBase",
            PartnerHomeProsSelectFormController
        ]);

    function PartnerHomeProsSelectFormController($modal, $state, $stateParams, resources, PartnerTemplatesItemsEditModal, api, $log, opts, $q, IndexBase) {
        var Model = function() {
            // call the parent class
            IndexBase.call(this);
            this.$modal = $modal;
            this.$state = $state;
            this.partnerId = $stateParams.partnerId;
            this.templateId = $stateParams.templateId;
            this.resources = angular.extend({}, this.resources, resources.partnersHomeProsSelectForm);
            this.itemsPerPage = 10;
            this.proArgs = {
                partner_id: $stateParams.partnerId
            };
            // delete prop
            this.nameProperty = "name";
            this.PartnerTemplatesItemsEditModal = PartnerTemplatesItemsEditModal;
            this.$stateParams = $stateParams;
            this.api = api;
            this.$log = $log;
            this.selected_items = [];
            this.submitted = false;
            this.onSave = opts.onSave;
            this.$q = $q;
            this.showLoading = false;
            // delete prop
            this.nameProperty = "name";
            this.sortOptions = [{
                orderBy: "partner_contractors.created_at",
                order: "desc",
                desc: this.resources.creationDate + " - " + this.resources.descending
            }, {
                orderBy: "partner_contractors.created_at",
                order: "asc",
                desc: this.resources.creationDate + " - " + this.resources.ascending
            }, {
                orderBy: "partner_contractors.name",
                order: "desc",
                desc: this.resources.name + " - " + this.resources.descending
            }, {
                orderBy: "partner_contractors.name",
                order: "asc",
                desc: this.resources.name + " - " + this.resources.ascending
            }];
            this.sortOption = this.sortOptions[0];
            this.orderBy = this.sortOption.orderBy;
            this.order = this.sortOption.order;
            // set the refresh API call
            this.refreshCall = api.partnerContractor.all;
            // set the delete API call
            this.deleteCall = api.partnerContractor.destroy;
            this.refresh();
        };

        Model.prototype = Object.create(IndexBase.prototype);

        angular.extend(Model.prototype, {

            addQueryArgs: function() {
                this.addArgs("partner_id");
            },

            addArgs: function(arg) {
                if (this.proArgs[arg]) {
                    this.queryArgs[arg] = this.proArgs[arg];
                }
                else {
                    delete this.queryArgs[arg];
                }
            },

            onRefreshSuccess: function(response) {
                this.items = response.data.items.map(function(item) {
                    return {
                        id: item.id,
                        selected: false,
                        name: item.name,
                        email: item.email,
                        types: item.types
                    };
                });
                this.totalItems = response.data.total;
                this.selected_items.forEach(angular.bind(this, function(selected) {
                    this.items.forEach(function(item) {
                        if (selected.id === item.id) {
                            item.selected = true;
                        }
                    });
                }));
            },

            onError: function(response) {
                this.$log.error(response.data);
            },

            onCheck: function(item) {
                if (item.selected) {
                    this.selected_items.push(item);
                }
                else {
                    var index = this.selected_items.indexOf(item);
                    if (index > -1) {
                        this.selected_items.splice(index, 1);
                    }
                }
            },

            create: function() {
                this.submitted = true;
                var promises = [];
                this.selected_items.forEach(angular.bind(this, function(item) {
                    var opts = {
                        partner_contractor_id: item.id,
                        binder_template_id: this.templateId
                    };
                    promises.push(
                        this.api.contractorTemplates.create(this.templateId, opts).then(
                            angular.bind(this, this.createProSuccess),
                            angular.bind(this, this.onError))
                    );
                }));

                this.$q.all(promises).then(
                    angular.bind(this, this.closeModal)
                );
            },

            createProSuccess: function(response) {
                this.onSave(response.data, -1);
            },

            closeModal: function() {
                this.$modal.close();
            },

            cancel: function() {
                this.$modal.dismiss("cancel");
            }

        });

        this.model = new Model();
    }

})();