describe("PartnerAgentsImportFormController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $modal,
        api,
        notify,
        $log,
        loading,
        $q,
        $stateParams,
        $state,
        uploader,
        PartnerAgentsImportForm,
        ModalService;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        PartnerAgentsImportForm = _$injector_.get("PartnerAgentsImportForm");
        ModalService = _$injector_.get("ModalService");

        $stateParams = {};

        $state = {
            go: function() {}
        };

        api = {
            partner: {
                uploadCSV: function(id, data) {}
            }
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        $modal = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };

        notify = {
            success: function() {},
            error: function() {}
        };

        $log = {
            error: function() {}
        };

        uploader = {
            api: {
                pendingUploads: function() {}
            }
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        $stateParams.partnerId = 100;
        ctrl = $controller("PartnerAgentsImportFormController", {
            "$modalInstance": $modal,
            "hb.api": api,
            "Notify": notify,
            "$log": $log,
            "Loading": loading,
            "$state": $state,
            "$stateParams": $stateParams
        });
        ctrl.uploader = uploader;
        $scope.ctrl = ctrl;
    }

    describe("ctrl.newAgent", function() {

        it("should call new agent state", function() {

            spyOn($modal, "dismiss");
            spyOn($state, "go");

            createController();
            $rootScope.$apply();
            ctrl.newAgent();
            $rootScope.$apply();

            expect($state.go).toHaveBeenCalledWith("partner.agents_new", {
                partnerId: 100
            });
            expect($modal.dismiss).toHaveBeenCalledWith('cancel');
        });

    });

    describe("ctrl.importAgent", function() {

        it("should call import agent state", function() {

            spyOn($modal, "dismiss");
            spyOn($state, "go");

            createController();
            $rootScope.$apply();
            ctrl.importAgent();
            $rootScope.$apply();

            expect($state.go).toHaveBeenCalledWith("partner.agents_import", {
                partnerId: 100
            });
            expect($modal.dismiss).toHaveBeenCalledWith('cancel');
        });
    });

    describe("ctrl.cancel", function() {

        it("should call $modalInstance dismiss function", function() {

            spyOn($modal, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.cancel();
            $rootScope.$apply();

            expect($modal.dismiss).toHaveBeenCalledWith('cancel');
        });

        it("should call $modalInstance close function", function() {

            spyOn($modal, "close");

            createController();
            $rootScope.$apply();
            ctrl.step = "three";
            ctrl.cancel();
            $rootScope.$apply();

            expect($modal.close).toHaveBeenCalled();
        });

    });

    describe('PartnerAgentsImportForm', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            PartnerAgentsImportForm.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });

});