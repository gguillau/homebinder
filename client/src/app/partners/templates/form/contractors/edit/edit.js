(function() {
    "use strict";

    angular
        .module("hb.partner.templates.form.contractors.edit", [])
        .directive('arrayRequired', function() {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function(scope, elem, attrs, ngModel) {
                    ngModel.$validators.arrayRequired = function(modelValue, viewValue) {
                        modelValue = modelValue || {};
                        return (modelValue.length > 0 ? true : false);
                    };
                }
            };
        })
        .controller("PartnerTemplatesContractorsEditController", [
            'PartnerTemplatesItemsEditModalController',
            "hb.resources",
            'data',
            "$modalInstance",
            "hb.api",
            "Notify",
            "$filter",
            "Context",
            "Address",
            "$log",
            "HomeProSelect",
            PartnerTemplatesContractorsEditController
        ]);

    function PartnerTemplatesContractorsEditController(PartnerTemplatesItemsEditModalController, resources, opts, $modal, api, notify, $filter, context, Address, $log, HomeProSelect) {

        var Model = function() {
            // call the parent class
            PartnerTemplatesItemsEditModalController.call(this);
            // set the resources
            this.resources = resources.partnerTemplatesContractorsEdit;
            // the item being updated/created
            this.item = opts.item;
            this.partner_contractor_types = [];
            this.partner_contractor_sub_types = [];
            this.filtered_types = [];
            this.filtered_sub_types = [];
            this.filtered_partner_contractor_types = [];
            this.filtered_partner_contractor_sub_types = [];
            // the call that updates the item in the list view
            this.onSave = opts.onSave;
            // the index of the item in the items list
            this.index = opts.index;
            // the createCall
            this.createCall = api.contractorTemplates.create;
            // the updateCall
            this.updateCall = api.contractorTemplates.update;
            // set the modalInstance
            this.$modal = $modal;
            this.homeProLookupCfg = {
                homeProSelected: angular.bind(this, this.homeProSelected)
            };
            this.url_regex = /^((?:http|ftp)s?:\/\/)(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|localhost|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(?::\d+)?(?:\/?|[\/?]\S+)$/i;
            // get the current partner
            this.init();
        };

        Model.prototype = Object.create(PartnerTemplatesItemsEditModalController.prototype);

        angular.extend(Model.prototype, {
            homeProSelected: function(pro) {
                this.item.partner_contractor_attributes.name = pro.name;
                this.item.partner_contractor_attributes.email = pro.email;
                this.item.partner_contractor_attributes.contractor_id = pro.id;
            },

            searchTypes: function(value) {
                // don't do the search when no search value is provided just clear the list
                if (value === null ||
                    value === undefined ||
                    value.length < 3) {
                    this.typeOptions = [];
                    return;
                }

                var opts = {
                    page: 1,
                    count: 50,
                    search: value,
                    verified: true
                };

                api.contractorType.all(opts).then(
                    angular.bind(this, this.searchComplete),
                    angular.bind(this, this.searchError)
                );
            },

            searchComplete: function(response) {
                this.typeOptions = response.data.items.filter(angular.bind(this, function(item) {
                    var array = $filter("filter")(this.item.partner_contractor_attributes.partner_contractor_types, {
                        type: {
                            name: item.name
                        }
                    });
                    return array.length < 1;
                }));
            },

            searchError: function(response) {
                notify.error(response.data);
            },

            onTypeSelect: function($select, $item) {
                // remove the item first and then add back again
                var index = this.item.partner_contractor_attributes.partner_contractor_types.indexOf($item);
                if (index > -1) {
                    this.item.partner_contractor_attributes.partner_contractor_types.splice(index, 1);
                }

                var hash = this.findExisting($item);
                if (hash.index < 0) {
                    if (hash.item.id) {
                        this.item.partner_contractor_attributes.partner_contractor_types.push({ type: { id: hash.item.id, name: hash.item.name } });
                        hash.item.sub_types.forEach(angular.bind(this, function(sub_type) {
                            this.onSubTypeSelect({}, sub_type);
                        }));
                    }
                    else {
                        this.item.partner_contractor_attributes.partner_contractor_types.push({ type: { name: hash.item.name } });
                    }
                }
            },

            onTypeRemove: function($item) {
                if (!this.item.partner_contractor_attributes.partner_contractor_types) {
                    this.item.partner_contractor_attributes.partner_contractor_types = [];
                }
                var hash = this.findExisting($item);
                if (hash.index > -1) {
                    this.item.partner_contractor_attributes.partner_contractor_types.splice(hash.index, 1);
                }
            },

            tagTransform: function(newTag) {
                var item = {
                    name: newTag,
                    sub_types: []
                };

                return item;
            },

            searchSubTypes: function(value) {
                // don't do the search when no search value is provided just clear the list
                if (value === null ||
                    value === undefined ||
                    value.length < 3) {
                    this.subTypeOptions = [];
                    return;
                }

                var opts = {
                    page: 1,
                    count: 50,
                    search: value
                };

                api.contractorSubType.all(opts).then(
                    angular.bind(this, this.searchSubComplete),
                    angular.bind(this, this.searchError)
                );
            },

            searchSubComplete: function(response) {
                this.subTypeOptions = response.data.items.filter(angular.bind(this, function(item) {
                    var array = $filter("filter")(this.item.partner_contractor_attributes.partner_contractor_sub_types, {
                        sub_type: {
                            name: item.name
                        }
                    });
                    return array.length < 1;
                }));
            },

            onSubTypeSelect: function($select, $item) {
                //remove the item first and then add back again
                var index = this.item.partner_contractor_attributes.partner_contractor_sub_types.indexOf($item);
                if (index > -1) {
                    this.item.partner_contractor_attributes.partner_contractor_sub_types.splice(index, 1);
                }

                var hash = this.findExistingSubTypes($item);
                if (hash.index < 0) {
                    if (hash.item.id) {
                        this.item.partner_contractor_attributes.partner_contractor_sub_types.push({ sub_type: { id: hash.item.id, name: hash.item.name } });
                    }
                    else {
                        this.item.partner_contractor_attributes.partner_contractor_sub_types.push({ sub_type: { name: hash.item.name } });
                    }
                }
            },

            onSubTypeRemove: function($item) {
                var hash = this.findExistingSubTypes($item);
                if (hash.index > -1) {
                    this.item.partner_contractor_attributes.partner_contractor_sub_types.splice(hash.index, 1);
                }
            },

            findExisting: function(item) {
                var name = null;
                if (!item.id && !item.isTag) {
                    name = item.type.name.toLowerCase();
                    item = { name: name };
                }
                return { index: this.item.partner_contractor_attributes.partner_contractor_types.indexOf(item), item: item };
            },

            findExistingSubTypes: function(item) {
                var name = null,
                    index = null;
                if (!item.id && !item.isTag) {
                    name = item.sub_type.name.toLowerCase();
                    item = { name: name };
                }

                var array = $filter("filter")(this.item.partner_contractor_attributes.partner_contractor_sub_types, {
                    sub_type: {
                        name: item.name
                    }
                }, true);

                if (array.length > 0) {
                    index = 0;
                }
                else {
                    index = -1;
                }

                return { index: index, item: item };
            },

            init: function() {
                this.address = Address;
                context.getPartner().then(angular.bind(this, function(partner) {
                    this.partner = partner;
                    this.country = Address.findCountryByCode(this.partner.address.country);
                    this.states = Address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                        return state.country.value === this.country.value;
                    }));
                }));
            },

            beforeSave: function() {
                this.filtered_types = this.item.partner_contractor_attributes.partner_contractor_types.filter(function(partner_contractor_type) {
                    return !partner_contractor_type.id && !partner_contractor_type.type.id;
                });

                this.filtered_partner_contractor_types = this.item.partner_contractor_attributes.partner_contractor_types.filter(function(partner_contractor_type) {
                    return partner_contractor_type.id || partner_contractor_type.type.id;
                });

                this.filtered_sub_types = this.item.partner_contractor_attributes.partner_contractor_sub_types.filter(function(partner_contractor_sub_type) {
                    return !partner_contractor_sub_type.id && !partner_contractor_sub_type.sub_type.id;
                });

                this.filtered_partner_contractor_sub_types = this.item.partner_contractor_attributes.partner_contractor_sub_types.filter(function(partner_contractor_sub_type) {
                    return partner_contractor_sub_type.id || partner_contractor_sub_type.sub_type.id;
                });

                this.data = {
                    binder_template_id: this.templateId,
                    partner_contractor_attributes: {
                        id: this.item.partner_contractor_attributes.id,
                        partner_id: this.item.partner_contractor_attributes.partner_id,
                        name: this.item.partner_contractor_attributes.name,
                        website: this.item.partner_contractor_attributes.website,
                        phone: "+" + this.country.code + this.item.partner_contractor_attributes.phone.national,
                        email: this.item.partner_contractor_attributes.email,
                        partner_contractor_types_attributes: this.filtered_partner_contractor_types.map(function(partner_contractor_type) {
                            if (partner_contractor_type.id) {
                                return { id: partner_contractor_type.id };
                            }
                            else if (partner_contractor_type.type.id) {
                                return { contractor_category_id: partner_contractor_type.type.id };
                            }
                        }),
                        types_attributes: this.filtered_types.map(function(partner_contractor_type) {
                            if (!partner_contractor_type.id && !partner_contractor_type.type.id) {
                                return { name: partner_contractor_type.type.name };
                            }
                        }),
                        partner_contractor_sub_types_attributes: this.filtered_partner_contractor_sub_types.map(function(partner_contractor_sub_type) {
                            if (partner_contractor_sub_type.id) {
                                return { id: partner_contractor_sub_type.id };
                            }
                            else if (partner_contractor_sub_type.sub_type.id) {
                                return { contractor_sub_category_id: partner_contractor_sub_type.sub_type.id };
                            }
                        }),
                        sub_types_attributes: this.filtered_sub_types.map(function(partner_contractor_sub_type) {
                            if (!partner_contractor_sub_type.id && !partner_contractor_sub_type.sub_type.id) {
                                return { name: partner_contractor_sub_type.sub_type.name };
                            }
                        }),
                        send_email_invitation: this.item.partner_contractor_attributes.send_email_invitation,
                        contractor_id: this.item.partner_contractor_attributes.contractor_id,
                        secondary_name: this.item.partner_contractor_attributes.secondary_name,
                        secondary_email: this.item.partner_contractor_attributes.secondary_email,
                        secondary_phone: "+" + this.country.code + this.item.partner_contractor_attributes.secondary_phone.national,
                        secondary_notes: this.item.partner_contractor_attributes.secondary_notes,
                        address_attributes: {
                            id: this.item.partner_contractor_attributes.address.id,
                            address1: this.item.partner_contractor_attributes.address.address1,
                            address2: this.item.partner_contractor_attributes.address.address2,
                            city: this.item.partner_contractor_attributes.address.city,
                            state: this.item.partner_contractor_attributes.address.state,
                            zip: this.item.partner_contractor_attributes.address.zip,
                            country: this.country.value
                        }
                    }
                };

                if (this.item && !this.item.id && !this.item.partner_contractor_attributes.contractor_id) {
                    this.data.partner_contractor_attributes.contractor_attributes = {
                        name: this.item.partner_contractor_attributes.name,
                        url: this.item.partner_contractor_attributes.website,
                        phone: "+" + this.country.code + this.item.partner_contractor_attributes.phone.national,
                        email: this.item.partner_contractor_attributes.email,
                        created_by: this.currentUser.id,
                        contractor_types_attributes: this.filtered_partner_contractor_types.map(function(partner_contractor_type) {
                            if (partner_contractor_type.id) {
                                return { id: partner_contractor_type.id };
                            }
                            else if (partner_contractor_type.type.id) {
                                return { contractor_category_id: partner_contractor_type.type.id };
                            }
                        }),
                        types_attributes: this.filtered_types.map(function(partner_contractor_type) {
                            if (!partner_contractor_type.id && !partner_contractor_type.type.id) {
                                return { name: partner_contractor_type.type.name };
                            }
                        }),
                        contractor_sub_types_attributes: this.filtered_partner_contractor_sub_types.map(function(partner_contractor_sub_type) {
                            if (partner_contractor_sub_type.id) {
                                return { id: partner_contractor_sub_type.id };
                            }
                            else if (partner_contractor_sub_type.sub_type.id) {
                                return { contractor_sub_category_id: partner_contractor_sub_type.sub_type.id };
                            }
                        }),
                        sub_types_attributes: this.filtered_sub_types.map(function(partner_contractor_sub_type) {
                            if (!partner_contractor_sub_type.id && !partner_contractor_sub_type.sub_type.id) {
                                return { name: partner_contractor_sub_type.sub_type.name };
                            }
                        }),
                        address_attributes: {
                            address1: this.item.partner_contractor_attributes.address.address1,
                            address2: this.item.partner_contractor_attributes.address.address2,
                            city: this.item.partner_contractor_attributes.address.city,
                            state: this.item.partner_contractor_attributes.address.state,
                            country: this.country.value
                        }
                    };
                }
            },

            checkForExisting: function() {
                api.contractor.all({ count: 5, searchType: "fuzzy", name: this.item.partner_contractor_attributes.name.toLowerCase(), email: this.item.partner_contractor_attributes.email.toLowerCase(), phone: "+" + this.country.code + this.item.partner_contractor_attributes.phone.national }).then(
                    angular.bind(this, this.existingSuccess),
                    angular.bind(this, this.existingError));
            },

            existingSuccess: function(response) {
                var email = this.item.partner_contractor_attributes.email ? this.item.partner_contractor_attributes.email.toLowerCase() : "";
                var contractors = response.data.items;
                if (contractors.length > 0) {
                    HomeProSelect.show({
                        contractors: contractors,
                        email: email,
                        closed: angular.bind(this, this.setHomePro)
                    });
                }
                else {
                    this.createOrUpdate();
                }
            },

            existingError: function(response) {
                $log.error(response);
                notify.error(response.data);
            },

            setHomePro: function(pro) {
                if (pro) {
                    this.homeProSelected(pro);
                    this.createOrUpdate();
                }
                else {
                    this.createOrUpdate();
                }
            },

            save: function() {
                this.form.$submitted = true;
                if (this.form.$invalid) {
                    return;
                }

                if (this.item && this.item.id) {
                    this.createOrUpdate();
                }
                else {
                    // check for existing home pro
                    this.checkForExisting();
                }
            },

            createOrUpdate: function() {

                this.beforeSave();

                if (this.item && this.item.id) {
                    this.updateCall(this.item.id, this.data).then(
                        angular.bind(this, this.onSaveSuccess),
                        angular.bind(this, this.onError)
                    );
                }
                else {
                    this.createCall(this.binder_template ? this.binder_template.id : this.templateId, this.data).then(
                        angular.bind(this, this.onSaveSuccess),
                        angular.bind(this, this.onError)
                    );
                }
            }
        });

        this.model = new Model();
    }

})();