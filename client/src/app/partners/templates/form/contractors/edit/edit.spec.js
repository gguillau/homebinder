describe("PartnerTemplatesContractorsEditController", function() {
    var $rootScope,
        $controller,
        PartnerTemplatesItemsEditModalController,
        $modalInstance,
        ctrl,
        api,
        $q,
        notify,
        HomeProSelect,
        defer;


    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the $modalInstance service
        $provide.factory("$modalInstance", function() {
            return {
                close: function() {},
                dismiss: function() {}
            };
        });
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getPartner: function() {
                    defer = $q.defer();
                    defer.resolve({ id: 268, address: { id: 1 } });
                    return defer.promise;
                }
            };
        });
    }));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        PartnerTemplatesItemsEditModalController = _$injector_.get("PartnerTemplatesItemsEditModalController");
        $modalInstance = _$injector_.get("$modalInstance");
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        notify = _$injector_.get("Notify");
        HomeProSelect = _$injector_.get("HomeProSelect");
    }));

    function createController() {
        ctrl = $controller("PartnerTemplatesContractorsEditController", {
            "PartnerTemplatesItemsEditModalController": PartnerTemplatesItemsEditModalController,
            "data": {},
            "$modalInstance": $modalInstance
        });
        $rootScope.$apply();
    }

    describe('#homeProSelected', function() {
        it("sets the pro and calls populate", function() {
            createController();
            ctrl.model.item = {
                id: 1,
                partner_contractor_attributes: {
                    contractor_id: 2
                }
            };
            ctrl.model.homeProSelected({ id: 1, name: "selected" });

            expect(ctrl.model.item.partner_contractor_attributes.contractor_id).toEqual(1);
        });
    });

    describe("searchTypes", function() {
        it("calls api contractorType all", function() {
            spyOn(api.contractorType, "all").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.model.searchTypes("search");

            expect(api.contractorType.all).toHaveBeenCalled();
        });

        it("does not call api contractorSubType all", function() {
            spyOn(api.contractorType, "all").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.model.searchTypes(undefined);

            expect(api.contractorType.all).not.toHaveBeenCalled();
        });
    });

    describe("searchComplete", function() {
        it("sets the typeOptions", function() {
            createController();
            ctrl.model.item = { partner_contractor_attributes: { partner_contractor_types: [] } };
            ctrl.model.searchComplete({ data: { items: [{ id: 1 }] } });

            expect(ctrl.model.typeOptions.length).toEqual(1);
        });
    });

    describe("searchSubTypes", function() {
        it("calls api contractorSubType all", function() {
            spyOn(api.contractorSubType, "all").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.model.searchSubTypes("search");

            expect(api.contractorSubType.all).toHaveBeenCalled();
        });

        it("does not call api contractorSubType all", function() {
            spyOn(api.contractorSubType, "all").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.model.searchSubTypes(undefined);

            expect(api.contractorSubType.all).not.toHaveBeenCalled();
        });
    });

    describe("searchSubComplete", function() {
        it("sets the subTypeOptions", function() {
            createController();
            ctrl.model.item = { partner_contractor_attributes: { partner_contractor_sub_types: [] } };
            ctrl.model.searchSubComplete({ data: { items: [{ id: 1 }] } });

            expect(ctrl.model.subTypeOptions.length).toEqual(1);
        });
    });

    describe("searchError", function() {
        it("calls notify error", function() {
            spyOn(notify, "error");

            createController();
            ctrl.model.searchError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("onTypeSelect", function() {
        it("calls findExisting and updates the sub_types", function() {
            createController();
            var type = { id: 1, name: "test" };
            ctrl.model.item = { partner_contractor_attributes: { partner_contractor_types: [type] } };
            spyOn(ctrl.model, "findExisting").and.returnValue({ index: -1, item: { id: 1, name: "test", sub_types: [{ id: 1, name: "cpa" }] } });
            spyOn(ctrl.model, "onSubTypeSelect");
            ctrl.model.onTypeSelect({}, type);

            expect(ctrl.model.item.partner_contractor_attributes.partner_contractor_types.length).toEqual(1);
            expect(ctrl.model.onSubTypeSelect).toHaveBeenCalled();
        });
    });

    describe("onSubTypeSelect", function() {
        it("calls findExistingSubTypes and updates the sub_types", function() {
            createController();
            var type = { id: 1, name: "test" };
            ctrl.model.item = { partner_contractor_attributes: { partner_contractor_sub_types: [type] } };
            spyOn(ctrl.model, "findExistingSubTypes").and.returnValue({ index: -1, item: { name: "test" } });
            ctrl.model.onSubTypeSelect({}, type);

            expect(ctrl.model.item.partner_contractor_attributes.partner_contractor_sub_types.length).toEqual(1);
            expect(ctrl.model.findExistingSubTypes).toHaveBeenCalled();
        });
    });

    describe("onTypeRemove", function() {
        it("calls findExisting and updates the item's types", function() {
            createController();
            var type = { id: 1, name: "test", sub_types: ["test"] };
            ctrl.model.item = { partner_contractor_attributes: { partner_contractor_types: [type] } };
            expect(ctrl.model.item.partner_contractor_attributes.partner_contractor_types.length).toEqual(1);
            spyOn(ctrl.model, "findExisting").and.returnValue({ index: 0, name: "test" });
            ctrl.model.onTypeRemove(type);

            expect(ctrl.model.item.partner_contractor_attributes.partner_contractor_types.length).toEqual(0);
        });

        it("calls findExisting and updates the item's types", function() {
            createController();
            var type = { id: 1, name: "test", sub_types: ["test"] };
            ctrl.model.item = { partner_contractor_attributes: { partner_contractor_types: undefined } };
            spyOn(ctrl.model, "findExisting").and.returnValue({ index: 0, name: "test" });
            ctrl.model.onTypeRemove(type);

            expect(ctrl.model.item.partner_contractor_attributes.partner_contractor_types.length).toEqual(0);
        });
    });

    describe("onSubTypeRemove", function() {
        it("calls findExistingSubTypes and updates the item's sub_types", function() {
            createController();
            var type = { id: 1, name: "test" };
            ctrl.model.item = { partner_contractor_attributes: { partner_contractor_sub_types: [type] } };
            expect(ctrl.model.item.partner_contractor_attributes.partner_contractor_sub_types.length).toEqual(1);
            spyOn(ctrl.model, "findExistingSubTypes").and.returnValue({ index: 0, name: "test" });
            ctrl.model.onSubTypeRemove(type);

            expect(ctrl.model.item.partner_contractor_attributes.partner_contractor_sub_types.length).toEqual(0);
        });
    });

    describe("findExisting", function() {
        it("returns an index of -1", function() {
            createController();
            ctrl.model.item = { partner_contractor_attributes: { partner_contractor_types: [] } };
            var hash = ctrl.model.findExisting({ type: { name: "test" } });

            expect(hash.index).toEqual(-1);
        });
    });

    describe("findExistingSubTypes", function() {
        it("returns an index of -1", function() {
            createController();
            ctrl.model.item = { partner_contractor_attributes: { partner_contractor_sub_types: [] } };
            var hash = ctrl.model.findExistingSubTypes({ sub_type: { name: "test" } });

            expect(hash.index).toEqual(-1);
        });
    });

    describe('beforeSave', function() {
        it("sets the data object", function() {
            createController();
            ctrl.model.currentUser = {};
            ctrl.model.item = { partner_contractor_attributes: { name: "test", phone: {}, address: {}, secondary_phone: {}, partner_contractor_types: [], partner_contractor_sub_types: [] } };
            ctrl.model.beforeSave();

            expect(ctrl.model.data.partner_contractor_attributes.name).toEqual("test");
            expect(ctrl.model.data.partner_contractor_attributes.contractor_attributes.name).toEqual("test");
        });
        
        it("does not set contractor_attributes since contractor_id is present", function() {
            createController();
            ctrl.model.currentUser = {};
            ctrl.model.item = { partner_contractor_attributes: { name: "test", phone: {}, contractor_id: 1, address: {}, secondary_phone: {}, partner_contractor_types: [], partner_contractor_sub_types: [] } };
            ctrl.model.beforeSave();

            expect(ctrl.model.data.partner_contractor_attributes.name).toEqual("test");
            expect(ctrl.model.data.partner_contractor_attributes.contractor_attributes).toBe(undefined);
        });
    });

    describe('#checkForExisting', function() {
        it("calls contractor all", function() {
            createController();
            spyOn(api.contractor, "all").and.returnValue($q.when({ data: { items: [] } }));
            ctrl.model.item = { partner_contractor_attributes: { name: "test", email: "email", phone: {} } };
            ctrl.model.checkForExisting();

            expect(api.contractor.all).toHaveBeenCalled();
        });
    });

    describe('#existingSuccess', function() {
        it("calls Home Pro Select modal", function() {
            createController();
            spyOn(HomeProSelect, "show");

            ctrl.model.item = { partner_contractor_attributes: { email: "test@gmail.com" } };
            ctrl.model.existingSuccess({ data: { items: [{ id: 1 }] } });

            expect(HomeProSelect.show).toHaveBeenCalled();
        });

        it("calls createOrUpdate", function() {
            createController();
            ctrl.model.item = { partner_contractor_attributes: {} };
            spyOn(ctrl.model, "createOrUpdate");

            ctrl.model.existingSuccess({ data: { items: [] } });

            expect(ctrl.model.createOrUpdate).toHaveBeenCalled();
        });
    });

    describe('#existingError', function() {
        it("calls notify error", function() {
            createController();
            spyOn(notify, "error");

            ctrl.model.existingError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe('#setHomePro', function() {
        it("sets the correct fields and calls createOrUpdate", function() {
            createController();
            spyOn(ctrl.model, "createOrUpdate");

            ctrl.model.item = { partner_contractor_attributes: {} };
            ctrl.model.setHomePro({ name: "test" });

            expect(ctrl.model.item.partner_contractor_attributes.name).toEqual("test");
            expect(ctrl.model.createOrUpdate).toHaveBeenCalled();
        });

        it("calls createOrUpdate", function() {
            createController();
            spyOn(ctrl.model, "createOrUpdate");

            ctrl.model.setHomePro();

            expect(ctrl.model.createOrUpdate).toHaveBeenCalled();
        });
    });

    describe('#save', function() {
        it("returns and does not call createOrUpdate or checkForExisting", function() {
            createController();
            spyOn(ctrl.model, "createOrUpdate");
            spyOn(ctrl.model, "checkForExisting");
            ctrl.model.form = { $invalid: true };

            ctrl.model.save();

            expect(ctrl.model.createOrUpdate).not.toHaveBeenCalled();
            expect(ctrl.model.checkForExisting).not.toHaveBeenCalled();
        });

        it("calls createOrUpdate but not checkForExisting", function() {
            createController();
            spyOn(ctrl.model, "createOrUpdate");
            spyOn(ctrl.model, "checkForExisting");
            ctrl.model.form = { $invalid: false };
            ctrl.model.item = { id: 1, partner_contractor_attributes: { id: 1 } };
            ctrl.model.save();

            expect(ctrl.model.createOrUpdate).toHaveBeenCalled();
            expect(ctrl.model.checkForExisting).not.toHaveBeenCalled();
        });

        it("calls checkForExisting but not createOrUpdate", function() {
            createController();
            spyOn(ctrl.model, "createOrUpdate");
            spyOn(ctrl.model, "checkForExisting");
            ctrl.model.form = { $invalid: false };
            ctrl.model.item = { partner_contractor_attributes: {} };
            ctrl.model.save();

            expect(ctrl.model.createOrUpdate).not.toHaveBeenCalled();
            expect(ctrl.model.checkForExisting).toHaveBeenCalled();
        });
    });

    describe('#createOrUpdate', function() {
        it("calls updateCall", function() {
            createController();
            spyOn(ctrl.model, "updateCall").and.returnValue($q.when({ data: {} }));
            ctrl.model.item = { id: 1, partner_contractor_attributes: { phone: {}, address: {}, secondary_phone: {}, partner_contractor_types: [], partner_contractor_sub_types: [] } };
            ctrl.model.createOrUpdate();

            expect(ctrl.model.updateCall).toHaveBeenCalled();
        });

        it("calls createCall", function() {
            createController();
            spyOn(ctrl.model, "createCall").and.returnValue($q.when({ data: {} }));
            spyOn(ctrl.model, "checkForExisting");
            ctrl.model.form = { $invalid: false };
            ctrl.model.item = { partner_contractor_attributes: { phone: {}, address: {}, secondary_phone: {}, partner_contractor_types: [], partner_contractor_sub_types: [] } };
            ctrl.model.currentUser = {};
            ctrl.model.createOrUpdate();

            expect(ctrl.model.createCall).toHaveBeenCalled();
        });
    });

});