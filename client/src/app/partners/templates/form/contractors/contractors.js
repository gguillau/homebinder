(function() {
	"use strict";

	angular
		.module("hb.partner.templates.form.contractors", [
			"hb.partner.templates.form.contractors.add",
			"hb.partner.templates.form.contractors.edit",
			"hb.partner.templates.form.contractors.select"
		])
		.directive("partnerTemplatesContractors", function() {
			return {
				restrict: "E",
				templateUrl: "partners/templates/form/contractors/contractors.tpl.html",
				controller: "PartnerTemplatesContractorsController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {
					contractors: "=",
					create: "&",
					contractForm: "="
				}
			};
		})
		.controller("PartnerTemplatesContractorsController", [
			'hb.api',
			'hb.utils',
			'Loading',
			'$log',
			'Notify',
			'$stateParams',
			"PartnerTemplatesItemsEditModal",
			"hb.resources",
			"PartnerHomeProsAddForm",
			PartnerTemplatesContractorsController
		]);

	function PartnerTemplatesContractorsController(api, utils, loading, $log, notify, $stateParams, PartnerTemplatesItemsEditModal, resources, PartnerHomeProsAddForm) {
		this.templateId = $stateParams["templateId"];
		this.api = api;
		this.utils = utils;
		this.validationErrors = resources.validationErrors;
		this.$log = $log;
		this.notify = notify;
		this.loading = loading;
		this.PartnerTemplatesItemsEditModal = PartnerTemplatesItemsEditModal;
		this.resources = resources.partnerTemplatesContractors;
		this.is_contract_collapsed = false;
		this.PartnerHomeProsAddForm = PartnerHomeProsAddForm;
	}

	PartnerTemplatesContractorsController.prototype = {

		remove: function(item) {
			var index = this.contractors.indexOf(item);
			var contr = this.contractors[index];
			if (contr.id) {
				this.api.contractorTemplates.destroy(contr.id).then(
					angular.bind(this, this.onRemoveSuccess, index),
					angular.bind(this, this.onRemoveError)
				);
			}
		},

		onRemoveSuccess: function(index, response) {
			this.contractors.splice(index, 1);
			this.notify.success(this.resources.removed);
		},

		onRemoveError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
		},

		addContractor: function() {
			this.PartnerHomeProsAddForm.show({ onSave: angular.bind(this, this.insertContractor) });
		},

		edit: function(item) {
			var index = this.contractors.indexOf(item);
			this.PartnerTemplatesItemsEditModal.show({
				templateUrl: "partners/templates/form/contractors/edit/edit.tpl.html",
				controller: "PartnerTemplatesContractorsEditController as ctrl",
				item: { id: item.id, partner_contractor_id: item.partner_contractor_id, binder_template_id: item.binder_template_id, partner_contractor_attributes: item.partner_contractor },
				index: index,
				onSave: angular.bind(this, this.insertContractor)
			});
		},

		insertContractor: function(item, index) {
			if (index > -1) {
				this.contractors[index] = item;
			}
			else {
				this.contractors.push(item);
			}
		}
	};

})();