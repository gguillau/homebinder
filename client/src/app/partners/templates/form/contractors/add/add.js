(function() {
    "use strict";

    angular
        .module("hb.partner.templates.form.contractors.add", [])
        .factory("PartnerHomeProsAddForm", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "partners/templates/form/contractors/add/add.tpl.html",
                            controller: "PartnerHomeProsAddFormController as ctrl",
                            backdrop: "static",
                            keyboard: false,
                            resolveData: opts,
                            closed: opts.closed
                        });
                    }
                };
            }
        ])
        .controller("PartnerHomeProsAddFormController", [
            "$modalInstance",
            "$state",
            "$stateParams",
            "hb.resources",
            "PartnerTemplatesItemsEditModal",
            "PartnerHomeProsSelectForm",
            "data",
            PartnerHomeProsAddFormController
        ]);

    function PartnerHomeProsAddFormController($modal, $state, $stateParams, resources, PartnerTemplatesItemsEditModal, PartnerHomeProsSelectForm, opts) {
        this.$modal = $modal;
        this.$state = $state;
        this.partnerId = $stateParams.partnerId;
        this.resources = resources.partnersHomeProsAddForm;
        this.PartnerTemplatesItemsEditModal = PartnerTemplatesItemsEditModal;
        this.PartnerHomeProsSelectForm = PartnerHomeProsSelectForm;
        this.onSave = opts.onSave;
    }

    PartnerHomeProsAddFormController.prototype = {

        add: function() {
            this.$modal.dismiss("cancel");
            var item = {
                partner_contractor_attributes: {
                    name: "",
                    partner_id: this.partnerId,
                    email: "",
                    notes: "",
                    partner_contractor_types: [],
                    partner_contractor_sub_types: [],
                    address: {},
                    phone: {},
                    secondary_name: "",
                    secondary_email: "",
                    secondary_phone: {},
                    send_email_invitation: true
                }
            };
            this.PartnerTemplatesItemsEditModal.show({
                templateUrl: "partners/templates/form/contractors/edit/edit.tpl.html",
                controller: "PartnerTemplatesContractorsEditController as ctrl",
                item: item,
                index: -1,
                onSave: angular.bind(this, this.onSave)
            });
        },

        select: function() {
            this.$modal.dismiss("cancel");
            this.PartnerHomeProsSelectForm.show({
                onSave: angular.bind(this, this.onSave)
            });
        },

        cancel: function() {
            this.$modal.dismiss("cancel");
        }

    };
})();