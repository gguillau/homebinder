(function() {
    "use strict";

    angular
        .module("hb.partner.templates.form.editModal", [])
        .factory("PartnerTemplatesItemsEditModal", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: opts.templateUrl,
                            controller: opts.controller,
                            resolveData: opts,
                            backdrop: true
                        });
                    }
                };
            }
        ])
        .factory("PartnerTemplatesItemsEditModalController", [
            "hb.api",
            "$stateParams",
            "$log",
            "Notify",
            "Session",
            function(api, $stateParams, $log, notify, session) {
                var Model = function() {
                    // the templateId
                    this.templateId = $stateParams["templateId"];
                    // get the current user
                    this.currentUser = session.getUser();
                    // photo uploader
                    this.photoUploader = {
                        fileTypes: "image",
                        hideUploadButton: true,
                        showTable: false,
                        selectButtonLabel: "Upload Default Photo",
                        fileLimit: true,
                        multiSelect: false,
                        hide_upload: false
                    };
                };

                angular.extend(Model.prototype, {

                    init: function() {
                        if (this.item.library_template) {
                            this.getTemplates();
                        }
                    },

                    getTemplates: function() {
                        api.binderTemplate.all({ system: true }).then(
                            angular.bind(this, this.getTemplatesSuccess),
                            angular.bind(this, this.onError));
                    },

                    getTemplatesSuccess: function(response) {
                        this.binder_templates = response.data.items;
                        if (this.item.binder_template_id) {
                            response.data.items.forEach(angular.bind(this, function(item) {
                                if (item.id === this.item.binder_template_id) {
                                    this.binder_template = item;
                                    this.item.binder_template_id = item.id;
                                }
                            }));
                        }
                        else {
                            this.binder_template = response.data.items[0];
                        }
                    },

                    save: function() {
                        this.form.$submitted = true;
                        if (this.form.$invalid) {
                            return;
                        }

                        this.beforeSave();

                        if (this.item && this.item.id) {
                            this.updateCall(this.item.id, this.data).then(
                                angular.bind(this, this.onSaveSuccess),
                                angular.bind(this, this.onError)
                            );
                        }
                        else {
                            this.createCall(this.binder_template ? this.binder_template.id : this.templateId, this.data).then(
                                angular.bind(this, this.onSaveSuccess),
                                angular.bind(this, this.onError)
                            );
                        }
                    },

                    onSaveSuccess: function(response) {
                        notify.success(this.resources.saved);
                        this.$modal.close();
                        this.onSave(response.data, this.index);
                    },

                    onError: function(response) {
                        $log.error(response);
                        notify.error(response.data);
                    },

                    changePhoto: function() {
                        var pending = this.photoUploader.api.pendingUploads();
                        this.item.image_url = pending[0].window_url;
                        this.item.image = pending[0].based64;
                        this.item.image_file_name = pending[0].name;
                    },

                    cancel: function() {
                        this.$modal.dismiss("cancel");
                    }
                });

                return Model;
            }
        ]);
})();