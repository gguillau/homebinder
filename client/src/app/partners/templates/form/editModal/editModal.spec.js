describe("PartnerTemplatesItemsEditModalController", function() {
    var PartnerTemplatesItemsEditModalController,
        TestClass,
        $modal,
        api,
        $q,
        PartnerTemplatesItemsEditModal,
        ModalService;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the $modalInstance service
        $provide.factory("$modalInstance", function() {
            return {
                dismiss: function() {},
                close: function() {}
            };
        });

    }));
    // get the base class under test
    beforeEach(inject(function($injector) {
        PartnerTemplatesItemsEditModalController = $injector.get("PartnerTemplatesItemsEditModalController");
        PartnerTemplatesItemsEditModal = $injector.get("PartnerTemplatesItemsEditModal");
        ModalService = $injector.get("ModalService");
        $modal = $injector.get("$modalInstance");
        api = $injector.get("hb.api");
        $q = $injector.get("$q");
    }));

    function createTestClass() {
        TestClass = function() {
            PartnerTemplatesItemsEditModalController.call(this);
            this.$modal = $modal;
        };

        TestClass.prototype = Object.create(PartnerTemplatesItemsEditModalController.prototype);

        return new TestClass();
    }

    describe("init", function() {
        it("calls getTemplates", inject(function(Notify) {
            var model = createTestClass();
            spyOn(model, "getTemplates");
            model.item = {
                library_template: true
            };
            model.init();

            expect(model.getTemplates).toHaveBeenCalled();
        }));
    });

    describe("getTemplates", function() {
        it("calls binderTemplate all", inject(function(Notify) {
            var model = createTestClass();
            spyOn(api.binderTemplate, "all").and.returnValue($q.when({ data: {} }));
            model.getTemplates();

            expect(api.binderTemplate.all).toHaveBeenCalled();
        }));
    });

    describe("getTemplatesSuccess", function() {
        it("sets the templates and the default template", inject(function(Notify) {
            var model = createTestClass();
            var templates = [{ id: 1 }, { id: 2, name: "system default" }];
            model.item = { binder_template_id: 2 };
            model.getTemplatesSuccess({ data: { items: templates } });

            expect(model.binder_templates.length).toEqual(2);
            expect(model.binder_template.name).toEqual("system default");
        }));

        it("sets the templates and the default template when item has no template id", inject(function(Notify) {
            var model = createTestClass();
            var templates = [{ id: 1, name: "partner default" }, { id: 2, name: "system default" }];
            model.item = { binder_template_id: null };
            model.getTemplatesSuccess({ data: { items: templates } });

            expect(model.binder_templates.length).toEqual(2);
            expect(model.binder_template.name).toEqual("partner default");
        }));
    });

    describe("save", function() {
        it("doesn't make any api calls", inject(function(Notify) {
            var model = createTestClass();
            model.createCall = function() {};
            model.updateCall = function() {};
            model.form = { $invalid: true };
            spyOn(model, "createCall");
            spyOn(model, "updateCall");
            model.save();

            expect(model.createCall).not.toHaveBeenCalled();
            expect(model.updateCall).not.toHaveBeenCalled();
        }));

        it("calls createCall", inject(function(Notify, $q) {
            var model = createTestClass();
            model.createCall = function() {};
            model.updateCall = function() {};
            model.form = { $invalid: false };
            model.beforeSave = function() {};
            spyOn(model, "createCall").and.returnValue($q.when({ data: {} }));
            spyOn(model, "updateCall");
            model.save();

            expect(model.createCall).toHaveBeenCalled();
            expect(model.updateCall).not.toHaveBeenCalled();
        }));

        it("calls updateCall", inject(function(Notify, $q) {
            var model = createTestClass();
            model.createCall = function() {};
            model.updateCall = function() {};
            model.form = { $invalid: false };
            model.beforeSave = function() {};
            model.item = { id: 1 };
            spyOn(model, "createCall");
            spyOn(model, "updateCall").and.returnValue($q.when({ data: {} }));
            model.save();

            expect(model.createCall).not.toHaveBeenCalled();
            expect(model.updateCall).toHaveBeenCalled();
        }));
    });

    describe("onSaveSuccess", function() {
        it("calls notify", inject(function(Notify) {
            var model = createTestClass();
            spyOn(Notify, "success");
            model.resources = {};
            model.onSave = function() {};
            model.onSaveSuccess({ data: { id: 1 } });

            expect(Notify.success).toHaveBeenCalled();
        }));
    });

    describe("onError", function() {
        it("calls notify", inject(function(Notify) {
            var model = createTestClass();
            spyOn(Notify, "error");
            model.onError({ data: {} });

            expect(Notify.error).toHaveBeenCalled();
        }));
    });

    describe("changePhoto", function() {
        it("sets the photo", inject(function($rootScope) {
            var model = createTestClass();
            model.photoUploader = {
                api: {
                    pendingUploads: function() {}
                }
            };
            var pending = { window_url: "url", based64: "64", name: "name" };
            spyOn(model.photoUploader.api, "pendingUploads").and.returnValue([pending]);
            model.item = {};
            model.changePhoto();

            expect(model.item.image).toBe("64");
            expect(model.item.image_url).toBe("url");
            expect(model.item.image_file_name).toBe("name");
        }));

    });

    describe("cancel", function() {
        it("calls modal dismiss", inject(function($modalInstance) {
            var model = createTestClass();
            spyOn($modalInstance, "dismiss");
            model.cancel();

            expect($modalInstance.dismiss).toHaveBeenCalled();
        }));
    });

    describe('PartnerTemplatesItemsEditModal', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            PartnerTemplatesItemsEditModal.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });

});