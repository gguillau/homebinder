(function() {
	"use strict";

	angular
		.module("hb.partner.templates", [
			"ui.bootstrap",
			"hb.components",
			"hb.partner.templates.form"
		])
		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("partner.templates", {
					url: "^/partners/:partnerId/templates",
					template: "<partner-templates></partner-templates>",
					access: {
						requiresLogin: true
					}
				});
		}])
		.directive("partnerTemplates", function() {
			return {
				restrict: "E",
				templateUrl: "partners/templates/templates.tpl.html",
				controller: "PartnerTemplatesController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("PartnerTemplatesController", [
			"$stateParams",
			"$log",
			"hb.api",
			"Notify",
			"Loading",
			"$state",
			"ModalService",
			"Context",
			"hb.resources",
			PartnerTemplatesController
		]);

	function PartnerTemplatesController($stateParams, $log, api, notify, loading, $state, modals, context, resources) {
		this.$stateParams = $stateParams;
		this.$log = $log;
		this.api = api;
		this.notify = notify;
		this.partnerId = this.$stateParams["partnerId"];
		this.loading = loading;
		this.$state = $state;
		this.modals = modals;
		this.resources = resources.partnerTemplates;
		this.toolbarCfg = {
			title: this.resources.title,
			button: {
				title: this.resources.newItem,
				click: angular.bind(this, this.newTemplate)
			},
			small_title: this.resources.subTitle
		};
		this.configuration = null;
		this.templates_array = [];
		this.templates = [];
		this.context = context;
		context.getPartner().then(angular.bind(this, function(partner) {
			this.partner = partner;
			this.refresh();
		}));
	}

	PartnerTemplatesController.prototype = {

		refresh: function() {
			this.loading.show(this.resources.loading);
			this.api.partnerConfiguration.get(this.partner.configuration.id).then(
				angular.bind(this, this.onGetConfigSuccess),
				angular.bind(this, this.onGetConfigError));
		},

		onGetConfigSuccess: function(response) {
			this.configuration = response.data;
			this.templates_array = response.data.binder_templates;
			this.sortArray();
			this.loading.close();
		},

		onGetConfigError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		newTemplate: function() {
			this.loading.show(this.resources.creating);
			this.api.partner.createTemplate(this.partnerId, { name: "New template" }).then(
				angular.bind(this, this.templateCreateSuccess),
				angular.bind(this, this.templateCreateError));
		},

		templateCreateSuccess: function(response) {
			this.loading.close();
			this.editTemplate(response.data.id);
		},

		templateCreateError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		confirmDelete: function(template) {
			this.modals.confirm({
				message: "Delete " + template.name + "? This can not be undone.",
				glyphicon: "glyphicon glyphicon-trash",
				confirm: angular.bind(this, this.deleteTemplate, template)
			});
		},

		deleteTemplate: function(template) {
			var index = this.templates_array.indexOf(template);
			this.loading.show("Deleting " + template.name + "...");
			this.api.binderTemplate.destroy(template.id).then(
				angular.bind(this, this.deleteTemplateSuccess, index),
				angular.bind(this, this.deleteTemplateError));
		},

		deleteTemplateSuccess: function(index, response) {
			this.templates_array.splice(index, 1);
			this.sortArray();
			this.notify.success(this.resources.removeMsg);
			this.loading.close();
		},

		deleteTemplateError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		editTemplate: function(id) {
			this.$state.go("partner.templates_form", {
				partnerId: this.partnerId,
				templateId: id
			});
		},

		createBinder: function(id) {
			this.$state.go("partner.new_binder.clientTemplate", {
				partnerId: this.partnerId,
				templateId: id
			});
		},

		isDefaultTemplate: function(id) {
			return this.configuration.default_binder_template_id == id;
		},

		save: function(id) {
			var configuration = {
				partner_id: this.partnerId,
				default_binder_template_id: id
			};
			this.loading.show("Saving configuration...");
			this.api.partnerConfiguration.update(this.configuration.id, { partner_configuration: configuration }).then(
				angular.bind(this, this.saveSuccess),
				angular.bind(this, this.saveError));
		},

		saveSuccess: function(response) {
			this.configuration = response.data;
			this.sortArray();
			this.loading.close();
		},

		saveError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		copy: function(id) {
			this.loading.show(this.resources.copying);
			this.api.binderTemplate.copy(id).then(
				angular.bind(this, this.copyTemplateSuccess),
				angular.bind(this, this.copyTemplateError));
		},

		copyTemplateSuccess: function(response) {
			this.templates_array.push(response.data);
			this.sortArray();
			this.loading.close();
		},

		copyTemplateError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		sortArray: function() {
			this.templates_array.forEach(angular.bind(this, function(template, key) {
				if (template.id === this.configuration.default_binder_template_id) {
					this.templates_array.splice(key, 1);
					this.templates_array.unshift(template);
				}
			}));
		}

	};
})();