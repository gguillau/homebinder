describe("PartnerTemplatesController", function() {

    var $rootScope,
        $scope,
        $controller,
        $q,
        $stateParams,
        ctrl,
        api,
        $log,
        Session,
        notify,
        loading,
        $state,
        modals,
        defer;

    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getPartner: function() {
                    defer = $q.defer();
                    defer.resolve({ id: 268, configuration: { id: 1 } });
                    return defer.promise;
                }
            };
        });
    }));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $stateParams = {};

        modals = {
            confirm: function(args) {}
        };

        $state = {
            go: function(args) {}
        };

        loading = {
            show: function(msg) {},
            close: function() {},
            setMessage: function() {}
        };

        notify = {
            error: function(msg) {},
            success: function(msg) {}
        };

        $log = {
            error: function(msg) {}
        };

        Session = {
            getJwt: function() {}
        };

        $stateParams.partnerId = 268;

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("PartnerTemplatesController", {
            "$stateParams": $stateParams,
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "Session": Session,
            "Loading": loading,
            "$state": $state,
            "ModalService": modals
        });

        $scope.ctrl = ctrl;

    }

    describe('ctrl.refresh', function() {
        it('should retrieve partner configuration and partner template', function() {

            var configuration = {
                binder_templates: [{
                    id: 1
                }, {
                    id: 2
                }],
                id: 1,
                isn_configuration: {},
                maintenance_note: null,
                partner_id: 268
            };
            spyOn(api.partnerConfiguration, "get").and.returnValue($q.when({
                data: configuration
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            spyOn(ctrl, "sortArray").and.callThrough();
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalled();
            expect(api.partnerConfiguration.get).toHaveBeenCalledWith(1);
            expect(ctrl.configuration).toEqual(configuration);
            expect(ctrl.templates_array.length).toBe(2);
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.sortArray).toHaveBeenCalled();
        });

        it('should try to retrieve partner configuration and return an error', function() {

            spyOn(api.partnerConfiguration, "get").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalled();
            expect(api.partnerConfiguration.get).toHaveBeenCalledWith(1);
            expect(notify.error).toHaveBeenCalledWith("error");
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(loading.close).toHaveBeenCalled();

        });
    });

    describe('ctrl.newTemplate', function() {
        it('should create a new template and call edit template function', function() {

            var configuration = {
                binder_templates: [{
                    id: 1
                }, {
                    id: 2
                }],
                id: 1,
                isn_configuration: {},
                maintenance_note: null,
                partner_id: 268
            };
            spyOn(api.partnerConfiguration, "get").and.returnValue($q.when({
                data: configuration
            }));
            spyOn(api.partner, "createTemplate").and.returnValue($q.when({
                data: {
                    id: 1
                }
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn($state, "go");

            createController();
            spyOn(ctrl, "editTemplate").and.callThrough();
            ctrl.newTemplate();
            $rootScope.$apply();

            expect(api.partner.createTemplate).toHaveBeenCalledWith(268, {
                name: "New template"
            });
            expect(loading.show).toHaveBeenCalledWith("Creating template...");
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.editTemplate).toHaveBeenCalledWith(1);
            expect($state.go).toHaveBeenCalledWith("partner.templates_form", {
                partnerId: 268,
                templateId: 1
            });

        });

        it('should call createTemplate function and return an error', function() {

            var configuration = {
                binder_templates: [{
                    id: 1
                }, {
                    id: 2
                }],
                id: 1,
                isn_configuration: {},
                maintenance_note: null,
                partner_id: 268
            };
            spyOn(api.partnerConfiguration, "get").and.returnValue($q.when({
                data: configuration
            }));
            spyOn(api.partner, "createTemplate").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            ctrl.newTemplate();
            $rootScope.$apply();

            expect(api.partner.createTemplate).toHaveBeenCalledWith(268, {
                name: "New template"
            });
            expect(loading.show).toHaveBeenCalledWith("Creating template...");
            expect(loading.close).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalledWith("error");
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });

        });
    });

    describe('ctrl.confirmDelete', function() {
        it('should call modals confirm function', function() {

            var configuration = {
                binder_templates: [{
                    id: 1,
                    name: "Default"
                }, {
                    id: 2
                }],
                id: 1,
                isn_configuration: {},
                maintenance_note: null,
                partner_id: 268
            };
            spyOn(api.partnerConfiguration, "get").and.returnValue($q.when({
                data: configuration
            }));
            spyOn(modals, "confirm");

            createController();
            $rootScope.$apply();
            ctrl.confirmDelete(ctrl.templates_array[0]);
            $rootScope.$apply();

            expect(modals.confirm).toHaveBeenCalledWith({
                message: "Delete Default? This can not be undone.",
                glyphicon: "glyphicon glyphicon-trash",
                confirm: jasmine.any(Function)
            });

        });
    });

    describe('ctrl.deleteTemplate', function() {
        it('should call deleteTemplate function succesfully', function() {

            var configuration = {
                binder_templates: [{
                    id: 1,
                    name: "Default"
                }, {
                    id: 2
                }],
                id: 1,
                isn_configuration: {},
                maintenance_note: null,
                partner_id: 268
            };
            spyOn(api.partnerConfiguration, "get").and.returnValue($q.when({
                data: configuration
            }));
            spyOn(api.binderTemplate, "destroy").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn(notify, "success");


            createController();
            spyOn(ctrl, "sortArray").and.callThrough();
            $rootScope.$apply();
            spyOn(ctrl.templates_array, "splice").and.callThrough();
            ctrl.deleteTemplate(ctrl.templates_array[0]);
            $rootScope.$apply();

            expect(api.binderTemplate.destroy).toHaveBeenCalledWith(1);
            expect(loading.show).toHaveBeenCalledWith("Deleting Default...");
            expect(loading.close).toHaveBeenCalled();
            expect(notify.success).toHaveBeenCalledWith("Template deleted.");
            expect(ctrl.templates_array.splice).toHaveBeenCalledWith(0, 1);
            expect(ctrl.templates_array.length).toBe(1);
            expect(ctrl.sortArray).toHaveBeenCalled();
        });

        it('should call deleteTemplate function and return an error', function() {

            var configuration = {
                binder_templates: [{
                    id: 1,
                    name: "Default"
                }, {
                    id: 2
                }],
                id: 1,
                isn_configuration: {},
                maintenance_note: null,
                partner_id: 268
            };
            spyOn(api.partnerConfiguration, "get").and.returnValue($q.when({
                data: configuration
            }));
            spyOn(api.binderTemplate, "destroy").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn($log, "error");
            spyOn(notify, "error");


            createController();
            $rootScope.$apply();
            ctrl.deleteTemplate(ctrl.templates_array[0]);
            $rootScope.$apply();

            expect(api.binderTemplate.destroy).toHaveBeenCalledWith(1);
            expect(loading.show).toHaveBeenCalledWith("Deleting Default...");
            expect(loading.close).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalledWith("error");
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });


        });
    });

    describe('ctrl.editTemplate', function() {
        it('should call $state go function', function() {

            var configuration = {
                binder_templates: [{
                    id: 1,
                    name: "Default"
                }, {
                    id: 2
                }],
                id: 1,
                isn_configuration: {},
                maintenance_note: null,
                partner_id: 268
            };
            spyOn(api.partnerConfiguration, "get").and.returnValue($q.when({
                data: configuration
            }));
            spyOn($state, "go");

            createController();
            $rootScope.$apply();
            ctrl.editTemplate(ctrl.templates_array[0].id);
            $rootScope.$apply();

            expect($state.go).toHaveBeenCalledWith("partner.templates_form", {
                partnerId: 268,
                templateId: 1
            });

        });
    });

    describe('ctrl.createBinder', function() {
        it('should call $state go function', function() {

            var configuration = {
                binder_templates: [{
                    id: 1,
                    name: "Default"
                }, {
                    id: 2
                }],
                id: 1,
                isn_configuration: {},
                maintenance_note: null,
                partner_id: 268
            };
            spyOn(api.partnerConfiguration, "get").and.returnValue($q.when({
                data: configuration
            }));
            spyOn($state, "go");

            createController();
            $rootScope.$apply();
            ctrl.createBinder(ctrl.templates_array[0].id);
            $rootScope.$apply();

            expect($state.go).toHaveBeenCalledWith("partner.new_binder.clientTemplate", {
                partnerId: 268,
                templateId: 1
            });

        });
    });

    describe('ctrl.isDefaultTemplate', function() {
        it('should call return false from isDefaultTemplate call', function() {

            var configuration = {
                binder_templates: [{
                    id: 1,
                    name: "Default"
                }, {
                    id: 2
                }],
                id: 1,
                isn_configuration: {},
                maintenance_note: null,
                partner_id: 268,
                default_binder_template_id: 2
            };
            spyOn(api.partnerConfiguration, "get").and.returnValue($q.when({
                data: configuration
            }));
            spyOn($state, "go");

            createController();
            $rootScope.$apply();
            var boolean_value = ctrl.isDefaultTemplate(1);
            $rootScope.$apply();

            expect(boolean_value).toBe(false);

        });

        it('should call return true from isDefaultTemplate call', function() {

            var configuration = {
                binder_templates: [{
                    id: 1,
                    name: "Default"
                }, {
                    id: 2
                }],
                id: 1,
                isn_configuration: {},
                maintenance_note: null,
                partner_id: 268,
                default_binder_template_id: 2
            };
            spyOn(api.partnerConfiguration, "get").and.returnValue($q.when({
                data: configuration
            }));
            spyOn($state, "go");

            createController();
            $rootScope.$apply();
            var boolean_value = ctrl.isDefaultTemplate(2);
            $rootScope.$apply();

            expect(boolean_value).toBe(true);

        });
    });

    describe('ctrl.save', function() {
        it('should update configuration success', function() {

            var configuration = {
                binder_templates: [{
                    id: 1
                }, {
                    id: 2
                }],
                id: 1,
                isn_configuration: {},
                maintenance_note: null,
                partner_id: 268,
                default_binder_template_id: 2
            };

            var save_configuration = {
                id: 1,
                partner_id: 268,
                default_binder_template_id: 1
            };

            var data = {
                id: 268,
                configuration: save_configuration
            };

            spyOn(api.partnerConfiguration, "get").and.returnValue($q.when({
                data: configuration
            }));
            spyOn(api.partnerConfiguration, "update").and.returnValue($q.when({
                data: configuration
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();
            expect(ctrl.configuration.default_binder_template_id).toBe(2);
            spyOn(ctrl, "sortArray").and.callThrough();
            ctrl.save(1);
            configuration.default_binder_template_id = 1;
            $rootScope.$apply();

            expect(api.partnerConfiguration.update).toHaveBeenCalled();
            expect(loading.show).toHaveBeenCalledWith("Saving configuration...");
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.sortArray).toHaveBeenCalled();
            expect(ctrl.configuration.default_binder_template_id).toBe(1);

        });

        it('should call updateConfiguration and return an error', function() {

            var configuration = {
                binder_templates: [{
                    id: 1
                }, {
                    id: 2
                }],
                id: 1,
                isn_configuration: {},
                maintenance_note: null,
                partner_id: 268,
                default_binder_template_id: 2
            };

            var save_configuration = {
                id: 1,
                partner_id: 268,
                default_binder_template_id: 1
            };

            var data = {
                id: 268,
                configuration: save_configuration
            };

            spyOn(api.partnerConfiguration, "get").and.returnValue($q.when({
                data: configuration
            }));
            spyOn(api.partnerConfiguration, "update").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            $rootScope.$apply();
            ctrl.save(1);
            $rootScope.$apply();

            expect(api.partnerConfiguration.update).toHaveBeenCalled();
            expect(loading.show).toHaveBeenCalledWith("Saving configuration...");
            expect(loading.close).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalledWith("error");
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });

        });
    });

    describe('ctrl.copy', function() {
        it('should call binderTemplate copy', function() {

            var configuration = {
                binder_templates: [{
                    id: 1
                }, {
                    id: 2
                }],
                id: 1,
                isn_configuration: {},
                maintenance_note: null,
                partner_id: 268,
                default_binder_template_id: 2
            };

            spyOn(api.partnerConfiguration, "get").and.returnValue($q.when({
                data: configuration
            }));
            spyOn(api.binderTemplate, "copy").and.returnValue($q.when({
                data: {}
            }));

            createController();
            ctrl.copy(1);

            expect(api.binderTemplate.copy).toHaveBeenCalled();

        });
    });

    describe('ctrl.copyTemplateSuccess', function() {
        it('should add to templates_array', function() {
            var configuration = {
                binder_templates: [{
                    id: 1
                }, {
                    id: 2
                }],
                id: 1,
                isn_configuration: {},
                maintenance_note: null,
                partner_id: 268,
                default_binder_template_id: 2
            };

            spyOn(api.partnerConfiguration, "get").and.returnValue($q.when({
                data: configuration
            }));

            createController();
            spyOn(ctrl, "sortArray");

            ctrl.templates_array = [];
            ctrl.copyTemplateSuccess({ data: { id: 1 } });

            expect(ctrl.templates_array.length).toEqual(1);

        });
    });

    describe('ctrl.copyTemplateError', function() {
        it('should call notify errory', function() {
            var configuration = {
                binder_templates: [{
                    id: 1
                }, {
                    id: 2
                }],
                id: 1,
                isn_configuration: {},
                maintenance_note: null,
                partner_id: 268,
                default_binder_template_id: 2
            };

            spyOn(api.partnerConfiguration, "get").and.returnValue($q.when({
                data: configuration
            }));

            spyOn(notify, "error");

            createController();
            ctrl.copyTemplateError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();

        });
    });

});

describe('partnerTemplates', function() {
    var $scope, $compile, element, $q, api, context;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        context = _$injector_.get("Context");

        spyOn(context, "getPartner").and.returnValue($q.when({ configuration: { id: 1 } }));
        spyOn(api.partnerConfiguration, "get").and.returnValue($q.when({ data: { binder_templates: [] } }));

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = {};

        element = $compile('<partner-templates></partner-templates>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("Default Binder Items for Client Homes");
        });
    });
});