describe("PartnerUsersNewController", function() {
    var controller,
        base,
        $q_,
        defer;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getPartner: function() {
                    defer = $q_.defer();
                    defer.resolve({ partner_type: "inspector", address: {}, can_perform_apr: true });
                    return defer.promise;
                }
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        $q_ = $q;
        base = $injector.get("UsersNewController");

        controller = $controller("PartnerUsersNewController", {
            "UsersNewController": base
        });
    }));

    describe("init", function() {
        it("sets the indexState", function() {
            expect(controller.model.indexState).toEqual("partner.users");
        });
    });

});