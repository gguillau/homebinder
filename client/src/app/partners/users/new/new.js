(function() {
    "use strict";

    angular.module('hb.partner.users.new', [])
        .config(['$stateProvider', partnerUsersNewConfig])
        .controller("PartnerUsersNewController", [
            "UsersNewController",
            "$stateParams",
            "Context",
            "$filter",
            "Address",
            PartnerUsersNewController
        ]);

    function PartnerUsersNewController(UsersNewController, $stateParams, Context, $filter, Address) {

        var Model = function() {
            // call the parent class
            UsersNewController.call(this);
            this.$stateParams = $stateParams;
            this.partnerId = this.$stateParams.partnerId;
            Context.getPartner().then(angular.bind(this, function(partner) {
                this.country = Address.findCountryByCode(partner.address.country);
                // this is changed in the backend so no security concerns here.
                this.item.password = "password";
                this.item.role = partner.partner_type;
                this.item.user_profile_attributes.company = partner.name;
                this.item.user_profile_attributes.address_attributes.country = this.country.value;
                this.roles = [{
                    name: partner.partner_type,
                    value: $filter("titleize")(partner.partner_type)
                }];
                if (partner.partner_type === "inspector" && partner.can_perform_apr) {
                    this.roles.push({
                        name: "apr_reviewer",
                        value: "APR Reviewer"
                    });
                }
            }));
            this.$stateParams = $stateParams;
            this.partnerId = this.$stateParams.partnerId;
            this.indexState = "partner.users";
            this.indexParams = { partnerId: this.partnerId };
            this.data.partnerId = this.partnerId;
        };

        Model.prototype = Object.create(UsersNewController.prototype);

        angular.extend(Model.prototype);

        this.model = new Model();
    }

    function partnerUsersNewConfig($stateProvider) {
        $stateProvider
            .state('partner.users_new', {
                url: '^/partners/:partnerId/users/new',
                templateUrl: 'components/users/form/form.tpl.html',
                controller: "PartnerUsersNewController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
    }

})();