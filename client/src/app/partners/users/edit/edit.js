(function() {
    "use strict";

    angular.module('hb.partner.users.edit', [])
        .config(['$stateProvider', partnerUsersEditConfig])
        .controller("PartnerUsersEditController", [
            'UsersEditController',
            '$filter',
            'Context',
            PartnerUsersEditController
        ]);

    function PartnerUsersEditController(UsersEditController, $filter, Context) {
        var Model = function() {
            // call the parent class
            UsersEditController.call(this);
            this.partnerId = this.$stateParams.partnerId;

            Context.getPartner().then(angular.bind(this, function(partner) {
                this.roles = [{
                    name: partner.partner_type,
                    value: $filter("titleize")(partner.partner_type)
                }];
                if (partner.partner_type === "inspector" && partner.can_perform_apr) {
                    this.roles.push({
                        name: "apr_reviewer",
                        value: "APR Reviewer"
                    });
                }
            }));
            this.partnerId = this.$stateParams.partnerId;
            this.indexState = "partner.users";
            this.indexParams = { partnerId: this.partnerId };
            // refresh the item
            this.refresh();
        };

        Model.prototype = Object.create(UsersEditController.prototype);

        angular.extend(Model.prototype);

        this.model = new Model();
    }

    function partnerUsersEditConfig($stateProvider) {
        $stateProvider
            .state('partner.users_edit', {
                url: '^/partners/:partnerId/users/:userId/edit',
                templateUrl: 'components/users/form/form.tpl.html',
                controller: "PartnerUsersEditController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
    }
})();
