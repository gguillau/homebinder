describe("PartnerUsersIndexController", function() {
    var controller,
        base,
        $q_,
        notify,
        api,
        defer;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getPartner: function() {
                    defer = $q_.defer();
                    defer.resolve({ partner_type: "inspector", address: {}, can_perform_apr: true });
                    return defer.promise;
                }
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        $q_ = $q;
        base = $injector.get("UsersIndexController");
        notify = $injector.get("Notify");
        api = $injector.get("hb.api");

        controller = $controller("PartnerUsersIndexController", {
            "UsersIndexController": base,
            "Notify": notify,
            "hb.api": api
        });
    }));

    describe("deleteItem", function() {
        it("calls api", function() {
            spyOn(api.partner, "removeUser").and.returnValue($q_.when({ data: {} }));
            controller.model.deleteItem({ partner_role: "member" }, 0);

            expect(api.partner.removeUser).toHaveBeenCalled();
        });

        it("calls notify", function() {
            spyOn(controller.model.notify, "info");
            controller.model.deleteItem({ partner_role: "admin" }, 0);

            expect(controller.model.notify.info).toHaveBeenCalled();
        });
    });

});