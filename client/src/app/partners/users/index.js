(function() {
	"use strict";

	angular.module('hb.partner.users', [
			'hb.partner.users.edit',
			"hb.partner.users.new",
			'ui.router',
			'ui.bootstrap',
			'hb.components'
		])
		.config(['$stateProvider', partnerUsersConfig])
		.controller("PartnerUsersIndexController", [
			"UsersIndexController",
			"$stateParams",
			"Context",
			PartnerUsersIndexController
		]);

	function PartnerUsersIndexController(UsersIndexController, $stateParams, context) {

		var Model = function() {
			// call the parent class
			UsersIndexController.call(this);
			this.editState = "partner.users_edit";
			this.editParams = {};
			this.editParamsProperty = "userId";
			this.newState = "partner.users_new";
			this.$stateParams = $stateParams;
			this.partnerId = this.$stateParams.partnerId;
			this.queryArgs = {
				role: context.getPartner().partner_type,
				partnerId: this.partnerId,
				searchMethod: "for_partner"
			};
			this.toolbarCfg.filter = null;
			this.settings.canImpersonate = false;
			this.settings.canViewHeadshot = true;
			context.getPartner().then(angular.bind(this, function(partner) {
				this.userArgs.role = partner.partner_type;
				this.queryArgs = {
					role: partner.partner_type,
					partnerId: this.partnerId,
					searchMethod: "for_partner"
				};
				this.refresh();
			}));
		};

		Model.prototype = Object.create(UsersIndexController.prototype);

		angular.extend(Model.prototype, {
			deleteItem: function(item, index) {
				if (item.partner_role === "admin") {
					this.notify.info("This is the Admin user for the account. To delete or change please contact HomeBinder at support@homebinder.com or call 800-377-6915");
				}
				else {
					this.api.partner.removeUser(this.partnerId, item.id).then(
						angular.bind(this, this.onDeleteItemSuccess, index),
						angular.bind(this, this.onDeleteItemError)
					);
				}
			}
		});

		this.model = new Model();
	}

	function partnerUsersConfig($stateProvider) {
		$stateProvider
			.state('partner.users', {
				url: '^/partners/:partnerId/users',
				templateUrl: 'components/users/index/index.tpl.html',
				controller: "PartnerUsersIndexController",
				controllerAs: "ctrl",
				access: {
					requiresLogin: true
				}
			});
	}

})();