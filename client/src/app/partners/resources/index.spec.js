describe("PartnersResourcesController", function() {
    var controller,
        base,
        $q_;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        $q_ = $q;
        base = $injector.get("MarketingResourcesIndexController");

        controller = $controller("PartnersResourcesController", {
            "MarketingResourcesIndexController": base
        });
    }));

    describe("refresh", function() {
        it("calls the refreshCall", function() {
            spyOn(controller.model, "refreshCall").and.returnValue($q_.when({ data: { items: [] } }));

            controller.model.refresh();

            expect(controller.model.refreshCall).toHaveBeenCalled();
        });
    });

    describe("onRefreshSuccess", function() {
        it("sets the items", function() {
            var items = [{ resource_type: "document", document_file: "" }, { resource_type: "link", url: "" }, { resource_type: "image", image_file: "" }];
            var response = {
                data: {
                    items: items
                }
            };
            controller.model.onRefreshSuccess(response);
            expect(controller.model.items.length).toEqual(3);
        });
    });

    describe("newItem", function() {
        it("calls MarketingResourcesModal showForm", function() {
            spyOn(controller.model.MarketingResourcesModal, "showForm");

            controller.model.newItem();

            expect(controller.model.MarketingResourcesModal.showForm).toHaveBeenCalled();
        });
    });

    describe("editItem", function() {
        it("calls MarketingResourcesModal showForm", function() {
            spyOn(controller.model.MarketingResourcesModal, "showForm");

            controller.model.editItem({ id: 1 });

            expect(controller.model.MarketingResourcesModal.showForm).toHaveBeenCalled();
        });
    });

});