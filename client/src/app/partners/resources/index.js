(function() {
    "use strict";

    angular
        .module("hb.partner.resources", [
            "ui.router",
            "ui.bootstrap"
        ])
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state("partner.resources", {
                    url: "^/partners/:partnerId/resources",
                    templateUrl: "components/marketingResources/index/index.tpl.html",
                    controller: "PartnersResourcesController",
                    controllerAs: "ctrl",
                    resolve: {
                        partner: function(partner) {
                            return partner;
                        }
                    },
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .controller("PartnersResourcesController", [
            "MarketingResourcesIndexController",
            "$stateParams",
            PartnersResourcesController
        ]);

    function PartnersResourcesController(MarketingResourcesIndexController, $stateParams) {
        var Model = function() {
            // call the parent class
            MarketingResourcesIndexController.call(this);
            // set the refresh API call
            this.refreshCall = this.api.marketingResource.all;
            this.partnerId = $stateParams.partnerId;
            this.refresh();
        };

        Model.prototype = Object.create(MarketingResourcesIndexController.prototype);

        angular.extend(Model.prototype, {
            // refreshes the current view
            refresh: function() {
                var opts = {
                    partnerId: this.partnerId,
                    count: 30
                };

                this.refreshCall(opts).then(
                    angular.bind(this, this.onRefreshSuccess),
                    angular.bind(this, this.onRefreshError)
                );
            },

            // Refresh callback. Updates the item list and totals
            onRefreshSuccess: function(response) {
                this.items = response.data.items.map(angular.bind(this, function(item) {
                    if (item.resource_type === "document") {
                        item.document_file = this.$sce.trustAsResourceUrl(item.document_file);
                    }
                    else if (item.resource_type === "link") {
                        item.url = this.$sce.trustAsResourceUrl(item.url);
                    }
                    else if (item.resource_type === "image") {
                        item.image_file = this.$sce.trustAsResourceUrl(item.image_file);
                    }
                    return item;
                }));
                this.totalItems = this.toolbarCfg.total = response.data.total;
            },

            newItem: function() {
                var formOpts = {};
                formOpts.partnerId = this.partnerId;
                formOpts.onSaved = angular.bind(this, this.onAdded);
                this.MarketingResourcesModal.showForm(formOpts);
            },

            editItem: function(item) {
                var formOpts = {};
                formOpts.marketingResource = item;
                formOpts.partnerId = this.partnerId;
                var index = this.items.indexOf(item);
                formOpts.onSaved = angular.bind(this, this.onEdited, index);
                this.MarketingResourcesModal.showForm(formOpts);
            }
        });

        this.model = new Model();
    }
})();