angular.module('hb.partner.widgets', [])
	.config(['$stateProvider', function($stateProvider) {
		$stateProvider
			.state('partner.widgets', {
				url: '^/partners/:partnerId/widgets',
				template: '<hb-partner-widgets></hb-partner-widgets>',
				access: {
					requiresLogin: true
				}
			});
	}])
	.directive("hbPartnerWidgets", function() {
		return {
			restrict: "E",
			template: "<hb-widget-index cfg=\"ctrl.indexConfig\"></hb-widget-index>",
			controller: "PartnerWidgetsController",
			controllerAs: "ctrl",
			bindToController: true,
			scope: {}
		};
	})
	.controller("PartnerWidgetsController", [
		'$stateParams',
		function($stateParams) {
			this.indexConfig = {
				byPartner: true,
				partnerId: $stateParams.partnerId
			};
		}
	]);
