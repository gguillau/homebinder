describe("PartnerWidgetsController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("PartnerWidgetsController", {});
        $scope.ctrl = ctrl;

    }

    describe('ctrl.init', function() {
        it('inits the controller', function() {
            createController();
            expect(ctrl.indexConfig.byPartner).toBe(true);
        });
    });

});

describe('hbPartnerWidgets', function() {
    var $scope, $compile, element, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        spyOn(api.widget, "all").and.returnValue($q.when({ data: { items: [] } }));

        $scope = $rootScope.$new(), $compile = _$compile_;

        element = $compile('<hb-partner-widgets></hb-partner-widgets>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("Widgets");
        });
    });
});