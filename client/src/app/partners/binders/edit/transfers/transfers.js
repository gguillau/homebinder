(function() {
    "use strict";

    angular.module("hb.partner.binders.edit_binder.transfers", [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('partner.edit_binder.transfers', {
                url: '^/partners/:partnerId/binders/:binderId/transfers',
                templateUrl: 'components/binders/form/transfers/transfers.tpl.html',
                controller: "BinderFormTransfersController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();