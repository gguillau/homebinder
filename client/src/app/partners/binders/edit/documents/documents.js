(function() {
    "use strict";

    angular.module("hb.partner.binders.edit_binder.documents", [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('partner.edit_binder.documents', {
                url: '^/partners/:partnerId/binders/:binderId/documents',
                templateUrl: 'components/binders/form/documents/documents.tpl.html',
                controller: "BinderFormDocumentsController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();