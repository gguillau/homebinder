(function() {
    "use strict";

    angular.module("hb.partner.binders.edit_binder.contractors.edit", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('partner.edit_binder.contractorsEdit', {
                    url: '^/partners/:partnerId/binders/:binderId/contractors/{id:int}/edit',
                    templateUrl: "homebinders/homebinder/binderContractors/form/form.tpl.html",
                    controller: "PartnerBindersContractorFormController",
                    controllerAs: "ctrl",
                    bindToController: true,
                    scope: {},
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();