(function() {
    "use strict";

    angular.module("hb.partner.binders.edit_binder.contractors.newTemplate", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state("partner.edit_binder.contractorsNewTemplate", {
                    url: "^/partners/:partnerId/binders/:binderId/contractors/new/{templateId:int}",
                    templateUrl: "homebinders/homebinder/binderContractors/form/form.tpl.html",
                    controller: "PartnerBindersContractorFormController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();