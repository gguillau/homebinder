(function() {
    "use strict";

    angular
        .module("hb.partner.binders.edit_binder.contractors", [
            "hb.partner.binders.edit_binder.contractors.index",
            "hb.partner.binders.edit_binder.contractors.edit",
            "hb.partner.binders.edit_binder.contractors.new",
            "hb.partner.binders.edit_binder.contractors.newTemplate"
        ]);
})();