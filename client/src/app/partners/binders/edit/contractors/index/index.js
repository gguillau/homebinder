(function() {
    "use strict";

    angular.module("hb.partner.binders.edit_binder.contractors.index", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('partner.edit_binder.contractors', {
                    url: '^/partners/:partnerId/binders/:binderId/contractors',
                    templateUrl: 'components/binders/form/contractors/contractors.tpl.html',
                    controller: "PartnerBindersContractorsController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .controller("PartnerBindersContractorsController", [
            "BinderFormBinderContractorsController",
            "$state",
            "$stateParams",
            PartnerBindersContractorsController
        ]);

    function PartnerBindersContractorsController(BinderFormBinderContractorsController, $state, $stateParams) {
        var Model = function() {
            // call the parent class
            BinderFormBinderContractorsController.call(this);
            this.itemNewState = "partner.edit_binder.contractorsNew";
            this.itemNewTemplateState = "partner.edit_binder.contactorsNewTemplate";
        };

        Model.prototype = Object.create(BinderFormBinderContractorsController.prototype);

        angular.extend(Model.prototype, {
            // send user back to editState
            edit: function(item) {
                $state.go("partner.edit_binder.contractorsEdit", { id: item.id, binderId: $stateParams.binderId, partnerId: $stateParams.partnerId });
            },

            showForm: function(item) {
                if (item) {
                    $state.go(this.itemNewTemplateState, { binderId: this.binder.id, templateId: item.id, partnerId: $stateParams.partnerId });
                }
                else {
                    $state.go(this.itemNewState, { binderId: $stateParams.binderId, partnerId: $stateParams.partnerId });
                }
            }
        });

        this.model = new Model();
    }
})();