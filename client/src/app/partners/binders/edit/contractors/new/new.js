(function() {
    "use strict";

    angular.module("hb.partner.binders.edit_binder.contractors.new", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('partner.edit_binder.contractorsNew', {
                    url: '^/partners/:partnerId/binders/:binderId/contractors/new',
                    templateUrl: "homebinders/homebinder/binderContractors/form/form.tpl.html",
                    controller: "PartnerBindersContractorFormController",
                    controllerAs: "ctrl",
                    bindToController: true,
                    scope: {},
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .controller("PartnerBindersContractorFormController", [
            "BinderContractorFormController",
            "$stateParams",
            PartnerBindersContractorFormController
        ]);

    function PartnerBindersContractorFormController(BinderContractorFormController, $stateParams) {
        var Model = function() {
            this.indexState = "partner.edit_binder.contractors";
            this.indexParams = { binderId: $stateParams.binderId, partnerId: $stateParams.partnerId };
            this.editState = "partner.edit_binder.contractorsEdit";
            BinderContractorFormController.call(this);
        };

        Model.prototype = Object.create(BinderContractorFormController.prototype);

        angular.extend(Model.prototype, {});

        this.model = new Model();
    }
})();