(function() {
    "use strict";

    angular.module("hb.partner.binders.edit_binder.warranties", [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('partner.edit_binder.warranties', {
                url: '^/partners/:partnerId/binders/:binderId/warranties',
                templateUrl: 'components/binders/edit/warranties/warranties.tpl.html',
                controller: "BindersEditWarrantiesController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();