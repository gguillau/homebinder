(function() {
    "use strict";

    angular.module('hb.partner.binders.edit', [
            "hb.partner.binders.edit_binder.client",
            "hb.partner.binders.edit_binder.agents",
            "hb.partner.binders.edit_binder.images",
            "hb.partner.binders.edit_binder.appliances",
            "hb.partner.binders.edit_binder.maintenanceItems",
            "hb.partner.binders.edit_binder.contractors",
            "hb.partner.binders.edit_binder.documents",
            "hb.partner.binders.edit_binder.transactions",
            "hb.partner.binders.edit_binder.warranties",
            "hb.partner.binders.edit_binder.subscriptions",
            "hb.partner.binders.edit_binder.transfers"
        ])
        .config(['$stateProvider', partnerBindersEditConfig])
        .controller("PartnerBindersEditController", [
            "BindersEditController",
            "Context",
            "Address",
            PartnerBindersEditController
        ]);

    function PartnerBindersEditController(BindersEditController, context, Address) {

        var Model = function() {
            // call the parent class
            BindersEditController.call(this);
            this.indexState = "partner.binders";
            this.indexParams = {};
            this.formCfg.settings.showBinderAgents = this.currentUser.role === "inspector" || this.currentUser.role === "broker" || this.currentUser.role === "lender" || this.currentUser.role === "builder" ? true : false;
            this.formCfg.settings.showBinderEmails = false;
            this.formCfg.settings.showBinderUsers = false;
            this.formCfg.settings.showBinderPartners = false;
            this.formCfg.settings.showBinderBrandings = false;
            this.formCfg.settings.showBinderTransactions = true;
            this.formCfg.settings.showBinderSubscriptions = true;
            this.formCfg.settings.showBinderTransfers = false;
            context.getPartner().then(angular.bind(this, function(partner) {
                this.partner = partner;
                this.country = Address.findCountryByCode(this.partner.address.country);
                if (this.partner.configuration.active_warranty_account === true) {
                    this.formCfg.settings.showBinderWarranties = true;
                }
                this.refresh();
                this.initiate();
            }));
        };

        Model.prototype = Object.create(BindersEditController.prototype);

        angular.extend(Model.prototype, {
            initiate: function() {
                this.navigation_links = [{
                        name: "Binder Info",
                        links: [
                            { name: "Client/Property Info", value: "client", state: "partner.edit_binder.client", allowed: true }
                        ]
                    },
                    {
                        name: "Binder Items",
                        links: [
                            { name: "Images", value: "images", state: "partner.edit_binder.images", allowed: this.formCfg.settings.showAdditionalLinks },
                            { name: "Appliances", value: "appliances", state: "partner.edit_binder.appliances", allowed: this.formCfg.settings.showAdditionalLinks },
                            { name: "Maintenance Items", value: "maintenance_items", state: "partner.edit_binder.maintenanceItems", allowed: this.formCfg.settings.showAdditionalLinks },
                            { name: "Preferred Home Pros", value: "home_pros", state: "partner.edit_binder.contractors", allowed: this.formCfg.settings.showAdditionalLinks },
                            { name: "Documents", value: "documents", state: "partner.edit_binder.documents", allowed: this.formCfg.settings.showAdditionalLinks }
                        ]
                    }
                ];
                if (this.formCfg.settings.showBinderUsers) {
                    this.navigation_links[0].links.push({ name: "Users", value: "users", state: "partner.edit_binder.users", allowed: this.formCfg.settings.showAdditionalLinks });
                }
                if (this.formCfg.settings.showBinderAgents) {
                    this.navigation_links[0].links.push({ name: "Agents", value: "agents", state: "partner.edit_binder.agents", allowed: this.formCfg.settings.showAdditionalLinks });
                }
                if (this.formCfg.settings.showBinderBrandings) {
                    this.navigation_links[0].links.push({ name: "Brandings", value: "brandings", state: "partner.edit_binder.brandings", allowed: this.formCfg.settings.showAdditionalLinks });
                }
                if (this.formCfg.settings.showBinderPartners) {
                    this.navigation_links[0].links.push({ name: "Partners", value: "partners", state: "partner.edit_binder.partners", allowed: this.formCfg.settings.showAdditionalLinks });
                }
                if (this.formCfg.settings.showBinderEmails) {
                    this.navigation_links[0].links.push({ name: "Emails", value: "emails", state: "partner.edit_binder.emails", allowed: this.formCfg.settings.showAdditionalLinks });
                }
                if (this.formCfg.settings.showBinderWarranties) {
                    this.navigation_links[0].links.push({ name: "Warranties", value: "warranties", state: "partner.edit_binder.warranties", allowed: this.formCfg.settings.showAdditionalLinks });
                }
                if (this.formCfg.settings.showBinderTransactions) {
                    this.navigation_links[0].links.push({ name: "Transactions", value: "transactions", state: "partner.edit_binder.transactions", allowed: this.formCfg.settings.showAdditionalLinks });
                }
                if (this.formCfg.settings.showBinderSubscriptions) {
                    this.navigation_links[0].links.push({ name: "Subscription", value: "subscriptions", state: "partner.edit_binder.subscriptions", allowed: this.formCfg.settings.showAdditionalLinks });
                }
                if (this.formCfg.settings.showBinderTransfers) {
                    this.navigation_links[1].links.push({ name: "Transfers", value: "transfers", state: "partner.edit_binder.transfers", allowed: this.formCfg.settings.showAdditionalLinks });
                }

                if (this.currentUser.role === "homepro" || this.currentUser.role === "property_manager") {
                    this.formCfg.inspection_date = new Date();
                }
            }
        });

        this.model = new Model();
    }

    function partnerBindersEditConfig($stateProvider) {
        $stateProvider
            .state('partner.edit_binder', {
                url: '^/partners/:partnerId/binders/:binderId',
                abstract: true,
                templateUrl: 'components/binders/form/form.tpl.html',
                controller: "PartnerBindersEditController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                },
                resolve: {
                    binder: ["Context", "$stateParams", function(Context, $stateParams) {
                        return Context.getBinder($stateParams.binderId);
                    }]
                }
            });
    }

})();