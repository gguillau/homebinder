(function() {
    "use strict";

    angular.module('hb.partner.binders.edit_binder.images', [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('partner.edit_binder.images', {
                url: '^/partners/:partnerId/binders/:binderId/images',
                templateUrl: 'components/binders/form/images/images.tpl.html',
                controller: "BinderFormImagesController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();