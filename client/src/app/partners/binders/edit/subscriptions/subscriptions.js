(function() {
    "use strict";

    angular.module("hb.partner.binders.edit_binder.subscriptions", [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('partner.edit_binder.subscriptions', {
                url: '^/partners/:partnerId/binders/:binderId/subscriptions',
                templateUrl: 'components/binders/edit/subscriptions/subscriptions.tpl.html',
                controller: "BinderFormTransfersController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();