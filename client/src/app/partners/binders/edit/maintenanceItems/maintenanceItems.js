(function() {
    "use strict";

    angular
        .module("hb.partner.binders.edit_binder.maintenanceItems", [
            "hb.partner.binders.edit_binder.maintenanceItems.index",
            "hb.partner.binders.edit_binder.maintenanceItems.edit",
            "hb.partner.binders.edit_binder.maintenanceItems.new",
            "hb.partner.binders.edit_binder.maintenanceItems.newTemplate"
        ]);
})();