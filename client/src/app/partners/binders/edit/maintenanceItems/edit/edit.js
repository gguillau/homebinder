(function() {
    "use strict";

    angular.module("hb.partner.binders.edit_binder.maintenanceItems.edit", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('partner.edit_binder.maintenanceItemsEdit', {
                    url: '^/partners/:partnerId/binders/:binderId/maintenanceItems/{id:int}/edit',
                    templateUrl: "homebinders/homebinder/maintenanceItems/form/form.tpl.html",
                    controller: "PartnerBindersMaintenanceItemsFormController",
                    controllerAs: "ctrl",
                    bindToController: true,
                    scope: {},
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();