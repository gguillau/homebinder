(function() {
    "use strict";

    angular.module("hb.partner.binders.edit_binder.maintenanceItems.newTemplate", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state("partner.edit_binder.maintenanceItemsNewTemplate", {
                    url: "^/partners/:partnerId/binders/:binderId/maintenanceItems/new/{templateId:int}",
                    templateUrl: "homebinders/homebinder/maintenanceItems/form/form.tpl.html",
                    controller: "PartnerBindersMaintenanceItemsFormController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();