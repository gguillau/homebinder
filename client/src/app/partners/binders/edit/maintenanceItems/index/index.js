(function() {
    "use strict";

    angular.module("hb.partner.binders.edit_binder.maintenanceItems.index", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('partner.edit_binder.maintenanceItems', {
                    url: '^/partners/:partnerId/binders/:binderId/maintenanceItems',
                    templateUrl: 'components/binders/form/maintenanceItems/maintenanceItems.tpl.html',
                    controller: "PartnerBindersMaintenanceItemsController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .controller("PartnerBindersMaintenanceItemsController", [
            "BinderFormMaintenanceItemsController",
            "$state",
            "$stateParams",
            PartnerBindersMaintenanceItemsController
        ]);

    function PartnerBindersMaintenanceItemsController(BinderFormMaintenanceItemsController, $state, $stateParams) {
        var Model = function() {
            // call the parent class
            BinderFormMaintenanceItemsController.call(this);
            this.itemNewState = "partner.edit_binder.maintenanceItemsNew";
            this.itemNewTemplateState = "partner.edit_binder.maintenanceItemsNewTemplate";
        };

        Model.prototype = Object.create(BinderFormMaintenanceItemsController.prototype);

        angular.extend(Model.prototype, {
            // send user back to editState
            edit: function(item) {
                $state.go("partner.edit_binder.maintenanceItemsEdit", { id: item.id, binderId: $stateParams.binderId, partnerId: $stateParams.partnerId });
            },

            showForm: function(item) {
                if (item) {
                    $state.go(this.itemNewTemplateState, { binderId: this.binder.id, templateId: item.id, partnerId: $stateParams.partnerId });
                }
                else {
                    $state.go(this.itemNewState, { binderId: $stateParams.binderId, partnerId: $stateParams.partnerId });
                }
            }
        });

        this.model = new Model();
    }
})();