(function() {
    "use strict";

    angular.module("hb.partner.binders.edit_binder.maintenanceItems.new", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('partner.edit_binder.maintenanceItemsNew', {
                    url: '^/partners/:partnerId/binders/:binderId/maintenanceItems/new',
                    templateUrl: "homebinders/homebinder/maintenanceItems/form/form.tpl.html",
                    controller: "PartnerBindersMaintenanceItemsFormController",
                    controllerAs: "ctrl",
                    bindToController: true,
                    scope: {},
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        // Model backing the appliance modal form
        .controller("PartnerBindersMaintenanceItemsFormController", [
            "MaintenanceItemFormController",
            "$stateParams",
            PartnerBindersMaintenanceItemsFormController
        ]);

    function PartnerBindersMaintenanceItemsFormController(MaintenanceItemFormController, $stateParams) {
        var Model = function() {
            this.indexState = "partner.edit_binder.maintenanceItems";
            this.indexParams = { binderId: $stateParams.binderId, partnerId: $stateParams.partnerId };
            this.editState = "partner.edit_binder.maintenanceItemsEdit";
            MaintenanceItemFormController.call(this);
        };

        Model.prototype = Object.create(MaintenanceItemFormController.prototype);

        angular.extend(Model.prototype, {});

        this.model = new Model();
    }
})();