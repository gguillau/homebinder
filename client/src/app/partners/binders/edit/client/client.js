(function() {
    "use strict";

    angular.module('hb.partner.binders.edit_binder.client', [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('partner.edit_binder.client', {
                url: '^/partners/:partnerId/binders/:binderId/client',
                templateUrl: 'components/binders/form/clientInformation/clientInformation.tpl.html',
                controller: "PartnerBindersEditController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();