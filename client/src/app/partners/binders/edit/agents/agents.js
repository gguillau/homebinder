(function() {
    "use strict";

    angular.module('hb.partner.binders.edit_binder.agents', [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('partner.edit_binder.agents', {
                url: '^/partners/:partnerId/binders/:binderId/agents',
                templateUrl: 'components/binders/form/agents/agents.tpl.html',
                controller: "BinderFormAgentController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();