(function() {
    "use strict";

    angular.module("hb.partner.binders.edit_binder.transactions", [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('partner.edit_binder.transactions', {
                url: '^/partners/:partnerId/binders/:binderId/transactions',
                templateUrl: 'components/binders/edit/transactions/transactions.tpl.html',
                controller: "BinderFormTransactionsController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();