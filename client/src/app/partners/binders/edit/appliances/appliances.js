(function() {
    "use strict";

    angular
        .module("hb.partner.binders.edit_binder.appliances", [
            "hb.partner.binders.edit_binder.appliances.index",
            "hb.partner.binders.edit_binder.appliances.edit",
            "hb.partner.binders.edit_binder.appliances.new",
            "hb.partner.binders.edit_binder.appliances.newTemplate"
        ]);
})();