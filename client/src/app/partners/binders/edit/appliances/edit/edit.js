(function() {
    "use strict";

    angular.module("hb.partner.binders.edit_binder.appliances.edit", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('partner.edit_binder.appliancesEdit', {
                    url: '^/partners/:partnerId/binders/:binderId/appliances/{id:int}/edit',
                    templateUrl: "homebinders/homebinder/appliances/form/form.tpl.html",
                    controller: "PartnerBindersApplianceFormController",
                    controllerAs: "ctrl",
                    bindToController: true,
                    scope: {},
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();