(function() {
    "use strict";

    angular.module("hb.partner.binders.edit_binder.appliances.newTemplate", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state("partner.edit_binder.appliancesNewTemplate", {
                    url: "^/partners/:partnerId/binders/:binderId/appliances/new/{templateId:int}",
                    templateUrl: "homebinders/homebinder/appliances/form/form.tpl.html",
                    controller: "PartnerBindersApplianceFormController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();