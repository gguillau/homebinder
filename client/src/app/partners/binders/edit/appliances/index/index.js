(function() {
    "use strict";

    angular.module("hb.partner.binders.edit_binder.appliances.index", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('partner.edit_binder.appliances', {
                    url: '^/partners/:partnerId/binders/:binderId/appliances',
                    templateUrl: 'components/binders/form/appliances/appliances.tpl.html',
                    controller: "PartnerBindersAppliancesController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .controller("PartnerBindersAppliancesController", [
            "BinderFormAppliancesController",
            "$state",
            "$stateParams",
            PartnerBindersAppliancesController
        ]);

    function PartnerBindersAppliancesController(BinderFormAppliancesController, $state, $stateParams) {
        var Model = function() {
            // call the parent class
            BinderFormAppliancesController.call(this);
            this.itemNewState = "partner.edit_binder.appliancesNew";
            this.itemNewTemplateState = "partner.edit_binder.appliancesNewTemplate";
        };

        Model.prototype = Object.create(BinderFormAppliancesController.prototype);

        angular.extend(Model.prototype, {
            // send user back to editState
            edit: function(item) {
                $state.go("partner.edit_binder.appliancesEdit", { id: item.id, binderId: $stateParams.binderId, partnerId: $stateParams.partnerId });
            },

            showForm: function(item) {
                if (item) {
                    $state.go(this.itemNewTemplateState, { binderId: this.binder.id, templateId: item.id, partnerId: $stateParams.partnerId });
                }
                else {
                    $state.go(this.itemNewState, { binderId: $stateParams.binderId, partnerId: $stateParams.partnerId });
                }
            }
        });

        this.model = new Model();
    }
})();