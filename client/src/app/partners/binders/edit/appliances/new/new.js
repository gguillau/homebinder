(function() {
    "use strict";

    angular.module("hb.partner.binders.edit_binder.appliances.new", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('partner.edit_binder.appliancesNew', {
                    url: '^/partners/:partnerId/binders/:binderId/appliances/new',
                    templateUrl: "homebinders/homebinder/appliances/form/form.tpl.html",
                    controller: "PartnerBindersApplianceFormController",
                    controllerAs: "ctrl",
                    bindToController: true,
                    scope: {},
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        // Model backing the appliance modal form
        .controller("PartnerBindersApplianceFormController", [
            "ApplianceFormController",
            "$stateParams",
            PartnerBindersApplianceFormController
        ]);

    function PartnerBindersApplianceFormController(ApplianceFormController, $stateParams) {
        var Model = function() {
            this.indexState = "partner.edit_binder.appliances";
            this.indexParams = { binderId: $stateParams.binderId, partnerId: $stateParams.partnerId };
            this.editState = "partner.edit_binder.appliancesEdit";
            ApplianceFormController.call(this);
        };

        Model.prototype = Object.create(ApplianceFormController.prototype);

        angular.extend(Model.prototype, {});

        this.model = new Model();
    }
})();