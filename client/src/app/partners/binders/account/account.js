(function() {
    "use strict";

    angular
        .module("hb.partner.binders.account", [])
        .factory("AccountStatus", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "partners/binders/account/account.tpl.html",
                            controller: "AccountStatusController as ctrl",
                            resolveData: opts
                        });
                    }
                };
            }
        ])
        .controller("AccountStatusController", [
            "$modalInstance",
            "data",
            "Address",
            "hb.api",
            "Notify",
            "$log",
            "Loading",
            "hb.resources",
            AccountStatusController
        ]);

    function AccountStatusController($modal, opts, Address, api, notify, $log, loading, resources) {
        this.$modal = $modal;
        this.Address = Address;
        this.api = api;
        this.notify = notify;
        this.$log = $log;
        this.loading = loading;
        this.opts = opts;
        this.resources = resources.accountStatus;
        this.expired_message = this.resources.expired;
        this.closed_message = this.resources.closed;
        this.suspended_message = this.resources.suspended;
        this.init();
    }

    AccountStatusController.prototype = {
        
        init: function(){
            var partner = this.opts.partner;
            switch(partner.account.account_status){
                case "expired":
                    this.status_message = this.expired_message;
                    break;
                case "suspended":
                    this.status_message = this.suspended_message;
                    break;
                case "closed":
                    this.status_message = this.closed_message;
                    break;
                default:
                    this.status_message = this.resources.activate;
                    break;
            }
        },

        cancel: function() {
            this.$modal.dismiss("cancel");
        }

    };
})();