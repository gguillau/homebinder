describe("AccountStatusController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        opts,
        $modal,
        api,
        notify,
        $log,
        loading,
        address,
        partner,
        resources,
        AccountStatus,
        ModalService;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        resources = _$injector_.get("hb.resources");
        AccountStatus = _$injector_.get("AccountStatus");
        ModalService = _$injector_.get("ModalService");

        $modal = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };

        partner = {
            address: null,
            coupons: [],
            tags: [],
            id: 100,
            name: "John Smith Inspection Services",
            partner_type: "inspector",
            code: "JohnFREEFORLIFE",
            contact: "",
            phone: "",
            email: "johnsmith@gmail.com",
            binder_logo_id: 13,
            sellers_logo_id: null,
            binder_count: 0,
            vendors: [],
            created_at: "2015-09-21 19:09:37.484248",
            account: {
                account_status: "active"
            }
        };

        opts = {
            partner: partner
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("AccountStatusController", {
            "$modalInstance": $modal,
            "Address": address,
            "data": opts,
            "hb.api": api,
            "Notify": notify,
            "$log": $log,
            "Loading": loading,
            "hb.resources": resources
        });

        $scope.ctrl = ctrl;
    }

    describe("Init", function() {

        it("should have the intialize the appropriate variables from the opts object", function() {

            partner.account.account_status = "expired";

            createController();
            $rootScope.$apply();

            expect(ctrl.status_message).toBe("Your Free Trial has expired.  To activate your account please contact support@homebinder.com or call 800-377-6915.");

        });

        it("should have the intialize the appropriate variables from the opts object", function() {

            partner.account.account_status = "suspended";

            createController();
            $rootScope.$apply();

            expect(ctrl.status_message).toBe("Your account is not currently active.  To activate your account please contact support@homebinder.com or call 800-377-6915.");

        });

        it("should have the intialize the appropriate variables from the opts object", function() {

            partner.account.account_status = "closed";

            createController();
            $rootScope.$apply();

            expect(ctrl.status_message).toBe("Your account has been closed.  If this is not correct, please contact support@homebinder.com or call 800-377-6915.");

        });

        it("should have the intialize the appropriate variables from the opts object", function() {

            partner.account.account_status = "fake";

            createController();
            $rootScope.$apply();

            expect(ctrl.status_message).toBe("To activate your account please contact support@homebinder.com or call 800-377-6915.");

        });

    });

    describe("ctrl.cancel", function() {

        it("should call $modalInstance dismiss function", function() {

            spyOn($modal, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.cancel();
            $rootScope.$apply();

            expect($modal.dismiss).toHaveBeenCalledWith('cancel');
        });

    });

    describe('AccountStatus', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            AccountStatus.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});