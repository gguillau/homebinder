describe("PartnerBindersIndexController", function() {
    var controller,
        base,
        api,
        resources,
        $q,
        defer;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getPartner: function() {
                    defer = $q.defer();
                    defer.resolve({ settings: { navigation: {} } });
                    return defer.promise;
                }
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $rootScope) {
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        $q = $injector.get("$q");
        resources = $injector.get("hb.resources");

        spyOn(api.binder, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));

        controller = $controller("PartnerBindersIndexController", {
            "hb.framework.indexBase": base,
            "hb.api": api,
            "hb.resources": resources
        });

        $rootScope.$apply();
    }));

    describe("searchValue", function() {
        it("calls refresh", function() {

            controller.model.searchValue = "test";
            controller.model.refresh();

            expect(controller.model.api.binder.all).toHaveBeenCalledWith({ page: 1, count: 10, order: 'desc', orderBy: 'binders.id', search: 'test', filter: 'created', partnerId: undefined, searchMethod: 'for_partner' });
        });
    });

    describe("onViewChanged", function() {
        it("calls refresh", function() {
            spyOn(controller.model, "refresh");

            controller.model.onViewChanged("test");

            expect(controller.model.refresh).toHaveBeenCalled();
        });
    });

    describe("newBinder", function() {
        it("calls showStatusModal", function() {
            spyOn(controller.model, "showStatusModal");

            controller.model.newBinder();

            expect(controller.model.showStatusModal).toHaveBeenCalled();
        });
        it("calls newItem", function() {
            spyOn(controller.model, "newItem");

            controller.model.partner = {
                account: {
                    account_status: "active"
                }
            };
            controller.model.newBinder();

            expect(controller.model.newItem).toHaveBeenCalled();
        });
    });

    describe("verifyStatus", function() {
        it("returns false", function() {
            var result = controller.model.verifyStatus();

            expect(result).toEqual(false);
        });
        it("returns true", function() {
            controller.model.partner = {
                account: {
                    account_status: "active"
                }
            };
            var result = controller.model.verifyStatus();

            expect(result).toEqual(true);
        });
    });

    describe("showStatusModal", function() {
        it("calls AccountStatus show", function() {
            spyOn(controller.model.AccountStatus, "show");

            controller.model.showStatusModal();

            expect(controller.model.AccountStatus.show).toHaveBeenCalled();
        });
    });

    describe("showSearch", function() {
        it("returns false", function() {
            var result = controller.model.showSearch();

            expect(result).toEqual(false);
        });
    });

    describe("searchTypeChange", function() {
        it("calls refresh", function() {
            spyOn(controller.model, "refresh");

            controller.model.searchTypeChange();

            expect(controller.model.refresh).toHaveBeenCalled();
        });
    });
});