describe("PartnerBindersNewController", function() {
    var controller,
        base,
        $q_,
        defer;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getPartner: function() {
                    defer = $q_.defer();
                    defer.resolve(null);
                    return defer.promise;
                }
            };
        });

        // mock the session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        $q_ = $q;
        base = $injector.get("BindersNewController");

        controller = $controller("PartnerBindersNewController", {
            "BindersNewController": base
        });
    }));

    describe("init", function() {
        it("sets the indexState", function() {
            expect(controller.model.indexState).toEqual("partner.binders");
        });
    });

});