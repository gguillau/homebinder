(function() {
    "use strict";

    angular.module('hb.partner.binders.new', [
            "hb.partner.binders.new.client"
        ])
        .config(['$stateProvider', partnerBindersNewConfig])
        .controller("PartnerBindersNewController", [
            "BindersNewController",
            "$stateParams",
            "Context",
            PartnerBindersEditController
        ]);

    function PartnerBindersEditController(BindersNewController, $stateParams, context) {

        var Model = function() {
            // call the parent class
            BindersNewController.call(this);
            this.$stateParams = $stateParams;
            this.context = context;
            // get the partner id
            this.partnerId = this.$stateParams["partnerId"];
            // get the templateId
            this.templateId = this.$stateParams["templateId"];
            // set the index state and index params
            this.indexState = "partner.binders";
            this.indexParams = {
                partnerId: this.partnerId
            };
            this.editState = "partner.edit_binder.client";
            this.editParams = {
                partnerId: this.partnerId,
                binderId: null
            };
            context.getPartner().then(angular.bind(this, function(partner) {
                this.partner = partner;
                this.formCfg.partner = this.partner;
                this.formCfg.settings.showPartner = false;
                this.formCfg.settings.showAdditionalBranding = false;
                this.formCfg.templateId = this.templateId;
                this.init();
            }));
        };

        Model.prototype = Object.create(BindersNewController.prototype, {});

        angular.extend(Model.prototype, {
            initiate: function() {
                this.navigation_links = [{
                    name: "Binder Info",
                    links: [
                        { name: "Client/Property Info", value: "client", state: "partner.new_binder.client", allowed: true }
                    ]
                }];
            }
        });

        this.model = new Model();
    }

    function partnerBindersNewConfig($stateProvider) {
        $stateProvider
            .state('partner.new_binder', {
                abstract: true,
                url: "^/partners/:partnerId/binders/new",
                templateUrl: "components/binders/form/form.tpl.html",
                controller: "PartnerBindersNewController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
    }

})();