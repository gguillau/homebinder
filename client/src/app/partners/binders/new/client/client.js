(function() {
    "use strict";

    angular.module('hb.partner.binders.new.client', [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('partner.new_binder.client', {
                    url: '^/partners/:partnerId/binders/new/client',
                    templateUrl: 'components/binders/form/clientInformation/clientInformation.tpl.html',
                    controller: "PartnerBindersNewController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                }).state('partner.new_binder.clientTemplate', {
                    url: '^/partners/:partnerId/binders/new/template/:templateId/client',
                    templateUrl: 'components/binders/form/clientInformation/clientInformation.tpl.html',
                    controller: "PartnerBindersNewController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();