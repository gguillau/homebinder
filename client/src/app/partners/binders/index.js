(function() {
	"use strict";

	angular.module('hb.partner.binders', [
			"hb.partner.binders.account",
			"hb.partner.binders.new",
			"hb.partner.binders.edit",
			"hb.partner.binders.dashboard"
		])
		.config(['$stateProvider', adminBindersConfig])
		.controller("PartnerBindersIndexController", [
			"BindersIndexController",
			"AccountStatus",
			"hb.utils",
			"$stateParams",
			"Context",
			PartnerBindersIndexController
		]);

	function PartnerBindersIndexController(BindersIndexController, AccountStatus, utils, $stateParams, Context) {

		var Model = function() {
			// call the parent class
			BindersIndexController.call(this);
			this.AccountStatus = AccountStatus;
			this.Context = Context;
			this.utils = utils;
			this.today = this.utils.utils.getToday(true);
			this.$stateParams = $stateParams;
			this.editState = "partner.edit_binder.client";
			this.newState = "partner.new_binder.client";
			this.currentDashboard = "binders";
			this.views = [{
				id: 'my',
				label: 'View my binders'
			}, {
				id: 'all',
				label: 'View all binders'
			}];
			this.currentView = this.views[0];
			this.partnerId = this.$stateParams["partnerId"];
			// remove owner header
			this.headers.splice(4, 1);
			this.resources.searchInfo = this.resources.searchPartners;
			this.searchTypes = [{
				name: "all",
				value: "All Binders"
			}, {
				name: "transferred",
				value: "Transferred Binders"
			}, {
				name: "shared",
				value: "Shared Binders"
			}, {
				name: "created",
				value: "Binders not sent"
			}];
			this.resources.emptyTableText = this.resources.emptyTablePartner;
			this.searchType = this.searchTypes[3].name;
			angular.extend(this.toolbarCfg, {
				views: this.views,
				currentView: this.currentView,
				onViewChanged: angular.bind(this, this.onViewChanged),
				button: {
					title: this.resources.newButton,
					click: angular.bind(this, this.newBinder)
				}
			});
			Context.getPartner().then(angular.bind(this, function(partner) {
				this.partner = partner;
				if (this.partner && !this.partner.binder_count) {
					this.partner.binder_count = 0;
				}
				this.refresh();
			}));
		};

		Model.prototype = Object.create(BindersIndexController.prototype);

		angular.extend(Model.prototype, {

			refresh: function() {
				var opts = {
					page: this.currentPage,
					count: this.itemsPerPage,
					order: this.sortOption.order,
					orderBy: this.sortOption.orderBy
				};

				if (this.searchValue) {
					opts.search = this.searchValue;
				}

				var promise;
				this.loading.show("Loading binders...");

				opts.filter = this.searchType;
				opts.partnerId = this.partnerId;
				opts.searchMethod = "for_partner";

				// get all binders for the partner
				promise = this.api.binder.all(opts);

				promise.then(
					angular.bind(this, this.onRefreshSuccess),
					angular.bind(this, this.onRefreshError)
				);
			},

			onViewChanged: function(item) {
				this.currentView = this.toolbarCfg.currentView = item;
				this.currentPage = 1;
				this.searchValue = null;
				this.refresh();
			},

			newBinder: function() {
				if (this.verifyStatus()) {
					this.newItem();
				}
				else {
					this.showStatusModal();
				}
			},

			verifyStatus: function() {
				if (!this.partner.account) {
					return false;
				}
				return this.partner.account.account_status === "active";
			},

			showStatusModal: function() {
				this.AccountStatus.show({
					partner: this.partner
				});
			},

			showSearch: function() {
				return false;
			},

			searchTypeChange: function() {
				this.refresh();
			},

			setDashboard: function(dashboard) {
				this.currentDashboard = dashboard;
			}
		});

		this.model = new Model();
	}

	function adminBindersConfig($stateProvider) {
		$stateProvider
			.state('partner.binders', {
				url: '^/partner/:partnerId/binders',
				templateUrl: 'partners/binders/index.tpl.html',
				controller: "PartnerBindersIndexController",
				controllerAs: "ctrl",
				access: {
					requiresLogin: true
				}
			});
	}

})();