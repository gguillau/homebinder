describe('partnersBindersDashboard', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        $scope = $rootScope.$new(), $compile = _$compile_;
        
        element = $compile('<partners-binders-dashboard></partners-binders-dashboard>')($scope);
        $scope.$digest();
    }));

    describe('when the page compiles the partners-binders-dashboard directive', function() {
        it('page contains the correct strings', function() {
            expect(element.html()).toContain("Refresh your binders list");
        });
    });
});