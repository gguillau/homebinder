(function() {
    "use strict";

    angular
        .module("hb.partner.binders.dashboard", [])
        .directive("partnersBindersDashboard", function() {
            return {
                restrict: "E",
                templateUrl: "partners/binders/bindersDashboard/bindersDashboard.tpl.html",
                controller: "PartnerBindersIndexController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        });
})();