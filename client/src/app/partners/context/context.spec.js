describe("Context", function() {
    'use strict';

    var Context;

    beforeEach(function() {
        module("homebinder");

        inject(function(_$injector_) {
            Context = _$injector_.get("Context");
        });
    });

    it("returns true", function() {
        var result;

        // call the function
        result = Context.showRecallModal();

        // verify results
        expect(result).toBe(true);
    });

    it("returns false", function() {
        var result;

        // call the function
        Context.setRecallModalStatus(false);
        result = Context.showRecallModal();

        // verify results
        expect(result).toBe(false);
    });

    it("returns the config", function() {
        var config;

        // call the function
        Context.setPartner({ id: 1, configuration: { id: 1 }, setings: {} });
        config = Context.getConfiguration();

        // verify the config
        expect(config).toEqual({ id: 1 });
    });

    it("returns null", function() {
        var config;

        // call the function
        Context.setPartner({ id: 1 });
        config = Context.getConfiguration();

        // verify the config
        expect(config).toBeNull();
    });

    it("sets and returns the navList", function() {
        var list;

        // call the function
        Context.setNavList([{ name: "Menu" }]);
        list = Context.getNavList();

        // verify the list
        expect(list).toEqual([{ name: "Menu" }]);
    });
});