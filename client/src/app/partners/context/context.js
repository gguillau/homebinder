(function() {
    "use strict";

    angular
        .module("hb.partner.context", [])
        .factory("Context", [
            "hb.api",
            "Loading",
            "$state",
            "$q",
            Context
        ]);

    function Context(api, Loading, $state, $q) {
        var state = {
            binder: null,
            partner: null,
            settings: null,
            show_recall_modal: true,
            binderLoaded: false,
            partnerLoaded: false,
            navList: []
        };

        return {
            showRecallModal: function() {
                return state.show_recall_modal;
            },

            setRecallModalStatus: function(bool) {
                state.show_recall_modal = bool;
            },

            setPartner: function(newPartner) {
                state.partner = angular.extend({}, state.partner, newPartner);
                state.partnerLoaded = true;
                if (newPartner.settings) {
                    state.settings = newPartner.settings;
                }
            },

            getPartner: function(id) {
                var d = null;
                if (!id && !state.partnerLoaded) {
                    d = $q.defer();
                    d.resolve(state.partner);
                    return d.promise;
                }
                else if (state.partnerLoaded) {
                    if (!id) {
                        d = $q.defer();
                        d.resolve(state.partner);
                        return d.promise;
                    }
                    if (id && state.partner.id === id) {
                        d = $q.defer();
                        d.resolve(state.partner);
                        return d.promise;
                    }
                    else if (id) {
                        // make the api call to get the partner
                        return api.partner.get(id).then(
                            function(response) {
                                state.partner = response.data;
                                state.partnerLoaded = true;
                                return response.data;
                            });
                    }
                }
                else {
                    if (id) {
                        // make the api call to get the partner
                        return api.partner.get(id).then(
                            function(response) {
                                state.partner = response.data;
                                state.partnerLoaded = true;
                                return response.data;
                            });
                    }
                    else {
                        d = $q.defer();
                        d.resolve(state.partner);
                        return d.promise;
                    }
                }
            },

            getBinder: function(id) {
                var d = null;
                if (!id && !state.binderLoaded) {
                    d = $q.defer();
                    d.resolve(state.binder);
                    return d.promise;
                }
                else if (state.binderLoaded) {
                    if (!id) {
                        d = $q.defer();
                        d.resolve(state.binder);
                        return d.promise;
                    }
                    if (id && state.binder.id == id) {
                        d = $q.defer();
                        d.resolve(state.binder);
                        return d.promise;
                    }
                    else if (id) {
                        // make the api call to get the keys
                        return api.binder.get(id).then(
                            function(response) {
                                state.binder = response.data;
                                state.binderLoaded = true;
                                return response.data;
                            });
                    }
                }
                else {
                    if (id) {
                        // make the api call to get the keys
                        return api.binder.get(id).then(
                            function(response) {
                                state.binder = response.data;
                                state.binderLoaded = true;
                                return response.data;
                            });
                    }
                    else {
                        d = $q.defer();
                        d.resolve(state.binder);
                        return d.promise;
                    }
                }
            },

            setBinder: function(newBinder) {
                state.binder = angular.extend({}, state.binder, newBinder);
                state.binderLoaded = true;
            },

            getConfiguration: function() {
                if (state.partner.configuration) {
                    return state.partner.configuration;
                }
                return null;
            },

            setNavList: function(list) {
                state.navList = list;
            },

            getNavList: function() {
                return state.navList;
            },

            reset: function() {
                state = {
                    binder: null,
                    binderLoaded: false,
                    partnerLoaded: false,
                    partner: null,
                    settings: null,
                    show_recall_modal: true,
                    navList: []
                };
            }
        };
    }
})();