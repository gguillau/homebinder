(function() {
    "use strict";

    angular.module("hb.partner.registration", [
            "ui.router",
            "hb.components",
            "hb.api"
        ])
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state("partnerRegistration", {
                    url: "^/partners/register/:type",
                    template: "<partner-registration></partner-registration>",
                    controller: "PartnerRegistrationController"
                });
        }])
        .directive("partnerRegistration", function() {
            return {
                restrict: "E",
                templateUrl: "partners/registration/partnerRegistration.tpl.html",
                controller: "PartnerRegistrationController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("PartnerRegistrationController", [
            "hb.api",
            "Notify",
            "$state",
            "$stateParams",
            "Loading",
            "$log",
            "Session",
            "AppDelegate",
            "Context",
            "hb.resources",
            "Address",
            "hb.utils",
            PartnerRegistrationController
        ]);

    function PartnerRegistrationController(api, notify, $state, $stateParams, loading, $log, session, appDelegate, context, resources, Address, utils) {
        this.api = api;
        this.notify = notify;
        this.formSubmitted = false;
        this.$state = $state;
        this.$stateParams = $stateParams;
        this.validationErrors = resources.validationErrors;
        this.loading = loading;
        this.$log = $log;
        this.session = session;
        this.appDelegate = appDelegate;
        this.context = context;
        this.resources = resources.partnerRegistration;
        this.countries = Address.countries();
        this.utils = utils.utils;
        this.deviceName = "Unknown";
        this.deviceVersion = "Unknown";
        this.operatingSystem = "Unknown";
        this.init();
        this.data = {
            address_attributes: {
                country: null
            }
        };
        this.year = new Date().getFullYear();
        this.userCreated = false;
        this.setTypes();
    }

    PartnerRegistrationController.prototype = {

        init: function() {
            var device = this.utils.getDeviceInfo();
            this.deviceName = device[0];
            this.deviceVersion = device[1];
            this.operatingSystem = this.utils.getOperatingSystem();
        },

        setTypes: function() {
            this.options = [
                { name: this.resources.broker, type: "broker"}, 
                { name: this.resources.inspector, type: "inspector"}, 
                { name: this.resources.lender, type: "lender" },
                { name: this.resources.propertyManager, type: "property_manager" },
                { name: this.resources.homepro, type: "homepro" },
                { name: this.resources.builder, type: "builder" },
                { name: this.resources.hoa, type: "hoa" },
                { name: this.resources.insurance, type: "insurance" }
            ];

            switch (this.$stateParams['type']) {
                case "agent":
                    this.data.type = this.options[0];
                    break;
                case "inspector":
                    this.data.type = this.options[1];
                    break;
                case "lender":
                    this.data.type = this.options[2];
                    break;
                case "manager":
                    this.data.type = this.options[3];
                    break;
                case "homepro":
                    this.data.type = this.options[4];
                    break;
                case "builder":
                    this.data.type = this.options[5];
                    break;
                case "hoa":
                    this.data.type = this.options[6];
                    break;
                case "insurance":
                    this.data.type = this.options[7];
                    break;
            }

        },

        submit: function() {
            this.loading.show(this.resources.saving);
            this.formSubmitted = true;

            var code = this.data.email + this.data.name.replace(/\s+/g, '') + this.data.type.type;

            var registration = {
                user: {
                    email: this.data.email,
                    role: this.data.type.type,
                    password: this.data.password,
                    user_profile_attributes: {
                        first_name: this.data.first,
                        last_name: this.data.last,
                        mobile_phone: "+" + this.country.code + this.data.phone,
                        company: this.data.name,
                        address_attributes: {
                            country: this.country.value
                        }
                    }
                },
                device_type: "web",
                device_name: this.deviceName,
                device_version: this.deviceVersion,
                operating_system: this.operatingSystem,
                partner: {
                    name: this.data.name,
                    partner_type: this.data.type.type,
                    code: code,
                    email: this.data.email,
                    contact: this.data.first + " " + this.data.last,
                    phone: "+" + this.country.code + this.data.phone,
                    affiliate_code: this.data.affiliate_code,
                    email_display_name: this.data.name.length > 50 ? this.data.first + " " + this.data.last : this.data.name,
                    address_attributes: {
                        country: this.country.value
                    }
                }
            };

            this.api.partner.create(registration).then(
                angular.bind(this, this.registered),
                angular.bind(this, this.failed));
        },

        registered: function(response) {
            this.appDelegate.loggedOn(response.data.token);
            this.loading.close();
            var user = this.session.getUser();
            this.$state.go("partner.onboard", {
                partnerId: user.partner_id
            });
        },

        failed: function(response) {
            this.formSubmitted = false;
            this.$log.error(response);
            this.notify.error(response.data);
            this.loading.close();
        },

        cancel: function() {
            this.$state.go('root');
        }
    };

})();