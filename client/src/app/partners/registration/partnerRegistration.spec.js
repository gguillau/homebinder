describe("PartnerRegistrationController", function() {

    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $state,
        $compile,
        template,
        $templateCache,
        api,
        notify,
        loading,
        $log,
        $stateParams,
        session,
        appDelegate;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_, _$templateCache_) {
        $rootScope = _$injector_.get("$rootScope");
        $compile = _$compile_;
        $controller = _$controller_;
        $templateCache = _$templateCache_;
        $q = _$injector_.get("$q");

        $stateParams = {};

        context = {
            setPartner: function() {}
        };

        loading = {
            show: function(message) {},
            setMessage: function(message) {},
            close: function() {}
        };

        $state = {
            go: function(state, args) {}
        };

        api = {
            partner: {
                create: function() {}
            }
        };

        notify = {
            error: function(msg) {}
        };

        $log = {
            error: function(msg) {}
        };

        session = {
            getUser: function() {}
        };

        appDelegate = {
            loggedOn: function() {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("PartnerRegistrationController", {
            "$state": $state,
            "hb.api": api,
            "Notify": notify,
            "Loading": loading,
            "$log": $log,
            "$stateParams": $stateParams,
            "Session": session,
            "AppDelegate": appDelegate,
            "Context": context
        });

        $scope.partnerRegistration = ctrl;

        template = $templateCache.get('partners/registration/partnerRegistration.tpl.html');
        $compile(template)($scope);
        $scope.$apply();
        $scope.$digest();
    }

    describe('ctrl.setTypes', function() {
        it('should set data type for agent', function() {
            createController();
            
            ctrl.$stateParams['type'] = 'agent';
            ctrl.setTypes();
            
            expect(ctrl.data.type).toEqual(ctrl.options[0]);
        });
        
        it('should set data type for inspector', function() {
            createController();
            
            ctrl.$stateParams['type'] = 'inspector';
            ctrl.setTypes();
            
            expect(ctrl.data.type).toEqual(ctrl.options[1]);
        });
        
        it('should set data type for lender', function() {
            createController();
            
            ctrl.$stateParams['type'] = 'lender';
            ctrl.setTypes();
            
            expect(ctrl.data.type).toEqual(ctrl.options[2]);
        });
        
        it('should set data type for manager', function() {
            createController();
            
            ctrl.$stateParams['type'] = 'manager';
            ctrl.setTypes();
            
            expect(ctrl.data.type).toEqual(ctrl.options[3]);
        });
        
        it('should set data type for homepro', function() {
            createController();
            
            ctrl.$stateParams['type'] = 'homepro';
            ctrl.setTypes();
            
            expect(ctrl.data.type).toEqual(ctrl.options[4]);
        });
        
        it('should set data type for builder', function() {
            createController();
            
            ctrl.$stateParams['type'] = 'builder';
            ctrl.setTypes();
            
            expect(ctrl.data.type).toEqual(ctrl.options[5]);
        });
        
        it('should set data type for hoa', function() {
            createController();
            
            ctrl.$stateParams['type'] = 'hoa';
            ctrl.setTypes();
            
            expect(ctrl.data.type).toEqual(ctrl.options[6]);
        });
        
        it('should set data type for insurance', function() {
            createController();
            
            ctrl.$stateParams['type'] = 'insurance';
            ctrl.setTypes();
            
            expect(ctrl.data.type).toEqual(ctrl.options[7]);
        });
    });

    describe('ctrl.submit', function() {

        it('should notify user of registration errors', function() {

            spyOn(api.partner, 'create').and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn($log, "error");
            spyOn(notify, "error");

            var user = {
                email: "johnsmith@gmail.com",
                password: "password",
                first_name: "John",
                last_name: "Smith"
            };
            var code = user.email + "John Smith Inspection Services".replace(/\s+/g, '') + "inspector";
            var partner = {
                name: "John Smith Inspection Services",
                partner_type: "inspector",
                code: code,
                email: user.email,
                contact: "John Smith",
                phone: "617-233-5006",
                affiliate_code: "ASHI",
                email_display_name: "John Smith Inspection Services"
            };

            createController();
            ctrl.country = {
                value: "US",
                code: 1
            };
            ctrl.data = {
                first: "John",
                last: "Smith",
                name: partner.name,
                email: user.email,
                phone: partner.phone,
                type: {
                    type: partner.partner_type
                },
                password: user.password,
                affiliate_code: partner.affiliate_code,
                address_attributes: {
                    country: "US"
                }
            };
            ctrl.submit();
            $rootScope.$apply();

            expect(api.partner.create).toHaveBeenCalledWith(jasmine.objectContaining({
                user: {
                    email: "johnsmith@gmail.com",
                    password: "password",
                    role: "inspector",
                    user_profile_attributes: {
                        first_name: "John",
                        last_name: "Smith",
                        company: partner.name,
                        mobile_phone: "+" + ctrl.country.code + partner.phone,
                        address_attributes: {
                            country: "US"
                        }
                    }
                },
                partner: {
                    name: "John Smith Inspection Services",
                    partner_type: "inspector",
                    code: code,
                    email: user.email,
                    contact: "John Smith",
                    phone: "+" + ctrl.country.code + partner.phone,
                    affiliate_code: "ASHI",
                    email_display_name: "John Smith Inspection Services",
                    address_attributes: {
                        country: "US"
                    }
                }
            }));
            expect(ctrl.formSubmitted).toBe(false);
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.show).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(loading.close).toHaveBeenCalled();
        });

        it('should send user to onboarding page upon registration success', function() {

            spyOn(api.partner, 'create').and.returnValue($q.when({
                data: {
                    token: "token"
                }
            }));

            spyOn(session, "getUser").and.returnValue({
                partner_id: 100
            });
            spyOn(appDelegate, "loggedOn");

            spyOn($state, "go");
            spyOn(loading, "show");
            spyOn(loading, "close");

            var user = {
                email: "johnsmith@gmail.com",
                password: "password",
                first_name: "John",
                last_name: "Smith"
            };
            var code = user.email + "John Smith Inspection Services".replace(/\s+/g, '') + "inspector";
            var data_partner = {
                name: "John Smith Inspection Services",
                partner_type: "inspector",
                code: code,
                email: user.email,
                contact: "John Smith",
                phone: "617-233-5006",
                affiliate_code: "ASHI",
                email_display_name: "John Smith Inspection Services"
            };

            createController();
            ctrl.country = {
                value: "US",
                code: 1
            };
            ctrl.data = {
                first: "John",
                last: "Smith",
                name: data_partner.name,
                email: user.email,
                phone: data_partner.phone,
                type: {
                    type: data_partner.partner_type
                },
                password: user.password,
                affiliate_code: data_partner.affiliate_code,
                address_attributes: {
                    country: "US"
                }
            };
            ctrl.submit();
            $rootScope.$apply();

            expect(api.partner.create).toHaveBeenCalledWith(jasmine.objectContaining({
                user: {
                    email: "johnsmith@gmail.com",
                    password: "password",
                    role: "inspector",
                    user_profile_attributes: {
                        first_name: "John",
                        last_name: "Smith",
                        company: data_partner.name,
                        mobile_phone: "+" + ctrl.country.code + data_partner.phone,
                        address_attributes: {
                            country: "US"
                        }
                    }
                },
                partner: {
                    name: "John Smith Inspection Services",
                    partner_type: "inspector",
                    code: code,
                    email: user.email,
                    contact: "John Smith",
                    phone: "+" + ctrl.country.code + data_partner.phone,
                    affiliate_code: "ASHI",
                    email_display_name: "John Smith Inspection Services",
                    address_attributes: {
                        country: "US"
                    }
                }
            }));
            expect(appDelegate.loggedOn).toHaveBeenCalledWith("token");
            expect(session.getUser).toHaveBeenCalled();
            expect(loading.show).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();
            expect($state.go).toHaveBeenCalledWith('partner.onboard', {
                partnerId: 100
            });
        });

    });

    describe('ctrl.cancel', function() {
        it('should call $state go function send user to root', function() {

            spyOn($state, "go");
            $stateParams.type = "agent";
            createController();
            ctrl.cancel();
            $rootScope.$apply();

            expect(ctrl.data.type.type).toEqual("broker");
            expect($state.go).toHaveBeenCalledWith("root");

        });
    });

});