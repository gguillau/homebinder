describe("PartnerHomeownerOnboardingController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $state,
        $stateParams,
        $location,
        $log,
        api,
        address,
        resources,
        homeowners,
        loading,
        notify;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        address = _$injector_.get("Address");
        resources = _$injector_.get("hb.resources");
        notify = _$injector_.get("Notify");
        $location = _$injector_.get("$location");

        $stateParams = {};

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        $state = {
            go: function(state, args) {}
        };
        api = {
            partner: {
                automation: function() {},
                abs: function() {}
            }
        };
    }));
    beforeEach(function() {
        $stateParams.partnerCode = "test";
        spyOn(api.partner, "automation").and.returnValue($q.when({
            data: {
                api_key: "fakekey",
                default_binder_template_id: 117,
                address: {}
            }
        }));
    });

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("PartnerHomeownerOnboardingController", {
            "$state": $state,
            "$stateParams": $stateParams,
            "$location": $location,
            "hb.api": api,
            "Address": address,
            "hb.resources": resources,
            "Loading": loading,
            "Notify": notify
        });
        $scope.ctrl = ctrl;
    }

    it("retrieves the partner information", function() {

        createController();
        $rootScope.$apply();

        expect(ctrl.partnerCode).toBe("test");
        expect(ctrl.partner.default_binder_template_id).toBe(117);
        expect(ctrl.partner.api_key).toBe("fakekey");
    });

    it("calls notify info", function() {

        createController();
        spyOn(ctrl.Notify, "info");
        ctrl.refreshError({ data: "error" });

        expect(ctrl.Notify.info).toHaveBeenCalled();
    });

    it("calls $location hash", function() {

        createController();
        spyOn(ctrl.$location, "hash");
        ctrl.learnMore();

        expect(ctrl.$location.hash).toHaveBeenCalled();
    });

    it("sends a request and returns success", function() {

        spyOn(api.partner, "abs").and.returnValue($q.when({
            data: "success"
        }));
        spyOn(notify, "success");
        spyOn(loading, "show");
        spyOn(loading, "close");

        createController();
        $rootScope.$apply();
        ctrl.info = {};
        ctrl.submit();
        $rootScope.$apply();

        expect(api.partner.abs).toHaveBeenCalled();
        expect(notify.success).toHaveBeenCalled();
        expect(loading.show).toHaveBeenCalled();
        expect(loading.close).toHaveBeenCalled();
    });

    it("sends a request and returns an error", function() {

        spyOn(api.partner, "abs").and.returnValue($q.reject({
            data: "error"
        }));
        spyOn(notify, "info");
        spyOn(loading, "show");
        spyOn(loading, "close");

        createController();
        $rootScope.$apply();
        ctrl.info = {};
        ctrl.submit();
        $rootScope.$apply();

        expect(api.partner.abs).toHaveBeenCalled();
        expect(notify.info).toHaveBeenCalled();
        expect(loading.show).toHaveBeenCalled();
        expect(loading.close).toHaveBeenCalled();
    });

    it("sends user to the root state", function() {

        spyOn($state, "go");

        createController();
        $rootScope.$apply();
        ctrl.cancel();
        $rootScope.$apply();

        expect($state.go).toHaveBeenCalled();
    });

    it("set the country", function() {

        createController();
        $rootScope.$apply();
        ctrl.info = {
            state: "MA"
        };
        ctrl.onSelect({
            country: {
                value: "US"
            }
        });
        $rootScope.$apply();

        expect(ctrl.info.country).toEqual("US");
    });

});
