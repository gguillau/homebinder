(function() {
	"use strict";

	angular.module("hb.partner.homeowners", [])
		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("partner_homeowners", {
					url: "^/partners/:partnerCode/homeowners",
					templateUrl: "partners/homeowners/homeowners.tpl.html",
					controller: "PartnerHomeownerOnboardingController",
					controllerAs: "ctrl"
				});
		}])
		.controller("PartnerHomeownerOnboardingController", [
			"$state",
			"$stateParams",
			"$location",
			"$anchorScroll",
			"$log",
			"hb.api",
			"Address",
			"hb.resources",
			"Loading",
			"Notify",
			PartnerHomeownerOnboardingController
		]);

	function PartnerHomeownerOnboardingController($state, $stateParams, $location, $anchorScroll, $log, api, address, resources, loading, Notify) {
		this.$state = $state;
		this.$stateParams = $stateParams;
		this.$location = $location;
		this.$anchorScroll = $anchorScroll;
		this.$log = $log;
		this.api = api;
		this.address = address;
		this.info = {
			phone: {}
		};
		this.states = address.getStatesAndProvinces();
		this.strings = resources.partnerHomeownerResources;
		this.common = resources.commonResources;
		this.loading = loading;
		this.Notify = Notify;
		this.partnerCode = $stateParams.partnerCode;
		this.hbLogoUrl = "img/flatlogo.png";
		this.refresh();
	}

	angular.extend(PartnerHomeownerOnboardingController.prototype, {

		refresh: function() {
			this.api.partner.automation(this.partnerCode).then(
				angular.bind(this, this.refreshSuccess),
				angular.bind(this, this.refreshError));
		},

		refreshSuccess: function(response) {
			this.partner = response.data;
			this.country = this.address.findCountryByCode(this.partner.address.country);
			this.info.country = this.country.value;
		},

		refreshError: function(response) {
			this.$log.error(response);
			this.Notify.info(this.strings.error);
		},

		learnMore: function() {
			this.$location.hash("learn-more");
			this.$anchorScroll();
		},

		submit: function() {
			var params = {
				method: "self_serve",
				binder_template_id: this.partner.default_binder_template_id,
				client: {
					email: this.info.email,
					first: this.info.first,
					last: this.info.last,
					phone: "+" + this.country.code + this.info.phone
				},
				property: {
					address: this.info.address1,
					address2: this.info.address2,
					city: this.info.city,
					state: this.info.state,
					zip: this.info.zip,
					country: this.info.country
				}
			};

			this.loading.show(this.strings.creating);
			this.api.partner.abs(this.partnerCode, params, this.partner.api_key).then(
				angular.bind(this, this.submitComplete),
				angular.bind(this, this.submitCompleteError)
			);
		},

		submitComplete: function(response) {
			this.loading.close();
			this.cancel();
			this.Notify.success(this.strings.success, 10000);
		},

		submitCompleteError: function(response) {
			this.loading.close();
			this.cancel();
			this.Notify.info(response.data + " " + this.strings.error, 10000);
		},

		cancel: function() {
			this.$state.go("root");
		},

		onSelect: function(item) {
			if (item) {
				this.info.country = item.country.value;
			}
		}
	});
})();