describe("PartnersSettingsController", function() {
    var controller,
        base,
        $q_,
        defer;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getPartner: function() {
                    defer = $q_.defer();
                    defer.resolve({ settings: { navigation: {} } });
                    return defer.promise;
                }
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("PartnersEditController");
        $q_ = $q;
        controller = $controller("PartnersSettingsController", {
            "PartnersEditController": base
        });
    }));

    describe("init", function() {
        it("sets the settings", function() {
            expect(controller.model.settings.showClientCalls).toEqual(false);
        });
    });

});