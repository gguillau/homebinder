(function() {
    "use strict";

    angular.module('hb.partner.settings', [])
        .config(['$stateProvider', adminPartnersEditConfig])
        .controller("PartnersSettingsController", [
            'PartnersEditController',
            PartnersSettingsController
        ]);

    function PartnersSettingsController(PartnersEditController) {
        var Model = function() {
            // call the parent class
            PartnersEditController.call(this);
            this.settings.showClientCalls = false;
        };

        Model.prototype = Object.create(PartnersEditController.prototype);

        angular.extend(Model.prototype);

        this.model = new Model();
    }

    function adminPartnersEditConfig($stateProvider) {
        $stateProvider
            .state('partner.settings', {
                url: '^/partners/:partnerId/settings',
                templateUrl: 'components/partners/edit/edit.tpl.html',
                controller: "PartnersSettingsController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
    }
})();
