(function() {
    "use strict";

    angular
        .module("hb.partner.analytics.info", [])
        .factory("AnalyticsInfo", [
            "ModalService",
            function(Modals) {
                return {
                    showIndexInfo: function(opts) {
                        Modals.show({
                            templateUrl: "partners/analytics/info/indexInfo.tpl.html",
                            controller: "AnalyticsInfoController as ctrl",
                            backdrop: "static",
                            keyboard: true
                        });
                    },
                    showLoginInfo: function(opts) {
                        Modals.show({
                            templateUrl: "partners/analytics/info/loginInfo.tpl.html",
                            controller: "AnalyticsInfoController as ctrl",
                            backdrop: "static",
                            keyboard: true
                        });
                    },
                    showPerformanceInfo: function(opts) {
                        Modals.show({
                            templateUrl: "partners/analytics/info/performanceInfo.tpl.html",
                            controller: "AnalyticsInfoController as ctrl",
                            backdrop: "static",
                            keyboard: true
                        });
                    }
                };
            }
        ])
        .controller("AnalyticsInfoController", [
            "$modalInstance",
            "hb.resources",
            AnalyticsInfoController
        ]);

    function AnalyticsInfoController($modal, resources) {
        this.$modal = $modal;
        this.resources = resources.partnerAnalyticsInfo;
    }

    AnalyticsInfoController.prototype = {

        cancel: function() {
            this.$modal.close();
        }
    };
})();