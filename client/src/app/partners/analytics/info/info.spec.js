describe("AnalyticsInfoController", function() {
    var $rootScope,
        $controller,
        resources,
        ctrl,
        $modalInstance,
        AnalyticsInfo,
        ModalService;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        resources = _$injector_.get("hb.resources");
        AnalyticsInfo = _$injector_.get("AnalyticsInfo");
        ModalService = _$injector_.get("ModalService");

        $modalInstance = {
            close: function() {}
        };
    }));

    function createController() {
        ctrl = $controller("AnalyticsInfoController", {
            "$modalInstance": $modalInstance,
            "hb.resources": resources
        });
        $rootScope.$apply();
    }

    describe('cancel', function() {
        it("calls close", function() {
            spyOn($modalInstance, "close");

            createController();
            ctrl.cancel();

            expect($modalInstance.close).toHaveBeenCalled();
        });
    });

    describe('AnalyticsInfo', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            AnalyticsInfo.showIndexInfo({});

            expect(ModalService.show).toHaveBeenCalled();
        });
        
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            AnalyticsInfo.showPerformanceInfo({});

            expect(ModalService.show).toHaveBeenCalled();
        });

        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            AnalyticsInfo.showLoginInfo({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});