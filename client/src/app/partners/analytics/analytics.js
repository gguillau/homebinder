(function() {
	"use strict";

	angular
		.module("hb.partner.analytics", [
			"hb.partner.analytics.info",
			"ui.router",
			"ui.bootstrap"
		])
		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("partner.analytics", {
					url: "^/partners/:partnerId/analytics",
					templateUrl: "partners/analytics/analytics.tpl.html",
					controller: "PartnerAnalyticsController",
					controllerAs: "ctrl",
					access: {
						requiresLogin: true
					}
				});
		}])
		.controller("PartnerAnalyticsController", [
			"$state",
			"$stateParams",
			"$log",
			"hb.api",
			"Notify",
			"ModalService",
			"Loading",
			"AnalyticsInfo",
			"$window",
			"Context",
			"hb.resources",
			"$q",
			PartnerAnalyticsController
		]);

	function PartnerAnalyticsController($state, $stateParams, $log, api, notify, modals, loading, AnalyticsInfo, $window, context, resources, $q) {
		this.$state = $state;
		this.$stateParams = $stateParams;
		this.partnerId = this.$stateParams.partnerId;
		this.$log = $log;
		this.api = api;
		this.notify = notify;
		this.modals = modals;
		this.loading = loading;
		this.AnalyticsInfo = AnalyticsInfo;
		this.$window = $window;
		this.$q = $q;
		this.resources = resources.partnerAnalytics;
		this.tabs = [{
			name: this.resources.tab1
		}, {
			name: this.resources.tab2
		}, {
			name: this.resources.tab5
		}];
		this.current_tab = this.tabs[0].name;
		this.analytics = {
			overview: null,
			users: [],
			reminders: [],
			agentAnalytics: {
				agents: []
			},
			inspectors: []
		};
		this.filtered_logins = this.filtered_reminders = [];
		this.usersPage = this.remindersPage = 1;
		this.maxSize = 20;
		this.itemsPerPage = 20;
		this.sortBy = "accepted_transfer_at";
		this.reverseSort = true;
		this.data = {
			thankyou30: 0,
			thankyouAll: 0,
			maintenance30: 0,
			maintenanceYtd: 0,
			maintenanceAll: 0,
			binders30: 0,
			bindersYtd: 0,
			bindersAll: 0,
			allClients: 0,
			acceptedCurrent: 0,
			accepted30: 0,
			accepted90: 0,
			active90: 0,
			activeYtd: 0,
			grade: "F"
		};
		context.getPartner().then(angular.bind(this, function(partner) {
			this.partner = partner;
			if (this.partner.partner_type === "inspector") {
				this.tabs.push({
					name: this.resources.tab4
				});
			}
			this.updatePercentages();
			this.refresh();
		}));
	}

	PartnerAnalyticsController.prototype = {
		updatePercentages: function() {
			this.percentages = {
				acceptedCurrent: this.calculatePercentages(this.data.acceptedCurrent, this.data.allClients),
				accepted30: this.calculatePercentages(this.data.accepted30, this.data.allClients),
				accepted90: this.calculatePercentages(this.data.accepted90, this.data.allClients),
				active90: this.calculatePercentages(this.data.active90, this.data.allClients),
				activeYtd: this.calculatePercentages(this.data.activeYtd, this.data.allClients)
			};

			if (this.percentages.acceptedCurrent > 44) {
				this.data.grade = "A";
			}
			else if (this.percentages.acceptedCurrent > 34 && this.percentages.acceptedCurrent < 45) {
				this.data.grade = "B";
			}
			else if (this.percentages.acceptedCurrent < 34) {
				this.data.grade = "C";
			}
		},

		calculatePercentages: function(numberOne, numberTwo) {
			if (numberTwo !== 0) {
				return Math.round((numberOne / numberTwo) * 100);
			}
			return 0;
		},

		refresh: function() {
			var promises = [];
			var today = new Date();
			var lte = today;
			lte.setDate(today.getDate() + 1);
			lte = lte.toLocaleDateString("en-US");
			var thirtyDaysAgo = new Date();
			var ninetyDaysAgo = new Date();
			var allTime = new Date();
			var ytd = new Date(new Date().getFullYear(), 0, 1).toLocaleDateString("en-US");
			thirtyDaysAgo.setDate(today.getDate() - 30);
			ninetyDaysAgo.setDate(today.getDate() - 90);
			allTime.setFullYear(allTime.getFullYear() - 5);

			var thirty = thirtyDaysAgo.toLocaleDateString("en-US");
			var ninety = ninetyDaysAgo.toLocaleDateString("en-US");
			var all = allTime.toLocaleDateString("en-US");

			promises.push(
				this.api.partnerClient.all({
					searchType: "aggregation",
					query: "partner_id: " + this.partnerId,
					gte: all,
					lte: lte,
					range: "created_at",
					search: true
				}).then(
					angular.bind(this, this.setData, "allClients"),
					angular.bind(this, this.onError)),

				this.api.event.all({
					searchType: "aggregation",
					query: "event_name: feedback_given AND partner_id:" + this.partnerId,
					gte: thirty,
					lte: lte,
					range: "created_at",
					search: true
				}).then(
					angular.bind(this, this.setData, "thankyou30"),
					angular.bind(this, this.onError)),

				this.api.event.all({
					searchType: "aggregation",
					query: "event_name: feedback_given AND partner_id:" + this.partnerId,
					gte: ytd,
					lte: lte,
					range: "created_at",
					search: true
				}).then(
					angular.bind(this, this.setData, "thankyouAll"),
					angular.bind(this, this.onError)),

				this.api.partnerBinder.all({
					searchType: "aggregation",
					query: "partner_id:" + this.partnerId,
					gte: thirty,
					lte: lte,
					range: "created_at",
					search: true
				}).then(
					angular.bind(this, this.setData, "binders30"),
					angular.bind(this, this.onError)),

				this.api.partnerBinder.all({
					searchType: "aggregation",
					query: "partner_id:" + this.partnerId,
					gte: ytd,
					lte: lte,
					range: "created_at",
					search: true
				}).then(
					angular.bind(this, this.setData, "bindersYtd"),
					angular.bind(this, this.onError)),

				this.api.partnerBinder.all({
					searchType: "aggregation",
					query: "partner_id:" + this.partnerId,
					gte: all,
					lte: lte,
					range: "created_at",
					search: true
				}).then(
					angular.bind(this, this.setData, "bindersAll"),
					angular.bind(this, this.onError)),

				this.api.maintenanceEvent.index({
					searchType: "aggregation",
					query: "partner.role: " + this.partner.partner_type + " AND partner.partner_id:" + this.partnerId,
					gte: thirty,
					lte: lte,
					range: "do_date",
					search: true
				}).then(
					angular.bind(this, this.setData, "maintenance30"),
					angular.bind(this, this.onError)),

				this.api.maintenanceEvent.index({
					searchType: "aggregation",
					query: "partner.role: " + this.partner.partner_type + " AND partner.partner_id:" + this.partnerId,
					gte: ytd,
					lte: lte,
					range: "do_date",
					search: true
				}).then(
					angular.bind(this, this.setData, "maintenanceYtd"),
					angular.bind(this, this.onError)),

				this.api.maintenanceEvent.index({
					searchType: "aggregation",
					query: "partner.role: " + this.partner.partner_type + " AND partner.partner_id:" + this.partnerId,
					gte: all,
					lte: lte,
					range: "do_date",
					search: true
				}).then(
					angular.bind(this, this.setData, "maintenanceAll"),
					angular.bind(this, this.onError)),

				this.api.partnerClient.all({
					searchType: "aggregation",
					query: "client.sign_in_count:>0 AND partner_id: " + this.partnerId,
					gte: ninety,
					lte: lte,
					range: "client.last_sign_in_at",
					search: true
				}).then(
					angular.bind(this, this.setData, "active90"),
					angular.bind(this, this.onError)),

				this.api.partnerClient.all({
					searchType: "aggregation",
					query: "client.sign_in_count:>0 AND partner_id: " + this.partnerId,
					gte: ytd,
					lte: lte,
					range: "client.last_sign_in_at",
					search: true
				}).then(
					angular.bind(this, this.setData, "activeYtd"),
					angular.bind(this, this.onError)),

				this.api.partnerClient.all({
					searchType: "aggregation",
					query: "client.sign_in_count:>0 AND partner_id: " + this.partnerId,
					gte: all,
					lte: lte,
					range: "created_at",
					search: true
				}).then(
					angular.bind(this, this.setData, "acceptedCurrent"),
					angular.bind(this, this.onError)),

				this.api.partnerClient.all({
					searchType: "aggregation",
					query: "client.sign_in_count:>0 AND partner_id: " + this.partnerId,
					gte: all,
					lte: thirty,
					range: "created_at",
					search: true
				}).then(
					angular.bind(this, this.setData, "accepted30"),
					angular.bind(this, this.onError)),

				this.api.partnerClient.all({
					searchType: "aggregation",
					query: "client.sign_in_count:>0 AND partner_id: " + this.partnerId,
					gte: all,
					lte: ninety,
					range: "created_at",
					search: true
				}).then(
					angular.bind(this, this.setData, "accepted90"),
					angular.bind(this, this.onError))
			);

			this.$q.all(promises).then(
				angular.bind(this, this.updatePercentages)
			);
		},

		getData: function(api, opts, name) {
			api(opts).then(
				angular.bind(this, this.setData, name),
				angular.bind(this, this.onError));
		},

		setData: function(name, response) {
			this.data[name] = response.data;
		},

		overViewSuccess: function(response) {
			this.analytics.overview = response.data;
			this.loading.close();
		},

		setTabName: function(tab) {
			this.current_tab = tab.name;
			this.callApi(tab);
		},

		callApi: function(tab) {
			this.query = "";
			if (tab.name === "Clients") {
				this.getLogins();
			}
			else if (tab.name === "Maintenance Reminders") {
				this.getReminders();
			}
			else if (tab.name === "All Users") {
				this.getInspectors();
			}
			else if (tab.name === "Agents") {
				this.getAgents();
			}
		},

		getLogins: function() {
			if (this.analytics.users.length < 1) {
				this.loading.show(this.resources.loading);
				this.api.partner.analyticsLogins(this.partnerId).then(
					angular.bind(this, this.getLoginsSuccess),
					angular.bind(this, this.onError));
			}
		},

		getLoginsSuccess: function(response) {
			this.analytics.users = response.data;
			this.filterLogins();
			this.loading.close();
		},

		getReminders: function() {
			if (this.analytics.reminders.length < 1) {
				this.loading.show(this.resources.loading);
				this.api.partner.analyticsReminders(this.partnerId).then(
					angular.bind(this, this.getRemindersSuccess),
					angular.bind(this, this.onError));
			}
		},

		getRemindersSuccess: function(response) {
			this.analytics.reminders = response.data;
			this.filterReminders();
			this.loading.close();
		},

		getInspectors: function() {
			if (this.analytics.inspectors.length < 1) {
				this.loading.show(this.resources.loading);
				var opts = {
					role: this.partner.partner_type,
					partnerId: this.partnerId,
					searchMethod: "for_partner"
				};
				this.api.user.all(opts).then(
					angular.bind(this, this.getInspectorsSuccess),
					angular.bind(this, this.onError));
			}
		},

		getInspectorsSuccess: function(response) {
			var promises = [];
			var today = new Date();
			var lte = today;
			lte.setDate(today.getDate() + 1);
			lte = lte.toLocaleDateString("en-us");
			var allTime = new Date();
			allTime.setFullYear(allTime.getFullYear() - 5);

			var all = allTime.toLocaleDateString("en-us");

			this.analytics.inspectors = response.data.items;
			this.analytics.inspectors.forEach(angular.bind(this, function(inspector, index) {
				var partner_user_id = inspector.partner_users[0].id;

				promises.push(
					this.api.partnerBinder.all({
						searchType: "aggregation",
						query: "partner_user_id: " + partner_user_id + " AND partner_id:" + this.partnerId,
						gte: all,
						lte: lte,
						range: "created_at",
						search: true
					}).then(
						angular.bind(this, this.setBinderTotal, index),
						angular.bind(this, this.onError)),

					this.api.partnerBinder.all({
						searchType: "aggregation",
						query: "accepted: true AND partner_user_id: " + partner_user_id + " AND partner_id:" + this.partnerId,
						gte: all,
						lte: lte,
						range: "created_at",
						search: true
					}).then(
						angular.bind(this, this.setAcceptedTotal, index),
						angular.bind(this, this.onError))
				);
			}));

			this.$q.all(promises).then(
				angular.bind(this, this.updateAcceptancePercentages)
			);

			this.loading.close();
		},

		setBinderTotal: function(index, response) {
			this.analytics.inspectors[index].total_binders = response.data;
		},

		setAcceptedTotal: function(index, response) {
			this.analytics.inspectors[index].accepted_binders = response.data;
		},

		updateAcceptancePercentages: function() {
			this.analytics.inspectors.forEach(angular.bind(this, function(inspector) {
				inspector.acceptance_rate = this.calculatePercentages(inspector.accepted_binders, inspector.total_binders);
			}));
		},

		getAgents: function() {
			if (this.analytics.agentAnalytics.agents.length < 1) {
				this.loading.show(this.resources.loading);
				this.api.partner.analyticsAgents(this.partnerId).then(
					angular.bind(this, this.getAgentsSuccess),
					angular.bind(this, this.onError));
			}
		},

		getAgentsSuccess: function(response) {
			this.analytics.agentAnalytics = response.data;
			this.loading.close();
		},

		filterLogins: function() {
			var begin = ((this.usersPage - 1) * this.itemsPerPage);
			var end = begin + this.itemsPerPage;
			this.filtered_logins = this.analytics.users.slice(begin, end);
		},

		filterReminders: function() {
			var begin = ((this.remindersPage - 1) * this.itemsPerPage);
			var end = begin + this.itemsPerPage;
			this.filtered_reminders = this.analytics.reminders.slice(begin, end);
		},

		pageChange: function(type) {
			if (type === "logins") {
				this.filterLogins();
			}
			else {
				this.filterReminders();
			}
		},

		showIndexInfo: function() {
			this.AnalyticsInfo.showIndexInfo();
		},

		showPerformanceInfo: function() {
			this.AnalyticsInfo.showPerformanceInfo();
		},

		showLoginInfo: function() {
			this.AnalyticsInfo.showLoginInfo();
		},

		showDownloadButton: function(items) {
			return items.length > 0;
		},

		downloadReminders: function() {
			this.loading.show(this.resources.loading);
			this.api.partner.downloadReminders(this.partnerId).then(
				angular.bind(this, this.getDownloadSuccess, "reminders_"),
				angular.bind(this, this.onError));
		},

		downloadClients: function() {
			this.loading.show(this.resources.loading);
			this.api.partner.downloadClients(this.partnerId).then(
				angular.bind(this, this.getDownloadSuccess, "clients_"),
				angular.bind(this, this.onError));
		},

		getDownloadSuccess: function(title, response) {
			var file = new Blob([response.data], {
				type: "text/csv"
			}, {
				encoding: "raw"
			});
			var filename = title + this.partnerId + ".csv";

			if (this.$window.navigator.msSaveOrOpenBlob) {
				this.$window.navigator.msSaveOrOpenBlob(file, filename);
			}
			else {
				var fileURL = this.$window.URL.createObjectURL(file);
				var a = document.createElement("a");
				a.href = fileURL;
				a.target = "_blank";
				a.download = filename;
				document.body.appendChild(a);
				a.click();
				document.body.removeChild(a);
				this.$window.URL.revokeObjectURL(file);
			}
			this.loading.close();
		},

		onError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		}

	};
})();