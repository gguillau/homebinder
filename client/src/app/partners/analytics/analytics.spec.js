describe("PartnerAnalyticsController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $state,
        $stateParams,
        modal,
        notify,
        $log,
        api,
        loading,
        AnalyticsInfo,
        context,
        response,
        defer;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $stateParams = {};

        AnalyticsInfo = {
            showIndexInfo: function() {},
            showLoginInfo: function() {},
            showPerformanceInfo: function() {}
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        modal = {
            show: function(args) {},
            confirm: function(args) {}
        };

        $state = {
            go: function(state, args) {}
        };

        $log = {
            error: function(msg) {}
        };

        notify = {
            error: function(msg) {},
            success: function(msg) {}
        };

        response = {
            data: {
                address: null,
                coupons: [],
                tags: [],
                id: 100,
                name: "John Smith Inspection Services",
                partner_type: "inspector",
                code: "JohnFREEFORLIFE",
                contact: "",
                phone: "",
                email: "johnsmith@gmail.com",
                binder_logo_id: 13,
                sellers_logo_id: null,
                binder_count: 0,
                vendors: [],
                created_at: "2015-09-21 19:09:37.484248",
                account: {
                    account_status: "active"
                }
            }
        };

        context = {
            getPartner: function() {
                defer = $q.defer();
                defer.resolve({ settings: { navigation: {} }, account: { account_sub_type: "paid" } });
                return defer.promise;
            }
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("PartnerAnalyticsController", {
            "$state": $state,
            "$stateParams": $stateParams,
            "hb.api": api,
            "$log": $log,
            "Notify": notify,
            "ModalService": modal,
            "Loading": loading,
            "AnalyticsInfo": AnalyticsInfo,
            "Context": context
        });

        $scope.ctrl = ctrl;

    }

    beforeEach(function() {
        $stateParams.partnerId = 100;
    });

    describe('ctrl.refresh', function() {
        it('should call partner analyticsOverview function successfully', function() {

            spyOn(api.partnerBinder, "all").and.returnValue($q.when({
                data: "data"
            }));

            createController();
            $rootScope.$apply();

            expect(api.partnerBinder.all).toHaveBeenCalled();

        });

        it('should call partner analyticsOverview function unsuccessfully', function() {

            spyOn(api.partnerBinder, "all").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            $rootScope.$apply();

            expect(api.partnerBinder.all).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");
        });
    });

    describe('ctrl.setTabName', function() {

        it('should change the current_tab', function() {

            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();
            var tab = {
                name: "Tab",
                disabled: false
            };
            ctrl.setTabName(tab);
            $rootScope.$apply();

            expect(ctrl.current_tab).toBe("Tab");

        });
    });

    describe('ctrl.callApi', function() {

        it('should make the correct api call for the tab', function() {

            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));
            spyOn(api.partner, "analyticsLogins").and.returnValue($q.when({
                data: []
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();
            spyOn(ctrl, "getLogins").and.callThrough();
            var tab = {
                name: "Clients",
                disabled: false
            };
            ctrl.callApi(tab);
            $rootScope.$apply();

            expect(api.partner.analyticsLogins).toHaveBeenCalledWith(100);
            expect(ctrl.getLogins).toHaveBeenCalled();
        });

        it('should make the correct api call for the tab', function() {

            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));
            spyOn(api.partner, "analyticsReminders").and.returnValue($q.when({
                data: []
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();
            spyOn(ctrl, "getReminders").and.callThrough();
            var tab = {
                name: "Maintenance Reminders",
                disabled: false
            };
            ctrl.callApi(tab);
            $rootScope.$apply();

            expect(api.partner.analyticsReminders).toHaveBeenCalledWith(100);
            expect(ctrl.getReminders).toHaveBeenCalled();
        });

        it('should make the correct api call for the tab', function() {
            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));

            createController();
            spyOn(ctrl, "getAgents");
            ctrl.callApi({ name: "Agents" });

            expect(ctrl.getAgents).toHaveBeenCalled();
        });
    });

    describe('ctrl.getLogins', function() {

        it('should make a successful call to analyticsLogins', function() {

            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));
            spyOn(api.partner, "analyticsLogins").and.returnValue($q.when({
                data: [{
                    id: 1
                }, {
                    id: 2
                }]
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();
            ctrl.getLogins();
            $rootScope.$apply();

            expect(api.partner.analyticsLogins).toHaveBeenCalledWith(100);
            expect(ctrl.analytics.users.length).toBe(2);
            expect(loading.show).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();
        });

        it('should make a unsuccessful call to analyticsLogins', function() {

            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));
            spyOn(api.partner, "analyticsLogins").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            $rootScope.$apply();
            ctrl.getLogins();
            $rootScope.$apply();

            expect(api.partner.analyticsLogins).toHaveBeenCalledWith(100);
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.show).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();
        });
    });

    describe('ctrl.getReminders', function() {

        it('should make a successful call to analyticsReminders', function() {

            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));
            spyOn(api.partner, "analyticsReminders").and.returnValue($q.when({
                data: [{
                    id: 1
                }, {
                    id: 2
                }]
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();
            ctrl.getReminders();
            $rootScope.$apply();

            expect(api.partner.analyticsReminders).toHaveBeenCalledWith(100);
            expect(ctrl.analytics.reminders.length).toBe(2);
            expect(loading.show).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();
        });

        it('should make a unsuccessful call to analyticsReminders', function() {

            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));
            spyOn(api.partner, "analyticsReminders").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            $rootScope.$apply();
            ctrl.getReminders();
            $rootScope.$apply();

            expect(api.partner.analyticsReminders).toHaveBeenCalledWith(100);
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.show).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();
        });
    });

    describe('ctrl.showIndexInfo', function() {

        it('should call the show function for the AnalyticsInfo modal', function() {

            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));
            spyOn(AnalyticsInfo, "showIndexInfo");

            createController();
            $rootScope.$apply();
            ctrl.showIndexInfo();
            $rootScope.$apply();

            expect(AnalyticsInfo.showIndexInfo).toHaveBeenCalled();
        });
    });

    describe('ctrl.showPerformanceInfo', function() {

        it('should call the show function for the AnalyticsInfo modal', function() {

            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));
            spyOn(AnalyticsInfo, "showPerformanceInfo");

            createController();
            $rootScope.$apply();
            ctrl.showPerformanceInfo();
            $rootScope.$apply();

            expect(AnalyticsInfo.showPerformanceInfo).toHaveBeenCalled();
        });
    });

    describe('ctrl.showLoginInfo', function() {

        it('should call the show function for the AnalyticsInfo modal', function() {

            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));
            spyOn(AnalyticsInfo, "showLoginInfo");

            createController();
            $rootScope.$apply();
            ctrl.showLoginInfo();
            $rootScope.$apply();

            expect(AnalyticsInfo.showLoginInfo).toHaveBeenCalled();
        });
    });

    describe('ctrl.showDownloadButton', function() {

        it('should return false', function() {

            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();
            var boolean_value = ctrl.showDownloadButton(ctrl.filtered_logins);
            $rootScope.$apply();

            expect(boolean_value).toBe(false);
            expect(loading.close).toHaveBeenCalled();
        });

        it('should return true', function() {

            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();
            ctrl.analytics.reminders = [{}, {}];
            var boolean_value = ctrl.showDownloadButton(ctrl.analytics.reminders);
            $rootScope.$apply();

            expect(boolean_value).toBe(true);
            expect(loading.close).toHaveBeenCalled();
        });

    });

    describe('ctrl.downloadReminders', function() {

        it('should make a successful call to downloadReminders', function() {

            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));
            spyOn(api.partner, "downloadReminders").and.returnValue($q.when({
                data: ""
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();
            ctrl.downloadReminders();
            $rootScope.$apply();

            expect(api.partner.downloadReminders).toHaveBeenCalledWith(100);
            expect(loading.close).toHaveBeenCalled();
        });

        it('should make a unsuccessful call to downloadReminders', function() {

            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));
            spyOn(api.partner, "downloadReminders").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            $rootScope.$apply();
            ctrl.downloadReminders();
            $rootScope.$apply();

            expect(api.partner.downloadReminders).toHaveBeenCalledWith(100);
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.close).toHaveBeenCalled();
        });
    });

    describe('ctrl.downloadClients', function() {

        it('should make a successful call to downloadClients', function() {

            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));
            spyOn(api.partner, "downloadClients").and.returnValue($q.when({
                data: ""
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();
            ctrl.downloadClients();
            $rootScope.$apply();

            expect(api.partner.downloadClients).toHaveBeenCalledWith(100);
            expect(loading.close).toHaveBeenCalled();
        });

        it('should make a unsuccessful call to downloadClients', function() {

            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));
            spyOn(api.partner, "downloadClients").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            $rootScope.$apply();
            ctrl.downloadClients();
            $rootScope.$apply();

            expect(api.partner.downloadClients).toHaveBeenCalledWith(100);
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.close).toHaveBeenCalled();
        });
    });

    describe('ctrl.getInspectors', function() {
        it('should make a successful call to user.all', function() {

            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));
            spyOn(api.user, "all").and.returnValue($q.when({
                data: "data"
            }));
            spyOn(loading, "show");

            createController();
            ctrl.partner = { partner_type: "inspector" };
            ctrl.getInspectors();

            expect(api.user.all).toHaveBeenCalled();
            expect(loading.show).toHaveBeenCalled();
        });

        it('should not make a call to user.all when inspectors have already been retrieved', function() {

            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));

            spyOn(api.user, "all").and.returnValue($q.when({
                data: "data"
            }));

            createController();
            ctrl.analytics.inspectors = ["inspector", "inspector"];
            spyOn(loading, "show");
            ctrl.getInspectors();

            expect(api.user.all).not.toHaveBeenCalled();
            expect(loading.show).not.toHaveBeenCalled();
        });
    });

    describe('ctrl.getInspectorsSuccess', function() {
        it('sets the inspectors', function() {
            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));
            createController();

            spyOn(loading, "close");
            ctrl.getInspectorsSuccess({ data: { items: [{ id: 1, partner_users: [{ id: 1 }] }] } });

            expect(ctrl.analytics.inspectors.length).toEqual(1);
            expect(loading.close).toHaveBeenCalled();
        });
    });

    describe('ctrl.getAgents', function() {
        it('should make a successful call to analyticsAgents', function() {

            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));

            spyOn(api.partner, "analyticsAgents").and.returnValue($q.when({
                data: "data"
            }));

            spyOn(loading, "show");

            createController();
            ctrl.getAgents();

            expect(api.partner.analyticsAgents).toHaveBeenCalled();
            expect(loading.show).toHaveBeenCalled();
        });

        it('should not make a call to analyticsAgents when agents have already been retrieved', function() {

            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));

            spyOn(api.partner, "analyticsAgents").and.returnValue($q.when({
                data: "data"
            }));
            createController();

            spyOn(loading, "show");
            ctrl.analytics.agentAnalytics = {
                agents: ["agent", "agent"]
            };
            ctrl.getAgents();

            expect(api.partner.analyticsAgents).not.toHaveBeenCalled();
            expect(loading.show).not.toHaveBeenCalled();
        });
    });

    describe('ctrl.getAgentsSuccess', function() {
        it('sets the agents', function() {
            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));

            spyOn(loading, "close");

            createController();
            ctrl.getAgentsSuccess({ data: {agents: [{ id: 1 }]} });

            expect(ctrl.analytics.agentAnalytics.agents.length).toEqual(1);
            expect(loading.close).toHaveBeenCalled();
        });
    });

    describe('ctrl.pageChange', function() {
        it('calls filterLogins', function() {
            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));

            createController();
            spyOn(ctrl, "filterLogins");
            ctrl.pageChange("logins");

            expect(ctrl.filterLogins).toHaveBeenCalled();
        });

        it('calls filterReminders', function() {
            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));

            createController();
            spyOn(ctrl, "filterReminders");
            ctrl.pageChange({ data: "error" });

            expect(ctrl.filterReminders).toHaveBeenCalled();
        });
    });

    describe('ctrl.getDownloadSuccess', function() {
        it('calls msSaveOrOpenBlob', function() {
            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));

            createController();
            ctrl.$window = {
                navigator: {
                    msSaveOrOpenBlob: function() {}
                }
            };

            spyOn(ctrl.$window.navigator, "msSaveOrOpenBlob");
            ctrl.getDownloadSuccess("", { data: {} });

            expect(ctrl.$window.navigator.msSaveOrOpenBlob).toHaveBeenCalled();
        });
    });

    describe('ctrl.onError', function() {
        it('calls $log, notify, and loading', function() {
            spyOn(api.partner, "analyticsOverview").and.returnValue($q.when({
                data: "data"
            }));

            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "close");

            createController();
            ctrl.onError({ data: "error" });

            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.close).toHaveBeenCalled();
        });
    });

});