(function() {
	"use strict";

	angular
		.module("hb.partner.repair_pricer", [])
		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("partner.repair_pricer", {
					url: "^/partners/:partnerId/repair_pricer",
					templateUrl: 'partners/repair_pricer/repair_pricer.tpl.html',
					controller: 'PartnersRepairPricerController',
					controllerAs: "ctrl",
					access: {
						requiresLogin: true
					}
				});
		}])
		.controller("PartnersRepairPricerController", [
			"RepairPricerModal",
			"$sce",
			PartnersRepairPricerController
		]);

	function PartnersRepairPricerController(RepairPricerModal, $sce) {
		this.RepairPricerModal = RepairPricerModal;
		this.$sce = $sce;
	}

	PartnersRepairPricerController.prototype = {

		order: function() {
			this.RepairPricerModal.show({
				link: this.$sce.trustAsResourceUrl("https://form.jotform.com/80565948184166")
			});
		}
	};
})();