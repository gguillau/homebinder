describe("PartnersRepairPricerController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        RepairPricerModal,
        $sce;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        RepairPricerModal = _$injector_.get("RepairPricerModal");
        $sce = _$injector_.get("$sce");
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("PartnersRepairPricerController", {
            "RepairPricerModal": RepairPricerModal,
            "$sce": $sce
        });
        $scope.ctrl = ctrl;
    }

    describe('ctrl.order', function() {
        it('should call RepairPricerModal', function() {
            spyOn(RepairPricerModal, "show");

            createController();
            ctrl.order();

            expect(RepairPricerModal.show).toHaveBeenCalled();
        });
    });
});