# The `src/app/partners` Directory

The partners folder contains all the client side code related to partners from 
their point of view. The landing pages, dashboards and forms are under here.

Landing pages
------------------------------------
The landing pages for the respective partners types is in the folder 
partners/{partner type}/{landing}.

For inspectors
partners/inspectors/landing/landing.html

For brokers and agents
partners/agents/landing/landing.html

States
----------------------------------------
The state tree for partners is

partners -> the base partner state. Loads a controller inherited by all partner 
controllers. Sets up the main structure of the views

partners.inspectors -> the base state for inspectors. Loads a controller 
inherited by all the inspector controllers.

partners.inspectors.binders -> displays the binders for an inspector

partners.inspectors.binder_new -> displayes the new binder form for an 
inspector

partners.inspectors.vendors -> displays the list of vendors (partners) of an 
inspector

partners.inspectors.vendor_new -> displays the new vendor form
partners.inspectors.vendor_edit -> displays the edit vendor form
partners.inspectors.users -> displays the admins and members of an inspector

partners.agents -> the base state for agents. Loads a controller inherited by 
all agent controllers.

partners.agents.binders -> displays the binders for an agent
partners.agents.users -> displays the admins and members of an agent

The binders state for both inspectors and agents use the same view and 
controller. There is conditional code in there to detect the partner type and 
act appropriately. If the view start to differ significantly we will want to 
stop using the same view and controller. The users state for both inspectors 
and agents use the same view and controller. There code is the same for all 
types of partners.

Controllers
--------------------------------------
The partners state controller has some inherited functions to modify the header 
and navigation elements. All the controllers from other states inherit the 
functions. The inspector and agent base controllers configure the navigation 
list for each type of partner.

