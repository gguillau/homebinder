(function() {
	"use strict";

	angular
		.module("hb.partner.buildfax", [])
		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("partner.buildfax", {
					url: "^/partners/:partnerId/buildfax",
					templateUrl: "partners/buildfax/buildfax.tpl.html",
					controller: "PartnerBuildFaxController",
					controllerAs: "ctrl",
					access: {
						requiresLogin: true
					}
				});
		}])
		.factory("BuildFaxController", [
			'hb.framework.indexBase',
			'hb.api',
			'hb.resources',
			'WidgetModal',
			'AfterLogin',
			'Session',
			'$state',
			'$stateParams',
			'ModalService',
			'$window',
			'Address',
			'Notify',
			'Context',
			'AccountStatus',
			function(IndexBase, api, resources, widgetModal, AfterLogin, Session, $state, $stateParams, ModalService, $window, Address, notify, context, AccountStatus) {
				var Model = function() {
					// call the parent class
					IndexBase.call(this);
					// apply warranty company specific resource strings
					this.resources = angular.extend({}, this.resources, resources.buildFaxReportIndex);
					// set the refresh API call
					this.refreshCall = api.buildFaxReport.all;
					// set the delete API call
					this.deleteCall = api.buildFaxReport.destroy;
					// set the binder_id property used in the delete message
					this.nameProperty = "name";
					this.maxSize = 10;
					this.sortOptions = [{
						orderBy: "build_fax_reports.created_at",
						order: "desc",
						desc: this.resources.creationDate + " - " + this.resources.descending
					}, {
						orderBy: "build_fax_reports.created_at",
						order: "asc",
						desc: this.resources.creationDate + " - " + this.resources.ascending
					}];
					this.sortOption = this.sortOptions[0];
					this.orderBy = this.sortOption.orderBy;
					this.order = this.sortOption.order;
					this.headers = null;
					this.partnerId = $stateParams.partnerId;
					this.reportsArgs = {
						partnerId: $stateParams.partnerId,
						status: undefined
					};
					this.Address = Address;
					this.statesOptions = Address.getStatesAndProvinces();
					this.statusOptions = [{
							name: this.resources.statusAll,
							value: undefined
						},
						{
							name: this.resources.statusPaid,
							value: "paid"
						},
						{
							name: this.resources.statusNotPaid,
							value: "not_paid"
						}
					];
					this.countries = this.Address.countries();
					this.states = this.Address.getStatesAndProvinces();
					this.currentUser = Session.getUser();
				};

				Model.prototype = Object.create(IndexBase.prototype);

				angular.extend(Model.prototype, {

					setHeaders: function() {
						this.columnspan = 8;
						this.headers = this.resources.reportAttributes.map(angular.bind(this, function(attribute) {
							var sortable = false,
								orderBy = null,
								sorted = false,
								show = true;
							if (attribute === "ID") {
								sorted = true;
								sortable = true;
								orderBy = "build_fax_reports.id";
							}
							else if (attribute === "Partner ID") {
								sortable = true;
								orderBy = "build_fax_reports.partner_id";
							}
							else if (attribute === "Report Name") {
								sortable = true;
								orderBy = "build_fax_reports.report_file_name";
							}
							else if (attribute === "Created Date") {
								sortable = true;
								orderBy = "build_fax_reports.created_at";
							}
							else if (attribute === "Status") {
								sortable = true;
								orderBy = "build_fax_reports.status";
							}
							return {
								name: attribute,
								sortable: sortable,
								sorted: sorted,
								orderBy: orderBy,
								show: show,
								order: "desc"
							};
						}));
						if (this.partnerId) {
							// remove partnerId and status columns
							this.headers.splice(5, 1);
							this.headers.splice(5, 1);
						}
						this.sortOption = this.headers[0];
					},

					updateStatus: function(index, status) {
						var item = this.items[index];
						item.processing = true;
						var data = {
							status: status
						};

						api.buildFaxReport.update(item.id, data).then(
							angular.bind(this, this.onEdited, index),
							angular.bind(this, this.errorCallback, index));
					},

					onEdited: function(index, response) {
						this.items[index] = response.data;
						this.items[index].processing = false;
					},

					addQueryArgs: function() {
						this.addArgs("partnerId");
						this.addArgs("status");
					},

					addArgs: function(arg) {
						if (this.reportsArgs[arg]) {
							this.queryArgs[arg] = this.reportsArgs[arg];
						}
						else {
							delete this.queryArgs[arg];
						}
					},

					viewReport: function(report) {
						$window.open(report.report);
					},

					searchBuildfax: function() {
						if (this.verifyStatus()) {
							this.processing = true;
							var data = {
								address: this.address1,
								city: this.city,
								state: this.state,
								zip: this.zip
							};
							this.loading.show(this.resources.searching);
							api.buildFaxReport.search(data).then(
								angular.bind(this, this.searchComplete),
								angular.bind(this, this.errorCallback, -1));
						}
						else {
							this.showStatusModal();
						}
					},

					verifyStatus: function() {
						if (!this.partner.account) {
							return false;
						}
						return this.partner.account.account_sub_type === "paid";
					},

					showStatusModal: function() {
						AccountStatus.show({
							partner: this.partner
						});
					},

					create: function() {
						this.processing = true;
						var data = {
							address: this.address1,
							city: this.city,
							state: this.state,
							zip: this.zip,
							partner_id: $stateParams.partnerId
						};
						this.loading.show(this.resources.downloading);
						api.buildFaxReport.create(data).then(
							angular.bind(this, this.createComplete),
							angular.bind(this, this.errorCallback, -1));
					},

					createComplete: function(response) {
						this.items.push(response.data);
						this.processing = false;
						this.loading.close();
					},

					searchComplete: function(response) {
						this.processing = false;
						this.loading.close();
						if (response.data.code === 2) {
							ModalService.confirm({
								message: this.resources.codeTwo,
								glyphicon: "glyphicon glyphicon-map-marker",
								title: this.resources.downloadReport,
								confirm: angular.bind(this, this.create)
							});
						}
						else if (response.data.code === 1) {
							ModalService.confirm({
								message: response.data.message + " " + this.resources.codeOne,
								glyphicon: "glyphicon glyphicon-map-marker",
								title: this.resources.downloadReport,
								confirm: angular.bind(this, this.create)
							});
						}
						else {
							ModalService.alert({
								majorMessage: response.data.message,
								glyphicon: "glyphicon glyphicon-map-marker"
							});
						}
					},

					errorCallback: function(index, response) {
						if (index >= 0) {
							this.items[index].processing = false;
						}
						notify.error(response.data);
						this.processing = false;
						this.loading.close();
					}
				});

				return Model;
			}

		])
		.controller("PartnerBuildFaxController", [
			"BuildFaxController",
			"Context",
			PartnerBuildFaxController
		]);

	function PartnerBuildFaxController(BuildFaxController, context) {
		var Model = function() {
			// call the parent class
			BuildFaxController.call(this);
			this.setHeaders();
			context.getPartner().then(angular.bind(this, function(partner) {
				this.partner = partner;
				this.refresh();
			}));
		};

		Model.prototype = Object.create(BuildFaxController.prototype);

		angular.extend(Model.prototype);

		this.model = new Model();
	}
})();