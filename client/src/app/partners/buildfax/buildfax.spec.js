describe("PartnerBuildFaxController", function() {
    var controller,
        base,
        api,
        resources,
        $window,
        AccountStatus,
        $q,
        defer;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });

        // mock the context service
        $provide.factory("Context", function() {
            return {
                getPartner: function() {
                    defer = $q.defer();
                    defer.resolve({ settings: { navigation: {} }, account: { account_sub_type: "paid" } });
                    return defer.promise;
                },
                close: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $rootScope) {
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        $window = $injector.get("$window");
        AccountStatus = $injector.get("AccountStatus");
        $q = $injector.get("$q");

        spyOn(api.buildFaxReport, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    status: "not_paid"
                }]
            }
        }));
        spyOn(api.buildFaxReport, "destroy").and.returnValue($q.when({}));

        controller = $controller("PartnerBuildFaxController", {
            "hb.framework.indexBase": base,
            "hb.api": api,
            "hb.resources": resources,
            "$window": $window,
            "AccountStatus": AccountStatus
        });

        $rootScope.$apply();
    }));

    describe("setHeaders", function() {
        it("sets headers length 8", function() {
            controller.model.setHeaders();

            expect(controller.model.headers.length).toEqual(8);
        });

        it("sets headers length 6", function() {
            controller.model.partnerId = 1;
            controller.model.setHeaders();

            expect(controller.model.headers.length).toEqual(6);
        });
    });

    describe("updateStatus", function() {
        it("returns success", inject(function($q, Notify, $log, $rootScope) {

            spyOn(api.buildFaxReport, "update").and.returnValue($q.when({
                data: { status: "paid" }
            }));

            controller.model.updateStatus(0, "paid");
            $rootScope.$apply();

            expect(api.buildFaxReport.update).toHaveBeenCalled();
            expect(controller.model.items[0].status).toEqual("paid");
        }));

        it("returns error", inject(function($q, Notify, $log, $rootScope) {

            spyOn(api.buildFaxReport, "update").and.returnValue($q.reject({
                data: "error"
            }));

            spyOn(Notify, "error");

            controller.model.updateStatus(0, "paid");
            $rootScope.$apply();

            expect(api.buildFaxReport.update).toHaveBeenCalled();
            expect(Notify.error).toHaveBeenCalled();
        }));
    });

    describe("onEdited", function() {
        it("inserts item into array", function() {
            expect(controller.model.items[0].status).toBe("not_paid");
            controller.model.onEdited(0, {
                data: { status: "paid" }
            });
            expect(controller.model.items[0].status).toBe("paid");
        });
    });

    describe("addQueryArgs", function() {
        it("adds the query arguments", inject(function() {

            controller.model.reportsArgs["partnerId"] = 1;
            controller.model.addQueryArgs();

            expect(controller.model.reportsArgs["partnerId"]).toEqual(1);
        }));

    });

    describe("addArgs", function() {
        it("adds the query argument", inject(function() {
            controller.model.reportsArgs["partnerId"] = 1;
            controller.model.addArgs("partnerId");

            expect(controller.model.queryArgs["partnerId"]).toEqual(1);
        }));
    });

    describe("viewReport", function() {
        it("opens the link with window", inject(function() {
            spyOn($window, "open");

            controller.model.viewReport({ report: "link" });

            expect($window.open).toHaveBeenCalled();
        }));
    });

    describe("searchBuildfax", function() {
        it("calls showStatusModal", inject(function($q, Notify, $log, $rootScope) {
            spyOn(controller.model, "showStatusModal");

            controller.model.partner.account = null;
            controller.model.searchBuildfax();
            $rootScope.$apply();

            expect(controller.model.showStatusModal).toHaveBeenCalled();
        }));

        it("returns success", inject(function($q, Notify, $log, $rootScope) {

            spyOn(api.buildFaxReport, "search").and.returnValue($q.when({
                data: { code: 2 }
            }));

            controller.model.searchBuildfax();
            $rootScope.$apply();

            expect(api.buildFaxReport.search).toHaveBeenCalled();
        }));

        it("returns success", inject(function($q, Notify, $log, $rootScope) {

            spyOn(api.buildFaxReport, "search").and.returnValue($q.when({
                data: { code: 1 }
            }));

            controller.model.searchBuildfax();
            $rootScope.$apply();

            expect(api.buildFaxReport.search).toHaveBeenCalled();
        }));

        it("returns success", inject(function($q, Notify, $log, $rootScope) {

            spyOn(api.buildFaxReport, "search").and.returnValue($q.when({
                data: { code: 0 }
            }));

            controller.model.searchBuildfax();
            $rootScope.$apply();

            expect(api.buildFaxReport.search).toHaveBeenCalled();
        }));

        it("returns error", inject(function($q, Notify, $log, $rootScope) {

            spyOn(api.buildFaxReport, "search").and.returnValue($q.reject({
                data: "error"
            }));

            spyOn(Notify, "error");

            controller.model.searchBuildfax();
            $rootScope.$apply();

            expect(api.buildFaxReport.search).toHaveBeenCalled();
            expect(Notify.error).toHaveBeenCalled();
        }));
    });

    describe("create", function() {
        it("returns success", inject(function($q, Notify, $log, $rootScope) {

            spyOn(api.buildFaxReport, "create").and.returnValue($q.when({
                data: { status: "paid" }
            }));

            controller.model.create();
            $rootScope.$apply();

            expect(api.buildFaxReport.create).toHaveBeenCalled();
        }));

        it("returns error", inject(function($q, Notify, $log, $rootScope) {

            spyOn(api.buildFaxReport, "create").and.returnValue($q.reject({
                data: "error"
            }));

            spyOn(Notify, "error");

            controller.model.create();
            $rootScope.$apply();

            expect(api.buildFaxReport.create).toHaveBeenCalled();
            expect(Notify.error).toHaveBeenCalled();
        }));
    });

    describe("verifyStatus", function() {
        it("returns false", function() {
            controller.model.partner.account = null;
            var result = controller.model.verifyStatus();

            expect(result).toEqual(false);
        });
        it("returns true", function() {
            controller.model.partner = {
                account: {
                    account_sub_type: "paid"
                }
            };
            var result = controller.model.verifyStatus();

            expect(result).toEqual(true);
        });
    });

    describe("showStatusModal", function() {
        it("calls AccountStatus show", function() {
            spyOn(AccountStatus, "show");

            controller.model.showStatusModal();

            expect(AccountStatus.show).toHaveBeenCalled();
        });
    });
});