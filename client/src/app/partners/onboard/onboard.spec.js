describe("PartnerOnboardController", function() {

    var $rootScope,
        $scope,
        $controller,
        $q,
        $log,
        ctrl,
        $state,
        $stateParams,
        api,
        element,
        $compile,
        Session,
        notify,
        loading,
        AfterLogin,
        uploadApi,
        $timeout,
        context,
        partner,
        resolvedPartner;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $compile = _$compile_;
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        $stateParams = {};

        partner = {
            address: null,
            coupons: [],
            tags: [],
            id: 100,
            name: "John Smith Inspection Services",
            partner_type: "inspector",
            code: "JohnFREEFORLIFE",
            contact: "",
            phone: "",
            email: "johnsmith@gmail.com",
            binder_logo_id: null,
            sellers_logo_id: null,
            binder_count: 0,
            vendors: [],
            created_at: "2015-09-21 19:09:37.484248"
        };

        context = {
            getPartner: function() {},
            setPartner: function() {}
        };

        uploadApi = {
            pendingUploads: function() {},
            updateParams: function(params) {},
            uploadFiles: function() {}
        };

        AfterLogin = {
            go: function() {}
        };

        loading = {
            show: function(msg) {},
            setMessage: function(msg) {},
            close: function() {}
        };

        $state = {
            go: function(state, args) {}
        };

        api = {
            partner: {
                get: function(partnerId) {},
                update: function(partnerId, data) {},
                onboard: function() {}
            },
            accounts: {
                create: function() {}
            }
        };

        $log = {
            error: function(msg) {}
        };

        notify = {
            success: function(msg) {},
            error: function(msg) {}
        };

        Session = {
            getJwt: function() {}
        };

        resolvedPartner = {
            address: null,
            coupons: [],
            tags: [],
            id: 100,
            name: "John Smith Inspection Services",
            partner_type: "inspector",
            code: "JohnFREEFORLIFE",
            contact: "",
            phone: "",
            email: "johnsmith@gmail.com",
            binder_logo_id: 13,
            sellers_logo_id: null,
            binder_count: 0,
            vendors: [],
            created_at: "2015-09-21 19:09:37.484248",
            account: {
                account_status: "active"
            }
        };

    }));

    function createController() {

        $scope = $rootScope.$new();
        ctrl = $controller("PartnerOnboardController", {
            "$state": $state,
            "$stateParams": $stateParams,
            "hb.api": api,
            "$log": $log,
            "Session": Session,
            "Notify": notify,
            "Loading": loading,
            "AfterLogin": AfterLogin,
            "Context": context,
            "partner": resolvedPartner
        });
        ctrl.uploader.api = uploadApi;
        $scope.ctrl = ctrl;

        element = angular.element('<hb-uploader cfg="ctrl.uploader" file-uploaded=""></hb-uploader>');
        $compile(element)($scope);
        $scope.$digest();
    }

    describe('ctrl.refresh', function() {

        it('should set the partner attributes', function() {

            var partner = {
                address: null,
                coupons: [],
                tags: [],
                id: 100,
                name: "John Smith Inspection Services",
                partner_type: "inspector",
                code: "JohnFREEFORLIFE",
                contact: "",
                phone: "",
                email: "johnsmith@gmail.com",
                binder_logo_id: null,
                sellers_logo_id: null,
                binder_count: 0,
                vendors: [],
                created_at: "2015-09-21 19:09:37.484248"
            };

            var data = {
                contact: "",
                email: "johnsmith@gmail.com",
                name: "John Smith Inspection Services"
            };

            $stateParams.partnerId = 100;
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();

            expect(ctrl.partnerId).toBe(100);
            expect(ctrl.type).toBe(partner.partner_type);
            expect(ctrl.partner).toEqual(jasmine.objectContaining(resolvedPartner));
            expect(ctrl.data).toEqual(jasmine.objectContaining(data));
            expect(ctrl.account_type).toBe("Inspector");
            expect(ctrl.leadSource[5]).toEqual({
                text: "Home Inspector"
            });
        });

        it('should send the partner to the binders view', function() {

            resolvedPartner.completed_onboarding = true;

            $stateParams.partnerId = 100;
            createController();
            spyOn(ctrl, "goToBinders").and.callThrough();
            spyOn(ctrl.afterLogin, "go").and.callThrough();
            $rootScope.$apply();
            ctrl.refresh();
            $rootScope.$apply();

            expect(ctrl.afterLogin.go).toHaveBeenCalled();

        });


    });

    describe('ctrl.setView', function() {
        beforeEach(function() {
            var partner = {
                address: null,
                coupons: [],
                tags: [],
                id: 100,
                name: "John Smith Inspection Services",
                partner_type: "inspector",
                code: "JohnFREEFORLIFE",
                contact: "",
                phone: "",
                email: "johnsmith@gmail.com",
                binder_logo_id: null,
                sellers_logo_id: null,
                binder_count: 0,
                vendors: [],
                created_at: "2015-09-21 19:09:37.484248"
            };

            $stateParams.partnerId = 100;
            spyOn(api.partner, "get").and.returnValue($q.when({
                data: partner
            }));
        });

        it('should set the next view in the onboarding process', function() {

            createController();
            ctrl.uploader = {
                api: {
                    pendingUploads: function() { return [{ id: 1 }]; }
                }
            };
            ctrl.view = 'stepOne';
            ctrl.setView('stepThree');
            expect(ctrl.view).toEqual('stepThree');

        });

        it('should set the next view in the onboarding process', function() {
            createController();
            ctrl.view = 'stepOne';
            ctrl.setView('stepFour');
            expect(ctrl.view).toEqual('stepFour');

        });

        it('should call updatePartner', function() {
            createController();
            spyOn(ctrl, "updatePartner");

            ctrl.view = 'stepOne';
            ctrl.partner.partner_type = "broker";
            ctrl.setView('stepFour');

            expect(ctrl.updatePartner).toHaveBeenCalled();

        });
    });

    describe('ctrl.isChecked', function() {
        beforeEach(function() {
            var partner = {
                address: null,
                coupons: [],
                tags: [],
                id: 100,
                name: "John Smith Inspection Services",
                partner_type: "inspector",
                code: "JohnFREEFORLIFE",
                contact: "",
                phone: "",
                email: "johnsmith@gmail.com",
                binder_logo_id: null,
                sellers_logo_id: null,
                binder_count: 0,
                vendors: [],
                created_at: "2015-09-21 19:09:37.484248"
            };

            $stateParams.partnerId = 100;
            spyOn(api.partner, "get").and.returnValue($q.when({
                data: partner
            }));
        });

        it('should increment the checked_count variable if value is true', function() {
            createController();
            ctrl.isChecked(true, "checked");
            expect(ctrl.checked_count).toEqual(1);
            expect(ctrl.source_count).toEqual(0);
        });

        it('should decrement the count variable if value is false', function() {
            createController();
            ctrl.checked_count = 1;
            ctrl.isChecked(false, "checked");
            expect(ctrl.checked_count).toEqual(0);
        });

        it('should set the value of required to false if count is greater than 0', function() {
            createController();
            ctrl.type = "broker";
            ctrl.isChecked(true, "checked");
            ctrl.isChecked(true, "source");
            expect(ctrl.required).toEqual(false);
        });

        it('should set the value of required to false if count is greater than 0', function() {
            createController();
            ctrl.isChecked(false, "checked");
            expect(ctrl.required).toEqual(true);
        });

        it('should set the value of required to false if count is greater than 0', function() {
            createController();
            ctrl.isChecked(false, "software");
            expect(ctrl.required).toEqual(true);
        });

        it('should set the value of required to false if count is greater than 0', function() {
            createController();
            ctrl.isChecked(true, "software");
            expect(ctrl.required).toEqual(true);
        });

        it('should set the value of required to false if count is greater than 0', function() {
            createController();
            ctrl.source_count = 2;
            ctrl.software_count = 1;
            ctrl.isChecked(false, "source");
            expect(ctrl.required).toEqual(false);
        });
    });

    describe('ctrl.goToBinders', function() {

        beforeEach(function() {
            var partner = {
                address: null,
                coupons: [],
                tags: [],
                id: 100,
                name: "John Smith Inspection Services",
                partner_type: "inspector",
                code: "JohnFREEFORLIFE",
                contact: "",
                phone: "",
                email: "johnsmith@gmail.com",
                binder_logo_id: null,
                sellers_logo_id: null,
                binder_count: 0,
                vendors: [],
                created_at: "2015-09-21 19:09:37.484248"
            };

            $stateParams.partnerId = 100;
            spyOn(api.partner, "get").and.returnValue($q.when({
                data: partner
            }));
        });


        it('should send the user to the partner binders state', function() {
            spyOn(AfterLogin, 'go');
            createController();
            $rootScope.$apply();
            ctrl.partner.id = 100;
            ctrl.goToBinders();
            expect(AfterLogin.go).toHaveBeenCalled();
        });
    });

    describe('ctrl.radioCheck', function() {
        beforeEach(function() {
            var partner = {
                address: null,
                coupons: [],
                tags: [],
                id: 100,
                name: "John Smith Inspection Services",
                partner_type: "inspector",
                code: "JohnFREEFORLIFE",
                contact: "",
                phone: "",
                email: "johnsmith@gmail.com",
                binder_logo_id: null,
                sellers_logo_id: null,
                binder_count: 0,
                vendors: [],
                created_at: "2015-09-21 19:09:37.484248"
            };

            $stateParams.partnerId = 100;
            spyOn(api.partner, "get").and.returnValue($q.when({
                data: partner
            }));
        });

        it('sets otherSoftware to empty string', function() {
            createController();
            ctrl.survey = {
                software: "ISN"
            };
            ctrl.radioCheck();
            expect(ctrl.survey.otherSoftware).toEqual("");
        });
    });

    describe('ctrl.updatePartner', function() {

        beforeEach(function() {


            var partner = {
                address: null,
                coupons: [],
                tags: [],
                id: 100,
                name: "John Smith Inspection Services",
                partner_type: "inspector",
                code: "JohnFREEFORLIFE",
                contact: "",
                phone: "",
                email: "johnsmith@gmail.com",
                binder_logo_id: null,
                completed_onboarding: false,
                sellers_logo_id: null,
                binder_count: 0,
                vendors: [],
                created_at: "2015-09-21 19:09:37.484248"
            };

            $stateParams.partnerId = 100;
            spyOn(api.partner, "get").and.returnValue($q.when({
                data: partner
            }));
        });


        it('should call goToBinders', function() {
            spyOn(context, "getPartner").and.returnValue({
                data: {
                    partner: partner
                }
            });

            spyOn(api.partner, 'onboard').and.returnValue($q.when({
                data: {
                    id: 100
                }
            }));
            spyOn(notify, "success");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            spyOn(ctrl, "goToBinders");
            ctrl.data = {
                id: 1,
                name: "John Smith Inspection Services",
                contact: "John Smith",
                phone: "+2024561111",
                email: "johnsmith@gmail.com"
            };

            ctrl.survey = {
                checked: {
                    "Adds value to inspections": true,
                    "Appliance Recall Checks": true
                },

                inspections: {
                    range: "10-50"
                },

                employees: {
                    range: "10-50"
                }
            };

            var data = {
                partner: ctrl.data,
                data: ctrl.survey
            };

            ctrl.updatePartner();
            $rootScope.$apply();

            expect(api.partner.onboard).toHaveBeenCalledWith(100, jasmine.objectContaining(data));
            expect(ctrl.goToBinders).toHaveBeenCalled();
            expect(loading.show).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();
        });

        it('should notify user upon submission with error', function() {

            spyOn(api.partner, 'onboard').and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(notify, "error");
            spyOn($log, "error");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();

            ctrl.updatePartner();
            $rootScope.$apply();

            expect(ctrl.formSubmitted).toEqual(false);
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.show).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();
        });

    });

});