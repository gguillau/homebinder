(function() {
    "use strict";

    angular
        .module("hb.partner.onboard", [])
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state("partner.onboard", {
                    url: "^/partners/:partnerId/onboard",
                    views: {
                        '@': {
                            templateUrl: "partners/onboard/onboard.tpl.html",
                            controller: "PartnerOnboardController",
                            controllerAs: "ctrl"
                        }
                    },
                    resolve: {
                        partner: function(partner) {
                            return partner;
                        }
                    },
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .controller("PartnerOnboardController", [
            "$state",
            "$stateParams",
            "$log",
            "hb.api",
            "Notify",
            "Session",
            "$timeout",
            "Loading",
            "AfterLogin",
            "Context",
            "hb.resources",
            "partner",
            PartnerOnboardController
        ]);

    function PartnerOnboardController($state, $stateParams, $log, api, notify, Session, $timeout, loading, AfterLogin, context, resources, partner) {
        this.$state = $state;
        this.$stateParams = $stateParams;
        this.$log = $log;
        this.api = api;
        this.partnerId = this.$stateParams["partnerId"];
        this.notify = notify;
        this.Session = Session;
        this.formSubmitted = false;
        this.$timeout = $timeout;
        this.validationErrors = resources.validationErrors;
        this.loading = loading;
        this.afterLogin = AfterLogin;
        this.context = context;
        this.resources = resources.partnerOnboard;
        this.resources = angular.extend({}, this.resources, resources.partnerRegistration);
        this.checked_count = 0;
        this.source_count = 0;
        this.software_count = 0;
        this.required = true;
        this.partner = this.partner_context = partner;
        this.transactions = [{
            range: "1-5"
        }, {
            range: "5-10"
        }, {
            range: "10-50"
        }, {
            range: "50-100"
        }, {
            range: "100+"
        }];
        this.years = [{
            range: "0-5"
        }, {
            range: "5-10"
        }, {
            range: "10-25"
        }, {
            range: "25+"
        }];
        this.inspections = [{
            range: "0-50"
        }, {
            range: "50-250"
        }, {
            range: "250-500"
        }, {
            range: "500-1000"
        }, {
            range: "1100+"
        }];
        this.newClients = [{
            range: "1-50"
        }, {
            range: "50-250"
        }, {
            range: "250-500"
        }, {
            range: "500-1000"
        }, {
            range: "1000-5000"
        }, {
            range: "5000-10,000"
        }, {
            range: "10,000+"
        }];
        this.totalClients = [{
            range: "1-50"
        }, {
            range: "50-250"
        }, {
            range: "250-500"
        }, {
            range: "500-1000"
        }, {
            range: "1000-5000"
        }, {
            range: "5000-20,000"
        }, {
            range: "20,000-100,000"
        }, {
            range: "100,000+"
        }];
        this.employees = [{
            range: "Just me"
        }, {
            range: "2-5"
        }, {
            range: "5-10"
        }, {
            range: "10-50"
        }, {
            range: "50+"
        }];
        this.agentOptions = [{
            text: this.resources.dripCampaign
        }, {
            text: this.resources.appraisal
        }, {
            text: this.resources.sellerReports
        }, {
            text: this.resources.gifts
        }];
        this.inspectorOptions = [{
            text: this.resources.threeD
        }, {
            text: this.resources.hubZone
        }, {
            text: this.resources.inspectIt
        }, {
            text: this.resources.isn
        }, {
            text: this.resources.homeGauge
        }, {
            text: this.resources.palmTech
        }, {
            text: this.resources.hip
        }, {
            text: this.resources.tap
        }, {
            text: this.resources.horizon
        }, {
            text: this.resources.spectacular
        }, {
            text: this.resources.spectora
        }, {
            text: this.resources.other
        }];
        this.leadSource = [];
        this.uploader = {
            fileTypes: "image",
            hideUploadButton: true,
            showTable: true,
            selectButtonLabel: this.resources.upload,
            fileLimit: true,
            url: "/api/v1/logos",
            jwt: this.Session.getJwt(),
            multiSelect: false,
            limitAlert: false
        };
        this.year = new Date().getFullYear();
        this.refresh();
    }

    PartnerOnboardController.prototype = {

        refresh: function() {
            if (this.partner.completed_onboarding === true) {
                this.goToBinders();
            }
            else {
                this.onRefreshSuccess();
            }
        },

        onRefreshSuccess: function() {
            this.type = this.partner.partner_type;
            this.data = {
                contact: this.partner.contact,
                email: this.partner.email,
                name: this.partner.name
            };
            var another = this.type === "inspector" ? "Home Inspector" : "Agent";
            this.leadSource = [{
                    text: this.resources.social
                }, {
                    text: this.resources.web
                }, {
                    text: this.resources.trade
                }, {
                    text: this.resources.advertising
                },
                {
                    text: this.resources.other
                }
            ];
            if (this.type === "inspector" || this.type === "broker") {
                this.leadSource.push({
                    text: another
                });
            }
            this.setType();
        },

        setType: function() {
            switch (this.type) {
                case "inspector":
                    this.account_type = "Inspector";
                    break;
                case "insurance":
                    this.account_type = "Insurance Company";
                    break;
                case "hoa":
                    this.account_type = "HOA";
                    break;
                case "broker":
                    this.account_type = "Agent/Broker";
                    break;
                case "lender":
                    this.account_type = "Lender";
                    break;
                case "homepro":
                    this.account_type = "Home Professional";
                    break;
                case "builder":
                    this.account_type = "Home Builder";
                    break;
                case "property_manager":
                    this.account_type = "Property Manager";
                    break;
            }
        },

        setView: function(view) {
            if (view === "stepFour" && this.partner.partner_type === "inspector") {
                this.view = view;
                return;
            }
            else if (view === "stepFour" && this.partner.partner_type != "inspector") {
                this.updatePartner();
                return;
            }
            this.view = view;
            var pending = this.uploader.api.pendingUploads();
            if (view === "stepThree" && pending.length > 0) {
                this.data.logo = pending[0].based64;
                this.data.logo_file_name = pending[0].name;
            }
        },

        isChecked: function(value, type) {
            if (value && (type === "checked")) {
                this.checked_count++;
            }
            if (value && (type === "software")) {
                this.software_count++;
            }
            else if (value && (type === "source")) {
                this.source_count++;
            }
            else if (!value && (type === "checked")) {
                this.checked_count--;
            }
            else if (!value && (type === "software")) {
                this.software_count--;
            }
            else if (!value && (type === "source")) {
                this.source_count--;
            }
            if (this.type === "broker") {
                if (this.source_count > 0 && this.checked_count > 0) {
                    this.required = false;
                }
                else {
                    this.required = true;
                }
            }
            else if (this.type != "broker" && this.type != "inspector") {
                if (this.source_count > 0) {
                    this.required = false;
                }
                else {
                    this.required = true;
                }
            }
            else {
                if (this.source_count > 0 && this.software_count > 0) {
                    this.required = false;
                }
                else {
                    this.required = true;
                }
            }
        },

        radioCheck: function() {
            if (this.survey.software != "Other") {
                this.survey.otherSoftware = "";
            }
        },

        updatePartner: function() {
            this.formSubmitted = true;

            this.loading.show(this.resources.updating);

            var data = {
                partner: this.data,
                data: this.survey
            };
            this.api.partner.onboard(this.partnerId, data).then(
                angular.bind(this, this.onUpdateSuccess),
                angular.bind(this, this.onUpdateError));
        },

        onUpdateSuccess: function(response) {
            this.partner = response.data;
            this.partner_context.completed_onboarding = true;
            this.partner_context.logo_file = response.data.logo_file;
            this.context.setPartner(this.partner_context);
            this.notify.success(this.resources.updated);
            this.loading.close();
            this.goToBinders();
        },

        onUpdateError: function(response) {
            this.$log.error(response);
            this.formSubmitted = false;
            this.notify.error(response.data);
            this.loading.close();
        },

        goToBinders: function() {
            this.afterLogin.go();
        }

    };
})();