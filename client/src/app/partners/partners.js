(function() {
	"use strict";

	angular.module("hb.partner", [
			"hb.partner.context",
			"hb.partner.binders",
			"hb.partner.users",
			"hb.partner.agents",
			"hb.partner.analytics",
			"hb.partner.homeowners",
			"hb.partner.onboard",
			"hb.partner.settings",
			"hb.partner.registration",
			"hb.partner.resources",
			"hb.partner.templates",
			"hb.partner.widgets",
			"hb.partner.dashboards",
			"hb.partner.annualPropertyReviews",
			"hb.partner.buildfax",
			"hb.partner.welcome",
			"hb.partner.repair_pricer",
			"hb.partner.homepros",
			"hb.partner.maintenanceItems"
		])
		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("partner", {
					url: "^/partners/:partnerId",
					abstract: true,
					templateUrl: "partners/partners.tpl.html",
					controller: "PartnerController",
					controllerAs: "ctrl",
					resolve: {
						partner: function(Context, $stateParams) {
							return Context.getPartner($stateParams.partnerId);
						}
					}
				});
		}])
		.controller("PartnerController", [
			"$state",
			"$stateParams",
			"$log",
			"hb.api",
			"hb.utils",
			"Session",
			"Notify",
			"Context",
			"$window",
			"partner",
			PartnerController
		]);

	function PartnerController($state, $stateParams, $log, api, utils, session, notify, context, $window, partner) {
		this.$state = $state;
		this.$stateParams = $stateParams;
		this.$log = $log;
		this.api = api;
		this.utils = utils;
		this.notify = notify;
		this.session = session;
		this.currentUser = this.session.getUser();
		this.context = context;
		this.$window = $window;
		this.partnerId = this.$stateParams["partnerId"];
		this.navCfg = {
			navList: []
		};
		this.partner = partner;
		this.setNavigation();
	}

	PartnerController.prototype = {
		setNavigation: function() {
			var navigation = this.partner.settings.navigation;
			if (navigation.binders && this.currentUser.role != "apr_reviewer") {
				this.navCfg.navList.push({
					id: "binders",
					state: "partner.binders",
					label: "Binders",
					icon: "glyphicon glyphicon-home"
				});
			}

			if (navigation.agents && this.currentUser.role != "apr_reviewer") {
				this.navCfg.navList.push({
					id: "agents",
					state: "partner.agents",
					label: "Agents",
					icon: "glyphicon glyphicon-tags"
				});
			}
			if (navigation.users && this.currentUser.role != "apr_reviewer") {
				this.navCfg.navList.push({
					id: "users",
					state: "partner.users",
					label: "Users",
					icon: "glyphicon glyphicon-user"
				});
			}
			if (this.currentUser.role != "apr_reviewer") {
				this.navCfg.navList.push({
					id: "settings",
					state: "partner.settings",
					label: "Settings",
					icon: "glyphicon glyphicon-wrench"

				});
			}
			if (navigation.homepros && this.currentUser.role != "apr_reviewer") {
				this.navCfg.navList.push({
					id: "homepros",
					state: "partner.homepros",
					label: "Preferred Home Pros",
					icon: "glyphicon glyphicon-book"
				});
			}
			if (navigation.users) {
				this.navCfg.navList.push({
					id: "templates",
					state: "partner.templates",
					label: "Templates",
					icon: "glyphicon glyphicon-th-list"

				});
			}

			if (navigation.analytics) {
				this.navCfg.navList.push({
					id: "analytics",
					state: "partner.analytics",
					label: "Analytics",
					icon: "glyphicon glyphicon-stats"
				});
			}

			if (navigation.resources) {
				this.navCfg.navList.push({
					id: "resources",
					state: "partner.resources",
					label: "Resource Center",
					icon: "glyphicon glyphicon-file"
				});
			}

			this.navCfg.partner = this.partner;
			if (this.partner.completed_onboarding === false) {
				this.$state.go("partner.onboard", {
					partnerId: this.partner.id
				});
			}
			if (this.partner.can_perform_apr && navigation.apr) {
				this.navCfg.navList.push({
					id: "aprs",
					state: "partner.annualPropertyReviews",
					label: "APRs",
					icon: "glyphicon glyphicon-calendar"

				});
			}
			if (this.currentUser.role != "apr_reviewer" && navigation.buildfax) {
				this.navCfg.navList.push({
					id: "buildfax",
					state: "partner.buildfax",
					label: "BuildFax",
					icon: "glyphicon glyphicon-map-marker"
				});
			}

			if (this.currentUser.role === "broker") {
				this.navCfg.navList.push({
					id: "repair_pricer",
					state: "partner.repair_pricer",
					label: "Repair Pricer",
					icon: "glyphicon glyphicon-list-alt"
				});
			}
			this.context.setNavList(this.navCfg);
		}
	};
})();
