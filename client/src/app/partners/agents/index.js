(function() {
	"use strict";

	angular.module('hb.partner.agents', [
			'hb.partner.agents.edit',
			"hb.partner.agents.import",
			"hb.partner.agents.merge",
			"hb.partner.agents.new",
			'ui.router',
			'ui.bootstrap',
			'hb.components'
		])
		.config(['$stateProvider', partnerAgentsConfig])
		.controller("AgentsIndexController", [
			"UsersIndexController",
			"$stateParams",
			"hb.resources",
			"PartnerAgentsImportForm",
			"AgentMergeModal",
			AgentsIndexController
		]);

	function AgentsIndexController(UsersIndexController, $stateParams, resources, PartnerAgentsImportForm, AgentMergeModal) {

		var Model = function() {
			// call the parent class
			UsersIndexController.call(this);
			this.resources = angular.extend({}, this.resources, resources.partnerAgentsIndex);
			this.editState = "partner.agents_edit";
			this.editParams = {};
			this.editParamsProperty = "userId";
			this.newState = "partner.agents_new";
			this.PartnerAgentsImportForm = PartnerAgentsImportForm;
			this.AgentMergeModal = AgentMergeModal;
			this.$stateParams = $stateParams;
			this.partnerId = this.$stateParams.partnerId;
			this.queryArgs = {
				role: "agent",
				partnerId: this.partnerId,
				searchMethod: "for_partner"
			};
			this.userArgs.role = "agent";
			this.toolbarCfg.filter = null;
			// set page specific values on toolbar
			angular.extend(this.toolbarCfg, {
				title: this.resources.title,
				button: {
					title: this.resources.newButton,
					click: angular.bind(this, this.newItem)
				}
			});
			this.settings.canEdit = true;
			this.settings.canImpersonate = false;
			this.settings.canViewHeadshot = true;
			this.columns.sign_in_count = false;
			this.partnerAgentsTable = true;

			this.refresh();
		};

		Model.prototype = Object.create(UsersIndexController.prototype);

		angular.extend(Model.prototype, {
			refresh: function() {
				this.numChecked = 0;
				this.maxChecked = 2;
				UsersIndexController.prototype.refresh.call(this);
			},

			// remove sign in count
			removeHeaders: function() {
				this.headers.splice(5, 1);
			},

			deleteItem: function(item, index) {
				this.api.partner.removeUser(this.partnerId, item.id).then(
					angular.bind(this, this.onDeleteItemSuccess, index),
					angular.bind(this, this.onDeleteItemError)
				);
			},

			newItem: function() {
				this.PartnerAgentsImportForm.show({
					closed: angular.bind(this, this.refresh)
				});
			},

			agentMergeModal: function() {
				var agents = [];
				this.items.forEach(function(agent) {
					if (agent.checked) {
						agents.push(agent);
					}
				});
				if (this.numChecked === 1) {
					this.AgentMergeModal.showAgentSearch({
						agent1: agents[0]
					});
				}
				else if (this.numChecked === 2) {
					this.AgentMergeModal.showMerge({
						agents: agents
					});
				}
			},

			checkChanged: function(agent) {
				if (agent.checked) {
					this.numChecked++;
				}
				else {
					this.numChecked--;
				}
			}
		});

		this.model = new Model();
	}

	function partnerAgentsConfig($stateProvider) {
		$stateProvider
			.state('partner.agents', {
				url: '^/partners/:partnerId/agents',
				templateUrl: 'components/users/index/index.tpl.html',
				controller: "AgentsIndexController",
				controllerAs: "ctrl",
				access: {
					requiresLogin: true
				}
			});
	}

})();