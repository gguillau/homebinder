describe("AgentsIndexController", function() {
    var controller,
        base,
        $q_,
        notify,
        api,
        PartnerAgentsImportForm,
        AgentMergeModal;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        $q_ = $q;
        base = $injector.get("UsersIndexController");
        notify = $injector.get("Notify");
        api = $injector.get("hb.api");
        PartnerAgentsImportForm = $injector.get("PartnerAgentsImportForm");
        AgentMergeModal = $injector.get("AgentMergeModal");

        controller = $controller("AgentsIndexController", {
            "UsersIndexController": base,
            "Notify": notify,
            "hb.api": api,
            "AgentMergeModal": AgentMergeModal
        });
    }));
    
    describe("init", function() {
        it("sets the headers", function() {
            expect(controller.model.settings.canEdit).toEqual(true);
            expect(controller.model.settings.canImpersonate).toEqual(false);
            expect(controller.model.settings.canViewHeadshot).toEqual(true);
            expect(controller.model.columns.sign_in_count).toEqual(false);
            expect(controller.model.partnerAgentsTable).toEqual(true);
        });
    });

    describe("removeHeaders", function() {
        it("sets the headers", function() {
            expect(controller.model.headers.length).toEqual(5);
        });
    });

    describe("deleteItem", function() {
        it("calls api", function() {
            spyOn(api.partner, "removeUser").and.returnValue($q_.when({ data: {} }));
            controller.model.deleteItem({ partner_role: "member" }, 0);

            expect(api.partner.removeUser).toHaveBeenCalled();
        });
    });

    describe("newItem", function() {
        it("calls api", function() {
            spyOn(PartnerAgentsImportForm, "show");
            controller.model.newItem();

            expect(PartnerAgentsImportForm.show).toHaveBeenCalled();
        });
    });
    
    describe("agentMergeModal", function() {
        it("calls nothing when no agents are checked", function() {
            spyOn(AgentMergeModal, "showAgentSearch");
            spyOn(AgentMergeModal, "showMerge");

            controller.model.numChecked = 0;
            controller.model.items = [{id: 1, checked: false}, {id: 2, checked: false}];
            controller.model.agentMergeModal();
            
            expect(AgentMergeModal.showAgentSearch).not.toHaveBeenCalled();
            expect(AgentMergeModal.showMerge).not.toHaveBeenCalled();
        });
        
        it("calls showAgentSearch when only one agent is checked", function() {
            spyOn(AgentMergeModal, "showAgentSearch");
            
            controller.model.numChecked = 1;
            controller.model.items = [{id: 1, checked: true}, {id: 2, checked: false}];
            controller.model.agentMergeModal();
            
            expect(AgentMergeModal.showAgentSearch).toHaveBeenCalledWith({agent1: {id: 1, checked: true}});
        });
        
        it("calls showAgentSearch when only one agent is checked", function() {
            spyOn(AgentMergeModal, "showMerge");

            var agents = [{id: 1, checked: true}, {id: 2, checked: true}];
            controller.model.numChecked = 2;
            controller.model.items = agents;
            controller.model.agentMergeModal();
            
            expect(AgentMergeModal.showMerge).toHaveBeenCalledWith({agents: agents});
        });
    });
    
    describe("checkChanged", function() {
        it("increases numChecked when the agent is checked", function() {
            controller.model.numChecked = 0;
            controller.model.checkChanged({checked: true});
            
            expect(controller.model.numChecked).toEqual(1);
        });
        
        it("increases numChecked when the agent is checked", function() {
            controller.model.numChecked = 1;
            controller.model.checkChanged({checked: false});
            
            expect(controller.model.numChecked).toEqual(0);
        });
    });

});