(function() {
    "use strict";

    angular.module('hb.partner.agents.merge.search', [])
        .controller("AgentSearchModalController", [
            "data",
            "$modalInstance",
            "UsersIndexController",
            "$stateParams",
            "hb.resources",
            "AgentMergeModal",
            AgentSearchModalController
        ]);

    function AgentSearchModalController(data, $modal, UsersIndexController, $stateParams, resources, AgentMergeModal) {

        var Model = function() {
            // call the parent class
            UsersIndexController.call(this);
            this.agent1 = data.agent1;
            this.agent2 = {};
            this.$modal = $modal;
            this.resources = angular.extend({}, this.resources, resources.partnerAgentsIndex);
            this.resources = angular.extend({}, this.resources, resources.agentSearchModal);
            this.AgentMergeModal = AgentMergeModal;

            this.$stateParams = $stateParams;
            this.partnerId = this.$stateParams.partnerId;
            this.queryArgs = {
                role: "agent",
                partnerId: this.partnerId,
                searchMethod: "for_partner"
            };
            this.userArgs.role = "agent";

            this.refresh();
        };

        Model.prototype = Object.create(UsersIndexController.prototype);

        angular.extend(Model.prototype, {
            refresh: function() {
                this.numChecked = 1;
                this.maxChecked = 2;
                UsersIndexController.prototype.refresh.call(this);
            },

            // remove sign in count and role
            removeHeaders: function() {
                this.headers.splice(4, 2);
            },

            checkChanged: function(agent) {
                if (agent.checked) {
                    this.numChecked++;
                }
                else {
                    this.numChecked--;
                }
            },

            agentMergeModal: function() {
                this.close();

                var agents = [this.agent1];
                this.items.forEach(angular.bind(this, function(agent) {
                    if (agent.checked && (agent.id !== this.agent1.id)) {
                        this.agent2 = agent;
                        agents.push(this.agent2);
                    }
                }));
                this.AgentMergeModal.showMerge({
                    agents: agents
                });
            },

            close: function() {
                this.$modal.close();
            }
        });

        this.model = new Model();
    }

})();