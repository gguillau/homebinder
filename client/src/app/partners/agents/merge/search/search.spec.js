describe("AgentSearchModalController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $modal,
        UsersIndexController,
        $stateParams,
        api,
        notify,
        AgentMergeModal;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$templateCache_, _$compile_) {
        $rootScope = _$injector_.get("$rootScope");
        UsersIndexController = _$injector_.get("UsersIndexController");
        $stateParams = _$injector_.get("$stateParams");
        api = _$injector_.get("hb.api");
        notify = _$injector_.get("Notify");
        AgentMergeModal = _$injector_.get("AgentMergeModal");
        
        $modal = {
            close: function(arg) {}
        };
        
        $controller = _$controller_;
    }));

    function createController() {
        $scope = $rootScope.$new();
        
        var agent1 = {
                user_profile_attributes: {
                    first_name: "first",
                    last_name: "last",
                    email: "email",
                    company: "company",
                    mobile_phone: {national: "phone"},
                    address_attributes: {address1: "address"},
                    logo_file: "logo",
                    headshot_file: "headshot",
                    message: "message",
                    branding: {binder_count: 1}
                }
            };
        
        var data = {agent1: agent1};
        
        ctrl = $controller("AgentSearchModalController", {
            "data": data,
            "$modalInstance": $modal,
            "UsersIndexController": UsersIndexController,
            "$stateParams": $stateParams,
            "hb.api": api,
            "Notify": notify,
            "AgentMergeModal": AgentMergeModal
        });

        $scope.ctrl = ctrl;
        $scope.$apply();
    }

    describe("refresh", function() {
        it("sets numChecked and maxChecked, and calls UsersIndexController refresh method", function() {
            spyOn(UsersIndexController.prototype.refresh, "call");
            createController();
            
            ctrl.model.numChecked = 0;
            ctrl.model.maxChecked = 0;
            ctrl.model.refresh();

            expect(ctrl.model.numChecked).toEqual(1);
            expect(ctrl.model.maxChecked).toEqual(2);
            expect(UsersIndexController.prototype.refresh.call).toHaveBeenCalled();
        });
    });
    
    describe("removeHeaders", function() {
        it("sets numChecked and maxChecked, and calls UsersIndexController refresh method", function() {
            createController();
            
            ctrl.model.removeHeaders();

            expect(ctrl.model.headers.length).toEqual(4);
        });
    });
    
    describe("checkChanged", function() {
        it("increases numChecked when the agent is checked", function() {
            createController();
            ctrl.model.numChecked = 0;
            ctrl.model.checkChanged({checked: true});
            
            expect(ctrl.model.numChecked).toEqual(1);
        });
        
        it("increases numChecked when the agent is checked", function() {
            createController();
            ctrl.model.numChecked = 1;
            ctrl.model.checkChanged({checked: false});
            
            expect(ctrl.model.numChecked).toEqual(0);
        });
    });
    
    describe("agentMergeModal", function() {
        it("closes the current modal and opens the second modal", function() {
            spyOn(AgentMergeModal, "showMerge");
            createController();
            spyOn(ctrl.model, "close");
            
            var agent1 = {id: 1, checked: true};
            var agent2 = {id: 2, checked: true};
            var agent3 = {id: 3, checked: false};
            ctrl.model.agent1 = agent1;
            ctrl.model.items = [agent1, agent2, agent3];
            ctrl.model.agentMergeModal();
            
            expect(ctrl.model.close).toHaveBeenCalled();
            expect(AgentMergeModal.showMerge).toHaveBeenCalledWith({agents: [agent1, agent2]});
        });
    });
    
    describe("close", function() {
        it("closes the modal", function() {
            createController();
            spyOn($modal, "close");

            ctrl.model.close();

            expect($modal.close).toHaveBeenCalled();
        });
    });

});