(function() {
    "use strict";

    angular.module("hb.partner.agents.merge", [
            "hb.partner.agents.merge.search"
        ])
        .factory("AgentMergeModal", [
            "ModalService",
            function(Modals) {
                return {
                    showMerge: function(opts) {
                        Modals.show({
                            templateUrl: "partners/agents/merge/merge.tpl.html",
                            controller: "AgentMergeModalController as ctrl",
                            resolveData: opts,
                            backdrop: "static"
                        });
                    },
                    showAgentSearch: function(opts) {
                        Modals.show({
                            templateUrl: "partners/agents/merge/search/search.tpl.html",
                            controller: "AgentSearchModalController as ctrl",
                            resolveData: opts,
                            backdrop: "static"
                        });
                    }
                };
            }
        ])
        .controller("AgentMergeModalController", [
            "data",
            "$modalInstance",
            "ModalService",
            "hb.resources",
            "hb.api",
            "Notify",
            "$log",
            "Loading",
            "Session",
            AgentMergeModalController
        ]);

    function AgentMergeModalController(data, $modal, ModalService, resources, api, notify, $log, loading, Session) {
        this.agents = data.agents;
        this.agent1 = {};
        this.agent2 = {};
        this.$modal = $modal;
        this.modals = ModalService;
        this.resources = resources.agentMergeModal;
        this.api = api;
        this.notify = notify;
        this.$log = $log;
        this.loading = loading;
        this.checkedAgent = 1;
        this.user = Session.getUser();
        this.refresh();
    }

    AgentMergeModalController.prototype = {

        refresh: function() {
            var agents = [];

            for (var i = 0; i < 2; i++) {
                var attributes = this.agents[i].user_profile_attributes,
                    id = this.agents[i].id,
                    firstName = attributes.first_name,
                    lastName = attributes.last_name,
                    email = attributes.email,
                    company = attributes.company,
                    phone = attributes.mobile_phone ? attributes.mobile_phone.national : null,
                    address = attributes.address_attributes ? attributes.address_attributes.address1 : null,
                    logo = attributes.logo_file,
                    headshot = attributes.head_shot_file,
                    additionalFields = this.hasAdditionalFields(this.agents[i]),
                    yes = this.resources.yes,
                    no = this.resources.no;

                agents[i] = {
                    id: id,
                    firstName: firstName ? firstName : no,
                    lastName: lastName ? lastName : no,
                    email: email ? email : no,
                    company: company ? company : no,
                    phone: phone ? phone : no,
                    address: address ? address : no,
                    logoPresent: logo ? yes : no,
                    headshotPresent: headshot ? yes : no,
                    additionalFields: additionalFields ? yes : no
                };

                var opts = {
                    searchType: "aggregation",
                    query: "user_id: " + agents[i].id,
                    search: true
                };
                this.api.userBinder.all(opts).then(
                    angular.bind(this, this.getNumBinders, agents, i),
                    angular.bind(this, this.onError));
            }

            this.agent1 = agents[0];
            this.agent2 = agents[1];
        },

        getNumBinders: function(agents, i, response) {
            agents[i].numBinders = typeof response.data === "number" ? response.data : "n/a";
        },

        hasAdditionalFields: function(data) {
            var attributes = data.user_profile_attributes;

            if (attributes.message || attributes.bio || attributes.website || attributes.dob || attributes.home_phone || attributes.sex) {
                return true;
            }
            return false;
        },

        close: function() {
            this.$modal.close();
        },

        confirmMerge: function() {
            this.modals.confirm({
                title: this.resources.confirm,
                message: this.resources.message,
                confirm: angular.bind(this, this.getNewAndOldAgents)
            });
        },

        getNewAndOldAgents: function() {
            var oldAgent = {};
            var newAgent = {};

            if (this.checkedAgent == 1) {
                oldAgent = this.agent2;
                newAgent = this.agent1;
            }
            else if (this.checkedAgent == 2) {
                oldAgent = this.agent1;
                newAgent = this.agent2;
            }

            this.mergeAgents(oldAgent.id, newAgent.id);
        },

        mergeAgents: function(old_agent_id, new_agent_id) {
            this.loading.show(this.resources.loading);
            var opts = {
                old_agent_id: old_agent_id,
                new_agent_id: new_agent_id,
                merger_id: this.user.id
            };
            this.api.user.mergeAgents(opts).then(
                angular.bind(this, this.mergeAgentSuccess),
                angular.bind(this, this.mergeAgentError));
        },

        mergeAgentSuccess: function(response) {
            this.close();
            this.notify.success(this.resources.agentsMerged);
            this.loading.close();
        },

        mergeAgentError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.close();
            this.loading.close();
        }

    };

})();
