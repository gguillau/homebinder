describe("AgentMergeModalController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $modal,
        modals,
        api,
        notify,
        $log,
        loading,
        $q;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$templateCache_, _$compile_) {
        $rootScope = _$injector_.get("$rootScope");
        modals = _$injector_.get("ModalService");
        api = _$injector_.get("hb.api");
        notify = _$injector_.get("Notify");
        $log = _$injector_.get("$log");
        loading = _$injector_.get("Loading");
        $q = _$injector_.get("$q");
        
        $modal = {
            close: function(arg) {}
        };
        
        $controller = _$controller_;
    }));

    function createController() {
        $scope = $rootScope.$new();
            var agent1 = {
                id: 1,
                user_profile_attributes: {
                    first_name: "first",
                    last_name: "last",
                    email: "email",
                    company: "company",
                    mobile_phone: {national: "phone"},
                    address_attributes: {address1: "address"},
                    logo_file: "logo",
                    head_shot_file: "headshot",
                    message: "message",
                    branding: {binder_count: 1}
                }
            };
            var agent2 = {
                id: 2,
                user_profile_attributes: {
                    mobile_phone: {},
                    address_attributes: {},
                    branding: {}
                }
            };
            
            var data = {agents: [agent1, agent2]};
        
        
        ctrl = $controller("AgentMergeModalController", {
            "$modalInstance": $modal,
            "ModalService": modals,
            "data": data,
            "hb.api": api,
            "Notify": notify,
            "$log": $log,
            "Loading": loading
        });

        $scope.ctrl = ctrl;
        $scope.$apply();
    }
    
    describe("refresh", function() {
        it("sets the agents information to be displayed", function() {
            createController();
            spyOn(api.userBinder, "all").and.returnValue($q.when({data: 1}));

            var result1 = {
                id: 1,
                firstName: "first",
                lastName: "last",
                email: "email",
                company: "company",
                phone: "phone",
                address: "address",
                logoPresent: "Yes",
                headshotPresent: "Yes",
                additionalFields: "Yes"
            };
            var result2 = {
                id: 2,
                firstName: "No",
                lastName: "No",
                email: "No",
                company: "No",
                phone: "No",
                address: "No",
                logoPresent: "No",
                headshotPresent: "No",
                additionalFields: "No"
            };
            ctrl.refresh();

            expect(ctrl.agent1).toEqual(result1);
            expect(ctrl.agent2).toEqual(result2);
            expect(api.userBinder.all.calls.count()).toEqual(2);
        });
    });
    
    describe("hasAdditionalFields", function() {
        it("returns false when no additional fields are present", function() {
            createController();

            var data = {user_profile_attributes: {}};
            var result = ctrl.hasAdditionalFields(data);

            expect(result).toEqual(false);
        });
        
        it("returns true when a message is present", function() {
            createController();

            var data = {user_profile_attributes: {message: "message"}};
            var result = ctrl.hasAdditionalFields(data);

            expect(result).toEqual(true);
        });
        
        it("returns true when a bio is present", function() {
            createController();

            var data = {user_profile_attributes: {bio: "bio"}};
            var result = ctrl.hasAdditionalFields(data);

            expect(result).toEqual(true);
        });
        
        it("returns true when a website is present", function() {
            createController();

            var data = {user_profile_attributes: {website: "website"}};
            var result = ctrl.hasAdditionalFields(data);

            expect(result).toEqual(true);
        });
        
        it("returns true when a dob is present", function() {
            createController();

            var data = {user_profile_attributes: {dob: "dob"}};
            var result = ctrl.hasAdditionalFields(data);

            expect(result).toEqual(true);
        });
        
        it("returns true when a home phone is present", function() {
            createController();

            var data = {user_profile_attributes: {home_phone: "home_phone"}};
            var result = ctrl.hasAdditionalFields(data);

            expect(result).toEqual(true);
        });
        
        it("returns true when a sex is present", function() {
            createController();

            var data = {user_profile_attributes: {sex: "sex"}};
            var result = ctrl.hasAdditionalFields(data);

            expect(result).toEqual(true);
        });
    });

    describe("close", function() {
        it("closes the modal", function() {
            createController();
            spyOn($modal, "close");

            ctrl.close();

            expect($modal.close).toHaveBeenCalled();

        });
    });
    
    describe("confirmMerge", function() {
        it("calls the confirmation modal", function() {
            spyOn(modals, "confirm");
            createController();

            ctrl.confirmMerge();

            expect(modals.confirm).toHaveBeenCalled();
        });
    });
    
    describe("getNewAndOldAgents", function() {
        it("gets new and old agents when checkedAgent is 1 and calls mergeAgents", function() {
            spyOn(api.user, "mergeAgents");
            createController();
            spyOn(ctrl, "mergeAgents");

            var agent1 = {id: 1};
            var agent2 = {id: 2};
            ctrl.agent1 = agent1;
            ctrl.agent2 = agent2;
            ctrl.checkedAgent = 1;
            ctrl.getNewAndOldAgents();

            expect(ctrl.mergeAgents).toHaveBeenCalledWith(2, 1);
        });
        
        it("gets new and old agents when checkedAgent is 2 and calls mergeAgents", function() {
            spyOn(api.user, "mergeAgents");
            createController();
            spyOn(ctrl, "mergeAgents").and.returnValue($q.when({ data: null }));

            var agent1 = {id: 1};
            var agent2 = {id: 2};
            ctrl.agent1 = agent1;
            ctrl.agent2 = agent2;
            ctrl.checkedAgent = 2;
            ctrl.getNewAndOldAgents();

            expect(ctrl.mergeAgents).toHaveBeenCalledWith(1, 2);
        });
    });
    
    describe("mergeAgents", function() {
        it("calls loading.show and api.user.mergeAgents", function() {
            spyOn(loading, "show");
            spyOn(api.user, "mergeAgents").and.returnValue($q.when({ data: null }));
            createController();

            ctrl.user = {id: 3};
            ctrl.mergeAgents(1, 2);

            expect(loading.show).toHaveBeenCalled();
            expect(api.user.mergeAgents).toHaveBeenCalledWith({old_agent_id: 1, new_agent_id: 2, merger_id: 3});
        });
    });
    
    describe("mergeAgentSuccess", function() {
        it("calls this.close, notify.success, and loading.close", function() {
            createController();
            spyOn(ctrl, "close");
            spyOn(notify, "success");
            spyOn(loading, "close");

            ctrl.mergeAgentSuccess();

            expect(ctrl.close).toHaveBeenCalled();
            expect(notify.success).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();
        });
    });
    
    describe("mergeAgentError", function() {
        it("calls $log.error, notify.error, this.close, and loading.close", function() {
            createController();
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(ctrl, "close");
            spyOn(loading, "close");

            ctrl.mergeAgentError({data: "error"});

            expect($log.error).toHaveBeenCalledWith({data: "error"});
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(ctrl.close).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();
        });
    });

});