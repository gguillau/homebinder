(function() {
    "use strict";

    angular.module('hb.partner.agents.edit', [])
        .config(['$stateProvider', partnerAgentsEditConfig])
        .controller("PartnerAgentsEditController", [
            'UsersEditController',
            "hb.resources",
            PartnerAgentsEditController
        ]);

    function PartnerAgentsEditController(UsersEditController, resources) {
        var Model = function() {
            // call the parent class
            UsersEditController.call(this);
            this.resources = angular.extend({}, this.resources, resources.partnerAgentsIndex);
            this.title = this.resources.title;
            this.roles = [{
                name: "agent",
                value: "Agent"
            }];
            this.partnerId = this.$stateParams.partnerId;
            this.indexState = "partner.agents";
            this.indexParams = { partnerId: this.partnerId };
            // partners
            this.navigation_links[0].links.push({
                active: false,
                name: "Partners",
                value: "partners"
            });
            // refresh the item
            this.refresh();
            // allow inspectors to edit
            this.settings.canEdit = true;
        };

        Model.prototype = Object.create(UsersEditController.prototype);

        angular.extend(Model.prototype);

        this.model = new Model();
    }

    function partnerAgentsEditConfig($stateProvider) {
        $stateProvider
            .state('partner.agents_edit', {
                url: '^/partners/:partnerId/agents/:userId/edit',
                templateUrl: 'components/users/form/form.tpl.html',
                controller: "PartnerAgentsEditController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
    }
})();
