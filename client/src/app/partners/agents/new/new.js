(function() {
    "use strict";

    angular.module('hb.partner.agents.new', [])
        .config(['$stateProvider', partnerAgentsNewConfig])
        .controller("PartnerAgentsNewController", [
            "UsersNewController",
            "$stateParams",
            "hb.resources",
            PartnerAgentsNewController
        ]);

    function PartnerAgentsNewController(UsersNewController, $stateParams, resources) {

        var Model = function() {
            // call the parent class
            UsersNewController.call(this);
            /*jshint -W024*/
            this.resources = angular.extend({}, this.resources, resources.partnerAgentsNew);
            this.title = this.resources.title;
            // this is changed in the backend so no security concerns here.
            this.item.password = "password";
            this.item.role = "agent";
            this.roles = [{
                name: "agent",
                value: "Agent"
            }];
            this.item.user_profile_attributes.address_attributes.country = this.country.value;
            this.$stateParams = $stateParams;
            this.partnerId = this.$stateParams.partnerId;
            this.indexState = "partner.agents";
            this.indexParams = { partnerId: this.partnerId };
            this.data.partnerId = this.partnerId;
            this.data.role = "agent";
        };

        Model.prototype = Object.create(UsersNewController.prototype);

        angular.extend(Model.prototype);

        this.model = new Model();
    }

    function partnerAgentsNewConfig($stateProvider) {
        $stateProvider
            .state('partner.agents_new', {
                url: '^/partners/:partnerId/agents/new',
                templateUrl: 'components/users/form/form.tpl.html',
                controller: "PartnerAgentsNewController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
    }

})();