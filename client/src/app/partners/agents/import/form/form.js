(function() {
    "use strict";

    angular
        .module("hb.partner.agents.import.form", [])
        .factory("PartnerAgentsImportForm", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "partners/agents/import/form/form.tpl.html",
                            controller: "PartnerAgentsImportFormController as ctrl",
                            backdrop: "static",
                            keyboard: false,
                            closed: opts.closed
                        });
                    }
                };
            }
        ])
        .controller("PartnerAgentsImportFormController", [
            "$modalInstance",
            "$state",
            "$stateParams",
            "hb.resources",
            PartnerAgentsImportFormController
        ]);

    function PartnerAgentsImportFormController($modal, $state, $stateParams, resources) {
        this.$modal = $modal;
        this.$state = $state;
        this.partnerId = $stateParams.partnerId;
        this.resources = resources.agentImportForm;
    }

    PartnerAgentsImportFormController.prototype = {

        newAgent: function() {
            this.$modal.dismiss("cancel");
            this.$state.go("partner.agents_new", {
                partnerId: this.partnerId
            });
        },

        importAgent: function() {
            this.$modal.dismiss("cancel");
            this.$state.go("partner.agents_import", {
                partnerId: this.partnerId
            });
        },

        cancel: function() {
            if (this.step == "three") {
                this.$modal.close();
            }
            else {
                this.$modal.dismiss("cancel");
            }
        }

    };
})();