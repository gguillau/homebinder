(function() {
    "use strict";

    angular.module('hb.partner.agents.import', [
            "hb.partner.agents.import.form",
            'ui.router',
            'ui.bootstrap',
            'hb.components'
        ])
        .config(['$stateProvider', partnerAgentsImportConfig])
        .controller("AgentsImportController", [
            "hb.api",
            "Notify",
            "$log",
            "Loading",
            "$state",
            "$stateParams",
            "Papa",
            "$scope",
            "$timeout",
            "Address",
            "hb.resources",
            AgentsImportController
        ]);

    function AgentsImportController(api, notify, $log, loading, $state, $stateParams, Papa, $scope, $timeout, Address, resources) {
        this.api = api;
        this.notify = notify;
        this.$log = $log;
        this.loading = loading;
        this.$state = $state;
        this.partnerId = $stateParams.partnerId;
        this.Papa = Papa.Papa;
        this.$scope = $scope;
        this.$timeout = $timeout;
        this.Address = Address;
        this.headers = this.agents = [];
        this.states = this.Address.getStatesAndProvinces();
        this.countries = this.Address.countries();
        this.parsed_file = null;
        this.isLoading = false;
        this.resources = resources.agentImport;
        this.step = "one";
        this.uploader = {
            fileTypes: "csv",
            hideUploadButton: true,
            showTable: false,
            selectButtonLabel: this.resources.upload,
            url: "/api/v1/documents/",
            id: "uploadDocuments",
            hide_upload: false,
            uploadOnAdd: true,
            multiSelect: false
        };
        this.totalAgents = 0;
        this.current_page = 1;
        this.agentsPerPage = 20;
        this.maxSize = 5;
        this.filtered_agents = [];
    }

    AgentsImportController.prototype = {

        parse: function(files) {
            this.loading.show(this.resources.reading);
            var pending = this.uploader.api.pendingUploads();
            this.$timeout(angular.bind(this, function() {
                this.Papa.parse(pending[0], {
                    preview: 2000,
                    header: true,
                    step: angular.bind(this, this.parseStep),
                    skipEmptyLines: true,
                    fastMode: true,
                    beforeFirstChunk: angular.bind(this, this.chunk),
                    complete: angular.bind(this, this.parseComplete)
                });
            }));
        },

        chunk: function(chunk) {
            var rows = chunk.split(/\r\n|\r|\n/);
            rows[0] = ["first_name", "last_name", "company", "email", "mobile_phone", "address1", "address2", "city", "state", "zip", "country", "website", "headshot", "logo"];
            return rows.join('\n');
        },

        parseStep: function(results, parse) {
            if (this.headers.length < 1) {
                this.headers = results.meta.fields;
            }
            this.agents.push(results.data[0]);
        },

        parseComplete: function(results, file) {
            this.$scope.$apply();
            if (this.agents.length > 0) {
                this.importAgents();
            }
            else {
                var pending = this.uploader.api.pendingUploads();
                this.uploader.api.remove(pending[0]);
                this.notify.info(this.resources.emptyFile);
                this.loading.close();
            }
        },

        importAgents: function() {
            this.loading.show(this.resources.importing);
            var options = {
                users: this.agents,
                role: "agent"
            };
            this.api.partner.import(this.partnerId, options).then(
                angular.bind(this, this.onUploadSuccess),
                angular.bind(this, this.onUploadError)
            );
        },

        onUploadSuccess: function(response) {
            this.notify.success(this.resources.success);
            this.loading.close();
            this.$state.go("partner.agents", {
                partnerId: this.partnerId
            });
        },

        onUploadError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            var pending = this.uploader.api.pendingUploads();
            this.uploader.api.remove(pending[0]);
            this.loading.close();
        }

    };

    function partnerAgentsImportConfig($stateProvider) {
        $stateProvider
            .state('partner.agents_import', {
                url: '^/partners/:partnerId/agents/import',
                templateUrl: 'partners/agents/import/import.tpl.html',
                controller: "AgentsImportController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
    }

})();