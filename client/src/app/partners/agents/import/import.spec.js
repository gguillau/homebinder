describe("AgentsImportController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $modal,
        api,
        notify,
        $log,
        loading,
        $q,
        $stateParams,
        $state,
        uploader,
        $timeout;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        $timeout = _$injector_.get("$timeout");

        $stateParams = {};

        $state = {
            go: function() {}
        };

        api = {
            partner: {
                import: function(id, options) {}
            }
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        $modal = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };

        notify = {
            success: function() {},
            error: function() {},
            info: function() {}
        };

        $log = {
            error: function() {}
        };

        uploader = {
            api: {
                pendingUploads: function() {}
            }
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        $stateParams.partnerId = 100;
        ctrl = $controller("AgentsImportController", {
            "$modalInstance": $modal,
            "hb.api": api,
            "Notify": notify,
            "$log": $log,
            "Loading": loading,
            "$state": $state,
            "$stateParams": $stateParams,
            "$scope": $scope
        });
        ctrl.uploader = uploader;
        $scope.ctrl = ctrl;
    }

    describe("ctrl.parse", function() {
        it("should call Papa parse", function() {
            createController();
            ctrl.uploader = {
                api: {
                    pendingUploads: function() {}
                }
            };
            spyOn(ctrl.uploader.api, "pendingUploads").and.returnValue([{ id: 1 }]);
            spyOn(ctrl.Papa, "parse");

            ctrl.parse();
            $timeout.flush();

            expect(ctrl.Papa.parse).toHaveBeenCalled();
        });
    });
    
    describe("ctrl.chunk", function() {
        it("adds header row", function() {
            createController();
            var rows = ctrl.chunk("");
            
            expect(rows).toEqual("first_name,last_name,company,email,mobile_phone,address1,address2,city,state,zip,country,website,headshot,logo");
        });
    });

    describe("ctrl.parseStep", function() {
        it("sets the headers and adds the agents", function() {
            createController();
            var results = {
                data: [{ id: 1 }],
                meta: {
                    fields: ["email"]
                }
            };
            ctrl.parseStep(results, {});

            expect(ctrl.headers.length).toEqual(1);
            expect(ctrl.agents.length).toEqual(1);
        });
    });

    describe("ctrl.parseComplete", function() {
        it("calls importAgents", function() {
            createController();
            spyOn(ctrl, "importAgents");
            ctrl.agents = [{ id: 1 }];
            ctrl.parseComplete({}, {});

            expect(ctrl.importAgents).toHaveBeenCalled();
        });

        it("calls notify", function() {
            createController();
            spyOn(notify, "info");
            ctrl.agents = [];
            ctrl.uploader = {
                api: {
                    remove: function() {},
                    pendingUploads: function() { return [{}]; }
                }
            };
            ctrl.parseComplete({}, {});

            expect(ctrl.notify.info).toHaveBeenCalled();
        });
    });

    describe("ctrl.importAgents", function() {
        it("should call import API and return success", function() {

            spyOn(api.partner, "import").and.returnValue($q.when({
                data: []
            }));

            createController();
            ctrl.importAgents();

            expect(api.partner.import).toHaveBeenCalled();
        });
    });

    describe("ctrl.onUploadSuccess", function() {
        it("should $state go", function() {

            spyOn($modal, "dismiss");
            spyOn(loading, "close");
            spyOn($state, "go");

            createController();
            ctrl.onUploadSuccess({ data: { id: 1 } });

            expect($state.go).toHaveBeenCalledWith("partner.agents", {
                partnerId: 100
            });
            expect(loading.close).toHaveBeenCalled();
        });
    });

    describe("ctrl.onUploadError", function() {
        it("should notify error", function() {

            spyOn(notify, "error");
            spyOn(loading, "close");

            createController();
            ctrl.uploader = {
                api: {
                    remove: function() {},
                    pendingUploads: function() { return [{}]; }
                }
            };
            ctrl.onUploadError({ data: "error" });

            expect(notify.error);
            expect(loading.close).toHaveBeenCalled();
        });
    });

});