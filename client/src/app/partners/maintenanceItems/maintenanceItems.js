(function() {
    "use strict";

    angular
        .module("hb.partner.maintenanceItems", [])
        .directive("partnersMaintenanceItems", function() {
            return {
                restrict: "E",
                templateUrl: "partners/maintenanceItems/maintenanceItems.tpl.html",
                controller: "PartnerMaintenanceItemsController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("PartnerMaintenanceItemsController", [
            "BinderMaintenanceItemsController",
            "hb.resources",
            "Session",
            PartnerMaintenanceItemsController
        ]);

    function PartnerMaintenanceItemsController(BinderMaintenanceItemsController, resources, session) {
        var Model = function() {
            // call the parent class
            BinderMaintenanceItemsController.call(this);
            this.currentUser = session.getUser();
            this.resources = angular.extend({}, this.resources, resources.maintenanceItemsIndex);
            this.resources.attributes = this.resources.partnerAttributes;
            this.resources.title = this.resources.partnerTitle;
            this.itemsPerPage = 10;
            this.maintenanceItemsArgs = {
                binderId: undefined,
                name: undefined,
                do_date: undefined,
                created_by: this.currentUser.id
            };
            // delete prop
            this.nameProperty = "name";
            this.current_filter = null;
            this.datepicker = {
                format: "MMMM dd, yyyy",
                options: {
                    "show-button-bar": false
                }
            };
            this.sortOptions = [{
                orderBy: "maintenance_events.do_date",
                order: "asc",
                desc: this.resources.creationDate + " - " + this.resources.descending
            }, {
                orderBy: "maintenance_items.created_at",
                order: "desc",
                desc: this.resources.creationDate + " - " + this.resources.descending
            }, {
                orderBy: "maintenance_items.created_at",
                order: "asc",
                desc: this.resources.creationDate + " - " + this.resources.ascending
            }, {
                orderBy: "maintenance_items.name",
                order: "desc",
                desc: this.resources.name + " - " + this.resources.descending
            }, {
                orderBy: "maintenance_items.name",
                order: "asc",
                desc: this.resources.name + " - " + this.resources.ascending
            }];
            this.sortOption = this.sortOptions[0];
            this.orderBy = this.sortOption.orderBy;
            this.order = this.sortOption.order;
            this.date_opened = false;
            this.setHeaders();
            this.init();
        };

        Model.prototype = Object.create(BinderMaintenanceItemsController.prototype);

        angular.extend(Model.prototype, {

            setHeaders: function() {
                this.headers = this.resources.attributes.map(angular.bind(this, function(attribute) {
                    var sortable = false,
                        orderBy = null,
                        sorted = false;
                    if (attribute === "ID") {
                        sorted = true;
                        sortable = true;
                        orderBy = "maintenance_items.id";
                    }
                    else if (attribute === "Name") {
                        sortable = true;
                        orderBy = "maintenance_items.name";
                    }
                    else if (attribute === "Binder ID") {
                        sortable = true;
                        orderBy = "maintenance_items.binder_id";
                    }
                    return {
                        name: attribute,
                        sortable: sortable,
                        sorted: sorted,
                        orderBy: orderBy,
                        order: "desc"
                    };
                }));
                this.headerOptions = angular.copy(this.headers);
                this.sortOption = this.headers[0];
            },

            addQueryArgs: function() {
                this.addArgs("binderId");
                this.addArgs("name");
                this.addArgs("do_date");
                this.addArgs("created_by");
            },

            addArgs: function(arg) {
                if (this.maintenanceItemsArgs[arg]) {
                    this.queryArgs[arg] = this.maintenanceItemsArgs[arg];
                }
                else {
                    delete this.queryArgs[arg];
                }
            }
        });

        this.model = new Model();
    }

})();