angular
    .module('hb.partner.annualPropertyReviews', [
        'hb.partner.annualPropertyReviews.request'
    ])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state('partner.annualPropertyReviews', {
                url: '^/partners/:partnerId/annual_property_reviews',
                template: '<hb-annual-property-reviews cfg="{ view: \'partner\' }"></hb-annual-property-reviews>',
                access: {
                    requiresLogin: true
                }
            });
    }]);