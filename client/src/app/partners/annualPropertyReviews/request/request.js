angular
.module('hb.partner.annualPropertyReviews.request', [
])
.config(['$stateProvider', function($stateProvider) {
    $stateProvider
        .state('annualPropertyReviewRequest', {
            url: '^/partners/:partnerId/apr',
            template: '<hb-annual-property-review-request></hb-annual-property-review-request>'
        });
}]);