describe("PartnerController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $state,
        $stateParams,
        utils,
        notify,
        $log,
        api,
        Session;

    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        $provide.value("partner", {
            address: null,
            coupons: [],
            tags: [],
            id: 100,
            name: "John Smith Inspection Services",
            partner_type: "inspector",
            code: "JohnFREEFORLIFE",
            contact: "",
            phone: "",
            email: "johnsmith@gmail.com",
            binder_logo_id: null,
            can_perform_apr: true,
            sellers_logo_id: null,
            binder_count: 0,
            vendors: [],
            completed_onboarding: false,
            created_at: "2015-09-21 19:09:37.484248",
            settings: {
                navigation: {
                    binders: true,
                    users: true,
                    vendors: true,
                    isn: true,
                    settings: true,
                    analytics: true,
                    agents: true
                },
                binderForm: {
                    usage: false,
                    clientInformation: true,
                    agentInformation: true,
                    maintenance: true,
                    appliances: true,
                    documents: true,
                    images: true,
                    notes: true,
                    contractors: true,
                    autoTransfer: true
                }
            }

        });
    }));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $state = _$injector_.get("$state");
        api = _$injector_.get("hb.api");
        $log = _$injector_.get("$log");
        notify = _$injector_.get("Notify");
        $q = _$injector_.get("$q");

        $stateParams = {};

        utils = {
            utils: {}
        };

        Session = {
            getUser: function() {
                return { role: "admin" };
            }
        };
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("PartnerController", {
            "$state": $state,
            "$stateParams": $stateParams,
            "hb.api": api,
            "hb.utils": utils,
            "$log": $log,
            "Session": Session,
            "Notify": notify
        });
        $scope.ctrl = ctrl;
    }

    describe('ctrl.setNavigation', function() {
        it('inits the controller', function() {
            createController();
            expect(ctrl.navCfg.navList.length).toEqual(6);
        });
    });
});