describe("homebinder", function() {
    'use strict';
    var AppDelegate, Session, $scope, api, $q, $location, $state;

    beforeEach(module("homebinder"));

    beforeEach(inject(function(_$injector_, _$rootScope_) {
        Session = _$injector_.get("Session");
        $scope = _$rootScope_;
        AppDelegate = _$injector_.get("AppDelegate");
        $location = _$injector_.get("$location");
        $state = _$injector_.get("$state");
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
    }));

    it("re-arranges the array", function() {
        var array = [1, 2, 3, 4, 5];
        expect(array[0]).toEqual(1);

        array.move(0, 4);

        expect(array[0]).toEqual(2);
    });
    
    it("re-arranges the array when new index is greater than length", function() {
        var array = [1, 2, 3, 4, 5];
        expect(array[0]).toEqual(1);

        array.move(0, 6);

        expect(array[0]).toEqual(2);
    });

    describe("AppDelegate", function() {
        it("logs on and then off", function() {
            spyOn(Session, "setJwt").and.callThrough();

            var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MywiZW1haWwiOiJkZXZAaG9tZWJpbmRlci5jb20iLCJ1c2VyX3Rva2VuIjoic001TGtqc1J2Z21aXzVOZlNqLU4iLCJyb2xlIjoiYWRtaW4iLCJjcmVhdGVkX2F0IjoiMjAxOC0wMy0wNiAxNzoyNjo0NSAtMDUwMCIsImludGVyY29tX2tleSI6IjJiZjJiMTA2YTExMjhmOWQ3YjExNDUzNTYxNTJlZTY1ZDZiZTA0YzgxYmU0ZGNiOWRmMTljMzVlZTgzMmYxMDgifQ.x46fq31HiOY3zkmEhsDYjJaYRs5CscIlhM3YX6bIJus";
            AppDelegate.loggedOn(token);

            expect(Session.setJwt).toHaveBeenCalled();

            spyOn(api.user, "logoff").and.returnValue($q.when({ data: null }));

            AppDelegate.logOff();
            $scope.$apply();

            expect(api.user.logoff).toHaveBeenCalled();
        });
    });
    
    describe("$urlRouterProvider", function() {
        it("sends the user to the seller report page", function() {
            
            $location.url("/SellerReport/2342304");
            $scope.$apply();
            
            expect($state.current.name).toEqual("seller_report");
        });
    });
});