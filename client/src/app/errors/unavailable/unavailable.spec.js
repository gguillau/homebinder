describe("UnavailableController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        Notify,
        AfterLogin;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        Notify = _$injector_.get("Notify");
        AfterLogin = _$injector_.get("AfterLogin");
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("UnavailableController", {
            "Notify": Notify,
            "AfterLogin": AfterLogin
        });
        $scope.ctrl = ctrl;

    }

    describe('brandClick', function() {
        it('should call AfterLogin goToDashboard', function() {
            spyOn(AfterLogin, "goToDashboard");

            createController();
            ctrl.brandClick();

            expect(AfterLogin.goToDashboard).toHaveBeenCalled();
        });
    });
});
