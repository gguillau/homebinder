(function() {
    "use strict";

    angular
        .module("hb.errors.unavailable", [
            "ui.router"
        ])
        .config([
            '$stateProvider',
            function($stateProvider) {
                $stateProvider
                    .state("unavailable", {
                        url: "^/unavailable",
                        templateUrl: "errors/unavailable/unavailable.tpl.html",
                        controller: "UnavailableController",
                        controllerAs: "ctrl"
                    });
            }
        ])
        .controller("UnavailableController", [
            "Notify",
            "AfterLogin",
            UnavailableController
        ]);

    function UnavailableController(Notify, AfterLogin) {
        this.AfterLogin = AfterLogin;
        Notify.info("We are currently performing maintenance on the site.");
    }

    UnavailableController.prototype = {

        brandClick: function() {
            this.AfterLogin.goToDashboard();
        }

    };

})();