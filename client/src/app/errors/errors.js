(function() {
    "use strict";
    
    angular
        .module("hb.errors", [
            "ui.router",
			"hb.errors.unavailable"
        ])
        .config(['$stateProvider', errorsConfig]);
        
    function errorsConfig($stateProvider) {
        $stateProvider
            .state('internal_error', {
				url: '^/internal_error',
				templateUrl: 'errors/500.tpl.html'
			});
    }
})();