angular
    .module("hb.framework", [
        "hb.framework.indexBase",
        "hb.framework.editBase",
        "hb.framework.newBase",
        "hb.framework.toggleModalBase"
    ]);