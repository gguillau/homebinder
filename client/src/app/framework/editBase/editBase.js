angular
    .module("hb.framework.editBase", [])
    .factory("hb.framework.editBase", [
        'ModalService',
        'Notify',
        '$log',
        'Loading',
        'hb.resources',
        '$state',
        function(modals, notify, $log, loading, resources, $state) {
            var EditBase = function() {
                // resources used by the class. Resources here are
                // only used in code not the view. Override in subclass
                this.resources = angular.extend({}, this.resources, resources.editBase);
                // the item we are going to edit
                this.item = {};
                // the item being sent to the server for updating
                this.updatedItem = null;
                this.loading = loading;
                this.notify = notify;
                this.$log = $log;
                this.settings = {
                    readOnly: true,
                    canEdit: true,
                    canCancel: true
                };
            };

            angular.extend(EditBase.prototype, {

                // refreshes the current view
                refresh: function() {
                    // make sure the refresh API call is set
                    if (!this.refreshCall) {
                        throw "refreshCall not set";
                    }
                    // display the loading message
                    loading.show(this.resources.loading);
                    // get the item
                    this.refreshCall(this.itemId).then(
                        angular.bind(this, this.onRefreshSuccess),
                        angular.bind(this, this.onRefreshError)
                    );
                },

                // Refresh callback. Updates the item list and totals
                onRefreshSuccess: function(response) {
                    this.item = response.data;
                    loading.close();
                },

                // Refresh error callback. Logs an error and notifies the user of an error
                onRefreshError: function(response) {
                    $log.error(response);
                    notify.error(response.data);
                    loading.close();
                },

                // set up the saveItem object to be sent to the server
                beforeSave: function() {},

                // performs a save on the server
                onSave: function() {
                    // set the updatedItem
                    this.beforeSave();

                    // make sure the updatedItem is set
                    if (!this.updatedItem) {
                        throw "updatedItem not set";
                    }

                    this.form.$submitted = true;
                    if (this.form.$invalid) {
                        return;
                    }

                    loading.show(this.resources.savingMsg);
                    this.saveCall(this.item.id, this.updatedItem).then(
                        angular.bind(this, this.onSaveSuccess),
                        angular.bind(this, this.onSaveError)
                    );
                },

                // save success
                onSaveSuccess: function(response) {
                    this.cancelForm();
                    this.item = response.data;
                    notify.success(this.resources.saveSuccess);
                    loading.close();
                },

                // save error
                onSaveError: function(response) {
                    $log.error(response);
                    notify.error(response.data);
                    loading.close();
                },

                // lets the user edit the form
                editForm: function() {
                    this.settings.readOnly = false;
                },

                // lets the user exist edit s
                cancelForm: function() {
                    this.settings.readOnly = true;
                },

                // send user back to editState
                edit: function() {
                    $state.go(this.editState, this.editParams);
                },

                // send user back to editBase/indexBase
                back: function() {
                    $state.go(this.indexState, this.indexParams);
                }
            });

            return EditBase;
        }
    ]);