describe("EditBase", function() {
    var EditBase,
        TestClass,
        $state;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
        // mock the notify service
        $provide.factory("Notify", function() {
            return {
                success: function() {},
                error: function() {}
            };
        });
        // mock the $log service

        $provide.factory("$log", function() {
            return {
                error: function() {},
                warn: function() {}
            };
        });

        // mock the modals service
        $provide.factory("ModalService", function() {
            return {
                confirm: function() {}
            };
        });
    }));
    // get the base class under test
    beforeEach(inject(function($injector) {
        EditBase = $injector.get("hb.framework.editBase");
        $state = $injector.get("$state");
    }));

    function createTestClass() {
        TestClass = function() {
            EditBase.call(this);
            this.resources = {};
        };

        TestClass.prototype = Object.create(EditBase.prototype);

        return new TestClass();
    }

    describe("refresh", function() {
        it("calls refreshCall", inject(function($q, $rootScope) {
            var model = createTestClass();

            model.refreshCall = function() {};
            spyOn(model, "refreshCall").and.returnValue($q.when({ data: { items: [{ name: "item 1" }, { name: "item 2" }], total: 2 } }));
            model.refresh();

            expect(model.refreshCall).toHaveBeenCalled();
        }));
    });

    describe("onRefreshSuccess", function() {
        it("sets the item", inject(function($q, $rootScope) {
            var model = createTestClass();
            model.onRefreshSuccess({ data: { id: 1 } });

            expect(model.item).toEqual({ id: 1 });
        }));
    });

    describe("onRefreshError", function() {
        it("calls notify", inject(function($q, $rootScope, Notify) {
            spyOn(Notify, "error");

            var model = createTestClass();
            model.onRefreshError({ data: "error" });

            expect(Notify.error).toHaveBeenCalled();
        }));
    });

    describe("onSave", function() {
        it("calls saveCall", inject(function($q, $rootScope) {
            var model = createTestClass();

            model.saveCall = function() {};
            spyOn(model, "saveCall").and.returnValue($q.when({ data: { items: [{ name: "item 1" }, { name: "item 2" }], total: 2 } }));
            model.form = { $invalid: false };
            model.updatedItem = { id: 1 };
            model.item = { id: 1 };
            model.onSave();

            expect(model.saveCall).toHaveBeenCalled();
        }));
    });

    describe("onSaveSuccess", function() {
        it("sets the item", inject(function($q, $rootScope) {
            var model = createTestClass();
            model.onSaveSuccess({ data: { id: 1 } });

            expect(model.item).toEqual({ id: 1 });
        }));
    });

    describe("onSaveError", function() {
        it("calls notify", inject(function($q, $rootScope, Notify) {
            spyOn(Notify, "error");

            var model = createTestClass();
            model.onSaveError({ data: "error" });

            expect(Notify.error).toHaveBeenCalled();
        }));
    });

    describe("editForm", function() {
        it("sets readOnly to false", inject(function($q, $rootScope, Notify) {
            spyOn(Notify, "error");

            var model = createTestClass();
            model.editForm();

            expect(model.settings.readOnly).toEqual(false);
        }));
    });
    
    describe("back", function() {
        it("calls $state go", inject(function($q, $rootScope, Notify) {
            spyOn($state, "go");

            var model = createTestClass();
            model.back();

            expect($state.go).toHaveBeenCalled();
        }));
    });

});