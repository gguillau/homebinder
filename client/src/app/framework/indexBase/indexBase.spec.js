describe("IndexBase", function() {
    var IndexBase,
        TestClass,
        responder,
        $state;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
        // mock the notify service
        $provide.factory("Notify", function() {
            return {
                success: function() {},
                error: function() {}
            };
        });
        // mock the $log service

        $provide.factory("$log", function() {
            return {
                error: function() {},
                warn: function() {}
            };
        });

        // mock the modals service
        $provide.factory("ModalService", function() {
            return {
                confirm: function() {}
            };
        });
    }));
    // get the base class under test
    beforeEach(inject(function($injector) {
        IndexBase = $injector.get("hb.framework.indexBase");
        $state = $injector.get("$state");
    }));

    function createTestClass() {
        TestClass = function() {
            IndexBase.call(this);

            this.resources = angular.extend({}, this.resources, {
                loading: "loading",
                loadError: "load error",
                confirmDelete: "confirm",
                deleteMsg1: "a ",
                deleteMsg2: " b",
                deleteSuccess: "deleted",
                deleteError: "delete error"
            });

            this.nameProperty = "name";
        };

        TestClass.prototype = Object.create(IndexBase.prototype);

        return new TestClass();
    }

    responder = {
        refreshCall: function() {},
        deleteCall: function() {}
    };

    describe("clearSearch", function() {
        it("clears the search value", inject(function($q, $rootScope) {

            var model = createTestClass();
            model.searchValue = "findme";
            expect(model.searchValue).toEqual("findme");
            model.clearSearch();

            expect(model.searchValue).toBe(undefined);
        }));
    });

    describe("sort", function() {
        it("sets the order and orderBy to the passed sortOption", inject(function($q, $rootScope) {
            var model = createTestClass();
            spyOn(model, "refresh");
            model.sort({ order: "desc", orderBy: "name" });

            expect(model.order).toEqual("desc");
            expect(model.orderBy).toEqual("name");
        }));

        it("sets the order and orderBy to null", inject(function($q, $rootScope) {
            var model = createTestClass();
            spyOn(model, "refresh");
            model.sort();

            expect(model.order).toBeNull();
            expect(model.orderBy).toBeNull();
        }));
    });

    describe("refresh", function() {
        it("throws when refreshCall is not set", function() {
            var model = createTestClass();

            expect(model.refresh).toThrow();
        });

        it("provides current page and items per page", inject(function($q, $rootScope, Loading) {
            spyOn(responder, "refreshCall").and.returnValue($q.when({ data: { items: [{ name: "item 1" }, { name: "item 2" }], total: 2 } }));
            spyOn(Loading, "show");
            spyOn(Loading, "close");

            var model = createTestClass();
            model.refreshCall = responder.refreshCall;
            model.refresh();

            $rootScope.$apply();
            expect(model.items.length).toEqual(2);
            expect(model.toolbarCfg.total).toEqual(2);
            expect(responder.refreshCall).toHaveBeenCalledWith(jasmine.objectContaining({ page: 1, count: 10 }));
            expect(Loading.show).toHaveBeenCalledWith(model.resources.loading);
            expect(Loading.close).toHaveBeenCalled();
        }));

        it("includes search value", inject(function($q, $rootScope) {
            spyOn(responder, "refreshCall").and.returnValue($q.when({ data: { items: [{ name: "item 1" }, { name: "item 2" }], total: 2 } }));

            var model = createTestClass();
            model.refreshCall = responder.refreshCall;
            model.searchValue = "search";
            model.refresh();

            $rootScope.$apply();
            expect(model.items.length).toEqual(2);
            expect(responder.refreshCall).toHaveBeenCalledWith(jasmine.objectContaining({ page: 1, count: 10, search: "search" }));
        }));

        it("includes query args", inject(function($q, $rootScope) {
            spyOn(responder, "refreshCall").and.returnValue($q.when({ data: { items: [{ name: "item 1" }, { name: "item 2" }], total: 2 } }));

            var model = createTestClass();
            model.refreshCall = responder.refreshCall;
            model.queryArgs = { query: "args" };
            model.refresh();

            $rootScope.$apply();
            expect(model.items.length).toEqual(2);
            expect(responder.refreshCall).toHaveBeenCalledWith(jasmine.objectContaining({ page: 1, count: 10, query: "args" }));
        }));

        it("handles an error", inject(function($q, $rootScope, Notify, $log) {
            spyOn(responder, "refreshCall").and.returnValue($q.reject({ data: "error" }));
            spyOn(Notify, "error");
            spyOn($log, "error");

            var model = createTestClass();
            model.refreshCall = responder.refreshCall;
            model.refresh();

            $rootScope.$apply();
            expect(Notify.error).toHaveBeenCalledWith("error");
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
        }));
    });

    describe("pageChange", function() {
        it("sets new page and refreshes", inject(function($q, $rootScope) {
            spyOn(responder, "refreshCall").and.returnValue($q.when({ data: { items: [{ name: "item 1" }, { name: "item 2" }], total: 2 } }));

            var model = createTestClass();
            model.refreshCall = responder.refreshCall;
            model.currentPage = 2;
            model.pageChange();

            $rootScope.$apply();
            expect(model.items.length).toEqual(2);
            expect(responder.refreshCall).toHaveBeenCalledWith(jasmine.objectContaining({ page: 2, count: 10 }));
        }));
    });

    describe("search", function() {
        it("resets the page and refreshes", inject(function($q, $rootScope) {
            spyOn(responder, "refreshCall").and.returnValue($q.when({ data: { items: [{ name: "item 1" }, { name: "item 2" }], total: 2 } }));

            var model = createTestClass();
            model.refreshCall = responder.refreshCall;
            model.searchValue = "findme";
            model.search();

            $rootScope.$apply();
            expect(model.items.length).toEqual(2);
            expect(responder.refreshCall).toHaveBeenCalledWith(jasmine.objectContaining({ page: 1, count: 10, search: "findme" }));
        }));
    });

    describe("deleteItem", function() {
        it("throws when deleteCall not set", function() {
            var model = createTestClass();
            model.items = [{ name: "item 1" }, { name: "item 2" }];
            expect(model.deleteItem).toThrow();
        });

        it("confirms with the user", inject(function($q, ModalService) {
            spyOn(ModalService, "confirm");

            var model = createTestClass();
            model.items = [{ name: "item 1" }, { name: "item 2" }];
            model.deleteCall = responder.deleteCall;
            model.deleteItem(model.items[0], 0);

            expect(ModalService.confirm).toHaveBeenCalledWith(jasmine.objectContaining({ title: model.resources.confirmDelete, message: "a item 1 b" }));
        }));

        it("deletes an item", inject(function($q, $rootScope, Loading, Notify) {
            spyOn(responder, "deleteCall").and.returnValue($q.when({}));
            spyOn(Loading, "show");
            spyOn(Loading, "close");
            spyOn(Notify, "success");

            var model = createTestClass();
            model.items = [{ id: 1, name: "item 1" }, { id: 2, name: "item 2" }];
            model.toolbarCfg.total = 2;
            model.deleteCall = responder.deleteCall;
            model.onConfirmDeleteItem(model.items[0], 0);

            $rootScope.$apply();
            expect(responder.deleteCall).toHaveBeenCalledWith(1);
            expect(Loading.show).toHaveBeenCalledWith("Deleting...");
            expect(Loading.close).toHaveBeenCalled();
            expect(Notify.success).toHaveBeenCalledWith(model.resources.deleteSuccess);
            expect(model.items.length).toEqual(1);
            expect(model.items[0].id).toEqual(2);
            expect(model.toolbarCfg.total).toEqual(1);
        }));

        it("notifies the user of an error", inject(function($q, $rootScope, Notify, Loading, $log) {
            spyOn(responder, "deleteCall").and.returnValue($q.reject({ data: "error" }));
            spyOn(Notify, "error");
            spyOn($log, "error");
            spyOn(Loading, "show");
            spyOn(Loading, "close");

            var model = createTestClass();
            model.items = [{ id: 1, name: "item 1" }, { id: 2, name: "item 2" }];
            model.deleteCall = responder.deleteCall;
            model.onConfirmDeleteItem(model.items[0], 0);

            $rootScope.$apply();
            expect(Notify.error).toHaveBeenCalledWith("error");
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(Loading.show).toHaveBeenCalledWith("Deleting...");
            expect(Loading.close).toHaveBeenCalled();
        }));
    });

    describe("onAdded", function() {
        it("adds to items", function() {
            var model = createTestClass();
            model.onAdded({ name: "new item" });

            expect(model.items.length).toEqual(1);
            expect(model.toolbarCfg.total).toEqual(1);
        });
    });

    describe("onUpdated", function() {
        it("updates the item in the list", function() {
            var model = createTestClass();
            model.items = [{ id: 1, name: "item 1" }];
            model.onUpdated({ id: 1, name: "my item" });

            expect(model.items.length).toEqual(1);
            expect(model.items[0].name).toEqual("my item");
        });
    });

    describe("editItem", function() {
        it("calls $state go", inject(function($q, $rootScope) {
            var model = createTestClass();
            var item = { id: 1 };
            spyOn($state, "go");

            model.editItem(item);

            expect($state.go).toHaveBeenCalled();
        }));
    });

    describe("newItem", function() {
        it("calls $state go", inject(function($q, $rootScope) {
            var model = createTestClass();
            spyOn($state, "go");

            model.newItem();

            expect($state.go).toHaveBeenCalled();
        }));
    });

    describe("sortTable", function() {
        it("sorts the table using the given attribute", inject(function($q, $rootScope) {
            var model = createTestClass();
            spyOn(model, "sort");

            var attribute = { name: "name", sortable: true, order: "desc" };
            model.sortOption = { name: "name" };
            model.sortTable(attribute);

            expect(attribute.order).toEqual("asc");
            expect(model.sort).toHaveBeenCalled();
        }));

        it("sorts the table using the given attribute and sets the attribute as the sort option", inject(function($q, $rootScope) {
            var model = createTestClass();
            spyOn(model, "sort");

            var attribute = { name: "id", sortable: true, order: "desc" };
            model.sortOption = { name: "name" };
            model.sortTable(attribute);

            expect(attribute.order).toEqual("desc");
            expect(model.sortOption).toEqual(attribute);
            expect(model.sort).toHaveBeenCalled();
        }));
    });

});