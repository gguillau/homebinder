angular
    .module("hb.framework.indexBase", [])
    .factory("hb.framework.indexBase", [
        'ModalService',
        'Notify',
        '$log',
        'Loading',
        'hb.resources',
        '$state',
        function(modals, notify, $log, loading, resources, $state) {
            var IndexBase = function() {
                // resources used by the class. Resources here are
                // only used in code not the view. Override in subclass
                this.resources = resources.indexBase;
                this.loading = loading;
                this.$log = $log;
                this.notify = notify;
                this.$state = $state;
                // the base toolbar configuration. Extend in subclasses
                this.toolbarCfg = {
                    searchBox: true,
                    search: angular.bind(this, this.search),
                    title: this.resources.title,
                    total: 0
                };
                // items in the view
                this.items = [];
                // the total number of items available on the server
                this.totalItems = 0;
                // the number of items to display per page
                this.itemsOptions = [10, 20, 30, 40, 50];
                this.itemsPerPage = this.itemsOptions[0];
                // the current page in view
                this.currentPage = 1;
                // the current value being searched
                this.searchValue = null;
                // selected sort option
                this.sortOption = null;
                // oder items by
                this.orderBy = null;
                // reverse the sort order
                this.reverseSort = false;
                // the api call to execute on a refresh
                this.refreshCall = null;
                // the api call to execute on a delete
                this.deleteCall = null;
                // the property on an item used in the confirm delete message
                this.deleteInfoProperty = null;
                // the max amount of pagination boxes
                this.maxSize = 10;
                // the edit state to send the user to
                this.editState = null;
                // the params needed in the edit state
                this.editParams = {};
                // the params property to add to the editParams hash
                this.editParamsProperty = null;
                // the new state to send the user to
                this.newState = null;
                // the params needed in the new state
                this.newParams = {};
                // query args
                this.queryArgs = {};
                // show loading
                this.showLoading = true;
            };

            angular.extend(IndexBase.prototype, {
                // sets the page to user wishes to view and refreshes the view
                pageChange: function() {
                    this.refresh();
                },

                // initiates a server side search
                search: function() {
                    this.currentPage = 1;
                    this.refresh();
                },

                // set searchValue to undefined
                clearSearch: function() {
                    this.searchValue = undefined;
                },

                // initiates a server side sort
                sort: function(sortOption) {
                    if (sortOption) {
                        this.orderBy = sortOption.orderBy;
                        this.order = sortOption.order;
                    }
                    else {
                        this.orderBy = null;
                        this.order = null;
                    }
                    this.refresh();
                },

                // refreshes the current view
                refresh: function() {
                    // make sure the refresh API call is set
                    if (!this.refreshCall) {
                        throw "refreshCall not set";
                    }

                    // create the base query arguments
                    var opts = {
                        page: this.currentPage,
                        count: this.itemsPerPage
                    };
                    // if a search value is set add it to the query string. NOTE: server side services must be
                    // looking for :search.
                    if (this.searchValue) {
                        opts.search = this.searchValue;
                    }
                    // if there is a sort options set it
                    if (this.orderBy) {
                        opts.orderBy = this.orderBy;
                        opts.order = this.order;
                    }

                    this.addQueryArgs();

                    // if there are other query args provided by the sub class add them
                    if (!angular.equals({}, this.queryArgs)) {
                        angular.extend(opts, this.queryArgs);
                    }

                    if (this.showLoading) {
                        // display the loading message
                        loading.show(this.resources.loading);
                    }

                    this.refreshCall(opts).then(
                        angular.bind(this, this.onRefreshSuccess),
                        angular.bind(this, this.onRefreshError)
                    );
                },

                // Refresh callback. Updates the item list and totals
                onRefreshSuccess: function(response) {
                    this.items = response.data.items;
                    this.totalItems = this.toolbarCfg.total = response.data.total;
                    loading.close();
                },

                // Refresh error callback. Logs an error and notifies the user of an error
                onRefreshError: function(response) {
                    $log.error(response);
                    notify.error(response.data);
                    loading.close();
                },

                // initiates a delete sequence
                deleteItem: function(item, index) {
                    // make sure a call is set
                    if (!this.deleteCall) {
                        throw "deleteCall not set";
                    }

                    // confirm the delete
                    modals.confirm({
                        title: this.resources.confirmDelete,
                        glyphicon: "glyphicon glyphicon-trash",
                        message: this.resources.deleteMsg1 + item[this.nameProperty] + this.resources.deleteMsg2,
                        confirm: angular.bind(this, this.onConfirmDeleteItem, item, index)
                    });
                },

                // performs a delete on the server
                onConfirmDeleteItem: function(item, index) {
                    loading.show(this.resources.deletingMsg);
                    this.deleteCall(item.id).then(
                        angular.bind(this, this.onDeleteItemSuccess, item),
                        angular.bind(this, this.onDeleteItemError)
                    );
                },

                // updates the view after a delete
                onDeleteItemSuccess: function(item, response) {
                    var index = this.items.indexOf(item);
                    this.items.splice(index, 1);
                    this.totalItems = this.toolbarCfg.total -= 1;
                    notify.success(this.resources.deleteSuccess);
                    loading.close();
                },

                // notifies the user of a delete error. logs a message
                onDeleteItemError: function(response) {
                    $log.error(response);
                    notify.error(response.data);
                    loading.close();
                },

                // item was added to the collection
                onAdded: function(item) {
                    this.items.push(item);
                    this.totalItems = this.toolbarCfg.total += 1;
                },

                // an item was updated
                onUpdated: function(item) {
                    // remove the area
                    this.removeItem(item);
                    // add it again
                    this.onAdded(item);
                },

                // an item was deleted
                removeItem: function(item) {
                    var k,
                        len;

                    for (k = 0, len = this.items.length; k < len; k++) {
                        if (this.items[k].id == item.id) {
                            this.items.splice(k, 1);
                            break;
                        }
                    }
                },

                // send user to edit item view
                editItem: function(item) {
                    this.editParams[this.editParamsProperty] = item.id;
                    $state.go(this.editState, this.editParams);
                },

                // send user to new item view
                newItem: function() {
                    $state.go(this.newState, this.newStateParams);
                },

                // sorts table by header attribute that was clicked in the DOM
                sortTable: function(attribute) {
                    if (attribute.sortable) {
                        if (this.sortOption.name === attribute.name) {
                            attribute.order = attribute.order === "asc" ? "desc" : "asc";
                            this.sortOption = attribute;
                        }
                        else {
                            this.sortOption.sorted = false;
                            attribute.sorted = true;
                            this.sortOption = attribute;
                        }
                        this.sort(this.sortOption);
                    }
                },

                // add extra args to the queryArgs object
                addQueryArgs: function() {

                }
            });

            return IndexBase;
        }
    ]);