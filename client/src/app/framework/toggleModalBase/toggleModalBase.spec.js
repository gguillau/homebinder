describe("ToggleModalBase", function() {
    var ModalBase,
        ModalControllerBase,
        TestClass,
        responder,
        $q;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
        // mock the notify service
        $provide.factory("Notify", function() {
            return {
                success: function() {},
                error: function() {}
            };
        });
        // mock the $log service
        
        $provide.factory("$log", function() {
            return {
                error: function() {},
                warn: function() {}
            };
        });
        
        // mock the modals service
        $provide.factory("$modal", function() {
            return {
                open: function() {}
            };
        });
    }));
    // get the base class under test
    beforeEach(inject(function($injector) {
        $q = $injector.get("$q");
        ModalBase = $injector.get("hb.framework.toggleModalBase");
        ModalControllerBase = $injector.get("hb.framework.toggleModalControllerBase");
    }));
    
    responder = {
        refreshCall: function() {},
        deleteCall: function() {}
    };
    
    
    /*
     * Test ToggleModalBase class
     */
    function createServiceTestClass() {
        var Service = function() {
            ModalBase.call(this);
            
            this.templateUrl = "mytemplate";
            this.controller = "mycontroller";
            this.windowClass = "myclass";
            this.objectProperty = "object";
            this.objectIdProperty = "objectId";
            this.nameProperty = "name";
        };
        
        Service.prototype = Object.create(ModalBase.prototype);
        
        return new Service();
    }
    
    /*
     * ToggleModalBase tests
     */
    describe("ToggleModalBase", function() {
        describe("show", function() {
            it ("opens a new modal instance", inject(function($q, $modal, $rootScope){
                var args = { onClosed: function() {} },
                    service = createServiceTestClass(),
                    deferred = $q.defer(),
                    modalInstance = {
                        result: deferred.promise
                    };
                    
                spyOn($modal, "open").and.returnValue(modalInstance);
                spyOn(args, "onClosed");
                service.show(args);
                
                deferred.resolve({});
                $rootScope.$apply();
                expect($modal.open).toHaveBeenCalledWith(jasmine.objectContaining({
                    templateUrl: "mytemplate",
                    controller: "mycontroller",
                    windowClass: "myclass"
                }));
                expect(args.onClosed).toHaveBeenCalled();
            }));
        });
        
        describe("showDetails", function() {
            it ("calls show with view set to details", function() {
                var service = createServiceTestClass(),
                    args = { object: {} };
                
                spyOn(service, "show").and.callFake(function() {});
                    
                service.showDetails(args);
                
                expect(service.show).toHaveBeenCalledWith(jasmine.objectContaining({ view: "details" }));
            });
        });
        
        describe("showForm", function() {
            it ("calls show with view set to edit when object is set", function() {
                var service = createServiceTestClass(),
                    args = { object: { id: 1 } };
                    
                spyOn(service, "show").and.callFake(function() {});
                
                service.showForm(args);
                
                expect(service.show).toHaveBeenCalledWith(jasmine.objectContaining({ view: "edit" }));
            });
            
            it ("calls show with view set to edit when object id is set", function() {
                var service = createServiceTestClass(),
                    args = { objectId: 1 };
                    
                spyOn(service, "show").and.callFake(function() {});
                
                service.showForm(args);
                
                expect(service.show).toHaveBeenCalledWith(jasmine.objectContaining({ view: "edit" }));
            });
            
            it ("calls show with view set to new when object and object id not set", function() {
                var service = createServiceTestClass(),
                    args = { };
                    
                spyOn(service, "show").and.callFake(function() {});
                
                service.showForm(args);
                
                expect(service.show).toHaveBeenCalledWith(jasmine.objectContaining({ view: "new" }));
            });
        });
        
        describe("close", function() {
            it ("closes a modal instance", function() {
                var service = createServiceTestClass(),
                    deferred = $q.defer(),
                    modalInstance = {
                        result: deferred.promise,
                        dismiss: function() {}
                    };
                    
                spyOn(modalInstance, "dismiss");
                
                service.$modalInstance = modalInstance;
                service.close();
                
                expect(modalInstance.dismiss).toHaveBeenCalled();
            });
        });
    });
    
    /*
     * Test ToggleModalController class
     */
    function createControllerTestClass() {
        var Controller = function() {
            this.$modalInstance = { dismiss: function() {} };

            this.objectProperty = "object";
            this.objectIdProperty = "objectId";
            this.nameProperty = "name";
            this.populateCall = function() {};
            
            this.resources = angular.extend({}, this.resources, {
                newTitle: "New test",
                loadError: "load error"
            });
            
            ModalControllerBase.call(this);
        };
        
        Controller.prototype = Object.create(ModalControllerBase.prototype);
            
        return new Controller();
    }
    
    /*
     * ToggleModalController tests
     */
    describe("ToggleModalControllerBase", function() {
        describe("init", function() {
            it ("shows the details view and populates", function() {
                var controller = createControllerTestClass();
                controller.args = {
                    view: "details",
                    object: {}
                };
                
                spyOn(controller, "populate").and.callFake(function() {});
                
                controller.init();
                
                expect(controller.initialView).toEqual("details");
                expect(controller.currentView).toEqual("details");
                expect(controller.showDetails).toEqual(true);
                expect(controller.showForm).toEqual(false);
                expect(controller.populate).toHaveBeenCalled();
            });
            
            it("sets the new form view and populates", inject(function() {
                var controller = createControllerTestClass();
                controller.args = {
                    view: "new"
                };
                
                spyOn(controller, "populate").and.callFake(function() {});
                
                controller.init();
                
                expect(controller.initialView).toEqual("new");
                expect(controller.currentView).toEqual("form");
                expect(controller.showDetails).toEqual(false);
                expect(controller.showForm).toEqual(true);
                expect(controller.populate).toHaveBeenCalled();
            }));
            
            it("sets the edit form view and populates", function() {
                var controller = createControllerTestClass();
                controller.args = {
                    view: "edit",
                    object: {}
                };
                
                spyOn(controller, "populate").and.callFake(function() {});
                
                controller.init();
                
                expect(controller.initialView).toEqual("edit");
                expect(controller.currentView).toEqual("form");
                expect(controller.showDetails).toEqual(false);
                expect(controller.showForm).toEqual(true);
                expect(controller.populate).toHaveBeenCalled();
            });
        });
        
        describe("populate", function() {
            it("populates on new", function() {
                var controller = createControllerTestClass();
                controller.args = {
                    view: "new"  
                };
                
                spyOn(controller, "setVisibleButtons");
                
                controller.populate();
                
                expect(controller.title).toEqual("New test");
                expect(controller.isLoading).toEqual(false);
                expect(controller.setVisibleButtons).toHaveBeenCalled();
            });
            
            it("populates from a passed in object", function() {
                var controller = createControllerTestClass();
                controller.args = {
                    view: "edit",
                    object: {
                        id: 1,
                        name: "item name"
                    }
                };
                
                spyOn(controller, "setVisibleButtons");
                
                controller.populate();
                
                expect(controller.title).toEqual("item name");
                expect(controller.isLoading).toEqual(false);
                expect(controller.item).toEqual(jasmine.objectContaining({ id: 1, name: "item name" }));
                expect(controller.detailsCfg.item).toEqual(jasmine.objectContaining({ id: 1, name: "item name" }));
                expect(controller.formCfg.item).toEqual(jasmine.objectContaining({ id: 1, name: "item name" }));
                expect(controller.setVisibleButtons).toHaveBeenCalled();
            });
            
            it("populates from an objectId", inject(function($q, $rootScope) {
                var controller = createControllerTestClass();
                controller.args = {
                    view: "edit",
                    objectId: 1
                };
                
                spyOn(controller, "populateCall").and.returnValue($q.when({ data: { id: 1, name: "item name" }}));
                spyOn(controller, "setVisibleButtons");
                
                controller.populate();
                
                $rootScope.$apply();
                expect(controller.title).toEqual("item name");
                expect(controller.isLoading).toEqual(false);
                expect(controller.item).toEqual(jasmine.objectContaining({ id: 1, name: "item name" }));
                expect(controller.detailsCfg.item).toEqual(jasmine.objectContaining({ id: 1, name: "item name" }));
                expect(controller.formCfg.item).toEqual(jasmine.objectContaining({ id: 1, name: "item name" }));
                expect(controller.setVisibleButtons).toHaveBeenCalled();
            }));
            
            it("notifies the user of a populate error from an id", inject(function($q, $log, $rootScope, Notify) {
                var controller = createControllerTestClass();
                controller.args = {
                    view: "edit",
                    objectId: 1
                };
                
                spyOn(controller, "populateCall").and.returnValue($q.reject({ data: "error" }));
                spyOn(controller, "close");
                spyOn($log, "error");
                spyOn(Notify, "error");
                
                controller.populate();
                
                $rootScope.$apply();
                expect($log.error).toHaveBeenCalledWith("error");
                expect(Notify.error).toHaveBeenCalledWith("load error");
                expect(controller.close).toHaveBeenCalled();
            }));
        });
        
        describe("setVisibleButtons", function() {
            it("sets form buttons", function() {
                var controller = createControllerTestClass();
                controller.currentView = "form";
                controller.setVisibleButtons();
                expect(controller.showDelete).toEqual(false);
                expect(controller.showEdit).toEqual(false);
                expect(controller.showClose).toEqual(false);
                expect(controller.showSave).toEqual(true);
                expect(controller.showCancel).toEqual(true);
            });
            
            it("sets details buttons", function() {
                var controller = createControllerTestClass();
                controller.currentView = "details";
                controller.setVisibleButtons();
                expect(controller.showDelete).toEqual(true);
                expect(controller.showEdit).toEqual(true);
                expect(controller.showClose).toEqual(true);
                expect(controller.showSave).toEqual(false);
                expect(controller.showCancel).toEqual(false);
            });
        });
        
        describe("toggleView", function() {
            it("toggles from details to form", function() {
                var controller = createControllerTestClass();
                controller.item = { id: 1, name: "item name" };
                controller.showDetails = true;
                controller.showForm = false;
                
                spyOn(controller, "setVisibleButtons");
                
                controller.toggleView();
                
                expect(controller.showDetails).toEqual(false);
                expect(controller.showForm).toEqual(true);
                expect(controller.currentView).toEqual("form");
                expect(controller.title).toEqual("item name");
                expect(controller.setVisibleButtons).toHaveBeenCalled();
            });
            
            it("toggles from form to details", function() {
                var controller = createControllerTestClass();
                controller.item = { id: 1, name: "item name" };
                controller.showDetails = false;
                controller.showForm = true;
                
                spyOn(controller, "setVisibleButtons");
                
                controller.toggleView();
                
                expect(controller.showDetails).toEqual(true);
                expect(controller.showForm).toEqual(false);
                expect(controller.currentView).toEqual("details");
                expect(controller.title).toEqual("item name");
                expect(controller.setVisibleButtons).toHaveBeenCalled();
            });
        });
        
        describe("destroy", function() {
            it("calls destroy on the detailsCfg.api", function() {
                var controller = createControllerTestClass();
                controller.detailsCfg.api = {
                    destroy: function() {}  
                };
                
                spyOn(controller.detailsCfg.api, "destroy");
                
                controller.destroy();
                
                expect(controller.detailsCfg.api.destroy).toHaveBeenCalled();
            });
        });
        
        describe("onDestroyed", function() {
            it("closes the modal", function() {
                var controller = createControllerTestClass();
                controller.$modalInstance = {
                    close: function() {}  
                };
                controller.args = {};
                
                spyOn(controller.$modalInstance, "close");
                
                controller.onDestroyed({ id: 1, name: "item name" });
                
                expect(controller.$modalInstance.close).toHaveBeenCalledWith(jasmine.objectContaining({ refresh: true }));
            });
            
            it("calls the provided callback", function() {
                var controller = createControllerTestClass();
                controller.$modalInstance = {
                    close: function() {}  
                };
                controller.args = {
                    onDeleted: function() {}  
                };
                
                spyOn(controller.args, "onDeleted");
                
                controller.onDestroyed({ id: 1, name: "item name" });
                
                expect(controller.args.onDeleted).toHaveBeenCalledWith(jasmine.objectContaining({ id: 1, name: "item name" }));
            });
        });
        
        describe("edit", function() {
            it("toggles the view", function() {
                var controller = createControllerTestClass();
                spyOn(controller, "toggleView");
                
                controller.edit();
                
                expect(controller.toggleView).toHaveBeenCalled();
            });
        });
        
        describe("save", function() {
            it("calls formCfg.api.save", function(){
                var controller = createControllerTestClass();
                controller.formCfg.api = {
                    save: function() {}  
                };
                
                spyOn(controller.formCfg.api, "save");
                
                controller.save();
                
                expect(controller.formCfg.api.save).toHaveBeenCalled();
            });
        });
        
        describe("onSaved", function() {
            it("closes the modal after initial edit", function() {
                var controller = createControllerTestClass(),
                    item = { id: 1, name: "item" };
                controller.initialView = "edit";
                controller.args = {
                    onSaved: function() {}  
                };
                controller.$modalInstance = {
                    close: function() {}  
                };
                
                spyOn(controller.args, "onSaved");
                spyOn(controller.$modalInstance, "close");
                
                controller.onSaved(item);
                
                expect(controller.args.onSaved).toHaveBeenCalledWith(jasmine.objectContaining(item));
                expect(controller.$modalInstance.close).toHaveBeenCalledWith(jasmine.objectContaining({ refresh: true }));
            });
            
            it("closes the modal after initial new", function() {
                var controller = createControllerTestClass(),
                    item = { id: 1, name: "item" };
                controller.initialView = "new";
                controller.args = {
                    onSaved: function() {}  
                };
                controller.$modalInstance = {
                    close: function() {}  
                };
                
                spyOn(controller.args, "onSaved");
                spyOn(controller.$modalInstance, "close");
                
                controller.onSaved(item);
                
                expect(controller.args.onSaved).toHaveBeenCalledWith(jasmine.objectContaining(item));
                expect(controller.$modalInstance.close).toHaveBeenCalledWith(jasmine.objectContaining({ refresh: true }));
            });
        });
        
        describe("close", function() {
            it("closes the modal from the details view", function() {
                var controller = createControllerTestClass();
                controller.currentView = "details";
                controller.$modalInstance = {
                    dismiss: function() {}  
                };
                
                spyOn(controller.$modalInstance, "dismiss");
                
                controller.close();
                
                expect(controller.$modalInstance.dismiss).toHaveBeenCalled();
            });
            
            it("toggles view when initial view is details and current is form", function() {
                var controller = createControllerTestClass();
                controller.initialView = "details";
                controller.currentView = "form";
                controller.$modalInstance = {
                    dismiss: function() {}  
                };
                
                spyOn(controller.$modalInstance, "dismiss");
                spyOn(controller, "toggleView");
                
                controller.close();
                
                expect(controller.$modalInstance.dismiss).not.toHaveBeenCalled();
                expect(controller.toggleView).toHaveBeenCalled();
            });
            
            it("closes the modal when initial view is form", function() {
                var controller = createControllerTestClass();
                controller.initialView = "new";
                controller.currentView = "form";
                controller.$modalInstance = {
                    dismiss: function() {}  
                };
                
                spyOn(controller.$modalInstance, "dismiss");
                
                controller.close();
                
                expect(controller.$modalInstance.dismiss).toHaveBeenCalled();
            });
        });
    });

});