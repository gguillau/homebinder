angular
    .module("hb.framework.toggleModalBase", [])
    .factory("hb.framework.toggleModalBase", [
        '$modal',
        function($modal) {
            var ModalBase = function() {
                // modal instance returned when the modal is opened
                this.$modalInstance = null;
                // the template url to display in the modal
                this.templateUrl = null;
                // the controller to be applied to the modal
                this.controller = null;
                // class to be applied to the modal
                this.windowClass = null;
                // the name of the object property on the args object. This is used to determine if an object was passed in
                this.objectProperty = null;
                // the name of the object id property on the args object. This is used to determine if an object id was passed in
                this.objectIdProperty = null;
            };

            angular.extend(ModalBase.prototype, {
                show: function(args) {
                    // open the modal
                    this.$modalInstance = $modal.open({
                        templateUrl: this.templateUrl,
                        controller: this.controller,
                        resolve: {
                            args: function() {
                                return args;
                            }
                        },
                        backdrop: "static",
                        windowClass: this.windowClass
                    });

                    /*jshint -W024*/
                    this.$modalInstance
                        .result
                        .finally(angular.bind(this, function() {
                            this.$modalInstance = null;
                            if (args.onClosed) {
                                args.onClosed();
                            }
                        }));
                },
                showDetails: function(args) {
                    // set the view type on the args before showing
                    args.view = "details";
                    // show the modal
                    this.show(args);
                },
                showForm: function(args) {
                    // set the view type on the args before showing
                    args.view = args.hasOwnProperty(this.objectProperty) || args.hasOwnProperty(this.objectIdProperty) ? "edit" : "new";
                    // show the modal
                    this.show(args);
                },
                close: function() {
                    if (this.$modalInstance) {
                        this.$modalInstance.dismiss();
                        this.$modalInstance = null;
                    }
                }
            });

            return ModalBase;
        }
    ])
    .factory("hb.framework.toggleModalControllerBase", [
        "$log",
        "Notify",
        "Loading",
        "hb.resources",
        function($log, notify, loading, resources) {
            var ControllerBase = function() {
                this.resources = angular.extend(resources.toggleModalBase, this.resources);
                this.initialView = null;
                this.currentView = null;
                this.isLoading = true;
                this.formCfg = angular.extend({}, this.args);
                this.detailsCfg = {};
            };

            angular.extend(ControllerBase.prototype, {
                init: function() {
                    // save the initial view presented from the caller. This can be
                    // details, new or edit
                    this.initialView = this.args.view;
                    // show the details directive if the view is details
                    this.showDetails = this.args.view == "details";
                    // show the form directive if the view is new or edit
                    this.showForm = this.args.view == "new" || this.args.view == "edit";
                    // save the current vliew
                    this.currentView = this.showDetails ? "details" : "form";
                    // populate loads the area for the modal if it has not been provided. Doing this here instead
                    // of in the directives to reduce code since both may need to load the area
                    this.populate();
                },

                populate: function() {
                    // if an item was provided load it. Otherwise if an ID is provided load from that.
                    // if neither is provided assume new
                    if (this.args.hasOwnProperty(this.objectProperty) &&
                        angular.isObject(this.args[this.objectProperty])) {
                        // an item was passed in. use it as the working item
                        this.item = this.args[this.objectProperty];
                        this.finishPopulate();
                    }
                    else if (this.args.hasOwnProperty(this.objectIdProperty)) {
                        // an item id was passed in. get the item
                        this.populateCall(this.args[this.objectIdProperty]).then(
                            angular.bind(this, this.loadedItem),
                            angular.bind(this, this.loadItemError)
                        );
                    }
                    else if (this.args.view == "new") {
                        // the view is new. no item to set or get
                        this.title = this.resources.newTitle;
                        this.isLoading = false;
                    }
                    else {
                        // the view is new. nothing to load
                        this.finishPopulate();
                    }

                    this.setVisibleButtons();
                },

                finishPopulate: function() {
                    // set the item onto the config objects of each directive
                    if (this.item) {
                        this.detailsCfg.item = this.item;
                        this.formCfg.item = this.item;
                        this.title = this.item[this.nameProperty];
                    }
                    else {
                        this.title = this.resources.newTitle;
                    }

                    // done loading
                    this.isLoading = false;
                },

                loadedItem: function(response) {
                    // save the area and finish
                    this.item = response.data;
                    this.finishPopulate();
                },

                loadItemError: function(error) {
                    // if we fail to load the area tell the user and close the
                    // modal since we can't do anthing
                    $log.error(error.data);
                    notify.error(this.resources.loadError);
                    this.close();
                },

                setVisibleButtons: function() {
                    this.showDelete = this.currentView == "details";
                    this.showEdit = this.currentView == "details";
                    this.showClose = this.currentView == "details";
                    this.showCancel = this.currentView == "form";
                    this.showSave = this.currentView == "form";
                },

                toggleView: function() {
                    // toggle the currently active directive
                    this.showDetails = !this.showDetails;
                    this.showForm = !this.showForm;
                    // store the current view
                    this.currentView = this.showDetails ? "details" : "form";
                    // set the title of the modal
                    this.title = this.item ? this.item[this.nameProperty] : this.resources.newTitle;
                    // set the visible butons
                    this.setVisibleButtons();
                },

                destroy: function() {
                    // tell the details directive to delete
                    this.detailsCfg.api.destroy();
                },

                onDestroyed: function(item) {
                    // call the deleted callback if provided
                    if (this.args.onDeleted) {
                        this.args.onDeleted(item);
                    }

                    // close the modal and pass the area that was removed
                    this.$modalInstance.close({
                        refresh: true
                    });
                },

                edit: function() {
                    // edit comes from the details directive. All we need to
                    // do it toggle the view
                    this.toggleView();
                },

                save: function() {
                    // tell the form to save
                    this.formCfg.api.save();
                },

                onSaved: function(item) {
                    // call the saved callback if provided
                    if (this.args.onSaved) {
                        this.args.onSaved(item);
                    }
                    this.$modalInstance.close({ refresh: true });
                },

                close: function() {
                    // if the current view is details and the close event occurred
                    // close the modal
                    if (this.currentView == "details") {
                        this.showDetails = false;
                        this.detailsCfg.hide = true;
                        this.$modalInstance.dismiss();
                    }
                    else {
                        // the current view is the form. if the initial was details
                        // toggle back otherwise close the dialog
                        if (this.initialView == "details") {
                            this.formCfg.hide = true;
                            this.toggleView();
                        }
                        else {
                            this.showForm = false;
                            this.detailsCfg.hide = true;
                            this.$modalInstance.dismiss();
                        }
                    }
                }
            });

            return ControllerBase;
        }
    ]);