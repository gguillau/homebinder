describe("NewBase", function() {
    var NewBase,
        TestClass;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
        // mock the notify service
        $provide.factory("Notify", function() {
            return {
                success: function() {},
                error: function() {}
            };
        });
        // mock the $log service

        $provide.factory("$log", function() {
            return {
                error: function() {},
                warn: function() {}
            };
        });

        // mock the modals service
        $provide.factory("ModalService", function() {
            return {
                confirm: function() {}
            };
        });
    }));
    // get the base class under test
    beforeEach(inject(function($injector) {
        NewBase = $injector.get("hb.framework.newBase");
    }));

    function createTestClass() {
        TestClass = function() {
            NewBase.call(this);

            this.resources = angular.extend({}, this.resources, {
                loading: "loading",
                loadError: "load error",
                confirmDelete: "confirm",
                deleteMsg1: "a ",
                deleteMsg2: " b",
                deleteSuccess: "deleted",
                deleteError: "delete error"
            });

            this.nameProperty = "name";
        };

        TestClass.prototype = Object.create(NewBase.prototype);

        return new TestClass();
    }

    describe("onSave", function() {
        it("calls createCall", inject(function($q, $rootScope) {
            var model = createTestClass();

            model.createCall = function() {};
            spyOn(model, "createCall").and.returnValue($q.when({ data: { items: [{ name: "item 1" }, { name: "item 2" }], total: 2 } }));
            model.form = { $invalid: false };
            model.onSave();

            expect(model.createCall).toHaveBeenCalled();
        }));

        it("does not call createCall", inject(function($q, $rootScope) {
            var model = createTestClass();

            model.createCall = function() {};
            spyOn(model, "createCall").and.returnValue($q.when({ data: { items: [{ name: "item 1" }, { name: "item 2" }], total: 2 } }));
            model.form = { $invalid: true };
            model.onSave();

            expect(model.createCall).not.toHaveBeenCalled();
        }));
    });

    describe("onCreateSuccess", function() {
        it("sets the item", inject(function($q, $rootScope) {
            var model = createTestClass();
            model.onCreateSuccess({ data: { id: 1 } });

            expect(model.item).toEqual({ id: 1 });
        }));
    });

    describe("onCreateError", function() {
        it("calls notify", inject(function($q, $rootScope, Notify) {
            spyOn(Notify, "error");

            var model = createTestClass();
            model.onCreateError({ data: "error" });

            expect(Notify.error).toHaveBeenCalled();
        }));
    });

});