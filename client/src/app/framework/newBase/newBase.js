angular
    .module("hb.framework.newBase", [])
    .factory("hb.framework.newBase", [
        'ModalService',
        'Notify',
        '$log',
        'Loading',
        'hb.resources',
        '$state',
        function(modals, notify, $log, loading, resources, $state) {
            var NewBase = function() {
                // resources used by the class. Resources here are
                // only used in code not the view. Override in subclass
                this.resources = resources.newBase;
                this.loading = loading;
                this.$state = $state;
                this.notify = notify;
                this.$log = $log;
                // them item we are going to create
                this.item = {};
                // the api call to execute on a create
                this.createCall = null;
            };

            angular.extend(NewBase.prototype, {

                // creates the item
                onSave: function() {
                    // make sure the refresh API call is set
                    if (!this.createCall) {
                        throw "createCall not set";
                    }
                    this.form.$submitted = true;
                    if (this.form.$invalid) {
                        return;
                    }
                    // display the loading message
                    loading.show(this.resources.savingMsg);
                    // get the item
                    this.createCall(this.item).then(
                        angular.bind(this, this.onCreateSuccess),
                        angular.bind(this, this.onCreateError)
                    );
                },

                // Create callback. Get the new item
                onCreateSuccess: function(response) {
                    this.item = response.data;
                    loading.close();
                },

                // Create error callback. Logs an error and notifies the user of an error
                onCreateError: function(response) {
                    $log.error(response);
                    notify.error(response.data);
                    loading.close();
                }
            });

            return NewBase;
        }
    ]);