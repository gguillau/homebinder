angular
	.module("homebinder", [
		"templates-app",
		"templates-common",
		"ngMessages",
		"ngAnimate",
		"ui.router",
		"ui.bootstrap",
		"selectize",
		"ui.select",
		"angulartics",
		"angulartics.google.analytics",
		"angulartics.facebook.pixel",
		"ngIntercom",
		"textAngular",
		"ui.mask",
		"papa-promise",
		"hb.accounts",
		"hb.admin",
		"hb.agents",
		"hb.api",
		'hb.annualPropertyReviews',
		"hb.appDelegate",
		"hb.components",
		"hb.dashboards",
		"hb.errors",
		"hb.framework",
		"hb.homebinders",
		"hb.homeowners",
		"hb.landing",
		"hb.legal",
		"hb.partner",
		"hb.resources",
		"hb.session",
		"hb.utils",
		"hb.widgets"
	])
	.config([
		"$urlMatcherFactoryProvider",
		"$stateProvider",
		"$urlRouterProvider",
		"$httpProvider",
		"uiSelectConfig",
		"$provide",
		"$locationProvider",
		"$intercomProvider",
		"$compileProvider",
		function($urlMatcherFactoryProvider, $stateProvider, $urlRouterProvider, $httpProvider, uiSelectConfig, $provide, $locationProvider, $intercomProvider, $compileProvider) {
			$intercomProvider.asyncLoading(true);

			$urlMatcherFactoryProvider.caseInsensitive(true);

			// Using Safari, in Private Browsing Mode, all calls to local storage setItem
			// throws a QuotaExceededError. We're going to detect this and alert the user to switch
			// to a normal browser
			if (typeof localStorage === 'object') {
				try {
					localStorage.setItem('localStorage', 1);
					localStorage.removeItem('localStorage');
				}
				catch (e) {
					Storage.prototype._setItem = Storage.prototype.setItem;
					Storage.prototype.setItem = function() {};
					alert('It seems like you are using your browser in Private mode. Please switch to a regular browser.');
				}
			}

			// unknown states go to the root
			$urlRouterProvider.otherwise("/");
			// use the HTML5 History API
			$locationProvider.html5Mode({
				enabled: true,
				requireBase: true,
				rewriteLinks: true
			});

			//allow case insensitive urls - need to fix this rule for sellerreport
			$urlRouterProvider.rule(function($injector, $location) {
				var path = $location.path(),
					normalized = path.toLowerCase();
				if (path !== normalized) {
					return normalized;
				}
				return null;
			});

			$httpProvider.interceptors.push('hbInterceptor');

			// set the ui-select theme
			uiSelectConfig.theme = 'bootstrap';
			uiSelectConfig.resetSearchInput = true;
			uiSelectConfig.appendToBody = true;

			$provide.decorator('taOptions', ['$delegate', function(taOptions) {
				// here we override the default toolbars and classes specified in taOptions.
				taOptions.forceTextAngularSanitize = true; // set false to allow the textAngular-sanitize provider to be replaced
				taOptions.keyMappings = []; // allow customizable keyMappings for specialized key boards or languages
				taOptions.toolbar = [
					['bold', 'italics', 'underline', 'strikeThrough', 'ol'],
					['insertLink']
				];
				taOptions.classes = {
					focussed: 'focussed',
					toolbar: 'btn-toolbar',
					toolbarGroup: 'btn-group',
					toolbarButton: 'btn btn-default',
					toolbarButtonActive: 'active',
					disabled: 'disabled',
					textEditor: 'form-control',
					htmlEditor: 'form-control'
				};
				taOptions.defaultTagAttributes = {
					a: {
						target: "_blank"
					}
				};
				return taOptions;
			}]);

			switch (window.location.hostname) {
				case "www.homebinder.com":
					$compileProvider.debugInfoEnabled(false);
					break;
			}
		}
	])

	// startup operations
	.run(['$intercom', '$injector', function($intercom, $injector) {
		var $rootScope = $injector.get('$rootScope');
		var $anchorScroll = $injector.get('$anchorScroll');
		var $http = $injector.get('$http');
		var api = $injector.get("hb.api");
		var User = $injector.get('User');
		var constants = $injector.get('constants');
		var serviceKeyList = $injector.get('ServiceKeyList');
		var AfterLogin = $injector.get('AfterLogin');
		var AppDelegate = $injector.get("AppDelegate");
		var $state = $injector.get("$state");
		var $location = $injector.get("$location");
		var eventCreate = $injector.get("Event");

		$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
			if (toState && toState.name === "binder.contractors" && $location.search().proClicked && $location.search().maintenanceEventId) {
				var eventId = $location.search().maintenanceEventId;
				eventCreate.create({
					event_name: "find_home_pro_clicked",
					event_type: "engagement",
					maintenance_item_event: eventId

				});
			}
			if (!User.current() && toState && toState.access && toState.access.requiresLogin) {
				event.preventDefault();
				AfterLogin.setNextState(toState, toParams);
				$state.go("login");
			}
		});

		$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
			if (User.current()) {
				window.Intercom('update');
			}
			$anchorScroll(0);
		});
		
		// Refreshes Appcues between page changes
		$rootScope.$on('$locationChangeSuccess', function() {
			if (window.Appcues) {
				window.Appcues.page();
			}
		});

		// Set the API Key
		$http.defaults.headers.common['HB-ApiKey'] = constants.ApiKey;
		// Add HB Version to Headers
		$http.defaults.headers.common['HB-Version'] = constants.version;

		// init the service keys
		serviceKeyList.init(api.serviceKey.index);

		// Try and init the JWT and user
		AppDelegate.startup();
	}]);

/*
 * utility function for moving array elements.
 */
Array.prototype.move = function(old_index, new_index) {
	if (new_index >= this.length) {
		var k = new_index - this.length;
		while ((k--) + 1) {
			this.push(undefined);
		}
	}
	this.splice(new_index, 0, this.splice(old_index, 1)[0]);
	// return this; // for testing purposes
};
