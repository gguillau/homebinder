/*
 * The AppDelegate handles application level functions such as startup, logon, logoff, shutdown
 */
angular
	.module("hb.appDelegate", [])
	.factory("AppDelegate", [
		"$state",
		"$http",
		"hb.api",
		"Session",
		"Context",
		"User",
		"AfterLogin",
		"$timeout",
		function($state, $http, api, session, context, User, AfterLogin, $timeout) {

			function init() {
				// Try and init the JWT and user
				var user = User.init();
				if (user && $state.current.name === "root") {
					AfterLogin.go();
				}
				else {
					var locale = session.getLocale();
					if (!locale) {
						session.setLocale("en-us", false);
					}
				}
			}

			return {
				// handles initialization code for the application when it is starting
				startup: function() {
					function storageChange(event) {
						if (event.key === 'logged_out') {
							Intercom('shutdown');
							session.clearAll();
							context.reset();
							$state.go("root");
						}
					}
					window.addEventListener('storage', storageChange, false);
					init();
				},
				loggedOn: function(token) {
					session.setJwt(token);
					$http.defaults.headers.common["HB-UserToken"] = token;
					window.localStorage.setItem('logged_in', false);
				},
				// handles cleanup code the application when the user is logging off
				logOff: function() {
					// get the user token
					var utoken = session.getUser().user_token;
					api.user
						.logoff(utoken)
						.then(
							function() {
								Intercom('shutdown');
								session.clearAll();
								context.reset();
								$state.go("root");
							});

					window.localStorage.setItem('logged_out', false);
				}
			};
		}
	]);