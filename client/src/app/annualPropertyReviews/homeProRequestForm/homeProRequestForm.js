(function() {
    "use strict";

    angular
        .module("hb.annualPropertyReviews.homeProRequest", [])
        .factory("AprHomeProRequestForm", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "annualPropertyReviews/homeProRequestForm/homeProRequestForm.tpl.html",
                            controller: "AprHomeProRequestFormController as ctrl",
                            backdrop: "static",
                            resolveData: opts
                        });
                    },
                    display: function(opts) {
                        Modals.show({
                            templateUrl: "annualPropertyReviews/homeProRequestForm/binderHomeProRequest.tpl.html",
                            controller: "AprHomeProRequestFormController as ctrl",
                            backdrop: "static",
                            resolveData: opts
                        });
                    }
                };
            }
        ])
        .controller("AprHomeProRequestFormController", [
            "$modalInstance",
            "data",
            "$filter",
            "hb.api",
            "Notify",
            "hb.resources",
            "Session",
            "$interpolate",
            AprHomeProRequestFormController
        ]);

    function AprHomeProRequestFormController($modal, data, $filter, api, notify, resources, session, $interpolate) {
        this.$modal = $modal;
        this.$filter = $filter;
        this.apr = data.apr;
        this.pro = data.pro;
        this.requestType = data.requestType;
        this.api = api;
        this.notify = notify;
        this.resources = this.apr ? resources.aprHomeProRequest : resources.binderHomeProRequest;
        this.currentUser = session.getUser();
        var exp = $interpolate(this.resources.defaultComments || "");
        this.comments = this.apr ? "" : exp({ companyName: this.pro.displayName, email: this.currentUser.email, phone: this.currentUser.phone || this.resources.phoneMessage });
        this.showError = false;
        this.processing = false;
        this.submitted = false;
        this.selectAll = true;
        this.populate();
    }

    AprHomeProRequestFormController.prototype = {

        populate: function() {
            if (this.apr && this.apr.annual_property_review_findings) {
                this.findings = this.$filter("filter")(this.apr.annual_property_review_findings, {
                    recommended_homepro: { displayName: this.pro.displayName }
                });

                this.findings.forEach(angular.bind(this, function(finding) {
                    finding.selected = this.selectAll;
                }));
            }
        },

        prioritySort: function(finding) {
            switch (finding.priority) {
                case "critical":
                    return 1;
                case "high":
                    return 2;
                case "low":
                    return 3;
                case "cosmetic":
                    return 4;
                case "tbd":
                    return 5;
            }
        },

        request: function() {
            if (this.requestType === "apr") {
                var selected = this.$filter("filter")(this.findings, {
                    selected: true
                });

                if (selected.length < 1) {
                    this.showError = true;
                    return;
                }

                this.showError = false;
                this.processing = true;

                var data = {
                    recommended_homepro_id: this.pro.id,
                    selected_ids: selected.map(function(finding) { return finding.id; }),
                    comments: this.comments
                };

                this.api.annualPropertyReview.requestQuote(this.apr.id, data).then(
                    angular.bind(this, this.requestSuccess),
                    angular.bind(this, this.requestError));
            }
            else if (this.requestType === "contact") {
                this.api.binderContractor.contact(this.pro.id, { comments: this.comments }).then(
                    angular.bind(this, this.requestSuccess),
                    angular.bind(this, this.requestError));
            }
        },

        requestSuccess: function(response) {
            this.submitted = true;
        },

        requestError: function(response) {
            this.processing = false;
            this.notify.error(response.data);
        },

        select: function() {
            this.findings.forEach(angular.bind(this, function(finding) {
                finding.selected = this.selectAll;
            }));
        },

        cancel: function() {
            this.$modal.dismiss();
        }

    };
})();