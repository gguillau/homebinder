describe("AprHomeProRequestFormController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $modalInstance,
        api,
        apr,
        pro,
        notify,
        $log,
        AprHomeProRequestForm,
        ModalService,
        $interpolate;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        notify = _$injector_.get("Notify");
        $log = _$injector_.get("$log");
        AprHomeProRequestForm = _$injector_.get("AprHomeProRequestForm");
        ModalService = _$injector_.get("ModalService");
        $interpolate = _$injector_.get("$interpolate");

        $modalInstance = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };

        pro = {
            id: 1,
            displayName: "Test"
        };

        apr = {
            id: 1,
            reviewer_company: {
                id: 1,
                name: "test"
            },
            annual_property_review_findings: [{ id: 1, recommended_homepro: { displayName: "test" } }]
        };

    }));

    function createController(data) {
        $scope = $rootScope.$new();
        ctrl = $controller("AprHomeProRequestFormController", {
            "hb.api": api,
            "$modalInstance": $modalInstance,
            "data": data,
            "Notify": notify,
            "$log": $log,
            "$interpolate": $interpolate
        });

        $scope.ctrl = ctrl;
    }

    describe("ctrl.populate", function() {
        it("should set the findings", function() {

            createController({ apr: apr, pro: pro });
            $rootScope.$apply();

            expect(ctrl.findings.length).toEqual(1);
        });

    });

    describe("ctrl.prioritySort", function() {
        it("returns 1", function() {

            createController({ apr: apr, pro: pro });
            $rootScope.$apply();
            var result = ctrl.prioritySort({ priority: "critical" });
            $scope.$apply();

            expect(result).toEqual(1);
        });

        it("returns 2", function() {

            createController({ apr: apr, pro: pro });
            $rootScope.$apply();
            var result = ctrl.prioritySort({ priority: "high" });
            $scope.$apply();

            expect(result).toEqual(2);
        });

        it("returns 3", function() {

            createController({ apr: apr, pro: pro });
            $rootScope.$apply();
            var result = ctrl.prioritySort({ priority: "low" });
            $scope.$apply();

            expect(result).toEqual(3);
        });

        it("returns 4", function() {

            createController({ apr: apr, pro: pro });
            $rootScope.$apply();
            var result = ctrl.prioritySort({ priority: "cosmetic" });
            $scope.$apply();

            expect(result).toEqual(4);
        });

        it("returns 5", function() {

            createController({ apr: apr, pro: pro });
            $rootScope.$apply();
            var result = ctrl.prioritySort({ priority: "tbd" });
            $scope.$apply();

            expect(result).toEqual(5);
        });
    });

    describe("ctrl.request", function() {
        it("returns because no findings selected", function() {
            createController({ apr: apr, pro: pro, requestType: "apr" });
            $rootScope.$apply();

            expect(ctrl.findings[0].selected).toBe(true);

            ctrl.selectAll = false;
            ctrl.select();
            $rootScope.$apply();

            ctrl.request();
            $rootScope.$apply();

            expect(ctrl.showError).toBe(true);
        });

        it("sends request", function() {
            spyOn(api.annualPropertyReview, "requestQuote").and.returnValue($q.when({ data: "success" }));

            createController({ apr: apr, pro: pro, requestType: "apr" });
            $rootScope.$apply();
            ctrl.request();
            $rootScope.$apply();

            expect(api.annualPropertyReview.requestQuote).toHaveBeenCalled();
            expect(ctrl.submitted).toBe(true);
        });

        it("sends request", function() {
            spyOn(api.binderContractor, "contact").and.returnValue($q.when({ data: "success" }));

            createController({ apr: {}, pro: pro, requestType: "contact" });
            $rootScope.$apply();
            ctrl.request();
            $rootScope.$apply();

            expect(api.binderContractor.contact).toHaveBeenCalled();
            expect(ctrl.submitted).toBe(true);
        });

        it("sends request and returns error", function() {
            spyOn(api.annualPropertyReview, "requestQuote").and.returnValue($q.reject({ data: "error" }));
            spyOn(notify, "error");

            createController({ apr: apr, pro: pro, requestType: "apr" });
            $rootScope.$apply();
            ctrl.request();
            $rootScope.$apply();

            expect(api.annualPropertyReview.requestQuote).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
            expect(ctrl.submitted).toBe(false);
        });

    });

    describe("ctrl.select", function() {
        it("sets findings selected to false", function() {

            createController({ apr: apr, pro: pro });
            $rootScope.$apply();

            expect(ctrl.findings[0].selected).toBe(true);

            ctrl.selectAll = false;
            ctrl.select();
            $rootScope.$apply();

            expect(ctrl.findings[0].selected).toBe(false);
        });

    });

    describe("ctrl.cancel", function() {
        it("should call $modalInstance dismiss function", function() {
            spyOn($modalInstance, "dismiss");

            createController({ apr: apr, pro: pro });
            $rootScope.$apply();
            ctrl.cancel();
            $rootScope.$apply();

            expect($modalInstance.dismiss).toHaveBeenCalled();
        });

    });

    describe('AprHomeProRequestForm', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            AprHomeProRequestForm.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});