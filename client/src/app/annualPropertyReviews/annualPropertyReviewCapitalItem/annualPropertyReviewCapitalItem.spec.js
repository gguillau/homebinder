describe("annualPropertyReviewCapitalItem", function() {
    var ctrl,
        controller,
        $scope,
        $q,
        $log,
        api,
        session,
        resources,
        $modalInstance,
        AprCapitalItemModal,
        ModalService,
        notify;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector, $rootScope) {
        controller = $controller;
        $scope = $rootScope;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        session = $injector.get("Session");
        resources = $injector.get("hb.resources");
        AprCapitalItemModal = $injector.get("AprCapitalItemModal");
        ModalService = $injector.get("ModalService");
        notify = $injector.get("Notify");

        spyOn(session, "getJwt").and.returnValue("jwt");
    }));

    $modalInstance = {
        close: function() {},
        dismiss: function() {}
    };

    function createController(data) {
        ctrl = controller("AprCapitalItemController", {
            "$log": $log,
            "hb.api": api,
            "Session": session,
            "hb.resources": resources,
            "$modalInstance": $modalInstance,
            "data": data
        });
    }

    describe("populate", function() {
        it("does nothing when no capitalItem is passed", function() {
            createController({ apr: undefined, capitalItem: undefined });
            expect(ctrl.eul).toEqual("unknown");
            expect(ctrl.costRange).toEqual("unknown");
        });

        it("populates when a capitalItem is passed", function() {
            createController({
                apr: {
                    id: 1
                },
                capitalItem: {
                    id: 1,
                    name: "item 1",
                    life: "one_to_two",
                    cost: "five_hun_to_one_thou",
                    comments: "hello"
                }
            });
            expect(ctrl.name).toEqual("item 1");
            expect(ctrl.eul).toEqual("one_to_two");
            expect(ctrl.costRange).toEqual("five_hun_to_one_thou");
        });
    });

    describe("submit", function() {
        it("does not submit when for is invalid", function() {
            spyOn(api.annualPropertyReviewCapitalItem, "create").and.returnValue($q.when({}));
            createController({ apr: { id: 1 }, capitalItem: undefined });
            ctrl.form = {
                $invalid: true
            };
            ctrl.submit();
            expect(ctrl.form.$submitted).toEqual(true);
            expect(api.annualPropertyReviewCapitalItem.create).not.toHaveBeenCalled();
        });

        it("creates a new capital item", function() {
            spyOn(api.annualPropertyReviewCapitalItem, "create").and.returnValue($q.when({ data: { id: 1 } }));
            spyOn($modalInstance, "close");
            createController({ apr: { id: 1 }, capitalItem: undefined });
            ctrl.name = "item";
            ctrl.eul = ctrl.euls[1].value;
            ctrl.costRange = ctrl.costRanges[1].value;
            ctrl.comments = "comments";
            pendingUploads = [];
            ctrl.form = {
                $invalid: false
            };
            ctrl.submit();
            $scope.$apply();
            expect(api.annualPropertyReviewCapitalItem.create).toHaveBeenCalledWith(jasmine.objectContaining({
                annual_property_review_capital_item: {
                    annual_property_review_id: 1,
                    name: "item",
                    eul: ctrl.euls[1].value,
                    cost_range: ctrl.costRanges[1].value,
                    comments: "comments"
                }
            }));
            expect($modalInstance.close).toHaveBeenCalled();
        });

        it("updates an existing capital item", function() {
            spyOn(api.annualPropertyReviewCapitalItem, "update").and.returnValue($q.when({ data: { id: 1 } }));
            spyOn($modalInstance, "close");
            createController({
                apr: { id: 1 },
                capitalItem: {
                    id: 1,
                    name: "item",
                    life: "less_one",
                    cost: "less_five_hun",
                    comments: "hello"
                }
            });
            pendingUploads = [];
            ctrl.form = {
                $invalid: false
            };
            ctrl.submit();
            $scope.$apply();
            expect(api.annualPropertyReviewCapitalItem.update).toHaveBeenCalledWith(1, jasmine.objectContaining({
                annual_property_review_capital_item: {
                    annual_property_review_id: 1,
                    name: "item",
                    eul: ctrl.euls[3].value,
                    cost_range: ctrl.costRanges[1].value,
                    comments: "hello"
                }
            }));
            expect($modalInstance.close).toHaveBeenCalled();
        });

        it("returns an error and calls notify", function() {
            spyOn(api.annualPropertyReviewCapitalItem, "update").and.returnValue($q.reject({ data: "error" }));
            spyOn(notify, "error");
            createController({
                apr: { id: 1 },
                capitalItem: {
                    id: 1,
                    name: "item",
                    life: "less_one",
                    cost: "less_five_hun",
                    comments: "hello"
                }
            });
            pendingUploads = [];
            ctrl.form = {
                $invalid: false
            };
            ctrl.submit();
            $scope.$apply();

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("cancel", function() {
        it("dismisses the dialog", function() {
            spyOn($modalInstance, "dismiss");
            createController({ apr: { id: 1 }, capitalItem: undefined });
            ctrl.cancel();
            expect($modalInstance.dismiss).toHaveBeenCalled();
        });
    });

    describe('AprCapitalItemModal', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            AprCapitalItemModal.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});
