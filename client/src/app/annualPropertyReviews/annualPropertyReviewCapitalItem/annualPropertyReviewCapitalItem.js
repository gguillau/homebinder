angular
    .module("hb.annualPropertyReviews.annualPropertyReviewCapitalItem", [])
    .factory("AprCapitalItemModal", [
        "ModalService",
        function(modals) {
            return {
                show: function(args) {
                    return modals.show({
                        aside: false,
                        templateUrl: "annualPropertyReviews/annualPropertyReviewCapitalItem/annualPropertyReviewCapitalItem.tpl.html",
                        controller: "AprCapitalItemController as ctrl",
                        resolveData: args
                    });
                }
            };
        }
    ])
    .controller("AprCapitalItemController", [
        "$log",
        "hb.api",
        "Session",
        "hb.resources",
        "$modalInstance",
        "data",
        "Notify",
        function($log, api, session, resources, $modalInstance, args, notify) {
            var vm = this;
            vm.resources = resources.aprCapitalItem;
            vm.apr = args.apr;
            vm.item = args.capitalItem;
            vm.euls = [
                { name: vm.resources.eulUnknown, value: "unknown" },
                { name: vm.resources.eulNone, value: "none" },
                { name: vm.resources.eulNA, value: "not_applicable" },
                { name: vm.resources.eulLessOne, value: "less_one" },
                { name: vm.resources.eulOneToTwo, value: "one_to_two" },
                { name: vm.resources.eulTwoToThree, value: "two_to_three" },
                { name: vm.resources.eulThreeToFive, value: "three_to_five" },
                { name: vm.resources.eulFivetoTen, value: "five_to_ten" },
                { name: vm.resources.eulTenToFiften, value: "ten_to_fifteen" },
                { name: vm.resources.eulFifteenToTwenty, value: "fifteen_to_twenty" },
                { name: vm.resources.eulTwentyPlus, value: "twenty_plus" }
            ];
            vm.costRanges = [
                { name: vm.resources.eulUnknown, value: "unknown" },
                { name: vm.resources.costRangeLessFiveHun, value: "less_five_hun" },
                { name: vm.resources.costRangeFiveHunToOneThou, value: "five_hun_to_one_thou" },
                { name: vm.resources.costRangeOneThouToTwentyFiveHun, value: "one_thou_to_twentyfive_hun" },
                { name: vm.resources.costRangeTwentyFiveHunToFiveThou, value: "twentyfive_hun_to_five_thou" },
                { name: vm.resources.costRangeFiveThouToTenThou, value: "five_thou_to_ten_thou" },
                { name: vm.resources.costRangeTenThouToFifteenThou, value: "ten_thou_to_fifteen_thou" },
                { name: vm.resources.costRangeFifteenThouToTwentyThou, value: "fifteen_thou_to_twenty_thou" },
                { name: vm.resources.costRangeTwentyThouPlus, value: "twenty_thou_plus" }
            ];
            vm.eul = vm.euls[0].value;
            vm.costRange = vm.costRanges[0].value;
            vm.submit = submit;
            vm.cancel = cancel;

            vm.imgUploader = {
                fileTypes: "image",
                hideUploadButton: true,
                showTable: false,
                selectButtonLabel: resources.aprCapitalItem.addPhoto,
                fileLimit: false,
                url: "/api/v1/annual_property_review_photos",
                multiSelect: true,
                id: "uploadImages",
                hide_upload: false,
                jwt: session.getJwt()
            };

            populate();

            function populate() {
                if (!vm.item) {
                    return;
                }

                vm.name = vm.item.name;
                vm.eul = vm.item.life;
                vm.costRange = vm.item.cost;
                vm.comments = vm.item.comments;
            }

            function submit() {
                vm.form.$submitted = true;
                if (vm.form.$invalid) {
                    return;
                }

                var data = {
                    annual_property_review_id: vm.apr.id,
                    name: vm.name,
                    eul: vm.eul,
                    cost_range: vm.costRange,
                    comments: vm.comments
                };

                if (vm.item) {
                    api.annualPropertyReviewCapitalItem.update(vm.item.id, { annual_property_review_capital_item: data }).then(saved, error);
                }
                else {
                    api.annualPropertyReviewCapitalItem.create({ annual_property_review_capital_item: data }).then(saved, error);
                }
            }

            function saved(response) {
                vm.item = response.data;
                $modalInstance.close(vm.item);
            }

            function error(response) {
                notify.error(response.data);
            }

            function cancel() {
                $modalInstance.dismiss();
            }
        }
    ]);
