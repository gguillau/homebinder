describe("aprCancel", function() {
    var ctrl,
        controller,
        $scope,
        $q,
        $log,
        api,
        resources,
        notify,
        $modalInstance,
        AprCancelModal,
        ModalService;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector, $rootScope) {
        controller = $controller;
        $scope = $rootScope;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        AprCancelModal = $injector.get("AprCancelModal");
        ModalService = $injector.get("ModalService");
    }));

    $modalInstance = {
        close: function() {},
        dismiss: function() {}
    };

    function createController(data) {
        ctrl = controller("AprCancelController", {
            "$log": $log,
            "hb.api": api,
            "hb.resources": resources,
            "Notify": notify,
            "$modalInstance": $modalInstance,
            "data": { apr: data }
        });
    }

    describe("confirm", function() {
        it("cancels an apr", function() {
            spyOn(api.annualPropertyReview, "update").and.returnValue($q.when({}));
            spyOn($modalInstance, "close");
            createController({ id: 1 });
            ctrl.cancelReason = "because";
            ctrl.confirm();
            $scope.$apply();
            expect(api.annualPropertyReview.update).toHaveBeenCalledWith(1, jasmine.objectContaining({
                annual_property_review: {
                    status: "canceled",
                    cancel_reason: "because"
                }
            }));
            expect($modalInstance.close).toHaveBeenCalled();
        });

        it("notifies an apr can not be canceled", function() {
            spyOn(api.annualPropertyReview, "update").and.returnValue($q.reject({ status: 400, data: "error" }));
            spyOn($modalInstance, "close");
            createController({ id: 1 });
            ctrl.confirm();
            $scope.$apply();
            expect(api.annualPropertyReview.update).toHaveBeenCalled();
            expect($modalInstance.close).not.toHaveBeenCalled();
            expect(ctrl.invalid).toEqual(true);
            expect(ctrl.error).toEqual("error");
        });

        it("notifies of other errors", function() {
            spyOn(api.annualPropertyReview, "update").and.returnValue($q.reject({ status: 500, data: "error" }));
            spyOn($modalInstance, "close");
            spyOn(notify, "error");
            createController({ id: 1 });
            ctrl.confirm();
            $scope.$apply();
            expect(api.annualPropertyReview.update).toHaveBeenCalled();
            expect($modalInstance.close).not.toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalledWith("error");
        });
    });

    describe("cancel", function() {
        it("dismisses the dialog", function() {
            spyOn($modalInstance, "dismiss");
            createController({ id: 1 });
            ctrl.cancel();
            expect($modalInstance.dismiss).toHaveBeenCalled();
        });
    });

    describe('AprCancelModal', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            AprCancelModal.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});
