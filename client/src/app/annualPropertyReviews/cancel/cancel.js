angular
    .module("hb.annualPropertyReviews.cancel", [])
    .factory("AprCancelModal", [
        "ModalService",
        function(modals) {
            return {
                show: function(opts) {
                    return modals.show({
                        templateUrl: "annualPropertyReviews/cancel/cancel.tpl.html",
                        controller: "AprCancelController as ctrl",
                        resolveData: opts
                    });
                }
            };
        }
    ])
    .controller("AprCancelController", [
        "$log",
        "hb.api",
        "hb.resources",
        "Notify",
        "$modalInstance",
        "data",
        function($log, api, resources, notify, $modalInstance, opts) {
            var vm = this;
            vm.resources = resources.aprCancel;
            vm.confirm = confirm;
            vm.cancel = cancel;
            vm.invalid = false;
            var apr = opts.apr;

            function confirm() {
                api.annualPropertyReview.update(apr.id, {
                    annual_property_review: {
                        status: "canceled",
                        cancel_reason: vm.cancelReason
                    }
                }).then(
                    confirmed,
                    notConfirmd
                );
            }

            function confirmed(response) {
                $modalInstance.close(response);
            }

            function notConfirmd(err) {
                if (err.status == 400) {
                    vm.invalid = true;
                    vm.error = err.data;
                }
                else {
                    notify.error(err.data);
                }
            }

            function cancel() {
                $modalInstance.dismiss();
            }
        }
    ]);