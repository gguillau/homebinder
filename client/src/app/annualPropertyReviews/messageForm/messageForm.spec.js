describe("AprMessageFormController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $modalInstance,
        api,
        apr,
        user,
        notify,
        $log,
        AprMessageForm,
        ModalService;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        notify = _$injector_.get("Notify");
        $log = _$injector_.get("$log");
        AprMessageForm = _$injector_.get("AprMessageForm");
        ModalService = _$injector_.get("ModalService");

        $modalInstance = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };

        user = {
            id: 1
        };

        apr = {
            id: 1,
            reviewer_company: {
                id: 1,
                name: "test"
            }
        };

    }));

    function createController(data) {
        $scope = $rootScope.$new();
        ctrl = $controller("AprMessageFormController", {
            "hb.api": api,
            "$modalInstance": $modalInstance,
            "data": data,
            "Notify": notify,
            "$log": $log
        });

        $scope.ctrl = ctrl;
    }

    describe("ctrl.init", function() {
        it("sets the apr and user", function() {
            createController({ apr: apr, user: user });
            $rootScope.$apply();

            expect(ctrl.apr).toEqual(apr);
            expect(ctrl.user).toEqual(user);
        });

        it("calls annualPropertyReview get", function() {
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: { id: 1, reviewer_company: {} } }));

            createController({ apr: { id: 1 }, user: undefined });
            $rootScope.$apply();

            expect(api.annualPropertyReview.get).toHaveBeenCalled();
        });
    });

    describe("ctrl.setMessage", function() {
        it("sets message", function() {
            createController({ apr: apr, user: user });
            ctrl.user = undefined;
            ctrl.setMessage();

            expect(ctrl.message).toEqual("<p style='font-size: 14px;'>Hello test!</p><p style='font-size: 14px;'>Thank you for handling my Annual Property Review&trade;!</p><p style='font-size: 14px;'>I would like to reschedule my Annual Property Review&trade; for undefined undefined, undefined undefined. You can call me at undefined.</p><p style='font-size: 14px;'>Best regards,</p><p style='font-size: 14px;'>undefined undefined</p>");
        });
    });

    describe("ctrl.getApr", function() {

        it("sets the apr and calls setMessage", function() {
            createController({ apr: apr, user: user });
            $rootScope.$apply();
            spyOn(ctrl, "setMessage");
            ctrl.getApr({ data: { id: 1 } });
            $rootScope.$apply();

            expect(ctrl.apr).toEqual({ id: 1 });
            expect(ctrl.setMessage).toHaveBeenCalled();
        });

    });

    describe("ctrl.sendMessage", function() {

        it("sends message", function() {

            spyOn(api.annualPropertyReview, "message").and.returnValue($q.when({ data: {} }));
            spyOn(notify, "info");
            spyOn($modalInstance, "close");

            createController({ apr: apr, user: user });
            $rootScope.$apply();
            ctrl.sendMessage();
            $rootScope.$apply();

            expect(api.annualPropertyReview.message).toHaveBeenCalled();
            expect(notify.info).toHaveBeenCalled();
            expect($modalInstance.close).toHaveBeenCalled();
        });

        it("returns an error", function() {

            spyOn(api.annualPropertyReview, "message").and.returnValue($q.reject({ data: "error" }));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController({ apr: apr, user: user });
            $rootScope.$apply();
            ctrl.sendMessage();
            $rootScope.$apply();

            expect(api.annualPropertyReview.message).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
        });

    });

    describe("ctrl.cancel", function() {

        it("should call $modalInstance dismiss function", function() {
            spyOn($modalInstance, "dismiss");

            createController({ apr: apr, user: user });
            $rootScope.$apply();
            ctrl.cancel();
            $rootScope.$apply();

            expect($modalInstance.dismiss).toHaveBeenCalledWith('cancel');
        });

    });

    describe('AprMessageForm', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            AprMessageForm.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});