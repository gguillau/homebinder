(function() {
    "use strict";

    angular
        .module("hb.annualPropertyReviews.messages", [])
        .factory("AprMessageForm", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "annualPropertyReviews/messageForm/messageForm.tpl.html",
                            controller: "AprMessageFormController as ctrl",
                            backdrop: true,
                            resolveData: opts
                        });
                    }
                };
            }
        ])
        .controller("AprMessageFormController", [
            "$modalInstance",
            "data",
            "$filter",
            "hb.api",
            "Notify",
            "$log",
            "hb.resources",
            AprMessageFormController
        ]);

    function AprMessageFormController($modal, data, $filter, api, notify, $log, resources) {
        this.$modal = $modal;
        this.apr = data.apr;
        this.user = data.user;
        this.$filter = $filter;
        this.api = api;
        this.notify = notify;
        this.$log = $log;
        this.message = null;
        this.sending = false;
        this.resources = resources.aprMessageForm;
        this.init();
    }

    AprMessageFormController.prototype = {

        init: function() {
            if (!this.apr.reviewer_company) {
                this.api.annualPropertyReview.get(this.apr.id)
                    .then(angular.bind(this, this.getApr), angular.bind(this, this.errorCallback));
            }
            else {
                this.setMessage();
            }

        },

        getApr: function(response) {
            this.apr = response.data;
            this.setMessage();
        },

        setMessage: function() {
            if (this.user) {
                this.message = "<p style='font-size: 14px;'>Hello " + this.apr.client_first_name + "!</p><p style='font-size: 14px;'>Thank you for ordering your Annual Property Review&trade;!</p><p style='font-size: 14px;'>Let me know if you have any questions, comments or concerns.</p><p style='font-size: 14px;'>Best regards,</p><p style='font-size: 14px;'>" + this.apr.reviewer_company.name + "</p>";
            }
            else {
                this.message = "<p style='font-size: 14px;'>Hello " + this.apr.reviewer_company.name + "!</p><p style='font-size: 14px;'>Thank you for handling my Annual Property Review&trade;!</p><p style='font-size: 14px;'>I would like to reschedule my Annual Property Review&trade; for " + this.apr.address1 + " " + this.apr.city + ", " + this.apr.state + " " + this.apr.zip + ". You can call me at " + this.$filter("tel")(this.apr.client_phone) + ".</p><p style='font-size: 14px;'>Best regards,</p><p style='font-size: 14px;'>" + this.apr.client_first_name + " " + this.apr.client_last_name + "</p>";
            }
        },

        sendMessage: function() {
            this.sending = true;
            var data = {
                message: this.message
            };
            this.api.annualPropertyReview.message(this.apr.id, data).then(angular.bind(this, this.sendMessageSuccess), angular.bind(this, this.errorCallback));
        },

        sendMessageSuccess: function(response) {
            this.notify.info(this.resources.messageSent);
            this.$modal.close(this.message);
        },

        errorCallback: function(response) {
            this.sending = false;
            this.$log.error(response);
            this.notify.error(response.data);
        },

        cancel: function() {
            this.$modal.dismiss("cancel");
        }

    };
})();