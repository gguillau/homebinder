angular
    .module("hb.annualPropertyReviews.details.binder", [])
    .factory("AprBinderModal", [
        "ModalService",
        function(modals) {
            return {
                show: function(args) {
                    return modals.show({
                        aside: false,
                        templateUrl: "annualPropertyReviews/details/binder/binder.tpl.html",
                        controller: "AprBinderModalController as ctrl",
                        resolveData: args.apr,
                        closed: args.closed,
                        keyboard: false,
                        backdrop: "static"
                    });
                }
            };
        }
    ])
    .controller("AprBinderModalController", [
        "$log",
        "hb.api",
        "hb.resources",
        "$modalInstance",
        "data",
        "Notify",
        "Address",
        AprBinderModalController
    ]);

function AprBinderModalController($log, api, resources, $modal, apr, notify, Address) {
    this.$log = $log;
    this.api = api;
    this.resources = resources.aprRequest;
    this.resources = angular.extend(this.resources, resources.partnerHomeownerResources);
    this.$modal = $modal;
    this.apr = apr;
    this.hbLogoUrl = "img/flatlogo.png";
    this.notify = notify;
    this.address = Address;
    this.country = this.address.findCountryByCode(this.apr.country);
    this.countries = this.address.countries();
    this.states = this.address.getStatesAndProvinces();
    this.refresh();
}

AprBinderModalController.prototype = {
    refresh: function() {
        this.api.partner.automation(this.apr.reviewer_company.partner_key).then(
            angular.bind(this, this.refreshSuccess),
            angular.bind(this, this.refreshError));
    },

    refreshSuccess: function(response) {
        this.partner = response.data;
    },

    refreshError: function(response) {
        this.$log.error(response);
        this.notify.info(response.data);
    },

    submit: function() {
        var params = {
            method: "manual",
            binder_template_id: this.partner.default_binder_template_id,
            client: {
                email: this.apr.client_email,
                first: this.apr.client_first_name,
                last: this.apr.client_last_name,
                phone: this.apr.client_email
            },
            property: {
                address: this.apr.address1,
                address2: this.apr.address2,
                city: this.apr.city,
                state: this.apr.state,
                zip: this.apr.zip,
                country: this.apr.country
            }
        };

        this.api.partner.abs(this.apr.reviewer_company.partner_key, params, this.partner.api_key).then(
            angular.bind(this, this.createBinderSuccess),
            angular.bind(this, this.createBinderError)
        );
    },

    createBinderSuccess: function(response) {
        this.notify.success("Created!");
        this.$modal.close(response.data);
    },

    createBinderError: function(response) {
        this.notify.error(response.data);
    },

    cancel: function() {
        this.$modal.dismiss();
    },

    onSelect: function(item) {
        if (item) {
            this.country = item.country;
            this.apr.country = item.country.value;
        }
    }
};