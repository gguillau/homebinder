describe("AprBinderModalController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $state,
        api,
        address,
        resources,
        $modal,
        loading,
        notify,
        AprBinderModal,
        ModalService;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        address = _$injector_.get("Address");
        resources = _$injector_.get("hb.resources");
        AprBinderModal = _$injector_.get("AprBinderModal");
        ModalService = _$injector_.get("ModalService");

        $modal = {
            dismiss: function() {},
            close: function() {}
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        $state = {
            go: function(state, args) {}
        };
        api = {
            partner: {
                automation: function() {},
                abs: function() {}
            }
        };
        notify = {
            success: function() {},
            error: function() {},
            info: function() {}
        };
    }));
    beforeEach(function() {
        spyOn(api.partner, "automation").and.returnValue($q.when({
            data: {
                api_key: "fakekey",
                default_binder_template_id: 117
            }
        }));
    });

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("AprBinderModalController", {
            "$state": $state,
            "hb.api": api,
            "Address": address,
            "hb.resources": resources,
            "Loading": loading,
            "Notify": notify,
            "$modalInstance": $modal,
            "data": { reviewer_company: { partner_key: "test" } }
        });
        $scope.ctrl = ctrl;
    }

    describe("init", function() {
        it("calls api partner automation", function() {
            createController();
            expect(api.partner.automation).toHaveBeenCalled();
        });
    });

    describe("refreshSuccess", function() {
        it("sets the partner", function() {
            createController();
            ctrl.refreshSuccess({ data: { id: 1 } });

            expect(ctrl.partner).toEqual({ id: 1 });
        });
    });

    describe("refreshError", function() {
        it("sets the country", function() {
            createController();
            spyOn(notify, "info");
            ctrl.refreshError({ data: "error" });

            expect(notify.info).toHaveBeenCalled();
        });
    });

    describe("submit", function() {
        it("calls api partner abs", function() {
            spyOn(api.partner, "abs").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.apr = { reviewer_company: {} };
            ctrl.partner = {};
            ctrl.submit();

            expect(api.partner.abs).toHaveBeenCalled();
        });
    });

    describe("createBinderSuccess", function() {
        it("calls notify", function() {
            createController();
            spyOn(notify, "success");
            ctrl.createBinderSuccess({ data: { id: 1 } });

            expect(notify.success).toHaveBeenCalled();
        });
    });

    describe("createBinderError", function() {
        it("calls notify", function() {
            createController();
            spyOn(notify, "error");
            ctrl.createBinderError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("cancel", function() {
        it("calls modal dismiss", function() {
            createController();
            spyOn($modal, "dismiss");
            ctrl.cancel();

            expect($modal.dismiss).toHaveBeenCalled();
        });
    });

    describe("onSelect", function() {
        it("sets the country", function() {
            createController();
            spyOn($modal, "dismiss");
            var item = { country: {} };
            ctrl.onSelect(item);

            expect(ctrl.country).toEqual(item.country);
        });
    });

    describe('AprBinderModal', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            AprBinderModal.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});
