angular
    .module("hb.annualPropertyReviews.details", [
        "hb.annualPropertyReviews.details.binder"
    ])
    .directive("hbAnnualPropertyReview", [
        function() {
            return {
                restrict: "E",
                templateUrl: "annualPropertyReviews/details/details.tpl.html",
                controller: "AprDetailsController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        }
    ])
    .controller("AprDetailsController", [
        '$state',
        '$stateParams',
        'hb.api',
        '$log',
        '$filter',
        'ModalService',
        'AprScheduleModal',
        'AprFindingModal',
        'AprCapitalItemModal',
        'AprCancelModal',
        'hb.resources',
        'Session',
        'Notify',
        'Loading',
        '$window',
        'AprMessageForm',
        'AprEditModal',
        '$location',
        'AprHomeProForm',
        'AprHomeProRequestForm',
        'AprFindingOptions',
        'AprBinderModal',
        function($state, $stateParams, api, $log, $filter, modals, aprScheduleModal, aprFindingModal,
            aprCapitalItemModal, aprCancelModal, resources, session, notify, loading, $window, AprMessageForm, AprEditModal, $location, AprHomeProForm, AprHomeProRequestForm, AprFindingOptions,
            AprBinderModal) {

            var vm = this;
            vm.resources = resources.aprs;
            vm.resources = angular.extend({}, vm.resources, resources.aprDetails);
            vm.apr = null;
            vm.refresh = refresh;
            vm.addFinding = addFinding;
            vm.editFinding = editFinding;
            vm.deleteFinding = deleteFinding;
            vm.addCapitalItem = addCapitalItem;
            vm.editCapitalItem = editCapitalItem;
            vm.deleteCapitalItem = deleteCapitalItem;
            vm.addUpdateItemInArray = addUpdateItemInArray;
            vm.imgAdded = imgAdded;
            vm.deletePhoto = deletePhoto;
            vm.createBinder = createBinder;
            vm.updateApr = updateApr;
            vm.populate = populate;
            vm.findingSaved = findingSaved;
            vm.capitalItemSaved = capitalItemSaved;
            vm.confirmDelete = confirmDelete;
            vm.canSchedule = false;
            vm.canDraftComplete = false;
            vm.canComplete = false;
            vm.canCancel = false;
            vm.canHold = false;
            vm.canMessageReviewer = false;
            vm.canMessageHomeowner = false;
            vm.schedule = schedule;
            vm.draftComplete = draftComplete;
            vm.complete = complete;
            vm.cancel = cancel;
            vm.hold = hold;
            vm.download = download;
            vm.reviewerCommentsUpdated = reviewerCommentsUpdated;
            vm.imgUploadSuccess = imgUploadSuccess;
            vm.imgUploadError = imgUploadError;
            vm.uploadImages = uploadImages;
            vm.startDraft = startDraft;
            vm.messageModal = messageModal;
            vm.prioritySort = prioritySort;
            vm.lifeSort = lifeSort;
            vm.backHome = backHome;
            vm.editApr = editApr;
            vm.reDraft = reDraft;
            vm.viewHomePro = viewHomePro;
            vm.deleteConfirmed = deleteConfirmed;
            vm.statusChangeConfirmed = statusChangeConfirmed; //for testing purposes
            vm.newFinding = newFinding; //for testing purposes
            vm.requestQuote = requestQuote;
            vm.canEdit = false;
            vm.hbLogoUrl = "img/flatlogo.png";
            vm.default_comments = "No Comments left by the Reviewer";
            vm.max = 100;
            vm.progress = 0;
            vm.finding_id = null;
            vm.index = null;
            vm.indexParams = {};
            vm.id = "uploadImages" + new Date().getTime();
            vm.imgUploader = {
                fileTypes: "image",
                hideUploadButton: true,
                showTable: false,
                selectButtonLabel: resources.aprDetails.addPhoto,
                fileLimit: false,
                url: "/api/v1/annual_property_review_photos",
                multiSelect: false,
                id: vm.id,
                hide_upload: true,
                jwt: session.getJwt()
            };

            vm.refresh();

            function refresh() {
                loading.show(vm.resources.loading);
                api.annualPropertyReview.get($stateParams.aprId)
                    .then(refreshComplete, refreshError);
            }

            function refreshComplete(response) {
                vm.apr = response.data;
                vm.imgUploader.params = {
                    annual_property_review_id: vm.apr.id
                };
                loading.close();
                populate();
            }

            function refreshError(response) {
                notify.error(response.data);
                loading.close();
            }

            function setSettings() {
                vm.user = session.getUser();
                if (!vm.user || vm.user.role.toLowerCase() === "homeowner") {
                    vm.canEdit = false;
                    vm.canMessageReviewer = true;
                }
                else {
                    vm.canMessageHomeowner = true;
                    vm.canEdit = vm.apr.status === "draft" || vm.apr.status === "draft_completed";
                    vm.canSchedule = vm.apr.status == "active" || vm.apr.status == "scheduled" || vm.apr.status == "on_hold";
                    vm.canDraftComplete = vm.apr.status == "draft";
                    vm.canReDraft = vm.apr.status == "completed";
                    vm.canComplete = vm.apr.status == "draft_completed";
                    vm.canStartDraft = vm.apr.status == "scheduled";
                    vm.canEditComments = vm.apr.status != "completed" && vm.apr.status != "canceled" && vm.apr.status != "on_hold";
                }

                // can a partner also cancel and hold
                vm.canCancel = vm.apr.status == "active" || vm.apr.status == "scheduled" || vm.apr.status == "on_hold" || vm.apr.status == "draft";
                vm.canHold = vm.apr.status == "active" || vm.apr.status == "scheduled";
                vm.canDownload = vm.apr.status === "completed" || vm.user;
                setIndex();
            }

            function setIndex() {
                if (vm.user) {
                    switch (vm.user.role) {
                        case "admin":
                            vm.index = "admin.reports";
                            break;
                        case "inspector":
                        case "apr_reviewer":
                        case "broker":
                            vm.index = "partner.annualPropertyReviews";
                            vm.indexParams = { partnerId: vm.user.partner_id };
                            break;
                    }
                }
            }

            function uploadImages(id) {
                if (!vm.canEdit) {
                    return;
                }
                else if (vm.apr.status === "completed") {
                    return;
                }
                if (id) {
                    vm.finding_id = id;
                }
                angular.element('#' + vm.id).trigger('click');
            }

            function populate() {
                setSettings();

                var url = $location.protocol() + '://' + $location.host();
                var link = url + "/apr/" + vm.apr.uuid;

                vm.details = {
                    today: new Date(),
                    preparedFor: vm.apr.client_first_name + " " + vm.apr.client_last_name,
                    address: vm.apr.address1 + (vm.apr.address2 ? " " + vm.apr.address2 : "") + ", " + vm.apr.city + ", " + vm.apr.state,
                    reviewedOn: vm.apr.reviewed_on ? $filter('date')(new Date(vm.apr.reviewed_on), 'medium') : vm.apr.scheduled_for ? $filter('date')(new Date(vm.apr.scheduled_for), 'medium') : "--",
                    reviewerCompany: vm.apr.reviewer_company.name,
                    reviewerPhone: vm.apr.reviewer_company.phone,
                    reviewer: vm.apr.reviewer ? vm.apr.reviewer.first_name + " " + vm.apr.reviewer.last_name : vm.resources.notAssigned,
                    promo_code: vm.apr.promo_code_name,
                    promo_code_amount_off: vm.apr.promo_code_amount_off,
                    binder_id: vm.apr.binder ? vm.apr.binder.id : null,
                    homeownerLink: link
                };
                vm.findings = vm.apr.annual_property_review_findings.map(function(finding) {
                    if (finding.recommended_homepro) {
                        finding.recommended_homepro.displayName = finding.recommended_homepro.name;
                        return finding;
                    }
                    else {
                        return finding;
                    }
                });
                vm.capitalItems = vm.apr.annual_property_review_capital_items.map(function(item) {
                    return {
                        id: item.id,
                        name: item.name,
                        life: item.eul,
                        cost: item.cost_range,
                        comments: item.comments,
                        photos: item.annual_property_review_photos
                    };
                });
                vm.reviewerComments = vm.apr.property_reviewer_comments;

                switch (vm.apr.status) {
                    case "active":
                        vm.progress = 20;
                        break;
                    case "scheduled":
                        vm.progress = 40;
                        break;
                    case "draft":
                        vm.progress = 60;
                        break;
                    case "draft_completed":
                        vm.progress = 80;
                        break;
                    case "completed":
                        vm.progress = 100;
                        break;
                }
            }

            function addUpdateItemInArray(arr, item) {
                var k = 0,
                    len;
                for (k = 0, len = arr.length; k < len; k++) {
                    if (arr[k].id == item.id) {
                        arr.splice(k, 1);
                        break;
                    }
                }
                arr.push(item);
            }

            function addFinding() {
                AprFindingOptions.show({
                    findingSaved: findingSaved,
                    closed: newFinding
                });
            }

            function newFinding() {
                aprFindingModal.show({
                    apr: vm.apr,
                    closed: findingSaved
                });
            }

            function editFinding(finding) {
                aprFindingModal.show({
                    apr: vm.apr,
                    finding: finding,
                    closed: findingSaved
                });
            }

            function findingSaved(finding) {
                addUpdateItemInArray(vm.apr.annual_property_review_findings, finding);
                populate();
            }

            function deleteFinding(finding) {
                confirmDelete({
                    item: finding,
                    deleteService: angular.bind(api.annualPropertyReviewFinding, api.annualPropertyReviewFinding.destroy, finding.id),
                    source: vm.apr.annual_property_review_findings
                });
            }

            function addCapitalItem() {
                aprCapitalItemModal.show({
                    apr: vm.apr,
                    closed: capitalItemSaved
                });
            }

            function editCapitalItem(item) {
                aprCapitalItemModal.show({
                    apr: vm.apr,
                    capitalItem: item,
                    closed: capitalItemSaved
                });
            }

            function capitalItemSaved(capitalItem) {
                addUpdateItemInArray(vm.apr.annual_property_review_capital_items, capitalItem);
                populate();
            }

            function deleteCapitalItem(capitalItem) {
                confirmDelete({
                    item: capitalItem,
                    deleteService: angular.bind(api.annualPropertyReviewCapitalItem, api.annualPropertyReviewCapitalItem.destroy, capitalItem.id),
                    source: vm.apr.annual_property_review_capital_items
                });
            }

            function confirmDelete(args) {
                modals.confirm({
                    message: vm.resources.deleteMajor + args.item.name + "?",
                    minorMessage: vm.resources.deleteMinor,
                    confirm: angular.bind(vm, deleteConfirmed, args)
                });
            }

            function deleteConfirmed(args) {
                args.deleteService().then(function() {
                    var k, len;
                    for (k = 0, len = args.source.length; k < len; k++) {
                        if (args.source[k].id == args.item.id) {
                            args.source.splice(k, 1);
                            populate();
                            return;
                        }
                    }
                });
            }

            function imgAdded() {
                loading.show(vm.resources.uploading);
                var uploadApi = vm.imgUploader.api;
                if (uploadApi && uploadApi.pendingUploads().length > 0) {
                    if (vm.finding_id) {
                        uploadApi.updateParams({
                            annual_property_review_id: vm.apr.id,
                            annual_property_review_finding_id: vm.finding_id
                        });
                    }
                    else {
                        uploadApi.updateParams({
                            annual_property_review_id: vm.apr.id
                        });
                    }
                    vm.finding_id = null;
                    uploadApi.uploadFiles();
                }
                else {
                    loading.close();
                }
            }

            function imgUploadSuccess(file) {
                if (file.annual_property_review_finding_id) {
                    var findings = $filter("filter")(vm.findings, {
                        id: file.annual_property_review_finding_id
                    }, true);

                    if (findings.length > 0) {
                        var index = vm.findings.indexOf(findings[0]);
                        vm.findings[index].annual_property_review_photos.push(file);
                    }
                }
                else {
                    if (vm.apr.annual_property_review_photos.length > 0) {
                        deletePhoto(vm.apr.annual_property_review_photos[0]);
                    }
                    vm.apr.annual_property_review_photos.push(file);
                }
                if (vm.imgUploader.api.pendingUploads().length === 0) {
                    loading.close();
                    return;
                }
                vm.imgAdded();
            }

            function imgUploadError(response) {
                $log.error(response);
                notify.error(response.data);
                var files = vm.imgUploader.api.pendingUploads();
                vm.imgUploader.api.remove(files[0]);
                vm.imgAdded();
            }

            function deletePhoto(photo) {
                deleteConfirmed({
                    deleteService: angular.bind(api.annualPropertyReviewPhoto, api.annualPropertyReviewPhoto.destroy, photo.id),
                    item: photo,
                    source: vm.apr.annual_property_review_photos
                });
            }

            function schedule() {
                aprScheduleModal.show({
                    apr: vm.apr,
                    closed: aprUpdated
                });
            }

            function startDraft() {
                statusChangeConfirmed("draft");
            }

            function draftComplete() {
                modals.confirm({
                    title: vm.resources.confirmTitle,
                    message: vm.resources.draftCompleteMajorMessage,
                    confirmLabel: vm.resources.confirmComplete,
                    cancelLabel: vm.resources.cancel,
                    confirm: angular.bind(vm, statusChangeConfirmed, "draft_completed")
                });
            }

            function reDraft() {
                modals.confirm({
                    title: vm.resources.confirmTitle,
                    message: vm.resources.reDraftMajorMessage,
                    confirmLabel: vm.resources.reDraft,
                    cancelLabel: vm.resources.cancel,
                    confirm: angular.bind(vm, statusChangeConfirmed, "draft")
                });
            }

            function complete() {
                modals.confirm({
                    title: vm.resources.confirmTitle,
                    message: vm.resources.completeMajorMessage,
                    minorMessage: vm.resources.completeMinorMessage,
                    confirmLabel: vm.resources.confirmComplete,
                    cancelLabel: vm.resources.cancel,
                    confirm: angular.bind(vm, statusChangeConfirmed, "completed")
                });
            }

            function cancel() {
                aprCancelModal.show(vm.apr).then(
                    aprUpdated,
                    aprUpdateError
                );
            }

            function hold() {
                modals.confirm({
                    title: vm.resources.confirmTitle,
                    message: vm.resources.holdMajorMessage,
                    minorMessage: vm.resources.holdMinorMessage,
                    confirmLabel: vm.resources.confirmHold,
                    cancelLabel: vm.resources.cancel,
                    confirm: angular.bind(vm, statusChangeConfirmed, "on_hold")
                });
            }

            function reviewerCommentsUpdated() {
                var data = {
                    property_reviewer_comments: vm.reviewerComments
                };
                loading.show(vm.resources.updating);
                api.annualPropertyReview.update(vm.apr.id, {
                    annual_property_review: data
                }).then(
                    aprUpdated,
                    aprUpdateError
                );
            }

            function statusChangeConfirmed(status) {
                vm.apr.status = status;
                var data = {
                    status: status
                };
                loading.show(vm.resources.updating);
                api.annualPropertyReview.update(vm.apr.id, {
                    annual_property_review: data
                }).then(
                    aprUpdated,
                    aprUpdateError
                );
            }

            function aprUpdated(response) {
                vm.apr = response.data;
                loading.close();
                populate();
            }

            function aprUpdateError(response) {
                notify.error(response.data);
                loading.close();
            }

            function download() {
                loading.show(vm.resources.download);
                api.annualPropertyReview.download(vm.apr.id).then(downloadSuccess, downloadError);
            }

            function downloadSuccess(response) {
                notify.info(vm.resources.downloadFinish);
                loading.close();
            }

            function downloadError(response) {
                $log.error(response);
                notify.error(response.data);
                loading.close();
            }

            function messageModal() {
                AprMessageForm.show({
                    user: vm.user,
                    apr: vm.apr
                });
            }

            function prioritySort(finding) {
                switch (finding.priority) {
                    case "critical":
                        return 1;
                    case "high":
                        return 2;
                    case "low":
                        return 3;
                    case "cosmetic":
                        return 4;
                    case "tbd":
                        return 5;
                }
            }

            function lifeSort(capitalItem) {
                switch (capitalItem.life) {
                    case "none":
                        return 1;
                    case "less_one":
                        return 2;
                    case "one_to_two":
                        return 3;
                    case "two_to_three":
                        return 4;
                    case "three_to_five":
                        return 5;
                    case "five_to_ten":
                        return 6;
                    case "ten_to_fifteen":
                        return 7;
                    case "fifteen_to_twenty":
                        return 8;
                    case "twenty_plus":
                        return 9;
                }
            }

            function backHome() {
                $state.go(vm.index, vm.indexParams);
            }

            function editApr() {
                AprEditModal.show({
                    apr: vm.apr,
                    closed: aprUpdated
                });
            }

            function viewHomePro(pro) {
                AprHomeProForm.show({
                    pro: pro,
                    apr: vm.apr
                });
            }

            function requestQuote(pro) {
                AprHomeProRequestForm.show({
                    pro: pro,
                    apr: vm.apr,
                    requestType: "apr"
                });
            }

            function createBinder() {
                AprBinderModal.show({ apr: vm.apr, closed: updateApr });
            }

            function updateApr(binder) {
                vm.details.binder_id = binder.id;
                api.annualPropertyReview.update(vm.apr.id, {
                    annual_property_review: { binder_id: binder.id }
                }).then(
                    aprUpdated,
                    aprUpdateError
                );
            }

        }
    ]);
