describe("AprDetailsController", function() {
    var ctrl,
        controller,
        $scope,
        $q,
        $log,
        api,
        resources,
        notify,
        $state,
        $location,
        $stateParams,
        session,
        AprFindingModal,
        AprFindingOptions,
        AprCapitalItemModal,
        AprScheduleModal,
        ModalService,
        Loading,
        AprMessageForm,
        AprEditModal,
        AprHomeProForm,
        AprHomeProRequestForm,
        AprBinderModal,
        aprCancelModal;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector, $rootScope) {
        controller = $controller;
        $scope = $rootScope;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        $state = $injector.get("$state");
        resources = $injector.get("hb.resources");
        $location = $injector.get("$location");
        $stateParams = $injector.get("$stateParams");
        session = $injector.get("Session");
        AprFindingModal = $injector.get("AprFindingModal");
        AprCapitalItemModal = $injector.get("AprCapitalItemModal");
        AprScheduleModal = $injector.get("AprScheduleModal");
        ModalService = $injector.get("ModalService");
        Loading = $injector.get("Loading");
        AprMessageForm = $injector.get("AprMessageForm");
        AprEditModal = $injector.get("AprEditModal");
        AprHomeProForm = $injector.get("AprHomeProForm");
        AprHomeProRequestForm = $injector.get("AprHomeProRequestForm");
        AprFindingOptions = $injector.get("AprFindingOptions");
        AprBinderModal = $injector.get("AprBinderModal");
        aprCancelModal = $injector.get("AprCancelModal");
    }));

    function createController(data) {
        ctrl = controller("AprDetailsController", {
            "$log": $log,
            "hb.api": api,
            "hb.resources": resources,
            "Notify": notify,
            "$state": $state,
            "$location": $location,
            "$stateParams": $stateParams,
            "Session": session,
            "AprFindingModal": AprFindingModal,
            "AprCapitalItemModal": AprCapitalItemModal,
            "AprScheduleModal": AprScheduleModal,
            "ModalService": ModalService,
            "AprBinderModal": AprBinderModal
        });
    }

    describe("init", function() {
        it("gets the apr for homeowner", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                status: "active",
                annual_property_review_findings: [{ id: 1, recommended_homepro: {} }, { id: 2 }],
                annual_property_review_capital_items: [{ id: 1 }]
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;

            createController();
            $scope.$apply();

            expect(api.annualPropertyReview.get).toHaveBeenCalled();
            expect(ctrl.apr.id).toEqual(1);
            expect(ctrl.canEdit).toBe(false);
            expect(ctrl.canMessageReviewer).toBe(true);
        });

        it("gets the apr for admin", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                status: "scheduled",
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(session, "getUser").and.returnValue({ role: "admin" });
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;

            createController();
            $scope.$apply();

            expect(api.annualPropertyReview.get).toHaveBeenCalled();
            expect(ctrl.apr.id).toEqual(1);
            expect(ctrl.canMessageReviewer).toBe(false);
            expect(ctrl.canMessageHomeowner).toBe(true);
            expect(ctrl.index).toEqual("admin.reports");
        });

        it("gets the apr for broker", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                status: "draft",
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(session, "getUser").and.returnValue({ role: "broker", partner_id: 1 });
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;

            createController();
            $scope.$apply();

            expect(api.annualPropertyReview.get).toHaveBeenCalled();
            expect(ctrl.apr.id).toEqual(1);
            expect(ctrl.canMessageReviewer).toBe(false);
            expect(ctrl.canMessageHomeowner).toBe(true);
            expect(ctrl.index).toEqual("partner.annualPropertyReviews");
            expect(ctrl.indexParams).toEqual({ partnerId: 1 });
        });

        it("gets the apr for inspector", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                status: "draft_completed",
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(session, "getUser").and.returnValue({ role: "inspector", partner_id: 1 });
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;

            createController();
            $scope.$apply();

            expect(api.annualPropertyReview.get).toHaveBeenCalled();
            expect(ctrl.apr.id).toEqual(1);
            expect(ctrl.canMessageReviewer).toBe(false);
            expect(ctrl.canMessageHomeowner).toBe(true);
            expect(ctrl.index).toEqual("partner.annualPropertyReviews");
            expect(ctrl.indexParams).toEqual({ partnerId: 1 });
        });

        it("returns an error", function() {
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.reject({ data: "error" }));
            spyOn(notify, "error");
            $stateParams.aprId = 1;

            createController();
            $scope.$apply();

            expect(api.annualPropertyReview.get).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("uploadImages", function() {
        it("returns", function() {
            createController();
            ctrl.canEdit = false;
            ctrl.uploadImages(1);
        });

        it("returns", function() {
            createController();
            ctrl.apr = {
                status: "completed"
            };
            ctrl.canEdit = true;
            ctrl.uploadImages(1);
        });

        it("triggers the click", function() {
            createController();
            ctrl.apr = {
                status: "active"
            };
            ctrl.canEdit = true;
            ctrl.uploadImages(1);
        });
    });

    describe("addUpdateItemInArray", function() {
        it("updates the item in the array", function() {

            createController();
            var item = { id: 1, name: "updated" };
            var array = [{ id: 1, name: "old" }];
            expect(array[0].name).toEqual("old");
            ctrl.addUpdateItemInArray(array, item);


            expect(array[0].name).toEqual("updated");
        });
    });

    describe("findingSaved", function() {
        it("calls addUpdateItemInArray and populate", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;

            createController();
            spyOn(ctrl, "addUpdateItemInArray");
            spyOn(ctrl, "populate");
            $scope.$apply();
            ctrl.findingSaved({});
        });
    });

    describe("capitalItemSaved", function() {
        it("calls addUpdateItemInArray and populate", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;

            createController();
            spyOn(ctrl, "addUpdateItemInArray");
            spyOn(ctrl, "populate");
            $scope.$apply();
            ctrl.capitalItemSaved({});
        });
    });

    describe("deleteFinding", function() {
        it("calls confirmDelete", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;

            createController();
            spyOn(ctrl, "confirmDelete");
            $scope.$apply();
            ctrl.deleteFinding({});
        });
    });

    describe("deleteCapitalItem", function() {
        it("calls confirmDelete", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;

            createController();
            spyOn(ctrl, "confirmDelete");
            $scope.$apply();
            ctrl.deleteCapitalItem({});
        });
    });

    describe("addFinding", function() {
        it("calls the finding modal", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprFindingOptions, "show");

            createController();
            $scope.$apply();
            ctrl.addFinding();
            $scope.$apply();

            expect(AprFindingOptions.show).toHaveBeenCalled();
        });
    });

    describe("newFinding", function() {
        it("calls the finding modal", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprFindingModal, "show");

            createController();
            $scope.$apply();
            ctrl.newFinding();
            $scope.$apply();

            expect(AprFindingModal.show).toHaveBeenCalled();
        });
    });

    describe("editFinding", function() {
        it("calls the finding modal", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprFindingModal, "show");

            createController();
            $scope.$apply();
            ctrl.editFinding({ id: 1 });
            $scope.$apply();

            expect(AprFindingModal.show).toHaveBeenCalled();
        });
    });

    describe("addCapitalItem", function() {
        it("calls the capitalItem modal", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprCapitalItemModal, "show");

            createController();
            $scope.$apply();
            ctrl.addCapitalItem();
            $scope.$apply();

            expect(AprCapitalItemModal.show).toHaveBeenCalled();
        });
    });

    describe("editCapitalItem", function() {
        it("calls the capitalItem modal", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprCapitalItemModal, "show");

            createController();
            $scope.$apply();
            ctrl.editCapitalItem({ id: 1 });
            $scope.$apply();

            expect(AprCapitalItemModal.show).toHaveBeenCalled();
        });
    });

    describe("schedule", function() {
        it("calls the AprScheduleModal modal", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprScheduleModal, "show");

            createController();
            $scope.$apply();
            ctrl.schedule();
            $scope.$apply();

            expect(AprScheduleModal.show).toHaveBeenCalled();
        });
    });

    describe("draftComplete", function() {
        it("calls the modal confirm function", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(ModalService, "confirm");

            createController();
            $scope.$apply();
            ctrl.draftComplete();
            $scope.$apply();

            expect(ModalService.confirm).toHaveBeenCalled();
        });
    });

    describe("reDraft", function() {
        it("calls the modal confirm function", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(ModalService, "confirm");

            createController();
            $scope.$apply();
            ctrl.reDraft();
            $scope.$apply();

            expect(ModalService.confirm).toHaveBeenCalled();
        });
    });

    describe("complete", function() {
        it("calls the modal confirm function", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(ModalService, "confirm");

            createController();
            $scope.$apply();
            ctrl.complete();
            $scope.$apply();

            expect(ModalService.confirm).toHaveBeenCalled();
        });
    });

    describe("hold", function() {
        it("calls the modal confirm function", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(ModalService, "confirm");

            createController();
            $scope.$apply();
            ctrl.hold();
            $scope.$apply();

            expect(ModalService.confirm).toHaveBeenCalled();
        });
    });

    describe("imgAdded", function() {
        it("gets the pending uploads and closes the loading modal", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            spyOn(Loading, "show");
            spyOn(Loading, "close");
            $stateParams.aprId = 1;

            createController();
            $scope.$apply();
            ctrl.imgUploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            spyOn(ctrl.imgUploader.api, "pendingUploads").and.returnValue([]);
            spyOn(ctrl.imgUploader.api, "uploadFiles");
            ctrl.finding_id = 1;
            ctrl.imgAdded();
            $scope.$apply();

            expect(ctrl.imgUploader.api.pendingUploads).toHaveBeenCalled();
            expect(Loading.show.calls.count()).toEqual(2);
            expect(Loading.close.calls.count()).toEqual(2);
            expect(ctrl.imgUploader.api.uploadFiles).not.toHaveBeenCalled();
        });

        it("gets the pending uploads and uploads and sets the finding id", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            spyOn(Loading, "show");
            spyOn(Loading, "close");
            $stateParams.aprId = 1;

            createController();
            $scope.$apply();
            ctrl.imgUploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            spyOn(ctrl.imgUploader.api, "pendingUploads").and.returnValue([{ file_file_name: "file.jpg" }]);
            spyOn(ctrl.imgUploader.api, "uploadFiles");
            ctrl.finding_id = 1;
            ctrl.imgAdded();
            $scope.$apply();

            expect(ctrl.imgUploader.api.pendingUploads).toHaveBeenCalled();
            expect(ctrl.imgUploader.api.uploadFiles).toHaveBeenCalled();
            expect(Loading.show.calls.count()).toEqual(2);
            expect(Loading.close.calls.count()).toEqual(1);
        });

        it("gets the pending uploads and uploads", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            spyOn(Loading, "show");
            spyOn(Loading, "close");
            $stateParams.aprId = 1;

            createController();
            $scope.$apply();
            ctrl.imgUploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            spyOn(ctrl.imgUploader.api, "pendingUploads").and.returnValue([{ file_file_name: "file.jpg" }]);
            spyOn(ctrl.imgUploader.api, "uploadFiles");
            ctrl.imgAdded();
            $scope.$apply();

            expect(ctrl.imgUploader.api.pendingUploads).toHaveBeenCalled();
            expect(ctrl.imgUploader.api.uploadFiles).toHaveBeenCalled();
            expect(Loading.show.calls.count()).toEqual(2);
            expect(Loading.close.calls.count()).toEqual(1);
        });
    });

    describe("imgUploadSuccess", function() {
        it("gets the pending uploads and closes the loading modal", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            spyOn(Loading, "show");
            spyOn(Loading, "close");
            $stateParams.aprId = 1;

            createController();
            $scope.$apply();
            ctrl.imgUploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            spyOn(ctrl.imgUploader.api, "pendingUploads").and.returnValue([]);
            spyOn(ctrl.imgUploader.api, "uploadFiles");
            ctrl.finding_id = 1;
            var file = { annual_property_review_finding_id: 1 };
            ctrl.findings = [{ id: 1, annual_property_review_photos: [] }];
            ctrl.imgUploadSuccess(file);

            expect(ctrl.findings[0].annual_property_review_photos.length).toEqual(1);
        });

        it("gets the pending uploads and closes the loading modal", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            spyOn(Loading, "show");
            spyOn(Loading, "close");
            $stateParams.aprId = 1;

            createController();
            $scope.$apply();
            ctrl.imgUploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            spyOn(ctrl.imgUploader.api, "pendingUploads").and.returnValue([{ id: 2 }]);
            spyOn(ctrl.imgUploader.api, "uploadFiles");
            ctrl.finding_id = 2;
            var file = { };
            ctrl.apr = {
                annual_property_review_photos: [{ id: 1 }]
            };
            spyOn(ctrl, "imgAdded");
            ctrl.findings = [{ id: 1, annual_property_review_photos: [] }];
            ctrl.imgUploadSuccess(file);

            expect(ctrl.apr.annual_property_review_photos.length).toEqual(2);
        });

    });

    describe("imgUploadError", function() {
        it("gets the pending uploads and closes the loading modal", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            spyOn(Loading, "show");
            spyOn(Loading, "close");
            $stateParams.aprId = 1;

            createController();
            $scope.$apply();
            ctrl.imgUploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            spyOn(ctrl.imgUploader.api, "pendingUploads").and.returnValue([]);
            spyOn(ctrl.imgUploader.api, "uploadFiles");
            ctrl.finding_id = 1;
            ctrl.imgAdded();
            $scope.$apply();

            expect(ctrl.imgUploader.api.pendingUploads).toHaveBeenCalled();
            expect(Loading.show.calls.count()).toEqual(2);
            expect(Loading.close.calls.count()).toEqual(2);
            expect(ctrl.imgUploader.api.uploadFiles).not.toHaveBeenCalled();
        });

        it("calls notify", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            spyOn(Loading, "show");
            spyOn(Loading, "close");
            spyOn(notify, "error");
            $stateParams.aprId = 1;

            createController();
            $scope.$apply();
            ctrl.imgUploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {},
                    remove: function() {}
                }
            };
            spyOn(ctrl.imgUploader.api, "pendingUploads").and.returnValue([{ file_file_name: "file.jpg" }]);
            spyOn(ctrl.imgUploader.api, "uploadFiles");

            ctrl.imgUploadError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("deletePhoto", function() {
        it("calls the modal confirm function", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: [],
                annual_property_review_photos: [{ id: 2 }]
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            spyOn(api.annualPropertyReviewPhoto, "destroy").and.returnValue($q.when({ data: {} }));
            $stateParams.aprId = 1;

            createController();
            $scope.$apply();
            ctrl.deletePhoto({ id: 2 });
            $scope.$apply();

            expect(api.annualPropertyReviewPhoto.destroy).toHaveBeenCalled();
        });
    });

    describe("reviewerCommentsUpdated", function() {
        it("updates the apr for admin", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(session, "getUser").and.returnValue({ role: "admin" });
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            spyOn(api.annualPropertyReview, "update").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;

            createController();
            $scope.$apply();

            ctrl.reviewerCommentsUpdated();
            $scope.$apply();

            expect(api.annualPropertyReview.update).toHaveBeenCalled();
        });

        it("returns an error", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(session, "getUser").and.returnValue({ role: "admin" });
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            spyOn(api.annualPropertyReview, "update").and.returnValue($q.reject({ data: "error" }));
            spyOn(notify, "error");
            $stateParams.aprId = 1;

            createController();
            $scope.$apply();

            ctrl.reviewerCommentsUpdated();
            $scope.$apply();

            expect(api.annualPropertyReview.update).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("startDraft", function() {
        it("calls statusChangeConfirmed", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;

            createController();
            spyOn(ctrl, "statusChangeConfirmed");
            $scope.$apply();
            ctrl.startDraft({});
        });
    });

    describe("statusChangeConfirmed", function() {
        it("updates the apr for admin", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(session, "getUser").and.returnValue({ role: "admin" });
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            spyOn(api.annualPropertyReview, "update").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;

            createController();
            $scope.$apply();

            ctrl.statusChangeConfirmed("completed");
            $scope.$apply();

            expect(api.annualPropertyReview.update).toHaveBeenCalled();
        });

        it("returns an error", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(session, "getUser").and.returnValue({ role: "admin" });
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            spyOn(api.annualPropertyReview, "update").and.returnValue($q.reject({ data: "error" }));
            spyOn(notify, "error");
            $stateParams.aprId = 1;

            createController();
            $scope.$apply();

            ctrl.statusChangeConfirmed("completed");
            $scope.$apply();

            expect(api.annualPropertyReview.update).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("download", function() {
        it("updates the apr for admin", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(session, "getUser").and.returnValue({ role: "admin" });
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            spyOn(api.annualPropertyReview, "download").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;

            createController();
            $scope.$apply();

            ctrl.download();
            $scope.$apply();

            expect(api.annualPropertyReview.download).toHaveBeenCalled();
        });

        it("returns an error", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(session, "getUser").and.returnValue({ role: "admin" });
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            spyOn(api.annualPropertyReview, "download").and.returnValue($q.reject({ data: "error" }));
            spyOn(notify, "error");
            $stateParams.aprId = 1;

            createController();
            $scope.$apply();

            ctrl.download();
            $scope.$apply();

            expect(api.annualPropertyReview.download).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("messageModal", function() {
        it("calls the AprMessageForm modal", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprMessageForm, "show");

            createController();
            $scope.$apply();
            ctrl.messageModal();
            $scope.$apply();

            expect(AprMessageForm.show).toHaveBeenCalled();
        });
    });

    describe("prioritySort", function() {
        it("returns 1", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprMessageForm, "show");

            createController();
            $scope.$apply();
            var result = ctrl.prioritySort({ priority: "critical" });
            $scope.$apply();

            expect(result).toEqual(1);
        });

        it("returns 2", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprMessageForm, "show");

            createController();
            $scope.$apply();
            var result = ctrl.prioritySort({ priority: "high" });
            $scope.$apply();

            expect(result).toEqual(2);
        });

        it("returns 3", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprMessageForm, "show");

            createController();
            $scope.$apply();
            var result = ctrl.prioritySort({ priority: "low" });
            $scope.$apply();

            expect(result).toEqual(3);
        });

        it("returns 4", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprMessageForm, "show");

            createController();
            $scope.$apply();
            var result = ctrl.prioritySort({ priority: "cosmetic" });
            $scope.$apply();

            expect(result).toEqual(4);
        });

        it("returns 5", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprMessageForm, "show");

            createController();
            $scope.$apply();
            var result = ctrl.prioritySort({ priority: "tbd" });
            $scope.$apply();

            expect(result).toEqual(5);
        });
    });

    describe("lifeSort", function() {
        it("returns 1", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprMessageForm, "show");

            createController();
            $scope.$apply();
            var result = ctrl.lifeSort({ life: "none" });
            $scope.$apply();

            expect(result).toEqual(1);
        });

        it("returns 2", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprMessageForm, "show");

            createController();
            $scope.$apply();
            var result = ctrl.lifeSort({ life: "less_one" });
            $scope.$apply();

            expect(result).toEqual(2);
        });

        it("returns 3", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprMessageForm, "show");

            createController();
            $scope.$apply();
            var result = ctrl.lifeSort({ life: "one_to_two" });
            $scope.$apply();

            expect(result).toEqual(3);
        });

        it("returns 4", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprMessageForm, "show");

            createController();
            $scope.$apply();
            var result = ctrl.lifeSort({ life: "two_to_three" });
            $scope.$apply();

            expect(result).toEqual(4);
        });

        it("returns 5", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprMessageForm, "show");

            createController();
            $scope.$apply();
            var result = ctrl.lifeSort({ life: "three_to_five" });
            $scope.$apply();

            expect(result).toEqual(5);
        });

        it("returns 6", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprMessageForm, "show");

            createController();
            $scope.$apply();
            var result = ctrl.lifeSort({ life: "five_to_ten" });
            $scope.$apply();

            expect(result).toEqual(6);
        });

        it("returns 7", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprMessageForm, "show");

            createController();
            $scope.$apply();
            var result = ctrl.lifeSort({ life: "ten_to_fifteen" });
            $scope.$apply();

            expect(result).toEqual(7);
        });

        it("returns 8", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprMessageForm, "show");

            createController();
            $scope.$apply();
            var result = ctrl.lifeSort({ life: "fifteen_to_twenty" });
            $scope.$apply();

            expect(result).toEqual(8);
        });

        it("returns 9", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprMessageForm, "show");

            createController();
            $scope.$apply();
            var result = ctrl.lifeSort({ life: "twenty_plus" });
            $scope.$apply();

            expect(result).toEqual(9);
        });
    });

    describe("backHome", function() {
        it("goes back to admin dashboard", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(session, "getUser").and.returnValue({ role: "admin" });
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn($state, "go");

            createController();
            $scope.$apply();
            ctrl.backHome();
            $scope.$apply();

            expect($state.go).toHaveBeenCalledWith("admin.reports", {});
        });

        it("goes back to partner dashboard", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(session, "getUser").and.returnValue({ role: "broker", partner_id: 1 });
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn($state, "go");

            createController();
            $scope.$apply();
            ctrl.backHome();
            $scope.$apply();

            expect($state.go).toHaveBeenCalledWith("partner.annualPropertyReviews", { partnerId: 1 });
        });

        it("goes back to partner dashboard", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(session, "getUser").and.returnValue({ role: "inspector", partner_id: 1 });
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn($state, "go");

            createController();
            $scope.$apply();
            ctrl.backHome();
            $scope.$apply();

            expect($state.go).toHaveBeenCalledWith("partner.annualPropertyReviews", { partnerId: 1 });
        });
    });

    describe("editApr", function() {
        it("calls the AprEditModal modal", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprEditModal, "show");

            createController();
            $scope.$apply();
            ctrl.editApr();
            $scope.$apply();

            expect(AprEditModal.show).toHaveBeenCalled();
        });
    });

    describe("viewHomePro", function() {
        it("calls the HomePro modal", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprHomeProForm, "show");

            createController();
            $scope.$apply();
            ctrl.viewHomePro({ email: "" });
            $scope.$apply();

            expect(AprHomeProForm.show).toHaveBeenCalled();
        });
    });

    describe("requestQuote", function() {
        it("calls the requestQuote modal", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprHomeProRequestForm, "show");

            createController();
            $scope.$apply();
            ctrl.requestQuote({ email: "" });
            $scope.$apply();

            expect(AprHomeProRequestForm.show).toHaveBeenCalled();
        });
    });

    describe("cancel", function() {
        it("calls the cancel modal", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(aprCancelModal, "show").and.returnValue($q.when({ data: {} }));

            createController();
            $scope.$apply();
            ctrl.cancel();

            expect(aprCancelModal.show).toHaveBeenCalled();
        });
    });

    describe("createBinder", function() {
        it("calls the AprBinderModal modal", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(AprBinderModal, "show");

            createController();
            ctrl.createBinder();

            expect(AprBinderModal.show).toHaveBeenCalled();
        });
    });

    describe("updateApr", function() {
        it("calls the annualPropertyReview update", function() {
            var apr = {
                id: 1,
                reviewer_company: {
                    name: "company"
                },
                annual_property_review_findings: [],
                annual_property_review_capital_items: []
            };
            spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: apr }));
            $stateParams.aprId = 1;
            spyOn(api.annualPropertyReview, "update").and.returnValue($q.when({ data: {} }));

            createController();
            ctrl.details = {};
            ctrl.apr = {};
            ctrl.updateApr({ id: 1 });

            expect(api.annualPropertyReview.update).toHaveBeenCalled();
        });
    });

});

describe('hbAnnualPropertyReview', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.annualPropertyReview, "get").and.returnValue($q.when({ data: { id: 1, reviewer_company: { name: "Test" }, reviewer: {}, annual_property_review_findings: [], annual_property_review_capital_items: [] } }));

        $compile('<hb-annual-property-review></hb-annual-property-review>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the hb-annual-property-review directive', function() {
        it('calls api annualPropertyReview get', function() {
            expect(api.annualPropertyReview.get).toHaveBeenCalled();
        });
    });
});