angular
    .module("hb.annualPropertyReviews", [
        "hb.annualPropertyReviews.annualPropertyReviewFinding",
        "hb.annualPropertyReviews.annualPropertyReviewCapitalItem",
        "hb.annualPropertyReviews.details",
        "hb.annualPropertyReviews.schedule",
        "hb.annualPropertyReviews.edit",
        "hb.annualPropertyReviews.cancel",
        "hb.annualPropertyReviews.request",
        "hb.annualPropertyReviews.routes",
        "hb.annualPropertyReviews.reviewers",
        "hb.annualPropertyReviews.messages",
        "hb.annualPropertyReviews.homePro",
        "hb.annualPropertyReviews.homeProRequest"
    ])
    .directive("hbAnnualPropertyReviews", function() {
        return {
            restrict: "E",
            templateUrl: "annualPropertyReviews/annualPropertyReviews.tpl.html",
            controller: "AnnualPropertyReviewsController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {
                cfg: "="
            }
        };
    })
    .controller("AnnualPropertyReviewsController", [
        '$state',
        '$stateParams',
        'hb.framework.indexBase',
        'hb.api',
        'hb.resources',
        'Notify',
        'Context',
        'Session',
        'AprReviewerFindForm',
        'Loading',
        '$log',
        '$window',
        'ModalService',
        'AprScheduleModal',
        'AprCancelModal',
        'AprMessageForm',
        '$location',
        'AprEditModal',
        function($state, $stateParams, IndexBase, api, resources, notify, Context, Session, AprReviewerFindForm,
            Loading, $log, $window, ModalService, aprScheduleModal, AprCancelModal, AprMessageForm, $location, AprEditModal) {
            var Model = function(cfg) {
                this.cfg = cfg;

                // call the parent class
                IndexBase.call(this);

                // apply apr specific resource strings
                this.resources = angular.extend({}, this.resources, resources.aprs);
                this.resources = angular.extend({}, this.resources, resources.aprDetails);
                this.homeownerView = $stateParams.binderId !== undefined;
                this.partnerView = $stateParams.partnerId !== undefined;
                this.adminView = !this.partnerView;
                this.canSchedule = this.adminView || this.partnerView;
                this.canStartDraft = this.adminView || this.partnerView;
                this.canDraftComplete = this.adminView || this.partnerView;
                this.canReDraft = this.adminView || this.partnerView;
                this.canComplete = this.adminView;
                this.canCancel = this.adminView || this.homeownerView;
                this.canHold = this.adminView || this.homeownerView;
                this.currentUser = Session.getUser();
                this.canSearchCompanies = this.currentUser.role === "admin";
                this.goToDraft = false;
                this.aprArgs = {
                    staus: undefined,
                    companyId: $stateParams.partnerId
                };
                this.statusOptions = [{
                    name: undefined,
                    value: this.resources.allStatuses
                }, {
                    name: "active",
                    value: this.resources.active
                }, {
                    name: "scheduled",
                    value: this.resources.scheduled
                }, {
                    name: "draft",
                    value: this.resources.draft
                }, {
                    name: "draft_completed",
                    value: this.resources.draftCompleted
                }, {
                    name: "canceled",
                    value: this.resources.canceled
                }, {
                    name: "on_hold",
                    value: this.resources.onHold
                }, {
                    name: "completed",
                    value: this.resources.completed
                }];
                // set the refresh API call
                if (this.partnerView) {
                    this.queryArgs = { companyId: $stateParams.partnerId, searchMethod: "for_company" };
                    this.refreshCall = angular.bind(api.annualPropertyReview, api.annualPropertyReview.index);
                }
                else {
                    this.refreshCall = angular.bind(api.annualPropertyReview, api.annualPropertyReview.index);
                }

                // set page specific values on toolbar
                angular.extend(this.toolbarCfg, {
                    title: this.resources.title,
                    searchBox: true,
                    total: null,
                    button: this.homeownerView ? {
                        title: this.resources.newButton,
                        click: this.requestApr
                    } : null
                    /*sort: {
                        title: this.resources.sortTitle,
                        sortOptions: [
                            { desc: this.resources.sortById, orderBy: "organizations.id", order: "asc" },
                            { desc: this.resources.sortByIdDesc, orderBy: "organizations.id", order: "desc" },
                            { desc: this.resources.sortByName, orderBy: "organizations.name", order: "asc" },
                            { desc: this.resources.sortByNameDesc, orderBy: "organizations.name", order: "desc" }
                        ],
                        execute: angular.bind(this, this.sort)
                    }*/
                });
                //this.toolbarCfg.sort.sortOption = this.toolbarCfg.sort.sortOptions[2];

                if (!this.homeownerView) {
                    this.filters = [
                        { value: "", text: "All" },
                        { value: "emailed", text: "Email" },
                        { value: "active", text: "Active" },
                        { value: "scheduled", text: "Scheduled" },
                        { value: "draft", text: "Draft" },
                        { value: "draft_completed", text: "Draft Completed" },
                        { value: "completed", text: "Completed" },
                        { value: "canceled", text: "Canceled" },
                        { value: "on_hold", text: "On Hold" }
                    ]; // TODO: scope filters based on whether page is admin, partner or user
                    this.aprFilter = this.filters[0];
                }

                // refresh the list
                this.refresh();
            };

            Model.prototype = Object.create(IndexBase.prototype);

            angular.extend(Model.prototype, {
                selectFilter: function(filter) {
                    this.aprFilter = filter;
                    this.queryArgs = angular.extend({}, this.queryArgs, { filter: filter.value });
                    this.refresh();
                },

                requestApr: function() {
                    if (this.currentUser.role === "admin" && !$stateParams.partnerId) {
                        AprReviewerFindForm.show();
                    }
                    else {
                        Context.getPartner($stateParams.partnerId).then(function(partner) {
                            var url = $location.protocol() + '://' + $location.host();
                            var aprLink = partner.partner_key ? url + "/partners/" + partner.partner_key + "/apr#order" : null;
                            $window.open(aprLink, "_blank");
                        });
                    }
                },

                view: function(apr) {
                    $state.go("annualPropertyReview", { aprId: apr.id });
                },

                canHoldApr: function(apr) {
                    return apr.status === "active" || apr.status === "scheduled";
                },

                canScheduleApr: function(apr) {
                    return apr.status === "active" || apr.status === "scheduled" || apr.status === "on_hold";
                },

                canStartDraftApr: function(apr) {
                    return apr.status === "scheduled";
                },

                canDraftCompleteApr: function(apr) {
                    return apr.status === "draft";
                },

                canReDraftApr: function(apr) {
                    return apr.status === "completed";
                },

                canCompleteApr: function(apr) {
                    return apr.status === "draft_completed";
                },

                canCancelApr: function(apr) {
                    return apr.status === "active" || apr.status === "scheduled" || apr.status === "on_hold" || apr.status === "draft";
                },

                schedule: function(apr) {
                    aprScheduleModal.show({
                        apr: apr,
                        closed: angular.bind(this, this.aprUpdated, apr)
                    });
                },

                startDraft: function(apr) {
                    this.goToDraft = true;
                    this.statusChangeConfirmed(apr, "draft");
                },

                draftComplete: function(apr) {
                    ModalService.confirm({
                        title: this.resources.confirmTitle,
                        message: this.resources.draftCompleteMajorMessage,
                        confirmLabel: this.resources.confirmComplete,
                        cancelLabel: this.resources.cancel,
                        confirm: angular.bind(this, this.statusChangeConfirmed, apr, "draft_completed")
                    });
                },

                reDraft: function(apr) {
                    ModalService.confirm({
                        title: this.resources.confirmTitle,
                        message: this.resources.reDraftMajorMessage,
                        confirmLabel: this.resources.reDraft,
                        cancelLabel: this.resources.cancel,
                        confirm: angular.bind(this, this.statusChangeConfirmed, apr, "draft")
                    });
                },

                complete: function(apr) {
                    ModalService.confirm({
                        title: this.resources.confirmTitle,
                        message: this.resources.completeMajorMessage,
                        minorMessage: this.resources.completeMinorMessage,
                        confirmLabel: this.resources.confirmComplete,
                        cancelLabel: this.resources.cancel,
                        confirm: angular.bind(this, this.statusChangeConfirmed, apr, "completed")
                    });
                },

                cancel: function(apr) {
                    AprCancelModal.show({ apr: apr, closed: angular.bind(this, this.aprUpdated, apr) });
                },

                hold: function(apr) {
                    ModalService.confirm({
                        title: this.resources.confirmTitle,
                        message: this.resources.holdMajorMessage,
                        minorMessage: this.resources.holdMinorMessage,
                        confirmLabel: this.resources.confirmHold,
                        cancelLabel: this.resources.cancel,
                        confirm: angular.bind(this, this.statusChangeConfirmed, apr, "on_hold")
                    });
                },

                statusChangeConfirmed: function(apr, status) {
                    apr.status = status;
                    var data = {
                        status: status
                    };
                    Loading.show(this.resources.updating);
                    api.annualPropertyReview.update(apr.id, {
                        annual_property_review: data
                    }).then(
                        angular.bind(this, this.aprUpdated, apr),
                        angular.bind(this, this.aprUpdateError)
                    );
                },

                aprUpdated: function(apr, response) {
                    var index = this.items.indexOf(apr);
                    this.items[index] = response.data;

                    if (this.goToDraft === true) {
                        this.view(response.data);
                    }
                    Loading.close();
                },

                aprUpdateError: function(response) {
                    notify.error(response.data);
                    Loading.close();
                },

                download: function(apr) {
                    Loading.show(this.resources.download);
                    api.annualPropertyReview.download(apr.id).then(angular.bind(this, this.downloadSuccess, apr), angular.bind(this, this.downloadError));
                },

                downloadSuccess: function(apr, response) {
                    notify.info(this.resources.downloadFinish);
                    Loading.close();
                },

                downloadError: function(response) {
                    $log.error(response);
                    notify.error(response.data);
                    Loading.close();
                },

                statusSort: function(apr) {
                    switch (apr.status) {
                        case "unassigned":
                            return 0;
                        case "active":
                            return 1;

                        case "scheduled":
                            return 2;

                        case "draft":
                            return 3;

                        case "draft_completed":
                            return 4;

                        case "on_hold":
                            return 5;
                        case "canceled":
                            return 6;
                        case "completed":
                            return 7;
                    }
                },

                addQueryArgs: function() {
                    this.addArgs("status");
                    this.addArgs("companyId");
                },

                addArgs: function(arg) {
                    if (this.aprArgs[arg]) {
                        this.queryArgs[arg] = this.aprArgs[arg];
                    }
                    else {
                        delete this.queryArgs[arg];
                    }
                },

                messageModal: function(apr) {
                    AprMessageForm.show({
                        user: this.currentUser,
                        apr: apr
                    });
                },

                editApr: function(apr) {
                    AprEditModal.show({ apr: apr, closed: angular.bind(this, this.aprUpdated, apr) });
                }
            });

            this.model = new Model(this.cfg);
        }
    ]);
