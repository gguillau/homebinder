(function() {
    "use strict";

    angular
        .module("hb.annualPropertyReviews.homePro", [])
        .factory("AprHomeProForm", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "annualPropertyReviews/homeProForm/homeProForm.tpl.html",
                            controller: "AprHomeProFormController as ctrl",
                            backdrop: true,
                            resolveData: opts
                        });
                    }
                };
            }
        ])
        .controller("AprHomeProFormController", [
            "data",
            "hb.resources",
            AprHomeProFormController
        ]);

    function AprHomeProFormController(data, resources) {
        this.apr = data.apr;
        this.pro = data.pro;
        this.resources = resources.aprHomePro;
    }
})();