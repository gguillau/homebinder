describe("AprHomeProFormController", function() {
    var $rootScope,
        $controller,
        resources,
        ctrl,
        $modalInstance,
        AprHomeProForm,
        ModalService;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        resources = _$injector_.get("hb.resources");
        AprHomeProForm = _$injector_.get("AprHomeProForm");
        ModalService = _$injector_.get("ModalService");

        $modalInstance = {
            close: function() {}
        };
    }));

    function createController() {
        ctrl = $controller("AprHomeProFormController", {
            "$modalInstance": $modalInstance,
            "hb.resources": resources,
            "data": {}
        });
        $rootScope.$apply();
    }

    describe('init', function() {
        it("sets the resources", function() {
            createController();
            expect(ctrl.resources.email).toEqual("Email");
        });
    });

    describe('AprHomeProForm', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            AprHomeProForm.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });

});