(function() {
    "use strict";

    angular
        .module("hb.annualPropertyReviews.request", [])
        .directive("hbAnnualPropertyReviewRequest", [
            function() {
                return {
                    restrict: "E",
                    templateUrl: "annualPropertyReviews/request/request.tpl.html",
                    controller: "AprRequestController",
                    controllerAs: "ctrl",
                    bindToController: true,
                    scope: {}
                };
            }
        ])
        .controller("AprRequestController", [
            "$state",
            "$stateParams",
            "$log",
            "$location",
            "hb.api",
            "hb.resources",
            "Address",
            "$anchorScroll",
            "Notify",
            AprRequestController
        ]);

    function AprRequestController($state, $stateParams, $log, $location, api, resources, address, $anchorScroll, Notify) {
        var vm = this;
        vm.resources = resources.aprRequest;
        vm.address = address;
        vm.states = address.getStatesAndProvinces();
        vm.country = address.findCountryByCode();
        vm.selectState = selectState;
        vm.submit = submit;
        vm.cancel = cancel;
        vm.scroll = scroll;
        vm.$location = $location;
        vm.performSubmitRequest = performSubmitRequest; // for testing purposes
        vm.onError = onError;
        vm.applyPromoCode = applyPromoCode;
        vm.promo_code = "";
        vm.applyingCode = false;
        vm.hidePromo = false;
        vm.amount_off = 0;
        vm.sampleLink = "https://www.homebinder.com/apr/c7bdef5083904ff3bbf825b27d0bc422";

        vm.hbLogoUrl = "img/flatlogo.png";
        vm.canPerformApr = true;
        vm.processing = false;
        vm.created = false;

        vm.creditCardConfig = {
            onReceiveToken: performSubmitRequest,
            onError: onError
        };

        vm.skipTokenize = false;

        init();

        function scroll(element) {
            var old = $location.hash();
            $location.hash(element);
            $anchorScroll();
            $location.hash(old);
        }

        function init() {
            if (!$stateParams.partnerId) {
                vm.partner = {
                    aprPrice: 180
                };
                return;
            }
            getPartner();
        }

        function getPartner() {
            vm.processing = true;
            api.partner.get_apr_config($stateParams.partnerId)
                .then(gotPartner, getPartnerError);
        }

        function gotPartner(response) {
            vm.partner = {
                id: response.data.id,
                name: response.data.name,
                logo: response.data.logo_file,
                aprPrice: response.data.annual_property_review_cost / 100,
                canPerformApr: response.data.can_perform_apr,
                email: response.data.email,
                phone: response.data.phone
            };
            vm.processing = false;
            if (!vm.partner.canPerformApr) {
                $state.go("root");
            }
        }

        function getPartnerError(response) {
            Notify.info(vm.resources.partnerError);
            $state.go("root");
        }

        function selectState(state) {
            if (state) {
                vm.state = state;
                vm.country = state.country;
            }
        }

        function submit() {
            vm.processing = true;

            // verify client information is valid
            vm.form.$submitted = true;
            if (vm.form.$invalid) {
                vm.processing = false;
                return;
            }

            // clear any errors
            vm.error = null;

            if (!vm.skipTokenize) {
                // generate a stripe card token first
                vm.creditCardConfig.tokenize();
            }
            else {
                vm.performSubmitRequest(null);
            }
        }

        function onError(error) {
            vm.processing = false;
            Notify.info(error);
        }

        function performSubmitRequest(token) {
            var status, partnerId;

            if (vm.partner && vm.partner.id) {
                partnerId = vm.partner.id;
                status = "active";
            }
            else {
                status = "unassigned";
            }

            var data = {
                annual_property_review: {
                    client_first_name: vm.firstName,
                    client_last_name: vm.lastName,
                    client_email: vm.email,
                    client_phone: vm.phone,
                    address1: vm.address1,
                    address2: vm.address2,
                    city: vm.city,
                    state: vm.state.value,
                    zip: vm.zip,
                    country: vm.country.value,
                    reviewer_company_id: partnerId,
                    client_comments: vm.comments,
                    status: status
                }
            };

            if (token) {
                data.payment = {
                    card: token,
                    cost: vm.partner.aprPrice,
                    amount_off: vm.amount_off
                };
            }

            api.annualPropertyReview.create(data).then(
                onRequestCreated,
                onRequestError
            );
        }

        function onRequestCreated(response) {
            vm.created = true;
            vm.processing = false;
            vm.apr = response.data;
            Notify.success(vm.resources.orderMessage);
        }

        function onRequestError(err) {
            vm.processing = false;
            vm.error = err.data;
            Notify.error(err.data);
        }

        function cancel() {
            $state.go('root');
        }

        function applyPromoCode() {
            vm.applyingCode = true;

            var data = {
                amount: vm.partner.aprPrice * 100,
                promoCode: vm.promo_code
            };

            api.promoCode.verify(data).then(
                promoCodeSuccess,
                promoCodeError);
        }

        function promoCodeSuccess(response) {
            vm.amount_off = response.data;
            vm.hidePromo = true;
            vm.partner.aprPrice = (vm.partner.aprPrice * 100) - response.data;
            vm.partner.aprPrice /= 100;
            Notify.info(vm.resources.promoMessage);
            if (vm.partner.aprPrice < 1) {
                vm.skipTokenize = true;
            }
        }

        function promoCodeError(response) {
            vm.applyingCode = false;
            Notify.error(response.data);
        }

    }
})();
