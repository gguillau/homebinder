describe("AprRequestController", function() {
    var ctrl,
        controller,
        $scope,
        $q,
        $log,
        api,
        resources,
        notify,
        $state,
        $location,
        $stateParams;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector, $rootScope) {
        controller = $controller;
        $scope = $rootScope;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        $state = $injector.get("$state");
        resources = $injector.get("hb.resources");
        $location = $injector.get("$location");
        $stateParams = $injector.get("$stateParams");
    }));

    function createController(data) {
        ctrl = controller("AprRequestController", {
            "$log": $log,
            "hb.api": api,
            "hb.resources": resources,
            "Notify": notify,
            "$state": $state,
            "$location": $location,
            "$stateParams": $stateParams
        });
    }

    describe("scroll", function() {
        it("calls $location hash", function() {
            spyOn($location, "hash");
            createController();
            ctrl.scroll("#order");
            expect($location.hash).toHaveBeenCalled();
        });
    });

    describe("init", function() {
        it("gets the partner", function() {
            var partner = {
                id: 1,
                name: "Partner",
                logo_file: "logo",
                annual_property_review_cost: 19900,
                can_perform_apr: true,
                email: "partner@gmail.com",
                phone: "phone"
            };
            spyOn(api.partner, "get_apr_config").and.returnValue($q.when({ data: partner }));
            $stateParams.partnerId = 1;

            createController();
            $scope.$apply();

            expect(api.partner.get_apr_config).toHaveBeenCalled();
            expect(ctrl.partner.id).toEqual(1);
        });

        it("sends user to root", function() {
            var partner = {
                id: 1,
                name: "Partner",
                logo_file: "logo",
                annual_property_review_cost: 19900,
                can_perform_apr: false,
                email: "partner@gmail.com",
                phone: "phone"
            };
            spyOn($state, "go");
            spyOn(api.partner, "get_apr_config").and.returnValue($q.when({ data: partner }));
            $stateParams.partnerId = 1;

            createController();
            $scope.$apply();

            expect(api.partner.get_apr_config).toHaveBeenCalled();
            expect(ctrl.partner.id).toEqual(1);
            expect($state.go).toHaveBeenCalled();
        });

        it("returns an error", function() {
            spyOn(notify, "info");
            spyOn($state, "go");
            spyOn(api.partner, "get_apr_config").and.returnValue($q.reject({ data: "error" }));
            $stateParams.partnerId = 1;

            createController();
            $scope.$apply();

            expect(api.partner.get_apr_config).toHaveBeenCalled();
            expect(notify.info).toHaveBeenCalled();
            expect($state.go).toHaveBeenCalled();
        });
    });

    describe("submit", function() {
        it("submits the form", function() {
            var partner = {
                id: 1,
                name: "Partner",
                logo_file: "logo",
                annual_property_review_cost: 19900,
                can_perform_apr: true,
                email: "partner@gmail.com",
                phone: "phone"
            };

            spyOn(api.partner, "get_apr_config").and.returnValue($q.when({ data: partner }));
            $stateParams.partnerId = 1;

            createController();
            $scope.$apply();

            ctrl.creditCardConfig = { tokenize: function() {} };
            spyOn(ctrl.creditCardConfig, "tokenize").and.returnValue($q.when("token"));

            ctrl.form = { $submitted: true };
            ctrl.submit();
            $scope.$apply();

            expect(ctrl.creditCardConfig.tokenize).toHaveBeenCalled();
        });

        it("does not submit the form", function() {
            createController();

            ctrl.creditCardConfig = { tokenize: function() {} };
            spyOn(ctrl.creditCardConfig, "tokenize").and.returnValue($q.when("token"));
            spyOn(ctrl, "performSubmitRequest");

            ctrl.form = { $submitted: true, $invalid: true };
            ctrl.submit();

            expect(ctrl.creditCardConfig.tokenize).not.toHaveBeenCalled();
            expect(ctrl.performSubmitRequest).not.toHaveBeenCalled();
        });

        it("calls performSubmitRequest", function() {
            createController();

            ctrl.creditCardConfig = { tokenize: function() {} };
            spyOn(ctrl.creditCardConfig, "tokenize").and.returnValue($q.when("token"));
            spyOn(ctrl, "performSubmitRequest");

            ctrl.form = { $submitted: true, $invalid: false };
            ctrl.skipTokenize = true;
            ctrl.submit();

            expect(ctrl.creditCardConfig.tokenize).not.toHaveBeenCalled();
            expect(ctrl.performSubmitRequest).toHaveBeenCalled();
        });
    });

    describe("performSubmitRequest", function() {
        it("performs the submit", function() {
            var partner = {
                id: 1,
                name: "Partner",
                logo_file: "logo",
                annual_property_review_cost: 19900,
                can_perform_apr: true,
                email: "partner@gmail.com",
                phone: "phone"
            };

            spyOn(api.partner, "get_apr_config").and.returnValue($q.when({ data: partner }));
            spyOn(api.annualPropertyReview, "create").and.returnValue($q.when({ data: {} }));
            $stateParams.partnerId = 1;

            createController();
            $scope.$apply();
            ctrl.state = { value: "MA" };
            ctrl.country = { value: "MA" };
            ctrl.performSubmitRequest("token");
            $scope.$apply();

            expect(api.annualPropertyReview.create).toHaveBeenCalled();
            expect(ctrl.created).toBe(true);
        });
        
        it("performs the submit", function() {
            var partner = {
                id: 1,
                name: "Partner",
                logo_file: "logo",
                annual_property_review_cost: 19900,
                can_perform_apr: true,
                email: "partner@gmail.com",
                phone: "phone"
            };

            spyOn(api.partner, "get_apr_config").and.returnValue($q.when({ data: partner }));
            spyOn(api.annualPropertyReview, "create").and.returnValue($q.when({ data: {} }));
            $stateParams.partnerId = null;

            createController();
            $scope.$apply();
            ctrl.state = { value: "MA" };
            ctrl.country = { value: "MA" };
            ctrl.performSubmitRequest("token");
            $scope.$apply();

            expect(api.annualPropertyReview.create).toHaveBeenCalled();
            expect(ctrl.created).toBe(true);
        });

        it("performs the submit and returns an error", function() {
            var partner = {
                id: 1,
                name: "Partner",
                logo_file: "logo",
                annual_property_review_cost: 19900,
                can_perform_apr: true,
                email: "partner@gmail.com",
                phone: "phone"
            };

            spyOn(notify, "error");
            spyOn(api.partner, "get_apr_config").and.returnValue($q.when({ data: partner }));
            spyOn(api.annualPropertyReview, "create").and.returnValue($q.reject({ data: "error" }));
            $stateParams.partnerId = 1;

            createController();
            $scope.$apply();
            ctrl.state = { value: "MA" };
            ctrl.country = { value: "MA" };
            ctrl.performSubmitRequest("token");
            $scope.$apply();

            expect(api.annualPropertyReview.create).toHaveBeenCalled();
            expect(ctrl.created).toBe(false);
            expect(ctrl.processing).toBe(false);
            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("cancel", function() {
        it("sends user to root", function() {
            spyOn($state, "go");
            createController();
            ctrl.cancel();
            expect($state.go).toHaveBeenCalledWith("root");
        });
    });

    describe("applyPromoCode", function() {
        it("verifies promo code", function() {
            var partner = {
                id: 1,
                name: "Partner",
                logo_file: "logo",
                annual_property_review_cost: 14500,
                can_perform_apr: true,
                email: "partner@gmail.com",
                phone: "phone"
            };

            spyOn(api.partner, "get_apr_config").and.returnValue($q.when({ data: partner }));
            spyOn(api.promoCode, "verify").and.returnValue($q.when({ data: 14500 }));
            $stateParams.partnerId = 1;

            createController();
            $scope.$apply();
            ctrl.applyPromoCode();
            $scope.$apply();

            expect(api.promoCode.verify).toHaveBeenCalled();
            expect(ctrl.hidePromo).toBe(true);
            expect(ctrl.skipTokenize).toBe(true);
            expect(ctrl.partner.aprPrice).toEqual(0);
        });

        it("verifies promo code", function() {
            var partner = {
                id: 1,
                name: "Partner",
                logo_file: "logo",
                annual_property_review_cost: 14500,
                can_perform_apr: true,
                email: "partner@gmail.com",
                phone: "phone"
            };

            spyOn(api.partner, "get_apr_config").and.returnValue($q.when({ data: partner }));
            spyOn(api.promoCode, "verify").and.returnValue($q.when({ data: 1000 }));
            $stateParams.partnerId = 1;

            createController();
            $scope.$apply();
            ctrl.applyPromoCode();
            $scope.$apply();

            expect(api.promoCode.verify).toHaveBeenCalled();
            expect(ctrl.hidePromo).toBe(true);
            expect(ctrl.skipTokenize).toBe(false);
            expect(ctrl.partner.aprPrice).toEqual(135);
        });

        it("returns an error", function() {
            var partner = {
                id: 1,
                name: "Partner",
                logo_file: "logo",
                annual_property_review_cost: 14500,
                can_perform_apr: true,
                email: "partner@gmail.com",
                phone: "phone"
            };

            spyOn(api.partner, "get_apr_config").and.returnValue($q.when({ data: partner }));
            spyOn(api.promoCode, "verify").and.returnValue($q.reject({ data: "error" }));
            $stateParams.partnerId = 1;

            createController();
            $scope.$apply();
            ctrl.applyPromoCode();
            $scope.$apply();

            expect(api.promoCode.verify).toHaveBeenCalled();
            expect(ctrl.hidePromo).toBe(false);
            expect(ctrl.skipTokenize).toBe(false);
            expect(ctrl.partner.aprPrice).toEqual(145);
            expect(ctrl.applyingCode).toBe(false);
        });

    });

    describe("selectState", function() {
        it("sets the state", function() {
            createController();
            ctrl.selectState({ value: "MA", country: { value: "US" } });

            expect(ctrl.country.value).toEqual("US");
        });
    });

    describe("onError", function() {
        it("sets processing to false", function() {
            spyOn(notify, "error");

            createController();
            ctrl.processing = true;
            ctrl.onError({ data: "error" });

            expect(ctrl.processing).toBe(false);
        });
    });
});

var Stripe;

describe('hbAnnualPropertyReviewRequest', function() {
    var $scope, $compile, element, $q, ServiceKeyList, dfd;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        $q = _$injector_.get("$q");
        ServiceKeyList = _$injector_.get("ServiceKeyList");

        Stripe = function() {
            return {
                elements: function() {
                    return {
                        create: function() {
                            return {
                                mount: function() {}
                            };
                        }
                    };
                }
            };
        };

        spyOn(ServiceKeyList, "keys").and.returnValue($q.when({ data: { stripe: "ADOADKADK" } }));

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = {};

        element = $compile('<hb-annual-property-review-request></hb-annual-property-review-request>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("Annual Property Review");
        });
    });
});