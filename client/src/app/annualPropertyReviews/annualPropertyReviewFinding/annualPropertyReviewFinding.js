angular
    .module("hb.annualPropertyReviews.annualPropertyReviewFinding", [
        "hb.annualPropertyReviews.annualPropertyReviewFinding.options"
    ])
    .factory("AprFindingModal", [
        "ModalService",
        function(modals) {
            return {
                show: function(args) {
                    return modals.show({
                        aside: false,
                        templateUrl: "annualPropertyReviews/annualPropertyReviewFinding/annualPropertyReviewFinding.tpl.html",
                        controller: "AprFindingController as ctrl",
                        resolveData: args
                    });
                }
            };
        }
    ])
    .controller("AprFindingController", [
        "$log",
        "hb.api",
        "Session",
        "hb.resources",
        "$modalInstance",
        "data",
        "ModalService",
        "Notify",
        function($log, api, session, resources, $modalInstance, args, modals, notify) {
            var vm = this;
            vm.apr = args.apr;
            vm.finding = args.finding;
            vm.resources = resources.aprFinding;
            vm.estimates = [
                { name: vm.resources.estimateRangeUnknown, value: "unknown" },
                { name: vm.resources.estimateRangeTbd, value: "tbd" },
                { name: vm.resources.estimateRangeLessHun, value: "less_hun" },
                { name: vm.resources.estimateRangeOneHuntoTwoFifty, value: "one_hun_to_two_fity" },
                { name: vm.resources.estimateRangeTwoFiftyToFiveHun, value: "two_fifty_to_five_hun" },
                { name: vm.resources.estimateRangeFiveHunToOneThou, value: "five_hun_to_one_thou" },
                { name: vm.resources.estimateRangeOneThouToTwentyHun, value: "one_thou_to_twenty_hun" },
                { name: vm.resources.estimateRangeTwentyHunToFiveThou, value: "twenty_hun_to_five_thou" },
                { name: vm.resources.estimateRangeFiveThouPlus, value: "five_thou_plus" }
            ];
            vm.priorities = [
                { name: resources.aprFinding.tbd, value: "tbd" },
                { name: resources.aprFinding.critical, value: "critical" },
                { name: resources.aprFinding.high, value: "high" },
                { name: resources.aprFinding.low, value: "low" },
                { name: resources.aprFinding.cosmetic, value: "cosmetic" }
            ];
            vm.statuses = [
                { name: resources.aprFinding.identified, value: "identified" },
                { name: resources.aprFinding.dismissed, value: "dismissed" },
                { name: resources.aprFinding.bidRequested, value: "bid_requested" }
            ];
            vm.priority = vm.priorities[0].value;
            vm.status = vm.statuses[0].value;
            vm.estimate = vm.estimates[0].value;
            vm.submit = submit;
            vm.cancel = cancel;
            vm.error = error;
            vm.deleteConfirmed = deleteConfirmed;
            vm.deletePhoto = deletePhoto;
            vm.recommendedHomeProSelected = recommendedHomeProSelected;
            vm.selectedHomeProSelected = selectedHomeProSelected;
            vm.imgUploader = {
                fileTypes: "image",
                hideUploadButton: true,
                showTable: false,
                selectButtonLabel: resources.aprFinding.addPhoto,
                fileLimit: false,
                url: "/api/v1/annual_property_review_photos",
                multiSelect: true,
                id: "uploadImages",
                hide_upload: false,
                jwt: session.getJwt()
            };

            vm.recommendedHomeProLookupCfg = {
                partnerId: vm.apr.reviewer_company.id,
                homeProSelected: recommendedHomeProSelected
            };

            vm.selectedHomeProLookupCfg = {
                homeProSelected: selectedHomeProSelected
            };

            populate();

            function populate() {
                if (!vm.finding) {
                    return;
                }

                vm.name = vm.finding.name;
                vm.priority = vm.finding.priority;
                vm.estimate = vm.finding.estimated_cost;
                vm.details = vm.finding.details;
                vm.recommendedHomePro = vm.finding.recommended_homepro;
                vm.selectedHomePro = vm.finding.selected_homepro;
                vm.status = vm.finding.status;
                vm.homeowner_item = vm.finding.homeowner_item;
            }

            function recommendedHomeProSelected(homePro) {
                vm.recommendedHomePro = homePro;
            }

            function selectedHomeProSelected(homePro) {
                vm.selectedHomePro = homePro;
            }

            function submit() {
                vm.form.$submitted = true;
                if (vm.form.$invalid) {
                    return;
                }

                var data = {
                    annual_property_review_id: vm.apr.id,
                    name: vm.name,
                    priority: vm.priority,
                    estimated_cost: vm.estimate,
                    details: vm.details,
                    recommended_homepro_id: vm.recommendedHomePro ? vm.recommendedHomePro.id : null,
                    selected_homepro_id: vm.selectedHomePro ? vm.selectedHomePro.id : null,
                    status: vm.status,
                    homeowner_item: vm.homeowner_item
                };

                if (vm.finding) {
                    api.annualPropertyReviewFinding.update(vm.finding.id, { annual_property_review_finding: data }).then(saved, error);
                }
                else {
                    api.annualPropertyReviewFinding.create({ annual_property_review_finding: data }).then(saved, error);
                }
            }

            function saved(response) {
                vm.finding = response.data;
                $modalInstance.close(vm.finding);
            }

            function error(response) {
                notify.error(response.data);
            }

            function cancel() {
                $modalInstance.dismiss("cancel");
            }

            function deleteConfirmed(args) {
                args.deleteService().then(function() {
                    var k, len;
                    for (k = 0, len = args.source.length; k < len; k++) {
                        if (args.source[k].id == args.item.id) {
                            args.source.splice(k, 1);
                            return;
                        }
                    }
                });
            }

            function deletePhoto(photo) {
                modals.confirm({
                    message: vm.resources.deletePhoto,
                    minorMessage: vm.resources.deleteMinor,
                    confirm: angular.bind(vm, deleteConfirmed, {
                        deleteService: angular.bind(api.annualPropertyReviewPhoto, api.annualPropertyReviewPhoto.destroy, photo.id),
                        item: photo,
                        source: vm.finding.annual_property_review_photos
                    })
                });
            }
        }
    ]);
