describe("annualPropertyReviewFinding", function() {
    var ctrl,
        controller,
        $scope,
        $q,
        $log,
        api,
        session,
        resources,
        $modalInstance,
        pendingUploads,
        notify,
        AprFindingModal,
        ModalService;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector, $rootScope) {
        controller = $controller;
        $scope = $rootScope;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        session = $injector.get("Session");
        resources = $injector.get("hb.resources");
        AprFindingModal = $injector.get("AprFindingModal");
        ModalService = $injector.get("ModalService");
        notify = $injector.get("Notify");

        spyOn(session, "getJwt").and.returnValue("jwt");
    }));

    $modalInstance = {
        close: function() {},
        dismiss: function() {}
    };

    function createController(data) {
        ctrl = controller("AprFindingController", {
            "$log": $log,
            "hb.api": api,
            "Session": session,
            "hb.resources": resources,
            "$modalInstance": $modalInstance,
            "data": data
        });
    }

    describe("populate", function() {
        it("does nothing when no finding is passed", function() {
            createController({ apr: { id: 1, reviewer_company: { id: 1 } }, finding: undefined });
            expect(ctrl.priority).toEqual(ctrl.priorities[0].value);
            expect(ctrl.estimate).toEqual("unknown");
            expect(ctrl.status).toEqual(ctrl.statuses[0].value);
        });

        it("populates when a finding is passed", function() {
            createController({
                apr: {
                    id: 1,
                    reviewer_company: {
                        id: 1
                    }
                },
                finding: {
                    id: 1,
                    name: "finding 1",
                    priority: "critical",
                    estimated_cost: 500,
                    details: "hello",
                    recommended_homepro: {
                        id: 1,
                        displayName: "Mr Plumber",
                        email: "plumber@email.com"
                    },
                    selected_homepro: {
                        id: 2,
                        displayName: "Mrs Plumber",
                        email: "mplumber@email.com"
                    },
                    status: "dismissed"
                }
            });
            expect(ctrl.name).toEqual("finding 1");
            expect(ctrl.priority).toEqual(ctrl.priorities[1].value);
            expect(ctrl.estimate).toEqual(500);
            expect(ctrl.details).toEqual("hello");
            expect(ctrl.recommendedHomePro).toEqual(jasmine.objectContaining({
                id: 1,
                displayName: "Mr Plumber",
                email: "plumber@email.com"
            }));
            expect(ctrl.selectedHomePro).toEqual(jasmine.objectContaining({
                id: 2,
                displayName: "Mrs Plumber",
                email: "mplumber@email.com"
            }));
            expect(ctrl.status).toEqual(ctrl.statuses[1].value);
        });
    });

    describe("submit", function() {
        it("does not submit when for is invalid", function() {
            spyOn(api.annualPropertyReviewFinding, "create").and.returnValue($q.when({}));
            createController({ apr: { id: 1, reviewer_company: { id: 1 } }, finding: undefined });
            ctrl.form = {
                $invalid: true
            };
            ctrl.submit();
            expect(ctrl.form.$submitted).toEqual(true);
            expect(api.annualPropertyReviewFinding.create).not.toHaveBeenCalled();
        });

        it("creates a new finding", function() {
            spyOn(api.annualPropertyReviewFinding, "create").and.returnValue($q.when({ data: { id: 1 } }));
            spyOn($modalInstance, "close");
            createController({ apr: { id: 1, reviewer_company: { id: 1 } }, finding: undefined });
            ctrl.name = "finding";
            ctrl.priority = ctrl.priorities[1].value;
            ctrl.estimate = 500;
            ctrl.details = "hello";
            ctrl.recommendedHomePro = {
                id: 1,
                displayName: "Mr Plumber",
                email: "plumber@email.com"
            };
            ctrl.selectedHomePro = {
                id: 2,
                displayName: "Mrs Plumber",
                email: "mplumber@email.com"
            };
            ctrl.status = ctrl.statuses[1].value;
            pendingUploads = [];
            ctrl.form = {
                $invalid: false
            };
            ctrl.submit();
            $scope.$apply();
            expect(api.annualPropertyReviewFinding.create).toHaveBeenCalledWith(jasmine.objectContaining({
                annual_property_review_finding: {
                    annual_property_review_id: 1,
                    name: "finding",
                    priority: ctrl.priorities[1].value,
                    estimated_cost: 500,
                    details: "hello",
                    recommended_homepro_id: 1,
                    selected_homepro_id: 2,
                    status: ctrl.statuses[1].value,
                    homeowner_item: undefined
                }
            }));
            expect($modalInstance.close).toHaveBeenCalled();
        });

        it("updates an existing finding", function() {
            spyOn(api.annualPropertyReviewFinding, "update").and.returnValue($q.when({ data: { id: 1 } }));
            spyOn($modalInstance, "close");
            createController({
                apr: { id: 1, reviewer_company: { id: 1 } },
                finding: {
                    id: 1,
                    name: "finding",
                    priority: "critical",
                    estimated_cost: 500,
                    details: "hello",
                    recommended_homepro: {
                        id: 1,
                        displayName: "Mr Plumber",
                        email: "plumber@email.com"
                    },
                    selected_homepro: {
                        id: 2,
                        displayName: "Mrs Plumber",
                        email: "mplumber@email.com"
                    },
                    status: "dismissed"
                }
            });
            pendingUploads = [];
            ctrl.form = {
                $invalid: false
            };
            ctrl.submit();
            $scope.$apply();
            expect(api.annualPropertyReviewFinding.update).toHaveBeenCalledWith(1, jasmine.objectContaining({
                annual_property_review_finding: {
                    annual_property_review_id: 1,
                    name: "finding",
                    priority: ctrl.priorities[1].value,
                    estimated_cost: 500,
                    details: "hello",
                    recommended_homepro_id: 1,
                    selected_homepro_id: 2,
                    status: ctrl.statuses[1].value,
                    homeowner_item: undefined
                }
            }));
            expect($modalInstance.close).toHaveBeenCalled();
        });
    });

    describe("cancel", function() {
        it("dismisses the dialog", function() {
            spyOn($modalInstance, "dismiss");
            createController({ apr: { id: 1, reviewer_company: { id: 1 } }, finding: undefined });
            ctrl.cancel();
            expect($modalInstance.dismiss).toHaveBeenCalled();
        });
    });

    describe("recommendedHomeProSelected", function() {
        it("sets the recommended_homepro", function() {
            spyOn($modalInstance, "dismiss");
            createController({ apr: { id: 1, reviewer_company: { id: 1 } }, finding: undefined });

            ctrl.recommendedHomeProSelected({ id: 1, name: "home pro" });

            expect(ctrl.recommendedHomePro).toEqual({ id: 1, name: "home pro" });
        });
    });

    describe("selectedHomeProSelected", function() {
        it("sets the selectedHomePro", function() {
            spyOn($modalInstance, "dismiss");
            createController({ apr: { id: 1, reviewer_company: { id: 1 } }, finding: undefined });

            ctrl.selectedHomeProSelected({ id: 1, name: "home pro" });

            expect(ctrl.selectedHomePro).toEqual({ id: 1, name: "home pro" });
        });
    });

    describe("error", function() {
        it("calls notify error", function() {
            spyOn(notify, "error");
            createController({ apr: { id: 1, reviewer_company: { id: 1 } }, finding: undefined });
            ctrl.error({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("deleteConfirmed", function() {
        it("calls the delete service and removes the item from the array", function() {
            spyOn(notify, "error");
            createController({ apr: { id: 1, reviewer_company: { id: 1 } }, finding: undefined });
            spyOn(api.annualPropertyReviewPhoto, "destroy").and.returnValue($q.when({ data: {} }));

            var deleteService = api.annualPropertyReviewPhoto.destroy;
            var item = { id: 1 };
            var array = [item];
            expect(array.length).toEqual(1);
            ctrl.deleteConfirmed({ source: array, item: item, deleteService: deleteService });
            $scope.$apply();

            expect(array.length).toEqual(0);
        });
    });

    describe("deletePhoto", function() {
        it("calls the modal confirm", function() {
            spyOn(ModalService, "confirm");
            createController({ apr: { id: 1, reviewer_company: { id: 1 } }, finding: undefined });
            var photo = { id: 1 };
            ctrl.finding = {
                annual_property_review_photos: [photo]
            };
            ctrl.deletePhoto(photo);

            expect(ModalService.confirm).toHaveBeenCalled();
        });
    });

    describe('AprFindingModal', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            AprFindingModal.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});
