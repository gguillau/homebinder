(function() {
    "use strict";

    angular
        .module("hb.annualPropertyReviews.annualPropertyReviewFinding.options", [])
        .factory("AprFindingOptions", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "annualPropertyReviews/annualPropertyReviewFinding/options/options.tpl.html",
                            controller: "AprFindingOptionsController as ctrl",
                            backdrop: "static",
                            keyboard: false,
                            resolveData: opts,
                            closed: opts.closed
                        });
                    }
                };
            }
        ])
        .controller("AprFindingOptionsController", [
            "$modalInstance",
            "hb.api",
            "Notify",
            "$log",
            "Loading",
            "$state",
            "$stateParams",
            "data",
            "Session",
            "$timeout",
            AprFindingOptionsController
        ]);

    function AprFindingOptionsController($modal, api, notify, $log, loading, $state, $stateParams, opts, session, $timeout) {
        this.$modal = $modal;
        this.api = api;
        this.notify = notify;
        this.$log = $log;
        this.loading = loading;
        this.$state = $state;
        this.aprId = $stateParams.aprId;
        this.$timeout = $timeout;
        this.closed = opts.closed;
        this.findingSaved = opts.findingSaved;
        this.id = "uploadImages" + new Date().getTime();
        this.uploader = {
            fileTypes: "image",
            hideUploadButton: true,
            showTable: false,
            selectButtonLabel: "Add Photo",
            fileLimit: false,
            url: "/api/v1/annual_property_review_photos",
            multiSelect: false,
            id: this.id,
            hide_upload: true,
            jwt: session.getJwt()
        };
    }

    AprFindingOptionsController.prototype = {

        addPhoto: function() {
            this.uploader.api.click();
        },

        uploadImage: function() {
            var uploadApi = this.uploader.api;
            if (uploadApi && uploadApi.pendingUploads().length > 0) {
                this.newFinding();
            }
        },

        newFinding: function() {
            this.loading.show("Creating finding...");
            var data = {
                annual_property_review_id: this.aprId,
                name: "Untitled"
            };

            this.api.annualPropertyReviewFinding.create({ annual_property_review_finding: data }).then(
                angular.bind(this, this.createSuccess),
                angular.bind(this, this.createError));
        },

        createSuccess: function(response) {
            this.finding = response.data;
            this.createImage();
        },

        createError: function(response) {
            this.notify.error(response.data);
            this.loading.close();
        },

        createImage: function() {
            var uploadApi = this.uploader.api;
            if (uploadApi && uploadApi.pendingUploads().length > 0) {

                uploadApi.updateParams({
                    annual_property_review_id: this.aprId,
                    annual_property_review_finding_id: this.finding.id
                });

                uploadApi.uploadFiles();
            }
        },

        uploadSuccess: function(response) {
            this.finding.annual_property_review_photos.push(response);
            this.findingSaved(this.finding);
            this.$modal.dismiss("cancel");
            this.loading.close();
        },

        uploadError: function(response) {
            this.notify.error(response.data);
            this.findingSaved(this.finding);
            this.$modal.dismiss("cancel");
            this.loading.close();
        },

        createFinding: function() {
            this.$modal.close();
        },

        cancel: function() {
            this.$modal.dismiss("cancel");
        }

    };
})();