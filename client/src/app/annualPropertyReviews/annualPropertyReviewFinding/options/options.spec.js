describe("AprFindingOptionsController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $modalInstance,
        api,
        data,
        notify,
        $log,
        AprFindingOptions,
        ModalService;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        notify = _$injector_.get("Notify");
        $log = _$injector_.get("$log");
        AprFindingOptions = _$injector_.get("AprFindingOptions");
        ModalService = _$injector_.get("ModalService");

        $modalInstance = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };

        data = {};

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("AprFindingOptionsController", {
            "hb.api": api,
            "$modalInstance": $modalInstance,
            "data": data,
            "Notify": notify,
            "$log": $log
        });

        $scope.ctrl = ctrl;
    }

    describe("ctrl.addPhoto", function() {
        it("calls click function", function() {
            spyOn($modalInstance, "dismiss");

            createController();
            ctrl.uploader = {
                api: {
                    click: function() {}
                }
            };
            spyOn(ctrl.uploader.api, "click");
            ctrl.addPhoto();

            expect(ctrl.uploader.api.click).toHaveBeenCalled();
        });

    });

    describe("ctrl.uploadImage", function() {
        it("calls newFinding function", function() {
            spyOn($modalInstance, "dismiss");

            createController();
            ctrl.uploader = {
                api: {
                    pendingUploads: function() {}
                }
            };
            spyOn(ctrl.uploader.api, "pendingUploads").and.returnValue([{ id: 1 }]);
            spyOn(ctrl, "newFinding");
            ctrl.uploadImage();

            expect(ctrl.newFinding).toHaveBeenCalled();
        });

    });

    describe("ctrl.newFinding", function() {
        it("calls api.annualPropertyReviewFinding.create", function() {
            spyOn(api.annualPropertyReviewFinding, "create").and.returnValue($q.when({ data: {} }));

            createController();
            ctrl.newFinding();

            expect(api.annualPropertyReviewFinding.create).toHaveBeenCalled();
        });
    });

    describe("ctrl.createSuccess", function() {
        it("calls createImage", function() {
            createController();
            spyOn(ctrl, "createImage");
            ctrl.createSuccess({ data: { id: 1 } });

            expect(ctrl.createImage).toHaveBeenCalled();
        });

    });

    describe("ctrl.createError", function() {
        it("calls notify error", function() {
            createController();
            spyOn(ctrl.notify, "error");
            ctrl.createError({ data: "error" });

            expect(ctrl.notify.error).toHaveBeenCalled();
        });

    });

    describe("ctrl.createImage", function() {
        it("calls uploadFiles function", function() {
            spyOn($modalInstance, "dismiss");

            createController();
            ctrl.uploader = {
                api: {
                    pendingUploads: function() {},
                    updateParams: function() {},
                    uploadFiles: function() {}
                }
            };
            spyOn(ctrl.uploader.api, "pendingUploads").and.returnValue([{ id: 1 }]);
            spyOn(ctrl.uploader.api, "uploadFiles").and.returnValue([{ id: 1 }]);

            ctrl.finding = { id: 1 };
            ctrl.createImage();

            expect(ctrl.uploader.api.uploadFiles).toHaveBeenCalled();
        });
    });

    describe("ctrl.uploadSuccess", function() {
        it("adds the image", function() {
            createController();
            ctrl.finding = {
                id: 1,
                annual_property_review_photos: []
            };
            ctrl.findingSaved = function() {};
            ctrl.uploadSuccess({ data: { id: 1 } });

            expect(ctrl.finding.annual_property_review_photos.length).toEqual(1);
        });

    });

    describe("ctrl.uploadError", function() {
        it("calls notify error", function() {
            createController();
            ctrl.findingSaved = function() {};
            spyOn(ctrl.notify, "error");
            ctrl.uploadError({ data: "error" });

            expect(ctrl.notify.error).toHaveBeenCalled();
        });

    });

    describe("ctrl.createFinding", function() {
        it("should call $modalInstance close function", function() {
            spyOn($modalInstance, "close");

            createController();
            $rootScope.$apply();
            ctrl.createFinding();
            $rootScope.$apply();

            expect($modalInstance.close).toHaveBeenCalled();
        });

    });

    describe("ctrl.cancel", function() {
        it("should call $modalInstance dismiss function", function() {
            spyOn($modalInstance, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.cancel();
            $rootScope.$apply();

            expect($modalInstance.dismiss).toHaveBeenCalled();
        });

    });

    describe('AprFindingOptions', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            AprFindingOptions.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});