angular.module("hb.annualPropertyReviews.routes", [])
    .config(['$stateProvider', function($stateProvider) {
        $stateProvider
            .state("aprLanding", {
                url: "^/apr",
                template: "<hb-annual-property-review-request></hb-annual-property-review-request>"
            })
            .state("annualPropertyReview", {
                url: "^/apr/:aprId",
                template: "<hb-annual-property-review></hb-annual-property-review>"
            });
    }]);