angular
    .module("hb.annualPropertyReviews.edit", [])
    .factory("AprEditModal", [
        "ModalService",
        function(modals) {
            return {
                show: function(args) {
                    return modals.show({
                        aside: false,
                        templateUrl: "annualPropertyReviews/edit/edit.tpl.html",
                        controller: "AprEditModalController as ctrl",
                        resolveData: args.apr,
                        closed: args.closed
                    });
                }
            };
        }
    ])
    .controller("AprEditModalController", [
        "$log",
        "hb.api",
        "hb.resources",
        "$modalInstance",
        "data",
        "hb.utils",
        "Notify",
        "Address",
        "Session",
        "$http",
        "constants",
        function($log, api, resources, $modalInstance, apr, utils, notify, Address, Session, $http, constants) {
            var vm = this;
            vm.apr = angular.copy(apr);
            vm.submit = submit;
            vm.cancel = cancel;
            vm.populate = populate;
            vm.country = Address.findCountryByCode(vm.apr.country);
            vm.states = Address.getStatesAndProvinces().filter(angular.bind(vm, function(state) {
                return state.country.value === vm.country.value;
            }));
            vm.setReviewerCompany = setReviewerCompany;
            vm.binderLookup = binderLookup;
            vm.partnerLookup = partnerLookup;
            vm.onSelect = onSelect;
            vm.createBinderSuccess = createBinderSuccess;
            vm.createBinderError = createBinderError;
            vm.resources = resources.aprRequest;
            vm.address = Address;
            vm.onSelectCountry = onSelectCountry;
            vm.countries = vm.address.countries();
            vm.states = vm.address.getStatesAndProvinces();
            vm.processing = false;
            vm.createBinder = createBinder;
            vm.user = Session.getUser();
            vm.binderLookupCfg = {
                allowClear: true,
                placeholder: "Search for binder",
                refresh: binderLookup,
                itemTemplate: "components/lookup/partner/partnerDropDownItem.tpl.html",
                selectionTemplate: "components/lookup/partner/partnerSelection.tpl.html"
            };

            vm.partnerLookupCfg = {
                allowClear: true,
                placeholder: "Search for partner",
                refresh: partnerLookup,
                itemTemplate: "components/lookup/partner/partnerDropDownItem.tpl.html",
                selectionTemplate: "components/lookup/partner/partnerSelection.tpl.html"
            };

            populate();

            function populate() {
                if (vm.apr.reviewer_company_id) {
                    api.partner.get(vm.apr.reviewer_company_id).then(setReviewerCompany);
                }
            }

            function setReviewerCompany(response) {
                vm.apr.reviewer_company = response.data;
            }

            function binderLookup(value) {
                var opts = {
                    page: 1,
                    count: 50,
                    search: value,
                    searchMethod: "for_admin"
                };
                return api.binder.all(opts).then(
                    angular.bind(this, function(response) {
                        return {
                            data: response.data.items
                        };
                    })
                );
            }

            function partnerLookup(value) {
                var opts = {
                    page: 1,
                    count: 50,
                    search: value
                };
                return api.partner.all(opts).then(
                    angular.bind(this, function(response) {
                        return {
                            data: response.data.items
                        };
                    })
                );
            }

            function submit() {
                vm.form.$submitted = true;
                
                if (vm.form.$invalid) {
                    return;
                }
                vm.processing = true;

                var data = {
                    client_first_name: vm.apr.client_first_name,
                    client_last_name: vm.apr.client_last_name,
                    client_email: vm.apr.client_email,
                    client_phone: "+" + vm.country.code + vm.apr.client_phone.national,
                    address1: vm.apr.address1,
                    address2: vm.apr.address2,
                    state: vm.apr.state,
                    city: vm.apr.city,
                    zip: vm.apr.zip,
                    country: vm.apr.country
                };

                if (vm.apr.binder) {
                    data.binder_id = vm.apr.binder.id;
                }

                if (vm.apr.status === "unassigned" && vm.apr.reviewer_company) {
                    data.reviewer_company_id = vm.apr.reviewer_company.id;
                    data.status = "active";
                }
                else if (vm.apr.reviewer_company) {
                    data.reviewer_company_id = vm.apr.reviewer_company.id;
                }

                api.annualPropertyReview.update(apr.id, {
                    annual_property_review: data
                }).then(
                    onSave,
                    onError
                );
            }

            function onSave(response) {
                $modalInstance.close(response);
            }

            function onError(response) {
                notify.error(response.data);
                vm.processing = false;
            }

            function cancel() {
                $modalInstance.dismiss();
            }

            function onSelect(item) {
                if (item) {
                    vm.country = item.country;
                    vm.apr.country = item.country.value;
                }
            }

            function createBinder() {
                vm.processing = true;
                var params = {
                    method: "manual",
                    binder_template_id: vm.apr.reviewer_company.configuration.default_binder_template_id,
                    client: {
                        email: vm.apr.client_email,
                        first: vm.apr.client_first_name,
                        last: vm.apr.client_last_name,
                        phone: vm.apr.client_email
                    },
                    property: {
                        address: vm.apr.address1,
                        address2: vm.apr.address2,
                        city: vm.apr.city,
                        state: vm.apr.state,
                        zip: vm.apr.zip,
                        country: vm.apr.country
                    }
                };

                api.partner.abs(vm.apr.reviewer_company.partner_key, params, vm.apr.reviewer_company.api_key).then(
                    createBinderSuccess,
                    createBinderError
                );
            }

            function createBinderSuccess(response) {
                vm.apr.binder_id = response.data.id;
                vm.apr.binder = response.data;
                notify.success("Binder Created!");
                vm.processing = false;
                $http.defaults.headers.common['HB-ApiKey'] = constants.ApiKey;
                vm.submit();
            }

            function createBinderError(response) {
                vm.processing = false;
                notify.error(response.data);
            }

            function onSelectCountry(item) {
                if (item) {
                    vm.country = item;
                    vm.states = Address.getStatesAndProvinces().filter(angular.bind(vm, function(state) {
                        return state.country.value === vm.country.value;
                    }));
                }
            }
        }
    ]);
