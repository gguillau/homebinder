describe("AprEditModalController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $modalInstance,
        api,
        $location,
        $window,
        apr,
        notify,
        AprEditModal,
        ModalService;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $window = _$injector_.get("$window");
        $location = _$injector_.get("$location");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        AprEditModal = _$injector_.get("AprEditModal");
        ModalService = _$injector_.get("ModalService");
        api = _$injector_.get("hb.api");
        notify = _$injector_.get("Notify");
        $modalInstance = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };

        apr = {
            id: 1,
            client_phone: {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("AprEditModalController", {
            "hb.api": api,
            "$modalInstance": $modalInstance,
            "$window": $window,
            "$location": $location,
            "data": apr,
            "Notify": notify
        });

        $scope.ctrl = ctrl;
    }

    describe("ctrl.populate", function() {
        it("should call partner get", function() {
            spyOn(api.partner, "get").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.apr = { reviewer_company_id: 1 };
            ctrl.populate();

            expect(api.partner.get).toHaveBeenCalled();
        });
    });

    describe("ctrl.setReviewerCompany", function() {
        it("sets the reviewer_company", function() {
            createController();
            ctrl.apr = { reviewer_company_id: 1 };
            ctrl.setReviewerCompany({ data: { id: 1, name: "test" } });

            expect(ctrl.apr.reviewer_company).toEqual({ id: 1, name: "test" });
        });
    });

    describe("ctrl.binderLookup", function() {
        it("returns a list of binders", function() {
            spyOn(api.binder, "all").and.returnValue($q.when({ data: { items: [{ id: 1, name: "test" }] } }));
            createController();
            var promise = ctrl.binderLookup("test");
            $scope.$apply();

            expect(promise.$$state.value.data.length).toEqual(1);
        });
    });

    describe("ctrl.partnerLookup", function() {
        it("returns a list of partners", function() {
            spyOn(api.partner, "all").and.returnValue($q.when({ data: { items: [{ id: 1, name: "test" }] } }));
            createController();
            var promise = ctrl.partnerLookup("test");
            $scope.$apply();

            expect(promise.$$state.value.data.length).toEqual(1);
        });
    });

    describe("ctrl.onSelect", function() {
        it("sets the country", function() {
            createController();
            ctrl.apr = { reviewer_company_id: 1, state: "MA" };
            ctrl.onSelect({ country: { value: "US" } });

            expect(ctrl.apr.country).toEqual("US");
        });
    });

    describe("ctrl.submit", function() {
        it("should update the apr", function() {
            spyOn($modalInstance, "close");
            spyOn(api.annualPropertyReview, "update").and.returnValue($q.when({ data: {} }));
            createController();
            $rootScope.$apply();
            ctrl.form = { $invalid: false };
            ctrl.apr.binder = { id: 1 };
            ctrl.apr.status = "unassigned";
            ctrl.apr.reviewer_company = { id: 1 };
            ctrl.submit();
            $rootScope.$apply();

            expect(api.annualPropertyReview.update).toHaveBeenCalled();
            expect($modalInstance.close).toHaveBeenCalled();
        });

        it("returns an error", function() {
            spyOn(notify, "error");
            spyOn(api.annualPropertyReview, "update").and.returnValue($q.reject({ data: "error" }));
            createController();
            $rootScope.$apply();
            ctrl.form = { $invalid: false };
            ctrl.apr.reviewer_company = { id: 1 };
            ctrl.submit();
            $rootScope.$apply();

            expect(api.annualPropertyReview.update).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
        });

        it("doesnt make the api call", function() {
            spyOn(api.annualPropertyReview, "update");
            createController();
            ctrl.form = { $invalid: true };
            ctrl.apr.reviewer_company = { id: 1 };
            ctrl.submit();

            expect(api.annualPropertyReview.update).not.toHaveBeenCalled();
        });
    });

    describe("ctrl.cancel", function() {

        it("should call $modalInstance dismiss function", function() {
            spyOn($modalInstance, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.cancel();
            $rootScope.$apply();

            expect($modalInstance.dismiss).toHaveBeenCalled();
        });

    });

    describe("ctrl.createBinder", function() {
        it("should call partner abs", function() {
            spyOn(api.partner, "abs").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.apr = { reviewer_company_id: 1, reviewer_company: { configuration: {} } };
            ctrl.createBinder();

            expect(api.partner.abs).toHaveBeenCalled();
        });
    });

    describe("ctrl.createBinderSuccess", function() {
        it("sets the apr binder and calls submit", function() {
            createController();
            spyOn(ctrl, "submit");
            ctrl.apr = { reviewer_company_id: 1, reviewer_company: { configuration: {} } };
            ctrl.createBinderSuccess({ data: { id: 1, name: "new apr binder" } });

            expect(ctrl.apr.binder).toEqual({ id: 1, name: "new apr binder" });
            expect(ctrl.submit).toHaveBeenCalled();
        });
    });

    describe("ctrl.createBinderError", function() {
        it("calls notify", function() {
            createController();
            spyOn(notify, "error");
            ctrl.createBinderError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe('ctrl.onSelectCountry', function() {
        it('sets the error property country', function() {
            createController();

            ctrl.onSelectCountry({ value: "MA" });

            expect(ctrl.country.value).toEqual("MA");
        });
    });

    describe('AprEditModal', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            AprEditModal.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});