(function() {
    "use strict";

    angular
        .module("hb.annualPropertyReviews.reviewers", [])
        .factory("AprReviewerFindForm", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "annualPropertyReviews/reviewerForm/reviewerForm.tpl.html",
                            controller: "AprReviewerFindFormController as ctrl",
                            resolveData: opts
                        });
                    }
                };
            }
        ])
        .controller("AprReviewerFindFormController", [
            "$modalInstance",
            "hb.api",
            "$window",
            "$location",
            "hb.resources",
            AprReviewerFindFormController
        ]);

    function AprReviewerFindFormController($modal, api, $window, $location, resources) {
        this.$modal = $modal;
        this.api = api;
        this.$window = $window;
        this.$location = $location;
        this.resources = resources.aprReviewerForm;
        this.partnerLookupCfg = {
            allowClear: true,
            placeholder: this.resources.partnerSearch,
            refresh: angular.bind(this, this.partnerLookup),
            itemTemplate: "components/lookup/partner/partnerDropDownItem.tpl.html",
            selectionTemplate: "components/lookup/partner/partnerSelection.tpl.html"
        };
    }

    AprReviewerFindFormController.prototype = {

        partnerLookup: function(value) {
            var opts = {
                page: 1,
                count: 50,
                search: value
            };
            return this.api.partner.all(opts).then(
                angular.bind(this, function(response) {
                    return {
                        data: response.data.items
                    };
                })
            );
        },

        newApr: function() {
            this.$modal.dismiss("cancel");
            var url = this.$location.protocol() + '://' + this.$location.host();
            var aprLink = this.partner.partner_key ? url + "/partners/" + this.partner.partner_key + "/apr#order" : null;
            this.$window.open(aprLink, "_blank");
        },

        cancel: function() {
            this.$modal.dismiss("cancel");
        }

    };
})();