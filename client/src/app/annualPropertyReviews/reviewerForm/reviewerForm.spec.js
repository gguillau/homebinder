describe("AprReviewerFindFormController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $modalInstance,
        api,
        $location,
        $window,
        AprReviewerFindForm,
        ModalService;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $window = _$injector_.get("$window");
        $location = _$injector_.get("$location");
        api = _$injector_.get("hb.api");
        AprReviewerFindForm = _$injector_.get("AprReviewerFindForm");
        ModalService = _$injector_.get("ModalService");
        $controller = _$controller_;
        $q = _$injector_.get("$q");

        $modalInstance = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("AprReviewerFindFormController", {
            "hb.api": api,
            "$modalInstance": $modalInstance,
            "$window": $window,
            "$location": $location
        });

        $scope.ctrl = ctrl;
    }

    describe("partnerLookup", function() {
        it("loads the partners", function() {
            spyOn(api.partner, "all").and.returnValue($q.when({
                data: {
                    items: []
                }
            }));
            createController();
            ctrl.partnerLookup();
            $scope.$apply();

            expect(api.partner.all).toHaveBeenCalled();
        });
    });

    describe("ctrl.newApr", function() {
        it("opens new window", function() {

            spyOn($window, "open");
            spyOn($modalInstance, "dismiss");

            createController();
            $rootScope.$apply();

            ctrl.partner = { partner_key: "test" };
            ctrl.newApr();
            $rootScope.$apply();

            expect($window.open).toHaveBeenCalledWith("http://server/partners/test/apr#order", "_blank");
            expect($modalInstance.dismiss).toHaveBeenCalledWith('cancel');
        });
    });

    describe("ctrl.cancel", function() {
        it("should call $modalInstance dismiss function", function() {
            spyOn($modalInstance, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.cancel();
            $rootScope.$apply();

            expect($modalInstance.dismiss).toHaveBeenCalledWith('cancel');
        });

    });

    describe('AprReviewerFindForm', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            AprReviewerFindForm.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});