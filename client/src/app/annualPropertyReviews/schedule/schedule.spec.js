describe("aprSchedule", function() {
    var ctrl,
        controller,
        $scope,
        $q,
        $log,
        api,
        resources,
        $modalInstance,
        AprScheduleModal,
        ModalService;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector, $rootScope) {
        controller = $controller;
        $scope = $rootScope;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        AprScheduleModal = $injector.get("AprScheduleModal");
        ModalService = $injector.get("ModalService");
    }));

    $modalInstance = {
        close: function() {},
        dismiss: function() {}
    };

    function createController(data) {
        ctrl = controller("AprScheduleController", {
            "$log": $log,
            "hb.api": api,
            "hb.resources": resources,
            "$modalInstance": $modalInstance,
            "data": data
        });
    }

    describe("populate", function() {
        it("populates with a default date", function() {
            createController({ id: 1, reviewer_company: { id: 1 } });
            var d = new Date();
            expect(ctrl.scheduledDate.getFullYear()).toEqual(d.getFullYear());
            expect(ctrl.scheduledDate.getMonth()).toEqual(d.getMonth());
            expect(ctrl.scheduledDate.getDate()).toEqual(d.getDate());
            expect(ctrl.scheduledTime.getHours()).toEqual(13);
            expect(ctrl.scheduledTime.getMinutes()).toEqual(0);
        });

        it("populates with a scheduled date", function() {
            createController({ id: 1, reviewer_company: { id: 1 }, reviewer: { id: 1 }, scheduled_for: "2020-07-07T15:00:00" });
            var d = new Date();
            expect(ctrl.scheduledDate.getFullYear()).toEqual(2020);
            expect(ctrl.scheduledDate.getMonth()).toEqual(6);
            expect(ctrl.scheduledDate.getDate()).toEqual(7);
            //expect(ctrl.scheduledTime.getHours()).toEqual(15); // GMT
            expect(ctrl.scheduledTime.getMinutes()).toEqual(0);
        });

        it("gets the reviewer", function() {
            spyOn(api.user, "get").and.returnValue($q.when({
                data: {
                    id: 1,
                    user_profile_attributes: {
                        first_name: "bob",
                        last_name: "smith"
                    },
                    email: "bob@reviewer.com"
                }
            }));
            createController({ id: 1, scheduled_for: "2020-07-07 15:00:00", reviewer_id: 1, reviewer_company: { id: 1 } });
            $scope.$apply();
            expect(api.user.get).toHaveBeenCalledWith(1);
            expect(ctrl.reviewer).toEqual(jasmine.objectContaining({
                id: 1,
                displayName: 'bob smith',
                email: 'bob@reviewer.com'
            }));
        });
    });

    describe("schedule", function() {
        it("does not schedule in the past", function() {
            spyOn(api.annualPropertyReview, "update");
            createController({ id: 1, reviewer_company: { id: 1 } });
            var d = new Date();
            ctrl.scheduledTime = new Date(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes() - 1);
            ctrl.reviewer = {
                id: 99,
                displayName: "bob smith",
                email: "bob@reviewer.com"
            };
            ctrl.schedule();
            expect(ctrl.invalid).toEqual(true);
            expect(api.annualPropertyReview.update).not.toHaveBeenCalled();
        });

        it("does not schedule without a reviewer", function() {
            spyOn(api.annualPropertyReview, "update");
            createController({ id: 1, reviewer_company: { id: 1 } });
            var d = new Date();
            ctrl.scheduledTime = new Date(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMinutes() + 1);
            ctrl.schedule();
            expect(ctrl.invalid).toEqual(true);
            expect(api.annualPropertyReview.update).not.toHaveBeenCalled();
        });

        it("schedules the apr", function() {
            spyOn(api.annualPropertyReview, "update").and.returnValue($q.when({}));
            spyOn($modalInstance, "close");
            createController({ id: 1, reviewer_company: { id: 1 } });
            var d = new Date();
            ctrl.scheduledTime = new Date(d.getFullYear(), d.getMonth(), d.getDate() + 1, d.getHours() + 1, 0);
            ctrl.scheduledDate = new Date(
                ctrl.scheduledDate.getFullYear(),
                ctrl.scheduledDate.getMonth(),
                ctrl.scheduledDate.getDate() + 1);
            ctrl.reviewer = {
                id: 99,
                displayName: "bob smith",
                email: "bob@reviewer.com"
            };
            ctrl.schedule();
            $scope.$apply();
            expect(ctrl.invalid).toEqual(false);
            expect(api.annualPropertyReview.update).toHaveBeenCalled();
            expect($modalInstance.close).toHaveBeenCalled();
        });
    });

    describe("cancel", function() {
        it("dismisses the dialog", function() {
            spyOn($modalInstance, "dismiss");
            createController({ id: 1, reviewer_company: { id: 1 } });
            ctrl.cancel();
            expect($modalInstance.dismiss).toHaveBeenCalled();
        });
    });

    describe('AprScheduleModal', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            AprScheduleModal.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});
