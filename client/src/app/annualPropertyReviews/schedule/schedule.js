angular
    .module("hb.annualPropertyReviews.schedule", [])
    .factory("AprScheduleModal", [
        "ModalService",
        function(modals) {
            return {
                show: function(args) {
                    return modals.show({
                        aside: false,
                        templateUrl: "annualPropertyReviews/schedule/schedule.tpl.html",
                        controller: "AprScheduleController as ctrl",
                        resolveData: args.apr,
                        closed: args.closed
                    });
                }
            };
        }
    ])
    .controller("AprScheduleController", [
        "$log",
        "hb.api",
        "hb.resources",
        "$modalInstance",
        "data",
        "hb.utils",
        function($log, api, resources, $modalInstance, apr, utils) {
            var vm = this;
            vm.apr = apr;
            vm.resources = resources.aprSchedule;
            vm.schedule = schedule;
            vm.cancel = cancel;
            vm.today = utils.utils.getToday(false);
            vm.min = new Date();
            vm.max = vm.min.setMonth(vm.min.getMonth() + 14);
            vm.datepicker = {
                format: "MMMM dd, yyyy",
                options: {
                    "show-button-bar": false
                }
            };

            vm.lookupCfg = {
                partnerId: vm.apr.reviewer_company_id
            };

            vm.hstep = 1;
            vm.mstep = 15;
            vm.reviewers = [];
            vm.ismeridian = true;
            vm.invalid = false;

            populate();

            function populate() {
                if (apr.scheduled_for) {
                    var scheduledFor = new Date(apr.scheduled_for);
                    vm.scheduledDate = new Date(
                        scheduledFor.getFullYear(),
                        scheduledFor.getMonth(),
                        scheduledFor.getDate()
                    );
                    vm.scheduledTime = new Date(
                        scheduledFor.getFullYear(),
                        scheduledFor.getMonth(),
                        scheduledFor.getDate(),
                        scheduledFor.getHours(),
                        scheduledFor.getMinutes()
                    );
                }
                else {
                    vm.scheduledDate = new Date();
                    vm.scheduledTime = new Date(vm.scheduledDate.getFullYear(), 0, 1, 13, 0);
                }

                if (vm.apr.reviewer_id) {
                    api.user.get(vm.apr.reviewer_id).then(setReviewer);
                }
                else if (vm.apr.reviewer) {
                    api.user.get(vm.apr.reviewer.id).then(setReviewer);
                }
                if (vm.apr.reviewer_company) {
                    vm.lookupCfg.role = vm.apr.reviewer_company.partner_type;
                }
            }

            function setReviewer(response) {
                var user = response.data;
                vm.reviewer = {
                    id: user.id,
                    displayName: user.user_profile_attributes.first_name + " " + user.user_profile_attributes.last_name,
                    email: user.email
                };
            }

            function schedule() {
                var scheduledFor = new Date(
                    vm.scheduledDate.getFullYear(),
                    vm.scheduledDate.getMonth(),
                    vm.scheduledDate.getDate(),
                    vm.scheduledTime.getHours(),
                    vm.scheduledTime.getMinutes()
                );

                if (scheduledFor < new Date() || !vm.reviewer) {
                    vm.invalid = true;
                    return;
                }

                vm.invalid = false;

                api.annualPropertyReview.update(apr.id, {
                    annual_property_review: {
                        status: "scheduled",
                        reviewer_id: vm.reviewer.id,
                        scheduled_for: scheduledFor
                    }
                }).then(
                    scheduled
                );
            }

            function scheduled(response) {
                $modalInstance.close(response);
            }

            function cancel() {
                $modalInstance.dismiss();
            }
        }
    ]);
