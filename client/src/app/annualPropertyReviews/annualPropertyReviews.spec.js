describe("AnnualPropertyReviewsController", function() {
    var controller,
        base,
        api,
        resources,
        AprReviewerFindForm,
        $state,
        AprScheduleModal,
        ModalService,
        AprCancelModal,
        AprMessageForm,
        AprEditModal,
        $window,
        $q_,
        defer,
        $rootScope_;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
        // mock the loading service
        $provide.factory("Context", function() {
            return {
                getPartner: function() {
                    defer = $q_.defer();
                    defer.resolve({ partner_key: "key" });
                    return defer.promise;
                }
            };
        });
        // mock the Session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return {
                        role: "admin"
                    };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        $rootScope_ = $rootScope;
        $q_ = $q;
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        $state = $injector.get("$state");
        AprReviewerFindForm = $injector.get("AprReviewerFindForm");
        AprScheduleModal = $injector.get("AprScheduleModal");
        ModalService = $injector.get("ModalService");
        AprCancelModal = $injector.get("AprCancelModal");
        AprMessageForm = $injector.get("AprMessageForm");
        AprEditModal = $injector.get("AprEditModal");
        $window = $injector.get("$window");

        spyOn(api.annualPropertyReview, "index").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));
        spyOn(api.annualPropertyReview, "destroy").and.returnValue($q.when({}));

        controller = $controller("AnnualPropertyReviewsController", {
            "hb.framework.indexBase": base,
            "hb.api": api,
            "hb.resources": resources
        });

        $rootScope.$apply();
    }));

    describe("selectFilter", function() {
        it("calls refresh", function() {
            spyOn(controller.model, "refresh");
            controller.model.selectFilter({ value: "binders" });

            expect(controller.model.refresh).toHaveBeenCalled();
        });
    });

    describe("refresh", function() {
        it("gets the aprs", function() {
            expect(api.annualPropertyReview.index).toHaveBeenCalled();
        });
    });

    describe("requestApr", function() {
        it("calls modal", function() {
            spyOn(AprReviewerFindForm, "show");
            controller.model.requestApr();
            expect(AprReviewerFindForm.show).toHaveBeenCalledWith();
        });

        it("calls window open", function() {
            spyOn($window, "open");
            var $scope = $rootScope_.$new();
            controller.model.currentUser.role = "inspector";
            controller.model.requestApr();
            $scope.$digest();
            
            expect($window.open).toHaveBeenCalled();
        });
    });

    describe("view", function() {
        it("sends to details view", function() {
            spyOn($state, "go");
            controller.model.view({ id: 1 });
            expect($state.go).toHaveBeenCalled();
        });
    });

    describe("canHoldApr", function() {
        it("returns true", function() {
            spyOn($state, "go");
            var result = controller.model.canHoldApr({ status: "active" });
            expect(result).toEqual(true);
        });

        it("returns true", function() {
            spyOn($state, "go");
            var result = controller.model.canHoldApr({ status: "scheduled" });
            expect(result).toEqual(true);
        });

        it("returns false", function() {
            spyOn($state, "go");
            var result = controller.model.canHoldApr({ status: "on_hold" });
            expect(result).toEqual(false);
        });
    });

    describe("canScheduleApr", function() {
        it("returns true", function() {
            spyOn($state, "go");
            var result = controller.model.canScheduleApr({ status: "active" });
            expect(result).toEqual(true);
        });

        it("returns true", function() {
            spyOn($state, "go");
            var result = controller.model.canScheduleApr({ status: "scheduled" });
            expect(result).toEqual(true);
        });

        it("returns true", function() {
            spyOn($state, "go");
            var result = controller.model.canScheduleApr({ status: "on_hold" });
            expect(result).toEqual(true);
        });

        it("returns false", function() {
            spyOn($state, "go");
            var result = controller.model.canScheduleApr({ status: "completed" });
            expect(result).toEqual(false);
        });
    });

    describe("canStartDraftApr", function() {
        it("returns true", function() {
            spyOn($state, "go");
            var result = controller.model.canStartDraftApr({ status: "scheduled" });
            expect(result).toEqual(true);
        });

        it("returns false", function() {
            spyOn($state, "go");
            var result = controller.model.canStartDraftApr({ status: "on_hold" });
            expect(result).toEqual(false);
        });
    });

    describe("canDraftCompleteApr", function() {
        it("returns true", function() {
            spyOn($state, "go");
            var result = controller.model.canDraftCompleteApr({ status: "draft" });
            expect(result).toEqual(true);
        });

        it("returns false", function() {
            spyOn($state, "go");
            var result = controller.model.canDraftCompleteApr({ status: "on_hold" });
            expect(result).toEqual(false);
        });
    });

    describe("canReDraftApr", function() {
        it("returns true", function() {
            spyOn($state, "go");
            var result = controller.model.canReDraftApr({ status: "completed" });
            expect(result).toEqual(true);
        });

        it("returns false", function() {
            spyOn($state, "go");
            var result = controller.model.canReDraftApr({ status: "on_hold" });
            expect(result).toEqual(false);
        });
    });

    describe("canCompleteApr", function() {
        it("returns true", function() {
            spyOn($state, "go");
            var result = controller.model.canCompleteApr({ status: "draft_completed" });
            expect(result).toEqual(true);
        });

        it("returns false", function() {
            spyOn($state, "go");
            var result = controller.model.canCompleteApr({ status: "on_hold" });
            expect(result).toEqual(false);
        });
    });

    describe("canCancelApr", function() {
        it("returns true", function() {
            spyOn($state, "go");
            var result = controller.model.canCancelApr({ status: "active" });
            expect(result).toEqual(true);
        });

        it("returns true", function() {
            spyOn($state, "go");
            var result = controller.model.canCancelApr({ status: "scheduled" });
            expect(result).toEqual(true);
        });

        it("returns true", function() {
            spyOn($state, "go");
            var result = controller.model.canCancelApr({ status: "on_hold" });
            expect(result).toEqual(true);
        });

        it("returns false", function() {
            spyOn($state, "go");
            var result = controller.model.canCancelApr({ status: "completed" });
            expect(result).toEqual(false);
        });
    });

    describe("schedule", function() {
        it("calls modal", function() {
            spyOn(AprScheduleModal, "show");
            controller.model.schedule({ id: 1 });
            expect(AprScheduleModal.show).toHaveBeenCalled();
        });
    });

    describe("startDraft", function() {
        it("switches to draft mode", function() {
            spyOn(controller.model, "statusChangeConfirmed");
            controller.model.startDraft({ id: 1 });
            expect(controller.model.goToDraft).toBe(true);
        });
    });

    describe("draftComplete", function() {
        it("calls modal", function() {
            spyOn(ModalService, "confirm");
            controller.model.draftComplete({ id: 1 });
            expect(ModalService.confirm).toHaveBeenCalled();
        });
    });

    describe("reDraft", function() {
        it("calls modal", function() {
            spyOn(ModalService, "confirm");
            controller.model.reDraft({ id: 1 });
            expect(ModalService.confirm).toHaveBeenCalled();
        });
    });

    describe("complete", function() {
        it("calls modal", function() {
            spyOn(ModalService, "confirm");
            controller.model.complete({ id: 1 });
            expect(ModalService.confirm).toHaveBeenCalled();
        });
    });

    describe("cancel", function() {
        it("calls modal", inject(function($q) {
            spyOn(AprCancelModal, "show").and.returnValue($q.when({ data: {} }));
            controller.model.cancel({ id: 1 });
            expect(AprCancelModal.show).toHaveBeenCalled();
        }));
    });

    describe("hold", function() {
        it("calls modal", function() {
            spyOn(ModalService, "confirm");
            controller.model.hold({ id: 1 });
            expect(ModalService.confirm).toHaveBeenCalled();
        });
    });

    describe("statusChangeConfirmed", function() {
        it("changes the status", inject(function($rootScope, $q) {
            spyOn(api.annualPropertyReview, "update").and.returnValue($q.when({ data: {} }));
            $rootScope.$apply();
            controller.model.statusChangeConfirmed({ status: "draft" }, "draft_completed");
            $rootScope.$apply();

            expect(api.annualPropertyReview.update).toHaveBeenCalled();
        }));
    });

    describe("aprUpdated", function() {
        it("closes the modal", inject(function(Loading) {
            spyOn(Loading, "close");
            controller.model.aprUpdated({ id: 1 }, { data: { id: 1 } });
            expect(Loading.close).toHaveBeenCalled();
        }));

        it("calls the view function", inject(function(Loading) {
            spyOn(Loading, "close");
            controller.model.goToDraft = true;
            spyOn(controller.model, "view");
            controller.model.aprUpdated({ id: 1 }, { data: { id: 1 } });
            expect(controller.model.view).toHaveBeenCalled();
            expect(Loading.close).toHaveBeenCalled();
        }));
    });

    describe("aprUpdateError", function() {
        it("closes the modal", inject(function(Loading, Notify) {
            spyOn(Loading, "close");
            spyOn(Notify, "error");
            controller.model.aprUpdateError({ data: "error" });
            expect(Loading.close).toHaveBeenCalled();
            expect(Notify.error).toHaveBeenCalled();
        }));
    });

    describe("download", function() {
        it("download the apr report", inject(function($rootScope, $q) {
            spyOn(api.annualPropertyReview, "download").and.returnValue($q.when({ data: {} }));
            controller.model.download({ id: 1 });
            $rootScope.$apply();

            expect(api.annualPropertyReview.download).toHaveBeenCalled();
        }));
    });

    describe("downloadSuccess", function() {
        it("closes the modal", inject(function(Loading) {
            spyOn(Loading, "close");
            controller.model.downloadSuccess({ id: 1 }, {});
            expect(Loading.close).toHaveBeenCalled();
        }));
    });

    describe("downloadError", function() {
        it("closes the modal", inject(function(Loading, $log, Notify) {
            spyOn(Loading, "close");
            spyOn($log, "error");
            spyOn(Notify, "error");
            controller.model.downloadError({ data: "error" });
            expect(Loading.close).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
            expect(Notify.error).toHaveBeenCalled();
        }));
    });

    describe("statusSort", function() {
        it("returns 0", function() {
            var result = controller.model.statusSort({ status: "unassigned" });
            expect(result).toEqual(0);
        });
        it("returns 1", function() {
            var result = controller.model.statusSort({ status: "active" });
            expect(result).toEqual(1);
        });
        it("returns 2", function() {
            var result = controller.model.statusSort({ status: "scheduled" });
            expect(result).toEqual(2);
        });
        it("returns 3", function() {
            var result = controller.model.statusSort({ status: "draft" });
            expect(result).toEqual(3);
        });
        it("returns 4", function() {
            var result = controller.model.statusSort({ status: "draft_completed" });
            expect(result).toEqual(4);
        });
        it("returns 5", function() {
            var result = controller.model.statusSort({ status: "on_hold" });
            expect(result).toEqual(5);
        });
        it("returns 6", function() {
            var result = controller.model.statusSort({ status: "canceled" });
            expect(result).toEqual(6);
        });
        it("returns 7", function() {
            var result = controller.model.statusSort({ status: "completed" });
            expect(result).toEqual(7);
        });
    });

    describe("messageModal", function() {
        it("calls modal", inject(function($q) {
            spyOn(AprMessageForm, "show");
            controller.model.messageModal({ id: 1 });
            expect(AprMessageForm.show).toHaveBeenCalled();
        }));
    });

    describe("editApr", function() {
        it("calls modal", inject(function($q) {
            spyOn(AprEditModal, "show");
            controller.model.editApr({ id: 1 });
            expect(AprEditModal.show).toHaveBeenCalled();
        }));
    });
});

describe('hbAnnualPropertyReviews', function() {
    var $scope, $compile, element, $q, api, session, $stateParams;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        session = _$injector_.get("Session");
        $stateParams = _$injector_.get("$stateParams");

        $stateParams.partnerId = 1;
        spyOn(session, "getUser").and.returnValue({ id: 1 });
        spyOn(api.annualPropertyReview, "index").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = {};

        element = $compile('<hb-annual-property-reviews cfg="cfg"></hb-annual-property-reviews>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("Annual Property Review");
        });
    });
});