(function() {
    "use strict";

    angular
        .module("hb.legal", [])
        .config(['$stateProvider', legalConfig]);

    function legalConfig($stateProvider) {
        $stateProvider
            .state('terms', {
                url: '^/terms',
                templateUrl: 'legal/terms.tpl.html'
            })
            .state('privacy', {
                url: '^/privacy',
                templateUrl: 'legal/privacy.tpl.html'
            })
            .state('aprterms', {
                url: '^/APRTermsOfService',
                templateUrl: 'legal/apr.tpl.html'
            });
    }
})();