describe("AgentsLandingController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $location,
        $state,
        FreeTrialModal,
        utils;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");

        utils = {
            utils: {
                getYear: function() {}
            }
        };

        FreeTrialModal = {
            show: function() {}
        };

        $location = {
            hash: function(element) {}
        };

        $state = {
            go: function() {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("AgentsLandingController", {
            "$state": $state,
            "$location": $location,
            "FreeTrialModal": FreeTrialModal,
            "hb.utils": utils
        });
        $scope.ctrl = ctrl;

    }

    describe('ctrl.scrollToElement', function() {

        it('should call $location hash function', function() {

            spyOn($location, "hash");

            createController();
            $rootScope.$apply();
            ctrl.scrollToElement('element');
            $rootScope.$apply();

            expect($location.hash).toHaveBeenCalledWith('element');

        });

    });

    describe('ctrl.startTrial', function() {

        it('should call $state go function', function() {

            spyOn($state, "go");

            createController();
            $rootScope.$apply();
            ctrl.startTrial();
            $rootScope.$apply();

            expect($state.go).toHaveBeenCalledWith("partnerRegistration", {
                type: 'agent'
            });

        });

    });

    describe('ctrl.requestTrial', function() {

        it('should call FreeTrialModal show function', function() {

            spyOn(FreeTrialModal, "show");

            createController();
            $rootScope.$apply();
            ctrl.requestTrial();
            $rootScope.$apply();

            expect(FreeTrialModal.show).toHaveBeenCalled();

        });

    });

});

describe('agentsLanding', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        $scope = $rootScope.$new(), $compile = _$compile_;

        element = $compile('<agents-landing></agents-landing>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the element', function() {
            expect(element.html()).toContain("We Help Real Estate Agents");
        });
    });
});