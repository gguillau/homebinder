(function() {
  "use strict";

  angular
    .module("hb.landing.agents", [])
    .config(['$stateProvider', function($stateProvider) {
      $stateProvider
        .state("agents_landing", {
          url: "^/agents",
          template: "<agents-landing></agents-landing>"
        });
    }])
    .directive("agentsLanding", function() {
      return {
        restrict: "E",
        templateUrl: "landing/agents/agents.tpl.html",
        controller: "AgentsLandingController",
        controllerAs: "ctrl",
        bindToController: true,
        scope: {}
      };
    })
    .controller("AgentsLandingController", [
      "$location",
      "$interval",
      "$state",
      "FreeTrialModal",
      "hb.utils",
      "hb.resources",
      "Session",
      AgentsLandingController
    ]);

  function AgentsLandingController($location, $interval, $state, FreeTrialModal, utils, resources, Session) {
    this.utils = utils;
    this.yyyy = this.utils.utils.getYear();
    this.$interval = $interval;
    this.$state = $state;
    this.$location = $location;
    this.FreeTrialModal = FreeTrialModal;
    this.resources = resources.agentLanding;
    this.resources = angular.extend({}, this.resources, resources.standardLanding);
    this.resources = angular.extend({}, this.resources, resources.genericLanding);
    this.setLocale = Session.setLocale;
  }

  AgentsLandingController.prototype = {
    startTrial: function() {
      this.$state.go("partnerRegistration", {
        type: 'agent'
      });
    },

    requestTrial: function() {
      this.FreeTrialModal.show();
    },

    scrollToElement: function(element) {
      this.$location.hash(element);
    }
  };
})();