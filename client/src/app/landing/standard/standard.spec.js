describe("StandardLandingController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        utils;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        utils = _$injector_.get("hb.utils");
        $controller = _$controller_;
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("StandardLandingController", {
            "hb.utils": utils
        });
        $scope.ctrl = ctrl;
    }

    describe('init', function() {
        it('sets the year', function() {
            createController();
            expect(ctrl.yyyy).toEqual(utils.utils.getYear());
        });
    });
});