(function() {
    "use strict";

    angular
        .module("hb.landing.standard", [])
        .config(['$stateProvider', landingConfig])
        .controller("StandardLandingController", [
            "hb.utils",
            "hb.resources",
            "Session",
            StandardLandingController
        ]);

    function StandardLandingController(utils, resources, session) {
        this.resources = resources.homeownerNewLanding;
        this.resources = angular.extend({}, this.resources, resources.genericLanding);
        this.resources = angular.extend({}, this.resources, resources.standardLanding);
        this.utils = utils;
        this.yyyy = this.utils.utils.getYear();
        this.setLocale = session.setLocale;
    }


    function landingConfig($stateProvider) {
        $stateProvider
            .state('root', {
                url: '^/',
                templateUrl: 'landing/standard/standard.tpl.html',
                controller: "StandardLandingController as ctrl"
            });
    }
})();