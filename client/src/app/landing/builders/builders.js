(function() {
	"use strict";

	angular.module('hb.landing.builders', [])
		.config(['$stateProvider', buildersLandingConfig])
		.controller("BuildersLandingController", [
			"hb.landing.landingBase",
			BuildersLandingController
		]);

	function BuildersLandingController(LandingBaseController) {

		var Model = function() {
			// call the parent class
			LandingBaseController.call(this);
			this.type = "builder";
		};

		Model.prototype = Object.create(LandingBaseController.prototype);

		this.model = new Model();
	}

	function buildersLandingConfig($stateProvider) {
		$stateProvider
			.state('builders_landing', {
				url: '^/builders',
				templateUrl: 'landing/framework/landing.tpl.html',
				controller: "BuildersLandingController",
				controllerAs: "ctrl"
			});
	}

})();