(function() {
    "use strict";

    angular
        .module("hb.landing.inspectors", [
            "hb.landing.inspectors.video"
        ])
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state("inspectors_landing", {
                    url: "^/inspectors",
                    template: "<inspectors-landing></inspectors-landing>"
                });
        }])
        .directive("inspectorsLanding", function() {
            return {
                restrict: "E",
                templateUrl: "landing/inspectors/inspectors.tpl.html",
                controller: "InspectorsLandingController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("InspectorsLandingController", [
            "$location",
            "$interval",
            "$state",
            "FreeTrialModal",
            "hb.utils",
            "InspectorVideo",
            "constants",
            "$intercom",
            "$window",
            "hb.resources",
            "Session",
            InspectorsLandingController
        ]);

    function InspectorsLandingController($location, $interval, $state, FreeTrialModal, utils, InspectorVideo, constants, $intercom, $window, resources, Session) {
        this.utils = utils;
        this.yyyy = this.utils.utils.getYear();
        this.$interval = $interval;
        this.$state = $state;
        this.$location = $location;
        this.FreeTrialModal = FreeTrialModal;
        this.InspectorVideo = InspectorVideo;
        this.constants = constants;
        this.$intercom = $intercom;
        this.$window = $window;
        this.resources = resources.inspectorNewLanding;
        this.resources = angular.extend({}, this.resources, resources.genericLanding);
        this.resources = angular.extend({}, this.resources, resources.standardLanding);
        this.setLocale = Session.setLocale;
        // boot intercom
        this.loadIntercom();
    }

    InspectorsLandingController.prototype = {

        scrollToElement: function(element) {
            this.$location.hash(element);
        },

        startTrial: function() {
            this.$state.go("partnerRegistration", {
                type: 'inspector'
            });
        },

        requestTrial: function() {
            this.FreeTrialModal.show();
        },

        watchVideo: function() {
            this.InspectorVideo.show();
        },

        loadIntercom: function() {
            var host = this.$location.host();
            var id = host === "www.homebinder.com" ? this.constants.ProductionIntercomAppId : this.constants.TestIntercomAppId;
            if (!this.onMobile()) {
                this.$intercom.boot({
                    app_id: id
                });
            }
        },

        /*  detect when a user is on a mobile device to
                    prevent the intercom chat window from loading
                    iPhone 6s width: 328 - height: 558
                */

        onMobile: function() {
            if (this.$window.innerWidth <= 700 && this.$window.innerHeight <= 800) {
                return true;
            }
            else {
                return false;
            }
        }
    };
})();
