describe("InspectorsLandingController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $location,
        $state,
        FreeTrialModal,
        utils,
        InspectorVideo;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");

        utils = {
            utils: {
                getYear: function() {}
            }
        };

        FreeTrialModal = {
            show: function() {}
        };

        InspectorVideo = {
            show: function() {}
        };

        $location = {
            hash: function(element) {},
            host: function() {}
        };

        $state = {
            go: function() {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("InspectorsLandingController", {
            "$state": $state,
            "$location": $location,
            "FreeTrialModal": FreeTrialModal,
            "hb.utils": utils,
            "InspectorVideo": InspectorVideo
        });
        $scope.ctrl = ctrl;

    }

    describe('ctrl.scrollToElement', function() {

        it('should call $location hash function', function() {

            spyOn($location, "hash");
            spyOn($location, "host").and.returnValue("www.homebinder.com");

            createController();
            $rootScope.$apply();
            ctrl.scrollToElement('element');
            $rootScope.$apply();

            expect($location.hash).toHaveBeenCalledWith('element');

        });

    });

    describe('ctrl.startTrial', function() {

        it('should call $state go function', function() {

            spyOn($state, "go");
            spyOn($location, "host").and.returnValue("www.homebinder.com");

            createController();
            $rootScope.$apply();
            ctrl.startTrial();
            $rootScope.$apply();

            expect($state.go).toHaveBeenCalledWith("partnerRegistration", {
                type: 'inspector'
            });

        });

    });

    describe('ctrl.requestTrial', function() {

        it('should call FreeTrialModal show function', function() {

            spyOn(FreeTrialModal, "show");
            spyOn($location, "host").and.returnValue("www.homebinder.com");

            createController();
            $rootScope.$apply();
            ctrl.requestTrial();
            $rootScope.$apply();

            expect(FreeTrialModal.show).toHaveBeenCalled();

        });

    });

    describe('ctrl.watchVideo', function() {
        it('should call InspectorVideo show function', function() {

            spyOn(InspectorVideo, "show");

            createController();
            ctrl.watchVideo();

            expect(InspectorVideo.show).toHaveBeenCalled();

        });

    });

    describe('ctrl.onMobile', function() {
        it('return true', function() {
            createController();
            ctrl.$window.innerWidth = 699;
            ctrl.$window.innerHeight = 799;
            var result = ctrl.onMobile();

            expect(result).toEqual(true);
        });

    });

    describe('ctrl.loadIntercom', function() {
        it('loads intercom', function() {
            createController();
            spyOn(ctrl, "onMobile").and.returnValue(false);
            spyOn(ctrl.$intercom, "boot");

            ctrl.loadIntercom();
            expect(ctrl.onMobile).toHaveBeenCalled();
            expect(ctrl.$intercom.boot).toHaveBeenCalled();
        });

    });

});

describe('inspectorsLanding', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        $scope = $rootScope.$new(), $compile = _$compile_;

        element = $compile('<inspectors-landing></inspectors-landing>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the element', function() {
            expect(element.html()).toContain("Give your clients our mobile app and desktop site");
        });
    });
});