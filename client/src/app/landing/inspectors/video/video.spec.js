describe("InspectorVideoController", function() {
    var $rootScope,
        $controller,
        resources,
        ctrl,
        $modalInstance,
        ModalService,
        InspectorVideo;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        resources = _$injector_.get("hb.resources");
        ModalService = _$injector_.get("ModalService");
        InspectorVideo = _$injector_.get("InspectorVideo");
        
        $modalInstance = {
            dismiss: function(){}
        };
    }));

    function createController() {
        ctrl = $controller("InspectorVideoController", {
            "$modalInstance": $modalInstance,
            "hb.resources": resources
        });
        $rootScope.$apply();
    }

    describe('cancel', function() {
        it("calls dismiss", function() {
            spyOn($modalInstance, "dismiss");
            
            createController();
            ctrl.cancel();
            
            expect($modalInstance.dismiss).toHaveBeenCalled();
        });
    });
    
    describe('InspectorVideo', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            InspectorVideo.show();

            expect(ModalService.show).toHaveBeenCalled();
        });
    });

});