(function() {
    "use strict";

    angular
        .module("hb.landing.inspectors.video", [])
        .factory("InspectorVideo", [
            "ModalService",
            function(Modals) {
                return {
                    show: function() {
                        Modals.show({
                            templateUrl: "landing/inspectors/video/video.tpl.html",
                            controller: "InspectorVideoController as ctrl",
                            backdrop: true
                        });
                    }
                };
            }
        ])
        .controller("InspectorVideoController", [
            "$modalInstance",
            InspectorVideoController
        ]);

    function InspectorVideoController($modal) {
        this.$modal = $modal;
    }

    InspectorVideoController.prototype = {

        cancel: function() {
            this.$modal.dismiss("cancel");
        }
    };
})();