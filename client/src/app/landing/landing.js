angular
	.module("hb.landing", [
		"hb.landing.agents",
		"hb.landing.inspectors",
		"hb.landing.standard",
		"hb.landing.freeTrial",
		"hb.landing.lenders",
		"hb.landing.pros",
		"hb.landing.repair_pricer_form",
		"hb.landing.builders",
		"hb.landing.managers",
		"hb.landing.hoas",
		"hb.landing.insurances",
		"hb.landing.landingBase"
	]);