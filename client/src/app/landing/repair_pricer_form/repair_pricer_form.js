(function() {
	"use strict";

	angular
		.module("hb.landing.repair_pricer_form", [])
		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("order_repair_pricer", {
					url: "^/order_repair_pricer",
					template: "<p></p>",
					controller: 'RepairPricerOrderFormController',
					controllerAs: "ctrl",
					access: {
						requiresLogin: false
					}
				});
		}])
		.controller("RepairPricerOrderFormController", [
			"RepairPricerModal",
			"hb.api",
			"$location",
			"Loading",
			"$log",
			"Notify",
			RepairPricerOrderFormController
		]);

	function RepairPricerOrderFormController(RepairPricerModal, api, $location, loading, $log, notify) {
		this.RepairPricerModal = RepairPricerModal;
		this.api = api;
		this.$location = $location;
		this.loading = loading;
		this.$log = $log;
		this.notify = notify;
		this.repair_pricer_token = this.$location.search().repair_pricer_token;
		
		this.init();
	}

	RepairPricerOrderFormController.prototype = {
		init: function() {
			this.loading.show("Loading...");
			this.api.userBinder.find_by_repair_pricer_token(this.repair_pricer_token).then(
				angular.bind(this, this.getUserBinderSuccess),
				angular.bind(this, this.getUserBinderError));
		},

		getUserBinderSuccess: function(response) {
			// Set the data for the agent
			this.agent_binder = response.data;
			this.agent_binder.user.first_name = this.agent_binder.user.user_profile_attributes.first_name;
			this.agent_binder.user.last_name = this.agent_binder.user.user_profile_attributes.last_name;
			
			this.binder = response.data.binder;
			this.client = this.binder.partner_binders[0].homeowner;
			this.client.user_profile_attributes.address_attributes = this.binder.property;
			this.partner = this.binder.partner_binders[0].partner;

			this.loading.close();
			this.showForm();
		},

		getUserBinderError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		showForm: function() {
			this.RepairPricerModal.show_agent_form({
				user: this.client,
				buyer_agent: this.agent_binder.user,
				binder_id: this.binder.id,
				partner_id: this.partner.id,
				repair_pricer_pre_paid_reports: this.agent_binder.access_repair_pricer,
				no_login: true
			});
		}
	};
})();