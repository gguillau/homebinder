describe("RepairPricerOrderFormController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        RepairPricerModal,
        api,
        $location,
        loading,
        $log,
        notify,
        $q,
        agent_binder;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        RepairPricerModal = _$injector_.get("RepairPricerModal");
        api = _$injector_.get("hb.api");
        $location = _$injector_.get("$location");
        loading = _$injector_.get("Loading");
        $log = _$injector_.get("$log");
        notify = _$injector_.get("Notify");
        $q = _$injector_.get("$q");
        $controller = _$controller_;

        agent_binder = {
            binder: {
                id: 1,
                partner_binders: [{
                    homeowner: {
                        user_profile_attributes: {
                            role: "homeowner",
                            address_attributes: {
                                address1: null,
                                address2: null,
                                city: null,
                                country: null,
                                state: null,
                                zip: null
                            }
                        }
                    },
                    partner: { id: 1 }
                }],
                property: {
                    address1: "Address",
                    address2: null,
                    city: "City",
                    country: "US",
                    state: "MA",
                    zip: "02360"
                }
            },
            user: {
                user_profile_attributes: {
                    first_name: "Agent First Name",
                    last_name: "Agent Last Name"
                },
                email: "agent@email.com"
            }
        };
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("RepairPricerOrderFormController", {
            "RepairPricerModal": RepairPricerModal,
            "hb.api": api,
            "$location": $location,
            "Loading": loading,
            "$log": $log,
            "Notify": notify
        });
        $scope.ctrl = ctrl;
    }

    describe('ctrl.showForm', function() {
        it('calls loading.show and api.userBinder.find_by_repair_pricer_token', function() {
            spyOn(loading, "show");
            spyOn(api.userBinder, "find_by_repair_pricer_token").and.returnValue($q.when({
                data: agent_binder
            }));

            createController();

            expect(loading.show).toHaveBeenCalled();
            expect(api.userBinder.find_by_repair_pricer_token).toHaveBeenCalled();
        });
    });

    describe('ctrl.getUserBinderSuccess', function() {
        it('sets the data', function() {
            spyOn(loading, "close");
            spyOn(api.userBinder, "find_by_repair_pricer_token").and.returnValue($q.when({
                data: agent_binder
            }));

            createController();
            spyOn(ctrl, "showForm");
            
            var client = agent_binder.binder.partner_binders[0].homeowner;
            client.user_profile_attributes.address_attributes = agent_binder.binder.property;
            ctrl.getUserBinderSuccess({ data: agent_binder });

            expect(ctrl.agent_binder).toEqual(agent_binder);
            expect(ctrl.agent_binder.user.first_name).toEqual("Agent First Name");
            expect(ctrl.agent_binder.user.last_name).toEqual("Agent Last Name");
            expect(ctrl.binder).toEqual(agent_binder.binder);
            expect(ctrl.client).toEqual(client);
            expect(ctrl.partner).toEqual(agent_binder.binder.partner_binders[0].partner);
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.showForm).toHaveBeenCalled();
        });
    });

    describe('ctrl.getUserBinderError', function() {
        it('calls $log, notify, and loading.close', function() {
            spyOn(loading, "close");
            spyOn(notify, "error");
            spyOn($log, "error");
            spyOn(api.userBinder, "find_by_repair_pricer_token").and.returnValue($q.when({
                data: agent_binder
            }));

            createController();
            ctrl.getUserBinderError({ data: "error" });

            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.close).toHaveBeenCalled();
        });
    });

    describe('ctrl.showForm', function() {
        it('calls RepairPricerModal.show_agent_form', function() {
            spyOn(RepairPricerModal, "show_agent_form");
            spyOn(api.userBinder, "find_by_repair_pricer_token").and.returnValue($q.when({
                data: agent_binder
            }));

            createController();
            ctrl.agent_binder = {
                user: {},
                access_repair_pricer: false
            };
            ctrl.binder = {};
            ctrl.partner = {};
            ctrl.showForm();

            expect(RepairPricerModal.show_agent_form).toHaveBeenCalled();
        });
    });
});