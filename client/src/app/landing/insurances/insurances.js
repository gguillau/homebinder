(function() {
	"use strict";

	angular.module('hb.landing.insurances', [])
		.config(['$stateProvider', insurancesLandingConfig])
		.controller("InsurancesLandingController", [
			"hb.landing.landingBase",
			InsurancesLandingController
		]);

	function InsurancesLandingController(LandingBaseController) {

		var Model = function() {
			// call the parent class
			LandingBaseController.call(this);
			this.type = "insurance";
		};

		Model.prototype = Object.create(LandingBaseController.prototype);

		this.model = new Model();
	}

	function insurancesLandingConfig($stateProvider) {
		$stateProvider
			.state('insurances_landing', {
				url: '^/insurances',
				templateUrl: 'landing/framework/landing.tpl.html',
				controller: "InsurancesLandingController",
				controllerAs: "ctrl"
			});
	}

})();