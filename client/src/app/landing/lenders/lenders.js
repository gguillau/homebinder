(function() {
	"use strict";

	angular.module('hb.landing.lenders', [])
		.config(['$stateProvider', lendersLandingConfig])
		.controller("LendersLandingController", [
			"hb.landing.landingBase",
			LendersLandingController
		]);

	function LendersLandingController(LandingBaseController) {

		var Model = function() {
			// call the parent class
			LandingBaseController.call(this);
			this.type = "lender";
			this.firstPrice = "$20";
			this.secondPrice = "$70";
		};

		Model.prototype = Object.create(LandingBaseController.prototype);

		this.model = new Model();
	}

	function lendersLandingConfig($stateProvider) {
		$stateProvider
			.state('lenders_landing', {
				url: '^/lenders',
				templateUrl: 'landing/framework/landing.tpl.html',
				controller: "LendersLandingController",
				controllerAs: "ctrl"
			});
	}

})();