describe("LendersLandingController", function() {
    var ctrl,
        controller,
        $log;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $log = $injector.get("$log");
    }));

    function createController() {
        ctrl = controller("LendersLandingController", {
            "$log": $log
        });
    }

    describe("init", function() {
        it("sets the firstPrice", function() {
            createController();
            expect(ctrl.model.firstPrice).toEqual("$20");
        });
    });
});