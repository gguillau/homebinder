describe("hb.landing.landingBase", function() {
    var TestClass,
        landingBase,
        $state,
        FreeTrialModal;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // get the base class under test
    beforeEach(inject(function($injector) {
        landingBase = $injector.get("hb.landing.landingBase");
        $state = $injector.get("$state");
        FreeTrialModal = $injector.get("FreeTrialModal");
    }));

    function createTestClass() {
        TestClass = function() {
            landingBase.call(this);
        };

        TestClass.prototype = Object.create(landingBase.prototype);

        return new TestClass();
    }

    describe("startTrial", function() {
        it("calls $state", inject(function($rootScope) {
            spyOn($state, "go");

            var model = createTestClass();
            model.startTrial();

            expect($state.go).toHaveBeenCalled();
        }));
    });

    describe("requestTrial", function() {
        it("calls FreeTrialModal show", inject(function($rootScope) {
            spyOn(FreeTrialModal, "show");

            var model = createTestClass();
            model.requestTrial();

            expect(FreeTrialModal.show).toHaveBeenCalled();
        }));
    });

});