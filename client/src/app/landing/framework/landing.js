angular
    .module("hb.landing.landingBase", [])
    .factory("hb.landing.landingBase", [
        "hb.resources",
        "$state",
        "FreeTrialModal",
        "Session",
        function(resources, $state, FreeTrialModal, Session) {
            var LandingBase = function() {
                this.type = null;
                this.resources = resources.genericLanding;
                this.resources = angular.extend({}, this.resources, resources.standardLanding);
                this.setLocale = Session.setLocale;
                this.firstPrice = "$5";
                this.secondPrice = "$35";
            };

            angular.extend(LandingBase.prototype, {

                startTrial: function() {
                    $state.go("partnerRegistration", {
                        type: this.type
                    });
                },

                requestTrial: function() {
                    FreeTrialModal.show();
                }
            });

            return LandingBase;
        }
    ]);