describe("HoasLandingController", function() {
    var ctrl,
        controller,
        $log;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $log = $injector.get("$log");
    }));

    function createController() {
        ctrl = controller("HoasLandingController", {
            "$log": $log
        });
    }

    describe("init", function() {
        it("sets the type", function() {
            createController();
            expect(ctrl.model.type).toEqual("hoa");
        });
    });
});