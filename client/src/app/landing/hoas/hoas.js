(function() {
	"use strict";

	angular.module('hb.landing.hoas', [])
		.config(['$stateProvider', hoasLandingConfig])
		.controller("HoasLandingController", [
			"hb.landing.landingBase",
			HoasLandingController
		]);

	function HoasLandingController(LandingBaseController) {

		var Model = function() {
			// call the parent class
			LandingBaseController.call(this);
			this.type = "hoa";
		};

		Model.prototype = Object.create(LandingBaseController.prototype);

		this.model = new Model();
	}

	function hoasLandingConfig($stateProvider) {
		$stateProvider
			.state('hoas_landing', {
				url: '^/hoas',
				templateUrl: 'landing/framework/landing.tpl.html',
				controller: "HoasLandingController",
				controllerAs: "ctrl"
			});
	}

})();