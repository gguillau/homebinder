(function() {
	"use strict";

	angular.module('hb.landing.managers', [])
		.config(['$stateProvider', managersLandingConfig])
		.controller("ManagersLandingController", [
			"hb.landing.landingBase",
			ManagersLandingController
		]);

	function ManagersLandingController(LandingBaseController) {

		var Model = function() {
			// call the parent class
			LandingBaseController.call(this);
			this.type = "manager";
		};

		Model.prototype = Object.create(LandingBaseController.prototype);

		this.model = new Model();
	}

	function managersLandingConfig($stateProvider) {
		$stateProvider
			.state('managers_landing', {
				url: '^/managers',
				templateUrl: 'landing/framework/landing.tpl.html',
				controller: "ManagersLandingController",
				controllerAs: "ctrl"
			});
	}

})();