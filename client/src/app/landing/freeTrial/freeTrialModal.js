(function() {
    "use strict";

    angular
        .module("hb.landing.freeTrial", [])
        .factory("FreeTrialModal", [
            "ModalService",
            function(Modals) {
                return {
                    show: function() {
                        Modals.show({
                            templateUrl: "landing/freeTrial/freeTrialModal.tpl.html",
                            controller: "FreeTrialModalController as ctrl"
                        });
                    }
                };
            }
        ])
        .controller("FreeTrialModalController", [
            "$modalInstance",
            "hb.api",
            "$log",
            "Notify",
            "Address",
            "hb.resources",
            FreeTrialModalController
        ]);

    function FreeTrialModalController($modal, api, $log, notify, Address, resources) {
        this.$modal = $modal;
        this.api = api;
        this.$log = $log;
        this.notify = notify;
        this.form = {
            phone: ""
        };
        this.loading = false;
        this.email_sent = false;
        this.countries = Address.countries();
        this.resources = resources.inspectorLanding;
        this.resources = angular.extend({}, this.resources, resources.genericLanding);
    }

    FreeTrialModalController.prototype = {

        requestTrial: function() {
            this.loading = true;
            this.api.freeTrial.create(this.form).then(
                angular.bind(this, this.requestSuccess),
                angular.bind(this, this.requestError));
        },

        requestSuccess: function(response) {
            this.loading = false;
            this.email_sent = true;
        },

        requestError: function(response) {
            this.loading = false;
            this.email_sent = false;
            this.$log.error(response);
            this.notify.error(response.data);
        },

        cancel: function() {
            this.$modal.dismiss("cancel");
        }
    };
})();