describe("FreeTrialModalController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        api,
        $q,
        $modalInstance,
        $log,
        notify,
        FreeTrialModal,
        ModalService;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        FreeTrialModal = _$injector_.get("FreeTrialModal");
        ModalService = _$injector_.get("ModalService");

        notify = {
            error: function(msg) {}
        };

        $log = {
            error: function(msg) {}
        };

        $modalInstance = {
            dismiss: function(msg) {}
        };

        api = {
            freeTrial: {
                create: function(opts) {}
            }
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("FreeTrialModalController", {
            "$modalInstance": $modalInstance,
            "hb.api": api,
            "$log": $log,
            "Notify": notify
        });

        $scope.ctrl = ctrl;
    }

    describe('ctrl.requestTrial', function() {

        it('should call freeTrial create function successfully', function() {

            spyOn(api.freeTrial, "create").and.returnValue($q.when({ data: "Email Sent." }));

            createController();
            $rootScope.$apply();
            ctrl.form = {
                name: "Test Agent",
                email: "testagentemail@gmail.com"
            };
            ctrl.requestTrial();
            $rootScope.$apply();

            expect(api.freeTrial.create).toHaveBeenCalledWith({ name: "Test Agent", email: "testagentemail@gmail.com" });
            expect(ctrl.email_sent).toBe(true);
        });

        it('should call freeTrial create function and return an error', function() {

            spyOn(api.freeTrial, "create").and.returnValue($q.reject({ data: "error" }));
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            $rootScope.$apply();
            ctrl.form = {
                name: "Test Agent",
                email: "testagentemail@gmail.com"
            };
            ctrl.requestTrial();
            $rootScope.$apply();

            expect(api.freeTrial.create).toHaveBeenCalledWith({ name: "Test Agent", email: "testagentemail@gmail.com" });
            expect(ctrl.email_sent).toBe(false);
            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(notify.error).toHaveBeenCalledWith("error");
        });
    });

    describe('ctrl.cancel', function() {

        it('should call $modalInstance dismiss function', function() {

            spyOn($modalInstance, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.cancel();
            $rootScope.$apply();

            expect($modalInstance.dismiss).toHaveBeenCalledWith('cancel');
        });
    });

    describe('FreeTrialModal', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            FreeTrialModal.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});