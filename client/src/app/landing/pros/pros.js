(function() {
	"use strict";

	angular.module('hb.landing.pros', [])
		.config(['$stateProvider', prosLandingConfig])
		.controller("ProsLandingController", [
			"hb.landing.landingBase",
			"hb.resources",
			ProsLandingController
		]);

	function ProsLandingController(LandingBaseController, resources) {

		var Model = function() {
			// call the parent class
			LandingBaseController.call(this);
			this.resources = resources.genericLanding;
			this.resources = angular.extend({}, this.resources, resources.standardLanding);
			this.resources = angular.extend({}, this.resources, resources.proLanding);
			this.type = "homepro";
		};

		Model.prototype = Object.create(LandingBaseController.prototype);

		this.model = new Model();
	}

	function prosLandingConfig($stateProvider) {
		$stateProvider
			.state('pros_landing', {
				url: '^/pros',
				templateUrl: 'landing/pros/pros.tpl.html',
				controller: "ProsLandingController",
				controllerAs: "ctrl"
			});
	}

})();