describe("HomeownerWelcomeController", function() {
    var ctrl,
        controller,
        RepairPricerModal,
        $log,
        api,
        notify,
        resources,
        loading,
        $state,
        $q,
        Event;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        RepairPricerModal = $injector.get("RepairPricerModal");
        $log = $injector.get("$log");
        $q = $injector.get("$q");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
        $state = $injector.get("$state");
        Event = $injector.get("Event");
    }));

    function createController() {
        ctrl = controller("HomeownerWelcomeController", {
            "RepairPricerModal": RepairPricerModal,
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "hb.resources": resources,
            "Loading": loading,
            "$state": $state,
            "Event": Event
        });
        
        var response = { data: { sign_in_count: 0, user_profile_attributes: { id: 1, default_locale: "en-us" } } };
        spyOn(api.user, "bindersWaitingById").and.returnValue($q.when(response));
    }

    describe("onRefreshSuccess", function() {
        it("calls $state go", function() {
            spyOn($state, "go");
            spyOn(loading, "close");
            createController();
            
            var response = { data: { sign_in_count: 1, user_profile_attributes: { id: 1, default_locale: "en-us" } } };
            ctrl.model.onRefreshSuccess(response);

            expect(ctrl.model.user).toEqual(response.data);
            expect(loading.close).toHaveBeenCalled();
            expect($state.go).toHaveBeenCalledWith("login");
        });

        it("calls bindersWaitingById", function() {
            createController();
            spyOn(ctrl.model, "bindersWaitingById");
            
            var response = { data: { sign_in_count: 0, user_profile_attributes: { id: 1, default_locale: "en-us" } } };
            ctrl.model.onRefreshSuccess(response);

            expect(ctrl.model.user).toEqual(response.data);
            expect(ctrl.model.bindersWaitingById).toHaveBeenCalled();
        });
    });

    describe("onRefreshError", function() {
        it("calls $state go", function() {
            spyOn($state, "go");
            spyOn(loading, "close");
            spyOn(notify, "info");

            createController();
            ctrl.model.onRefreshError({ data: "error" });

            expect(notify.info).toHaveBeenCalled();
            expect($state.go).toHaveBeenCalledWith("regHomeowner");
            expect(loading.close).toHaveBeenCalled();
        });
    });

    describe("bindersWaitingById", function() {
        it("calls api user bindersWaitingById", function() {
            createController();
            ctrl.model.user = { id: 1 };
            ctrl.model.bindersWaitingById();

            expect(api.user.bindersWaitingById).toHaveBeenCalledWith(1);
        });
    });

    describe("onBinderSuccess", function() {
        it("calls $state go when no binders exist", function() {
            spyOn(notify, "info");
            spyOn($state, "go");
            spyOn(loading, "close");

            createController();
            ctrl.model.onBinderSuccess({ data: { binders: [] } });

            expect(notify.info).toHaveBeenCalled();
            expect($state.go).toHaveBeenCalledWith("login");
            expect(loading.close).toHaveBeenCalled();
        });

        it("sets the binder when one binder exists", function() {
            spyOn(loading, "close");
            createController();
            
            var user = {id: 1, email: "homeowner@email.com"};
            ctrl.model.user = user;
            ctrl.model.onBinderSuccess({ data: { binders: [{id: 1}] } });

            expect(ctrl.model.binder).toEqual({id: 1});
            expect(ctrl.model.to).toEqual(user.email);
            expect(ctrl.model.partner_branding).toEqual(undefined);
            expect(ctrl.model.user_branding).toEqual(undefined);
            expect(loading.close).toHaveBeenCalled();
        });
        
        it("sets the binder when multiple binders exists", function() {
            spyOn(loading, "close");
            createController();
            
            var user = {id: 1, email: "homeowner@email.com"};
            ctrl.model.user = user;
            ctrl.model.onBinderSuccess({ data: { binders: [{id: 1}, {id: 2}] } });

            expect(ctrl.model.binder).toEqual({id: 1});
            expect(ctrl.model.to).toEqual(user.email);
            expect(ctrl.model.partner_branding).toEqual(undefined);
            expect(ctrl.model.user_branding).toEqual(undefined);
            expect(loading.close).toHaveBeenCalled();
        });
        
        it("sets the user branding", function() {
            spyOn(loading, "close");
            createController();
            
            var user = {id: 1, email: "homeowner@email.com"};
            ctrl.model.user = user;
            ctrl.model.onBinderSuccess({ data: { binders: [{id: 1}], branding: {full_role: "Homeowner"} } });

            expect(ctrl.model.binder).toEqual({id: 1});
            expect(ctrl.model.to).toEqual(user.email);
            expect(ctrl.model.partner_branding).toEqual(undefined);
            expect(ctrl.model.user_branding).toEqual({full_role: "Homeowner"});
            expect(loading.close).toHaveBeenCalled();
        });
        
        it("sets the user branding", function() {
            spyOn(loading, "close");
            createController();
            
            var user = {id: 1, email: "homeowner@email.com"};
            ctrl.model.user = user;
            ctrl.model.onBinderSuccess({ data: { binders: [{id: 1}], branding: {full_role: "Inspector"} } });

            expect(ctrl.model.binder).toEqual({id: 1});
            expect(ctrl.model.to).toEqual(user.email);
            expect(ctrl.model.partner_branding).toEqual({full_role: "Inspector"});
            expect(ctrl.model.user_branding).toEqual(undefined);
            expect(loading.close).toHaveBeenCalled();
        });
    });

    describe("onBinderError", function() {
        it("calls $state go when response.status is 404", function() {
            spyOn(loading, "close");
            spyOn(notify, "info");
            spyOn($state, "go");

            createController();
            ctrl.model.onBinderError({ data: "error", status: 404 });

            expect(loading.close).toHaveBeenCalled();
            expect(notify.info).toHaveBeenCalled();
            expect($state.go).toHaveBeenCalled();
        });

        it("calls $log error when response.status is not 404", function() {
            spyOn(loading, "close");
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            ctrl.model.onBinderError({ data: "error", status: 401 });

            expect(loading.close).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalledWith({ data: "error", status: 401 });
            expect(notify.error).toHaveBeenCalledWith("error");
        });
    });
    
    describe("onSubmitSuccess", function() {
        it("goes to step 2 if the binder was transferred", function() {
            createController();
            spyOn(ctrl.model, "getMaintenanceAndPartnerBinder");
            
            ctrl.model.user = {create_method: "transfer"};
            ctrl.model.onSubmitSuccess();

            expect(ctrl.model.step).toEqual(2);
            expect(ctrl.model.getMaintenanceAndPartnerBinder).toHaveBeenCalled();
        });
        
        it("goes to binders if the binder was shared", function() {
            spyOn(loading, "close");
            createController();
            spyOn(ctrl.model, "goToBinder");
            
            ctrl.model.user = {create_method: "share"};
            ctrl.model.onSubmitSuccess();

            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.model.goToBinder).toHaveBeenCalled();
        });
    });
    
    describe("getMaintenanceAndPartnerBinder", function() {
        it("gets maintenance items and partner binders", function() {
            createController();
            spyOn(api.maintenanceItem, "all").and.returnValue($q.when({ data: {} }));
            spyOn(api.partnerBinder, "all").and.returnValue($q.when({ data: {} }));
            
            ctrl.model.binder = {id: 1};
            ctrl.model.getMaintenanceAndPartnerBinder();

            expect(api.maintenanceItem.all).toHaveBeenCalledWith({binderId: 1, count: 100});
            expect(api.partnerBinder.all).toHaveBeenCalledWith({binder_id: 1});
        });
    });
    
    describe("maintenanceItemsSuccess", function() {
        it("sets the maintenance items to checked", function() {
            createController();
            
            var maintenanceItems = [{checked: false}, {checked: 1}, {}];
            ctrl.model.maintenanceItemsSuccess({data: {items: maintenanceItems}});

            expect(ctrl.model.maintenanceItems).toEqual([{checked: true}, {checked: true}, {checked: true}]);
        });
    });
    
    describe("partnerBinderSuccess", function() {
        it("does nothing when no partner binders are retrieved", function() {
            createController();
            
            ctrl.model.partnerBinderSuccess({data: {items: []}});

            expect(ctrl.model.partner_binder).toEqual(undefined);
            expect(ctrl.model.partner).toEqual(undefined);
            expect(ctrl.model.canOrderRp).toEqual(false);
        });
        
        it("sets the partner", function() {
            createController();
            
            ctrl.model.binder = {access_repair_pricer: false};
            ctrl.model.partnerBinderSuccess({data: {items: [{partner: {id: 1, configuration: {}}}, {partner: {id: 2, configuration: {}}}]}});

            expect(ctrl.model.partner_binder).toEqual({partner: {id: 1, configuration: {}}});
            expect(ctrl.model.partner).toEqual({id: 1, configuration: {}});
            expect(ctrl.model.canOrderRp).toEqual(false);
            expect(ctrl.model.resources.repairPricerTitle).toEqual(resources.homeownerOnboarding.repairPricerTitle);
            expect(ctrl.model.resources.repairPricerSubtitle).toEqual(resources.homeownerOnboarding.repairPricerSubtitle);
        });
        
        it("keeps canOrderRp false when repair pricer is not enabled", function() {
            createController();
            
            var partner = {
                configuration: {
                    repair_pricer_enabled: false,
                    repair_pricer_messaging_enabled: true
                }
            };
            ctrl.model.binder = {access_repair_pricer: true};
            ctrl.model.partnerBinderSuccess({data: {items: [{partner: partner}]}});

            expect(ctrl.model.canOrderRp).toEqual(false);
        });
        
        it("keeps canOrderRp false when repair pricer messaging is not enabled", function() {
            createController();
            
            var partner = {
                configuration: {
                    repair_pricer_enabled: true,
                    repair_pricer_messaging_enabled: false
                }
            };
            ctrl.model.binder = {access_repair_pricer: true};
            ctrl.model.partnerBinderSuccess({data: {items: [{partner: partner}]}});

            expect(ctrl.model.canOrderRp).toEqual(false);
        });
        
        it("keeps canOrderRp false when access repair pricer is false", function() {
            createController();
            
            var partner = {
                configuration: {
                    repair_pricer_enabled: true,
                    repair_pricer_messaging_enabled: true
                }
            };
            ctrl.model.binder = {access_repair_pricer: false};
            ctrl.model.partnerBinderSuccess({data: {items: [{partner: partner}]}});

            expect(ctrl.model.canOrderRp).toEqual(false);
        });
        
        it("sets canOrderRp to true when RP is enabled, messaging is on, and the binder has access", function() {
            createController();
            
            var partner = {
                configuration: {
                    repair_pricer_enabled: true,
                    repair_pricer_messaging_enabled: true
                }
            };
            ctrl.model.binder = {access_repair_pricer: true};
            ctrl.model.partnerBinderSuccess({data: {items: [{partner: partner}]}});

            expect(ctrl.model.canOrderRp).toEqual(true);
        });
        
        it("sets the prepaid RP", function() {
            createController();
            
            var partner = {
                configuration: {
                    repair_pricer_pre_paid_reports: true
                }
            };
            ctrl.model.binder = {access_repair_pricer: true};
            ctrl.model.partnerBinderSuccess({data: {items: [{partner: partner}]}});

            expect(ctrl.model.resources.repairPricerTitle).toEqual(resources.homeownerOnboarding.repairPricerTitlePrepaid);
            expect(ctrl.model.resources.repairPricerSubtitle).toEqual(resources.homeownerOnboarding.repairPricerSubtitlePrepaid);
        });
    });
    
    describe("submitMaintenanceForm", function() {
        it("deletes no maintenance items when all are checked", function() {
            createController();
            spyOn(api.maintenanceItem, "destroy").and.returnValue($q.when({ data: {} }));
            spyOn(ctrl.model, "stepThree");
            
            ctrl.model.maintenanceItems = [{checked: true}, {checked: true}];
            ctrl.model.submitMaintenanceForm();

            expect(api.maintenanceItem.destroy).not.toHaveBeenCalled();
            expect(ctrl.model.stepThree).toHaveBeenCalled();
        });
        
        it("deletes the maintenance items", function() {
            createController();
            spyOn(api.maintenanceItem, "destroy").and.returnValue($q.when({ data: {} }));
            spyOn(ctrl.model, "stepThree");
            
            ctrl.model.maintenanceItems = [{checked: false}, {checked: false}];
            ctrl.model.submitMaintenanceForm();

            expect(api.maintenanceItem.destroy.calls.count()).toEqual(2);
        });
    });
    
    describe("stepThree", function() {
        it("sets step to 3 and does not go to binders", function() {
            createController();
            spyOn(loading, "close");
            spyOn(ctrl.model, "goToBinder");
            
            ctrl.model.canOrderRp = true;
            ctrl.model.stepThree();

            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.model.step).toEqual(3);
            expect(ctrl.model.goToBinder).not.toHaveBeenCalled();
        });

        it("calls goToBinder", function() {
            createController();
            spyOn(loading, "close");
            spyOn(ctrl.model, "goToBinder");
            
            ctrl.model.canOrderRp = false;
            ctrl.model.stepThree();

            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.model.goToBinder).toHaveBeenCalled();
        });
    });
    
    describe("showModal", function() {
        it("shows the repair pricer modal", function() {
            createController();
            spyOn(RepairPricerModal, "show");
            spyOn(Event, "create");
            
            ctrl.model.user = {
                user_profile_attributes: {
                    address_attributes: {}
                }
            };
            ctrl.model.binder = {
                property: {country: "US"}
            };
            ctrl.model.partner = {configuration: {}};
            ctrl.model.showModal();

            expect(ctrl.model.user.user_profile_attributes.address_attributes).toEqual({country: "US"});
            expect(RepairPricerModal.show).toHaveBeenCalled();
            expect(Event.create.calls.count()).toEqual(2);
        });
    });
    
    describe("goToBinder", function() {
        it("calls afterLogin.go", function() {
            createController();
            spyOn(ctrl.model.afterLogin, "go");
            
            ctrl.model.goToBinder();

            expect(ctrl.model.goingToBinders).toEqual(true);
            expect(ctrl.model.afterLogin.go).toHaveBeenCalled();
        });
    });
    
    describe("onError", function() {
        it("calls $log, notify, and loading", function() {
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "close");

            createController();
            ctrl.model.onError({ data: "error"});

            expect($log.error).toHaveBeenCalledWith({ data: "error"});
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.close).toHaveBeenCalled();
        });
    });
});
describe('homeownerWelcome', function() {
    var $scope, $compile, api, $stateParams, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $stateParams = _$injector_.get("$stateParams");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        $stateParams.accessToken = 1;

        spyOn(api.user, "welcome").and.returnValue($q.when({ data: { id: 1, sign_in_count: 1, user_profile_attributes: { id: 1, default_locale: "en-us" } } }));

        $compile('<homeowner-welcome></homeowner-welcome>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls api user welcome', function() {
            expect(api.user.welcome).toHaveBeenCalled();
        });
    });
});