(function() {
	"use strict";

	angular
		.module("hb.homeowners.welcome", [])
		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("homeowners.welcome", {
					url: "^/homeowners/:accessToken/welcome",
					template: "<homeowner-welcome></homeowner-welcome>"
				});
		}])
		.directive("homeownerWelcome", function() {
			return {
				restrict: "E",
				templateUrl: "homeowners/welcome/welcome.tpl.html",
				controller: "HomeownerWelcomeController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("HomeownerWelcomeController", [
			"UsersWelcomeController",
			"RepairPricerModal",
			"hb.resources",
			"$q",
			"Loading",
			"Notify",
			"$log",
			"Event",
			HomeownerWelcomeController
		]);

	function HomeownerWelcomeController(UsersWelcomeController, RepairPricerModal, resources, $q, loading, notify, $log, event) {
		var Model = function() {
			// call the parent class
			UsersWelcomeController.call(this);
			this.resources = resources.homeownerOnboarding;
			this.resources = angular.extend({}, this.resources, resources.userWelcome);
			this.$q = $q;
			this.loading = loading;
			this.notify = notify;
			this.$log = $log;
			this.canOrderRp = false;
			this.goingToBinders = false;
			this.step = 1;

			this.refresh();
		};

		Model.prototype = Object.create(UsersWelcomeController.prototype);

		angular.extend(Model.prototype, {
			// refresh success
			onRefreshSuccess: function(response) {
				this.user = response.data;
				this.user.user_profile_attributes.default_locale = this.default_locale;

				if (response.data.sign_in_count > 0) {
					this.loading.close();
					this.$state.go("login");
				}
				else {
					this.bindersWaitingById();
				}
			},

			// refresh error
			onRefreshError: function(response) {
				this.notify.info(this.resources.loadingHomeownerError, 10000);
				this.$state.go("regHomeowner");
				this.loading.close();
			},

			// get waiting_binder by user_id
			bindersWaitingById: function() {
				this.api.user.bindersWaitingById(this.user.id).then(
					angular.bind(this, this.onBinderSuccess),
					angular.bind(this, this.onBinderError)
				);
			},

			// get binder success
			onBinderSuccess: function(response) {
				this.binders = response.data.binders;
				var branding = response.data.branding;

				if (this.binders.length > 0) {
					this.binder = this.binders[0];

					if (branding && branding.full_role != "Homeowner") {
						this.partner_branding = branding;
					}
					else if (branding) {
						this.user_branding = branding;
					}

					this.to = this.user.email;
				}
				else {
					this.notify.info(this.resources.noBinders);
					this.$state.go("login");
				}
				
				this.loading.close();
			},

			// get binder error
			onBinderError: function(response) {
				this.loading.close();
				if (response.status == 404) {
					this.notify.info(this.resources.noBinders);
					this.$state.go("login");
				}
				else {
					this.$log.error(response);
					this.notify.error(response.data);
				}
			},

			// first form (password form) submitted successfully
			onSubmitSuccess: function(response) {
				if (this.user.create_method === "transfer") {
					this.step = 2;
					this.getMaintenanceAndPartnerBinder();
				}
				else {
					this.loading.close();
					this.goToBinder();
				}
			},

			// get maintenace items and partner binder
			getMaintenanceAndPartnerBinder: function() {
				var promises = [];
				promises.push(
					this.api.maintenanceItem.all({binderId: this.binder.id, count: 100}).then(
						angular.bind(this, this.maintenanceItemsSuccess),
						angular.bind(this, this.onError)),

					this.api.partnerBinder.all({binder_id: this.binder.id}).then(
						angular.bind(this, this.partnerBinderSuccess),
						angular.bind(this, this.onError))			
				);

				this.$q.all(promises).then(
					angular.bind(this, function() {this.loading.close();}),
					angular.bind(this, this.onError)
				);
			},

			// get maintenance items success
			maintenanceItemsSuccess: function(response) {
				this.maintenanceItems = response.data.items;
				for (var i = 0; i < this.maintenanceItems.length; i++) {
					this.maintenanceItems[i].checked = true;
				}
			},

			// get partner binder success
			partnerBinderSuccess: function(response) {
				if (response.data.items.length > 0) {
					this.partner_binder = response.data.items[0];
					this.partner = this.partner_binder.partner;

					if (this.partner) {
						if (this.partner.configuration.repair_pricer_enabled && this.partner.configuration.repair_pricer_messaging_enabled && this.binder.access_repair_pricer) {
							this.canOrderRp = true;
						}
						if (this.partner.configuration.repair_pricer_pre_paid_reports) {
							this.resources.repairPricerTitle = this.resources.repairPricerTitlePrepaid;
							this.resources.repairPricerSubtitle = this.resources.repairPricerSubtitlePrepaid;
						}
					}
				}
			},

			// second form (maintenace form) submitted successfully
			submitMaintenanceForm: function() {
				var promises = [];
				
				this.maintenanceItems.forEach(angular.bind(this, function(item) {
					// if the item isn't checked, call api to delete it
					if (!item.checked) {
						promises.push(
							this.api.maintenanceItem.destroy(item.id).then(
								function() {},
								angular.bind(this, this.onError))
						);
					}
				}));

				if (promises.length > 0) {
					this.loading.show(this.resources.oneMoment);
					this.$q.all(promises).then(
						angular.bind(this, this.stepThree)
					);
				}
				else {
					this.stepThree();
				}
			},

			// go to repair pricer form if applicable, else binder
			stepThree: function(response) {
				this.loading.close();
				if (this.canOrderRp) {
					this.step = 3;
				}
				else {
					this.goToBinder();
				}
			},

			// show repair pricer modal
			showModal: function() {
				this.user.user_profile_attributes.address_attributes = this.binder.property;
				RepairPricerModal.show({
					user: this.user,
					binder_id: this.binder.id,
					partner_id: this.partner.id,
					report_type: this.partner.configuration.default_repair_pricer_report_type,
					repair_pricer_pre_paid_reports: this.partner.configuration.repair_pricer_pre_paid_reports,
					closed: angular.bind(this, this.goToBinder)
				});

				event.create({ event_type: "repair_pricer", event_name: "widget_order_now_clicked" });
				event.create({ event_name: "widget_order_now_clicked", event_type: "engagement", user_id: this.user.id, user_role: this.user.role, user_created_at: this.user.created_at, user_create_method: this.user.create_method });
			},
			
			goToBinder: function() {
				this.goingToBinders = true;
				this.afterLogin.go();
			},

			// on error for API calls
			onError: function(response) {
				this.$log.error(response);
				this.notify.error(response.data);
				this.loading.close();
			}

		});

		this.model = new Model();
	}
})();