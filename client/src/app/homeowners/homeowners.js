(function(){
    "use strict";
    
    angular.module("hb.homeowners", [
		"hb.homeowners.claimBinder",
		"hb.homeowners.registration",
		"hb.homeowners.welcome",
		"hb.homeowners.decline",
		"hb.homeowners.onboard",
		"hb.homeowners.repairPricer"
    ])
	.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("homeowners", {
					abstract: true,
					template: "<div ui-view></div>"
				});
		}]);
})();
