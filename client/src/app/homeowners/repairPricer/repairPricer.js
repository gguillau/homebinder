(function() {
    "use strict";

    angular.module("hb.homeowners.repairPricer", [])
        .factory("RepairPricerModal", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "homeowners/repairPricer/repairPricer.tpl.html",
                            controller: "RepairPricerModalController as ctrl",
                            resolveData: opts,
                            backdrop: "static"
                        });
                    },
                    show_agent_form: function(opts) {
                        Modals.show({
                            templateUrl: "homeowners/repairPricer/repairPricerAgentForm.tpl.html",
                            controller: "RepairPricerModalController as ctrl",
                            resolveData: opts
                        });
                    }
                };
            }
        ])
        .controller("RepairPricerModalController", [
            "data",
            "$modalInstance",
            "Event",
            "hb.resources",
            "Address",
            "hb.api",
            "Notify",
            "Session",
            RepairPricerModalController
        ]);

    function RepairPricerModalController(data, $modal, event, resources, Address, api, notify, session) {
        this.$modal = $modal;
        this.user = data.user;
        this.binder_id = data.binder_id;
        this.partner_id = data.partner_id;
        this.report_type = data.report_type;
        this.buyer_agent = data.buyer_agent;
        this.event = event;
        this.resources = resources.homeownerOnboard;
        this.resources = angular.extend({}, this.resources, resources.usersForm);
        this.resources = angular.extend({}, this.resources, resources.repairPricerModal);
        this.address = Address;
        this.api = api;
        this.country = this.address.findCountryByCode(this.user.user_profile_attributes.address_attributes.country);
        this.states = this.address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
            return state.country.value === this.country.value;
        }));
        this.reportOptions = {
            pool: false,
            history: false,
            rush: false
        };
        this.datepicker = {
            format: "MMMM dd, yyyy",
            options: {
                "show-button-bar": false
            }
        };
        this.date_opened = false;
        this.step = "one";
        this.documents = [];
        this.notify = notify;
        this.uploader = {
            fileTypes: "documents",
            hideUploadButton: true,
            showTable: false,
            selectButtonLabel: this.resources.browseButton,
            id: "uploadDocuments",
            hide_upload: false,
            uploadOnAdd: false,
            multiSelect: false
        };
        this.poolUploader = {
            fileTypes: "documents",
            hideUploadButton: true,
            showTable: false,
            selectButtonLabel: this.resources.browseButton,
            id: "uploadPool",
            hide_upload: false,
            uploadOnAdd: false,
            multiSelect: false
        };
        this.repair_pricer_costs = {
            report: data.repair_pricer_pre_paid_reports ? 0 : 49.00,
            pool: 0,
            history: 0,
            rush: 0
        };
        this.inspectionReport = null;
        this.poolReport = null;
        this.closing_date = new Date();
        this.total = 0;
        this.document_id = null;
        this.creditCardConfig = {
            onReceiveToken: angular.bind(this, this.order),
            onError: angular.bind(this, this.onError)
        };
        if (data.no_login) {
            this.currentUser = this.buyer_agent;
            this.creator = this.buyer_agent;
        }
        else {
            this.currentUser = session.getUser();
            if (this.currentUser.role === "admin") {
                this.creator = this.user;
            }
            else {
                this.creator = this.currentUser;
            }
        }
        this.processing = false;
        this.userLookupCfg = {
            placeholder: "Search for a user",
            refresh: angular.bind(this, this.userLookupRefresh),
            itemTemplate: "components/lookup/user/userDropDownItem.tpl.html",
            selectionTemplate: "components/lookup/user/userSelection.tpl.html",
            allowClear: false
        };
        this.getBuyerAgent();
    }

    RepairPricerModalController.prototype = {
        userLookupRefresh: function(value) {
            var opts = {
                page: 1,
                count: 50,
                search: value
            };
            opts.search = value;
            return this.api.user.all(opts).then(
                angular.bind(this, function(response) {
                    return {
                        data: response.data.items
                    };
                })
            );
        },

        getBuyerAgent: function() {
            var default_agent = {
                    first_name: "",
                    last_name: "",
                    email: ""
                };
            
            if (!this.buyer_agent) {
                this.buyer_agent = default_agent;
            }

            if (this.binder_id && (this.buyer_agent === default_agent)) {
                this.api.userBinder.all({ binder_id: this.binder_id, role: "buyer_agent" }).then(
                    angular.bind(this, this.getAgentSuccess),
                    angular.bind(this, this.onError));
            }
        },

        getAgentSuccess: function(response) {
            var agents = response.data.items;
            if (agents.length > 0) {
                this.buyer_agent = {
                    first_name: agents[0].user.user_profile_attributes.first_name,
                    last_name: agents[0].user.user_profile_attributes.last_name,
                    email: agents[0].user.email
                };
            }
        },

        selectDocument: function(docu) {
            this.documents.forEach(function(doc) {
                if (docu.id != doc.id) {
                    doc.selected = false;
                }
            });
            docu.selected = true;
            this.document_id = docu.id;
            this.next("three");
        },

        next: function(step) {
            var previous = this.step;
            this.step = step;

            if (previous === "two" && this.step === "three" && !this.reportOptions.pool) {
                this.step = "four";
            }

            if (previous === "four" && this.step === "three" && !this.reportOptions.pool) {
                this.step = "two";
            }

            if (this.step === "two") {
                this.getDocuments();
            }

            if (previous == "three" && this.step === "two" && this.document_id) {
                this.document_id = null;
            }
            else if (previous == "three" && this.step === "two" && this.inspectionReport) {
                this.uploader.api.remove(this.inspectionReport);
            }
            else if (previous == "four" && this.step === "three" && this.poolReport) {
                this.poolUploader.api.remove(this.poolReport);
            }
            else if (this.step === "four") {
                this.getInspectionReport();
                this.getPoolReport();
                this.calculateTotal();
            }
        },

        getDocuments: function() {
            if (this.currentUser.role === "homeowner") {
                this.api.document.all({ binderId: this.binder_id, count: 200 }).then(
                    angular.bind(this, this.getDocumentsSuccess),
                    angular.bind(this, this.onError));
            }
        },

        getInspectionReport: function() {
            var documents = this.uploader.api.pendingUploads();
            if (documents.length > 0) {
                this.inspectionReport = documents[0];
            }
        },

        getPoolReport: function() {
            if (this.poolUploader.api) {
                var documents = this.poolUploader.api.pendingUploads();
                if (documents.length > 0) {
                    this.poolReport = documents[0];
                }
            }
        },

        calculateTotal: function() {
            this.total = 0;
            this.repair_pricer_costs.pool = 0;
            this.repair_pricer_costs.history = 0;
            this.repair_pricer_costs.rush = 0;

            this.total += this.repair_pricer_costs.report;

            if (this.reportOptions.pool) {
                this.total += 29;
                this.repair_pricer_costs.pool = 29;
            }
            if (this.reportOptions.history) {
                this.total += 29;
                this.repair_pricer_costs.history = 29;
            }
            if (this.reportOptions.rush) {
                this.total += 49;
                this.repair_pricer_costs.rush = 49;
            }
        },

        getDocumentsSuccess: function(response) {
            var documents = response.data.items.map(function(doc) {
                return {
                    id: doc.id,
                    name: doc.name,
                    selected: false,
                    library_source_id: doc.library_source_id
                };
            });

            this.documents = documents.filter(function(docu) {
                if (!docu.library_source_id) {
                    return true;
                }
                return docu.library_source_id === (undefined || null);
            });
        },

        onError: function(error) {
            this.processing = false;
            if (error.data) {
                this.notify.error(error.data);
            }
            else {
                this.notify.error(error);
            }
        },

        getToken: function() {
            if (this.total > 0) {
                this.creditCardConfig.tokenize();
            }
            else {
                this.order();
            }
        },

        order: function(token) {
            if (!this.inspectionReport && !this.document_id) {
                this.notify.info(this.resources.reportRequired);
                return;
            }
            this.processing = true;
            // check if inspection report was selected or added
            var payload = {
                repair_pricer_report: {
                    client_first: this.user.user_profile_attributes.first_name,
                    client_last: this.user.user_profile_attributes.last_name,
                    client_email: this.user.email,
                    client_phone: this.user.user_profile_attributes.mobile_phone.national,
                    buyer_agent_first: this.buyer_agent.first_name,
                    buyer_agent_last: this.buyer_agent.last_name,
                    buyer_agent_email: this.buyer_agent.email,
                    binder_id: this.binder_id,
                    closing_date: this.closing_date,
                    partner_id: this.partner_id,
                    order_pool_report: this.reportOptions.pool,
                    order_home_history_report: this.reportOptions.history,
                    rush_report: this.reportOptions.rush,
                    report_type: this.report_type || "F",
                    creator_id: this.creator.id
                },
                address: {
                    address1: this.user.user_profile_attributes.address_attributes.address1,
                    address2: this.user.user_profile_attributes.address_attributes.address2,
                    city: this.user.user_profile_attributes.address_attributes.city,
                    state: this.user.user_profile_attributes.address_attributes.state,
                    zip: this.user.user_profile_attributes.address_attributes.zip,
                    country: this.user.user_profile_attributes.address_attributes.country
                },
                payment: {
                    card: token,
                    total: this.total
                },
                document_id: this.document_id
            };

            if (this.inspectionReport) {
                payload.repair_pricer_report.inspection_report = this.inspectionReport.based64;
            }

            if (this.poolReport) {
                payload.repair_pricer_report.pool_report = this.poolReport.based64;
            }

            if (this.total > 0) {
                payload.repair_pricer_report.payment_status = 1;
            }
            else {
                payload.repair_pricer_report.payment_status = 0;
            }

            this.api.repairPricerReport.create(payload).then(
                angular.bind(this, this.onCreateSuccess),
                angular.bind(this, this.onError));
        },

        onCreateSuccess: function(response) {
            this.notify.success(this.resources.createSuccess);
            this.step = "five";
        },

        close: function() {
            this.$modal.close();
        }
    };
})();
