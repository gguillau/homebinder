describe("RepairPricerModalController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        modals,
        RepairPricerModal,
        ModalService,
        api,
        $q,
        notify;

    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the Session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return {
                        role: "admin"
                    };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
    }));
    beforeEach(inject(function(_$controller_, _$injector_, _$templateCache_, _$compile_) {
        $rootScope = _$injector_.get("$rootScope");
        RepairPricerModal = _$injector_.get("RepairPricerModal");
        ModalService = _$injector_.get("ModalService");
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        notify = _$injector_.get("Notify");
        $controller = _$controller_;

        modals = {
            close: function(opts) {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("RepairPricerModalController", {
            "$modalInstance": modals,
            "data": {
                user: {
                    user_profile_attributes: {
                        address_attributes: {

                        },
                        mobile_phone: {

                        }
                    }
                }
            },
            "hb.api": api
        });

        $scope.ctrl = ctrl;
        $scope.$apply();
    }

    describe("ctrl.getBuyerAgent", function() {
        it("calls userBinder all when the agent does not exist", function() {
            createController();
            spyOn(api.userBinder, "all").and.returnValue($q.when({ data: {} }));
            
            var default_agent = {
                first_name: "",
                last_name: "",
                email: ""
            };
            ctrl.binder_id = 1;
            ctrl.buyer_agent = null;
            
            ctrl.getBuyerAgent();

            expect(ctrl.buyer_agent).toEqual(default_agent);
            expect(api.userBinder.all).toHaveBeenCalledWith({ binder_id: 1, role: "buyer_agent" });
        });
        
    it("calls userBinder all", function() {
            createController();
            spyOn(api.userBinder, "all").and.returnValue($q.when({ data: {} }));
            
            var agent = {
                first_name: "first",
                last_name: "last",
                email: "email"
            };
            ctrl.binder_id = 1;
            ctrl.buyer_agent = agent;
            
            ctrl.getBuyerAgent();

            expect(ctrl.buyer_agent).toEqual(agent);
            expect(api.userBinder.all).not.toHaveBeenCalled();
        });
    });

    describe("ctrl.getAgentSuccess", function() {
        it("sets the buyer_agent", function() {
            createController();

            ctrl.getAgentSuccess({ data: { items: [{ id: 1, user: { email: "agent@gmail.com", user_profile_attributes: { first_name: "First", last_name: "Last" } } }] } });

            expect(ctrl.buyer_agent.email).toEqual("agent@gmail.com");
        });
    });

    describe("ctrl.selectDocument", function() {
        it("calls next with three", function() {
            createController();
            ctrl.documents = [{ id: 1, selected: false }, { id: 2, selected: true }];
            ctrl.selectDocument({ id: 1 });

            expect(ctrl.document_id).toEqual(1);
        });
    });

    describe("ctrl.next", function() {
        it("sets step to four", function() {
            createController();
            spyOn(ctrl, "getInspectionReport");
            spyOn(ctrl, "getPoolReport");
            spyOn(ctrl, "calculateTotal");

            ctrl.step = "two";
            ctrl.next("four");

            expect(ctrl.step).toEqual("four");
            expect(ctrl.getInspectionReport).toHaveBeenCalled();
            expect(ctrl.getPoolReport).toHaveBeenCalled();
            expect(ctrl.calculateTotal).toHaveBeenCalled();
        });

        it("sets step to two", function() {
            createController();
            spyOn(ctrl, "getDocuments");

            ctrl.step = "four";
            ctrl.next("three");

            expect(ctrl.step).toEqual("two");
            expect(ctrl.getDocuments).toHaveBeenCalled();
        });

        it("sets step to two", function() {
            createController();
            spyOn(ctrl, "getDocuments");

            ctrl.step = "three";
            ctrl.document_id = 1;
            ctrl.next("two");

            expect(ctrl.step).toEqual("two");
            expect(ctrl.document_id).toBeNull();
        });

        it("sets step to two", function() {
            createController();
            spyOn(ctrl, "getDocuments");

            ctrl.step = "three";
            ctrl.inspectionReport = "data";
            ctrl.uploader = {
                api: {
                    remove: function() {}
                }
            };
            spyOn(ctrl.uploader.api, "remove");

            ctrl.next("two");

            expect(ctrl.step).toEqual("two");
            expect(ctrl.uploader.api.remove).toHaveBeenCalled();
        });

        it("sets step to two", function() {
            createController();
            spyOn(ctrl, "getDocuments");

            ctrl.step = "four";
            ctrl.poolReport = "data";
            ctrl.reportOptions.pool = true;
            ctrl.poolUploader = {
                api: {
                    remove: function() {}
                }
            };
            spyOn(ctrl.poolUploader.api, "remove");

            ctrl.next("three");

            expect(ctrl.step).toEqual("three");
            expect(ctrl.poolUploader.api.remove).toHaveBeenCalled();
        });
    });

    describe("ctrl.getDocuments", function() {
        it("calls document all", function() {
            createController();
            spyOn(api.document, "all").and.returnValue($q.when({ data: {} }));

            ctrl.currentUser = { role: "homeowner" };
            ctrl.getDocuments();

            expect(api.document.all).toHaveBeenCalled();
        });
    });

    describe("ctrl.getInspectionReport", function() {
        it("sets the inspection report", function() {
            createController();
            ctrl.uploader = {
                api: {
                    pendingUploads: function() {
                        return [{ id: 1 }];
                    }
                }
            };
            ctrl.getInspectionReport();

            expect(ctrl.inspectionReport).toEqual({ id: 1 });
        });
    });

    describe("ctrl.getPoolReport", function() {
        it("sets the inspection report", function() {
            createController();
            ctrl.poolUploader = {
                api: {
                    pendingUploads: function() {
                        return [{ id: 1 }];
                    }
                }
            };
            ctrl.getPoolReport();

            expect(ctrl.poolReport).toEqual({ id: 1 });
        });
    });

    describe("ctrl.calculateTotal", function() {
        it("calculates the total report cost", function() {
            createController();
            ctrl.calculateTotal();

            expect(ctrl.total).toEqual(49);
        });
        it("calculates the total report cost", function() {
            createController();
            ctrl.reportOptions.pool = true;
            ctrl.reportOptions.history = true;
            ctrl.reportOptions.rush = true;
            ctrl.calculateTotal();

            expect(ctrl.total).toEqual(156);
        });
    });

    describe("ctrl.getDocumentsSuccess", function() {
        it("sets the documents", function() {
            createController();
            ctrl.getDocumentsSuccess({ data: { items: [{ id: 1 }] } });

            expect(ctrl.documents.length).toEqual(1);
        });

        it("sets the documents and filters out new homeowner guide", function() {
            createController();
            ctrl.getDocumentsSuccess({ data: { items: [{ id: 1, name: "New_Homeowner_Guide.pdf", library_source_id: 1 }] } });

            expect(ctrl.documents.length).toEqual(0);
        });
    });

    describe("ctrl.onError", function() {
        it("calls notify", function() {
            createController();
            spyOn(notify, "error");
            ctrl.onError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });

        it("calls notify", function() {
            createController();
            spyOn(notify, "error");
            ctrl.onError("error");

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("ctrl.getToken", function() {
        it("calls tokenize", function() {
            createController();
            ctrl.creditCardConfig = {
                tokenize: function() {}
            };
            spyOn(ctrl.creditCardConfig, "tokenize");
            ctrl.total = 1;
            ctrl.getToken();

            expect(ctrl.creditCardConfig.tokenize).toHaveBeenCalled();
        });

        it("calls order", function() {
            createController();
            spyOn(ctrl, "order");
            ctrl.total = 0;
            ctrl.getToken();

            expect(ctrl.order).toHaveBeenCalled();
        });
    });

    describe("ctrl.order", function() {
        it("calls notify", function() {
            createController();
            spyOn(notify, "info");
            ctrl.order();

            expect(notify.info).toHaveBeenCalled();
        });

        it("calls repairPricerReport create", function() {
            createController();
            ctrl.inspectionReport = "data";
            ctrl.poolReport = "data";
            ctrl.total = 1;
            spyOn(api.repairPricerReport, "create").and.returnValue($q.when({ data: {} }));

            ctrl.order("token");

            expect(api.repairPricerReport.create).toHaveBeenCalled();
        });

        it("calls repairPricerReport create", function() {
            createController();
            ctrl.inspectionReport = "data";
            ctrl.poolReport = "data";
            spyOn(api.repairPricerReport, "create").and.returnValue($q.when({ data: {} }));

            ctrl.order("token");

            expect(api.repairPricerReport.create).toHaveBeenCalled();
        });
    });

    describe("ctrl.onCreateSuccess", function() {
        it("sets the step to five", function() {
            createController();
            ctrl.onCreateSuccess({ data: { items: [{ id: 1 }] } });

            expect(ctrl.step).toEqual("five");
        });
    });

    describe("ctrl.close", function() {
        it("closes the modal", function() {
            createController();
            spyOn(modals, "close");

            ctrl.close();

            expect(modals.close).toHaveBeenCalled();

        });

    });

    describe('RepairPricerModal', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            RepairPricerModal.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });

});