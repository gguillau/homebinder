(function() {
    "use strict";

    angular
        .module("hb.homeowners.onboard", [
            "ui.router",
            "hb.components",
            "hb.api"
        ])
        .factory("HomeownerOnboard", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "homeowners/onboard/onboard.tpl.html",
                            controller: "HomeownerOnboardController as ctrl",
                            resolveData: opts,
                            backdrop: "static"
                        });
                    }
                };
            }
        ])
        .controller("HomeownerOnboardController", [
            "hb.api",
            "Notify",
            "$stateParams",
            "data",
            "Address",
            "$modalInstance",
            "hb.resources",
            "$window",
            "$sce",
            "RepairPricerModal",
            "Event",
            "Context",
            HomeownerOnboardController
        ]);

    function HomeownerOnboardController(api, notify, $stateParams, data, Address, $modal, resources, $window, $sce, RepairPricerModal, event, context) {
        this.$modal = $modal;
        this.user = data.user;
        this.api = api;
        this.notify = notify;
        this.$stateParams = $stateParams;
        this.address = Address;
        this.country = this.address.findCountryByCode(this.user.user_profile_attributes.address_attributes.country);
        this.states = this.address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
            return state.country.value === this.country.value;
        }));
        this.countries = this.address.countries();
        this.resources = resources.homeownerOnboard;
        this.resources = angular.extend({}, this.resources, resources.usersForm);
        this.step = "one";
        this.$window = $window;
        this.processing = false;
        this.transactions = [];
        this.$sce = $sce;
        this.RepairPricerModal = RepairPricerModal;
        this.event = event;
        this.context = context;
        context.getBinder().then(angular.bind(this, function(binder) {
            this.binder = binder;
            this.getPartnerBinder();
        }));
        this.canOrderRp = false;
    }

    HomeownerOnboardController.prototype = {

        getPartnerBinder: function() {
            if (this.$stateParams.binderId) {
                this.api.partnerBinder.all({ binder_id: this.$stateParams.binderId }).then(
                    angular.bind(this, this.partnerBinderSuccess),
                    angular.bind(this, this.onError));
            }
        },

        partnerBinderSuccess: function(response) {
            if (response.data.items.length > 0) {
                this.partner_binder = response.data.items[0];
                this.partner = this.partner_binder.partner;
                if (this.partner) {
                    if (this.partner.configuration.repair_pricer_enabled && this.partner.configuration.repair_pricer_messaging_enabled) {
                        this.canOrderRp = true;
                    }
                    if (this.partner.configuration.repair_pricer_pre_paid_reports) {
                        this.resources.repairPricerInfo = this.resources.freeRepairPricerInfo;
                        this.resources.repairPricerHelp = this.resources.freeRepairPricerHelp;
                    }
                }
            }
        },

        next: function() {
            this.step = "two";
        },

        complete: function() {
            this.form.$submitted = true;
            if (this.form.$invalid) {
                return;
            }

            this.processing = true;

            this.updatedItem = {
                email: this.user.email,
                role: this.user.role,
                completed_onboarding: true,
                user_profile_attributes: {
                    id: this.user.user_profile_attributes.id,
                    address_attributes: {
                        id: this.user.user_profile_attributes.address_attributes.id,
                        address1: this.user.user_profile_attributes.address_attributes.address1,
                        address2: this.user.user_profile_attributes.address_attributes.address2,
                        city: this.user.user_profile_attributes.address_attributes.city,
                        country: this.user.user_profile_attributes.address_attributes.country,
                        state: this.user.user_profile_attributes.address_attributes.state,
                        zip: this.user.user_profile_attributes.address_attributes.zip
                    },
                    bio: this.user.user_profile_attributes.bio,
                    company: this.user.user_profile_attributes.company,
                    dob: this.user.user_profile_attributes.dob,
                    first_name: this.user.user_profile_attributes.first_name,
                    home_phone: this.user.user_profile_attributes.home_phone,
                    last_name: this.user.user_profile_attributes.last_name,
                    message: this.user.user_profile_attributes.message,
                    mobile_phone: "+" + this.country.code + this.user.user_profile_attributes.mobile_phone.national,
                    monthly_email: this.user.user_profile_attributes.monthly_email,
                    sex: this.user.user_profile_attributes.sex,
                    website: this.user.user_profile_attributes.website
                }
            };

            this.api.user.update(this.user.id, this.updatedItem).then(
                angular.bind(this, this.onSuccess),
                angular.bind(this, this.onError));
        },

        onSuccess: function(response) {
            this.user = response.data;
            this.notify.success(this.resources.updated);
            if (this.user.create_method === "transfer" && this.canOrderRp && this.binder.permissions.access_repair_pricer) {
                // show repair pricer information
                this.step = "three";
            }
            else {
                this.$modal.close();
            }
        },

        onError: function(response) {
            this.notify.error(response.data);
            this.processing = false;
        },

        orderNow: function() {
            this.user.user_profile_attributes.address_attributes = this.binder.property;
            this.RepairPricerModal.show({
                user: this.user,
                binder_id: this.binder.id,
                partner_id: this.partner.id,
                report_type: this.partner.configuration.default_repair_pricer_report_type,
                repair_pricer_pre_paid_reports: this.partner.configuration.repair_pricer_pre_paid_reports
            });

            this.$modal.close();

            this.event.create({ event_name: "onboarding_order_now_clicked", event_type: "repair_pricer", user_id: this.user.id, user_role: this.user.role, user_created_at: this.user.user_created_at, partner_id: this.partner.id, partner_type: this.partner.partner_type });
            this.event.create({ event_name: "onboarding_order_now_clicked", event_type: "engagement", user_id: this.user.id, user_role: this.user.role, user_created_at: this.user.created_at, user_create_method: this.user.create_method });
        },

        close: function() {
            this.$modal.close();
        },

        createEvent: function() {
            var event = {
                event_name: "learn_more_clicked",
                event_type: "repair_pricer"
            };
            this.event.create(event);
        },

        setLink: function() {
            return this.partner.configuration.repair_pricer_pre_paid_reports ? "https://form.jotform.com/81105892377159?" : "https://form.jotform.com/80565948184166?";
        },

        onSelect: function(item) {
            if (item) {
                this.country = item.country;
                this.user.user_profile_attributes.address_attributes.country = item.country.value;
            }
        },

        onSelectCountry: function(item) {
            if (item) {
                this.country = item;
                this.user.user_profile_attributes.address_attributes.country = item.value;
                this.states = this.address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                    return state.country.value === this.country.value;
                }));
            }
        }
    };
})();
