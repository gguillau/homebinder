describe("HomeownerOnboardController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        data,
        ctrl,
        notify,
        modals,
        api,
        address,
        RepairPricerModal,
        event,
        HomeownerOnboard,
        ModalService,
        context;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$templateCache_, _$compile_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $window = _$injector_.get("$window");
        event = _$injector_.get("Event");
        address = _$injector_.get("Address");
        RepairPricerModal = _$injector_.get("RepairPricerModal");
        HomeownerOnboard = _$injector_.get("HomeownerOnboard");
        ModalService = _$injector_.get("ModalService");
        context = _$injector_.get("Context");

        data = {
            user: {
                email: "test@gmail.com",
                user_profile_attributes: {
                    address_attributes: {
                        id: 1
                    },
                    id: 1,
                    head_shot_file: null,
                    mobile_phone: {}
                }
            }
        };

        modals = {
            close: function(opts) {}
        };

        notify = {
            success: function(msg) {},
            error: function(msg) {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("HomeownerOnboardController", {
            "hb.api": api,
            "$modalInstance": modals,
            "Notify": notify,
            "Address": address,
            "data": data,
            "Context": context,
            "RepairPricerModal": RepairPricerModal
        });

        $scope.ctrl = ctrl;
        $scope.$apply();
    }

    beforeEach(function() {
        spyOn(context, "getBinder").and.returnValue($q.when({ id: 1, permissions: { access_repair_pricer: true } }));
    });

    describe("ctrl.getPartnerBinder", function() {
        it("calls binderTransaction all", function() {

            spyOn(api.partnerBinder, "all").and.returnValue($q.when({
                data: { items: [] }
            }));

            createController();
            ctrl.$stateParams.binderId = 1;
            ctrl.partner = {
                id: 1,
                configuration: {
                    repair_pricer_enabled: true,
                    repair_pricer_id: 1
                }
            };
            ctrl.getPartnerBinder();

            expect(api.partnerBinder.all).toHaveBeenCalled();

        });

    });

    describe("ctrl.partnerBinderSuccess", function() {
        it("sets canOrderRp to true", function() {
            createController();

            ctrl.partnerBinderSuccess({ data: { items: [{ id: 1, partner: { id: 1, configuration: { repair_pricer_enabled: true, repair_pricer_pre_paid_reports: true, repair_pricer_messaging_enabled: true } } }] } });

            expect(ctrl.canOrderRp).toBe(true);
        });
    });

    describe('ctrl.next', function() {

        it('should move increment current step and set next step', function() {

            createController();
            $rootScope.$apply();
            ctrl.next();
            $rootScope.$apply();

            expect(ctrl.step).toBe('two');
        });

    });

    describe("ctrl.complete", function() {
        it("does not call update", function() {

            spyOn(api.user, "update").and.returnValue($q.when({
                data: {}
            }));

            createController();
            $rootScope.$apply();
            ctrl.form = {
                $invalid: true,
                $submitted: true
            };

            ctrl.complete();
            $rootScope.$apply();

            expect(api.user.update).not.toHaveBeenCalled();

        });

        it("call update", function() {

            spyOn(api.user, "update").and.returnValue($q.when({
                data: {}
            }));

            createController();
            $rootScope.$apply();
            ctrl.form = {
                $submitted: true
            };

            ctrl.complete();
            $rootScope.$apply();

            expect(api.user.update).toHaveBeenCalled();

        });
    });

    describe("ctrl.onSuccess", function() {
        it("sets the step to three", function() {
            createController();
            ctrl.canOrderRp = true;
            ctrl.onSuccess({ data: { create_method: "transfer" } });

            expect(ctrl.step).toEqual("three");

        });

        it("closes the modal", function() {
            createController();
            spyOn(modals, "close");

            ctrl.onSuccess({ data: { create_method: "share" } });

            expect(modals.close).toHaveBeenCalled();

        });

    });

    describe("ctrl.onError", function() {
        it("calls notify", function() {
            createController();
            spyOn(notify, "error");

            ctrl.onError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();

        });

    });

    describe("ctrl.orderNow", function() {
        it("calls $window open", function() {
            createController();
            spyOn(RepairPricerModal, "show");
            ctrl.user = {
                user_profile_attributes: {
                    mobile_phone: {

                    },
                    address_attributes: {

                    }
                }
            };
            ctrl.partner = {
                id: 1,
                configuration: {
                    repair_pricer_enabled: true,
                    repair_pricer_id: 1
                }
            };
            ctrl.orderNow();

            expect(RepairPricerModal.show).toHaveBeenCalled();
        });

    });

    describe("ctrl.close", function() {
        it("closes the modal", function() {
            createController();
            spyOn(modals, "close");

            ctrl.close();

            expect(modals.close).toHaveBeenCalled();

        });

    });

    describe("ctrl.createEvent", function() {
        it("calls event create", function() {
            createController();
            spyOn(event, "create");

            ctrl.createEvent("test");

            expect(event.create).toHaveBeenCalled();
        });
    });

    describe("onSelect", function() {
        it("sets the country", function() {
            createController();
            var item = { country: {} };
            ctrl.onSelect(item);

            expect(ctrl.country).toEqual(item.country);
        });
    });

    describe('HomeownerOnboard', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            HomeownerOnboard.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});