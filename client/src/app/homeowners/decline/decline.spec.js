describe("HomeownerDeclineController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $state,
        $stateParams,
        notify,
        $log,
        api,
        loading,
        modal,
        resources;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        resources = _$injector_.get("hb.resources");
        $stateParams = {};

        modal = {
            confirm: function() {}
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        $state = {
            go: function(state, args) {}
        };

        api = {
            user: {
                optOut: function(id) {}
            }
        };

        $log = {
            error: function(msg) {}
        };

        notify = {
            error: function(msg) {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("HomeownerDeclineController", {
            "$state": $state,
            "$stateParams": $stateParams,
            "hb.api": api,
            "$log": $log,
            "Notify": notify,
            "Loading": loading,
            "ModalService": modal,
            "hb.resources": resources
        });

        $scope.ctrl = ctrl;

    }

    describe('ctrl.decline', function() {

        it('should call transfer decline function successfully', function() {

            $stateParams.userId = 100;
            spyOn(api.user, "optOut").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(modal, "confirm");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();
            ctrl.decline();
            $rootScope.$apply();

            expect(modal.confirm).toHaveBeenCalledWith({
                title: "Confirm Decline",
                cancelLabel: "Cancel",
                confirmLabel: "Decline",
                glyphicon: "glyphicon glyphicon-thumbs-down",
                message: "By clicking decline below, you will decline a free lifetime subscription to HomeBinder.",
                confirm: jasmine.any(Function),
                cancel: jasmine.any(Function)
            });
            expect(ctrl.declining).toEqual(false);
            expect(api.user.optOut).toHaveBeenCalledWith(100);
            expect(loading.show).toHaveBeenCalledWith("One moment please.");
            expect(loading.close).toHaveBeenCalled();

        });

        it('should call transfer decline and attempt to decline transfer but return an error', function() {

            $stateParams.userId = 100;
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn(api.user, "optOut").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            $rootScope.$apply();
            ctrl.decline();
            $rootScope.$apply();


            expect(ctrl.declining).toEqual(false);
            expect(api.user.optOut).toHaveBeenCalledWith(100);
            expect(loading.show).toHaveBeenCalledWith("One moment please.");
            expect(loading.close).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalledWith("error");
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
        });

    });

    describe('ctrl.welcome', function() {

        it('should call $state go and send user to welcome page', function() {

            $stateParams.userId = 100;
            spyOn($state, "go");

            createController();
            $rootScope.$apply();
            ctrl.welcome();
            $rootScope.$apply();

            expect($state.go).toHaveBeenCalledWith("homeowners.welcome", {
                userId: 100
            });

        });

    });
});

describe('homeownerDecline', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        element = $compile('<homeowner-decline></homeowner-decline>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the element', function() {
            expect(element.html()).toContain("We are sorry to see you go!");
        });
    });
});