(function() {
	"use strict";

	angular
		.module("hb.homeowners.decline", [
			"hb.components"
		])
		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("homeowners.decline", {
					url: "^/homeowners/:userId/decline",
					template: "<homeowner-decline></homeowner-decline>"
				});
		}])
		.directive("homeownerDecline", function() {
			return {
				restrict: "E",
				templateUrl: "homeowners/decline/decline.tpl.html",
				controller: "HomeownerDeclineController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("HomeownerDeclineController", [
			"$state",
			"$stateParams",
			"$log",
			"hb.api",
			"Notify",
			"Loading",
			"ModalService",
			"hb.resources",
			HomeownerDeclineController
		]);

	function HomeownerDeclineController($state, $stateParams, $log, api, notify, Loading, modal, resources) {
		this.$state = $state;
		this.$log = $log;
		this.api = api;
		this.notify = notify;
		this.loading = Loading;
		this.modal = modal;
		this.userId = $stateParams.userId;
		this.resources = resources.userDecline;
		this.declining = this.has_error = false;
		this.decliningMessage = this.resources.decliningMessage;
		this.sorryText = this.resources.sorryText;
		this.anyFriendsText = this.resources.anyFriendsText;
		this.givePart1Text = this.resources.givePart1Text;
		this.giftCodeText = this.resources.giftCodeText;
		this.givePart2Text = this.resources.givePart2Text;
		this.benefitsText = this.resources.benefitsText;
		this.questionsText = this.resources.questionsText;
		this.errorsText = this.resources.error;
		this.teamText = this.resources.teamText;
		this.year = new Date().getFullYear();
		this.showModal();
	}

	HomeownerDeclineController.prototype = {

		showModal: function() {
			this.modal.confirm({
				message: this.resources.declineConfirm,
				glyphicon: "glyphicon glyphicon-thumbs-down",
				title: "Confirm Decline",
				confirmLabel: "Decline",
				cancelLabel: "Cancel",
				confirm: angular.bind(this, this.decline),
				cancel: angular.bind(this, this.welcome)
			});
		},

		decline: function() {
			this.declining = true;
			this.loading.show(this.decliningMessage);
			this.api.user.optOut(this.userId).then(
				angular.bind(this, this.declined),
				angular.bind(this, this.declineError)
			);
		},

		declined: function(response) {
			this.declining = false;
			this.loading.close();
		},

		declineError: function(response) {
			this.has_error = true;
			this.declining = false;
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		welcome: function() {
			this.$state.go("homeowners.welcome", {
				userId: this.userId
			});
		}
	};
})();