describe("HomeownerRegistrationController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        notify,
        modals,
        $log,
        api,
        uploader,
        $window,
        $state,
        loading,
        AfterLogin,
        address,
        $templateCache,
        $compile,
        template,
        registerForm,
        propertyForm,
        BinderConfirm;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$templateCache_, _$compile_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        address = _$injector_.get("Address");
        notify = _$injector_.get("Notify");
        $compile = _$compile_;
        $templateCache = _$templateCache_;

        AfterLogin = {
            go: function() {}
        };

        loading = {
            show: function(msg) {},
            setMessage: function(msg) {},
            close: function() {}
        };

        api = {
            user: {
                register: function(info) {},
                bindersWaitingByEmail: function() {}
            },
            binder: {
                create: function(info) {}
            },
            maintenanceItem: {
                create: function(mi) {}
            },
            appliance: {
                create: function(app) {}
            },
            binderContractor: {
                create: function(bc) {}
            },
            project: {
                create: function(proj) {}
            }
        };

        $log = {
            error: function(msg) {}
        };

        uploader = {
            api: {
                uploadFiles: function() {},
                updateParams: function(params) {},
                pendingUploads: function() {},
                remove: function() {},
                setJwt: function(jwt) {}
            }
        };

        $window = {
            URL: {
                createObjectURL: function() {},
                revokeObjectURL: function() {}
            }
        };

        modals = {
            alert: function(opts) {}
        };

        BinderConfirm = {
            show: function() {}
        };

        $state = {
            go: function() {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("HomeownerRegistrationController", {
            "hb.api": api,
            "$window": $window,
            "$log": $log,
            "ModalService": modals,
            "Notify": notify,
            "$state": $state,
            "Loading": loading,
            "AfterLogin": AfterLogin,
            "Address": address,
            "BinderConfirmModal": BinderConfirm
        });
        ctrl.uploader = uploader;
        $scope.ctrl = ctrl;
        template = $templateCache.get("homeowners/registration/homeownersRegistration.tpl.html");
        $compile(template)($scope);
        registerForm = $scope.ctrl.registerForm;
        propertyForm = $scope.ctrl.propertyForm;
        $scope.$apply();
        $scope.$digest();

    }

    describe('registerForm', function() {

        it('should expect registerForm to be defined and form validation to be false due to missing first name', function() {

            createController();

            ctrl.info.first = "";
            ctrl.info.last = "Smith";
            ctrl.info.email = "bobsmith@gmail.com";
            ctrl.info.password = "password";
            ctrl.info.confirmPassword = "password";
            ctrl.info.country = "US";
            ctrl.country = { name: "United States" };
            ctrl.info.phone = "+16175002500";
            $rootScope.$apply();

            expect(registerForm).toBeDefined();
            expect(registerForm.$valid).toBe(false);

        });
        
        it('should expect registerForm to be defined and form validation to be false due to missing last name', function() {

            createController();

            ctrl.info.first = "Bob";
            ctrl.info.last = "";
            ctrl.info.email = "bobsmith@gmail.com";
            ctrl.info.password = "password";
            ctrl.info.confirmPassword = "password";
            ctrl.info.country = "US";
            ctrl.country = { name: "United States" };
            ctrl.info.phone = "+16175002500";
            $rootScope.$apply();

            expect(registerForm).toBeDefined();
            expect(registerForm.$valid).toBe(false);

        });
        
        it('should expect registerForm to be defined and form validation to be false due to missing email', function() {

            createController();

            ctrl.info.first = "Bob";
            ctrl.info.last = "Smith";
            ctrl.info.email = "";
            ctrl.info.password = "password";
            ctrl.info.confirmPassword = "password";
            ctrl.info.country = "US";
            ctrl.country = { name: "United States" };
            ctrl.info.phone = "+16175002500";
            $rootScope.$apply();

            expect(registerForm).toBeDefined();
            expect(registerForm.$valid).toBe(false);

        });
        
        it('should expect registerForm to be defined and form validation to be false due to missing password', function() {

            createController();

            ctrl.info.first = "Bob";
            ctrl.info.last = "Smith";
            ctrl.info.email = "bobsmith@gmail.com";
            ctrl.info.password = "";
            ctrl.info.confirmPassword = "password";
            ctrl.info.country = "US";
            ctrl.country = { name: "United States" };
            ctrl.info.phone = "+16175002500";
            $rootScope.$apply();

            expect(registerForm).toBeDefined();
            expect(registerForm.$valid).toBe(false);

        });
        
        it('should expect registerForm to be defined and form validation to be false due to missing confirm password', function() {

            createController();

            ctrl.info.first = "Bob";
            ctrl.info.last = "Smith";
            ctrl.info.email = "bobsmith@gmail.com";
            ctrl.info.password = "password";
            ctrl.info.confirmPassword = "";
            ctrl.info.country = "US";
            ctrl.country = { name: "United States" };
            ctrl.info.phone = "+16175002500";
            $rootScope.$apply();

            expect(registerForm).toBeDefined();
            expect(registerForm.$valid).toBe(false);

        });
        
        it('should expect registerForm to be defined and form validation to be false due to missing phone', function() {

            createController();

            ctrl.info.first = "Bob";
            ctrl.info.last = "Smith";
            ctrl.info.email = "bobsmith@gmail.com";
            ctrl.info.password = "password";
            ctrl.info.confirmPassword = "password";
            ctrl.info.country = "US";
            ctrl.country = { name: "United States" };
            ctrl.info.phone = "";
            $rootScope.$apply();

            expect(registerForm).toBeDefined();
            expect(registerForm.$valid).toBe(false);

        });

        it('should expect registerForm to be defined and form validation to be false due to long first name', function() {

            createController();

            ctrl.info.first = "3yktkypvhmjr7qhs7v7rqm5q68vhfza9ctvsytp6ckqv12bjv57";
            ctrl.info.last = "Smith";
            ctrl.info.email = "bobsmith@gmail.com";
            ctrl.info.password = "password";
            ctrl.info.confirmPassword = "password";
            ctrl.info.country = "US";
            ctrl.country = { name: "United States" };
            ctrl.info.phone = "";
            $rootScope.$apply();

            expect(registerForm).toBeDefined();
            expect(registerForm.$valid).toBe(false);

        });

        it('should expect registerForm to be defined and form validation to be false due to long last name', function() {

            createController();

            ctrl.info.first = "Bob";
            ctrl.info.last = "3yktkypvhmjr7qhs7v7rqm5q68vhfza9ctvsytp6ckqv12bjv57";
            ctrl.info.email = "bobsmith@gmail.com";
            ctrl.info.password = "password";
            ctrl.info.confirmPassword = "password";
            ctrl.info.country = "US";
            ctrl.country = { name: "United States" };
            ctrl.info.phone = "";
            $rootScope.$apply();

            expect(registerForm).toBeDefined();
            expect(registerForm.$valid).toBe(false);

        });

        it('should expect registerForm to be defined and form validation to be false due to short password', function() {

            createController();

            ctrl.info.first = "Bob";
            ctrl.info.last = "Smith";
            ctrl.info.email = "bobsmith@gmail.com";
            ctrl.info.password = "passwor";
            ctrl.info.confirmPassword = "passwor";
            ctrl.info.country = "US";
            ctrl.country = { name: "United States" };
            ctrl.info.phone = "";
            $rootScope.$apply();

            expect(registerForm).toBeDefined();
            expect(registerForm.$valid).toBe(false);

        });

        it('should expect registerForm to be defined and form validation to be false due to long password', function() {

            createController();

            ctrl.info.first = "Bob";
            ctrl.info.last = "Smith";
            ctrl.info.email = "bobsmith@gmail.com";
            ctrl.info.password = "916rzzn756oi087z9cvuumawxw1ahmsxkk07wursa7jkbwj39l0lkggx180utfn2u4u5nozlyogctrmiwoov5lgjd6iovh7g964ouvkj1sv43uatrzm6rh9rmgmkmz8kq";
            ctrl.info.confirmPassword = "916rzzn756oi087z9cvuumawxw1ahmsxkk07wursa7jkbwj39l0lkggx180utfn2u4u5nozlyogctrmiwoov5lgjd6iovh7g964ouvkj1sv43uatrzm6rh9rmgmkmz8kq";
            ctrl.info.country = "US";
            ctrl.country = { name: "United States" };
            ctrl.info.phone = "";
            $rootScope.$apply();

            expect(registerForm).toBeDefined();
            expect(registerForm.$valid).toBe(false);

        });
        
        it('should expect registerForm to be defined and form validation to be false when passwords are not equal', function() {

            createController();

            ctrl.info.first = "Bob";
            ctrl.info.last = "Smith";
            ctrl.info.email = "bobsmith@gmail.com";
            ctrl.info.password = "password";
            ctrl.info.confirmPassword = "pqssword";
            ctrl.info.country = "US";
            ctrl.country = { name: "United States" };
            ctrl.info.phone = "+16175002500";
            $rootScope.$apply();

            expect(registerForm).toBeDefined();
            expect(registerForm.$valid).toBe(false);

        });
        
        it('should expect registerForm to be defined and form validation to be false when passwords are equal according to regex', function() {

            createController();

            ctrl.info.first = "Bob";
            ctrl.info.last = "Smith";
            ctrl.info.email = "bobsmith@gmail.com";
            ctrl.info.password = "********";
            ctrl.info.confirmPassword = "12345678";
            ctrl.info.country = "US";
            ctrl.country = { name: "United States" };
            ctrl.info.phone = "+16175002500";
            $rootScope.$apply();

            expect(registerForm).toBeDefined();
            expect(registerForm.$valid).toBe(false);

        });
        
        it('should expect registerForm to be defined and form validation to be false when passwords are equal according to regex', function() {

            createController();

            ctrl.info.first = "Bob";
            ctrl.info.last = "Smith";
            ctrl.info.email = "bobsmith@gmail.com";
            ctrl.info.password = "\\w\\w\\w\\w";
            ctrl.info.confirmPassword = "abcd";
            ctrl.info.country = "US";
            ctrl.country = { name: "United States" };
            ctrl.info.phone = "+16175002500";
            $rootScope.$apply();

            expect(registerForm).toBeDefined();
            expect(registerForm.$valid).toBe(false);

        });

        it('should expect registerForm to be defined and form validation to be true due to valid form', function() {

            createController();

            ctrl.info.first = "Bob";
            ctrl.info.last = "Smith";
            ctrl.info.email = "bobsmith@gmail.com";
            ctrl.info.password = "password";
            ctrl.info.confirmPassword = "password";
            ctrl.info.country = "US";
            ctrl.country = { name: "United States" };
            ctrl.info.phone = "+16175002500";
            $rootScope.$apply();

            expect(registerForm).toBeDefined();
            expect(registerForm.$valid).toBe(true);

        });
        
    });

    describe('propertyForm', function() {

        it('should expect propertyForm to be defined and form validation to be false', function() {

            createController();

            ctrl.info.name = "";
            ctrl.info.address1 = "";
            ctrl.info.address2 = "";
            ctrl.info.city = "";
            ctrl.info.state = "";
            $rootScope.$apply();

            expect(propertyForm).toBeDefined();
            expect(propertyForm.$valid).toBe(false);

        });

        it('should expect propertyForm to be defined and form validation to be false', function() {

            createController();

            ctrl.info.name = "Binder Name";
            ctrl.info.address1 = "";
            ctrl.info.address2 = "";
            ctrl.info.city = "";
            ctrl.info.state = "";
            $rootScope.$apply();

            expect(propertyForm).toBeDefined();
            expect(propertyForm.$valid).toBe(false);

        });

        it('should expect propertyForm to be defined and form validation to be false', function() {

            createController();

            ctrl.info.name = "Binder Name";
            ctrl.info.address1 = "321 Summer Street";
            ctrl.info.address2 = "";
            ctrl.info.city = "";
            ctrl.info.state = "";
            $rootScope.$apply();

            expect(propertyForm).toBeDefined();
            expect(propertyForm.$valid).toBe(false);

        });

        it('should expect propertyForm to be defined and form validation to be false', function() {

            createController();

            ctrl.info.name = "Binder Name";
            ctrl.info.address1 = "321 Summer Street";
            ctrl.info.address2 = "";
            ctrl.info.city = "Kansas City";
            ctrl.info.state = "";
            $rootScope.$apply();

            expect(propertyForm).toBeDefined();
            expect(propertyForm.$valid).toBe(false);

        });

        it('should expect propertyForm to be defined and form validation to be false', function() {

            createController();

            ctrl.info.name = "d0avx8972fusilgwtfd3tg64tygqsdzizgpear1qla1avce1dgv";
            ctrl.info.address1 = "321 Summer Street";
            ctrl.info.address2 = "";
            ctrl.info.city = "Kansas City";
            ctrl.info.state = "";
            $rootScope.$apply();

            expect(propertyForm).toBeDefined();
            expect(propertyForm.$valid).toBe(false);

        });

        it('should expect propertyForm to be defined and form validation to be false', function() {

            createController();

            ctrl.info.name = "Binder Name";
            ctrl.info.address1 = "d0avx8972fusilgwtfd3tg64tygqsdzizgpear1qla1avce1dgv";
            ctrl.info.address2 = "";
            ctrl.info.city = "Kansas City";
            ctrl.info.state = "";
            $rootScope.$apply();

            expect(propertyForm).toBeDefined();
            expect(propertyForm.$valid).toBe(false);

        });

        it('should expect propertyForm to be defined and form validation to be false', function() {

            createController();

            ctrl.info.name = "Binder Name";
            ctrl.info.address1 = "321 Summer Street";
            ctrl.info.address2 = "d0avx8972fusilgwtfd3tg64tygqsdzizgpear1qla1avce1dgv";
            ctrl.info.city = "Kansas City";
            ctrl.info.state = "";
            $rootScope.$apply();

            expect(propertyForm).toBeDefined();
            expect(propertyForm.$valid).toBe(false);

        });

        it('should expect propertyForm to be defined and form validation to be false', function() {

            createController();

            ctrl.info.name = "Binder Name";
            ctrl.info.address1 = "321 Summer Street";
            ctrl.info.address2 = "Apt. 2";
            ctrl.info.city = "d0avx8972fusilgwtfd3tg64tygqsdzizgpear1qla1avce1dgv";
            ctrl.info.state = "KS";
            $rootScope.$apply();

            expect(propertyForm).toBeDefined();
            expect(propertyForm.$valid).toBe(false);

        });

        it('should expect propertyForm to be defined and form validation to be true', function() {

            createController();

            ctrl.info.name = "Binder Name";
            ctrl.info.address1 = "321 Summer Street";
            ctrl.info.address2 = "";
            ctrl.info.city = "Kansas City";
            ctrl.info.state = "KS";
            ctrl.info.zip = "01221";
            $rootScope.$apply();

            expect(propertyForm).toBeDefined();
            expect(propertyForm.$valid).toBe(true);

        });
    });

    describe('ctrl.next', function() {

        it('should $state go when there are pending binders', function() {

            spyOn(api.user, "bindersWaitingByEmail").and.returnValue($q.when({
                data: { binders: [{ access_token: 1 }] }
            }));
            spyOn($state, "go");

            createController();
            $rootScope.$apply();
            ctrl.info.email = "homeowner@gmail.com";
            ctrl.next('buyer');
            $rootScope.$apply();

            expect(api.user.bindersWaitingByEmail).toHaveBeenCalledWith("homeowner@gmail.com");
            expect($state.go).toHaveBeenCalled();
        });

        it('should call advanceSteps', function() {

            spyOn(api.user, "bindersWaitingByEmail").and.returnValue($q.when({
                data: []
            }));

            createController();
            $rootScope.$apply();
            spyOn(ctrl, "advanceSteps").and.callThrough();
            ctrl.info.email = "homeowner@gmail.com";
            ctrl.next('buyer');
            $rootScope.$apply();

            expect(api.user.bindersWaitingByEmail).toHaveBeenCalledWith("homeowner@gmail.com");
            expect(ctrl.advanceSteps).toHaveBeenCalled();
            expect(ctrl.type).toBe('buyer');
        });

        it('should call bindersWaitingByEmail, return an error and call advanceSteps', function() {

            spyOn(api.user, "bindersWaitingByEmail").and.returnValue($q.reject({
                data: "error"
            }));

            createController();
            $rootScope.$apply();
            spyOn(ctrl, "advanceSteps").and.callThrough();
            ctrl.info.email = "homeowner@gmail.com";
            ctrl.next('buyer');
            $rootScope.$apply();

            expect(api.user.bindersWaitingByEmail).toHaveBeenCalledWith("homeowner@gmail.com");
            expect(ctrl.advanceSteps).toHaveBeenCalled();
            expect(ctrl.type).toBe('buyer');
        });

        it('should move increment current step, set the type of client and next step', function() {

            createController();
            ctrl.current_step = 1;
            $rootScope.$apply();
            spyOn(ctrl, "advanceSteps").and.callThrough();
            ctrl.next('seller');
            $rootScope.$apply();

            expect(ctrl.advanceSteps).toHaveBeenCalled();
            expect(ctrl.type).toBe('seller');
            expect(ctrl.current_step).toBe(2);
            expect(ctrl.step).toBe('stepThree');
        });

        it('should move increment current step, set the type of client and next step', function() {

            createController();
            ctrl.current_step = 1;
            $rootScope.$apply();
            spyOn(ctrl, "advanceSteps").and.callThrough();
            ctrl.next('buyer');
            $rootScope.$apply();

            expect(ctrl.advanceSteps).toHaveBeenCalled();
            expect(ctrl.type).toBe('buyer');
            expect(ctrl.current_step).toBe(2);
            expect(ctrl.step).toBe('stepThree');
        });

        it('should move increment current step and set next step', function() {

            createController();
            $rootScope.$apply();
            ctrl.current_step = 2;
            spyOn(ctrl, "advanceSteps").and.callThrough();
            ctrl.next();
            $rootScope.$apply();

            expect(ctrl.advanceSteps).toHaveBeenCalled();
            expect(ctrl.current_step).toBe(3);
            expect(ctrl.step).toBe('stepFour');
        });

    });

    describe('ctrl.onCheckForBinders', function() {
        it('calls $state go', function() {
            spyOn($state, "go");

            createController();
            ctrl.onCheckForBinders({ data: { message: "User already exists" } });

            expect($state.go).toHaveBeenCalled();
        });
    });

    describe('ctrl.previousSteps', function() {

        it('should move decrement current step and set the previous step', function() {

            createController();
            $rootScope.$apply();
            ctrl.current_step = 3;
            ctrl.previousSteps();
            $rootScope.$apply();

            expect(ctrl.current_step).toBe(2);
            expect(ctrl.step).toBe('stepThree');
        });

        it('should move decrement current step and set the previous step', function() {

            createController();
            $rootScope.$apply();
            ctrl.current_step = 2;
            ctrl.previousSteps();
            $rootScope.$apply();

            expect(ctrl.current_step).toBe(1);
            expect(ctrl.step).toBe('stepTwo');
        });

        it('should move decrement current step and set the previous step', function() {

            createController();
            $rootScope.$apply();
            ctrl.current_step = 1;
            ctrl.previousSteps();
            $rootScope.$apply();

            expect(ctrl.current_step).toBe(0);
            expect(ctrl.step).toBe('stepOne');
        });

    });

    describe('ctrl.advanceSteps', function() {

        it('should move increment current step and set the next step', function() {

            createController();
            $rootScope.$apply();
            ctrl.current_step = 1;
            ctrl.advanceSteps();
            $rootScope.$apply();

            expect(ctrl.current_step).toBe(2);
            expect(ctrl.step).toBe('stepThree');
        });

        it('should move increment current step and set the next step', function() {

            createController();
            $rootScope.$apply();
            ctrl.current_step = 2;
            ctrl.advanceSteps();
            $rootScope.$apply();

            expect(ctrl.current_step).toBe(3);
            expect(ctrl.step).toBe('stepFour');
        });

    });

    describe('ctrl.fileAdded', function() {

        it('should call $window object createObjectURL function', function() {

            spyOn($window.URL, "createObjectURL").and.returnValue("url");

            var images = [{
            }];

            createController();
            spyOn(ctrl.uploader.api, "pendingUploads").and.returnValue(images);
            $rootScope.$apply();
            ctrl.fileAdded();
            $rootScope.$apply();

            //expect($window.URL.createObjectURL).toHaveBeenCalledWith("native_file");
            expect(ctrl.uploader.api.pendingUploads).toHaveBeenCalled();
            expect(ctrl.image.src).toBe("url");
            expect(ctrl.url).toBe("url");

        });

    });

    describe('ctrl.remove_image', function() {

        it('should call $window object revokeObjectURL function', function() {

            spyOn($window.URL, "revokeObjectURL");
            spyOn($window.URL, "createObjectURL").and.returnValue("url");

            var images = [{
            }];

            createController();
            spyOn(ctrl.uploader.api, "pendingUploads").and.returnValue(images);
            spyOn(ctrl.uploader.api, "remove");
            $rootScope.$apply();
            ctrl.fileAdded();
            $rootScope.$apply();
            ctrl.remove_image();
            $rootScope.$apply();

            expect($window.URL.revokeObjectURL).toHaveBeenCalledWith("url");
            expect(ctrl.uploader.api.remove).toHaveBeenCalledWith(ctrl.images[0]);

        });

    });

    describe('ctrl.submitForm', function() {

        it('should call BinderForm show function for buyer', function() {

            spyOn(BinderConfirm, "show");

            var info = {
                first: "Jack",
                last: "Frost",
                email: "jackfrost@gmail.com",
                address1: "123 Main Street",
                city: "Boston",
                state: "MA"
            };

            var information = {
                client: info,
                maintenance: [],
                title: "My info",
                image_url: "url"
            };

            spyOn($window.URL, "createObjectURL").and.returnValue("url");

            var images = [{
            }];

            createController();
            spyOn(ctrl.uploader.api, "pendingUploads").and.returnValue(images);
            $rootScope.$apply();
            ctrl.fileAdded();
            ctrl.type = "buyer";
            $rootScope.$apply();
            ctrl.info = info;
            ctrl.submitForm();
            $rootScope.$apply();

            expect(BinderConfirm.show).toHaveBeenCalled();

        });

        it('should call BinderForm show function for seller', function() {

            spyOn(BinderConfirm, "show");

            var info = {
                first: "Jack",
                last: "Frost",
                email: "jackfrost@gmail.com",
                address1: "123 Main Street",
                city: "Boston",
                state: "MA"
            };

            var information = {
                client: info,
                maintenance: [],
                title: "My info",
                appliances: [],
                contractors: [],
                projects: []
            };

            createController();
            ctrl.type = "seller";
            $rootScope.$apply();
            ctrl.info = info;
            ctrl.submitForm();
            $rootScope.$apply();

            expect(BinderConfirm.show).toHaveBeenCalled();

        });

    });

    describe('ctrl.createUser', function() {

        it('should successfully call api user create function', function() {

            spyOn(api.user, "register").and.returnValue($q.when({
                data: {
                    token: "token"
                }
            }));
            spyOn(notify, "success");
            spyOn(loading, "show");

            var info = {
                email: "homeowner@gmail.com",
                password: "securepassword",
                first: "first",
                last: "last"
            };

            createController();
            spyOn(ctrl, "createBinder");
            $rootScope.$apply();
            ctrl.info = info;
            ctrl.createUser();
            $rootScope.$apply();

            expect(api.user.register).toHaveBeenCalled();
            expect(loading.show).toHaveBeenCalled();
            expect(ctrl.createBinder).toHaveBeenCalled();

        });

        it('should unsuccessfully call api user create function', function() {

            spyOn(api.user, "register").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(notify, "success");
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn($log, "error");
            spyOn(notify, "error");

            var info = {
                email: "homeowner@gmail.com",
                password: "securepassword",
                first: "first",
                last: "last"
            };

            createController();
            spyOn(ctrl, "createBinder");
            $rootScope.$apply();
            ctrl.info = info;
            ctrl.createUser();
            $rootScope.$apply();

            expect(api.user.register).toHaveBeenCalled();
            expect(loading.show).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalled();
            expect(ctrl.current_step).toBe(1);
            expect(ctrl.step).toBe("stepTwo");

        });

    });

    describe('ctrl.createBinder', function() {

        it('should call api binder create function successfully and call createMaintenance', function() {

            spyOn(api.binder, "create").and.returnValue($q.when({
                data: {
                    id: 121
                }
            }));
            spyOn(loading, "setMessage");
            spyOn(notify, "success");

            var info = {
                name: "Jack Frost Home",
                first: "Jack",
                last: "Frost",
                email: "jackfrost@gmail.com",
                address1: "123 Main Street",
                address2: "",
                city: "Boston",
                state: "MA"
            };

            var binder = {
                name: info.first + " " + info.last + " Home",
                create_method: "manual",
                property_attributes: {
                    address1: info.address1,
                    address2: info.address2,
                    city: info.city,
                    state: info.state,
                    country: undefined
                }
            };

            var request = {
                binder: binder,
                user_binders: [{
                    user_id: 1,
                    role: 'owner'
                }]
            };


            createController();
            $rootScope.$apply();
            spyOn(ctrl, "createMaintenance");
            ctrl.info = info;
            ctrl.user = {
                id: 1
            };
            ctrl.createBinder();
            $rootScope.$apply();

            expect(api.binder.create).toHaveBeenCalledWith(request);
            expect(notify.success).toHaveBeenCalled();
            expect(ctrl.binderId).toBe(121);
            expect(ctrl.createMaintenance).toHaveBeenCalled();

        });

        it('should call api binder create function and return error', function() {

            spyOn(api.binder, "create").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn(loading, "setMessage");
            spyOn(loading, "close");
            spyOn($log, "error");
            spyOn(notify, "error");

            var info = {
                name: "Jack Frost Home",
                first: "Jack",
                last: "Frost",
                email: "jackfrost@gmail.com",
                address1: "123 Main Street",
                address2: "",
                city: "Boston",
                state: "MA"
            };

            var binder = {
                name: info.first + " " + info.last + " Home",
                create_method: "manual",
                property_attributes: {
                    address1: info.address1,
                    address2: info.address2,
                    city: info.city,
                    state: info.state,
                    country: undefined
                }
            };

            var request = {
                binder: binder,
                user_binders: [{
                    user_id: 1,
                    role: 'owner'
                }]
            };

            createController();
            $rootScope.$apply();
            spyOn(ctrl, "createMaintenance");
            ctrl.info = info;
            ctrl.user = {
                id: 1
            };
            ctrl.createBinder();
            $rootScope.$apply();

            expect(api.binder.create).toHaveBeenCalledWith(request);
            expect(loading.close).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(ctrl.current_step).toBe(2);
            expect(ctrl.step).toBe("stepThree");

        });

    });

    describe('ctrl.createMaintenance', function() {

        it("should call createMaintenance function successfully", function() {

            spyOn(api.maintenanceItem, "create").and.returnValue($q.when({
                data: "success"
            }));

            var items = [{
                name: "Smoke Detector Battery",
                interval: "Annual",
                do_date: "",
                details: ""
            }, {
                name: "Sweep Chimney1",
                interval: "Semi-Annual",
                do_date: "",
                details: ""
            }, {
                name: "Sweep Chimney2",
                interval: "Quarterly",
                do_date: "",
                details: ""
            }, {
                name: "Sweep Chimney3",
                interval: "Monthly",
                do_date: "",
                details: ""
            }, {
                name: "Sweep Chimney4",
                interval: "Every-Other-Year",
                do_date: "",
                details: ""
            }, {
                name: "Sweep Chimney5",
                interval: "Every-3-Years",
                do_date: "",
                details: ""
            }, {
                name: "Sweep Chimney6",
                interval: "Every-4-Years",
                do_date: "",
                details: ""
            }, {
                name: "Sweep Chimney7",
                interval: "Every-5-Years",
                do_date: "",
                details: ""
            }, {
                name: "Sweep Chimney8",
                interval: "Every-10-Years",
                do_date: "",
                details: ""
            }, {
                name: "Sweep Chimney9",
                interval: "Every-40-Years",
                do_date: "",
                details: ""
            }];

            createController();
            ctrl.binder = { id: 121 };
            ctrl.maintenanceItems = items;
            spyOn(ctrl, "createAppliances");
            ctrl.createMaintenance();
            $rootScope.$apply();

            expect(api.maintenanceItem.create.calls.count()).toBe(10);
        });

        it("should call createMaintenance function successfully and also return an error", function() {

            spyOn(api.maintenanceItem, "create").and.callFake(function(opts) {
                if (opts.name === "Smoke Detector Battery") {
                    return $q.when({
                        data: "success"
                    });
                }
                else if (opts.name === "Sweep Chimney") {
                    return $q.reject({
                        data: "error"
                    });
                }
            });
            spyOn(loading, "setMessage");
            spyOn($log, "error");
            spyOn(notify, "error");

            var items = [{
                name: "Smoke Detector Battery",
                interval: 1,
                do_date: "",
                details: ""
            }, {
                name: "Sweep Chimney",
                interval: 3,
                do_date: "",
                details: ""
            }];

            createController();
            $rootScope.$apply();
            ctrl.binderId = 121;
            ctrl.maintenanceItems = items;
            spyOn(ctrl.maintenanceItems, "splice").and.callThrough();
            spyOn(ctrl, "createAppliances");
            spyOn(ctrl, "createMaintenance").and.callThrough();
            ctrl.createMaintenance();
            $rootScope.$apply();

            expect(api.maintenanceItem.create.calls.count()).toBe(2);
            expect(ctrl.maintenanceItems.splice).toHaveBeenCalledWith(0, 1);
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(ctrl.createAppliances).toHaveBeenCalled();
            expect(ctrl.createMaintenance).toHaveBeenCalled();

        });
    });

    describe('ctrl.createAppliances', function() {

        it("should call createAppliances function successfully", function() {

            spyOn(api.appliance, "create").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(loading, "setMessage");

            var appliances = [{
                name: "Refrigerator",
                manufacturer: ""
            }, {
                name: "Dishwasher",
                manufacturer: ""
            }];

            createController();
            $rootScope.$apply();
            ctrl.appliances = appliances;
            ctrl.binderId = 121;
            spyOn(ctrl.appliances, "splice").and.callThrough();
            spyOn(ctrl, "uploadImages");
            spyOn(ctrl, "createAppliances").and.callThrough();
            ctrl.createAppliances();
            $rootScope.$apply();

            expect(api.appliance.create.calls.count()).toBe(2);
            expect(ctrl.appliances.splice).toHaveBeenCalledWith(0, 1);
            expect(loading.setMessage).toHaveBeenCalledWith("Adding appliances to binder...");
            expect(ctrl.createAppliances).toHaveBeenCalled();
            expect(ctrl.uploadImages).toHaveBeenCalled();
        });

        it("should call createAppliances function successfully and also return an error", function() {

            spyOn(api.appliance, "create").and.callFake(function(opts) {
                if (opts.name === "Refrigerator") {
                    return $q.when({
                        data: "success"
                    });
                }
                else if (opts.name === "Dishwasher") {
                    return $q.reject({
                        data: "error"
                    });
                }
            });
            spyOn(loading, "setMessage");
            spyOn($log, "error");
            spyOn(notify, "error");

            var appliances = [{
                name: "Refrigerator",
                manufacturer: ""
            }, {
                name: "Dishwasher",
                manufacturer: ""
            }];

            createController();
            $rootScope.$apply();
            ctrl.appliances = appliances;
            ctrl.binderId = 121;
            spyOn(ctrl.appliances, "splice").and.callThrough();
            spyOn(ctrl, "uploadImages");
            spyOn(ctrl, "createAppliances").and.callThrough();
            ctrl.createAppliances();
            $rootScope.$apply();

            expect(api.appliance.create.calls.count()).toBe(2);
            expect(ctrl.appliances.splice).toHaveBeenCalledWith(0, 1);
            expect(loading.setMessage).toHaveBeenCalledWith("Adding appliances to binder...");
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(ctrl.createAppliances).toHaveBeenCalled();
            expect(ctrl.uploadImages).toHaveBeenCalled();

        });
    });

    describe('ctrl.uploadImages', function() {

        it("should call uploadFiles successfully", function() {

            var images = [{}];
            spyOn(loading, "setMessage");

            createController();
            $rootScope.$apply();
            spyOn(ctrl.uploader.api, "pendingUploads").and.returnValue(images);
            spyOn(ctrl.uploader.api, "updateParams");
            ctrl.binderId = 121;
            ctrl.uploadImages();
            $rootScope.$apply();

            expect(ctrl.uploader.api.pendingUploads).toHaveBeenCalled();
            expect(ctrl.uploader.api.updateParams).toHaveBeenCalledWith({
                binder_id: 121,
                hero_image_id: true
            });

        });

        it("should call createContractors", function() {

            var images = [];

            createController();
            $rootScope.$apply();
            spyOn(ctrl.uploader.api, "pendingUploads").and.returnValue(images);
            spyOn(ctrl, "createContractors");
            ctrl.uploadImages();
            $rootScope.$apply();

            expect(ctrl.uploader.api.pendingUploads).toHaveBeenCalled();
            expect(ctrl.createContractors).toHaveBeenCalled();

        });

    });

    describe('ctrl.imgUploaded', function() {

        it("should call addToContractors", function() {

            createController(true);
            $rootScope.$apply();
            spyOn(ctrl.uploader.api, "pendingUploads").and.returnValue([]);
            spyOn(ctrl, "createContractors");
            ctrl.imgUploaded({
                name: "uploaded_image.png"
            });
            $rootScope.$apply();

            expect(ctrl.uploader.api.pendingUploads).toHaveBeenCalled();
            expect(ctrl.createContractors).toHaveBeenCalled();

        });

    });

    describe('ctrl.imgUploadError', function() {

        it("should call createContractors", function() {

            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            $rootScope.$apply();
            spyOn(ctrl, "createContractors");
            ctrl.imgUploadError({
                data: "error"
            });
            $rootScope.$apply();

            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(ctrl.createContractors).toHaveBeenCalled();

        });

    });

    describe('ctrl.createContractors', function() {

        it("should call createContractors function successfully", function() {

            spyOn(api.binderContractor, "create").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(loading, "setMessage");

            var contractors = [{
                name: "Bob the Builder",
                type: "Contractor",
                email: "bob@gmail.com",
                phone: "517-232-1200",
                notes: "Hire him!"
            }, {
                name: "Paul the Plumber",
                type: "Plumber",
                email: "paul@gmail.com",
                phone: "555-510-6030",
                notes: "Best plumber around!"
            }];

            createController();
            $rootScope.$apply();
            ctrl.contractors = contractors;
            ctrl.binderId = 121;
            spyOn(ctrl.contractors, "splice").and.callThrough();
            spyOn(ctrl, "createProjects");
            spyOn(ctrl, "createContractors").and.callThrough();
            ctrl.createContractors();
            $rootScope.$apply();

            expect(api.binderContractor.create.calls.count()).toBe(2);
            expect(ctrl.contractors.splice).toHaveBeenCalledWith(0, 1);
            expect(loading.setMessage.calls.count()).toBe(2);
            expect(ctrl.createProjects).toHaveBeenCalled();
            expect(ctrl.createContractors).toHaveBeenCalled();
        });

        it("should call createContractors function successfully and also return an error", function() {

            spyOn(api.binderContractor, "create").and.callFake(function(opts) {
                if (opts.contractor_attributes.email == "paul@gmail.com") {
                    return $q.when({
                        data: "success"
                    });
                }
                else if (opts.contractor_attributes.email === "bob@gmail.com") {
                    return $q.reject({
                        data: "error"
                    });
                }
            });
            spyOn(loading, "setMessage");
            spyOn($log, "error");
            spyOn(notify, "error");

            var contractors = [{
                name: "Bob the Builder",
                type: "Contractor",
                email: "bob@gmail.com",
                phone: "517-232-1200",
                notes: "Hire him!"
            }, {
                name: "Paul the Plumber",
                type: "Plumber",
                email: "paul@gmail.com",
                phone: "555-510-6030",
                notes: "Best plumber around!"
            }];

            createController();
            $rootScope.$apply();
            ctrl.contractors = contractors;
            ctrl.binderId = 121;
            spyOn(ctrl.contractors, "splice").and.callThrough();
            spyOn(ctrl, "createProjects");
            spyOn(ctrl, "createContractors").and.callThrough();
            ctrl.createContractors();
            $rootScope.$apply();

            expect(api.binderContractor.create.calls.count()).toBe(2);
            expect(ctrl.contractors.splice).toHaveBeenCalledWith(0, 1);
            expect(loading.setMessage.calls.count()).toBe(2);
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(ctrl.createProjects).toHaveBeenCalled();
            expect(ctrl.createContractors).toHaveBeenCalled();

        });
    });

    describe('ctrl.createProjects', function() {

        it("should call createProjects function successfully", function() {

            spyOn(api.project, "create").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(loading, "setMessage");

            var projects = [{
                name: "Fix Porch",
                end_date: "",
                details: ""
            }, {
                name: "Add Mud Room",
                end_date: "",
                details: ""
            }];

            createController();
            $rootScope.$apply();
            ctrl.projects = projects;
            ctrl.binderId = 121;
            spyOn(ctrl.projects, "splice").and.callThrough();
            spyOn(ctrl, "submitComplete");
            spyOn(ctrl, "createProjects").and.callThrough();
            ctrl.createProjects();
            $rootScope.$apply();

            expect(api.project.create.calls.count()).toBe(2);
            expect(ctrl.projects.splice).toHaveBeenCalledWith(0, 1);
            expect(loading.setMessage.calls.count()).toBe(2);
            expect(ctrl.submitComplete).toHaveBeenCalled();
            expect(ctrl.createProjects).toHaveBeenCalled();
        });

        it("should call createProjects function successfully and also return an error", function() {

            spyOn(api.project, "create").and.callFake(function(opts) {
                if (opts.name == "Fix Porch") {
                    return $q.when({
                        data: "success"
                    });
                }
                else if (opts.name === "Add Mud Room") {
                    return $q.reject({
                        data: "error"
                    });
                }
            });
            spyOn(loading, "setMessage");
            spyOn($log, "error");
            spyOn(notify, "error");

            var projects = [{
                name: "Fix Porch",
                end_date: "",
                details: ""
            }, {
                name: "Add Mud Room",
                end_date: "",
                details: ""
            }];

            createController();
            $rootScope.$apply();
            ctrl.projects = projects;
            ctrl.binderId = 121;
            spyOn(ctrl.projects, "splice").and.callThrough();
            spyOn(ctrl, "submitComplete");
            spyOn(ctrl, "createProjects").and.callThrough();
            ctrl.createProjects();
            $rootScope.$apply();

            expect(api.project.create.calls.count()).toBe(2);
            expect(ctrl.projects.splice).toHaveBeenCalledWith(0, 1);
            expect(loading.setMessage.calls.count()).toBe(2);
            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(ctrl.submitComplete).toHaveBeenCalled();
            expect(ctrl.createProjects).toHaveBeenCalled();

        });
    });

    describe('ctrl.submitComplete', function() {

        it('should call afterLogin go', function() {

            spyOn(AfterLogin, "go");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();
            ctrl.submitComplete();
            $rootScope.$apply();

            expect(AfterLogin.go).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();

        });

    });

    describe("ctrl.onSelect", function() {
        it("sets the country", function() {
            createController();
            var item = { country: {} };
            ctrl.onSelect(item);

            expect(ctrl.country).toEqual(item.country);
        });
    });

    describe("ctrl.setStates", function() {
        it("sets the states", function() {
            createController();
            ctrl.country = { value: "US" };
            ctrl.setStates();

            expect(ctrl.states.length).toEqual(53);
        });
    });

});