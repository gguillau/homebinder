(function() {
    "use strict";

    angular
        .module("hb.homeowners.registration", [
            "hb.homeowners.registration.components",
            "ui.router",
            "hb.components",
            "hb.api"
        ])
        .config(['$stateProvider', registrationConfig])
        .controller("HomeownerRegistrationController", [
            "hb.api",
            "AfterLogin",
            "Address",
            "Session",
            "$window",
            "ModalService",
            "Notify",
            "$log",
            "$state",
            "Loading",
            "Context",
            "BinderConfirmModal",
            "hb.resources",
            "hb.utils",
            HomeownerRegistrationController
        ]);

    function registrationConfig($stateProvider) {
        $stateProvider
            .state("regHomeowner", {
                url: "^/homeowners/register",
                templateUrl: "homeowners/registration/homeownersRegistration.tpl.html",
                controller: "HomeownerRegistrationController",
                controllerAs: "ctrl",
                bindToController: true
            });
    }

    function HomeownerRegistrationController(api, AfterLogin, address, Session, $window, modals, notify, $log, $state, loading, context, BinderConfirm, resources, utils) {
        this.validationErrors = resources.validationErrors;
        this.session = Session;
        this.setLocale = Session.setLocale;
        this.$window = $window;
        this.modals = modals;
        this.notify = notify;
        this.$log = $log;
        this.$state = $state;
        this.loading = loading;
        this.Address = address;
        this.countries = address.countries();
        this.country = this.countries[0];
        this.context = context;
        this.BinderConfirm = BinderConfirm;
        this.utils = utils.utils;
        this.deviceName = "Unknown";
        this.deviceVersion = "Unknown";
        this.operatingSystem = "Unknown";
        this.images = [];
        this.image = {};
        this.type = null;
        this.steps = ["stepOne", "stepTwo", "stepThree", "stepFour"];
        this.maintenanceItems = [];
        this.appliances = [];
        this.contractors = [];
        this.projects = [];
        this.current_step = 0;
        this.step = this.steps[this.current_step];
        this.api = api;
        this.afterLogin = AfterLogin;
        this.states = address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
            return state.country.value === this.country.value;
        }));
        this.resources = resources.homeownerRegistration;
        this.resources = angular.extend({}, this.resources, resources.usersForm);
        this.resources = angular.extend({}, this.resources, resources.standardLanding);
        this.uploader = {
            fileTypes: "image",
            hideUploadButton: true,
            showTable: false,
            selectButtonLabel: this.resources.uploadPhoto,
            fileLimit: true,
            url: "/api/v1/images",
            jwt: null,
            multiSelect: false
        };
        this.data = this.user = {};
        this.has_submitted = false;
        this.localeOptions = [{
            name: this.resources.english,
            value: "en-us"
        }, {
            name: this.resources.french,
            value: "fr-fr"
        }, {
            name: this.resources.spanish,
            value: "sp-sp"
        }];
        this.init();
    }

    HomeownerRegistrationController.prototype = {

        init: function() {
            var device = this.utils.getDeviceInfo();
            this.deviceName = device[0];
            this.deviceVersion = device[1];
            this.operatingSystem = this.utils.getOperatingSystem();
            var locale = this.session.getLocale();
            this.info = {
                default_locale: locale ? locale : "en-us",
                country: this.country.value
            };
        },

        next: function(type) {
            if (type) {
                this.type = type;
            }
            // If this is the first step check the email for existing binders from a transfer or share
            if (this.current_step === 0) {
                this.info.name = this.info.first + " " + this.info.last + " Home";
                this.api.user.bindersWaitingByEmail(this.info.email).then(
                    angular.bind(this, this.onCheckForBinders),
                    angular.bind(this, this.onCheckForBindersError));
            }
            else {
                this.advanceSteps();
            }
        },

        onCheckForBinders: function(response) {
            if (response.data.message && response.data.message === "User already exists") {
                this.notify.info(this.resources.existingMessage);
                this.$state.go("login");
            }
            // we'll need to forward existing users to welcome page
            else if (response.data && response.data.binders && response.data.binders.length > 0) {
                var binder = response.data.binders[0];
                this.$state.go("homeowners.welcome", { accessToken: binder.access_token });
            }
            else {
                this.advanceSteps();
            }
        },

        onCheckForBindersError: function(response) {
            this.advanceSteps();
        },

        previousSteps: function() {
            this.current_step--;
            this.step = this.steps[this.current_step];
        },

        advanceSteps: function() {
            this.current_step++;
            this.step = this.steps[this.current_step];
        },

        fileAdded: function() {
            this.images = this.uploader.api.pendingUploads();
            var native_file = this.images[0];
            this.image.src = this.url = this.$window.URL.createObjectURL(native_file);
        },

        remove_image: function() {
            this.uploader.api.remove(this.images[0]);
            this.$window.URL.revokeObjectURL(this.image.src);
        },

        submitForm: function() {
            var information = {
                client: this.info,
                maintenance: this.maintenanceItems,
                title: this.resources.info
            };

            if (this.type === 'seller') {
                information.appliances = this.appliances;
                information.contractors = this.contractors;
                information.projects = this.projects;
            }

            if (this.images.length > 0) {
                information.image_url = this.url;
            }

            this.BinderConfirm.show({
                binder: information,
                confirm: angular.bind(this, this.createUser)
            });
        },

        createUser: function() {
            this.has_submitted = true;
            var data = {
                user: {
                    email: this.info.email,
                    password: this.info.password,
                    user_profile_attributes: {
                        first_name: this.info.first,
                        last_name: this.info.last,
                        mobile_phone: "+" + this.country.code + this.info.phone,
                        address_attributes: {
                            address1: this.info.address1,
                            address2: this.info.address2,
                            city: this.info.city,
                            state: this.info.state,
                            zip: this.info.zip,
                            country: this.info.country
                        }
                    }
                },
                device_type: "web",
                device_name: this.deviceName,
                device_version: this.deviceVersion,
                operating_system: this.operatingSystem
            };
            this.loading.show(this.resources.saving + "...");
            this.api.user.register(data).then(
                angular.bind(this, this.registerSuccess),
                angular.bind(this, this.registerError)
            );
        },

        registerSuccess: function(response) {
            this.user = this.session.getUser();
            this.createBinder();
        },

        registerError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.has_submitted = false;
            this.current_step = 1;
            this.step = this.steps[this.current_step];
            this.loading.close();
        },

        createBinder: function() {

            var binder = {
                name: this.info.name,
                create_method: "manual",
                property_attributes: {
                    address1: this.info.address1,
                    address2: this.info.address2,
                    city: this.info.city,
                    state: this.info.state,
                    country: this.info.country
                }
            };

            var request = {
                binder: binder,
                user_binders: [{
                    user_id: this.user.id,
                    role: "owner"
                }]
            };

            this.api.binder.create(request).then(
                angular.bind(this, this.onCreateBinder),
                angular.bind(this, this.onCreateBinderError)
            );
        },

        onCreateBinder: function(response) {
            this.notify.success(this.resources.saved);
            this.context.setBinder(response.data);
            this.binderId = response.data.id;
            this.createMaintenance();
        },

        onCreateBinderError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.has_submitted = false;
            this.current_step = 2;
            this.step = this.steps[this.current_step];
            this.loading.close();
        },

        createMaintenance: function() {
            // If there are maintenance items to create start creating them.
            // If not go to appliances.
            if (this.maintenanceItems && this.maintenanceItems.length > 0) {
                var item = this.maintenanceItems[0];
                this.maintenanceItems.splice(0, 1);
                var mi = {
                    binder_id: this.binderId,
                    name: item.name,
                    do_date: item.do_date,
                    details: item.details
                };
                // convert interval dropdown for maintenance_cycle
                switch (item.interval) {
                    case "Annual":
                        mi.interval = 1;
                        mi.maintenance_cycle = "Years";
                        break;
                    case "Semi-Annual":
                        mi.interval = 6;
                        mi.maintenance_cycle = "Months";
                        break;
                    case "Quarterly":
                        mi.interval = 3;
                        mi.maintenance_cycle = "Months";
                        break;
                    case "Monthly":
                        mi.interval = 1;
                        mi.maintenance_cycle = "Months";
                        break;
                    case "Every-Other-Year":
                        mi.interval = 2;
                        mi.maintenance_cycle = "Years";
                        break;
                    case "Every-3-Years":
                        mi.interval = 3;
                        mi.maintenance_cycle = "Years";
                        break;
                    case "Every-4-Years":
                        mi.interval = 4;
                        mi.maintenance_cycle = "Years";
                        break;
                    case "Every-5-Years":
                        mi.interval = 5;
                        mi.maintenance_cycle = "Years";
                        break;
                    case "Every-10-Years":
                        mi.interval = 10;
                        mi.maintenance_cycle = "Years";
                        break;
                    case "Every-40-Years":
                        mi.interval = 40;
                        mi.maintenance_cycle = "Years";
                        break;
                }

                this.api.maintenanceItem.create(mi).then(
                    angular.bind(this, this.onCreateMaintenanceItemSuccess),
                    angular.bind(this, this.onCreateMaintenanceItemError)
                );
            }
            else {
                this.createAppliances();
            }
        },

        onCreateMaintenanceItemSuccess: function(response) {
            this.createMaintenance();
        },

        onCreateMaintenanceItemError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.createMaintenance();
        },

        // we currently do not create appliances/projects/contractors at the moment

        createAppliances: function() {

            if (this.appliances && this.appliances.length > 0) {
                var app = this.appliances[0];
                this.appliances.splice(0, 1);
                var a = {
                    binder_id: this.binderId,
                    name: app.name,
                    appliance_type: app.value,
                    manufacturer: app.manufacturer,
                    model: app.model
                };

                this.loading.setMessage("Adding appliances to binder...");
                this.api.appliance.create(a).then(
                    angular.bind(this, this.onCreateApplianceSuccess),
                    angular.bind(this, this.onCreateApplianceError)
                );
            }
            else {
                this.uploadImages();
            }
        },

        onCreateApplianceSuccess: function(response) {
            this.createAppliances();
        },

        onCreateApplianceError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.createAppliances();
        },

        uploadImages: function() {
            var uploadApi = this.uploader.api;
            if (uploadApi && uploadApi.pendingUploads().length > 0) {
                uploadApi.updateParams({
                    binder_id: this.binderId,
                    hero_image_id: true
                });

                // TODO: Hack
                setTimeout(function() {
                    uploadApi.uploadFiles();
                }, 0);
                this.$window.URL.revokeObjectURL(this.image.src);
            }
            else {
                this.createContractors();
            }
        },

        imgUploaded: function(file) {
            if (this.uploader.api.pendingUploads().length === 0) {
                this.createContractors();
            }
        },

        imgUploadError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.createContractors();
        },

        createContractors: function() {
            if (this.contractors && this.contractors.length > 0) {
                var contractor = this.contractors[0];
                this.contractors.splice(0, 1);
                var contract = {
                    binder_id: this.binderId,
                    account_number: "",
                    contact: contractor.name,
                    details: contractor.notes,
                    contractor_attributes: {
                        name: contractor.name,
                        contractor_type: contractor.type,
                        phone: contractor.phone,
                        email: contractor.email,
                        url: "",
                        address_attributes: {
                            address1: "",
                            address2: "",
                            city: "",
                            state: "",
                            zip: ""
                        }
                    }
                };

                this.loading.setMessage("Adding information about " + contract.name + " to binder...");
                this.api.binderContractor.create(contract).then(
                    angular.bind(this, this.onCreateContractorsSuccess),
                    angular.bind(this, this.onCreateContractorsError)
                );
            }
            else {
                this.createProjects();
            }
        },

        onCreateContractorsSuccess: function(response) {
            this.createContractors();
        },

        onCreateContractorsError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.createContractors();
        },

        createProjects: function() {

            if (this.projects && this.projects.length > 0) {
                var project = this.projects[0];
                project.binder_id = this.binderId;
                this.projects.splice(0, 1);
                this.loading.setMessage("Adding project " + project.name + " to binder...");
                this.api.project.create(project).then(
                    angular.bind(this, this.onCreateProjectsSuccess),
                    angular.bind(this, this.onCreateProjectsError)
                );
            }
            else {
                this.submitComplete();
            }
        },

        onCreateProjectsSuccess: function(response) {
            this.createProjects();
        },

        onCreateProjectsError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.createProjects();
        },

        submitComplete: function() {
            this.loading.close();
            this.afterLogin.go();
        },

        onSelect: function(item) {
            if (item) {
                this.country = item.country;
                this.info.country = item.country.value;
            }
        },

        setStates: function() {
            this.info.state = null;
            this.states = this.Address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                return state.country.value === this.country.value;
            }));
        }

    };
})();
