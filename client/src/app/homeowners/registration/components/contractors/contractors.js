(function() {
    "use strict";

    angular
        .module("hb.homeowners.registration.components.contractors", [])
        .directive("homeownerFormContractors", function() {
            return {
                restrict: "E",
                templateUrl: "homeowners/registration/components/contractors/contractors.tpl.html",
                controller: "HomeownerFormContractorsController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    contractors: "="
                }
            };
        })
        .controller("HomeownerFormContractorsController", [
            "hb.utils",
            "hb.resources",
            HomeownerFormContractorsController
        ]);

    function HomeownerFormContractorsController(utils, resources) {
        this.is_contract_collapsed = false;
        this.utils = utils;
        this.contractors = [];
        this.validationErrors = resources.validationErrors;
    }

    HomeownerFormContractorsController.prototype = {

        remove: function(index) {
            this.contractors.splice(index, 1);
        },

        addContractor: function() {
            var item = { name: "", type: "", email: "", notes: "", new_item: true };
            this.contractors.push(item);
        }
    };

})();