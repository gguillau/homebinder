describe("HomeownerFormContractorsController", function() {
    
    var $rootScope,
        $scope,
        $controller,
        ctrl;

    
    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("HomeownerFormContractorsController",
            {

            });
            
        $scope.ctrl = ctrl;
        $scope.$apply();
        $scope.$digest();
        
    }
   
    describe('ctrl.remove', function(){
        
        it('should remove contractor at index 0 from contractors array', function() {
            var contractor = {name: "Adam Driver", type: "Plumbing", email: "adamdriver@gmail.com", phone: "6175002500"};
            createController();
            $rootScope.$apply();
            ctrl.contractors.push(contractor);
            ctrl.remove(0);
            $rootScope.$apply();
            expect(ctrl.contractors.length).toEqual(0);
        });

    });
    
    describe('ctrl.addContractor', function(){
        
        it('should add contractor to contractors array', function() {
            
            createController();
            $rootScope.$apply();
            ctrl.addContractor();
            $rootScope.$apply();
            
            expect(ctrl.contractors.length).toEqual(1);
        });

    });
    
});

describe('homeownerFormContractors', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        
        $scope.contractors = [];
        
        element = $compile('<homeowner-form-contractors contractors="contractors"></homeowner-form-contractors>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("Home Pros");
        });
    });
});