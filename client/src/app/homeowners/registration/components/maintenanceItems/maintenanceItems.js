(function() {
	"use strict";

	angular
		.module("hb.homeowners.registration.components.maintenanceItems", [])
		.directive("homeownerFormMaintenanceItems", function() {
			return {
				restrict: "E",
				templateUrl: "homeowners/registration/components/maintenanceItems/maintenanceItems.tpl.html",
				controller: "HomeownerFormMaintenanceItemsController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {
					items: "=",
					type: "="
				}
			};
		})
		.controller("HomeownerFormMaintenanceItemsController", [
			"hb.utils",
			"hb.resources",
			Controller
		]);

	function Controller(utils, resources) {
		this.is_maint_collapsed = false;
		this.utils = utils;
		this.validationErrors = resources.validationErrors;
		this.today = this.utils.utils.getToday(false);
		this.next_month = this.utils.utils.getNextMonth();
		this.yyyy = this.utils.utils.getYear();
		this.datepicker = {
			format: "MMMM dd, yyyy",
			options: {
				"show-button-bar": false
			}
		};
		this.frequencies = [{
			name: "Annual",
			value: "Annual"
		}, {
			name: "Quarterly",
			value: "Quarterly"
		}, {
			name: "Monthly",
			value: "Monthly"
		}, {
			name: "Every Other Year",
			value: "Every-Other-Year"
		}, {
			name: "Every 3 Years",
			value: "Every-3-Years"
		}, {
			name: "Every 4 Years",
			value: "Every-4-Years"
		}, {
			name: "Every 5 Years",
			value: "Every-5-Years"
		}, {
			name: "Every 10 Years",
			value: "Every-10-Years"
		}, {
			name: "Every 40 Years",
			value: "Every-40-Years"
		}, {
			name: "Semi-Annual",
			value: "Semi-Annual"
		}];
		this.items = [{
			"name": "Smoke Detector Battery",
			"interval": "Annual",
			"do_date": this.next_month
		}, {
			"name": "Sweep Chimney",
			"interval": "Annual",
			"do_date": this.setDate('fall')
		}, {
			"name": "Pump Septic Tank",
			"interval": "Annual",
			"do_date": this.setDate('summer')
		}, {
			"name": "Change A/C Filter",
			"interval": "Quarterly",
			"do_date": this.next_month
		}, {
			"name": "Blow Out Sprinkler Lines",
			"interval": "Annual",
			"do_date": this.setDate('fall')
		}, {
			"name": "Change Water Filter",
			"interval": "Annual",
			"do_date": this.setDate('fall')
		}, {
			"name": "Add Water Softener",
			"interval": "Semi-Annual",
			"do_date": this.next_month
		}, {
			"name": "Clean Gutters",
			"interval": "Annual",
			"do_date": this.setDate('fall')
		}, {
			"name": "Inspect Crawlspace",
			"interval": "Annual",
			"do_date": this.setDate('spring')
		}, {
			"name": "Clean Dryer Vent",
			"interval": "Annual",
			"do_date": this.setDate('fall')
		}, {
			"name": "Service Furnace",
			"interval": "Annual",
			"do_date": this.setDate('fall')
		}, {
			"name": "Schedule Home Energy Audit",
			"interval": "Every-10-Years",
			"do_date": this.setDate('fall')
		}];
	}

	Controller.prototype = {

		setDate: function(season) {

			if (season === 'spring') {
				var spring = 'March 15, ' + this.setYear('March 15, ' + this.yyyy, this.today);
				return spring;
			}
			else if (season === 'summer') {
				var summer = 'June 15, ' + this.setYear('June 15, ' + this.yyyy, this.today);
				return summer;
			}
			else if (season === 'fall') {
				var fall = 'October 15, ' + this.setYear('October 15, ' + this.yyyy, this.today);
				return fall;
			}
		},

		setYear: function(date_one, date_two) {
			var first = Date.parse(date_one);
			var second = Date.parse(date_two);
			if (first < second) {
				return this.yyyy + 1;
			}
			return this.yyyy;
		},

		remove: function(index) {
			this.items.splice(index, 1);
		},

		addItem: function() {
			var item = {
				name: "",
				interval: "Annual",
				do_date: this.next_month,
				new_item: true
			};
			this.items.push(item);
		}
	};

})();