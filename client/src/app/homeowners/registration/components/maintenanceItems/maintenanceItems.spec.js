describe("HomeownerFormMaintenanceItemsController", function() {

    var $rootScope,
        $scope,
        $controller,
        ctrl;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("HomeownerFormMaintenanceItemsController", {

        });

        $scope.ctrl = ctrl;
        $scope.$apply();
        $scope.$digest();

    }

    describe('ctrl.remove', function() {

        it('should remove item at index 1 from items array', function() {

            createController();
            $rootScope.$apply();
            ctrl.remove(1);
            $rootScope.$apply();

            expect(ctrl.items.length).toEqual(11);
        });

    });

    describe('ctrl.addItem', function() {

        it('should add item to items array', function() {

            createController();
            $rootScope.$apply();
            ctrl.addItem();
            $rootScope.$apply();

            expect(ctrl.items.length).toEqual(13);
        });

    });

    describe('ctrl.setDate', function() {

        it('should set date for maintenance item', function() {

            createController();
            $rootScope.$apply();
            ctrl.today = new Date("January 15 2007");
            ctrl.yyyy = ctrl.today.getFullYear();
            var date = ctrl.setDate('spring');
            $rootScope.$apply();

            expect(date).toBe("March 15, 2007");
        });

        it('should set date for maintenance item', function() {

            createController();
            $rootScope.$apply();
            ctrl.today = new Date("June 14 2007");
            ctrl.yyyy = ctrl.today.getFullYear();
            var date = ctrl.setDate('summer');
            $rootScope.$apply();

            expect(date).toBe("June 15, 2007");
        });

        it('should set date for maintenance item', function() {

            createController();
            $rootScope.$apply();
            ctrl.today = new Date("January 14 2007");
            ctrl.yyyy = ctrl.today.getFullYear();
            var date = ctrl.setDate('fall');
            $rootScope.$apply();

            expect(date).toBe("October 15, 2007");
        });

    });
    
    describe('ctrl.setYear', function(){
        
        it('should return the next year', function() {
            
            createController();
            $rootScope.$apply();
            var today = new Date();
            var yesterday = new Date(today.getTime());
            yesterday.setDate(today.getDate() - 1);
            var year = ctrl.setYear(yesterday, today);
            $rootScope.$apply();
            
            expect(year).toEqual(today.getFullYear() + 1);
        });
        
        it('should return the current year', function() {
            
            createController();
            $rootScope.$apply();
            var today = new Date();
            var tomorrow = new Date(today.getTime());
            tomorrow.setDate(today.getDate() + 1);
            var year = ctrl.setYear(tomorrow, today);
            $rootScope.$apply();
            
            expect(year).toEqual(today.getFullYear());
        });
    });

});

describe('homeownerFormMaintenanceItems', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        
        $scope.items = [];
        $scope.type = "";
        
        element = $compile('<homeowner-form-maintenance-items items="items" type="type"></homeowner-form-maintenance-items>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("Maintenance Items");
        });
    });
});