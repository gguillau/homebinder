angular
    .module("hb.homeowners.registration.components", [
        "hb.homeowners.registration.components.maintenanceItems",
        "hb.homeowners.registration.components.appliances",
        "hb.homeowners.registration.components.contractors",
        "hb.homeowners.registration.components.projects"
    ]);