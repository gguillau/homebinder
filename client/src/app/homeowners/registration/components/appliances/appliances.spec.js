describe("HomeownerFormAppliancesController", function() {
    
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $q,
        api;

    
    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        
        api = {
            applianceManufacturer: {
                all: function(){}
            }
        };

    }));
    
    beforeEach(function() {
        spyOn(api.applianceManufacturer, "all").and.returnValue($q.when({data: []}));
    });

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("HomeownerFormAppliancesController",
            {
                "hb.api": api
            });
            
        $scope.ctrl = ctrl;
        $scope.$apply();
        $scope.$digest();
        
    }
   
    describe('ctrl.remove', function(){
        
        it('should remove appliance at index 1 from appliances array', function() {
            
            createController();
            $rootScope.$apply();
            ctrl.remove(2);
            $rootScope.$apply();
            expect(ctrl.appliances.length).toEqual(8);
        });

    });
    
    describe('ctrl.addApp', function(){
        
        it('should add appliance to appliances array', function() {
            
            createController();
            $rootScope.$apply();
            ctrl.addApp();
            $rootScope.$apply();
            
            expect(ctrl.appliances.length).toEqual(10);
        });

    });
    
});

describe('homeownerFormAppliances', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        
        $scope.appliances = [];
        
        element = $compile('<homeowner-form-appliances appliances="appliances"></homeowner-form-appliances>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("Home Appliances");
        });
    });
});