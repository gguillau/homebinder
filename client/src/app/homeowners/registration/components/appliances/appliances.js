(function() {
    "use strict";

    angular
        .module("hb.homeowners.registration.components.appliances", [])
        .directive("homeownerFormAppliances", function() {
            return {
                restrict: "E",
                templateUrl: "homeowners/registration/components/appliances/appliances.tpl.html",
                controller: "HomeownerFormAppliancesController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    appliances: "="
                }
            };
        })
        .controller("HomeownerFormAppliancesController", [
            'hb.api',
            'hb.utils',
            'hb.resources',
            HomeownerFormAppliancesController
        ]);

    function HomeownerFormAppliancesController(api, utils, resources) {
        this.api = api;
        this.utils = utils;
        this.validationErrors = resources.validationErrors;
        this.appliances = [
            { name: "Refrigerator", value: "Refrigerator" },
            { name: "Dishwasher", value: "Dishwasher" },
            { name: "Microwave", value: "Microwave" },
            { name: "Stove", value: "Stove" },
            { name: "Oven/Range", value: "OvenRange" },
            { name: "Disposal", value: "Disposal" },
            { name: "Washer", value: "Washer" },
            { name: "Dryer", value: "Dryer" },
            { name: "Water Heater", value: "WaterHeater" }
        ];
        this.manufacturers = [
            "Bosch",
            "Ge",
            "Kenmore",
            "Kitchenaid",
            "Lg",
            "Samsung",
            "Whirlpool"
        ];
        this.is_app_collapsed = false;
    }

    HomeownerFormAppliancesController.prototype = {
        remove: function(index) {
            this.appliances.splice(index, 1);
        },

        addApp: function() {
            var appliance = { name: "", manufacturer: "", model: "", new_app: true };
            this.appliances.push(appliance);
        }
    };

})();