(function() {
    "use strict";

    angular
        .module("hb.homeowners.registration.components.projects", [])
        .directive("homeownerFormProjects", function() {
            return {
                restrict: "E",
                templateUrl: "homeowners/registration/components/projects/projects.tpl.html",
                controller: "HomeownerFormProjectsController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    projects: "="
                }
            };
        })
        .controller("HomeownerFormProjectsController", [
            "hb.utils",
            "hb.resources",
            HomeownerFormProjectsController
        ]);

    function HomeownerFormProjectsController(utils, resources) {
        this.is_projects_collapsed = false;
        this.utils = utils;
        this.projects = [];
        this.datepicker = {
            format: "yyyy",
            options: {
                datepickerMode: "'year'",
                minMode: "'year'",
                minDate: "minDate",
                showWeeks: "false"
            }
        };
        this.validationErrors = resources.validationErrors;
    }

    HomeownerFormProjectsController.prototype = {

        remove: function(index) {
            this.projects.splice(index, 1);
        },

        addProject: function() {
            var item = { name: "", end_date: "", details: "", cost_cents: 0 };
            this.projects.push(item);
        }
    };

})();