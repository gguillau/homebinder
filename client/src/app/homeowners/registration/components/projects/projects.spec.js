describe("HomeownerFormProjectsController", function() {
    
    var $rootScope,
        $scope,
        $controller,
        ctrl;

    
    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("HomeownerFormProjectsController",
            {

            });
            
        $scope.ctrl = ctrl;
        $scope.$apply();
        $scope.$digest();
        
    }
   
    describe('ctrl.remove', function(){
        
        it('should remove the project at index 0 from projects array', function() {
            var project = {name: "Reseal Driveway", year: "2008", notes: ""};
            createController();
            $rootScope.$apply();
            ctrl.projects.push(project);
            ctrl.remove(0);
            $rootScope.$apply();
            expect(ctrl.projects.length).toEqual(0);
        });

    });
    
    describe('ctrl.addProject', function(){
        
        it('should add a project to projects array', function() {
            
            createController();
            $rootScope.$apply();
            ctrl.addProject();
            $rootScope.$apply();
            
            expect(ctrl.projects.length).toEqual(1);
        });

    });
    
});

describe('homeownerFormProjects', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        
        $scope.projects = [];
        
        element = $compile('<homeowner-form-projects projects="projects"></homeowner-form-projects>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("Projects");
        });
    });
});