describe("HomeownerClaimBinderController", function() {
    var ctrl,
        controller,
        api,
        resources,
        $q,
        loading,
        notify,
        $log,
        Event,
        Session,
        $state,
        Address;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        $q = $injector.get("$q");
        loading = $injector.get("Loading");
        notify = $injector.get("Notify");
        $log = $injector.get("$log");
        Event = $injector.get("Event");
        Session = $injector.get("Session");
        $state = $injector.get("$state");
        Address = $injector.get("Address");

    }));

    function createController() {
        ctrl = controller("HomeownerClaimBinderController", {
            "hb.api": api,
            "hb.resources": resources,
            "$q": $q,
            "Loading": loading,
            "Notify": notify,
            "$log": $log,
            "Event": Event,
            "Session": Session,
            "$state": $state,
            "Address": Address
        });
    }

    describe("init", function() {
        it("sets the initial variables", function() {
            createController();
            expect(ctrl.model.searchData).toEqual({country: "US"});
            expect(ctrl.model.verifyData).toEqual({});
            expect(ctrl.model.step).toEqual(1);
        });
    });
    
    describe("searchBinders", function() {
        it("calls api.userBinder.find_by_property", function() {
            spyOn(api.userBinder, "find_by_property").and.returnValue($q.when({ data: [] }));
            spyOn(loading, "show");
            
            createController();
            spyOn(ctrl.model, "formatAddress");
            ctrl.model.searchBinders();
            
            expect(loading.show).toHaveBeenCalled();
            expect(ctrl.model.noBindersPendingError).toEqual(false);
            expect(ctrl.model.noBindersError).toEqual(false);
            expect(ctrl.model.formatAddress).toHaveBeenCalled();
            expect(api.userBinder.find_by_property).toHaveBeenCalled();
        });
    });
    
    describe("formatAddress", function() {
        it("trims the white space and removes the last word", function() {
            createController();
            
            ctrl.model.searchData.address1 = "  1234 Main St.  ";
            ctrl.model.formatAddress();
            
            expect(ctrl.model.originalAddress).toEqual("  1234 Main St.  ");
            expect(ctrl.model.searchData.address1).toEqual("1234 Main");
        });
    });
    
    describe("onUserBinderSuccess", function() {
        it("sets noBindersError to true when there are no results", function() {
            spyOn(loading, "close");
            
            createController();
            spyOn(ctrl.model, "checkForPendingBinders");
            
            var response = {
                data: []
            };
            ctrl.model.originalAddress = "original";
            ctrl.model.onUserBinderSuccess(response);
            
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.model.results).toEqual([]);
            expect(ctrl.model.noBindersError).toEqual(true);
            expect(ctrl.model.searchData.address1).toEqual("original");
            expect(ctrl.model.checkForPendingBinders).not.toHaveBeenCalled();
        });
        
        it("calls checkForPendingBinders", function() {
            spyOn(loading, "close");
            
            createController();
            spyOn(ctrl.model, "checkForPendingBinders");
            
            var response = {
                data: [{id: 1}]
            };
            ctrl.model.onUserBinderSuccess(response);
            
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.model.results).toEqual([{id: 1}]);
            expect(ctrl.model.noBindersError).toEqual(undefined);
            expect(ctrl.model.checkForPendingBinders).toHaveBeenCalled();
        });
    });

    describe("checkForPendingBinders", function() {
        it("does not go to step 2 if no binder is pending", function() {
            createController();
            
            var results = [{
                transfer_status: "created"
            }, {
                transfer_status: "created"
            }];
            ctrl.model.results = results;
            ctrl.model.originalAddress = "original";
            ctrl.model.checkForPendingBinders();
            
            expect(ctrl.model.pendingBinder).toEqual(false);
            expect(ctrl.model.step).toEqual(1);
            expect(ctrl.model.noBindersPendingError).toEqual(true);
            expect(ctrl.model.searchData.address1).toEqual("original");
        });
        
        it("goes to step 2 when at least one binder is pending", function() {
            createController();
            
            var results = [{
                transfer_status: "pending"
            }, {
                transfer_status: "created"
            }];
            ctrl.model.results = results;
            ctrl.model.checkForPendingBinders();
            
            expect(ctrl.model.pendingBinder).toEqual(true);
            expect(ctrl.model.step).toEqual(2);
            expect(ctrl.model.noBindersPendingError).toEqual(undefined);
        });
    });
    
    describe("verifyBinder", function() {
        it("sets the preferred candidates and chooses the first", function() {
            createController();
            spyOn(ctrl.model, "formatPhone");
            spyOn($state, "go");
            
            ctrl.model.country.code = 1;
            ctrl.model.verifyData = {
                email: "test@email.com",
                phone: "5555555555"
            };
            
            var results = [{
                owner: {
                    email: "test@email.com",
                    phone: "+15555555555"
                },
                access_token: "access1",
                transfer_status: "pending"
            }, {
                owner: {
                    email: "test@email.com",
                    phone: "+15555555555"
                },
                access_token: "access2",
                transfer_status: "pending"
            }, {
                owner: {
                    email: "test@email.com",
                    phone: "+12222222222"
                },
                access_token: "access3",
                transfer_status: "pending"
            }, {
                owner: {
                    email: "test@email.com",
                    phone: "+12222222222"
                },
                access_token: "access4",
                transfer_status: "pending"
            }, {
                owner: {
                    email: "test+mismatch@email.com",
                    phone: "+12222222222"
                },
                access_token: "access5",
                transfer_status: "pending"
            }];
            ctrl.model.results = results;
            ctrl.model.verifyBinder();
            
            expect($state.go).toHaveBeenCalledWith("homeowners.welcome", {accessToken: "access1"});
            expect(ctrl.model.noMatchError).toEqual(false);
        });
        
        it("sets the candidates and chooses the first", function() {
            createController();
            spyOn(ctrl.model, "formatPhone");
            spyOn($state, "go");
            
            ctrl.model.country.code = 1;
            ctrl.model.verifyData = {
                email: "test@email.com",
                phone: "5555555555"
            };
            
            var results = [{
                owner: {
                    email: "test@email.com",
                    phone: "+15555555555"
                },
                access_token: "access1",
                transfer_status: "created"
            }, {
                owner: {
                    email: "test@email.com",
                    phone: "+15555555555"
                },
                access_token: "access2",
                transfer_status: "created"
            }, {
                owner: {
                    email: "test@email.com",
                    phone: "+12222222222"
                },
                access_token: "access3",
                transfer_status: "pending"
            }, {
                owner: {
                    email: "test@email.com",
                    phone: "+12222222222"
                },
                access_token: "access4",
                transfer_status: "pending"
            }, {
                owner: {
                    email: "test+mismatch@email.com",
                    phone: "+12222222222"
                },
                access_token: "access5",
                transfer_status: "pending"
            }];
            ctrl.model.results = results;
            ctrl.model.verifyBinder();
            
            expect($state.go).toHaveBeenCalledWith("homeowners.welcome", {accessToken: "access3"});
            expect(ctrl.model.noMatchError).toEqual(false);
        });
        
        it("does not find any candidates", function() {
            createController();
            spyOn(ctrl.model, "formatPhone");
            spyOn($state, "go");
            
            ctrl.model.country.code = 1;
            ctrl.model.verifyData = {
                email: "test@email.com",
                phone: "5555555555"
            };
            
            var results = [{
                owner: {
                    email: "test@email.com",
                    phone: "+15555555555"
                },
                access_token: "access1",
                transfer_status: "created"
            }, {
                owner: {
                    email: "test+mismatch@email.com",
                    phone: "+445555555555"
                },
                access_token: "access2",
                transfer_status: "pending"
            }, {
                owner: {
                    email: "",
                    phone: ""
                },
                access_token: "access3",
                transfer_status: "pending"
            }, {
                owner: {
                    email: "test@email.com",
                    phone: "+12222222222"
                },
                access_token: "access4",
                transfer_status: "created"
            }, {
                owner: {
                    email: "test+mismatch@email.com",
                    phone: "+12222222222"
                },
                access_token: "access5",
                transfer_status: "pending"
            }];
            ctrl.model.results = results;
            ctrl.model.verifyBinder();
            
            expect($state.go).not.toHaveBeenCalled();
            expect(ctrl.model.noMatchError).toEqual(true);
        });
    });
    
    describe("formatPhone", function() {
        it("formats the phone number", function() {
            createController();
            
            ctrl.model.country.code = 1;
            ctrl.model.verifyData.phone = "1234567890";
            ctrl.model.formatPhone();
            
            expect(ctrl.model.formattedPhone).toEqual("+11234567890");
        });
    });
    
    describe("onSelect", function() {
        it("set the country", inject(function() {
            createController();
            
            ctrl.model.currentUser = {
                role: "admin"
            };
            var item = { country: { value: "US" } };
            ctrl.model.item = {
                user_profile_attributes: {
                    address_attributes: {

                    }
                }
            };
            ctrl.model.onSelect(item);

            expect(ctrl.model.country).toEqual(item.country);
        }));
    });

    describe("onSelectCountry", function() {
        it("sets the country", inject(function() {
            createController();
            
            ctrl.model.onSelectCountry({ value: "MA" });

            expect(ctrl.model.country.value).toEqual("MA");
        }));
    });
    
    describe("onError", function() {
        it("calls $log, notify, and loading", function() {
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "close");

            createController();
            ctrl.model.onError({ data: "error"});

            expect($log.error).toHaveBeenCalledWith({ data: "error"});
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.close).toHaveBeenCalled();
        });
    });

});

describe('homeownerClaimBinder', function() {
    var $scope, $compile, api, $stateParams, $q, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $stateParams = _$injector_.get("$stateParams");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        $stateParams.accessToken = 1;

        spyOn(api.user, "welcome").and.returnValue($q.when({ data: { id: 1, sign_in_count: 1, user_profile_attributes: { id: 1, default_locale: "en-us" } } }));

        element = $compile('<homeowner-claim-binder></homeowner-claim-binder>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {
            expect(element.html()).toContain("Claim Transferred HomeBinder");
        });
    });
});