(function() {
	"use strict";

	angular
		.module("hb.homeowners.claimBinder", [])
		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("homeowners.claimBinder", {
					url: "^/homeowners/claim_binder",
					template: "<homeowner-claim-binder></homeowner-claim-binder>"
				});
		}])
		.directive("homeownerClaimBinder", function() {
			return {
				restrict: "E",
				templateUrl: "homeowners/claimBinder/claimBinder.tpl.html",
				controller: "HomeownerClaimBinderController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("HomeownerClaimBinderController", [
			"hb.api",
			"hb.resources",
			"$q",
			"Loading",
			"Notify",
			"$log",
			"Event",
			"Session",
			"$state",
			"Address",
			HomeownerClaimBinderController
		]);

	function HomeownerClaimBinderController(api, resources, $q, loading, notify, $log, event, session, $state, Address) {
		var Model = function() {
			this.api = api;
			this.resources = resources.claimBinder;
			this.resources = angular.extend({}, this.resources, resources.userWelcome);
			this.resources = angular.extend({}, this.resources, resources.standardLanding);
			this.resources = angular.extend({}, this.resources, resources.homeownerOnboarding);
			this.validationErrors = resources.validationErrors;
			this.$q = $q;
			this.loading = loading;
			this.notify = notify;
			this.$log = $log;
			this.$state = $state;
			this.Address = Address;
			this.countries = Address.countries();
			this.country = this.countries[0];
			this.states = Address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
				return state.country.value === this.country.value;
			}));
			this.searchData = {country: "US"};
			this.verifyData = {};
			this.step = 1;
		};

		Model.prototype = Object.create(HomeownerClaimBinderController.prototype);

		angular.extend(Model.prototype, {
			searchBinders: function() {
				this.loading.show(this.resources.oneMoment);
				this.noBindersPendingError = false;
				this.noBindersError = false;
				this.bindersAcceptedError = false;
				
				// format address for better searching
				this.formatAddress();
				
				this.api.userBinder.find_by_property(this.searchData).then(
					angular.bind(this, this.onUserBinderSuccess),
					angular.bind(this, this.onError)
				);
			},
			
			// remove trailing whitespace and remove last word from search (remove street type)
			formatAddress: function() {
				// store original address in case the search fails, since we don't want front-end data to be altered
				this.originalAddress = this.searchData.address1;
				this.searchData.address1 = this.searchData.address1.trim();
				var lastIndex = this.searchData.address1.lastIndexOf(" ");
				this.searchData.address1 = this.searchData.address1.substring(0, lastIndex);
			},

			onUserBinderSuccess: function(response) {
				this.results = response.data;
				this.loading.close();

				if (this.results.length < 1) {
					this.noBindersError = true;
					this.searchData.address1 = this.originalAddress;
				}
				else {
					this.checkForPendingBinders();
				}
			},
			
			// go to step 2 if at least 1 retrieved binder's transfer is pending, else display an error
			checkForPendingBinders: function() {
				this.pendingBinder = false;
				for (var i = 0; i < this.results.length; i++) {
					if (this.results[i].transfer_status === "pending") {
						this.pendingBinder = true;
					}
				}

				if (this.pendingBinder) {
					this.step = 2;
				}
				else {
					this.checkForAcceptedBinders();
				}
			},
			
			// Binders already claimed ,if all the retrieved binder's transfer is accepted, else display no pending binders
			checkForAcceptedBinders: function(){
				for (var i = 0; i < this.results.length; i++) {
					this.acceptedBinder = false;
					if (this.results[i].transfer_status === "accepted") {
						this.acceptedBinder = true;
					}
				}
				
				if (this.acceptedBinder) {
					var address = this.searchData.address1 + " " + this.searchData.city + ", " + this.searchData.state + " " + this.searchData.country;
					this.bindersAcceptedError = true;
					this.bindersAcceptedErrorMessage = "Sorry, a binder with address " + address + " has already been claimed";
				}
				else {
					this.noBindersPendingError = true;
				}
				this.searchData.address1 = this.originalAddress;
			},

			verifyBinder: function() {
				this.noMatchError = false;
				this.multipleMatchError = false;
				var preferredCandidates = [];
				var candidates = [];

				// add country code to phone string for searching
				this.formatPhone();

				// check if any of the binders match the verification
				for (var i = 0; i < this.results.length; i++) {
					if (this.results[i].owner.email === this.verifyData.email && this.results[i].owner.phone === this.formattedPhone && this.results[i].transfer_status === "pending") {
						preferredCandidates.push(this.results[i]);
					}
					else if ((this.results[i].owner.email === this.verifyData.email || this.results[i].owner.phone === this.formattedPhone) && this.results[i].transfer_status === "pending") {
						candidates.push(this.results[i]);
					}
				}

				// take user to a preferred binder if possible, otherwise candidate, otherwise no match
				if (preferredCandidates.length > 0) {
					this.$state.go("homeowners.welcome", {accessToken: preferredCandidates[0].access_token});
				}
				else if (candidates.length > 0) {
					this.$state.go("homeowners.welcome", {accessToken: candidates[0].access_token});
				}
				else {
					this.noMatchError = true;
				}
			},

			// add country code to phone for searching
			formatPhone: function() {
				this.formattedPhone = "+" + this.country.code + this.verifyData.phone;
			},

			onSelect: function(item) {
				if (item) {
					this.country = item.country;
					this.searchData.country = item.country.value;
				}
			},

			onSelectCountry: function(item) {
				this.country = item;
				this.states = Address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
					return state.country.value === this.country.value;
				}));
			},

			// on error for API calls
			onError: function(response) {
				this.$log.error(response);
				this.notify.error(response.data);
				this.loading.close();
			}

		});

		this.model = new Model();
	}
})();