describe("PasswordResetController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $log,
        api,
        notify,
        loading,
        $templateCache,
        $compile,
        template,
        passwordForm,
        $state;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$templateCache_, _$compile_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        $compile = _$compile_;
        $templateCache = _$templateCache_;

        $state = {
            go: function() {}
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        notify = {
            error: function() {},
            clearAll: function() {},
            info: function() {}
        };

        api = {
            user: {
                resetPassword: function(info) {}
            }
        };

        $log = {
            error: function(msg) {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("PasswordResetController", {
            "hb.api": api,
            "$log": $log,
            "Notify": notify,
            "Loading": loading,
            "$state": $state
        });
        $scope.ctrl = ctrl;
        template = $templateCache.get("accounts/passwords/reset/passwordReset.tpl.html");
        $compile(template)($scope);
        passwordForm = $scope.ctrl.passwordForm;

    }

    describe('passwordForm', function() {

        it('should expect passwordForm to be defined and form validation to be false', function() {

            createController();

            ctrl.data.email = "";
            $rootScope.$apply();

            expect(passwordForm).toBeDefined();
            expect(passwordForm.$valid).toBe(false);
            expect(passwordForm.email.$error.required).toBe(true);

        });

        it('should expect passwordForm to be defined and form validation to be true', function() {

            createController();

            ctrl.data.email = "tst@gmail.com";
            $rootScope.$apply();

            expect(passwordForm).toBeDefined();
            expect(passwordForm.$valid).toBe(true);

        });

    });

    describe("ctrl.submit", function() {
        
        it("return because form is invalid", function() {

            spyOn(api.user, "resetPassword").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(notify, "info");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            ctrl.data.email = "test@gmail.com";
            $rootScope.$apply();
            ctrl.passwordForm.$invalid = true;
            ctrl.submit();
            $rootScope.$apply();

            expect(ctrl.sent).not.toBe(true);
            expect(notify.info).not.toHaveBeenCalledWith("Password reset");
            expect(loading.show).not.toHaveBeenCalledWith("Resetting password...");
            expect(loading.close).not.toHaveBeenCalled();
        });

        it("should successfully reset user password", function() {

            spyOn(api.user, "resetPassword").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(notify, "info");
            spyOn(loading, "show");
            spyOn(loading, "close");

            var year = new Date().getFullYear();
            createController();
            ctrl.data.email = "test@gmail.com";
            $rootScope.$apply();
            ctrl.submit();
            $rootScope.$apply();

            expect(ctrl.sent).toBe(true);
            expect(ctrl.year).toBe(year);
            expect(notify.info).toHaveBeenCalledWith("Password reset");
            expect(loading.show).toHaveBeenCalledWith("Resetting password...");
            expect(loading.close).toHaveBeenCalled();
            expect(api.user.resetPassword).toHaveBeenCalledWith("test@gmail.com");
        });

        it('should attempt to reset user password and return an error', function() {

            spyOn(api.user, "resetPassword").and.returnValue($q.reject({
                data: {
                    message: "error"
                }
            }));
            spyOn($log, "error");
            spyOn(notify, "info");
            spyOn(loading, "show");
            spyOn(loading, "close");


            createController();
            ctrl.data.email = "test@gmail.com";
            $rootScope.$apply();
            ctrl.submit();
            $rootScope.$apply();

            expect($log.error).toHaveBeenCalledWith({
                data: {
                    message: "error"
                }
            });
            expect(notify.info).toHaveBeenCalledWith({
                message: "error"
            });
            expect(loading.show).toHaveBeenCalledWith("Resetting password...");
            expect(loading.close).toHaveBeenCalled();
        });

        it('should attempt to reset user password and return an error and send user to root', function() {

            spyOn(api.user, "resetPassword").and.returnValue($q.reject({
                data: {
                    message: "error"
                }
            }));
            spyOn($state, "go");
            spyOn($log, "error");
            spyOn(notify, "info");
            spyOn(loading, "show");
            spyOn(loading, "close");


            createController();
            ctrl.data.email = "test@gmail.com";
            ctrl.failed_login_attempts = 4;
            $rootScope.$apply();
            ctrl.submit();
            $rootScope.$apply();

            expect($log.error).toHaveBeenCalledWith({
                data: {
                    message: "error"
                }
            });
            expect(loading.show).toHaveBeenCalledWith("Resetting password...");
            expect(loading.close).toHaveBeenCalled();
            expect(notify.info).toHaveBeenCalledWith("Too many failed attempts.", 5000);
            expect($state.go).toHaveBeenCalledWith("root");
        });

    });

    describe("ctrl.failedAttempts", function() {

        it("should increase the amount of failed attempts", function() {

            spyOn(notify, "info");

            createController();
            ctrl.failed_login_attempts = 1;
            $rootScope.$apply();
            ctrl.failedAttempts({data: "error"});
            $rootScope.$apply();

            expect(ctrl.failed_login_attempts).toEqual(2);
            expect(notify.info).toHaveBeenCalled();
        });

        it("should increase the amount of failed attempts to 5 and send user to password reset", function() {

            spyOn(notify, "info");
            spyOn(notify, "clearAll");
            spyOn($state, "go");

            createController();
            ctrl.failed_login_attempts = 4;
            $rootScope.$apply();
            ctrl.failedAttempts({data: "error"});
            $rootScope.$apply();

            expect(ctrl.failed_login_attempts).toEqual(5);
            expect(notify.clearAll).toHaveBeenCalled();
            expect(notify.info).toHaveBeenCalled();
            expect($state.go).toHaveBeenCalled();
        });

    });


});
