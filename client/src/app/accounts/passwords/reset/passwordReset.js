(function() {
	"use strict";

	angular.module("hb.accounts.passwords.reset", [
			"ui.router",
			"hb.components",
			"hb.api"
		])
		.config(['$stateProvider', passwordsConfig])
		.controller("PasswordResetController", [
			"$state",
			"hb.api",
			"$log",
			"hb.resources",
			"Loading",
			"Notify",
			PasswordResetController
		]);

	function PasswordResetController($state, api, $log, resources, loading, notify) {
		this.$state = $state;
		this.api = api;
		this.$log = $log;
		this.validationErrors = resources.validationErrors;
		this.resources = resources.passwordReset;
		this.loading = loading;
		this.notify = notify;
		this.year = new Date().getFullYear();
		this.sent = false;
		this.data = {};
		this.passwordForm = {};
		this.failed_login_attempts = 0; //track failed reset attempts
	}

	PasswordResetController.prototype = {

		submit: function() {
			this.passwordForm.$submitted = true;
			if (this.passwordForm.$invalid) {
				return;
			}
			this.loading.show(this.resources.loading);
			this.api.user.resetPassword(this.data.email)
				.then(
					angular.bind(this, this.resetSent),
					angular.bind(this, this.resetError)
				);
		},

		resetSent: function(response) {
			this.notify.info(this.resources.resetSentText);
			this.sent = true;
			this.loading.close();
		},

		resetError: function(response) {
			this.passwordForm.$submitted = false;
			this.$log.error(response);
			this.failedAttempts(response);
			this.loading.close();
		},

		failedAttempts: function(response) {
			this.failed_login_attempts++;
			if (this.failed_login_attempts === 5) {
				this.notify.clearAll();
				this.notify.info(this.resources.failedText, 5000);
				this.$state.go("root");
			}
			else {
				this.notify.info(response.data);
			}
		}
	};

	function passwordsConfig($stateProvider) {
		$stateProvider
			.state("password_reset", {
				url: "^/passwords/reset",
				templateUrl: "accounts/passwords/reset/passwordReset.tpl.html",
				controller: "PasswordResetController",
				controllerAs: "ctrl"
			});
	}
})();