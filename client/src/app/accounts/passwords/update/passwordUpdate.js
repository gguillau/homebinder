(function() {
	"use strict";

	angular.module("hb.accounts.passwords.update", [
			"ui.router",
			"hb.components",
			"hb.api"
		])
		.config(['$stateProvider', passwordsConfig])
		.controller("PasswordUpdateController", [
			"$state",
			"$stateParams",
			"hb.api",
			"Notify",
			"$log",
			"Loading",
			"hb.resources",
			"$location",
			PasswordUpdateController
		]);

	function PasswordUpdateController($state, $stateParams, api, Notify, $log, loading, resources, $location) {
		this.$state = $state;
		this.api = api;
		this.notify = Notify;
		this.$log = $log;
		this.loading = loading;
		this.data = {};
		this.passwordForm = {};
		this.year = new Date().getFullYear();
		this.token = $stateParams["token"];
		this.userId = $location.search().userId;
		this.failed_login_attempts = 0; //track failed update attempts
		this.resources = angular.extend({}, this.resources, resources.passwordUpdate);
        this.resources = angular.extend({}, this.resources, resources.validationErrors);
		this.refresh();
	}

	PasswordUpdateController.prototype = {

		refresh: function() {
			this.loading.show(this.resources.loading);
			this.api.user.confirm_password(this.userId, this.token)
				.then(
					angular.bind(this, this.userSuccess),
					angular.bind(this, this.userError)
				);
		},

		userSuccess: function(response) {
			this.data.email = response.data.email;
			this.loading.close();
		},

		userError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		submit: function() {
			this.passwordForm.$submitted = true;
			if (this.passwordForm.$invalid) {
				return;
			}
			var data = {
				email: this.data.email,
				reset_password_token: this.token,
				password: this.data.first_password,
				password_confirmation: this.data.first_password
			};
			this.loading.show(this.resources.loading);
			this.api.user.updatePassword(data)
				.then(
					angular.bind(this, this.passwordUpdated),
					angular.bind(this, this.passwordUpdateError)
				);
		},

		passwordUpdated: function(response) {
			this.notify.info(this.resources.passwordUpdated);
			this.loading.close();
			this.$state.go("login");
		},

		passwordUpdateError: function(response) {
			this.passwordForm.$submitted = false;
			this.$log.error(response);
			this.failedAttempts(response);
			this.loading.close();
		},

		failedAttempts: function(response) {
			this.failed_login_attempts++;
			if (this.failed_login_attempts === 5) {
				this.notify.clearAll();
				this.notify.info(this.resources.failedText, 5000);
				this.$state.go("root");
			}
			else {
				this.notify.error(response.data);
			}
		}
	};

	function passwordsConfig($stateProvider) {
		$stateProvider
			.state("passwordupdate", {
				url: "^/passwords/:token/update",
				templateUrl: "accounts/passwords/update/passwordUpdate.tpl.html",
				controller: "PasswordUpdateController",
				controllerAs: "ctrl"
			});
	}
})();