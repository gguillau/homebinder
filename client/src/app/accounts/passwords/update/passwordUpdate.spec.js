describe("PasswordUpdateController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $log,
        $state,
        $stateParams,
        notify,
        api,
        loading,
        $templateCache,
        $compile,
        template,
        passwordForm;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$templateCache_, _$compile_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $compile = _$compile_;
        $templateCache = _$templateCache_;
        $stateParams = {};

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        $state = {
            go: function() {}
        };

        $log = {
            error: function(msg) {}
        };

        notify = {
            info: function(msg) {},
            error: function(msg) {},
            clearAll: function() {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("PasswordUpdateController", {
            "hb.api": api,
            "$log": $log,
            "$state": $state,
            "$stateParams": $stateParams,
            "Notify": notify,
            "Loading": loading
        });
        $scope.ctrl = ctrl;
        template = $templateCache.get("accounts/passwords/update/passwordUpdate.tpl.html");
        $compile(template)($scope);
        passwordForm = $scope.ctrl.passwordForm;
    }

    describe('passwordForm', function() {

        it('should expect passwordForm to be defined and form validation to be false due to missing email', function() {

            createController();

            ctrl.data.email = "";
            ctrl.data.first_password = "password";
            ctrl.data.second_password = "password";
            $rootScope.$apply();

            expect(passwordForm).toBeDefined();
            expect(passwordForm.$valid).toBe(false);
            expect(passwordForm.email.$error.required).toBe(true);

        });

        it('should expect passwordForm to be defined and form validation to be false due to missing first_password', function() {

            createController();

            ctrl.data.email = "test@gmail.com";
            ctrl.data.first_password = "";
            ctrl.data.second_password = "password";
            $rootScope.$apply();

            expect(passwordForm).toBeDefined();
            expect(passwordForm.$valid).toBe(false);
            expect(passwordForm.first_password.$error.required).toBe(true);

        });
    

        it('should expect passwordForm to be defined and form validation to be false due to missing second_password', function() {

            createController();

            ctrl.data.email = "test@gmail.com";
            ctrl.data.first_password = "password";
            ctrl.data.second_password = "";
            $rootScope.$apply();

            expect(passwordForm).toBeDefined();
            expect(passwordForm.$valid).toBe(false);
            expect(passwordForm.second_password.$error.required).toBe(true);

        });
        
        it('should expect passwordForm to be defined and form validation to be false due to mismatched passwords', function() {

            createController();

            ctrl.data.email = "test@gmail.com";
            ctrl.data.first_password = "password";
            ctrl.data.second_password = "p0assword";
            $rootScope.$apply();

            expect(passwordForm).toBeDefined();
            expect(passwordForm.$valid).toBe(false);
            expect(passwordForm.second_password.$error.compareTo).toBe(true);

        });
        
        it('should expect passwordForm to be defined and form validation to be false due to mismatched password case', function() {

            createController();

            ctrl.data.email = "test@gmail.com";
            ctrl.data.first_password = "password";
            ctrl.data.second_password = "PaSsWoRd";
            $rootScope.$apply();

            expect(passwordForm).toBeDefined();
            expect(passwordForm.$valid).toBe(false);
            expect(passwordForm.second_password.$error.compareTo).toBe(true);

        });

        it('should expect passwordForm to be defined and form validation to be false due to mismatched password regex', function() {

            createController();

            ctrl.data.email = "test@gmail.com";
            ctrl.data.first_password = "********";
            ctrl.data.second_password = "12345678";
            $rootScope.$apply();

            expect(passwordForm).toBeDefined();
            expect(passwordForm.$valid).toBe(false);
            expect(passwordForm.second_password.$error.compareTo).toBe(true);

        });
        
        it('should expect passwordForm to be defined and form validation to be false due to mismatched password regex word characters', function() {

            createController();

            ctrl.data.email = "test@gmail.com";
            ctrl.data.first_password = "\\w\\w\\w\\w";
            ctrl.data.second_password = "1234";
            $rootScope.$apply();

            expect(passwordForm).toBeDefined();
            expect(passwordForm.$valid).toBe(false);
            expect(passwordForm.second_password.$error.compareTo).toBe(true);

        });

        it('should expect passwordForm to be defined and form validation to be true due to valid entries', function() {

            createController();

            ctrl.data.email = "test@gmail.com";
            ctrl.data.first_password = "password";
            ctrl.data.second_password = "password";
            $rootScope.$apply();

            expect(passwordForm).toBeDefined();
            expect(passwordForm.$valid).toBe(true);

        });

    });

    describe("ctrl.submit", function() {

        it("return since form is invalid", function() {

            $stateParams["token"] = "token";
            spyOn(api.user, "updatePassword").and.returnValue($q.when({
                data: "success"
            }));

            createController();
            ctrl.data.email = "test@gmail.com";
            ctrl.data.first_password = "password";
            ctrl.data.second_password = "password";
            $rootScope.$apply();
            ctrl.passwordForm.$invalid = true;
            ctrl.submit();
            $rootScope.$apply();

            expect(api.user.updatePassword).not.toHaveBeenCalledWith({
                reset_password_token: "token",
                email: "test@gmail.com",
                password: "password",
                password_confirmation: "password"
            });
        });

        it("should successfully update user password", function() {

            $stateParams["token"] = "token";
            spyOn(api.user, "updatePassword").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(notify, "info");
            spyOn($state, "go");

            var year = new Date().getFullYear();
            createController();
            ctrl.data.email = "test@gmail.com";
            ctrl.data.first_password = "password";
            ctrl.data.second_password = "password";
            $rootScope.$apply();
            ctrl.submit();
            $rootScope.$apply();

            expect(api.user.updatePassword).toHaveBeenCalledWith({
                reset_password_token: "token",
                email: "test@gmail.com",
                password: "password",
                password_confirmation: "password"
            });
            expect(notify.info).toHaveBeenCalledWith("Your password has been changed.");
            expect($state.go).toHaveBeenCalledWith("login");
            expect(ctrl.year).toBe(year);
            expect(ctrl.token).toBe("token");
        });

        it("should attempt to update user password and return an error", function() {

            $stateParams["token"] = "token";
            spyOn(api.user, "updatePassword").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            ctrl.data.email = "test@gmail.com";
            ctrl.data.first_password = "password";
            ctrl.data.second_password = "password";
            $rootScope.$apply();
            ctrl.submit();
            $rootScope.$apply();

            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(ctrl.token).toBe("token");
        });

        it("should attempt to update user password and return an error and send user to root", function() {

            $stateParams["token"] = "token";
            spyOn(api.user, "updatePassword").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($state, "go");
            spyOn($log, "error");
            spyOn(notify, "info");

            createController();
            ctrl.data.email = "test@gmail.com";
            ctrl.data.first_password = "password";
            ctrl.data.second_password = "password";
            ctrl.failed_login_attempts = 4;
            $rootScope.$apply();
            ctrl.submit();
            $rootScope.$apply();

            expect($log.error).toHaveBeenCalledWith({
                data: "error"
            });
            expect($state.go).toHaveBeenCalledWith("root");
            expect(notify.info).toHaveBeenCalledWith("Too many failed attempts.", 5000);
            expect(ctrl.token).toBe("token");
        });

    });

    describe("ctrl.failedAttempts", function() {

        it("should increase the amount of failed attempts", function() {

            spyOn(notify, "error");

            createController();
            ctrl.failed_login_attempts = 1;
            $rootScope.$apply();
            ctrl.failedAttempts({ data: "error" });
            $rootScope.$apply();

            expect(ctrl.failed_login_attempts).toEqual(2);
            expect(notify.error).toHaveBeenCalled();
        });

        it("should increase the amount of failed attempts to 5 and send user to password reset", function() {

            spyOn(notify, "info");
            spyOn(notify, "clearAll");
            spyOn($state, "go");

            createController();
            ctrl.failed_login_attempts = 4;
            $rootScope.$apply();
            ctrl.failedAttempts({ data: "error" });
            $rootScope.$apply();

            expect(ctrl.failed_login_attempts).toEqual(5);
            expect(notify.clearAll).toHaveBeenCalled();
            expect(notify.info).toHaveBeenCalled();
            expect($state.go).toHaveBeenCalled();
        });

    });


});
