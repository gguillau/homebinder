(function() {
    "use strict";
    
    angular.module("hb.accounts.passwords", [
        "ui.router",
        "hb.components",
        "hb.api",
		"hb.accounts.passwords.reset",
		"hb.accounts.passwords.update"
    ]);
})();