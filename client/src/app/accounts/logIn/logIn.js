(function() {
    "use strict";

    angular.module("hb.accounts.logIn", [
            "ui.router",
            "hb.components",
            "hb.api"
        ])
        .config(['$stateProvider', logInConfig])
        .controller("LoginController", [
            "hb.api",
            "AfterLogin",
            "hb.resources",
            "$log",
            "Loading",
            "Notify",
            "$state",
            "Session",
            "hb.utils",
            LoginController
        ]);

    function LoginController(api, afterLogin, resources, $log, loading, notify, $state, Session, utils) {
        this.api = api;
        this.afterLogin = afterLogin;
        this.validationErrors = resources.validationErrors;
        this.resources = resources.logIn;
        this.utils = utils.utils;
        this.$log = $log;
        this.loading = loading;
        this.notify = notify;
        this.$state = $state;
        this.Session = Session;
        this.loginForm = {};
        this.data = {};
        this.data.rememberMe = true;
        this.year = new Date().getFullYear();
        this.failed_login_attempts = 0; //track failed login attempts
        this.deviceName = "Unknown";
        this.deviceVersion = "Unknown";
        this.operatingSystem = "Unknown";
        this.init();
    }

    LoginController.prototype = {

        init: function() {
            var user = this.Session.getUser();
            if (user) {
                this.afterLogin.go();
            }
            else {
                var device = this.utils.getDeviceInfo();
                this.deviceName = device[0];
                this.deviceVersion = device[1];
                this.operatingSystem = this.utils.getOperatingSystem();
            }
        },

        submit: function() {
            this.loginForm.$submitted = true;
            if (this.loginForm.$invalid) {
                return;
            }
            this.loading.show(this.resources.loadingText);
            var logonData = {
                email: this.data.email,
                password: this.data.password,
                rememberMe: this.data.rememberMe,
                device_type: "web",
                device_name: this.deviceName,
                device_version: this.deviceVersion,
                operating_system: this.operatingSystem
            };
            this.api.user.logon(logonData)
                .then(
                    angular.bind(this, this.loginSuccess),
                    angular.bind(this, this.loginError)
                );
        },

        loginSuccess: function(response) {
            this.hideForm = true;
            this.loading.close();
            this.afterLogin.go();
        },

        loginError: function(response) {
            this.loginForm.$submitted = false;
            this.$log.error(response);
            this.failedAttempts(response);
            this.loading.close();
        },

        failedAttempts: function(response) {
            this.failed_login_attempts++;
            if (this.failed_login_attempts === 5) {
                this.notify.clearAll();
                this.notify.info(this.resources.failedText, 5000);
                this.$state.go("password_reset");
            }
            else {
                this.notify.error(response.data);
            }
        }

    };

    function logInConfig($stateProvider) {
        $stateProvider
            .state("login", {
                url: "^/login",
                templateUrl: "accounts/logIn/logIn.tpl.html",
                controller: "LoginController",
                controllerAs: "ctrl"
            });
    }
})();