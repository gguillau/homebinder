describe("LoginController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        AfterLogin,
        $log,
        api,
        loading,
        notify,
        $templateCache,
        $compile,
        template,
        loginForm,
        $state,
        session;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$templateCache_, _$compile_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        $compile = _$compile_;
        $templateCache = _$templateCache_;
        session = _$injector_.get("Session");

        $state = {
            go: function() {}
        };

        notify = {
            error: function() {},
            clearAll: function() {},
            info: function() {}
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        api = {
            user: {
                logon: function(info) {}
            }
        };

        AfterLogin = {
            go: function() {}
        };

        $log = {
            error: function(msg) {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("LoginController", {
            "hb.api": api,
            "AfterLogin": AfterLogin,
            "$log": $log,
            "Loading": loading,
            "Notify": notify,
            "$state": $state,
            "Session": session
        });
        $scope.ctrl = ctrl;
        template = $templateCache.get("accounts/logIn/logIn.tpl.html");
        $compile(template)($scope);
        loginForm = $scope.ctrl.loginForm;

    }

    describe('loginForm', function() {

        it('should expect loginForm to be defined and form validation to be false', function() {

            createController();

            ctrl.data.email = "";
            ctrl.data.password = "";
            $rootScope.$apply();

            expect(loginForm).toBeDefined();
            expect(loginForm.$valid).toBe(false);
            expect(loginForm.email.$error.required).toBe(true);
            expect(loginForm.password.$error.required).toBe(true);

        });


        it('should expect loginForm to be defined and form validation to be false', function() {

            createController();

            ctrl.data.email = "test@gmail.com";
            ctrl.data.password = "";
            $rootScope.$apply();

            expect(loginForm).toBeDefined();
            expect(loginForm.$valid).toBe(false);
            expect(loginForm.password.$error.required).toBe(true);

        });

        it('should expect loginForm to be defined and form validation to be false', function() {

            createController();

            ctrl.data.email = "o5jjr5lwbvqrfchmyno4lap9b9n1uufqfwki2222adyivkjr61agzjt6sewm9k5kh7jjx1zglu6rmc6bc9yko7xcm26g9t5qvck2kgqz7lqf6s4mb39yagwh9vsr1t6pu46zhi7msijdo4mon8yulkb45g7fbvx9velmaevwfguamuhtk3dcpbkoxbzta6x6rsgada7dh2lyrb23o419853zc9j9p1wq5wg4lcleezcaoclre22jaasdasdsdasdasd@gmail.com";
            ctrl.data.password = "password";
            $rootScope.$apply();
            expect(loginForm).toBeDefined();
            expect(loginForm.$valid).toBe(false);
            expect(loginForm.email.$error.maxlength).toBe(true);

        });

        it('should expect loginForm to be defined and form validation to be false', function() {

            createController();

            ctrl.data.email = "o5jjr5lwbvqrfchmyno4lap9b9n1uufqfwki2222adyivkjr61agzjt6sewm9k5kh7jjx1zglu6rmc6bc9yko7xcm26g9t5qvck2kgqz7lqf6s4mb39yagwh9vsr1t6pu46zhi7msijdo4mon8yulkb45g7fbvx9velmaevwfguamuhtk3dcpbkoxbzta6x6rsgada7dh2lyrb23o419853zc9j9p1wq5wg4lcleezcaoclre22jaasdasdsdasdasd@gmail.com";
            ctrl.data.password = "1couon73atqg84i84tgnjani1h0td08ytdyr9lnpxf4st0pgixjosiunvowjl9zv5k9ma5xcyyi2khvly9hqrgu9nople1gp9crp6s2wlw4ujgzeqiaxi4usyjmubguu6";
            $rootScope.$apply();

            expect(loginForm).toBeDefined();
            expect(loginForm.$valid).toBe(false);
            expect(loginForm.password.$error.maxlength).toBe(true);

        });

        it('should expect loginForm to be defined and form validation to be true', function() {

            createController();

            ctrl.data.email = "test@gmail.com";
            ctrl.data.password = "password";
            $rootScope.$apply();

            expect(loginForm).toBeDefined();
            expect(loginForm.$valid).toBe(true);

        });

    });

    describe("ctrl.init", function() {
        it("call afterLogin go", function() {
            spyOn(session, "getUser").and.returnValue({ id: 1 });
            spyOn(AfterLogin, "go");

            createController();
            ctrl.init();

            expect(AfterLogin.go).toHaveBeenCalled();
        });

    });

    describe("ctrl.submit", function() {

        it("doesnt call the api", function() {

            spyOn(api.user, "logon").and.returnValue($q.when({
                data: "success"
            }));

            createController();
            ctrl.data.email = "test@gmail.com";
            ctrl.data.password = "password";
            ctrl.data.rememberMe = false;
            $rootScope.$apply();
            ctrl.loginForm.$invalid = true;
            ctrl.submit();
            $rootScope.$apply();


            expect(api.user.logon).not.toHaveBeenCalled();

        });

        it("should successfully log user in", function() {

            spyOn(api.user, "logon").and.returnValue($q.when({
                data: "success"
            }));
            spyOn(loading, "show");
            spyOn(loading, "close");
            spyOn(AfterLogin, "go");

            var year = new Date().getFullYear();
            createController();
            ctrl.data.email = "test@gmail.com";
            ctrl.data.password = "password";
            ctrl.data.rememberMe = false;
            $rootScope.$apply();
            ctrl.submit();
            $rootScope.$apply();

            expect(AfterLogin.go).toHaveBeenCalled();
            expect(ctrl.year).toBe(year);
            expect(loading.show).toHaveBeenCalledWith("Logging in...");
            expect(loading.close).toHaveBeenCalled();
            expect(api.user.logon).toHaveBeenCalled();
            expect(ctrl.hideForm).toBe(true);

        });

        it("should attempt to log user in and return an error", function() {

            spyOn(api.user, "logon").and.returnValue($q.reject({
                data: {
                    message: "error"
                }
            }));
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            ctrl.data.email = "test@gmail.com";
            ctrl.data.password = "password";
            ctrl.data.rememberMe = false;
            $rootScope.$apply();
            ctrl.submit();
            $rootScope.$apply();

            expect($log.error).toHaveBeenCalledWith({
                data: {
                    message: "error"
                }
            });
            expect(notify.error).toHaveBeenCalledWith({
                message: "error"
            });
            expect(loading.show).toHaveBeenCalledWith("Logging in...");
            expect(loading.close).toHaveBeenCalled();
        });

        it("should attempt to log user in and return an error and send user to password reset page", function() {

            spyOn(api.user, "logon").and.returnValue($q.reject({
                data: {
                    message: "error"
                }
            }));
            spyOn($state, "go");
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(notify, "info");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            ctrl.data.email = "test@gmail.com";
            ctrl.data.password = "password";
            ctrl.data.rememberMe = false;
            ctrl.failed_login_attempts = 4;
            $rootScope.$apply();
            ctrl.submit();
            $rootScope.$apply();

            expect($log.error).toHaveBeenCalledWith({
                data: {
                    message: "error"
                }
            });
            expect(notify.info).toHaveBeenCalledWith("Too many failed login attempts. Please reset your password.", 5000);
            expect(loading.show).toHaveBeenCalledWith("Logging in...");
            expect(loading.close).toHaveBeenCalled();
            expect($state.go).toHaveBeenCalledWith("password_reset");
        });

    });

    describe("ctrl.failedAttempts", function() {

        it("should increase the amount of failed attempts", function() {

            spyOn(notify, "error");

            createController();
            ctrl.failed_login_attempts = 1;
            $rootScope.$apply();
            ctrl.failedAttempts({ data: "error" });
            $rootScope.$apply();

            expect(ctrl.failed_login_attempts).toEqual(2);
            expect(notify.error).toHaveBeenCalled();
        });

        it("should increase the amount of failed attempts to 5 and send user to password reset", function() {

            spyOn(notify, "info");
            spyOn(notify, "clearAll");
            spyOn($state, "go");

            createController();
            ctrl.failed_login_attempts = 4;
            $rootScope.$apply();
            ctrl.failedAttempts({ data: "error" });
            $rootScope.$apply();

            expect(ctrl.failed_login_attempts).toEqual(5);
            expect(notify.clearAll).toHaveBeenCalled();
            expect(notify.info).toHaveBeenCalled();
            expect($state.go).toHaveBeenCalled();
        });

    });

});
