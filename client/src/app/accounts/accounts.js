(function() {
    "use strict";
    
    angular.module("hb.accounts", [
        "hb.accounts.afterLogin",
        "hb.accounts.logIn",
        "hb.accounts.passwords"
    ]);
})();