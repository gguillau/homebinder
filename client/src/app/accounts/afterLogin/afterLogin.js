(function() {
    "use strict";

    angular
        .module("hb.accounts.afterLogin", [])
        .factory("AfterLogin", [
            "$state",
            "$http",
            "hb.api",
            "Session",
            "ModalService",
            "constants",
            "$location",
            "$intercom",
            "hb.userRole",
            "Notify",
            "Loading",
            "$window",
            "Appcues",
            function($state, $http, api, Session, ModalService, constants, $location, $intercom, userRole, notify, loading, $window, Appcues) {
                var user = null,
                    id = null,
                    nextState = null,
                    nextParams = null;

                function go() {
                    user = api.user.current();
                    Session.setLocale(user.default_locale, false);
                    Appcues.identify(user);

                    // check the user role
                    if (user.role == userRole.HOMEOWNER) {
                        loadIntercom(user);
                        if (nextState) {
                            $state.go(nextState, nextParams);
                            nextState = nextParams = null;
                        }
                        else {
                            loadBinders(user);
                        }
                    }
                    else {
                        if (nextState) {
                            $state.go(nextState, nextParams);
                            nextState = nextParams = null;
                        }
                        else {
                            goToDashboard();
                        }
                    }
                }

                function goToDashboard() {
                    user = api.user.current();
                    // check the user role
                    if (user) {
                        if (user.role == userRole.HOMEOWNER) {
                            Appcues.identify(user);
                            loadIntercom(user);
                            loadBinders(user);
                        }
                        else if (user.role == userRole.AGENT) {
                            $state.go("agent.binders", {
                                userId: user.id
                            });
                            loadIntercom(user);
                        }
                        else if (user.role == userRole.PARTNER ||
                            user.role == userRole.BROKER ||
                            user.role == "lender" ||
                            user.role == "hoa" ||
                            user.role == "insurance" ||
                            user.role == "homepro" ||
                            user.role == "builder" ||
                            user.role == "property_manager" ||
                            user.role == userRole.INSPECTOR) {
                            $state.go("partner.binders", {
                                partnerId: user.partner_id
                            });
                            loadIntercom(user);
                        }
                        else if (user.role == userRole.REVIEWER) {
                            $state.go("partner.annualPropertyReviews", {
                                partnerId: user.partner_id
                            });
                        }
                        else if (user.role === userRole.ADMIN || user.role === "limited_admin") {
                            // hide intercom for admins
                            Intercom('shutdown');
                            $state.go("admin.users");
                        }
                    }
                    else {
                        loading.close();
                        $state.go("root");
                    }
                }

                function loadBinders(user) {
                    // Then determine where to go based on the binders
                    api.binder.all({ searchMethod: "for_user", userId: user.id }).then(
                        function(result) {
                            if (result.data.total === 0) {
                                loading.close();
                                // Go to new binder
                                $state.go("binders.new");
                            }
                            else if (result.data.total == 1) {
                                // Only 1 binder go to it
                                $state.go("binder.overview", {
                                    binderId: result.data.items[0].id
                                });
                            }
                            else {
                                // check for a primary binder. If there is 1 go to it
                                angular.forEach(result.data.items, function(binder) {
                                    if (binder.primary) {
                                        $state.go("binder.overview", {
                                            binderId: binder.id
                                        });
                                        return;
                                    }
                                });

                                // Go to the list of binders
                                $state.go("binders");
                            }
                        }
                    );
                }

                /*  detect when a user is on a mobile device to
                    prevent the intercom chat window from loading
                    iPhone 6s width: 328 - height: 558
                */

                function onMobile() {
                    if ($window.innerWidth <= 700 && $window.innerHeight <= 800) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }

                function loadIntercom(user) {
                    var host = $location.host();
                    id = host === "www.homebinder.com" ? constants.ProductionIntercomAppId : constants.TestIntercomAppId;
                    if (!onMobile()) {
                        window.Intercom('boot', {
                            app_id: id,
                            email: user.email,
                            user_hash: user.intercom_key
                        });

                    }

                    if (window.LogRocket) {
                        switch (window.location.hostname) {
                            case "www.homebinder.com": // production

                                window.LogRocket.identify(user.id, {
                                    id: user.id,
                                    email: user.email,
                                    created_at: user.created_at,
                                    role: user.role
                                });

                                window.LogRocket.getSessionURL(function(sessionURL) {
                                    if (!onMobile()) {
                                        window.Intercom("trackEvent", "LogRocket", { sessionURL: sessionURL });
                                    }
                                });

                                window.Sentry.configureScope(function(scope) {
                                    scope.setUser({
                                        id: user.id,
                                        email: user.email,
                                        created_at: user.created_at,
                                        role: user.role,
                                        logRocketUrl: window.LogRocket.sessionURL
                                    });
                                });

                                break;
                        }
                    }

                }

                return {
                    go: function() {
                        go();
                    },
                    goToDashboard: function() {
                        goToDashboard();
                    },
                    setNextState: function(state, params) {
                        nextState = state;
                        nextParams = params;
                    }
                };
            }
        ]);
})();