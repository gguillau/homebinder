describe("AfterLogin", function() {
    var AfterLogin,
        api,
        $q,
        $location,
        $state;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        AfterLogin = _$injector_.get("AfterLogin");
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        $location = _$injector_.get("$location");
        $state = _$injector_.get("$state");
    }));

    describe('go', function() {

        it('call loadIntercom and loadBinders', function() {
            var user = {role: "homeowner"};
            spyOn(api.user, "current").and.returnValue(user);
            spyOn(api.binder, "all").and.returnValue($q.when({
                data: []
            }));
            spyOn($location, "host").and.returnValue("www.homebinder.com");

            AfterLogin.go();

            expect($location.host).toHaveBeenCalled();
            expect(api.binder.all).toHaveBeenCalled();
        });

        it("call $state object", function() {
            var user = {
                role: "admin"
            };
            spyOn(api.user, "current").and.returnValue(user);
            spyOn($state, "go");

            AfterLogin.go();

            expect($state.go).toHaveBeenCalledWith("admin.users");
            expect(api.user.current).toHaveBeenCalled();
        });


    });

});