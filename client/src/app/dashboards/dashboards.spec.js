describe("DashboardController", function() {
    var DashboardController,
        TestClass;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the Session service
        $provide.factory("Session", function() {
            return {
                getJwt: function() {},
                getLocale: function() {
                    return null;
                },
                setLocale: function() {}
            };
        });

    }));
    // get the base class under test
    beforeEach(inject(function($injector) {
        DashboardController = $injector.get("DashboardController");
    }));

    function createTestClass() {
        TestClass = function() {
            DashboardController.call(this);
            this.cfg = {};
        };

        TestClass.prototype = Object.create(DashboardController.prototype);

        return new TestClass();
    }

    describe("init", function() {
        it("sets the title", inject(function() {
            var model = createTestClass();
            expect(model.resources.title).toEqual("Dashboards");
        }));
    });

    describe("setHeaders", function() {
        it("creates the headers", inject(function() {
            var model = createTestClass();
            model.setHeaders();

            expect(model.headers.length).toEqual(4);
        }));
    });

    describe("addDashboard", function() {
        it("calls HBDashboardModal show", inject(function(HBDashboardModal) {
            spyOn(HBDashboardModal, "show");
            var model = createTestClass();
            model.addDashboard();

            expect(HBDashboardModal.show).toHaveBeenCalled();
        }));
    });

    describe("editDashboard", function() {
        it("calls HBDashboardModal show", inject(function(HBDashboardModal) {
            spyOn(HBDashboardModal, "show");
            var model = createTestClass();
            model.editDashboard();

            expect(HBDashboardModal.show).toHaveBeenCalled();
        }));
    });

});