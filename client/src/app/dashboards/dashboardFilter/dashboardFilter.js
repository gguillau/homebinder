angular
    .module("hb.dashboards.dashboardFilter", [])
    .directive('hbDashboardFilter', [
        function() {
            return {
                restrict: "E",
                templateUrl: "dashboards/dashboardFilter/dashboardFilter.tpl.html",
                controller: "HBDashboardFilterController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    cfg: "="
                }
            };
        }
    ])
    .controller('HBDashboardFilterController', [
        '$scope',
        'hb.api',
        function($scope, api) {
            var self = this;
            this.category = null;
            this.item = null;
            this.lookupCfg = null;
            this.showLookup = false;
            this.disableFilter = false;
            this.categories = [{
                    id: "none",
                    label: "None"
                },
                {
                    id: "org",
                    label: "Organization",
                    lookupCfg: {
                        placeholder: "Search for organization",
                        refresh: lookupOrg,
                        itemTemplate: "admin/dashboards/lookupItemTemplate.tpl.html",
                        selectionTemplate: "admin/dashboards/lookupItemSelection.tpl.html"
                    }
                },
                {
                    id: "partner",
                    label: "Partner",
                    lookupCfg: {
                        placeholder: "Search for partner",
                        refresh: lookupPartner,
                        itemTemplate: "admin/dashboards/lookupItemTemplate.tpl.html",
                        selectionTemplate: "admin/dashboards/lookupItemSelection.tpl.html"
                    }
                }
            ];
            this.executeFilter = executeFilter;
            this.lookupOrg = lookupOrg;
            this.lookupPartner = lookupPartner;
            this.lookupComplete = lookupComplete;

            $scope.$watch('ctrl.category', function(newCategory) {
                self.lookupCfg = newCategory ? newCategory.lookupCfg : null;
                self.showLookup = self.lookupCfg != null;
                self.item = null;
                setButtonState();
            });

            $scope.$watch('ctrl.item', function() {
                setButtonState();
            });

            function setButtonState() {
                self.disableFilter = (self.category === null || self.category.id != 'none') && self.item === null;
            }

            function lookupOrg(search) {
                return api.organization.all({ search: search }).then(
                    lookupComplete
                );
            }

            function lookupPartner(search) {
                return api.partner.all({ search: search }).then(
                    lookupComplete
                );
            }

            function lookupComplete(response) {
                return { data: response.data.items };
            }

            function executeFilter() {
                if (self.cfg.filter) {
                    self.cfg.filter(self.category, self.item);
                }
            }
        }
    ]);