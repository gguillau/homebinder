describe("HBDashboardFilterController", function() {
    var controller,
        base,
        api,
        resources,
        $q;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });

        // mock the Session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return {
                        role: "admin"
                    };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, _$q_, $rootScope) {
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        $q = _$q_;

        controller = $controller("HBDashboardFilterController", {
            "hb.framework.indexBase": base,
            "hb.api": api,
            "hb.resources": resources,
            "$scope": $rootScope
        });

        $rootScope.$apply();
    }));

    describe("init", function() {
        it("sets the controller", function() {
            expect(controller.disableFilter).toBe(true);
        });
    });

    describe("lookupOrg", function() {
        it("calls api organization all", function() {
            spyOn(api.organization, "all").and.returnValue($q.when({ data: { items: [] } }));
            controller.lookupOrg("search");

            expect(api.organization.all).toHaveBeenCalled();
        });
    });

    describe("lookupPartner", function() {
        it("calls api partner all", function() {
            spyOn(api.partner, "all").and.returnValue($q.when({ data: { items: [] } }));
            controller.lookupPartner("search");

            expect(api.partner.all).toHaveBeenCalled();
        });
    });

    describe("lookupComplete", function() {
        it("returns the items", function() {
            var response = controller.lookupComplete({ data: { items: [{ id: 1 }] } });
            expect(response.data.length).toEqual(1);
        });
    });

    describe("executeFilter", function() {
        it("calls the cfg filter function", function() {
            controller.cfg = {
                filter: function() {}
            };
            spyOn(controller.cfg, "filter");
            controller.executeFilter();

            expect(controller.cfg.filter).toHaveBeenCalled();
        });
    });
});

describe('hbDashboardFilter', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = {};

        element = $compile('<hb-dashboard-filter cfg="cfg"></hb-dashboard-filter>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {
            expect(element.html()).toContain("Start typing");
        });
    });
});