(function() {
	"use strict";

	angular.module('hb.dashboards', [
			"hb.dashboards.dashboardModal",
			"hb.dashboards.dashboardFilter"
		])
		.factory("DashboardController", [
			'hb.framework.indexBase',
			'hb.api',
			'HBDashboardModal',
			'hb.resources',
			function(IndexBase, api, dashboardModal, resources) {
				var Model = function() {
					// call the parent class
					IndexBase.call(this);
					// apply widget specific resource strings
					this.resources = angular.extend({}, this.resources, resources.dashboards);
					// set the refresh API call
					this.refreshCall = api.dashboard.all;
					// set the delete API call
					this.deleteCall = api.dashboard.destroy;
					// set the name property used in the delete message
					this.nameProperty = "name";
					this.setHeaders();
				};

				Model.prototype = Object.create(IndexBase.prototype);

				angular.extend(Model.prototype, {
					setHeaders: function() {
						this.headers = this.resources.headers.map(angular.bind(this, function(attribute) {
							var sortable = false,
								orderBy = null,
								sorted = false,
								show = true;
							if (attribute === "ID") {
								sorted = true;
								sortable = true;
								orderBy = "dashboards.id";
							}
							else if (attribute === "Name") {
								sortable = true;
								orderBy = "dashboards.name";
							}
							else if (attribute === "Description") {
								sortable = true;
								orderBy = "dashboards.description";
							}
							else if (attribute === "Default") {
								sortable = true;
								orderBy = "dashboards.system";
							}
							return {
								name: attribute,
								sortable: sortable,
								sorted: sorted,
								orderBy: orderBy,
								show: show,
								order: "desc"
							};
						}));
						this.sortOption = this.headers[0];
					},
					addDashboard: function() {
						dashboardModal.show({
							admin: this.cfg.admin,
							partnerId: this.cfg.partnerId,
							organizationId: this.cfg.organizationId,
							dashboardSaved: angular.bind(this, this.onAdded)
						});
					},
					editDashboard: function(board) {
						dashboardModal.show({
							admin: this.cfg.admin,
							partnerId: this.cfg.partnerId,
							organizationId: this.cfg.organizationId,
							dashboard: board,
							dashboardSaved: angular.bind(this, this.onUpdated)
						});
					}
				});

				return Model;
			}

		]);
})();