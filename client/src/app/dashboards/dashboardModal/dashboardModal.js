angular
    .module("hb.dashboards.dashboardModal", [])
    .factory('HBDashboardModal', [
        '$modal',
        function($modal) {
            return {
                show: function(args) {
                    var modalInstance = $modal.open({
                        templateUrl: "dashboards/dashboardModal/dashboardModal.tpl.html",
                        controller: "DashboardModalController as ctrl",
                        backdrop: "static",
                        resolve: {
                            args: function() {
                                return args;
                            }
                        }
                    });

                    return modalInstance;
                }
            };
        }
    ])
    .controller('DashboardModalController', [
        'hb.api',
        'hb.resources',
        'args',
        '$modalInstance',
        'HBWidgetPicker',
        'Notify',
        '$log',
        function(api, resources, args, $modalInstance, hbWidgetPicker, notify, $log) {
            var that = this;
            var Model = function() {
                this.args = args;
                this.resources = resources.dashboardModal;
                this.dashboard = args ? args.dashboard : null;
                this.name = null;
                this["default"] = false;
                this.description = null;
                this.widgets = [];
                this.showDetails = true;
                this.showWidgets = false;
                this.admin = args.admin;
                this.typeOptions = [
                    { id: "binder", label: this.resources.binderType },
                    { id: "user", label: this.resources.userType },
                    { id: "admin", label: this.resources.adminType },
                    { id: "inspector", label: this.resources.inspectorType },
                    { id: "broker", label: this.resources.brokerType },
                    { id: "partner", label: this.resources.partnerType },
                    { id: "agent", label: this.resources.agentType }
                ];
                this.scope = this.typeOptions[0];
                if (args.admin) {
                    this.appliesTo = "system";
                    this.lookupCfg = {
                        placeholder: this.resources.lookupPlaceholder,
                        refresh: angular.bind(this, this.lookup),
                        itemTemplate: "dashboards/dashboardModal/lookupItemTemplate.tpl.html",
                        selectionTemplate: "dashboards/dashboardModal/lookupItemSelection.tpl.html"
                    };
                }
                this.widgetHostConfig = {
                    dragDrop: true,
                    editMode: true,
                    previewMode: true
                };
                if (args.organizationId) {
                    this.organizationId = args.organizationId;
                }
                if (args.partnerId) {
                    this.partnerId = args.partnerId;
                }
                if (args.userId) {
                    this.userId = args.userId;
                }
                if (this.dashboard) {
                    this.populate();
                }
            };

            angular.extend(Model.prototype, {
                populate: function() {
                    if (!this.dashboard) {
                        return;
                    }

                    this.name = this.dashboard.name;
                    this.description = this.dashboard.description;
                    this["default"] = this.dashboard["default"];
                    if (this.admin) {
                        var k, len;
                        for (k = 0, len = this.typeOptions.length; k < len; k++) {
                            if (this.typeOptions[k].id == this.dashboard.scope) {
                                this.scope = this.typeOptions[k];
                                break;
                            }
                        }
                        if (this.dashboard.partner_id) {
                            this.appliesTo = "partner";
                            this.applyTo = this.dashboard.partner;
                        }
                        else if (this.dashboard.organization_id) {
                            this.appliesTo = "org";
                            this.applyTo = this.dashboard.organization;
                        }
                        else if (this.dashboard.user_id) {
                            this.appliesTo = "user";
                            this.applyTo = this.dashboard.user;
                        }
                        else {
                            this.appliesTo = "system";
                        }
                    }
                    this.widgets = this.dashboard.dashboard_widgets.sort(function(a, b) {
                        return a.index < b.index ? -1 : a.index > b.index ? 1 : 0;
                    });
                },
                lookup: function(search) {
                    if (this.appliesTo == "partner") {
                        return api.partner.all({ search: search }).then(
                            angular.bind(this, this.lookupComplete)
                        );
                    }
                    else {
                        return api.organization.all({ search: search }).then(
                            angular.bind(this, this.lookupComplete)
                        );
                    }
                },
                lookupComplete: function(response) {
                    return { data: response.data.items };
                },
                showLookup: function() {
                    return this.appliesTo == "org" || this.appliesTo == "partner";
                },
                viewDetails: function() {
                    this.showDetails = true;
                    this.showWidgets = false;
                },
                viewWidgets: function() {
                    this.showDetails = false;
                    this.showWidgets = true;
                },
                addWidgets: function() {
                    hbWidgetPicker.show({
                        addWidget: angular.bind(this, this.widgetAdded)
                    });
                },
                widgetAdded: function(widget) {
                    this.widgets.push({ widget_id: widget.id, widget_key: widget.key });
                },
                save: function() {
                    var promise;

                    that.form.$submitted = true;
                    if (that.form.$invalid) {
                        return;
                    }

                    var data = {
                        dashboard: {
                            name: this.name,
                            description: this.description
                        },
                        widgets: this.widgets.map(function(widget, index) {
                            return {
                                id: widget.id,
                                widget_id: widget.widget_id,
                                index: index
                            };
                        })
                    };

                    // default is a keyword
                    data.dashboard["default"] = this["default"];

                    if (this.partnerId) {
                        data.dashboard.partner_id = this.partnerId;
                        data.dashboard.system = false;
                        data.dashboard.scope = "binder";
                    }
                    else if (this.organizationId) {
                        data.dashboard.organization_id = this.organizationId;
                        data.dashboard.system = false;
                        data.dashboard.scope = "binder";
                    }
                    else {
                        switch (this.appliesTo) {
                            case "system":
                                data.dashboard.system = true;
                                break;
                            case "partner":
                                data.dashboard.system = false;
                                data.dashboard.partner_id = this.applyTo.id;
                                break;
                            case "org":
                                data.dashboard.system = false;
                                data.dashboard.organization_id = this.applyTo.id;
                                break;
                            default:
                                break;
                        }
                        data.dashboard.scope = this.scope.id;
                    }

                    if (this.dashboard) {
                        promise = api.dashboard.update(this.dashboard.id, data);
                    }
                    else {
                        promise = api.dashboard.create(data);
                    }

                    promise.then(
                        angular.bind(this, this.saved),
                        angular.bind(this, this.saveError)
                    );
                },
                saved: function(response) {
                    var dashboard = response.data;

                    if (args.dashboardSaved) {
                        args.dashboardSaved(dashboard);
                    }

                    $modalInstance.close(this.dashboard);
                },
                saveError: function(response) {
                    notify.error(response.data);
                    $log.error(response.data);
                },
                cancel: function() {
                    $modalInstance.dismiss();
                }
            });

            this.model = new Model();
        }
    ]);