describe("DashboardModalController", function() {
    var ctrl,
        controller,
        $q,
        $log,
        api,
        notify,
        resources,
        loading,
        $modal,
        args,
        HBDashboardModal,
        hbWidgetPicker;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
        HBDashboardModal = $injector.get("HBDashboardModal");
        hbWidgetPicker = $injector.get("HBWidgetPicker");

        $modal = {
            open: function() {},
            dismiss: function(msg) {},
            close: function(arg) {}
        };
        var dashboard = { name: "Test", dashboard_widgets: [] };
        args = { admin: true, organizationId: 1, partnerId: 1, userId: 1, dashboard: dashboard };
    }));

    function createController() {
        ctrl = controller("DashboardModalController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "hb.resources": resources,
            "Loading": loading,
            "args": args,
            "$modalInstance": $modal
        });
    }

    describe("populate", function() {
        it("does not populate the dashboard", function() {
            createController();
            ctrl.model.dashboard = null;
            ctrl.model.populate();
        });

        it("populates the dashboard", function() {
            createController();
            ctrl.model.dashboard = {
                name: "Test",
                description: "Description",
                scope: "binder",
                dashboard_widgets: [
                    { index: 3 },
                    { index: 1 },
                    { index: 2 }
                ]
            };
            ctrl.model.populate();

            expect(ctrl.model.name).toEqual("Test");
            expect(ctrl.model.description).toEqual("Description");
            expect(ctrl.model.scope).toEqual({ id: 'binder', label: 'Binder' });
            expect(ctrl.model.widgets.length).toEqual(3);
            expect(ctrl.model.appliesTo).toEqual("system");
        });

        it("populates the dashboard with partnerId", function() {
            createController();
            ctrl.model.dashboard = {
                name: "Test",
                description: "Description",
                scope: "binder",
                partner_id: 1,
                partner: {
                    name: "Partner"
                },
                dashboard_widgets: [
                    { index: 3 },
                    { index: 1 },
                    { index: 2 }
                ]
            };
            ctrl.model.populate();

            expect(ctrl.model.name).toEqual("Test");
            expect(ctrl.model.description).toEqual("Description");
            expect(ctrl.model.scope).toEqual({ id: 'binder', label: 'Binder' });
            expect(ctrl.model.widgets.length).toEqual(3);
            expect(ctrl.model.appliesTo).toEqual("partner");
        });

        it("populates the dashboard with organizationId", function() {
            createController();
            ctrl.model.dashboard = {
                name: "Test",
                description: "Description",
                scope: "binder",
                organization_id: 1,
                organization: {
                    name: "Organization"
                },
                dashboard_widgets: [
                    { index: 3 },
                    { index: 1 },
                    { index: 2 }
                ]
            };
            ctrl.model.populate();

            expect(ctrl.model.name).toEqual("Test");
            expect(ctrl.model.description).toEqual("Description");
            expect(ctrl.model.scope).toEqual({ id: 'binder', label: 'Binder' });
            expect(ctrl.model.widgets.length).toEqual(3);
            expect(ctrl.model.appliesTo).toEqual("org");
        });
    });

    describe("lookupComplete", function() {
        it("returns the items", function() {
            createController();
            var response = ctrl.model.lookupComplete({ data: { items: [{ id: 1 }] } });

            expect(response.data.length).toEqual(1);
        });
    });

    describe("showLookup", function() {
        it("returns true", function() {
            createController();
            ctrl.model.appliesTo = "org";
            var value = ctrl.model.showLookup();

            expect(value).toBe(true);
        });

        it("returns true", function() {
            createController();
            ctrl.model.appliesTo = "partner";
            var value = ctrl.model.showLookup();

            expect(value).toBe(true);
        });

        it("returns false", function() {
            createController();
            ctrl.model.appliesTo = "system";
            var value = ctrl.model.showLookup();

            expect(value).toBe(false);
        });
    });

    describe("viewDetails", function() {
        it("sets show details to true and show widgets to false", function() {
            createController();
            ctrl.model.showDetails = false;
            ctrl.model.showWidgets = true;
            ctrl.model.viewDetails();

            expect(ctrl.model.showDetails).toBe(true);
            expect(ctrl.model.showWidgets).toBe(false);
        });
    });

    describe("viewWidgets", function() {
        it("sets show details to false and show widgets to true", function() {
            createController();
            ctrl.model.showDetails = true;
            ctrl.model.showWidgets = false;
            ctrl.model.viewWidgets();

            expect(ctrl.model.showDetails).toBe(false);
            expect(ctrl.model.showWidgets).toBe(true);
        });
    });

    describe("widgetAdded", function() {
        it("increments widgets length", function() {
            createController();
            ctrl.model.widgetAdded({ id: 1, key: "widget" });

            expect(ctrl.model.widgets.length).toEqual(1);
        });
    });

    describe("save", function() {
        it("does not submit invalid data", function() {
            spyOn(api.dashboard, "create").and.returnValue($q.when({}));
            createController();
            ctrl.form = {
                $invalid: true
            };
            ctrl.model.save();

            expect(ctrl.form.$submitted).toEqual(true);
            expect(api.dashboard.create).not.toHaveBeenCalled();
        });

        it("creates a new dashboard", function() {
            spyOn(api.dashboard, "create").and.returnValue($q.when({}));
            createController();
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.name = "dashboard";
            ctrl.model.description = "description";
            ctrl.model.dashboard = null;
            ctrl.model.widgets = [{ id: 1, widget_id: 1 }];
            ctrl.model.applyTo = "system";
            ctrl.model.partnerId = 1;
            ctrl.model.save();
            expect(api.dashboard.create).toHaveBeenCalled();
        });

        it("creates a new dashboard", function() {
            spyOn(api.dashboard, "create").and.returnValue($q.when({}));
            createController();
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.name = "dashboard";
            ctrl.model.description = "description";
            ctrl.model.dashboard = null;
            ctrl.model.widgets = [{ id: 1, widget_id: 1 }];
            ctrl.model.applyTo = "system";
            ctrl.model.partnerId = null;
            ctrl.model.organizationId = 1;
            ctrl.model.save();
            expect(api.dashboard.create).toHaveBeenCalled();
        });

        it("updates an existing dashboard", function() {
            spyOn(api.dashboard, "update").and.returnValue($q.when({}));
            createController();
            ctrl.model.dashboard = {
                id: 1
            };
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.name = "dashboard";
            ctrl.model.description = "description";
            ctrl.model.appliesTo = "system";
            ctrl.model.partnerId = null;
            ctrl.model.organizationId = null;
            ctrl.model.save();
            expect(api.dashboard.update).toHaveBeenCalled();
        });

        it("updates an existing dashboard for partner", function() {
            spyOn(api.dashboard, "update").and.returnValue($q.when({}));
            createController();
            ctrl.model.dashboard = {
                id: 1
            };
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.name = "dashboard";
            ctrl.model.description = "description";
            ctrl.model.appliesTo = "partner";
            ctrl.model.applyTo = { id: 1 };
            ctrl.model.partnerId = null;
            ctrl.model.organizationId = null;
            ctrl.model.save();
            expect(api.dashboard.update).toHaveBeenCalled();
        });

        it("updates an existing dashboard for org", function() {
            spyOn(api.dashboard, "update").and.returnValue($q.when({}));
            createController();
            ctrl.model.dashboard = {
                id: 1
            };
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.name = "dashboard";
            ctrl.model.description = "description";
            ctrl.model.appliesTo = "org";
            ctrl.model.applyTo = { id: 1 };
            ctrl.model.partnerId = null;
            ctrl.model.organizationId = null;
            ctrl.model.save();
            expect(api.dashboard.update).toHaveBeenCalled();
        });
    });

    describe("saved", function() {
        it("saves the response and calls dashboardSaved", function() {
            var dashboard = {
                id: 1,
                name: "dashboard",
                description: "description"
            };
            createController();
            args.dashboardSaved = function() {};
            spyOn(args, "dashboardSaved");
            ctrl.model.saved({ data: dashboard });

            expect(args.dashboardSaved).toHaveBeenCalled();
        });
    });

    describe("saveError", function() {
        it("notifies the user of an error", function() {
            spyOn(notify, "error");

            createController();
            ctrl.model.saveError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("cancel", function() {
        it("closes the modal", function() {
            spyOn($modal, "dismiss");

            createController();
            ctrl.model.cancel();

            expect($modal.dismiss).toHaveBeenCalled();
        });
    });

    describe('HBDashboardModal', function() {
        it('should call show and then $modal open', function() {
            HBDashboardModal.show({});
        });
    });

    describe("lookup", function() {
        it("lookups up partners", function() {
            createController();
            spyOn(api.partner, "all").and.returnValue($q.when({ data: { items: [] } }));
            ctrl.model.appliesTo = "partner";
            ctrl.model.lookup();

            expect(api.partner.all).toHaveBeenCalled();
        });

        it("lookups up organizations", function() {
            createController();
            spyOn(api.organization, "all").and.returnValue($q.when({ data: { items: [] } }));
            ctrl.model.appliesTo = "org";
            ctrl.model.lookup();

            expect(api.organization.all).toHaveBeenCalled();
        });
    });

    describe("addWidgets", function() {
        it("calls hbWidgetPicker show", function() {
            createController();
            spyOn(hbWidgetPicker, "show");
            ctrl.model.addWidgets();

            expect(hbWidgetPicker.show).toHaveBeenCalled();
        });
    });
});