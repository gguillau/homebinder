describe("WidgetsController", function() {
    var controller,
        base,
        api,
        resources,
        modal;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        modal = $injector.get("WidgetModal");

        spyOn(api.widget, "all").and.returnValue($q.when({ data: { total: 1, items: [{ id: 1, name: "name" }] } }));
        spyOn(api.widget, "destroy").and.returnValue($q.when({}));

        controller = $controller("WidgetIndexController", {
            "hb.framework.indexBase": base,
            "hb.api": api,
            "hb.resources": resources,
            "WidgetModal": modal
        });

        $rootScope.$apply();
    }));

    describe("refresh", function() {
        it("calls hb.api.widget.all", function() {
            controller.model.refresh();
            expect(api.widget.all).toHaveBeenCalled();
        });
    });

    describe("destroy", function() {
        it("calls hb.api.widget.destroy", function() {
            controller.model.onConfirmDeleteItem({ id: 1, name: "name" }, 0);
            expect(api.widget.destroy).toHaveBeenCalled();
        });
    });

    describe("add widget", function() {
        it("calls widgetModal.showForm", inject(function(WidgetModal) {
            spyOn(WidgetModal, "showForm");
            controller.model.addWidget();
            expect(WidgetModal.showForm).toHaveBeenCalled();
        }));
    });

    describe("edit widget", function() {
        it("calls wigetModal.editForm", inject(function(WidgetModal) {
            spyOn(WidgetModal, "showForm");
            controller.model.editWidget({ id: 1, name: "widget" });
            expect(WidgetModal.showForm).toHaveBeenCalledWith(jasmine.objectContaining({ widget: { id: 1, name: "widget" } }));
        }));
    });

    describe("formOpts", function() {
        it("returns asAdmin as true", inject(function(WidgetModal) {
            controller.model.cfg = { byAdmin: true };
            var opts = controller.model.formOpts();
            expect(opts.asAdmin).toBe(true);
        }));

        it("returns asPartner as true", inject(function(WidgetModal) {
            controller.model.cfg = { byPartner: true };
            var opts = controller.model.formOpts();
            expect(opts.asPartner).toBe(true);
        }));

        it("returns asOrg as true", inject(function(WidgetModal) {
            controller.model.cfg = { byOrg: true };
            var opts = controller.model.formOpts();
            expect(opts.asOrg).toBe(true);
        }));

        it("returns empty hash", inject(function(WidgetModal) {
            controller.model.cfg = { byUser: true };
            var opts = controller.model.formOpts();
            expect(opts).toEqual({});
        }));
    });
});

describe('hbWidgetIndex', function() {
    var $scope, $compile, element, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;

        spyOn(api.widget, "all").and.returnValue($q.when({ data: { items: [] } }));

        $scope.cfg = {};
        element = $compile('<hb-widget-index cfg="cfg"></hb-widget-index>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the hb-widget-index directive', function() {
        it('adds the title to the html', function() {
            expect(element.html()).toContain("New Widget");
        });
    });
});