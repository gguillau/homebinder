(function() {
	"use strict";

	angular.module('hb.widgets.widgetIndex', [])
		.directive("hbWidgetIndex", function() {
			return {
				restrict: "E",
				templateUrl: "widgets/widgetIndex/widgetIndex.tpl.html",
				controller: "WidgetIndexController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {
					cfg: '='
				}
			};
		})
		.controller("WidgetIndexController", [
			'hb.framework.indexBase',
			'hb.api',
			'hb.resources',
			'WidgetModal',
			Controller
		]);

	function Controller(IndexBase, api, resources, widgetModal) {
		var that = this;
		var Model = function() {
			// call the parent class
			IndexBase.call(this);

			// store the passed in config
			this.cfg = that.cfg;

			// apply widget specific resource strings
			this.resources = angular.extend({}, this.resources, resources.widgetIndex);

			// set the refresh API call
			this.refreshCall = api.widget.all;

			// set the delete API call
			this.deleteCall = api.widget.destroy;

			// set the name property used in the delete message
			this.nameProperty = "name";

			// set page specific values on toolbar
			angular.extend(this.toolbarCfg, {
				title: this.resources.title,
				button: {
					title: this.resources.newButton,
					click: angular.bind(this, this.addWidget)
				},
				sort: {
					title: "Sort by",
					refresh: angular.bind(this, this.refresh),
					sortOptions: [
						{ orderBy: "widgets.name", order: "asc", desc: this.resources.sortNameAsc },
						{ orderBy: "widgets.name", order: "desc", desc: this.resources.sortNameDesc },
						{ orderBy: "widgets.category", order: "asc", desc: this.resources.sortCategoryAsc },
						{ orderBy: "widgets.category", order: "desc", desc: this.resources.sortCategoryDesc }
					],
					execute: angular.bind(this, this.sort)
				}
			});
			this.toolbarCfg.sort.sortOption = this.toolbarCfg.sort.sortOptions[0];

			// refresh the list
			this.refresh();
		};

		Model.prototype = Object.create(IndexBase.prototype);

		angular.extend(Model.prototype, {
			formOpts: function(saved) {
				if (!this.cfg) {
					return {};
				}

				if (this.cfg.byAdmin) {
					return { asAdmin: true };
				}
				else if (this.cfg.byPartner) {
					return {
						asPartner: true,
						partnerId: this.cfg.partnerId
					};
				}
				else if (this.cfg.byOrg) {
					return {
						asOrg: true,
						orgId: this.cfg.orgId
					};
				}
				else {
					return {};
				}
			},
			addWidget: function() {
				var formOpts = this.formOpts();
				formOpts.onSaved = angular.bind(this, this.onAdded);
				widgetModal.showForm(formOpts);
			},
			editWidget: function(widget) {
				var formOpts = this.formOpts();
				formOpts.widget = widget;
				formOpts.onSaved = angular.bind(this, this.onUpdated);
				widgetModal.showForm(formOpts);
			}
		});

		this.model = new Model();
	}
})();