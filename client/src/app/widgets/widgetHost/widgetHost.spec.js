describe('hbWidgetHost', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        $scope.cfg = {};
        $scope.widgetHostCfg = {
            previewMode: true,
            editMode: true,
            dragDrop: true
        };
        $scope.widgets = [{ index: 0, widget_key: "thirdPartyServices", name: "Third Party Services Form" }];

        element = $compile('<hb-widget-host cfg="widgetHostCfg" widgets="widgets"></hb-widget-host>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the widget host directive', function() {
        it('displays the widget', function() {
            expect(element.html()).toContain("How Can We Help?");
        });

        it('calls widgetCollectionChanged and returns', function() {
            expect(element.html()).toContain("How Can We Help?");

            $scope.widgets = [];
            $scope.$apply();
        });

        it('calls widgetCollectionChanged and inserts the new widget', function() {
            expect(element.html()).toContain("How Can We Help?");

            $scope.widgets = [{ index: 1, widget_key: "apr", name: "APR Widget" }];
            $scope.$apply();

            expect(element.html()).not.toContain("How Can We Help?");
            expect(element.html()).toContain("When was the last time a professional reviewed your home?");

            element.find(".remove-button").click();

            // ensure the widget was removed
            expect(element.html()).not.toContain("When was the last time a professional reviewed your home?");
        });
    });
});