angular
    .module("hb.widgets.widgetHost", [])
    .directive("hbWidgetHost", [
        '$compile',
        '$rootScope',
        'hbWidget',
        function($compile, $scope, hbWidget) {
            return {
                scope: {
                    widgets: "=",
                    cfg: "=",
                    orderChanged: "&",
                    widgetRemoved: "&"
                },
                link: function($scope, element, attrs) {
                    var init = false,
                        freeze = false;

                    // get the HTML element we are adding to
                    element = element[0];

                    // listen for changes to the widgets collection
                    $scope.$watchCollection('widgets', widgetCollectionChanged);

                    function widgetCollectionChanged(newCollection) {
                        var widgetDef, widgetCfg;

                        // this function is called when the directive is first rendered.
                        // we want to ignore it in that case
                        if (!init) {
                            init = true;
                            return;
                        }

                        // if the UI is frozen (array is changing from remove) or there
                        // is nothing in the collection ignore
                        if (freeze || newCollection.length === 0) {
                            return;
                        }

                        // check for widgets that don't have 
                        newCollection.forEach(function(widget) {
                            if (!widget.elemId) {
                                widgetDef = hbWidget.get(widget.widget_key);
                                if (widgetDef) {
                                    widgetDef.elemId = randomString(),
                                        widget.elemId = widgetDef.elemId;
                                    widgetCfg = {
                                        previewMode: $scope.cfg.previewMode,
                                        config: widget.configuration
                                    };
                                    insertWidgetUI(widgetDef, widgetCfg, null, $scope.widgets.length - 1);
                                }
                            }
                        }, this);

                        render();
                    }

                    // render the directive
                    function render() {
                        var widgetDef,
                            widgetCfg;

                        // make sure the element is empty
                        if (element.childNodes.length > 0) {
                            element.removeChild(element.firstChild);
                        }

                        // create the bootstrap container-fluid div
                        var container = document.createElement('div');
                        container = element.appendChild(container);
                        container.className = "container-fluid";

                        // create the bootstrap row div
                        var row = document.createElement('div');
                        row = container.appendChild(row);
                        row.className = "masonry widget-row";
                        if ($scope.cfg.dragDrop) {
                            row.addEventListener('dragover', allowDrop);
                            row.addEventListener('drop', drop);
                        }

                        // add each widget
                        $scope.widgets.forEach(function(widget, index) {
                            // lookup the widget definition
                            widgetDef = hbWidget.get(widget.widget_key);
                            // set an identifier on the widget
                            widgetDef.elemId = randomString();
                            widget.elemId = widgetDef.elemId;
                            // set the config object for the widget
                            widgetCfg = {};
                            // set preview mode
                            widgetCfg.previewMode = $scope.cfg.previewMode;
                            // set the custom config
                            widgetCfg.config = widget.configuration;
                            widgetCfg.removeFromDashboard = removeFromDashboard;
                            widgetCfg.widget = widget;
                            // add the widget
                            insertWidgetUI(widgetDef, widgetCfg, row, index);
                        }, this);
                    }

                    function insertWidgetUI(widget, widgetCfg, widgetRowDiv, index) {
                        var widgetContainerDiv,
                            widgetDiv,
                            widgetScope;

                        // if the widget row wasn't provided, create it
                        if (!widgetRowDiv) {
                            widgetRowDiv = document.createElement('div');
                            widgetRowDiv.className = "widget-row masonry";
                        }

                        // create the container div
                        widgetContainerDiv = document.createElement('div');
                        widgetContainerDiv = widgetRowDiv.appendChild(widgetContainerDiv);
                        // add an identifier
                        widgetContainerDiv.setAttribute("id", widget.elemId);
                        //add the classes
                        widgetContainerDiv.className = "widget-container item";
                        switch (widget.width) {
                            case 'sm':
                                widgetContainerDiv.className += " sm";
                                break;
                            case 'md':
                                widgetContainerDiv.className += " md";
                                break;
                            case 'lg':
                                widgetContainerDiv.className += " lg";
                                break;
                            case 'xl':
                                widgetContainerDiv.className += " xl";
                                break;
                        }

                        if ($scope.cfg.dragDrop) {
                            widgetContainerDiv.setAttribute("draggable", true);
                            // listen for drag and drop
                            widgetContainerDiv.addEventListener("dragstart", drag);
                            widgetContainerDiv.addEventListener('dragover', allowDrop);
                            widgetContainerDiv.addEventListener('drop', drop);
                        }

                        // add the widget
                        widgetDiv = document.createElement('widget-renderer');
                        // add classes
                        widgetDiv.className = "widget";
                        // set the attributes
                        widgetDiv.setAttribute("widget", "widget");
                        widgetDiv.setAttribute("cfg", "cfg");
                        // add the event listeners
                        if ($scope.cfg.dragDrop) {
                            widgetDiv.addEventListener("dragover", dontAllowDrop);
                        }

                        // compile and add the widget
                        widgetScope = $scope.$new(true);
                        widgetScope.widget = widget;
                        widgetScope.cfg = widgetCfg;
                        $compile(widgetDiv)(widgetScope);
                        widgetContainerDiv.appendChild(widgetDiv);

                        // if in edit mode add the remove button
                        if ($scope.cfg.editMode) {
                            var removeBtn = document.createElement("button");
                            removeBtn.setAttribute("type", "button");
                            removeBtn.setAttribute("title", "Remove");
                            removeBtn.className = "btn btn-primary btn-xs remove-button";
                            removeBtn.addEventListener("click", removeWidget);
                            var removeSpan = document.createElement("span");
                            removeSpan.className = "glyphicon glyphicon-remove";
                            removeBtn.appendChild(removeSpan);
                            widgetDiv.appendChild(removeBtn);
                        }
                    }

                    function allowDrop(e) {
                        e.preventDefault();
                    }

                    function dontAllowDrop(e) {
                        e.preventDefault();
                        e.dataTransfer.dropEffect = "none";
                    }

                    function drag(e) {
                        e.dataTransfer.setData("text", e.target.id);
                    }

                    function drop(e) {
                        e.preventDefault();

                        var dropX = e.clientX,
                            dropY = e.clientY,
                            widget,
                            boundingRect,
                            moveId = e.dataTransfer.getData("text"),
                            moveMe = document.getElementById(moveId), // gets a widget container
                            indexBeingMoved,
                            indexBeingMovedTo,
                            moved = false;

                        // the drop event can occur twice, once in the container and once
                        // in the row. if moved to a container the element will be missing
                        // from the document on the second drop, the row check.
                        if (moveMe === null) {
                            return;
                        }

                        indexBeingMoved = indexOfNode(moveMe);

                        // check if the drop is a widget block.
                        if (containsClass("widget-container", e.currentTarget.classList)) {
                            // drop target if a widget block
                            widget = e.currentTarget.getElementsByClassName("widget")[0];
                            boundingRect = widget.getBoundingClientRect();
                            if (dropX < boundingRect.left || dropY < boundingRect.top) {
                                // get the index of the node to insert before
                                indexBeingMovedTo = indexOfNode(e.currentTarget);
                                moved = true;
                                // move the element
                                e.currentTarget.parentNode.insertBefore(moveMe, e.currentTarget.parentNode.childNodes[indexBeingMovedTo]);
                            }
                            else if (dropX > boundingRect.right || dropY > boundingRect.bottom) {
                                insertBeforeIndex = indexOfNode(e.currentTarget);
                                if (indexBeingMovedTo == e.currentTarget.parentNode.childNodes.length - 1) {
                                    e.currentTarget.parentNode.appendChild(moveMe);
                                    indexBeingMovedTo = e.currentTarget.parentNode.childNodes.length;
                                }
                                else {
                                    e.currentTarget.parentNode.insertBefore(moveMe, e.currentTarget.parentNode.childNodes[indexBeingMovedTo + 1]);
                                }
                                moved = true;
                            }
                        }
                        else {
                            // make sure there is not a widget block in the hit area. If there is the drop was already handled
                            var blocks = e.currentTarget.getElementsByClassName("widget-container");
                            for (k = 0, len = blocks.length; k < len; k++) {
                                // get the bounding rectangle for the current item
                                boundingRect = blocks[k].getBoundingClientRect();
                                if (dropX >= boundingRect.left && dropX <= boundingRect.right &&
                                    dropY >= boundingRect.top && dropY <= boundingRect.bottom) {
                                    return;
                                }
                            }

                            // drop was in an empty area of the main container						
                            for (k = 0, len = blocks.length; k < len; k++) {
                                boundingRect = blocks[k].getBoundingClientRect();
                                if (dropY < boundingRect.top) {
                                    // get the index of the node to insert before
                                    indexBeingMovedTo = indexOfNode(blocks[k]);
                                    move = true;
                                    // move the element
                                    e.currentTarget.insertBefore(moveMe, e.currentTarget.childNodes[indexBeingMovedTo]);
                                    break;
                                }
                            }
                        }
                        // if there was a move update the scope widgets to reflect the order of the UI
                        if (moved) {
                            // freeze prevent updates until this operation is done
                            freeze = true;

                            var widgetRow = document.getElementsByClassName("widget-row")[0],
                                elemId,
                                newIndex = 0,
                                oldIndex,
                                len;

                            widgetRow.childNodes.forEach(function(node) {
                                elemId = node.getAttribute("id");
                                for (oldIndex = 0, len < $scope.widgets.length; k < len; oldIndex++) {
                                    if ($scope.widgets[oldIndex].elemId == elemId) {
                                        break;
                                    }
                                }

                                // move the widget
                                $scope.widgets.move(oldIndex, newIndex);

                                newIndex++;
                            }, this);

                            // allow updates again
                            freeze = false;
                        }
                    }

                    function removeFromDashboard(widget) {
                        $scope.cfg.onRemoveWidget(widget.id);
                        var index = $scope.widgets.indexOf(widget);
                        $scope.widgets.splice(index, 1);
                    }

                    function removeWidget(e) {
                        var index,
                            node = e.target,
                            widget;

                        // determine the index of the widget being removed
                        while (node != null) {
                            if (node.className.indexOf("widget-container") > -1) {
                                break;
                            }
                            else {
                                node = node.parentNode;
                            }
                        }

                        // if there is a node remove it and the widget
                        if (node) {
                            index = indexOfNode(node);
                            widget = $scope.widgets[index];
                            freeze = true;
                            node.parentNode.removeChild(node);
                            $scope.widgets.splice(index, 1);
                            $scope.$apply(function() {
                                $scope.widgetRemoved({ widget: widget });
                            });
                            freeze = false;
                        }
                    }

                    function containsClass(name, classList) {
                        var k,
                            len;

                        for (k = 0, len = classList.length; k < len; k++) {
                            if (classList[k] == name) {
                                return true;
                            }
                        }

                        return false;
                    }

                    function indexOfNode(node) {
                        var k, len;
                        for (k = 0, len = node.parentNode.childNodes.length; k < len; k++) {
                            if (node.parentNode.childNodes[k] == node) {
                                return k;
                            }
                        }
                        return -1;
                    }

                    function randomString() {
                        var str = "",
                            options = "abcdefghijklmnopqrstuvwxyz0123456789",
                            len = 6;

                        for (var i = 0; i < len; i++) {
                            str += options.charAt(Math.floor(Math.random() * options.length));
                        }
                        return str;
                    }

                    render();
                }
            };

        }
    ]);
