(function() {
    "use strict";

    angular
        .module("hb.widgets.widgetForm", [])
        .directive("hbWidgetForm", function() {
            return {
                restrict: "E",
                templateUrl: "widgets/widgetForm/widgetForm.tpl.html",
                controller: "WidgetFormController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    cfg: "=",
                    onSaved: "&",
                    onCancel: "&"
                }
            };
        })
        // Model backing the area modal form
        .controller("WidgetFormController", [
            "$log",
            "hb.api",
            "Notify",
            "hbWidget",
            "Loading",
            "hb.resources",
            Controller
        ]);

    function Controller($log, api, notify, hbWidget, loading, resources) {
        var that = this;
        var Model = function() {
            this.$log = $log;
            this.api = api;
            this.notify = notify;
            this.hbWidget = hbWidget;
            this.resources = resources.widgetForm;
            this.loading = loading;
            this.widgetDefs = [];
            this.name = null;
            this.description = null;
            this.restrict = null;
            this.configuration = { };
            this.title = this.resources.newTitle;
            this.restrictLabel = null;
            this.widgetDef = null;
            this.configuration = {
                changed: angular.bind(this, this.configChanged)
            };
            this.error422 = false;
            
            this.populate();
        };
    
        angular.extend(Model.prototype, {
            getRestrictLabel: function() {
                if (that.cfg.asAdmin) {
                    return this.resources.adminRestrictLabel;
                } else if (that.cfg.asOrg) {
                    return this.resources.orgRestrictLabel;
                } else if (that.cfg.asPartner) {
                    return this.resources.partnerRestrictLabel;
                } else {
                    return "unknown use";
                }
            },
            
            populate: function() {
                var k, len;
                
                if (!that.cfg) {
                    return;
                }
                
                // set the restrict label
                this.restrictLabel = this.getRestrictLabel();
                
                // setup the save callback
                that.cfg.api = {
                    save: angular.bind(this, this.submit)
                };
                
                // load widget defs
                this.widgetDefs = this.hbWidget.getAll().filter(function(def) {
                    if (that.cfg.asAdmin) {
                        return true;
                    } else {
                        return def.config;
                    }
                }, this);
                
                // load the widget
                if (that.cfg.item) {
                    this.widget = that.cfg.item;
                    this.category = this.widget.category;
                    this.title = this.widget.name;
                    this.name = this.widget.name;
                    this.description = this.widget.description;
                    this.restrict = this.widget.organization_restriction_id ||
                                    this.widget.partner_restriction_id ||
                                    this.widget.admin_restriction ? true : false;
                    for (k = 0, len = this.widgetDefs.length; k < len; k++) {
                        if (this.widgetDefs[k].key == this.widget.key) {
                            this.widgetDef = this.widgetDefs[k];
                            break;
                        }
                    }
                }
            },
            
            widgetDefChanged: function(newWidgetDef) {
                this.category = newWidgetDef ? newWidgetDef.category : null;  
            },
            
            configChanged: function(config) {
                this.configuration = config;  
            },

            submit: function() {
                that.form.$submitted = true;
                
                if (that.form.$invalid) {
                    return;
                }
                
                var data = {
                    name: this.name,
                    description: this.description,
                    key: this.widgetDef.key,
                    category: this.category,
                    configuration: this.widgetDef.config ? JSON.stringify(this.configuration) : undefined
                },
                promise;
                
                if (that.cfg.asAdmin) {
                    data.admin_restriction = this.restrict;
                } else if (that.cfg.asOrg) {
                    data.organization_restriction_id = this.restrict ? that.cfg.orgId : undefined;
                } else if (that.cfg.asPartner) {
                    data.partner_restriction_id = this.restrict ? that.cfg.partnerId : undefined;
                }
                
                this.error422 = false;
                this.loading.show(this.resources.saving);
                
                if (this.widget) {
                    promise = api.widget.update(this.widget.id, data);
                } else {
                    promise = api.widget.create(data);
                }
                
                promise.then(angular.bind(this, this.onWidgetSaved),
                            angular.bind(this, this.onWidgetSaveError))
                        /*jshint -W024*/
                        .finally(angular.bind(this, this.onDoneSaving));
            },
    
            onWidgetSaved: function(response) {
                this.widget = response.data;
                that.cfg.item = this.widget;
                that.onSaved({
                    item: this.widget
                });
            },
    
            onWidgetSaveError: function(response) {
                if (response.status == 422) {
                    this.error422 = true;
                } else {
                    this.notify.error(this.resources.saveError);    
                }
            },
            
            onDoneSaving: function() {
                this.loading.close();
            }
        });
        
        this.model = new Model();
    }
})();