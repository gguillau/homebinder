describe("hbWidgetForm", function() {
    var ctrl,
        controller,
        $q,
        $log,
        api,
        notify,
        resources,
        loading,
        hbWidget;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
        hbWidget = $injector.get("hbWidget");
    }));

    function createController() {
        ctrl = controller("WidgetFormController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "hbWidget": hbWidget,
            "Loading": loading,
            "hb.resources": resources
        });
    }

    describe("getRestrictLabel", function() {
        it("gets for admin", function() {
            createController();
            ctrl.cfg = { asAdmin: true };
            expect(ctrl.model.getRestrictLabel()).toEqual(resources.widgetForm.adminRestrictLabel);
        });

        it("gets for partner", function() {
            createController();
            ctrl.cfg = { asPartner: true };
            expect(ctrl.model.getRestrictLabel()).toEqual(resources.widgetForm.partnerRestrictLabel);
        });

        it("gets for organization", function() {
            createController();
            ctrl.cfg = { asOrg: true };
            expect(ctrl.model.getRestrictLabel()).toEqual(resources.widgetForm.orgRestrictLabel);
        });
    });

    describe("populate", function() {
        it("returns when no cfg is provided", function() {
            createController();
            ctrl.cfg = null;
            ctrl.model.populate();

            // no exceptions?
        });

        it("populates no restriction", function() {
            createController();
            ctrl.cfg = {
                asAdmin: true,
                item: {
                    name: "widget",
                    key: "doubleBusinessCard",
                    description: "widget description",
                    category: "cat",
                    organization_restriction_id: null,
                    partner_restriction_id: null,
                    admin_restriction: false
                }
            };
            ctrl.model.populate();

            expect(ctrl.model.widget).not.toBeNull();
            expect(ctrl.model.name).toEqual("widget");
            expect(ctrl.model.description).toEqual("widget description");
            expect(ctrl.model.category).toEqual("cat");
            expect(ctrl.model.restrict).toEqual(false);
            expect(ctrl.model.widgetDef).not.toBeNull();
            expect(ctrl.cfg.api.save).not.toBeNull();
        });

        it("populates partner restriction", function() {
            createController();
            ctrl.cfg = {
                asAdmin: true,
                item: {
                    name: "widget",
                    key: "doubleBusinessCard",
                    description: "widget description",
                    category: "cat",
                    organization_restriction_id: null,
                    partner_restriction_id: 1,
                    admin_restriction: false
                }
            };
            ctrl.model.populate();

            expect(ctrl.model.widget).not.toBeNull();
            expect(ctrl.model.name).toEqual("widget");
            expect(ctrl.model.description).toEqual("widget description");
            expect(ctrl.model.category).toEqual("cat");
            expect(ctrl.model.restrict).toEqual(true);
            expect(ctrl.model.widgetDef).not.toBeNull();
            expect(ctrl.cfg.api.save).not.toBeNull();
        });

        it("populates org restriction", function() {
            createController();
            ctrl.cfg = {
                asAdmin: true,
                item: {
                    name: "widget",
                    key: "doubleBusinessCard",
                    description: "widget description",
                    category: "cat",
                    organization_restriction_id: 1,
                    partner_restriction_id: null,
                    admin_restriction: false
                }
            };
            ctrl.model.populate();

            expect(ctrl.model.widget).not.toBeNull();
            expect(ctrl.model.name).toEqual("widget");
            expect(ctrl.model.description).toEqual("widget description");
            expect(ctrl.model.category).toEqual("cat");
            expect(ctrl.model.restrict).toEqual(true);
            expect(ctrl.model.widgetDef).not.toBeNull();
            expect(ctrl.cfg.api.save).not.toBeNull();
        });

        it("populates partner restriction", function() {
            createController();
            ctrl.cfg = {
                asAdmin: true,
                item: {
                    name: "widget",
                    key: "doubleBusinessCard",
                    description: "widget description",
                    category: "cat",
                    organization_restriction_id: null,
                    partner_restriction_id: null,
                    admin_restriction: true
                }
            };
            ctrl.model.populate();

            expect(ctrl.model.widget).not.toBeNull();
            expect(ctrl.model.name).toEqual("widget");
            expect(ctrl.model.description).toEqual("widget description");
            expect(ctrl.model.category).toEqual("cat");
            expect(ctrl.model.restrict).toEqual(true);
            expect(ctrl.model.widgetDef).not.toBeNull();
            expect(ctrl.cfg.api.save).not.toBeNull();
        });

        it("does not populate when no widget is provided", function() {
            createController();
            ctrl.cfg = {};
            ctrl.model.populate();

            expect(ctrl.model.widget).toBeUndefined();
            expect(ctrl.cfg.api.save).not.toBeNull();
        });
    });

    describe("submit", function() {
        it("does not submit invalid data", function() {
            spyOn(api.widget, "create").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = {};
            ctrl.form = {
                $invalid: true
            };
            ctrl.model.submit();
            expect(ctrl.form.$submitted).toEqual(true);
            expect(api.widget.create).not.toHaveBeenCalled();
        });

        it("creates a new widget", function() {
            spyOn(api.widget, "create").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = {};
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.name = "widget";
            ctrl.model.description = "description";
            ctrl.model.category = "cat";
            ctrl.model.widgetDef = {
                key: "key",
                category: "cat"
            };
            ctrl.model.submit();
            expect(api.widget.create).toHaveBeenCalledWith(jasmine.objectContaining({
                name: "widget",
                key: "key",
                description: "description",
                category: "cat",
                configuration: undefined
            }));
        });

        it("creates a new widget with admin restrict", function() {
            spyOn(api.widget, "create").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = { asAdmin: true };
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.name = "widget";
            ctrl.model.description = "description";
            ctrl.model.category = "cat";
            ctrl.model.widgetDef = {
                key: "key",
                category: "cat"
            };
            ctrl.model.restrict = true;
            ctrl.model.submit();
            expect(api.widget.create).toHaveBeenCalledWith(jasmine.objectContaining({
                name: "widget",
                key: "key",
                description: "description",
                category: "cat",
                admin_restriction: true
            }));
        });

        it("creates a new widget with partner restrict", function() {
            spyOn(api.widget, "create").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = { asPartner: true, partnerId: 1 };
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.name = "widget";
            ctrl.model.description = "description";
            ctrl.model.category = "cat";
            ctrl.model.widgetDef = {
                key: "key",
                category: "cat"
            };
            ctrl.model.restrict = true;
            ctrl.model.submit();
            expect(api.widget.create).toHaveBeenCalledWith(jasmine.objectContaining({
                name: "widget",
                key: "key",
                description: "description",
                category: "cat",
                partner_restriction_id: 1
            }));
        });

        it("creates a new widget with org restrict", function() {
            spyOn(api.widget, "create").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = { asOrg: true, orgId: 1 };
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.name = "widget";
            ctrl.model.description = "description";
            ctrl.model.category = "cat";
            ctrl.model.widgetDef = {
                key: "key",
                category: "cat"
            };
            ctrl.model.restrict = true;
            ctrl.model.submit();
            expect(api.widget.create).toHaveBeenCalledWith(jasmine.objectContaining({
                name: "widget",
                key: "key",
                description: "description",
                category: "cat",
                organization_restriction_id: 1
            }));
        });

        it("creates a new widget with configuration", function() {
            spyOn(api.widget, "create").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = { asAdmin: true };
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.name = "widget";
            ctrl.model.description = "description";
            ctrl.model.category = "cat";
            ctrl.model.widgetDef = {
                key: "key",
                category: "cat",
                config: {
                    directive: "directive"
                }
            };
            ctrl.model.restrict = false;
            ctrl.model.configuration = "config";
            ctrl.model.submit();
            expect(api.widget.create).toHaveBeenCalledWith(jasmine.objectContaining({
                name: "widget",
                key: "key",
                description: "description",
                category: "cat",
                configuration: JSON.stringify("config")
            }));
        });

        it("updates an existing widget", function() {
            spyOn(api.widget, "update").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = {};
            ctrl.model.widget = {
                id: 1
            };
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.name = "widget";
            ctrl.model.description = "description";
            ctrl.model.category = "cat";
            ctrl.model.widgetDef = {
                key: "key",
                category: "cat"
            };
            ctrl.model.submit();
            expect(api.widget.update).toHaveBeenCalledWith(1, jasmine.objectContaining({
                name: "widget",
                key: "key",
                description: "description",
                category: "cat"
            }));
        });
    });

    describe("onWidgetSaved", function() {
        it("saves the response and calls onSaved", function() {
            var widget = {
                id: 1,
                name: "widget",
                key: "widget",
                description: "description",
                category: "category"
            };
            createController();
            ctrl.cfg = {};
            ctrl.onSaved = function() {};
            spyOn(ctrl, "onSaved");
            ctrl.model.onWidgetSaved({ data: widget });
            expect(ctrl.model.widget).toEqual(widget);
            expect(ctrl.cfg.item).toEqual(widget);
            expect(ctrl.onSaved).toHaveBeenCalledWith(jasmine.objectContaining({ item: widget }));
        });
    });

    describe("onWidgetSaveError", function() {
        it("handles a 422 error", function() {
            createController();
            ctrl.cfg = {};
            ctrl.model.onWidgetSaveError({ status: 422, data: "error" });
            expect(ctrl.model.error422).toEqual(true);
        });

        it("handles a general error", function() {
            spyOn(notify, "error");
            createController();
            ctrl.cfg = {};
            ctrl.model.onWidgetSaveError({ status: 500, data: "error" });
            expect(ctrl.model.error422).toEqual(false);
            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("widgetDefChanged", function() {
        it("changes the widget category", function() {
            createController();
            ctrl.model.category = null;
            ctrl.model.widgetDefChanged({ category: "binder" });

            expect(ctrl.model.category).toEqual("binder");
        });
    });

    describe("configChanged", function() {
        it("changes the widget config", function() {
            createController();
            var config = { id: 1 };
            ctrl.model.configChanged(config);

            expect(ctrl.model.configuration).toEqual(config);
        });
    });

    describe("onDoneSaving", function() {
        it("calls loading close", function() {
            spyOn(loading, "close");

            createController();
            ctrl.model.onDoneSaving();

            expect(loading.close).toHaveBeenCalled();
        });
    });
});

describe('hbWidgetForm', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = {};
        $scope.onSaved = function() {};
        $scope.onCancel = function() {};

        element = $compile('<hb-widget-form cfg="cfg" on-saved="onSaved" on-cancel="onCancel"></hb-widget-form>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the form elements', function() {
            expect(element.html()).toContain("Widget type");
        });
    });
});