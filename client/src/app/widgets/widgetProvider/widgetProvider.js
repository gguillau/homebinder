angular
    .module("hb.widgets.widgetProvider", [])
    .provider("hbWidget", function(){
        var widgets = {},
            service = {
                get: function(key) {
                    if (widgets.hasOwnProperty(key)) {
                        return widgets[key];
                    }
                    
                    throw "Widget with key " + key + " not found.";
                },
                getAll: function() {
                    var list = [],
                        widget;
                        
                    for(widget in widgets) {
                        list.push(widgets[widget]);
                    }
                    return list;
                }
            };
        
        this.$get = function() {
            return service;
        };
        
        this.register = function(key, config) {
            if (!widgets.hasOwnProperty(key)) {
                widgets[key] = config;
            } else {
                throw "Widget with key " + key + " already registered.";
            }
        };
        
    });