describe("WidgetProvider", function() {
    var widgetProvider,
        widget;
    
    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(function () {
        module(['hbWidgetProvider', function (hbWidgetProvider) {
            widgetProvider = hbWidgetProvider;
        }]);
    });
    beforeEach(function () {
        inject(['hbWidget', function (hbWidget) {
            widget = hbWidget;
        }]);
      });
    
    describe("register", function() {
        it ("adds the widget", function() {
            widgetProvider.register("widget", { x: 1 });
            expect(widget.get("widget")).not.toBeNull();
        });
        
        it ("throws when the widget is already registered", function() {
            try {
                widgetProvider.register("widget", { x: 1 });
                widgetProvider.register("widget", { x: 1 });
            } catch(e) {
                expect(true).toEqual(true);
            }
        });
    });
   
    describe("get", function() {
        it ("throws when the widget is not found", function() {
            try{
                widget.get("unknown");
            } catch(e) {
                expect(true).toEqual(true);
            }
        });
    });
});