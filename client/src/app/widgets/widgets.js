angular
    .module("hb.widgets", [
        'hb.widgets.library',
        'hb.widgets.widgetForm',
        'hb.widgets.widgetHost',
        'hb.widgets.widgetIndex',
        'hb.widgets.widgetModal',
        'hb.widgets.widgetPicker',
        'hb.widgets.widgetProvider',
        'hb.widgets.widgetRenderer',
        'hb.widgets.widgetState'
    ]);