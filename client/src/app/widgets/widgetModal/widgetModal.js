 angular
    .module("hb.widgets.widgetModal", [
    ])
    .factory("WidgetModal", [
        "hb.framework.toggleModalBase",
        function(ModalBase) {
            var instance = null;
            var Modal = function() {
                ModalBase.call(this);

                this.templateUrl = "widgets/widgetModal/widgetModal.tpl.html";
                this.controller = "WidgetsModalController as ctrl";
                this.windowClass = "widget-modal";
                this.objectProperty = "widget";
                this.objectIdProperty = "widgetId";
                this.nameProperty = "name";
            };
            
            Modal.prototype = Object.create(ModalBase.prototype);
            
            if (!instance) {
                instance = new Modal();
            }
            
            return instance;
        }
    ])
    .controller("WidgetsModalController", [
        "hb.framework.toggleModalControllerBase",
        "$modalInstance",
        "args",
        "hb.api",
        "hbWidget",
        "hb.resources",
        "Notify",
        "$log",
        function(ToggleModalControllerBase, $modalInstance, args, api, hbWidget, resources, notify, $log) {
            var Model = function() {
                this.args = args;
                this.objectProperty = "widget";
                this.objectIdProperty = "widgetId";
                this.nameProperty = "name";
                this.populateCall = api.widget.get;
                this.$modalInstance = $modalInstance;
                this.resources = resources.widgetModal;
                
                ToggleModalControllerBase.call(this);
                
                this.init();
            };
            
            Model.prototype = Object.create(ToggleModalControllerBase.prototype);
            
            this.model = new Model();
        }
    ]);