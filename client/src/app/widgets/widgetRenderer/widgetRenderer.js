angular
    .module('hb.widgets.widgetRenderer', [])
    .directive('widgetRenderer', function($compile) {
        return {
            restrict: 'E',
            scope: {
                widget: '=',
                cfg: '='
            },
            link: function(scope, element) {
                var html = '<' + scope.widget.directive + ' cfg="cfg"></' + scope.widget.directive + '>';
                element.append($compile(html)(scope));
            }
        };
});