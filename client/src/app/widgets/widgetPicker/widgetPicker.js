angular
    .module("hb.widgets.widgetPicker", [])
    .factory('HBWidgetPicker',[
        '$modal',
        function($modal) {
            return {
                show: function(args) {
                    var modalInstance = $modal.open({
                        templateUrl: "widgets/widgetPicker/widgetPicker.tpl.html",
                        controller: "WidgetPickerController as ctrl",
                        backdrop: "static",
                        windowClass: "widgetpicker-modal",
                        resolve: {
                            args: function() {
                                return args;
                            }
                        }
                    });

                    return modalInstance;
                }
            };
        }
    ])
    .controller('WidgetPickerController',[
        '$log',
        'hb.api',
        'hb.resources',
        'Notify',
        'args',
        '$modalInstance',
        function($log, api, resources, notify, args, $modalInstance) {
            var Model = function() {
                this.resources = resources.widgetPicker;
                this.widgets = [];
                this.filteredWidgets = [];
                this.categories = [this.resources.categoryAll];
                this.init();
            };
            
            angular.extend(Model.prototype, {
                init: function() {
                    api.widget.all().then(
                        angular.bind(this, this.loadWidgets),
                        angular.bind(this, this.loadWidgetError)
                    );
                },
                loadWidgets: function(response) {
                    var category,
                        parts;
                    
                    this.widgets = response.data.items;
                    this.categories = [];
                    this.widgets.forEach(function(widget) {
                        parts = widget.category.split('.');
                        parts.forEach(function(part){
                            category = category ? category + "." + part : part;
                            
                            if (this.categories.indexOf(category) == -1) {
                                this.categories.push(category);
                            }
                        }, this);
                        category = null;
                    }, this);
                    this.categories.sort();
                    this.categories.unshift(this.resources.categoryAll);
                    this.filterWidgets(this.resources.categoryAll);
                },
                loadWidgetError: function(response) {
                     $log.error(response.data);
                     notify.error(this.resources.errorLoading);
                },
                filterWidgets: function(category) {
                    this.category = category;
                    this.filteredWidgets = this.widgets.filter(function(widget) {
                        if (this.category == this.resources.categoryAll) {
                            return true;
                        } else {
                            return widget.category.indexOf(this.category) === 0;
                        }
                    }, this);
                },
                addWidget: function(widget) {
                    if (args.addWidget) {
                        args.addWidget(widget);
                    }
                },
                close: function() {
                    $modalInstance.dismiss();
                }
            });
            
            this.model = new Model();
        }
    ]);