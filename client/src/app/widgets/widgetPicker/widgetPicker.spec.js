describe("WidgetPickerController", function() {
    var ctrl,
        controller,
        $q,
        $log,
        api,
        notify,
        resources,
        loading,
        $modal,
        args,
        HBWidgetPicker,
        modal;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
        HBWidgetPicker = $injector.get("HBWidgetPicker");
        modal = $injector.get("$modal");

        $modal = {
            show: function() {},
            dismiss: function(msg) {},
            close: function(arg) {}
        };

        args = {};
    }));

    function createController() {
        ctrl = controller("WidgetPickerController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "hb.resources": resources,
            "Loading": loading,
            "args": args,
            "$modalInstance": $modal
        });
    }

    describe("init", function() {
        it("gets the widgets", function() {
            spyOn(api.widget, "all").and.returnValue($q.when({ data: [] }));

            createController();
            ctrl.model.init();

            expect(api.widget.all).toHaveBeenCalled();
        });
    });

    describe("loadWidgets", function() {
        it("sets the widgets", function() {
            var items = [
                { category: "binder.projects" }
            ];
            createController();
            ctrl.model.loadWidgets({ data: { items: items } });

            expect(ctrl.model.widgets.length).toEqual(1);
        });
    });

    describe("loadWidgetError", function() {
        it("calls notify", function() {
            spyOn(notify, "error");

            createController();
            ctrl.model.loadWidgetError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("filterWidgets", function() {
        it("sets category and filters the widgets", function() {
            createController();
            ctrl.model.widgets = [{ id: 1, category: "binders.projects" }];
            ctrl.model.filterWidgets("binders.projects");

            expect(ctrl.model.category).toEqual("binders.projects");
        });
    });

    describe("addWidget", function() {
        it("calls args addWidget", function() {
            createController();
            args.addWidget = function() {};
            spyOn(args, "addWidget");
            ctrl.model.addWidget({ id: 1 });

            expect(args.addWidget).toHaveBeenCalled();
        });
    });

    describe("close", function() {
        it("closes the modal", function() {
            spyOn($modal, "dismiss");

            createController();
            ctrl.model.close();

            expect($modal.dismiss).toHaveBeenCalled();
        });
    });

    describe('HBWidgetPicker', function() {
        it('should call show and then $modal open', function() {
            spyOn(modal, "open");

            HBWidgetPicker.show({});
            
            expect(modal.open).toHaveBeenCalled();
        });
    });
});