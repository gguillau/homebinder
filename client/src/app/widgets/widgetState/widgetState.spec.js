describe("hb.widgets.widgetState", function() {
    'use strict';

    var widgetState;

    beforeEach(function() {
        module("hb.widgets.widgetState");

        inject(function($injector) {
            widgetState = $injector.get("widgetState");
        });
    });

    it("returns the correct state", function() {
        var model = new widgetState();
        expect(model.state).toEqual(1);

        model.busy();

        expect(model.state).toEqual(2);
    });

});