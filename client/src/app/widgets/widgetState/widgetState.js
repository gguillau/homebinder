angular
    .module('hb.widgets.widgetState', [])
    .factory('widgetState', [
        function() {
            var STATES = {
                IDLE: 1,
                BUSY: 2,
                ERROR: 3
            };
            var WidgetState = function() {
                this.state = STATES.IDLE;
            };

            angular.extend(WidgetState.prototype, {
                busy: function() {
                    this.state = STATES.BUSY;
                },
                idle: function() {
                    this.state = STATES.IDLE;
                },
                error: function() {
                    this.state = STATES.ERROR;
                },
                isBusy: function() {
                    return this.state == STATES.BUSY;
                },
                isIdle: function() {
                    return this.state == STATES.IDLE;
                },
                isError: function() {
                    return this.state == STATES.ERROR;
                }
            });
            return WidgetState;
        }
    ]);