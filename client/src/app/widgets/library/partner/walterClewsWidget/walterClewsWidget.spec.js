describe("WalterClewsServicesFormWidgetController", function() {
    var $controllerService,
        controller,
        $rootScope,
        $q,
        $log,
        $stateParams,
        api,
        widgetState,
        resources,
        notify;

    // load the homebinder module
    beforeEach(module("homebinder"));

    // create the controller
    beforeEach(inject(function($controller, $injector) {
        $controllerService = $controller;
        $rootScope = $injector.get("$rootScope");
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        $stateParams = $injector.get("$stateParams");
        $stateParams.binderId = 1;
        api = $injector.get("hb.api");
        widgetState = $injector.get("widgetState");
        resources = $injector.get("hb.resources");
        notify = $injector.get("Notify");
    }));

    function createController() {
        controller = $controllerService("WalterClewsServicesFormWidgetController", {
            "log": $log,
            "$stateParams": $stateParams,
            "hb.api": api,
            "widgetState": widgetState,
            "hb.resources": resources,
            "Notify": notify
        });
    }

    describe("requestServices", function() {
        it("calls hb.api.binder.requestServices", function() {
            spyOn(api.binder, "requestServices").and.returnValue($q.when({ data: "success" }));

            createController();
            controller.model.cfg = {
                removeFromDashboard: function() {}
            };
            controller.model.requestServices();

            $rootScope.$apply();

            expect(api.binder.requestServices).toHaveBeenCalledWith(1, ['Pest Control'], "clews-office@amerispec.net");
        });

        it("handles an error", function() {
            spyOn(api.binder, "requestServices").and.returnValue($q.reject({ data: "error" }));
            spyOn($log, "error");

            createController();
            controller.model.cfg = {
                removeFromDashboard: function() {}
            };
            controller.model.requestServices();

            $rootScope.$apply();

            expect($log.error).toHaveBeenCalledWith("error");
        });
    });

    describe("declineServices", function() {
        it("calls hb.api.binder.declineServices", function() {
            spyOn(api.binder, "declineServices").and.returnValue($q.when({ data: "success" }));

            createController();
            controller.model.cfg = {
                removeFromDashboard: function() {}
            };
            controller.model.declineServices();

            $rootScope.$apply();

            expect(api.binder.declineServices).toHaveBeenCalledWith(1, "clews-office@amerispec.net");
        });

        it("handles an error", function() {
            spyOn(api.binder, "declineServices").and.returnValue($q.reject({ data: "error" }));
            spyOn($log, "error");

            createController();
            controller.model.cfg = {
                removeFromDashboard: function() {}
            };
            controller.model.declineServices();

            $rootScope.$apply();

            expect($log.error).toHaveBeenCalledWith("error");
        });
    });
});

describe('hbWalterClewsServicesFormWidget', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = {};

        element = $compile('<hb-walter-clews-services-form-widget cfg="cfg"></hb-walter-clews-services-form-widget>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the widget elements', function() {
            expect(element.html()).toContain("How Can We Help?");
        });
    });
});