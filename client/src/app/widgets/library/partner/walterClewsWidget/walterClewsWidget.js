angular
    .module('hb.widgets.library.partner.walterClewsWidget', [
        'hb.widgets.widgetProvider'
    ])
    .config(['hbWidgetProvider', function(widgetProvider) {
        widgetProvider.register('walterClewsWidget', {
            name: 'Walter Clews Third Party Services Form',
            key: 'walterClewsWidget',
            description: 'Allow homeowners to request services from Walter clews',
            category: 'Partner.walterClewsWidget',
            template: '<hb-walter-clews-services-form-widget></hb-walter-clews-services-form-widget>',
            directive: 'hb-walter-clews-services-form-widget',
            width: 'md',
            height: 'sm'
        });
    }])
    .directive('hbWalterClewsServicesFormWidget', function() {
        return {
            restrict: "E",
            templateUrl: "widgets/library/partner/walterClewsWidget/walterClewsWidget.tpl.html",
            controller: "WalterClewsServicesFormWidgetController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {
                cfg: '='
            }
        };
    })
    .controller('WalterClewsServicesFormWidgetController', [
        '$log',
        '$stateParams',
        'hb.api',
        'widgetState',
        'hb.resources',
        'Notify',
        '$filter',
        'HBWidgetBase',
        function($log, $stateParams, api, widgetState, resources, Notify, $filter, HBWidgetBase) {
            var Controller = function(cfg) {
                HBWidgetBase.call(this);
                this.cfg = cfg;
                this.binderId = $stateParams.binderId;
                this.state = new widgetState();
                this.resources = resources.thirdPartyServicesWidget;
                this.services = [{
                    name: "warranties",
                    label: this.resources.warranties,
                    selected: false
                }, {
                    name: "pest",
                    label: this.resources.pests,
                    selected: true
                }, {
                    name: "alarm",
                    label: this.resources.alarm,
                    selected: false
                }];
                this.step = 1;
                this.processing = false;
            };

            Controller.prototype = Object.create(HBWidgetBase.prototype);

            angular.extend(Controller.prototype, {
                requestServices: function() {
                    this.processing = true;
                    var services = $filter('filter')(this.services, true);

                    services = services.map(angular.bind(this, function(service) {
                        return service.label;
                    }));

                    api.binder.requestServices(this.binderId, services, "clews-office@amerispec.net").then(
                        angular.bind(this, this.requestServicesSuccess),
                        angular.bind(this, this.servicesError));
                },

                requestServicesSuccess: function(response) {
                    this.step = 2;
                    this.feedbackText = this.resources.submitText;
                    Notify.info(this.feedbackText);
                },

                servicesError: function(error) {
                    $log.error(error.data);
                    this.step = 2;
                    this.feedbackText = this.resources.error;
                },

                declineServices: function() {
                    this.processing = true;
                    api.binder.declineServices(this.binderId, "clews-office@amerispec.net").then(
                        angular.bind(this, this.declineServicesSuccess),
                        angular.bind(this, this.servicesError));
                },

                declineServicesSuccess: function(response) {
                    this.step = 2;
                    this.feedbackText = this.resources.decline;
                    Notify.info(this.feedbackText);
                }
            });

            this.model = new Controller(this.cfg);
        }
    ]);