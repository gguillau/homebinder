describe("CustomWidgetController", function() {
    var $controllerService,
        ctrl,
        $q,
        $log,
        api,
        widgetState,
        resources,
        context,
        $filter,
        event,
        $window,
        session;

    // load the homebinder module
    beforeEach(module("homebinder"));

    // create the controller
    beforeEach(inject(function($controller, $injector) {
        $controllerService = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        widgetState = $injector.get("widgetState");
        resources = $injector.get("hb.resources");
        context = $injector.get("Context");
        $filter = $injector.get("$filter");
        event = $injector.get("Event");
        $window = $injector.get("$window");
        session = $injector.get("Session");
    }));

    function createController() {
        ctrl = $controllerService("CustomWidgetController", {
            "log": $log,
            "hb.api": api,
            "widgetState": widgetState,
            "hb.resources": resources,
            "Context": context,
            "$filter": $filter,
            "Event": event,
            "$window": $window,
            "Session": session
        });
        
        var binder = {
            id: 1,
            branding_user: {}
        };
        spyOn(ctrl.model.context, "getBinder").and.returnValue($q.when(binder));
        
        var partnerWidget = {
            id: 1,
            partner_id: 1,
            widget_id: 1,
            configuration: {}
        };
        spyOn(api.partnerWidget, "all").and.returnValue($q.when([partnerWidget]));
    }
    
    describe("refresh", function() {
        it("gets the binder when cfg does not exist", function() {
            createController();
            spyOn(ctrl.model.state, "busy");
            
            ctrl.model.cfg = null;
            ctrl.model.refresh();

            expect(ctrl.model.state.busy).toHaveBeenCalled();
            expect(ctrl.model.context.getBinder).toHaveBeenCalled();
        });
        
        it("gets the binder when not in preview mode", function() {
            createController();
            spyOn(ctrl.model.state, "busy");
            
            ctrl.model.cfg = {previewMode: false};
            ctrl.model.refresh();

            expect(ctrl.model.state.busy).toHaveBeenCalled();
            expect(ctrl.model.context.getBinder).toHaveBeenCalled();
        });
        
        it("sets the configuration to preview data when in preview mode", function() {
            createController();
            spyOn(ctrl.model.state, "busy");
            
            ctrl.model.cfg = {previewMode: true};
            ctrl.model.refresh();

            expect(ctrl.model.state.busy).not.toHaveBeenCalled();
            expect(ctrl.model.context.getBinder).not.toHaveBeenCalled();
            expect(ctrl.model.configuration).toEqual(resources.customWidget.preview);
        });
    });
    
    describe("getPartnerId", function() {
        it("throws an error when a partner with binder branding does not exist", function() {
            createController();
            spyOn(ctrl.model, "onError");
            
            var binder = {
                branding_users: [{scope: "transfer_email", partner_id: 1}]
            };
            ctrl.model.getPartnerId(binder);

            expect(ctrl.model.onError).toHaveBeenCalledWith({ data: "Couldn't find branding_user with scope: 'binder'" });
        });
        
        it("sets the partner when a partner with binder branding exists", function() {
            createController();
            spyOn(ctrl.model, "getWidgetConfiguration");
            
            var binder = {
                branding_users: [{scope: "binder", partner_id: 1}, {scope: "transfer_email", partner_id: 2}]
            };
            ctrl.model.getPartnerId(binder);

            expect(ctrl.model.partner_id).toEqual(1);
            expect(ctrl.model.getWidgetConfiguration).toHaveBeenCalled();
        });
    });
    
    describe("getWidgetConfiguration", function() {
        it("calls api.partnerWidget.all", function() {
            createController();

            ctrl.model.getWidgetConfiguration();

            expect(api.partnerWidget.all).toHaveBeenCalled();
        });
    });
    
    describe("getWidgetConfigurationSuccess", function() {
        it("throws an error when response length is 0", function() {
            createController();
            spyOn(ctrl.model, "onError");

            var response = {data: {items: []}};
            ctrl.model.partner_id = 1;
            ctrl.model.getWidgetConfigurationSuccess(response);

            expect(ctrl.model.onError).toHaveBeenCalledWith({ data: "Couldn't find partner_widget for partner_id 1" });
        });
        
        it("sets the configuration when response length greater than 0", function() {
            createController();
            spyOn(ctrl.model.state, "idle");

            var configuration = JSON.stringify({
                title: "title", 
                body: "body", 
                buttonLabel: "button", 
                url: "url"
            });
            var response = {data: {items: [{configuration: configuration}]}};
            ctrl.model.getWidgetConfigurationSuccess(response);

            expect(ctrl.model.configuration).toEqual(JSON.parse(configuration));
            expect(ctrl.model.state.idle).toHaveBeenCalled();
        });
    });
    
    describe("onLinkClick", function() {
        it("creates a kibana event and opens the correct url", function() {
            createController();
            spyOn(event, "create");
            spyOn($window, "open");

            ctrl.model.configuration = {url: "www.homebinder.com"};
            ctrl.model.currentUser = {
                id: 1,
                role: "homeowner",
                user_created_at: "time"
            };
            ctrl.model.partner_id = 1;
            ctrl.model.onLinkClick();

            expect(event.create).toHaveBeenCalledWith({ event_name: "partner_custom_widget_clicked", event_type: "engagement", user_id: 1, user_role: "homeowner", user_created_at: "time", partner_id: 1 });
            expect($window.open).toHaveBeenCalledWith("http://www.homebinder.com", "_blank");
        });
    });
    
    describe("onError", function() {
        it("creates a kibana event and opens the correct url", function() {
            createController();
            spyOn($log, "error");
            spyOn(ctrl.model.state, "error");

            var response = {data: "error"};
            ctrl.model.onError(response);

            expect($log.error).toHaveBeenCalledWith({data: "error"});
            expect(ctrl.model.state.error).toHaveBeenCalled();
        });
    });
    
});

describe('hbCustomWidget', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = {previewMode: true};

        element = $compile('<hb-custom-widget cfg="cfg"></hb-custom-widget>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the widget elements', function() {
            expect(element.html()).toContain("Partner Widget");
        });
    });
});