angular
    .module('hb.widgets.library.partner.custom', [
        'hb.widgets.widgetProvider'
    ])
    .config(['hbWidgetProvider', function(widgetProvider) {
        widgetProvider.register('customWidget', {
            name: 'Custom',
            key: 'customWidget',
            description: 'A custom widget from partners for their clients\' dashboards',
            category: 'Partner.custom',
            template: '<hb-custom-widget></hb-custom-widget>',
            directive: 'hb-custom-widget',
            width: 'md',
            height: 'sm'
        });
    }])
    .directive('hbCustomWidget', function() {
        return {
            restrict: "E",
            templateUrl: "widgets/library/partner/custom/custom.tpl.html",
            controller: "CustomWidgetController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {
                cfg: '='
            }
        };
    })
    .controller('CustomWidgetController', [
        '$log',
        'hb.api',
        'widgetState',
        'hb.resources',
        'Context',
        '$filter',
        'Event',
        '$window',
        'Session',
        function($log, api, widgetState, resources, context, $filter, Event, $window, Session) {
            var Model = function(cfg) {
                this.cfg = cfg;
                this.$log = $log;
                this.api = api;
                this.state = new widgetState();
                this.resources = resources.customWidget;
                this.context = context;
                this.$filter = $filter;
                this.event = Event;
                this.$window = $window;
                this.currentUser = Session.getUser();

                this.refresh();
            };

            angular.extend(Model.prototype, {
                refresh: function() {
                    if (!this.cfg || !this.cfg.previewMode) {
                        this.state.busy();
                        this.context.getBinder().then(angular.bind(this, this.getPartnerId));
                    }
                    else {
                        // set preview text
                        this.configuration = this.resources.preview;
                    }
                },

                getPartnerId: function(binder) {
                    var partners = this.$filter("filter")(binder.branding_users, { scope: "binder" }, true);

                    if (partners.length > 0) {
                        this.partner_id = partners[0].partner_id;
                        this.getWidgetConfiguration();
                    }
                    else {
                        this.onError({ data: "Couldn't find branding_user with scope: 'binder'" });
                    }
                },

                getWidgetConfiguration: function() {
                    this.api.partnerWidget.all({ partnerId: this.partner_id }).then(
                        angular.bind(this, this.getWidgetConfigurationSuccess),
                        angular.bind(this, this.onError));
                },

                getWidgetConfigurationSuccess: function(response) {
                    if (response.data.items.length > 0) {
                        this.configuration = JSON.parse(response.data.items[0].configuration);
                        this.state.idle();
                    }
                    else {
                        this.onError({ data: "Couldn't find partner_widget for partner_id " + this.partner_id });
                    }
                },
                
                onLinkClick: function() {
                    var url = this.$filter("url")(this.configuration.url);
                    this.event.create({ event_name: "partner_custom_widget_clicked", event_type: "engagement", user_id: this.currentUser.id, user_role: this.currentUser.role, user_created_at: this.currentUser.user_created_at, partner_id: this.partner_id });
                    this.$window.open(url, "_blank");
                },

                onError: function(response) {
                    this.$log.error(response);
                    this.state.error();
                }
            });

            this.model = new Model(this.cfg);
        }
    ]);