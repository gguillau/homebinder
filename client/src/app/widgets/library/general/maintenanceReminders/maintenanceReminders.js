angular
    .module('hb.widgets.library.general.maintenanceReminders', [
        'hb.widgets.widgetProvider'
    ])
    .config(['hbWidgetProvider', function(widgetProvider) {
        widgetProvider.register('maintenanceReminders', {
            name: 'Maintenance Reminders',
            key: 'maintenanceReminders',
            description: 'Dislays the next 5 maintenance reminders.',
            category: 'Binder.Maintenance',
            template: '<hb-maintenance-reminders-widget></hb-maintenance-reminders-widget>',
            directive: 'hb-maintenance-reminders-widget',
            width: 'md',
            height: 'sm'
        });
    }])
    .directive('hbMaintenanceRemindersWidget', function() {
        return {
            restrict: "E",
            templateUrl: "widgets/library/general/maintenanceReminders/maintenanceReminders.tpl.html",
            controller: "MaintenanceRemindersWidgetController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {
                cfg: '='
            }
        };
    })
    .controller('MaintenanceRemindersWidgetController', [
        '$log',
        '$stateParams',
        'hb.api',
        'widgetState',
        'hb.resources',
        function($log, $stateParams, api, widgetState, resources) {

            var Controller = function(cfg) {
                this.cfg = cfg;
                this.binderId = $stateParams.binderId;
                this.maintenanceItems = [];
                this.state = new widgetState();
                this.resources = resources.widgetMaintenanceReminders;
                this.today = new Date();
                this.now = this.today.getTime();
                this.fourteen = this.today.setDate(this.today.getDate() + 14);
                this.refresh();
            };

            angular.extend(Controller.prototype, {
                refresh: function() {
                    if (this.cfg && this.cfg.previewMode) {
                        this.maintenanceItems = [{
                            name: this.resources.item1,
                            do_date: new Date()
                        }, {
                            name: this.resources.item2,
                            do_date: new Date()
                        }, {
                            name: this.resources.item3,
                            do_date: new Date()
                        }];
                    }
                    else {
                        this.state.busy();
                        var opts = {
                            page: 1,
                            count: 10,
                            binderId: this.binderId
                        };
                        api.maintenanceItem.all(opts).then(
                            angular.bind(this, this.refreshComplete),
                            angular.bind(this, this.refreshError));
                    }
                },
                refreshComplete: function(response) {
                    var due;

                    this.maintenanceItems = response.data.items.map(function(item) {
                        due = new Date(item.next_event_date).getTime();

                        if (this.now > due) {
                            item.rowclass = "danger";
                        }
                        else if (this.fourteen > due) {
                            item.rowclass = "warning";
                        }
                        else {
                            item.rowclass = "";
                        }

                        return item;

                    }, this);

                    this.state.idle();
                },
                refreshError: function(error) {
                    $log.error(error.data);
                    this.state.error();
                }
            });

            this.model = new Controller(this.cfg);
        }
    ]);