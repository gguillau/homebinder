describe("MaintenanceRemindersWidgetController", function(){
    var $controllerService,
        controller,
        base,
        $rootScope,
        $q,
        $log,
        $stateParams,
        api,
        widgetState,
        resources,
        today = new Date();
        
    // load the homebinder module
    beforeEach(module("homebinder"));

    // create the controller
    beforeEach(inject(function($controller, $injector) {
        $controllerService = $controller;
        $rootScope = $injector.get("$rootScope");
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        $stateParams = $injector.get("$stateParams");
        $stateParams.binderId = 1;
        api = $injector.get("hb.api");
        widgetState = $injector.get("widgetState");
        resources = $injector.get("hb.resources");
    }));
    
    function createController() {
        controller = $controllerService("MaintenanceRemindersWidgetController", {
            "log": $log,
            "$stateParams": $stateParams,
            "hb.api": api,
            "widgetState": widgetState,
            "hb.resources": resources
        });
    }

    describe("refresh", function() {
        it("calls hb.api.maintenanceItem.all", function(){
            spyOn(api.maintenanceItem, "all").and.returnValue($q.when({ data:{items: [
                {id: 1, name: "name", next_event_date: new Date().setDate(today.getDate() - 1)},
                {id: 2, name: "two", next_event_date:  new Date().setDate(today.getDate() + 12)},
                {id: 3, name: "three", next_event_date: new Date().setDate(today.getDate() + 30)}
            ]}}));
            
            createController();
            controller.model.refresh();
            
            $rootScope.$apply();
            
            expect(api.maintenanceItem.all).toHaveBeenCalledWith({
                page: 1,
                count: 10,
                binderId: 1
            });
            expect(controller.model.maintenanceItems.length).toEqual(3);
            expect(controller.model.maintenanceItems[0].rowclass).toEqual("danger");
            expect(controller.model.maintenanceItems[1].rowclass).toEqual("warning");
            expect(controller.model.maintenanceItems[2].rowclass).toEqual("");
            expect(controller.model.state.isIdle()).toEqual(true);
        });
        
        it("handles an error", function() {
            spyOn(api.maintenanceItem, "all").and.returnValue($q.reject({ data: "error" }));
            spyOn($log, "error");
            
            createController();
            controller.model.refresh();
            
            $rootScope.$apply();
            
            expect($log.error).toHaveBeenCalledWith("error");
            expect(controller.model.state.isError()).toEqual(true);
        });
    });
});

describe('hbMaintenanceRemindersWidget', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = { previewMode: true };

        element = $compile('<hb-maintenance-reminders-widget cfg="cfg"></hb-maintenance-reminders-widget>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("Change Smoke Detectors");
        });
    });
});