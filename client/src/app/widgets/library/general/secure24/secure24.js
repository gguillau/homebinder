angular
    .module('hb.widgets.library.general.secure24', [
        'hb.widgets.widgetProvider'
    ])
    .config(['hbWidgetProvider', function(widgetProvider) {
        widgetProvider.register('secure24Widget', {
            name: 'Secure 24 Widget',
            key: 'secure24Widget',
            description: 'Allow homeowners to send requests to Secure 24 Alarm Systems, the nation’s leading ADT Authorized Dealer',
            category: 'Binder.Secure24',
            template: '<hb-secure-24-widget></hb-secure-24-widget>',
            directive: 'hb-secure-24-widget',
            width: 'md',
            height: 'sm'
        });
    }])
    .directive('hbSecure24Widget', function() {
        return {
            restrict: "E",
            templateUrl: "widgets/library/general/secure24/secure24.tpl.html",
            controller: "Secure24WidgetController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {
                cfg: '='
            }
        };
    })
    .controller('Secure24WidgetController', [
        '$log',
        '$stateParams',
        'hb.api',
        'widgetState',
        'hb.resources',
        '$location',
        '$window',
        'Notify',
        'Session',
        '$sce',
        'Event',
        'HBWidgetBase',
        function($log, $stateParams, api, widgetState, resources, $location, $window, Notify, Session, $sce, event, HBWidgetBase) {
            var Controller = function(cfg) {
                HBWidgetBase.call(this);
                this.cfg = cfg;
                this.widget = this.cfg.widget;
                this.binderId = $stateParams.binderId;
                this.state = new widgetState();
                this.resources = resources.secure24;
                this.api = api;
            };

            Controller.prototype = Object.create(HBWidgetBase.prototype);

            angular.extend(Controller.prototype, {

                request: function() {
                    api.binder.secure24(this.binderId);
                    Notify.info("Your request has been processed.  You will be contacted in the next 24-48 hours.");
                    event.create({ event_name: "secure24_widget_request", event_type: "engagement", user_id: this.user.id, user_role: this.user.role, user_created_at: this.user.created_at, user_create_method: this.user.create_method });
                },

                createEvent: function(new_event) {
                    event.create(new_event);
                }
            });

            this.model = new Controller(this.cfg);
        }
    ]);