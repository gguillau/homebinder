describe("Secure24WidgetController", function() {
    var $controllerService,
        controller,
        $rootScope,
        $q,
        $log,
        $stateParams,
        api,
        widgetState,
        resources,
        $location,
        $window,
        Notify,
        event;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Session", function() {
            return {
                getJwt: function() {},
                getLocale: function() {
                    return null;
                },
                setLocale: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector) {
        $controllerService = $controller;
        $rootScope = $injector.get("$rootScope");
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        $stateParams = $injector.get("$stateParams");
        api = $injector.get("hb.api");
        widgetState = $injector.get("widgetState");
        resources = $injector.get("hb.resources");
        $location = $injector.get("$location");
        $window = $injector.get("$window");
        Notify = $injector.get("Notify");
        event = $injector.get("Event");

        spyOn(api.user, "get").and.returnValue($q.reject({ data: "error" }));
    }));

    function createController() {
        controller = $controllerService("Secure24WidgetController", {
            "log": $log,
            "$stateParams": $stateParams,
            "hb.api": api,
            "widgetState": widgetState,
            "hb.resources": resources,
            "$location": $location,
            "$window": $window,
            "Notify": Notify,
            "Event": event
        }, { cfg: {} });
    }

    describe("init", function() {
        it("sets the controller", function() {
            createController();
            expect(controller.model.resources.subTitle).toEqual("Is your home secure? Have Secure24 contact you to learn more about security options for your home.");
        });
    });

    describe("request", function() {
        it("calls event create", function() {
            createController();
            spyOn(api.binder, "secure24").and.returnValue($q.when({ data: {} }));
            controller.model.user = {};
            controller.model.request();

            expect(api.binder.secure24).toHaveBeenCalled();
        });
    });

    describe("createEvent", function() {
        it("calls event create", function() {
            createController();
            spyOn(event, "create");
            controller.model.createEvent();

            expect(event.create).toHaveBeenCalled();
        });
    });

});

describe('hbSecure24Widget', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = {};

        element = $compile('<hb-secure-24-widget cfg="cfg"></hb-secure-24-widget>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("Is your home secure? Have Secure24 contact you to learn more about security options for your home.");
        });
    });
});