angular
    .module('hb.widgets.library.general.repairPricer', [
        'hb.widgets.widgetProvider'
    ])
    .config(['hbWidgetProvider', function(widgetProvider) {
        widgetProvider.register('repairPricerWidget', {
            name: 'Repair Pricer Widget',
            key: 'repairPricerWidget',
            description: 'Displays information about Repair Pricer',
            category: 'Binder.RP',
            template: '<hb-repair-pricer-widget></hb-repair-pricer-widget>',
            directive: 'hb-repair-pricer-widget',
            width: 'md',
            height: 'sm'
        });
    }])
    .directive('hbRepairPricerWidget', function() {
        return {
            restrict: "E",
            templateUrl: "widgets/library/general/repairPricer/repairPricer.tpl.html",
            controller: "RepairPricerWidgetController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {
                cfg: '='
            }
        };
    })
    .controller('RepairPricerWidgetController', [
        '$log',
        '$stateParams',
        'hb.api',
        'widgetState',
        'hb.resources',
        '$location',
        '$window',
        'Notify',
        'Session',
        'RepairPricerModal',
        '$sce',
        'Event',
        'HBWidgetBase',
        function($log, $stateParams, api, widgetState, resources, $location, $window, Notify, Session, RepairPricerModal, $sce, event, HBWidgetBase) {
            var Controller = function(cfg) {
                HBWidgetBase.call(this);
                this.cfg = cfg;
                this.widget = this.cfg.widget;
                this.binderId = $stateParams.binderId;
                this.state = new widgetState();
                this.resources = resources.repairPricerWidget;
                this.user = Session.getUser();
                this.getPartnerBinder();
                this.getUser();
                this.api = api;
            };

            Controller.prototype = Object.create(HBWidgetBase.prototype);

            angular.extend(Controller.prototype, {

                getUser: function() {
                    api.user.get(this.user.id).then(
                        angular.bind(this, this.getUserSuccess),
                        angular.bind(this, this.getUserError));
                },

                getUserSuccess: function(response) {
                    this.user = response.data;
                },

                getUserError: function(response) {
                    $log.error(response.data);
                },

                getPartnerBinder: function() {
                    if ($stateParams.binderId) {
                        api.partnerBinder.all({ binder_id: $stateParams.binderId }).then(
                            angular.bind(this, this.partnerBinderSuccess),
                            angular.bind(this, this.onError));
                    }
                },

                partnerBinderSuccess: function(response) {
                    if (response.data.items.length > 0) {
                        this.partner_binder = response.data.items[0];
                        this.partner = this.partner_binder.partner;
                        this.binder = this.partner_binder.binder;
                        if (this.partner) {
                            this.getTransactions();
                            if (this.partner.configuration.repair_pricer_pre_paid_reports) {
                                this.resources.subTitle = this.resources.freeSubTitle;
                                this.resources.helpText = this.resources.freeHelpText;
                            }
                        }
                    }
                },

                getTransactions: function() {
                    if ($stateParams.binderId && this.partner.configuration.repair_pricer_enabled) {
                        this.api.binderTransaction.all({ binder_id: $stateParams.binderId, partner_id: this.partner.id, transaction_type: "buy_side_inspection" }).then(
                            angular.bind(this, this.transactionSuccess),
                            angular.bind(this, this.onError));
                    }
                },

                transactionSuccess: function(response) {
                    this.transactions = response.data.items.filter(function(transaction) {
                        return transaction.transaction_type === "buy_side_inspection";
                    });
                },

                order: function() {
                    this.user.user_profile_attributes.address_attributes = this.binder.property;
                    RepairPricerModal.show({
                        user: this.user,
                        binder_id: $stateParams.binderId,
                        partner_id: this.partner.id,
                        report_type: this.partner.configuration.default_repair_pricer_report_type,
                        repair_pricer_pre_paid_reports: this.partner.configuration.repair_pricer_pre_paid_reports
                    });

                    event.create({ event_type: "repair_pricer", event_name: "widget_order_now_clicked" });
                    event.create({ event_name: "widget_order_now_clicked", event_type: "engagement", user_id: this.user.id, user_role: this.user.role, user_created_at: this.user.created_at, user_create_method: this.user.create_method });
                }
            });

            this.model = new Controller(this.cfg);
        }
    ]);