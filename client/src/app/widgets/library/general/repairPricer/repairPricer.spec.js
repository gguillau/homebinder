describe("RepairPricerWidgetController", function() {
    var $controllerService,
        controller,
        $q,
        $log,
        $stateParams,
        api,
        widgetState,
        resources,
        $location,
        $window,
        Notify,
        RepairPricerModal;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Session", function() {
            return {
                getUser: function() { return { id: 1, role: "admin" }; },
                getJwt: function() {},
                getLocale: function() {
                    return null;
                },
                setLocale: function() {}
            };
        });
    }));

    // create the controller
    beforeEach(inject(function($controller, $injector) {
        $controllerService = $controller;
        $rootScope = $injector.get("$rootScope");
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        $stateParams = $injector.get("$stateParams");
        api = $injector.get("hb.api");
        widgetState = $injector.get("widgetState");
        resources = $injector.get("hb.resources");
        $location = $injector.get("$location");
        $window = $injector.get("$window");
        Notify = $injector.get("Notify");
        RepairPricerModal = $injector.get("RepairPricerModal");

        spyOn(api.user, "get").and.returnValue($q.reject({ data: "error" }));
    }));

    function createController() {
        controller = $controllerService("RepairPricerWidgetController", {
            "log": $log,
            "$stateParams": $stateParams,
            "hb.api": api,
            "widgetState": widgetState,
            "hb.resources": resources,
            "$location": $location,
            "$window": $window,
            "Notify": Notify
        }, { cfg: {} });
    }

    describe("getUser", function() {
        it("calls user get", function() {
            createController();
            controller.model.getUser();

            expect(api.user.get).toHaveBeenCalled();
        });
    });

    describe("getUserSuccess", function() {
        it("sets the user", function() {
            createController();

            controller.model.getUserSuccess({ data: { id: 1 } });

            expect(controller.model.user).toEqual({ id: 1 });

        });
    });

    describe("getUserError", function() {
        it("calls $log error", function() {
            createController();
            spyOn($log, "error");

            controller.model.getUserError({ data: "error" });

            expect($log.error).toHaveBeenCalled();

        });
    });

    describe("getPartnerBinder", function() {
        it("calls binderTransaction all", function() {

            spyOn(api.partnerBinder, "all").and.returnValue($q.when({
                data: { items: [] }
            }));
            $stateParams.binderId = 1;

            createController();
            controller.model.partner = {
                id: 1,
                configuration: {
                    repair_pricer_enabled: true,
                    repair_pricer_id: 1
                }
            };
            controller.model.getPartnerBinder();

            expect(api.partnerBinder.all).toHaveBeenCalled();

        });

    });

    describe("partnerBinderSuccess", function() {
        it("calls getTransactions", function() {

            createController();
            spyOn(controller.model, "getTransactions");

            controller.model.partnerBinderSuccess({ data: { items: [{ id: 1, partner: { id: 1, configuration: { repair_pricer_pre_paid_reports: true } } }] } });

            expect(controller.model.getTransactions).toHaveBeenCalled();

        });
        
        it("sets the subtitle to the default subtitle", function() {
            createController();

            controller.model.partnerBinderSuccess({ data: { items: [{ id: 1, partner: { id: 1, configuration: { repair_pricer_pre_paid_reports: false } } }] } });

            expect(controller.model.resources.subTitle).toEqual("Convert your inspection report into a full breakdown of projected repair costs in less than 24 hours. Order to receive $10 off a cost estimate report from Repair Pricer for just $50.");

        });
        
        it("sets the subtitle to the free subtitle", function() {
            createController();

            controller.model.partnerBinderSuccess({ data: { items: [{ id: 1, partner: { id: 1, configuration: { repair_pricer_pre_paid_reports: true } } }] } });

            expect(controller.model.resources.subTitle).toEqual("Convert your inspection report into a full breakdown of projected repair costs in less than 24 hours. Order to receive a FREE cost estimate report from Repair Pricer ($60 Value).");

        });

    });

    describe("getTransactions", function() {
        it("calls binderTransaction all", function() {

            spyOn(api.binderTransaction, "all").and.returnValue($q.when({
                data: { items: [] }
            }));
            $stateParams.binderId = 1;

            createController();
            controller.model.partner = {
                id: 1,
                configuration: {
                    repair_pricer_enabled: true,
                    repair_pricer_id: 1
                }
            };
            controller.model.getTransactions();

            expect(api.binderTransaction.all).toHaveBeenCalled();

        });

    });

    describe("transactionSuccess", function() {
        it("sets the transactions", function() {

            createController();
            controller.model.transactionSuccess({ data: { items: [{ id: 1, transaction_type: "buy_side_inspection" }] } });

            expect(controller.model.transactions.length).toEqual(1);

        });

    });

    describe("order", function() {
        it("calls $window open", function() {
            createController();
            spyOn(RepairPricerModal, "show");
            controller.model.user = {
                user_profile_attributes: {
                    address_attributes: {

                    }
                }
            };
            controller.model.partner = {
                id: 1,
                configuration: {
                    repair_pricer_enabled: true,
                    repair_pricer_id: 1
                }
            };
            controller.model.user = {
                user_profile_attributes: {
                    address_attributes: {}
                }
            };
            controller.model.binder = {
                property: {}
            };
            controller.model.order();

            expect(RepairPricerModal.show).toHaveBeenCalled();
        });

    });
});

describe('hbRepairPricerWidget', function() {
    var $scope, $compile, api, session, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        session = _$injector_.get("Session");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(session, "getUser").and.returnValue({ id: 1 });
        spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1 } }));

        $scope.cfg = {};

        $compile('<hb-repair-pricer-widget cfg="cfg"></hb-repair-pricer-widget>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls api user get', function() {
            expect(api.user.get).toHaveBeenCalled();
        });
    });
});