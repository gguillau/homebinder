angular
    .module('hb.widgets.library.general.thirdPartyServices', [
        'hb.widgets.widgetProvider',
        'hb.widgets.library.general.thirdPartyServices.confirm',
        'hb.widgets.library.general.thirdPartyServices.nextSteps'
    ])
    .config(['hbWidgetProvider', function(widgetProvider) {
        widgetProvider.register('thirdPartyServices', {
            name: 'Third Party Services Form',
            key: 'thirdPartyServices',
            description: 'Allow homeowners to request services from third party companies',
            category: 'Binder.ThirdPartyServices',
            template: '<hb-third-party-services-form-widget></hb-third-party-services-form-widget>',
            directive: 'hb-third-party-services-form-widget',
            width: 'md',
            height: 'sm'
        });
    }])
    .directive('hbThirdPartyServicesFormWidget', function() {
        return {
            restrict: "E",
            templateUrl: "widgets/library/general/thirdPartyServices/thirdPartyServices.tpl.html",
            controller: "ThirdPartyServicesFormWidgetController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {
                cfg: '='
            }
        };
    })
    .controller('ThirdPartyServicesFormWidgetController', [
        '$log',
        '$stateParams',
        'hb.api',
        'widgetState',
        'hb.resources',
        'Notify',
        'Loading',
        '$filter',
        'HBWidgetBase',
        'ThirdPartyServicesConfirmForm',
        'NextStepsModal',
        'Context',
        function($log, $stateParams, api, widgetState, resources, Notify, Loading, $filter, HBWidgetBase, ThirdPartyServicesConfirmForm, NextStepsModal, context) {
            var Controller = function(cfg) {
                HBWidgetBase.call(this);
                this.cfg = cfg;
                this.widget = this.cfg.widget;
                this.binderId = $stateParams.binderId;
                this.state = new widgetState();
                this.resources = resources.thirdPartyServicesWidget;
                this.services = [{
                    name: "utilites",
                    label: this.resources.utilites,
                    selected: true
                }, {
                    name: "cable",
                    label: this.resources.cable,
                    selected: true
                }, {
                    name: "project",
                    label: this.resources.projects,
                    selected: true
                }, {
                    name: "security",
                    label: this.resources.security,
                    selected: true
                }];
                this.step = 1;
                this.processing = false;
                this.solarUrl = "https://www.energysage.com/";
                this.showSolarInfo = false;
                context.getBinder(this.binderId).then(angular.bind(this, function(binder) {
                    var valid_states = ["AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "IL", "IN",
                        "IA", "LA", "MA", "MD", "ME", "MI", "MN", "MO", "NC", "NH",
                        "NJ", "NM", "NV", "NY", "OH", "OR", "PA", "RI", "SC", "TX",
                        "UT", "VA", "VT", "WA", "WI"
                    ];

                    if (valid_states.indexOf(binder.property.state) > -1) {
                        this.services.push({
                            name: "solar",
                            label: this.resources.solar,
                            selected: true
                        });
                    }

                }));
            };

            Controller.prototype = Object.create(HBWidgetBase.prototype);

            angular.extend(Controller.prototype, {
                confirm: function() {
                    ThirdPartyServicesConfirmForm.show({
                        closed: angular.bind(this, this.onConfirm)
                    });
                },

                onConfirm: function() {
                    this.processing = true;
                    var services = $filter('filter')(this.services, true);
                    services = services.map(angular.bind(this, function(service) {
                        return service.label;
                    }));
                    
                    var solarServices = $filter('filter')(services, this.resources.solar);
                    solarServices = solarServices[0];
                    
                    if (solarServices) {
                        this.requestSolarServices(services);
                    }
                    else {
                        this.requestServices(services);
                    }
                },
                
                requestSolarServices: function(services) {
                    Loading.show(this.resources.loading);
                    api.binder.requestSolarServices(this.binderId, "support@homebinder.com").then(
                        angular.bind(this, this.requestSolarServicesSuccess, services),
                        angular.bind(this, this.servicesError));
                },

                requestSolarServicesSuccess: function(services, response) {
                    Loading.close();
                    this.solarUrl = response.data;
                    this.showSolarInfo = true;
                    this.requestServices(services);
                },
                
                requestServices: function(services) {
                    api.binder.requestServices(this.binderId, services, "support@homebinder.com").then(
                        angular.bind(this, this.requestServicesSuccess),
                        angular.bind(this, this.servicesError));
                },

                requestServicesSuccess: function(response) {
                    NextStepsModal.show({
                        solarUrl: this.solarUrl,
                        showSolarInfo: this.showSolarInfo
                    });
                    
                    this.step = 2;
                    Notify.success(this.resources.success);
                },

                servicesError: function(error) {
                    Loading.close();
                    $log.error(error.data);
                    this.step = 2;
                    this.feedbackText = this.resources.error;
                },

                declineServices: function() {
                    this.processing = true;
                    api.binder.declineServices(this.binderId, "support@homebinder.com").then(
                        angular.bind(this, this.declineServicesSuccess),
                        angular.bind(this, this.servicesError));
                },

                declineServicesSuccess: function(response) {
                    this.step = 2;
                    this.feedbackText = this.resources.decline;
                    Notify.info(this.feedbackText);
                }
            });

            this.model = new Controller(this.cfg);
        }
    ]);