describe("NextStepsModalController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $modalInstance;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;

        $modalInstance = {
            close: function() {},
            dismiss: function() {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("NextStepsModalController", {
            "$modalInstance": $modalInstance,
            "data": {}
        });

        $scope.ctrl = ctrl;

    }

    describe('ctrl.close', function() {
        it('should close the modal', function() {
            spyOn($modalInstance, "close");
            createController();
            ctrl.close();
            $rootScope.$apply();

            expect($modalInstance.close).toHaveBeenCalled();
        });
    });
});