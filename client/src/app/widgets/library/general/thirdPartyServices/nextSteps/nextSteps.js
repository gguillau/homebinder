(function() {
	"use strict";

	angular
		.module("hb.widgets.library.general.thirdPartyServices.nextSteps", [])
		.factory("NextStepsModal", [
			"ModalService",
			function(Modals) {
				return {
					show: function(opts) {
						Modals.show({
							templateUrl: "widgets/library/general/thirdPartyServices/nextSteps/nextSteps.tpl.html",
							controller: "NextStepsModalController as ctrl",
							resolveData: opts,
							backdrop: true
						});
					}
				};
			}
		])
		.controller("NextStepsModalController", [
			"data",
			"$modalInstance",
			"hb.resources",
			NextStepsModalController
		]);

	function NextStepsModalController(opts, $modalInstance, resources) {
		this.opts = opts;
		this.solarUrl = opts.solarUrl;
		this.showSolarInfo = opts.showSolarInfo;
		this.$modal = $modalInstance;
		this.resources = resources.nextStepsModal;
	}

	NextStepsModalController.prototype = {
		close: function() {
			this.$modal.close();
		}
	};

})();