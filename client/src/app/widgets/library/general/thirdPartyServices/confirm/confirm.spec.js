describe("ThirdPartyServicesConfirmFormController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $modalInstance;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;

        $modalInstance = {
            close: function(button) {},
            dismiss: function(button) {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("ThirdPartyServicesConfirmFormController", {
            "$modalInstance": $modalInstance
        });

        $scope.ctrl = ctrl;

    }

    describe('ctrl.cancel', function() {
        it('should dismiss the modal', function() {
            spyOn($modalInstance, "dismiss");
            createController();
            ctrl.cancel();
            $rootScope.$apply();

            expect($modalInstance.dismiss).toHaveBeenCalledWith("cancel");
        });
    });
    
    describe('ctrl.close', function() {
        it('should close the modal', function() {
            spyOn($modalInstance, "close");
            createController();
            ctrl.close();
            $rootScope.$apply();

            expect($modalInstance.close).toHaveBeenCalled();
        });
    });
});