(function() {
	"use strict";

	angular
		.module("hb.widgets.library.general.thirdPartyServices.confirm", [])
		.factory("ThirdPartyServicesConfirmForm", [
			"ModalService",
			function(Modals) {
				return {
					show: function(opts) {
						Modals.show({
							templateUrl: "widgets/library/general/thirdPartyServices/confirm/confirm.tpl.html",
							controller: "ThirdPartyServicesConfirmFormController as ctrl",
							resolveData: opts,
							backdrop: true,
							closed: opts.closed
						});
					}
				};
			}
		])
		.controller("ThirdPartyServicesConfirmFormController", [
			"hb.resources",
			"$modalInstance",
			ThirdPartyServicesConfirmFormController
		]);

	function ThirdPartyServicesConfirmFormController(resources, $modalInstance) {
		this.resources = resources.thirdPartyConfirm;
		this.$modal = $modalInstance;
	}

	ThirdPartyServicesConfirmFormController.prototype = {
		close: function() {
			this.$modal.close();
		},
		cancel: function() {
			this.$modal.dismiss("cancel");
		}
	};

})();