describe("ThirdPartyServicesFormWidgetController", function() {
    var $controllerService,
        controller,
        $rootScope,
        $q,
        $log,
        $stateParams,
        api,
        widgetState,
        resources,
        notify,
        loading,
        ThirdPartyServicesConfirmForm,
        NextStepsModal;

    // load the homebinder module
    beforeEach(module("homebinder"));

    // create the controller
    beforeEach(inject(function($controller, $injector) {
        $controllerService = $controller;
        $rootScope = $injector.get("$rootScope");
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        $stateParams = $injector.get("$stateParams");
        $stateParams.binderId = 1;
        api = $injector.get("hb.api");
        widgetState = $injector.get("widgetState");
        resources = $injector.get("hb.resources");
        notify = $injector.get("Notify");
        loading = $injector.get("Loading");
        ThirdPartyServicesConfirmForm = $injector.get("ThirdPartyServicesConfirmForm");
        NextStepsModal = $injector.get("NextStepsModal");
    }));

    function createController() {
        controller = $controllerService("ThirdPartyServicesFormWidgetController", {
            "log": $log,
            "$stateParams": $stateParams,
            "hb.api": api,
            "widgetState": widgetState,
            "hb.resources": resources,
            "Notify": notify,
            "Loading": loading,
            "ThirdPartyServicesConfirmForm": ThirdPartyServicesConfirmForm,
            "NextStepsModal": NextStepsModal
        }, { cfg: {} });
    }

    describe("confirm", function() {
        it("calls ThirdPartyServicesConfirmForm show", function() {
            spyOn(ThirdPartyServicesConfirmForm, "show");

            createController();
            controller.model.confirm();

            expect(ThirdPartyServicesConfirmForm.show).toHaveBeenCalled();
        });
    });
    
    describe("onConfirm", function() {
        it("calls requestSolarServices", function() {
            spyOn(api.binder, "requestSolarServices").and.returnValue($q.when({ data: "success" }));
            spyOn(api.binder, "requestServices").and.returnValue($q.when({ data: "success" }));

            createController();
            controller.model.services = [{
                    name: "utilites",
                    label: controller.model.resources.utilites,
                    selected: true
                }, {
                    name: "solar",
                    label: controller.model.resources.solar,
                    selected: true
                }];
            spyOn(controller.model, "requestSolarServices");
            
            controller.model.onConfirm();
            $rootScope.$apply();

            expect(controller.model.processing).toEqual(true);
            expect(controller.model.requestSolarServices).toHaveBeenCalled();
        });
        
        it("calls requestServices", function() {
            spyOn(api.binder, "requestSolarServices").and.returnValue($q.when({ data: "success" }));
            spyOn(api.binder, "requestServices").and.returnValue($q.when({ data: "success" }));

            createController();
            controller.model.services = [{
                    name: "utilites",
                    label: controller.model.resources.utilites,
                    selected: true
                }];
            spyOn(controller.model, "requestServices");
            
            controller.model.onConfirm();
            $rootScope.$apply();

            expect(controller.model.processing).toEqual(true);
            expect(controller.model.requestServices).toHaveBeenCalled();
        });
    });
    
    describe("requestSolarServices", function() {
        it("calls hb.api.binder.requestSolarServices", function() {
            spyOn(api.binder, "requestSolarServices").and.returnValue($q.when({ data: "success" }));
            spyOn(api.binder, "requestServices").and.returnValue($q.when({ data: "success" }));
            spyOn(loading, "show");

            createController();
            controller.model.cfg = {
                removeFromDashboard: function() {}
            };
            
            spyOn(controller.model, "requestSolarServicesSuccess");
            controller.model.requestSolarServices();

            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalled();
            expect(api.binder.requestSolarServices).toHaveBeenCalledWith(1, 'support@homebinder.com');
            expect(controller.model.requestSolarServicesSuccess).toHaveBeenCalled();
        });

        it("handles an error", function() {
            spyOn(api.binder, "requestSolarServices").and.returnValue($q.reject({ data: "error" }));
            spyOn(loading, "show");

            createController();
            controller.model.cfg = {
                removeFromDashboard: function() {}
            };
            
            spyOn(controller.model, "servicesError");
            controller.model.requestSolarServices();

            $rootScope.$apply();
            
            expect(loading.show).toHaveBeenCalled();
            expect(api.binder.requestSolarServices).toHaveBeenCalledWith(1, "support@homebinder.com");
            expect(controller.model.servicesError).toHaveBeenCalled();
        });
    });
    
    describe("requestSolarServicesSuccess", function() {
        it("calls Loading.close and requestServices", function() {
            spyOn(api.binder, "requestServices").and.returnValue($q.when({ data: "success" }));
            spyOn(loading, "close");

            createController();
            controller.model.cfg = {
                removeFromDashboard: function() {}
            };
            
            spyOn(controller.model, "requestServices");
            controller.model.requestSolarServicesSuccess([], {data: "url"});

            $rootScope.$apply();

            expect(loading.close).toHaveBeenCalled();
            expect(controller.model.solarUrl).toEqual("url");
            expect(controller.model.showSolarInfo).toEqual(true);
            expect(controller.model.requestServices).toHaveBeenCalled();
        });
    });

    describe("requestServices", function() {
        it("calls hb.api.binder.requestServices", function() {
            spyOn(api.binder, "requestServices").and.returnValue($q.when({ data: "success" }));

            createController();
            controller.model.cfg = {
                removeFromDashboard: function() {}
            };
            var services = [
                'Setting up utilities (gas, electric, water)', 
                'Choosing cable, internet or satellite TV providers', 
                'Planning projects or getting quotes from local home pros', 
                'Installing home security systems'];
            controller.model.requestServices(services);

            $rootScope.$apply();

            expect(api.binder.requestServices).toHaveBeenCalledWith(1, ['Setting up utilities (gas, electric, water)', 'Choosing cable, internet or satellite TV providers', 'Planning projects or getting quotes from local home pros', 'Installing home security systems'], 'support@homebinder.com');
        });

        it("handles an error", function() {
            spyOn(api.binder, "requestServices").and.returnValue($q.reject({ data: "error" }));
            spyOn($log, "error");

            createController();
            controller.model.cfg = {
                removeFromDashboard: function() {}
            };
            var services = [];
            controller.model.requestServices(services);

            $rootScope.$apply();

            expect(api.binder.requestServices).toHaveBeenCalledWith(1, [], "support@homebinder.com");
            expect($log.error).toHaveBeenCalledWith("error");
        });
    });
    
    describe("requestServicesSuccess", function() {
        it("calls NextStepsModal and notify", function() {
            spyOn(api.binder, "requestServices").and.returnValue($q.when({ data: "success" }));
            spyOn(NextStepsModal, "show");
            
            createController();
            controller.model.cfg = {
                removeFromDashboard: function() {}
            };
            
            spyOn(notify, "success");
            controller.model.requestServicesSuccess();

            $rootScope.$apply();

            expect(NextStepsModal.show).toHaveBeenCalled();
            expect(controller.model.step).toEqual(2);
            expect(notify.success).toHaveBeenCalled();
        });
    });
    
    describe("servicesError", function() {
        it("calls loading.close and $log.error", function() {
            spyOn(loading, "close");
            spyOn($log, "error");
            
            createController();
            controller.model.cfg = {
                removeFromDashboard: function() {}
            };
            
            controller.model.servicesError({data: "error"});

            $rootScope.$apply();

            expect(loading.close).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalledWith("error");
            expect(controller.model.step).toEqual(2);
        });
    });

    describe("declineServices", function() {
        it("calls hb.api.binder.declineServices", function() {
            spyOn(api.binder, "declineServices").and.returnValue($q.when({ data: "success" }));
            spyOn(notify, "info");

            createController();
            controller.model.cfg = {
                removeFromDashboard: function() {}
            };
            controller.model.declineServices();

            $rootScope.$apply();

            expect(controller.model.processing).toEqual(true);
            expect(api.binder.declineServices).toHaveBeenCalledWith(1, "support@homebinder.com");
            expect(notify.info).toHaveBeenCalled();
        });

        it("handles an error", function() {
            spyOn(api.binder, "declineServices").and.returnValue($q.reject({ data: "error" }));
            spyOn($log, "error");

            createController();
            controller.model.cfg = {
                removeFromDashboard: function() {}
            };
            controller.model.declineServices();

            $rootScope.$apply();

            expect(controller.model.processing).toEqual(true);
            expect($log.error).toHaveBeenCalledWith("error");
        });
    });
});

describe('hbThirdPartyServicesFormWidget', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = {};

        element = $compile('<hb-third-party-services-form-widget cfg="cfg"></hb-third-party-services-form-widget>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("How Can We Help?");
        });
    });
});