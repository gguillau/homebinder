describe("SellSideInspectionWidgetController", function() {
    var $controllerService,
        controller,
        $q,
        $log,
        $stateParams,
        api,
        widgetState,
        resources,
        $location,
        $state,
        Notify;

    // load the homebinder module
    beforeEach(module("homebinder"));

    // create the controller
    beforeEach(inject(function($controller, $injector) {
        $controllerService = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        $stateParams = $injector.get("$stateParams");
        $stateParams.binderId = 1;
        api = $injector.get("hb.api");
        widgetState = $injector.get("widgetState");
        resources = $injector.get("hb.resources");
        $location = $injector.get("$location");
        $state = $injector.get("$state");
        Notify = $injector.get("Notify");
    }));

    function createController() {
        controller = $controllerService("SellSideInspectionWidgetController", {
            "log": $log,
            "$stateParams": $stateParams,
            "hb.api": api,
            "widgetState": widgetState,
            "hb.resources": resources,
            "$location": $location,
            "Notify": Notify
        }, { cfg: {} });
    }

    describe("create", function() {
        it("calls hb.api.sellerReport.create", function() {
            spyOn(api.sellerReport, "create").and.returnValue($q.when({ data: { id: 1 } }));

            createController();
            controller.model.create();

            expect(api.sellerReport.create).toHaveBeenCalled();
        });
    });

    describe("onCreateReportSuccess", function() {
        it("calls $state go", function() {
            createController();
            spyOn($state, "go");
            controller.model.onCreateReportSuccess({ data: { code: 1 } });

            expect($state.go).toHaveBeenCalled();
        });
    });

    describe("onCreateReportError", function() {
        it("calls Notify error", function() {
            spyOn(Notify, "error");

            createController();

            controller.model.onCreateReportError({ data: "error" });

            expect(Notify.error).toHaveBeenCalled();
        });
    });
});

describe('hbSellSideInspectionWidget', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = {};

        element = $compile('<hb-sell-side-inspection-widget cfg="cfg"></hb-sell-side-inspection-widget>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("Learn how HomeBinder can help sell your home faster and at a higher price!");
        });
    });
});