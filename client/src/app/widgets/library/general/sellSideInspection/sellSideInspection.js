angular
    .module('hb.widgets.library.general.sellSideInspection', [
        'hb.widgets.widgetProvider'
    ])
    .config(['hbWidgetProvider', function(widgetProvider) {
        widgetProvider.register('sellSideInspection', {
            name: 'Sell Side Inspection Widget',
            key: 'sellSideInspection',
            description: 'Displays information about an APR',
            category: 'Binder.sellSideInspection',
            template: '<hb-sell-side-inspection-widget></hb-sell-side-inspection-widget>',
            directive: 'hb-sell-side-inspection-widget',
            width: 'md',
            height: 'sm'
        });
    }])
    .directive('hbSellSideInspectionWidget', function() {
        return {
            restrict: "E",
            templateUrl: "widgets/library/general/sellSideInspection/sellSideInspection.tpl.html",
            controller: "SellSideInspectionWidgetController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {
                cfg: '='
            }
        };
    })
    .controller('SellSideInspectionWidgetController', [
        '$log',
        '$stateParams',
        'hb.api',
        'widgetState',
        'hb.resources',
        '$location',
        '$window',
        'Notify',
        '$state',
        'HBWidgetBase',
        function($log, $stateParams, api, widgetState, resources, $location, $window, Notify, $state, HBWidgetBase) {
            var Controller = function(cfg) {
                this.cfg = cfg;
                this.widget = this.cfg.widget;
                this.binderId = $stateParams.binderId;
                this.state = new widgetState();
                this.resources = resources.sellSideInspections;
            };

            Controller.prototype = Object.create(HBWidgetBase.prototype);

            angular.extend(Controller.prototype, {

                create: function() {
                    api.sellerReport.create({
                        "binder_id": this.binderId,
                        "public": true
                    }).then(
                        angular.bind(this, this.onCreateReportSuccess),
                        angular.bind(this, this.onCreateReportError)
                    );
                },

                onCreateReportSuccess: function(response) {
                    $state.go("seller_report_edit", {
                        code: response.data.code
                    });
                },

                onCreateReportError: function(response) {
                    Notify.error(response.data);
                }
            });

            this.model = new Controller(this.cfg);
        }
    ]);