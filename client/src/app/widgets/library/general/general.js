angular
    .module('hb.widgets.library.general', [
        'hb.widgets.library.general.apr',
        'hb.widgets.library.general.binderCompleteness',
        'hb.widgets.library.general.binderHeader',
        'hb.widgets.library.general.doubleBusinessCard',
        'hb.widgets.library.general.homeSelfe',
        'hb.widgets.library.general.inspectionReport',
        'hb.widgets.library.general.maintenanceReminders',
        'hb.widgets.library.general.recentProjects',
        'hb.widgets.library.general.repairPricer',
        'hb.widgets.library.general.secure24',
        'hb.widgets.library.general.sellSideInspection',
        'hb.widgets.library.general.thirdPartyServices',
        'hb.widgets.library.general.topHomePros'
    ]);