describe("InspectionReportWidgetController", function() {
    var $controllerService,
        controller,
        $q,
        $log,
        $stateParams,
        api,
        widgetState,
        resources,
        $window,
        event,
        notify;

    // load the homebinder module
    beforeEach(module("homebinder"));
    
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Session", function() {
            return {
                getUser: function() { return { id: 1, role: "admin" }; },
                getJwt: function() {},
                getLocale: function() {
                    return null;
                },
                setLocale: function() {}
            };
        });
    }));
    
    // create the controller
    beforeEach(inject(function($controller, $injector) {
        $controllerService = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        $stateParams = $injector.get("$stateParams");
        api = $injector.get("hb.api");
        widgetState = $injector.get("widgetState");
        resources = $injector.get("hb.resources");
        $window = $injector.get("$window");
        notify = $injector.get("Notify");
        event = $injector.get("Event");

        spyOn(api.document, "all").and.returnValue($q.when({ data: { items: [{id: 1}] } }));
        $stateParams.binderId = 1;
    }));

    function createController() {
        controller = $controllerService("InspectionReportWidgetController", {
            "log": $log,
            "$stateParams": $stateParams,
            "hb.api": api,
            "widgetState": widgetState,
            "hb.resources": resources,
            "$window": $window,
            "Notify": notify,
            "Event": event
        }, { cfg: {} });
    }

    describe("refresh", function() {
        it("calls document all", function() {
            createController();
            controller.model.refresh();

            expect(api.document.all).toHaveBeenCalled();
        });
    });

    describe("onRefreshSuccess", function() {
        it("sets the inspection report to undefined", function() {
            createController();
            
            var response = {
                data: {
                    items: []
                }
            };

            controller.model.onRefreshSuccess(response);

            expect(controller.model.inspectionReport).toEqual(undefined);

        });
        
        it("sets the inspection to items[0]", function() {
            createController();
            
            var response = {
                data: {
                    items: [{id: 1}]
                }
            };

            controller.model.onRefreshSuccess(response);

            expect(controller.model.inspectionReport).toEqual({id: 1});

        });
    });
    
    describe("onRefreshError", function() {
        it("calls $log.error and notify.error", function() {
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            
            var response = {
                data: "error"
            };

            controller.model.onRefreshError(response);

            expect($log.error).toHaveBeenCalledWith({data: "error"});
            expect(notify.error).toHaveBeenCalledWith("error");
        });

    });

    describe("viewReport", function() {
        it("creates an event and opens the supplied url", function() {
            spyOn(event, "create");
            spyOn($window, "open");

            createController();
            controller.model.viewReport("url");

            expect(event.create).toHaveBeenCalled();
            expect($window.open).toHaveBeenCalledWith("url");

        });
    });
});

describe('hbInspectionReportWidget', function() {
    var $scope, $compile, api, session, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        session = _$injector_.get("Session");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(session, "getUser").and.returnValue({ id: 1 });
        spyOn(api.document, "all").and.returnValue($q.when({ data: { items: [{id: 1}] } }));

        $scope.cfg = {};

        $compile('<hb-inspection-report-widget cfg="cfg"></hb-inspection-report-widget>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls api document all', function() {
            expect(api.document.all).toHaveBeenCalled();
        });
    });
});