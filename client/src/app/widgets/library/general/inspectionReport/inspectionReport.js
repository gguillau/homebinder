angular
    .module('hb.widgets.library.general.inspectionReport', [
        'hb.widgets.widgetProvider'
    ])
    .config(['hbWidgetProvider', function(widgetProvider) {
        widgetProvider.register('inspectionReportWidget', {
            name: 'Inspection Report Widget',
            key: 'inspectionReportWidget',
            description: 'Allow user to view inspection report',
            category: 'Binder.IR',
            template: '<hb-inspection-report-widget></hb-inspection-report-widget>',
            directive: 'hb-inspection-report-widget',
            width: 'md',
            height: 'sm'
        });
    }])
    .directive('hbInspectionReportWidget', function() {
        return {
            restrict: "E",
            templateUrl: "widgets/library/general/inspectionReport/inspectionReport.tpl.html",
            controller: "InspectionReportWidgetController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {
                cfg: '='
            }
        };
    })
    .controller('InspectionReportWidgetController', [
        '$log',
        '$stateParams',
        'hb.api',
        'widgetState',
        'hb.resources',
        '$window',
        'Notify',
        'Session',
        '$sce',
        'Event',
        'HBWidgetBase',
        function($log, $stateParams, api, widgetState, resources, $window, Notify, Session, $sce, event, HBWidgetBase) {
            var Controller = function(cfg) {
                HBWidgetBase.call(this);
                this.cfg = cfg;
                this.widget = this.cfg.widget;
                this.binderId = $stateParams.binderId;
                this.state = new widgetState();
                this.resources = resources.inspectionReportWidget;
                this.user = Session.getUser();
                this.api = api;
                this.notify = Notify;
                this.$log = $log;
                this.inspectionReport = null;
                this.refresh();
            };

            Controller.prototype = Object.create(HBWidgetBase.prototype);

            angular.extend(Controller.prototype, {
                
                // View report button clicked
                refresh: function() {
                    var opts = {
                        binder_id: this.binderId,
                        document_type_id: 1
                    };
                    this.api.document.all(opts).then(
                        angular.bind(this, this.onRefreshSuccess),
                        angular.bind(this, this.onRefreshError)
                    );
                },
                
                // Refresh success, so set inspection report if it exists
                onRefreshSuccess: function(response) {
                    this.inspectionReport = response.data.items[0];
                },
                
                // Refresh error
                onRefreshError: function(response) {
                    this.$log.error(response);
                    this.notify.error(response.data);
                },
                
                // viewReport button pressed
                viewReport: function(url) {
                    event.create({ event_name: "view_document", event_type: "engagement", user_id: this.user.id, user_role: this.user.role, user_created_at: this.user.created_at, user_create_method: this.user.create_method });
                    $window.open(url);
                }
            });

            this.model = new Controller(this.cfg);
        }
    ]);