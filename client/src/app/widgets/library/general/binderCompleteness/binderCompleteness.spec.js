describe("BinderCompletenessWidgetController", function() {
    var $controllerService,
        controller,
        $q,
        $log,
        $stateParams,
        api,
        widgetState,
        resources,
        $window,
        event,
        notify;

    // load the homebinder module
    beforeEach(module("homebinder"));
    
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Session", function() {
            return {
                getUser: function() { return { id: 1, role: "admin" }; },
                getJwt: function() {},
                getLocale: function() {
                    return null;
                },
                setLocale: function() {}
            };
        });
    }));
    
    // create the controller
    beforeEach(inject(function($controller, $injector) {
        $controllerService = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        $stateParams = $injector.get("$stateParams");
        api = $injector.get("hb.api");
        widgetState = $injector.get("widgetState");
        resources = $injector.get("hb.resources");
        $window = $injector.get("$window");
        notify = $injector.get("Notify");
        event = $injector.get("Event");

        // Spy on API call and return response
        var response = { data: { items: [{id: 1}] } };
        spyOn(api.maintenanceItem, "all").and.returnValue($q.when(response));
        spyOn(api.project, "all").and.returnValue($q.when(response));
        spyOn(api.document, "all").and.returnValue($q.when(response));
        spyOn(api.area, "all").and.returnValue($q.when(response));
        spyOn(api.paint, "all").and.returnValue($q.when(response));
        spyOn(api.inventoryItem, "all").and.returnValue($q.when(response));
        spyOn(api.appliance, "all").and.returnValue($q.when(response));
        spyOn(api.permit, "all").and.returnValue($q.when(response));
        spyOn(api.binder, "get").and.returnValue($q.when({data: {id: 1}}));
        spyOn(api.binderContractor, "all").and.returnValue($q.when(response));
        
        $stateParams.binderId = 1;
    }));

    function createController() {
        controller = $controllerService("BinderCompletenessWidgetController", {
            "log": $log,
            "$stateParams": $stateParams,
            "hb.api": api,
            "widgetState": widgetState,
            "hb.resources": resources,
            "$window": $window,
            "Notify": notify,
            "Event": event
        }, { cfg: {} });
    }

    describe("refresh", function() {
        it("makes numerous API calls when cfg is null", function() {
            createController();
            
            spyOn(controller.model.state, "busy");
            
            var opts = { binderId: 1, count: 10 };
            controller.model.cfg = null;
            controller.model.refresh();

            expect(controller.model.state.busy).toHaveBeenCalled();
            expect(api.maintenanceItem.all).toHaveBeenCalledWith(opts);
            expect(api.project.all).toHaveBeenCalledWith(opts);
            expect(api.document.all).toHaveBeenCalledWith(opts);
            expect(api.area.all).toHaveBeenCalledWith(opts);
            expect(api.paint.all).toHaveBeenCalledWith(opts);
            expect(api.inventoryItem.all).toHaveBeenCalledWith(opts);
            expect(api.appliance.all).toHaveBeenCalledWith(opts);
            expect(api.permit.all).toHaveBeenCalledWith(opts);
            expect(api.binder.get).toHaveBeenCalledWith(1);
            expect(api.binderContractor.all).toHaveBeenCalledWith({binderId: 1});
            expect(api.document.all).toHaveBeenCalledWith({binder_id: 1, document_type_id: 1});
        });
        
        it("makes numerous API calls when cfg.previewMode is false", function() {
            createController();
            
            spyOn(controller.model.state, "busy");
            
            var opts = { binderId: 1, count: 10 };
            controller.model.cfg = {
                previewMode: false
            };
            controller.model.refresh();

            expect(controller.model.state.busy).toHaveBeenCalled();
            expect(api.maintenanceItem.all).toHaveBeenCalledWith(opts);
            expect(api.project.all).toHaveBeenCalledWith(opts);
            expect(api.document.all).toHaveBeenCalledWith(opts);
            expect(api.area.all).toHaveBeenCalledWith(opts);
            expect(api.paint.all).toHaveBeenCalledWith(opts);
            expect(api.inventoryItem.all).toHaveBeenCalledWith(opts);
            expect(api.appliance.all).toHaveBeenCalledWith(opts);
            expect(api.permit.all).toHaveBeenCalledWith(opts);
            expect(api.binder.get).toHaveBeenCalledWith(1);
            expect(api.binderContractor.all).toHaveBeenCalledWith({binderId: 1});
            expect(api.document.all).toHaveBeenCalledWith({binder_id: 1, document_type_id: 1});
        });
        
        it("sets up preview when cfg.previewMode is true", function() {
            createController();
            
            controller.model.cfg = {
                previewMode: true
            };
            controller.model.refresh();

            for (var section in controller.model.sections) {
                expect(controller.model.sections[section].complete).toBe(true);
            }
        });
    });
    
    describe("onRefreshSuccess", function() {
        it("sums the score when all points are 0 (default initalization)", function() {
            createController();
            
            spyOn(controller.model.state, "idle");
            
            controller.model.onRefreshSuccess();

            expect(controller.model.score).toBe(0);
            expect(controller.model.percentComplete).toBe(0);
            expect(controller.model.state.idle).toHaveBeenCalled();
        });
        
        it("sums the score when all points are 1", function() {
            createController();
            
            spyOn(controller.model.state, "idle");
            
            var points = 0;
            controller.model.inspectionReportPoints = 1;
            controller.model.inspectorProsPoints = 1;
            points += 2;
            
            for (var section in controller.model.sections) {
                controller.model.sections[section].points = 1;
                points++;
            }
            
            controller.model.maxScore = 100;
            controller.model.onRefreshSuccess();

            expect(controller.model.score).toBe(points);
            expect(controller.model.percentComplete).toBe(Math.round((points / 100) * 100));
            expect(controller.model.state.idle).toHaveBeenCalled();
        });
        
        it("sums the score and rounds to the nearest whole number", function() {
            createController();
            
            spyOn(controller.model.state, "idle");
            
            // 50/119 = 0.42016806722
            controller.model.inspectionReportPoints = 50;
            controller.model.maxScore = 119;
            
            controller.model.onRefreshSuccess();

            expect(controller.model.score).toBe(50);
            expect(controller.model.percentComplete).toBe(42);
            expect(controller.model.state.idle).toHaveBeenCalled();
        });
    });
    
    describe("getComponentSuccess", function() {
        it("sets this section's points when less than maxPoints", function() {
            createController();
    
            var section = "maintenanceItems";
            controller.model.sections[section].pointsPerItem = 2;
            controller.model.sections[section].maxPoints = 10;
            
            var response = {
                data: {
                    items: [1, 2, 3]
                }
            };
            
            controller.model.getComponentSuccess(section, response);

            expect(controller.model.sections[section].points).toBe(6);
            expect(controller.model.sections[section].complete).toBe(false);
        });
        
        it("sets this section's points to maxPoints when greater than max points, and sets complete to true", function() {
            createController();
    
            var section = "maintenanceItems";
            controller.model.sections[section].pointsPerItem = 2;
            controller.model.sections[section].maxPoints = 10;
            
            var response = {
                data: {
                    items: [1, 2, 3, 4, 5, 6, 7]
                }
            };
            
            controller.model.getComponentSuccess(section, response);

            expect(controller.model.sections[section].points).toBe(10);
            expect(controller.model.sections[section].complete).toBe(true);
        });
        
        it("only counts appliances with model and serial numbers (none)", function() {
            createController();
    
            var section = "appliances";
            controller.model.sections[section].pointsPerItem = 1;
            controller.model.sections[section].maxPoints = 10;
            
            var response = {
                data: {
                    items: [
                        {id: 1},
                        {id: 2},
                        {id: 3}
                    ]
                }
            };
            
            controller.model.getComponentSuccess(section, response);

            expect(controller.model.sections[section].points).toBe(0);
            expect(controller.model.sections[section].complete).toBe(false);
        });
        
        it("only counts appliances with model and serial numbers (mixed)", function() {
            createController();
    
            var section = "appliances";
            controller.model.sections[section].pointsPerItem = 1;
            controller.model.sections[section].maxPoints = 10;
            
            var response = {
                data: {
                    items: [
                        {model: 1, serial_no: 1},
                        {model: 2, serial_no: 2},
                        {id: 1},
                        {id: 2},
                        {id: 3}
                    ]
                }
            };
            
            controller.model.getComponentSuccess(section, response);

            expect(controller.model.sections[section].points).toBe(2);
            expect(controller.model.sections[section].complete).toBe(false);
        });

        it("only counts appliances with model and serial numbers (mixed order)", function() {
            createController();
    
            var section = "appliances";
            controller.model.sections[section].pointsPerItem = 1;
            controller.model.sections[section].maxPoints = 10;
            
            var response = {
                data: {
                    items: [
                        {model: 1, serial_no: 1},
                        {model: 2, serial_no: 2},
                        {id: 1},
                        {id: 2},
                        {model: 3, serial_no: 3},
                        {id: 3},
                        {model: 4, serial_no: 4}
                    ]
                }
            };
            
            controller.model.getComponentSuccess(section, response);

            expect(controller.model.sections[section].points).toBe(4);
            expect(controller.model.sections[section].complete).toBe(false);
        });
        
        it("only counts appliances with model and serial numbers (all)", function() {
            createController();
    
            var section = "appliances";
            controller.model.sections[section].pointsPerItem = 1;
            controller.model.sections[section].maxPoints = 10;
            
            var response = {
                data: {
                    items: [
                        {model: 1, serial_no: 1},
                        {model: 2, serial_no: 2}
                    ]
                }
            };
            
            controller.model.getComponentSuccess(section, response);

            expect(controller.model.sections[section].points).toBe(2);
            expect(controller.model.sections[section].complete).toBe(false);
        });
    });
    
    describe("getDetailSuccess", function() {
        it("does not yield any points due to missing price and date", function() {
            createController();
            
            var response = {
                data: {
                    property: {
                        
                    }
                }
            };
            
            controller.model.sections["homePriceDetail"].maxPoints = 10;
            controller.model.sections["homeDateDetail"].maxPoints = 10;
            controller.model.getDetailSuccess(response);

            expect(controller.model.sections["homePriceDetail"].points).toBe(0);
            expect(controller.model.sections["homePriceDetail"].complete).toBe(false);
            
            expect(controller.model.sections["homeDateDetail"].points).toBe(0);
            expect(controller.model.sections["homeDateDetail"].complete).toBe(false);
        });
        
        it("does not yield any points due to null price and date", function() {
            createController();
            
            var response = {
                data: {
                    property: {
                        purchase_price: null,
                        purchase_date: null
                    }
                }
            };
            
            controller.model.sections["homePriceDetail"].maxPoints = 10;
            controller.model.sections["homeDateDetail"].maxPoints = 10;
            controller.model.getDetailSuccess(response);

            expect(controller.model.sections["homePriceDetail"].points).toBe(0);
            expect(controller.model.sections["homePriceDetail"].complete).toBe(false);
            
            expect(controller.model.sections["homeDateDetail"].points).toBe(0);
            expect(controller.model.sections["homeDateDetail"].complete).toBe(false);
        });
        
        it("does yields points and sets sections to complete", function() {
            createController();
            
            var response = {
                data: {
                    property: {
                        purchase_price: 1,
                        purchase_date: 1
                    }
                }
            };
            
            controller.model.sections["homePriceDetail"].maxPoints = 10;
            controller.model.sections["homeDateDetail"].maxPoints = 10;
            controller.model.getDetailSuccess(response);

            expect(controller.model.sections["homePriceDetail"].points).toBe(10);
            expect(controller.model.sections["homePriceDetail"].complete).toBe(true);
            
            expect(controller.model.sections["homeDateDetail"].points).toBe(10);
            expect(controller.model.sections["homeDateDetail"].complete).toBe(true);
        });
    });
    
    describe("getHomeProSuccess", function() {
        it("correctly counts the number of home pros added by the homeowner", function() {
            createController();
            
            controller.model.sections["homePros"].pointsPerItem = 2;
            controller.model.sections["homePros"].maxPoints = 10;
            var homePro = {contractor: {created_by: 1}};
            var response = {
                data: {
                    items: [ 
                        homePro,
                        homePro,
                        homePro
                    ]
                }
            };
            
            controller.model.getHomeProSuccess(response);

            expect(controller.model.sections["homePros"].points).toBe(6);
            expect(controller.model.sections["homePros"].complete).toBe(false);
            expect(controller.model.inspectorProsPoints).toBe(0);
        });
        
        it("correctly counts the number of home pros added by the homeowner up to maxPoints", function() {
            createController();
            
            controller.model.sections["homePros"].pointsPerItem = 5;
            controller.model.sections["homePros"].maxPoints = 10;
            var homePro = {contractor: {created_by: 1}};
            var response = {
                data: {
                    items: [ 
                        homePro,
                        homePro,
                        homePro
                    ]
                }
            };
            
            controller.model.getHomeProSuccess(response);

            expect(controller.model.sections["homePros"].points).toBe(10);
            expect(controller.model.sections["homePros"].complete).toBe(true);
            expect(controller.model.inspectorProsPoints).toBe(0);
        });
        
        it("correctly counts the number of home pros added by the inspector", function() {
            createController();
            
            controller.model.sections["homePros"].pointsPerItem = 2;
            controller.model.sections["homePros"].maxPoints = 10;
            var homePro = {contractor: {created_by: null}};
            var response = {
                data: {
                    items: [
                        homePro,
                        homePro,
                        homePro
                    ]
                }
            };
            
            controller.model.getHomeProSuccess(response);

            expect(controller.model.sections["homePros"].points).toBe(0);
            expect(controller.model.sections["homePros"].complete).toBe(false);
            expect(controller.model.inspectorProsPoints).toBe(3);
        });
        
        it("correctly counts the number of home pros added by the inspector up to 10", function() {
            createController();
            
            controller.model.sections["homePros"].pointsPerItem = 2;
            controller.model.sections["homePros"].maxPoints = 10;
            var homePro = {contractor: {created_by: null}};
            var response = {
                data: {
                    items: [
                        homePro,
                        homePro,
                        homePro,
                        homePro,
                        homePro,
                        homePro,
                        homePro,
                        homePro,
                        homePro,
                        homePro,
                        homePro
                    ]
                }
            };
            
            controller.model.getHomeProSuccess(response);

            expect(controller.model.sections["homePros"].points).toBe(0);
            expect(controller.model.sections["homePros"].complete).toBe(false);
            expect(controller.model.inspectorProsPoints).toBe(10);
        });
        
        it("correctly counts points for homeowner and inspector when mixed", function() {
            createController();
            
            controller.model.sections["homePros"].pointsPerItem = 2;
            controller.model.sections["homePros"].maxPoints = 10;
            var homeownerHomePro = {contractor: {created_by: 1}};
            var inspectorHomePro = {contractor: {created_by: null}};
            var response = {
                data: {
                    items: [
                        homeownerHomePro,
                        inspectorHomePro,
                        inspectorHomePro,
                        homeownerHomePro,
                        homeownerHomePro
                    ]
                }
            };
            
            controller.model.getHomeProSuccess(response);

            expect(controller.model.sections["homePros"].points).toBe(6);
            expect(controller.model.sections["homePros"].complete).toBe(false);
            expect(controller.model.inspectorProsPoints).toBe(2);
        });
    });
    
    describe("getReportSuccess", function() {
        it("does not give any points for a missing inspection report", function() {
            createController();
        
            var response = {
                data: {
                    items: []
                }
            };
            
            controller.model.getReportSuccess(response);

            expect(controller.model.inspectionReportPoints).toBe(0);
        });
        
        it("gives points for a present inspection report", function() {
            createController();
        
            var response = {
                data: {
                    items: [{document_type_id: 1}]
                }
            };
            
            controller.model.getReportSuccess(response);

            expect(controller.model.inspectionReportPoints).toBe(10);
        });
    });
    
    describe("getComponentError", function() {
        it("calls $log.error, notify.error, and state.error", function() {
            createController();
            
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(controller.model.state, "error");
        
            var response = {
                data: "error"
            };
            
            controller.model.getComponentError(response);

            expect($log.error).toHaveBeenCalledWith({data: "error"});
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(controller.model.state.error).toHaveBeenCalled();
        });
    });
});

describe('hbBinderCompletenessWidget', function() {
    var $scope, $compile, api, session, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        session = _$injector_.get("Session");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(session, "getUser").and.returnValue({ id: 1 });
        spyOn(api.document, "all").and.returnValue($q.when({ data: { items: [{id: 1}] } }));

        $scope.cfg = {};

        $compile('<hb-binder-completeness-widget cfg="cfg"></hb-binder-completeness-widget>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls api document all', function() {
            expect(api.document.all).toHaveBeenCalled();
        });
    });
});