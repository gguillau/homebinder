angular
    .module('hb.widgets.library.general.binderCompleteness', [
        'hb.widgets.widgetProvider'
    ])
    .config(['hbWidgetProvider', function(widgetProvider) {
        widgetProvider.register('binderCompletenessWidget', {
            name: 'Binder Completeness Widget',
            key: 'binderCompletenessWidget',
            description: 'Give homeowners a gauge to see how complete their binder is and motivate completion through links to binder sections',
            category: 'Binder.BC',
            template: '<hb-binder-completeness-widget></hb-binder-completeness-widget>',
            directive: 'hb-binder-completeness-widget',
            width: 'md',
            height: 'sm'
        });
    }])
    .directive('hbBinderCompletenessWidget', function() {
        return {
            restrict: "E",
            templateUrl: "widgets/library/general/binderCompleteness/binderCompleteness.tpl.html",
            controller: "BinderCompletenessWidgetController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {
                cfg: '='
            }
        };
    })
    .controller('BinderCompletenessWidgetController', [
        '$log',
        '$q',
        '$stateParams',
        'hb.api',
        'widgetState',
        'hb.resources',
        '$window',
        'Notify',
        'Session',
        '$sce',
        'Event',
        'HBWidgetBase',
        function($log, $q, $stateParams, api, widgetState, resources, $window, Notify, Session, $sce, event, HBWidgetBase) {
            var Controller = function(cfg) {
                HBWidgetBase.call(this);
                this.cfg = cfg;
                this.widget = this.cfg.widget;
                this.binderId = $stateParams.binderId;
                this.state = new widgetState();
                this.resources = resources.binderCompletenessWidget;
                this.user = Session.getUser();
                this.api = api;
                this.notify = Notify;
                this.$log = $log;
                this.$q = $q;

                // These sections correspond to the links that will appear to encourage a homeowner to complete their binder
                this.sections = {
                    maintenanceItems: {
                        complete: false,
                        link: "maintenanceItems",
                        text: this.resources.addMaintenanceItem,
                        pointsPerItem: 1,
                        maxPoints: 10,
                        points: 0
                    },
                    projects: {
                        complete: false,
                        link: "projects",
                        text: this.resources.addProject,
                        pointsPerItem: 2,
                        maxPoints: 10,
                        points: 0
                    },
                    homePros: {
                        complete: false,
                        link: "binder_contractors",
                        text: this.resources.addHomePro,
                        pointsPerItem: 2,
                        maxPoints: 10,
                        points: 0
                    },
                    documents: {
                        complete: false,
                        link: "documents",
                        text: this.resources.addDocument,
                        pointsPerItem: 1,
                        maxPoints: 10,
                        points: 0
                    },
                    rooms: {
                        complete: false,
                        link: "areas",
                        text: this.resources.addRoom,
                        pointsPerItem: 2,
                        maxPoints: 10,
                        points: 0
                    },
                    paints: {
                        complete: false,
                        link: "paints",
                        text: this.resources.addPaint,
                        pointsPerItem: 3,
                        maxPoints: 9,
                        points: 0
                    },
                    inventoryItems: {
                        complete: false,
                        link: "inventory_items",
                        text: this.resources.addInventoryItem,
                        pointsPerItem: 1,
                        maxPoints: 10,
                        points: 0
                    },
                    homePriceDetail: {
                        complete: false,
                        link: "edit",
                        text: this.resources.addHomePriceDetail,
                        pointsPerItem: 5,
                        maxPoints: 5,
                        points: 0
                    },
                    homeDateDetail: {
                        complete: false,
                        link: "edit",
                        text: this.resources.addHomeDateDetail,
                        pointsPerItem: 5,
                        maxPoints: 5,
                        points: 0
                    },
                    appliances: {
                        complete: false,
                        link: "appliances",
                        text: this.resources.addAppliances,
                        pointsPerItem: 2,
                        maxPoints: 10,
                        points: 0
                    },
                    permits: {
                        complete: false,
                        link: "permits",
                        text: this.resources.addPermits,
                        pointsPerItem: 10,
                        maxPoints: 10,
                        points: 0
                    }
                };
                
                this.score = 0;                     // Total score which represents a binder's completeness
                this.maxScore = 119;                // Max score as specified by Riley's Binder Completeness Scoring
                this.inspectionReportPoints = 0;    // Points for having an identified inspection report
                this.inspectorProsPoints = 0;       // Points for having home pros added by inspectors

                this.refresh();
            };

            Controller.prototype = Object.create(HBWidgetBase.prototype);

            angular.extend(Controller.prototype, {

                refresh: function() {
                    if (!this.cfg || !this.cfg.previewMode) {
                        this.state.busy();
                        var promises = [];
                        var opts = { binderId: this.binderId, count: 10 };

                        promises.push(
                            this.api.maintenanceItem.all(opts).then(
                                angular.bind(this, this.getComponentSuccess, "maintenanceItems"),
                                angular.bind(this, this.getComponentError)),

                            this.api.project.all(opts).then(
                                angular.bind(this, this.getComponentSuccess, "projects"),
                                angular.bind(this, this.getComponentError)),

                            this.api.document.all(opts).then(
                                angular.bind(this, this.getComponentSuccess, "documents"),
                                angular.bind(this, this.getComponentError)),

                            this.api.area.all(opts).then(
                                angular.bind(this, this.getComponentSuccess, "rooms"),
                                angular.bind(this, this.getComponentError)),

                            this.api.paint.all(opts).then(
                                angular.bind(this, this.getComponentSuccess, "paints"),
                                angular.bind(this, this.getComponentError)),

                            this.api.inventoryItem.all(opts).then(
                                angular.bind(this, this.getComponentSuccess, "inventoryItems"),
                                angular.bind(this, this.getComponentError)),

                            this.api.appliance.all(opts).then(
                                angular.bind(this, this.getComponentSuccess, "appliances"),
                                angular.bind(this, this.getComponentError)),

                            this.api.permit.all(opts).then(
                                angular.bind(this, this.getComponentSuccess, "permits"),
                                angular.bind(this, this.getComponentError)),
                                
                            this.api.binder.get(this.binderId).then(
                                angular.bind(this, this.getDetailSuccess),
                                angular.bind(this, this.getComponentError)),
                                
                            this.api.binderContractor.all({binderId: this.binderId}).then(
                                angular.bind(this, this.getHomeProSuccess),
                                angular.bind(this, this.getComponentError)),
                                
                            this.api.document.all({binder_id: this.binderId, document_type_id: 1}).then(
                                angular.bind(this, this.getReportSuccess),
                                angular.bind(this, this.getComponentError))
                        );

                        this.$q.all(promises).then(
                            angular.bind(this, this.onRefreshSuccess)
                        );
                    }
                    // Setup preview
                    else {
                        for (var section in this.sections) {
                            this.sections[section].complete = true;
                        }
                    }
                },

                // Refresh success, update the total score
                onRefreshSuccess: function() {
                    // Add up points from each section
                    for (var section in this.sections) {
                        this.score += this.sections[section].points;
                    }
                    // Add points for having an inspection report and home pros added by inspector
                    this.score += (this.inspectionReportPoints + this.inspectorProsPoints);
                    this.percentComplete = Math.round((this.score / this.maxScore) * 100);
                    this.state.idle();
                },

                // Get component success
                getComponentSuccess: function(section, response) {
                    var items = response.data.items;
                    var pointsPerItem = this.sections[section].pointsPerItem;
                    var maxPoints = this.sections[section].maxPoints;
                    var points = 0;
                        
                    // Only retrieve appliances with model and serial numbers
                    if (section === "appliances") {
                        for (var i = 0; i < items.length; i++) {
                            if (!items[i].model || !items[i].serial_no) {
                                items.splice(i, 1);
                                i--;
                            }
                        }
                    }

                    // Calculate section's points
                    points = items.length * pointsPerItem;

                    if (points >= maxPoints) {
                        this.sections[section].points = maxPoints;
                        this.sections[section].complete = true;
                    }
                    else {
                        this.sections[section].points = points;
                    }
                },
                
                // Purchase price and purchase date use the same API call to binder, so we handle both cases here
                getDetailSuccess: function(response) {
                    var binder = response.data;
                    var property = binder.property;

                    if (property.purchase_price) {
                        this.sections["homePriceDetail"].points = this.sections["homePriceDetail"].maxPoints;
                        this.sections["homePriceDetail"].complete = true;
                    }

                    if (property.purchase_date) {
                        this.sections["homeDateDetail"].points = this.sections["homeDateDetail"].maxPoints;
                        this.sections["homeDateDetail"].complete = true;
                    }
                },
                
                // Home Pros can be added by inspectors or home owners, so we handle both cases here
                getHomeProSuccess: function(response) {
                    var homePros = response.data.items;
                    var pointsPerItem = this.sections["homePros"].pointsPerItem;
                    var maxPoints = this.sections["homePros"].maxPoints;
                    var numHomeownerPros = 0;
                    var numInspectorPros = 0;
                    var points = 0;
                    
                    // Count number of home pros added by inspector and homeowner
                    for (var i = 0; i < homePros.length; i++) {
                        if (homePros[i].contractor.created_by === this.user.id) {
                            numHomeownerPros++;
                        }
                        else {
                            numInspectorPros++;
                        }
                    }
                    
                    // Calculate section's points
                    points = numHomeownerPros * pointsPerItem;
                    if (points >= maxPoints) {
                        this.sections["homePros"].points = maxPoints;
                        this.sections["homePros"].complete = true;
                    }
                    else {
                        this.sections["homePros"].points = points;
                    }
                    
                    // Calculate points from inspector's additions (using values from Riley's scoring criteria)
                    if (numInspectorPros >= 10) {
                        this.inspectorProsPoints = 10;
                    }
                    else {
                        this.inspectorProsPoints = numInspectorPros;
                    }
                    
                },
                
                // Retrieve inspection report if it exists and assign points accordingly
                getReportSuccess: function(response) {
                    if (response.data.items[0]) {
                        this.inspectionReportPoints = 10;
                    }
                },

                // Get component error
                getComponentError: function(response) {
                    this.$log.error(response);
                    this.notify.error(response.data);
                    this.state.error();
                }
            });

            this.model = new Controller(this.cfg);
        }
    ]);