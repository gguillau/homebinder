angular
    .module('hb.widgets.library.general.apr', [
        'hb.widgets.widgetProvider'
    ])
    .config(['hbWidgetProvider', function(widgetProvider) {
        widgetProvider.register('apr', {
            name: 'APR Widget',
            key: 'apr',
            description: 'Displays information about an APR',
            category: 'Binder.APR',
            template: '<hb-apr-widget></hb-apr-widget>',
            directive: 'hb-apr-widget',
            width: 'md',
            height: 'sm'
        });
    }])
    .directive('hbAprWidget', function() {
        return {
            restrict: "E",
            templateUrl: "widgets/library/general/apr/apr.tpl.html",
            controller: "AprWidgetController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {
                cfg: '='
            }
        };
    })
    .controller('AprWidgetController', [
        '$log',
        '$stateParams',
        'hb.api',
        'widgetState',
        'hb.resources',
        '$location',
        '$window',
        'Notify',
        'HBWidgetBase',
        'Event',
        function($log, $stateParams, api, widgetState, resources, $location, $window, Notify, HBWidgetBase, Event) {
            var Controller = function(cfg) {
                HBWidgetBase.call(this);
                this.cfg = cfg;
                this.widget = this.cfg.widget;
                this.binderId = $stateParams.binderId;
                this.homePros = [];
                this.state = new widgetState();
                this.resources = resources.aprWidget;
            };

            Controller.prototype = Object.create(HBWidgetBase.prototype);

            angular.extend(Controller.prototype, {

                learnMore: function() {
                    api.binder.learnMoreApr(this.binderId).then(
                        angular.bind(this, this.learnMoreSuccess),
                        angular.bind(this, this.learnMoreError));
                },

                learnMoreSuccess: function(response) {
                    Event.create({ event_name: "apr_widget_clicked", event_type: "engagement", user_id: this.user.id, user_role: this.user.role, user_created_at: this.user.created_at, user_create_method: this.user.create_method });
                },

                learnMoreError: function(response) {
                    $log.error(response.data);
                }
            });

            this.model = new Controller(this.cfg);
        }
    ]);