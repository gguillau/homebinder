describe("AprWidgetController", function() {
    var $controllerService,
        controller,
        $rootScope,
        $q,
        $log,
        $stateParams,
        api,
        widgetState,
        resources,
        $location,
        $window,
        Notify;

    // load the homebinder module
    beforeEach(module("homebinder"));

    // create the controller
    beforeEach(inject(function($controller, $injector) {
        $controllerService = $controller;
        $rootScope = $injector.get("$rootScope");
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        $stateParams = $injector.get("$stateParams");
        $stateParams.binderId = 1;
        api = $injector.get("hb.api");
        widgetState = $injector.get("widgetState");
        resources = $injector.get("hb.resources");
        $location = $injector.get("$location");
        $window = $injector.get("$window");
        Notify = $injector.get("Notify");
    }));

    function createController() {
        controller = $controllerService("AprWidgetController", {
            "log": $log,
            "$stateParams": $stateParams,
            "hb.api": api,
            "widgetState": widgetState,
            "hb.resources": resources,
            "$location": $location,
            "$window": $window,
            "Notify": Notify
        }, { cfg: {} });
    }

    describe("learnMore", function() {
        it("calls hb.api.binder.learnMoreApr", function() {
            spyOn(api.binder, "learnMoreApr").and.returnValue($q.when({ data: "success" }));

            createController();
            controller.model.learnMore();

            $rootScope.$apply();

            expect(api.binder.learnMoreApr).toHaveBeenCalledWith(1);
        });

        it("handles an error", function() {
            spyOn(api.binder, "learnMoreApr").and.returnValue($q.reject({ data: "error" }));
            spyOn($log, "error");

            createController();
            controller.model.learnMore();

            $rootScope.$apply();

            expect($log.error).toHaveBeenCalledWith("error");
        });
    });
});

describe('hbAprWidget', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = {};

        element = $compile('<hb-apr-widget cfg="cfg"></hb-apr-widget>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("When was the last time a professional reviewed your home?");
        });
    });
});