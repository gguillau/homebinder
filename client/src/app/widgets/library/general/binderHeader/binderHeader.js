angular
    .module('hb.widgets.library.general.binderHeader', [
        'hb.widgets.library.general.binderHeader.block'
    ])
    .factory('HBBinderHeader', [
        '$log',
        '$stateParams',
        'hb.api',
        'widgetState',
        function($log, $stateParams, api, widgetState) {
            var Header = function() {
                this.binderId = $stateParams.binderId;
                this.binder = null;
                this.getImage = true;
                this.showHeroImage = false;
                this.state = new widgetState();
            };

            angular.extend(Header.prototype, {
                refresh: function() {
                    if (this.cfg && this.cfg.previewMode) {
                        this.binder = {
                            name: "My Binder",
                            property: {
                                address1: "1 Main St",
                                city: "Boston",
                                state: "MA"
                            },
                            image: ""
                        };
                        this.showHeroImage = true;
                    }
                    else {
                        this.state.busy();
                        api.binder.get(this.binderId).then(
                            angular.bind(this, this.refreshDone),
                            angular.bind(this, this.refreshError)
                        );
                    }
                },
                refreshDone: function(response) {
                    this.binder = response.data;
                    if (this.getImage) {
                        this.loadImage();
                    }
                    else {
                        this.state.idle();
                    }
                },
                refreshError: function(error) {
                    $log.error(error.data);
                    this.state.error();
                },
                loadImage: function() {
                    if (this.binder.hero_image_id) {
                        api.image.get(this.binder.hero_image_id).then(
                            angular.bind(this, this.loadImageSuccess),
                            angular.bind(this, this.loadImageError));
                    }
                    else {
                        this.state.idle();
                    }
                },

                loadImageSuccess: function(response) {
                    this.binder.image = response.data;
                    this.showHeroImage = true;
                    this.state.idle();
                },

                loadImageError: function(error) {
                    $log.error(error.data);
                    this.state.error();
                }
            });

            return Header;
        }
    ]);