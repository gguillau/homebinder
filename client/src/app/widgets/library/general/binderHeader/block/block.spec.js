describe("BinderHeaderBlockController", function() {
    var $controllerService,
        $q,
        $log,
        $stateParams,
        api,
        widgetState,
        resources,
        notify,
        controller,
        events;

    // load the homebinder module
    beforeEach(module("homebinder"));

    // create the controller
    beforeEach(inject(function($controller, $injector) {
        $controllerService = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        $stateParams = $injector.get("$stateParams");
        $stateParams.binderId = 1;
        api = $injector.get("hb.api");
        widgetState = $injector.get("widgetState");
        resources = $injector.get("hb.resources");
        notify = $injector.get("Notify");
        session = $injector.get("Session");
        context = $injector.get("Context");
    }));

    function createController() {
        controller = $controllerService("BinderHeaderBlockController", {
            "log": $log,
            "$stateParams": $stateParams,
            "hb.api": api,
            "widgetState": widgetState,
            "hb.resources": resources,
            "Notify": notify
        });
    }

    describe("init", function() {
        it("sets the controller", function() {
            createController();
            expect(controller.model.showHeroImage).toEqual(false);
        });
    });

});

describe('hbBinderHeaderBlockWidget', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = { previewMode: true };

        element = $compile('<hb-binder-header-block-widget cfg="cfg"></hb-binder-header-block-widget>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("My Binder");
        });
    });
});