angular
    .module('hb.widgets.library.general.binderHeader.block', [
        'hb.widgets.widgetProvider'
    ])
    .config(['hbWidgetProvider', function(widgetProvider){
        widgetProvider.register('binderHeaderBlock', {
            name: 'Binder Header Block',
            key: 'binderHeaderBlock',
            description: 'Displays the address and image or location of the property on a map.',
            category: 'Binder.Overview',
            template: '<hb-binder-header-block-widget></hb-binder-header-block-widget>',
            directive: 'hb-binder-header-block-widget',
            width: 'md',
            height: 'lg'
        });
    }])
    .directive("hbBinderHeaderBlockWidget", function() {
        return {
            restrict: "E",
            templateUrl: "widgets/library/general/binderHeader/block/block.tpl.html",
            controller: "BinderHeaderBlockController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {
                cfg:'='
            }
        };
    })
    .controller('BinderHeaderBlockController', [
        'HBBinderHeader',
        'hb.resources',
        function(BinderHeader, resources) {
            var that = this;
            var Controller = function() {    
                BinderHeader.call(this);
               
                this.cfg = that.cfg;
                this.resources = resources.widgetBinderHeaderBlock;
               
                this.refresh();
            };
            
            Controller.prototype = Object.create(BinderHeader.prototype);

            this.model = new Controller();
        }
    ]);