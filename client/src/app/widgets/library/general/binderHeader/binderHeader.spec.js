describe("BinderHeaderFactory", function(){
    var binderHeader,
        $rootScope,
        $q,
        $log,
        $stateParams,
        api,
        widgetState;
        
    // load the homebinder module
    beforeEach(module("homebinder"));

    // init stuff
    beforeEach(inject(function($injector) {
        binderHeader = $injector.get("HBBinderHeader");
        $rootScope = $injector.get("$rootScope");
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        $stateParams = $injector.get("$stateParams");
        $stateParams.binderId = 1;
        api = $injector.get("hb.api");
        widgetState = $injector.get("widgetState");
    }));

    describe("refresh", function() {
        it("gets the binder only", function() {
            spyOn(api.binder, "get").and.returnValue($q.when({ data:
                {id: 1, name: "binder"}
            }));
            spyOn(api.image, "get");
            
            var header = new binderHeader($log, $stateParams, api, widgetState);
            header.getImage = false;
            header.refresh();
            
            $rootScope.$apply();
            
            expect(api.binder.get).toHaveBeenCalledWith(1);
            expect(api.image.get).not.toHaveBeenCalled();
            expect(header.binder).not.toBeNull();
            expect(header.state.isIdle()).toEqual(true);
        });
        
        it("gets the binder and image", function() {
            spyOn(api.binder, "get").and.returnValue($q.when({ data:
                {id: 1, name: "binder", hero_image_id: 1}
            }));
            spyOn(api.image, "get").and.returnValue($q.when({ data: { id: 1, name: "image" }}));
            
            var header = new binderHeader($log, $stateParams, api, widgetState);
            header.refresh();
            
            $rootScope.$apply();
            
            expect(api.binder.get).toHaveBeenCalledWith(1);
            expect(api.image.get).toHaveBeenCalledWith(1);
            expect(header.binder).not.toBeNull();
            expect(header.binder.image).not.toBeNull();
            expect(header.showHeroImage).toEqual(true);
            expect(header.state.isIdle()).toEqual(true);
        });
        
        it("handles get binder error", function() {
            spyOn(api.binder, "get").and.returnValue($q.reject({ data: "error" }));
            spyOn(api.image, "get");
            spyOn($log, "error");
            
            var header = new binderHeader($log, $stateParams, api, widgetState);
            header.refresh();
            
            $rootScope.$apply();
            
            expect($log.error).toHaveBeenCalledWith("error");
            expect(header.state.isError()).toEqual(true);
            expect(api.image.get).not.toHaveBeenCalled();
        });
        
        it("handles get image error", function() {
            spyOn(api.binder, "get").and.returnValue($q.when({ data:
                {id: 1, name: "binder", hero_image_id: 1}
            }));
            spyOn(api.image, "get").and.returnValue($q.reject({ data: "error" }));
            spyOn($log, "error");
            
            var header = new binderHeader($log, $stateParams, api, widgetState);
            header.refresh();
            
            $rootScope.$apply();
            
            expect($log.error).toHaveBeenCalledWith("error");
            expect(header.state.isError()).toEqual(true);
        });
    });
    
    describe("loadImage", function() {
        it("calls state idle", function() {
            var header = new binderHeader($log, $stateParams, api, widgetState);
            spyOn(header.state, "idle");
            header.binder = {hero_image_id: null};
            header.loadImage();
            
            expect(header.state.isIdle()).toEqual(true);
        });
    });
});