angular
    .module('hb.widgets.library.general.topHomePros', [
        'hb.widgets.widgetProvider'
    ])
    .config(['hbWidgetProvider', function(widgetProvider) {
        widgetProvider.register('topHomePros', {
            name: 'Top Home Pros',
            key: 'topHomePros',
            description: 'Displays the top 5 home pros.',
            category: 'Binder.Home Professionals',
            template: '<hb-top-home-pros-widget></hb-top-home-pros-widget>',
            directive: 'hb-top-home-pros-widget',
            width: 'md',
            height: 'sm'
        });
    }])
    .directive('hbTopHomeProsWidget', function() {
        return {
            restrict: "E",
            templateUrl: "widgets/library/general/topHomePros/topHomePros.tpl.html",
            controller: "TopHomeProsWidgetController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {
                cfg: '='
            }
        };
    })
    .controller('TopHomeProsWidgetController', [
        '$log',
        '$stateParams',
        'hb.api',
        'widgetState',
        'hb.resources',
        function($log, $stateParams, api, widgetState, resources) {
            var Controller = function(cfg) {
                this.cfg = cfg;
                this.binderId = $stateParams.binderId;
                this.homePros = [];
                this.state = new widgetState();
                this.resources = resources.widgetTopHomePros;

                this.refresh();
            };

            angular.extend(Controller.prototype, {
                refresh: function() {
                    if (this.cfg && this.cfg.previewMode) {
                        this.homePros = [
                            { contractor: { name: "Plumber", contractor_type: "Plumbing", phone: "555-555-1234" } },
                            { contractor: { name: "Electrician", contractor_type: "Electrical", email: "electrician@electrical.com" } }
                        ];
                    }
                    else {

                        this.state.busy();
                        var opts = {
                            page: 1,
                            count: 10,
                            binderId: this.binderId,
                            orderBy: "binder_contractors.contact"
                        };
                        api.binderContractor.all(opts).then(
                            angular.bind(this, this.refreshComplete),
                            angular.bind(this, this.refreshError));
                    }
                },
                refreshComplete: function(response) {
                    this.homePros = response.data.items;
                    this.state.idle();
                },
                refreshError: function(error) {
                    $log.error(error.data);
                    this.state.error();
                }
            });

            this.model = new Controller(this.cfg);
        }
    ]);