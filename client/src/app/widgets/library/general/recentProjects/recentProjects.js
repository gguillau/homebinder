angular
    .module('hb.widgets.library.general.recentProjects', [
        'hb.widgets.widgetProvider'
    ])
    .config(['hbWidgetProvider', function(widgetProvider) {
        widgetProvider.register('recentProjects', {
            name: 'Recent Projects',
            key: 'recentProjects',
            description: 'Displays the 5 most recent projects.',
            category: 'Binder.Projects',
            template: '<hb-recent-projects-widget></hb-recent-projects-widget>',
            directive: 'hb-recent-projects-widget',
            width: 'md',
            height: 'sm'
        });
    }])
    .directive('hbRecentProjectsWidget', function() {
        return {
            restrict: "E",
            templateUrl: "widgets/library/general/recentProjects/recentProjects.tpl.html",
            controller: "RecentProjectsWidgetController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {
                cfg: '='
            }
        };
    })
    .controller('RecentProjectsWidgetController', [
        '$log',
        '$stateParams',
        'hb.api',
        'widgetState',
        'hb.resources',
        function($log, $stateParams, api, widgetState, resources) {
            var Controller = function(cfg) {
                this.cfg = cfg;
                this.binderId = $stateParams.binderId;
                this.projects = [];
                this.state = new widgetState();
                this.resources = resources.widgetRecentProjects;
                this.refresh();
            };

            angular.extend(Controller.prototype, {
                refresh: function() {
                    if (this.cfg && this.cfg.previewMode) {
                        // add some fake projects
                        this.projects = [
                            { name: this.resources.item1 },
                            { name: this.resources.item2 },
                            { name: this.resources.item3 }
                        ];
                    }
                    else {
                        this.state.busy();
                        var opts = {
                            page: 1,
                            count: 10,
                            binderId: this.binderId
                        };
                        api.project.all(opts).then(
                            angular.bind(this, this.refreshComplete),
                            angular.bind(this, this.refreshError));
                    }
                },
                refreshComplete: function(response) {
                    this.projects = response.data.items;
                    this.state.idle();
                },
                refreshError: function(error) {
                    $log.error(error.data);
                    this.state.error();
                }
            });

            this.model = new Controller(this.cfg);
        }
    ]);