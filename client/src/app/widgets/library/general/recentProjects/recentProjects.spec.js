describe("RecentProjectsWidgetController", function() {
    var $controllerService,
        controller,
        base,
        $rootScope,
        $q,
        $log,
        $stateParams,
        api,
        widgetState,
        resources,
        today = new Date();

    // load the homebinder module
    beforeEach(module("homebinder"));

    // create the controller
    beforeEach(inject(function($controller, $injector) {
        $controllerService = $controller;
        $rootScope = $injector.get("$rootScope");
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        $stateParams = $injector.get("$stateParams");
        $stateParams.binderId = 1;
        api = $injector.get("hb.api");
        widgetState = $injector.get("widgetState");
        resources = $injector.get("hb.resources");
    }));

    function createController() {
        controller = $controllerService("RecentProjectsWidgetController", {
            "log": $log,
            "$stateParams": $stateParams,
            "hb.api": api,
            "widgetState": widgetState,
            "hb.resources": resources
        });
    }

    describe("refresh", function() {
        it("calls hb.api.project.get", function() {
            spyOn(api.project, "all").and.returnValue($q.when({
                data: {
                    items: [
                        { id: 1, name: "name" }
                    ]
                }
            }));

            createController();
            controller.model.refresh();

            $rootScope.$apply();

            expect(api.project.all).toHaveBeenCalledWith({
                page: 1,
                count: 10,
                binderId: 1
            });
            expect(controller.model.projects.length).toEqual(1);
            expect(controller.model.state.isIdle()).toEqual(true);
        });

        it("handles an error", function() {
            spyOn(api.project, "all").and.returnValue($q.reject({ data: "error" }));
            spyOn($log, "error");

            createController();
            controller.model.refresh();

            $rootScope.$apply();

            expect($log.error).toHaveBeenCalledWith("error");
            expect(controller.model.state.isError()).toEqual(true);
        });
    });
});

describe('hbRecentProjectsWidget', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = { previewMode: true };

        element = $compile('<hb-recent-projects-widget cfg="cfg"></hb-recent-projects-widget>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("Renovate Kitchen");
        });
    });
});