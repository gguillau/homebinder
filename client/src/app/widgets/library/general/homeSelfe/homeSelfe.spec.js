describe("HomeSelfeFormWidgetController", function() {
    var $controllerService,
        controller,
        $q,
        $log,
        $stateParams,
        api,
        widgetState,
        resources,
        notify,
        session,
        context,
        event,
        $rootScope;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // create the controller
    beforeEach(inject(function($controller, $injector) {
        $controllerService = $controller;
        $rootScope = $injector.get("$rootScope");
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        $stateParams = $injector.get("$stateParams");
        $stateParams.binderId = 1;
        api = $injector.get("hb.api");
        widgetState = $injector.get("widgetState");
        resources = $injector.get("hb.resources");
        notify = $injector.get("Notify");
        session = $injector.get("Session");
        context = $injector.get("Context");
        event = $injector.get("Event");
        $window = $injector.get("$window");
    }));

    function createController() {
        controller = $controllerService("HomeSelfeFormWidgetController", {
            "log": $log,
            "$stateParams": $stateParams,
            "hb.api": api,
            "widgetState": widgetState,
            "hb.resources": resources,
            "Notify": notify,
            "Event": event
        }, { cfg: {} });
    }

    describe("init", function() {
        it("calls hb.api.user.get", function() {
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1 } }));
            spyOn(session, "getUser").and.returnValue({ id: 1 });
            createController();
            expect(api.user.get).toHaveBeenCalled();
        });
    });

    describe("getUserSuccess", function() {
        it("sets the user", function() {
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1 } }));
            spyOn(session, "getUser").and.returnValue({ id: 1 });
            spyOn(context, "getBinder").and.returnValue($q.when({ id: 1 }));
            createController();
            controller.model.getUserSuccess({ data: { id: 1 } });
            $rootScope.$apply();
            
            expect(controller.model.user).toEqual({ id: 1 });
            expect(controller.model.binder).toEqual({ id: 1 });
        });
    });

    describe("getUserError", function() {
        it("calls $log", function() {
            spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1 } }));
            spyOn(session, "getUser").and.returnValue({ id: 1 });

            createController();
            spyOn(controller.model.$log, "error");

            controller.model.getUserError({ data: "error" });

            expect(controller.model.$log.error).toHaveBeenCalled();
        });
    });

    describe("requestServices", function() {
        it("calls hb.api.events.create", function() {
            spyOn(event, "create");
            spyOn(session, "getUser").and.returnValue({ id: 1, user_profile_attributes: {} });
            createController();

            controller.model.user = { id: 1, user_profile_attributes: {} };
            controller.model.binder = { id: 1, property: {} };
            controller.model.requestServices();

            expect(event.create).toHaveBeenCalled();
        });
    });
});

describe('hbHomeSelfeFormWidget', function() {
    var $scope, $compile, Context, session, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        Context = _$injector_.get("Context");
        session = _$injector_.get("Session");
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(session, "getUser").and.returnValue({ id: 1 });
        spyOn(api.user, "get").and.returnValue($q.when({ data: { id: 1 } }));
        spyOn(Context, "getBinder").and.returnValue({ data: { access_list: [{ role: "buyer_agent", user: { user_profile_attributes: { branding: { headshot: "headshot" } } } }] } });

        $scope.cfg = {};

        $compile('<hb-home-selfe-form-widget cfg="cfg"></hb-home-selfe-form-widget>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls context getBinder', function() {

            expect(Context.getBinder).toHaveBeenCalled();
        });
    });
});