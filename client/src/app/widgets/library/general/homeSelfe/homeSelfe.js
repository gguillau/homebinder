angular
    .module('hb.widgets.library.general.homeSelfe', [
        'hb.widgets.widgetProvider'
    ])
    .config(['hbWidgetProvider', function(widgetProvider) {
        widgetProvider.register('homeSelfe', {
            name: 'HomeSelfe Form',
            key: 'homeSelfe',
            description: 'Allow homeowners to send their info to homeSelfe',
            category: 'Binder.HomeSelfe',
            template: '<hb-home-selfe-form-widget></hb-home-selfe-form-widget>',
            directive: 'hb-home-selfe-form-widget',
            width: 'md',
            height: 'sm'
        });
    }])
    .directive('hbHomeSelfeFormWidget', function() {
        return {
            restrict: "E",
            templateUrl: "widgets/library/general/homeSelfe/homeSelfe.tpl.html",
            controller: "HomeSelfeFormWidgetController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {
                cfg: '='
            }
        };
    })
    .controller('HomeSelfeFormWidgetController', [
        '$log',
        '$stateParams',
        'hb.api',
        'widgetState',
        'hb.resources',
        'Notify',
        '$filter',
        'Session',
        'Context',
        '$window',
        '$rootScope',
        "Event",
        "HBWidgetBase",
        function($log, $stateParams, api, widgetState, resources, Notify, $filter, Session, Context, $window, $rootScope, event, HBWidgetBase) {
            var Controller = function(cfg) {
                HBWidgetBase.call(this);
                this.cfg = cfg;
                this.widget = this.cfg.widget;
                this.binderId = $stateParams.binderId;
                this.state = new widgetState();
                this.processing = false;
                this.session = Session;
                this.api = api;
                this.context = Context;
                this.$log = $log;
                this.$window = $window;
                this.selfeLink = null;
                this.user = null;
                this.binder = null;
                this.event = event;
                this.init();
            };

            Controller.prototype = Object.create(HBWidgetBase.prototype);

            angular.extend(Controller.prototype, {
                init: function() {
                    var user = this.session.getUser();
                    this.api.user.get(user.id).then(
                        angular.bind(this, this.getUserSuccess),
                        angular.bind(this, this.getUserError));
                },

                getUserSuccess: function(response) {
                    this.user = response.data;
                    this.context.getBinder().then(angular.bind(this, function(binder) {
                        this.binder = binder;
                    }));
                },

                getUserError: function(response) {
                    this.$log.error(response.data);
                },

                requestServices: function() {
                    var link = "http://www.homeselfe.com/amerispec?";
                    link += "firstname=" + this.user.user_profile_attributes.first_name + "&";
                    link += "lastname=" + this.user.user_profile_attributes.last_name + "&";
                    link += "emailid=" + this.user.email + "&";
                    link += "zip1=" + this.binder.property.zip + "&";
                    link += "add1=" + this.binder.property.address1 + "&";
                    link += "add2=" + this.binder.property.address2 + "&";
                    link += "city=" + this.binder.property.city;

                    this.$window.open(link, "_blank");
                    this.event.create({ event_name: "selfe_widget_clicked", event_type: "widget" });
                    this.event.create({ event_name: "selfe_widget_clicked", event_type: "engagement", user_id: this.user.id, user_role: this.user.role, user_created_at: this.user.created_at, user_create_method: this.user.create_method });
                }
            });
            this.model = new Controller(this.cfg);
        }
    ]);