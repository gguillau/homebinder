describe("DoubleBusinessCardWidgetController", function() {
    var $controllerService,
        ctrl,
        $rootScope,
        $stateParams,
        $log,
        api,
        widgetState,
        resources,
        Context,
        $filter,
        Notify,
        Event,
        Session,
        user,
        $q;

    // load the homebinder module
    beforeEach(module("homebinder"));

    // create the controller
    beforeEach(inject(function($controller, $injector) {
        $controllerService = $controller;
        $rootScope = $injector.get("$rootScope");
        $stateParams = $injector.get("$stateParams");
        $stateParams.binderId = 1;
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        widgetState = $injector.get("widgetState");
        resources = $injector.get("hb.resources");
        Context = $injector.get("Context");
        $filter = $injector.get("$filter");
        Notify = $injector.get("Notify");
        Event = $injector.get("Event");
        $q = $injector.get("$q");

        user = {
            id: 1,
            role: "admin",
            created_at: "created_at",
            create_method: "create_method"
        };

        Session = {
            getUser: function() {
                return user;
            }
        };
    }));

    function createController(cfg) {
        ctrl = $controllerService("DoubleBusinessCardWidgetController", {
            "$stateParams": $stateParams,
            "$log": $log,
            "hb.api": api,
            "widgetState": widgetState,
            "hb.resources": resources,
            "Context": Context,
            "$filter": $filter,
            "Notify": Notify,
            "Event": Event,
            "Session": Session
        }, { cfg: cfg });
    }

    describe("refresh", function() {
        it("sets the default partner and agent when in preview mode", function() {
            var cfg = {
                previewMode: true
            };
            createController(cfg);
            $rootScope.$apply();

            expect(ctrl.model.partner).toEqual({
                first_name: "Steve",
                last_name: "N. Spector",
                company: "Inspection Company",
                email: "Steve@Inspections.com",
                mobile_phone: {
                    national: "(617) 244-5000"
                },
                website: "www.homebinder.com",
                head_shot_file: "https://s3.amazonaws.com/homebinderstatic/img/headshot.png"
            });
            expect(ctrl.model.agent).toEqual({
                first_name: "John",
                last_name: "Smith",
                company: "Keller Williams",
                email: "johnsmith@gmail.com",
                mobile_phone: "(617) 244-5000",
                website: "www.homebinder.com",
                headshot: "https://s3.amazonaws.com/homebinderstatic/img/headshot.png"
            });
        });

        it("calls context.getBinder() when not in preview mode", function() {
            spyOn(Context, "getBinder").and.returnValue($q.when({}));
            var cfg = {
                previewMode: false
            };

            createController(cfg);
            $rootScope.$apply();

            expect(Context.getBinder).toHaveBeenCalled();
        });

        it("calls context.getBinder() when cfg does not exist", function() {
            spyOn(Context, "getBinder").and.returnValue($q.when({}));
            createController();
            $rootScope.$apply();

            expect(Context.getBinder).toHaveBeenCalled();
        });

        it("automatically sets the current user in the constructor", function() {
            spyOn(Context, "getBinder").and.returnValue($q.when({}));
            createController();
            $rootScope.$apply();
            expect(ctrl.model.user).toEqual(user);
        });
    });

    describe("getWidgetDetails", function() {
        it("sets the partner and agent", function() {
            spyOn(Context, "getBinder").and.returnValue($q.when({}));
            createController();
            $rootScope.$apply();

            var agent = {
                first_name: "Steve",
                last_name: "Smith",
                mobile_phone: "423-501-3030",
                email: "steve-agent@gmail.com",
                logo: null,
                headshot: "headshot",
                company: "Agent Company"
            };
            var partner = {
                first_name: "Steve",
                last_name: "N. Spector",
                mobile_phone: {
                    national: "(555) 555-5555"
                },
                email: "steve-inspector@gmail.com",
                logo_file: null,
                head_shot_file: "headshot",
                company: "Inspector Company"
            };

            var user = {
                user_profile_attributes: {
                    branding: agent
                }
            };

            var binder = {
                access_list: [{ role: "buyer_agent", user: user }],
                branding_users: [{ scope: "binder", user: partner }],
                partner_binders: [{ thanked: false }]
            };

            ctrl.model.getWidgetDetails(binder);
            $rootScope.$apply();

            expect(ctrl.model.partnerBinder).toEqual({ thanked: false });
            expect(ctrl.model.agent).toEqual(agent);
            expect(ctrl.model.partner).toEqual(partner);
        });

        it("keeps partner and agent as default if they don't exist", function() {
            spyOn(Context, "getBinder").and.returnValue($q.when({}));
            createController();
            $rootScope.$apply();

            var binder = {
                access_list: [],
                branding_users: [],
                partner_binders: []
            };

            ctrl.model.getWidgetDetails(binder);
            $rootScope.$apply();

            expect(ctrl.model.partnerBinder).toEqual({ thanked: true });
            expect(ctrl.model.agent).toEqual(null);
            expect(ctrl.model.partner).toEqual(null);
        });

        it("sets the partner and agent headshots to default if they don't exist", function() {
            spyOn(Context, "getBinder").and.returnValue($q.when({}));
            createController();
            $rootScope.$apply();

            var agent = {
                headshot: null
            };
            var partner = {
                email: "test1@gmail.com",
                head_shot_file: null
            };
            var user = {
                email: "test2@gmail.com",
                user_profile_attributes: {
                    branding: agent
                }
            };
            var binder = {
                access_list: [{ role: "buyer_agent", user: user }],
                branding_users: [{ scope: "binder", user: partner }],
                partner_binders: []
            };

            ctrl.model.getWidgetDetails(binder);
            $rootScope.$apply();

            expect(ctrl.model.agent.headshot).toEqual("https://s3.amazonaws.com/homebinderstatic/img/headshot.png");
            expect(ctrl.model.partner.head_shot_file).toEqual("https://s3.amazonaws.com/homebinderstatic/img/headshot.png");
        });

        it("sets the partner and agent to first results in filtered array", function() {
            spyOn(Context, "getBinder").and.returnValue($q.when({}));
            createController();
            $rootScope.$apply();

            var agent = {
                first_name: "Steve",
                last_name: "Smith",
                mobile_phone: "423-501-3030",
                email: "steve-agent@gmail.com",
                logo: null,
                headshot: "headshot",
                company: "Agent Company"
            };
            var partner = {
                first_name: "Steve",
                last_name: "N. Spector",
                mobile_phone: {
                    national: "(555) 555-5555"
                },
                email: "steve-inspector@gmail.com",
                logo_file: null,
                head_shot_file: "headshot",
                company: "Inspector Company"
            };

            var user = {
                user_profile_attributes: {
                    branding: agent
                }
            };

            var binder = {
                access_list: [{ role: "seller_agent", user: {} }, { role: "buyer_agent", user: user }, { role: "buyer_agent", user: {} }],
                branding_users: [{ scope: "maintenance_reminders", user: partner }, { scope: "binder", user: partner }, { scope: "binder", user: {} }],
                partner_binders: []
            };

            ctrl.model.getWidgetDetails(binder);
            $rootScope.$apply();

            expect(ctrl.model.agent).toEqual(agent);
            expect(ctrl.model.partner).toEqual(partner);
        });
    });

    describe("sendFeedback", function() {
        it("calls api.binder.sendFeedback", function() {
            spyOn(Context, "getBinder").and.returnValue($q.when({}));
            createController();
            $rootScope.$apply();

            spyOn(api.binder, "sendFeedback").and.returnValue($q.when({}));
            spyOn(api.partnerBinder, "update").and.returnValue($q.when({}));

            ctrl.model.sendFeedback();
            $rootScope.$apply();

            expect(ctrl.model.partnerBinder.thanked).toEqual(true);
            expect(api.binder.sendFeedback).toHaveBeenCalledWith(1);
        });
    });

    describe("sendFeedbackSuccess", function() {
        it("calls api.partnerBinder.update", function() {
            spyOn(Context, "getBinder").and.returnValue($q.when({}));
            createController();
            $rootScope.$apply();

            spyOn(api.partnerBinder, "update").and.returnValue($q.when({}));

            ctrl.model.partnerBinder = {
                id: 1
            };

            ctrl.model.sendFeedbackSuccess();
            $rootScope.$apply();

            expect(api.partnerBinder.update).toHaveBeenCalledWith(1, { thanked: true });
        });
    });

    describe("updatePartnerBinderSuccess", function() {
        it("calls Event and Notify", function() {
            spyOn(Context, "getBinder").and.returnValue($q.when({}));
            createController();
            $rootScope.$apply();

            spyOn(Event, "create");
            spyOn(Notify, "info");

            ctrl.model.partner = {
                company: "company"
            };

            ctrl.model.updatePartnerBinderSuccess("response");
            $rootScope.$apply();

            expect(Event.create).toHaveBeenCalled();
            expect(Notify.info).toHaveBeenCalled();
        });
    });

    describe("sendFeedbackError", function() {
        it("calls $log and notify", function() {
            spyOn(Context, "getBinder").and.returnValue($q.when({}));
            createController();
            $rootScope.$apply();

            spyOn($log, "error");
            spyOn(Notify, "error");

            ctrl.model.sendFeedbackError({ data: "error" });
            $rootScope.$apply();

            expect($log.error).toHaveBeenCalledWith({ data: "error" });
            expect(Notify.error).toHaveBeenCalledWith("error");
        });
    });
});

describe('hbDoubleBusinessCardWidget', function() {
    var $scope, $compile, Context, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        Context = _$injector_.get("Context");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;

        spyOn(Context, "getBinder").and.returnValue($q.when({ access_list: [{ role: "buyer_agent", user: { user_profile_attributes: { branding: { headshot: "headshot" } } } }] }));

        $scope.cfg = {};

        $compile('<hb-double-business-card-widget cfg="cfg"></hb-double-business-card-widget>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the hb-double-business-card-widget directive', function() {
        it('calls context getBinder', function() {

            expect(Context.getBinder).toHaveBeenCalled();
        });
    });
});