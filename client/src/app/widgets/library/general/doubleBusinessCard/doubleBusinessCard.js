angular
    .module('hb.widgets.library.general.doubleBusinessCard', [
        'hb.widgets.widgetProvider'
    ])
    .config([
        'hbWidgetProvider',
        function(widgetProvider, resources) {
            widgetProvider.register('doubleBusinessCard', {
                name: 'Double Business Card',
                key: 'doubleBusinessCard',
                description: 'Displays a user\'s business card with the partner and agent visible',
                category: 'Binder',
                directive: 'hb-double-business-card-widget',
                width: 'md'
            });
        }
    ])
    .directive('hbDoubleBusinessCardWidget', function() {
        return {
            restrict: "E",
            templateUrl: "widgets/library/general/doubleBusinessCard/doubleBusinessCard.tpl.html",
            controller: "DoubleBusinessCardWidgetController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {
                cfg: '='
            }
        };
    })
    .controller('DoubleBusinessCardWidgetController', [
        '$stateParams',
        '$log',
        'hb.api',
        'widgetState',
        'hb.resources',
        'Context',
        '$filter',
        'Notify',
        'Event',
        'Session',
        function($stateParams, $log, api, widgetState, resources, context, $filter, notify, Event, Session) {
            var Controller = function(cfg) {
                this.cfg = cfg;
                this.binderId = $stateParams.binderId;
                this.$log = $log;
                this.api = api;
                this.state = new widgetState();
                this.resources = resources.doubleBusinessCard;
                this.context = context;
                this.$filter = $filter;
                this.notify = notify;
                this.Event = Event;
                this.user = Session.getUser();
                this.preview_partner = {
                    first_name: "Steve",
                    last_name: "N. Spector",
                    company: "Inspection Company",
                    email: "Steve@Inspections.com",
                    mobile_phone: {
                        national: "(617) 244-5000"
                    },
                    website: "www.homebinder.com",
                    head_shot_file: "https://s3.amazonaws.com/homebinderstatic/img/headshot.png"
                };
                this.preview_agent = {
                    first_name: "John",
                    last_name: "Smith",
                    company: "Keller Williams",
                    email: "johnsmith@gmail.com",
                    mobile_phone: "(617) 244-5000",
                    website: "www.homebinder.com",
                    headshot: "https://s3.amazonaws.com/homebinderstatic/img/headshot.png"
                };
                this.partner = null;
                this.agent = null;
                this.partnerBinder = {
                    thanked: true
                };
                this.refresh();
            };

            angular.extend(Controller.prototype, {
                refresh: function() {
                    if (!this.cfg || !this.cfg.previewMode) {
                        this.context.getBinder().then(angular.bind(this, this.getWidgetDetails));
                    }
                    else {
                        this.partner = this.preview_partner;
                        this.agent = this.preview_agent;
                    }
                },

                getWidgetDetails: function(binder) {
                    var partners = this.$filter("filter")(binder.branding_users, {scope: "binder"}, true);
                    var agents = this.$filter("filter")(binder.access_list, {role: "buyer_agent"}, true);

                    if (partners.length > 0) {
                        this.partner = partners[0].user;
                        
                        if (binder.partner_binders[0]) {
                            this.partnerBinder = binder.partner_binders[0];
                        }
                        
                        if (!this.partner.head_shot_file) {
                            this.partner.head_shot_file = "https://s3.amazonaws.com/homebinderstatic/img/headshot.png";
                        }
                    }
                    if (agents.length > 0 && this.checkDuplicates(agents, this.partner)) {
                        this.agent = agents[0].user.user_profile_attributes.branding;
                        if (!this.agent.headshot) {
                            this.agent.headshot = "https://s3.amazonaws.com/homebinderstatic/img/headshot.png";
                        }
                    }
                },
                
                checkDuplicates: function(agents, partner){
                    if(!partner){
                        return true;
                    }
                    return agents[0].user.email != partner.email;
                },
                
                sendFeedback: function() {
                    this.partnerBinder.thanked = true;
                    this.api.binder.sendFeedback(this.binderId).then(
                        angular.bind(this, this.sendFeedbackSuccess),
                        angular.bind(this, this.sendFeedbackError));
                },

                sendFeedbackSuccess: function(response) {
                    // Make api call to update partner on the backend
                    this.api.partnerBinder.update(this.partnerBinder.id, {thanked: true}).then(
                        angular.bind(this, this.updatePartnerBinderSuccess),
                        angular.bind(this, this.sendFeedbackError));
                },
                
                updatePartnerBinderSuccess: function(response) {
                    this.Event.create({ event_name: "feedback_given", event_type: "engagement", user_id: this.user.id, user_role: this.user.role, user_created_at: this.user.created_at, user_create_method: this.user.create_method });
                    this.notify.info(this.resources.feedbackText + this.partner.company);
                },

                sendFeedbackError: function(response) {
                    this.$log.error(response);
                    this.notify.error(response.data);
                }
            });

            this.model = new Controller(this.cfg);
        }
    ]);