describe("HBWidgetBase", function() {
    var HBWidgetBase,
        TestClass,
        $log;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
    }));
    // get the base class under test
    beforeEach(inject(function($injector) {
        HBWidgetBase = $injector.get("HBWidgetBase");
        $log = $injector.get("$log");
    }));

    function createTestClass() {
        TestClass = function() {
            HBWidgetBase.call(this);
        };

        TestClass.prototype = Object.create(HBWidgetBase.prototype);

        return new TestClass();
    }

    describe("init", function() {
        it("sets previewMode to false", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            expect(model.previewMode).toBe(false);
        }));
    });

    describe("refreshError", function() {
        it("calls log error", inject(function($q, Loading, $rootScope, $state) {
            var model = createTestClass();
            spyOn($log, "error");

            model.refreshError({ data: "error" });
            expect($log.error).toHaveBeenCalled();
        }));
    });

    describe("remove", function() {
        it("calls cfg removeFromDashboard", function() {
            var model = createTestClass();
            model.cfg = {
                removeFromDashboard: function() {}
            };
            spyOn(model.cfg, "removeFromDashboard");
            model.remove();

            expect(model.cfg.removeFromDashboard).toHaveBeenCalled();
        });
    });

});