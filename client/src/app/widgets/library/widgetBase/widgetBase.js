angular
    .module("hb.components.widgets.widgetBase", [])
    .factory('HBWidgetBase', [
        'widgetState',
        '$log',
        function(widgetState, $log) {
            var Model = function() {
                // the state of the widget. Idle, Busy, Error
                this.state = new widgetState();
                // When in preview mode the widget should not attempt to load live data.
                // It should provide some fake data so the user can preview the widget
                this.previewMode = false;
            };

            angular.extend(Model.prototype, {
                refreshError: function(error) {
                    $log.error(error.data);
                    this.state.error();
                },

                remove: function() {
                    this.cfg.removeFromDashboard(this.widget);
                }

            });

            return Model;
        }
    ]);