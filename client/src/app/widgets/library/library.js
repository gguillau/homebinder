angular
    .module("hb.widgets.library", [
        'hb.components.widgets.widgetBase',
        'hb.widgets.library.admin',
        'hb.widgets.library.general',
        'hb.widgets.library.partner'
    ]);