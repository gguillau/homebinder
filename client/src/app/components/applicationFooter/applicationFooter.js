(function() {
	"use strict";

	angular
		.module("hb.components.applicationFooter", [])
		.directive("applicationFooter", function() {
			return {
				restrict: "E",
				templateUrl: "components/applicationFooter/applicationFooter.tpl.html",
				controller: "ApplicationFooterController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("ApplicationFooterController", ["constants", "$scope", "hb.utils", "hb.resources", function(constants, $scope, utils, resources) {
			this.constants = constants;
			this.resources = resources.applicationFooter;
			this.yyyy = utils.utils.getYear();
		}]);
})();
