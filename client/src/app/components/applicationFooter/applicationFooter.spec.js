describe("ApplicationFooterController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("ApplicationFooterController", {
            "$scope": $scope
        });
        $scope.ctrl = ctrl;

    }

    describe('init', function() {
        it('sets the controller', function() {
            createController();
            expect(ctrl.resources.copyright).toEqual("Copyright");
        });
    });

});