(function() {
    "use strict";

    angular
        .module("hb.components", [
            "ui.bootstrap",
            "toastr",
            "hb.components.appcues",
            "hb.components.applicationFooter",
            "hb.components.applicationHeader",
            "hb.components.binders",
            "hb.components.compareTo",
            "hb.components.convertDate",
            "hb.components.creditCardForm",
            "hb.components.detailsNav",
            "hb.components.documents",
            "hb.components.documentsNav",
            "hb.components.documentUploader",
            "hb.components.filter",
            "hb.components.gallery",
            "hb.components.header",
            "hb.components.homeProLookup",
            "hb.components.homeProSelect",
            "hb.components.images",
            "hb.components.imageUploader",
            "hb.components.loading",
            "hb.components.lookup",
            "hb.components.maintenanceEvents",
            "hb.components.marketingResources",
            "hb.components.modal",
            "hb.components.notify",
            "hb.components.ngEnter",
            "hb.components.partners",
            "hb.components.partnerLookup",
            "hb.components.payment",
            "hb.components.popover",
            "hb.components.supportText",
            "hb.components.tagger",
            "hb.components.tagView",
            "hb.components.toolbar",
            "hb.components.users",
            "hb.components.userLookup",
            "hb.components.uploader",
            "hb.components.verticalNav"
        ])
        .config([
            'toastrConfig',
            function(toastrConfig) {
                angular.extend(toastrConfig, {
                    positionClass: "toast-top-right"
                });
            }
        ]);
})();
