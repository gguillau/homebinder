describe("HBFilterController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("HBFilterController", {
        });
        $scope.ctrl = ctrl;

    }

    describe('ctrl.search', function() {
        it('calls cfg search', function() {
            createController();
            ctrl.cfg = {
                filter: "",
                search: function() {},
                searchValue: "search"
            };
            spyOn(ctrl.cfg, "search");
            ctrl.search();

            expect(ctrl.cfg.search).toHaveBeenCalled();
        });
    });

});