(function() {
    "use strict";

    angular
        .module("hb.components.filter", [])
        .directive('hbFilter', [
            function() {
                return {
                    restrict: "E",
                    templateUrl: "components/filter/filter.tpl.html",
                    controller: "HBFilterController",
                    controllerAs: "ctrl",
                    bindToController: true,
                    scope: {
                        cfg: "="
                    }
                };
            }
        ])
        .controller('HBFilterController', [
            HBFilterController
        ]);

    function HBFilterController() {}

    HBFilterController.prototype = {
        search: function() {
            if (this.cfg.search) {
                this.cfg.search(this.cfg.filter);
            }
        }
    };

})();