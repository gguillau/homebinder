angular
    .module("hb.components.popover", [])
    .directive('hbPopover', [
        '$compile',
        '$timeout',
        function($compile, $timeout) {
            return {
                restrict: 'A',
                scope: {
                  popoverCfg: "="
                },
                link: function($scope, element, attrs) {
                    // configure the element for a popup. Have only been able to get this
                    // to somewhat work when adding the compiled angular directive when
                    // shown. Using content duplicates the directive each time the popover
                    // opens. Revisit this with new bootstrap.
                    element.popover({
                        title: $scope.popoverCfg.title || "Popover",
                        html: true,
                        placement: $scope.popoverCfg.placement || 'bottom',
                        container: 'body'
                    }).on("shown.bs.popover", function() {
                        if ($($scope.popoverCfg.directive).length) {
                            return;
                        }
                        
                        // create scope for the popover directive
                        var popoverScope = $scope.$new(true);
                        popoverScope.cfg = $scope.popoverCfg.directiveCfg;
                        
                        // create the directive HTML
                        var html = "<" + $scope.popoverCfg.directive + " cfg='cfg'></" + $scope.popoverCfg.directive + ">";
                        
                        // append the new html to the popover content
                        var content = $(".popover-content");
                        content.append($compile(html)(popoverScope));
                        
                        // if a class was provided add it to the popover
                        if ($scope.popoverCfg.popoverClass) {
                            $(".popover").addClass($scope.popoverCfg.popoverClass);
                        }
                    });
                    
                    $scope.popoverCfg.close = close;
                    
                    function close() {
                        element.popover('hide');
                    }
                }
            };
        }
    ]);