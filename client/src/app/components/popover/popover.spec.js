describe('hbPopover', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        
        $scope.popoverCfg = {};
        $scope.popoverCfg = {
            title: "Test Popover",
            popoverClass: "filter-popover",
            directive: 'hb-filter',
            templateUrl: "components/filter/filter.tpl.html"
        };

        element = $compile('<button hb-popover popover-cfg="popoverCfg" popover-is-open="true" class="btn btn-primary"></button>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the popover directive', function() {
        it('displays the title', function() {
            element.popover('show');
            
            var content = $(".popover-title");
            expect(content[0].outerHTML).toContain("Test Popover");
        });
    });
});