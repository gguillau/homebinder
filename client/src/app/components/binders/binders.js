(function() {
    "use strict";

    angular.module("hb.components.binders", [
        "hb.components.binders.index",
        "hb.components.binders.new",
        "hb.components.binders.form",
        "hb.components.binders.edit"
    ]);
})();
