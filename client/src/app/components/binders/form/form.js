(function() {
    "use strict";

    angular.module("hb.components.binders.form", [
            "hb.components.binders.form.clientInformation",
            "hb.components.binders.form.images",
            "hb.components.binders.form.appliances",
            "hb.components.binders.form.maintenanceItems",
            "hb.components.binders.form.contractors",
            "hb.components.binders.form.documents",
            "hb.components.binders.form.transfers",
            "hb.components.binders.form.agents",
            "hb.components.binders.form.branding",
            "hb.components.binders.form.confirm"
        ])
        .factory("BindersFormController", [
            'hb.resources',
            "hb.utils",
            "Session",
            "hb.api",
            "Address",
            "Loading",
            "Notify",
            "$log",
            "$state",
            "TransferModal",
            function(resources, utils, session, api, address, loading, notify, $log, $state, TransferModal) {
                var Model = function() {
                    this.api = api;
                    this.$log = $log;
                    this.notify = notify;
                    this.loading = loading;
                    this.address = address;
                    this.$state = $state;
                    this.countries = address.countries();
                    this.country = this.countries[0];
                    this.states = address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                        return state.country.value === this.country.value;
                    }));
                    this.propertyTypes = [{
                        name: 'Single Family',
                        value: 'Single Family'
                    }, {
                        name: 'Multi Family',
                        value: 'Multi Family'
                    }, {
                        name: 'Condo',
                        value: 'Condo'
                    }];
                    this.propertyTypesConfig = {
                        valueField: "value",
                        labelField: "name",
                        sortField: "name",
                        searchField: ["name"],
                        maxItems: 1,
                        create: false
                    };
                    this.statesConfig = {
                        valueField: "value",
                        labelField: "name",
                        sortField: "name",
                        searchField: ["name"],
                        maxItems: 1,
                        create: false
                    };
                    this.statusOptions = [{
                            name: "Active",
                            value: "active"
                        },
                        {
                            name: "Orphan",
                            value: "orphan"
                        }
                    ];
                    this.default_transaction_types = [{
                            name: "Buy Side",
                            value: "buy_side"
                        },
                        {
                            name: "Sell Side",
                            value: "sell_side"
                        }
                    ];
                    this.email_error_message = "The client email address can't be the same as the user creating the binder.";
                    /*jshint -W024*/
                    this.resources = angular.extend({}, this.resources, resources.bindersNew);
                    this.resources = angular.extend({}, this.resources, resources.bindersClient);
                    this.resources = angular.extend({}, this.resources, resources.bindersConfiguration);
                    this.resources = angular.extend({}, this.resources, resources.bindersOptions);
                    this.currentUser = session.getUser();
                    this.title = this.resources.title;
                    this.utils = utils;
                    this.today = this.utils.utils.getToday(false);
                    this.next_month = this.utils.utils.getNextMonth();
                    this.yyyy = this.utils.utils.getYear();
                    this.binder_brandings = [];
                    this.formCfg = {
                        currentUser: this.currentUser,
                        binderOwner: null,
                        partner: {},
                        binderBrandings: {
                            binder: null,
                            maintenance: null,
                            recall: null,
                            sellerReport: null,
                            agent: {
                                maintenance: null,
                                recall: null
                            }
                        },
                        settings: {
                            showPageTitle: true,
                            showConfigForm: true,
                            showAdditionalLinks: false
                        },
                        binderTemplate: {},
                        access_list: []
                    };
                    this.datepicker = {
                        format: "MMMM dd, yyyy",
                        options: {
                            "show-button-bar": false
                        }
                    };
                    this.date = [{
                        opened: false
                    }, {
                        opened: false
                    }];
                    this.partnerLookupCfg = {
                        allowClear: true,
                        placeholder: "Search for partner",
                        refresh: angular.bind(this, this.partnerLookup),
                        itemTemplate: "components/lookup/partner/partnerDropDownItem.tpl.html",
                        selectionTemplate: "components/lookup/partner/partnerSelection.tpl.html"
                    };
                };

                angular.extend(Model.prototype, {

                    verifyEmail: function() {
                        if (this.currentUser.email === this.clientInformation.email) {
                            this.notify.info("The homeowner email cannot be the same as your email.");
                            this.loading.close();
                            return true;
                        }
                        return false;
                    },

                    partnerLookup: function(value) {
                        var opts = {
                            page: 1,
                            count: 50,
                            search: value
                        };
                        return this.api.partner.all(opts).then(
                            angular.bind(this, function(response) {
                                return {
                                    data: response.data.items
                                };
                            })
                        );
                    },

                    initiate: function() {
                        this.navigation_links = [{
                                name: "Binder Info",
                                links: [
                                    { name: "Client/Property Info", value: "client", state: "admin.binders_edit.client", allowed: true }
                                ]
                            },
                            {
                                name: "Binder Items",
                                links: [
                                    { name: "Images", value: "images", state: "admin.binders_edit.images", allowed: this.formCfg.settings.showAdditionalLinks },
                                    { name: "Appliances", value: "appliances", state: "admin.binders_edit.appliances", allowed: this.formCfg.settings.showAdditionalLinks },
                                    { name: "Maintenance Items", value: "maintenance_items", state: "admin.binders_edit.maintenanceItems", allowed: this.formCfg.settings.showAdditionalLinks },
                                    { name: "Preferred Home Pros", value: "home_pros", state: "admin.binders_edit.contractors", allowed: this.formCfg.settings.showAdditionalLinks },
                                    { name: "Documents", value: "documents", state: "admin.binders_edit.documents", allowed: this.formCfg.settings.showAdditionalLinks }
                                ]
                            }
                        ];
                        if (this.formCfg.settings.showBinderUsers) {
                            this.navigation_links[0].links.push({ name: "Users", value: "users", state: "admin.binders_edit.users", allowed: this.formCfg.settings.showAdditionalLinks });
                        }
                        if (this.formCfg.settings.showBinderAgents) {
                            this.navigation_links[0].links.push({ name: "Agents", value: "agents", state: "admin.binders_edit.agents", allowed: this.formCfg.settings.showAdditionalLinks });
                        }
                        if (this.formCfg.settings.showBinderBrandings) {
                            this.navigation_links[0].links.push({ name: "Brandings", value: "brandings", state: "admin.binders_edit.brandings", allowed: this.formCfg.settings.showAdditionalLinks });
                        }
                        if (this.formCfg.settings.showBinderPartners) {
                            this.navigation_links[0].links.push({ name: "Partners", value: "partners", state: "admin.binders_edit.partners", allowed: this.formCfg.settings.showAdditionalLinks });
                        }
                        if (this.formCfg.settings.showBinderEmails) {
                            this.navigation_links[0].links.push({ name: "Emails", value: "emails", state: "admin.binders_edit.emails", allowed: this.formCfg.settings.showAdditionalLinks });
                        }
                        if (this.formCfg.settings.showBinderWarranties) {
                            this.navigation_links[0].links.push({ name: "Warranties", value: "warranties", state: "admin.binders_edit.warranties", allowed: this.formCfg.settings.showAdditionalLinks });
                        }
                        if (this.formCfg.settings.showBinderTransactions) {
                            this.navigation_links[0].links.push({ name: "Transactions", value: "transactions", state: "admin.binders_edit.transactions", allowed: this.formCfg.settings.showAdditionalLinks });
                        }
                        if (this.formCfg.settings.showBinderSubscriptions) {
                            this.navigation_links[0].links.push({ name: "Subscription", value: "subscriptions", state: "admin.binders_edit.subscriptions", allowed: this.formCfg.settings.showAdditionalLinks });
                        }
                        if (this.formCfg.settings.showBinderTransfers) {
                            this.navigation_links[1].links.push({ name: "Transfers", value: "transfers", state: "admin.binders_edit.transfers", allowed: this.formCfg.settings.showAdditionalLinks });
                        }

                        if (this.currentUser.role === "homepro" || this.currentUser.role === "property_manager") {
                            this.formCfg.inspection_date = new Date();
                        }
                    },

                    setBinderName: function() {
                        if (!this.clientInformation.name && this.clientInformation.first && this.clientInformation.last) {
                            this.clientInformation.name = this.clientInformation.first + " " + this.clientInformation.last + " Home";
                        }
                    },

                    onSelect: function(item) {
                        if (item) {
                            this.country = item.country;
                            this.clientInformation.country = item.country.value;
                        }
                    },

                    open: function(index) {
                        this.date[index].opened = true;
                    },

                    transferBinder: function() {
                        TransferModal.show({
                            binder: this.binder,
                            showTransferTypes: false,
                            defaultTransferType: "ownership",
                            closed: angular.bind(this, this.close)
                        });
                    },

                    close: function() {
                        this.$state.go(this.indexState, this.indexParams);
                    },

                    onSelectCountry: function(item) {
                        if (item) {
                            this.country = item;
                            this.states = address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                                return state.country.value === this.country.value;
                            }));
                        }
                    },

                    setPropertyType: function(address2) {
                        if (address2) {
                            return "Condo";
                        }
                        else {
                            return "Single Family";
                        }
                    }

                });

                return Model;
            }
        ]);
})();
