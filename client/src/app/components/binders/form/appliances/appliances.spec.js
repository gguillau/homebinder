describe("BinderFormComponentAppliancesController", function() {
    var ctrl,
        controller,
        $log,
        api,
        notify,
        resources,
        loading,
        $modal,
        $q,
        defer,
        $rootScope;

    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q.defer();
                    defer.resolve({ permissions: { can_read: true } });
                    return defer.promise;
                }
            };
        });
    }));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
        $rootScope = $injector.get("$rootScope");

        $modal = {
            show: function() {}
        };
    }));

    function createController() {
        ctrl = controller("BinderFormComponentAppliancesController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "hb.resources": resources,
            "Loading": loading,
            "ModalService": $modal
        });
    }

    describe("uploadImages", function() {
        it("sets the applianceUploaderId", function() {
            createController();
            ctrl.model.uploadImages(1);

            expect(ctrl.model.applianceUploaderId).toEqual(1);
        });
    });

    describe("imgAdded", function() {
        it("calls uploadFiles", function() {
            createController();
            $rootScope.$apply();
            var uploaderApi = {
                pendingUploads: function() {},
                uploadFiles: function() {},
                updateParams: function() {}
            };
            ctrl.model.imgUploader.api = uploaderApi;
            spyOn(uploaderApi, "pendingUploads").and.returnValue([{ id: 1 }]);
            spyOn(uploaderApi, "uploadFiles");
            ctrl.model.imgAdded();

            expect(uploaderApi.uploadFiles).toHaveBeenCalled();
        });

        it("calls loading close", function() {
            spyOn(loading, "close");

            createController();
            var uploaderApi = {
                pendingUploads: function() {},
                uploadFiles: function() {},
                updateParams: function() {}
            };
            ctrl.model.imgUploader.api = uploaderApi;
            spyOn(uploaderApi, "pendingUploads").and.returnValue([]);
            spyOn(uploaderApi, "uploadFiles");
            ctrl.model.imgAdded();

            expect(loading.close).toHaveBeenCalled();
        });
    });

    describe("imgUploadSuccess", function() {
        it("calls imgAdded", function() {
            createController();
            var uploaderApi = {
                pendingUploads: function() {},
                uploadFiles: function() {},
                updateParams: function() {}
            };
            ctrl.model.imgUploader.api = uploaderApi;
            spyOn(uploaderApi, "pendingUploads").and.returnValue([{ id: 1 }]);
            spyOn(ctrl.model, "imgAdded");
            ctrl.model.imgUploadSuccess({});

            expect(ctrl.model.imgAdded).toHaveBeenCalled();
        });

        it("calls notify", function() {
            createController();
            var uploaderApi = {
                pendingUploads: function() {},
                uploadFiles: function() {},
                updateParams: function() {}
            };
            ctrl.model.imgUploader.api = uploaderApi;
            spyOn(uploaderApi, "pendingUploads").and.returnValue([]);
            spyOn(notify, "success");
            ctrl.model.imgUploadSuccess({});

            expect(notify.success).toHaveBeenCalled();
        });
    });

    describe("imgUploadError", function() {
        it("calls imgAdded", function() {
            createController();
            var uploaderApi = {
                pendingUploads: function() {},
                uploadFiles: function() {},
                updateParams: function() {},
                remove: function() {}
            };
            ctrl.model.imgUploader.api = uploaderApi;
            spyOn(uploaderApi, "pendingUploads").and.returnValue([{ id: 1 }]);
            spyOn(ctrl.model, "imgAdded");
            ctrl.model.imgUploadError({ data: "error" });

            expect(ctrl.model.imgAdded).toHaveBeenCalled();
        });
    });

});