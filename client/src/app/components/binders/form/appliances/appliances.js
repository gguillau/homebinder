(function() {
    "use strict";

    angular
        .module("hb.components.binders.form.appliances", [])
        .factory("BinderFormAppliancesController", [
            "BinderAppliancesController",
            "hb.resources",
            "Context",
            "Session",
            "$log",
            "Notify",
            "Loading",
            "$state",
            function(BinderAppliancesController, resources, context, session, $log, notify, loading, $state) {
                var Model = function() {
                    // call the parent class
                    BinderAppliancesController.call(this);
                    this.resources = angular.extend({}, this.resources, resources.bindersAppliances);
                    context.getBinder().then(angular.bind(this, function(binder) {
                        this.binder = binder;
                        if (binder.permissions.can_read) {
                            this.init();
                        }
                        else {
                            this.resources.info = this.resources.readOnly;
                        }
                    }));
                    this.uploaderId = "uploadImages" + new Date().getTime();
                    this.imgUploader = {
                        fileTypes: "image",
                        hideUploadButton: true,
                        showTable: false,
                        fileLimit: false,
                        url: "/api/v1/images",
                        multiSelect: true,
                        id: this.uploaderId,
                        hide_upload: true,
                        jwt: session.getJwt()
                    };
                    this.applianceUploaderId = null;
                };

                Model.prototype = Object.create(BinderAppliancesController.prototype);

                angular.extend(Model.prototype, {
                    uploadImages: function(id) {
                        this.applianceUploaderId = id;
                        angular.element('#' + this.uploaderId).trigger('click');
                    },

                    imgAdded: function() {
                        loading.show("Uploading photos...");
                        var uploadApi = this.imgUploader.api;
                        if (uploadApi && uploadApi.pendingUploads().length > 0) {
                            uploadApi.updateParams({
                                binder_id: this.binder.id,
                                appliance_id: this.applianceUploaderId
                            });
                            uploadApi.uploadFiles();
                        }
                        else {
                            loading.close();
                        }
                    },

                    imgUploadSuccess: function(file) {
                        var uploadApi = this.imgUploader.api;
                        if (uploadApi && uploadApi.pendingUploads().length > 0) {
                            this.imgAdded();
                        }
                        else {
                            notify.success("Images uploaded!");
                            loading.close();
                        }
                    },

                    imgUploadError: function(response) {
                        $log.error(response);
                        notify.error(response.data);
                        var files = this.imgUploader.api.pendingUploads();
                        this.imgUploader.api.remove(files[0]);
                        this.imgAdded();
                    }
                });

                return Model;


            }
        ])
        .controller("BinderFormComponentAppliancesController", [
            "BinderFormAppliancesController",
            BinderFormComponentAppliancesController
        ]);

    function BinderFormComponentAppliancesController(BinderFormAppliancesController) {
        var Model = function() {
            // call the parent class
            BinderFormAppliancesController.call(this);
            this.init();
        };

        Model.prototype = Object.create(BinderFormAppliancesController.prototype);

        angular.extend(Model.prototype, {});

        this.model = new Model();
    }

})();