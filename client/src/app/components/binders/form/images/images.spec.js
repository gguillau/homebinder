describe("BinderFormImagesController", function() {
    var ctrl,
        controller,
        $log,
        api,
        notify,
        resources,
        loading,
        $modal,
        $q,
        defer;

    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q.defer();
                    defer.resolve({ permissions: { can_read: true } });
                    return defer.promise;
                }
            };
        });
    }));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");

        $modal = {
            show: function() {}
        };
    }));

    function createController() {
        ctrl = controller("BinderFormImagesController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "hb.resources": resources,
            "Loading": loading,
            "ModalService": $modal
        });
    }

    describe("init", function() {
        it("sets the info text", function() {
            createController();
            expect(ctrl.model.resources.info).toEqual("Upload multiple images including the 'hero photo'. The 'hero photo' will be used in several places including the binder overview page.");
        });
    });

});

describe('binderFormImages', function() {
    var $scope, $compile, element, context, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        context = _$injector_.get("Context");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(context, "getBinder").and.returnValue($q.when({ permissions: { can_read: false } }));

        element = $compile('<binder-form-images></binder-form-images>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {
            expect(element.html()).toContain("Upload images");
        });
    });
});
