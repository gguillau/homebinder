(function() {
    "use strict";

    angular
        .module("hb.components.binders.form.images", [])
        .directive("binderFormImages", function() {
            return {
                restrict: "E",
                templateUrl: "components/binders/form/images/images.tpl.html",
                controller: "BinderFormImagesController",
                controllerAs: "ctrl",
                bindToController: true
            };
        })
        .controller("BinderFormImagesController", [
            "BinderImagesController",
            "hb.resources",
            "Context",
            BinderFormImagesController
        ]);

    function BinderFormImagesController(BinderImagesController, resources, context) {

        this.resources = resources;
        var Model = function() {
            // call the parent class
            BinderImagesController.call(this);
            this.resources = angular.extend({}, this.resources, resources.bindersImages);
            this.uploadCfg = {
                id: "uploadImages",
                fileTypes: "image",
                url: "/api/v1/images",
                jwt: this.session.getJwt(),
                params: {
                    binder_id: this.binderId
                },
                multiSelect: true,
                hideUploadButton: true,
                hide_upload: true,
                showTable: false
            };
            context.getBinder().then(angular.bind(this, function(binder) {
                if (binder.permissions.can_read) {
                    this.init();
                }
                else {
                    this.resources.info = this.resources.readOnly;
                }
            }));
        };

        Model.prototype = Object.create(BinderImagesController.prototype);

        this.model = new Model();
    }
})();