(function() {
	"use strict";

	angular
		.module("hb.components.binders.form.agents", [
			"hb.components.binders.form.agents.edit"
		])
		.directive("binderFormAgents", function() {
			return {
				restrict: "E",
				templateUrl: "components/binders/form/agents/agents.tpl.html",
				controller: "BinderFormAgentController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("BinderFormAgentController", [
			"hb.api",
			"hb.utils",
			"Notify",
			"$log",
			"$stateParams",
			"$scope",
			"$filter",
			"hb.resources",
			"BinderFormAgentsEditModal",
			"Loading",
			"ModalService",
			BinderFormAgentController
		]);

	function BinderFormAgentController(api, utils, notify, $log, $stateParams, $scope, $filter, resources, BinderFormAgentsEditModal, Loading, modals) {
		this.api = api;
		this.utils = utils;
		this.notify = notify;
		this.$log = $log;
		this.$filter = $filter;
		this.resources = resources.indexBase;
		this.resources = angular.extend({}, this.resources, resources.bindersAgents);
		this.binderId = $stateParams.binderId;
		this.partnerId = $stateParams.partnerId;
		this.BinderFormAgentsEditModal = BinderFormAgentsEditModal;
		this.deleteCall = api.userBinder.destroy;
		this.loading = Loading;
		this.buyer_agent = null;
		this.seller_agent = null;
		this.branding_users = [];
		this.modals = modals;
		this.refresh();
	}

	BinderFormAgentController.prototype = {
		refresh: function() {
			this.loading.show("Loading...");
			this.api.binder.get_acl(this.binderId).then(
				angular.bind(this, this.getAclSuccess),
				angular.bind(this, this.getAclError));
		},

		getAclSuccess: function(response) {
			this.access_list = response.data;
			var buyer_agents = response.data.filter(function(user_binder) {
				return user_binder.role === "buyer_agent";
			});

			if (buyer_agents.length > 0) {
				this.buyer_agent = buyer_agents[0];
			}

			var seller_agents = response.data.filter(function(user_binder) {
				return user_binder.role === "seller_agent";
			});

			if (seller_agents.length > 0) {
				this.seller_agent = seller_agents[0];
			}
			this.getBrandings();
			this.loading.close();
		},

		getBrandings: function() {
			this.api.binderBranding.all({ binderId: this.binderId }).then(
				angular.bind(this, this.getBrandingsSuccess),
				angular.bind(this, this.getBrandingsError));
		},

		getBrandingsSuccess: function(response) {
			this.branding_users = response.data.items;
		},

		getBrandingsError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
		},

		getAclError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		},

		add: function(role) {
			var agent = {
				role: "agent",
				user_profile_attributes: {
					mobile_phone: {

					}
				}
			};

			this.BinderFormAgentsEditModal.show({
				agent: agent,
				role: role,
				branding_users: this.branding_users,
				onSave: angular.bind(this, this.insertAgent)
			});
		},

		edit: function(agent) {
			this.BinderFormAgentsEditModal.show({
				agent: agent.user,
				role: agent.role,
				user_binder: agent,
				branding_users: this.branding_users,
				onSave: angular.bind(this, this.insertAgent)
			});
		},

		insertAgent: function(item, role) {
			if (role === "buyer_agent") {
				this.buyer_agent = {};
				this.buyer_agent.role = "buyer_agent";
				this.buyer_agent.user = item.user;
				this.buyer_agent.id = item.id;
			}
			else {
				this.seller_agent = {};
				this.seller_agent.role = "seller_agent";
				this.seller_agent.user = item.user;
				this.seller_agent.id = item.id;
			}
			this.getBrandings();
		},

		// initiates a delete sequence
		deleteItem: function(item) {
			// confirm the delete
			this.modals.confirm({
				title: this.resources.confirmDelete,
				glyphicon: "glyphicon glyphicon-trash",
				message: this.resources.deleteMsg1 + item.user.email + this.resources.deleteMsg2,
				confirm: angular.bind(this, this.onConfirmDeleteItem, item)
			});
		},

		// performs a delete on the server
		onConfirmDeleteItem: function(item) {
			this.loading.show(this.resources.deletingMsg);
			this.deleteCall(item.id).then(
				angular.bind(this, this.onDeleteItemSuccess, item),
				angular.bind(this, this.onDeleteItemError)
			);
		},

		// updates the view after a delete
		onDeleteItemSuccess: function(item, response) {
			if (this.buyer_agent && this.buyer_agent.user.email === item.user.email) {
				this.buyer_agent = null;
			}
			else {
				this.seller_agent = null;
			}
			this.branding_users.forEach(angular.bind(this, function(branding_user) {
				if (item.user.id === branding_user.user.user_id) {
					this.api.binderBranding.destroy(branding_user.id).then(
						angular.bind(this, this.onDeleteBranding, branding_user),
						angular.bind(this, this.onDeleteItemError)
					);
				}
			}));
			this.loading.close();
		},

		onDeleteBranding: function(item, response) {
			var index = this.branding_users.indexOf(item);
			this.branding_users.splice(index, 1);
		},

		onBrandingError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
		},

		// notifies the user of a delete error. logs a message
		onDeleteItemError: function(response) {
			this.$log.error(response);
			this.notify.error(response.data);
			this.loading.close();
		}
	};

})();
