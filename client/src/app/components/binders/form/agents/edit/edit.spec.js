describe("BinderFormAgentsEditController", function() {
    var ctrl,
        controller,
        $q,
        $log,
        api,
        notify,
        resources,
        loading,
        $modal,
        args,
        BinderFormAgentsEditModal,
        ModalService,
        $rootScope,
        defer;

    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Context", function() {
            return {
                getPartner: function() {
                    defer = $q.defer();
                    defer.resolve({ address: { country: "US" } });
                    return defer.promise;
                }
            };
        });
    }));

    beforeEach(inject(function($controller, $injector) {
        $rootScope = $injector.get("$rootScope");
        controller = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
        BinderFormAgentsEditModal = $injector.get("BinderFormAgentsEditModal");
        ModalService = $injector.get("ModalService");
        $modal = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };

        args = { agent: {} };
    }));

    function createController() {
        ctrl = controller("BinderFormAgentsEditController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "hb.resources": resources,
            "Loading": loading,
            "data": args,
            "$modalInstance": $modal
        });
    }

    describe("lookupRefresh", function() {
        it("loads the users", function() {
            spyOn(api.user, "all").and.returnValue($q.when({
                data: {
                    items: []
                }
            }));
            createController();
            ctrl.partnerId = 1;
            ctrl.lookupRefresh();
            $rootScope.$apply();

            expect(api.user.all).toHaveBeenCalled();
        });
    });

    describe("save", function() {
        it("does not call create/update", function() {
            spyOn(api.user, "create");
            spyOn(api.user, "update");

            createController();
            ctrl.form = { $invalid: true };
            ctrl.save();

            expect(api.user.create).not.toHaveBeenCalled();
            expect(api.user.update).not.toHaveBeenCalled();
        });

        it("calls update", function() {
            spyOn(api.user, "create");
            spyOn(api.user, "update").and.returnValue($q.when({ data: { id: 1 } }));

            createController();
            $rootScope.$apply();
            spyOn(ctrl, "addUser");
            ctrl.form = { $invalid: false };
            ctrl.partnerId = 1;
            ctrl.agent = {
                id: 1,
                email: "agent@gmail.com",
                user_profile_attributes: {
                    mobile_phone: {}
                }
            };
            ctrl.save();

            expect(api.user.create).not.toHaveBeenCalled();
            expect(api.user.update).toHaveBeenCalled();
        });

        it("calls update when user binder is present", function() {
            spyOn(api.user, "create");
            spyOn(api.user, "update").and.returnValue($q.when({ data: { id: 1 } }));

            createController();
            $rootScope.$apply();
            spyOn(ctrl, "addUser");
            ctrl.form = { $invalid: false };
            ctrl.partnerId = 1;
            ctrl.user_binder = {
                id: 1
            };
            ctrl.agent = {
                id: 1,
                email: "agent@gmail.com",
                user_profile_attributes: {
                    mobile_phone: {}
                }
            };
            ctrl.save();

            expect(api.user.create).not.toHaveBeenCalled();
            expect(api.user.update).toHaveBeenCalled();
        });

        it("calls create", function() {
            spyOn(api.user, "create").and.returnValue($q.when({ data: { id: 1 } }));
            spyOn(api.user, "update");

            createController();
            $rootScope.$apply();
            spyOn(ctrl, "addUser");
            ctrl.form = { $invalid: false };
            ctrl.partnerId = 1;
            ctrl.agent = {
                email: "agent@gmail.com",
                user_profile_attributes: {
                    mobile_phone: {}
                }
            };
            ctrl.save();

            expect(api.user.create).toHaveBeenCalled();
            expect(api.user.update).not.toHaveBeenCalled();
        });
    });

    describe("onAgentError", function() {
        it("notifies error", function() {
            spyOn(notify, "error");

            createController();
            ctrl.onAgentError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("addBinderRole", function() {
        it("calls updateBrandings", function() {
            createController();
            ctrl.partner = {
                id: 1,
                configuration: {
                    default_user_branding_option: "co_brand"
                }
            };
            ctrl.role = "buyer_agent";
            spyOn(ctrl, "updateBrandings");
            spyOn(ctrl, "addUser");
            ctrl.addBinderRole({ data: { id: 1 } });

            expect(ctrl.updateBrandings).toHaveBeenCalled();
            expect(ctrl.addUser).toHaveBeenCalled();
        });
    });

    describe("addUser", function() {
        it("calls api binder add_user", function() {
            spyOn(api.binder, "add_user").and.returnValue($q.when({ data: { id: 1 } }));

            createController();
            ctrl.addUser({ id: 1 });

            expect(api.binder.add_user).toHaveBeenCalled();
        });
    });

    describe("updateBrandings", function() {
        it("updates existing brandings", function() {
            createController();
            ctrl.partner = {
                partner_type: "inspector"
            };
            ctrl.branding_users = [
                { id: 1, scope: "seller_report", user: { role: "agent" } },
                { id: 2, scope: "recall_email", user: { role: "agent" } },
                { id: 3, scope: "maintenance_email", user: { role: "agent" } }
            ];
            ctrl.updateBrandings({ data: { id: 1, user_profile_attributes: { id: 2 } } });

            expect(ctrl.existing_brandings.length).toEqual(3);
        });

        it("creates new brandings when partner is inspector", function() {
            createController();
            ctrl.partner = {
                partner_type: "inspector"
            };
            ctrl.branding_users = [];
            ctrl.updateBrandings({ data: { id: 1, user_profile_attributes: { id: 2 } } });

            expect(ctrl.brandings.length).toEqual(3);
        });

        it("creates new brandings when partner is broker", function() {
            createController();
            ctrl.partner = {
                partner_type: "broker"
            };
            ctrl.branding_users = [];
            ctrl.updateBrandings({ data: { id: 1, user_profile_attributes: { id: 2 } } });

            expect(ctrl.brandings.length).toEqual(2);
        });
    });

    describe("addBranding", function() {
        it("calls api binderBranding create", function() {
            spyOn(api.binderBranding, "create").and.returnValue($q.when({ data: { id: 1 } }));

            createController();
            ctrl.brandings = [{ scope: "recall_email" }];
            ctrl.user_binder = {};
            ctrl.addBranding({ data: { id: 1 } });

            expect(api.binderBranding.create).toHaveBeenCalled();
        });

        it("calls api binderBranding update", function() {
            spyOn(api.binderBranding, "update").and.returnValue($q.when({ data: { id: 1 } }));

            createController();
            ctrl.existing_brandings = [{ id: 1, scope: "recall_email" }];
            ctrl.user_binder = {};
            ctrl.addBranding({ data: { id: 1 } });

            expect(api.binderBranding.update).toHaveBeenCalled();
        });

        it("calls addUserSuccess", function() {
            createController();
            spyOn(ctrl, "addUserSuccess");
            ctrl.user_binder = {};
            ctrl.addBranding({ data: { id: 1 } });

            expect(ctrl.addUserSuccess).toHaveBeenCalled();
        });
    });

    describe("addBrandingSuccess", function() {
        it("calls addBranding", function() {
            createController();
            spyOn(ctrl, "addBranding");
            ctrl.user_binder = {};
            ctrl.addBrandingSuccess();

            expect(ctrl.addBranding).toHaveBeenCalled();
        });
    });

    describe("addBrandingError", function() {
        it("calls addBranding", function() {
            createController();
            spyOn(ctrl, "addBranding");
            ctrl.user_binder = {};
            ctrl.addBrandingError({ data: "error" });

            expect(ctrl.addBranding).toHaveBeenCalled();
        });
    });

    describe("addUserSuccess", function() {
        it("calls onSave", function() {
            createController();
            ctrl.onSave = function() {};
            spyOn(ctrl, "onSave");
            ctrl.addUserSuccess({ data: { user: {} } });

            expect(ctrl.onSave).toHaveBeenCalled();
        });
    });

    describe("addUserError", function() {
        it("calls notify", function() {
            spyOn(notify, "error");

            createController();
            ctrl.addUserError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("cancel", function() {
        it("closes the modal", function() {
            spyOn($modal, "dismiss");

            createController();
            ctrl.cancel();

            expect($modal.dismiss).toHaveBeenCalled();
        });
    });

    describe('BinderFormAgentsEditModal', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            BinderFormAgentsEditModal.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});