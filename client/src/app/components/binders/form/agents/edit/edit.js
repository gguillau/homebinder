(function() {
    "use strict";

    angular
        .module("hb.components.binders.form.agents.edit", [])
        .factory("BinderFormAgentsEditModal", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "components/binders/form/agents/edit/edit.tpl.html",
                            controller: "BinderFormAgentsEditController as ctrl",
                            resolveData: opts,
                            backdrop: true
                        });
                    }
                };
            }
        ])
        .controller("BinderFormAgentsEditController", [
            'data',
            "$modalInstance",
            "hb.utils",
            "$stateParams",
            "hb.resources",
            "hb.api",
            "Notify",
            "$log",
            "Context",
            "$filter",
            "Address",
            BinderFormAgentsEditController
        ]);

    function BinderFormAgentsEditController(opts, $modal, utils, $stateParams, resources, api, notify, $log, context, $filter, Address) {
        this.binderId = $stateParams.binderId;
        this.partnerId = $stateParams.partnerId;
        this.$modal = $modal;
        this.agent = opts.agent;
        this.role = opts.role;
        this.onSave = opts.onSave;
        this.user_binder = opts.user_binder;
        this.branding_users = opts.branding_users;
        this.utils = utils;
        this.resources = resources.bindersAgents;
        this.api = api;
        this.notify = notify;
        this.$log = $log;
        this.context = context;
        this.$filter = $filter;
        context.getPartner().then(angular.bind(this, function(partner) {
            this.partner = partner;
            this.country = Address.findCountryByCode(this.partner.address.country);
        }));
        this.processing = false;
        this.brandings = [];
        this.existing_brandings = [];
        this.lookupCfg = {
            placeholder: "Search for agent...",
            refresh: angular.bind(this, this.lookupRefresh),
            itemTemplate: "components/lookup/user/userDropDownItem.tpl.html",
            selectionTemplate: "components/lookup/user/userSelection.tpl.html"
        };
        this.setRepairPricer();
    }

    BinderFormAgentsEditController.prototype = {
        setRepairPricer: function() {
            if (this.agent.id) {
                this.access_repair_pricer = this.user_binder.access_list;
            }
            else {
                this.access_repair_pricer = this.role === "buyer_agent" ? true : false;
            }
        },

        lookupRefresh: function(value) {
            var opts = {
                page: 1,
                count: 50,
                role: "agent",
                search: value
            };

            if (this.partnerId) {
                opts.partnerId = this.partnerId;
                opts.searchMethod = "for_partner";
            }

            opts.search = value;
            return this.api.user.all(opts).then(
                angular.bind(this, function(response) {
                    return {
                        data: response.data.items
                    };
                })
            );
        },

        save: function() {
            this.form.$submitted = true;
            if (this.form.$invalid) {
                return;
            }
            this.processing = true;
            var user = {};
            if (this.agent.id) {
                user = {
                    email: this.agent.email,
                    role: this.agent.role,
                    user_profile_attributes: {
                        id: this.agent.user_profile_attributes.id,
                        company: this.agent.user_profile_attributes.company,
                        first_name: this.agent.user_profile_attributes.first_name,
                        home_phone: this.agent.user_profile_attributes.home_phone,
                        last_name: this.agent.user_profile_attributes.last_name,
                        mobile_phone: "+" + this.country.code + this.agent.user_profile_attributes.mobile_phone.national,
                        website: this.agent.user_profile_attributes.website,
                        address_attributes: {
                            country: this.country.value
                        }
                    }
                };
                this.api.user.update(this.agent.id, user).then(
                    angular.bind(this, this.addBinderRole),
                    angular.bind(this, this.onAgentError));
            }
            else {
                user = {
                    email: this.agent.email,
                    role: this.agent.role,
                    user_profile_attributes: {
                        company: this.agent.user_profile_attributes.company,
                        first_name: this.agent.user_profile_attributes.first_name,
                        home_phone: this.agent.user_profile_attributes.home_phone,
                        last_name: this.agent.user_profile_attributes.last_name,
                        mobile_phone: "+" + this.country.code + this.agent.user_profile_attributes.mobile_phone.national,
                        website: this.agent.user_profile_attributes.website,
                        address_attributes: {
                            country: this.country.value
                        }
                    }
                };

                this.api.user.create({ user: user, partnerId: this.partnerId }).then(
                    angular.bind(this, this.addBinderRole),
                    angular.bind(this, this.onAgentError));
            }
        },

        onAgentError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.processing = false;
        },

        addBinderRole: function(response) {
            this.user_binder = {
                role: this.role,
                user: response.data
            };
            var data = {
                user_id: response.data.id,
                role: this.role,
                access_repair_pricer: this.access_repair_pricer
            };

            // check if we need to add the binder brandings
            if (this.role === "buyer_agent" && (this.partner && this.partner.configuration.default_user_branding_option === "co_brand" || this.partner.configuration.default_user_branding_option === "agent")) {
                this.updateBrandings(response);
            }
            this.addUser(data);
        },

        addUser: function(data) {
            this.api.binder.add_user(this.binderId, data).then(
                angular.bind(this, this.addBranding),
                angular.bind(this, this.addUserError));
        },

        updateBrandings: function(response) {
            var recall_email_brandings = this.$filter("filter")(this.branding_users, {
                scope: "recall_email",
                user: { role: "agent" }
            }, true);

            var maintenance_email_brandings = this.$filter("filter")(this.branding_users, {
                scope: "maintenance_email",
                user: { role: "agent" }
            }, true);

            if (recall_email_brandings.length > 0) {
                this.existing_brandings.push({
                    id: recall_email_brandings[0].id,
                    binder_id: this.binderId,
                    user_branding_id: response.data.user_profile_attributes.id,
                    scope: "recall_email"
                });
            }
            else {
                this.brandings.push({
                    binder_id: this.binderId,
                    user_branding_id: response.data.user_profile_attributes.id,
                    scope: "recall_email"
                });
            }

            if (maintenance_email_brandings.length > 0) {
                this.existing_brandings.push({
                    id: maintenance_email_brandings[0].id,
                    binder_id: this.binderId,
                    user_branding_id: response.data.user_profile_attributes.id,
                    scope: "maintenance_email"
                });
            }
            else {
                this.brandings.push({
                    binder_id: this.binderId,
                    user_branding_id: response.data.user_profile_attributes.id,
                    scope: "maintenance_email"
                });
            }

            if (this.partner.partner_type != "broker") {

                var seller_report_brandings = this.$filter("filter")(this.branding_users, {
                    scope: "seller_report",
                    user: { role: "agent" }
                }, true);

                if (seller_report_brandings.length > 0) {
                    this.existing_brandings.push({
                        id: seller_report_brandings[0].id,
                        binder_id: this.binderId,
                        user_branding_id: response.data.user_profile_attributes.id,
                        scope: "seller_report"
                    });
                }
                else {
                    this.brandings.push({
                        binder_id: this.binderId,
                        user_branding_id: response.data.user_profile_attributes.id,
                        scope: "seller_report"
                    });
                }
            }
        },

        addBranding: function(response) {
            this.user_binder.id = response.data.id;
            var branding = null;
            if (this.brandings.length > 0) {
                branding = this.brandings[0];
                this.brandings.splice(0, 1);
                this.api.binderBranding.create(branding).then(
                    angular.bind(this, this.addBrandingSuccess),
                    angular.bind(this, this.addBrandingError));
            }
            else if (this.existing_brandings.length > 0) {
                branding = this.existing_brandings[0];
                this.existing_brandings.splice(0, 1);
                this.api.binderBranding.update(branding.id, branding).then(
                    angular.bind(this, this.addBrandingSuccess),
                    angular.bind(this, this.addBrandingError));
            }
            else {
                this.addUserSuccess(this.user_binder);
            }
        },

        addBrandingSuccess: function(response) {
            this.addBranding({ data: { id: this.user_binder.id } });
        },

        addBrandingError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.addBranding({ data: { id: this.user_binder.id } });
        },

        addUserSuccess: function(user_binder) {
            this.notify.success("Saved");
            this.$modal.close();
            this.onSave(user_binder, this.role);
        },

        addUserError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.processing = false;
        },

        cancel: function() {
            this.$modal.dismiss("cancel");
        },

        onSelect: function() {
            this.agent.partner_users.forEach(angular.bind(this, function(partnerUser) {
                if (this.partnerId && (partnerUser.partner.id == this.partnerId)) {
                    this.access_repair_pricer = partnerUser.access_repair_pricer;
                }
            }));
        }
    };

})();