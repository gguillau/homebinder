describe("BinderFormAgentController", function() {

    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        api,
        $log,
        utils,
        notify,
        cfg,
        loading,
        BinderFormAgentsEditModal;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$compile_, _$templateCache_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $log = _$injector_.get("$log");
        notify = _$injector_.get("Notify");
        utils = _$injector_.get("hb.utils");
        loading = _$injector_.get("Loading");
        BinderFormAgentsEditModal = _$injector_.get("BinderFormAgentsEditModal");

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("BinderFormAgentController", {
            "hb.api": api,
            "hb.utils": utils,
            "Notify": notify,
            "$log": $log,
            "$scope": $scope,
            "BinderFormAgentsEditModal": BinderFormAgentsEditModal
        }, { cfg: cfg });

        $scope.ctrl = ctrl;
        $scope.$apply();
        $scope.$digest();

    }

    describe('ctrl.refresh', function() {
        it('retrieves agents', function() {
            spyOn(loading, "close");
            spyOn(api.binder, "get_acl").and.returnValue($q.when({
                data: [{ role: "buyer_agent" }, { role: "seller_agent" }]
            }));
            spyOn(api.binderBranding, "all").and.returnValue($q.when({
                data: { items: [] }
            }));

            createController();
            $rootScope.$apply();

            expect(api.binder.get_acl).toHaveBeenCalled();
            expect(api.binderBranding.all).toHaveBeenCalled();
        });

        it('calls notify', function() {
            spyOn(notify, "error");
            spyOn(api.binder, "get_acl").and.returnValue($q.reject({
                data: "error"
            }));
            createController();
            $rootScope.$apply();

            expect(notify.error).toHaveBeenCalled();
        });

        it('calls notify', function() {
            spyOn(notify, "error");
            spyOn(api.binder, "get_acl").and.returnValue($q.when({
                data: []
            }));
            spyOn(api.binderBranding, "all").and.returnValue($q.reject({
                data: "error"
            }));

            createController();
            $rootScope.$apply();

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe('ctrl.add', function() {
        it('calls BinderFormAgentsEditModal', function() {
            spyOn(BinderFormAgentsEditModal, "show");
            spyOn(api.binder, "get_acl").and.returnValue($q.when({
                data: []
            }));
            spyOn(api.binderBranding, "all").and.returnValue($q.when({
                data: { items: [] }
            }));

            createController();
            $rootScope.$apply();
            ctrl.add("buyer_agent");
            $rootScope.$apply();

            expect(BinderFormAgentsEditModal.show).toHaveBeenCalled();
        });
    });

    describe('ctrl.edit', function() {
        it('calls BinderFormAgentsEditModal', function() {
            spyOn(BinderFormAgentsEditModal, "show");
            spyOn(api.binder, "get_acl").and.returnValue($q.when({
                data: []
            }));
            spyOn(api.binderBranding, "all").and.returnValue($q.when({
                data: { items: [] }
            }));

            createController();
            $rootScope.$apply();
            ctrl.edit({ role: "buyer_agent", user: {} });
            $rootScope.$apply();

            expect(BinderFormAgentsEditModal.show).toHaveBeenCalled();
        });
    });

    describe('ctrl.insertAgent', function() {
        it('sets the buyer agent', function() {
            spyOn(BinderFormAgentsEditModal, "show");
            spyOn(api.binder, "get_acl").and.returnValue($q.when({
                data: []
            }));
            spyOn(api.binderBranding, "all").and.returnValue($q.when({
                data: { items: [] }
            }));

            createController();
            $rootScope.$apply();
            ctrl.insertAgent({}, "buyer_agent");
            $rootScope.$apply();

            expect(ctrl.buyer_agent).toEqual({ role: "buyer_agent", user: undefined, id: undefined });
        });

        it('sets the seller agent', function() {
            spyOn(BinderFormAgentsEditModal, "show");
            spyOn(api.binder, "get_acl").and.returnValue($q.when({
                data: []
            }));
            spyOn(api.binderBranding, "all").and.returnValue($q.when({
                data: { items: [] }
            }));

            createController();
            $rootScope.$apply();
            ctrl.insertAgent({}, "seller_agent");
            $rootScope.$apply();

            expect(ctrl.seller_agent).toEqual({ role: "seller_agent", user: undefined, id: undefined });
        });
    });

});

describe('binderFormAgents', function() {
    var $scope, $compile, element, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");

        spyOn(api.binder, "get_acl").and.returnValue($q.when({
            data: []
        }));

        spyOn(api.binderBranding, "all").and.returnValue($q.when({
            data: { items: [] }
        }));

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = {};

        element = $compile('<binder-form-agents></binder-form-agents>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {
            expect(element.html()).toContain("Real Estate Agents");
        });
    });
});