(function() {
	"use strict";

	angular
		.module("hb.components.binders.form.transfers", [
				"hb.components.binders.form.transfers.table"
			])
		.directive("binderFormTransfers", function() {
			return {
				restrict: "E",
				templateUrl: "components/binders/form/transfers/transfers.tpl.html",
				controller: "BinderFormTransfersController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("BinderFormTransfersController", [
			"TransfersController",
			"hb.resources",
			BinderFormTransfersController
		]);

	function BinderFormTransfersController(TransfersController, resources) {
		var Model = function() {
			// call the parent class
			TransfersController.call(this);
			this.resources = angular.extend({}, this.resources, resources.bindersTransfers);
            this.refresh();
		};

		Model.prototype = Object.create(TransfersController.prototype);
		this.model = new Model();
	}

})();