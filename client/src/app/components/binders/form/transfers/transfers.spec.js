describe('binderFormTransfers', function() {
    var $scope, $compile, element, context;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        context = _$injector_.get("Context");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(context, "getBinder").and.returnValue({ data: { permissions: { can_read: false } } });

        element = $compile('<binder-form-transfers></binder-form-transfers>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('adds text', function() {
            expect(element.html()).toContain("Transfers -");
        });
    });
});