describe('binderFormTransfersTable', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        element = $compile('<binder-form-transfers-table></binder-form-transfers-table>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the binderFormTransfersTable directive', function() {
        it('shows the the table template', function() {
            expect(element.html()).toContain("glyphicon glyphicon-info-sign");
        });
    });
});