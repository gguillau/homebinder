(function() {
    "use strict";

    angular
        .module("hb.components.binders.form.transfers.table", [])
        .directive("binderFormTransfersTable", function() {
            return {
                restrict: "E",
                templateUrl: "components/binders/form/transfers/table/table.tpl.html"
            };
        });
})();