(function() {
	"use strict";

	angular
		.module("hb.components.binders.form.maintenanceItems", [])
		.factory("BinderFormMaintenanceItemsController", [
			"BinderMaintenanceItemsController",
			"hb.resources",
			"Context",
			"Loading",
			"Notify",
			function(BinderMaintenanceItemsController, resources, context, loading, notify) {
				var Model = function() {
					// call the parent class
					BinderMaintenanceItemsController.call(this);
					this.resources = angular.extend({}, this.resources, resources.bindersMaintenance);
					context.getBinder().then(angular.bind(this, function(binder) {
						this.binder = binder;
						if (this.binder.permissions.can_read) {
							this.init();
						}
						else {
							this.resources.info = this.resources.readOnly;
						}
					}));
				};

				Model.prototype = Object.create(BinderMaintenanceItemsController.prototype);

				angular.extend(Model.prototype, {
					// Refresh callback. Updates the item list and totals
					onRefreshSuccess: function(response) {
						this.items = response.data.items;
						this.totalItems = this.toolbarCfg.total = response.data.total;
						this.items.forEach(angular.bind(this, this.setFrequency));
						loading.close();
					},

					// item was added to the collection
					onAdded: function(item) {
						if (this.binder.permissions.can_read) {
							this.items.push(item);
							this.totalItems = this.toolbarCfg.total += 1;
							this.setFrequency(item);
							notify.success("Added!");
						}
						else {
							notify.success("Successfully added to binder! An email will be sent to the binder owner within the next 2 hours.");
						}
					},

					setFrequency: function(item) {
						// convert interval dropdown for maintenance_cycle
						var frequency = item.interval;
						if (item.maintenance_cycle === "Years") {
							switch (frequency) {
								case 1:
									item.frequency = "Annual";
									break;
								case 2:
									item.frequency = "Every-Other-Year";
									break;
								case 3:
									item.frequency = "Every-3-Years";
									break;
								case 4:
									item.frequency = "Every-4-Years";
									break;
								case 5:
									item.frequency = "Every-5-Years";
									break;
								case 10:
									item.frequency = "Every-10-Years";
									break;
								case 40:
									item.frequency = "Every-40-Years";
									break;
							}
						}
						else if (item.maintenance_cycle === "Months" && item.interval === 6) {
							item.frequency = "Semi-Annual";
						}
						else if (item.maintenance_cycle === "Months" && item.interval === 3) {
							item.frequency = "Quarterly";
						}
						else if (item.maintenance_cycle === "Months") {
							item.frequency = "Monthly";
						}
						else if (!item.maintenance_cycle) {
							item.frequency = "Annual";
						}
						else {
							item.frequency = item.maintenance_cycle;
						}
					}
				});

				return Model;


			}
		])
		.controller("BinderFormComponentMaintenanceItemsController", [
			"BinderFormMaintenanceItemsController",
			"hb.resources",
			"Context",
			"Loading",
			"Notify",
			BinderFormComponentMaintenanceItemsController
		]);

	function BinderFormComponentMaintenanceItemsController(BinderFormMaintenanceItemsController, resources, context, loading, notify) {
		var Model = function() {
			// call the parent class
			BinderFormMaintenanceItemsController.call(this);
		};

		Model.prototype = Object.create(BinderFormMaintenanceItemsController.prototype);

		angular.extend(Model.prototype, {});

		this.model = new Model();
	}

})();