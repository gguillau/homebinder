describe("BinderFormComponentMaintenanceItemsController", function() {
    var controller,
        base,
        notify,
        $q_,
        defer,
        $rootScope_;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getJwt: function() {},
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getUser: function(){}
            };
        });
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q_.defer();
                    defer.resolve({ permissions: { can_read: true } });
                    return defer.promise;
                }
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        $q_ = $q;
        base = $injector.get("BinderFormMaintenanceItemsController");
        notify = $injector.get("Notify");
        $rootScope_ = $rootScope;

        controller = $controller("BinderFormComponentMaintenanceItemsController", {
            "BinderFormMaintenanceItemsController": base
        });
    }));

    describe("onRefreshSuccess", function() {
        it("sets the items", function() {
            controller.model.onRefreshSuccess({ data: { items: [{ id: 1 }] } });

            expect(controller.model.items.length).toEqual(1);
        });
    });

    describe("onAdded", function() {
        it("adds the item", function() {
            spyOn(controller.model, "setFrequency");

            $rootScope_.$apply();
            controller.model.onAdded({ id: 1 });

            expect(controller.model.items.length).toEqual(1);
            expect(controller.model.setFrequency).toHaveBeenCalled();
        });

        it("calls notify", function() {
            spyOn(notify, "success");

            $rootScope_.$apply();
            controller.model.binder.permissions.can_read = false;
            controller.model.onAdded({ id: 1 });

            expect(notify.success).toHaveBeenCalledWith("Successfully added to binder! An email will be sent to the binder owner within the next 2 hours.");
        });
    });

    describe('setFrequency', function() {

        it('should frequency to Annual', function() {
            var item = { interval: 1, maintenance_cycle: "Years" };
            controller.model.setFrequency(item);

            expect(item.frequency).toBe("Annual");
        });

        it('should frequency to Every-Other-Year', function() {
            var item = { interval: 2, maintenance_cycle: "Years" };
            controller.model.setFrequency(item);

            expect(item.frequency).toBe("Every-Other-Year");
        });

        it('should frequency to Every-3-Years', function() {
            var item = { interval: 3, maintenance_cycle: "Years" };
            controller.model.setFrequency(item);

            expect(item.frequency).toBe("Every-3-Years");
        });

        it('should frequency to Every-4-Years', function() {
            var item = { interval: 4, maintenance_cycle: "Years" };
            controller.model.setFrequency(item);

            expect(item.frequency).toBe("Every-4-Years");
        });

        it('should frequency to Every-5-Years', function() {
            var item = { interval: 5, maintenance_cycle: "Years" };
            controller.model.setFrequency(item);

            expect(item.frequency).toBe("Every-5-Years");
        });

        it('should frequency to Every-10-Years', function() {
            var item = { interval: 10, maintenance_cycle: "Years" };
            controller.model.setFrequency(item);

            expect(item.frequency).toBe("Every-10-Years");
        });

        it('should frequency to Every-40-Years', function() {
            var item = { interval: 40, maintenance_cycle: "Years" };
            controller.model.setFrequency(item);

            expect(item.frequency).toBe("Every-40-Years");
        });

        it('should frequency to Monthly', function() {
            var item = { interval: 1, maintenance_cycle: "Months" };
            controller.model.setFrequency(item);

            expect(item.frequency).toBe("Monthly");
        });

        it('should frequency to Annual', function() {
            var item = { interval: 1, maintenance_cycle: null };
            controller.model.setFrequency(item);

            expect(item.frequency).toBe("Annual");
        });

        it('should frequency to Days', function() {
            var item = { interval: 1, maintenance_cycle: "Days" };
            controller.model.setFrequency(item);

            expect(item.frequency).toBe("Days");
        });

        it('should frequency to Semi-Annual', function() {
            var item = { interval: 6, maintenance_cycle: "Months" };
            controller.model.setFrequency(item);

            expect(item.frequency).toBe("Semi-Annual");
        });

        it('should frequency to Quarterly', function() {
            var item = { interval: 3, maintenance_cycle: "Months" };
            controller.model.setFrequency(item);

            expect(item.frequency).toBe("Quarterly");
        });

    });
});