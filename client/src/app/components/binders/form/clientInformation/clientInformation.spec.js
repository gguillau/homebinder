describe('binderFormClientInformation', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        
        element = $compile('<binder-form-client-information></binder-form-client-information>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the binderFormClientInformation directive', function() {
        it('shows the the binderFormClientInformation template', function() {
            expect(element.html()).toContain("Use this form to create binders for your clients");
        });
    });
});