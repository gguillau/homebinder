(function() {
	"use strict";

	angular
		.module("hb.components.binders.form.clientInformation", [])
		.directive("binderFormClientInformation", function() {
			return {
				restrict: "E",
				templateUrl: "components/binders/form/clientInformation/clientInformation.tpl.html"
			};
		});

})();