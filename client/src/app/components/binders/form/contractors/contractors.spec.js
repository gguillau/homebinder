describe("BinderFormComponentBinderContractorsController", function() {
    var ctrl,
        controller,
        $log,
        api,
        notify,
        resources,
        loading,
        $modal,
        $q,
        $rootScope;

    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    var defer = $q.defer();
                    defer.resolve({});
                    return defer.promise;
                }
            };
        });
    }));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
        $rootScope = $injector.get("$rootScope");
        $modal = {
            show: function() {}
        };
    }));

    function createController() {
        ctrl = controller("BinderFormComponentBinderContractorsController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "hb.resources": resources,
            "Loading": loading,
            "ModalService": $modal
        });
    }

    describe("init", function() {
        it("sets the info text", function() {
            createController();
            $rootScope.$apply();

            expect(ctrl.model.resources.title).toEqual("My Home Pros");
        });
    });

});