(function() {
	"use strict";

	angular
		.module("hb.components.binders.form.contractors", [])
		.factory("BinderFormBinderContractorsController", [
			"BinderContractorsController",
			"hb.resources",
			"Context",
			function(BinderContractorsController, resources, context) {
				var Model = function() {
					// call the parent class
					BinderContractorsController.call(this);
					this.resources = angular.extend({}, this.resources, resources.bindersMaintenance);
					this.resources = angular.extend({}, this.resources, resources.bindersContractors);
					context.getBinder().then(angular.bind(this, function(binder) {
						if (binder.permissions.can_read) {
							this.init();
						}
						else {
							this.resources.info = this.resources.readOnly;
						}
					}));
				};

				Model.prototype = Object.create(BinderContractorsController.prototype);

				angular.extend(Model.prototype, {});

				return Model;


			}
		])
		.controller("BinderFormComponentBinderContractorsController", [
			"BinderContractorsController",
			"hb.resources",
			"Context",
			BinderFormComponentBinderContractorsController
		]);

	function BinderFormComponentBinderContractorsController(BinderContractorsController, resources, context) {
		var Model = function() {
			// call the parent class
			BinderContractorsController.call(this);
		};

		Model.prototype = Object.create(BinderContractorsController.prototype);

		this.model = new Model();
	}

})();