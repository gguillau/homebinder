describe("BinderFormBrandingController", function() {

    var $rootScope,
        $scope,
        $controller,
        session,
        ctrl,
        utils,
        $location,
        cfg;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$compile_, _$templateCache_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        utils = _$injector_.get("hb.utils");
        $location = _$injector_.get("$location");
        session = _$injector_.get("Session");


        cfg = {
            configuration: {
                default_user_branding_option: "none"
            }
        };
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("BinderFormBrandingController", {
            "Session": session,
            "hb.utils": utils,
            "$location": $location
        }, { cfg: cfg });

        $scope.ctrl = ctrl;
        $scope.$apply();
        $scope.$digest();
    }

    describe('ctrl.showAdditionalBranding', function() {

        it("returns false", function() {

            createController();
            $rootScope.$apply();
            var value = ctrl.showAdditionalBranding();
            $rootScope.$apply();

            expect(value).toEqual(false);

        });

        it("returns true", function() {

            cfg.configuration.default_user_branding_option = "co_brand";
            createController();
            $rootScope.$apply();
            var value = ctrl.showAdditionalBranding();
            $rootScope.$apply();

            expect(value).toEqual(true);

        });

    });

});

describe('binderFormBranding', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = { configuration: { default_user_branding_option: "user" } };

        element = $compile('<binder-form-branding cfg="cfg"></binder-form-branding>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("Branding");
        });
    });
});