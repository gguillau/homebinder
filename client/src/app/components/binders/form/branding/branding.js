(function() {
    "use strict";

    angular
        .module("hb.components.binders.form.branding", [])
        .directive("binderFormBranding", function() {
            return {
                restrict: "E",
                templateUrl: "components/binders/form/branding/branding.tpl.html",
                controller: "BinderFormBrandingController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    cfg: "="
                }
            };
        })
        .controller("BinderFormBrandingController", [
            "Session",
            "$window",
            "Notify",
            "hb.api",
            "Loading",
            "$log",
            "hb.resources",
            BinderFormBrandingController
        ]);

    function BinderFormBrandingController(session, $window, Notify, api, Loading, $log, resources) {
        this.$window = $window;
        this.notify = Notify;
        this.api = api;
        this.loading = Loading;
        this.$log = $log;
        this.resources = resources.bindersBranding;
    }

    BinderFormBrandingController.prototype = {
        showAdditionalBranding: function(){
            return this.cfg.configuration.default_user_branding_option === "co_brand";
        }
    };
})();