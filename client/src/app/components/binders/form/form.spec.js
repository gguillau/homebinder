describe("BindersFormController", function() {
    var BindersFormController,
        TestClass,
        BindersNewController,
        $q,
        $scope;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the Session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return {
                        role: "admin"
                    };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
    }));
    // get the base class under test
    beforeEach(inject(function($injector, $rootScope) {
        $scope = $rootScope;
        BindersFormController = $injector.get("BindersFormController");
        BindersNewController = $injector.get("BindersNewController");
        $q = $injector.get("$q");
    }));

    function createTestClass() {
        TestClass = function() {
            BindersFormController.call(this);
            BindersNewController.call(this);

            this.currentUser = {
                role: "admin"
            };
            this.resources = angular.extend({}, this.resources, {
                loading: "loading",
                loadError: "load error",
                confirmDelete: "confirm",
                deleteMsg1: "a ",
                deleteMsg2: " b",
                deleteSuccess: "deleted",
                deleteError: "delete error",
                saveSuccess: "SAVE",
                impersonateLoading: "LO"
            });
            this.indexState = "admin.binders";
            this.indexParams = {};
            this.nameProperty = "name";
        };

        TestClass.prototype = Object.create(BindersFormController.prototype);

        angular.extend(TestClass.prototype, BindersNewController.prototype);

        return new TestClass();
    }

    describe('#verifyEmail', function() {
        it('returns true', function() {
            var model = createTestClass();
            spyOn(model.notify, "info");
            model.currentUser = {
                email: "test@gmail.com"
            };
            model.clientInformation = {
                email: "test@gmail.com"
            };
            var result = model.verifyEmail();

            expect(result).toBe(true);
        });

        it('returns false', function() {
            var model = createTestClass();
            spyOn(model.notify, "info");
            model.currentUser = {
                email: "test@gmail.com"
            };
            model.clientInformation = {
                email: "tes1t@gmail.com"
            };
            var result = model.verifyEmail();

            expect(result).toBe(false);
        });
    });

    describe('#initiate', function() {
        it('adds all the links to navigation_links', function() {
            var model = createTestClass();

            model.formCfg.settings.showBinderUsers = true;
            model.formCfg.settings.showBinderAgents = true;
            model.formCfg.settings.showBinderBrandings = true;
            model.formCfg.settings.showBinderPartners = true;
            model.formCfg.settings.showBinderEmails = true;
            model.formCfg.settings.showBinderWarranties = true;
            model.formCfg.settings.showBinderTransactions = true;
            model.formCfg.settings.showBinderSubscriptions = true;
            model.initiate();

            expect(model.navigation_links[0].links.length).toEqual(9);
        });
    });

    describe('#setBinderName', function() {
        it('sets the binder name', function() {
            var model = createTestClass();

            model.clientInformation = {};
            model.clientInformation.name = null;
            model.clientInformation.first = "First";
            model.clientInformation.last = "Last";

            model.setBinderName();

            expect(model.clientInformation.name).toEqual("First Last Home");
        });
    });

    describe('#onSelect', function() {
        it('sets the binder property country', function() {
            var model = createTestClass();
            spyOn(model.address, "setCountry");

            model.clientInformation = {};
            model.clientInformation.state = {};

            model.onSelect({ country: { value: "MA" } });

            expect(model.clientInformation.country).toEqual("MA");
        });
    });

    describe('#onSelectCountry', function() {
        it('sets the binder property country', function() {
            var model = createTestClass();

            model.onSelectCountry({ value: "MA" });

            expect(model.country.value).toEqual("MA");
        });
    });

    describe('#open', function() {
        it('sets the opened attribute at 0 index to true', function() {
            var model = createTestClass();
            model.open(0);

            expect(model.date[0].opened).toBe(true);
        });
    });

    describe('#transferBinder', function() {
        it('calls the transfer modal', inject(function(TransferModal) {
            spyOn(TransferModal, "show");

            var model = createTestClass();
            model.transferBinder();

            expect(TransferModal.show).toHaveBeenCalled();
        }));
    });

    describe('#close', function() {
        it('calls $state go', inject(function(TransferModal) {
            var model = createTestClass();
            spyOn(model.$state, "go");

            model.close();

            expect(model.$state.go).toHaveBeenCalled();
        }));
    });

    describe("#partnerLookup", function() {
        it("loads the partners", function() {
            var model = createTestClass();
            spyOn(model.api.partner, "all").and.returnValue($q.when({
                data: {
                    items: []
                }
            }));

            model.partnerLookup("search");
            $scope.$apply();

            expect(model.api.partner.all).toHaveBeenCalled();
        });
    });

    describe('#setPropertyType', function() {
        it('returns condo', inject(function(TransferModal) {
            var model = createTestClass();

            var propertyType = model.setPropertyType("Apt. 2");

            expect(propertyType).toEqual("Condo");
        }));

        it('returns Single Family', inject(function(TransferModal) {
            var model = createTestClass();

            var propertyType = model.setPropertyType();

            expect(propertyType).toEqual("Single Family");
        }));
    });

});