(function() {
    "use strict";

    angular
        .module("hb.components.binders.form.documents", [])
        .directive("binderFormDocuments", function() {
            return {
                restrict: "E",
                templateUrl: "components/binders/form/documents/documents.tpl.html",
                controller: "BinderFormDocumentsController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("BinderFormDocumentsController", [
            "BinderDocumentsController",
            "hb.resources",
            "Context",
            BinderFormDocumentsController
        ]);

    function BinderFormDocumentsController(BinderDocumentsController, resources, context) {

        this.resources = resources;
        var Model = function() {
            // call the parent class
            BinderDocumentsController.call(this);
            this.resources = angular.extend({}, this.resources, resources.bindersDocuments);
            this.uploadCfg = {
                id: "uploadDocuments",
                fileTypes: "documents",
                url: "/api/v1/documents",
                jwt: this.session.getJwt(),
                params: {
                    binder_id: this.binderId
                },
                multiSelect: true,
                hideUploadButton: true,
                hide_upload: true,
                showTable: false
            };
            context.getBinder().then(angular.bind(this, function(binder) {
                if (binder.permissions.can_read) {
                    this.init();
                }
                else {
                    this.resources.info = this.resources.readOnly;
                }
            }));
        };

        Model.prototype = Object.create(BinderDocumentsController.prototype);

        this.model = new Model();
    }
})();