describe("BinderConfirmModalController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        modals,
        BinderConfirmModal,
        ModalService;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$templateCache_, _$compile_) {
        $rootScope = _$injector_.get("$rootScope");
        BinderConfirmModal = _$injector_.get("BinderConfirmModal");
        ModalService = _$injector_.get("ModalService");
        
        $controller = _$controller_;

        modals = {
            close: function() {},
            dismiss: function() {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("BinderConfirmModalController", {
            "data": { binder: {}, confirm: function() {} },
            "$modalInstance": modals
        });

        $scope.ctrl = ctrl;
        $scope.$apply();
    }

    describe('ctrl.submit', function() {
        it('calls confirm', function() {
            createController();
            spyOn(modals, "close");
            ctrl.submit();

            expect(modals.close).toHaveBeenCalled();
        });

    });

    describe('ctrl.cancel', function() {
        it('calls dismiss', function() {
            createController();
            spyOn(modals, "dismiss");
            ctrl.cancel();

            expect(modals.dismiss).toHaveBeenCalled();
        });

    });

    describe('BinderConfirmModal', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            BinderConfirmModal.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });

});