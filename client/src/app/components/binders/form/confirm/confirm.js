(function() {


    angular
        .module("hb.components.binders.form.confirm", [])
        .factory("BinderConfirmModal", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "components/binders/form/confirm/confirm.tpl.html",
                            controller: "BinderConfirmModalController as ctrl",
                            resolveData: opts,
                            backdrop: true,
                            closed: opts.closed
                        });
                    }
                };
            }
        ])
        .controller("BinderConfirmModalController", [
            "data",
            "$modalInstance",
            "hb.resources",
            BinderConfirmModalController
        ]);

    function BinderConfirmModalController(opts, $modal, resources) {
        this.$modal = $modal;
        this.binder = opts.binder;
        this.confirm = opts.confirm;
        this.resources = resources.common;
    }

    BinderConfirmModalController.prototype = {
        submit: function(){
            this.$modal.close();
            this.confirm();
        },
        
        cancel: function() {
            this.$modal.dismiss("cancel");
        }
    };
})();