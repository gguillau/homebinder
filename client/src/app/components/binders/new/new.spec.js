describe("BindersNewController", function() {
    var BindersNewController,
        TestClass;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {},
                setMessage: function() {}
            };
        });
        // mock the notify service
        $provide.factory("Notify", function() {
            return {
                success: function() {},
                error: function() {},
                info: function() {}
            };
        });
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
        // mock the BinderConfirmModal factory
        $provide.factory("BinderConfirmModal", function() {
            return {
                show: function() {}
            };
        });

    }));

    // get the base class under test
    beforeEach(inject(function($injector) {
        BindersNewController = $injector.get("BindersNewController");
    }));

    function createTestClass() {
        TestClass = function() {
            BindersNewController.call(this);

            this.resources = angular.extend({}, this.resources, {
                loading: "loading",
                loadError: "load error",
                confirmDelete: "confirm",
                deleteMsg1: "a ",
                deleteMsg2: " b",
                deleteSuccess: "deleted",
                deleteError: "delete error",
                saveSuccess: "SAVE",
                impersonateLoading: "LO"
            });
            this.indexState = "admin.binders";
            this.indexParams = {};
            this.nameProperty = "name";
            this.editState = "partner.edit_binder.client";
            this.editParams = {
                partnerId: null,
                binderId: null
            };
        };

        TestClass.prototype = Object.create(BindersNewController.prototype);

        return new TestClass();
    }

    describe("init", function() {
        it("calls getConfiguration", inject(function($q, $rootScope, Notify, $log, $state) {
            var model = createTestClass();
            model.formCfg = {
                currentUser: {
                    role: "inspector"
                },
                partner: {
                    id: 1,
                    address: {}
                }
            };
            spyOn(model, "getConfiguration");
            model.init();

            expect(model.getConfiguration).toHaveBeenCalled();
        }));
    });

    describe("getConfiguration", function() {
        it("calls binderTemplate get", inject(function($q, $rootScope, Notify, $log, $state) {
            var partnerConfiguration = {
                get: function() {}
            };

            spyOn(partnerConfiguration, "get").and.returnValue($q.when({
                data: {}
            }));

            var model = createTestClass();
            model.api.partnerConfiguration = partnerConfiguration;
            model.partner = {
                id: 1,
                configuration: {
                    id: 1
                }
            };
            model.getConfiguration();

            expect(partnerConfiguration.get).toHaveBeenCalled();
        }));
    });

    describe("onGetConfigSuccess", function() {
        it("sets the default template", inject(function(BinderConfirmModal, Loading, $q, $rootScope) {
            var model = createTestClass();
            model.formCfg = {
                templateId: 1
            };
            spyOn(model, "getTemplate");
            var config = {
                default_transaction_type: "buy_side",
                binder_templates: [{ id: 1, name: "not default" }]
            };
            model.onGetConfigSuccess({ data: config });

            expect(model.template.name).toEqual("not default");
            expect(model.getTemplate).toHaveBeenCalled();
        }));

        it("sets the default template", inject(function(BinderConfirmModal, Loading, $q, $rootScope) {
            var model = createTestClass();
            model.formCfg = {};
            spyOn(model, "getTemplate");
            var config = {
                default_binder_template_id: 1,
                default_transaction_type: "buy_side",
                binder_templates: [{ id: 1, name: "real default" }]
            };
            model.onGetConfigSuccess({ data: config });

            expect(model.template.name).toEqual("real default");
            expect(model.getTemplate).toHaveBeenCalled();
        }));
    });

    describe("onGetConfigError", function() {
        it("calls notify", inject(function(BinderConfirmModal, Loading, $q, $rootScope) {
            var model = createTestClass();
            model.formCfg = {};
            spyOn(model.notify, "error");
            model.onGetConfigError({ data: "error" });

            expect(model.notify.error).toHaveBeenCalled();
        }));
    });

    describe("getTemplate", function() {
        it("calls getUsers", inject(function($q, $rootScope, Notify, $log, $state) {
            var model = createTestClass();

            spyOn(model, "getUsers");

            model.getTemplate(true);

            expect(model.getUsers).toHaveBeenCalled();
        }));
    });

    describe("getTemplateConfirm", function() {
        it("calls binderTemplate get", inject(function($q, $rootScope, Notify, $log, $state) {
            var binderTemplate = {
                get: function() {}
            };

            spyOn(binderTemplate, "get").and.returnValue($q.when({
                data: {}
            }));

            var model = createTestClass();
            model.api.binderTemplate = binderTemplate;
            model.template = {
                id: 1
            };
            model.getTemplateConfirm();

            expect(binderTemplate.get).toHaveBeenCalled();
        }));
    });

    describe("getUsers", function() {
        it("calls user all", inject(function($q, $rootScope, Notify, $log, $state) {
            var user = {
                all: function() {}
            };

            spyOn(user, "all").and.returnValue($q.when({
                data: {}
            }));

            var model = createTestClass();
            model.api.user = user;
            model.partner = {
                id: 1
            };
            model.formCfg = {
                currentUser: {
                    role: "inspector"
                }
            };
            model.getUsers();

            expect(user.all).toHaveBeenCalled();
        }));
    });

    describe("onGetUsersSuccess", function() {
        it("sets the default user", inject(function(BinderConfirmModal, Loading, $q, $rootScope) {
            var model = createTestClass();
            model.formCfg = { configuration: { default_user_branding_id: 1 } };
            spyOn(model, "setBranding");
            model.onGetUsersSuccess({ data: { items: [{ id: 1, user_profile_attributes: { id: 1 } }] } });

            expect(model.setBranding).toHaveBeenCalled();
        }));
    });

    describe("onGetUsersError", function() {
        it("calls notify", inject(function(BinderConfirmModal, Loading, $q, $rootScope) {
            var model = createTestClass();
            model.formCfg = {};
            spyOn(model.notify, "error");
            model.onGetUsersError({ data: "error" });

            expect(model.notify.error).toHaveBeenCalled();
        }));
    });

    describe("setBranding", function() {
        it("sets the branding for the broker", inject(function(BinderConfirmModal, Loading, $q, $rootScope) {
            var model = createTestClass();
            model.formCfg = {
                binderOwner: {
                    id: 1,
                    email: "test@gmail.com"
                },
                binderBrandings: {

                },
                configuration: {
                    default_user_branding_option: "user"
                },
                partner: {
                    partner_type: "broker"
                }
            };
            model.setBranding();

            expect(model.formCfg.binderBrandings.maintenance).toEqual(model.formCfg.binderOwner);
        }));
    });

    describe("findHomeowner", function() {
        it("doesnt call the api when form is invalid", inject(function(BinderConfirmModal, Loading, $q, $rootScope) {
            var user = {
                find_by_email: function() {},
                create: function() {}
            };

            spyOn(user, "find_by_email").and.returnValue($q.when({
                data: {}
            }));
            var model = createTestClass();

            spyOn(model, "verifyEmail");
            model.form = { $invalid: true };
            model.api.user = user;
            model.findHomeowner();

            expect(user.find_by_email).not.toHaveBeenCalled();
            expect(model.verifyEmail).not.toHaveBeenCalled();
        }));

        it("doesnt call the api when verify returns ture", inject(function(BinderConfirmModal, Loading, $q, $rootScope) {
            var user = {
                find_by_email: function() {},
                create: function() {}
            };

            spyOn(user, "find_by_email").and.returnValue($q.when({
                data: {}
            }));
            var model = createTestClass();

            spyOn(model, "verifyEmail").and.returnValue(true);
            model.form = { $invalid: false };
            model.api.user = user;
            model.findHomeowner();

            expect(user.find_by_email).not.toHaveBeenCalled();
            expect(model.verifyEmail).toHaveBeenCalled();
        }));

        it("doesnt find homeowner", inject(function(BinderConfirmModal, Loading, $q, $rootScope) {
            var user = {
                find_by_email: function() {},
                create: function() {}
            };

            spyOn(user, "find_by_email").and.returnValue($q.when({
                data: {}
            }));
            var model = createTestClass();
            model.currentUser = {
                email: "etst@gmail.com"
            };
            model.clientInformation = {
                email: "test@gmail.com"
            };
            spyOn(model, "createHomeowner");
            model.form = {};
            model.api.user = user;
            model.findHomeowner();
            $rootScope.$apply();

            expect(user.find_by_email).toHaveBeenCalledWith("test@gmail.com");
            expect(model.createHomeowner).toHaveBeenCalled();
        }));

        it("finds existing homeowner", inject(function(BinderConfirmModal, Loading, $q, $rootScope) {
            var user = {
                find_by_email: function() {}
            };

            spyOn(user, "find_by_email").and.returnValue($q.when({
                data: {
                    id: 1
                }
            }));
            var model = createTestClass();
            model.currentUser = {
                email: "etst@gmail.com"
            };
            model.clientInformation = {
                email: "test@gmail.com"
            };
            spyOn(model, "onCreateHomeownerSuccess");

            model.form = {};
            model.api.user = user;
            model.findHomeowner();
            $rootScope.$apply();

            expect(user.find_by_email).toHaveBeenCalledWith("test@gmail.com");
            expect(model.onCreateHomeownerSuccess).toHaveBeenCalled();
        }));
    });

    describe("createHomeowner", function() {
        it("creates the homeowner", inject(function(BinderConfirmModal, Loading, $q, $rootScope) {
            var user = {
                create: function() {}
            };

            spyOn(user, "create").and.returnValue($q.when({
                data: {
                    id: 1
                }
            }));
            var model = createTestClass();

            model.country = {};
            model.clientInformation = {
                first: "Test",
                last: "Client",
                email: "test@gmail.com",
                phone: {},
                address1: "123 Main Street",
                address2: "",
                city: "Boston",
                state: "MA",
                zip: "02210",
                country: "US"
            };

            spyOn(model, "setBinderBranding");

            model.api.user = user;
            model.currentUser = {
                id: 24
            };
            model.formCfg.binderOwner = { id: 1 };
            model.createHomeowner();
            $rootScope.$apply();

            expect(user.create).toHaveBeenCalled();
            expect(model.setBinderBranding).toHaveBeenCalled();
            expect(model.homeowner).toEqual({
                id: 1
            });
        }));

        it("returns an error", inject(function(BinderConfirmModal, Loading, $q, $rootScope, Notify, $log) {
            var user = {
                create: function() {}
            };

            spyOn(user, "create").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(Notify, "error");

            var model = createTestClass();

            model.country = {};
            model.clientInformation = {
                first: "Test",
                last: "Client",
                email: "test@gmail.com",
                phone: {},
                address1: "123 Main Street",
                address2: "",
                city: "Boston",
                state: "MA",
                zip: "02210",
                country: "US"
            };

            model.api.user = user;
            model.currentUser = {
                id: 24
            };
            model.createHomeowner();
            $rootScope.$apply();

            expect(user.create).toHaveBeenCalled();
            expect(Notify.error).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
        }));
    });

    describe("addUsers", function() {
        it("sets user_binders and partner_binders for buyerAgent", inject(function(BinderConfirmModal, Loading, $q, $rootScope) {
            var model = createTestClass();

            model.formCfg.buyerAgent = {
                id: 12,
                email: "buyerAgent@gmail.com"
            };

            model.formCfg.sellerAgent = {
                id: 12,
                email: "sellerAgent@gmail.com"
            };

            model.formCfg.partner = {
                id: 100
            };

            model.formCfg.binderOwner = {
                id: 10
            };
            model.agentSelection = "buyer_agent";
            model.addUsers();
            $rootScope.$apply();

            expect(model.formCfg.partner_binders.length).toEqual(1);
            expect(model.user_binders.length).toEqual(2);
        }));

        it("sets user_binders and partner_binders for sellerAgent", inject(function(BinderConfirmModal, Loading, $q, $rootScope) {
            var model = createTestClass();

            model.formCfg.buyerAgent = {
                id: 12,
                email: "buyerAgent@gmail.com"
            };

            model.formCfg.sellerAgent = {
                id: 12,
                email: "sellerAgent@gmail.com"
            };

            model.formCfg.partner = {
                id: 100
            };

            model.formCfg.binderOwner = {
                id: 10
            };
            model.agentSelection = "seller_agent";
            model.addUsers();
            $rootScope.$apply();

            expect(model.formCfg.partner_binders.length).toEqual(1);
            expect(model.user_binders.length).toEqual(2);
        }));
    });

    describe("setBinderBranding", function() {
        it("sets the branding users", inject(function(BinderConfirmModal, Loading, $q, $rootScope) {

            var model = createTestClass();

            model.formCfg.buyerAgent = {
                id: 1,
                email: "buyerAgent@gmail.com",
                user_profile_attributes: {
                    id: 1
                }
            };

            model.formCfg.sellerAgent = {
                id: 12,
                email: "sellerAgent@gmail.com"
            };

            model.formCfg.partner = {
                id: 100
            };

            model.formCfg.binderOwner = {
                id: 10,
                user_profile_attributes: {
                    id: 100
                }
            };

            model.formCfg.binderBrandings.agent.maintenance = model.formCfg.buyerAgent;
            model.formCfg.binderBrandings.agent.recall = model.formCfg.buyerAgent;
            model.formCfg.binderBrandings.sellerReport = model.formCfg.buyerAgent;
            model.formCfg.binderBrandings.binder = model.formCfg.binderOwner;
            model.formCfg.binderBrandings.maintenance = model.formCfg.binderOwner;
            model.formCfg.binderBrandings.recall = model.formCfg.binderOwner;

            spyOn(model, "checkExistingBinder");

            model.setBinderBranding();
            $rootScope.$apply();

            expect(model.formCfg.partner_binders.length).toEqual(1);
            expect(model.user_binders.length).toEqual(1);
            expect(model.binder_brandings.length).toEqual(6);

        }));
    });

    describe("findBrandingUser", function() {

        it("retrieves 1 user", inject(function(BinderConfirmModal, Loading) {
            var model = createTestClass();
            model.formCfg.partnerUsers = [{
                email: "findme@gmail.com"
            }];
            var user = model.findBrandingUser({
                email: "findme@gmail.com"
            });

            expect(user).toEqual({
                email: "findme@gmail.com"
            });
        }));
    });

    describe("createBinder", function() {
        it("creates the homeowner", inject(function(BinderConfirmModal, Loading, $q, $rootScope) {
            var binder = {
                create: function() {}
            };

            spyOn(binder, "create").and.returnValue($q.when({
                data: {
                    id: 1
                }
            }));
            var model = createTestClass();

            model.clientInformation = {
                first: "Test",
                last: "Client",
                email: "test@gmail.com",
                phone: "222-222-2222",
                address1: "123 Main Street",
                address2: "",
                city: "Boston",
                state: "MA",
                zip: "02210",
                country: "US",
                apn: "apn",
                county: "Suffolk",
                acres: 2.1,
                sq_ft: 3000,
                year_built: 1960
            };
            model.currentUser = { role: "admin" };
            model.formCfg.partner = { id: 1 };
            model.formCfg.binderTemplate.transfer_note = "Thank you";
            // new Date() would sometimes makes the test fail so we set date to a simple string
            model.formCfg.inspection_date = "test";
            model.binder_brandings.push({
                scope: "share_email",
                partner_id: 1,
                user_branding_id: 10
            });

            model.user_binders = [];
            model.user_binders.push({
                role: "owner",
                user_id: 1
            });

            model.formCfg.partner_binders = [{
                role: "binder",
                partner_id: 1
            }];

            spyOn(model, "createTransfer");

            model.api.binder = binder;
            model.homeowner = { id: 1 };
            model.createBinder();
            $rootScope.$apply();

            expect(binder.create).toHaveBeenCalled();
            expect(model.createTransfer).toHaveBeenCalled();
            expect(model.binder).toEqual({
                id: 1
            });
        }));

        it("returns an error", inject(function(BinderConfirmModal, Loading, $q, $rootScope, Notify, $log) {
            var binder = {
                create: function() {}
            };

            spyOn(binder, "create").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(Notify, "error");

            var model = createTestClass();

            model.clientInformation = {
                first: "Test",
                last: "Client",
                email: "test@gmail.com",
                phone: "222-222-2222",
                address1: "123 Main Street",
                address2: "",
                city: "Boston",
                state: "MA",
                zip: "02210",
                country: "US"
            };

            spyOn(model, "createTransfer");

            model.formCfg.partner = { id: 1 };
            model.api.binder = binder;
            model.homeowner = { id: 1 };
            model.createBinder();
            $rootScope.$apply();

            expect(binder.create).toHaveBeenCalled();
            expect(model.createTransfer).not.toHaveBeenCalled();
            expect(Notify.error).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
        }));
    });

    describe("onCreateBinder", function() {
        it("increment binder count for partner", inject(function(BinderConfirmModal, Loading) {
            var model = createTestClass();
            var context = {
                setPartner: function() {}
            };
            model.context = context;
            model.currentUser = {
                role: "broker"
            };
            model.partner = {
                id: 1,
                binder_count: 0
            };

            spyOn(model, "createTransfer");

            model.onCreateBinder({
                data: {
                    id: 1
                }
            });

            expect(model.partner.binder_count).toEqual(1);
        }));
    });

    describe("createTransfer", function() {
        it("creates the transfer", inject(function($q, $rootScope, Notify, $log, $state) {
            var model = createTestClass();
            var transfer = {
                create: function() {}
            };

            spyOn(transfer, "create").and.returnValue($q.when({
                data: {
                    id: 1
                }
            }));
            spyOn($state, "go");

            model.api.transfer = transfer;
            model.binder = {
                id: 1
            };
            model.currentUser = {
                id: 1
            };
            model.homeowner = {
                id: 1
            };

            model.createTransfer();
            $rootScope.$apply();

            expect(transfer.create).toHaveBeenCalledWith({
                transfer_type: "ownership",
                binder_id: 1,
                status: "created",
                sender_id: 1,
                receiver_id: 1
            });
        }));

        it("returns an error", inject(function($q, $rootScope, Notify, $log, $state) {
            var model = createTestClass();
            var transfer = {
                create: function() {}
            };

            spyOn(transfer, "create").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(Notify, "error");
            spyOn($state, "go");

            model.api.transfer = transfer;
            model.binder = {
                id: 1
            };
            model.currentUser = {
                id: 1
            };
            model.homeowner = {
                id: 1
            };

            model.createTransfer();
            $rootScope.$apply();

            expect(transfer.create).toHaveBeenCalledWith({
                transfer_type: "ownership",
                binder_id: 1,
                status: "created",
                sender_id: 1,
                receiver_id: 1
            });
        }));
    });

    describe("submitComplete", function() {
        it("calls loading close", inject(function(Loading, Notify) {
            var model = createTestClass();

            spyOn(Loading, "close");
            spyOn(Notify, "success");

            model.submitComplete();

            expect(Loading.close).toHaveBeenCalled();
            expect(Notify.success).toHaveBeenCalled();
        }));
    });

    describe("setPropertyType", function() {
        it("returns condo", inject(function(Loading, Notify) {
            var model = createTestClass();

            var result = model.setPropertyType("Apt. 2");

            expect(result).toEqual("Condo");
        }));
    });

});