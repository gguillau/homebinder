(function() {
    "use strict";

    angular.module('hb.components.binders.new', [])
        .factory("BindersNewController", [
            "BindersFormController",
            "$filter",
            "Address",
            "ModalService",
            function(BindersFormController, $filter, Address, modal) {
                var Model = function() {
                    // call the parent class
                    BindersFormController.call(this);
                    this.$filter = $filter;
                    this.saveCall = this.findHomeowner;
                    this.templates = [];
                    this.clientInformation = {
                        phone: {}
                    };
                    this.editParams = {
                        binderId: null
                    };
                    this.initiate();
                };

                Model.prototype = Object.create(BindersFormController.prototype);

                angular.extend(Model.prototype, {
                    init: function() {
                        if (this.formCfg.partner) {
                            this.partner = this.formCfg.partner;
                            this.country = Address.findCountryByCode(this.partner.address.country);
                            this.clientInformation.state = this.partner.address.state;
                            this.clientInformation.country = this.country.value;
                            this.getConfiguration();
                        }
                        // set the owner text
                        if (this.formCfg.currentUser.role != "admin") {
                            this.resources.owner = this.formCfg.currentUser.role;
                        }
                    },

                    getConfiguration: function() {
                        // get the partner configuration object if the partner object is present
                        if (this.partner && this.partner.id) {
                            this.formCfg.partner = this.partner;
                            this.api.partnerConfiguration.get(this.partner.configuration.id).then(
                                angular.bind(this, this.onGetConfigSuccess),
                                angular.bind(this, this.onGetConfigError));
                        }
                    },

                    onGetConfigSuccess: function(response) {
                        var default_templates = [];
                        this.formCfg.configuration = response.data;
                        this.transaction_type = this.formCfg.configuration.default_transaction_type;
                        // check if we are creating a binder from a template
                        if (this.formCfg.templateId) {
                            default_templates = response.data.binder_templates.filter(angular.bind(this, function(template) {
                                return template.id == this.formCfg.templateId;
                            }));
                            this.templates = this.templates.concat(response.data.binder_templates);
                            this.template = default_templates[0];
                        }
                        // create a binder using a default template
                        else {
                            default_templates = response.data.binder_templates.filter(angular.bind(this, function(template) {
                                return template.id == this.formCfg.configuration.default_binder_template_id;
                            }));
                            this.templates = response.data.binder_templates;
                            this.template = default_templates[0];
                        }
                        this.getTemplate(true);
                    },

                    onGetConfigError: function(response) {
                        this.$log.error(response);
                        this.notify.error(response.data);
                    },

                    getTemplate: function(getUsers) {
                        //set the template
                        this.formCfg.binderTemplate = this.template;
                        // only get the users when changing partners
                        if (getUsers === true) {
                            this.getUsers();
                        }
                    },

                    getTemplateConfirm: function() {
                        this.api.binderTemplate.get(this.template.id).then(
                            angular.bind(this, this.getTemplateSuccess),
                            angular.bind(this, this.getTemplateError));
                    },

                    getUsers: function() {
                        var opts = {
                            page: 1,
                            count: 20,
                            role: this.partner.partner_type,
                            partnerId: this.partner.id
                        };
                        if (this.formCfg.currentUser.role != "admin") {
                            opts.searchMethod = "for_partner";
                        }
                        this.api.user.all(opts).then(
                            angular.bind(this, this.onGetUsersSuccess),
                            angular.bind(this, this.onGetUsersError)
                        );
                    },

                    onGetUsersSuccess: function(response) {
                        this.formCfg.partnerUsers = response.data.items;
                        var users = this.formCfg.partnerUsers.filter(angular.bind(this, function(user) {
                            return user.user_profile_attributes.id == this.formCfg.configuration.default_user_branding_id;
                        }));
                        // set the binder owner and binder brandings
                        this.formCfg.binderOwner = users[0];
                        this.setBranding();
                    },

                    onGetUsersError: function(response) {
                        this.$log.error(response);
                        this.notify.error(response.data);
                        this.formCfg.binderOwner = this.formCfg.currentUser;
                    },

                    setBranding: function() {
                        // get the default user branding option
                        var default_branding = this.formCfg.configuration.default_user_branding_option;
                        switch (default_branding) {
                            // if partner has chosen user or co branding then we add the default user
                            case "user":
                            case "co_brand":
                                this.formCfg.binderBrandings.recall =
                                    this.formCfg.binderBrandings.maintenance =
                                    this.formCfg.binderBrandings.binder = this.formCfg.binderOwner;
                                // if partner is a broker then we set add user to seller report
                                // also add partner users to agentOptions
                                if (this.formCfg.partner.partner_type === "broker") {
                                    this.formCfg.binderBrandings.sellerReport = this.formCfg.binderOwner;
                                }
                                break;
                        }
                    },

                    // check whether homeowner already exists
                    findHomeowner: function() {
                        this.form.$submitted = true;
                        if (this.form.$invalid) {
                            return;
                        }
                        if (this.verifyEmail()) {
                            return;
                        }
                        this.api.user.find_by_email(this.clientInformation.email).then(
                            angular.bind(this, this.findHomeownerSuccess),
                            angular.bind(this, this.onCreateError, angular.bind(this, this.createHomeowner))
                        );
                    },

                    findHomeownerSuccess: function(response) {
                        if (response.data && response.data.id) {
                            this.onCreateHomeownerSuccess(response);
                        }
                        else {
                            this.createHomeowner();
                        }
                    },

                    createHomeowner: function() {
                        var homeowner = {
                            email: this.clientInformation.email,
                            password: Math.random().toString(36).substring(4) + Math.random().toString(36).substring(4),
                            role: "homeowner",
                            create_method: this.transaction_type === "sell_side" ? "sell_side_inspection" : "transfer",
                            created_by: this.currentUser.id,
                            user_profile_attributes: {
                                first_name: this.clientInformation.first,
                                last_name: this.clientInformation.last,
                                mobile_phone: "+" + this.country.code + this.clientInformation.phone.national,
                                address_attributes: {
                                    address1: this.clientInformation.address1,
                                    address2: this.clientInformation.address2,
                                    city: this.clientInformation.city,
                                    state: this.clientInformation.state,
                                    zip: this.clientInformation.zip,
                                    country: this.clientInformation.country
                                }
                            }
                        };

                        this.loading.show("Creating homeowner...");
                        this.api.user.create({ user: homeowner }).then(
                            angular.bind(this, this.onCreateHomeownerSuccess),
                            angular.bind(this, this.onCreateHomeownerError)
                        );

                    },

                    onCreateHomeownerSuccess: function(response) {
                        this.homeowner = response.data;
                        this.setBinderBranding();
                    },

                    onCreateHomeownerError: function(response) {
                        this.$log.error(response);
                        this.notify.error(response.data);
                        this.loading.close();
                    },

                    addUsers: function() {
                        // add the user
                        this.user_binders = [{
                            user_id: this.formCfg.binderOwner.id,
                            role: "owner"
                        }];

                        // add the partner
                        if (this.formCfg.partner && this.formCfg.partner.id) {
                            this.formCfg.partner_binders = [{
                                partner_id: this.formCfg.partner.id,
                                role: "binder"
                            }];
                        }

                        if (this.agentSelection && this.agentSelection === "buyer_agent") {
                            // add the user as buyer agent
                            this.user_binders.push({
                                user_id: this.formCfg.binderOwner.id,
                                role: "buyer_agent"
                            });
                        }
                        else if (this.agentSelection && this.agentSelection === "seller_agent") {
                            // add the user as seller agent
                            this.user_binders.push({
                                user_id: this.formCfg.binderOwner.id,
                                role: "seller_agent"
                            });
                        }
                    },

                    setBinderBranding: function() {
                        // add users
                        this.addUsers();

                        // branding user
                        var branding_user = {};

                        if (this.formCfg.binderBrandings.binder) {
                            branding_user = this.findBrandingUser(this.formCfg.binderBrandings.binder);
                            if (branding_user.user_profile_attributes.id) {
                                this.binder_brandings.push({
                                    scope: "binder",
                                    partner_id: this.formCfg.partner ? this.formCfg.partner.id : null,
                                    user_branding_id: branding_user.user_profile_attributes.id
                                });
                            }
                        }
                        if (this.formCfg.binderBrandings.maintenance) {
                            branding_user = this.findBrandingUser(this.formCfg.binderBrandings.maintenance);
                            if (branding_user.user_profile_attributes.id) {
                                this.binder_brandings.push({
                                    scope: "maintenance_email",
                                    partner_id: this.formCfg.partner ? this.formCfg.partner.id : null,
                                    user_branding_id: branding_user.user_profile_attributes.id
                                });
                            }
                        }
                        if (this.formCfg.binderBrandings.recall) {
                            branding_user = this.findBrandingUser(this.formCfg.binderBrandings.recall);
                            if (branding_user.user_profile_attributes.id) {
                                this.binder_brandings.push({
                                    scope: "recall_email",
                                    partner_id: this.formCfg.partner ? this.formCfg.partner.id : null,
                                    user_branding_id: branding_user.user_profile_attributes.id
                                });
                            }
                        }
                        if (this.formCfg.binderBrandings.sellerReport) {
                            branding_user = this.findBrandingUser(this.formCfg.binderBrandings.sellerReport);
                            if (branding_user.user_profile_attributes.id) {
                                this.binder_brandings.push({
                                    scope: "seller_report",
                                    partner_id: this.formCfg.partner ? this.formCfg.partner.id : null,
                                    user_branding_id: branding_user.user_profile_attributes.id
                                });
                            }
                        }

                        // branding for share/transfer emails never changed. they are always the binder owner
                        this.binder_brandings.push({
                            scope: "share_email",
                            partner_id: this.formCfg.partner ? this.formCfg.partner.id : null,
                            user_branding_id: this.formCfg.binderOwner.user_profile_attributes.id
                        });
                        this.binder_brandings.push({
                            scope: "transfer_email",
                            partner_id: this.formCfg.partner ? this.formCfg.partner.id : null,
                            user_branding_id: this.formCfg.binderOwner.user_profile_attributes.id
                        });
                        this.checkExistingBinder();
                    },

                    checkExistingBinder: function() {
                        var data = {
                            property: {
                                address: this.clientInformation.address1,
                                address2: this.clientInformation.address2,
                                city: this.clientInformation.city,
                                state: this.clientInformation.state,
                                postalcode: this.clientInformation.zip,
                                country: this.clientInformation.country
                            },
                            client: {
                                email: this.clientInformation.email
                            }
                        };
                        this.api.binder.checkExisting(data).then(
                            angular.bind(this, this.existingSuccess),
                            angular.bind(this, this.existingError));
                    },

                    existingSuccess: function(response) {
                        this.createBinder();
                    },

                    existingError: function(response) {
                        modal.confirm({
                            message: "A Binder with this property already exists. Would you like to continue creating this binder?",
                            glyphicon: "glyphicon glyphicon-duplicate",
                            title: "Confirm Creation",
                            confirm: angular.bind(this, this.createBinder)
                        });
                    },

                    findBrandingUser: function(user) {
                        if (this.formCfg.binderOwner) {
                            return user;
                        }
                        else {
                            var users = this.$filter("filter")(this.formCfg.partnerUsers, {
                                email: user.email
                            });
                            return users[0];
                        }
                    },

                    createBinder: function() {
                        var property_type = this.setPropertyType(this.clientInformation.address2);

                        var request = {
                            binder: {
                                name: this.clientInformation.first + " " + this.clientInformation.last + " Home",
                                details: this.formCfg.binderTemplate.transfer_note,
                                create_method: "manual",
                                binder_template_id: this.formCfg.binderTemplate.id,
                                property_attributes: {
                                    property_type: property_type,
                                    address1: this.clientInformation.address1,
                                    address2: this.clientInformation.address2,
                                    city: this.clientInformation.city,
                                    state: this.clientInformation.state,
                                    zip: this.clientInformation.zip,
                                    country: this.clientInformation.country,
                                    apn: this.clientInformation.apn,
                                    county: this.clientInformation.county,
                                    acres: this.clientInformation.acres,
                                    sq_ft: this.clientInformation.sq_ft,
                                    year_built: this.clientInformation.year_built
                                }
                            },
                            homeowner_id: this.homeowner.id,
                            inspection_date: this.formCfg.inspection_date,
                            user_binders: this.user_binders,
                            partner_binders: this.formCfg.partner_binders,
                            binder_brandings: this.binder_brandings,
                            binder_template_id: this.formCfg.binderTemplate.id,
                            partner_id: this.formCfg.partner.id,
                            transaction_type: this.transaction_type
                        };

                        this.loading.setMessage("Creating binder...");
                        this.api.binder.create(request).then(
                            angular.bind(this, this.onCreateBinder),
                            angular.bind(this, this.onCreateBinderError)
                        );
                    },

                    onCreateBinder: function(response) {
                        // set the binder
                        this.binder = response.data, this.binderId = response.data.id;
                        this.editParams.binderId = this.binder.id;
                        // if user is a partner, we update the partner binder count for UI purposes
                        if (this.partner) {
                            this.partner.binder_count++;
                            this.context.setPartner(this.partner);
                        }
                        // automatically create the transfer
                        this.createTransfer();
                    },

                    onCreateBinderError: function(response) {
                        this.$log.error(response);
                        this.notify.error(response.data);
                        this.loading.close();
                    },

                    createTransfer: function() {
                        var transfer = {
                            transfer_type: "ownership",
                            binder_id: this.binder.id,
                            status: "created",
                            sender_id: this.currentUser.id,
                            receiver_id: this.homeowner.id
                        };

                        this.api.transfer.create(transfer).then(
                            angular.bind(this, this.submitComplete),
                            angular.bind(this, this.submitComplete)
                        );
                    },

                    submitComplete: function(response) {
                        this.loading.close();
                        this.$state.go(this.editState, this.editParams);
                        this.notify.success("Binder successfully created");
                    }
                });

                return Model;
            }
        ]);
})();
