describe("BinderFormTransactionsController", function() {
    var controller,
        base,
        api,
        $q,
        notify;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getJwt: function() {},
                getLocale: function() {
                    return null;
                },
                setLocale: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector) {
        $q = $injector.get("$q");
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");

        controller = $controller("BinderFormTransactionsController", {
            "hb.framework.indexBase": base,
            "Notify": notify
        });
    }));

    describe("update", function() {
        it("calls api.binderTransaction.update", function() {
            spyOn(api.binderTransaction, "update").and.returnValue($q.when({
                data: "success"
            }));

            controller.model.update({ id: 1 });

            expect(api.binderTransaction.update).toHaveBeenCalled();
        });
    });

    describe("onSuccess", function() {
        it("calls notify success", function() {
            spyOn(notify, "success");

            controller.model.onSuccess({ data: "" });

            expect(notify.success).toHaveBeenCalled();
        });
    });

    describe("onError", function() {
        it("calls notify error", function() {
            spyOn(notify, "error");

            controller.model.onError({ data: "" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

});

describe('binderFormTransactions', function() {
    var $scope, $compile, element, context, $q, api;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        context = _$injector_.get("Context");
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(context, "getBinder").and.returnValue({ data: { permissions: { can_read: false } } });
        spyOn(api.binderTransaction, "all").and.returnValue($q.when({ data: { items: [] } }));

        element = $compile('<binder-form-transactions></binder-form-transactions>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("Binder Transactions");
        });
    });
});