(function() {
	"use strict";

	angular.module('hb.components.binders.edit.transactions', [])
		.directive("binderFormTransactions", function() {
			return {
				restrict: "E",
				templateUrl: "components/binders/edit/transactions/transactions.tpl.html",
				controller: "BinderFormTransactionsController",
				controllerAs: "ctrl"
			};
		})
		.controller("BinderFormTransactionsController", [
			"hb.framework.indexBase",
			"hb.api",
			"hb.resources",
			"$stateParams",
			"Notify",
			"$log",
			BinderFormTransactionsController
		]);

	function BinderFormTransactionsController(IndexBase, api, resources, $stateParams, notify, $log) {

		var Model = function() {
			// call the parent class
			IndexBase.call(this);
			this.api = api;
			// apply sellerReport specific resource strings
			this.resources = angular.extend({}, this.resources, resources.binderTransaction);
			// set the refresh API call
			this.refreshCall = api.binderTransaction.all;
			// set the delete API call
			this.deleteCall = api.binderTransaction.destroy;
			// delete prop
			this.nameProperty = "created_at";
			this.maxSize = 10;
			this.sortOptions = [{
				orderBy: "binder_transactions.created_at",
				order: "desc"
			}, {
				orderBy: "binder_transactions.created_at",
				order: "asc"
			}];
			this.sortOption = this.sortOptions[0];
			this.orderBy = this.sortOption.orderBy;
			this.order = this.sortOption.order;
			this.queryArgs = {
				binder_id: $stateParams.binderId
			};
			this.default_transaction_types = [{
					name: "Buy Side",
					value: "buy_side_inspection"
				},
				{
					name: "Sell Side",
					value: "sell_side_inspection"
				},
				{
					name: "Warranty",
					value: "warranty"
				}
			];
			this.refresh();
		};

		Model.prototype = Object.create(IndexBase.prototype);

		angular.extend(Model.prototype, {

			update: function(transaction) {
				api.binderTransaction.update(transaction.id, transaction).then(
					angular.bind(this, this.onSuccess),
					angular.bind(this, this.onError)
				);
			},

			onSuccess: function(response) {
				notify.success("Updated!");
			},

			onError: function(response) {
				$log.error(response);
				notify.error(response);
			}
		});

		this.model = new Model();
	}

})();