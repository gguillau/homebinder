describe("BindersEditController", function() {
    var BindersFormController,
        TestClass,
        binder,
        BindersEditController,
        utils,
        $q_,
        defer;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {},
                setMessage: function() {}
            };
        });
        // mock the notify service
        $provide.factory("Notify", function() {
            return {
                success: function() {},
                error: function() {}
            };
        });
        // mock the Session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return {
                        role: "admin"
                    };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });

        // mock the Context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q_.defer();
                    defer.resolve({ permissions: { can_read: true } });
                    return defer.promise;
                },
                setBinder: function() {}
            };
        });
    }));

    // get the base class under test
    beforeEach(inject(function($injector, $q) {
        $q_ = $q;
        BindersFormController = $injector.get("BindersFormController");
        BindersEditController = $injector.get("BindersEditController");
        utils = $injector.get("hb.utils");
    }));

    function createTestClass() {
        TestClass = function() {
            BindersFormController.call(this);

            this.currentUser = {
                role: "admin"
            };
            this.resources = angular.extend({}, this.resources, {
                loading: "loading",
                loadError: "load error",
                confirmDelete: "confirm",
                deleteMsg1: "a ",
                deleteMsg2: " b",
                deleteSuccess: "deleted",
                deleteError: "delete error",
                saveSuccess: "SAVE",
                impersonateLoading: "LO"
            });
            this.utils = utils;
            this.today = this.utils.utils.getToday(false);
            this.next_month = this.utils.utils.getNextMonth();
            this.yyyy = this.utils.utils.getYear();
            this.indexState = "admin.binders";
            this.indexParams = {};
            this.nameProperty = "name";
        };

        TestClass.prototype = Object.create(BindersEditController.prototype);

        angular.extend(TestClass.prototype, BindersFormController.prototype);

        return new TestClass();
    }

    binder = {
        id: 1,
        name: "Binder",
        primary: true,
        property: {
            address1: "123 Main Street",
            address2: "Apt. 2",
            property_type: "Single Family",
            city: "City",
            state: "MA",
            country: "US",
            apn: 24,
            acres: 2,
            sq_ft: 2000,
            year_built: 1900,
            zip: "02210",
            county: "County"
        },
        details: "Details",
        transfer: { id: 1, receiver_id: 1 },
        user_binders: [],
        branding_users: [],
        partner_binders: []
    };

    var client = {
        name: "Binder",
        first: "",
        last: "",
        email: "",
        address1: binder.property.address1,
        address2: binder.property.address2,
        property_type: binder.property.property_type,
        city: binder.property.city,
        state: binder.property.state,
        country: binder.property.country,
        zip: binder.property.zip,
        apn: binder.property.apn,
        acres: binder.property.acres,
        sq_ft: binder.property.sq_ft,
        year_built: binder.property.year_built,
        county: binder.property.county
    };

    describe("refresh", function() {
        it("sets the binder", inject(function($q, $rootScope) {
            var model = createTestClass();
            spyOn(model.api.binder, "get").and.returnValue($q.when({ data: binder }));

            spyOn(model, "getTransferToFromBinder");

            model.refresh();
            $rootScope.$apply();

            expect(model.api.binder.get).toHaveBeenCalled();
            expect(model.binder).toEqual(binder);
            expect(model.transfer).toEqual({ id: 1, receiver_id: 1 });
            expect(model.formCfg.binderId).toEqual(1);
            expect(model.getTransferToFromBinder).toHaveBeenCalled();
        }));

        it("sets the form settings", inject(function($q, $rootScope, Context) {
            spyOn(Context, "getBinder").and.returnValue($q.when({ id: 1, permissions: { can_read: false } }));
            var model = createTestClass();

            model.refresh();
            $rootScope.$apply();

            expect(model.formCfg.settings.showBinderAgents).toBe(false);
        }));

        it("returns an error", inject(function($q, $rootScope, $log, Notify) {
            var model = createTestClass();

            spyOn(model.api.binder, "get").and.returnValue($q.reject({ data: "error" }));

            spyOn($log, "error");
            spyOn(Notify, "error");

            model.refresh();
            $rootScope.$apply();

            expect(model.api.binder.get).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
            expect(Notify.error).toHaveBeenCalled();
        }));
    });

    describe("getTransferToFromBinder", function() {
        it("sets the client info", inject(function($q, $rootScope) {
            var model = createTestClass();
            spyOn(model, "getBinderOwner").and.callThrough();
            spyOn(model, "setClientInformation");

            model.formCfg = {};
            model.binder = binder;
            model.binder.access_list = [];
            model.getTransferToFromBinder(binder);
            $rootScope.$apply();

            expect(model.getBinderOwner).toHaveBeenCalled();
            expect(model.setClientInformation).not.toHaveBeenCalled();
            expect(model.clientInformation).toEqual(client);
        }));

        it("sets the client info and calls setClientInformation", inject(function($q, $rootScope) {
            var model = createTestClass();

            var user_binders = [{ role: "owner", user: { id: 1, email: "test@gmail.com", role: "inspector" } }];
            spyOn(model, "getBinderOwner").and.callThrough();
            spyOn(model, "setClientInformation");

            model.formCfg = {};
            model.formCfg.settings = {
                showClientForm: false
            };
            model.binder = binder;
            model.binder.access_list = user_binders;
            model.getTransferToFromBinder(binder);
            $rootScope.$apply();

            expect(model.getBinderOwner).toHaveBeenCalled();
            expect(model.setClientInformation).toHaveBeenCalled();
            expect(model.formCfg.settings.showClientForm).toEqual(true);
        }));
    });

    describe("getBinderOwner", function() {
        it("returns null", inject(function($q, $rootScope) {
            var model = createTestClass();
            model.binder = {};
            model.binder.access_list = [];
            var owner = model.getBinderOwner();
            $rootScope.$apply();

            expect(owner).toBe(null);
        }));

        it("returns the owner", inject(function($q, $rootScope) {
            var model = createTestClass();
            var owner = { role: "owner", user: { id: 1, email: "test@gmail.com" } };
            model.binder = {};
            model.binder.access_list = [owner];
            var value = model.getBinderOwner();
            $rootScope.$apply();

            expect(value).toEqual(owner);
        }));
    });

    describe("setClientInformation", function() {
        it("gets client info from co_owner", inject(function($q, $rootScope) {
            var model = createTestClass();
            var co_owner = { role: "co_owner", user: { id: 1, email: "test@gmail.com", user_profile_attributes: { first_name: "First", last_name: "Last", mobile_phone: "Phone" } } };
            model.binder = binder;
            model.binder.access_list = [co_owner];
            client.first = "First", client.last = "Last", client.phone = "Phone", client.email = "test@gmail.com";
            spyOn(model, "getHomeowner");
            model.setClientInformation(model.binder);
            $rootScope.$apply();

            //expect(model.clientInformation).toEqual(client);
            expect(model.getHomeowner).not.toHaveBeenCalled();
        }));

        it("calls getHomeowner", inject(function($q, $rootScope) {
            var model = createTestClass();
            var owner = { role: "owner", user: { id: 1, email: "test@gmail.com", user_profile_attributes: { first_name: "First", last_name: "Last", mobile_phone: "Phone" } } };
            model.binder = {
                access_list: [owner]
            };
            spyOn(model, "getHomeowner");
            model.setClientInformation(binder);
            $rootScope.$apply();

            expect(model.getHomeowner).toHaveBeenCalled();
        }));
    });

    describe("getHomeowner", function() {
        it("sets the homeowner and readonly to false", inject(function($q, $rootScope) {
            var model = createTestClass();
            var homeowner = {
                id: 1,
                email: "test@gmail.com",
                role: "homeowner",
                user_profile_attributes: {
                    id: 1,
                    first_name: "First",
                    last_name: "Last",
                    mobile_phone: "Phone"
                }
            };
            spyOn(model.api.user, "get").and.returnValue($q.when({ data: homeowner }));

            model.binder = binder;
            model.binder = binder;
            model.getHomeowner(binder);
            client.name = "Binder", client.first = "First", client.last = "Last", client.phone = "Phone", client.email = "test@gmail.com", client.readonly = false;
            $rootScope.$apply();

            expect(model.api.user.get).toHaveBeenCalled();
            expect(model.client).toEqual({
                id: 1,
                email: "test@gmail.com",
                first_name: "First",
                last_name: "Last",
                phone: "Phone"
            });

            expect(model.clientInformation).toEqual(client);
        }));

        it("sets the homeowner and readonly to true", inject(function($q, $rootScope) {
            var model = createTestClass();
            var homeowner = {
                id: 1,
                email: "test@gmail.com",
                sign_in_count: 1,
                user_profile_attributes: {
                    id: 1,
                    first_name: "First",
                    last_name: "Last",
                    mobile_phone: "Phone"
                }
            };
            spyOn(model.api.user, "get").and.returnValue($q.when({ data: homeowner }));

            model.binder = binder;
            model.binder = binder;
            model.getHomeowner(binder);
            client.name = "Binder", client.first = "First", client.last = "Last", client.phone = "Phone", client.email = "test@gmail.com", client.readonly = true;
            $rootScope.$apply();

            expect(model.api.user.get).toHaveBeenCalled();
            expect(model.client).toEqual({
                id: 1,
                email: "test@gmail.com",
                first_name: "First",
                last_name: "Last",
                phone: "Phone"
            });

            expect(model.clientInformation).toEqual(client);
        }));

        it("returns an error", inject(function($q, $rootScope, $log, Notify) {

            spyOn($log, "error");
            spyOn(Notify, "error");

            var model = createTestClass();

            spyOn(model.api.user, "get").and.returnValue($q.reject({ data: "error" }));

            model.binder = binder;
            model.binder = binder;
            model.getHomeowner(binder);
            $rootScope.$apply();

            expect(model.api.user.get).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
            expect(Notify.error).toHaveBeenCalled();
        }));

    });

    describe("updateBinder", function() {
        it("does not updates the binder", inject(function($q, $rootScope) {
            var model = createTestClass();

            spyOn(model.api.binder, "get").and.returnValue($q.when({ data: binder }));
            spyOn(model.api.binder, "update").and.returnValue($q.when({ data: binder }));
            spyOn(model, "verifyEmail").and.returnValue(true);

            model.refresh();
            model.updateBinder();

            expect(model.api.binder.update).not.toHaveBeenCalled();

        }));

        it("updates the binder", inject(function($q, $rootScope) {
            var model = createTestClass();

            spyOn(model.api.binder, "get").and.returnValue($q.when({ data: binder }));
            spyOn(model.api.binder, "update").and.returnValue($q.when({ data: binder }));
            spyOn(model, "updateHomeowner");
            spyOn(model, "verifyEmail").and.returnValue(false);

            model.refresh();
            $rootScope.$apply();
            model.clientInformation = {};
            model.clientInformation.property_type = undefined;
            model.updateBinder();
            $rootScope.$apply();

            expect(model.api.binder.update).toHaveBeenCalled();
            expect(model.updateHomeowner).toHaveBeenCalled();

        }));

        it("returns an error", inject(function($q, $rootScope, $log, Notify) {
            var model = createTestClass();

            spyOn(model.api.binder, "get").and.returnValue($q.when({ data: binder }));
            spyOn(model.api.binder, "update").and.returnValue($q.reject({ data: "error" }));
            spyOn(model, "updateHomeowner");
            spyOn($log, "error");
            spyOn(Notify, "error");

            model.refresh();
            $rootScope.$apply();
            model.updateBinder();
            $rootScope.$apply();

            expect(model.api.binder.update).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
            expect(Notify.error).toHaveBeenCalled();
        }));
    });

    describe("updateHomeowner", function() {
        it("updates the homeowner", inject(function($q, $rootScope) {
            var model = createTestClass();

            spyOn(model.api.binder, "get").and.returnValue($q.when({ data: binder }));
            spyOn(model.api.user, "update").and.returnValue($q.when({ data: client }));

            model.refresh();
            $rootScope.$apply();
            model.country = {
                code: "1"
            };
            model.clientInformation = client;
            model.clientInformation.phone = {};
            model.client = {
                id: 1,
                email: "test@gmail.com",
                user_profile_attributes: {
                    id: 1,
                    first_name: "First",
                    last_name: "Last",
                    mobile_phone: "Phone"
                }
            };
            model.clientInformation.readonly = false;
            model.updateHomeowner();
            $rootScope.$apply();

            expect(model.api.user.update).toHaveBeenCalled();

        }));

        it("doesn't update the homeowner", inject(function($q, $rootScope) {
            var model = createTestClass();

            spyOn(model.api.binder, "get").and.returnValue($q.when({ data: binder }));
            spyOn(model.api.user, "update").and.returnValue($q.when({ data: client }));

            model.refresh();
            $rootScope.$apply();
            model.clientInformation = client;
            model.client = {
                id: 1,
                email: "test@gmail.com",
                user_profile_attributes: {
                    id: 1,
                    first_name: "First",
                    last_name: "Last",
                    mobile_phone: "Phone"
                }
            };
            model.clientInformation.readonly = true;
            model.updateHomeowner();
            $rootScope.$apply();

            expect(model.api.user.update).not.toHaveBeenCalled();

        }));

        it("returns an error", inject(function($q, $rootScope, $log, Notify) {
            var model = createTestClass();

            spyOn(model.api.binder, "get").and.returnValue($q.when({ data: binder }));
            spyOn(model.api.binder, "update").and.returnValue($q.reject({ data: "error" }));
            spyOn(model, "updateHomeowner");
            spyOn($log, "error");
            spyOn(Notify, "error");

            model.refresh();
            $rootScope.$apply();
            model.clientInformation = client;
            model.client = {
                id: 1,
                email: "test@gmail.com",
                user_profile_attributes: {
                    id: 1,
                    first_name: "First",
                    last_name: "Last",
                    mobile_phone: "Phone"
                }
            };
            model.clientInformation.readonly = false;
            model.updateBinder();
            $rootScope.$apply();

            expect(model.api.binder.update).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
            expect(Notify.error).toHaveBeenCalled();
        }));
    });

    describe("onUpdateHomeownerError", function() {
        it("calls complete", inject(function($q, $rootScope) {
            var model = createTestClass();
            spyOn(model.loading, "close");
            model.onUpdateHomeownerError({ data: "error" });
            $rootScope.$apply();

            expect(model.loading.close).toHaveBeenCalled();
        }));
    });

    describe("submitComplete", function() {
        it("calls complete", inject(function($q, $rootScope) {
            var model = createTestClass();
            spyOn(model.loading, "close");
            model.submitComplete({ data: { id: 1 } });
            $rootScope.$apply();

            expect(model.loading.close).toHaveBeenCalled();
        }));
    });

});