describe("BindersEditWarrantiesController", function() {
    var controller,
        base,
        api,
        resources,
        BindersWarrantiesOrderModalForm,
        $q_,
        defer,
        $rootScope_;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q_.defer();
                    defer.resolve({ id: 1 });
                    return defer.promise;
                }
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        $rootScope_ = $rootScope;
        $q_ = $q;
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        BindersWarrantiesOrderModalForm = $injector.get("BindersWarrantiesOrderModalForm");

        spyOn(api.warranty, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));
        spyOn(api.warranty, "destroy").and.returnValue($q.when({}));

        controller = $controller("BindersEditWarrantiesController", {
            "hb.framework.indexBase": base,
            "hb.api": api,
            "hb.resources": resources,
            "BindersWarrantiesOrderModalForm": BindersWarrantiesOrderModalForm
        });

        $rootScope.$apply();
    }));

    describe("orderNew", function() {
        it("calls modal", function() {
            var $scope = $rootScope_.$new();
            spyOn(BindersWarrantiesOrderModalForm, "show");
            controller.model.orderNew();
            $scope.$apply();

            expect(BindersWarrantiesOrderModalForm.show).toHaveBeenCalledWith({
                binder: { id: 1 }
            });
        });
    });
});

describe('binderFormWarranties', function() {
    var $scope, $compile, element, $q, api;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        spyOn(api.warranty, "all").and.returnValue($q.when({ data: { items: [] } }));

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = {};

        element = $compile('<binder-form-warranties></binder-form-warranties>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {
            expect(element.html()).toContain("Warranties");
        });
    });
});