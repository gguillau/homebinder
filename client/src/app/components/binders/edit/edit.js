(function() {
    "use strict";

    angular.module('hb.components.binders.edit', [
            "hb.components.binders.edit.users",
            "hb.components.binders.edit.branding",
            "hb.components.binders.edit.partners",
            "hb.components.binders.edit.emails",
            "hb.components.binders.edit.warranties",
            "hb.components.binders.edit.transactions",
            "hb.components.binders.edit.subscriptions"
        ])
        .factory("BindersEditController", [
            'BindersFormController',
            '$stateParams',
            '$filter',
            'hb.resources',
            'Context',
            "Address",
            function(BindersFormController, $stateParams, $filter, resources, context, address) {
                var Model = function() {
                    // call the parent class
                    BindersFormController.call(this);
                    this.resources = angular.extend({}, this.resources, resources.bindersEdit);
                    this.$stateParams = $stateParams;
                    this.$filter = $filter;
                    this.binderId = this.$stateParams.binderId;
                    this.formCfg.settings.showConfigForm = false;
                    this.formCfg.settings.showPageTitle = false;
                    this.formCfg.settings.showClientForm = false;
                    this.formCfg.settings.showAdditionalLinks = true;
                    this.saveCall = this.updateBinder;
                };

                Model.prototype = Object.create(BindersFormController.prototype);

                angular.extend(Model.prototype, {

                    refresh: function() {
                        context.getBinder().then(angular.bind(this, function(binder) {
                            this.binder = binder;
                            if (!this.binder || this.binder.permissions.can_read) {
                                this.loading.show(this.resources.loading);
                                this.api.binder.get(this.binderId).then(
                                    angular.bind(this, this.getBinderSuccess),
                                    angular.bind(this, this.getBinderError));
                            }
                            else {
                                this.formCfg.settings.showBinderAgents = false;
                                this.formCfg.settings.showBinderWarranties = false;
                                this.formCfg.settings.showBinderTransactions = false;
                                this.formCfg.settings.showBinderSubscriptions = false;
                                this.initiate();
                            }
                        }));
                    },

                    getBinderSuccess: function(response) {
                        context.setBinder(response.data);
                        this.binder = response.data;
                        this.formCfg.binderTemplate.transfer_note = this.binder.details;
                        this.formCfg.binderId = response.data.id;
                        this.transfer = this.binder.transfer;
                        this.country = address.findCountryByCode(this.binder.property.country);
                        this.states = address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                            return state.country.value === this.country.value;
                        }));
                        this.getTransferToFromBinder(response.data);
                    },

                    getBinderError: function(response) {
                        this.$log.error(response);
                        this.notify.error(response.data);
                        this.loading.close();
                    },

                    getTransferToFromBinder: function(binder) {
                        this.clientInformation = {
                            name: this.binder.name,
                            first: "",
                            last: "",
                            email: "",
                            address1: binder.property.address1,
                            address2: binder.property.address2,
                            property_type: binder.property.property_type,
                            city: binder.property.city,
                            state: binder.property.state,
                            zip: binder.property.zip,
                            country: binder.property.country,
                            apn: binder.property.apn,
                            county: binder.property.county,
                            acres: binder.property.acres,
                            sq_ft: binder.property.sq_ft,
                            year_built: binder.property.year_built
                        };
                        var owner = this.getBinderOwner();
                        if (owner != null && (owner.user.role === "insurance" || owner.user.role === "hoa" || owner.user.role === "inspector" || owner.user.role === "broker" || owner.user.role === "agent" || owner.user.role === "lender" || owner.user.role === "homepro" || owner.user.role === "builder" || owner.user.role === "property_manager")) {
                            this.formCfg.settings.showClientForm = true;
                            this.setClientInformation(binder);
                        }
                        this.loading.close();
                    },

                    getBinderOwner: function() {
                        // get an exact
                        var owners = $filter("filter")(this.binder.access_list, {
                            role: "owner"
                        }, true);
                        if (owners.length > 0) {
                            return owners[0];
                        }
                        else {
                            return null;
                        }
                    },

                    setClientInformation: function(binder) {
                        this.client = {};
                        this.binder.access_list.forEach(angular.bind(this, function(user) {
                            if (user.role === "co_owner") {
                                this.client = {
                                    id: user.user.id,
                                    first_name: user.user.user_profile_attributes.first_name,
                                    last_name: user.user.user_profile_attributes.last_name,
                                    email: user.user.email,
                                    phone: user.user.user_profile_attributes.mobile_phone
                                };
                            }
                        }));
                        if (this.client.email) {
                            this.clientInformation = {
                                name: this.binder.name,
                                first: this.client.first_name,
                                last: this.client.last_name,
                                email: this.client.email,
                                phone: this.client.phone,
                                address1: binder.property.address1,
                                address2: binder.property.address2,
                                property_type: binder.property.property_type,
                                city: binder.property.city,
                                state: binder.property.state,
                                zip: binder.property.zip,
                                country: binder.property.country,
                                apn: binder.property.apn,
                                county: binder.property.county,
                                acres: binder.property.acres,
                                sq_ft: binder.property.sq_ft,
                                year_built: binder.property.year_built
                            };
                        }
                        else {
                            this.getHomeowner(binder);
                        }
                    },

                    getHomeowner: function(binder) {
                        if (this.binder.transfer) {
                            this.api.user.get(this.binder.transfer.receiver_id).then(
                                angular.bind(this, this.getHomeownerSuccess, binder),
                                angular.bind(this, this.getHomeownerError));
                        }
                    },

                    getHomeownerSuccess: function(binder, response) {
                        var user = response.data;
                        this.client = {
                            id: user.id,
                            first_name: user.user_profile_attributes.first_name,
                            last_name: user.user_profile_attributes.last_name,
                            email: user.email,
                            phone: user.user_profile_attributes.mobile_phone
                        };
                        this.clientInformation = {
                            name: this.binder.name,
                            first: this.client.first_name,
                            last: this.client.last_name,
                            email: this.client.email,
                            phone: this.client.phone,
                            address1: binder.property.address1,
                            address2: binder.property.address2,
                            property_type: binder.property.property_type,
                            city: binder.property.city,
                            state: binder.property.state,
                            zip: binder.property.zip,
                            country: binder.property.country,
                            apn: binder.property.apn,
                            county: binder.property.county,
                            acres: binder.property.acres,
                            sq_ft: binder.property.sq_ft,
                            year_built: binder.property.year_built
                        };
                        this.clientInformation.readonly = false;
                        if (response.data.sign_in_count > 0 || response.data.role != "homeowner") {
                            this.clientInformation.readonly = true;
                        }
                    },

                    getHomeownerError: function(response) {
                        this.$log.error(response);
                        this.notify.error(response.data);
                    },

                    updateBinder: function() {
                        if (this.verifyEmail()) {
                            return;
                        }
                        this.loading.show(this.resources.updating);

                        var data = {
                            name: this.clientInformation.name,
                            details: this.formCfg.binderTemplate.transfer_note,
                            status: this.binder.binder_status,
                            property_attributes: {
                                property_type: this.clientInformation.property_type,
                                address1: this.clientInformation.address1,
                                address2: this.clientInformation.address2,
                                city: this.clientInformation.city,
                                state: this.clientInformation.state,
                                zip: this.clientInformation.zip,
                                country: this.clientInformation.country,
                                apn: this.clientInformation.apn,
                                county: this.clientInformation.county,
                                acres: this.clientInformation.acres,
                                sq_ft: this.clientInformation.sq_ft,
                                year_built: this.clientInformation.year_built
                            }
                        };
                        this.api.binder
                            .update(this.binder.id, data)
                            .then(
                                angular.bind(this, this.binderUpdateSuccess),
                                angular.bind(this, this.binderUpdateError)
                            );
                    },

                    binderUpdateSuccess: function(response) {
                        this.updateHomeowner();
                    },

                    binderUpdateError: function(response) {
                        this.$log.error(response);
                        this.notify.error(response.data);
                        this.loading.close();
                    },

                    updateHomeowner: function() {
                        if (this.clientInformation.readonly === false) {
                            var homeowner = {
                                email: this.clientInformation.email,
                                user_profile_attributes: {
                                    first_name: this.clientInformation.first,
                                    last_name: this.clientInformation.last,
                                    mobile_phone: "+" + this.country.code + this.clientInformation.phone.national,
                                    address_attributes: {
                                        address1: this.clientInformation.address1,
                                        address2: this.clientInformation.address2,
                                        city: this.clientInformation.city,
                                        state: this.clientInformation.state,
                                        zip: this.clientInformation.zip,
                                        country: this.clientInformation.country
                                    }
                                }
                            };

                            this.loading.setMessage("Updating homeowner...");
                            this.api.user.update(this.client.id, homeowner).then(
                                angular.bind(this, this.onUpdateHomeownerSuccess),
                                angular.bind(this, this.onUpdateHomeownerError)
                            );
                        }
                        else {
                            this.submitComplete();
                        }

                    },

                    onUpdateHomeownerSuccess: function(response) {
                        this.submitComplete();
                    },

                    onUpdateHomeownerError: function(response) {
                        this.$log.error(response);
                        this.notify.error(response.data);
                        this.submitComplete();
                    },

                    submitComplete: function(response) {
                        this.loading.close();
                        this.notify.success(this.resources.saveSuccess);
                    }
                });

                return Model;
            }
        ]);
})();
