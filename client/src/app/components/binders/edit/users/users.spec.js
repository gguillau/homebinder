describe("BindersFormUsersController", function() {

    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        api,
        $log,
        utils,
        notify,
        cfg;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$compile_, _$templateCache_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $log = _$injector_.get("$log");
        notify = _$injector_.get("Notify");
        utils = _$injector_.get("hb.utils");

        cfg = {
            binderId: 1,
            users: []
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("BindersFormUsersController", {
            "hb.api": api,
            "hb.utils": utils,
            "Notify": notify,
            "$log": $log,
            "$scope": $scope
        }, { cfg: cfg });

        $scope.ctrl = ctrl;
        $scope.$apply();
        $scope.$digest();

    }

    beforeEach(function() {
        spyOn(api.userBinder, "all").and.returnValue($q.when({
            data: { items: [], total: 0 }
        }));
    });

    describe('ctrl.getAclError', function() {
        it('calls notify', function() {
            spyOn(notify, "error");

            createController();
            ctrl.getAclError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe('ctrl.lookupRefresh', function() {
        it('calls api user all', function() {
            spyOn(api.user, "all").and.returnValue($q.when({ data: {} }));

            createController();
            ctrl.partner = { id: 1 };
            ctrl.lookupRefresh("search");
            $scope.$apply();

            expect(api.user.all).toHaveBeenCalled();
        });
    });

    describe('ctrl.addUser', function() {

        it('adds a user', function() {

            createController();
            $scope.$apply();
            ctrl.addUser();
            $scope.$apply();

            expect(ctrl.users.length).toEqual(1);
        });
    });

    describe('ctrl.save', function() {

        it('adds a user', function() {
            var access = {
                role: "owner",
                user: {
                    id: 1
                }
            };
            spyOn(api.userBinder, "update").and.returnValue($q.when({
                data: {
                    id: 1,
                    role: "owner",
                    user: {
                        id: 1
                    }
                }
            }));

            createController();
            $scope.$apply();

            access.role = "co_owner";
            ctrl.users.push(access);
            ctrl.save(access, 0);
            $scope.$apply();

            expect(api.userBinder.update).toHaveBeenCalled();
            expect(ctrl.users[0].role).toEqual("owner");
        });

        it('returns an error', function() {
            var access = {
                role: "owner",
                user: {
                    id: 1
                }
            };

            spyOn(api.userBinder, "update").and.returnValue($q.reject({ data: "error" }));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            $scope.$apply();
            ctrl.users.push(access);
            ctrl.save(access, 0);
            $scope.$apply();

            expect(api.userBinder.update).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
        });
    });

    describe('ctrl.remove', function() {

        it('removes a user from the array', function() {
            var access = {
                role: "co_owner",
                user: {
                    id: 1
                },
                newUser: true
            };

            spyOn(api.userBinder, "destroy");

            createController();
            $scope.$apply();
            ctrl.users.push(access);
            ctrl.remove(access, 0);
            $scope.$apply();

            expect(api.userBinder.destroy).not.toHaveBeenCalled();
            expect(ctrl.users.length).toEqual(0);
        });

        it('removes a user from the server', function() {
            var access = {
                id: 1,
                role: "owner",
                user: {
                    id: 1
                },
                newUser: false
            };

            spyOn(api.userBinder, "destroy").and.returnValue($q.when({ data: {} }));

            createController();
            $scope.$apply();
            ctrl.users.push(access);
            ctrl.remove(access, 0);
            $scope.$apply();

            expect(api.userBinder.destroy).toHaveBeenCalled();
            expect(ctrl.users.length).toEqual(0);
        });

        it('return an error', function() {
            var access = {
                id: 1,
                role: "owner",
                user: {
                    id: 1
                },
                newUser: false
            };

            spyOn(api.userBinder, "destroy").and.returnValue($q.reject({ data: "error" }));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            $scope.$apply();
            ctrl.users.push(access);
            ctrl.remove(access, 0);
            $scope.$apply();

            expect(api.userBinder.destroy).toHaveBeenCalled();
            expect(ctrl.users.length).toEqual(1);
            expect(notify.error).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
        });

    });

    describe('ctrl.roleSort', function() {

        it('returns 1', function() {

            createController();
            $scope.$apply();
            var value = ctrl.roleSort({ role: "owner" });
            $scope.$apply();

            expect(value).toEqual(1);
        });

        it('returns 2', function() {

            createController();
            $scope.$apply();
            var value = ctrl.roleSort({ role: "co_owner" });
            $scope.$apply();

            expect(value).toEqual(2);
        });

        it('returns 3', function() {

            createController();
            $scope.$apply();
            var value = ctrl.roleSort({ role: "buyer_agent" });
            $scope.$apply();

            expect(value).toEqual(3);
        });

        it('returns 4', function() {

            createController();
            $scope.$apply();
            var value = ctrl.roleSort({ role: "seller_agent" });
            $scope.$apply();

            expect(value).toEqual(4);
        });

        it('returns 5', function() {

            createController();
            $scope.$apply();
            var value = ctrl.roleSort({ role: "reader" });
            $scope.$apply();

            expect(value).toEqual(5);
        });
    });

    describe('ctrl.showRole', function() {
        it('returns true', function() {

            createController();
            $scope.$apply();
            var value = ctrl.showRole("owner", "owner");
            $scope.$apply();

            expect(value).toEqual(true);
        });

        it('returns true', function() {

            createController();
            $scope.$apply();
            var value = ctrl.showRole("owner", "co_owner");
            $scope.$apply();

            expect(value).toEqual(true);
        });

        it('returns true', function() {

            createController();
            $scope.$apply();
            var value = ctrl.showRole("co_owner", "owner");
            $scope.$apply();

            expect(value).toEqual(true);
        });

        it('returns false', function() {

            createController();
            $scope.$apply();
            ctrl.users.push({ role: "owner" });
            var value = ctrl.showRole("co_owner", "owner");
            $scope.$apply();

            expect(value).toEqual(false);
        });
    });

});

describe('binderFormUsers', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.userBinder, "all").and.returnValue($q.when({
            data: {
                items: [{ id: 1 }]
            }
        }));

        $compile('<binder-form-users></binder-form-users>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls api userBinder all', function() {
            expect(api.userBinder.all).toHaveBeenCalled();
        });
    });
});