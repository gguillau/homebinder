(function() {
    "use strict";

    angular
        .module("hb.components.binders.edit.users.table", [])
        .directive("binderFormUsersTable", function() {
            return {
                restrict: "E",
                templateUrl: "components/binders/edit/users/table/table.tpl.html"

            };
        });
})();