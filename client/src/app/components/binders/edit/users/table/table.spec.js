describe('binderFormUsersTable', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        element = $compile('<binder-form-users-table></binder-form-users-table>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the binderFormUsersTable directive', function() {
        it('shows the the table template', function() {
            expect(element.html()).toContain("glyphicon glyphicon-info-sign");
        });
    });
});