(function() {
    "use strict";

    angular
        .module("hb.components.binders.edit.users", [
            "hb.components.binders.edit.users.table"
        ])
        .directive("binderFormUsers", function() {
            return {
                restrict: "E",
                templateUrl: "components/binders/edit/users/users.tpl.html",
                controller: "BindersFormUsersController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("BindersFormUsersController", [
            "hb.api",
            "hb.utils",
            "Notify",
            "$log",
            "$stateParams",
            "$scope",
            "$filter",
            "Loading",
            "hb.resources",
            BindersFormUsersController
        ]);

    function BindersFormUsersController(api, utils, notify, $log, $stateParams, $scope, $filter, Loading, resources) {
        this.api = api;
        this.$filter = $filter;
        this.notify = notify;
        this.$log = $log;
        this.resources = resources.bindersUsers;
        this.binderId = $stateParams.binderId;
        this.loading = Loading;
        this.binder_roles = [{
            value: "owner",
            name: "Binder Owner"
        }, {
            value: "co_owner",
            name: "Co-Owner"
        }, {
            value: "reader",
            name: "Reader"
        }, {
            value: "buyer_agent",
            name: "Buyer Agent"
        }, {
            value: "seller_agent",
            name: "Seller Agent"
        }];
        this.lookupCfg = {
            placeholder: "Search for user",
            refresh: angular.bind(this, this.lookupRefresh),
            itemTemplate: "components/lookup/user/userDropDownItem.tpl.html",
            selectionTemplate: "components/lookup/user/userSelection.tpl.html"
        };
        this.users = [];
        this.refresh();
    }

    BindersFormUsersController.prototype = {
        refresh: function() {
            this.loading.show("Loading...");
            this.api.userBinder.all({ binder_id: this.binderId }).then(
                angular.bind(this, this.getAclSuccess),
                angular.bind(this, this.getAclError));
        },

        getAclSuccess: function(response) {
            this.users = response.data.items;
            this.loading.close();
        },

        getAclError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.loading.close();
        },

        addUser: function() {
            this.users.push({
                role: null,
                newUser: true,
                saveUser: true
            });
        },

        lookupRefresh: function(value) {
            var opts = {
                page: 1,
                count: 50,
                search: value
            };

            if (this.partner && this.partner.id) {
                opts.role = this.partner.partner_type;
                opts.partnerId = this.partner.id;
            }

            opts.search = value;
            return this.api.user.all(opts).then(
                angular.bind(this, function(response) {
                    return {
                        data: response.data.items
                    };
                })
            );
        },

        save: function(access, index) {
            var data = {
                binder_id: this.binderId,
                user_id: access.user.id,
                role: access.role
            };
            if (access.newUser) {
                this.api.userBinder.create(data).then(
                    angular.bind(this, this.addUserSuccess, access),
                    angular.bind(this, this.addUserError));
            }
            else {
                this.api.userBinder.update(access.id, data).then(
                    angular.bind(this, this.addUserSuccess, access),
                    angular.bind(this, this.addUserError));
            }
        },

        addUserSuccess: function(access, response) {
            var index = this.users.indexOf(access);
            this.users[index] = response.data;
        },

        addUserError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
        },

        remove: function(access, index) {
            if (!access.newUser) {
                this.api.userBinder.destroy(access.id).then(
                    angular.bind(this, this.removeAccessSuccess, access),
                    angular.bind(this, this.removeAccessError));
            }
            else {
                index = this.users.indexOf(access);
                this.users.splice(index, 1);
            }
        },

        removeAccessSuccess: function(access, response) {
            var index = this.users.indexOf(access);
            this.users.splice(index, 1);
        },

        removeAccessError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
        },

        roleSort: function(access) {
            switch (access.role) {
                case "owner":
                    return 1;

                case "co_owner":
                    return 2;

                case "buyer_agent":
                    return 3;

                case "seller_agent":
                    return 4;

                case "reader":
                    return 5;
            }
        },

        showRole: function(userRole, roleOption) {
            if (userRole === roleOption) {
                return true;
            }
            switch (roleOption) {
                case "owner":
                    var roles = this.$filter("filter")(this.users, {
                        role: "owner"
                    }, true);
                    return roles.length < 1;
                default:
                    return true;
            }
        }
    };

})();