describe("BindersFormBrandingListController", function() {

    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        api,
        $log,
        utils,
        notify,
        cfg;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$compile_, _$templateCache_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $log = _$injector_.get("$log");
        notify = _$injector_.get("Notify");
        utils = _$injector_.get("hb.utils");

        cfg = {
            binderId: 1,
            branding_users: []
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("BindersFormBrandingListController", {
            "hb.api": api,
            "hb.utils": utils,
            "Notify": notify,
            "$log": $log,
            "$scope": $scope
        }, { cfg: cfg });

        $scope.ctrl = ctrl;
        $scope.$apply();
        $scope.$digest();
    }

    beforeEach(function() {
        spyOn(api.binderBranding, "all").and.returnValue($q.when({
            data: {
                items: []
            }
        }));
    });

    describe('ctrl.getBrandingsError', function() {
        it('calls notify error', function() {
            spyOn(notify, "error");

            createController();
            ctrl.getBrandingsError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });

    });

    describe('ctrl.addBranding', function() {

        it('adds a branding', function() {
            createController();
            $scope.$apply();
            ctrl.addBranding();
            $scope.$apply();

            expect(ctrl.branding_users.length).toEqual(1);
        });
    });

    describe("ctrl.lookupRefresh", function() {
        it("loads the users", function() {
            spyOn(api.user, "all").and.returnValue($q.when({
                data: {
                    items: []
                }
            }));
            createController();
            ctrl.partner = { id: 1 };
            ctrl.lookupRefresh("search");
            $scope.$apply();

            expect(api.user.all).toHaveBeenCalled();
        });
    });

    describe('ctrl.save', function() {

        it('adds a branding', function() {
            var branding = {
                id: 1,
                scope: "maintenance_email",
                user: {
                    id: 1
                }
            };
            spyOn(api.binderBranding, "create").and.returnValue($q.when({
                data: {
                    id: 1,
                    scope: "maintenance_email",
                    user: {
                        id: 1
                    }
                }
            }));

            createController();
            $scope.$apply();

            branding.scope = "transfer_email";
            ctrl.branding_users.push(branding);
            ctrl.save(branding, 0);
            $scope.$apply();

            expect(api.binderBranding.create).toHaveBeenCalled();
            expect(ctrl.branding_users[0].scope).toEqual("maintenance_email");
        });

        it('returns an error', function() {
            var branding = {
                id: 1,
                scope: "maintenance_email",
                user: {
                    id: 1
                }
            };

            spyOn(api.binderBranding, "create").and.returnValue($q.reject({ data: "error" }));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            $scope.$apply();
            ctrl.branding_users.push(branding);
            ctrl.save(branding, 0);
            $scope.$apply();

            expect(api.binderBranding.create).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
        });
    });

    describe('ctrl.remove', function() {

        it('removes a branding from the array', function() {
            var branding = {
                id: 1,
                scope: "maintenance_email",
                user: {
                    id: 1
                },
                newUser: true
            };

            spyOn(api.binderBranding, "destroy");

            createController();
            $scope.$apply();
            ctrl.branding_users.push(branding);
            ctrl.remove(branding, 0);
            $scope.$apply();

            expect(api.binderBranding.destroy).not.toHaveBeenCalled();
            expect(ctrl.branding_users.length).toEqual(0);
        });

        it('removes a partner from the server', function() {
            var branding = {
                id: 1,
                scope: "maintenance_email",
                user: {
                    id: 1
                },
                newUser: false
            };

            spyOn(api.binderBranding, "destroy").and.returnValue($q.when({ data: {} }));

            createController();
            $scope.$apply();
            ctrl.branding_users.push(branding);
            ctrl.remove(branding, 0);
            $scope.$apply();

            expect(api.binderBranding.destroy).toHaveBeenCalled();
            expect(ctrl.branding_users.length).toEqual(0);
        });

        it('return an error', function() {
            var branding = {
                id: 1,
                scope: "maintenance_email",
                user: {
                    id: 1
                },
                newUser: false
            };

            spyOn(api.binderBranding, "destroy").and.returnValue($q.reject({ data: "error" }));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            $scope.$apply();
            ctrl.branding_users.push(branding);
            ctrl.remove(branding, 0);
            $scope.$apply();

            expect(api.binderBranding.destroy).toHaveBeenCalled();
            expect(ctrl.branding_users.length).toEqual(1);
            expect(notify.error).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
        });

    });

    describe('ctrl.scopeSort', function() {

        it('returns 1', function() {

            createController();
            $scope.$apply();
            var value = ctrl.scopeSort({ scope: "binder" });
            $scope.$apply();

            expect(value).toEqual(1);
        });

        it('returns 2', function() {

            createController();
            $scope.$apply();
            var value = ctrl.scopeSort({ scope: "seller_report" });
            $scope.$apply();

            expect(value).toEqual(2);
        });

        it('returns 3', function() {

            createController();
            $scope.$apply();
            var value = ctrl.scopeSort({ scope: "maintenance_email" });
            $scope.$apply();

            expect(value).toEqual(3);
        });

        it('returns 4', function() {

            createController();
            $scope.$apply();
            var value = ctrl.scopeSort({ scope: "recall_email" });
            $scope.$apply();

            expect(value).toEqual(4);
        });

        it('returns 5', function() {

            createController();
            $scope.$apply();
            var value = ctrl.scopeSort({ scope: "transfer_email" });
            $scope.$apply();

            expect(value).toEqual(5);
        });

        it('returns 6', function() {

            createController();
            $scope.$apply();
            var value = ctrl.scopeSort({ scope: "share_email" });
            $scope.$apply();

            expect(value).toEqual(6);
        });
    });

    describe('ctrl.showScope', function() {

        it('returns true', function() {

            createController();
            $scope.$apply();
            var value = ctrl.showScope("binder", "binder");
            $scope.$apply();

            expect(value).toEqual(true);
        });

        it('returns true', function() {

            createController();
            $scope.$apply();
            var value = ctrl.showScope("seller_report", "binder");
            $scope.$apply();

            expect(value).toEqual(true);
        });

        it('returns false', function() {

            createController();
            $scope.$apply();
            ctrl.branding_users.push({ scope: "binder" });
            var value = ctrl.showScope("seller_report", "binder");
            $scope.$apply();

            expect(value).toEqual(false);
        });

        it('returns true', function() {

            createController();
            $scope.$apply();
            var value = ctrl.showScope("binder", "seller_report");
            $scope.$apply();

            expect(value).toEqual(true);
        });

        it('returns false', function() {

            createController();
            $scope.$apply();
            ctrl.branding_users.push({ scope: "seller_report" });
            var value = ctrl.showScope("binder", "seller_report");
            $scope.$apply();

            expect(value).toEqual(false);
        });

        it('returns true', function() {

            createController();
            $scope.$apply();
            var value = ctrl.showScope("binder", "recall_email");
            $scope.$apply();

            expect(value).toEqual(true);
        });

        it('returns true', function() {

            createController();
            $scope.$apply();
            ctrl.branding_users.push({ scope: "recall_email" });
            var value = ctrl.showScope("binder", "recall_email");
            $scope.$apply();

            expect(value).toEqual(true);
        });

        it('returns false', function() {

            createController();
            $scope.$apply();
            ctrl.branding_users.push({ scope: "recall_email" }, { scope: "recall_email" });
            var value = ctrl.showScope("binder", "recall_email");
            $scope.$apply();

            expect(value).toEqual(false);
        });

        it('returns true', function() {

            createController();
            $scope.$apply();
            var value = ctrl.showScope("binder", "maintenance_email");
            $scope.$apply();

            expect(value).toEqual(true);
        });

        it('returns true', function() {

            createController();
            $scope.$apply();
            ctrl.branding_users.push({ scope: "maintenance_email" });
            var value = ctrl.showScope("binder", "maintenance_email");
            $scope.$apply();

            expect(value).toEqual(true);
        });

        it('returns false', function() {

            createController();
            $scope.$apply();
            ctrl.branding_users.push({ scope: "maintenance_email" }, { scope: "maintenance_email" });
            var value = ctrl.showScope("binder", "maintenance_email");
            $scope.$apply();

            expect(value).toEqual(false);
        });

        it('returns true', function() {

            createController();
            $scope.$apply();
            var value = ctrl.showScope("seller_report", "transfer_email");
            $scope.$apply();

            expect(value).toEqual(true);
        });

        it('returns false', function() {

            createController();
            $scope.$apply();
            ctrl.branding_users.push({ scope: "transfer_email" });
            var value = ctrl.showScope("seller_report", "transfer_email");
            $scope.$apply();

            expect(value).toEqual(false);
        });

        it('returns true', function() {

            createController();
            $scope.$apply();
            var value = ctrl.showScope("seller_report", "share_email");
            $scope.$apply();

            expect(value).toEqual(true);
        });

        it('returns false', function() {

            createController();
            $scope.$apply();
            ctrl.branding_users.push({ scope: "share_email" });
            var value = ctrl.showScope("seller_report", "share_email");
            $scope.$apply();

            expect(value).toEqual(false);
        });
    });

    describe('ctrl.onSelect', function() {

        it('updates the branding in the array', function() {
            var branding = {
                id: 1,
                scope: "maintenance_email",
                user: {
                    id: 1,
                    user_profile_attributes: {
                        id: 1
                    }
                }
            };
            createController();
            $scope.$apply();
            ctrl.branding_users.push(branding);
            ctrl.onSelect(branding, 0);
            $scope.$apply();

            expect(ctrl.branding_users[0].newUser).toEqual(true);
            expect(ctrl.branding_users[0].saveUser).toEqual(true);
            expect(ctrl.branding_users[0].scope).toEqual(null);
            expect(ctrl.branding_users[0].user).toEqual(branding.user.user_profile_attributes);
        });

    });
});

describe('binderFormBrandingList', function() {
    var $scope, $compile, element, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");

        spyOn(api.binderBranding, "all").and.returnValue($q.when({
            data: {
                items: []
            }
        }));

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = {};

        element = $compile('<binder-form-branding-list></binder-form-branding-list>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {
            expect(element.html()).toContain("Binder Brandings");
        });
    });
});