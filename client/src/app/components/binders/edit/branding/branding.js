(function() {
    "use strict";

    angular
        .module("hb.components.binders.edit.branding", [])
        .directive("binderFormBrandingList", function() {
            return {
                restrict: "E",
                templateUrl: "components/binders/edit/branding/branding.tpl.html",
                controller: "BindersFormBrandingListController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("BindersFormBrandingListController", [
            "hb.api",
            "$log",
            "Notify",
            "$filter",
            "hb.resources",
            "Loading",
            "$stateParams",
            BindersFormBrandingListController
        ]);

    function BindersFormBrandingListController(api, $log, notify, $filter, resources, Loading, $stateParams) {
        this.loading = Loading;
        this.api = api;
        this.$log = $log;
        this.notify = notify;
        this.$filter = $filter;
        this.resources = resources.bindersBranding;
        this.binderId = $stateParams.binderId;
        this.branding_scopes = [{
            value: "seller_report",
            name: "Seller Report"
        }, {
            value: "recall_email",
            name: "Recall Email"
        }, {
            value: "binder",
            name: "Binder"
        }, {
            value: "maintenance_email",
            name: "Maintenance Email"
        }, {
            value: "transfer_email",
            name: "Transfer Email"
        }, {
            value: "share_email",
            name: "Share Email"
        }];
        this.lookupCfg = {
            placeholder: "Search for user",
            refresh: angular.bind(this, this.lookupRefresh),
            itemTemplate: "components/lookup/user/userDropDownItem.tpl.html",
            selectionTemplate: "components/lookup/user/userSelection.tpl.html"
        };
        this.branding_users = [];
        this.refresh();
    }

    BindersFormBrandingListController.prototype = {
        refresh: function() {
            this.loading.show("Loading...");
            this.api.binderBranding.all({ binderId: this.binderId }).then(
                angular.bind(this, this.getBrandingsSuccess),
                angular.bind(this, this.getBrandingsError));
        },

        getBrandingsSuccess: function(response) {
            this.branding_users = response.data.items;
            this.loading.close();
        },

        getBrandingsError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.loading.close();
        },

        addBranding: function() {
            this.branding_users.push({
                scope: null,
                newUser: true,
                saveUser: true
            });
        },

        lookupRefresh: function(value) {
            var opts = {
                page: 1,
                count: 50,
                search: value
            };

            if (this.partner && this.partner.id) {
                opts.role = this.partner.partner_type;
                opts.partnerId = this.partner.id;
            }

            opts.search = value;
            return this.api.user.all(opts).then(
                angular.bind(this, function(response) {
                    return {
                        data: response.data.items
                    };
                })
            );
        },

        save: function(branding, index) {
            var data = {
                binder_id: this.binderId,
                user_branding_id: branding.user.id,
                scope: branding.scope
            };
            this.api.binderBranding.create(data).then(
                angular.bind(this, this.addBrandingSuccess, branding),
                angular.bind(this, this.addBrandingError));
        },

        addBrandingSuccess: function(branding, response) {
            var index = this.branding_users.indexOf(branding);
            this.branding_users[index] = response.data;
        },

        addBrandingError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
        },

        remove: function(branding, index) {
            if (!branding.newUser) {
                this.api.binderBranding.destroy(branding.id).then(
                    angular.bind(this, this.removeBrandingSuccess, branding),
                    angular.bind(this, this.removeBrandingError));
            }
            else {
                index = this.branding_users.indexOf(branding);
                this.branding_users.splice(index, 1);

            }
        },

        removeBrandingSuccess: function(branding, response) {
            var index = this.branding_users.indexOf(branding);
            this.branding_users.splice(index, 1);
        },

        removeBrandingError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
        },

        scopeSort: function(branding) {
            switch (branding.scope) {
                case "binder":
                    return 1;

                case "seller_report":
                    return 2;

                case "maintenance_email":
                    return 3;

                case "recall_email":
                    return 4;
                case "transfer_email":
                    return 5;
                case "share_email":
                    return 6;
            }
        },

        /*  checks if there is already a user with the given scopeOption in the branding
            users array. Prevents duplicate scopes from being created */

        showScope: function(brandingScope, scopeOption) {
            var scopes;
            if (brandingScope === scopeOption) {
                return true;
            }
            switch (scopeOption) {
                /* there can only be one binder scope */
                case "binder":
                    scopes = this.$filter("filter")(this.branding_users, {
                        scope: "binder"
                    }, true);
                    return scopes.length < 1;
                    /* there can only be one seller_report scope */
                case "seller_report":
                    scopes = this.$filter("filter")(this.branding_users, {
                        scope: "seller_report"
                    }, true);
                    return scopes.length < 1;
                    /* there can only be two recall_email scopes */
                case "recall_email":
                    scopes = this.$filter("filter")(this.branding_users, {
                        scope: "recall_email"
                    }, true);
                    return scopes.length < 2;
                    /* there can only be two maintenance_email scopes */
                case "maintenance_email":
                    scopes = this.$filter("filter")(this.branding_users, {
                        scope: "maintenance_email"
                    }, true);
                    return scopes.length < 2;
                case "transfer_email":
                    scopes = this.$filter("filter")(this.branding_users, {
                        scope: "transfer_email"
                    }, true);
                    return scopes.length < 1;
                case "share_email":
                    scopes = this.$filter("filter")(this.branding_users, {
                        scope: "share_email"
                    }, true);
                    return scopes.length < 1;
            }
        },

        onSelect: function(user, index) {
            user = {
                newUser: true,
                saveUser: true,
                scope: null,
                user: user.user.user_profile_attributes
            };
            this.branding_users[index] = user;
        }
    };

})();