describe("BindersFormEmailsController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        api,
        $log,
        utils,
        notify;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$compile_, _$templateCache_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $log = _$injector_.get("$log");
        notify = _$injector_.get("Notify");
        utils = _$injector_.get("hb.utils");

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("BindersFormEmailsController", {
            "hb.api": api,
            "hb.utils": utils,
            "Notify": notify,
            "$log": $log,
            "$scope": $scope
        });

        $scope.ctrl = ctrl;
        $scope.$apply();
        $scope.$digest();

    }

    beforeEach(function() {
        spyOn(api.maintenanceItem, "all").and.returnValue($q.when({
            data: {
                items: [{ id: 1 }]
            }
        }));

        spyOn(api.appliance, "all").and.returnValue($q.when({
            data: {
                items: [{ id: 1 }]
            }
        }));
    });

    describe('ctrl.send', function() {

        it('sends an email', function() {

            spyOn(api.maintenanceItem, "sendEmail").and.returnValue($q.when({ data: {} }));
            spyOn(notify, "info");

            createController();
            $scope.$apply();

            var type = {
                item: {
                    id: 1
                },
                disabled: false,
                apiCall: ctrl.api.maintenanceItem.sendEmail
            };
            ctrl.send(type);
            $scope.$apply();

            expect(api.maintenanceItem.sendEmail).toHaveBeenCalled();
            expect(notify.info).toHaveBeenCalled();
        });

        it('sends an email', function() {

            spyOn(api.maintenanceItem, "sendEmail").and.returnValue($q.reject({ data: "error" }));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            $scope.$apply();

            var type = {
                item: {
                    id: 1
                },
                disabled: false,
                apiCall: ctrl.api.maintenanceItem.sendEmail
            };
            ctrl.send(type);
            $scope.$apply();

            expect(api.maintenanceItem.sendEmail).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
        });
    });

    describe('ctrl.getComponentError', function() {
        it('calls notify', function() {
            spyOn(notify, "error");

            createController();
            ctrl.getComponentError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });
});

describe('binderFormEmails', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.maintenanceItem, "all").and.returnValue($q.when({
            data: {
                items: [{ id: 1 }]
            }
        }));

        spyOn(api.appliance, "all").and.returnValue($q.when({
            data: {
                items: [{ id: 1 }]
            }
        }));

        $compile('<binder-form-emails></binder-form-emails>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls api appliance all', function() {
            expect(api.appliance.all).toHaveBeenCalled();
        });
    });
});