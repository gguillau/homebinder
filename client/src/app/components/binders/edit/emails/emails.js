(function() {
    "use strict";

    angular
        .module("hb.components.binders.edit.emails", [])
        .directive("binderFormEmails", function() {
            return {
                restrict: "E",
                templateUrl: "components/binders/edit/emails/emails.tpl.html",
                controller: "BindersFormEmailsController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("BindersFormEmailsController", [
            "hb.api",
            "hb.utils",
            "Notify",
            "$log",
            "$stateParams",
            "hb.resources",
            "Loading",
            "$q",
            BindersFormEmailsController
        ]);

    function BindersFormEmailsController(api, utils, notify, $log, $stateParams, resources, Loading, $q) {
        this.api = api;
        this.notify = notify;
        this.$log = $log;
        this.$stateParams = $stateParams;
        this.resources = resources.bindersEmail;
        this.loading = Loading;
        this.$q = $q;
        this.binderId = $stateParams.binderId;
        this.email_types = [{
            value: "recall_email",
            name: "Recall Email",
            disabled: false,
            apiCall: this.api.appliance.sendEmail,
            item: null
        }, {
            value: "maintenance_email",
            name: "Maintenance Email",
            disabled: false,
            apiCall: this.api.maintenanceItem.sendEmail,
            item: null
        }];
        this.item = null;
        this.refresh();
    }

    BindersFormEmailsController.prototype = {
        refresh: function() {
            var promises = [];
            var opts = { binderId: this.binderId, count: 20 };

            this.loading.show("Loading...");

            promises.push(
                this.api.appliance.all(opts).then(
                    angular.bind(this, this.getApplianceSuccess),
                    angular.bind(this, this.getComponentError)),

                this.api.maintenanceItem.all(opts).then(
                    angular.bind(this, this.getMaintenanceSuccess),
                    angular.bind(this, this.getComponentError))

            );

            this.$q.all(promises).then(
                angular.bind(this, this.closeLoading)
            );
        },

        getApplianceSuccess: function(response) {
            this.appliances = response.data.items;
            if (this.appliances.length > 0) {
                this.email_types[0].item = this.appliances[0];
            }
        },

        getMaintenanceSuccess: function(response) {
            this.items = response.data.items;
            if (this.items.length > 0) {
                this.email_types[1].item = this.items[0];
            }
        },

        getComponentError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
        },

        closeLoading: function() {
            this.loading.close();
        },

        send: function(type) {
            type.disabled = true;
            type.apiCall(type.item.id).then(
                angular.bind(this, this.onSendSuccess, type),
                angular.bind(this, this.onSendError, type));
        },

        onSendSuccess: function(type, response) {
            this.notify.info("Please check your email.");
            type.disabled = false;
        },

        onSendError: function(type, response) {
            this.$log.error(response);
            this.notify.error(response.data);
            type.disabled = false;
        }

    };

})();