describe("BinderFormSubscriptionsController", function() {
    var controller,
        $controller,
        base,
        api,
        $q,
        notify,
        loading,
        session,
        $log,
        AdminSubscriptionsModal;

    // load the homebinder module
    beforeEach(module("homebinder"));

    // create the controller
    beforeEach(inject(function(_$controller_, $injector) {
        $controller = _$controller_;
        $q = $injector.get("$q");
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        loading = $injector.get("Loading");
        session = $injector.get("Session");
        notify = {
            success: function(message) {},
            error: function(message) {}
        };
        $log = {
            error: function(data) {}
        };
        AdminSubscriptionsModal = $injector.get("AdminSubscriptionsModal");
    }));

    function createController() {
        controller = $controller("BinderFormSubscriptionsController", {
            "hb.framework.indexBase": base,
            "Loading": loading,
            "Session": session,
            "hb.api": api,
            "Notify": notify,
            "$log": $log,
            "AdminSubscriptionsModal": AdminSubscriptionsModal
        });

        controller.model.queryArgs.binder_id = 1;
    }

    describe("init", function() {
        it("sets the refreshCall", function() {
            createController();
            expect(controller.model.refreshCall).toEqual(api.subscription.all);
        });
    });

    describe("onRefreshSuccess", function() {
        it("sets the items and subscription", function() {
            createController();

            spyOn(controller.model.loading, "close");

            var data = { items: [{ id: 1, discount: 299 }] };
            controller.model.onRefreshSuccess({ data: data });

            expect(JSON.stringify(controller.model.items)).toEqual(JSON.stringify([{ id: 1, discount: 299, name: "subscription", discountDisplay: "2.99" }]));
            expect(controller.model.subscription).toEqual(jasmine.objectContaining({ id: 1, discount: 299 }));
            expect(controller.model.$stateParams.subscriptionId).toEqual(1);
            expect(controller.model.subscription.name).toEqual("subscription");
            expect(controller.model.subscription.discountDisplay).toEqual("2.99");
            expect(controller.model.items.length).toEqual(1);
            expect(controller.model.loading.close).toHaveBeenCalled();
        });

        it("does not set the items and subscription", function() {
            createController();

            spyOn(controller.model.loading, "close");

            controller.model.onRefreshSuccess({ data: { items: [] } });

            expect(controller.model.items).toEqual([]);
            expect(controller.model.subscription).toBe(undefined);
            expect(controller.model.items.length).toEqual(0);
            expect(controller.model.loading.close).toHaveBeenCalled();
        });
    });

    describe("addSubscription", function() {
        it("shows loading and calls api.subscription.create", function() {
            createController();

            var subscription = {
                binder_id: 1,
                plan_id: "free"
            };

            spyOn(controller.model.loading, "show");
            spyOn(controller.model.api.subscription, "create").and.returnValue($q.when({
                data: subscription
            }));

            controller.model.addSubscription();

            expect(controller.model.loading.show).toHaveBeenCalled();
            expect(controller.model.api.subscription.create).toHaveBeenCalledWith(subscription);
        });
    });

    describe("createSubscriptionSuccess", function() {
        it("shows notify, closes loading, and refreshes", function() {
            createController();

            spyOn(controller.model.notify, "success");
            spyOn(controller.model.loading, "close");
            spyOn(controller.model, "refresh");

            controller.model.createSubscriptionSuccess("response");

            expect(controller.model.notify.success).toHaveBeenCalledWith("Subscription added to binder!");
            expect(controller.model.loading.close).toHaveBeenCalled();
            expect(controller.model.refresh).toHaveBeenCalled();
        });
    });

    describe("createSubscriptionError", function() {
        it("logs error, notifies error, and loading closes", function() {
            createController();

            spyOn(controller.model.$log, "error");
            spyOn(controller.model.notify, "error");
            spyOn(controller.model.loading, "close");
            spyOn(controller.model, "refresh");

            controller.model.createSubscriptionError({ data: "error" });

            expect(controller.model.$log.error).toHaveBeenCalledWith({ data: "error" });
            expect(controller.model.notify.error).toHaveBeenCalledWith("error");
            expect(controller.model.loading.close).toHaveBeenCalled();
            expect(controller.model.refresh).not.toHaveBeenCalled();
        });
    });

    describe("upgradeSubscription", function() {
        it("calls AdminSubscriptionsModal.upgradeSubscription", function() {
            createController();

            spyOn(AdminSubscriptionsModal, "upgradeSubscription").and.returnValue($q.when({
                data: "success"
            }));

            var subscription = { binder: {} };
            controller.model.subscription = subscription;
            controller.model.upgradeSubscription();

            expect(AdminSubscriptionsModal.upgradeSubscription).toHaveBeenCalled();
        });
    });

    describe("downgradeSubscription", function() {
        it("calls loading.show and api.subscription.save", function() {
            createController();

            var subscription = {
                id: 1
            };

            spyOn(controller.model.loading, "show");
            spyOn(controller.model.api.subscription, "save").and.returnValue($q.when({
                data: subscription
            }));

            controller.model.subscription = subscription;
            controller.model.downgradeSubscription();

            expect(controller.model.loading.show).toHaveBeenCalled();
            expect(controller.model.api.subscription.save).toHaveBeenCalledWith("cancel", { id: 1 });
        });
    });

    describe("cancelSubscriptionSuccess", function() {
        it("calls notify.success, loading.close, and refresh", function() {
            createController();

            spyOn(controller.model.notify, "success");
            spyOn(controller.model.loading, "close");
            spyOn(controller.model, "refresh");

            controller.model.cancelSubscriptionSuccess("response");

            expect(controller.model.notify.success).toHaveBeenCalledWith("Subscription downgraded!");
            expect(controller.model.loading.close).toHaveBeenCalled();
            expect(controller.model.refresh).toHaveBeenCalled();
        });
    });

    describe("cancelSubscriptionError", function() {
        it("calls notify.success, loading.close, and refresh", function() {
            createController();

            spyOn(controller.model.$log, "error");
            spyOn(controller.model.notify, "error");
            spyOn(controller.model.loading, "close");
            spyOn(controller.model, "refresh");

            controller.model.cancelSubscriptionError({ data: "error" });

            expect(controller.model.$log.error).toHaveBeenCalledWith({ data: "error" });
            expect(controller.model.notify.error).toHaveBeenCalledWith("error");
            expect(controller.model.loading.close).toHaveBeenCalled();
            expect(controller.model.refresh).not.toHaveBeenCalled();
        });
    });

    describe("onDeleteItemSuccess", function() {
        it("calls notify.success, loading.close, and refresh", function() {
            createController();

            spyOn(controller.model.notify, "success");
            spyOn(controller.model.loading, "close");

            controller.subscription = { id: 1 };
            controller.model.onDeleteItemSuccess("item", "response");

            expect(controller.model.subscription).toEqual(null);
            expect(controller.model.notify.success).toHaveBeenCalledWith("Item has been deleted.");
            expect(controller.model.loading.close).toHaveBeenCalled();
        });
    });
});

describe('binderFormSubscriptions', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.subscription, "all").and.returnValue($q.when({ data: { items: [] } }));

        $compile('<binder-form-subscriptions></binder-form-subscriptions>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls api subscription all', function() {
            expect(api.subscription.all).toHaveBeenCalled();
        });
    });
});