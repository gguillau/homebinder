(function() {
	"use strict";

	angular.module("hb.components.binders.edit.subscriptions", [
			"hb.components.binders.edit.subscriptions.adminModal"
		])
		.directive("binderFormSubscriptions", function() {
			return {
				restrict: "E",
				templateUrl: "components/binders/edit/subscriptions/subscriptions.tpl.html",
				controller: "BinderFormSubscriptionsController",
				controllerAs: "ctrl"
			};
		})
		.controller("BinderFormSubscriptionsController", [
			"hb.framework.indexBase",
			"hb.api",
			"hb.resources",
			"$stateParams",
			"Notify",
			"$log",
			"Loading",
			"Session",
			"AdminSubscriptionsModal",
			BinderFormSubscriptionsController
		]);

	function BinderFormSubscriptionsController(IndexBase, api, resources, $stateParams, notify, $log, loading, session, AdminSubscriptionsModal, UserPaymentController) {

		var Model = function() {
			// call the parent class
			IndexBase.call(this);
			this.api = api;
			this.currentUser = session.getUser();
			this.loading = loading;
			// apply subscriptions specific resource strings
			this.resources = angular.extend({}, this.resources, resources.bindersSubscriptions);
			// $stateParams used in adminModal
			this.$stateParams = $stateParams;
			// set the refresh and delete API calls
			this.refreshCall = api.subscription.all;
			this.deleteCall = api.subscription.destroy;
			// set the name property used in the delete message
			this.nameProperty = "name";
			// get binder_id
			this.queryArgs = {
				binder_id: this.$stateParams.binderId
			};
			this.refresh();
		};

		Model.prototype = Object.create(IndexBase.prototype);

		angular.extend(Model.prototype, {

			// Refresh callback. Updates the item list and totals
			onRefreshSuccess: function(response) {
				this.items = response.data.items;
				if (this.items.length > 0) {
					this.subscription = response.data.items[0];
					this.$stateParams.subscriptionId = this.subscription.id;
					this.subscription.name = "subscription";
					this.subscription.discountDisplay = (this.subscription.discount / 100).toFixed(2);
				}
				this.totalItems = this.toolbarCfg.total = this.items.length;
				this.loading.close();
			},

			// Create a new subscription (add clicked)
			addSubscription: function() {
				var subscription = {
					binder_id: this.queryArgs.binder_id,
					plan_id: "free"
				};
				this.loading.show(this.resources.loading);
				this.api.subscription.create(subscription).then(
					angular.bind(this, this.createSubscriptionSuccess),
					angular.bind(this, this.createSubscriptionError));
			},

			// Create subscription suceeded
			createSubscriptionSuccess: function(response) {
				this.notify.success(this.resources.subscriptionAdded);
				this.loading.close();
				this.refresh();
			},

			// Create subscription failed
			createSubscriptionError: function(response) {
				this.$log.error(response);
				this.notify.error(response.data);
				this.loading.close();
			},

			// Upgrade clicked
			upgradeSubscription: function() {
				AdminSubscriptionsModal.upgradeSubscription({
					binder: this.subscription.binder,
					subscription: this.subscription,
					closed: angular.bind(this, this.refresh)
				});
			},

			// Downgrade clicked
			downgradeSubscription: function() {
				this.loading.show(this.resources.loading);
				this.api.subscription.save("cancel", { id: this.subscription.id }).then(
					angular.bind(this, this.cancelSubscriptionSuccess),
					angular.bind(this, this.cancelSubscriptionError));
			},

			// Update subscription suceeded
			cancelSubscriptionSuccess: function(response) {
				this.notify.success(this.resources.subscriptionDowngraded);
				this.loading.close();
				this.refresh();
			},

			// Update subscription failed
			cancelSubscriptionError: function(response) {
				this.$log.error(response);
				this.notify.error(response.data);
				this.loading.close();
			},

			// Updates the view after a delete
			onDeleteItemSuccess: function(item, response) {
				this.subscription = null;
				this.notify.success(this.resources.deleteSuccess);
				this.loading.close();
			}
		});

		this.model = new Model();
	}

})();