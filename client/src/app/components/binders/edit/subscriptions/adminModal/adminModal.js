(function() {
    "use strict";

    angular
        .module("hb.components.binders.edit.subscriptions.adminModal", [])
        .factory("AdminSubscriptionsModal", [
            "ModalService",
            function(Modals) {
                return {
                    upgradeSubscription: function(opts) {
                        Modals.show({
                            templateUrl: "components/binders/edit/subscriptions/adminModal/upgradeSubscription.tpl.html",
                            controller: "AdminSubscriptionsModalController as ctrl",
                            resolveData: opts,
                            closed: opts.closed
                        });
                    }
                };
            }
        ])
        .controller("AdminSubscriptionsModalController", [
            "UserPayments",
            "$modalInstance",
            "data",
            "hb.resources",
            AdminSubscriptionsModalController
        ]);

    function AdminSubscriptionsModalController(UserPayments, $modal, opts, resources) {
        var Model = function() {
            // Call parent
            UserPayments.call(this);
            this.$modal = $modal;
            this.binder = opts.binder;
            this.subscription = opts.subscription;
            this.subscriptionId = this.subscription.id;
            this.resources = resources.adminSubscriptionsModal;
            this.resources = angular.extend({}, this.resources, resources.subscriptionPayment);
        };
        Model.prototype = Object.create(UserPayments.prototype);

        angular.extend(Model.prototype, {

            // Close modal
            closeModal: function() {
                this.$modal.dismiss("cancel");
            },

            // Subscription successfully updated
            updateSubscriptionSuccess: function(response) {
                this.status = this.isUpgrade ?
                    this.resources.upgraded :
                    this.resources.updated;
                this.notify.info(this.status);
                this.loading.close();
                this.$modal.close();
            }
        });

        this.model = new Model();
    }

})();