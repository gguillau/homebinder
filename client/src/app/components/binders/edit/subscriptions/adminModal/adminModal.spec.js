describe("AdminSubscriptionsModalController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        opts,
        $modal,
        $templateCache,
        $compile,
        template,
        UserPayments;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$templateCache_, _$compile_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $compile = _$compile_;
        $templateCache = _$templateCache_;
        UserPayments = _$injector_.get("UserPayments");

        $modal = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };

        opts = {
            binder: {
                id: 1
            },
            subscription: {
                id: 1
            }
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("AdminSubscriptionsModalController", {
            "$modalInstance": $modal,
            "data": opts,
            "UserPayments": UserPayments
        });

        $scope.ctrl = ctrl;
        template = $templateCache.get("components/binders/edit/subscriptions/adminModal/upgradeSubscription.tpl.html");
        $compile(template)($scope);
    }
    
    describe("ctrl.closeModal", function() {
        it("should call $modal dismiss function", function() {

            spyOn($modal, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.model.closeModal();
            $rootScope.$apply();

            expect($modal.dismiss).toHaveBeenCalledWith('cancel');
        });
    });
    
    describe("ctrl.updateSubscriptionSuccess", function() {
        it("should call notify with upgraded message, loading.close(), and $modal.close()", function() {

            createController();
            
            spyOn(ctrl.model.notify, "info");
            spyOn(ctrl.model.loading, "close");
            spyOn(ctrl.model.$modal, "close");

            ctrl.model.isUpgrade = true;
            ctrl.model.updateSubscriptionSuccess();
            $rootScope.$apply();

            expect(ctrl.model.notify.info).toHaveBeenCalledWith('The subscription on your binder has been upgraded.');
            expect(ctrl.model.loading.close).toHaveBeenCalled();
            expect(ctrl.model.$modal.close).toHaveBeenCalled();
        });
        
        it("should call notify with updated message, loading.close(), and $modal.close()", function() {

            createController();
            
            spyOn(ctrl.model.notify, "info");
            spyOn(ctrl.model.loading, "close");
            spyOn(ctrl.model.$modal, "close");

            ctrl.model.isUpgrade = false;
            ctrl.model.updateSubscriptionSuccess();
            $rootScope.$apply();

            expect(ctrl.model.notify.info).toHaveBeenCalledWith('Your payment information has been updated');
            expect(ctrl.model.loading.close).toHaveBeenCalled();
            expect(ctrl.model.$modal.close).toHaveBeenCalled();
        });
    });
});
