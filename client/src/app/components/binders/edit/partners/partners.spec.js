describe("BindersFormPartnersListController", function() {

    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        api,
        $log,
        utils,
        notify,
        cfg,
        RepairPricerModal;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$compile_, _$templateCache_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $log = _$injector_.get("$log");
        notify = _$injector_.get("Notify");
        utils = _$injector_.get("hb.utils");
        RepairPricerModal = _$injector_.get("RepairPricerModal");

        cfg = {
            binderId: 1,
            partner_binders: []
        };

    }));

    beforeEach(function() {
        spyOn(api.partnerBinder, "all").and.returnValue($q.when({
            data: {
                items: []
            }
        }));
    });

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("BindersFormPartnersListController", {
            "hb.api": api,
            "hb.utils": utils,
            "Notify": notify,
            "$log": $log,
            "$scope": $scope,
            "RepairPricerModal": RepairPricerModal
        }, { cfg: cfg });

        $scope.ctrl = ctrl;
        $scope.$apply();
        $scope.$digest();

    }

    describe('ctrl.getItemsError', function() {
        it('calls notify error', function() {
            spyOn(notify, "error");

            createController();
            ctrl.getItemsError({ data: {} });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe('ctrl.addPartner', function() {

        it('increments partner_binders length', function() {

            createController();
            $scope.$apply();
            ctrl.addPartner();
            $scope.$apply();

            expect(ctrl.partner_binders.length).toEqual(1);
        });
    });

    describe('ctrl.save', function() {

        it('adds a partner', function() {
            var binder = {
                id: 0,
                partner: {
                    id: 1
                },
                homeowner: {
                    id: 1
                }
            };
            spyOn(api.binder, "add_partner").and.returnValue($q.when({ data: { id: 1, partner: { id: 1 } } }));
            createController();
            $scope.$apply();
            ctrl.partner_binders.push(binder);
            ctrl.save(binder, 0);
            $scope.$apply();

            expect(api.binder.add_partner).toHaveBeenCalled();
            expect(ctrl.partner_binders[0].id).toEqual(1);
        });

        it('returns an error', function() {
            var binder = {
                id: 0,
                partner: {
                    id: 1
                },
                homeowner: {
                    id: 1
                }
            };

            spyOn(api.binder, "add_partner").and.returnValue($q.reject({ data: "error" }));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            $scope.$apply();
            ctrl.partner_binders.push(binder);
            ctrl.save(binder, 0);
            $scope.$apply();

            expect(api.binder.add_partner).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
        });
    });

    describe('ctrl.remove', function() {

        it('removes a partner from the array', function() {
            var binder = {
                id: 0,
                partner: {
                    id: 1
                },
                homeowner: {
                    id: 1
                },
                newPartner: true
            };

            spyOn(api.binder, "remove_partner");

            createController();
            $scope.$apply();
            ctrl.partner_binders.push(binder);
            ctrl.remove(binder, 0);
            $scope.$apply();

            expect(api.binder.remove_partner).not.toHaveBeenCalled();
            expect(ctrl.partner_binders.length).toEqual(0);
        });

        it('removes a partner from the server', function() {
            var binder = {
                id: 0,
                partner: {
                    id: 1
                },
                homeowner: {
                    id: 1
                }
            };

            spyOn(api.binder, "remove_partner").and.returnValue($q.when({ data: {} }));

            createController();
            $scope.$apply();
            ctrl.partner_binders.push(binder);
            ctrl.remove(binder, 0);
            $scope.$apply();

            expect(api.binder.remove_partner).toHaveBeenCalled();
            expect(ctrl.partner_binders.length).toEqual(0);
        });

        it('return an error', function() {
            var binder = {
                id: 0,
                partner: {
                    id: 1
                },
                homeowner: {
                    id: 1
                }
            };

            spyOn(api.binder, "remove_partner").and.returnValue($q.reject({ data: "error" }));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            $scope.$apply();
            ctrl.partner_binders.push(binder);
            ctrl.remove(binder, 0);
            $scope.$apply();

            expect(api.binder.remove_partner).toHaveBeenCalled();
            expect(ctrl.partner_binders.length).toEqual(1);
            expect(notify.error).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
        });

    });

    describe('ctrl.onSelect', function() {

        it('updates the partner binder in the array', function() {
            var binder = {
                id: 0,
                partner: {
                    id: 1
                },
                homeowner: {
                    id: 1
                }
            };
            createController();
            $scope.$apply();
            ctrl.partner_binders.push(binder);
            ctrl.onSelect(binder, 0);
            $scope.$apply();

            expect(ctrl.partner_binders[0].newPartner).toEqual(true);
            expect(ctrl.partner_binders[0].savePartner).toEqual(true);
            expect(ctrl.partner_binders[0].role).toEqual("binder");
            expect(ctrl.partner_binders[0].partner).toEqual(binder.partner);
        });

    });

    describe('ctrl.canSave', function() {

        it('returns false', function() {
            var binder = {
                id: 0,
                partner: {
                    id: 1
                },
                homeowner: {
                    id: 1
                }
            };
            createController();
            $scope.$apply();
            var result = ctrl.canSave(binder);
            $scope.$apply();

            expect(result).toEqual(false);
        });

        it('returns true', function() {
            var binder = {
                id: 0
            };
            createController();
            $scope.$apply();
            var result = ctrl.canSave(binder);
            $scope.$apply();

            expect(result).toEqual(true);
        });

    });

    describe("lookupRefresh", function() {
        it("loads the partners", function() {
            spyOn(api.partner, "all").and.returnValue($q.when({
                data: {
                    items: []
                }
            }));
            createController();
            ctrl.lookupRefresh();
            $scope.$apply();

            expect(api.partner.all).toHaveBeenCalled();
        });
    });

    describe("homeownerLookupRefresh", function() {
        it("loads the users", function() {
            spyOn(api.user, "all").and.returnValue($q.when({
                data: {
                    items: []
                }
            }));
            createController();
            ctrl.homeownerLookupRefresh();
            $scope.$apply();

            expect(api.user.all).toHaveBeenCalled();
        });
    });

    describe("ctrl.orderRP", function() {
        it("calls RepairPricerModal show", function() {
            createController();
            spyOn(RepairPricerModal, "show");

            var partnerBinder = {
                id: 1,
                homeowner: {
                    user_profile_attributes: {
                        address_attributes: {}
                    }
                },
                binder: {
                    property: {}
                },
                partner: {
                    configuration: {
                        repair_pricer_enabled: true,
                        repair_pricer_id: 1
                    }
                }
            };
            ctrl.orderRP(partnerBinder);

            expect(RepairPricerModal.show).toHaveBeenCalled();
        });

    });
});

describe('binderFormPartnersList', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_, _$q_) {
        api = _$injector_.get("hb.api");
        $q = _$q_;
        spyOn(api.partnerBinder, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    partner: {
                        name: "name",
                        configuration: {}
                    }
                }]
            }
        }));
        $scope = $rootScope.$new(), $compile = _$compile_;

        $compile('<binder-form-partners-list></binder-form-partners-list>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the title', function() {
            expect(api.partnerBinder.all).toHaveBeenCalled();
        });
    });
});