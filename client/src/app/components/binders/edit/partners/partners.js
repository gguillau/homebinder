(function() {
    "use strict";

    angular
        .module("hb.components.binders.edit.partners", [])
        .directive("binderFormPartnersList", function() {
            return {
                restrict: "E",
                templateUrl: "components/binders/edit/partners/partners.tpl.html",
                controller: "BindersFormPartnersListController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("BindersFormPartnersListController", [
            "hb.api",
            "Notify",
            "$log",
            "hb.resources",
            "Loading",
            "$stateParams",
            "RepairPricerModal",
            BindersFormPartnersListController
        ]);

    function BindersFormPartnersListController(api, notify, $log, resources, Loading, $stateParams, RepairPricerModal) {
        this.api = api;
        this.notify = notify;
        this.$log = $log;
        this.resources = resources.bindersPartners;
        this.loading = Loading;
        this.binderId = $stateParams.binderId;
        this.partner_roles = [{
            value: "binder",
            name: "Binder"
        }];
        this.lookupCfg = {
            placeholder: "Search for partner",
            refresh: angular.bind(this, this.lookupRefresh),
            itemTemplate: "components/lookup/partner/partnerDropDownItem.tpl.html",
            selectionTemplate: "components/lookup/partner/partnerSelection.tpl.html"
        };
        this.homeownerLookupCfg = {
            placeholder: "Search for client",
            refresh: angular.bind(this, this.homeownerLookupRefresh),
            itemTemplate: "components/lookup/user/userDropDownItem.tpl.html",
            selectionTemplate: "components/lookup/user/userSelection.tpl.html"
        };
        this.RepairPricerModal = RepairPricerModal;
        this.refresh();
    }

    BindersFormPartnersListController.prototype = {
        orderRP: function(pb) {
            pb.homeowner.user_profile_attributes.address_attributes = pb.binder.property;
            this.RepairPricerModal.show({
                user: pb.homeowner,
                binder_id: pb.binder_id,
                partner_id: pb.partner_id,
                report_type: pb.partner.configuration.default_repair_pricer_report_type,
                repair_pricer_pre_paid_reports: pb.partner.configuration.repair_pricer_pre_paid_reports
            });
        },

        refresh: function() {
            this.loading.show("Loading...");
            this.api.partnerBinder.all({ binderId: this.binderId }).then(
                angular.bind(this, this.getItemsSuccess),
                angular.bind(this, this.getItemsError));
        },

        getItemsSuccess: function(response) {
            this.partner_binders = response.data.items;
            if (this.partner_binders.length > 0) {
                this.partner_binder = this.partner_binders[0];
                this.partner = this.partner_binder.partner;

                if (this.partner.configuration.repair_pricer_enabled) {
                    this.user = this.partner_binder.homeowner;

                    var address2 = this.user.user_profile_attributes.address_attributes.address2 ? this.user.user_profile_attributes.address_attributes.address2 : "",
                        link = this.setLink(),
                        repair_pricer_id = this.partner.configuration.repair_pricer_id ? this.partner.configuration.repair_pricer_id : "";

                    link += "yourName3[first]=" + this.user.user_profile_attributes.first_name + "&";
                    link += "yourName3[last]=" + this.user.user_profile_attributes.last_name + "&";
                    link += "phoneNumber[full]=" + this.user.user_profile_attributes.mobile_phone.international + "&";
                    link += "yourEmail=" + this.user.email + "&";
                    link += "propertyAddress[addr_line1]=" + this.user.user_profile_attributes.address_attributes.address1 + "&";
                    link += "propertyAddress[addr_line2]=" + address2 + "&";
                    link += "propertyAddress[city]=" + this.user.user_profile_attributes.address_attributes.city + "&";
                    link += "propertyAddress[state]=" + this.user.user_profile_attributes.address_attributes.state + "&";
                    link += "propertyAddress[postal]=" + this.user.user_profile_attributes.address_attributes.zip + "&";
                    link += "haveA=" + repair_pricer_id;

                    this.partner_binders[0].repair_pricer_link = link;
                }

            }
            this.loading.close();
        },

        getItemsError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
            this.loading.close();
        },

        setLink: function() {
            return this.partner.configuration.repair_pricer_pre_paid_reports ? "https://form.jotform.com/81105892377159?" : "https://form.jotform.com/80565948184166?";
        },

        addPartner: function() {
            this.partner_binders.push({
                role: "binder",
                newPartner: true,
                savePartner: true
            });
        },

        lookupRefresh: function(value) {
            var opts = {
                page: 1,
                count: 50,
                search: value
            };
            return this.api.partner.all(opts).then(
                angular.bind(this, function(response) {
                    return {
                        data: response.data.items
                    };
                })
            );
        },

        homeownerLookupRefresh: function(value) {
            var opts = {
                page: 1,
                count: 50,
                search: value
            };

            opts.search = value;
            return this.api.user.all(opts).then(
                angular.bind(this, function(response) {
                    return {
                        data: response.data.items
                    };
                })
            );
        },

        save: function(binder, index) {
            var data = {
                partner_id: binder.partner.id,
                role: binder.role,
                client_id: binder.homeowner.id
            };
            this.api.binder.add_partner(this.binderId, data).then(
                angular.bind(this, this.addPartnerSuccess, binder),
                angular.bind(this, this.addPartnerError));
        },

        addPartnerSuccess: function(binder, response) {
            var index = this.partner_binders.indexOf(binder);
            this.partner_binders[index] = response.data;
        },

        addPartnerError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
        },

        remove: function(partner_binder, index) {
            if (!partner_binder.newPartner) {
                this.api.binder.remove_partner(this.binderId, partner_binder.id).then(
                    angular.bind(this, this.removePartnerSuccess, partner_binder),
                    angular.bind(this, this.removePartnerError));
            }
            else {
                index = this.partner_binders.indexOf(partner_binder);
                this.partner_binders.splice(index, 1);
            }
        },

        removePartnerSuccess: function(partner_binder, response) {
            var index = this.partner_binders.indexOf(partner_binder);
            this.partner_binders.splice(index, 1);
        },

        removePartnerError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
        },

        onSelect: function(binder, index) {
            binder = {
                newPartner: true,
                savePartner: true,
                role: "binder",
                partner: binder.partner
            };
            this.partner_binders[index] = binder;
        },

        canSave: function(binder) {
            return !binder.partner;
        }
    };

})();