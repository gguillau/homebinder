describe('hbBinderTable', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        element = $compile('<hb-binder-table></hb-binder-table>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the partners-edit-form-apr directive', function() {
        it('shows the the table template', function() {
            expect(element.html()).toContain("Search");
        });
    });
});