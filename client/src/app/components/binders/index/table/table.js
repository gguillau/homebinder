(function() {
    "use strict";

    angular
        .module("hb.components.binders.index.table", [])
        .directive("hbBinderTable", function() {
            return {
                restrict: "E",
                templateUrl: "components/binders/index/table/table.tpl.html"
            };
        });
})();