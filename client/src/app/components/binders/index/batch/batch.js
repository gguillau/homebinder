(function() {
    "use strict";

    angular
        .module("hb.components.binders.index.batch", [])
        .factory("BindersBatchUploadForm", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "components/binders/index/batch/batch.tpl.html",
                            controller: "BindersBatchUploadFormController as ctrl",
                            resolveData: opts,
                            backdrop: true
                        });
                    }
                };
            }
        ])
        .controller("BindersBatchUploadFormController", [
            "$modalInstance",
            "Session",
            "$timeout",
            "Papa",
            "$scope",
            "hb.resources",
            "Loading",
            "Notify",
            "hb.api",
            "$stateParams",
            BindersBatchUploadFormController
        ]);

    function BindersBatchUploadFormController($modal, session, $timeout, Papa, $scope, resources, loading, notify, api, $stateParams) {
        this.$modal = $modal;
        this.$timeout = $timeout;
        this.papa = Papa.Papa;
        this.$scope = $scope;
        this.resources = resources.batchForm;
        this.notify = notify;
        this.api = api;
        this.loading = loading;
        this.$stateParams = $stateParams;
        this.step = "1";
        this.headers = [];
        this.invalidRows = [];
        this.uploader = {
            fileTypes: "document",
            hideUploadButton: true,
            showTable: false,
            selectButtonLabel: this.resources.upload,
            fileLimit: true,
            jwt: session.getJwt(),
            multiSelect: false,
            uploadOnAdd: true
        };
        this.binders = [];
        this.title = this.resources.title;
    }

    BindersBatchUploadFormController.prototype = {
        
        parse: function() {
            this.loading.show(this.resources.reading);
            var pending = this.uploader.api.pendingUploads();
            this.$timeout(angular.bind(this, function() {
                this.papa.parse(pending[0], {
                    preview: 2000,
                    header: true,
                    step: angular.bind(this, this.parseStep),
                    skipEmptyLines: true,
                    fastMode: true,
                    beforeFirstChunk: angular.bind(this, this.chunk),
                    complete: angular.bind(this, this.parseComplete)
                });
            }));
        },

        chunk: function(chunk) {
            var rows = chunk.split(/\r\n|\r|\n/);
            rows[0] = ["first_name", "last_name", "email", "phone", "address1", "address2", "city", "state", "zip", "country"];
            return rows.join('\n');
        },

        // we validate each row in this method/function
        parseStep: function(results, parse) {
            if (this.headers.length < 1) {
                this.headers = results.meta.fields;
            }
            if (this.validateRow(results.data[0])) {
                this.binders.push(results.data[0]);
            }
        },

        validateRow: function(row) {
            if (row.first_name && row.last_name && row.email && row.address1 && row.city && row.state && row.country) {
                return true;
            }
            this.invalidRows.push(row);
            return false;
        },

        parseComplete: function(results, file) {
            this.$scope.$apply();
            if (this.binders.length > 0) {
                this.step = "2";
                this.title = this.resources.processing;
            }
            else {
                var pending = this.uploader.api.pendingUploads();
                this.uploader.api.remove(pending[0]);
                this.title = this.resources.error;
                this.step = "3";
            }
            this.loading.close();
        },

        uploadBinders: function() {
            this.api.binder.batch({ binders: this.binders, partnerId: this.$stateParams.partnerId }).then(
                angular.bind(this, this.onUploadSuccess),
                angular.bind(this, this.onUploadError));
        },

        onUploadSuccess: function(response) {
            this.notify.info(this.resources.success);
            this.loading.close();
            this.cancel();
        },

        onUploadError: function(response) {
            this.notify.error(response.data);
            this.loading.close();
        },

        cancel: function() {
            this.$modal.dismiss("cancel");
        }

    };
})();