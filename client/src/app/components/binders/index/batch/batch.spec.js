describe("BindersBatchUploadFormController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $modal,
        api,
        notify,
        loading,
        $timeout,
        $q,
        BindersBatchUploadForm,
        ModalService;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $timeout = _$injector_.get("$timeout");
        notify = _$injector_.get("Notify");
        api = _$injector_.get("hb.api");
        loading = _$injector_.get("Loading");
        BindersBatchUploadForm = _$injector_.get("BindersBatchUploadForm");
        ModalService = _$injector_.get("ModalService");
        $q = _$injector_.get("$q");
        $controller = _$controller_;

        $modal = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("BindersBatchUploadFormController", {
            "$modalInstance": $modal,
            "$scope": $scope
        });

        $scope.ctrl = ctrl;
    }

    describe("ctrl.parse", function() {
        it("should call Papa parse", function() {
            createController();
            ctrl.uploader = {
                api: {
                    pendingUploads: function() {}
                }
            };
            spyOn(ctrl.uploader.api, "pendingUploads").and.returnValue([{ id: 1 }]);
            spyOn(ctrl.papa, "parse");

            ctrl.parse();
            $timeout.flush();

            expect(ctrl.papa.parse).toHaveBeenCalled();
        });
    });

    describe("ctrl.chunk", function() {
        it("adds header row", function() {
            createController();
            var rows = ctrl.chunk("");

            expect(rows).toEqual("first_name,last_name,email,phone,address1,address2,city,state,zip,country");
        });
    });

    describe("ctrl.validateRow", function() {
        it("returns true", function() {
            createController();
            var value = ctrl.validateRow({ first_name: "Test", last_name: "test", email: "test", address1: "test", city: "test", state: "test", country: "test" });

            expect(value).toBe(true);
        });

        it("returns false", function() {
            createController();
            var value = ctrl.validateRow({});

            expect(value).toBe(false);
        });
    });

    describe("ctrl.parseStep", function() {
        it("sets the headers and adds the binders", function() {
            createController();
            var results = {
                data: [{ first_name: "Test", last_name: "test", email: "test", address1: "test", city: "test", state: "test", country: "test" }],
                meta: {
                    fields: ["email"]
                }
            };
            ctrl.parseStep(results, {});

            expect(ctrl.headers.length).toEqual(1);
            expect(ctrl.binders.length).toEqual(1);
        });
    });

    describe("ctrl.parseComplete", function() {
        it("increments the step", function() {
            createController();
            ctrl.binders = [{ id: 1 }];
            ctrl.parseComplete({}, {});

            expect(ctrl.step).toEqual("2");
        });

        it("calls notify", function() {
            createController();
            spyOn(notify, "info");
            ctrl.binders = [];
            ctrl.uploader = {
                api: {
                    remove: function() {},
                    pendingUploads: function() { return [{}]; }
                }
            };
            ctrl.parseComplete({}, {});

            expect(ctrl.step).toEqual("3");
        });
    });

    describe("ctrl.uploadBinders", function() {
        it("should call import API and return success", function() {

            spyOn(api.binder, "batch").and.returnValue($q.when({
                data: []
            }));

            createController();
            ctrl.uploadBinders();

            expect(api.binder.batch).toHaveBeenCalled();
        });
    });

    describe("ctrl.onUploadSuccess", function() {
        it("should $state go", function() {

            spyOn(loading, "close");

            createController();
            ctrl.onUploadSuccess({ data: { id: 1 } });
            expect(loading.close).toHaveBeenCalled();
        });
    });

    describe("ctrl.onUploadError", function() {
        it("should notify error", function() {

            spyOn(notify, "error");
            spyOn(loading, "close");

            createController();
            ctrl.uploader = {
                api: {
                    remove: function() {},
                    pendingUploads: function() { return [{}]; }
                }
            };
            ctrl.onUploadError({ data: "error" });

            expect(notify.error);
            expect(loading.close).toHaveBeenCalled();
        });
    });

    describe('BindersBatchUploadForm', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            BindersBatchUploadForm.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});