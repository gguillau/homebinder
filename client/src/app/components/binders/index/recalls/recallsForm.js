(function() {
    "use strict";

    angular
        .module("hb.components.binders.index.recalls", [])
        .factory("BinderRecallModal", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "components/binders/index/recalls/recallsForm.tpl.html",
                            controller: "BinderRecallModalFormController as ctrl",
                            resolveData: opts,
                            closed: opts.closed
                        });
                    },
                    display: function(opts) {
                        Modals.show({
                            templateUrl: "components/binders/index/recalls/recallsNoticeForm.tpl.html",
                            controller: "BinderRecallModalFormController as ctrl",
                            resolveData: opts,
                            closed: opts.closed
                        });
                    }
                };
            }
        ])
        .controller("BinderRecallModalFormController", [
            "$modalInstance",
            "data",
            "$state",
            "$stateParams",
            "hb.resources",
            "Session",
            BinderRecallModalFormController
        ]);

    function BinderRecallModalFormController($modal, opts, $state, $stateParams, resources, session) {
        this.$modal = $modal;
        this.binder = opts.binder;
        this.downloadRecallReport = opts.downloadRecallReport;
        this.applianceSection = opts.applianceSection;
        this.message = opts.message;
        this.$state = $state;
        this.partnerId = $stateParams["partnerId"];
        this.recalls = opts.recalls;
        this.resources = resources.bindersRecall;
        this.user = session.getUser();
        this.text = this.user.role === "homeowner" ? this.resources.homeownerText : this.resources.partnerText;
    }

    BinderRecallModalFormController.prototype = {

        goToAppliances: function() {
            this.applianceSection();
            this.$modal.dismiss("cancel");
        },

        download: function() {
            this.downloadRecallReport();
            this.$modal.dismiss("cancel");
        },

        cancel: function() {
            this.$modal.dismiss("cancel");
        },

        ok: function() {
            this.$modal.close();
        }
    };
})();