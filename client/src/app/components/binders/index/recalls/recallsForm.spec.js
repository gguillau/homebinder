describe("BinderRecallModalFormController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        binder,
        opts,
        $modalInstance,
        $state,
        $stateParams,
        appliances,
        session,
        BinderRecallModal,
        ModalService;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        session = _$injector_.get("Session");
        BinderRecallModal = _$injector_.get("BinderRecallModal");
        ModalService = _$injector_.get("ModalService");

        $state = {
            go: function(opts) {}
        };

        $stateParams = {};

        $modalInstance = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };

        binder = {
            appliances: null,
            created_at: "2015-09-06T04:29:36Z",
            id: 1,
            name: "This is a fake home",
            primary: null,
            details: "this is a home",
            last_recall: null,
            partner: {
                address: null,
                binder_count: 7,
                binder_logo_id: null,
                code: "FAKE",
                contact: "Test Partner",
                coupons: null,
                created_at: "2015-04-25T15:56:15Z",
                email: "testpartner@gmail.com",
                id: 115,
                name: "Test Partner",
                partner_type: "inspector",
                phone: "+4042220192",
                sellers_logo_id: null,
                tags: [],
                vendors: []
            },
            vendor: null,
            recalls: 0,
            documents: null,
            maintenance_items: null,
            binder_contractors: null,
            property: null,
            subscription: null,
            seller_report: null,
            permissions: {
                can_create: true,
                can_create_seller_report: true,
                can_destroy: true,
                can_edit_seller_report: true,
                can_read: true,
                can_share: true,
                can_subscribe: true,
                can_transfer: true,
                can_view_master_report: false,
                can_write: true
            },
            tags: [{
                tag: "client",
                metadata: "{\"email\":\"faketransferemail@gmail.com\"}"
            }]
        };

        appliances = [
            { name: "Refrigerator", value: "Refrigerator" },
            { name: "Dishwasher", value: "Dishwasher" },
            { name: "Microwave", value: "Microwave" },
            { name: "Ventilator", value: "Ventilator" },
            { name: "Stove", value: "Stove" },
            { name: "Oven/Range", value: "OvenRange" },
            { name: "Disposal", value: "Disposal" },
            { name: "Washer", value: "Washer" },
            { name: "Dryer", value: "Dryer" },
            { name: "Water Heater", value: "WaterHeater" }
        ];

        opts = {
            binder: binder,
            appliances: appliances,
            message: "Sample Message",
            downloadRecallReport: function() {},
            applianceSection: function() {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("BinderRecallModalFormController", {
            "$modalInstance": $modalInstance,
            "data": opts,
            "$state": $state,
            "$stateParams": $stateParams,
            "Session": session
        });

        $scope.ctrl = ctrl;
    }

    beforeEach(function() {
        spyOn(session, "getUser").and.returnValue({ id: 1, role: "homeowner" });
    });

    describe("BinderRecallModalFormController", function() {
        it("should have the intialize the appropriate variables from the opts object", function() {

            $stateParams.partnerId = 115;

            createController();
            $rootScope.$apply();

            expect(ctrl.partnerId).toEqual(115);
            expect(ctrl.binder).toEqual(binder);
            expect(ctrl.message).toBe("Sample Message");
        });

    });

    describe("ctrl.goToAppliances", function() {
        it("should call $modalInstance dismiss function and applianceSection function", function() {

            $stateParams.partnerId = 115;
            spyOn($modalInstance, "dismiss");

            createController();
            spyOn(ctrl, "applianceSection");
            $rootScope.$apply();
            ctrl.goToAppliances();
            $rootScope.$apply();

            expect(ctrl.applianceSection).toHaveBeenCalled();
            expect($modalInstance.dismiss).toHaveBeenCalledWith('cancel');
        });

    });

    describe("ctrl.download", function() {
        it("should call $modalInstance dismiss function and downloadRecallReport function", function() {

            $stateParams.partnerId = 115;
            spyOn($modalInstance, "dismiss");

            createController();
            spyOn(ctrl, "downloadRecallReport");
            $rootScope.$apply();
            ctrl.download();
            $rootScope.$apply();

            expect(ctrl.downloadRecallReport).toHaveBeenCalled();
            expect($modalInstance.dismiss).toHaveBeenCalledWith('cancel');
        });

    });

    describe("ctrl.ok", function() {
        it("should call $modalInstance close function and $state.go function", function() {

            spyOn($modalInstance, "close");

            createController();
            $rootScope.$apply();
            ctrl.ok();
            $rootScope.$apply();

            expect($modalInstance.close).toHaveBeenCalled();

        });
    });

    describe("ctrl.cancel", function() {

        it("should call $modalInstance dismiss function", function() {

            $stateParams.partnerId = 115;
            spyOn($modalInstance, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.cancel();
            $rootScope.$apply();

            expect($modalInstance.dismiss).toHaveBeenCalledWith('cancel');
        });

    });

    describe('BinderRecallModal', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            BinderRecallModal.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });

        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            BinderRecallModal.display({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});