(function() {
	"use strict";

	angular.module('hb.components.binders.index', [
			"hb.components.binders.index.recalls",
			"hb.components.binders.index.upload",
			"hb.components.binders.index.table",
			"hb.components.binders.index.warranties",
			"hb.components.binders.index.batch"
		])
		.directive("hbBindersIndex", function() {
			return {
				restrict: "E",
				templateUrl: "components/binders/index/index.tpl.html"
			};
		})
		.factory("BindersIndexController", [
			'hb.framework.indexBase',
			'hb.api',
			'hb.resources',
			'WidgetModal',
			'AfterLogin',
			'Session',
			'TransferModal',
			'ModalService',
			'BindersImageUploadForm',
			'ApplianceService',
			'BinderRecallModal',
			'$window',
			"ShareForm",
			"Address",
			"BindersWarrantiesModalForm",
			"Context",
			"RepairPricerModal",
			"$sce",
			"Event",
			"BindersBatchUploadForm",
			function(IndexBase, api, resources, widgetModal, AfterLogin, Session, TransferModal, ModalService, PartnerBindersUploadForm, ApplianceService, PartnerRecallModal, $window, ShareForm, Address, BindersWarrantiesModalForm, Context, RepairPricerModal, $sce, event, BindersBatchUploadForm) {
				var Model = function() {
					// call the parent class
					IndexBase.call(this);

					// apply widget specific resource strings
					this.resources = angular.extend({}, this.resources, resources.bindersIndex);
					this.api = api;
					this.editParamsProperty = "binderId";
					this.AfterLogin = AfterLogin;
					this.TransferModal = TransferModal;
					this.modals = ModalService;
					this.PartnerBindersUploadForm = PartnerBindersUploadForm;
					this.ApplianceService = ApplianceService;
					this.PartnerRecallModal = PartnerRecallModal;
					this.$window = $window;
					this.ShareForm = ShareForm;
					this.Address = Address;
					this.BindersWarrantiesModalForm = BindersWarrantiesModalForm;
					this.statesOptions = [];
					this.countriesOptions = [{
						name: "All Countries",
						value: undefined
					}];
					this.countriesOptions = this.countriesOptions.concat(this.Address.countries());
					this.statesOptions = this.Address.getStatesAndProvinces();
					this.currentUser = Session.getUser();
					// set the refresh API call
					this.refreshCall = api.binder.all;
					// set the delete API call
					this.deleteCall = api.binder.destroy;
					// set the name property used in the delete message
					this.nameProperty = "name";
					this.editState = "admin.binders_edit";
					this.editParams = {};
					this.editParamsProperty = "binderId";
					this.newState = "admin.binders_new";
					this.maxSize = 10;
					this.current_filter = null;
					this.sortOptions = [{
						orderBy: "binders.created_at",
						order: "desc",
						desc: this.resources.creationDate + " - " + this.resources.descending
					}, {
						orderBy: "binders.created_at",
						order: "asc",
						desc: this.resources.creationDate + " - " + this.resources.ascending
					}, {
						orderBy: "binders.name",
						order: "desc",
						desc: this.resources.binderName + " - " + this.resources.descending
					}, {
						orderBy: "binders.name",
						order: "asc",
						desc: this.resources.binderName + " - " + this.resources.ascending
					}];
					this.searchTypes = [{
						name: "all",
						value: "All Binders"
					}, {
						name: "partners",
						value: "Partner Binders"
					}, {
						name: "users",
						value: "User Binders"
					}, {
						name: "orphans",
						value: "Orphaned Binders"
					}];
					this.createOptions = [{
						name: undefined,
						value: "All Methods"
					}, {
						name: "manual",
						value: "Manual"
					}, {
						name: "api",
						value: "API"
					}, {
						name: "email",
						value: "Email"
					}, {
						name: "self_serve",
						value: "Self-Serve"
					}, {
						name: "automated",
						value: "Automated (deprecated)"
					}];
					this.binderArgs = {
						create_method: undefined,
						city: undefined,
						state: undefined,
						zip: undefined,
						country: undefined,
						partnerId: undefined,
						userId: undefined
					};
					this.searchType = this.searchTypes[0].name;
					this.partnerLookupCfg = {
						placeholder: "Search for a partner",
						refresh: angular.bind(this, this.partnerLookupRefresh),
						itemTemplate: "components/lookup/partner/partnerDropDownItem.tpl.html",
						selectionTemplate: "components/lookup/partner/partnerSelection.tpl.html",
						allowClear: false
					};
					this.userLookupCfg = {
						placeholder: "Search for a user",
						refresh: angular.bind(this, this.userLookupRefresh),
						itemTemplate: "components/lookup/user/userDropDownItem.tpl.html",
						selectionTemplate: "components/lookup/user/userSelection.tpl.html",
						allowClear: false
					};
					this.sortOption = this.sortOptions[0];
					this.orderBy = this.sortOption.orderBy;
					this.order = this.sortOption.order;
					// set page specific values on toolbar
					angular.extend(this.toolbarCfg, {
						title: this.resources.title,
						sort: {
							title: this.resources.sortBy,
							refresh: angular.bind(this, this.refresh),
							sortOptions: this.sortOptions,
							sortOption: this.sortOption,
							execute: angular.bind(this, this.sort)
						},
						button: {
							title: this.resources.newButton,
							click: angular.bind(this, this.newItem)
						}
					});
					this.columnspan = this.resources.binderAttributes.length + 2;
					this.headers = null;
					this.Context = Context;
					this.setHeaders();
				};

				Model.prototype = Object.create(IndexBase.prototype);

				angular.extend(Model.prototype, {

					setHeaders: function() {
						this.headers = this.resources.binderAttributes.map(angular.bind(this, function(attribute) {
							var sortable = false,
								orderBy = null,
								sorted = false,
								show = true;
							if (attribute === "ID") {
								sorted = true;
								sortable = true;
								orderBy = "binders.id";
							}
							else if (attribute === "Name") {
								sortable = true;
								orderBy = "binders.name";
							}
							else if (attribute === "Address") {
								sortable = true;
								orderBy = "properties.address1";
							}
							else if (attribute === "Created") {
								sortable = true;
								orderBy = "binders.created_at";
							}
							else if (attribute === "Owner") {
								show = this.currentUser.role === "admin" ? true : false;
							}
							else if (attribute === "Recalls") {
								show = this.currentUser.role === "inspector" ? true : false;
							}
							else if (attribute === "Status") {
								show = this.currentUser.role != "admin" ? true : false;
							}
							return {
								name: attribute,
								sortable: sortable,
								sorted: sorted,
								orderBy: orderBy,
								show: show,
								order: "desc"
							};
						}));
						this.sortOption = this.headers[0];
					},

					transferBinder: function(binder) {
						this.TransferModal.show({
							binder: binder,
							showTransferTypes: false,
							defaultTransferType: "ownership",
							closed: angular.bind(this, this.onBinderTransferred, binder)
						});
					},

					onBinderTransferred: function(binder) {
						var index = this.items.indexOf(binder);
						this.items.splice(index, 1);
					},

					resendTransferEmail: function(binder) {
						this.TransferModal.resend({
							binder: binder,
							showTransferTypes: false,
							defaultTransferType: "ownership",
							resend: true,
							closed: angular.bind(this, this.onBinderTransferred, binder)
						});
					},

					updateBinder: function(index, binder) {
						this.items[index] = binder;
					},

					uploadPhoto: function(binder) {
						this.PartnerBindersUploadForm.show({
							binder: binder,
							closed: angular.bind(this, this.refresh)
						});
					},

					partnerType: function(type) {
						if (this.partner) {
							return this.partner.partner_type === type;
						}
					},

					viewDocument: function(doc) {
						this.$window.open(doc.location);
					},

					share: function(binder) {
						this.ShareForm.show({
							binder: binder,
							closed: angular.bind(this, this.refresh)
						});
					},

					getAppliances: function(binder) {
						this.api.appliance.all({ binderId: binder.id }).then(
							angular.bind(this, this.applianceCheck, binder),
							angular.bind(this, this.onError));
					},

					applianceCheck: function(binder, response) {
						binder.appliances = response.data;
						var applianceCheck = this.ApplianceService.makeAndModelCheck(binder.appliances);
						if (!applianceCheck.check) {
							this.PartnerRecallModal.show({
								binder: binder,
								message: applianceCheck.message,
								applianceSection: angular.bind(this, this.applianceSection, binder),
								downloadRecallReport: angular.bind(this, this.downloadRecallReport, binder)
							});
						}
						else {
							this.downloadRecallReport(binder);
						}
					},

					applianceSection: function(binder) {
						this.$state.go("binder.appliances", {
							binderId: binder.id
						});
					},

					downloadRecallReport: function(binder) {
						this.loading.show(this.resources.recallLoading);
						this.api.recallReportPDF.get(binder.id).then(
							angular.bind(this, this.recallReportSuccess, binder),
							angular.bind(this, this.recallReportError));
					},

					recallReportSuccess: function(binder, response) {
						var file = new Blob([response.data], {
							type: "application/pdf"
						}, {
							encoding: "raw"
						});
						var filename = "HomeBinder_Recall_Report_" + binder.name + "_" + this.today + ".pdf";

						if (this.$window.navigator.msSaveOrOpenBlob) {
							this.$window.navigator.msSaveOrOpenBlob(file, filename);
						}
						else {
							var fileURL = this.$window.URL.createObjectURL(file);
							var a = document.createElement("a");
							a.href = fileURL;
							a.target = "_blank";
							a.download = filename;
							document.body.appendChild(a);
							a.click();
							document.body.removeChild(a);
							this.$window.URL.revokeObjectURL(file);
						}
						this.loading.close();
					},

					recallReportError: function(response) {
						this.$log.error(response);
						this.notify.error(response.data);
						this.loading.close();
					},

					onError: function(response) {
						this.$log.error(response);
						this.notify.error(response.data);
					},

					ownerIsPartner: function(binder) {
						if (binder.owner) {
							return binder.owner.role === "inspector" || binder.owner.role === "broker";
						}
						return false;
					},

					userLookupRefresh: function(value) {
						var opts = {
							page: 1,
							count: 50,
							search: value
						};
						opts.search = value;
						return api.user.all(opts).then(
							angular.bind(this, function(response) {
								return {
									data: response.data.items
								};
							})
						);
					},

					partnerLookupRefresh: function(value) {
						var opts = {
							page: 1,
							count: 50,
							search: value
						};
						opts.search = value;
						return api.partner.all(opts).then(
							angular.bind(this, function(response) {
								return {
									data: response.data.items
								};
							})
						);
					},

					addQueryArgs: function() {
						if (this.partner) {
							this.binderArgs["partnerId"] = this.partner.id;
						}
						if (this.user) {
							this.binderArgs["userId"] = this.user.id;
						}
						this.addArgs("partnerId");
						this.addArgs("state");
						this.addArgs("country");
						this.addArgs("create_method");
						if (this.searchType != "orphans") {
							this.addArgs("userId");
						}
					},

					addArgs: function(arg) {
						if (this.binderArgs[arg]) {
							this.queryArgs[arg] = this.binderArgs[arg];
						}
						else {
							delete this.queryArgs[arg];
						}
					},

					searchTypeChange: function() {
						switch (this.searchType) {
							case "all":
								this.user = null;
								this.partner = null;
								this.binderArgs["partnerId"] = null;
								this.binderArgs["userId"] = null;
								this.refresh();
								break;
							case "partners":
								this.user = null;
								this.binderArgs["userId"] = null;
								break;
							case "users":
								this.partner = null;
								this.binderArgs["partnerId"] = null;
								break;
							case "orphans":
								this.user = null;
								this.partner = null;
								this.binderArgs["partnerId"] = null;
								angular.extend(this.queryArgs, {
									userId: null
								});
								this.refresh();
								break;
						}
					},

					showSearch: function() {
						switch (this.searchType) {
							case "all":
							case "orphans":
								return false;
						}
						return true;
					},

					orphanBinder: function(binder, index) {
						this.api.binder.orphan(binder.id).then(
							angular.bind(this, this.orphanSuccess, index),
							angular.bind(this, this.onError)
						);
					},

					orphanSuccess: function(index, response) {
						this.notify.info(this.resources.orphanSuccess);
						this.items[index] = response.data;
					},

					canViewWarranties: function() {
						var role = this.currentUser.role;
						switch (role) {
							case "admin":
								return true;
							case "inspector":
							case "broker":
								return this.canPartnerViewWarranties();
							default:
								return false;
						}
					},

					canPartnerViewWarranties: function() {
						if (this.partner && this.partner.configuration) {
							return this.partner.configuration.active_warranty_account === true;
						}
						else {
							return false;
						}
					},

					showWarranties: function(binder, index) {
						this.BindersWarrantiesModalForm.show({
							binder: binder
						});
					},

					// send user to edit item view
					viewBinder: function(item) {
						this.Context.setBinder(item);
						this.editItem(item);
					},

					canOrderRP: function(binder) {
						if (this.currentUser.role === "admin" || this.currentUser.role === "limited_admin") {
							return true;
						}
						else if (this.currentUser.role === "agent") {
							if (binder.partner_binders.length < 1) {
								return false;
							}
							else if (binder.partner_binders.length > 0 && binder.permissions.access_repair_pricer) {
								var partner = binder.partner_binders[0].partner;
								if (partner.configuration.repair_pricer_enabled === true) {
									return true;
								}
								else {
									return false;
								}
							}
						}
						else if (this.currentUser.role === "inspector") {
							return !this.partner.configuration.repair_pricer_pre_paid_reports;
						}
						else {
							return false;
						}
					},

					orderRP: function(binder) {
						this.user = binder.partner_binders[0].homeowner;
						this.partner = binder.partner_binders[0].partner;
						this.user.user_profile_attributes.address_attributes = binder.property;
						RepairPricerModal.show({
							user: this.user,
							binder_id: binder.id,
							partner_id: this.partner.id,
							repair_pricer_pre_paid_reports: this.partner.configuration.repair_pricer_pre_paid_reports
						});

						event.create({ event_name: "repair_pricer_order_now_clicked", event_type: "repair_pricer", user_id: this.currentUser.id, user_created_at: this.currentUser.user_created_at, user_role: this.currentUser.role, partner_id: this.partner.id, partner_type: this.partner.partner_type });

					},

					uploadBatch: function() {
						BindersBatchUploadForm.show();
					}
				});

				return Model;
			}

		]);
})();