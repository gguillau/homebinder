describe("BindersIndexController", function() {
    var BindersIndexController,
        TestClass,
        binder,
        api,
        RepairPricerModal,
        BindersBatchUploadForm;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the Session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return {
                        role: "admin"
                    };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
    }));

    // get the base class under test
    beforeEach(inject(function($injector) {
        BindersIndexController = $injector.get("BindersIndexController");
        api = $injector.get("hb.api");
        RepairPricerModal = $injector.get("RepairPricerModal");
        BindersBatchUploadForm = $injector.get("BindersBatchUploadForm");
    }));

    function createTestClass() {
        TestClass = function() {
            BindersIndexController.call(this);
            this.currentUser = {
                role: "admin"
            };
            this.resources = angular.extend({}, this.resources, {
                loading: "loading",
                loadError: "load error",
                confirmDelete: "confirm",
                deleteMsg1: "a ",
                deleteMsg2: " b",
                deleteSuccess: "deleted",
                deleteError: "delete error",
                saveSuccess: "SAVE",
                impersonateLoading: "LO"
            });
            this.indexState = "admin.binders";
            this.indexParams = {};
            this.nameProperty = "name";
        };

        TestClass.prototype = Object.create(BindersIndexController.prototype);

        return new TestClass();
    }

    binder = {
        orphan: function() {}
    };

    describe("setHeaders", function() {
        it("creates the headers", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.setHeaders();

            expect(model.headers.length).toEqual(7);
        }));
    });

    describe("transferBinder", function() {
        it("calls the transfer modal", inject(function(TransferModal) {

            spyOn(TransferModal, "show");

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.transferBinder({});

            expect(TransferModal.show).toHaveBeenCalledWith({
                binder: {},
                showTransferTypes: false,
                defaultTransferType: "ownership",
                closed: jasmine.any(Function)
            });
        }));
    });

    describe("onBinderTransferred", function() {
        it("calls refresh", inject(function($log, Notify) {

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            spyOn(model, "refresh");
            var item = { id: 1, name: "test" };
            model.items = [item];
            model.onBinderTransferred(item);

            expect(model.items.length).toEqual(0);
        }));
    });

    describe("resendTransferEmail", function() {
        it("calls the TransferModal resend function", inject(function(TransferModal) {

            spyOn(TransferModal, "resend");

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            var binder = {
                name: "Test",
                transfer: {
                    receiver_email: "test@gmail.com"
                }
            };
            model.resendTransferEmail(binder);

            expect(TransferModal.resend).toHaveBeenCalled();
        }));
    });

    describe("updateBinder", function() {
        it("updates item in items array", inject(function() {

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            var binder = {
                name: "Test",
                transfer: {
                    receiver_email: "test@gmail.com"
                }
            };
            model.items = [{
                name: "Binder",
                transfer: {
                    receiver_email: "test@gmail.com"
                }
            }];
            expect(model.items[0].name).toEqual("Binder");
            model.updateBinder(0, binder);

            expect(model.items[0]).toEqual(binder);
            expect(model.items[0].name).toEqual("Test");
        }));
    });

    describe("uploadPhoto", function() {
        it("calls the BindersImageUploadForm show function", inject(function(BindersImageUploadForm) {

            spyOn(BindersImageUploadForm, "show");

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            var binder = {
                name: "Test",
                transfer: {
                    receiver_email: "test@gmail.com"
                }
            };
            model.uploadPhoto(binder);

            expect(BindersImageUploadForm.show).toHaveBeenCalledWith({
                binder: binder,
                closed: jasmine.any(Function)
            });
        }));
    });

    describe("partnerType", function() {
        it("checks if partner type is equal to argument", inject(function() {

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.partner = {
                partner_type: "broker"
            };
            var value = model.partnerType("broker");

            expect(value).toEqual(true);
        }));

        it("checks if partner type is equal to argument", inject(function() {

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.partner = {
                partner_type: "broker"
            };
            var value = model.partnerType("inspector");

            expect(value).toEqual(false);
        }));

        it("checks if partner type is equal to argument", inject(function() {

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.partner = {
                partner_type: "inspector"
            };
            var value = model.partnerType("inspector");

            expect(value).toEqual(true);
        }));

        it("checks if partner type is equal to argument", inject(function() {

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.partner = {
                partner_type: "inspector"
            };
            var value = model.partnerType("broker");

            expect(value).toEqual(false);
        }));
    });

    describe("viewDocument", function() {
        it("calls $window open", inject(function($window) {
            spyOn($window, "open");

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.viewDocument({
                location: "test"
            });

            expect($window.open).toHaveBeenCalledWith("test");
        }));
    });

    describe("share", function() {
        it("calls the ShareForm show function", inject(function(ShareForm) {

            spyOn(ShareForm, "show");

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            var binder = {
                name: "Test",
                transfer: {
                    receiver_email: "test@gmail.com"
                }
            };
            model.share(binder);

            expect(ShareForm.show).toHaveBeenCalledWith({
                binder: binder,
                closed: jasmine.any(Function)
            });
        }));
    });

    describe("getAppliances", function() {
        it("gets the appliances and calls modal", inject(function(Notify, $rootScope, $q, BinderRecallModal) {
            var appliance = {
                all: function() {}
            };

            spyOn(BinderRecallModal, "show");

            spyOn(appliance, "all").and.returnValue($q.when({
                data: []
            }));

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.api.appliance = appliance;
            model.getAppliances({
                id: 1
            });
            $rootScope.$apply();

            expect(appliance.all).toHaveBeenCalledWith({ binderId: 1 });
            expect(BinderRecallModal.show).toHaveBeenCalledWith({
                binder: {
                    id: 1,
                    appliances: []
                },
                message: "No appliances have been created for this binder. Do you want to add appliances along with their make and model at this time?",
                applianceSection: jasmine.any(Function),
                downloadRecallReport: jasmine.any(Function)
            });
        }));

        it("gets the appliances and calls modal", inject(function(Notify, $rootScope, $q, BinderRecallModal) {
            var appliance = {
                all: function() {}
            };

            spyOn(BinderRecallModal, "show");

            spyOn(appliance, "all").and.returnValue($q.when({
                data: [{
                    id: 1,
                    name: "Fridge",
                    make: null
                }]
            }));

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.api.appliance = appliance;
            model.getAppliances({
                id: 1
            });
            $rootScope.$apply();

            expect(appliance.all).toHaveBeenCalledWith({ binderId: 1 });
            expect(BinderRecallModal.show).toHaveBeenCalledWith({
                binder: {
                    id: 1,
                    appliances: [{
                        id: 1,
                        name: "Fridge",
                        make: null
                    }]
                },
                message: "Make and Model information have not been entered for all appliances for this binder. Do you want to enter that information at this time?",
                applianceSection: jasmine.any(Function),
                downloadRecallReport: jasmine.any(Function)
            });
        }));

        it("gets the appliances and calls downloadRecallReport", inject(function(Notify, $rootScope, $q, BinderRecallModal) {
            var appliance = {
                all: function() {}
            };

            spyOn(BinderRecallModal, "show");

            spyOn(appliance, "all").and.returnValue($q.when({
                data: [{
                    id: 1,
                    name: "Fridge",
                    make: "Kenmore"
                }]
            }));

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            spyOn(model, "downloadRecallReport");
            model.api.appliance = appliance;
            model.getAppliances({
                id: 1
            });
            $rootScope.$apply();

            expect(appliance.all).toHaveBeenCalledWith({ binderId: 1 });
            expect(BinderRecallModal.show).not.toHaveBeenCalled();
            expect(model.downloadRecallReport).toHaveBeenCalledWith({
                id: 1,
                appliances: [{
                    id: 1,
                    name: "Fridge",
                    make: "Kenmore"
                }]
            });
        }));
        it("returns an error", inject(function($log, Notify, $rootScope, $q) {
            spyOn(Notify, "error");
            spyOn($log, "error");
            var appliance = {
                all: function() {}
            };
            spyOn(appliance, "all").and.returnValue($q.reject({
                data: "error"
            }));

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.api.appliance = appliance;
            model.getAppliances({
                id: 1
            });
            $rootScope.$apply();

            expect(appliance.all).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
            expect(Notify.error).toHaveBeenCalled();
        }));
    });

    describe("applianceSection", function() {
        it("takes user to appliances section", inject(function($state) {
            spyOn($state, "go");
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            var binder = {
                id: 1,
                name: "Test",
                transfer: {
                    receiver_email: "test@gmail.com"
                }
            };
            model.applianceSection(binder);

            expect($state.go).toHaveBeenCalledWith("binder.appliances", {
                binderId: binder.id
            });
        }));
    });

    describe("downloadRecallReport", function() {
        it("gets the report", inject(function(Notify, $rootScope, $q, BinderRecallModal, Loading) {
            var recallReportPDF = {
                get: function() {}
            };

            spyOn(Loading, "show");
            spyOn(Loading, "close");

            spyOn(recallReportPDF, "get").and.returnValue($q.when({
                data: []
            }));

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.api.recallReportPDF = recallReportPDF;
            model.downloadRecallReport({
                id: 1
            });
            $rootScope.$apply();

            expect(recallReportPDF.get).toHaveBeenCalledWith(1);
            expect(Loading.show).toHaveBeenCalledWith("Downloading report...");
            expect(Loading.close).toHaveBeenCalled();
        }));

        it("returns an error", inject(function($log, Notify, $rootScope, $q, BinderRecallModal, Loading) {
            var recallReportPDF = {
                get: function() {}
            };

            spyOn($log, "error");
            spyOn(Notify, "error");
            spyOn(Loading, "show");
            spyOn(Loading, "close");

            spyOn(recallReportPDF, "get").and.returnValue($q.reject({
                data: "error"
            }));

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.api.recallReportPDF = recallReportPDF;
            model.downloadRecallReport({
                id: 1
            });
            $rootScope.$apply();

            expect(recallReportPDF.get).toHaveBeenCalledWith(1);
            expect(Loading.show).toHaveBeenCalledWith("Downloading report...");
            expect($log.error).toHaveBeenCalled();
            expect(Notify.error).toHaveBeenCalled();
            expect(Loading.close).toHaveBeenCalled();
        }));
    });

    describe("recallReportSuccess", function() {
        it("calls msSaveOrOpenBlob", inject(function() {
            var model = createTestClass();
            model.$window = {
                navigator: {
                    msSaveOrOpenBlob: function() {}
                }
            };
            spyOn(model.$window.navigator, "msSaveOrOpenBlob");

            model.recallReportSuccess({}, { data: "" });

            expect(model.$window.navigator.msSaveOrOpenBlob).toHaveBeenCalled();
        }));
    });

    describe("ownerIsPartner", function() {
        it("returns false", inject(function() {

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            var binder = {
                name: "Test",
                transfer: {
                    receiver_email: "test@gmail.com"
                }
            };
            var value = model.ownerIsPartner(binder);

            expect(value).toBe(false);
        }));

        it("returns false", inject(function() {

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            var binder = {
                name: "Test",
                transfer: {
                    receiver_email: "test@gmail.com"
                },
                owner: {
                    role: "homeowner"
                }
            };
            var value = model.ownerIsPartner(binder);

            expect(value).toBe(false);
        }));

        it("returns true", inject(function() {

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            var binder = {
                name: "Test",
                transfer: {
                    receiver_email: "test@gmail.com"
                },
                owner: {
                    role: "inspector"
                }
            };
            var value = model.ownerIsPartner(binder);

            expect(value).toBe(true);
        }));

        it("returns true", inject(function() {

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            var binder = {
                name: "Test",
                transfer: {
                    receiver_email: "test@gmail.com"
                },
                owner: {
                    role: "broker"
                }
            };
            var value = model.ownerIsPartner(binder);

            expect(value).toBe(true);
        }));
    });

    describe("searchTypeChange", function() {
        it("calls refresh", inject(function() {

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };

            spyOn(model, "refresh");

            model.searchType = "all";
            model.searchTypeChange();

            expect(model.user).toBe(null);
            expect(model.partner).toBe(null);
            expect(model.binderArgs["userId"]).toBe(null);
            expect(model.binderArgs["partnerId"]).toBe(null);
            expect(model.refresh).toHaveBeenCalled();
        }));

        it("does not call refresh", inject(function() {

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };

            spyOn(model, "refresh");

            model.searchType = "partners";
            model.partner = {
                id: 1
            };
            model.binderArgs["partnerId"] = 1;
            model.searchTypeChange();

            expect(model.user).toBe(null);
            expect(model.partner).toEqual({
                id: 1
            });
            expect(model.binderArgs["userId"]).toBe(null);
            expect(model.binderArgs["partnerId"]).toEqual(1);
            expect(model.refresh).not.toHaveBeenCalled();
        }));

        it("does not call refresh", inject(function() {

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };

            spyOn(model, "refresh");

            model.searchType = "users";
            model.user = {
                id: 1
            };
            model.binderArgs["userId"] = 1;
            model.searchTypeChange();

            expect(model.user).toEqual({
                id: 1
            });
            expect(model.partner).toEqual(null);
            expect(model.binderArgs["userId"]).toBe(1);
            expect(model.binderArgs["partnerId"]).toEqual(null);
            expect(model.refresh).not.toHaveBeenCalled();
        }));

        it("calls refresh", inject(function() {

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };

            spyOn(model, "refresh");

            model.searchType = "orphans";
            model.searchTypeChange();

            expect(model.user).toBe(null);
            expect(model.partner).toBe(null);
            expect(model.binderArgs["partnerId"]).toBe(null);
            expect(model.refresh).toHaveBeenCalled();
        }));
    });

    describe("showSearch", function() {
        it("returns false", inject(function() {

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.searchType = "all";
            var value = model.showSearch();

            expect(value).toBe(false);
        }));

        it("returns false", inject(function() {

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.searchType = "orphans";
            var value = model.showSearch();

            expect(value).toBe(false);
        }));

        it("returns false", inject(function() {

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.searchType = "users";
            var value = model.showSearch();

            expect(value).toBe(true);
        }));
    });

    describe("onError", function() {
        it("logs the error and notifies the user", inject(function($log, Notify) {
            spyOn($log, "error");
            spyOn(Notify, "error");

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.onError({
                data: "error"
            });

            expect($log.error).toHaveBeenCalled();
            expect(Notify.error).toHaveBeenCalled();
        }));
    });

    describe("userLookupRefresh", function() {
        it("calls user all", inject(function($q, $rootScope) {
            var model = createTestClass();

            spyOn(api.user, "all").and.returnValue($q.when({ data: {} }));

            model.userLookupRefresh("test");
            $rootScope.$apply();

            expect(model.api.user.all).toHaveBeenCalled();
        }));
    });

    describe("partnerLookupRefresh", function() {
        it("calls partner all", inject(function($q, $rootScope) {
            var model = createTestClass();

            spyOn(api.partner, "all").and.returnValue($q.when({ data: {} }));

            model.partnerLookupRefresh("test");
            $rootScope.$apply();

            expect(model.api.partner.all).toHaveBeenCalled();
        }));
    });

    describe("addQueryArgs", function() {
        it("adds the query arguments", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.partner = {
                id: 1
            };
            model.user = {
                id: 1
            };

            model.binderArgs["state"] = "MA";
            model.binderArgs["state"] = "MA";
            model.binderArgs["state"] = "MA";
            model.binderArgs["country"] = "USA";
            model.binderArgs["create_method"] = "api";
            model.addQueryArgs();

            expect(model.queryArgs["state"]).toEqual("MA");
            expect(model.queryArgs["country"]).toEqual("USA");
            expect(model.queryArgs["create_method"]).toEqual("api");
            expect(model.queryArgs["partnerId"]).toEqual(1);
            expect(model.queryArgs["userId"]).toEqual(1);
        }));

    });

    describe("addArgs", function() {
        it("adds the query argument", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.binderArgs["create_method"] = "email";
            model.addArgs("create_method");

            expect(model.queryArgs["create_method"]).toEqual("email");
        }));
    });

    describe("orphan", function() {
        it("orphans the binder", inject(function(Notify, $rootScope, $q) {
            spyOn(Notify, "info");
            spyOn(binder, "orphan").and.returnValue($q.when({
                data: "success"
            }));

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.api.binder = binder;
            model.orphanBinder({
                id: 1
            }, 1);
            $rootScope.$apply();

            expect(binder.orphan).toHaveBeenCalled();
            expect(Notify.info).toHaveBeenCalled();
        }));
        it("returns an error", inject(function($log, Notify, $rootScope, $q) {
            spyOn(Notify, "error");
            spyOn($log, "error");
            spyOn(binder, "orphan").and.returnValue($q.reject({
                data: "error"
            }));

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.api.binder = binder;
            model.orphanBinder({
                id: 1
            }, 1);
            $rootScope.$apply();

            expect(binder.orphan).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
            expect(Notify.error).toHaveBeenCalled();
        }));
    });

    describe("canViewWarranties", function() {
        it("returns true", inject(function(Notify, $rootScope, $q) {

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.api.binder = binder;
            $rootScope.$apply();
            var result = model.canViewWarranties();
            expect(result).toBe(true);
        }));
        it("returns true", inject(function(Notify, $rootScope, $q) {

            var model = createTestClass();
            model.currentUser = {
                role: "inspector"
            };
            model.partner = {
                configuration: {
                    active_warranty_account: true
                }
            };
            model.api.binder = binder;
            $rootScope.$apply();
            var result = model.canViewWarranties();
            expect(result).toBe(true);
        }));
        it("returns true", inject(function(Notify, $rootScope, $q) {

            var model = createTestClass();
            model.currentUser = {
                role: "broker"
            };
            model.partner = {
                configuration: {
                    active_warranty_account: true
                }
            };
            model.api.binder = binder;
            $rootScope.$apply();
            var result = model.canViewWarranties();
            expect(result).toBe(true);
        }));
        it("returns false", inject(function(Notify, $rootScope, $q) {

            var model = createTestClass();
            model.currentUser = {
                role: "inspector"
            };
            model.api.binder = binder;
            $rootScope.$apply();
            var result = model.canViewWarranties();
            expect(result).toBe(false);
        }));
        it("returns false", inject(function(Notify, $rootScope, $q) {

            var model = createTestClass();
            model.currentUser = {
                role: "broker"
            };
            model.api.binder = binder;
            $rootScope.$apply();
            var result = model.canViewWarranties();
            expect(result).toBe(false);
        }));
        it("returns false", inject(function(Notify, $rootScope, $q) {

            var model = createTestClass();
            model.currentUser = {
                role: "homeowner"
            };
            model.api.binder = binder;
            $rootScope.$apply();
            var result = model.canViewWarranties();
            expect(result).toBe(false);
        }));
    });

    describe("canPartnerViewWarranties", function() {
        it("returns true", inject(function(Notify, $rootScope, $q) {

            var model = createTestClass();
            model.currentUser = {
                role: "inspector"
            };
            model.partner = {
                configuration: {
                    active_warranty_account: true
                }
            };
            model.api.binder = binder;
            $rootScope.$apply();
            var result = model.canPartnerViewWarranties();
            expect(result).toBe(true);
        }));
        it("returns true", inject(function(Notify, $rootScope, $q) {

            var model = createTestClass();
            model.currentUser = {
                role: "broker"
            };
            model.partner = {
                configuration: {
                    active_warranty_account: true
                }
            };
            model.api.binder = binder;
            $rootScope.$apply();
            var result = model.canPartnerViewWarranties();
            expect(result).toBe(true);
        }));
        it("returns false", inject(function(Notify, $rootScope, $q) {

            var model = createTestClass();
            model.currentUser = {
                role: "inspector"
            };
            model.api.binder = binder;
            $rootScope.$apply();
            var result = model.canPartnerViewWarranties();
            expect(result).toBe(false);
        }));
        it("returns false", inject(function(Notify, $rootScope, $q) {

            var model = createTestClass();
            model.currentUser = {
                role: "broker"
            };
            model.api.binder = binder;
            $rootScope.$apply();
            var result = model.canPartnerViewWarranties();
            expect(result).toBe(false);
        }));
    });

    describe("showWarranties", function() {
        it("calls the warranties modal", inject(function(BindersWarrantiesModalForm) {

            spyOn(BindersWarrantiesModalForm, "show");

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.showWarranties({});

            expect(BindersWarrantiesModalForm.show).toHaveBeenCalledWith({
                binder: {}
            });
        }));
    });

    describe("viewBinder", function() {
        it("adds the query argument", inject(function() {
            var model = createTestClass();
            spyOn(model, "editItem");

            model.viewBinder({ id: 1 });

            expect(model.editItem).toHaveBeenCalled();
        }));
    });

    describe("canOrderRP", function() {
        it("returns false", inject(function() {
            var model = createTestClass();
            model.currentUser = { role: "agent" };
            var boolean_value = model.canOrderRP({ id: 1, partner_binders: [] });
            expect(boolean_value).toBe(false);
        }));

        it("returns true", inject(function() {
            var model = createTestClass();
            model.currentUser = { role: "agent" };
            var boolean_value = model.canOrderRP({ id: 1, partner_binders: [{ partner: { configuration: { repair_pricer_enabled: true } } }], permissions: { access_repair_pricer: true } });
            expect(boolean_value).toBe(true);
        }));

        it("returns false", inject(function() {
            var model = createTestClass();
            model.currentUser = { role: "agent" };
            var boolean_value = model.canOrderRP({ id: 1, partner_binders: [{ partner: { configuration: { repair_pricer_enabled: false } } }], permissions: { access_repair_pricer: true } });
            expect(boolean_value).toBe(false);
        }));

        it("returns true", inject(function() {
            var model = createTestClass();
            model.currentUser = { role: "inspector" };
            model.partner = { configuration: { repair_pricer_pre_paid_reports: false } };
            var boolean_value = model.canOrderRP({ id: 1, partner_binders: [{ partner: { configuration: { repair_pricer_enabled: false } } }], permissions: { access_repair_pricer: true } });
            expect(boolean_value).toBe(true);
        }));

        it("returns false", inject(function() {
            var model = createTestClass();
            model.currentUser = { role: "inspector" };
            model.partner = { configuration: { repair_pricer_pre_paid_reports: true } };
            var boolean_value = model.canOrderRP({ id: 1, partner_binders: [{ partner: { configuration: { repair_pricer_enabled: false } } }], permissions: { access_repair_pricer: true } });
            expect(boolean_value).toBe(false);
        }));

        it("returns false", inject(function() {
            var model = createTestClass();
            model.currentUser = { role: "broker" };
            var boolean_value = model.canOrderRP({ id: 1, partner_binders: [{ partner: { configuration: { repair_pricer_enabled: false } } }], permissions: { access_repair_pricer: true } });
            expect(boolean_value).toBe(false);
        }));

        it("returns true", inject(function() {
            var model = createTestClass();
            model.currentUser = { role: "admin" };
            var boolean_value = model.canOrderRP({ id: 1, partner_binders: [{ partner: { configuration: { repair_pricer_enabled: false } } }], permissions: { access_repair_pricer: true } });
            expect(boolean_value).toBe(true);
        }));
    });

    describe("orderRP", function() {
        it("adds the query argument", inject(function() {
            var model = createTestClass();
            spyOn(RepairPricerModal, "show");

            var partner_binder = {
                homeowner: {
                    user_profile_attributes: {
                        mobile_phone: {

                        }
                    }
                },
                partner: {
                    configuration: {

                    }
                }
            };
            var property = {

            };
            model.orderRP({ id: 1, partner_binders: [partner_binder], property: property });

            expect(RepairPricerModal.show).toHaveBeenCalled();
        }));
    });

    describe("uploadBatch", function() {
        it("calls modal", inject(function() {
            var model = createTestClass();
            spyOn(BindersBatchUploadForm, "show");

            model.uploadBatch();

            expect(BindersBatchUploadForm.show).toHaveBeenCalled();
        }));
    });

});

describe('hbBindersIndex', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        element = $compile('<hb-binders-index></hb-binders-index>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("Refresh your items list");
        });
    });
});