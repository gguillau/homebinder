(function() {
    "use strict";

    angular
        .module("hb.components.binders.index.warranties", [
            "hb.components.binders.index.warranties.order"
        ])
        .factory("BindersWarrantiesModalForm", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "components/binders/index/warranties/warranties.tpl.html",
                            controller: "BindersWarrantiesModalFormController as ctrl",
                            resolveData: opts,
                            backdrop: true,
                            closed: opts.closed
                        });
                    }
                };
            }
        ])
        .controller("BindersWarrantiesModalFormController", [
            "$modalInstance",
            "data",
            "hb.api",
            "ModalService",
            "Notify",
            "$log",
            "Loading",
            "hb.framework.indexBase",
            "hb.resources",
            "WarrantyModal",
            "$stateParams",
            "BindersWarrantiesOrderModalForm",
            BindersWarrantiesModalFormController
        ]);

    function BindersWarrantiesModalFormController($modal, opts, api, ModalService, Notify, $log, loading, IndexBase, resources, WarrantyModal, $stateParams, BindersWarrantiesOrderModalForm) {
        var Model = function() {
            // call the parent class
            IndexBase.call(this);
            this.$modal = $modal;
            this.BindersWarrantiesOrderModalForm = BindersWarrantiesOrderModalForm;
            // apply warranty company specific resource strings
            this.resources = angular.extend({}, this.resources, resources.warrantyIndex);
            this.WarrantyModal = WarrantyModal;
            // set the refresh API call
            this.refreshCall = api.warranty.all;
            // set the delete API call
            this.deleteCall = api.warranty.destroy;
            // set the binder_id property used in the delete message
            this.nameProperty = "binder_id";
            this.maxSize = 10;
            this.sortOptions = [{
                orderBy: "warranties.created_at",
                order: "desc",
                desc: this.resources.creationDate + " - " + this.resources.descending
            }, {
                orderBy: "warranties.created_at",
                order: "asc",
                desc: this.resources.creationDate + " - " + this.resources.ascending
            }];
            this.sortOption = this.sortOptions[0];
            this.orderBy = this.sortOption.orderBy;
            this.order = this.sortOption.order;
            this.headers = null;
            this.binderId = opts.binder.id;
            this.queryArgs.binderId = this.binderId;
            this.setHeaders();
            this.refresh();
        };

        Model.prototype = Object.create(IndexBase.prototype);

        angular.extend(Model.prototype, {
            setHeaders: function() {
                this.columnspan = 6;
                this.headers = this.resources.warrantyAttributes.map(angular.bind(this, function(attribute) {
                    var sortable = false,
                        orderBy = null,
                        sorted = false,
                        show = true;
                    if (attribute === "ID") {
                        sorted = true;
                        sortable = true;
                        orderBy = "warranties.id";
                    }
                    if (attribute === "Binder ID") {
                        sortable = true;
                        orderBy = "warranties.binder_id";
                    }
                    else if (attribute === "Status") {
                        sortable = true;
                        orderBy = "warranties.status";
                    }
                    return {
                        name: attribute,
                        sortable: sortable,
                        sorted: sorted,
                        orderBy: orderBy,
                        show: show,
                        order: "desc"
                    };
                }));
                this.headers.splice(1, 1);
                this.headers.splice(1, 1);
                this.sortOption = this.headers[0];
            },

            orderNew: function() {
                this.BindersWarrantiesOrderModalForm.show({
                    binder: opts.binder
                });
                this.$modal.dismiss();
            },

            cancelForm: function() {
                this.$modal.dismiss();
            }
        });

        this.model = new Model();

    }

})();