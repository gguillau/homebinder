describe("BindersWarrantiesModalFormController", function() {
    var controller,
        base,
        api,
        resources,
        BindersWarrantiesOrderModalForm,
        $modalInstance,
        BindersWarrantiesModalForm,
        ModalService;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        BindersWarrantiesOrderModalForm = $injector.get("BindersWarrantiesOrderModalForm");
        BindersWarrantiesModalForm = $injector.get("BindersWarrantiesModalForm");
        ModalService = $injector.get("ModalService");

        $modalInstance = {
            dismiss: function() {}
        };

        spyOn(api.warranty, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));
        spyOn(api.warranty, "destroy").and.returnValue($q.when({}));

        controller = $controller("BindersWarrantiesModalFormController", {
            "hb.framework.indexBase": base,
            "hb.api": api,
            "hb.resources": resources,
            "BindersWarrantiesOrderModalForm": BindersWarrantiesOrderModalForm,
            "$modalInstance": $modalInstance,
            "data": { binder: {} }
        });

        $rootScope.$apply();
    }));

    describe("init", function() {
        it("sets the headers", function() {
            expect(controller.model.headers.length).toEqual(6);
        });
    });

    describe("orderNew", function() {
        it("calls modal", function() {
            spyOn(BindersWarrantiesOrderModalForm, "show");
            controller.model.orderNew();
            expect(BindersWarrantiesOrderModalForm.show).toHaveBeenCalled();
        });
    });

    describe("cancelForm", function() {
        it("calls modal", function() {
            spyOn($modalInstance, "dismiss");
            controller.model.cancelForm();
            expect($modalInstance.dismiss).toHaveBeenCalled();
        });
    });

    describe('BindersWarrantiesModalForm', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            BindersWarrantiesModalForm.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});