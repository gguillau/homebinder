describe("BindersWarrantiesOrderModalFormController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        opts,
        $modal,
        api,
        notify,
        $log,
        loading,
        address,
        $q,
        binder,
        session,
        BindersWarrantiesOrderModalForm,
        ModalService,
        context;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_, _$templateCache_, _$compile_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        BindersWarrantiesOrderModalForm = _$injector_.get("BindersWarrantiesOrderModalForm");
        ModalService = _$injector_.get("ModalService");
        api = _$injector_.get("hb.api");
        loading = _$injector_.get("Loading");
        context = _$injector_.get("Context");
        address = _$injector_.get("Address");

        binder = {
            created_at: "2015-09-06T04:29:36Z",
            id: 1,
            name: "This is a fake home",
            primary: null,
            details: "this is a home",
            partner: {
                address: null,
                binder_count: 7,
                binder_logo_id: null,
                code: "FAKE",
                contact: "Test Inspector",
                coupons: null,
                created_at: "2015-04-25T15:56:15Z",
                email: "gerlininspector@gmail.com",
                id: 115,
                name: "Test Inspector",
                partner_type: "inspector",
                phone: "+17814928454",
                sellers_logo_id: null,
                tags: [],
                vendors: []
            },
            property: {
                property_type: ""
            },
            access_list: [{
                role: "owner",
                user: {
                    accepted_transfer_at: "2017-04-26",
                    created_at: "2017-04-26T13:36:55.460-04:00",
                    email: "greg+accepttest@homebinder.com",
                    id: 61634,
                    last_sign_in_at: "2017-04-26T13:38:10.423-04:00",
                    role: "homeowner",
                    sign_in_count: 1,
                    user_profile_attributes: {
                        id: 61560,
                        user_id: 61634,
                        first_name: "Acceptance",
                        last_name: "Test",
                        home_phone: null,
                        address_attributes: {
                            country: "US",
                            name: null,
                            address1: "123 Accept St",
                            address2: null,
                            city: "Boston",
                            state: "MA",
                            zip: "02128"
                        },
                        bio: null,
                        company: null,
                        dob: null,
                        email: "greg+accepttest@homebinder.com",
                        head_shot_file: null,
                        logo_file: null,
                        message: null,
                        mobile_phone: "",
                        monthly_email: false,
                        sex: null,
                        website: null
                    }
                }
            }]
        };

        $modal = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };

        notify = {
            success: function() {},
            error: function() {},
            info: function() {}
        };

        $log = {
            error: function() {}
        };

        session = {
            getUser: function() {}
        };

    }));

    function createController(binder) {
        opts = {
            binder: binder
        };

        $scope = $rootScope.$new();
        ctrl = $controller("BindersWarrantiesOrderModalFormController", {
            "$modalInstance": $modal,
            "Address": address,
            "data": opts,
            "hb.api": api,
            "Notify": notify,
            "$log": $log,
            "Loading": loading,
            "Session": session,
            "Context": context
        });

        $scope.ctrl = ctrl;
    }

    describe("ctrl.refresh", function() {
        it("should call notify because partner doesnt exist", function() {
            spyOn(session, "getUser").and.returnValue({
                role: "admin"
            });

            spyOn(api.binder, "get").and.returnValue($q.when({
                data: binder
            }));

            spyOn(api.binderTransaction, "all").and.returnValue($q.when({
                data: {
                    items: [{
                        transaction_type: "buy_side_inspection",
                        transaction_date: ""
                    }]
                }
            }));

            spyOn(api.binder, "getPartner").and.returnValue($q.when({
                data: {}
            }));

            spyOn(notify, "info");

            createController(binder);
            $rootScope.$apply();
            ctrl.refresh();
            $rootScope.$apply();

            expect(notify.info).toHaveBeenCalled();
        });

        it("should call notify because partner warranties_configuration is undefined", function() {
            spyOn(session, "getUser").and.returnValue({
                role: "admin"
            });

            spyOn(api.binder, "get").and.returnValue($q.when({
                data: binder
            }));

            spyOn(api.binderTransaction, "all").and.returnValue($q.when({
                data: {
                    items: [{
                        transaction_type: "buy_side_inspection",
                        transaction_date: ""
                    }]
                }
            }));

            spyOn(api.binder, "getPartner").and.returnValue($q.when({
                data: {
                    id: 1
                }
            }));

            spyOn(notify, "info");

            createController(binder);
            $rootScope.$apply();
            ctrl.refresh();
            $rootScope.$apply();

            expect(notify.info).toHaveBeenCalled();
        });

        it("retrieves partner warranties_configuration when user is an admin", function() {
            spyOn(session, "getUser").and.returnValue({
                role: "admin"
            });

            spyOn(api.binder, "get").and.returnValue($q.when({
                data: binder
            }));

            spyOn(api.warrantyConfiguration, "get").and.returnValue($q.when({
                data: {
                    warranty_plan_id: 2
                }
            }));

            spyOn(api.partner, "warrantyPlans").and.returnValue($q.when({
                data: [{
                    plan: {
                        id: 2
                    }
                }]
            }));

            spyOn(api.binderTransaction, "all").and.returnValue($q.when({
                data: {
                    items: [{
                        transaction_type: "buy_side_inspection",
                        transaction_date: ""
                    }]
                }
            }));

            spyOn(api.binder, "getPartner").and.returnValue($q.when({
                data: {
                    id: 1,
                    warranties_configuration: {
                        id: 1
                    }
                }
            }));

            spyOn(notify, "info");

            createController(binder);
            $rootScope.$apply();
            ctrl.refresh();
            $rootScope.$apply();

            expect(ctrl.configuration).toEqual({
                warranty_plan_id: 2
            });
            expect(ctrl.partnerPlans.length).toBe(1);
        });

        it("retrieves partner warranties_configuration when user is a partner", function() {
            spyOn(session, "getUser").and.returnValue({
                role: "inspector"
            });

            spyOn(api.binder, "get").and.returnValue($q.when({
                data: binder
            }));

            spyOn(api.warrantyConfiguration, "get").and.returnValue($q.when({
                data: {
                    warranty_plan_id: 2
                }
            }));

            spyOn(api.partner, "warrantyPlans").and.returnValue($q.when({
                data: [{
                    plan: {
                        id: 2
                    }
                }]
            }));

            spyOn(api.binderTransaction, "all").and.returnValue($q.when({
                data: {
                    items: [{
                        transaction_type: "buy_side_inspection",
                        transaction_date: ""
                    }]
                }
            }));

            spyOn(api.binder, "getPartner").and.returnValue($q.when({
                data: {
                    id: 1,
                    warranties_configuration: {
                        id: 1
                    }
                }
            }));

            spyOn(notify, "info");

            spyOn(context, "getPartner").and.returnValue($q.when({ id: 1 }));
            createController(binder);
            ctrl.refresh();
            $rootScope.$apply();
            
            expect(notify.info).toHaveBeenCalled();
        });

        it("retrieves partner warranties_configuration when user is a partner", function() {
            spyOn(session, "getUser").and.returnValue({
                role: "inspector"
            });

            spyOn(api.binder, "get").and.returnValue($q.when({
                data: binder
            }));

            spyOn(api.warrantyConfiguration, "get").and.returnValue($q.when({
                data: {
                    warranty_plan_id: 2
                }
            }));

            spyOn(api.partner, "warrantyPlans").and.returnValue($q.when({
                data: [{
                    plan: {
                        id: 2
                    }
                }]
            }));

            spyOn(api.binderTransaction, "all").and.returnValue($q.when({
                data: {
                    items: [{
                        transaction_type: "buy_side_inspection",
                        transaction_date: ""
                    }]
                }
            }));

            spyOn(api.binder, "getPartner").and.returnValue($q.when({
                data: {
                    id: 1,
                    warranties_configuration: {
                        id: 1
                    }
                }
            }));

            spyOn(notify, "info");

            spyOn(context, "getPartner").and.returnValue($q.when({ id: 1 }));
            createController(binder);
            spyOn(ctrl, "getConfig");
            ctrl.refresh();

            expect(notify.info).not.toHaveBeenCalled();
        });
    });

    describe("ctrl.getPartnerSuccess", function() {
        it("should call notify because partner doesnt exist", function() {
            spyOn(notify, "info");

            createController();
            ctrl.getPartnerSuccess({ data: null });

            expect(notify.info).toHaveBeenCalled();
        });
    });

    describe("ctrl.getPartnerError", function() {
        it("should call notify and $log", function() {
            spyOn(notify, "info");

            createController();
            ctrl.getPartnerError({ data: "error" });

            expect(notify.info).toHaveBeenCalled();
        });
    });

    describe("ctrl.getBinderTransactions", function() {

        it("retrieves the transactions", function() {
            binder = null;
            var date = new Date("2012/09/11");
            spyOn(session, "getUser").and.returnValue({
                role: "admin"
            });

            spyOn(api.binderTransaction, "all").and.returnValue($q.when({
                data: {
                    items: [{
                        transaction_type: "buy_side_inspection",
                        transaction_date: date
                    }]
                }
            }));

            createController(null);
            $rootScope.$apply();
            ctrl.binder = {
                id: 1
            };
            ctrl.getBinderTransactions();
            $rootScope.$apply();

            expect(api.binderTransaction.all).toHaveBeenCalledWith(1);
            expect(ctrl.transactions.length).toBe(1);
            expect(ctrl.inspection_date).toEqual(date);
        });

        it("returns an error", function() {
            binder = null;
            spyOn(session, "getUser").and.returnValue({
                role: "admin"
            });

            spyOn(api.binderTransaction, "all").and.returnValue($q.reject({
                data: "error"
            }));

            spyOn($log, "error");

            createController(null);
            $rootScope.$apply();
            ctrl.binder = {
                id: 1
            };
            ctrl.getBinderTransactions();
            $rootScope.$apply();

            expect(api.binderTransaction.all).toHaveBeenCalledWith(1);
            expect($log.error).toHaveBeenCalled();
        });

    });

    describe("ctrl.getConfig", function() {

        it("retrieves the config", function() {
            binder = null;

            spyOn(session, "getUser").and.returnValue({
                role: "admin"
            });

            spyOn(api.warrantyConfiguration, "get").and.returnValue($q.when({
                data: {
                    id: 1,
                    warranty_plan_id: 1
                }
            }));

            spyOn(api.partner, "warrantyPlans").and.returnValue($q.when({
                data: [{
                    plan: {
                        id: 2
                    }
                }]
            }));

            createController(null);
            $rootScope.$apply();
            ctrl.partner = {
                id: 1,
                warranties_configuration: {
                    id: 1
                }
            };
            ctrl.getConfig();
            $rootScope.$apply();

            expect(api.warrantyConfiguration.get).toHaveBeenCalledWith(1);
            expect(ctrl.configuration).toEqual({
                id: 1,
                warranty_plan_id: 1
            });
        });

        it("returns an error", function() {
            binder = null;
            spyOn(session, "getUser").and.returnValue({
                role: "admin"
            });

            spyOn(api.warrantyConfiguration, "get").and.returnValue($q.reject({
                data: "error"
            }));

            spyOn(api.partner, "warrantyPlans").and.returnValue($q.when({
                data: [{
                    plan: {
                        id: 2
                    }
                }]
            }));

            spyOn($log, "error");

            createController(null);
            $rootScope.$apply();
            ctrl.partner = {
                id: 1,
                warranties_configuration: {
                    id: 1
                }
            };
            ctrl.getConfig();
            $rootScope.$apply();

            expect(api.warrantyConfiguration.get).toHaveBeenCalledWith(1);
            expect($log.error).toHaveBeenCalled();
        });

    });

    describe("ctrl.getBinder", function() {

        it("retrieves the binder", function() {

            spyOn(session, "getUser").and.returnValue({
                role: "admin"
            });

            spyOn(api.binder, "get").and.returnValue($q.when({
                data: binder
            }));

            createController(null);
            $rootScope.$apply();
            ctrl.binder = {
                id: 1
            };
            ctrl.getBinder();
            $rootScope.$apply();

            expect(api.binder.get).toHaveBeenCalledWith(1);
        });

        it("returns an error", function() {
            binder = null;
            spyOn(session, "getUser").and.returnValue({
                role: "admin"
            });

            spyOn(api.binder, "get").and.returnValue($q.reject({
                data: "error"
            }));

            spyOn($log, "error");

            createController(null);
            $rootScope.$apply();
            ctrl.binder = {
                id: 1
            };
            ctrl.getBinder();
            $rootScope.$apply();

            expect(api.binder.get).toHaveBeenCalledWith(1);
            expect($log.error).toHaveBeenCalled();
        });

    });

    describe("ctrl.getBinderOwner", function() {
        it("returns null owner", inject(function(ActivateAccountModal) {
            createController();
            ctrl.binder = {
                access_list: []
            };
            var owner = ctrl.getBinderOwner();

            expect(owner).toBeNull();
        }));

    });

    describe("ctrl.getUser", function() {

        it("retrieves the user", function() {

            spyOn(session, "getUser").and.returnValue({
                role: "admin"
            });

            spyOn(api.user, "get").and.returnValue($q.when({
                data: {
                    id: 1,
                    user_profile_attributes: {
                        address_attributes: {

                        },
                        first_name: "Mike",
                        last_name: "Carson",
                        mobile_phone: "123-144-12312"
                    },
                    email: "mikec@test.com"
                }
            }));

            createController(null);
            $rootScope.$apply();
            ctrl.transfer = {
                receiver_id: 1
            };
            ctrl.getUser();
            $rootScope.$apply();

            expect(api.user.get).toHaveBeenCalledWith(1);
        });

        it("returns an error", function() {
            binder = null;
            spyOn(session, "getUser").and.returnValue({
                role: "admin"
            });

            spyOn(api.user, "get").and.returnValue($q.reject({
                data: "error"
            }));

            spyOn($log, "error");

            createController(null);
            $rootScope.$apply();
            ctrl.transfer = {
                receiver_id: 1
            };
            ctrl.getUser();
            $rootScope.$apply();

            expect(api.user.get).toHaveBeenCalledWith(1);
            expect($log.error).toHaveBeenCalled();
        });

    });

    describe("ctrl.getPartnerPlans", function() {

        it("retrieves the partnerPlans", function() {

            spyOn(session, "getUser").and.returnValue({
                role: "admin"
            });

            spyOn(api.partner, "warrantyPlans").and.returnValue($q.when({
                data: [{
                    plan: {
                        id: 2
                    }
                }]
            }));

            createController(null);
            $rootScope.$apply();
            ctrl.partner = {
                id: 1
            };
            ctrl.configuration = {
                warranty_plan_id: 1
            };
            ctrl.getPartnerPlans();
            $rootScope.$apply();

            expect(api.partner.warrantyPlans).toHaveBeenCalledWith(1);
        });

        it("returns an error", function() {
            binder = null;
            spyOn(session, "getUser").and.returnValue({
                role: "admin"
            });

            spyOn(api.partner, "warrantyPlans").and.returnValue($q.reject({
                data: "error"
            }));

            spyOn($log, "error");

            createController(null);
            $rootScope.$apply();
            ctrl.partner = {
                id: 1
            };
            ctrl.getPartnerPlans();
            $rootScope.$apply();

            expect(api.partner.warrantyPlans).toHaveBeenCalledWith(1);
            expect($log.error).toHaveBeenCalled();
        });

    });

    describe("ctrl.onPartnerSelect", function() {

        it("retrieves the partnerPlans and sets warrantyConfiguration fields", function() {

            spyOn(session, "getUser").and.returnValue({
                role: "admin"
            });

            spyOn(api.partner, "warrantyPlans").and.returnValue($q.when({
                data: [{
                    plan: {
                        id: 2
                    }
                }]
            }));

            createController(null);
            $rootScope.$apply();
            ctrl.warranty_configuration = {
                warranty_plans: [{
                    id: 2
                }],
                warranty_company: {
                    name: "Name"
                },
                price: 25
            };
            ctrl.partner = {
                id: 1
            };
            ctrl.configuration = {
                warranty_plan_id: 1
            };
            ctrl.onPartnerSelect();
            $rootScope.$apply();

            expect(api.partner.warrantyPlans).toHaveBeenCalledWith(1);
            expect(ctrl.warranty_plans.length).toBe(1);
            expect(ctrl.warranty_company.name).toBe("Name");
            expect(ctrl.price).toBe(25);
        });

    });

    describe("ctrl.binderLookup", function() {
        it("calls api binder all for admin", function() {
            spyOn(api.binder, "all").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.currentUser = {
                role: "admin"
            };
            ctrl.binderLookup("search");
            $rootScope.$apply();

            expect(api.binder.all).toHaveBeenCalled();
        });

        it("calls api binder all for inspector", function() {
            spyOn(api.binder, "all").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.currentUser = {
                role: "inspector"
            };
            ctrl.binderLookup("search");

            expect(api.binder.all).toHaveBeenCalled();
        });
    });

    describe("ctrl.onBinderSelect", function() {
        it("calls refresh", function() {
            createController();
            spyOn(ctrl, "refresh");
            ctrl.onBinderSelect();

            expect(ctrl.refresh).toHaveBeenCalled();
        });
    });

    describe("ctrl.configurationLookup", function() {
        it("calls api warrantyConfiguration all for admin", function() {
            spyOn(api.warrantyConfiguration, "all").and.returnValue($q.when({ data: {} }));
            createController();
            ctrl.currentUser = {
                role: "admin"
            };
            ctrl.configurationLookup("search");
            $rootScope.$apply();

            expect(api.warrantyConfiguration.all).toHaveBeenCalled();
        });
    });

    describe("ctrl.submit", function() {

        it("does not submit the form", function() {

            spyOn(session, "getUser").and.returnValue({
                role: "admin"
            });

            spyOn(api.warrantyConfiguration, "order");

            createController(null);
            $rootScope.$apply();
            ctrl.form = {
                $invalid: true
            };
            ctrl.submit();
            $rootScope.$apply();

            expect(api.warrantyConfiguration.order).not.toHaveBeenCalled();
        });

        it("submits the order", function() {

            spyOn(session, "getUser").and.returnValue({
                role: "admin"
            });

            binder.transfer = {
                receiver_id: 1
            };

            spyOn(api.binder, "get").and.returnValue($q.when({
                data: binder
            }));

            spyOn(api.user, "get").and.returnValue($q.when({
                data: {
                    id: 1,
                    user_profile_attributes: {
                        address_attributes: {

                        },
                        first_name: "Mike",
                        last_name: "Carson",
                        mobile_phone: "123-144-12312"
                    },
                    email: "mikec@test.com"
                }
            }));

            spyOn(api.warrantyConfiguration, "get").and.returnValue($q.when({
                data: {
                    warranty_plan_id: 2,
                    account_number: "NUMB",
                    warranty_company: {
                        id: 1
                    }
                }
            }));

            spyOn(api.partner, "warrantyPlans").and.returnValue($q.when({
                data: [{
                    plan: {
                        id: 2
                    }
                }]
            }));

            spyOn(api.binderTransaction, "all").and.returnValue($q.when({
                data: {
                    items: [{
                        transaction_type: "buy_side_inspection",
                        transaction_date: ""
                    }]
                }
            }));

            spyOn(api.binder, "getPartner").and.returnValue($q.when({
                data: {
                    id: 1,
                    warranties_configuration: {
                        id: 1
                    }
                }
            }));

            spyOn(api.warrantyConfiguration, "order").and.returnValue($q.when({
                data: "success"
            }));

            spyOn(notify, "success");
            spyOn($modal, "close");

            createController(binder);
            $rootScope.$apply();
            ctrl.form = {
                $invalid: false
            };
            ctrl.submit();
            $rootScope.$apply();

            expect(api.warrantyConfiguration.order).toHaveBeenCalled();
            expect(notify.success).toHaveBeenCalled();
            expect($modal.close).toHaveBeenCalled();
        });

        it("submits the order and returns an error", function() {

            spyOn(session, "getUser").and.returnValue({
                role: "admin"
            });

            binder.transfer = {
                receiver_id: 1
            };

            spyOn(api.binder, "get").and.returnValue($q.when({
                data: binder
            }));

            spyOn(api.user, "get").and.returnValue($q.when({
                data: {
                    id: 1,
                    user_profile_attributes: {
                        address_attributes: {

                        },
                        first_name: "Mike",
                        last_name: "Carson",
                        mobile_phone: "123-144-12312"
                    },
                    email: "mikec@test.com"
                }
            }));

            spyOn(api.warrantyConfiguration, "get").and.returnValue($q.when({
                data: {
                    warranty_plan_id: 2,
                    account_number: "NUMB",
                    warranty_company: {
                        id: 1
                    }
                }
            }));

            spyOn(api.partner, "warrantyPlans").and.returnValue($q.when({
                data: [{
                    plan: {
                        id: 2
                    }
                }]
            }));

            spyOn(api.binderTransaction, "all").and.returnValue($q.when({
                data: {
                    items: [{
                        transaction_type: "buy_side_inspection",
                        transaction_date: ""
                    }]
                }
            }));

            spyOn(api.binder, "getPartner").and.returnValue($q.when({
                data: {
                    id: 1,
                    warranties_configuration: {
                        id: 1
                    }
                }
            }));

            spyOn(api.warrantyConfiguration, "order").and.returnValue($q.reject({
                data: "error"
            }));

            spyOn(notify, "error");
            spyOn($log, "error");

            createController(binder);
            $rootScope.$apply();
            ctrl.form = {
                $invalid: false
            };
            ctrl.submit();
            $rootScope.$apply();

            expect(api.warrantyConfiguration.order).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
        });

    });

    describe("ctrl.cancelForm", function() {

        it("should call $modalInstance dismiss function", function() {

            spyOn($modal, "dismiss");

            createController(null);
            $rootScope.$apply();
            ctrl.cancelForm();
            $rootScope.$apply();

            expect($modal.dismiss).toHaveBeenCalled();
        });

    });

    describe("ctrl.open", function() {
        it("updates the date", inject(function(ActivateAccountModal) {
            createController();

            expect(ctrl.date[0].opened).toBe(false);
            ctrl.open(0);

            expect(ctrl.date[0].opened).toBe(true);
        }));

    });

    describe('ctrl.onSelect', function() {
        it('sets the binder property country', function() {
            createController();
            spyOn(ctrl.address, "setCountry");

            ctrl.binder = {
                property: {}
            };
            ctrl.binder.property.state = {};

            ctrl.onSelect({ country: { value: "US" } });

            expect(ctrl.binder.property.country).toEqual("US");
        });
    });

    describe('ctrl.onSelectCountry', function() {
        it('sets the country', function() {
            createController();
            var item = { country: { value: "US" } };
            ctrl.onSelectCountry(item.country);

            expect(ctrl.country.value).toEqual("US");
        });
    });

    describe('BindersWarrantiesOrderModalForm', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            BindersWarrantiesOrderModalForm.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });

});