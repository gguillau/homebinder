(function() {
    "use strict";

    angular
        .module("hb.components.binders.index.warranties.order", [])
        .factory("BindersWarrantiesOrderModalForm", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "components/binders/index/warranties/order/order.tpl.html",
                            controller: "BindersWarrantiesOrderModalFormController as ctrl",
                            resolveData: opts,
                            backdrop: true,
                            closed: opts.closed
                        });
                    }
                };
            }
        ])
        .controller("BindersWarrantiesOrderModalFormController", [
            "$modalInstance",
            "data",
            "hb.api",
            "Notify",
            "$log",
            "Loading",
            "Session",
            "Context",
            "$filter",
            "Address",
            "$stateParams",
            BindersWarrantiesOrderModalFormController
        ]);

    function BindersWarrantiesOrderModalFormController($modal, opts, api, Notify, $log, loading, Session, Context, $filter, address, $stateParams) {
        this.$modal = $modal;
        this.Session = Session;
        this.context = Context;
        this.$log = $log;
        this.api = api;
        this.notify = Notify;
        this.$filter = $filter;
        this.currentUser = this.Session.getUser();
        this.binder = opts.binder;
        this.address = address;
        this.$stateParams = $stateParams;
        this.countries = address.countries();
        this.states = address.getStatesAndProvinces();
        this.country = this.countries[0];
        this.companyLookupCfg = {
            allowClear: true,
            placeholder: "Search for company",
            refresh: angular.bind(this, this.companyLookup),
            itemTemplate: "components/lookup/partner/partnerDropDownItem.tpl.html",
            selectionTemplate: "components/lookup/partner/partnerSelection.tpl.html"
        };
        this.planLookupCfg = {
            allowClear: true,
            placeholder: "Search for plan",
            refresh: angular.bind(this, this.planLookup),
            itemTemplate: "components/lookup/partner/partnerDropDownItem.tpl.html",
            selectionTemplate: "components/lookup/partner/partnerSelection.tpl.html"
        };
        this.binderLookupCfg = {
            allowClear: true,
            placeholder: "Search for binder",
            refresh: angular.bind(this, this.binderLookup),
            itemTemplate: "components/lookup/partner/partnerDropDownItem.tpl.html",
            selectionTemplate: "components/lookup/partner/partnerSelection.tpl.html"
        };
        this.configurationLookupCfg = {
            allowClear: true,
            placeholder: "Search for partner",
            refresh: angular.bind(this, this.configurationLookup),
            itemTemplate: "components/lookup/partner/partnerDropDownItem.tpl.html",
            selectionTemplate: "components/lookup/partner/partnerSelection.tpl.html"
        };
        this.datepicker = {
            format: "MMMM dd, yyyy",
            options: {
                "show-button-bar": false
            }
        };
        this.date = [{
            opened: false
        }, {
            opened: false
        }];
        this.refresh();
    }

    BindersWarrantiesOrderModalFormController.prototype = {

        refresh: function() {
            if (this.binder) {
                this.getBinder();
                this.getBinderTransactions();
                // if admin get the partner
                if (this.currentUser.role === "admin") {
                    this.api.binder.getPartner(this.binder.id).then(
                        angular.bind(this, this.getPartnerSuccess),
                        angular.bind(this, this.getPartnerError));
                }
                else {
                    // else if partner, get the warranty config
                    this.context.getPartner().then(angular.bind(this, function(partner) {
                        this.partner = partner;
                        if (this.partner.warranties_configuration) {
                            this.getConfig();
                        }
                        else {
                            this.notify.info("The partner branded on this binder does not have a warranty account set up.");
                            this.$modal.dismiss();
                        }
                    }));
                }
            }
        },

        getPartnerSuccess: function(response) {
            if (response.data) {
                this.partner = response.data;
                if (this.partner.warranties_configuration) {
                    this.getConfig();
                }
                else {
                    this.notify.info("The partner branded on this binder does not have a warranty account set up.");
                }
            }
            else {
                // if no partner added to binder then we allow admin to choose
                this.notify.info("There is no partner branded on this binder");
            }
        },

        getPartnerError: function(response) {
            this.$log.error(response);
            // if no partner added to binder then we allow admin to choose
            this.notify.info("There was an error retrieving the warranty configuration. Please try again later.");
            this.$modal.dismiss();
        },

        getBinderTransactions: function() {
            this.api.binderTransaction.all(this.binder.id).then(
                angular.bind(this, this.transactionSuccess),
                angular.bind(this, this.transactionError));
        },

        transactionSuccess: function(response) {
            // get the transactions and check if we have a buy_side_inspection
            this.transactions = response.data.items.filter(function(transaction) {
                return transaction.transaction_type === "buy_side_inspection";
            });
            if (this.transactions.length > 0) {
                if (this.transactions[0].transaction_date) {
                    this.inspection_date = new Date(this.transactions[0].transaction_date);
                }
            }
        },

        transactionError: function(response) {
            this.$log.error(response);
        },

        getConfig: function() {
            this.api.warrantyConfiguration.get(this.partner.warranties_configuration.id).then(
                angular.bind(this, this.getConfigSuccess),
                angular.bind(this, this.getConfigError));
        },

        getConfigSuccess: function(response) {
            this.configuration = response.data;
            this.getPartnerPlans();
        },

        getConfigError: function(response) {
            this.$log.error(response);
        },

        getBinder: function() {
            this.api.binder.get(this.binder.id).then(
                angular.bind(this, this.getBinderSuccess),
                angular.bind(this, this.getBinderError));
        },

        getBinderSuccess: function(response) {
            this.binder = response.data;
            this.transfer = response.data.transfer;
            this.country = this.address.findCountryByCode(this.binder.property.country);
            this.states = this.address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                return state.country.value === this.country.value;
            }));
            if (this.transfer) {
                this.getUser();
            }
            else {
                var owner = this.getBinderOwner();
                this.getUserSuccess({
                    data: owner.user
                });
            }
        },

        getBinderOwner: function() {
            // get an exact
            var owners = this.$filter("filter")(this.binder.access_list, {
                role: "owner"
            }, true);
            if (owners.length > 0) {
                return owners[0];
            }
            else {
                return null;
            }
        },

        getBinderError: function(response) {
            this.$log.error(response);
        },

        getUser: function() {
            this.api.user.get(this.transfer.receiver_id).then(
                angular.bind(this, this.getUserSuccess),
                angular.bind(this, this.getUserError));
        },

        getUserSuccess: function(response) {
            this.homeowner = {
                id: response.data.id,
                firstName: response.data.user_profile_attributes.first_name,
                lastName: response.data.user_profile_attributes.last_name,
                email: response.data.email,
                phone: response.data.user_profile_attributes.mobile_phone.national
            };
            this.country = this.address.findCountryByCode(response.data.user_profile_attributes.address_attributes.country);
        },

        getUserError: function(response) {
            this.$log.error(response);
        },

        getPartnerPlans: function() {
            this.api.partner.warrantyPlans(this.partner.id).then(
                angular.bind(this, this.partnerPlanSuccess),
                angular.bind(this, this.partnerPlanError));
        },

        partnerPlanSuccess: function(response) {
            this.partnerPlans = response.data;
            this.partnerPlans.forEach(angular.bind(this, function(plan) {
                if (plan.plan.id === this.configuration.warranty_plan_id) {
                    this.warranty_plan = plan;
                }
            }));
        },

        partnerPlanError: function(response) {
            this.$log.error(response);
        },

        onPartnerSelect: function() {
            this.warranty_plans = this.warranty_configuration.warranty_plans;
            this.warranty_company = this.warranty_configuration.warranty_company;
            this.price = this.warranty_configuration.price;
            this.getPartnerPlans();
        },

        binderLookup: function(value) {
            var opts = {
                page: 1,
                count: 50,
                search: value
            };

            if (this.currentUser.role === "admin") {
                opts.searchMethod = "for_admin";
            }
            else {
                opts.searchMethod = "for_partner";
                opts.partnerId = this.$stateParams.partnerId;
            }

            return this.api.binder.all(opts).then(
                angular.bind(this, function(response) {
                    return {
                        data: response.data.items
                    };
                })
            );
        },

        onBinderSelect: function() {
            this.refresh();
        },

        configurationLookup: function(value) {
            var opts = {
                page: 1,
                count: 50,
                search: value
            };
            return this.api.warrantyConfiguration.all(opts).then(
                angular.bind(this, function(response) {
                    return {
                        data: response.data.items
                    };
                })
            );
        },

        submit: function() {
            this.form.$submitted = true;
            if (this.form.$invalid) {
                return;
            }

            var homeowner = {
                id: this.homeowner.id,
                firstName: this.homeowner.firstName,
                lastName: this.homeowner.lastName,
                email: this.homeowner.email,
                phone: "+" + this.country.code + this.homeowner.phone
            };

            var order = {
                binder_id: this.binder.id,
                warranty_company_id: this.configuration.warranty_company.id,
                warranty_plan_id: this.warranty_plan.plan.id,
                account_number: this.configuration.account_number,
                price: this.warranty_plan.price,
                homeowner: homeowner,
                property: this.binder.property,
                inspection_date: this.inspection_date
            };

            this.api.warrantyConfiguration.order(this.configuration.id, order).then(
                angular.bind(this, this.orderSuccess),
                angular.bind(this, this.orderError));
        },

        orderSuccess: function(response) {
            this.notify.success("Warranty has been succesfully ordered.");
            this.$modal.close(response.data);
        },

        orderError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
        },

        open: function(index) {
            this.date[index].opened = true;
        },

        onSelect: function(item) {
            if (item) {
                this.binder.property.country = item.country.value;
            }
        },

        onSelectCountry: function(item) {
            this.country = item;
            this.states = this.address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                return state.country.value === this.country.value;
            }));
        },

        cancelForm: function() {
            this.$modal.dismiss();
        }
    };

})();
