(function() {
    "use strict";

    angular
        .module("hb.components.binders.index.upload", [])
        .factory("BindersImageUploadForm", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "components/binders/index/upload/upload.tpl.html",
                            controller: "BindersImageUploadFormController as ctrl",
                            resolveData: opts,
                            backdrop: true,
                            closed: opts.closed
                        });
                    }
                };
            }
        ])
        .controller("BindersImageUploadFormController", [
            "$modalInstance",
            "data",
            "Address",
            "hb.api",
            "Notify",
            "$log",
            "Loading",
            "Session",
            "hb.resources",
            BindersImageUploadFormController
        ]);

    function BindersImageUploadFormController($modal, opts, Address, api, notify, $log, loading, session, resources) {
        this.$modal = $modal;
        this.Address = Address;
        this.api = api;
        this.notify = notify;
        this.$log = $log;
        this.loading = loading;
        this.binder = opts.binder;
        this.session = session;
        this.resources = resources.bindersUpload;
        this.uploader = {
            fileTypes: "image",
            hideUploadButton: true,
            showTable: true,
            selectButtonLabel: "Upload property photo",
            fileLimit: true,
            url: "/api/v1/images",
            jwt: this.session.getJwt(),
            multiSelect: false,
            uploadOnAdd: true
        };
    }

    BindersImageUploadFormController.prototype = {

        uploadImage: function() {
            var uploadApi = this.uploader.api;
            if (uploadApi && uploadApi.pendingUploads().length > 0) {
                this.is_uploading = true;
                uploadApi.updateParams({
                    binder_id: this.binder.id,
                    hero_image_id: true
                });
                uploadApi.uploadFiles();
            }
        },

        uploadSuccess: function(response) {
            this.is_uploading = false;
            this.$modal.close("confirm");
            this.notify.success("Image successfully uploaded.");
        },

        uploadError: function(response) {
            this.is_uploading = false;
            this.$log.error(response);
            this.notify.error(response.data);
        },

        cancel: function() {
            this.$modal.dismiss("cancel");
        }

    };
})();