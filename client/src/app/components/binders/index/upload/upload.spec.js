describe("BindersImageUploadFormController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        binder,
        opts,
        $modal,
        api,
        notify,
        $log,
        loading,
        $q,
        BindersImageUploadForm,
        ModalService;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        BindersImageUploadForm = _$injector_.get("BindersImageUploadForm");
        ModalService = _$injector_.get("ModalService");

        api = {
            binder: {
                update: function(id, data) {}
            }

        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        $modal = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };

        binder = {
            appliances: null,
            created_at: "2015-09-06T04:29:36Z",
            id: 1,
            name: "This is a fake home",
            primary: null,
            details: "this is a home",
            last_recall: null,
            partner: {
                address: null,
                binder_count: 7,
                binder_logo_id: null,
                code: "FAKE",
                contact: "Test Inspector",
                coupons: null,
                created_at: "2015-04-25T15:56:15Z",
                email: "gerlininspector@gmail.com",
                id: 115,
                name: "Test Inspector",
                partner_type: "inspector",
                phone: "+17814928454",
                sellers_logo_id: null,
                tags: [],
                vendors: []
            },
            vendor: null,
            recalls: 0,
            documents: null,
            maintenance_items: null,
            binder_contractors: null,
            property: {
                property_type: ""
            },
            subscription: null,
            seller_report: null,
            permissions: {
                can_create: true,
                can_create_seller_report: true,
                can_destroy: true,
                can_edit_seller_report: true,
                can_read: true,
                can_share: true,
                can_subscribe: true,
                can_transfer: true,
                can_view_master_report: false,
                can_write: true
            },
            tags: [{
                id: 1042,
                tag: "client",
                metadata: "{\"firstName\":\"John\",\"lastName\":\"Smith\",\"email\":\"johnsmith@gmail.com\"}",
                tag_scope: null
            }]
        };

        opts = {
            binder: binder,
            closed: function() {}
        };

        notify = {
            success: function() {},
            error: function() {}
        };

        $log = {
            error: function() {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("BindersImageUploadFormController", {
            "$modalInstance": $modal,
            "data": opts,
            "hb.api": api,
            "Notify": notify,
            "$log": $log,
            "Loading": loading
        });

        $scope.ctrl = ctrl;
    }

    describe('ctrl.uploadImage', function() {

        it('should call upload files', function() {
            var api = {
                uploadFiles: function() {},
                updateParams: function() {},
                pendingUploads: function() {}
            };
            createController();
            ctrl.uploader.api = api;

            spyOn(ctrl.uploader.api, "pendingUploads").and.returnValue([{}]);
            spyOn(ctrl.uploader.api, "updateParams");
            spyOn(ctrl.uploader.api, "uploadFiles");

            $rootScope.$apply();
            ctrl.uploadImage();

            expect(ctrl.uploader.api.pendingUploads).toHaveBeenCalled();
            expect(ctrl.uploader.api.updateParams).toHaveBeenCalled();
            expect(ctrl.uploader.api.uploadFiles).toHaveBeenCalled();
        });

    });

    describe('ctrl.uploadSuccess', function() {

        it('should call click and notify success functions', function() {

            spyOn(notify, "success");
            createController();
            $rootScope.$apply();
            spyOn($modal, "close");
            ctrl.uploadSuccess();

            expect($modal.close).toHaveBeenCalledWith("confirm");
            expect(notify.success).toHaveBeenCalledWith("Image successfully uploaded.");

        });

    });

    describe('ctrl.uploadError', function() {

        it('should set uploading to false and call notify error function', function() {

            spyOn(notify, "error");
            createController();
            $rootScope.$apply();
            ctrl.uploadError({
                data: "error"
            });

            expect(ctrl.is_uploading).toBe(false);
            expect(notify.error).toHaveBeenCalledWith("error");

        });

    });

    describe('ctrl.cancel', function() {

        it('should close the modal', function() {
            spyOn($modal, "dismiss");
            createController();
            $rootScope.$apply();
            ctrl.cancel();

            expect($modal.dismiss).toHaveBeenCalled();
        });

    });

    describe('BindersImageUploadForm', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            BindersImageUploadForm.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });

});