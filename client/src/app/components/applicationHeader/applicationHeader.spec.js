describe("ApplicationHeaderController", function() {
    var ctrl,
        controller,
        resources,
        User,
        $state,
        $stateParams,
        Context,
        AfterLogin,
        Binder,
        appDelegate,
        $q_,
        loading;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector, $q) {
        $q_ = $q;
        controller = $controller;
        resources = $injector.get("hb.resources");
        User = $injector.get("User");
        $state = $injector.get("$state");
        $stateParams = $injector.get("$stateParams");
        Context = $injector.get("Context");
        AfterLogin = $injector.get("AfterLogin");
        Binder = $injector.get("Binder");
        appDelegate = $injector.get("AppDelegate");
        loading = $injector.get("Loading");
    }));

    function createController() {
        ctrl = controller("ApplicationHeaderController", {
            "hb.resources": resources,
            "User": User,
            "$state": $state,
            "$stateParams": $stateParams,
            "Context": Context,
            "AfterLogin": AfterLogin,
            "AppDelegate": appDelegate,
            "Loading": loading
        });
    }

    describe("init", function() {
        it("doesnt show the binders", function() {
            var user = { role: "admin" };

            spyOn(User, "current").and.returnValue(user);
            var name = {
                startsWith: function() {}
            };
            $state.current.name = name;
            spyOn($state.current.name, "startsWith").and.returnValue(false);

            createController();

            expect(ctrl.showMyBinders).toEqual(false);
        });

        it("shows the binders", function() {
            var user = { role: "homeowner" };

            spyOn(User, "current").and.returnValue(user);
            spyOn(Context, "getBinder").and.returnValue($q_.when({ data: { id: 1, name: "test", branding_users: [] } }));
            var name = {
                startsWith: function() {}
            };
            $state.current.name = name;
            $stateParams.binderId = 1;
            spyOn($state.current.name, "startsWith").and.returnValue(true);

            createController();
            spyOn(ctrl, "setBinders");
            spyOn(ctrl, "setBinder");

            expect(ctrl.showMyBinders).toEqual(true);
        });
    });

    describe("brandClick", function() {
        it("calls AfterLogin", function() {
            spyOn(AfterLogin, "goToDashboard");
            createController();
            ctrl.brandClick();

            expect(AfterLogin.goToDashboard).toHaveBeenCalled();
        });
    });

    describe("setBinders", function() {
        it("calls Binder binders and sets the binders", function() {
            spyOn(Binder, "all").and.returnValue($q_.when({ data: { items: [] } }));

            createController();
            ctrl.data = {
                user: {
                    id: 1
                }
            };
            ctrl.setBinders();

            expect(Binder.all).toHaveBeenCalled();
        });
    });

    describe("setBinder", function() {
        it("sets the current binder and branding", function() {
            var binder = { id: 1, name: "test", branding_users: [{ scope: "binder", user: { id: 1 } }] };
            createController();
            ctrl.setBinder(binder);

            expect(ctrl.data.binder).toEqual(binder);
            expect(ctrl.brandingUser).toEqual(binder.branding_users[0].user);
        });
    });

    describe("logout", function() {
        it("calls appDelegate logOff", function() {
            spyOn(appDelegate, "logOff");
            createController();
            ctrl.logout();

            expect(appDelegate.logOff).toHaveBeenCalled();
        });
    });

    describe("selectBinder", function() {
        it("calls $state reload", function() {
            spyOn($state, "reload");
            createController();
            ctrl.selectBinder({ id: 1, name: "Test" });

            expect($state.reload).toHaveBeenCalled();
        });
    });

    describe("goToSettings", function() {
        it("calls $state go for partner", function() {
            spyOn($state, "go");
            createController();
            ctrl.data.user = { role: "broker", partner_id: 1 };
            ctrl.goToSettings();

            expect($state.go).toHaveBeenCalledWith("partner.settings", {partnerId: 1});
        });

        it("does not call $state go for admin", function() {
            spyOn($state, "go");
            createController();
            ctrl.data.user = { role: "admin" };
            ctrl.goToSettings();

            expect($state.go).not.toHaveBeenCalled();
        });
        
        it("calls $state go for homeowner when $stateParams.binderId exists", function() {
            spyOn($state, "go");
            createController();
            ctrl.data.user = { role: "homeowner"};
            ctrl.$stateParams.binderId = 1;
            ctrl.goToSettings();

            expect($state.go).toHaveBeenCalledWith("binder.userSettings");
        });
        
        it("calls setBinders for homeowner when $stateParams.binderId does not exist", function() {
            spyOn(loading, "show");
            spyOn(Binder, "all").and.returnValue($q_.when({ data: { items: [{id: 1}] } }));
            
            createController();
            spyOn(ctrl, "setBinders");
            
            ctrl.data.user = { role: "homeowner"};
            ctrl.$stateParams.binderId = null;
            ctrl.goToSettings();

            expect(loading.show).toHaveBeenCalled();
            expect(ctrl.setBinders).toHaveBeenCalled();
        });

    });

    describe("showSettings", function() {
        it("returns true", function() {
            spyOn($state, "go");
            createController();
            ctrl.data.user = { role: "admin" };
            var value = ctrl.showSettings();

            expect(value).toEqual(false);
        });
        it("returns false", function() {
            spyOn($state, "go");
            createController();
            ctrl.data.user = { role: "agent" };
            var value = ctrl.showSettings();

            expect(value).toEqual(false);
        });
        it("returns false", function() {
            spyOn($state, "go");
            createController();
            ctrl.data.user = { role: "apr_reviewer" };
            var value = ctrl.showSettings();

            expect(value).toEqual(false);
        });
    });
    
    describe("getBindersSuccessSettings", function() {
        it("calls $state go for homeowner", function() {
            spyOn(loading, "close");
            spyOn($state, "go");
            
            createController();
            ctrl.getBindersSuccessSettings({ data: { items: [{id: 1}] } });

            expect($state.go).toHaveBeenCalledWith("binder.userSettings", {binderId: 1});
        });
    });

});

describe('applicationHeader', function() {
    var $scope, $compile, context, user, $stateParams, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        context = _$injector_.get("Context");
        user = _$injector_.get("User");
        $stateParams = _$injector_.get("$stateParams");

        $scope = $rootScope.$new(), $compile = _$compile_;

        $stateParams.binderId = 1;
        spyOn(user, "current").and.returnValue({ id: 1, role: "admin" });
        spyOn(context, "getPartner").and.returnValue({ data: { id: 1, warranties_configuration: {} } });

        element = $compile('<application-header></application-header>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the widget elements', function() {
            expect(element.html()).toContain("Log Out");
        });
    });
});