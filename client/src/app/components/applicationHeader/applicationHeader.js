(function() {
	"use strict";

	angular
		.module("hb.components.applicationHeader", [])
		.directive("applicationHeader", function() {
			return {
				restrict: "E",
				templateUrl: "components/applicationHeader/applicationHeader.tpl.html",
				controller: "ApplicationHeaderController",
				controllerAs: "ctrl",
				bindToController: true,
				scope: {}
			};
		})
		.controller("ApplicationHeaderController", ["User", "Binder", "AfterLogin", "$state", "Context", "$stateParams", "hb.userRole", "AppDelegate", "hb.resources", "$log", "Loading", ApplicationHeaderController]);

	function ApplicationHeaderController(User, Binder, AfterLogin, $state, Context, $stateParams, userRole, appDelegate, resources, $log, loading) {
		this.User = User;
		this.Binder = Binder;
		this.AfterLogin = AfterLogin;
		this.$state = $state;
		this.Context = Context;
		this.$stateParams = $stateParams;
		this.userRole = userRole;
		this.appDelegate = appDelegate;
		this.resources = resources.applicationHeader;
		this.data = {};
		this.needsToUpgrade = null;
		this.$log = $log;
		this.loading = loading;
		this.init();
	}

	ApplicationHeaderController.prototype = {

		init: function() {
			this.data.settings_tooltip = this.resources.your + " " + this.resources.settings;
			this.data.user = this.User.current();
			if (this.data.user) {
				switch (this.data.user.role) {
					case this.userRole.ADMIN:
					case this.userRole.PARTNER:
					case this.userRole.INSPECTOR:
					case this.userRole.BROKER:
						this.showMyBinders = false;
						if (this.$state.current.name && this.$state.current.name.startsWith("binder") && this.$stateParams.binderId && !this.$stateParams.partnerId) {
							this.Context.getBinder().then(angular.bind(this, this.setBinder), null);
						}
						break;
					default:
						if (this.$state.current.name && this.$state.current.name.startsWith("binder") && this.$stateParams.binderId) {
							this.showMyBinders = true;
							this.setBinders(this.getBindersSuccess);
							this.Context.getBinder().then(angular.bind(this, this.setBinder), null);
						}
						break;
				}
			}
		},

		brandClick: function() {
			this.AfterLogin.goToDashboard();
		},

		setBinders: function(callback) {
			this.Binder.all({ searchMethod: "for_user", userId: this.data.user.id }).then(
				angular.bind(this, callback),
				angular.bind(this, this.getBindersError));
		},

		getBindersSuccess: function(response) {
			this.data.binders = response.data.items;
		},

		getBindersError: function(response) {
			this.loading.close();
			this.$log.info(response.data);
		},

		setBinder: function(binder) {
			if (binder) {
				this.data.binder = binder;
				// get the brandings and check if we have a binder branding
				var brandings = this.data.binder.branding_users.filter(function(branding) {
					return branding.scope === "binder";
				});
				if (brandings.length > 0) {
					this.brandingUser = brandings[0].user;
				}

				if (binder.subscription && binder.subscription.plan === "free") {
					this.needsToUpgrade = { 'background-color': '#33CCFF' };
				}
			}
		},

		logout: function() {
			this.appDelegate.logOff();
		},

		selectBinder: function(binder) {
			/* reload the view when changing the binder so that everything loads correctly*/
			this.$stateParams.binderId = binder.id;
			this.$state.reload();
		},

		goToSettings: function() {
			switch (this.data.user.role) {
				case this.userRole.PARTNER:
				case this.userRole.INSPECTOR:
				case this.userRole.BROKER:
					this.$state.go("partner.settings", {
						partnerId: this.data.user.partner_id
					});
					break;
				case this.userRole.ADMIN:
					break;
				default:
					if (!this.$stateParams.binderId) {
						this.loading.show(this.resources.loading);
						this.setBinders(this.getBindersSuccessSettings);
					}
					else {
						this.$state.go("binder.userSettings");
					}
					break;
			}
		},

		showSettings: function() {
			switch (this.data.user.role) {
				case 'admin':
				case 'limited_admin':
				case 'agent':
				case 'apr_reviewer':
					return false;
				default:
					return true;
			}
		},
		
		getBindersSuccessSettings: function(response) {
			this.loading.close();
			this.data.binders = response.data.items;
			var binderId = this.data.binders[0].id;
			this.$state.go("binder.userSettings", {binderId: binderId});
		}
		
	};
})();