describe("PaymentModalController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        data,
        $modalInstance,
        notify,
        api,
        $q,
        PaymentModal,
        ModalService;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        notify = _$injector_.get("Notify");
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        PaymentModal = _$injector_.get("PaymentModal");
        ModalService = _$injector_.get("ModalService");

        data = {
            customer: {
                address: {}
            }
        };

        $modalInstance = {
            close: function() {},
            dismiss: function() {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("PaymentModalController", {
            "data": data,
            "$modalInstance": $modalInstance,
            "Notify": notify
        });

        $scope.ctrl = ctrl;
    }

    describe("init", function() {

        it("should set customer and account_id", function() {
            var customer = { id: 1, name: "Test", phone: "phone" };
            data.customer = customer;
            data.account_id = 100;
            data.cost = 49;
            data.plan = "49permonth";

            createController();
            $rootScope.$apply();

            expect(ctrl.customer).toEqual(customer);
            expect(ctrl.account_id).toEqual(100);
            expect(ctrl.cost).toEqual(49);
            expect(ctrl.plan).toEqual("49permonth");
        });
    });

    describe("ctrl.selectState", function() {

        it("should not set customer state and country", function() {
            var customer = { id: 1, name: "Test", phone: "phone", address: {} };
            data.customer = customer;

            createController();
            $rootScope.$apply();
            ctrl.selectState();
            $rootScope.$apply();

            expect(ctrl.customer.state).toBe(undefined);
            expect(ctrl.customer.country).toBe(undefined);
        });

        it("should set customer state and country", function() {
            var customer = { id: 1, name: "Test", phone: "phone", address: {} };
            data.customer = customer;

            createController();
            $rootScope.$apply();
            ctrl.selectState({ value: "MA", country: { value: "US" } });
            $rootScope.$apply();

            expect(ctrl.customer.address.state).toEqual("MA");
            expect(ctrl.customer.address.country).toEqual("US");
        });

    });

    describe("ctrl.submit", function() {

        it("should not call creditCardConfig tokenize function", function() {

            createController();
            $rootScope.$apply();
            ctrl.creditCardConfig.tokenize = function() {};
            spyOn(ctrl.creditCardConfig, "tokenize");
            ctrl.form = {
                $invalid: true
            };
            ctrl.submit();
            $rootScope.$apply();

            expect(ctrl.creditCardConfig.tokenize).not.toHaveBeenCalled();
        });

        it("should call creditCardConfig tokenize function", function() {

            createController();
            $rootScope.$apply();
            ctrl.creditCardConfig.tokenize = function() {};
            spyOn(ctrl.creditCardConfig, "tokenize");
            ctrl.form = {
                $invalid: false
            };
            ctrl.submit();
            $rootScope.$apply();

            expect(ctrl.creditCardConfig.tokenize).toHaveBeenCalled();
        });

    });

    describe("ctrl.onError", function() {

        it("sets processing to false and calls notify", function() {

            spyOn(notify, "info");

            createController();
            $rootScope.$apply();
            ctrl.onError("error");
            $rootScope.$apply();

            expect(ctrl.processing).toEqual(false);
            expect(notify.info).toHaveBeenCalledWith("error");
        });

    });

    describe("ctrl.process", function() {

        beforeEach(function() {
            var customer = { id: 1, name: "Test", phone: "phone", address: {} };
            data.customer = customer;
            data.account_id = 100;
            data.cost = 49;
            data.plan = "49permonth";
        });

        it("should call customer create", function() {

            spyOn(api.customer, "create").and.returnValue($q.when({ data: {} }));
            spyOn($modalInstance, "close");

            createController();
            $rootScope.$apply();
            ctrl.process("token");
            $rootScope.$apply();

            expect(api.customer.create).toHaveBeenCalled();
            expect($modalInstance.close).toHaveBeenCalledWith({ data: { address: {} } });
        });

        it("should call customer create", function() {

            data.customer.stripe_customer_id = 100;
            spyOn(api.customer, "update").and.returnValue($q.when({ data: {} }));
            spyOn($modalInstance, "close");

            createController();
            $rootScope.$apply();
            ctrl.process("token");
            $rootScope.$apply();

            expect(api.customer.update).toHaveBeenCalled();
            expect($modalInstance.close).toHaveBeenCalledWith({ data: { address: {} } });
        });

        it("should call customer create", function() {

            data.customer.stripe_customer_id = 100;
            spyOn(api.customer, "update").and.returnValue($q.reject({ data: "error" }));
            spyOn($modalInstance, "close");
            spyOn(notify, "error");

            createController();
            $rootScope.$apply();
            ctrl.process("token");
            $rootScope.$apply();

            expect(api.customer.update).toHaveBeenCalled();
            expect($modalInstance.close).not.toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
            expect(ctrl.processing).toEqual(false);
        });

    });

    describe("ctrl.cancel", function() {

        it("should call $modalInstance dismiss function", function() {

            spyOn($modalInstance, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.cancel();
            $rootScope.$apply();

            expect($modalInstance.dismiss).toHaveBeenCalledWith('cancel');
        });

    });

    describe('PaymentModal', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            PaymentModal.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});