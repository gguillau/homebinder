(function() {
    "use strict";

    angular
        .module("hb.components.payment", [])
        .factory("PaymentModal", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "components/payment/payment.tpl.html",
                            controller: "PaymentModalController as ctrl",
                            backdrop: "static",
                            resolveData: opts,
                            closed: opts.closed,
                            keyboard: false
                        });
                    }
                };
            }
        ])
        .controller("PaymentModalController", [
            "$log",
            "hb.api",
            "Session",
            "Notify",
            "$modalInstance",
            "data",
            "Loading",
            "Address",
            PaymentModalController
        ]);

    function PaymentModalController($log, api, session, notify, $modal, opts, loading, Address) {
        this.$log = $log;
        this.api = api;
        this.session = session;
        this.notify = notify;
        this.$modal = $modal;
        this.address = Address;
        this.states = this.address.getStatesAndProvinces();
        this.customer = opts.customer;
        this.address = opts.customer.address;
        this.account_id = opts.account_id;
        this.plan = opts.plan;
        this.cost = opts.cost;
        this.creditCardConfig = {
            onReceiveToken: angular.bind(this, this.process),
            onError: angular.bind(this, this.onError)
        };
    }

    PaymentModalController.prototype = {

        selectState: function(state) {
            if (!state) {
                return;
            }
            this.customer.address.state = state.value;
            this.customer.address.country = state.country.value;
        },

        submit: function() {
            // verify client information is valid
            this.form.$submitted = true;
            if (this.form.$invalid) {
                this.processing = false;
                return;
            }
            this.processing = true;
            // generate a stripe card token first
            this.creditCardConfig.tokenize();
        },

        onError: function(error) {
            this.processing = false;
            this.notify.info(error);
        },

        process: function(token) {
            var data = {
                customer: this.customer,
                payment: {
                    card: token
                }
            };
            if (this.customer.stripe_customer_id) {
                data.account_id = this.account_id;
                this.api.customer.update(this.customer.stripe_customer_id, data).then(
                    angular.bind(this, this.processSuccess),
                    angular.bind(this, this.processError));
            }
            else {
                data.account_id = this.account_id;
                data.cost = this.cost;
                data.plan = this.plan;
                this.api.customer.create(data).then(
                    angular.bind(this, this.processSuccess),
                    angular.bind(this, this.processError));
            }
        },

        processSuccess: function(response) {
            response.data.address = this.customer.address;
            this.$modal.close(response);
        },

        processError: function(response) {
            this.processing = false;
            this.notify.error(response.data);
        },

        cancel: function() {
            this.$modal.dismiss("cancel");
        }
    };
})();