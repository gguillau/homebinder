(function() {
    "use strict";

    angular.module('hb.components.marketingResources.index', [])
        .directive("hbMarketingResourcesIndex", function() {
            return {
                restrict: "E",
                templateUrl: "components/marketingResources/index/index.tpl.html"
            };
        })
        .factory("MarketingResourcesIndexController", [
            'hb.framework.indexBase',
            'hb.api',
            'hb.resources',
            'MarketingResourcesModal',
            'AfterLogin',
            'Session',
            "$filter",
            "$sce",
            "$window",
            function(IndexBase, api, resources, MarketingResourcesModal, AfterLogin, Session, $filter, $sce, $window) {
                var Model = function() {
                    // call the parent class
                    IndexBase.call(this);

                    // apply widget specific resource strings
                    this.resources = angular.extend({}, this.resources, resources.marketingResourcesIndex);
                    this.api = api;
                    this.$filter = $filter;
                    this.AfterLogin = AfterLogin;
                    this.$sce = $sce;
                    this.$window = $window;
                    this.currentUser = Session.getUser();
                    this.MarketingResourcesModal = MarketingResourcesModal;
                    // set the refresh API call
                    this.refreshCall = api.marketingResource.all;
                    // set the delete API call
                    this.deleteCall = api.marketingResource.destroy;
                    // set the name property used in the delete message
                    this.nameProperty = "name";
                    this.editState = "admin.partners_edit";
                    this.editParams = {};
                    this.editParamsProperty = "partnerId";
                    this.newState = "admin.partners_new";
                    this.title = this.resources.title;
                    this.resetItems();
                    this.configureSettings();
                };

                Model.prototype = Object.create(IndexBase.prototype);

                angular.extend(Model.prototype, {

                    configureSettings: function() {
                        this.settings = {
                            canEdit: this.currentUser.role === "admin",
                            canDelete: this.currentUser.role === "admin"
                        };
                    },

                    // refreshes the current view
                    refresh: function() {
                        this.refreshCall({}).then(
                            angular.bind(this, this.onRefreshSuccess),
                            angular.bind(this, this.onRefreshError)
                        );
                    },

                    // Refresh callback. Updates the item list and totals
                    onRefreshSuccess: function(response) {
                        this.items = response.data.items.map(angular.bind(this, function(item) {
                            if (item.resource_type === "document") {
                                item.document_file = this.$sce.trustAsResourceUrl(item.document_file);
                            }
                            else if (item.resource_type === "link") {
                                item.url = this.$sce.trustAsResourceUrl(item.url);
                            }
                            else if (item.resource_type === "image") {
                                item.image_file = this.$sce.trustAsResourceUrl(item.image_file);
                            }
                            return item;
                        }));
                        this.totalItems = this.toolbarCfg.total = response.data.total;

                        this.sortItems();
                    },

                    preDeleteItem: function(item) {
                        var index = this.items.indexOf(item);
                        this.deleteItem(item, index);
                    },

                    // updates the view after a delete
                    onDeleteItemSuccess: function(index, response) {
                        this.items.splice(index, 1);
                        this.totalItems = this.toolbarCfg.total -= 1;
                        this.notify.success(this.resources.deleteSuccess);
                        this.sortItems();
                        this.loading.close();
                    },

                    resetItems: function() {
                        this.documents = [];
                        this.images = [];
                        this.videos = [];
                        this.links = [];
                    },

                    sortItems: function() {
                        this.documents = this.$filter("filter")(this.items, {
                            resource_type: "document"
                        }, true);
                        this.links = this.$filter("filter")(this.items, {
                            resource_type: "link"
                        }, true);
                        this.videos = this.$filter("filter")(this.items, {
                            resource_type: "video"
                        }, true);
                        this.images = this.$filter("filter")(this.items, {
                            resource_type: "image"
                        }, true);
                    },

                    // item was added to the collection
                    onAdded: function(item) {
                        if (item.resource_type === "document") {
                            item.document_file = this.$sce.trustAsResourceUrl(item.document_file);
                        }
                        else if (item.resource_type === "link") {
                            item.url = this.$sce.trustAsResourceUrl(item.url);
                        }
                        else if (item.resource_type === "image") {
                            item.image_file = this.$sce.trustAsResourceUrl(item.image_file);
                        }
                        this.items.push(item);
                        this.totalItems = this.toolbarCfg.total += 1;
                        this.sortItems();
                    },

                    onEdited: function(index, item) {
                        if (item.resource_type === "document") {
                            item.document_file = this.$sce.trustAsResourceUrl(item.document_file);
                        }
                        this.items[index] = item;
                        this.resetItems();
                        this.sortItems();
                    },

                    newItem: function() {
                        var formOpts = {};
                        formOpts.onSaved = angular.bind(this, this.onAdded);
                        this.MarketingResourcesModal.showForm(formOpts);
                    },

                    editItem: function(item) {
                        var formOpts = {};
                        formOpts.marketingResource = item;
                        var index = this.items.indexOf(item);
                        formOpts.onSaved = angular.bind(this, this.onEdited, index);
                        this.MarketingResourcesModal.showForm(formOpts);
                    },

                    viewFile: function(item) {
                        if (item.resource_type === "document") {
                            this.$window.open(item.document_file);
                        }
                        else if (item.resource_type === "image") {
                            this.$window.open(item.image_file);
                        }
                        else {
                            this.$window.open(item.url);
                        }
                    },

                    canDownload: function(item) {
                        return item.resource_type === "document" || item.resource_type === "image";
                    },

                    getFileInformation: function(item) {
                        if (item.resource_type === "document") {
                            return {
                                url: item.document_file,
                                name: item.document_file_name
                            };
                        }
                        else if (item.resource_type === "image") {
                            return {
                                url: item.image_file,
                                name: item.image_file_name
                            };
                        }
                    },

                    downloadFile: function(item) {
                        var file = this.getFileInformation(item);
                        var a = document.createElement("a");
                        a.href = file.url;
                        a.target = "_blank";
                        a.download = file.name;
                        document.body.appendChild(a);
                        a.click();
                        document.body.removeChild(a);
                    },

                    canEdit: function() {
                        return this.currentUser.role === "admin";
                    }
                });

                return Model;
            }

        ]);
})();