describe("MarketingResourcesIndexController", function() {
    var MarketingResourcesIndexController,
        TestClass,
        responder;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
        // mock the notify service
        $provide.factory("Notify", function() {
            return {
                success: function() {},
                error: function() {},
                info: function() {}
            };
        });
        // mock the AfterLogin service
        $provide.factory("AfterLogin", function() {
            return {
                go: function() {}
            };
        });

        // mock the Session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return {
                        role: "admin"
                    };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });

        // mock the MarketingResourcesModal service
        $provide.factory("MarketingResourcesModal", function() {
            return {
                showForm: function() {}
            };
        });

    }));
    // get the base class under test
    beforeEach(inject(function($injector) {
        MarketingResourcesIndexController = $injector.get("MarketingResourcesIndexController");
    }));

    function createTestClass() {
        TestClass = function() {
            MarketingResourcesIndexController.call(this);

            this.resources = angular.extend({}, this.resources, {
                loading: "loading",
                loadError: "load error",
                confirmDelete: "confirm",
                deleteMsg1: "a ",
                deleteMsg2: " b",
                deleteSuccess: "deleted",
                deleteError: "delete error",
                saveSuccess: "SAVE",
                impersonateLoading: "LO"
            });
        };

        TestClass.prototype = Object.create(MarketingResourcesIndexController.prototype);

        return new TestClass();
    }

    responder = {
        refreshCall: function() {},
        deleteCall: function() {}
    };

    describe("configureSettings", function() {
        it("sets up the steps", inject(function(MarketingResourcesModal) {
            spyOn(MarketingResourcesModal, "showForm");

            var model = createTestClass();

            expect(model.settings.canDelete).toBe(true);
            expect(model.settings.canEdit).toBe(true);
        }));
    });

    describe("refresh", function() {
        it("gets the items", inject(function($q, $rootScope, Loading) {
            spyOn(responder, "refreshCall").and.returnValue($q.when({
                data: { items: [{ resource_type: "document", document_file: "" }, { resource_type: "link", url: "" }, { resource_type: "image", image_file: "" }], total: 3 }
            }));
            spyOn(Loading, "show");
            spyOn(Loading, "close");

            var model = createTestClass();
            model.refreshCall = responder.refreshCall;
            model.refresh();

            $rootScope.$apply();
            expect(model.items.length).toEqual(3);
            expect(model.toolbarCfg.total).toEqual(3);
            expect(responder.refreshCall).toHaveBeenCalled();
        }));
    });

    describe("newItem", function() {
        it("calls MarketingResourcesModal showForm", inject(function(MarketingResourcesModal) {
            spyOn(MarketingResourcesModal, "showForm");

            var model = createTestClass();
            model.newItem();

            expect(MarketingResourcesModal.showForm).toHaveBeenCalledWith({
                onSaved: jasmine.any(Function)
            });
        }));
    });

    describe("editItem", function() {
        it("calls MarketingResourcesModal showForm", inject(function(MarketingResourcesModal) {
            spyOn(MarketingResourcesModal, "showForm");

            var model = createTestClass();
            model.editItem({
                id: 1
            });

            expect(MarketingResourcesModal.showForm).toHaveBeenCalledWith({
                marketingResource: {
                    id: 1
                },
                onSaved: jasmine.any(Function)
            });
        }));
    });

    describe("preDeleteItem", function() {
        it("calls model deleteItem", inject(function() {
            var item = {
                id: 1,
                name: "item",
                description: "description"
            };
            var model = createTestClass();
            spyOn(model, "deleteItem");
            model.items = [item];
            model.preDeleteItem(item);

            expect(model.deleteItem).toHaveBeenCalledWith(item, 0);
        }));
    });

    describe("onDeleteItemSuccess", function() {
        it("calls model deleteItem", inject(function(Notify, Loading) {
            var item = {
                id: 1,
                name: "item",
                description: "description"
            };
            var items = [item];
            var model = createTestClass();

            spyOn(model, "sortItems");
            spyOn(items, "splice");
            spyOn(Notify, "success");
            spyOn(Loading, "close");

            model.items = items;
            model.totalItems = 1;
            model.toolbarCfg.total = 1;
            model.onDeleteItemSuccess(0, {});

            expect(model.sortItems).toHaveBeenCalled();
            expect(model.items.splice).toHaveBeenCalled();
            expect(Notify.success).toHaveBeenCalled();
            expect(Loading.close).toHaveBeenCalled();
            expect(model.totalItems).toEqual(0);
        }));
    });

    describe("resetItems", function() {
        it("sets all items to empty arrays", inject(function() {
            var item = {
                id: 1,
                name: "item",
                description: "description"
            };
            var items = [item];
            var model = createTestClass();

            model.documents = items;
            model.videos = items;
            model.links = items;
            model.images = items;

            expect(model.documents.length).toEqual(1);
            expect(model.videos.length).toEqual(1);
            expect(model.links.length).toEqual(1);
            expect(model.images.length).toEqual(1);

            model.resetItems();

            expect(model.documents.length).toEqual(0);
            expect(model.videos.length).toEqual(0);
            expect(model.links.length).toEqual(0);
            expect(model.images.length).toEqual(0);
        }));
    });

    describe("sortItems", function() {
        it("places items into separate arrays", inject(function() {
            var document = {
                id: 1,
                name: "item",
                description: "description",
                resource_type: "document"
            };
            var image = {
                id: 1,
                name: "item",
                description: "description",
                resource_type: "image"
            };
            var link = {
                id: 1,
                name: "item",
                description: "description",
                resource_type: "link"
            };
            var video = {
                id: 1,
                name: "item",
                description: "description",
                resource_type: "video"
            };

            var items = [document, image, link, video];
            var model = createTestClass();

            model.items = items;

            expect(model.documents.length).toEqual(0);
            expect(model.videos.length).toEqual(0);
            expect(model.links.length).toEqual(0);
            expect(model.images.length).toEqual(0);

            model.sortItems();

            expect(model.documents.length).toEqual(1);
            expect(model.videos.length).toEqual(1);
            expect(model.links.length).toEqual(1);
            expect(model.images.length).toEqual(1);
        }));
    });

    describe("onAdded", function() {
        it("adds the item to the items array", inject(function() {
            var document = {
                id: 1,
                name: "item",
                description: "description",
                resource_type: "document"
            };
            var model = createTestClass();
            spyOn(model, "sortItems");
            model.onAdded(document);

            expect(model.items.length).toEqual(1);
            expect(model.sortItems).toHaveBeenCalled();
            expect(model.totalItems).toEqual(1);
            expect(model.toolbarCfg.total).toEqual(1);
        }));

        it("adds the item to the items array", inject(function() {
            var document = {
                id: 1,
                name: "item",
                description: "description",
                resource_type: "link",
                url: ""
            };
            var model = createTestClass();
            spyOn(model, "sortItems");
            model.onAdded(document);

            expect(model.items.length).toEqual(1);
            expect(model.sortItems).toHaveBeenCalled();
            expect(model.totalItems).toEqual(1);
            expect(model.toolbarCfg.total).toEqual(1);
        }));

        it("adds the item to the items array", inject(function() {
            var document = {
                id: 1,
                name: "item",
                description: "description",
                resource_type: "image",
                image_file: ""
            };
            var model = createTestClass();
            spyOn(model, "sortItems");
            model.onAdded(document);

            expect(model.items.length).toEqual(1);
            expect(model.sortItems).toHaveBeenCalled();
            expect(model.totalItems).toEqual(1);
            expect(model.toolbarCfg.total).toEqual(1);
        }));
    });

    describe("onEdited", function() {
        it("adds the item to the items array", inject(function() {
            var document = {
                id: 1,
                name: "item",
                description: "description",
                resource_type: "document"
            };
            var model = createTestClass();

            spyOn(model, "sortItems");
            spyOn(model, "resetItems");

            model.items = [document];
            model.onEdited(0, document);

            expect(model.items.length).toEqual(1);
            expect(model.resetItems).toHaveBeenCalled();
            expect(model.sortItems).toHaveBeenCalled();
        }));
    });

    describe("viewFile", function() {
        it("opens the url link", inject(function($window) {
            var link = {
                id: 1,
                name: "item",
                description: "description",
                resource_type: "link",
                url: "link"
            };
            var model = createTestClass();

            spyOn($window, "open");

            model.viewFile(link);

            expect($window.open).toHaveBeenCalledWith("link");
        }));
        it("opens the document link", inject(function($window) {
            var document = {
                id: 1,
                name: "item",
                description: "description",
                resource_type: "document",
                document_file: "document_link"
            };
            var model = createTestClass();

            spyOn($window, "open");

            model.viewFile(document);

            expect($window.open).toHaveBeenCalledWith("document_link");
        }));
        it("opens the image link", inject(function($window) {
            var image = {
                id: 1,
                name: "item",
                description: "description",
                resource_type: "image",
                image_file: "image_link"
            };
            var model = createTestClass();

            spyOn($window, "open");

            model.viewFile(image);

            expect($window.open).toHaveBeenCalledWith("image_link");
        }));
    });

    describe("canDownload", function() {
        it("returns false", inject(function() {
            var link = {
                id: 1,
                name: "item",
                description: "description",
                resource_type: "link",
                url: "link"
            };
            var model = createTestClass();
            var boolean_value = model.canDownload(link);

            expect(boolean_value).toBe(false);
        }));
        it("returns true", inject(function() {
            var document = {
                id: 1,
                name: "item",
                description: "description",
                resource_type: "document",
                document_file: "document_link"
            };
            var model = createTestClass();
            var boolean_value = model.canDownload(document);

            expect(boolean_value).toBe(true);
        }));
        it("returns true", inject(function() {
            var image = {
                id: 1,
                name: "item",
                description: "description",
                resource_type: "image",
                image_file: "image_link"
            };
            var model = createTestClass();
            var boolean_value = model.canDownload(image);

            expect(boolean_value).toBe(true);
        }));
    });

    describe("getFileInformation", function() {

        it("gets the document file information", inject(function() {
            var document = {
                id: 1,
                name: "item",
                description: "description",
                resource_type: "document",
                document_file: "document_file",
                document_file_name: "document_file_name"
            };
            var model = createTestClass();

            var information = model.getFileInformation(document);

            expect(information.url).toEqual("document_file");
            expect(information.name).toEqual("document_file_name");

        }));
        it("gets the image file information", inject(function() {
            var image = {
                id: 1,
                name: "item",
                description: "description",
                resource_type: "image",
                image_file: "image_file",
                image_file_name: "image_file_name"
            };
            var model = createTestClass();

            var information = model.getFileInformation(image);

            expect(information.url).toEqual("image_file");
            expect(information.name).toEqual("image_file_name");

        }));
    });

    describe("downloadFile", function() {
        it("calls getFileInformation", inject(function() {
            var model = createTestClass();
            spyOn(model, "getFileInformation").and.callThrough();

            var file = { resource_type: "document", document_file_name: "" };
            model.downloadFile(file);

            expect(model.getFileInformation).toHaveBeenCalled();
        }));
    });

    describe("canEdit", function() {
        it("returns false", inject(function() {
            var model = createTestClass();
            model.currentUser = { role: "homeowner" };
            var boolean_value = model.canEdit();

            expect(boolean_value).toBe(false);
        }));
        it("returns true", inject(function() {
            var model = createTestClass();
            model.currentUser = { role: "admin" };
            var boolean_value = model.canEdit();

            expect(boolean_value).toBe(true);
        }));
    });

});

describe('hbMarketingResourcesIndex', function() {
    var $scope, $compile, api, $q, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.marketingResource, "all").and.returnValue($q.when({ data: { items: [] } }));

        element = $compile('<hb-marketing-resources-index></hb-marketing-resources-index>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the hb-marketing-resources-index directive', function() {
        it('calls api marketingResource all', function() {
            expect(element.html()).toContain("Your Marketing Materials");
        });
    });
});