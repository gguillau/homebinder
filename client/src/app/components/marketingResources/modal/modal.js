angular
    .module("hb.components.marketingResources.modal", [])
    .factory("MarketingResourcesModal", [
        "hb.framework.toggleModalBase",
        "hb.resources",
        function(ModalBase, resources) {
            var instance = null;
            var Modal = function() {
                ModalBase.call(this);
                this.templateUrl = "components/marketingResources/modal/modal.tpl.html";
                this.controller = "MarketingResourcesModalController as ctrl";
                this.windowClass = "widget-modal";
            };

            Modal.prototype = Object.create(ModalBase.prototype);

            if (!instance) {
                instance = new Modal();
            }

            return instance;
        }
    ])
    .controller("MarketingResourcesModalController", [
        "hb.framework.toggleModalControllerBase",
        "$modalInstance",
        "args",
        "hb.api",
        "hb.resources",
        "Notify",
        "$log",
        function(ToggleModalControllerBase, $modalInstance, args, api, resources, notify, $log) {
            var Model = function() {
                ToggleModalControllerBase.call(this);
                this.args = args;
                this.formCfg.organizationId = args.organizationId;
                this.formCfg.partnerId = args.partnerId;
                this.populateCall = api.marketingResource.get;
                this.$modalInstance = $modalInstance;
                this.objectProperty = "marketingResource";
                this.objectIdProperty = "marketingResourceId";
                this.nameProperty = "name";
                this.resources = resources.marketingResourcesForm;
                this.init();
            };

            Model.prototype = Object.create(ToggleModalControllerBase.prototype);

            this.model = new Model();
        }
    ]);