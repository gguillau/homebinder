describe("MarketingResourcesModalController", function() {
    var controller,
        base,
        api,
        resources;

    // load the homebinder module
    beforeEach(module("homebinder"));
    
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("hb.framework.toggleModalControllerBase");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        
        spyOn(api.marketingResource, "get").and.returnValue($q.when({
            data: {
                id: 1,
                name: "name"
            }
        }));

        controller = $controller("MarketingResourcesModalController", {
            "hb.framework.indexBase": base,
            "hb.api": api,
            "hb.resources": resources,
            "$modalInstance": {},
            "args": {}
        });

        $rootScope.$apply();
    }));

    describe("init", function() {
        it("sets the controller fields", function() {
            expect(controller.model.objectProperty).toEqual("marketingResource");
        });
    });

});