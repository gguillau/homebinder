(function() {
    "use strict";

    angular.module("hb.components.marketingResources", [
        "hb.components.marketingResources.index",
        "hb.components.marketingResources.form",
        "hb.components.marketingResources.modal"
    ]);
})();
