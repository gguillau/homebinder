(function() {
    "use strict";

    angular
        .module("hb.components.marketingResources.form", [])
        .directive("hbMarketingResourcesForm", function() {
            return {
                restrict: "E",
                templateUrl: "components/marketingResources/form/form.tpl.html",
                controller: "MarketingResourcesFormController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    cfg: "=",
                    onSaved: "&",
                    onCancel: "&"
                }
            };
        })
        // Model backing the area modal form
        .controller("MarketingResourcesFormController", [
            "$log",
            "hb.api",
            "Notify",
            "Loading",
            "hb.resources",
            Controller
        ]);

    function Controller($log, api, notify, loading, resources) {
        var that = this;
        var Model = function() {
            this.$log = $log;
            this.api = api;
            this.notify = notify;
            this.resources = resources.marketingResourcesForm;
            this.loading = loading;
            this.name = null;
            this.description = null;
            this.resourceTypes = [{
                name: "Image",
                value: "image"
            }, {
                name: "Document",
                value: "document"
            }, {
                name: "Link",
                value: "link"
            }];
            this.userTypes = [{
                name: "Homeowner",
                value: "homeowner"
            }, {
                name: "Inspector",
                value: "inspector"
            }, {
                name: "Broker",
                value: "broker"
            }];
            this.imageUploader = {
                fileTypes: "image",
                hideUploadButton: true,
                showTable: false,
                selectButtonLabel: this.resources.imageText,
                fileLimit: true,
                multiSelect: false,
                hide_upload: false
            };
            this.documentUploader = {
                fileTypes: "document",
                hideUploadButton: true,
                showTable: false,
                selectButtonLabel: this.resources.documentText,
                fileLimit: true,
                multiSelect: false,
                hide_upload: false
            };
            this.url_regex = /^((?:http|ftp)s?:\/\/)(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|localhost|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(?::\d+)?(?:\/?|[\/?]\S+)$/i;
            this.populate();
        };

        angular.extend(Model.prototype, {
            populate: function() {
                if (!that.cfg) {
                    return;
                }

                // setup the save callback
                that.cfg.api = {
                    save: angular.bind(this, this.submit)
                };

                this.partnerId = that.cfg.partnerId;
                this.organizationId = that.cfg.organizationId;

                // load the resource
                if (that.cfg.item) {
                    this.resource = that.cfg.item;
                    this.title = this.resource.name;
                    this.name = this.resource.name;
                    this.url = this.resource.url;
                    this.image_file = this.resource.image_file;
                    this.document_file = this.resource.document_file;
                    this.resourceType = this.resource.resource_type;
                    this.userType = this.resource.user_type;
                    this.description = this.resource.description;
                }
            },

            submit: function() {
                that.form.$submitted = true;

                if (that.form.$invalid) {
                    return;
                }

                var data = {
                        marketing_resource: {
                            name: this.name,
                            description: this.description,
                            resource_type: this.resourceType,
                            user_type: this.userType,
                            url: this.url
                        },
                        partner_id: this.partnerId,
                        organization_id: this.organizationId
                    },
                    promise;

                if (this.image) {
                    data.marketing_resource.image = this.image;
                    data.marketing_resource.image_file_name = this.image_file_name;
                }

                if (this.document) {
                    data.marketing_resource.document = this.document;
                    data.marketing_resource.document_file_name = this.document_file_name;
                }

                if (this.video) {
                    data.marketing_resource.video = this.video;
                    data.marketing_resource.video_file_name = this.video_file_name;
                }

                this.error422 = false;
                this.loading.show(this.resources.saving);

                if (this.resource) {
                    promise = api.marketingResource.update(this.resource.id, data);
                }
                else {
                    promise = api.marketingResource.create(data);
                }

                promise.then(angular.bind(this, this.onResourceSaved),
                        angular.bind(this, this.onResourceSaveError))
                    /*jshint -W024*/
                    .finally(angular.bind(this, this.onDoneSaving));
            },

            onResourceSaved: function(response) {
                this.resource = response.data;
                that.cfg.resource = this.resource;
                that.onSaved({
                    item: this.resource
                });
            },

            onResourceSaveError: function(response) {
                this.notify.error(response.data);
            },

            onDoneSaving: function() {
                this.loading.close();
            },

            changeImage: function() {
                var pending = this.imageUploader.api.pendingUploads();
                this.image_file = pending[0].window_url;
                this.image = pending[0].based64;
                this.image_file_name = pending[0].name;
            },

            changeDocument: function() {
                var pending = this.documentUploader.api.pendingUploads();
                this.document_file = pending[0].window_url;
                this.document = pending[0].based64;
                this.document_file_name = pending[0].name;
            }
        });

        this.model = new Model();
    }
})();