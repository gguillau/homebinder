describe("MarketingResourcesFormController", function() {
    var ctrl,
        controller,
        $q,
        $log,
        api,
        notify,
        resources,
        loading,
        uploaderApi;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
    }));

    function createController() {
        ctrl = controller("MarketingResourcesFormController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "Loading": loading,
            "hb.resources": resources
        });
    }

    uploaderApi = {
        pendingUploads: function() {}
    };

    describe("populate", function() {
        it("returns when no cfg is provided", function() {
            createController();
            ctrl.cfg = null;
            ctrl.model.populate();
        });

        it("populates resource", function() {
            createController();
            ctrl.cfg = {
                item: {
                    name: "resource",
                    description: "resource description",
                    url: "url",
                    image_file: "image_file",
                    document_file: "document_file",
                    resource_type: "link",
                    user_type: "homeowner"
                }
            };

            ctrl.model.populate();

            expect(ctrl.model.resource).not.toBeNull();
            expect(ctrl.model.name).toEqual("resource");
            expect(ctrl.model.title).toEqual("resource");
            expect(ctrl.model.url).toEqual("url");
            expect(ctrl.model.image_file).toEqual("image_file");
            expect(ctrl.model.document_file).toEqual("document_file");
            expect(ctrl.model.resourceType).toEqual("link");
            expect(ctrl.model.userType).toEqual("homeowner");
            expect(ctrl.model.description).toEqual("resource description");
            expect(ctrl.cfg.api.save).not.toBeNull();
        });

        it("does not populate when no resource is provided", function() {
            createController();
            ctrl.cfg = {};
            ctrl.model.populate();

            expect(ctrl.model.resource).toBeUndefined();
            expect(ctrl.cfg.api.save).not.toBeNull();
        });
    });

    describe("submit", function() {
        it("does not submit invalid data", function() {
            spyOn(api.marketingResource, "create").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = {};
            ctrl.form = {
                $invalid: true
            };
            ctrl.model.submit();
            expect(ctrl.form.$submitted).toEqual(true);
            expect(api.marketingResource.create).not.toHaveBeenCalled();
        });

        it("creates a new resource", function() {
            spyOn(api.marketingResource, "create").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = {};
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.name = "resource";
            ctrl.model.description = "resource description";
            ctrl.model.image = {};
            ctrl.model.document = {};
            ctrl.model.video = {};
            ctrl.model.submit();
            expect(api.marketingResource.create).toHaveBeenCalled();
        });

        it("updates an existing resource", function() {
            spyOn(api.marketingResource, "update").and.returnValue($q.when({}));
            createController();
            ctrl.cfg = {};
            ctrl.model.resource = {
                id: 1
            };
            ctrl.form = {
                $invalid: false
            };
            ctrl.model.name = "resource";
            ctrl.model.description = "resource description";
            ctrl.model.submit();
            expect(api.marketingResource.update).toHaveBeenCalledWith(1, {
                marketing_resource: {
                    name: 'resource',
                    description: 'resource description',
                    resource_type: undefined,
                    user_type: undefined,
                    url: undefined
                },
                partner_id: undefined,
                organization_id: undefined
            });
        });
    });

    describe("onResourceSaved", function() {
        it("saves the response and calls onSaved", function() {
            var resource = {
                id: 1,
                name: "resource",
                description: "description"
            };
            createController();
            ctrl.cfg = {};
            ctrl.onSaved = function() {};
            spyOn(ctrl, "onSaved");
            ctrl.model.onResourceSaved({
                data: resource
            });
            expect(ctrl.model.resource).toEqual(resource);
            expect(ctrl.cfg.resource).toEqual(resource);
            expect(ctrl.onSaved).toHaveBeenCalledWith(jasmine.objectContaining({
                item: resource
            }));
        });
    });

    describe("onResourceSaveError", function() {
        it("handles a general error", function() {
            spyOn(notify, "error");
            createController();
            ctrl.cfg = {};
            ctrl.model.onResourceSaveError({
                status: 500,
                data: "error"
            });
            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("onDoneSaving", function() {
        it("calls loading close", function() {
            spyOn(loading, "close");
            createController();
            ctrl.cfg = {};
            ctrl.model.onDoneSaving();
            expect(loading.close).toHaveBeenCalled();
        });
    });

    describe("changeImage", function() {
        it("sets the resource image", function() {
            spyOn(loading, "close");
            var pending = {
                window_url: "url",
                based64: "64",
                name: "name"
            };
            spyOn(uploaderApi, "pendingUploads").and.returnValue([pending]);
            createController();
            ctrl.model.imageUploader.api = uploaderApi;
            ctrl.cfg = {};
            ctrl.model.changeImage();

            expect(ctrl.model.image).toBe("64");
            expect(ctrl.model.image_file).toBe("url");
            expect(ctrl.model.image_file_name).toBe("name");
        });
    });

    describe("changeDocument", function() {
        it("sets the resource document", function() {
            spyOn(loading, "close");
            var pending = {
                window_url: "url",
                based64: "64",
                name: "name"
            };
            spyOn(uploaderApi, "pendingUploads").and.returnValue([pending]);
            createController();
            ctrl.model.documentUploader.api = uploaderApi;
            ctrl.cfg = {};
            ctrl.model.changeDocument();

            expect(ctrl.model.document).toBe("64");
            expect(ctrl.model.document_file).toBe("url");
            expect(ctrl.model.document_file_name).toBe("name");
        });
    });
});

describe('hbMarketingResourcesForm', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = {};
        $scope.onSaved = function() {};
        $scope.onCancel = function() {};

        element = $compile('<hb-marketing-resources-form cfg="cfg" onSaved="onSaved" onCancel="onCancel"></hb-marketing-resources-form>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("How to market HB to your clients");
        });
    });
});