describe("HBDocumentUploaderController", function() {
    var $rootScope,
        $q,
        $scope,
        $controller,
        ctrl;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("HBDocumentUploaderController", {});

        $scope.ctrl = ctrl;

    }

    describe('ctrl.documentAdded', function() {
        it("calls docAdded", function() {
            createController();

            ctrl.docAdded = function() {};
            ctrl.docUploader = {
                api: {
                    pendingUploads: function() {}
                }
            };
            spyOn(ctrl, "docAdded");
            spyOn(ctrl.docUploader.api, "pendingUploads").and.returnValue([{ id: 1 }]);
            ctrl.documentAdded();

            expect(ctrl.documents.length).toEqual(1);
            expect(ctrl.docAdded).toHaveBeenCalled();
        });
    });

    describe('ctrl.remove', function() {
        it("removes the document", function() {
            createController();

            ctrl.docUploader = {
                api: {
                    pendingUploads: function() {},
                    remove: function() {}
                }
            };

            spyOn(ctrl.docUploader.api, "remove");
            var document = { id: 1 };
            ctrl.documents = [document];
            ctrl.remove(0, document);

            expect(ctrl.documents.length).toEqual(0);
            expect(ctrl.docUploader.api.remove).toHaveBeenCalled();
        });
    });

});