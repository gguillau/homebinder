angular
    .module("hb.components.documentUploader", [])
    .directive("hbDocumentUploader", function() {
        return {
            restrict: "E",
            templateUrl: "components/documentUploader/documentUploader.tpl.html",
            controller: "HBDocumentUploaderController as ctrl",
            bindToController: true,
            scope: {
                docAdded: "&",
                docUploader: "=",
                docUploadSuccess: "&",
                docUploadError: "&"
            }
        };
    })
    .controller("HBDocumentUploaderController", [
        HBDocumentUploaderController
    ]);

function HBDocumentUploaderController() {
    this.documents = [];

}

HBDocumentUploaderController.prototype = {

    documentAdded: function() {
        this.documents = this.docUploader.api.pendingUploads().map(angular.bind(this, function(doc) {
            var document = {
                id: doc.id,
                name: doc.name,
                location: doc.location
            };
            return document;
        }));
        this.docAdded();

    },

    remove: function(index, file) {
        this.docUploader.api.remove(file);
        this.documents.splice(index, 1);
    }
};