describe("TaggerController", function() {
    var $rootScope,
        api,
        $log,
        $q,
        resources,
        ctrl,
        context;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        $q = _$injector_.get("$q");
        resources = _$injector_.get("hb.resources");
        context = _$injector_.get("Context");

        $log = {
            error: function() {},
            warn: function() {}
        };
        api = {
            appliance: { all: function() {} },
            area: { all: function() {} },
            binderContractor: { all: function() {} },
            document: { all: function() {} },
            finish: { all: function() {} },
            image: { all: function() {} },
            inventoryItem: { all: function() {} },
            maintenanceItem: { all: function() {} },
            paint: { all: function() {} },
            project: { all: function() {} },
            receipt: { all: function() {} },
            structure: { all: function() {} }
        };

        spyOn(context, "getBinder").and.returnValue($q.when({ id: 1, permissions: { can_read: true } }));
        spyOn(api.appliance, "all").and.returnValue($q.when({ data: { items: [{ id: 1, name: "one" }] } }));
        spyOn(api.area, "all").and.returnValue($q.when({ data: { items: [{ id: 1, name: "one" }] } }));
        spyOn(api.binderContractor, "all").and.returnValue($q.when({ data: { items: [{ id: 1, contractor: { id: 1, name: "one" } }] } }));
        spyOn(api.document, "all").and.returnValue($q.when({ data: { items: [{ id: 1, name: "one" }] } }));
        spyOn(api.finish, "all").and.returnValue($q.when({ data: { items: [{ id: 1, name: "one" }] } }));
        spyOn(api.image, "all").and.returnValue($q.when({ data: { items: [{ id: 1, name: "one" }] } }));
        spyOn(api.inventoryItem, "all").and.returnValue($q.when({ data: { items: [{ id: 1, name: "one" }] } }));
        spyOn(api.maintenanceItem, "all").and.returnValue($q.when({ data: { items: [{ id: 1, name: "one" }] } }));
        spyOn(api.paint, "all").and.returnValue($q.when({ data: { items: [{ id: 1, name: "one" }] } }));
        spyOn(api.project, "all").and.returnValue($q.when({ data: { items: [{ id: 1, name: "one" }] } }));
        spyOn(api.receipt, "all").and.returnValue($q.when({ data: { items: [{ id: 1, name: "one" }] } }));
        spyOn(api.structure, "all").and.returnValue($q.when({ data: { items: [{ id: 1, name: "one" }] } }));
    }));

    function createController() {
        ctrl = $controller("TaggerController", {
            "$q": $q,
            "$log": $log,
            "hb.api": api,
            "hb.resources": resources,
            "Context": context
        });
        $rootScope.$apply();
    }

    describe("init", function() {
        it("loads a custom config", function() {
            createController();

            ctrl.cfg = {
                binderId: 99,
                multiple: false,
                applianceTags: false
            };
            ctrl.init();
            expect(ctrl.config.binderId).toEqual(99);
            expect(ctrl.config.multiple).toEqual(false);
            expect(ctrl.config.applianceTags).toEqual(false);
            expect(ctrl.config.areaTags).toEqual(true);
        });
    });

    describe("refresh", function() {
        it("refreshes all tags", function() {
            createController();
            ctrl.tags = [{ id: 1, tag: "appliance_1" }];
            ctrl.items = [];
            ctrl.refresh();
            $rootScope.$apply();

            expect(ctrl.items.length).toEqual(12);
            expect(ctrl.items[0].obj).not.toBeNull();
            expect(ctrl.items[0].obj.name).toEqual("one");
            expect(ctrl.items[0].tag).toEqual("appliance_1");
            expect(ctrl.items[0].category).toEqual("Appliances");
            expect(ctrl.selected.length).toEqual(1);
            expect(ctrl.selected[0].tag).toEqual("appliance_1");
        });

        it("refreshes no tags", function() {
            createController();

            ctrl.cfg = {
                applianceTags: false,
                areaTags: false,
                contractorTags: false,
                documentTags: false,
                finishTags: false,
                imageTags: false,
                inventoryTags: false,
                maintenanceTags: false,
                paintTags: false,
                projectTags: false,
                receiptTags: false,
                structureTags: false
            };
            ctrl.items = [];
            ctrl.init();
            ctrl.refresh();

            expect(ctrl.items.length).toEqual(0);
        });
    });

    describe("onLoadError", function() {
        it("calls $log error", function() {
            createController();
            spyOn($log, "error");

            ctrl.onLoadError({ data: {} });
            expect($log.error).toHaveBeenCalled();
        });
    });

    describe("getGroup", function() {
        it("returns the item's category", function() {
            createController();
            var category = ctrl.getGroup({ category: "appliances" });
            expect(category).toEqual("appliances");
        });
    });

    describe("onSelect", function() {
        it("adds the item tag to the list of tags", function() {
            createController();
            ctrl.tags = [];
            expect(ctrl.tags.length).toEqual(0);
            ctrl.onSelect({ search: "test" }, { tag: "test" }, {});
            expect(ctrl.tags.length).toEqual(1);
        });
    });

    describe("onRemove", function() {
        it("adds the item tag to the list of tags and then removes it", function() {
            createController();
            ctrl.tags = [];
            expect(ctrl.tags.length).toEqual(0);
            var tag = { tag: "test" };
            ctrl.onSelect({ search: "test" }, tag, {});
            expect(ctrl.tags.length).toEqual(1);

            ctrl.onRemove(tag, {});
            expect(ctrl.tags.length).toEqual(0);
        });
    });
});

describe('hbTagger', function() {
    var $scope, $compile, context;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        context = _$injector_.get("Context");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(context, "getBinder").and.returnValue({ data: { permissions: { can_read: false } } });

        $scope.cfg = {};
        $scope.tags = [];

        $compile('<hb-tagger tags="tags" cfg="cfg"></hb-tagger>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the hb-tagger directive', function() {
        it('calls context getBinder', function() {
            expect(context.getBinder).toHaveBeenCalled();
        });
    });
});