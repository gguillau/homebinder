(function() {
    "use strict";

    angular
        .module("hb.components.tagger", [])
        .directive("hbTagger", function() {
            return {
                restrict: "E",
                templateUrl: "components/tagger/tagger.tpl.html",
                controller: "TaggerController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    cfg: "=",
                    tags: "="
                }
            };
        })
        .controller("TaggerController", [
            "$q",
            "$log",
            "hb.api",
            "hb.resources",
            "Context",
            Controller
        ]);

    function Controller($q, $log, api, resources, context) {
        this.$q = $q;
        this.$log = $log;
        this.api = api;
        this.resources = resources.tagger;
        this.items = [];
        this.selected = [];
        this.defaultCfg = {
            binderId: null,
            multiple: true,
            applianceTags: true,
            areaTags: true,
            contractorTags: true,
            documentTags: true,
            finishTags: true,
            imageTags: true,
            inventoryTags: true,
            maintenanceTags: true,
            paintTags: true,
            projectTags: true,
            receiptTags: true,
            structureTags: true
        };
        this.config = {};
        context.getBinder().then(angular.bind(this, function(binder) {
            this.binder = binder;
            this.init();
            this.refresh();
        }));
    }

    Controller.prototype = {
        init: function() {
            // merge the custom configuration into the default
            angular.extend(this.config, this.defaultCfg, this.cfg);
        },

        refresh: function() {
            if (!this.binder.permissions || !this.binder.permissions.can_read) {
                return;
            }
            var promises = [],
                getters = [];

            // clear the current list
            this.items = [];
            
            // appliances
            if (this.config.applianceTags) {
                getters.push({
                    get: this.api.appliance.all,
                    args: { binderId: this.binder.id, count: 100 },
                    definition: {
                        cls: "appliance",
                        category: this.resources.appliances
                    }
                });
            }

            // areas
            if (this.config.areaTags) {
                getters.push({
                    get: this.api.area.all,
                    args: { binderId: this.binder.id, count: 100 },
                    definition: {
                        cls: "area",
                        category: this.resources.areas
                    }
                });
            }

            // contractors
            if (this.config.contractorTags) {
                getters.push({
                    get: this.api.binderContractor.all,
                    args: { binderId: this.binder.id, count: 100 },
                    definition: {
                        cls: "binderContractor",
                        category: this.resources.contractors
                    }
                });
            }

            //documents
            if (this.config.documentTags) {
                getters.push({
                    get: this.api.document.all,
                    args: { binderId: this.binder.id, count: 100 },
                    definition: {
                        cls: "document",
                        category: this.resources.documents
                    }
                });
            }

            // finishes
            if (this.config.finishTags) {
                getters.push({
                    get: this.api.finish.all,
                    args: { binderId: this.binder.id, count: 100 },
                    definition: {
                        cls: "finish",
                        category: this.resources.finishes
                    }
                });
            }

            // images
            if (this.config.imageTags) {
                getters.push({
                    get: this.api.image.all,
                    args: { binderId: this.binder.id, count: 100 },
                    definition: {
                        cls: "image",
                        category: this.resources.images
                    }
                });
            }

            // inventory
            if (this.config.inventoryTags) {
                getters.push({
                    get: this.api.inventoryItem.all,
                    args: { binderId: this.binder.id, count: 100 },
                    definition: {
                        cls: "inventoryItem",
                        category: this.resources.inventory
                    }
                });
            }

            // maintenance
            if (this.config.maintenanceTags) {
                getters.push({
                    get: this.api.maintenanceItem.all,
                    args: { binderId: this.binder.id, count: 100 },
                    definition: {
                        cls: "maintenanceItem",
                        category: this.resources.maintenance
                    }
                });
            }

            // paints
            if (this.config.paintTags) {
                getters.push({
                    get: this.api.paint.all,
                    args: { binderId: this.binder.id, count: 100 },
                    definition: {
                        cls: "paint",
                        category: this.resources.paints
                    }
                });
            }

            // projects
            if (this.config.projectTags) {
                getters.push({
                    get: this.api.project.all,
                    args: { binderId: this.binder.id, count: 100 },
                    definition: {
                        cls: "project",
                        category: this.resources.projects
                    }
                });
            }

            // receipts
            if (this.config.receiptTags) {
                getters.push({
                    get: this.api.receipt.all,
                    args: { binderId: this.binder.id, count: 100 },
                    definition: {
                        cls: "receipt",
                        category: this.resources.receipts
                    }
                });
            }

            // structures
            if (this.config.structureTags) {
                getters.push({
                    get: this.api.structure.all,
                    args: { binderId: this.binder.id, count: 100 },
                    definition: {
                        cls: "structure",
                        category: this.resources.structures
                    }
                });
            }

            // make all the requests
            getters.forEach(function(getter) {
                promises.push(getter.get(getter.args).then(
                    angular.bind(this, this.onGetItems, getter.definition),
                    angular.bind(this, this.onLoadError)
                ));
            }, this);
        },

        onGetItems: function(definition, response) {
            var item,
                index,
                // map the response items to the expected format
                items = response.data.items.map(function(i) {
                    // basic object structure
                    item = {
                        obj: i,
                        name: i.name,
                        tag: definition.cls + "_" + i.id,
                        category: definition.category
                    };

                    // map contractor name
                    if (definition.cls == "binderContractor") {
                        item.name = i.contractor ? i.contractor.name : i.contact;
                    }

                    // set the meta data
                    item.metadata = JSON.stringify({ name: item.name });

                    return item;
                });

            this.items = this.items.concat(items);

            // add the selected tags
            items.forEach(function(i) {
                // if the item is found in the tags list add it to selected
                index = this.findTagIndex(i.tag);
                if (index >= 0) {
                    this.selected.push(i);
                }
            }, this);
        },

        onLoadError: function(response) {
            this.$log.error(response);
        },

        getGroup: function(item) {
            return item.category;
        },

        findTagIndex: function(tag) {
            var k,
                len;

            if (!this.tags) {
                return -1;
            }

            for (k = 0, len = this.tags.length; k < len; k++) {
                if (this.tags[k].tag == tag) {
                    return k;
                }
            }

            return -1;
        },

        onSelect: function($select, item, model) {
            $select.search = "";
            if (this.findTagIndex(item.tag) == -1) {
                this.tags.push({
                    tag: item.tag,
                    auto_generated: true,
                    metadata: item.metadata
                });
            }
        },

        onRemove: function(item, model) {
            var index = this.findTagIndex(item.tag);

            if (index > -1) {
                this.tags.splice(index, 1);
            }
        }

    };
})();