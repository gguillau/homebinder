describe("LoadingController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        loading;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        loading = _$injector_.get("Loading");

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("LoadingController", {
            "$scope": $rootScope,
            "message": "Hey!",
            "minorMessage": "Warning"
        });
        $scope.ctrl = ctrl;

    }

    describe('ctrl.init', function() {
        it('inits the controller', function() {
            createController();
            expect(ctrl.message).toEqual("Hey!");
        });
    });
    
    describe('ctrl.change', function() {
        it('inits the controller', function() {
            createController();
            ctrl.change("New");
            
            expect(ctrl.message).toEqual("New");
        });
    });

    describe("message change", function() {
        it("updates the message", function() {
            createController();
            expect(ctrl.message).toEqual("Hey!");

            $rootScope.$broadcast("change-loading-message", {
                message: "updated"
            });

            expect(ctrl.message).toEqual("updated");
        });
    });
    
    describe("Loading factory", function() {
        it("returns false", function() {
            var result = loading.isLoading();
            expect(result).toEqual(false);
        });
    });
});