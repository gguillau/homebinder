(function() {
    "use strict";

    angular
        .module("hb.components.loading", [])
        .controller("LoadingController", ["message", "minorMessage", "$scope", LoadingController])
        .factory("Loading", ["$modal", "$rootScope", Loading]);

    function LoadingController(message, minorMessage, $scope) {
        this.message = message;
        this.minorMessage = minorMessage;
        $scope.$on('change-loading-message', angular.bind(this, function(event, args) {
            this.message = args.message;
        })); 
    }
    
    LoadingController.prototype = {
        
        change: function(message){
            this.message = message;
        }
        
    };

    function Loading($modal, $rootScope) {
        var modalInstance = null,
            is_opened = false;
            
        return {
            show: function(message, minorMessage) {
                if(is_opened){
                    $rootScope.$broadcast("change-loading-message", {message: message});
                }else{
                    modalInstance = $modal.open({
                        templateUrl: "components/loading/loading.tpl.html",
                        backdrop: "static",
                        controller: "LoadingController as ctrl",
                        windowClass: "loading",
                        resolve: {
                            message: function() {
                                return message;
                            },
                            minorMessage: function(){
                                return minorMessage;
                            }
                        }
                    });
                    is_opened = true;
                }
            },
            
            setMessage: function(message){
                $rootScope.$broadcast("change-loading-message", {message: message});
            },
            
            isLoading:function(){
                return is_opened;
            },

            close: function() {
                if (modalInstance){
                    modalInstance.dismiss("cancel");
                    is_opened = false;
                    modalInstance = null;
                }
            }
        };
    }

})();