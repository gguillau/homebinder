describe("HomeProSelectModalController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        data,
        $modalInstance,
        notify,
        HomeProSelect,
        ModalService;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        HomeProSelect = _$injector_.get("HomeProSelect");
        ModalService = _$injector_.get("ModalService");
        data = { contractors: [] };
        $modalInstance = {
            close: function() {},
            dismiss: function() {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("HomeProSelectModalController", {
            "data": data,
            "$modalInstance": $modalInstance,
            "Notify": notify
        });

        $scope.ctrl = ctrl;
    }
    
    describe("ctrl.init", function() {
        it("should filter the contractors", function() {
            createController();
            ctrl.contractors = [{ id: 1, selected: false, email: "test@gmail.com" }, {id: 2, selected: false, email: "test2@gmail.com"}, {id: 3, selected: false, email: "test3@gmail.com"}];
            
            expect(ctrl.contractors.length).toEqual(3);
            
            ctrl.email = "test@gmail.com";
            ctrl.init();

            expect(ctrl.contractors.length).toEqual(1);
        });
    });

    describe("ctrl.createExisting", function() {
        it("should call $modalInstance close function", function() {
            spyOn($modalInstance, "close");

            createController();
            ctrl.contractors = [{ id: 1, selected: false }];
            ctrl.createExisting();
            $rootScope.$apply();

            expect($modalInstance.close).toHaveBeenCalled();
        });
    });

    describe("ctrl.createNew", function() {
        it("should call $modalInstance close function", function() {
            spyOn($modalInstance, "close");

            createController();
            ctrl.createNew();
            $rootScope.$apply();

            expect($modalInstance.close).toHaveBeenCalled();
        });
    });

    describe("ctrl.onSelect", function() {
        it("sets the selected contractor to true", function() {
            createController();
            ctrl.contractors = [{ id: 1, selected: false }, { id: 2, selected: true }];
            expect(ctrl.contractors[0].selected).toEqual(false);
            ctrl.onSelect(ctrl.contractors[0]);

            expect(ctrl.contractors[0].selected).toEqual(true);
        });
    });

    describe("ctrl.dismiss", function() {
        it("should call $modalInstance dismiss function", function() {

            spyOn($modalInstance, "dismiss");

            createController();
            ctrl.dismiss();
            $rootScope.$apply();

            expect($modalInstance.dismiss).toHaveBeenCalledWith('cancel');
        });
    });

    describe('HomeProSelect', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            HomeProSelect.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});