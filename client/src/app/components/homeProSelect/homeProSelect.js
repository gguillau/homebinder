(function() {
    "use strict";

    angular
        .module("hb.components.homeProSelect", [])
        .factory("HomeProSelect", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "components/homeProSelect/homeProSelect.tpl.html",
                            controller: "HomeProSelectModalController as ctrl",
                            backdrop: "static",
                            resolveData: opts,
                            closed: opts.closed,
                            keyboard: false
                        });
                    }
                };
            }
        ])
        .controller("HomeProSelectModalController", [
            "$modalInstance",
            "data",
            "$filter",
            HomeProSelectModalController
        ]);

    function HomeProSelectModalController($modal, opts, $filter) {
        this.$modal = $modal;
        this.$filter = $filter;
        this.contractors = opts.contractors;
        this.email = opts.email;
        this.contractor = null;
        this.canCreateNew = true;
        this.header = "Potential Match(s)";
        this.helpText = "We found a potential match for the information entered. The following company or companies are already on HomeBinder. Click to select existing one, or Create New one.";
        this.init();
    }

    HomeProSelectModalController.prototype = {

        init: function() {
            var array = this.$filter("filter")(this.contractors, {
                email: this.email
            }, true);

            if (array.length > 0 && this.email.length > 1) {
                this.contractors = array;
                this.contractor = array[0];
                this.contractor.selected = true;
                this.canCreateNew = false;
                this.header = "We Found a Match!";
                this.helpText = "This pro is already on HomeBinder.  Either select or cancel and add a new one with a different email address.";
            }
        },

        createExisting: function() {
            this.contractors.forEach(function(cont) {
                cont.selected = false;
            });

            this.$modal.close(this.contractor);
        },

        createNew: function() {
            this.$modal.close();
        },

        onSelect: function(contractor) {
            this.contractors.forEach(function(cont) {
                cont.selected = false;
            });
            contractor.selected = true;
            this.contractor = contractor;
        },

        dismiss: function() {
            this.$modal.dismiss("cancel");
        }
    };
})();