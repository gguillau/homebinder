describe("PartnersEditFormWidgetsController", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        $state,
        notify,
        $log,
        api,
        loading;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $log = _$injector_.get("$log");
        notify = _$injector_.get("Notify");

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("PartnersEditFormWidgetsController", {
            "$state": $state,
            "hb.api": api,
            "$log": $log,
            "Notify": notify,
            "Loading": loading
        });
        $scope.ctrl = ctrl;

    }

    describe('ctrl.refresh', function() {

        it('gets the excludedWidgets', function() {
            spyOn(api.partner, "excludedWidgets").and.returnValue($q.when({ data: [] }));

            createController();
            spyOn(ctrl, "getWidgets");
            spyOn(ctrl, "chunk");
            $rootScope.$apply();

            expect(api.partner.excludedWidgets).toHaveBeenCalled();
            expect(ctrl.chunk).toHaveBeenCalled();
            expect(ctrl.getWidgets).toHaveBeenCalled();
            expect(ctrl.list.length).toEqual(0);
        });

        it('returns an error', function() {
            spyOn(api.partner, "excludedWidgets").and.returnValue($q.reject({ data: "error" }));
            spyOn($log, "error");

            createController();
            $rootScope.$apply();

            expect(api.partner.excludedWidgets).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
        });

    });

    describe('ctrl.getWidgets', function() {

        it('gets the widgets', function() {
            spyOn(api.partner, "excludedWidgets").and.returnValue($q.when({ data: [] }));
            spyOn(api.widget, "all").and.returnValue($q.when({ data: { items: [] } }));

            createController();
            spyOn(ctrl, "chunk");
            $rootScope.$apply();
            ctrl.getWidgets();
            $rootScope.$apply();

            expect(api.widget.all).toHaveBeenCalled();
            expect(ctrl.chunk).toHaveBeenCalled();
            expect(ctrl.items.length).toEqual(0);
        });

        it('returns an error', function() {
            spyOn(api.partner, "excludedWidgets").and.returnValue($q.when({ data: [] }));
            spyOn(api.widget, "all").and.returnValue($q.reject({ data: "error" }));
            spyOn($log, "error");

            createController();
            $rootScope.$apply();
            ctrl.getWidgets();
            $rootScope.$apply();

            expect(api.widget.all).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
        });

    });

    describe('ctrl.chunk', function() {

        it('returns a 2x2 array', function() {
            var items = [{ id: 1 }, { id: 2 }, { id: 3 }, { id: 4 }];
            spyOn(api.partner, "excludedWidgets").and.returnValue($q.when({ data: [] }));
            spyOn(api.widget, "all").and.returnValue($q.when({ data: { items: [] } }));

            createController();
            spyOn(ctrl, "chunk").and.callThrough();
            $rootScope.$apply();
            var array = ctrl.chunk(items, 2);
            $rootScope.$apply();

            expect(array.length).toEqual(2);
            expect(array[0].length).toEqual(2);
            expect(array[1].length).toEqual(2);
        });

    });

    describe('ctrl.clicked', function() {

        it('calls save', function() {
            spyOn(api.partner, "excludedWidgets").and.returnValue($q.when({ data: [] }));

            createController();
            spyOn(ctrl, "getWidgets");
            $rootScope.$apply();
            spyOn(ctrl, "save");
            ctrl.items = [{ name: "widget", checked: true }];
            ctrl.clicked({ name: "widget" });
            $rootScope.$apply();

            expect(ctrl.save).toHaveBeenCalled();
        });

        it('calls remove', function() {
            spyOn(api.partner, "excludedWidgets").and.returnValue($q.when({ data: [{ name: "widget" }] }));

            createController();
            spyOn(ctrl, "getWidgets");
            $rootScope.$apply();
            spyOn(ctrl, "remove");
            ctrl.items = [{ name: "widget", checked: true }];
            ctrl.clicked({ name: "widget" });
            $rootScope.$apply();

            expect(ctrl.remove).toHaveBeenCalled();
        });

    });

    describe('ctrl.save', function() {

        it('adds the excluded widget', function() {
            spyOn(api.partner, "excludedWidgets").and.returnValue($q.when({ data: [] }));
            spyOn(api.widget, "all").and.returnValue($q.when({ data: { items: [] } }));
            spyOn(api.widgetExclusion, "create").and.returnValue($q.when({ data: { name: "widget" } }));

            createController();
            spyOn(ctrl, "chunk");
            $rootScope.$apply();
            ctrl.save({ id: 1 });
            $rootScope.$apply();

            expect(api.widgetExclusion.create).toHaveBeenCalled();
            expect(ctrl.list.length).toEqual(1);
        });

        it('returns an error', function() {
            spyOn(api.partner, "excludedWidgets").and.returnValue($q.when({ data: [] }));
            spyOn(api.widget, "all").and.returnValue($q.when({ data: { items: [] } }));
            spyOn(api.widgetExclusion, "create").and.returnValue($q.reject({ data: "error" }));
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            spyOn(ctrl, "chunk");
            $rootScope.$apply();
            ctrl.save({ id: 1 });
            $rootScope.$apply();

            expect(api.widgetExclusion.create).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
        });

    });

    describe('ctrl.remove', function() {

        it('removes the excluded widget', function() {
            spyOn(api.partner, "excludedWidgets").and.returnValue($q.when({ data: [{ id: 1 }] }));
            spyOn(api.widget, "all").and.returnValue($q.when({ data: { items: [] } }));
            spyOn(api.widgetExclusion, "destroy").and.returnValue($q.when({ data: {} }));

            createController();
            spyOn(ctrl, "chunk");
            $rootScope.$apply();
            ctrl.remove({ id: 1 });
            $rootScope.$apply();

            expect(api.widgetExclusion.destroy).toHaveBeenCalled();
            expect(ctrl.list.length).toEqual(0);
        });

        it('returns an error', function() {
            spyOn(api.partner, "excludedWidgets").and.returnValue($q.when({ data: [{ id: 1 }] }));
            spyOn(api.widget, "all").and.returnValue($q.when({ data: { items: [] } }));
            spyOn(api.widgetExclusion, "destroy").and.returnValue($q.reject({ data: "error" }));
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            spyOn(ctrl, "chunk");
            $rootScope.$apply();
            ctrl.remove({ id: 1 });
            $rootScope.$apply();

            expect(api.widgetExclusion.destroy).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
            expect(ctrl.list.length).toEqual(1);
        });

    });

});

describe('partnersEditFormWidgets', function() {
    var $scope, $compile, element, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");

        var widgets = [{ id: 1, name: "test1" }, { id: 2, name: "test2" }];
        var excluded = [{ id: 1, name: "test1" }];

        spyOn(api.partner, "excludedWidgets").and.returnValue($q.when({ data: excluded }));
        spyOn(api.widget, "all").and.returnValue($q.when({ data: { items: widgets } }));

        $scope = $rootScope.$new(), $compile = _$compile_;

        element = $compile('<partners-edit-form-widgets></partners-edit-form-widgets>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("Available Widgets");
        });
    });
});