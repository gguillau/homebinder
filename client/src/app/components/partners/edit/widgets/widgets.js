(function() {
    "use strict";

    angular
        .module("hb.components.partners.widgets", [])
        .directive("partnersEditFormWidgets", function() {
            return {
                restrict: "E",
                templateUrl: "components/partners/edit/widgets/widgets.tpl.html",
                controller: "PartnersEditFormWidgetsController as ctrl",
                bindToController: true,
                scope: {}
            };
        })
        .controller("PartnersEditFormWidgetsController", [
            "hb.api",
            "$log",
            "Notify",
            "$stateParams",
            "Loading",
            "$filter",
            PartnersEditFormWidgetsController
        ]);

    function PartnersEditFormWidgetsController(api, $log, notify, $stateParams, Loading, $filter) {
        this.api = api;
        this.$log = $log;
        this.notify = notify;
        this.$stateParams = $stateParams;
        this.$filter = $filter;
        this.refresh();
    }

    PartnersEditFormWidgetsController.prototype = {

        refresh: function() {
            this.api.partner.excludedWidgets(this.$stateParams.partnerId).then(
                angular.bind(this, this.onRefreshSuccess),
                angular.bind(this, this.onRefreshError)
            );
        },

        onRefreshSuccess: function(response) {
            this.list = response.data;
            this.excludedWidgets = this.chunk(this.list, 4);
            this.getWidgets();
        },

        onRefreshError: function(response) {
            this.$log.error(response.data);
        },

        getWidgets: function() {
            this.api.widget.all().then(
                angular.bind(this, this.getWidgetSuccess),
                angular.bind(this, this.getWidgetError));
        },

        getWidgetSuccess: function(response) {
            this.items = response.data.items.map(angular.bind(this, function(item) {
                var list = this.$filter("filter")(this.list, {
                    name: item.name
                }, true);
                if (list.length > 0) {
                    item.checked = true;
                }
                else {
                    item.checked = false;
                }
                return item;
            }));
            this.widgets = this.chunk(this.items, 4);
        },

        getWidgetError: function(response) {
            this.$log.error(response.data);
        },

        chunk: function(widgets, size) {
            var array = [];

            for (var i = 0; i < widgets.length; i += size) {
                array.push(widgets.slice(i, i + size));
            }
            return array;
        },

        clicked: function(widget) {
            var list = this.$filter("filter")(this.list, {
                name: widget.name
            }, true);
            if (list.length < 1) {
                this.save(widget);
            }
            else {
                var widg = list[0];
                this.remove(widg);
            }
        },

        save: function(widget) {
            var data = {
                widget_exclusion: {
                    widget_id: widget.id,
                    partner_id: this.$stateParams.partnerId
                }
            };
            this.api.widgetExclusion.create(data).then(
                angular.bind(this, this.saveSuccess),
                angular.bind(this, this.saveError));
        },

        saveSuccess: function(response) {
            this.list.push(response.data);
            this.excludedWidgets = this.chunk(this.list, 4);
        },

        saveError: function(response) {
            this.$log.error(response.data);
            this.notify.error(response.data);
        },

        remove: function(widget) {
            this.api.widgetExclusion.destroy(widget.id).then(
                angular.bind(this, this.removeSuccess, widget),
                angular.bind(this, this.removeError));
        },

        removeSuccess: function(widget, response) {
            var index = this.list.indexOf(widget);
            this.list.splice(index, 1);
            this.excludedWidgets = this.chunk(this.list, 4);
        },

        removeError: function(response) {
            this.$log.error(response.data);
            this.notify.error(response.data);
        }
    };
})();