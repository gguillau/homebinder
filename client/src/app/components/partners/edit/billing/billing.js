(function() {
    "use strict";

    angular
        .module("hb.components.partners.billing", [
            "hb.components.partners.activate"
        ])
        .directive("partnersEditFormBilling", function() {
            return {
                restrict: "E",
                templateUrl: "components/partners/edit/billing/billing.tpl.html",
                controller: "PartnerBillingController",
                controllerAs: "ctrl",
                bindToController: true
            };
        })
        .controller("PartnerBillingController", [
            "hb.framework.editBase",
            "hb.resources",
            "$stateParams",
            "hb.api",
            "hb.utils",
            "Session",
            "Loading",
            "Context",
            "ActivateAccountModal",
            "PaymentModal",
            PartnerBillingController
        ]);

    function PartnerBillingController(EditBase, resources, $stateParams, api, utils, session, loading, context, ActivateAccountModal, PaymentModal) {

        var Model = function() {
            // call the parent class
            EditBase.call(this);
            // apply widget specific resource strings
            this.resources = angular.extend({}, this.resources, resources.partnersEdit);
            this.resources = angular.extend({}, this.resources, resources.partnersBilling);
            this.itemId = $stateParams.partnerId;
            // set the refresh API call
            this.refreshCall = api.account.get;
            this.currentUser = session.getUser();
            // set the save API call
            this.saveCall = api.account.update;
            
            context.getPartner().then(angular.bind(this, function(partner) {
                this.partner = partner;
                // get the item
                this.refresh();
            }));
            this.account_statuses = [{
                name: "active",
                value: "Active"
            }, {
                name: "expired",
                value: "Expired"
            }, {
                name: "suspended",
                value: "Suspended"
            }, {
                name: "canceled",
                value: "Canceled"
            }, {
                name: "closed",
                value: "Closed"
            }];
            this.account_sub_statuses = [{
                name: "none",
                value: "None"
            }, {
                name: "invalid_credit_card",
                value: "InvalidCC"
            }, {
                name: "free_trial_expired",
                value: "Free Trial Expired"
            }];
            this.payment_type = [{
                name: "subscription",
                value: "Subscription"
            }, {
                name: "transaction",
                value: "Transactional"
            }];
            this.account_types = [{
                name: "single",
                value: "Single"
            }, {
                name: "corporate",
                value: "Corporate"
            }];
            this.payment_types = [{
                name: "subscription",
                value: "Subscription"
            }, {
                name: "transactional",
                value: "Transactional"
            }];
            this.account_sub_types = [{
                name: "free_trial",
                value: "Free Trial"
            }, {
                name: "paid",
                value: "Paying"
            }];
            this.billing_type = [{
                name: "",
                value: "Monthly"
            }, {
                name: "",
                value: "Annual"
            }, {
                name: "",
                value: "Quarterly"
            }, {
                name: "",
                value: "Per Binder"
            }];
            this.billing_frequencies = [{
                name: "per_transaction",
                value: "Per Transaction"
            }, {
                name: "annual",
                value: "Annual"
            }, {
                name: "quarterly",
                value: "Quarterly"
            }, {
                name: "monthly",
                value: "Monthly"
            }];
            this.utils = utils;
            this.today = this.utils.utils.getToday(false);
            this.next_month = this.utils.utils.getNextMonth();
            this.yyyy = this.utils.utils.getYear();
            this.datepicker = {
                format: "MMMM dd, yyyy",
                options: {
                    "show-button-bar": false
                }
            };
            this.date = [{
                opened: false
            }, {
                opened: false
            }];
        };

        Model.prototype = Object.create(EditBase.prototype);

        angular.extend(Model.prototype, {
            beforeSave: function() {
                this.account = angular.copy(this.item);
                delete this.account.id;
                delete this.account.account_number;
                delete this.account.partners;
                this.account.transaction_amount = this.item.transaction_amount_cents;
                this.account.subscription_amount = this.item.subscription_amount_cents;

                this.updatedItem = this.account;
            },

            onRefreshSuccess: function(response) {
                this.item = response.data;
                this.item.transaction_amount_cents = Number(this.item.transaction_amount_cents / 100).toFixed(2);
                this.item.subscription_amount_cents = Number(this.item.subscription_amount_cents / 100).toFixed(2);
                if (this.item.billing_activation) {
                    this.item.billing_activation = new Date(this.item.billing_activation);
                }
                if (this.item.trial_expiration) {
                    this.item.trial_expiration = new Date(this.item.trial_expiration);
                }
                if (this.item.stripe_customer_id) {
                    this.getCustomer();
                }
                loading.close();
            },

            getCustomer: function() {
                api.customer.get(this.item.stripe_customer_id).then(
                    angular.bind(this, this.customerGetSuccess),
                    angular.bind(this, this.customerGetError));
            },

            customerGetSuccess: function(response) {
                this.customer = response.data;
                if (this.customer.cards && this.customer.cards.data.length > 0) {
                    this.card = this.customer.cards.data[0];
                }
            },

            customerGetError: function(response) {
                this.$log.error(response);
            },

            // save success
            onSaveSuccess: function(response) {
                this.cancelForm();
                this.item = response.data;
                this.item.transaction_amount_cents = Number(this.item.transaction_amount_cents / 100).toFixed(2);
                this.item.subscription_amount_cents = Number(this.item.subscription_amount_cents / 100).toFixed(2);
                if (this.item.billing_activation) {
                    this.item.billing_activation = new Date(this.item.billing_activation);
                }
                if (this.item.trial_expiration) {
                    this.item.trial_expiration = new Date(this.item.trial_expiration);
                }
                this.notify.success(this.resources.saveSuccess);
                loading.close();
            },

            activateAccount: function() {
                var customer = {
                    id: this.partner.id,
                    name: this.item.name,
                    contact: this.partner.contact,
                    phone: this.partner.phone,
                    email: this.partner.email,
                    role: this.partner.partner_type,
                    address: this.partner.address
                };
                ActivateAccountModal.show({ customer: customer, account_id: this.item.id, closed: angular.bind(this, this.onPaymentUpdate) });
            },

            updatePayment: function() {
                var customer = {
                    email: this.partner.email,
                    address: this.partner.address,
                    stripe_customer_id: this.item.stripe_customer_id
                };
                PaymentModal.show({ customer: customer, account_id: this.item.id, closed: angular.bind(this, this.onPaymentUpdate) });
            },

            onPaymentUpdate: function(response) {
                this.item.stripe_customer_id = response.data.id;
                this.customerGetSuccess(response);
            },

            open: function(index) {
                this.date[index].opened = true;
            }

        });

        this.model = new Model();

    }
})();