describe("PartnerBillingController", function() {
    var controller,
        EditBase,
        api;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });

        // mock the Notify service
        $provide.factory("Notify", function() {
            return {
                info: function() {},
                error: function() {},
                success: function() {}
            };
        });

        // mock the Session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return {
                        role: "admin"
                    };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        EditBase = $injector.get("hb.framework.editBase");
        api = $injector.get("hb.api");

        spyOn(api.partner, "get").and.returnValue($q.when({
            data: { id: 1 }
        }));
        spyOn(api.account, "get").and.returnValue($q.when({
            data: { id: 1 }
        }));

        controller = $controller("PartnerBillingController", {
            "EditBase": EditBase,
            "hb.api": api
        });

        $rootScope.$apply();
    }));

    describe("beforeSave", function() {
        it("sets the object to be updated", inject(function(ActivateAccountModal) {
            var model = controller.model;
            model.item = {
                id: 1,
                name: "Company",
                transaction_amount_cents: 1000,
                subscription_amount_cents: 1000
            };
            model.beforeSave();

            expect(model.updatedItem.name).toEqual("Company");
        }));

    });

    describe("onRefreshSuccess", function() {
        it("sets the item", inject(function(ActivateAccountModal) {
            var model = controller.model;
            spyOn(model, "getCustomer");

            var item = {
                id: 1,
                name: "Company",
                transaction_amount_cents: 1000,
                subscription_amount_cents: 1000,
                billing_activation: new Date(),
                trial_expiration: new Date(),
                stripe_customer_id: 1
            };
            model.onRefreshSuccess({ data: item });

            expect(model.getCustomer).toHaveBeenCalled();
        }));

    });

    describe("getCustomer", function() {
        it("gets the customer object in Stripe", inject(function($q, Loading, $rootScope) {
            spyOn(api.customer, "get").and.returnValue($q.when({
                data: {
                    id: 1,
                    cards: {
                        data: [{ id: 1 }]
                    }
                }
            }));

            var model = controller.model;
            model.billingForm = {
                $submitted: false
            };
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                account: {
                    id: 1,
                    stripe_customer_id: 1
                }
            };
            model.getCustomer();

            $rootScope.$apply();
            expect(api.customer.get).toHaveBeenCalled();
            expect(model.customer).toEqual({ id: 1, cards: { data: [{ id: 1 }] } });
        }));

        it("gets the customer object in Stripe", inject(function($q, Loading, $rootScope, $log, Notify) {
            spyOn(api.customer, "get").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(Notify, "error");

            var model = controller.model;
            model.billingForm = {
                $submitted: false
            };
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                account: {
                    id: 1,
                    stripe_customer_id: 1
                }
            };
            model.getCustomer();

            $rootScope.$apply();
            expect(api.customer.get).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
        }));
    });

    describe("activateAccount", function() {
        it("calls the show function", inject(function(ActivateAccountModal) {
            spyOn(ActivateAccountModal, "show");
            var model = controller.model;
            model.partner = {
                id: 1,
                name: "Company",
                account: {
                    id: 1
                }
            };
            model.activateAccount();

            expect(ActivateAccountModal.show).toHaveBeenCalled();
        }));

    });

    describe("updatePayment", function() {
        it("calls the show function", inject(function(PaymentModal) {
            spyOn(PaymentModal, "show");
            var model = controller.model;
            model.partner = {
                id: 1,
                name: "Company",
                account: {
                    id: 1
                }
            };
            model.updatePayment();

            expect(PaymentModal.show).toHaveBeenCalled();
        }));

    });

    describe("onPaymentUpdate", function() {
        it("update the stripe_customer_id", inject(function() {
            var model = controller.model;
            model.partner = {
                id: 1,
                name: "Company",
                account: {
                    id: 1
                }
            };
            model.onPaymentUpdate({ data: { id: 100 } });

            expect(model.item.stripe_customer_id).toEqual(100);
        }));

    });

    describe("onSaveSuccess", function() {
        it("updates the item", inject(function(ActivateAccountModal) {
            var model = controller.model;
            spyOn(model.notify, "success");

            var item = {
                id: 1,
                name: "Company",
                transaction_amount_cents: 1000,
                subscription_amount_cents: 1000,
                billing_activation: new Date(),
                trial_expiration: new Date(),
                stripe_customer_id: 1
            };
            model.onSaveSuccess({ data: item });

            expect(model.notify.success).toHaveBeenCalled();
        }));

    });

    describe("open", function() {
        it("updates the date", inject(function(ActivateAccountModal) {
            var model = controller.model;

            expect(model.date[0].opened).toBe(false);
            model.open(0);

            expect(model.date[0].opened).toBe(true);
        }));

    });

});

describe('partnersEditFormBilling', function() {
    var $scope, $compile, api, $q, account;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        account = {
            transaction_amount_cents: 10000,
            subscription_amount_cents: 10000
        };
        spyOn(api.partner, "get").and.returnValue($q.when({ data: account }));
        spyOn(api.account, "get").and.returnValue($q.when({ data: account }));

        $compile('<partners-edit-form-billing></partners-edit-form-billing>')($scope);

        $scope.$apply();
    }));

    describe('when the page compiles the partners-edit-form-billing directive', function() {
        it('calls api account get', function() {
            expect(api.account.get).toHaveBeenCalled();
        });
    });
});