describe('partnersEditFormAutomation', function() {
    var $scope, $compile, session, api, $q, partner;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        session = _$injector_.get("Session");
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(session, "getUser").and.returnValue({ role: "admin" });
        partner = {
            address: {},
            configuration: {
                show_isn_tab: false
            }
        };
        spyOn(api.partner, "get").and.returnValue($q.when({ data: partner }));
        spyOn(api.user, "all").and.returnValue($q.when({ data: { items: [] } }));

        $scope.links = [];
        $scope.settings = {
            canEditKey: true
        };

        $compile('<partners-edit-form-automation links="links" settings="settings"></partners-edit-form-automation>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the partners-edit-form-automation directive', function() {
        it('calls api partner get', function() {
            expect(api.partner.get).toHaveBeenCalled();
        });
    });
});

describe('ngRegex', function() {
    var $scope, $compile, ngModel;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        $scope = $rootScope.$new(), $compile = _$compile_;

        var input = $compile('<input type="text" ng-model="model" ng-regex />')($scope);

        ngModel = input.controller('ngModel');

    }));

    it("returns true when invalid", function() {
        $scope.model = "t112./";
        $scope.$apply();

        expect(ngModel.$invalid).toBe(true);
    });
    
    it("returns true when valid", function() {
        $scope.model = "apiroutename";
        $scope.$apply();

        expect(ngModel.$valid).toBe(true);
    });
    
    it("returns true when empty value & valid", function() {
        $scope.model = "";
        $scope.$apply();

        expect(ngModel.$valid).toBe(true);
    });
});