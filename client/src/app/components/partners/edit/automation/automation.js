(function() {
    "use strict";

    angular
        .module("hb.components.partners.automation", [])
        .directive('ngRegex', function() {
            return {
                require: 'ngModel',
                link: function(scope, elm, attrs, ctrl) {
                    var partnerKeyRegex = /^[a-z]+$/;
                    ctrl.$validators.regex = function(modelValue, viewValue) {
                        if (ctrl.$isEmpty(modelValue)) {
                            // consider empty models to be valid
                            return true;
                        }

                        if (partnerKeyRegex.test(viewValue)) {
                            // it is valid
                            return true;
                        }

                        // it is invalid
                        return false;
                    };
                }
            };
        })
        .directive("partnersEditFormAutomation", function() {
            return {
                restrict: "E",
                templateUrl: "components/partners/edit/automation/automation.tpl.html",
                controller: "PartnerAccountController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    links: "=",
                    settings: "="
                }
            };
        });
})();