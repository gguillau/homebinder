describe("PartnerEditWarrantyConfigController", function() {
    var ctrl,
        controller,
        $q,
        $log,
        api,
        notify,
        resources,
        loading,
        editbase,
        defer;

    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getPartner: function() {
                    defer = $q.defer();
                    defer.resolve({ settings: { navigation: {} } });
                    return defer.promise;
                }
            };
        });
    }));

    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
        editbase = $injector.get("hb.framework.editBase");
    }));

    function createController() {
        ctrl = controller("PartnerEditWarrantyConfigController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "hb.resources": resources,
            "Loading": loading,
            "hb.framework.editBase": editbase
        });
    }

    describe("enableWarrantyAccount", function() {
        it("calls api enableWarrantyAccount", function() {
            spyOn(api.partner, "enableWarrantyAccount").and.returnValue($q.when({ data: {} }));

            createController();
            ctrl.model.enableWarrantyAccount();

            expect(api.partner.enableWarrantyAccount).toHaveBeenCalled();
        });
    });

    describe("accountSuccess", function() {
        it("calls notify", function() {
            spyOn(notify, "info");

            createController();
            ctrl.model.accountSuccess();

            expect(notify.info).toHaveBeenCalled();
        });
    });

    describe("accountError", function() {
        it("calls notify", function() {
            spyOn(notify, "info");

            createController();
            ctrl.model.accountError({ data: "error" });

            expect(notify.info).toHaveBeenCalled();
        });
    });

    describe("init", function() {
        it("calls refresh", function() {

            createController();
            spyOn(ctrl.model, "loadWarrantyCompanies");
            spyOn(ctrl.model, "refresh");

            ctrl.model.itemId = 1;
            ctrl.model.init();

            expect(ctrl.model.loadWarrantyCompanies).toHaveBeenCalled();
            expect(ctrl.model.refresh).toHaveBeenCalled();
        });
    });

    describe("onRefreshSuccess", function() {
        it("sets the item and calls loadWarrantyPlans", function() {
            createController();
            spyOn(ctrl.model, "loadWarrantyPlans");
            var item = { id: 1 };
            ctrl.model.onRefreshSuccess({ data: item });

            expect(ctrl.model.loadWarrantyPlans).toHaveBeenCalled();
            expect(ctrl.model.item).toEqual(item);
        });
    });

    describe("loadWarrantyCompanies", function() {
        it("calls api warrantyCompany all", function() {
            spyOn(api.warrantyCompany, "all").and.returnValue($q.when({ data: [] }));

            createController();
            ctrl.model.loadWarrantyCompanies();

            expect(api.warrantyCompany.all).toHaveBeenCalled();
        });
    });

    describe("onLoadWarrantyCompaniesSuccess", function() {
        it("sets the warranty_companies", function() {
            createController();
            ctrl.model.onLoadWarrantyCompaniesSuccess({ data: { items: [] } });

            expect(ctrl.model.warranty_companies).toEqual([]);
        });
    });

    describe("onLoadWarrantyCompaniesError", function() {
        it("calls $log", function() {
            spyOn($log, "error");

            createController();
            ctrl.model.onLoadWarrantyCompaniesError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
        });
    });

    describe("onCompanyChange", function() {
        it("calls loadWarrantyPlans", function() {
            createController();
            spyOn(ctrl.model, "loadWarrantyPlans");
            ctrl.model.item = { warranty_plan: { id: 1, name: "Test" } };
            ctrl.model.onCompanyChange();

            expect(ctrl.model.item.warranty_plan).toBeNull();
            expect(ctrl.model.loadWarrantyPlans).toHaveBeenCalled();
        });
    });

    describe("loadWarrantyPlans", function() {
        it("calls api warrantyPlan all", function() {
            spyOn(api.warrantyPlan, "all").and.returnValue($q.when({ data: [] }));

            createController();
            ctrl.model.item = { warranty_company: { id: 1, name: "Test" } };
            ctrl.model.loadWarrantyPlans();

            expect(api.warrantyPlan.all).toHaveBeenCalled();
        });
    });

    describe("onLoadWarrantyPlansSuccess", function() {
        it("sets the warranty_plans", function() {
            createController();
            ctrl.model.onLoadWarrantyPlansSuccess({ data: { items: [] } });

            expect(ctrl.model.warranty_plans).toEqual([]);
        });
    });

    describe("onLoadWarrantyPlansError", function() {
        it("calls $log", function() {
            spyOn($log, "error");

            createController();
            ctrl.model.onLoadWarrantyPlansError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
        });
    });

    describe("onSave", function() {
        it("does not call api warrantyConfiguration update", function() {
            spyOn(api.warrantyConfiguration, "update").and.returnValue($q.when({ data: [] }));

            createController();
            ctrl.model.item = { warranty_company: { id: 1, name: "Test" }, warranty_plan: { id: 1 } };
            ctrl.model.form = { $invalid: true };
            ctrl.model.onSave();

            expect(api.warrantyConfiguration.update).not.toHaveBeenCalled();
        });

        it("calls api warrantyConfiguration update", function() {
            spyOn(api.warrantyConfiguration, "update").and.returnValue($q.when({ data: [] }));

            createController();
            ctrl.model.item = { warranty_company: { id: 1, name: "Test" }, warranty_plan: { id: 1, name: "Test" } };
            ctrl.model.form = { $invalid: false };
            ctrl.model.onSave();

            expect(api.warrantyConfiguration.update).toHaveBeenCalled();
        });
    });

    describe("onSaveSuccess", function() {
        it("sets the item and calls loading close", function() {
            spyOn(loading, "close");

            createController();
            ctrl.model.onSaveSuccess({ data: { id: 2 } });

            expect(ctrl.model.item).toEqual({ id: 2 });
            expect(loading.close).toHaveBeenCalled();
        });
    });

});

describe('partnersEditFormWarranties', function() {
    var $scope, $compile, context;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        context = _$injector_.get("Context");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(context, "getPartner").and.returnValue({ data: { id: 1, warranties_configuration: {} } });

        $compile('<partners-edit-form-warranties></partners-edit-form-warranties>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls context getPartner', function() {
            expect(context.getPartner).toHaveBeenCalled();
        });
    });
});