(function() {
    "use strict";

    angular
        .module("hb.components.partners.warranties", [])
        .directive("partnersEditFormWarranties", function() {
            return {
                restrict: "E",
                templateUrl: "components/partners/edit/warranties/warranties.tpl.html",
                controller: "PartnerEditWarrantyConfigController",
                controllerAs: "ctrl",
                bindToController: true
            };
        }).controller("PartnerEditWarrantyConfigController", [
            "hb.framework.editBase",
            "hb.api",
            "hb.resources",
            "Loading",
            "Notify",
            "Session",
            "$stateParams",
            "$log",
            "Context",
            PartnerEditWarrantyConfigController
        ]);

    function PartnerEditWarrantyConfigController(EditBase, api, resources, loading, notify, session, $stateParams, $log, context) {
        var Model = function() {
            // call the parent class
            EditBase.call(this);
            // apply widget specific resource strings
            this.resources = angular.extend({}, this.resources, resources.partnersEditWarranties);
            // set the refresh API call
            this.refreshCall = api.warrantyConfiguration.get;
            // set the save API call
            this.saveCall = api.warrantyConfiguration.update;
            this.currentUser = session.getUser();
            this.api = api;
            this.$log = $log;
            this.loading = loading;
            this.notify = notify;
            this.companyLookupCfg = {
                allowClear: true,
                placeholder: "Search for company",
                refresh: angular.bind(this, this.companyLookup),
                itemTemplate: "components/lookup/partner/partnerDropDownItem.tpl.html",
                selectionTemplate: "components/lookup/partner/partnerSelection.tpl.html"
            };
            this.planLookupCfg = {
                allowClear: true,
                placeholder: "Search for plan",
                refresh: angular.bind(this, this.planLookup),
                itemTemplate: "components/lookup/partner/partnerDropDownItem.tpl.html",
                selectionTemplate: "components/lookup/partner/partnerSelection.tpl.html"
            };
            // init the controller
            context.getPartner().then(angular.bind(this, function(partner) {
                this.partner = partner;
                this.itemId = this.partner.warranties_configuration ? this.partner.warranties_configuration.id : null;
                this.init();
            }));
        };

        Model.prototype = Object.create(EditBase.prototype);

        angular.extend(Model.prototype, {
            enableWarrantyAccount: function() {
                this.api.partner.enableWarrantyAccount($stateParams.partnerId).then(
                    angular.bind(this, this.accountSuccess),
                    angular.bind(this, this.accountError));
            },

            accountSuccess: function(response) {
                this.notify.info("A HomeBinder respresentative will be in contact with you shortly!");
            },

            accountError: function(response) {
                this.notify.info(response.data);
            },

            init: function() {
                if (this.itemId) {
                    this.loadWarrantyCompanies();
                    this.refresh();
                }
            },

            // Refresh callback. Updates the item list and totals
            onRefreshSuccess: function(response) {
                this.item = response.data;
                // load the warranty companies/plans
                this.loadWarrantyPlans();
                this.loading.close();
            },

            loadWarrantyCompanies: function() {
                var opts = {
                    page: 1,
                    count: 50
                };
                this.api.warrantyCompany.all(opts).then(
                    angular.bind(this, this.onLoadWarrantyCompaniesSuccess),
                    angular.bind(this, this.onLoadWarrantyCompaniesError)
                );
            },

            onLoadWarrantyCompaniesSuccess: function(response) {
                this.warranty_companies = response.data.items;
            },

            onLoadWarrantyCompaniesError: function(response) {
                this.$log.error(response);
            },

            onCompanyChange: function() {
                this.item.warranty_plan = null;
                this.loadWarrantyPlans();
            },

            loadWarrantyPlans: function() {
                var opts = {
                    page: 1,
                    count: 50,
                    warranty_company_id: this.item.warranty_company.id
                };
                this.api.warrantyPlan.all(opts).then(
                    angular.bind(this, this.onLoadWarrantyPlansSuccess),
                    angular.bind(this, this.onLoadWarrantyPlansError)
                );
            },

            onLoadWarrantyPlansSuccess: function(response) {
                this.warranty_plans = response.data.items;
            },

            onLoadWarrantyPlansError: function(response) {
                this.$log.error(response);
            },

            beforeSave: function() {
                this.updatedItem = {
                    warranty_configuration: {
                        automatic_purchases: this.item.automatic_purchases,
                        active_account: this.item.active_account,
                        account_number: this.item.account_number,
                        warranty_plan_id: this.item.warranty_plan.id,
                        warranty_company_id: this.item.warranty_company.id
                    },
                    partner_warranty_plans: this.warranty_plans
                };
            }

        });

        this.model = new Model();

    }
})();