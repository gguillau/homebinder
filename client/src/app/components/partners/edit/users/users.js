(function() {
    "use strict";

    angular
        .module("hb.components.partners.users", [])
        .directive("partnersEditFormUsers", function() {
            return {
                restrict: "E",
                templateUrl: "components/partners/edit/users/users.tpl.html",
                controller: "PartnerEditUsersIndexController",
                controllerAs: "ctrl",
                bindToController: true
            };
        })
        .controller("PartnerEditUsersIndexController", [
            "UsersIndexController",
            "$stateParams",
            "Context",
            PartnerEditUsersIndexController
        ]);

    function PartnerEditUsersIndexController(UsersIndexController, $stateParams, context) {
        var Model = function() {
            // call the parent class
            UsersIndexController.call(this);
            this.partnerRoles = [{
                value: "member",
                name: "Partner Member"
            }, {
                value: "admin",
                name: "Partner Admin"
            }];
            this.partnerId = $stateParams.partnerId;
            context.getPartner().then(angular.bind(this, function(partner) {
                var role = partner.partner_type;
                this.queryArgs = {
                    role: role,
                    partnerId: this.partnerId,
                    searchMethod: "for_partner"
                };
                this.userArgs = {
                    role: role
                };
                this.refresh();
            }));
        };

        Model.prototype = Object.create(UsersIndexController.prototype);

        angular.extend(Model.prototype, {

            removeHeaders: function() {
                this.headers.splice(4, 1);
                this.headers.splice(3, 1);
                this.headers.splice(0, 1);
            },

            deleteItem: function(item, index) {
                if (item.partner_role === "admin") {
                    this.notify.info("This is the Admin user for the account. To delete or change please contact HomeBinder at support@homebinder.com or call 800-377-6915");
                }
                else {
                    this.api.partner.removeUser(this.partnerId, item.id).then(
                        angular.bind(this, this.onDeleteItemSuccess, index),
                        angular.bind(this, this.onDeleteItemError)
                    );
                }
            },

            saveRole: function(user, index) {
                var update_user = {
                    id: user.id,
                    role: user.partner_role
                };
                this.api.partner.updateRole(this.partnerId, update_user).then(
                    angular.bind(this, this.saveRoleSuccess, user),
                    angular.bind(this, this.saveRoleError));
            },

            saveRoleSuccess: function(user, response) {
                this.notify.success("Role Updated");
                user.saveUser = false;
            },

            saveRoleError: function(response) {
                this.$log.error(response);
                this.notify.error(response.data);
            }

        });

        this.model = new Model();
    }
})();