describe("PartnerEditUsersIndexController", function() {
    var controller,
        base,
        $q_,
        notify,
        api,
        defer;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getPartner: function() {
                    defer = $q_.defer();
                    defer.resolve({ settings: { navigation: {} } });
                    return defer.promise;
                }
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        $q_ = $q;
        base = $injector.get("UsersIndexController");
        notify = $injector.get("Notify");
        api = $injector.get("hb.api");

        controller = $controller("PartnerEditUsersIndexController", {
            "UsersIndexController": base,
            "Notify": notify,
            "hb.api": api
        });
    }));

    describe("removeHeaders", function() {
        it("sets the headers", function() {
            expect(controller.model.headers.length).toEqual(3);
        });
    });

    describe("deleteItem", function() {
        it("calls notify", function() {
            spyOn(notify, "info");
            controller.model.deleteItem({ partner_role: "admin" }, 0);

            expect(notify.info).toHaveBeenCalled();
        });

        it("calls api", function() {
            spyOn(api.partner, "removeUser").and.returnValue($q_.when({ data: {} }));
            controller.model.deleteItem({ partner_role: "member" }, 0);

            expect(api.partner.removeUser).toHaveBeenCalled();
        });
    });

    describe("saveRole", function() {
        it("calls api", function() {
            spyOn(api.partner, "updateRole").and.returnValue($q_.when({ data: {} }));
            controller.model.saveRole({ id: 1, partner_role: "member" }, 0);

            expect(api.partner.updateRole).toHaveBeenCalled();
        });
    });

    describe("saveRoleSuccess", function() {
        it("calls notify", function() {
            spyOn(notify, "success");
            controller.model.saveRoleSuccess({ data: {} });

            expect(notify.success).toHaveBeenCalled();
        });
    });

    describe("saveRoleError", function() {
        it("calls notify", function() {
            spyOn(notify, "error");
            controller.model.saveRoleError({ data: {} });

            expect(notify.error).toHaveBeenCalled();
        });
    });

});

describe('partnersEditFormUsers', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        context = _$injector_.get("Context");
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.user, "all").and.returnValue($q.when({ data: { items: [] } }));
        spyOn(context, "getPartner").and.returnValue({ data: { id: 1, warranties_configuration: {} } });

        $compile('<partners-edit-form-users></partners-edit-form-users>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls context getPartner', function() {
            expect(context.getPartner).toHaveBeenCalled();
        });
    });
});