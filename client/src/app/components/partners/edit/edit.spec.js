describe("PartnersEditController", function() {
    var PartnersEditController,
        TestClass,
        defer,
        $q,
        $rootScope;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getPartner: function() {
                    defer = $q.defer();
                    defer.resolve({ settings: { navigation: {} } });
                    return defer.promise;
                }
            };
        });
    }));
    // get the base class under test
    beforeEach(inject(function($injector) {
        PartnersEditController = $injector.get("PartnersEditController");
        $q = $injector.get("$q");
        $rootScope = $injector.get("$rootScope");
    }));

    function createTestClass() {
        TestClass = function() {
            PartnersEditController.call(this);

            this.resources = angular.extend({}, this.resources, {
                loading: "loading",
                loadError: "load error",
                confirmDelete: "confirm",
                deleteMsg1: "a ",
                deleteMsg2: " b",
                deleteSuccess: "deleted",
                deleteError: "delete error",
                saveSuccess: "SAVE",
                impersonateLoading: "LO"
            });
            this.indexState = "admin.partners";
            this.indexParams = {};
            this.nameProperty = "name";
        };

        TestClass.prototype = Object.create(PartnersEditController.prototype);

        return new TestClass();
    }

    describe("changeLink", function() {
        it("changes the current link", inject(function() {

            var model = createTestClass();
            $rootScope.$apply();
            
            expect(model.currentLink.active).toEqual(true);
            expect(model.currentLink.value).toEqual("account");
            expect(model.navigation_links[0].links[0].active).toEqual(true);

            var link = {
                value: "automation",
                name: "Automation",
                active: false
            };
            
            model.changeLink(link);

            expect(model.currentLink.active).toEqual(true);
            expect(model.currentLink.value).toEqual("automation");
            expect(model.navigation_links[0].links[0].active).toEqual(false);
        }));

    });

});

describe('httpPrefix', function() {
    var $scope, $compile;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        $scope = $rootScope.$new(), $compile = _$compile_;

        $compile('<input type="url" ng-model="model" http-prefix />')($scope);
    }));

    it("adds http://", function() {
        $scope.model = "homebinder.com";
        $scope.$apply();

        expect($scope.model).toEqual("http://homebinder.com");
    });

    it("doesnt add http://", function() {
        $scope.model = "http://homebinder";
        $scope.$apply();

        expect($scope.model).toEqual("http://homebinder");
    });
});