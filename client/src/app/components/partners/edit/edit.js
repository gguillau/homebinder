(function() {
    "use strict";

    angular.module('hb.components.partners.edit', [
            "hb.components.partners.account",
            "hb.components.partners.annualPropertyReview",
            "hb.components.partners.automation",
            "hb.components.partners.billing",
            "hb.components.partners.branding",
            "hb.components.partners.customWidget",
            "hb.components.partners.dashboards",
            "hb.components.partners.isn",
            "hb.components.partners.users",
            "hb.components.partners.warranties",
            "hb.components.partners.widgets"
        ])
        .directive('httpPrefix', function() {
            return {
                restrict: 'A',
                require: 'ngModel',
                link: function(scope, element, attrs, controller) {
                    function ensureHttpPrefix(value) {
                        // Need to add prefix if we don't have http:// prefix already AND we don't have part of it
                        if (value && !/^(https?):\/\//i.test(value) &&
                            'http://'.indexOf(value) !== 0 && 'https://'.indexOf(value) !== 0) {
                            controller.$setViewValue('http://' + value);
                            controller.$render();
                            return 'http://' + value;
                        }
                        else {
                            return value;
                        }
                    }
                    controller.$formatters.push(ensureHttpPrefix);
                    controller.$parsers.splice(0, 0, ensureHttpPrefix);
                }
            };
        })
        .factory("PartnersEditController", [
            'hb.api',
            'hb.resources',
            '$stateParams',
            'Context',
            'Session',
            function(api, resources, $stateParams, context, session) {
                var Model = function() {
                    // apply widget specific resource strings
                    this.resources = angular.extend({}, this.resources, resources.partnersEdit);
                    this.currentUser = session.getUser();
                    this.settings = {};
                    context.getPartner().then(angular.bind(this, function(partner) {
                        this.partner = partner;
                        this.init();
                    }));
                };

                angular.extend(Model.prototype, {
                    init: function() {
                        if (this.currentUser.role === "admin" || this.currentUser.role === "limited_admin") {
                            // default navigation links for admins
                            this.navigation_links = [{
                                name: "Settings",
                                links: [
                                    { name: "Account", value: "account", active: true },
                                    { name: "Automation", value: "automation", active: false },
                                    { name: "Billing", value: "billing", active: false },
                                    { name: "Branding", value: "branding", active: false },
                                    { name: "Annual Property Reviews", value: "apr", active: false },
                                    { name: "Warranties", value: "warranties", active: false },
                                    {
                                        value: "users",
                                        name: this.resources.users,
                                        active: false
                                    },
                                    {
                                        value: "widgets",
                                        name: this.resources.widgets,
                                        active: false
                                    },
                                    {
                                        value: "dashboards",
                                        name: this.resources.dashboard,
                                        active: false
                                    },
                                    {
                                        value: "customWidget",
                                        name: this.resources.customWidget,
                                        active: false
                                    }
                                ]
                            }];
                            this.settings.showClientCalls = true;
                            this.currentLink = this.navigation_links[0].links[0];
                        }
                        else {
                            // default navigation links for partners
                            this.navigation_links = [{
                                name: "Settings",
                                links: [
                                    { name: "Account", value: "account", active: true },
                                    { name: "Automation", value: "automation", active: false },
                                    { name: "Branding", value: "branding", active: false }
                                ]
                            }];
                            if (this.partner.settings.navigation.apr) {
                                this.navigation_links[0].links.push({ name: "Annual Property Reviews", value: "apr", active: false });
                            }
                            if (this.partner.settings.navigation.warranties) {
                                this.navigation_links[0].links.push({ name: "Warranties", value: "warranties", active: false });
                            }
                            if (this.partner.settings.navigation.billing) {
                                this.navigation_links[0].links.push({ name: "Billing", value: "billing", active: false });
                            }
                            this.navigation_links[0].links.push({ name: this.resources.customWidget, value: "customWidget", active: false });
                            this.currentLink = this.navigation_links[0].links[0];
                        }
                    },
                    changeLink: function(link) {
                        this.currentLink.active = false;
                        link.active = true;
                        this.currentLink = link;
                    }
                });

                return Model;
            }
        ]);
})();
