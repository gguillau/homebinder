describe("PartnerIsnModalCtrl", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        data,
        $modalInstance,
        notify,
        $log,
        api,
        loading,
        context,
        PartnerIsnModal,
        ModalService,
        defer;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        PartnerIsnModal = _$injector_.get("PartnerIsnModal");
        ModalService = _$injector_.get("ModalService");

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        api = {
            partner: {
                createIsnUser: function(id, user) {}
            },
            isnUsers: {
                create: function() {},
                update: function() {}
            }
        };

        $log = {
            error: function(msg) {}
        };

        notify = {
            error: function(msg) {},
            info: function(msg) {}
        };

        $modalInstance = {
            dismiss: function(msg) {},
            close: function(arg) {}
        };

        data = {
            partnerId: 100,
            addUserSuccess: function() {}
        };

        context = {
            getPartner: function() {
                defer = $q.defer();
                defer.resolve({ id: 100, settings: { navigation: {} }, configuration: { id: 100 } });
                return defer.promise;
            }
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("PartnerIsnModalCtrl", {
            "hb.api": api,
            "$log": $log,
            "Notify": notify,
            "$modalInstance": $modalInstance,
            "data": data,
            "Loading": loading,
            "Context": context
        });

        $scope.ctrl = ctrl;

    }

    describe("PartnerIsnModalCtrl", function() {

        it("should have the intialize the appropriate variables from the opts object", function() {

            createController();
            $rootScope.$apply();

            expect(ctrl.partnerId).toEqual(100);

        });

    });

    describe('ctrl.onSubmit', function() {

        it('should call isnUsers create function successfully', function() {

            var user = {
                name: "Bob Smith",
                company_key: "Test",
                email: "bobsmith@gmail.com",
                username: "bsmith",
                password: "password",
                partner_configuration_id: 100
            };

            spyOn(api.isnUsers, "create").and.returnValue($q.when({ data: user }));
            spyOn(notify, "info");
            spyOn($modalInstance, "dismiss");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            spyOn(ctrl, "addUserSuccess");
            $rootScope.$apply();
            ctrl.form = user;
            ctrl.onSubmit();
            $rootScope.$apply();

            expect(notify.info).toHaveBeenCalledWith("User saved.");
            expect(api.isnUsers.create).toHaveBeenCalledWith(user);
            expect($modalInstance.dismiss).toHaveBeenCalledWith("cancel");
            expect(loading.show).toHaveBeenCalledWith("Adding user bsmith");
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.addUserSuccess).toHaveBeenCalledWith(user);

        });

        it('should call isnUsers update function successfully', function() {

            var user = {
                id: 1,
                name: "Bob Smith",
                company_key: "Test",
                email: "bobsmith@gmail.com",
                username: "bsmith",
                password: "password",
                partner_configuration_id: 100
            };

            spyOn(api.isnUsers, "update").and.returnValue($q.when({ data: user }));
            spyOn(notify, "info");
            spyOn($modalInstance, "dismiss");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            spyOn(ctrl, "addUserSuccess");
            $rootScope.$apply();
            ctrl.form = user;
            ctrl.onSubmit();
            $rootScope.$apply();

            expect(notify.info).toHaveBeenCalledWith("User saved.");
            expect(api.isnUsers.update).toHaveBeenCalledWith(1, user);
            expect($modalInstance.dismiss).toHaveBeenCalledWith("cancel");
            expect(loading.show).toHaveBeenCalledWith("Updating user bsmith");
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.addUserSuccess).toHaveBeenCalledWith(user);

        });

        it('should call partner createIsnUser function and return an error', function() {

            var user = {
                name: "Bob Smith",
                company_key: "Test",
                email: "bobsmith@gmail.com",
                username: "bsmith",
                password: "password",
                partner_configuration_id: 100
            };

            spyOn(api.isnUsers, "create").and.returnValue($q.reject({ data: "error" }));
            spyOn(notify, "error");
            spyOn($log, "error");
            spyOn(loading, "show");
            spyOn(loading, "close");

            createController();
            $rootScope.$apply();
            ctrl.form = user;
            ctrl.onSubmit();
            $rootScope.$apply();

            expect(loading.show).toHaveBeenCalledWith("Adding user bsmith");
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.error).toEqual('error');
            expect(notify.error).toHaveBeenCalledWith("error");
            expect($log.error).toHaveBeenCalledWith({ data: "error" });

        });

    });

    describe('ctrl.onCancel', function() {

        it('should call $modalInstance dismiss function', function() {

            spyOn($modalInstance, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.onCancel();
            $rootScope.$apply();

            expect($modalInstance.dismiss).toHaveBeenCalledWith('cancel');
        });

    });

    describe('PartnerIsnModal', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            PartnerIsnModal.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });

});