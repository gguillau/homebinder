(function() {
    "use strict";

    angular
        .module("hb.components.partners.isn.form", [
            "ui.router",
            "ui.bootstrap",
            "hb.components"
        ])
        .factory("PartnerIsnModal", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: 'components/partners/edit/isn/form/form.tpl.html',
                            controller: 'PartnerIsnModalCtrl as ctrl',
                            resolveData: opts,
                            closed: opts.closed,
                            backdrop: "static"
                        });
                    }
                };
            }
        ])
        .controller("PartnerIsnModalCtrl", [
            "$log",
            "$modalInstance",
            "hb.api",
            "Notify",
            "data",
            "Loading",
            "Context",
            PartnerIsnModalCtrl
        ]);

    function PartnerIsnModalCtrl($log, $modalInstance, api, Notify, opts, loading, Context) {
        this.$log = $log;
        this.$modalInstance = $modalInstance;
        this.api = api;
        this.notify = Notify;
        this.partnerId = opts.partnerId;
        this.addUserSuccess = opts.addUserSuccess;
        this.loading = loading;
        this.form = opts.user;
        Context.getPartner().then(angular.bind(this, function(partner) {
            this.partner = partner;
        }));
    }

    PartnerIsnModalCtrl.prototype = {

        onSubmit: function() {
            var user = this.form;

            if (user.id) {
                this.loading.show("Updating user " + user.username);
                this.api.isnUsers.update(this.form.id, this.form).then(
                    angular.bind(this, this.onUserAddedSuccess),
                    angular.bind(this, this.onUserAddedError));
            }
            else {
                user.partner_configuration_id = this.partner.configuration.id;
                this.loading.show("Adding user " + user.username);
                this.api.isnUsers.create(user).then(
                    angular.bind(this, this.onUserAddedSuccess),
                    angular.bind(this, this.onUserAddedError));
            }
        },

        onUserAddedSuccess: function(response) {
            this.notify.info("User saved.");
            this.addUserSuccess(response.data);
            this.$modalInstance.dismiss("cancel");
            this.loading.close();
        },

        onUserAddedError: function(response) {
            this.error = response.data;
            this.$log.error(response);
            this.notify.error(response.data);
            this.loading.close();
        },

        onCancel: function() {
            this.$modalInstance.dismiss("cancel");
        }
    };
})();