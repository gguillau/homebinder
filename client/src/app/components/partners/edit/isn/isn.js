(function() {
    "use strict";

    angular
        .module("hb.components.partners.isn", [
            "hb.components.partners.isn.form"
        ])
        .directive("partnersEditFormIsn", function() {
            return {
                restrict: "E",
                templateUrl: "components/partners/edit/isn/isn.tpl.html",
                controller: "PartnerEditFormIsnCtrl",
                controllerAs: "ctrl",
                bindToController: true
            };
        })

        .controller("PartnerEditFormIsnCtrl", [
            "$log",
            "hb.api",
            "Notify",
            "$stateParams",
            "PartnerIsnModal",
            "Context",
            PartnerEditFormIsnCtrl
        ]);

    function PartnerEditFormIsnCtrl($log, api, Notify, $stateParams, PartnerIsnModal, context) {
        this.$log = $log;
        this.api = api;
        this.notify = Notify;
        this.partnerId = $stateParams.partnerId;
        this.PartnerIsnModal = PartnerIsnModal;
        context.getPartner().then(angular.bind(this, function(partner) {
            this.partner = partner;
            this.getUsers();
        }));
        this.users = [];
    }

    PartnerEditFormIsnCtrl.prototype = {

        getUsers: function() {
            this.api.isnUsers.all({ partner_configuration_id: this.partner.configuration.id }).then(
                angular.bind(this, this.getUsersSuccess),
                angular.bind(this, this.getUsersError));
        },

        getUsersSuccess: function(response) {
            this.users = response.data.items;
        },

        getUsersError: function(response) {
            this.notify.error(response);
        },

        refresh: function() {
            this.api.partner.getFootprints(this.partnerId).then(
                angular.bind(this, this.footprintsSuccess),
                angular.bind(this, this.footprintsError));
        },

        footprintsSuccess: function(response) {
            this.notify.info("Footprints are being created");
        },

        footprintsError: function(response) {
            this.notify.error(response.data);
        },

        add: function() {
            this.PartnerIsnModal.show({
                user: null,
                partnerId: this.partnerId,
                addUserSuccess: angular.bind(this, this.onAdd, -1)
            });
        },

        edit: function(user, $index) {
            this.PartnerIsnModal.show({
                user: user,
                partnerId: this.partnerId,
                addUserSuccess: angular.bind(this, this.onAdd, $index)
            });
        },

        remove: function(user, $index) {
            this.api.isnUsers.destroy(user.id).then(
                angular.bind(this, this.removeSuccess, $index),
                angular.bind(this, this.removeError));
        },

        removeSuccess: function($index, response) {
            this.users.splice($index, 1);
            this.notify.success("User removed");
        },

        removeError: function(response) {
            this.$log.error(response);
            this.notify.error(response.data);
        },

        onAdd: function(index, user) {
            if (index > -1) {
                this.users[index] = user;
            }
            else {
                this.users.push(user);
            }
        }
    };
})();