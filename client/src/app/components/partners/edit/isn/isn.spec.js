describe("PartnerEditFormIsnCtrl", function() {
    var $rootScope,
        $scope,
        $controller,
        $q,
        ctrl,
        notify,
        $log,
        api,
        PartnerIsnModal,
        $stateParams,
        defer;

    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the context service
        $provide.factory("Context", function() {
            return {
                getPartner: function() {
                    defer = $q.defer();
                    defer.resolve({ id: 1, settings: { navigation: {} }, configuration: { id: 1 } });
                    return defer.promise;
                }
            };
        });
    }));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        notify = _$injector_.get("Notify");
        PartnerIsnModal = _$injector_.get("PartnerIsnModal");
        $stateParams = _$injector_.get("$stateParams");
        $log = _$injector_.get("$log");
        api = _$injector_.get("hb.api");

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("PartnerEditFormIsnCtrl", {
            "hb.api": api,
            "$log": $log,
            "Notify": notify,
            "PartnerIsnModal": PartnerIsnModal,
            "$stateParams": $stateParams
        });

        $scope.ctrl = ctrl;
        $rootScope.$apply();
    }

    beforeEach(function() {
        spyOn(api.isnUsers, "all").and.returnValue($q.when({ data: { items: [] } }));
    });

    describe('ctrl.getUsersError', function() {
        it('calls notify', function() {
            spyOn(notify, "error");

            createController();
            $rootScope.$apply();
            ctrl.getUsersError({ data: "error" });
            $rootScope.$apply();

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe('ctrl.refresh', function() {
        it('retrieves footprints', function() {
            spyOn(api.partner, "getFootprints").and.returnValue($q.when({ data: "success" }));
            spyOn(notify, "info");

            createController();
            $rootScope.$apply();
            ctrl.refresh();
            $rootScope.$apply();

            expect(api.partner.getFootprints).toHaveBeenCalled();
            expect(notify.info).toHaveBeenCalled();
        });

        it('tries to removes user and return an error', function() {

            spyOn(api.partner, "getFootprints").and.returnValue($q.reject({ data: "error" }));
            spyOn(notify, "error");

            createController();
            $rootScope.$apply();
            ctrl.refresh();
            $rootScope.$apply();

            expect(api.partner.getFootprints).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe('ctrl.add', function() {

        it('calls PartnerIsnModal', function() {

            spyOn(PartnerIsnModal, "show");

            createController();
            $rootScope.$apply();
            ctrl.add();
            $rootScope.$apply();

            expect(PartnerIsnModal.show).toHaveBeenCalled();

        });

    });

    describe('ctrl.edit', function() {

        it('calls PartnerIsnModal', function() {

            spyOn(PartnerIsnModal, "show");

            createController();
            $rootScope.$apply();
            ctrl.users = [{ id: 1 }];
            ctrl.edit({ id: 1 }, 0);
            $rootScope.$apply();

            expect(PartnerIsnModal.show).toHaveBeenCalled();

        });

    });

    describe('ctrl.remove', function() {

        it('removes user', function() {

            spyOn(api.isnUsers, "destroy").and.returnValue($q.when({ data: "success" }));
            spyOn(notify, "success");

            createController();
            $rootScope.$apply();
            ctrl.users = [{ id: 1 }];
            ctrl.remove({ id: 1 }, 0);
            $rootScope.$apply();

            expect(api.isnUsers.destroy).toHaveBeenCalled();
            expect(notify.success).toHaveBeenCalled();
            expect(ctrl.users.length).toEqual(0);
        });

        it('tries to removes user and return an error', function() {

            spyOn(api.isnUsers, "destroy").and.returnValue($q.reject({ data: "error" }));
            spyOn(notify, "error");
            spyOn($log, "error");

            createController();
            $rootScope.$apply();
            ctrl.users = [{ id: 1 }];
            ctrl.remove({ id: 1 }, 0);
            $rootScope.$apply();

            expect(api.isnUsers.destroy).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
        });

    });

    describe('ctrl.onAdd', function() {

        it('adds user', function() {

            createController();
            $rootScope.$apply();
            ctrl.onAdd(-1, {});
            $rootScope.$apply();

            expect(ctrl.users.length).toEqual(1);

        });

        it('updates user', function() {

            createController();
            $rootScope.$apply();
            ctrl.users = [{ id: 1, name: "Test" }];
            ctrl.onAdd(0, { name: "best" });
            $rootScope.$apply();

            expect(ctrl.users.length).toEqual(1);
            expect(ctrl.users[0].name).toEqual("best");

        });

    });

});

describe('partnersEditFormIsn', function() {
    var $scope, $compile, api, $q, context;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        context = _$injector_.get("Context");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(context, "getPartner").and.returnValue($q.when({ configuration: { id: 1 } }));
        spyOn(api.isnUsers, "all").and.returnValue($q.when({ data: { items: [] } }));

        $compile('<partners-edit-form-isn></partners-edit-form-isn>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls api isnUsers all', function() {
            expect(api.isnUsers.all).toHaveBeenCalled();
        });
    });
});