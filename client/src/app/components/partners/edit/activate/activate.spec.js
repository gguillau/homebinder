describe("ActivateAccountModalController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        data,
        $modalInstance,
        PaymentModal,
        ActivateAccountModal,
        ModalService;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        PaymentModal = _$injector_.get("PaymentModal");
        ActivateAccountModal = _$injector_.get("ActivateAccountModal");
        ModalService = _$injector_.get("ModalService");

        data = {

        };

        $modalInstance = {
            dismiss: function() {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("ActivateAccountModalController", {
            "data": data,
            "$modalInstance": $modalInstance,
            "PaymentModal": PaymentModal
        });

        $scope.ctrl = ctrl;
    }

    describe("init", function() {

        it("should set customer and account_id", function() {
            var customer = { id: 1, name: "Test", phone: "phone" };
            data.customer = customer;
            data.account_id = 100;

            createController();
            $rootScope.$apply();

            expect(ctrl.customer).toEqual(customer);
            expect(ctrl.account_id).toEqual(100);
        });

        it("should set cost and plan", function() {
            var customer = { id: 1, name: "Test", phone: "phone", role: "broker" };
            data.customer = customer;
            data.account_id = 100;

            createController();
            $rootScope.$apply();

            expect(ctrl.customer).toEqual(customer);
            expect(ctrl.account_id).toEqual(100);
            expect(ctrl.cost).toEqual(49);
            expect(ctrl.plan).toEqual("49permonthagent");
            expect(ctrl.step).toEqual("two");
        });

    });

    describe("ctrl.next", function() {

        it("sets step to two and calls calculate", function() {

            createController();
            $rootScope.$apply();
            spyOn(ctrl, "calculate");
            ctrl.next();
            $rootScope.$apply();

            expect(ctrl.step).toEqual("two");
            expect(ctrl.calculate).toHaveBeenCalled();
        });

    });

    describe("ctrl.calculate", function() {

        it("sets plan and cost", function() {

            createController();
            $rootScope.$apply();
            ctrl.count = 49;
            ctrl.calculate();
            $rootScope.$apply();

            expect(ctrl.plan).toEqual("16newinspectorsubscription");
            expect(ctrl.cost).toEqual(16);
        });

        it("sets plan and cost", function() {

            createController();
            $rootScope.$apply();
            ctrl.count = 50;
            ctrl.calculate();
            $rootScope.$apply();

            expect(ctrl.plan).toEqual("49permonth");
            expect(ctrl.cost).toEqual(49);
        });

        it("sets plan and cost", function() {

            createController();
            $rootScope.$apply();
            ctrl.count = 200;
            ctrl.calculate();
            $rootScope.$apply();

            expect(ctrl.plan).toEqual("54permonth");
            expect(ctrl.cost).toEqual(54);
        });

        it("sets plan and cost", function() {

            createController();
            $rootScope.$apply();
            ctrl.count = 300;
            ctrl.calculate();
            $rootScope.$apply();

            expect(ctrl.plan).toEqual("54permonth");
            expect(ctrl.cost).toEqual(54);
        });

        it("sets plan and cost", function() {

            createController();
            $rootScope.$apply();
            ctrl.count = 301;
            ctrl.customer = { name: "Test" };
            ctrl.calculate();
            $rootScope.$apply();

            expect(ctrl.plan).toEqual("Test Subscription");
            expect(ctrl.cost).toEqual(59);
        });

    });

    describe("ctrl.pay", function() {

        it("should call cancel and PaymentModal", function() {

            spyOn($modalInstance, "dismiss");
            spyOn(PaymentModal, "show");

            createController();
            $rootScope.$apply();
            ctrl.pay();
            $rootScope.$apply();

            expect($modalInstance.dismiss).toHaveBeenCalledWith('cancel');
            expect(PaymentModal.show).toHaveBeenCalled();
        });

    });

    describe("ctrl.cancel", function() {

        it("should call $modalInstance dismiss function", function() {

            spyOn($modalInstance, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.cancel();
            $rootScope.$apply();

            expect($modalInstance.dismiss).toHaveBeenCalledWith('cancel');
        });

    });

    describe('ActivateAccountModal', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            ActivateAccountModal.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });
});