(function() {
    "use strict";

    angular
        .module("hb.components.partners.activate", [])
        .factory("ActivateAccountModal", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "components/partners/edit/activate/activate.tpl.html",
                            controller: "ActivateAccountModalController as ctrl",
                            backdrop: true,
                            resolveData: opts
                        });
                    }
                };
            }
        ])
        .controller("ActivateAccountModalController", [
            "data",
            "$modalInstance",
            "PaymentModal",
            ActivateAccountModalController
        ]);

    function ActivateAccountModalController(opts, $modal, PaymentModal) {
        this.customer = opts.customer;
        this.closed = opts.closed;
        this.account_id = opts.account_id;
        this.$modal = $modal;
        this.PaymentModal = PaymentModal;
        this.step = "one";
        this.cost = 4900;
        this.count = 50;

        if (this.customer && this.customer.role === "broker") {
            this.cost = 49;
            this.plan = "49permonthagent";
            this.step = "two";
        }
    }

    ActivateAccountModalController.prototype = {

        next: function() {
            this.step = "two";
            this.calculate();
        },

        calculate: function() {
            if (this.count < 50) {
                this.plan = "16newinspectorsubscription";
                this.cost = 16;
            }
            else if (this.count >= 50 && this.count < 200) {
                this.plan = "49permonth";
                this.cost = 49;
            }
            else if (this.count >= 200 && this.count <= 300) {
                this.plan = "54permonth";
                this.cost = 54;
            }
            else if (this.count >= 301) {
                this.plan = this.customer.name + " Subscription";
                this.cost = Math.round((this.count * 2.35) / 12);
            }
        },

        pay: function() {
            this.cancel();
            this.PaymentModal.show({ customer: this.customer, plan: this.plan, cost: this.cost, account_id: this.account_id, closed: angular.bind(this, this.closed) });
        },

        cancel: function() {
            this.$modal.dismiss("cancel");
        }
    };
})();