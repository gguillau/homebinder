(function() {
    "use strict";

    angular
        .module("hb.components.partners.dashboards", [])
        .directive("partnersEditFormDashboards", function() {
            return {
                restrict: "E",
                templateUrl: "dashboards/dashboards.tpl.html",
                controller: "HBPartnerDashboardsController",
                controllerAs: "ctrl",
                bindToController: true
            };
        });
})();