describe('partnersEditFormDashboards', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;

        spyOn(api.dashboard, "all").and.returnValue($q.when({ data: { items: [] } }));
        
        $scope = $rootScope.$new(), $compile = _$compile_;

        $compile('<partners-edit-form-dashboards></partners-edit-form-dashboards>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the partners-edit-form-dashboards directive', function() {
        it('calls api dashboard all', function() {
            expect(api.dashboard.all).toHaveBeenCalled();
        });
    });
});