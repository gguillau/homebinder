describe("PartnerCustomWidgetController", function() {
    var $controllerService,
        $q,
        ctrl,
        EditBase,
        resources,
        api,
        loading,
        notify,
        $log,
        context,
        modal;

    // load the homebinder module
    beforeEach(module("homebinder"));
    
    // create the controller
    beforeEach(inject(function($controller, $injector) {
        $controllerService = $controller;
        $q = $injector.get("$q");
        EditBase = $injector.get("hb.framework.editBase");
        resources = $injector.get("hb.resources");
        api = $injector.get("hb.api");
        loading = $injector.get("Loading");
        notify = $injector.get("Notify");
        $log = $injector.get("$log");
        context = $injector.get("Context");
        modal = $injector.get("ModalService");
    }));
    
    // create the controller
    function createController() {
        ctrl = $controllerService("PartnerCustomWidgetController", {
            "hb.framework.editBase": EditBase,
            "hb.resources": resources,
            "hb.api": api,
            "Loading": loading,
            "Notify": notify,
            "$log": $log,
            "Context": context,
            "ModalService": modal
        });
        
        var partnerWidget = {
            id: 1,
            partner_id: 1,
            widget_id: 1,
            configuration: {}
        };
        spyOn(api.partnerWidget, "all").and.returnValue($q.when([partnerWidget]));
    }
    
    describe("refresh", function() {
        it("calls loading.show and api.partnerWidget.all", function() {
            createController();
            spyOn(loading, "show");
            
            ctrl.model.partner = {id: 1};
            ctrl.model.refresh();

            expect(ctrl.model.loading.show).toHaveBeenCalled();
            expect(api.partnerWidget.all).toHaveBeenCalledWith({ partnerId: 1 });
        });
    });
    
    describe("onRefreshSuccess", function() {
        it("sets configuration to default when response.data.items.length is 0", function() {
            createController();
            spyOn(loading, "close");
            
            var response = {data: {items: []}};
            ctrl.model.onRefreshSuccess(response);

            expect(ctrl.model.item).toEqual(null);
            expect(ctrl.model.configuration).toEqual(ctrl.model.defaultConfiguration);
            expect(ctrl.model.widgetIsLive).toEqual(false);
            expect(ctrl.model.originalConfiguration).toEqual(ctrl.model.configuration);
            expect(ctrl.model.loading.close).toHaveBeenCalled();
        });
        
        it("sets configuration when response.data.items.length greater than 0", function() {
            createController();
            spyOn(loading, "close");
            
            var configuration = JSON.stringify({
                title: "title",
                body: "body",
                buttonLabel: "button",
                url: "url"
            });
            var response = {data: {items: [{configuration: configuration}]}};
            ctrl.model.onRefreshSuccess(response);

            expect(ctrl.model.item).toEqual({configuration: configuration});
            expect(ctrl.model.configuration).toEqual(JSON.parse(configuration));
            expect(ctrl.model.widgetIsLive).toEqual(true);
            expect(ctrl.model.originalConfiguration).toEqual(ctrl.model.configuration);
            expect(ctrl.model.loading.close).toHaveBeenCalled();
        });
    });
    
    describe("goLive", function() {
        it("opens the confirmation modal", function() {
            createController();
            spyOn(modal, "confirm");
            
            ctrl.model.goLive();

            expect(modal.confirm).toHaveBeenCalled();
        });
    });
    
    describe("createWidget", function() {
        it("calls api.widget.all", function() {
            createController();
            spyOn(loading, "show");
            spyOn(api.widget, "all").and.returnValue($q.when([]));
            
            ctrl.model.createWidget();

            expect(loading.show).toHaveBeenCalled();
            expect(ctrl.model.settings.readOnly).toEqual(true);
            expect(api.widget.all).toHaveBeenCalled();
        });
    });
    
    describe("getCustomWidgetSuccess", function() {
        it("throws an error when response length is 0", function() {
            createController();
            spyOn(ctrl.model, "onError");
            
            var response = {data: {items: []}};
            ctrl.model.getCustomWidgetSuccess(response);

            expect(ctrl.model.onError).toHaveBeenCalledWith({ data: "Widget with key 'customWidget' does not exist" });
        });
        
        it("calls api.partnerWidget.create when response length is greater than 0", function() {
            createController();
            spyOn(api.partnerWidget, "create").and.returnValue($q.when({}));
            
            var response = {data: {items: [{id: 1}]}};
            var configuration = {
                title: "title",
                body: "body",
                buttonLabel: "button",
                url: "url"
            };
            var partnerWidget = {
                partner_id: 1,
                widget_id: 1,
                configuration: JSON.stringify(configuration)
            };
            ctrl.model.configuration = configuration;
            ctrl.model.partner = {id: 1};
            ctrl.model.getCustomWidgetSuccess(response);

            expect(api.partnerWidget.create).toHaveBeenCalledWith(partnerWidget);
        });
    });
    
    describe("createPartnerWidgetSuccess", function() {
        it("calls loading.close, notify.success, and refresh", function() {
            createController();
            spyOn(loading, "close");
            spyOn(notify, "success");
            spyOn(ctrl.model, "refresh");
            
            ctrl.model.createPartnerWidgetSuccess();

            expect(loading.close).toHaveBeenCalled();
            expect(notify.success).toHaveBeenCalled();
            expect(ctrl.model.refresh).toHaveBeenCalled();
        });
    });
    
    describe("beforeSave", function() {
        it("sets the updatedItem", function() {
            createController();
            
            var configuration = {
                title: "title",
                body: "body",
                buttonLabel: "button",
                url: "url"
            };
            ctrl.model.configuration = configuration;
            ctrl.model.beforeSave();

            expect(ctrl.model.updatedItem).toEqual({configuration: JSON.stringify(configuration)});
        });
    });

    describe("onDeleteClick", function() {
        it("opens the confirmation modal", function() {
            createController();
            spyOn(modal, "confirm");
            
            ctrl.model.onDeleteClick();

            expect(modal.confirm).toHaveBeenCalled();
        });
    });
    
    describe("deletePartnerWidget", function() {
        it("calls loading.show and api.partnerWidget.destroy", function() {
            createController();
            spyOn(loading, "show");
            spyOn(api.partnerWidget, "destroy").and.returnValue($q.when([]));
            
            ctrl.model.deletePartnerWidget();

            expect(loading.show).toHaveBeenCalled();
            expect(api.partnerWidget.destroy).toHaveBeenCalled();
        });
    });
    
    describe("destroyPartnerWidgetSuccess", function() {
        it("sets all settings and configuration to default", function() {
            createController();
            spyOn(notify, "success");
            spyOn(loading, "close");
            
            ctrl.model.destroyPartnerWidgetSuccess();

            expect(notify.success).toHaveBeenCalled();
            expect(ctrl.model.item).toEqual(null);
            expect(ctrl.model.configuration).toEqual(ctrl.model.defaultConfiguration);
            expect(ctrl.model.widgetIsLive).toEqual(false);
            expect(ctrl.model.settings.readOnly).toEqual(true);
            expect(loading.close).toHaveBeenCalled();
        });
    });
    
    describe("cancel", function() {
        it("sets the configuration to the original configuration and cancels the form", function() {
            createController();
            spyOn(ctrl.model, "cancelForm");
            
            ctrl.model.originalConfiguration = {title: "original"};
            ctrl.model.cancel();
            
            expect(ctrl.model.configuration).toEqual({title: "original"});
            expect(ctrl.model.cancelForm).toHaveBeenCalled();
        });
    });
    
    describe("onError", function() {
        it("calls $log.error, notify.error, loading.close, and sets error to true", function() {
            createController();
            spyOn($log, "error");
            spyOn(notify, "error");
            spyOn(loading, "close");

            ctrl.model.onError({data: "error"});
            
            expect($log.error).toHaveBeenCalledWith({data: "error"});
            expect(notify.error).toHaveBeenCalledWith("error");
            expect(loading.close).toHaveBeenCalled();
            expect(ctrl.model.error).toEqual(true);
        });
    });

});

describe('partnersEditFormCustomWidget', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        element = $compile('<partners-edit-form-custom-widget></partners-edit-form-custom-widget>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the partnersEditFormCustomWidget directive', function() {
        it('shows the the partnersEditFormCustomWidget template', function() {
            expect(element.html()).toContain("Widget Headline");
        });
    });
});