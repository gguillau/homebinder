(function() {
    "use strict";

    angular
        .module("hb.components.partners.customWidget", [])
        .directive("partnersEditFormCustomWidget", function() {
            return {
                restrict: "E",
                templateUrl: "components/partners/edit/customWidget/customWidget.tpl.html",
                controller: "PartnerCustomWidgetController",
                controllerAs: "ctrl",
                bindToController: true
            };
        })
        .controller("PartnerCustomWidgetController", [
            "hb.framework.editBase",
            "hb.resources",
            "hb.api",
            "Loading",
            "Notify",
            "$log",
            "Context",
            "ModalService",
            PartnerCustomWidgetController
        ]);

    function PartnerCustomWidgetController(EditBase, resources, api, loading, notify, $log, context, ModalService) {

        var Model = function() {
            EditBase.call(this);
            this.resources = angular.extend({}, this.resources, resources.partnersEdit);
            this.resources = angular.extend({}, this.resources, resources.partnersCustomWidget);
            this.api = api;
            this.loading = loading;
            this.notify = notify;
            this.$log = $log;
            this.modal = ModalService;
            // set the save API call
            this.saveCall = api.partnerWidget.update;
            this.error = false;
            this.defaultConfiguration = {
                title: "Title",
                body: "Description",
                buttonLabel: "Button",
                url: "https://www.homebinder.com"
            };

            context.getPartner().then(angular.bind(this, function(partner) {
                this.partner = partner;
                this.refresh();
            }));
        };

        Model.prototype = Object.create(EditBase.prototype);

        angular.extend(Model.prototype, {
            refresh: function() {
                this.loading.show(this.resources.loading);
                this.api.partnerWidget.all({ partnerId: this.partner.id }).then(
                    angular.bind(this, this.onRefreshSuccess),
                    angular.bind(this, this.onError));
            },

            onRefreshSuccess: function(response) {
                if (response.data.items.length > 0) {
                    this.item = response.data.items[0];
                    this.configuration = JSON.parse(this.item.configuration);
                    this.widgetIsLive = true;
                }
                else {
                        this.item = null;
                        this.configuration = this.defaultConfiguration,
                        this.widgetIsLive = false;
                }
                this.originalConfiguration = {
                    title: this.configuration.title,
                    body: this.configuration.body,
                    buttonLabel: this.configuration.buttonLabel,
                    url: this.configuration.url
                };
                this.loading.close();
            },
            
            goLive: function() {
                this.modal.confirm({
                    title: this.resources.goLiveTitle,
                    message: this.resources.goLiveMessage,
                    glyphicon: "glyphicon glyphicon-question-sign",
                    confirm: angular.bind(this, this.createWidget)
                });
            },

            createWidget: function() {
                this.loading.show(this.resources.loading);
                this.settings.readOnly = true;
                this.api.widget.all({ key: "customWidget" }).then(
                    angular.bind(this, this.getCustomWidgetSuccess),
                    angular.bind(this, this.onError));
            },

            getCustomWidgetSuccess: function(response) {
                if (response.data.items.length > 0) {
                    var customWidget = response.data.items[0];
                    var partnerWidget = {
                        partner_id: this.partner.id,
                        widget_id: customWidget.id,
                        configuration: JSON.stringify(this.configuration)
                    };
                    this.api.partnerWidget.create(partnerWidget).then(
                        angular.bind(this, this.createPartnerWidgetSuccess),
                        angular.bind(this, this.onError));
                }
                else {
                    this.onError({ data: "Widget with key 'customWidget' does not exist" });
                }
            },

            createPartnerWidgetSuccess: function(response) {
                this.loading.close();
                this.notify.success(this.resources.widgetCreateSuccess);
                this.refresh();
            },

            beforeSave: function() {
                var stringConfiguration = JSON.stringify(this.configuration);
                this.updatedItem = { configuration: stringConfiguration };
            },

            onDeleteClick: function() {
                this.modal.confirm({
                    title: this.resources.confirmDeleteTitle,
                    message: this.resources.confirmDeleteMessage,
                    glyphicon: "glyphicon glyphicon-question-sign",
                    confirm: angular.bind(this, this.deletePartnerWidget)
                });
            },

            deletePartnerWidget: function() {
                this.loading.show(this.resources.loading);
                this.api.partnerWidget.destroy(this.item.id).then(
                    angular.bind(this, this.destroyPartnerWidgetSuccess),
                    angular.bind(this, this.onError));
            },
            
            destroyPartnerWidgetSuccess: function(response) {
                this.notify.success(this.resources.widgetDestroySuccess);
                this.item = null;
                this.configuration = this.defaultConfiguration,
                this.widgetIsLive = false;
                this.settings.readOnly = true;
                this.loading.close();
            },

            cancel: function() {
                this.configuration = this.originalConfiguration;
                this.cancelForm();
            },

            onError: function(response) {
                this.$log.error(response);
                this.notify.error(response.data);
                this.loading.close();
                this.error = true;
            }

        });

        this.model = new Model();

    }
})();