(function() {
    "use strict";

    angular
        .module("hb.components.partners.branding.agent", [
        ])
        .directive("agentBranding", function() {
            return {
                restrict: "E",
                templateUrl: "components/partners/edit/branding/agent/agent.tpl.html"
            };
        });
})();