describe('agentBranding', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        
        $scope.ctrl = {
            model: {
                item: {
                    configuration: {
                        default_user_branding_option: "agent"
                    }
                }
            }
        };
        element = $compile('<agent-branding></agent-branding>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the agentBranding directive', function() {
        it('shows the the agentBranding template', function() {
            expect(element.html()).toContain("Agent First and Last Name");
        });
    });
});
