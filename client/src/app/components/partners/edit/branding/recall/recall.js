(function() {
    "use strict";

    angular
        .module("hb.components.partners.branding.recall", [])
        .directive("recallEmail", function() {
            return {
                restrict: "E",
                templateUrl: "components/partners/edit/branding/recall/recall.tpl.html"
            };
        });
})();