describe('recallEmail', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        
        $scope.ctrl = {
            model: {
                item: {
                    partner_type: "inspector",
                    configuration: {
                        default_user_branding_option: "user"
                    }
                }
            }
        };
        element = $compile('<recall-email></recall-email>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the recallEmail directive', function() {
        it('shows the the recallEmail template', function() {
            expect(element.html()).toContain("A potential recall has been identified");
        });
    });
});