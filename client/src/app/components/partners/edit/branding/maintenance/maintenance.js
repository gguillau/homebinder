(function() {
    "use strict";

    angular
        .module("hb.components.partners.branding.maintenance", [])
        .directive("maintenanceEmail", function() {
            return {
                restrict: "E",
                templateUrl: "components/partners/edit/branding/maintenance/maintenance.tpl.html"
            };
        });
})();