describe('maintenanceEmail', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        element = $compile('<maintenance-email></maintenance-email>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the maintenanceEmail directive', function() {
        it('shows the the maintenanceEmail template', function() {
            expect(element.html()).toContain("Filter Service Due");
        });
    });
});