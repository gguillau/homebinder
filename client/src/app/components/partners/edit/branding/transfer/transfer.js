(function() {
    "use strict";

    angular
        .module("hb.components.partners.branding.transfer", [])
        .directive("transferEmail", function() {
            return {
                restrict: "E",
                templateUrl: "components/partners/edit/branding/transfer/transfer.tpl.html"
            };
        });
})();