describe('transferEmail', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        
        $scope.ctrl = {
            model: {
                item: {
                    partner_type: "inspector",
                    configuration: {
                        default_user_branding_option: "user"
                    }
                }
            }
        };
        element = $compile('<transfer-email></transfer-email>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the transferEmail directive', function() {
        it('shows the the transferEmail template', function() {
            expect(element.html()).toContain("HomeBinder is an online app");
        });
    });
});