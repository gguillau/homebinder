describe('hbBranding', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        
        $scope.ctrl = {
            model: {
                item: {
                    configuration: {
                        default_user_branding_option: "none"
                    }
                },
                email_type: "maintenance"
            }
        };
        element = $compile('<hb-branding></hb-branding>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the hbBranding directive', function() {
        it('shows the the hbBranding template', function() {
            expect(element.html()).toContain("The HomeBinder Team");
        });
    });
});