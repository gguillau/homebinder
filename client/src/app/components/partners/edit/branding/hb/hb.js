(function() {
    "use strict";

    angular
        .module("hb.components.partners.branding.hb", [
        ])
        .directive("hbBranding", function() {
            return {
                restrict: "E",
                templateUrl: "components/partners/edit/branding/hb/hb.tpl.html"
            };
        });
})();