describe('agentTransferEmail', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        
        $scope.ctrl = {
            model: {
                item: {
                    configuration: {
                        default_user_branding_option: "agent"
                    }
                }
            }
        };
        element = $compile('<agent-transfer-email></agent-transfer-email>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the agentTransferEmail directive', function() {
        it('shows the the agentTransferEmail template', function() {
            expect(element.html()).toContain("Hello Agent!");
        });
    });
});