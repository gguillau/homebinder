(function() {
    "use strict";

    angular
        .module("hb.components.partners.branding.agentTransfer", [
        ])
        .directive("agentTransferEmail", function() {
            return {
                restrict: "E",
                templateUrl: "components/partners/edit/branding/agentTransfer/agentTransfer.tpl.html"
            };
        });
})();