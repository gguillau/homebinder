(function() {
    "use strict";

    angular
        .module("hb.components.partners.branding", [
            "hb.components.partners.branding.maintenance",
            "hb.components.partners.branding.transfer",
            "hb.components.partners.branding.recall",
            "hb.components.partners.branding.user",
            "hb.components.partners.branding.agent",
            "hb.components.partners.branding.agentTransfer",
            "hb.components.partners.branding.hb",
            "hb.components.partners.branding.info"
        ])
        .directive("partnersEditFormBranding", function() {
            return {
                restrict: "E",
                templateUrl: "components/partners/edit/branding/branding.tpl.html",
                controller: "PartnerAccountController",
                controllerAs: "ctrl",
                bindToController: true
            };
        });
})();