describe('userBranding', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        $scope.ctrl = {
            model: {
                branding_user: {
                    user_profile_attributes: {
                        first_name: "Test",
                        last_name: "Name"
                    }
                },
                item: {
                    partner_type: "inspector",
                    configuration: {
                        default_user_branding_option: "user"
                    }
                }
            }
        };
        element = $compile('<user-branding></user-branding>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the userBranding directive', function() {
        it('shows the the userBranding template', function() {
            expect(element.html()).toContain("Test");
        });
    });
});