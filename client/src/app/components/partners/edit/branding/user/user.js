(function() {
    "use strict";

    angular
        .module("hb.components.partners.branding.user", [
        ])
        .directive("userBranding", function() {
            return {
                restrict: "E",
                templateUrl: "components/partners/edit/branding/user/user.tpl.html"
            };
        });
})();