(function() {
    "use strict";

    angular
        .module("hb.components.partners.branding.info", [])
        .factory("BrandingInfo", [
            "ModalService",
            function(Modals) {
                return {
                    show: function(opts) {
                        Modals.show({
                            templateUrl: "components/partners/edit/branding/info/info.tpl.html",
                            controller: "BrandingInfoController as ctrl",
                            backdrop: true,
                            resolveData: opts
                        });
                    }
                };
            }
        ])
        .controller("BrandingInfoController", [
            "$modalInstance",
            "data",
            BrandingInfoController
        ]);

    function BrandingInfoController($modal, opts) {
        this.brandingOptions = opts.brandingOptions;
        this.$modal = $modal;
    }

    BrandingInfoController.prototype = {

        cancel: function() {
            this.$modal.close();
        }
    };
})();