describe("BrandingInfoController", function() {
    var $rootScope,
        $controller,
        ctrl,
        $modalInstance,
        BrandingInfo,
        ModalService;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        BrandingInfo = _$injector_.get("BrandingInfo");
        ModalService = _$injector_.get("ModalService");

        $modalInstance = {
            close: function() {}
        };
    }));

    function createController() {
        ctrl = $controller("BrandingInfoController", {
            "$modalInstance": $modalInstance,
            "data": {}
        });
        $rootScope.$apply();
    }

    describe('cancel', function() {
        it("calls close", function() {
            spyOn($modalInstance, "close");

            createController();
            ctrl.cancel();

            expect($modalInstance.close).toHaveBeenCalled();
        });
    });

    describe('BrandingInfo', function() {
        it('should call show and then $modal open', function() {
            spyOn(ModalService, "show");

            BrandingInfo.show({});

            expect(ModalService.show).toHaveBeenCalled();
        });
    });

});