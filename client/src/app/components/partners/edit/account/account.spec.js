describe("PartnerAccountController", function() {
    var controller,
        base,
        api,
        resources,
        uploader,
        $q,
        modal;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
        // mock the context service
        $provide.factory("Context", function() {
            return {
                setPartner: function(item) {

                },
                getNavList: function() {
                    return { navList: [] };
                },
                setNavList: function() {}
            };
        });
    }));

    // create the controller
    beforeEach(inject(function($controller, $injector, $rootScope) {
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");
        $q = $injector.get("$q");
        modal = $injector.get("ModalService");

        spyOn(api.partner, "get").and.returnValue($q.when({
            data: {
                id: 1,
                address: {},
                configuration: {

                }
            }
        }));

        spyOn(api.user, "all").and.returnValue($q.when({
            data: {
                items: []
            }
        }));

        controller = $controller("PartnerAccountController", {
            "hb.framework.indexBase": base,
            "hb.api": api,
            "hb.resources": resources
        });

        $rootScope.$apply();
    }));

    uploader = {
        pendingUploads: function() {}
    };

    describe("init", function() {
        it("sets canEditKey to true when admin", function() {
            expect(controller.model.settings.canEditKey).toEqual(true);
        });

        it("sets canEditKey to false when not admin", function() {
            expect(controller.model.settings.canEditKey).toEqual(true);

            controller.model.currentUser.role = "inspector";

            controller.model.init();

            expect(controller.model.settings.canEditKey).toEqual(false);
        });
    });

    describe("onRefreshSuccess", function() {
        it("sets the partner", inject(function($location) {

            var model = controller.model;
            spyOn(model, "setSelfServeLink");
            spyOn(model, "setAprLink");
            spyOn(model, "getUsers");

            var item = {
                partner_type: "inspector",
                address: {},
                configuration: {
                    show_isn_tab: true
                }
            };
            model.onRefreshSuccess({ data: item });

            expect(model.setSelfServeLink).toHaveBeenCalled();

        }));

    });

    describe("getUsers", function() {
        it("calls user all", inject(function($location) {

            var model = controller.model;
            model.item = {
                partner_type: "inspector"
            };
            model.getUsers();

            expect(api.user.all).toHaveBeenCalled();

        }));
    });

    describe("onUserSuccess", function() {
        it("sets the users and the branding user", inject(function($location) {

            var model = controller.model;
            var users = [
                { id: 1, user_profile_attributes: { id: 1, first_name: "Bob" } },
                { id: 2, user_profile_attributes: { id: 2 } }
            ];
            model.item = {
                configuration: {
                    default_user_branding_id: 1
                }
            };
            model.onUserSuccess({ data: { items: users } });

            expect(model.branding_user).toEqual(users[0]);

        }));
    });

    describe("onError", function() {
        it("calls notify error", inject(function($location) {

            var model = controller.model;
            spyOn(model.notify, "error");

            model.onError({ data: "error" });

            expect(model.notify.error).toHaveBeenCalled();

        }));
    });

    describe("setSelfServeLink", function() {
        it("sets the self serve link", inject(function($location) {

            spyOn($location, "protocol").and.returnValue("https");
            spyOn($location, "host").and.returnValue("www.homebinder.com");

            var model = controller.model;
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                partner_key: "test"
            };
            model.setSelfServeLink();

            expect(model.selfServeLink).toEqual("https://www.homebinder.com/partners/test/homeowners");

        }));

    });

    describe("setAprLink", function() {
        it("sets the apr link", inject(function($location) {

            spyOn($location, "protocol").and.returnValue("https");
            spyOn($location, "host").and.returnValue("www.homebinder.com");

            var model = controller.model;
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                partner_key: "test"
            };
            model.setAprLink();

            expect(model.aprLink).toEqual("https://www.homebinder.com/partners/test/apr");

        }));

    });

    describe("beforeSave", function() {
        it("sets the updatedItem", inject(function($location) {

            var model = controller.model;

            model.item.name = "Test Partner";
            model.item.phone = {};
            model.beforeSave();

            expect(model.updatedItem.name).toEqual("Test Partner");

        }));
    });

    describe("onSaveSuccess", function() {
        it("calls updateBrandingUser", inject(function(Context) {

            var model = controller.model;
            spyOn(model, "updateBrandingUser");
            spyOn(Context, "setNavList");

            model.onSaveSuccess({ data: { can_perform_apr: true } });

            expect(model.updateBrandingUser).toHaveBeenCalled();
            expect(Context.setNavList).toHaveBeenCalled();

        }));
    });

    describe("updateBrandingUser", function() {
        it("calls user update", inject(function($location) {

            var model = controller.model;

            spyOn(api.user, "update").and.returnValue($q.when({ data: {} }));

            model.branding_user = {
                id: 1,
                user_profile_attributes: {
                    id: 1,
                    message: "Message"
                }
            };
            model.updateBrandingUser();

            expect(api.user.update).toHaveBeenCalled();

        }));

        it("calls cancelForm", inject(function($location) {
            var model = controller.model;

            spyOn(model, "cancelForm");

            model.branding_user = null;
            model.updateBrandingUser();

            expect(model.cancelForm).toHaveBeenCalled();

        }));
    });

    describe("updateUserSuccess", function() {
        it("calls cancelForm", inject(function($location) {
            var model = controller.model;

            spyOn(model, "cancelForm");

            model.updateUserSuccess();

            expect(model.cancelForm).toHaveBeenCalled();

        }));
    });

    describe("onConfirmAutoTransfer", function() {
        it("sets automation_binder_action to true", inject(function($rootScope) {
            var model = controller.model;
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                configuration: {
                    automation_binder_action: false
                }
            };

            expect(model.item.configuration.automation_binder_action).toBe(false);

            model.onConfirmAutoTransfer();

            expect(model.item.configuration.automation_binder_action).toBe(true);
        }));

    });

    describe("onCancelAutoTransfer", function() {
        it("sets automation_binder_action to false", inject(function($rootScope) {
            var model = controller.model;
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                configuration: {
                    automation_binder_action: true
                }
            };

            expect(model.item.configuration.automation_binder_action).toBe(true);

            model.onCancelAutoTransfer();

            expect(model.item.configuration.automation_binder_action).toBe(false);
        }));

    });

    describe("setLogo", function() {
        it("sets the logo", inject(function($rootScope) {
            var model = controller.model;
            model.imgUploadCfg.api = uploader;
            var pending = {
                window_url: "url",
                based64: "64",
                name: "name"
            };
            spyOn(uploader, "pendingUploads").and.returnValue([pending]);
            spyOn(model, "onSave");
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                logo: "",
                logo_file: "",
                logo_file_name: ""
            };
            model.setLogo();

            expect(model.onSave).toHaveBeenCalled();
            expect(model.item.logo).toBe("64");
            expect(model.item.logo_file).toBe("url");
            expect(model.item.logo_file_name).toBe("name");
        }));

    });

    describe("generateApiKey", function() {
        it("calls apikey create", inject(function($location) {

            var model = controller.model;

            spyOn(api.apiKey, "create").and.returnValue($q.when({ data: {} }));

            model.generateApiKey();

            expect(api.apiKey.create).toHaveBeenCalled();

        }));

    });

    describe("keyCreateSuccess", function() {
        it("sets the api key", inject(function($location) {
            var model = controller.model;

            model.keyCreateSuccess({ data: { key: "test" } });

            expect(model.item.api_key).toEqual("test");

        }));
    });

    describe("transferConfirm", function() {
        it("calls modal", inject(function($location) {
            var model = controller.model;
            spyOn(modal, "confirm");

            model.item = {
                configuration: {
                    automation_binder_action: true
                }
            };
            model.transferConfirm();

            expect(modal.confirm).toHaveBeenCalled();

        }));
    });

    describe("showBrandingInfo", function() {
        it("calls the show function", inject(function(BrandingInfo) {
            spyOn(BrandingInfo, "show");
            var model = controller.model;
            model.currentUser = {
                role: "admin"
            };
            model.showBrandingInfo();

            expect(BrandingInfo.show).toHaveBeenCalled();
        }));

    });

    describe("onSelectCountry", function() {
        it("sets the country", inject(function(BrandingInfo) {
            spyOn(BrandingInfo, "show");
            var model = controller.model;
            model.onSelectCountry({ value: "MA" });

            expect(model.country.value).toEqual("MA");
        }));

    });

    describe("addIsn", function() {
        it("adds isn tab to navigation links", inject(function($location) {
            var model = controller.model;
            controller.links = [{ links: [{ value: "apr" }] }]; //[0].links

            expect(controller.links[0].links.length).toEqual(1);
            model.addIsn();

            expect(controller.links[0].links.length).toEqual(2);
        }));

        it("does not add isn tab to navigation links", inject(function($location) {
            var model = controller.model;
            controller.links = [{ links: [{ value: "apr" }, { value: "isn" }] }]; //[0].links

            expect(controller.links[0].links.length).toEqual(2);
            model.addIsn();

            expect(controller.links[0].links.length).toEqual(2);
        }));
    });

});

describe('partnersEditFormAccount', function() {
    var $scope, $compile, api, session, $stateParams, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        session = _$injector_.get("Session");
        $stateParams = _$injector_.get("$stateParams");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        $stateParams.partnerId = 1;
        spyOn(session, "getUser").and.returnValue({ id: 1, role: "admin" });
        spyOn(api.partner, "get").and.returnValue($q.when({ data: { id: 1, address: {}, warranties_configuration: {}, configuration: {} } }));
        spyOn(api.user, "all").and.returnValue($q.when({ data: { items: [] } }));

        $compile('<partners-edit-form-account links="links"></partners-edit-form-account>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls api partner get', function() {
            expect(api.partner.get).toHaveBeenCalled();
        });
    });
});