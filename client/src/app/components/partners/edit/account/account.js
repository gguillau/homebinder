(function() {
    "use strict";

    angular
        .module("hb.components.partners.account", [])
        .directive("partnersEditFormAccount", function() {
            return {
                restrict: "E",
                templateUrl: "components/partners/edit/account/account.tpl.html",
                controller: "PartnerAccountController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    links: "="
                }
            };
        })
        .controller("PartnerAccountController", [
            "hb.framework.editBase",
            "hb.resources",
            "$stateParams",
            "hb.api",
            "$rootScope",
            "Context",
            "$filter",
            "Loading",
            "$location",
            "Session",
            "ModalService",
            "BrandingInfo",
            "Address",
            PartnerAccountController
        ]);

    function PartnerAccountController(EditBase, resources, $stateParams, api, $rootScope, context, $filter, loading, $location, session, modals, BrandingInfo, Address) {
        var that = this;
        var Model = function() {
            // call the parent class
            EditBase.call(this);
            // apply widget specific resource strings
            this.resources = angular.extend({}, this.resources, resources.partnersEdit);
            this.resources = angular.extend({}, this.resources, resources.partnersAutomation);
            this.resources = angular.extend({}, this.resources, resources.partnersApr);
            this.resources = angular.extend({}, this.resources, resources.partnersBranding);
            this.resources = angular.extend({}, this.resources, resources.partnersAccount);
            this.itemId = $stateParams.partnerId;
            // set the refresh API call
            this.refreshCall = api.partner.get;
            // set the save API call
            this.saveCall = api.partner.update;
            this.address = Address;
            this.states = Address.getStatesAndProvinces();
            this.countries = Address.countries();
            this.localeOptions = [{
                name: "English",
                value: "en-us"
            }, {
                name: "French",
                value: "fr-fr"
            }, {
                name: "Spanish",
                value: "sp-sp"
            }];
            this.transfer_options = [{
                name: "Transfer",
                value: "transfer"
            }, {
                name: "Share",
                value: "share"
            }];
            this.default_transaction_types = [{
                    name: "Buy Side",
                    value: "buy_side"
                },
                {
                    name: "Sell Side",
                    value: "sell_side"
                }
            ];
            this.branding_options = [{
                name: "None",
                value: "none",
                description: "No branding will show up in the binder overview page nor on any maintenance/recall emails."
            }];
            this.email_types = [{
                name: "Maintenance Email",
                value: "maintenance"
            }, {
                name: "Recall Email",
                value: "recall"
            }, {
                name: "Transfer Email",
                value: "transfer"
            }, {
                name: "Share Email",
                value: "share"
            }];
            this.email_type = this.email_types[0].value;
            this.imgUploadCfg = {
                fileTypes: "image",
                hideUploadButton: true,
                showTable: false,
                selectButtonLabel: this.resources.logoText,
                fileLimit: true,
                multiSelect: false,
                hide_upload: false
            };
            this.partner_type_options = [{
                name: "Agent/Broker",
                value: "broker"
            }, {
                name: "Home Inspector",
                value: "inspector"
            }, {
                name: "Lender",
                value: "lender"
            }, {
                name: "Home Professional",
                value: "homepro"
            }, {
                name: "Home Builder",
                value: "builder"
            }, {
                name: "Property Manager",
                value: "property_manager"
            }];
            this.agent_options = [{
                    name: "All binders",
                    value: "always"
                },
                {
                    name: "Only the first binder",
                    value: "once"
                },
                {
                    name: "Never",
                    value: "never"
                }
            ];
            this.report_type_options = [{
                    name: "Standard",
                    value: "f"
                },
                {
                    name: "New",
                    value: "F"
                },
                {
                    name: "Amerispec Branded",
                    value: "SA"
                }
            ];
            this.url_regex = /^((?:http|ftp)s?:\/\/)(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|localhost|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(?::\d+)?(?:\/?|[\/?]\S+)$/i;
            this.currentUser = session.getUser();
            this.init();
            // refresh the item
            this.refresh();
        };

        Model.prototype = Object.create(EditBase.prototype);

        angular.extend(Model.prototype, {

            init: function() {
                if (this.currentUser.role === "admin") {
                    // admins can edit the partner key
                    this.settings.canEditKey = true;
                }
                else {
                    // partners cannot edit the partner key
                    this.settings.canEditKey = false;
                }
            },

            // Refresh callback. Updates the item list and totals
            onRefreshSuccess: function(response) {
                this.item = response.data;
                context.setPartner(this.item);
                this.setSelfServeLink();
                this.setAprLink();

                // add email type
                if (this.item.partner_type === "inspector") {
                    this.email_types.push({
                        name: "Agent Transfer Notification Email",
                        value: "agent_transfer_notification"
                    });
                    
                    if (this.item.configuration.repair_pricer_pre_paid_reports) {
                        this.email_types.push({
                            name: "Agent Transfer Notification Email with Prepaid Repair Pricer Enabled",
                            value: "agent_prepaid_rp_transfer_notification"
                        });
                    }
                }

                if (this.item.configuration.show_isn_tab === true) {
                    this.addIsn();
                }

                // add additonal options for inspectors
                var partner_type = $filter("titleize")(this.item.partner_type);
                if (this.item.partner_type === "inspector" || this.item.partner_type === "lender" || this.item.partner_type === "builder" || this.item.partner_type === "broker") {
                    this.branding_options = this.branding_options.concat([{
                        name: "Agent",
                        value: "agent",
                        description: "Only the buyer agent's branding will show up in the binder overview page and any recall/maintenance emails."
                    }, {
                        name: "Both " + partner_type + " and agent",
                        value: "co_brand",
                        description: "Both the " + partner_type + "'s branding and agent's branding will show up in the binder overview page and any recall/maintenance emails."
                    }]);
                }
                this.branding_options.push({
                    name: partner_type,
                    value: "user",
                    description: "The " + partner_type + "'s branding will show up in the binder overview page and any recall/maintenance emails."
                });
                this.getUsers();
                this.country = Address.findCountryByCode(this.item.address.country);
                this.states = Address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                    return state.country.value === this.item.address.country;
                }));
                loading.close();
            },

            getUsers: function() {
                // set up the opts
                var opts = {
                    page: 1,
                    count: 20,
                    role: this.item.partner_type,
                    partnerId: this.itemId,
                    searchMethod: "for_partner"
                };

                //  get the users
                api.user.all(opts).then(
                    angular.bind(this, this.onUserSuccess),
                    angular.bind(this, this.onError));
            },

            onUserSuccess: function(response) {
                this.users = response.data.items;

                this.users.forEach(angular.bind(this, function(user) {
                    if (user.user_profile_attributes.id === this.item.configuration.default_user_branding_id) {
                        this.branding_user = user;
                    }
                }));
            },

            setSelfServeLink: function() {
                var url = $location.protocol() + '://' + $location.host();
                this.selfServeLink = this.item.partner_key ? url + "/partners/" + this.item.partner_key + "/homeowners" : null;
            },

            setAprLink: function() {
                var url = $location.protocol() + '://' + $location.host();
                this.aprLink = this.item.partner_key ? url + "/partners/" + this.item.partner_key + "/apr" : null;
            },

            beforeSave: function() {
                var configuration = angular.extend({}, this.item.configuration);
                configuration.annual_property_review_cost_cents = this.item.configuration.annual_property_review_cost * 100;
                delete configuration.annual_property_review_cost;
                delete configuration.binder_templates;

                this.updatedItem = {
                    name: this.item.name,
                    contact: this.item.contact,
                    phone: "+" + this.country.code + this.item.phone.national,
                    email: this.item.email,
                    website: this.item.website,
                    email_display_name: this.item.email_display_name,
                    partner_key: this.item.partner_key,
                    logo: this.item.logo,
                    partner_type: this.item.partner_type,
                    logo_file_name: this.item.logo_file_name,
                    partner_configuration_attributes: configuration,
                    address_attributes: this.item.address ? this.item.address : {}
                };
            },

            onSaveSuccess: function(response) {
                this.item.logo_file = response.data.logo_file;
                var navCfg = context.getNavList();

                // if current user is partner and navCfg is set
                if (response.data.can_perform_apr && navCfg) {

                    var list = $filter("filter")(navCfg.navList, {
                        id: "aprs"
                    }, true);

                    if (list.length < 1) {

                        navCfg.navList.push({
                            id: "aprs",
                            state: "partner.annualPropertyReviews",
                            label: "APRs",
                            icon: "glyphicon glyphicon-calendar"

                        });
                        context.setNavList(this.navCfg);
                    }

                }
                $rootScope.$broadcast("partner-logo-changed", {
                    logo: response.data.logo_file
                });
                this.updateBrandingUser();
            },

            updateBrandingUser: function() {
                if (this.branding_user) {
                    var user = {
                        user_profile_attributes: {
                            id: this.branding_user.user_profile_attributes.id,
                            message: this.branding_user.user_profile_attributes.message,
                            transfer_message_to_homeowner: this.branding_user.user_profile_attributes.transfer_message_to_homeowner,
                            transfer_message_to_agent: this.branding_user.user_profile_attributes.transfer_message_to_agent,
                            transfer_message_to_prepaid_agent: this.branding_user.user_profile_attributes.transfer_message_to_prepaid_agent
                        }
                    };
                    api.user.update(this.branding_user.id, user).then(
                        angular.bind(this, this.updateUserSuccess),
                        angular.bind(this, this.onError));
                }
                else {
                    loading.close();
                    this.cancelForm();
                }
            },

            updateUserSuccess: function(response) {
                this.cancelForm();
                loading.close();
            },

            setLogo: function() {
                var pending = this.imgUploadCfg.api.pendingUploads();
                this.item.logo_file = pending[0].window_url;
                this.item.logo = pending[0].based64;
                this.item.logo_file_name = pending[0].name;
                this.onSave();
            },

            generateApiKey: function() {
                loading.show(this.resources.apiKeySave);
                var partner = {
                    partner_id: this.item.id,
                    company_name: this.item.name,
                    application_name: "Website",
                    contact_email: this.item.email
                };
                api.apiKey.create(partner).then(
                    angular.bind(this, this.keyCreateSuccess),
                    angular.bind(this, this.onError));
            },

            keyCreateSuccess: function(response) {
                this.notify.success(this.resources.apiKeySuccess);
                this.item.api_key = response.data.key;
                loading.close();
            },

            onError: function(response) {
                this.$log.error(response);
                this.notify.error(response.data);
                loading.close();
            },

            transferConfirm: function() {
                if (this.item.configuration.automation_binder_action === true) {
                    modals.confirm({
                        title: this.resources.transferConfirmTitle,
                        glyphicon: "glyphicon glyphicon-transfer",
                        message: this.resources.transferConfirmText,
                        confirm: angular.bind(this, this.onConfirmAutoTransfer),
                        cancel: angular.bind(this, this.onCancelAutoTransfer)
                    });
                }
            },

            onConfirmAutoTransfer: function() {
                this.item.configuration.automation_binder_action = true;
            },

            onCancelAutoTransfer: function() {
                this.item.configuration.automation_binder_action = false;
            },

            showBrandingInfo: function() {
                BrandingInfo.show({
                    brandingOptions: this.branding_options
                });
            },

            addIsn: function() {
                var hasIsnLink = false;
                if (that.links) {
                    that.links[0].links.forEach(function(link) {
                        if (link.value === "isn") {
                            hasIsnLink = true;
                        }
                    });
                    if (!hasIsnLink) {
                        that.links[0].links.push({
                            value: "isn",
                            name: this.resources.isn,
                            active: false
                        });
                    }
                }
            },

            onSelectCountry: function(item) {
                if (item) {
                    this.country = item;
                    this.item.address.country = item.value;
                    this.states = Address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                        return state.country.value === this.country.value;
                    }));
                }
            }
        });

        this.model = new Model();

    }
})();