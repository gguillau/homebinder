describe('partnersEditFormApr', function() {
    var $scope, $compile, session, api, $q, partner;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        session = _$injector_.get("Session");
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(session, "getUser").and.returnValue({ role: "admin" });
        partner = {
            address: {},
            configuration: {
                show_isn_tab: false
            }
        };
        spyOn(api.partner, "get").and.returnValue($q.when({ data: partner }));
        spyOn(api.user, "all").and.returnValue($q.when({ data: { items: [] } }));

        $scope.links = [];
        $scope.settings = {
            canEditKey: true
        };

        $compile('<partners-edit-form-apr></partners-edit-form-apr>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the partners-edit-form-apr directive', function() {
        it('calls api partner get', function() {
            expect(api.partner.get).toHaveBeenCalled();
        });
    });
});