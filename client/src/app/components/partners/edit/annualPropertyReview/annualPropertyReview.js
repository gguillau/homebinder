(function() {
    "use strict";

    angular
        .module("hb.components.partners.annualPropertyReview", [])
        .directive("partnersEditFormApr", function() {
            return {
                restrict: "E",
                templateUrl: "components/partners/edit/annualPropertyReview/annualPropertyReview.tpl.html",
                controller: "PartnerAccountController",
                controllerAs: "ctrl",
                bindToController: true
            };
        });
})();
