(function() {
    "use strict";

    angular.module("hb.components.partners", [
        "hb.components.partners.index",
        "hb.components.partners.edit"
    ]);
})();
