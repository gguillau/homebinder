(function() {
    "use strict";

    angular.module('hb.components.partners.index', [])
        .directive("hbPartnersIndex", function() {
            return {
                restrict: "E",
                templateUrl: "components/partners/index/index.tpl.html"
            };
        })
        .factory("PartnersIndexController", [
            'hb.framework.indexBase',
            'hb.api',
            'hb.resources',
            'WidgetModal',
            'AfterLogin',
            'Session',
            '$state',
            function(IndexBase, api, resources, widgetModal, AfterLogin, Session, $state) {
                var Model = function() {
                    // call the parent class
                    IndexBase.call(this);

                    // apply widget specific resource strings
                    this.resources = angular.extend({}, this.resources, resources.partnersIndex);
                    this.api = api;
                    this.AfterLogin = AfterLogin;
                    this.currentUser = Session.getUser();
                    // set the refresh API call
                    this.refreshCall = api.partner.all;
                    // set the delete API call
                    this.deleteCall = api.partner.destroy;
                    // set the name property used in the delete message
                    this.nameProperty = "contact";
                    this.editState = "admin.partners_edit";
                    this.editParams = {};
                    this.editParamsProperty = "partnerId";
                    this.newState = "admin.partners_new";
                    this.maxSize = 10;
                    this.filters = [{
                        name: null,
                        value: "All Partners"
                    }, {
                        name: "inspector",
                        value: "Inspectors"
                    }, {
                        name: "broker",
                        value: "Brokers"
                    }];
                    this.partnerTypesOptions = [{
                        name: undefined,
                        value: "All Partner Types"
                    }, {
                        name: "inspector",
                        value: "Inspector"
                    }, {
                        name: "broker",
                        value: "Broker"
                    }, {
                        name: "lender",
                        value: "Lenders"
                    }, {
                        name: "hoa",
                        value: "HOAs"
                    }, {
                        name: "insurances",
                        value: "Insurance Companies"
                    }, {
                        name: "builder",
                        value: "Builders"
                    }, {
                        name: "property_manager",
                        value: "Property Managers"
                    }, {
                        name: "homepro",
                        value: "Home Pros"
                    }];
                    this.defaultActionOptions = [{
                        name: undefined,
                        value: "All Actions"
                    }, {
                        name: "share",
                        value: "Share"
                    }, {
                        name: "transfer",
                        value: "Transfer"
                    }];
                    this.autoActionOptions = [{
                        name: undefined,
                        value: "On/Off"
                    }, {
                        name: "true",
                        value: "On"
                    }, {
                        name: "false",
                        value: "Off"
                    }];
                    this.sendAgentOptions = [{
                        name: undefined,
                        value: "None"
                    }, {
                        name: "always",
                        value: "All Binders"
                    }, {
                        name: "once",
                        value: "Only for the first time"
                    }, {
                        name: "never",
                        value: "Never"
                    }];
                    this.defaultWarrantyOptions = [{
                        name: undefined,
                        value: "On/Off"
                    }, {
                        name: "true",
                        value: "On"
                    }, {
                        name: "false",
                        value: "Off"
                    }];
                    this.defaultAprOptions = [{
                        name: undefined,
                        value: "Can/Cannot"
                    }, {
                        name: "true",
                        value: "Can"
                    }, {
                        name: "false",
                        value: "Cannot"
                    }];
                    this.defaultBrandingOptions = [{
                        name: undefined,
                        value: "All Branding Options"
                    }, {
                        name: "user",
                        value: "User"
                    }, {
                        name: "agent",
                        value: "Agent"
                    }, {
                        name: "co_brand",
                        value: "Co-Branding"
                    }, {
                        name: "none",
                        value: "No Branding"
                    }];
                    this.partnerArgs = {
                        type: undefined,
                        default_action: undefined,
                        auto_action: undefined,
                        notify_agent: undefined,
                        branding_option: undefined,
                        warranty_option: undefined,
                        apr_option: undefined
                    };
                    this.current_filter = null;
                    this.sortOptions = [{
                        orderBy: "partners.created_at",
                        order: "desc",
                        desc: this.resources.creationDate + " - " + this.resources.descending
                    }, {
                        orderBy: "partners.created_at",
                        order: "asc",
                        desc: this.resources.creationDate + " - " + this.resources.ascending
                    }, {
                        orderBy: "partners.email",
                        order: "desc",
                        desc: this.resources.partnerEmail + " - " + this.resources.descending
                    }, {
                        orderBy: "partners.email",
                        order: "asc",
                        desc: this.resources.partnerEmail + " - " + this.resources.ascending
                    }];
                    this.sortOption = this.sortOptions[0];
                    this.orderBy = this.sortOption.orderBy;
                    this.order = this.sortOption.order;
                    // set page specific values on toolbar
                    angular.extend(this.toolbarCfg, {
                        title: this.resources.title,
                        sort: {
                            title: this.resources.sortBy,
                            refresh: angular.bind(this, this.refresh),
                            sortOptions: this.sortOptions,
                            sortOption: this.sortOption,
                            execute: angular.bind(this, this.sort)
                        },
                        button: {
                            title: this.resources.newButton,
                            click: angular.bind(this, this.newItem)
                        }
                    });
                    this.setHeaders();
                };

                Model.prototype = Object.create(IndexBase.prototype);

                angular.extend(Model.prototype, {

                    newItem: function() {
                        this.notify.info("This does not work yet.");
                    },

                    setHeaders: function() {
                        this.headers = this.resources.partnerAttributes.map(angular.bind(this, function(attribute) {
                            var sortable = false,
                                orderBy = null,
                                sorted = false;
                            if (attribute === "ID") {
                                sorted = true;
                                sortable = true;
                                orderBy = "partners.id";
                            }
                            else if (attribute === "Name") {
                                sortable = true;
                                orderBy = "partners.name";
                            }
                            else if (attribute === "Email") {
                                sortable = true;
                                orderBy = "partners.email";
                            }
                            else if (attribute === "Contact") {
                                sortable = true;
                                orderBy = "partners.contact";
                            }
                            else if (attribute === "API Route") {
                                sortable = true;
                                orderBy = "partners.partner_key";
                            }
                            return {
                                name: attribute,
                                sortable: sortable,
                                sorted: sorted,
                                orderBy: orderBy,
                                order: "desc"
                            };
                        }));
                        this.headerOptions = angular.copy(this.headers);
                        this.sortOption = this.headers[0];
                    },

                    impersonate: function(partner) {
                        $state.go("partner.binders", {
                            partnerId: partner.id
                        });
                    },

                    addQueryArgs: function() {
                        this.addArgs("type");
                        this.addArgs("default_action");
                        this.addArgs("auto_action");
                        this.addArgs("notify_agent");
                        this.addArgs("branding_option");
                        this.addArgs("warranty_option");
                        this.addArgs("apr_option");
                    },

                    addArgs: function(arg) {
                        if (this.partnerArgs[arg]) {
                            this.queryArgs[arg] = this.partnerArgs[arg];
                        }
                        else {
                            delete this.queryArgs[arg];
                        }
                    },

                    downloadSummaries: function() {
                        this.api.kpi.account_summaries().then(
                            angular.bind(this, this.onDownloadSuccess),
                            angular.bind(this, this.onDownloadError)
                        );
                    },

                    onDownloadSuccess: function(response) {
                        this.notify.info("Please check your email in a couple minutes.");
                    },

                    onDownloadError: function(response) {
                        this.notify.error("Unable to download account summaries");
                        this.$log.error(response.data);
                    }
                });

                return Model;
            }

        ]);
})();