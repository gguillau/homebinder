describe("PartnersIndexController", function() {
    var PartnersIndexController,
        TestClass,
        user,
        kpi;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
        // mock the notify service
        $provide.factory("Notify", function() {
            return {
                success: function() {},
                error: function() {},
                info: function() {}
            };
        });
        // mock the AfterLogin service
        $provide.factory("AfterLogin", function() {
            return {
                go: function() {}
            };
        });

    }));
    // get the base class under test
    beforeEach(inject(function($injector) {
        PartnersIndexController = $injector.get("PartnersIndexController");
    }));

    function createTestClass() {
        TestClass = function() {
            PartnersIndexController.call(this);

            this.resources = angular.extend({}, this.resources, {
                loading: "loading",
                loadError: "load error",
                confirmDelete: "confirm",
                deleteMsg1: "a ",
                deleteMsg2: " b",
                deleteSuccess: "deleted",
                deleteError: "delete error",
                saveSuccess: "SAVE",
                impersonateLoading: "LO"
            });
            this.indexState = "admin.partners";
            this.indexParams = {};
            this.nameProperty = "name";
        };

        TestClass.prototype = Object.create(PartnersIndexController.prototype);

        return new TestClass();
    }

    user = {
        impersonateUser: function() {},
        impersonate: function() {}
    };

    kpi = {
        account_summaries: function() {}
    };

    describe("newItem", function() {
        it("calls notify info", inject(function(Notify) {
            spyOn(Notify, "info");
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.newItem();

            expect(Notify.info).toHaveBeenCalled();
        }));
    });

    describe("impersonate", function() {
        it("impersonates the user", inject(function($q, Loading, $rootScope, $state) {

            spyOn($state, "go");

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.api.user = user;
            model.impersonate({
                id: 1
            });

            expect($state.go).toHaveBeenCalled();
        }));
    });

    describe("setHeaders", function() {
        it("creates the headers", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.setHeaders();

            expect(model.headers.length).toEqual(10);
        }));
    });

    describe("addQueryArgs", function() {
        it("adds the query arguments", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.partnerArgs["type"] = "broker";
            model.partnerArgs["default_action"] = "share";
            model.partnerArgs["auto_action"] = "false";
            model.partnerArgs["notify_agent"] = "true";
            model.partnerArgs["branding_option"] = "user";
            model.addQueryArgs();

            expect(model.queryArgs["type"]).toEqual("broker");
            expect(model.queryArgs["default_action"]).toEqual("share");
            expect(model.queryArgs["auto_action"]).toEqual("false");
            expect(model.queryArgs["notify_agent"]).toEqual("true");
            expect(model.queryArgs["branding_option"]).toEqual("user");
        }));

    });

    describe("addArgs", function() {
        it("adds the query argument", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.partnerArgs["branding_option"] = "agent";
            model.addArgs("branding_option");

            expect(model.queryArgs["branding_option"]).toEqual("agent");
        }));

        it("adds the query argument", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.partnerArgs["type"] = "inspector";
            model.addArgs("type");

            expect(model.queryArgs["type"]).toEqual("inspector");
        }));

        it("adds the query argument", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.partnerArgs["default_action"] = "transfer";
            model.addArgs("default_action");

            expect(model.queryArgs["default_action"]).toEqual("transfer");
        }));

        it("adds the query argument", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.partnerArgs["auto_action"] = "true";
            model.addArgs("auto_action");

            expect(model.queryArgs["auto_action"]).toEqual("true");
        }));

        it("deletes the query argument", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.partnerArgs["notify_agent"] = "true";
            model.addArgs("notify_agent");
            model.partnerArgs["notify_agent"] = undefined;
            model.addArgs("notify_agent");

            expect(model.queryArgs["notify_agent"]).toBe(undefined);
        }));
    });

    describe("downloadSummaries", function() {
        it("downloads", inject(function($q, Loading, $rootScope, Notify) {
            spyOn(kpi, "account_summaries").and.returnValue($q.when({
                data: {
                    token: "adasdsd"
                }
            }));
            spyOn(Notify, "info");

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.api.kpi = kpi;
            model.downloadSummaries();

            $rootScope.$apply();
            expect(kpi.account_summaries).toHaveBeenCalled();
            expect(Notify.info).toHaveBeenCalled();
        }));

        it("returns an error", inject(function($q, Loading, $rootScope, AfterLogin, $log, Notify) {
            spyOn(kpi, "account_summaries").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(Notify, "error");

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.api.kpi = kpi;
            model.downloadSummaries();

            $rootScope.$apply();
            expect(kpi.account_summaries).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
            expect(Notify.error).toHaveBeenCalled();
        }));
    });

});

describe('hbPartnersIndex', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        element = $compile('<hb-partners-index></hb-partners-index>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the headers', function() {
            expect(element.html()).toContain("Partners");
        });
    });
});