describe("HBImageUploaderController", function() {
    var $rootScope,
        $q,
        $scope,
        $controller,
        ctrl;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("HBImageUploaderController", {});

        $scope.ctrl = ctrl;

    }

    describe('ctrl.imageAdded', function() {
        it("calls imgAdded", function() {
            createController();

            ctrl.imgAdded = function() {};
            ctrl.imgUploader = {
                api: {
                    pendingUploads: function() {}
                }
            };
            spyOn(ctrl, "imgAdded");
            spyOn(ctrl.imgUploader.api, "pendingUploads").and.returnValue([{ id: 1 }]);
            ctrl.imageAdded();

            expect(ctrl.images.length).toEqual(1);
            expect(ctrl.imgAdded).toHaveBeenCalled();
        });
    });

    describe('ctrl.remove', function() {
        it("removes the image", function() {
            createController();

            ctrl.imgUploader = {
                api: {
                    pendingUploads: function() {},
                    remove: function() {}
                }
            };

            spyOn(ctrl.imgUploader.api, "remove");
            var image = { id: 1 };
            ctrl.images = [image];
            ctrl.remove(0, image);

            expect(ctrl.images.length).toEqual(0);
            expect(ctrl.imgUploader.api.remove).toHaveBeenCalled();
        });
    });

});