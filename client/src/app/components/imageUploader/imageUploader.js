angular
    .module("hb.components.imageUploader", [])
    .directive("hbImageUploader", function() {
        return {
            restrict: "E",
            templateUrl: "components/imageUploader/imageUploader.tpl.html",
            controller: "HBImageUploaderController as ctrl",
            bindToController: true,
            scope: {
                imgAdded: "&",
                imgUploader: "=",
                imgUploadSuccess: "&",
                imgUploadError: "&"
            }
        };
    })
    .controller("HBImageUploaderController", [
        HBImageUploaderController
    ]);

function HBImageUploaderController() {
    this.images = [];

}

HBImageUploaderController.prototype = {

    imageAdded: function() {
        this.images = this.imgUploader.api.pendingUploads().map(angular.bind(this, function(img) {
            var image = {
                id: img.id,
                name: img.name,
                location: img.location
            };
            return image;
        }));
        this.imgAdded();
    },

    remove: function(index, file) {
        this.imgUploader.api.remove(file);
        this.images.splice(index, 1);
    }

};