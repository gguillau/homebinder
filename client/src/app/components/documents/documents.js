angular
    .module("hb.components.documents", [])
    .directive("hbDocuments", function() {
        return {
            restrict: "E",
            templateUrl: "components/documents/documents.tpl.html",
            controller: "HBDocumentsController as ctrl",
            bindToController: true,
            scope: {
                opts: "=",
                documents: "=",
                binderId: "="
            }
        };
    })
    .controller("HBDocumentsController", [
        "hb.api",
        "$log",
        "Notify",
        "$stateParams",
        "$window",
        "ModalService",
        "Loading",
        "hb.resources",
        "Context",
        HBDocumentsController
    ]);

function HBDocumentsController(api, $log, notify, $stateParams, $window, ModalService, Loading, resources, context) {
    this.api = api;
    this.$log = $log;
    this.notify = notify;
    this.$stateParams = $stateParams;
    this.$window = $window;
    this.modal = ModalService;
    this.loading = Loading;
    this.resources = resources.documents;
    this.refresh();
}

HBDocumentsController.prototype = {

    refresh: function() {
        this.api.document.all(this.opts).then(
            angular.bind(this, this.onRefreshSuccess),
            angular.bind(this, this.onRefreshError)
        );
    },

    onRefreshSuccess: function(response) {
        this.documents = response.data.items.map(angular.bind(this, function(doc) {
            var document = {
                id: doc.id,
                name: doc.name,
                location: doc.location,
                icon: this.getIcon(doc.name)
            };
            return document;
        }));
    },

    onRefreshError: function(response) {
        this.$log.error(response);
        this.notify.error(response.data);
    },

    getIcon: function(name) {
        var file_extension = name.substring(name.length - 3);

        switch (file_extension) {
            case "pdf":
                return "fa fa-file-pdf-o";
            case "doc":
            case "ocx":
                return "fa fa-file-word-o";
            default:
                return "fa fa-file";
        }

    },

    viewDocument: function(doc) {
        this.$window.open(doc.location);
    },

    deleteDocument: function(doc, index) {
        this.modal.confirm({
            message: this.resources.deleteConfirm,
            glyphicon: "glyphicon glyphicon-trash",
            confirm: angular.bind(this, this.destroyConfirmed, doc, index)
        });

    },

    destroyConfirmed: function(doc, index) {
        this.api.document.destroy(doc.id).then(
            angular.bind(this, this.onDocDestroySuccess, index),
            angular.bind(this, this.onDocDestroyError)
        );
    },

    onDocDestroySuccess: function(index, response) {
        this.documents.splice(index, 1);
        this.notify.success(this.resources.removed);
    },

    onDocDestroyError: function(response) {
        this.$log.error(response);
        this.notify.error(response.data);
    }
};