describe("HBDocumentsController", function() {
    var $rootScope,
        $q,
        $scope,
        $controller,
        ctrl,
        notify,
        $http,
        $stateParams,
        $log,
        loading,
        modal,
        api,
        documents;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");
        $stateParams = {};

        documents = [{
            binder: null,
            file_size: 439531,
            id: 562,
            location: "https://somerandomlink.com/documents.doc",
            name: "fakedoc.doc",
            permissions: {
                can_read: true,
                can_write: true,
                can_create: false,
                can_destroy: true
            },
            seller_report_item: null,
            tags: null
        }, {
            binder: null,
            file_size: 439531,
            id: 563,
            location: "https://somerandomlink.com/documents.doc",
            name: "fakedoc.doc",
            permissions: {
                can_read: true,
                can_write: true,
                can_create: false,
                can_destroy: true
            },
            seller_report_item: null,
            tags: null
        }];

        $log = {
            error: function(msg) {}
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        modal = {
            confirm: function(confirmOptions) {}
        };

        notify = {
            success: function(msg) {},
            error: function(msg) {}
        };


    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("HBDocumentsController", {
            "ModalService": modal,
            "$http": $http,
            "Notify": notify,
            "hb.api": api,
            "$stateParams": $stateParams,
            "$log": $log,
            "Loading": loading
        }, {
            tagId: 1,
            documents: []
        });

        $scope.ctrl = ctrl;

    }

    describe('ctrl.refresh', function() {
        it("should retrieve all documents", function() {

            spyOn(api.document, "all").and.returnValue($q.when({
                data: { items: documents }
            }));

            createController();
            $rootScope.$apply();

            expect(api.document.all).toHaveBeenCalled();
            expect(ctrl.documents.length).toBe(2);
        });

        it("should return an error", function() {

            spyOn(api.document, "all").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            $rootScope.$apply();

            expect(api.document.all).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
            expect(ctrl.documents.length).toBe(0);
        });

    });

    describe('ctrl.getIcon', function() {
        it("sets icon to pdf", function() {
            spyOn(api.document, "all").and.returnValue($q.when({
                data: { items: [] }
            }));
            createController();

            var icon = ctrl.getIcon("test.pdf");

            expect(icon).toEqual("fa fa-file-pdf-o");
        });

        it("sets icon to word", function() {
            spyOn(api.document, "all").and.returnValue($q.when({
                data: { items: [] }
            }));
            createController();

            var icon = ctrl.getIcon("test.doc");

            expect(icon).toEqual("fa fa-file-word-o");
        });

        it("sets icon to file", function() {
            spyOn(api.document, "all").and.returnValue($q.when({
                data: { items: [] }
            }));
            createController();

            var icon = ctrl.getIcon("test.local");

            expect(icon).toEqual("fa fa-file");
        });
    });

    describe('ctrl.viewDocument', function() {
        it("calls window open", function() {
            spyOn(api.document, "all").and.returnValue($q.when({
                data: { items: [] }
            }));

            createController();
            spyOn(ctrl.$window, "open");

            ctrl.viewDocument({});

            expect(ctrl.$window.open).toHaveBeenCalled();
        });
    });

    describe('ctrl.deleteDocument', function() {
        it("calls modal", function() {
            spyOn(api.document, "all").and.returnValue($q.when({
                data: { items: [] }
            }));

            createController();
            spyOn(modal, "confirm");

            ctrl.deleteDocument({}, 0);

            expect(modal.confirm).toHaveBeenCalled();
        });
    });

    describe('ctrl.destroyConfirmed', function() {
        it("calls api document destroy", function() {
            spyOn(api.document, "all").and.returnValue($q.when({
                data: { items: [] }
            }));

            createController();
            spyOn(api.document, "destroy").and.returnValue($q.when({ data: {} }));

            ctrl.destroyConfirmed({}, 0);

            expect(api.document.destroy).toHaveBeenCalled();
        });
    });

    describe('ctrl.onDocDestroySuccess', function() {
        it("removes the document", function() {
            spyOn(api.document, "all").and.returnValue($q.when({
                data: { items: [] }
            }));

            createController();
            ctrl.documents = [{ id: 1 }];
            expect(ctrl.documents.length).toEqual(1);
            ctrl.onDocDestroySuccess(0, {});

            expect(ctrl.documents.length).toEqual(0);
        });
    });

    describe('ctrl.onDocDestroyError', function() {
        it("calls notify", function() {
            spyOn(api.document, "all").and.returnValue($q.when({
                data: { items: [] }
            }));

            createController();
            spyOn(ctrl.notify, "error");
            ctrl.onDocDestroyError({ data: "error" });

            expect(ctrl.notify.error).toHaveBeenCalled();
        });
    });

});

describe('hbDocuments', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.document, "all").and.returnValue($q.when({
            data: { items: [] }
        }));

        $scope.images = [];
        $scope.opts = {
            binderId: 1
        };
        $compile('<hb-documents documents="documents" binder-id="1" opts="opts"></hb-documents>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the hb-documents directive', function() {
        it('calls api document all', function() {
            expect(api.document.all).toHaveBeenCalled();
        });
    });
});