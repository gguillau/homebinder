(function() {
    "use strict";

    angular
        .module("hb.components.notify", [])
        .factory("Notify", [
            "toastr",
            function(toastr) {
                var opened_toasts = [],
                    toast;

                function registerDestroy(toast) {
                    toast.scope.$on('$destroy', function(item) {
                        opened_toasts = opened_toasts.filter(function(item) {
                            return item.toastId !== toast.toastId;
                        });
                    });
                }

                return {
                    error: function(message) {
                        toast = toastr.error(message, undefined, {
                            closeButton: true,
                            timeOut: 2000
                        });
                        opened_toasts.push(toast);
                        registerDestroy(toast);
                    },
                    info: function(message, timeout) {
                        toast = toastr.info(message, undefined, {
                            closeButton: true,
                            timeOut: timeout ? timeout : 2000
                        });
                        opened_toasts.push(toast);
                        registerDestroy(toast);
                    },
                    warning: function(message) {
                        toast = toastr.warning(message, undefined, {
                            closeButton: true,
                            timeOut: 2000
                        });
                        opened_toasts.push(toast);
                        registerDestroy(toast);
                    },
                    success: function(message, timeout) {
                        toast = toastr.success(message, undefined, {
                            closeButton: true,
                            timeOut: timeout ? timeout : 2000
                        });
                        opened_toasts.push(toast);
                        registerDestroy(toast);
                    },
                    clearAll: function() {
                        opened_toasts.forEach(function(toast) {
                            toastr.clear(toast);
                        });
                    }
                };
            }
        ]);
})();