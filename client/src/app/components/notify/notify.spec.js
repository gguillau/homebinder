describe("Notify", function() {
    'use strict';

    var Notify, toast;

    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the toastr service
        $provide.factory("toastr", function() {
            var response = {
                scope: {
                    $on: function() {}
                }
            };
            return {
                error: function() { return response; },
                info: function() { return response; },
                warning: function() { return response; },
                success: function() { return response; },
                clear: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($injector, toastr) {
        toast = toastr;
        Notify = $injector.get("Notify");
    }));

    it("calls the toast method", function() {
        spyOn(toast, "error").and.callThrough();
        Notify.error("test");

        expect(toast.error).toHaveBeenCalled();
    });

    it("calls the toast method", function() {
        spyOn(toast, "info").and.callThrough();
        Notify.info("test");

        expect(toast.info).toHaveBeenCalled();
    });

    it("calls the toast method", function() {
        spyOn(toast, "warning").and.callThrough();
        Notify.warning("test");

        expect(toast.warning).toHaveBeenCalled();
    });

    it("calls the toast method", function() {
        spyOn(toast, "success").and.callThrough();
        Notify.success("test");

        expect(toast.success).toHaveBeenCalled();
    });

    it("clears all open toasts", function() {
        spyOn(toast, "clear");
        Notify.success("test");
        Notify.clearAll();

        expect(toast.clear).toHaveBeenCalled();
    });
});