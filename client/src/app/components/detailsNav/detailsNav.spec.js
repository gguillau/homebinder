describe("DetailsNavController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("DetailsNavController", {
        });
        $scope.ctrl = ctrl;

    }

    describe('ctrl.init', function() {
        it("sets the tabs", function() {
            createController();
            $rootScope.$apply();
            expect(ctrl.tabs.length).toEqual(5);
        });
    });

});

describe('hbDetailsNav', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        element = $compile('<hb-details-nav></hb-details-nav>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the navigation', function() {
            expect(element.html()).toContain("Home Details");
        });
    });
});