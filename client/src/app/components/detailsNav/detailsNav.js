(function() {
    "use strict";

    angular
        .module("hb.components.detailsNav", [])
        .directive("hbDetailsNav", function() {
            return {
                restrict: "E",
                templateUrl: "components/detailsNav/detailsNav.tpl.html",
                controller: "DetailsNavController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    active: "="
                }
            };
        })
        .controller("DetailsNavController", [
            "hb.resources",
            DetailsNavController
        ]);

    function DetailsNavController(resources) {
        this.resources = resources.detailsNav;
        this.tabs = [{
            id: "areas",
            name: this.resources.tabAreas,
            resourceType: "area",
            state: "binder.areas"
        }, {
            id: "structures",
            name: this.resources.tabStructures,
            resourceType: "structure",
            state: "binder.structures"
        }, {
            id: "paints",
            name: this.resources.tabPaints,
            resourceType: "paint",
            state: "binder.paints"
        }, {
            id: "finishes",
            name: this.resources.tabFinishes,
            resourceType: "finish",
            state: "binder.finishes"
        }, {
            id: "edit_binder",
            name: this.resources.tabEditBinder,
            resourceType: "edit",
            state: "binder.edit"
        }];
        
        // Find the tab with the id that matches the active tab, and set its active attribute
        for (var i = 0; i < this.tabs.length; i++) {
            if (this.tabs[i].id === this.active) {
                this.tabs[i].active = "active";
            }
        }
    }
})();