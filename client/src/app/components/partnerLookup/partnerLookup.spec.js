describe("PartnerLookupController", function() {
    var ctrl,
        controller,
        $q,
        $log,
        api,
        notify,
        resources,
        loading;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
        args = {};
    }));

    function createController() {
        ctrl = controller("PartnerLookupController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "hb.resources": resources,
            "Loading": loading
        });
    }

    describe("lookupPartner", function() {
        it("calls partner all", function() {
            spyOn(api.partner, "all").and.returnValue($q.when({ data: { items: [] } }));
            createController();
            ctrl.model.lookupPartner();

            expect(api.partner.all).toHaveBeenCalled();
        });
    });

    describe("mapLookupResponse", function() {
        it("returns mapped partners", function() {
            createController();
            var array = [
                { id: 1, email: "test@gmail.com", name: "Test Company" }
            ];
            var partners = ctrl.model.mapLookupResponse({ data: { items: array } }).data;

            expect(partners).toEqual(array);
        });
    });

    describe("partnerSelected", function() {
        it("calls cfg partnerSelected", function() {
            createController();
            ctrl.model.cfg.partnerSelected = function() {};
            spyOn(ctrl.model.cfg, "partnerSelected");
            ctrl.model.partnerSelected({});

            expect(ctrl.model.cfg.partnerSelected).toHaveBeenCalled();
        });

        it("clears the selection", function() {
            createController();
            ctrl.model.cfg.clearOnSelect = true;
            ctrl.model.partnerSelected({});

            expect(ctrl.model.selection).toBe(undefined);
        });
    });

});