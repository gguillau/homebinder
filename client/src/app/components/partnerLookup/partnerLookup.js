angular
    .module("hb.components.partnerLookup", [])
    .directive("hbPartnerLookup", function() {
        return {
            restrict: "E",
            template: "<hb-lookup cfg=\"ctrl.model.lookupCfg\" selection=\"ctrl.selection\"></hb-lookup>",
            controller: "PartnerLookupController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {
                cfg: "=",
                selection: "=?"
            }
        };
    })
    .controller("PartnerLookupController", [
        "hb.api",
        "hb.resources",
        function(api, resources) {
            var that = this;
            var Model = function(cfg) {
                this.cfg = cfg || {};
                this.resources = resources.partnerLookup;
                this.lookupCfg = {
                    placeholder: this.resources.placeHolder,
                    refresh: angular.bind(this, this.lookupPartner),
                    itemSelected: angular.bind(this, this.partnerSelected),
                    itemTemplate: "components/partnerLookup/partnerLookupItem.tpl.html",
                    selectionTemplate: "components/partnerLookup/partnerLookupSelection.tpl.html"
                };
            };
            
            angular.extend(Model.prototype, {
                lookupPartner: function(value) {
                    return api.partner.all({ search: value }).then(
                        angular.bind(this, this.mapLookupResponse));
                },
                mapLookupResponse: function(response) {
                    return { data: response.data.items };  
                },
                partnerSelected: function(partner) {
                    if (this.cfg.partnerSelected) {
                        this.cfg.partnerSelected(partner);
                    }
                    if (this.cfg.clearOnSelect) {
                        that.selection = null;    
                    }
                }
            });
            
            this.model = new Model(this.cfg);
        }
    ]);