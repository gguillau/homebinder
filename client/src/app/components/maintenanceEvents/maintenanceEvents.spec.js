describe("HBMaintenanceEventsController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        api,
        $log,
        notify,
        $stateParams,
        resources,
        $q,
        context,
        defer;

    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q.defer();
                    defer.resolve({ id: 1, permissions: { can_read: true } });
                    return defer.promise;
                }
            };
        });
    }));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $q = _$injector_.get("$q");
        $controller = _$controller_;
        api = _$injector_.get("hb.api");
        $log = _$injector_.get("$log");
        notify = _$injector_.get("Notify");
        $stateParams = _$injector_.get("$stateParams");
        context = _$injector_.get("Context");
        resources = _$injector_.get("hb.resources");
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("HBMaintenanceEventsController", {
            "hb.api": api,
            "$log": $log,
            "Notify": notify,
            "$stateParams": $stateParams,
            "hb.resources": resources,
            "Context": context
        });

        $scope.ctrl = ctrl;
    }

    describe("ctrl.open", function() {
        it("sets opened to true", function() {

            createController();
            expect(ctrl.date[0].opened).toEqual(false);
            ctrl.open(0);

            expect(ctrl.date[0].opened).toEqual(true);
        });
    });

    describe("ctrl.getContractors", function() {
        it("calls api contractor all", function() {

            spyOn(api.contractor, "all").and.returnValue($q.when({ data: {} }));

            createController();
            ctrl.getContractors({ id: 1 });

            expect(api.contractor.all).toHaveBeenCalled();
        });
    });

    describe("ctrl.getContractorsSuccess", function() {
        it("sets the contractors and updates the events", function() {
            createController();
            ctrl.events = [{ do_date: "test", completed_date: "test" }];
            ctrl.getContractorsSuccess({ data: { items: [{ id: 1 }] } });

            expect(ctrl.contractors.length).toEqual(1);
        });
    });

    describe("ctrl.getContractorsError", function() {
        it("calls $log error", function() {

            spyOn($log, "error");

            createController();
            ctrl.getContractorsError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
        });
    });

    describe("ctrl.edit", function() {
        it("sets can_edit to true", function() {

            createController();
            var event = { can_edit: false };
            ctrl.edit(event);

            expect(event.can_edit).toEqual(true);
        });
    });

    describe("ctrl.save", function() {
        it("calls api maintenanceEvent update", function() {

            spyOn(api.maintenanceEvent, "update").and.returnValue($q.when({ data: {} }));

            createController();
            ctrl.save({ id: 1, tags: [] });

            expect(api.maintenanceEvent.update).toHaveBeenCalled();
        });
    });

    describe("ctrl.updateSuccess", function() {
        it("sets the contractors and updates the events", function() {
            spyOn(notify, "success");

            createController();
            ctrl.events = [{ id: 1 }];
            ctrl.updateSuccess(ctrl.events[0], { data: [{ id: 1 }] });

            expect(notify.success).toHaveBeenCalled();
        });
    });

    describe("ctrl.updateError", function() {
        it("calls $log error", function() {

            spyOn($log, "error");

            createController();
            ctrl.updateError({ data: "error" });

            expect($log.error).toHaveBeenCalled();
        });
    });
});

describe('hbMaintenanceEvents', function() {
    var $scope, $compile, $q, api, defer;

    beforeEach(module('homebinder'));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Context", function() {
            return {
                getBinder: function() {
                    defer = $q.defer();
                    defer.resolve({ permissions: { can_read: true } });
                    return defer.promise;
                }
            };
        });
    }));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        $q = _$injector_.get("$q");
        api = _$injector_.get("hb.api");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.contractor, "all").and.returnValue($q.when({ data: { items: [] } }));

        $scope.events = [];

        $compile('<hb-maintenance-events events="events"></hb-maintenance-events>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('calls api contractor all', function() {
            expect(api.contractor.all).toHaveBeenCalled();
        });
    });
});