angular
    .module("hb.components.maintenanceEvents", [])
    .directive("hbMaintenanceEvents", function() {
        return {
            restrict: "E",
            templateUrl: "components/maintenanceEvents/maintenanceEvents.tpl.html",
            controller: "HBMaintenanceEventsController as ctrl",
            bindToController: true,
            scope: {
                tagId: "=",
                events: "="
            }
        };
    })
    .controller("HBMaintenanceEventsController", [
        "hb.api",
        "$log",
        "Notify",
        "$stateParams",
        "hb.resources",
        "Context",
        HBMaintenanceEventsController
    ]);

function HBMaintenanceEventsController(api, $log, notify, $stateParams, resources, context) {
    this.api = api;
    this.$log = $log;
    this.notify = notify;
    this.datepicker = {
        format: "yyyy-MM-dd",
        options: {
            "show-button-bar": false
        }
    };
    this.date = [{
        opened: false
    }, {
        opened: false
    }];
    this.resources = resources.maintenanceEvents;
    context.getBinder().then(angular.bind(this, this.getContractors), null);
}

HBMaintenanceEventsController.prototype = {

    open: function(index) {
        this.date[index].opened = true;
    },

    getContractors: function(binder) {
        this.binder = binder;
        this.binderId = binder.id;
        this.api.contractor.all({ binderId: this.binder.id, count: 100 }).then(
            angular.bind(this, this.getContractorsSuccess),
            angular.bind(this, this.getContractorsError));
    },

    getContractorsSuccess: function(response) {
        this.contractors = response.data.items;
        this.events.forEach(function(item) {
            if (item.do_date) {
                item.do_date = new Date(item.do_date);
            }
            if (item.completed_date) {
                item.completed_date = new Date(item.completed_date);
            }
        });
    },

    getContractorsError: function(response) {
        this.$log.error(response.data);
    },

    edit: function(event) {
        event.can_edit = !event.can_edit;
    },

    save: function(event) {
        delete event.can_edit;
        delete event.contractor;
        event.tags = event.tags ? event.tags : [];
        this.api.maintenanceEvent.update(event.id, event).then(
            angular.bind(this, this.updateSuccess, event),
            angular.bind(this, this.updateError));
    },

    updateSuccess: function(event, response) {
        var index = this.events.indexOf(event);
        this.events[index] = response.data;
        this.notify.success(this.resources.updated);
    },

    updateError: function(response) {
        this.$log.error(response);
        this.notify.error(response.data);
    }
};