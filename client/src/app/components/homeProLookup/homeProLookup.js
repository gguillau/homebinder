angular
    .module("hb.components.homeProLookup", [])
    .directive("hbHomeProLookup", function() {
        return {
            restrict: "E",
            template: "<hb-lookup cfg=\"ctrl.model.lookupCfg\" selection=\"ctrl.selection\"></hb-lookup>",
            controller: "HomeProLookupController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {
                cfg: "=",
                selection: "=?"
            }
        };
    })
    .controller("HomeProLookupController", [
        "hb.api",
        "hb.resources",
        function(api, resources) {
            var that = this;
            var Model = function(cfg) {
                this.cfg = cfg || {};
                this.resources = resources.homeProLookup;
                this.lookupCfg = {
                    placeholder: this.resources.placeHolder,
                    refresh: angular.bind(this, this.lookupHomePro),
                    itemSelected: angular.bind(this, this.homeProSelected),
                    itemTemplate: this.cfg.itemTemplate || "components/homeProLookup/homeProLookupItem.tpl.html",
                    selectionTemplate: this.cfg.selectionTemplate || "components/homeProLookup/homeProLookupSelection.tpl.html"
                };
            };

            angular.extend(Model.prototype, {
                lookupHomePro: function(value) {
                    if (this.cfg.partnerId) {
                        return api.partnerContractor.all({ search: value }).then(angular.bind(this, this.mapLookupResponse));
                    }
                    else {
                        return api.contractor.all({ search: value }).then(angular.bind(this, this.mapLookupResponse));
                    }
                },
                mapLookupResponse: function(response) {
                    if (response.data.items) {
                        return {
                            data: response.data.items.map(function(homePro) {
                                var mapped = {
                                    id: homePro.id,
                                    displayName: homePro.name,
                                    email: homePro.email
                                };
                                return mapped;
                            }, this)
                        };
                    }
                    return {
                        data: response.data.map(function(homePro) {
                            var mapped = {
                                id: homePro.id,
                                displayName: homePro.name,
                                email: homePro.email
                            };
                            return mapped;
                        }, this)
                    };
                },
                homeProSelected: function(homePro) {
                    if (this.cfg.homeProSelected) {
                        this.cfg.homeProSelected(homePro);
                    }
                    if (this.cfg.clearOnSelect) {
                        that.selection = null;
                    }
                }
            });

            this.model = new Model(this.cfg);
        }
    ]);
