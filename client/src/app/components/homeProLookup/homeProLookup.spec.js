describe("HomeProLookupController", function() {
    var ctrl,
        controller,
        $q,
        $log,
        api,
        notify,
        resources,
        loading;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
        args = {};
    }));

    function createController() {
        ctrl = controller("HomeProLookupController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "hb.resources": resources,
            "Loading": loading
        });
    }

    describe("init", function() {
        it("sets the cfg", function() {
            createController();

            expect(ctrl.model.cfg).toEqual({});
        });
    });

    describe("lookupHomePro", function() {
        it("throws an error", function() {
            createController();
            expect(ctrl.model.lookupHomePro).toThrow();
        });

        it("calls api partnerContractor all", function() {
            spyOn(api.partnerContractor, "all").and.returnValue($q.when({ data: { items: [] } }));

            createController();
            ctrl.model.cfg.partnerId = 1;
            ctrl.model.lookupHomePro();

            expect(api.partnerContractor.all).toHaveBeenCalled();
        });
    });

    describe("mapLookupResponse", function() {
        it("returns mapped home pros", function() {
            createController();
            var array = [
                { id: 1, email: "test@gmail.com", name: "Test Home Pro" }
            ];
            var pros = ctrl.model.mapLookupResponse({ data: array });

            expect(pros.data[0]).toEqual({ id: 1, email: 'test@gmail.com', displayName: 'Test Home Pro' });
        });
    });

    describe("homeProSelected", function() {
        it("calls cfg homeProSelected", function() {
            createController();
            ctrl.model.cfg.homeProSelected = function() {};
            spyOn(ctrl.model.cfg, "homeProSelected");
            ctrl.model.homeProSelected({});

            expect(ctrl.model.cfg.homeProSelected).toHaveBeenCalled();
        });

        it("clears the selection", function() {
            createController();
            ctrl.model.cfg.clearOnSelect = true;
            ctrl.model.homeProSelected({});

            expect(ctrl.model.selection).toBe(undefined);
        });
    });

});

describe('hbHomeProLookup', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = {};
        $scope.selection = {};
        
        element = $compile('<hb-home-pro-lookup cfg="cfg"></hb-home-pro-lookup>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("Search for a Home Pro");
        });
    });
});