angular
    .module("hb.components.gallery", [])
    .directive("hbGallery", function() {
        return {
            restrict: "E",
            templateUrl: "components/gallery/gallery.tpl.html",
            scope: {
                chunked: "=",
                images: "=",
                destroy: "&",
                update: "&"
            },
            controller: "HBGalleryController as ctrl",
            bindToController: true
        };
    })
    .controller("HBGalleryController", [
        "hb.resources",
        "Event",
        "Session",
        HBGalleryController
    ]);

function HBGalleryController(resources, event, session) {
    this.resources = resources.gallery;
    this.event = event;
    this.session = session;
}

HBGalleryController.prototype = {
    viewImage: function() {
        this.currentUser = this.session.getUser();
        this.event.create({ event_name: "view image", event_type: "engagement", user_id: this.currentUser.id, user_role: this.currentUser.role, user_created_at: this.currentUser.created_at, user_create_method: this.currentUser.create_method });
    }
};