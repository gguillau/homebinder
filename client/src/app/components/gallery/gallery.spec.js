describe("HBGalleryController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        session,
        event;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        session = _$injector_.get("Session");
        event = _$injector_.get("Event");
        $controller = _$controller_;
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("HBGalleryController", {
            "Session": session,
            "Event": event
        });
        $scope.ctrl = ctrl;

    }

    describe('ctrl.init', function() {
        it('inits the controller', function() {
            createController();
            expect(ctrl.resources.remove).toEqual("Delete photo");
        });
    });

    describe('ctrl.viewImage', function() {
        it('calls event', function() {
            createController();
            spyOn(session, "getUser").and.returnValue({ id: 1 });
            spyOn(event, "create");

            ctrl.viewImage();

            expect(event.create).toHaveBeenCalled();
        });
    });

});

describe('hbGallery', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.chunked = [];
        $scope.images = [{ id: 1, location: "www.homebinder.com/img/pic.jpg" }];
        $scope.destroy = function() {};
        $scope.update = function() {};

        element = $compile('<hb-gallery chunked="chunked" images="images" destroy="destroy" update="update" ></hb-gallery>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the html elements', function() {

            expect(element.html()).toContain("Delete photo");
        });
    });
});