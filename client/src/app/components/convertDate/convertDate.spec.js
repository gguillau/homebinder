describe("convertDate", function() {
    var $scope, $compile, element, compiled;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        $scope = $rootScope.$new(), $compile = _$compile_;
    }));

    describe('when the page compiles the partners-binders-dashboard directive', function() {
        it('returns the correct date', function() {
            $scope.do_date = new Date("2019-04-25");
            element = $compile('<input ng-model="do_date" convert-date/>')($scope);
            $scope.$digest();

            expect(element.controller("ngModel").$modelValue).toEqual($scope.do_date);
        });

        it('returns an undefined date', function() {
            $scope.do_date = undefined;
            element = $compile('<input ng-model="do_date" convert-date/>')($scope);
            $scope.$digest();

            expect(element.controller("ngModel").$modelValue).toBe(undefined);
        });

        it("updates the date correctly", function() {
            $scope.do_date = new Date("2019-04-25");
            element = angular.element('<input ng-model="do_date" convert-date/>');
            compiled = $compile(element);
            compiled($scope);
            $scope.$digest();

            element.val(new Date("2019-04-27")).triggerHandler("input");
            $scope.$digest();

            expect($scope.do_date).toBe("2019-04-27");
        });
    });
});