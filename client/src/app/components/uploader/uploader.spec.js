describe("UploaderController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        notify,
        $modal,
        constants,
        $http,
        cfg,
        api,
        $q,
        $httpBackend,
        response,
        $document,
        $timeout,
        $window;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        $httpBackend = _$injector_.get("$httpBackend");
        $http = _$injector_.get("$http");
        notify = _$injector_.get("Notify");
        $timeout = _$injector_.get("$timeout");
        $window = _$injector_.get("$window");
        
        $controller = _$controller_;

        $document = {
            getElementById: function() { return { click: function() {} }; }
        };

        cfg = {
            fileTypes: "document",
            hideUploadButton: true,
            showTable: false,
            selectButtonLabel: "Upload Me",
            url: "/api/v1/documents/",
            multiSelect: true,
            id: "uploadDocuments",
            hide_upload: false
        };

        constants = {
            MaxFileSize: 501031030
        };

        $modal = {

        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("UploaderController", {
            "$scope": $scope,
            "ModalService": $modal,
            "constants": constants,
            "$http": $http,
            "Notify": notify,
            "$document": $document,
            "$window": $window
        }, {
            cfg: cfg
        });

        $scope.ctrl = ctrl;

    }

    describe('ctrl.init', function() {
        it("should initialize correct object", function() {
            createController();

            expect(ctrl.filters).toEqual({
                title: "Document files",
                extensions: ".csv,.pdf,.ppt,.pptx,.doc,.docx,.xls,.xlsx,.txt,.rar,.zip"
            });
        });

        it("should initialize correct object for csv", function() {
            createController();
            ctrl.cfg.fileTypes = "csv";
            ctrl.init();

            expect(ctrl.filters).toEqual({
                title: "CSV files",
                extensions: ".csv"
            });
        });

        it("should initialize correct object for image", function() {
            createController();
            ctrl.cfg.fileTypes = "image";
            ctrl.init();

            expect(ctrl.filters).toEqual({
                title: "Image files",
                extensions: "image/*"
            });
        });

        it("calls uploadFiles", function() {
            createController();
            ctrl.cfg.fileTypes = "image";
            ctrl.init();
            spyOn(ctrl, "uploadFiles");
            ctrl.cfg.api.uploadFiles();

            expect(ctrl.uploadFiles).toHaveBeenCalled();
        });

        it("returns the files", function() {
            createController();
            ctrl.cfg.fileTypes = "image";
            ctrl.init();
            ctrl.files = [{ id: 1 }];
            var files = ctrl.cfg.api.pendingUploads();

            expect(files.length).toEqual(1);
        });

        it("calls updateMultiPartParams", function() {
            createController();
            ctrl.cfg.fileTypes = "image";
            ctrl.init();
            spyOn(ctrl, "updateMultiPartParams");
            ctrl.cfg.api.updateParams({});

            expect(ctrl.updateMultiPartParams).toHaveBeenCalled();
        });

        it("calls removeFile", function() {
            createController();
            ctrl.cfg.fileTypes = "image";
            ctrl.init();
            spyOn(ctrl, "removeFile");
            ctrl.cfg.api.remove({});

            expect(ctrl.removeFile).toHaveBeenCalled();
        });

        it("returns the files", function() {
            createController();
            ctrl.cfg.fileTypes = "image";
            ctrl.init();
            ctrl.addedFiles = [{ id: 1 }];
            var files = ctrl.cfg.api.addedFiles();

            expect(files.length).toEqual(1);
        });

        it("calls click", function() {
            createController();
            ctrl.cfg.fileTypes = "image";
            ctrl.init();
            spyOn(ctrl, "click");
            ctrl.cfg.api.click({});

            expect(ctrl.click).toHaveBeenCalled();
        });

    });

    describe('ctrl.updateMultiPartParams', function() {
        it("should set disable to true", function() {
            createController();
            expect(ctrl.disabled).toBe(false);
            ctrl.cfg.params = { tags: [{ tag: "appliance_1", auto_generated: true }], binderId: 1 };
            ctrl.updateMultiPartParams();

            expect(ctrl.multipart_params).toEqual({ rtag1: 'appliance_1;true', binderId: 1 });
        });

    });

    describe('ctrl.disable', function() {

        it("should set disable to true", function() {
            createController();
            expect(ctrl.disabled).toBe(false);
            ctrl.cfg.fileLimit = true;
            ctrl.disable();

            expect(ctrl.disabled).toBe(true);
        });

    });

    describe('ctrl.removeFile', function() {

        it("should remove a file from the files array", function() {
            var file = { id: 1, name: "test.jpg" };

            createController();
            ctrl.files = [file];
            ctrl.disabled = true;
            ctrl.cfg.fileLimit = true;
            ctrl.removeFile(file);

            expect(ctrl.disabled).toBe(false);
            expect(ctrl.files.length).toEqual(0);
        });

    });

    describe('ctrl.showUploadButton', function() {
        it("returns true", function() {
            createController();
            ctrl.cfg.hideUploadButton = false;
            var result = ctrl.showUploadButton();

            expect(result).toBe(true);
        });

        it("returns false", function() {
            createController();
            ctrl.cfg.hideUploadButton = true;
            var result = ctrl.showUploadButton();

            expect(result).toBe(false);
        });

    });

    describe('ctrl.onSelect', function() {
        it("calls disable and notify when file is too large", function() {
            createController();
            spyOn(ctrl.notify, "info");
            var file = { size: 601031030, name: "test.pdf" };
            ctrl.$scope.files = [file];
            spyOn(ctrl, "disable");

            ctrl.onSelect();

            expect(ctrl.notify.info).toHaveBeenCalledWith("The file '" + file.name + "' is too large");
            expect(ctrl.disable).toHaveBeenCalled();
        });

        it("notifies user when there are duplicate files in current files", function() {
            createController();
            spyOn(ctrl.notify, "info");
            var file = { size: 400000, name: "test.pdf" };
            ctrl.$scope.files = [file];
            ctrl.currentFiles = [file];
            spyOn(ctrl, "disable");

            ctrl.onSelect();

            expect(ctrl.notify.info).toHaveBeenCalledWith("Cannot add duplicate files.");
            expect(ctrl.disable).toHaveBeenCalled();
        });

        it("notifies user when there are duplicate files in added files", function() {
            createController();
            spyOn(ctrl.notify, "info");
            var file = { size: 400000, name: "test.pdf" };
            ctrl.$scope.files = [file];
            ctrl.files = [file];
            spyOn(ctrl, "disable");

            ctrl.onSelect();

            expect(ctrl.notify.info).toHaveBeenCalledWith("Cannot add duplicate files.");
            expect(ctrl.disable).toHaveBeenCalled();
            expect(ctrl.$scope.files.length).toEqual(0);
        });

        it("adds the file", function() {
            createController();
            var file = { size: 400000, name: "test.pdf" };
            ctrl.$scope.files = [file];
            spyOn(ctrl, "getBase64");
            ctrl.fileAdded = function() {};
            spyOn(ctrl, "fileAdded");
            ctrl.onSelect();
            $timeout.flush();
            $scope.$apply();

            expect(ctrl.getBase64).toHaveBeenCalled();
            expect(ctrl.fileAdded).toHaveBeenCalled();
        });
    });

    describe('ctrl.getBase64', function() {
        it("adds the file", function() {
            createController();
            //var file = { size: 400000, name: "test.pdf" };
            var file = new Blob( [ new TextEncoder().encode( 'some text' ) ], { type: 'text/plain' } );
            spyOn(window, 'FileReader').and.returnValue({
                onLoad: function() {},
                readAsDataURL: function() {}
            });
            expect(ctrl.files.length).toEqual(0);
            ctrl.getBase64(file);
            window.FileReader().onload();

            expect(ctrl.files.length).toEqual(1);
        });

        it("calls notify", function() {
            createController();
            var file = { size: 400000, name: "test.pdf" };
            spyOn(notify, "error");
            spyOn(window, 'FileReader').and.returnValue({
                onLoad: function() {},
                readAsDataURL: function() {},
                onerror: function() {}
            });
            expect(ctrl.files.length).toEqual(0);
            ctrl.getBase64(file);
            window.FileReader().onerror("error");

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe('ctrl.uploadFiles', function() {
        it("calls callApi", function() {
            createController();
            ctrl.callApi = api.document.create;
            spyOn(ctrl, "callApi").and.returnValue($q.when({ data: {} }));
            var file = { size: 400000, name: "test.pdf" };
            ctrl.files = [file];
            ctrl.uploadFiles();

            expect(ctrl.callApi).toHaveBeenCalled();
        });
    });

    describe('callApi', function() {
        it("returns promise", function() {
            createController();
            response = { data: { name: "test" } };
            ctrl.cfg.url = "/api/v1/documents";
            $httpBackend.expectPOST("/api/v1/documents")
                .respond(response);

            ctrl.callApi({ name: "test" }).then(function(response) {
                expect(response.data).toEqual(response.data);
            });

            $httpBackend.flush();
        });
    });

    describe('ctrl.addParams', function() {
        it("adds params to the form object", function() {
            createController();
            var form = new FormData();
            ctrl.multipart_params = { binderId: 1 };
            ctrl.addParams(form);
        });
    });

    describe('ctrl.plFileUploaded', function() {
        it("adds the file to added files", function() {
            createController();

            ctrl.cfg.showTable = true;
            ctrl.files = [{ id: 1 }, { id: 2 }];
            ctrl.fileUploaded = function() {};

            expect(ctrl, "removeFile");
            expect(ctrl, "fileUploaded");

            ctrl.plFileUploaded({}, { data: {} });

            expect(ctrl.addedFiles.length).toEqual(1);
        });
    });

    describe('ctrl.plFileError', function() {
        it("adds the file to added files", function() {
            createController();

            ctrl.cfg.showTable = true;
            ctrl.$scope.files = [{ id: 1 }];
            ctrl.fileError = function() {};

            spyOn(ctrl, "uploadFiles");
            spyOn(ctrl.notify, "info");

            ctrl.plFileError({ name: "test.pdf" }, { data: { file: ["test.pdf already exists"] } });

            expect(ctrl.notify.info).toHaveBeenCalled();
        });
    });

    describe('ctrl.click', function() {
        xit("clicks document", function() {
            spyOn(document, "getElementById").and.callThrough();

            createController();
            ctrl.cfg = {};
            ctrl.click();

            expect($document.getElementById).toHaveBeenCalled();
        });
    });

});

describe('hbUploader', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = { multiSelect: true, hide_upload: false, selectButtonLabel: "Upload" };
        $scope.api = {};
        $scope.files = [];
        $scope.uploaded = function() {};
        $scope.error = function() {};
        $scope.added = function() {};

        element = $compile('<hb-uploader cfg="cfg" api="api" file-uploaded="uploaded" file-error="error" file-added="added" current-files="files"></hb-uploader>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the title', function() {
            expect(element.html()).toContain("Upload");
        });

        it('calls the onSelect', function() {
            element.find('input').trigger('change');
        });
    });
});