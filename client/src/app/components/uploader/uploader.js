(function() {
    "use strict";

    angular
        .module("hb.components.uploader", [])
        .directive("hbUploader", function() {
            return {
                restrict: "E",
                templateUrl: "components/uploader/uploader.tpl.html",
                bindToController: true,
                scope: {
                    cfg: "=",
                    api: "=",
                    fileUploaded: "&",
                    fileError: "&",
                    fileAdded: "&",
                    currentFiles: "="
                },
                controller: "UploaderController as ctrl"
            };
        })
        .directive("fileList", function() {
            return function($scope, elm, attrs) {
                elm.bind("change", function(evt) {
                    $scope.$apply(function() {
                        $scope.files = [];
                        Array.prototype.push.apply($scope.files, evt.target.files);
                        $scope.ctrl.onSelect();
                        elm.val("");
                    });
                });
            };
        })
        .directive("ngMultiple", function() {
            return {
                restrict: "A",
                scope: {
                    ngMultiple: "="
                },
                link: function($scope, element) {
                    if ($scope.ngMultiple) {
                        element.attr("multiple", "multiple");
                    }
                }
            };
        })
        .controller("UploaderController", [
            "$scope",
            "ModalService",
            "Notify",
            "constants",
            "$http",
            "$window",
            "$timeout",
            "$document",
            UploaderController
        ]);

    function UploaderController($scope, modals, notify, constants, $http, $window, $timeout, $document) {
        this.$scope = $scope;
        this.$scope.files = [];
        this.modals = modals;
        this.notify = notify;
        this.constants = constants;
        this.$http = $http;
        this.$window = $window;
        this.$timeout = $timeout;
        this.$document = $document;
        this.files = [];
        this.added_files = [];
        this.files_length = 0;
        this.addedFiles = [];
        this.multipart_params = {};
        this.bite = 1048576;
        this.maxFileSize = constants.MaxFileSize;
        this.limitAlert = "(Maximum file size " + this.maxFileSize / this.bite + " MB)";
        this.uploadError = "An error occurred uploding your file. Remove the file and give it another try. If the problem persists let us know.";
        this.disabled = false;
        this.errorMsgs = [];
        this.init();
    }

    UploaderController.prototype = {

        init: function() {
            this.cfg.api = {
                uploadFiles: angular.bind(this, function() {
                    this.uploadFiles();
                }),
                pendingUploads: angular.bind(this, function() {
                    return this.files;
                }),
                updateParams: angular.bind(this, function(params) {
                    this.cfg.params = params;
                    this.updateMultiPartParams(params);
                }),
                remove: angular.bind(this, function(file) {
                    this.removeFile(file);
                }),
                addedFiles: angular.bind(this, function() {
                    return this.addedFiles;
                }),
                click: angular.bind(this, function() {
                    this.click();
                })
            };

            if (this.cfg.fileTypes == "image") {
                this.filters = {
                    title: "Image files",
                    extensions: "image/*"
                };
            }
            else if (this.cfg.fileTypes == "csv") {
                this.filters = {
                    title: "CSV files",
                    extensions: ".csv"
                };
            }
            else if (this.cfg.fileTypes == "document") {
                this.cfg.selectButtonLabel = this.cfg.selectButtonLabel ? this.cfg.selectButtonLabel : "Select documents to upload";
                this.filters = {
                    title: "Document files",
                    extensions: ".csv,.pdf,.ppt,.pptx,.doc,.docx,.xls,.xlsx,.txt,.rar,.zip"
                };
            }

            this.updateMultiPartParams();
        },

        updateMultiPartParams: function() {
            for (var prop in this.cfg.params) {
                if (prop === "tags") {
                    var tags = this.cfg.params[prop];
                    for (var k = 0; k < tags.length; k++) {
                        this.multipart_params["rtag" + (k + 1)] = tags[k].tag + ";" + tags[k].auto_generated;
                    }
                }
                else {
                    this.multipart_params[prop] = this.cfg.params[prop];
                }
            }
        },

        disable: function() {
            if (this.cfg.fileLimit) {
                this.disabled = true;
            }
        },

        removeFile: function(file) {
            if (this.cfg.fileLimit) {
                this.disabled = false;
            }
            for (var k = 0; k < this.files.length; k++) {
                if (this.files[k].name == file.name) {
                    this.files.splice(k, 1);
                    this.$window.URL.revokeObjectURL(file.window_url);
                    var index = this.$scope.files.indexOf(file);
                    this.$scope.files.splice(index, 1);
                }
            }
            this.errorMsgs = [];
        },

        showUploadButton: function() {
            if (this.cfg.hideUploadButton) {
                return false;
            }
            return true;
        },

        onSelect: function() {
            this.disable();
            this.$scope.files.forEach(angular.bind(this, function(file) {
                var i;
                var add_file = true;
                var filesize = file.size / this.bite,
                    maxsize = this.maxFileSize / this.bite;

                if (filesize > maxsize) {
                    add_file = false;
                    this.notify.info("The file '" + file.name + "' is too large");
                }
                if (this.currentFiles) {
                    for (i = 0; i < this.currentFiles.length; i++) {

                        var current_file_name = this.currentFiles[i].name.replace(/\.[^/.]+$/, "");
                        var added_file_name = file.name.replace(/\.[^/.]+$/, "");
                        added_file_name = added_file_name.replace(/ /g, "_");

                        if (current_file_name === added_file_name) {
                            add_file = false;
                            this.notify.info("Cannot add duplicate files.");
                        }
                    }
                }
                for (i = 0; i < this.files.length; i++) {
                    if (this.files[i].name === file.name) {
                        add_file = false;
                        this.notify.info("Cannot add duplicate files.");
                    }
                }
                if (add_file === true) {
                    this.getBase64(file);
                }
                else {
                    var index = this.$scope.files.indexOf(file);
                    this.$scope.files.splice(index, 1);
                }
            }));
            //uploader needs timeout to update parent scope
            this.$timeout(angular.bind(this, function() {
                this.fileAdded(this.files);
            }), 500);
        },

        getBase64: function(file) {
            var reader = new FileReader();
            reader.readAsDataURL(file);

            reader.onload = angular.bind(this, function() {
                file.based64 = reader.result;
                file.window_url = this.$window.URL.createObjectURL(file);
                this.windowUrl = file.window_url;
                this.files.push(file);
            });

            reader.onerror = angular.bind(this, function(error) {
                this.notify.error("Error reading file" + file.name + ": " + error);
            });

            return reader.onload;
        },

        uploadFiles: function() {
            if (this.files.length > 0) {
                var file = this.files[0];
                var form = new FormData();
                form.append("file", file);
                this.addParams(form);
                this.callApi(form).then(
                    angular.bind(this, this.plFileUploaded, file),
                    angular.bind(this, this.plFileError, file)
                );
            }
        },

        callApi: function(form) {
            return this.$http.post(this.cfg.url, form, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            });
        },

        addParams: function(form) {
            for (var key in this.multipart_params) {
                form.append(key, this.multipart_params[key]);
            }
        },

        plFileUploaded: function(file, response) {
            this.removeFile(file);
            this.addedFiles.push(response.data);
            this.fileUploaded({
                file: response.data
            });
        },

        plFileError: function(file, response) {
            if (response.data) {
                var obj = response.data;
                if (obj.file && obj.file[0] === file.name + " already exists") {
                    this.notify.info("File with name: " + file.name + " already exists");
                }
            }
            this.removeFile(file);
            this.fileError({
                response: response
            });
        },

        click: function() {
            document.getElementById(this.cfg.id).click();
        }

    };

})();