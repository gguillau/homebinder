describe("UserLookupController", function() {
    var ctrl,
        controller,
        $q,
        $log,
        api,
        notify,
        resources,
        loading;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
        $q = $injector.get("$q");
        $log = $injector.get("$log");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");
        resources = $injector.get("hb.resources");
        loading = $injector.get("Loading");
        args = {};
    }));

    function createController() {
        ctrl = controller("UserLookupController", {
            "$log": $log,
            "hb.api": api,
            "Notify": notify,
            "hb.resources": resources,
            "Loading": loading
        });
    }

    describe("init", function() {
        it("sets the cfg", function() {
            createController();

            expect(ctrl.model.cfg).toEqual({});
        });
    });

    describe("lookupUser", function() {
        it("calls user all", function() {
            spyOn(api.user, "all").and.returnValue($q.when({ data: { items: [] } }));
            createController();
            ctrl.model.cfg.apr = true;
            ctrl.model.cfg.partnerId = 1;
            ctrl.model.cfg.role = "admin";
            ctrl.model.lookupUser();

            expect(api.user.all).toHaveBeenCalled();
        });
    });

    describe("mapLookupResponse", function() {
        it("returns mapped users", function() {
            createController();
            var array = [
                { id: 1, email: "test@gmail.com", partner_id: 1, partner_name: "Test Company", user_profile_attributes: { id: 1, company: "Test Company" } }
            ];
            var users = ctrl.model.mapLookupResponse({ data: { items: array } });

            expect(users.data[0]).toEqual({ id: 1, partner_id: 1, displayName: null, email: 'test@gmail.com', company: 'Test Company' });
        });
    });

    describe("userSelected", function() {
        it("calls cfg userSelected", function() {
            createController();
            ctrl.model.cfg.userSelected = function() {};
            spyOn(ctrl.model.cfg, "userSelected");
            ctrl.model.userSelected({});

            expect(ctrl.model.cfg.userSelected).toHaveBeenCalled();
        });

        it("clears the selection", function() {
            createController();
            ctrl.model.cfg.clearOnSelect = true;
            ctrl.model.userSelected({});

            expect(ctrl.model.selection).toBe(undefined);
        });
    });

});

describe('hbUserLookup', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        
        $scope.cfg = {};
        $scope.selection = {};
        
        element = $compile('<hb-user-lookup cfg="cfg" selection="selection"></hb-user-lookup>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the text', function() {
            expect(element.html()).toContain("Search for a user");
        });
    });
});