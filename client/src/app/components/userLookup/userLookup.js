angular
    .module("hb.components.userLookup", [])
    .directive("hbUserLookup", function() {
        return {
            restrict: "E",
            template: "<hb-lookup cfg=\"ctrl.model.lookupCfg\" selection=\"ctrl.selection\"></hb-lookup>",
            controller: "UserLookupController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {
                cfg: "=",
                selection: "=?"
            }
        };
    })
    .controller("UserLookupController", [
        "hb.api",
        "hb.resources",
        function(api, resources) {
            var that = this;
            var Model = function(cfg) {
                this.cfg = cfg || {};
                this.resources = resources.userLookup;
                this.lookupCfg = {
                    placeholder: this.resources.placeHolder,
                    refresh: angular.bind(this, this.lookupUser),
                    itemSelected: angular.bind(this, this.userSelected),
                    itemTemplate: this.cfg.itemTemplate || "components/userLookup/userLookupItem.tpl.html",
                    selectionTemplate: this.cfg.selectionTemplate || "components/userLookup/userLookupSelection.tpl.html"
                };
            };

            angular.extend(Model.prototype, {
                lookupUser: function(value) {
                    var opts = {
                        page: 1,
                        count: 50,
                        search: value
                    };
                    
                    if (this.cfg.partnerId) {
                        opts.searchMethod = "for_partner";
                        opts.partnerId = this.cfg.partnerId;
                    }
                    if (this.cfg.role) {
                        opts.role = this.cfg.role;
                    }
                    return api.user.all(opts).then(angular.bind(this, this.mapLookupResponse));
                },
                mapLookupResponse: function(response) {
                    return {
                        data: response.data.items.map(function(user) {
                            var mapped = {
                                id: user.id,
                                partner_id: user.partner_id,
                                displayName: user.user_profile_attributes.first_name ? user.user_profile_attributes.first_name + " " + user.user_profile_attributes.last_name : null,
                                email: user.email,
                                company: this.cfg.company == "partner" ? user.partner_name : user.user_profile_attributes.company ? user.user_profile_attributes.company : null
                            };
                            return mapped;
                        }, this)
                    };
                },
                userSelected: function(user) {
                    if (this.cfg.userSelected) {
                        this.cfg.userSelected(user);
                    }
                    if (this.cfg.clearOnSelect) {
                        that.selection = null;
                    }
                }
            });

            this.model = new Model(that.cfg);
        }
    ]);