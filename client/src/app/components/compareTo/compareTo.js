angular
    .module("hb.components.compareTo", [])
    .directive("compareTo", function() {
        return {
            require: "ngModel",
            scope: {
                otherString: "=compareTo"
            },
            link: function(scope, element, attributes, ngModel) {
            
                ngModel.$validators.compareTo = function(thisString) {
                    return thisString == scope.otherString;
                };
 
                scope.$watch("otherString", function() {
                    ngModel.$validate();
                });
            }
        };
    });