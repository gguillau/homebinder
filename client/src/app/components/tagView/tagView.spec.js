describe("TagViewController", function() {
    var $rootScope,
        $location,
        ctrl;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        $q = _$injector_.get("$q");

        $log = {
            error: function() {},
            warn: function() {}
        };
        $location = {
            url: function() {}
        };
    }));

    function createController() {
        ctrl = $controller("TagViewController", {
            "$log": $log,
            "$location": $location
        });
        $rootScope.$apply();
    }

    describe("populate", function() {
        it("loads all tags when no filter is provided", function() {
            createController();

            var tags = [
                { tag: "area_1", metadata: JSON.stringify({ name: "bar" }) },
                { tag: "appliance_1", metadata: JSON.stringify({ other: 9 }) },
                { tag: "structure_1" },
                { tag: "custom" }
            ];

            ctrl.cfg = {
                binderId: 1,
                linkToItems: false
            };
            ctrl.tags = tags;
            ctrl.populate();

            expect(ctrl.items.length).toEqual(2);
            expect(ctrl.items[0].name).toEqual("bar");
            expect(ctrl.items[0].link).toBeNull();
            expect(ctrl.items[1].name).toEqual("custom");
            expect(ctrl.items[1].link).toBeUndefined();
        });

        it("filters tags", function() {
            createController();

            var tags = [
                { tag: "area_1", metadata: JSON.stringify({ name: "bar" }) },
                { tag: "appliance_1", metadata: JSON.stringify({ other: 9 }) },
                { tag: "structure_1" },
                { tag: "custom" }
            ];

            ctrl.cfg = {
                binderId: 1,
                linkToItems: false,
                filter: ["area"]
            };
            ctrl.tags = tags;
            ctrl.populate();

            expect(ctrl.items.length).toEqual(1);
            expect(ctrl.items[0].name).toEqual("custom");
        });

        it("links to items", function() {
            createController();

            var tags = [
                { tag: "area_1", metadata: JSON.stringify({ name: "bar" }) },
                { tag: "appliance_1", metadata: JSON.stringify({ other: 9 }) },
                { tag: "structure_1" },
                { tag: "area_2", metadata: JSON.stringify({ name: "apple" }) },
                { tag: "area_3", metadata: JSON.stringify({ name: "bar" }) }
            ];

            ctrl.cfg = {
                binderId: 1,
                linkToItems: true
            };
            ctrl.tags = tags;
            ctrl.populate();

            expect(ctrl.items.length).toEqual(3);
            expect(ctrl.items[0].name).toEqual("apple");
            expect(ctrl.items[0].link).toEqual("/binders/1/areas");
            expect(ctrl.items[1].name).toEqual("bar");
            expect(ctrl.items[1].link).toEqual("/binders/1/areas");
            expect(ctrl.items[2].name).toEqual("bar");
        });
    });

    describe('click', function() {
        it("navigates to the tags url", function() {
            createController();

            spyOn($location, "url");
            ctrl.onClickItem({ name: "link", link: "url" });
            expect($location.url).toHaveBeenCalledWith("url");
        });

        it("does not navigate when no url is provided", function() {
            createController();

            spyOn($location, "url");
            ctrl.onClickItem({ name: "link", link: null });
            expect($location.url).not.toHaveBeenCalled();
        });
    });

    describe('pluralizeItem', function() {
        it("returns finishes", function() {
            createController();

            var pluralized = ctrl.pluralizeItem("finish");
            expect(pluralized).toEqual("finishes");
        });

        it("returns binder_contractors", function() {
            createController();

            var pluralized = ctrl.pluralizeItem("bindercontractor");
            expect(pluralized).toEqual("binder_contractors");
        });

        it("returns maintenanceItems", function() {
            createController();

            var pluralized = ctrl.pluralizeItem("maintenanceitem");
            expect(pluralized).toEqual("maintenanceItems");
        });

        it("returns inventory_items", function() {
            createController();

            var pluralized = ctrl.pluralizeItem("inventoryitem");
            expect(pluralized).toEqual("inventory_items");
        });
    });

    describe('generateLink', function() {
        it("returns default", function() {
            createController();

            var link = ctrl.generateLink(1, "custom");
            expect(link).toEqual("/customs/1");
        });
    });
});

describe('hbTagView', function() {
    var $scope, $compile;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.cfg = {};
        $scope.tags = [];

        $compile('<hb-tag-view tags="tags" cfg="cfg"></hb-tag-view>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the hb-tag-view directive', function() {
        it('sets the cfg refresh', function() {
            expect($scope.cfg.refresh).not.toBeNull();
        });
    });
});