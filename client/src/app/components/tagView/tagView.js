(function() {
    "use strict";

    angular
        .module("hb.components.tagView", [])
        .directive("hbTagView", function() {
            return {
                restrict: "E",
                templateUrl: "components/tagView/tagView.tpl.html",
                controller: "TagViewController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    cfg: "=",
                    tags: "="
                }
            };
        })
        .controller("TagViewController", [
            "$log",
            "$location",
            Controller
        ]);

    function Controller($log, $location) {
        this.$log = $log;
        this.$location = $location;
        this.items = [];
        this.populate();
        if (this.cfg) {
            this.cfg.refresh = angular.bind(this, this.populate);
        }
    }

    Controller.prototype = {
        populate: function() {
            var type,
                id,
                data,
                item,
                name;

            this.items = [];

            if (!this.tags) {
                return;
            }

            this.tags.forEach(function(t) {
                try {
                    // check for a _ in the tag. If there it's a generated tag with id
                    if (t.tag.lastIndexOf("_") > -1) {
                        // check for metadata. if no metadata all we have is something like
                        // area_51 so don't show it
                        if (t.metadata) {
                            // get the type of tag
                            type = t.tag.substr(0, t.tag.lastIndexOf("_")).toLowerCase();
                            // check if the tag should be included
                            if (this.includeTag(type)) {
                                // get the id of the object the tag references
                                id = t.tag.substr(t.tag.lastIndexOf("_") + 1);
                                // get the metadata
                                data = JSON.parse(t.metadata);
                                // get the name for the tag
                                name = data.name;
                                // if there is a name we can create a tag. if not ignore the tag
                                if (name) {
                                    // create the item for the list
                                    item = {
                                        name: name,
                                        link: this.cfg.linkToItems ? this.generateLink(id, type) : null
                                    };
                                    // add the item
                                    this.items.push(item);
                                }
                            }
                        }
                    }
                    else {
                        this.items.push({ name: t.tag });
                    }

                }
                catch (e) {
                    this.$log.warn("Error loading tag: " + e);
                }
            }, this);

            // Sort the tags by name
            this.items.sort(function(a, b) {
                if (a.name > b.name) {
                    return 1;
                }

                if (a.name < b.name) {
                    return -1;
                }
                return 0;
            });
        },

        includeTag: function(tagType) {
            if (!this.cfg.filter || this.cfg.filter.length === 0) {
                return true;
            }

            return !this.cfg.filter.includes(tagType);
        },

        generateLink: function(id, type) {
            switch (type) {
                case "area":
                case "appliance":
                case "bindercontractor":
                case "document":
                case "finish":
                case "image":
                case "inventoryitem":
                case "maintenanceitem":
                case "paint":
                case "project":
                case "receipt":
                case "structure":
                    return "/binders/" + this.cfg.binderId + "/" + this.pluralizeItem(type); // + "/" + id;
                default:
                    return "/" + this.pluralizeItem(type) + "/" + id;
            }
        },

        onClickItem: function(item) {
            if (item.link) {
                this.$location.url(item.link);
            }
        },

        pluralizeItem: function(type) {
            switch (type) {
                case "finish":
                    return "finishes";
                case "bindercontractor":
                    return "binder_contractors";
                case "maintenanceitem":
                    return "maintenanceItems";
                case "inventoryitem":
                    return "inventory_items";
                default:
                    return type + "s";
            }
        }
    };
})();