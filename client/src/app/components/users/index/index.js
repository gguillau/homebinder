(function() {
    "use strict";

    angular.module('hb.components.users.index', [])
        .directive("hbUsersIndex", function() {
            return {
                restrict: "E",
                templateUrl: "components/users/index/index.tpl.html"
            };
        })
        .factory("UsersIndexController", [
            'hb.framework.indexBase',
            'hb.api',
            'hb.resources',
            'WidgetModal',
            'AfterLogin',
            'Session',
            'Address',
            function(IndexBase, api, resources, widgetModal, AfterLogin, Session, Address) {
                var Model = function() {
                    // call the parent class
                    IndexBase.call(this);

                    // apply widget specific resource strings
                    this.resources = angular.extend({}, this.resources, resources.usersIndex);
                    this.api = api;
                    this.AfterLogin = AfterLogin;
                    this.Address = Address;
                    this.statesOptions = [];
                    this.countriesOptions = [{
                        name: "All Countries",
                        value: undefined
                    }];
                    this.countriesOptions = this.countriesOptions.concat(this.Address.countries());
                    this.statesOptions = this.Address.getStatesAndProvinces();
                    this.currentUser = Session.getUser();
                    // set the refresh API call
                    this.refreshCall = api.user.all;
                    // set the delete API call
                    this.deleteCall = api.user.destroy;
                    // set the name property used in the delete message
                    this.nameProperty = "email";
                    this.editState = "admin.users_edit";
                    this.editParams = {};
                    this.editParamsProperty = "userId";
                    this.newState = "admin.users_new";
                    this.maxSize = 10;
                    this.filters = [{
                        name: null,
                        value: "All users"
                    }, {
                        name: "inspector",
                        value: "Inspectors"
                    }, {
                        name: "broker",
                        value: "Brokers"
                    }, {
                        name: "agent",
                        value: "Agents"
                    }, {
                        name: "admin",
                        value: "Admins"
                    }, {
                        name: "homeowner",
                        value: "Homeowners"
                    }, {
                        name: "lender",
                        value: "Lenders"
                    }, {
                        name: "insurance",
                        value: "Insurance Users"
                    }, {
                        name: "hoa",
                        value: "HOAs"
                    }, {
                        name: "apr_reviewer",
                        value: "APR Reviewers"
                    }, {
                        name: "builder",
                        value: "Builders"
                    }, {
                        name: "property_manager",
                        value: "Property Managers"
                    }, {
                        name: "homepro",
                        value: "Home Pros"
                    }];
                    this.current_filter = null;
                    this.sortOptions = [{
                        orderBy: "users.created_at",
                        order: "desc",
                        desc: this.resources.creationDate + " - " + this.resources.descending
                    }, {
                        orderBy: "users.created_at",
                        order: "asc",
                        desc: this.resources.creationDate + " - " + this.resources.ascending
                    }, {
                        orderBy: "users.email",
                        order: "desc",
                        desc: this.resources.userEmail + " - " + this.resources.descending
                    }, {
                        orderBy: "users.email",
                        order: "asc",
                        desc: this.resources.userEmail + " - " + this.resources.ascending
                    }, {
                        orderBy: "users.sign_in_count",
                        order: "desc",
                        desc: this.resources.signIn + " - " + this.resources.descending
                    }, {
                        orderBy: "users.sign_in_count",
                        order: "asc",
                        desc: this.resources.signIn + " - " + this.resources.ascending
                    }];
                    this.sortOption = this.sortOptions[0];
                    this.orderBy = this.sortOption.orderBy;
                    this.order = this.sortOption.order;
                    this.userArgs = {
                        role: undefined,
                        state: undefined,
                        country: undefined,
                        create_method: undefined
                    };
                    this.rolesOptions = [{
                        name: undefined,
                        value: "All roles"
                    }, {
                        name: "homeowner",
                        value: "Homeowner"
                    }, {
                        name: "inspector",
                        value: "Inspector"
                    }, {
                        name: "admin",
                        value: "Admin"
                    }, {
                        name: "broker",
                        value: "Broker"
                    }, {
                        name: "agent",
                        value: "Agent"
                    }, {
                        name: "lender",
                        value: "Lenders"
                    }, {
                        name: "apr_reviewer",
                        value: "APR Reviewers"
                    }, {
                        name: "builder",
                        value: "Builders"
                    }, {
                        name: "property_manager",
                        value: "Property Managers"
                    }, {
                        name: "homepro",
                        value: "Home Pros"
                    }];
                    this.createOptions = [{
                        name: undefined,
                        value: "All Methods"
                    }, {
                        name: "register",
                        value: "Register"
                    }, {
                        name: "share",
                        value: "Share"
                    }, {
                        name: "inspector",
                        value: "Inspector"
                    }, {
                        name: "broker",
                        value: "Broker"
                    }, {
                        name: "agent",
                        value: "Agent"
                    }, {
                        name: "transfer",
                        value: "Transfer"
                    }, {
                        name: "admin",
                        value: "Admin"
                    }];
                    this.headerOptions = [];
                    this.columns = {
                        id: true,
                        name: true,
                        email: true,
                        phone: true,
                        role: true,
                        sign_in_count: true
                    };
                    // set page specific values on toolbar
                    angular.extend(this.toolbarCfg, {
                        title: this.resources.title,
                        sort: {
                            title: this.resources.sortBy,
                            refresh: angular.bind(this, this.refresh),
                            sortOptions: this.sortOptions,
                            sortOption: this.sortOption,
                            execute: angular.bind(this, this.sort)
                        },
                        filter: {
                            popoverCfg: {
                                title: "Filter",
                                directive: 'hb-filter',
                                templateUrl: "components/filter/filter.tpl.html",
                                directiveCfg: {
                                    filterButtonLabel: "Filter",
                                    filter: this.current_filter,
                                    filters: this.filters,
                                    search: angular.bind(this, this.filter)
                                },
                                popoverClass: "filter-popover"
                            }
                        },
                        button: {
                            title: this.resources.newButton,
                            click: angular.bind(this, this.newItem)
                        }
                    });
                    this.settings = {
                        canImpersonate: false,
                        canEdit: true,
                        canViewHeadshot: false
                    };
                    this.setHeaders();
                };

                Model.prototype = Object.create(IndexBase.prototype);

                angular.extend(Model.prototype, {

                    impersonate: function(user) {
                        this.loading.show(this.resources.impersonateLoading + " " + user.email + "...");

                        if (user.role == "agent") {
                            this.$state.go("agent.binders", { userId: user.id });
                        }
                        else {
                            this.$state.go("partner.binders", { partnerId: user.partner_id });
                        }

                    },

                    canImpersonate: function(user) {
                        if (this.settings.canImpersonate) {
                            switch (user.role) {
                                case "agent":
                                    return true;
                                case "broker":
                                case "inspector":
                                    if (!user.partner_id) {
                                        return false;
                                    }
                                    return true;
                                case "admin":
                                    return false;
                                default:
                                    return false;
                            }
                        }
                        return false;
                    },

                    invite: function(user) {
                        this.loading.show(this.resources.inviting + " " + user.email + "...");
                        this.api.user.invite(user.id)
                            .then(
                                angular.bind(this, this.onInviteSuccess),
                                angular.bind(this, this.onInviteError)
                            );
                    },

                    onInviteSuccess: function(response) {
                        this.notify.info("Invite sent!");
                        this.loading.close();
                    },

                    onInviteError: function(response) {
                        this.$log.error(response);
                        this.notify.error(response.data);
                        this.loading.close();
                    },

                    canInvite: function(user) {
                        if (user.sign_in_count < 1) {
                            switch (user.role) {
                                case "homeowner":
                                    return false;
                                default:
                                    return true;
                            }
                        }
                        else {
                            return false;
                        }
                    },

                    canViewHeadshot: function() {
                        return this.settings.canViewHeadshot;
                    },

                    setHeaders: function() {
                        this.headers = this.resources.userAttributes.map(angular.bind(this, function(attribute) {
                            var sortable = false,
                                orderBy = null,
                                sorted = false;
                            if (attribute === "ID") {
                                sorted = true;
                                sortable = true;
                                orderBy = "users.id";
                            }
                            else if (attribute === "Name") {
                                sortable = true;
                                orderBy = "user_profiles.first_name";
                            }
                            else if (attribute === "Email") {
                                sortable = true;
                                orderBy = "users.email";
                            }
                            else if (attribute === "Role") {
                                sortable = true;
                                orderBy = "users.role";
                            }
                            else if (attribute === "Sign In Count") {
                                sortable = true;
                                orderBy = "users.sign_in_count";
                            }
                            return {
                                name: attribute,
                                sortable: sortable,
                                sorted: sorted,
                                orderBy: orderBy,
                                order: "desc"
                            };
                        }));
                        this.headerOptions = angular.copy(this.headers);
                        this.headerOptions.push({
                            name: "Last Sign In",
                            sortable: true,
                            sorted: false,
                            orderBy: "users.last_sign_in_at",
                            order: "desc"
                        });
                        this.removeHeaders();
                        this.sortOption = this.headers[0];
                    },

                    removeHeaders: function() {

                    },

                    addQueryArgs: function() {
                        this.addArgs("role");
                        this.addArgs("state");
                        this.addArgs("country");
                        this.addArgs("create_method");
                    },

                    addArgs: function(arg) {
                        if (this.userArgs[arg]) {
                            this.queryArgs[arg] = this.userArgs[arg];
                        }
                        else {
                            delete this.queryArgs[arg];
                        }
                    },

                    canRemoveUsers: function() {
                        if (this.currentUser.role === "admin") {
                            return true;
                        }
                        else if (this.currentUser.partner_role && this.currentUser.partner_role === "admin") {
                            return true;
                        }
                        return false;
                    }
                });

                return Model;
            }

        ]);
})();