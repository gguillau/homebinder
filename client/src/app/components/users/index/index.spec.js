describe("UsersIndexController", function() {
    var UsersIndexController,
        TestClass,
        user,
        api,
        $q;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
        // mock the AfterLogin service
        $provide.factory("AfterLogin", function() {
            return {
                go: function() {}
            };
        });

        // mock the Session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });

    }));
    // get the base class under test
    beforeEach(inject(function($injector) {
        UsersIndexController = $injector.get("UsersIndexController");
        api = $injector.get("hb.api");
        $q = $injector.get("$q");
    }));

    function createTestClass() {
        TestClass = function() {
            UsersIndexController.call(this);
            this.currentUser = { role: "admin" };
            this.resources = angular.extend({}, this.resources, {
                loading: "loading",
                loadError: "load error",
                confirmDelete: "confirm",
                deleteMsg1: "a ",
                deleteMsg2: " b",
                deleteSuccess: "deleted",
                deleteError: "delete error",
                saveSuccess: "SAVE",
                impersonateLoading: "LO"
            });
            this.indexState = "admin.users";
            this.indexParams = {};
            this.nameProperty = "name";
        };

        TestClass.prototype = Object.create(UsersIndexController.prototype);

        return new TestClass();
    }

    user = {
        impersonateUser: function() {},
        impersonate: function() {}
    };

    describe("impersonate", function() {
        it("impersonates the user", inject(function($q, Loading, $rootScope, $state) {
            spyOn($state, "go");

            var model = createTestClass();
            model.api.user = user;
            $rootScope.$apply();
            model.impersonate({ email: "impersonate@gmail.com", role: "agent", id: 1 });

            $rootScope.$apply();
            expect($state.go).toHaveBeenCalled();
        }));

        it("impersonates the user", inject(function($q, Loading, $rootScope, $state) {
            spyOn($state, "go");

            var model = createTestClass();
            model.api.user = user;
            $rootScope.$apply();
            model.impersonate({ email: "impersonate@gmail.com", role: "inspector", id: 1 });

            $rootScope.$apply();
            expect($state.go).toHaveBeenCalled();
        }));

    });

    describe("setHeaders", function() {
        it("creates the headers", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.setHeaders();

            expect(model.headers.length).toEqual(6);
        }));
    });

    describe("invite", function() {
        it("calls user invite", inject(function() {
            spyOn(api.user, "invite").and.returnValue($q.when({ data: {} }));

            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.invite({ id: 1 });

            expect(api.user.invite).toHaveBeenCalled();
        }));
    });

    describe("onInviteSuccess", function() {
        it("calls notify info", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            spyOn(model.notify, "info");

            model.onInviteSuccess({ data: {} });

            expect(model.notify.info).toHaveBeenCalled();
        }));
    });

    describe("onInviteError", function() {
        it("calls notify error", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            spyOn(model.notify, "error");

            model.onInviteError({ data: "error" });

            expect(model.notify.error).toHaveBeenCalled();
        }));
    });

    describe("canInvite", function() {
        it("returns false", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            var value = model.canInvite({
                role: "admin"
            });

            expect(value).toEqual(false);
        }));

        it("returns true", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            var value = model.canInvite({
                sign_in_count: 0,
                role: "inspector"
            });

            expect(value).toEqual(true);
        }));

        it("returns false", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            var value = model.canInvite({
                sign_in_count: 0,
                role: "homeowner"
            });

            expect(value).toEqual(false);
        }));
    });

    describe("canRemoveUsers", function() {
        it("returns true", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            var value = model.canRemoveUsers();

            expect(value).toEqual(true);
        }));

        it("returns true", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "inspector",
                partner_role: "admin"
            };
            var value = model.canRemoveUsers();

            expect(value).toEqual(true);
        }));

        it("returns false", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "homeowner"
            };
            var value = model.canRemoveUsers();

            expect(value).toEqual(false);
        }));
    });

    describe("canImpersonate", function() {
        it("returns false", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.settings.canImpersonate = true;
            var value = model.canImpersonate({
                role: "admin"
            });

            expect(value).toEqual(false);
        }));

        it("returns false", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.settings.canImpersonate = true;
            var value = model.canImpersonate({
                role: "inspector"
            });

            expect(value).toEqual(false);
        }));

        it("returns true for agent when settings is true", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.settings.canImpersonate = true;
            var value = model.canImpersonate({
                role: "agent"
            });

            expect(value).toEqual(true);
        }));

        it("returns false for broker when settings is false", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.settings.canImpersonate = false;
            var value = model.canImpersonate({
                role: "broker",
                partner_id: 1
            });

            expect(value).toEqual(false);
        }));

        it("returns true for broker when settings is true", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.settings.canImpersonate = true;
            var value = model.canImpersonate({
                role: "broker",
                partner_id: 1
            });

            expect(value).toEqual(true);
        }));

        it("returns true for homeowner when settings is true", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.settings.canImpersonate = true;
            var value = model.canImpersonate({
                role: "homeowner"
            });

            expect(value).toEqual(false);
        }));

        it("returns false for homeowner when settings is false", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.settings.canImpersonate = false;
            var value = model.canImpersonate({
                role: "homeowner"
            });

            expect(value).toEqual(false);
        }));

    });

    describe("canViewHeadshot", function() {
        it("returns true", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.settings.canViewHeadshot = true;
            var value = model.canViewHeadshot();

            expect(value).toEqual(true);
        }));

        it("returns false", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.settings.canViewHeadshot = false;
            var value = model.canViewHeadshot();

            expect(value).toEqual(false);
        }));

    });

    describe("addQueryArgs", function() {
        it("adds the query arguments", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.userArgs["role"] = "inspector";
            model.userArgs["state"] = "MA";
            model.userArgs["country"] = "USA";
            model.userArgs["create_method"] = "api";
            model.addQueryArgs();

            expect(model.queryArgs["role"]).toEqual("inspector");
            expect(model.queryArgs["state"]).toEqual("MA");
            expect(model.queryArgs["country"]).toEqual("USA");
            expect(model.queryArgs["create_method"]).toEqual("api");
        }));

    });

    describe("addArgs", function() {
        it("adds the query argument", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.userArgs["role"] = "inspector";
            model.addArgs("role");

            expect(model.queryArgs["role"]).toEqual("inspector");
        }));

        it("adds the query argument", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.userArgs["state"] = "MA";
            model.addArgs("state");

            expect(model.queryArgs["state"]).toEqual("MA");
        }));

        it("adds the query argument", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.userArgs["country"] = "USA";
            model.addArgs("country");

            expect(model.queryArgs["country"]).toEqual("USA");
        }));

        it("adds the query argument", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.userArgs["create_method"] = "api";
            model.addArgs("create_method");

            expect(model.queryArgs["create_method"]).toEqual("api");
        }));

        it("deletes the query argument", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.userArgs["create_method"] = "api";
            model.addArgs("create_method");
            model.userArgs["create_method"] = undefined;
            model.addArgs("create_method");

            expect(model.queryArgs["create_method"]).toBe(undefined);
        }));
    });

});

describe('hbUsersIndex', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        element = $compile('<hb-users-index></hb-users-index>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('sets the text', function() {
            expect(element.html()).toContain("Refresh your items list");
        });
    });
});