describe("UsersNewController", function() {
    var UsersNewController,
        TestClass;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "homeowner" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
        // mock the notify service
        $provide.factory("Notify", function() {
            return {
                success: function() {},
                error: function() {}
            };
        });

    }));
    // get the base class under test
    beforeEach(inject(function($injector) {
        UsersNewController = $injector.get("UsersNewController");
    }));

    function createTestClass() {
        TestClass = function() {
            UsersNewController.call(this);

            this.resources = angular.extend({}, this.resources, {
                loading: "loading",
                loadError: "load error",
                confirmDelete: "confirm",
                deleteMsg1: "a ",
                deleteMsg2: " b",
                deleteSuccess: "deleted",
                deleteError: "delete error",
                saveSuccess: "SAVE"
            });
            this.currentUser = { role: "homeowner" };
            this.indexState = "admin.users";
            this.indexParams = {};
            this.nameProperty = "name";
        };

        TestClass.prototype = Object.create(UsersNewController.prototype);

        return new TestClass();
    }

    describe("beforeSave", function() {
        it("sets the updatedItem", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                email: "test@gmail.com",
                user_profile_attributes: {
                    id: 1,
                    address_attributes: {
                        id: 1
                    },
                    logo: "logo",
                    head_shot: "head_shot",
                    mobile_phone: {}
                }
            };
            model.beforeSave();

            expect(model.updatedItem.email).toEqual("test@gmail.com");
        }));
    });

    describe("onSave", function() {
        it("does not call the createCall", inject(function($q) {
            var model = createTestClass();

            spyOn(model, "createCall").and.returnValue($q.when({ data: {} }));
            model.currentUser = {
                role: "admin"
            };
            model.form = {
                $invalid: true
            };
            model.item = {
                email: "test@gmail.com",
                user_profile_attributes: { mobile_phone: {} }
            };
            model.onSave();

            expect(model.createCall).not.toHaveBeenCalled();
        }));

        it("calls the createCall", inject(function($q) {
            var model = createTestClass();

            spyOn(model, "createCall").and.returnValue($q.when({ data: {} }));
            model.country = { code: "1" };
            model.currentUser = {
                role: "admin"
            };
            model.form = {
                $invalid: false
            };
            model.item = {
                email: "test@gmail.com",
                user_profile_attributes: { mobile_phone: {} }
            };
            model.onSave();

            expect(model.createCall).toHaveBeenCalled();
        }));
    });

    describe("onCreateSuccess", function() {
        it("sends the user to the index state", inject(function(Notify, Loading, $state, Session) {
            var model = createTestClass();
            spyOn(Notify, "success");
            spyOn(Session, "getUser").and.returnValue({ role: "homeowner" });
            spyOn(Loading, "close");
            spyOn($state, "go");

            model.onCreateSuccess({ data: { id: 1, user_profile_attributes: { default_locale: "en-us" } } });

            expect(Loading.close).toHaveBeenCalled();
            expect($state.go).toHaveBeenCalled();
            expect(Notify.success).toHaveBeenCalledWith("SAVE");
        }));
    });

});