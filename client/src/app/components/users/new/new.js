(function() {
    "use strict";

    angular.module('hb.components.users.new', [])
        .factory("UsersNewController", [
            'UsersFormBase',
            "hb.framework.newBase",
            'hb.api',
            'hb.resources',
            'WidgetModal',
            "$stateParams",
            "Session",
            "Address",
            "Loading",
            function(FormBase, NewBase, api, resources, widgetModal, $stateParams, session, Address, loading) {
                var Model = function() {
                    // call the parent class
                    FormBase.call(this);
                    NewBase.call(this);
                    this.api = api;
                    this.updatedItem = {};
                    this.country = Address.findCountryByCode();
                    this.item = {
                        user_profile_attributes: {
                            mobile_phone: {

                            },
                            address_attributes: {
                                country: this.country.value
                            }
                        }
                    };
                    this.item.role = "homeowner";
                    /*jshint -W024*/ // apply widget specific resource strings
                    this.resources = angular.extend({}, this.resources, resources.usersNew);
                    this.resources = angular.extend({}, this.resources, resources.usersForm);
                    this.title = this.resources.title;
                    this.currentUser = session.getUser();
                    this.item.create_method = this.currentUser.role;
                    // set the create API call
                    this.createCall = api.user.create;
                    this.settings.readOnly = false;
                    this.settings.canCancel = false;
                    this.data = {
                        user: this.updatedItem
                    };
                };

                Model.prototype = Object.create(NewBase.prototype);

                angular.extend(Model.prototype, FormBase.prototype);

                angular.extend(Model.prototype, {

                    beforeSave: function() {
                        this.updatedItem = {
                            email: this.item.email,
                            password: this.item.password,
                            role: this.item.role,
                            user_profile_attributes: {
                                id: this.item.user_profile_attributes.id,
                                bio: this.item.user_profile_attributes.bio,
                                company: this.item.user_profile_attributes.company,
                                dob: this.item.user_profile_attributes.dob,
                                first_name: this.item.user_profile_attributes.first_name,
                                home_phone: this.item.user_profile_attributes.home_phone,
                                last_name: this.item.user_profile_attributes.last_name,
                                message: this.item.user_profile_attributes.message,
                                mobile_phone: "+" + this.country.code + this.item.user_profile_attributes.mobile_phone.national,
                                monthly_email: this.item.user_profile_attributes.monthly_email,
                                sex: this.item.user_profile_attributes.sex,
                                website: this.item.user_profile_attributes.website
                            }
                        };

                        if (this.item.user_profile_attributes.address_attributes) {
                            this.updatedItem.user_profile_attributes.address_attributes = {
                                id: this.item.user_profile_attributes.address_attributes.id,
                                address1: this.item.user_profile_attributes.address_attributes.address1,
                                address2: this.item.user_profile_attributes.address_attributes.address2,
                                city: this.item.user_profile_attributes.address_attributes.city,
                                country: this.item.user_profile_attributes.address_attributes.country,
                                state: this.item.user_profile_attributes.address_attributes.state,
                                zip: this.item.user_profile_attributes.address_attributes.zip
                            };
                        }

                        if (this.item.user_profile_attributes.logo) {
                            this.updatedItem.user_profile_attributes.logo = this.item.user_profile_attributes.logo;
                            this.updatedItem.user_profile_attributes.logo_file_name = this.item.user_profile_attributes.logo_file_name;
                        }

                        if (this.item.user_profile_attributes.head_shot) {
                            this.updatedItem.user_profile_attributes.head_shot = this.item.user_profile_attributes.head_shot;
                            this.updatedItem.user_profile_attributes.head_shot_file_name = this.item.user_profile_attributes.head_shot_file_name;
                        }

                        this.data.user = this.updatedItem;
                    },

                    // creates the item
                    onSave: function() {
                        // make sure the refresh API call is set
                        if (!this.createCall) {
                            throw "createCall not set";
                        }
                        this.form.$submitted = true;
                        if (this.form.$invalid) {
                            return;
                        }

                        this.beforeSave();

                        // display the loading message
                        loading.show(this.resources.savingMsg);

                        // get the item
                        this.createCall(this.data).then(
                            angular.bind(this, this.onCreateSuccess),
                            angular.bind(this, this.onCreateError)
                        );
                    },

                    // Create callback. Get the new item
                    onCreateSuccess: function(response) {
                        session.setLocale(response.data.user_profile_attributes.default_locale, false);
                        this.notify.success(this.resources.saveSuccess);
                        this.loading.close();
                        this.$state.go(this.indexState, this.indexParams);
                    },
                    
                    onCreateError: function(response) {
                         this.notify.error(response.data);
                        this.loading.close();
                    }
                });

                return Model;
            }
        ]);
})();
