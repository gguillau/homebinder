describe("UsersEditController", function() {
    var UsersEditController,
        TestClass;

    // load the homebinder module
    beforeEach(module("homebinder"));
    
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
        
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "homeowner" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
        
        // mock the notify service
        $provide.factory("Notify", function() {
            return {
                success: function() {},
                error: function() {}
            };
        });

    }));
    
    // get the base class under test
    beforeEach(inject(function($injector) {
        UsersEditController = $injector.get("UsersEditController");
    }));

    function createTestClass() {
        TestClass = function() {
            UsersEditController.call(this);

            this.resources = angular.extend({}, this.resources, {
                loading: "loading",
                loadError: "load error",
                confirmDelete: "confirm",
                deleteMsg1: "a ",
                deleteMsg2: " b",
                deleteSuccess: "deleted",
                deleteError: "delete error",
                saveSuccess: "SAVE"
            });
            this.currentUser = { role: "homeowner" };
            this.indexState = "admin.users";
            this.indexParams = {};
            this.nameProperty = "name";
        };

        TestClass.prototype = Object.create(UsersEditController.prototype);

        return new TestClass();
    }

    describe("beforeSave", function() {
        it("sets the updatedItem", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.country = { code: "1" };
            model.item = {
                email: "test@gmail.com",
                user_profile_attributes: {
                    mobile_phone: {},
                    address_attributes: {

                    },
                    logo: "logo",
                    head_shot: "head_shot"
                }
            };
            model.beforeSave();

            expect(model.updatedItem.email).toEqual("test@gmail.com");
        }));

    });

    describe("onRefreshSuccess", function() {
        it("sends the user to the index state", inject(function(Notify, Loading, $state, Session) {
            var model = createTestClass();
            spyOn(Session, "getUser").and.returnValue({ role: "homeowner" });
            spyOn(Loading, "close");

            model.onRefreshSuccess({ data: { user_profile_attributes: { dob: new Date(), address_attributes: {}, mobile_phone: {} } } });

            expect(Loading.close).toHaveBeenCalled();
        }));
    });

    describe("onSaveSuccess", function() {
        it("sends the user to the index state", inject(function(Notify, Loading, $state, Session) {
            var model = createTestClass();
            spyOn(Notify, "success");
            spyOn(Session, "getUser").and.returnValue({ role: "homeowner" });
            spyOn(Loading, "close");
            spyOn($state, "go");

            model.onSaveSuccess({ data: { id: 1, user_profile_attributes: { default_locale: "en-us" } } });

            expect(Loading.close).toHaveBeenCalled();
            expect($state.go).toHaveBeenCalled();
            expect(Notify.success).toHaveBeenCalledWith("SAVE");
        }));
    });
});