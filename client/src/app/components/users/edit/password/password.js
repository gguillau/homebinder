(function() {
    "use strict";

    angular
        .module("hb.components.users.edit.password", [])
        .directive("userEditPassword", function() {
            return {
                restrict: "E",
                templateUrl: "components/users/edit/password/password.tpl.html"
            };
        });
})();