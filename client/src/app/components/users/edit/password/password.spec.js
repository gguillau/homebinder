describe('userEditPassword', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        element = $compile('<user-edit-password></user-edit-password>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the userEditPassword directive', function() {
        it('shows the the userEditPassword template', function() {
            expect(element.html()).toContain("first_password");
        });
    });
});