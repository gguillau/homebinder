(function() {
    "use strict";

    angular
        .module("hb.components.users.edit.email", [])
        .directive("userEditEmail", function() {
            return {
                restrict: "E",
                templateUrl: "components/users/edit/email/email.tpl.html"
            };
        });
})();