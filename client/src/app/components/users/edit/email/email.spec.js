describe('userEditEmail', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        element = $compile('<user-edit-email></user-edit-email>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the userEditEmail directive', function() {
        it('shows the the userEditEmail template', function() {
            expect(element.html()).toContain("confirm_password");
        });
    });
});