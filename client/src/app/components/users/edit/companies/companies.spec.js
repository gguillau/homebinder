describe("UserCompaniesController", function() {
    var controller,
        base;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("hb.framework.indexBase");

        controller = $controller("UserCompaniesController", {
            "hb.framework.indexBase": base
        });
    }));

    describe("init", function() {
        it("sets the refreshCall", function() {
            expect(controller.model.refreshCall).toEqual(controller.model.api.partnerClient.all);
        });
    });

});

describe('userEditCompanies', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.partnerClient, "all").and.returnValue($q.when({ data: { items: [] } }));

        $compile('<user-edit-companies></user-edit-companies>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the user edit companies directive', function() {
        it('calls api partnerClient all', function() {
            expect(api.partnerClient.all).toHaveBeenCalled();
        });
    });
});