(function() {
	"use strict";

	angular.module('hb.components.users.edit.companies', [])
		.directive("userEditCompanies", function() {
			return {
				restrict: "E",
				templateUrl: "components/users/edit/companies/companies.tpl.html",
				controller: "UserCompaniesController",
				controllerAs: "ctrl"
			};
		})
		.controller("UserCompaniesController", [
			"hb.framework.indexBase",
			"hb.api",
			"hb.resources",
			"$stateParams",
			UserCompaniesController
		]);

	function UserCompaniesController(IndexBase, api, resources, $stateParams) {

		var Model = function() {
			// call the parent class
			IndexBase.call(this);
			this.api = api;
			// apply sellerReport specific resource strings
			this.resources = angular.extend({}, this.resources, resources.session);
			// set the refresh API call
			this.refreshCall = api.partnerClient.all;
			// set the delete API call
			this.deleteCall = api.partnerClient.destroy;
			// delete prop
			this.nameProperty = "created_at";
			this.maxSize = 10;
			this.sortOptions = [{
				orderBy: "partner_clients.created_at",
				order: "desc"
			}, {
				orderBy: "partner_clients.created_at",
				order: "asc"
			}];
			this.sortOption = this.sortOptions[0];
			this.orderBy = this.sortOption.orderBy;
			this.order = this.sortOption.order;
			this.queryArgs = {
				client_id: $stateParams.userId
			};
			this.refresh();
		};

		Model.prototype = Object.create(IndexBase.prototype);

		angular.extend(Model.prototype, {

		});

		this.model = new Model();
	}

})();