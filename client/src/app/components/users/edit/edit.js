(function() {
    "use strict";

    angular.module('hb.components.users.edit', [
            "hb.components.users.edit.email",
            "hb.components.users.edit.password",
            "hb.components.users.edit.sessions",
            "hb.components.users.edit.binders",
            "hb.components.users.edit.transfers",
            "hb.components.users.edit.shares",
            "hb.components.users.edit.companies",
            "hb.components.users.edit.partnerUsers",
            "hb.components.users.edit.locale"
        ])
        .factory("UsersEditController", [
            "UsersFormBase",
            "hb.framework.editBase",
            'hb.api',
            'hb.resources',
            'WidgetModal',
            "$stateParams",
            "Session",
            "Address",
            "$state",
            function(UsersFormBase, EditBase, api, resources, widgetModal, $stateParams, session, Address, $state) {
                var Model = function() {
                    
                    // call the parent class
                    UsersFormBase.call(this);
                    EditBase.call(this);
                    
                    // apply widget specific resource strings
                    this.resources = angular.extend({}, this.resources, resources.usersForm);
                    this.resources = angular.extend({}, this.resources, resources.validationErrors);
                    this.resources = angular.extend({}, this.resources, resources.usersEdit);
                    this.title = this.resources.title;
                    this.$stateParams = $stateParams;
                    this.$state = $state;
                    this.itemId = $stateParams.userId;
                    
                    // set the refresh API call
                    this.refreshCall = api.user.get;
                    
                    // set the save API call
                    this.saveCall = api.user.update;
                    
                    // make sure password is not required
                    this.settings.passwordRequired = false;
                };

                Model.prototype = Object.create(EditBase.prototype);
                angular.extend(Model.prototype, UsersFormBase.prototype);

                angular.extend(Model.prototype, {
                    
                    // Update the form before saving
                    beforeSave: function() {
                        
                        this.updatedItem = {
                            email: this.item.email,
                            password: this.item.password,
                            role: this.item.role,
                            user_profile_attributes: {
                                id: this.item.user_profile_attributes.id,
                                bio: this.item.user_profile_attributes.bio,
                                company: this.item.user_profile_attributes.company,
                                dob: this.item.user_profile_attributes.dob,
                                first_name: this.item.user_profile_attributes.first_name,
                                home_phone: this.item.user_profile_attributes.home_phone,
                                last_name: this.item.user_profile_attributes.last_name,
                                message: this.item.user_profile_attributes.message,
                                mobile_phone: "+" + this.country.code + this.item.user_profile_attributes.mobile_phone.national,
                                monthly_email: this.item.user_profile_attributes.monthly_email,
                                sex: this.item.user_profile_attributes.sex,
                                website: this.item.user_profile_attributes.website,
                                default_locale: this.item.user_profile_attributes.default_locale
                            }
                        };

                        if (this.item.user_profile_attributes.address_attributes) {
                            this.updatedItem.user_profile_attributes.address_attributes = {
                                id: this.item.user_profile_attributes.address_attributes.id,
                                address1: this.item.user_profile_attributes.address_attributes.address1,
                                address2: this.item.user_profile_attributes.address_attributes.address2,
                                city: this.item.user_profile_attributes.address_attributes.city,
                                country: this.item.user_profile_attributes.address_attributes.country,
                                state: this.item.user_profile_attributes.address_attributes.state,
                                zip: this.item.user_profile_attributes.address_attributes.zip
                            };
                        }

                        if (this.item.user_profile_attributes.logo) {
                            this.updatedItem.user_profile_attributes.logo = this.item.user_profile_attributes.logo;
                            this.updatedItem.user_profile_attributes.logo_file_name = this.item.user_profile_attributes.logo_file_name;
                        }

                        if (this.item.user_profile_attributes.head_shot) {
                            this.updatedItem.user_profile_attributes.head_shot = this.item.user_profile_attributes.head_shot;
                            this.updatedItem.user_profile_attributes.head_shot_file_name = this.item.user_profile_attributes.head_shot_file_name;
                        }
                    },

                    // Refresh callback. Updates the item list and totals
                    onRefreshSuccess: function(response) {
                        this.item = response.data;
                        this.country = Address.findCountryByCode(this.item.user_profile_attributes.address_attributes.country);
                        this.states = Address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                            return state.country.value === this.country.value;
                        }));
                        this.loading.close();
                    },

                    // Save success
                    onSaveSuccess: function(response) {
                        session.setLocale(response.data.user_profile_attributes.default_locale, false);
                        this.notify.success(this.resources.saveSuccess);
                        this.loading.close();
                        this.$state.go(this.indexState, this.indexParams);
                    },

                    updateLocale: function() {
                        // Set the updatedItem
                        this.beforeSave();

                        this.saveCall(this.item.id, this.updatedItem).then(
                            angular.bind(this, this.setLocale, this.item.user_profile_attributes.default_locale, true),
                            angular.bind(this, this.onSaveError)
                        );
                    }
                });

                return Model;
            }
        ]);

})();