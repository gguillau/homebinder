(function() {
    "use strict";

    angular
        .module("hb.components.users.edit.locale", [])
        .directive("userEditLocale", function() {
            return {
                restrict: "E",
                templateUrl: "components/users/edit/locale/locale.tpl.html"
            };
        });
})();