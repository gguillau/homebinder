describe('userEditLocale', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        element = $compile('<user-edit-locale></user-edit-lcoale>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the userEditLocale directive', function() {
        it('shows the the userEditLocaletemplate', function() {
            expect(element.html()).toContain("default_locale");
        });
    });
});