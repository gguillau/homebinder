(function() {
	"use strict";

	angular.module('hb.components.users.edit.partnerUsers', [])
		.directive("userEditPartnerUsers", function() {
			return {
				restrict: "E",
				templateUrl: "components/users/edit/partnerUsers/partnerUsers.tpl.html",
				controller: "UserPartnerUsersController",
				controllerAs: "ctrl"
			};
		})
		.controller("UserPartnerUsersController", [
			"hb.framework.indexBase",
			"hb.api",
			"hb.resources",
			"$stateParams",
			UserPartnerUsersController
		]);

	function UserPartnerUsersController(IndexBase, api, resources, $stateParams) {

		var Model = function() {
			// call the parent class
			IndexBase.call(this);
			this.api = api;
			// apply sellerReport specific resource strings
			this.resources = angular.extend({}, this.resources, resources.session);
			// set the refresh API call
			this.refreshCall = api.partnerUser.all;
			// set the delete API call
			this.deleteCall = api.partnerUser.destroy;
			// delete prop
			this.nameProperty = "created_at";
			this.maxSize = 10;
			this.sortOptions = [{
				orderBy: "partner_users.created_at",
				order: "desc"
			}, {
				orderBy: "partner_users.created_at",
				order: "asc"
			}];
			this.sortOption = this.sortOptions[0];
			this.orderBy = this.sortOption.orderBy;
			this.order = this.sortOption.order;
			this.queryArgs = {
				user_id: $stateParams.userId
			};
			this.refresh();
		};

		Model.prototype = Object.create(IndexBase.prototype);

		angular.extend(Model.prototype, {

			addQueryArgs: function() {
				this.addArgs("partnerId");
			},

			addArgs: function(arg) {
				if ($stateParams[arg]) {
					this.queryArgs[arg] = $stateParams[arg];
				}
				else {
					delete this.queryArgs[arg];
				}
			},

			updateUser: function(user) {
				this.loading.show("Loading...");
				var data = {
					id: user.id,
					access_repair_pricer: user.access_repair_pricer
				};
				this.api.partnerUser.update(data).then(
					angular.bind(this, this.updateSuccess),
					angular.bind(this, this.updateError));
			},

			updateSuccess: function(response) {
				this.loading.close();
			},

			updateError: function(response) {
				this.$log.error(response);
				this.notify.error(response.data);
				this.loading.close();
			}

		});

		this.model = new Model();
	}

})();