describe("UserPartnerUsersController", function() {
    var controller,
        base;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getJwt: function() {},
                getLocale: function() {
                    return null;
                },
                setLocale: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("hb.framework.indexBase");

        controller = $controller("UserPartnerUsersController", {
            "hb.framework.indexBase": base
        });
    }));

    describe("init", function() {
        it("sets the refreshCall", function() {
            expect(controller.model.refreshCall).toEqual(controller.model.api.partnerUser.all);
        });
    });

});

describe('userEditPartnerUsers', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.partnerUser, "all").and.returnValue($q.when({ data: { items: [] } }));

        $compile('<user-edit-partner-users></user-edit-partner-users>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the user edit partner-users directive', function() {
        it('calls api partnerClient all', function() {
            expect(api.partnerUser.all).toHaveBeenCalled();
        });
    });
});