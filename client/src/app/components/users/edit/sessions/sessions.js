(function() {
	"use strict";

	angular.module('hb.components.users.edit.sessions', [])
		.directive("userEditSessions", function() {
			return {
				restrict: "E",
				templateUrl: "components/users/edit/sessions/sessions.tpl.html",
				controller: "UserSessionsController",
				controllerAs: "ctrl"
			};
		})
		.controller("UserSessionsController", [
			"hb.framework.indexBase",
			"hb.api",
			"hb.resources",
			"$stateParams",
			UserSessionsController
		]);

	function UserSessionsController(IndexBase, api, resources, $stateParams) {

		var Model = function() {
			// call the parent class
			IndexBase.call(this);
			this.api = api;
			// apply sellerReport specific resource strings
			this.resources = angular.extend({}, this.resources, resources.session);
			// set the refresh API call
			this.refreshCall = api.sessions.all;
			// set the delete API call
			this.deleteCall = api.sessions.destroy;
			// delete prop
			this.nameProperty = "created_at";
			this.maxSize = 10;
			this.sortOptions = [{
				orderBy: "sessions.created_at",
				order: "desc"
			}, {
				orderBy: "sessions.created_at",
				order: "asc"
			}];
			this.sortOption = this.sortOptions[0];
			this.orderBy = this.sortOption.orderBy;
			this.order = this.sortOption.order;
			this.queryArgs = {
				user_id: $stateParams.userId
			};
			this.refresh();
		};

		Model.prototype = Object.create(IndexBase.prototype);

		angular.extend(Model.prototype, {

		});

		this.model = new Model();
	}

})();