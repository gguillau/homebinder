describe("UserSessionsController", function() {
    var controller,
        base;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getJwt: function() {},
                getLocale: function() {
                    return null;
                },
                setLocale: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("hb.framework.indexBase");

        controller = $controller("UserSessionsController", {
            "hb.framework.indexBase": base
        });
    }));

    describe("init", function() {
        it("sets the refreshCall", function() {
            expect(controller.model.refreshCall).toEqual(controller.model.api.sessions.all);
        });
    });

});

describe('userEditSessions', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.sessions, "all").and.returnValue($q.when({ data: { items: [] } }));

        $compile('<user-edit-sessions></user-edit-sessions>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the user edit sessions directive', function() {
        it('calls api sessions all', function() {
            expect(api.sessions.all).toHaveBeenCalled();
        });
    });
});