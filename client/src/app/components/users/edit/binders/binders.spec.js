describe("UserBindersController", function() {
    var controller,
        base;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("hb.framework.indexBase");

        controller = $controller("UserBindersController", {
            "hb.framework.indexBase": base
        });
    }));

    describe("init", function() {
        it("sets the refreshCall", function() {
            expect(controller.model.refreshCall).toEqual(controller.model.api.userBinder.all);
        });
    });

});

describe('userEditBinders', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.userBinder, "all").and.returnValue($q.when({ data: { items: [] } }));

        $compile('<user-edit-binders></user-edit-binders>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the user edit binders directive', function() {
        it('calls api userBinder all', function() {
            expect(api.userBinder.all).toHaveBeenCalled();
        });
    });
});