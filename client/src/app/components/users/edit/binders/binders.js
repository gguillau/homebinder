(function() {
	"use strict";

	angular.module('hb.components.users.edit.binders', [])
		.directive("userEditBinders", function() {
			return {
				restrict: "E",
				templateUrl: "components/users/edit/binders/binders.tpl.html",
				controller: "UserBindersController",
				controllerAs: "ctrl"
			};
		})
		.controller("UserBindersController", [
			"hb.framework.indexBase",
			"hb.api",
			"hb.resources",
			"$stateParams",
			UserBindersController
		]);

	function UserBindersController(IndexBase, api, resources, $stateParams) {

		var Model = function() {
			// call the parent class
			IndexBase.call(this);
			this.api = api;
			// apply sellerReport specific resource strings
			this.resources = angular.extend({}, this.resources, resources.session);
			// set the refresh API call
			this.refreshCall = api.userBinder.all;
			// set the delete API call
			this.deleteCall = api.userBinder.destroy;
			// delete prop
			this.nameProperty = "created_at";
			this.maxSize = 10;
			this.sortOptions = [{
				orderBy: "user_binders.created_at",
				order: "desc"
			}, {
				orderBy: "user_binders.created_at",
				order: "asc"
			}];
			this.sortOption = this.sortOptions[0];
			this.orderBy = this.sortOption.orderBy;
			this.order = this.sortOption.order;
			this.queryArgs = {
				user_id: $stateParams.userId
			};
			this.refresh();
		};

		Model.prototype = Object.create(IndexBase.prototype);

		angular.extend(Model.prototype, {

		});

		this.model = new Model();
	}

})();