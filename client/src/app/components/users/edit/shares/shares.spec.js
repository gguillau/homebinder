describe("UserSharesController", function() {
    var controller,
        base,
        api,
        $q,
        notify;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector) {
        $q = $injector.get("$q");
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        notify = $injector.get("Notify");

        controller = $controller("UserSharesController", {
            "hb.framework.indexBase": base,
            "Notify": notify
        });
    }));

    describe("resend", function() {
        it("calls api.share.resend", function() {
            spyOn(api.share, "resend").and.returnValue($q.when({
                data: "success"
            }));

            controller.model.resend({ id: 1 });

            expect(api.share.resend).toHaveBeenCalled();
        });
    });

    describe("resendSuccess", function() {
        it("calls notify info", function() {
            spyOn(notify, "info");

            controller.model.resendSuccess({ data: "" });

            expect(notify.info).toHaveBeenCalled();
        });
    });

    describe("onError", function() {
        it("calls notify error", function() {
            spyOn(notify, "error");

            controller.model.onError({ data: "" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe("canResend", function() {
        it("returns true", function() {
            spyOn(notify, "error");

            var result = controller.model.canResend({ status: "sent" });

            expect(result).toBe(true);
        });
        it("returns false", function() {
            spyOn(notify, "error");

            var result = controller.model.canResend({ status: "accepted" });

            expect(result).toBe(false);
        });
    });


});

describe('userEditShares', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.share, "all").and.returnValue($q.when({ data: { items: [] } }));

        $compile('<user-edit-shares></user-edit-shares>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the user edit shares directive', function() {
        it('calls api share all', function() {
            expect(api.share.all).toHaveBeenCalled();
        });
    });
});