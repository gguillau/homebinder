(function() {
	"use strict";

	angular.module('hb.components.users.edit.shares', [])
		.directive("userEditShares", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/share/index/index.tpl.html",
				controller: "UserSharesController",
				controllerAs: "ctrl"
			};
		})
		.controller("UserSharesController", [
			"SharesController",
			UserSharesController
		]);

	function UserSharesController(SharesController) {

		var Model = function() {
			// call the parent class
			SharesController.call(this);
			this.refresh();
		};

		Model.prototype = Object.create(SharesController.prototype);

		this.model = new Model();
	}

})();