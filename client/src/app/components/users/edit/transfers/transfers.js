(function() {
	"use strict";

	angular.module('hb.components.users.edit.transfers', [])
		.directive("userEditTransfers", function() {
			return {
				restrict: "E",
				templateUrl: "homebinders/homebinder/transfer/index/index.tpl.html",
				controller: "UserTransfersController",
				controllerAs: "ctrl"
			};
		})
		.controller("UserTransfersController", [
			"TransfersController",
			UserTransfersController
		]);

	function UserTransfersController(TransfersController) {

		var Model = function() {
			// call the parent class
			TransfersController.call(this);
			this.refresh();
		};

		Model.prototype = Object.create(TransfersController.prototype);

		this.model = new Model();
	}

})();