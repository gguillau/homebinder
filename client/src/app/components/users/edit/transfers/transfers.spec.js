describe("UserTransfersController", function() {
    var ctrl,
        controller;

    beforeEach(module("homebinder"));
    beforeEach(inject(function($controller, $injector) {
        controller = $controller;
    }));

    function createController() {
        ctrl = controller("UserTransfersController", {
        });
    }

    describe("init", function() {
        it("sets the settings", function() {
            createController();
            expect(ctrl.model.settings.canSearch).toBe(false);
        });
    });
});

describe('userEditTransfers', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.transfer, "all").and.returnValue($q.when({ data: { items: [] } }));

        $compile('<user-edit-transfers></user-edit-transfers>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the user edit transfers directive', function() {
        it('calls api transfer all', function() {
            expect(api.transfer.all).toHaveBeenCalled();
        });
    });
});