(function() {
    "use strict";

    angular.module('hb.components.users.form.profile', [])
        .directive("userEditProfile", function() {
            return {
                restrict: "E",
                templateUrl: "components/users/form/profile/profile.tpl.html"
            };
        });
})();
