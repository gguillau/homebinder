describe('userEditProfile', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;
        $scope.ctrl = {
            model: {
                title: "This is a title",
                item: {
                    email: "test@gmail.com"
                }
            }
        };
        element = $compile('<user-edit-profile></user-edit-profile>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the userEditProfile directive', function() {
        it('shows the the userEditProfile template', function() {
            expect(element.html()).toContain("This is a title");
        });
    });
});