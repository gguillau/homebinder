(function() {
    "use strict";

    angular.module('hb.components.users.form', [
            "hb.components.users.form.profile"
        ])
        .factory("UsersFormBase", [
            'hb.api',
            'hb.resources',
            'WidgetModal',
            "$stateParams",
            "Session",
            "Address",
            "$state",
            "hb.utils",
            function(api, resources, widgetModal, $stateParams, session, Address, $state, utils) {
                var Model = function() {
                    // default navigation links for partners and admins
                    this.navigation_links = [{
                        name: "Menu",
                        links: [
                            { name: "Profile", value: "profile", active: true }
                        ]
                    }];
                    this.currentLink = this.navigation_links[0].links[0];
                    this.Address = Address;
                    this.countries = Address.countries();
                    this.country = this.countries[0];
                    this.states = Address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                        return state.country.value === this.country.value;
                    }));
                    this.roles = [{
                        name: "homeowner",
                        value: "Homeowner"
                    }, {
                        name: "inspector",
                        value: "Inspector"
                    }, {
                        name: "broker",
                        value: "Broker"
                    }, {
                        name: "agent",
                        value: "Agent"
                    }];
                    // apply widget specific resource strings
                    this.resources = resources.usersForm;
                    this.resources = angular.extend({}, this.resources, resources.standardLanding);
                    this.currentUser = session.getUser();
                    this.images = [];
                    this.headshotUploader = {
                        fileTypes: "image",
                        hideUploadButton: true,
                        showTable: false,
                        selectButtonLabel: this.resources.headshotText,
                        fileLimit: true,
                        multiSelect: false,
                        hide_upload: false
                    };
                    this.companyLogoUploader = {
                        fileTypes: "image",
                        hideUploadButton: true,
                        showTable: false,
                        selectButtonLabel: this.resources.logoText,
                        fileLimit: true,
                        multiSelect: false,
                        hide_upload: false
                    };
                    this.settings = {
                        readOnly: true,
                        passwordRequired: true,
                        canEdit: true,
                        canCancel: true
                    };
                    this.utils = utils;
                    this.today = this.utils.utils.getToday(false);
                    this.next_month = this.utils.utils.getNextMonth();
                    this.yyyy = this.utils.utils.getYear();
                    this.date = [{
                        opened: false
                    }];
                    this.monthlyOptions = [{
                        name: this.resources.yes,
                        value: true
                    }, {
                        name: this.resources.no,
                        value: false
                    }];
                    this.sexOptions = [{
                        name: this.resources.male,
                        value: "m"
                    }, {
                        name: this.resources.female,
                        value: "f"
                    }];
                    this.localeOptions = [{
                        name: this.resources.english,
                        value: "en-us"
                    }, {
                        name: this.resources.french,
                        value: "fr-fr"
                    }, {
                        name: this.resources.spanish,
                        value: "sp-sp"
                    }];
                    this.setLocale = session.setLocale;
                };

                angular.extend(Model.prototype, {

                    changeLink: function(link) {
                        this.currentLink.active = false;
                        link.active = true;
                        this.currentLink = link;
                    },

                    showAdditionalFields: function() {
                        return this.item.role != "homeowner";
                    },

                    changeHeadshot: function() {
                        var pending = this.headshotUploader.api.pendingUploads();
                        this.item.user_profile_attributes.head_shot_file = pending[0].window_url;
                        this.item.user_profile_attributes.head_shot = pending[0].based64;
                        this.item.user_profile_attributes.head_shot_file_name = pending[0].name;
                    },

                    changeCompanyLogo: function(url) {
                        var pending = this.companyLogoUploader.api.pendingUploads();
                        this.item.user_profile_attributes.logo_file = pending[0].window_url;
                        this.item.user_profile_attributes.logo = pending[0].based64;
                        this.item.user_profile_attributes.logo_file_name = pending[0].name;
                    },

                    removeHeadShot: function() {
                        var pending = this.headshotUploader.api.pendingUploads();
                        if (pending.length > 0) {
                            this.headshotUploader.api.remove(pending[0]);
                        }
                        this.item.user_profile_attributes.head_shot = null;
                        this.item.user_profile_attributes.head_shot_file = null;
                        this.item.user_profile_attributes.head_shot_file_name = null;
                    },

                    removeCompanyLogo: function() {
                        var pending = this.companyLogoUploader.api.pendingUploads();
                        if (pending.length > 0) {
                            this.companyLogoUploader.api.remove(pending[0]);
                        }
                        this.item.user_profile_attributes.logo = null;
                        this.item.user_profile_attributes.logo_file = null;
                        this.item.user_profile_attributes.logo_file_name = null;
                    },

                    showPasswordField: function() {
                        // allow admins to change password
                        if (this.currentUser.role === "admin") {
                            return true;
                        }
                        // allow limited_admins to change password
                        if (this.currentUser.role === "limited_admin") {
                            return true;
                        }
                        // allow current users to edit their own passwords
                        else if (this.item.id) {
                            return this.item.id === this.currentUser.id;
                        }
                        return false;
                    },

                    companyRequired: function() {
                        switch (this.item.role) {
                            case "inspector":
                            case "agent":
                            case "admin":
                            case "broker":
                                return true;
                            default:
                                return false;
                        }
                    },

                    setCompany: function() {
                        if (this.item.role === "admin" && this.item.user_profile_attributes.company != "HomeBinder") {
                            this.item.user_profile_attributes.company = "HomeBinder";
                        }
                        else if (this.item.role != "admin" && this.item.user_profile_attributes.company === "HomeBinder") {
                            this.item.user_profile_attributes.company = null;
                        }
                    },

                    open: function(index) {
                        this.date[index].opened = true;
                    },

                    onSelect: function(item) {
                        if (item) {
                            this.country = item.country;
                            this.item.user_profile_attributes.address_attributes.country = item.country.value;
                        }
                    },

                    onSelectCountry: function(item) {
                        this.country = item;
                        this.states = Address.getStatesAndProvinces().filter(angular.bind(this, function(state) {
                            return state.country.value === this.country.value;
                        }));
                    }

                });

                return Model;
            }
        ]);
})();
