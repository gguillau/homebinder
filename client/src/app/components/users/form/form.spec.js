describe("UsersFormBase", function() {
    var UsersFormBase,
        TestClass,
        api;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // get the base class under test
    beforeEach(inject(function($injector) {
        UsersFormBase = $injector.get("UsersFormBase");
    }));

    function createTestClass() {
        TestClass = function() {
            UsersFormBase.call(this);

            this.resources = angular.extend({}, this.resources, {
                loading: "loading",
                loadError: "load error",
                confirmDelete: "confirm",
                deleteMsg1: "a ",
                deleteMsg2: " b",
                deleteSuccess: "deleted",
                deleteError: "delete error",
                saveSuccess: "SAVE",
                impersonateLoading: "LO"
            });
            this.indexState = "admin.users";
            this.indexParams = {};
            this.nameProperty = "name";
        };

        TestClass.prototype = Object.create(UsersFormBase.prototype);

        return new TestClass();
    }

    api = {
        pendingUploads: function() {},
        remove: function() {}
    };

    describe('#changeLink', function() {
        it('sets the current link', function() {
            var model = createTestClass();
            var link = { active: false };
            model.changeLink(link);

            expect(link.active).toBe(true);

        });

    });

    describe("changeHeadshot", function() {
        it("sets headshot to nil", inject(function($rootScope) {
            var model = createTestClass();
            model.headshotUploader.api = api;
            var pending = { window_url: "url", based64: "64", name: "name" };
            spyOn(api, "pendingUploads").and.returnValue([pending]);
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                user_profile_attributes: { head_shot: "", head_shot_file: "", head_shot_file_name: "" }
            };
            model.changeHeadshot();

            expect(model.item.user_profile_attributes.head_shot).toBe("64");
            expect(model.item.user_profile_attributes.head_shot_file).toBe("url");
            expect(model.item.user_profile_attributes.head_shot_file_name).toBe("name");
        }));

    });

    describe("changeCompanyLogo", function() {
        it("sets headshot to nil", inject(function($rootScope) {
            var model = createTestClass();
            model.companyLogoUploader.api = api;
            var pending = { window_url: "url", based64: "64", name: "name" };
            spyOn(api, "pendingUploads").and.returnValue([pending]);
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                user_profile_attributes: { logo: "", logo_file: "", logo_file_name: "" }
            };
            model.changeCompanyLogo();

            expect(model.item.user_profile_attributes.logo).toBe("64");
            expect(model.item.user_profile_attributes.logo_file).toBe("url");
            expect(model.item.user_profile_attributes.logo_file_name).toBe("name");
        }));

    });

    describe("removeHeadShot", function() {
        it("sets headshot to nil", inject(function($rootScope) {
            var model = createTestClass();
            model.headshotUploader.api = api;
            spyOn(api, "pendingUploads").and.returnValue([{ id: 1 }]);
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                user_profile_attributes: { head_shot: "", head_shot_file: "", head_shot_file_name: "" }
            };
            model.removeHeadShot();

            expect(model.item.user_profile_attributes.head_shot).toBe(null);
            expect(model.item.user_profile_attributes.head_shot_file).toBe(null);
            expect(model.item.user_profile_attributes.head_shot_file_name).toBe(null);
        }));

    });

    describe("removeCompanyLogo", function() {
        it("sets logo to nil", inject(function($rootScope) {
            var model = createTestClass();
            model.companyLogoUploader.api = api;
            spyOn(api, "pendingUploads").and.returnValue([{ id: 1 }]);
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                user_profile_attributes: { logo: "", logo_file: "", logo_file_name: "" }
            };
            model.removeCompanyLogo();

            expect(model.item.user_profile_attributes.logo).toBe(null);
            expect(model.item.user_profile_attributes.logo_file).toBe(null);
            expect(model.item.user_profile_attributes.logo_file_name).toBe(null);
        }));

    });

    describe("showAdditionalFields", function() {
        it("returns true", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                user_profile_attributes: {}
            };
            model.item.role = "admin";
            var value = model.showAdditionalFields();

            expect(value).toEqual(true);
        }));

        it("returns false", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                user_profile_attributes: {}
            };
            model.item.role = "homeowner";
            var value = model.showAdditionalFields();

            expect(value).toEqual(false);
        }));

    });

    describe("showPasswordField", function() {
        it("returns true", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                user_profile_attributes: {}
            };
            model.item.role = "admin";
            var value = model.showPasswordField();

            expect(value).toBe(true);
        }));

        it("returns true", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "limited_admin"
            };
            model.item = {
                user_profile_attributes: {}
            };
            model.item.role = "admin";
            var value = model.showPasswordField();

            expect(value).toBe(true);
        }));

        it("returns false", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                id: 1,
                role: "inspector"
            };
            model.item = {
                id: 2,
                user_profile_attributes: {}
            };
            model.item.role = "inspector";
            var value = model.showPasswordField();

            expect(value).toBe(false);
        }));

        it("returns false", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                id: 1,
                role: "inspector"
            };
            model.item = {
                id: 2,
                user_profile_attributes: {}
            };
            model.item.role = "agent";
            var value = model.showPasswordField();

            expect(value).toBe(false);
        }));

        it("returns false", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                id: 1,
                role: "inspector"
            };
            model.item = {
                user_profile_attributes: {}
            };
            model.item.role = "agent";
            var value = model.showPasswordField();

            expect(value).toBe(false);
        }));

        it("returns true", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                id: 1,
                role: "inspector"
            };
            model.item = {
                id: 1,
                user_profile_attributes: {}
            };
            model.item.role = "inspector";
            var value = model.showPasswordField();

            expect(value).toBe(true);
        }));
    });

    describe("companyRequired", function() {
        it("returns true", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                user_profile_attributes: {}
            };
            model.item.role = "admin";
            var value = model.companyRequired();

            expect(value).toBe(true);
        }));

        it("returns true", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                user_profile_attributes: {}
            };
            model.item.role = "agent";
            var value = model.companyRequired();

            expect(value).toBe(true);
        }));

        it("returns true", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                user_profile_attributes: {}
            };
            model.item.role = "broker";
            var value = model.companyRequired();

            expect(value).toBe(true);
        }));

        it("returns true", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                user_profile_attributes: {}
            };
            model.item.role = "inspector";
            var value = model.companyRequired();

            expect(value).toBe(true);
        }));

        it("returns true", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                user_profile_attributes: {}
            };
            model.item.role = "homeowner";
            var value = model.companyRequired();

            expect(value).toBe(false);
        }));

    });

    describe("setCompany", function() {
        it("sets the admin company name to HomeBinder", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                user_profile_attributes: {}
            };
            model.item.role = "admin";
            model.setCompany();

            expect(model.item.user_profile_attributes.company).toEqual("HomeBinder");
        }));

        it("sets the user company name to null when company name is HomeBinder", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.item = {
                user_profile_attributes: {}
            };
            model.item.role = "inspector";
            model.item.user_profile_attributes.company = "HomeBinder";
            model.setCompany();

            expect(model.item.user_profile_attributes.company).toBe(null);
        }));

    });

    describe("open", function() {
        it("set the date opened to true", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            model.open(0);

            expect(model.date[0].opened).toEqual(true);
        }));

    });

    describe("onSelect", function() {
        it("set the country", inject(function() {
            var model = createTestClass();
            model.currentUser = {
                role: "admin"
            };
            var item = { country: { value: "US" } };
            model.item = {
                user_profile_attributes: {
                    address_attributes: {

                    }
                }
            };
            model.onSelect(item);

            expect(model.country).toEqual(item.country);
        }));
    });

    describe("onSelectCountry", function() {
        it("sets the country", inject(function() {
            var model = createTestClass();
            model.onSelectCountry({ value: "MA" });

            expect(model.country.value).toEqual("MA");
        }));

    });

});