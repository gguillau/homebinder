(function() {
    "use strict";

    angular.module("hb.components.users", [
        "hb.components.users.index",
        "hb.components.users.edit",
        "hb.components.users.new",
        "hb.components.users.form",
        "hb.components.users.welcome"
    ]);
})();
