(function() {
	"use strict";

	angular
		.module("hb.components.users.welcome", [])
		.factory("UsersWelcomeController", [
			"$state",
			"$stateParams",
			"$log",
			"hb.api",
			"AfterLogin",
			"Notify",
			"hb.resources",
			"Loading",
			"hb.utils",
			"Session",
			"$location",
			function($state, $stateParams, $log, api, afterLogin, notify, resources, loading, utils, session, $location) {
				var WelcomeBase = function() {
					this.$state = $state;
					this.$stateParams = $stateParams;
					this.$log = $log;
					this.api = api;
					this.afterLogin = afterLogin;
					this.notify = notify;
					this.resources = resources.userWelcome;
					this.resources = angular.extend({}, this.resources, resources.standardLanding);
					this.validationErrors = resources.validationErrors;
					this.loading = loading;
					this.accessToken = $stateParams.accessToken;
					this.notes = [];
					this.transferText = null;
					this.year = new Date().getFullYear();
					this.to = null;
					this.newUser = true;
					this.utils = utils.utils;
					this.deviceName = "Unknown";
					this.deviceVersion = "Unknown";
					this.operatingSystem = "Unknown";
					this.localeOptions = [{
						name: this.resources.english,
						value: "en-us"
					}, {
						name: this.resources.french,
						value: "fr-fr"
					}, {
						name: this.resources.spanish,
						value: "sp-sp"
					}];
					this.setLocale = session.setLocale;
					this.default_locale = session.getLocale();
					this.$location = $location;
					this.init();
				};

				angular.extend(WelcomeBase.prototype, {

					init: function() {
						var device = this.utils.getDeviceInfo();
						this.deviceName = device[0];
						this.deviceVersion = device[1];
						this.operatingSystem = this.utils.getOperatingSystem();
						this.welcomeMethod = this.$location.search().notify;
					},

					refresh: function() {
						this.loading.show(this.resources.loading);
						this.api.user.welcome(this.accessToken, {welcomeMethod: this.welcomeMethod}).then(
							angular.bind(this, this.onRefreshSuccess),
							angular.bind(this, this.onRefreshError)
						);
					},

					onRefreshSuccess: function(response) {
						this.user = response.data;
						this.user.user_profile_attributes.default_locale = this.default_locale;
						this.to = this.user.email;
						this.loading.close();
						if (response.data.sign_in_count > 0) {
							this.$state.go("login");
						}
					},

					onRefreshError: function(response) {
						this.notify.info(this.resources.loadingError, 10000);
						this.$state.go("root");
						this.loading.close();
					},

					submit: function() {
						this.error = undefined;
						this.isBusy = true;

						var info = {
							user: {
								email: this.to,
								password: this.password,
								completed_onboarding: true,
								user_profile_attributes: {
									id: this.user.user_profile_attributes.id,
									default_locale: this.default_locale
								}
							},
							device_type: "web",
							device_name: this.deviceName,
							device_version: this.deviceVersion,
							operating_system: this.operatingSystem
						};

						this.loading.show(this.resources.signIn);
						this.api.user.register(info).then(
							angular.bind(this, this.onSubmitSuccess),
							angular.bind(this, this.onSubmitError)
						);
					},

					onSubmitSuccess: function(response) {
						this.loading.close();
						this.afterLogin.go();
					},

					onSubmitError: function(response) {
						this.notify.error(response.data);
						this.$log.error(response);
						this.isBusy = false;
						this.loading.close();
					}
				});

				return WelcomeBase;
			}

		]);
})();