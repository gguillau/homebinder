describe("UsersWelcomeController", function() {
    var UsersWelcomeController,
        TestClass,
        user;

    // load the homebinder module
    beforeEach(module("homebinder"));
    // mock the services used by the factory
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
        // mock the notify service
        $provide.factory("Notify", function() {
            return {
                success: function() {},
                error: function() {},
                info: function() {}
            };
        });
        // mock the AfterLogin service
        $provide.factory("AfterLogin", function() {
            return {
                go: function() {}
            };
        });

        // mock the Session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });

    }));
    // get the base class under test
    beforeEach(inject(function($injector) {
        UsersWelcomeController = $injector.get("UsersWelcomeController");
    }));

    function createTestClass() {
        TestClass = function() {
            UsersWelcomeController.call(this);
        };

        TestClass.prototype = Object.create(UsersWelcomeController.prototype);

        return new TestClass();
    }

    user = {
        welcome: function() {},
        bindersWaitingById: function() {},
        register: function() {}
    };

    describe("refresh", function() {
        it("gets the user", inject(function($q, Loading, $state, $rootScope) {
            spyOn(user, "welcome").and.returnValue($q.when({
                data: {
                    id: 1,
                    email: "test@gmail.com",
                    sign_in_count: 1,
                    user_profile_attributes: {
                        id: 1,
                        default_locale: "en-us"
                    }
                }
            }));
            spyOn($state, "go");

            spyOn(Loading, "show");
            spyOn(Loading, "close");

            var model = createTestClass();
            model.api.user = user;
            model.accessToken = 100;
            model.user = {
                id: 1,
                user_profile_attributes: {
                    id: 1,
                    default_locale: "en-us"
                }
            };
            $rootScope.$apply();
            model.refresh();

            $rootScope.$apply();
            expect(model.to).toEqual("test@gmail.com");
            expect(user.welcome).toHaveBeenCalledWith(100, { welcomeMethod: undefined });
            expect($state.go).toHaveBeenCalledWith("login");
            expect(Loading.show).toHaveBeenCalledWith("Checking user account...");
            expect(Loading.close).toHaveBeenCalled();
        }));

        it("returns an error", inject(function($q, Loading, $state, $rootScope, Notify) {
            spyOn(user, "welcome").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($state, "go");

            spyOn(Loading, "show");
            spyOn(Loading, "close");

            var model = createTestClass();
            model.api.user = user;
            model.accessToken = 100;
            model.user = {
                id: 1,
                user_profile_attributes: {
                    id: 1,
                    default_locale: "en-us"
                }
            };
            $rootScope.$apply();
            model.refresh();

            $rootScope.$apply();
            expect(user.welcome).toHaveBeenCalledWith(100, { welcomeMethod: undefined });
            expect($state.go).toHaveBeenCalledWith("root");
            expect(Loading.show).toHaveBeenCalledWith("Checking user account...");
            expect(Loading.close).toHaveBeenCalled();
        }));
    });

    describe("submit", function() {
        it("registers the user", inject(function($q, Loading, $state, $rootScope, AfterLogin) {
            spyOn(user, "register").and.returnValue($q.when({
                data: "user"
            }));
            spyOn(AfterLogin, "go");
            spyOn(Loading, "show");
            spyOn(Loading, "close");

            var model = createTestClass();
            model.api.user = user;
            model.accessToken = 100;
            model.user = {
                id: 1,
                user_profile_attributes: {
                    id: 1,
                    default_locale: "en-us"
                }
            };
            $rootScope.$apply();
            model.submit();

            $rootScope.$apply();
            expect(user.register).toHaveBeenCalled();
            expect(AfterLogin.go).toHaveBeenCalled();
        }));

        it("returns an error", inject(function($q, Loading, $state, $rootScope, AfterLogin, Notify) {
            spyOn(user, "register").and.returnValue($q.reject({
                data: "user"
            }));
            spyOn(Notify, "error");
            spyOn(Loading, "show");
            spyOn(Loading, "close");

            var model = createTestClass();
            model.api.user = user;
            model.accessToken = 100;
            model.user = {
                id: 1,
                user_profile_attributes: {
                    id: 1,
                    default_locale: "en-us"
                }
            };
            $rootScope.$apply();
            model.submit();

            $rootScope.$apply();
            expect(user.register).toHaveBeenCalled();
            expect(Notify.error).toHaveBeenCalled();
        }));

    });
});