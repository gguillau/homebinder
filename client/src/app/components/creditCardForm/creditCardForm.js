(function() {
    angular.module("hb.components.creditCardForm", [])
        .directive("hbCreditCardForm", [
            'ServiceKeyList',
            function(serviceKeyList) {
                return {
                    restrict: "E",
                    templateUrl: "components/creditCardForm/creditCardForm.tpl.html",
                    scope: {
                        cfg: '='
                    },
                    link: function($scope, element, attrs) {
                        serviceKeyList.keys().then(function(keys) {
                            // Custom styling can be passed to options when creating an Element.
                            // (Note that this demo uses a wider set of styles than the guide below.)
                            var style = {
                                base: {
                                    color: '#32325d',
                                    lineHeight: '18px',
                                    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                                    fontSmoothing: 'antialiased',
                                    fontSize: '16px',
                                    '::placeholder': {
                                        color: '#aab7c4'
                                    }
                                },
                                invalid: {
                                    color: '#fa755a',
                                    iconColor: '#fa755a'
                                }
                            };

                            var stripe = Stripe(keys.stripe),
                                elements = stripe.elements(),

                                card = elements.create("card", { style: style }),
                                $cardErrors = element.find("#card-errors");

                            // mount the stripe CC form
                            card.mount("#card-element");

                            // setup an API for external callers to generate the token
                            $scope.cfg.tokenize = function() {
                                stripe.createToken(card).then(tokenizeComplete);
                            };

                            function tokenizeComplete(result) {
                                if (result.token) {
                                    $scope.cfg.onReceiveToken(result.token.id);
                                }
                                else if (result.error) {
                                    if ($scope.cfg.onError) {
                                        $scope.cfg.onError(result.error.message);
                                    }
                                    else {
                                        $cardErrors.text(result.error.message);
                                    }
                                }
                            }
                        });

                    }
                };
            }
        ]);
})();
