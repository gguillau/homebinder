var Stripe;

describe('hbCreditCardForm', function() {

    var $scope, $compile, serviceKeyList, $q, element,
        dfd;

    beforeEach(module('homebinder'));

    describe("when successful", function() {
        beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
            serviceKeyList = _$injector_.get("ServiceKeyList");
            $q = _$injector_.get("$q");

            Stripe = function() {
                return {
                    createToken: function() {
                        dfd = $q.defer();
                        dfd.resolve({ token: { id: 1 } });
                        return dfd.promise;
                    },
                    elements: function() {
                        return {
                            create: function() {
                                return {
                                    mount: function() {}
                                };
                            },
                            find: function() {}
                        };
                    }
                };
            };

            spyOn(serviceKeyList, "keys").and.returnValue($q.when({ data: [] }));

            $scope = $rootScope.$new(), $compile = _$compile_;

            $scope.cfg = {
                tokenize: function() {},
                onReceiveToken: function() {}
            };

            element = $compile('<hb-credit-card-form cfg="cfg"></hb-credit-card-form>')($scope);

            $scope.$digest();
        }));

        describe('when the page compiles the directive', function() {
            it('sets the elements', function() {
                expect(element.html()).toContain("a Stripe Element will be inserted here");
            });
        });

        describe('when the page compiles the directive', function() {
            it('tokenizes', function() {
                spyOn($scope.cfg, "onReceiveToken");

                $scope.cfg.tokenize();
                $scope.$apply();

                expect($scope.cfg.onReceiveToken).toHaveBeenCalled();

            });

        });
    });

    describe("when unsuccessful and there is an error callback", function() {
        beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
            serviceKeyList = _$injector_.get("ServiceKeyList");
            $q = _$injector_.get("$q");

            Stripe = function() {
                return {
                    createToken: function() {
                        dfd = $q.defer();
                        dfd.resolve({ error: "error" });
                        return dfd.promise;
                    },
                    elements: function() {
                        return {
                            create: function() {
                                return {
                                    mount: function() {}
                                };
                            },
                            find: function() {}
                        };
                    }
                };
            };

            spyOn(serviceKeyList, "keys").and.returnValue($q.when({ data: [] }));

            $scope = $rootScope.$new(), $compile = _$compile_;

            $scope.cfg = {
                tokenize: function() {},
                onError: function() {}
            };

            element = $compile('<hb-credit-card-form cfg="cfg"></hb-credit-card-form>')($scope);

            $scope.$digest();
        }));

        describe('when the page compiles the directive', function() {
            it('tokenizes', function() {
                spyOn($scope.cfg, "onError");

                $scope.cfg.tokenize();
                $scope.$apply();

                expect($scope.cfg.onError).toHaveBeenCalled();

            });

        });
    });

    describe("when unsuccessful and no error callback", function() {
        beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
            serviceKeyList = _$injector_.get("ServiceKeyList");
            $q = _$injector_.get("$q");

            Stripe = function() {
                return {
                    createToken: function() {
                        dfd = $q.defer();
                        dfd.resolve({ error: "error" });
                        return dfd.promise;
                    },
                    elements: function() {
                        return {
                            create: function() {
                                return {
                                    mount: function() {}
                                };
                            },
                            find: function() {}
                        };
                    }
                };
            };

            spyOn(serviceKeyList, "keys").and.returnValue($q.when({ data: [] }));

            $scope = $rootScope.$new(), $compile = _$compile_;

            $scope.cfg = {
                tokenize: function() {}
            };

            element = $compile('<hb-credit-card-form cfg="cfg"></hb-credit-card-form>')($scope);

            $scope.$digest();
        }));

        describe('when the page compiles the directive', function() {
            it('tokenizes', function() {
                $scope.cfg.tokenize();
                $scope.$apply();
            });
        });
    });

});