(function() {
    "use strict";

    angular
        .module("hb.components.documentsNav", [])
        .directive("hbDocumentsNav", function() {
            return {
                restrict: "E",
                templateUrl: "components/documentsNav/documentsNav.tpl.html",
                controller: "DocumentsNavController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    active: "="
                }
            };
        })
        .controller("DocumentsNavController", [
            "hb.resources",
            DocumentsNavController
        ]);

    function DocumentsNavController(resources) {
        this.resources = resources.documentsNav;
        this.tabs = [{
                        id: "documents",
                        name: this.resources.homeDocuments,
                        resourceType: "document",
                        state: "binder.documents",
                        active: "inactive"
                    }, {
                        id: "images",
                        name: this.resources.photoGallery,
                        resourceType: "image",
                        state: "binder.images",
                        active: "inactive"
                    }, {
                        id: "permits",
                        name: this.resources.permits,
                        resourceType: "permit",
                        state: "binder.permits",
                        active: "inactive"
                    }, {
                        id: "receipts",
                        name: this.resources.receipts,
                        resourceType: "receipt",
                        state: "binder.receipts",
                        active: "inactive"
                    }];
        
        // Find the tab with the id that matches the active tab, and set its active attribute
        for (var i = 0; i < this.tabs.length; i++) {
            if (this.tabs[i].id === this.active) {
                this.tabs[i].active = "active";
            }
        }
    }
})();