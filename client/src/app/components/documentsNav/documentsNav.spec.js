describe("DocumentsNavController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("DocumentsNavController", {
        });
        $scope.ctrl = ctrl;

    }

    describe('ctrl.init', function() {
        it("sets the tabs", function() {
            createController();
            $rootScope.$apply();
            expect(ctrl.tabs.length).toEqual(4);
        });
    });

});

describe('hbDocumentsNav', function() {
    var $scope, $compile, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {

        $scope = $rootScope.$new(), $compile = _$compile_;

        element = $compile('<hb-documents-nav></hb-documents-nav>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the directive', function() {
        it('displays the navigation', function() {
            expect(element.html()).toContain("Documents");
        });
    });
});