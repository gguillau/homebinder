describe("LookupController", function() {
    var $rootScope,
        $q,
        $log,
        notify,
        resources,
        ctrl,
        select;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$compile_, _$controller_, _$injector_) {
        $controller = _$controller_;
        $rootScope = _$injector_.get("$rootScope");
        $q = _$injector_.get("$q");
        resources = _$injector_.get("hb.resources");

        $log = {
            error: function() {}
        };
        notify = {
            error: function() {}
        };

        select = function() {};
    }));

    function createController() {
        ctrl = $controller("LookupController", {
            "$scope": $rootScope.$new(),
            "$log": $log,
            "Notify": notify,
            "hb.resources": resources
        }, { select: select });
    }

    describe("initialization", function() {
        it("uses default settings", function() {
            createController();
            ctrl.cfg = {
                refresh: function() {}
            };
            ctrl.init();

            expect(ctrl.itemTemplate).toBeNull();
            expect(ctrl.selectionTemplate).toBeNull();
            expect(ctrl.delay).toEqual(250);
            expect(ctrl.inputLength).toEqual(1);
            expect(ctrl.placeholder).toEqual(resources.lookup.placeholder);
        });

        it("uses custom settings", function() {
            createController();
            ctrl.cfg = {
                refresh: function() {},
                itemTemplate: "itemTemplate",
                selectionTemplate: "selectionTemplate",
                delay: 500,
                inputLength: 3,
                placeholder: "hi"
            };
            ctrl.init();

            expect(ctrl.cfg.itemTemplate).toEqual("itemTemplate");
            expect(ctrl.cfg.selectionTemplate).toEqual("selectionTemplate");
            expect(ctrl.cfg.delay).toEqual(500);
            expect(ctrl.cfg.inputLength).toEqual(3);
            expect(ctrl.cfg.placeholder).toEqual("hi");
        });
    });

    describe('search', function() {
        it("clears options when search value is null", function() {
            createController();
            ctrl.cfg = {
                refresh: function(value) {}
            };
            ctrl.init();
            ctrl.options = [1, 2, 3];

            ctrl.search(null);
            expect(ctrl.options.length).toEqual(0);
        });

        it("clears options when search value is undefined", function() {
            createController();
            ctrl.cfg = {
                refresh: function(value) {}
            };
            ctrl.init();
            ctrl.options = [1, 2, 3];

            ctrl.search(undefined);
            expect(ctrl.options.length).toEqual(0);
        });

        it("clears options when search value is empty string", function() {
            createController();
            ctrl.cfg = {
                refresh: function(value) {}
            };
            ctrl.init();
            ctrl.options = [1, 2, 3];

            ctrl.search("");
            expect(ctrl.options.length).toEqual(0);
        });

        it("calls the provided service", function() {
            var called = false,
                searchVal = null,
                dfd;

            createController();
            ctrl.cfg = {
                refresh: function(value) {
                    called = true;
                    searchVal = value;
                    dfd = $q.defer();
                    dfd.resolve({ data: [1, 2, 3] });
                    return dfd.promise;
                }
            };
            ctrl.init();
            ctrl.search("test");

            $rootScope.$apply();

            expect(called).toEqual(true);
            expect(ctrl.options.length).toEqual(3);
            expect(ctrl.options[0]).toEqual(1);
            expect(ctrl.options[1]).toEqual(2);
            expect(ctrl.options[2]).toEqual(3);
            expect(searchVal).toEqual("test");
        });

        it("notifies the user of a search error", function() {
            var called = false;
            spyOn(notify, "error");
            spyOn($log, "error");
            createController();
            ctrl.cfg = {
                refresh: function(value) {
                    called = true;
                    dfd = $q.defer();
                    dfd.reject({ data: "error" });
                    return dfd.promise;
                }
            };
            ctrl.init();
            ctrl.search("test");

            $rootScope.$apply();

            expect(called).toEqual(true);
            expect($log.error).toHaveBeenCalledWith("Lookup control -> error");
            expect(notify.error).toHaveBeenCalledWith("error");
        });

        it("logs error when refresh not provided", function() {
            spyOn($log, "error");
            createController();
            ctrl.cfg = {};
            ctrl.init();
            ctrl.search("test");

            expect($log.error).toHaveBeenCalled();

        });
    });

    describe("select", function() {
        it("sets selection", function() {
            createController();
            ctrl.cfg = {
                itemSelected: function() {}
            };
            spyOn(ctrl.cfg, "itemSelected");
            ctrl.onSelect(1);

            expect(ctrl.selection).toEqual(1);
            expect(ctrl.cfg.itemSelected).toHaveBeenCalled();
        });
    });
});