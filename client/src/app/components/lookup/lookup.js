(function() {
    "use strict";

    angular
        .module("hb.components.lookup", [])
        .directive("hbLookup", function() {
            return {
                restrict: "E",
                templateUrl: "components/lookup/lookup.tpl.html",
                controller: "LookupController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    cfg: "=",
                    selection: "=?",
                    select: "&"
                }
            };
        })
        .controller("LookupController", [
            "$scope",
            "$log",
            "Notify",
            "hb.resources",
            Controller
        ]);

    function Controller($scope, $log, notify, resources) {
        this.$log = $log;
        this.notify = notify;
        this.resources = resources.lookup;
        this.itemTemplate = null;
        this.selectionTemplate = null;
        this.delay = 250;
        this.inputLength = 1;
        this.placeholder = this.resources.placeholder;
        this.options = [];
        this.init();
    }

    angular.extend(Controller.prototype, {
        init: function() {
            this.cfg = this.cfg || {};

            this.cfg.itemTemplate = this.cfg.itemTemplate || null;
            this.cfg.selectionTemplate = this.cfg.selectionTemplate || null;
            this.cfg.delay = this.cfg.delay || 250;
            this.cfg.inputLength = this.cfg.inputLength || 1;
            this.cfg.placeholder = this.cfg.placeholder || this.resources.placeholder;
        },

        search: function(value) {
            // don't do the search when no search value is provided just clear the list
            if (value === null ||
                value === undefined ||
                value.length < 3) {
                this.options = [];
                return;
            }

            // call the provided function
            if (this.cfg.refresh) {
                this.cfg.refresh(value).then(
                    angular.bind(this, this.searchComplete),
                    angular.bind(this, this.searchError)
                );
            }
            else {
                this.$log.error("no refresh callback provided");
            }
        },

        searchComplete: function(response) {
            // we are expecting the response to be { data: [] }
            this.options = response.data;
        },

        searchError: function(response) {
            this.$log.error("Lookup control -> " + response.data);
            this.notify.error(response.data);
        },

        onSelect: function($item, $model) {
            this.selection = $item;
            if (this.cfg.itemSelected) {
                this.cfg.itemSelected($item);
            }
            this.select();
        }
    });

})();