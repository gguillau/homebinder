(function() {
    "use strict";

    angular
        .module("hb.components.header", [])
        .directive("hbHeader", function() {
            return {
                restrict: "E",
                templateUrl: "components/header/header.tpl.html",
                controller: "HeaderController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    cfg: "=",
                    query: "=",
                    sort: "="
                }
            };
        })
        .controller("HeaderController", [
            "hb.resources",
            HeaderController
        ]);

    function HeaderController(resources) {
        this.resources = resources.hbHeader;
        this.searchTip = resources.searchTip;
        this.clearSearchTip = resources.clearSearchTip;
    }
})();