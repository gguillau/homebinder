describe("HeaderController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("HeaderController", {
        });
        $scope.ctrl = ctrl;

    }

    describe('ctrl.init', function() {
        it('inits the controller', function() {
            createController();
            expect(ctrl.resources.searchTip).toEqual("Search");
        });
    });

});