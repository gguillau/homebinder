describe("Appcues", function() {
    var Appcues;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        Appcues = _$injector_.get("Appcues");
        
        window.Appcues = {
            identify: function() {}
        };
    }));
    
    describe("identify", function() {
        it("calls Appcues.identify", function() {
            spyOn(window.Appcues, "identify");
            var user = {
                email: "email",
                role: "role"
            };
            Appcues.identify(user);
            expect(window.Appcues.identify).toHaveBeenCalled();
        });
    });
    
    describe("populate", function() {
        it("calls Appcues.identify", function() {
            spyOn(window.Appcues, "identify");
            var user = {
                email: "email",
                role: "role",
                user_profile_attributes: {}
            };
            Appcues.populate(user);
            expect(window.Appcues.identify).toHaveBeenCalled();
        });
    });
});