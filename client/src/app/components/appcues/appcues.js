(function() {
    "use strict";

    angular
        .module("hb.components.appcues", [])
        .factory("Appcues", [
            "$window",
            function($window) {
                return {
                    identify: function(user) {
                        if ($window.Appcues) {
                            $window.Appcues.identify(user.id, {
                                email: user.email,
                                role: user.role
                            });
                        }
                    },

                    populate: function(user, binderId) {
                        if ($window.Appcues) {
                            $window.Appcues.identify(user.id, {                         // unique identifier for current user
                                name: user.user_profile_attributes.display_name,        // name
                                email: user.email,                                      // email
                                created_at: user.created_at,                            // unix timestamp of user signup date
                                company: user.user_profile_attributes.company,          // company
                                role: user.role,                                        // role or permissions
                                create_method: user.create_method,                      // how the user was created
                                creator_role: user.creator ? user.creator.role : null,  // role of the user who created the account
                                completed_onboarding: user.completed_onboarding,        // whether or not the user has completed onboarding
                                binderId: binderId                                      // id for the current binder
                            });
                        }
                    }
                };
            }
        ]);
})();