angular
    .module("hb.components.supportText", [])
    .directive("hbSupportText", function() {
        return {
            restrict: "E",
            templateUrl: "components/supportText/supportText.tpl.html",
            controller: "SupportTextController",
            controllerAs: "ctrl",
            bindToController: true,
            scope: {}
        };
    }).controller("SupportTextController", [
        "hb.resources",
        SupportTextController
    ]);

function SupportTextController(resources) {
    this.resources = resources.supportText;
}