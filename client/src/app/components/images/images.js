angular
    .module("hb.components.images", [])
    .directive("hbImages", function() {
        return {
            restrict: "E",
            templateUrl: "components/images/images.tpl.html",
            controller: "HBImagesController as ctrl",
            bindToController: true,
            scope: {
                tagId: "=",
                images: "=",
                binderId: "=",
                showCarousel: "=",
                opts: "="
            }
        };
    })
    .controller("HBImagesController", [
        "hb.api",
        "$log",
        "Notify",
        "$stateParams",
        "$window",
        "ModalService",
        "Loading",
        "hb.resources",
        "Session",
        HBImagesController
    ]);

function HBImagesController(api, $log, notify, $stateParams, $window, ModalService, Loading, resources, session) {
    this.api = api;
    this.$log = $log;
    this.notify = notify;
    this.$stateParams = $stateParams;
    this.$window = $window;
    this.modal = ModalService;
    this.loading = Loading;
    this.resources = resources.images;
    this.currentUser = session.getUser();
    this.refresh();
}

HBImagesController.prototype = {

    refresh: function() {
        this.api.image.all(this.opts).then(
            angular.bind(this, this.onRefreshSuccess),
            angular.bind(this, this.onRefreshError)
        );
    },

    onRefreshSuccess: function(response) {
        this.images = response.data.items.map(angular.bind(this, function(image) {
            var img = {
                id: image.id,
                name: image.name,
                src: image.location,
                icon: this.getIcon(image.name)
            };
            return img;
        }));
    },

    onRefreshError: function(response) {
        this.$log.error(response);
        this.notify.error(response.data);
    },

    getIcon: function(name) {
        var file_extension = name.substring(name.length - 3);

        switch (file_extension) {
            case "pdf":
                return "fa fa-file-pdf-o";
            case "doc":
            case "ocx":
                return "fa fa-file-word-o";
            default:
                return "fa fa-file";
        }

    },

    viewDocument: function(img) {
        this.$window.open(img.location);
    },

    deleteImage: function(img, index) {
        this.modal.confirm({
            message: this.resources.deleteConfirm,
            glyphicon: "glyphicon glyphicon-trash",
            confirm: angular.bind(this, this.destroyConfirmed, img, index)
        });

    },

    destroyConfirmed: function(img, index) {
        this.loading.show(this.resources.removing + "...");
        this.api.image.destroy(img.id).then(
            angular.bind(this, this.onImgDestroySuccess, index),
            angular.bind(this, this.onImgDestroyError)
        );
    },

    onImgDestroySuccess: function(index, response) {
        this.images.splice(index, 1);
        this.notify.success(this.resources.removed);
        this.loading.close();
    },

    onImgDestroyError: function(response) {
        this.$log.error(response);
        this.notify.error(response.data);
        this.loading.close();
    }
};