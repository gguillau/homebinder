describe("HBImagesController", function() {
    var $rootScope,
        $q,
        $scope,
        $controller,
        ctrl,
        notify,
        $http,
        $stateParams,
        $log,
        loading,
        modal,
        api,
        images;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");

        $stateParams = {};

        images = [{
            binder: null,
            file_size: 439531,
            id: 562,
            location: "https://somerandomlink.com/fakedoc.png",
            name: "fakedoc.png",
            permissions: {
                can_read: true,
                can_write: true,
                can_create: false,
                can_destroy: true
            },
            seller_report_item: null,
            tags: null
        }, {
            binder: null,
            file_size: 439531,
            id: 563,
            location: "https://somerandomlink.com/fakedoc.jpeg",
            name: "fakedoc.jpeg",
            permissions: {
                can_read: true,
                can_write: true,
                can_create: false,
                can_destroy: true
            },
            seller_report_item: null,
            tags: null
        }];

        $log = {
            error: function(msg) {}
        };

        loading = {
            show: function(msg) {},
            close: function() {}
        };

        modal = {
            confirm: function(confirmOptions) {}
        };

        api = {
            image: {
                all: function(opts) {},
                destroy: function(imageId) {}
            }
        };

        notify = {
            success: function(msg) {},
            error: function(msg) {}
        };


    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("HBImagesController", {
            "ModalService": modal,
            "$http": $http,
            "Notify": notify,
            "hb.api": api,
            "$stateParams": $stateParams,
            "$log": $log,
            "Loading": loading
        }, {
            tagId: 1,
            images: []
        });

        $scope.ctrl = ctrl;

    }

    describe('ctrl.refresh', function() {
        it("should retrieve all images", function() {
            spyOn(api.image, "all").and.returnValue($q.when({
                data: { items: images }
            }));

            createController();
            $rootScope.$apply();

            expect(api.image.all).toHaveBeenCalled();
            expect(ctrl.images.length).toBe(2);
        });
        it("should return an error", function() {

            spyOn(api.image, "all").and.returnValue($q.reject({
                data: "error"
            }));
            spyOn($log, "error");
            spyOn(notify, "error");

            createController();
            $rootScope.$apply();

            expect(api.image.all).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
            expect(ctrl.images.length).toBe(0);
        });
    });

    describe('ctrl.getIcon', function() {
        it("sets icon to pdf", function() {
            spyOn(api.image, "all").and.returnValue($q.when({
                data: { items: images }
            }));
            createController();

            var icon = ctrl.getIcon("test.pdf");

            expect(icon).toEqual("fa fa-file-pdf-o");
        });

        it("sets icon to word", function() {
            spyOn(api.image, "all").and.returnValue($q.when({
                data: { items: images }
            }));
            createController();

            var icon = ctrl.getIcon("test.doc");

            expect(icon).toEqual("fa fa-file-word-o");
        });
    });

    describe('ctrl.viewDocument', function() {
        it("calls window open", function() {
            spyOn(api.image, "all").and.returnValue($q.when({
                data: { items: images }
            }));

            createController();
            spyOn(ctrl.$window, "open");

            ctrl.viewDocument({});

            expect(ctrl.$window.open).toHaveBeenCalled();
        });
    });

    describe('ctrl.deleteImage', function() {
        it("calls modal", function() {
            spyOn(api.image, "all").and.returnValue($q.when({
                data: { items: images }
            }));

            createController();
            spyOn(modal, "confirm");

            ctrl.deleteImage({}, 0);

            expect(modal.confirm).toHaveBeenCalled();
        });
    });

    describe('ctrl.destroyConfirmed', function() {
        it("calls api image destroy", function() {
            spyOn(api.image, "all").and.returnValue($q.when({
                data: { items: images }
            }));
            spyOn(loading, "show");

            createController();
            spyOn(api.image, "destroy").and.returnValue($q.when({ data: {} }));

            ctrl.destroyConfirmed({}, 0);

            expect(api.image.destroy).toHaveBeenCalled();
            expect(loading.show).toHaveBeenCalled();
        });
    });
    
    describe('ctrl.onImgDestroySuccess', function() {
        it("removes the image", function() {
            spyOn(api.image, "all").and.returnValue($q.when({
                data: { items: images }
            }));
            spyOn(loading, "close");
            spyOn(notify, "success");

            createController();
            ctrl.images = [{id: 1}];
            expect(ctrl.images.length).toEqual(1);
            ctrl.onImgDestroySuccess(0, {});

            expect(ctrl.images.length).toEqual(0);
            expect(loading.close).toHaveBeenCalled();
            expect(notify.success).toHaveBeenCalled();
        });
    });
    
    describe('ctrl.onImgDestroyError', function() {
        it("calls notify", function() {
            spyOn(api.image, "all").and.returnValue($q.when({
                data: { items: images }
            }));
            spyOn($log, "error");
            spyOn(loading, "close");
            spyOn(notify, "error");

            createController();
            ctrl.onImgDestroyError({data: "error"});

            expect(ctrl.notify.error).toHaveBeenCalled();
            expect($log.error).toHaveBeenCalled();
            expect(loading.close).toHaveBeenCalled();
            expect(notify.error).toHaveBeenCalled();
        });
    });
});

describe('hbImages', function() {
    var $scope, $compile, api, $q;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");

        $scope = $rootScope.$new(), $compile = _$compile_;
        spyOn(api.image, "all").and.returnValue($q.when({
            data: { items: [] }
        }));

        $scope.images = [];
        $scope.opts = {
            binderId: 1
        };
        $compile('<hb-images tag-id="1" images="images" binder-id="1" show-carousel="true" opts="opts"></hb-images>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the hb-images directive', function() {
        it('calls api image all', function() {
            expect(api.image.all).toHaveBeenCalled();
        });
    });
});