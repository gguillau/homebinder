(function() {
    "use strict";

    angular
        .module("hb.components.modal", [
            "hb.utils.filters"
        ])

        .factory("ModalService", [
            "$modal",
            function($modal) {
                return {
                    show: function(modalOptions) {
                        var modalInstance = $modal.open({
                            templateUrl: modalOptions.templateUrl,
                            controller: modalOptions.controller,
                            backdrop: modalOptions.backdrop,
                            keyboard: modalOptions.keyboard,
                            resolve: {
                                data: function() {
                                    return modalOptions.resolveData;
                                }
                            }
                        }).result.then(
                            function(resultData) {
                                // modal was closed
                                if (modalOptions.closed) {
                                    modalOptions.closed(resultData);
                                }
                                else if (modalOptions.resolveData && modalOptions.resolveData.closed) {
                                    modalOptions.resolveData.closed(resultData);
                                }
                                else {
                                    return resultData;
                                }
                            },
                            function(resultData) {
                                // modal was rejected
                                if (modalOptions.rejected) {
                                    modalOptions.rejected();
                                }
                                return;
                            }
                        );
                        return modalInstance;
                    },

                    confirm: function(confirmOptions) {
                        var modalInstance = $modal.open({
                            templateUrl: "components/modal/modal.tpl.html",
                            controller: "ModalCtrl as modal",
                            backdrop: "static",
                            resolve: {
                                title: function() {
                                    return confirmOptions.title || "Confirm Delete";
                                },
                                majorMessage: function() {
                                    return confirmOptions.message;
                                },
                                minorMessage: function() {
                                    return confirmOptions.minorMessage;
                                },
                                glyphicon: function() {
                                    return confirmOptions.glyphicon;
                                },
                                buttons: function() {
                                    return [{
                                        label: confirmOptions.confirmLabel ? confirmOptions.confirmLabel : "Yes",
                                        isPrimary: true,
                                        isFocused: true,
                                        action: "close",
                                        result: "confirm"
                                    }, {
                                        label: confirmOptions.cancelLabel ? confirmOptions.cancelLabel : "No",
                                        action: "dismiss"
                                    }];
                                }
                            }
                        });

                        modalInstance.result.then(
                            function(result) {
                                if (confirmOptions.confirm) {
                                    confirmOptions.confirm(result);
                                }
                            },
                            function(reason) {
                                if (confirmOptions.cancel) {
                                    confirmOptions.cancel(reason);
                                }
                            });

                        return modalInstance;
                    },

                    alert: function(alertOptions) {
                        var modalInstance = $modal.open({
                            templateUrl: "components/modal/modal.tpl.html",
                            controller: "ModalCtrl as modal",
                            backdrop: "static",
                            resolve: {
                                title: function() {
                                    return alertOptions.title;
                                },
                                majorMessage: function() {
                                    return alertOptions.majorMessage;
                                },
                                minorMessage: function() {
                                    return alertOptions.minorMessage;
                                },
                                glyphicon: function() {
                                    return alertOptions.glyphicon;
                                },
                                buttons: function() {
                                    return [{
                                        label: "OK",
                                        isPrimary: true,
                                        isFocused: true,
                                        action: "close"
                                    }];
                                }
                            }
                        });

                        modalInstance.result.then(
                            function(result) {
                                if (alertOptions.confirm) {
                                    alertOptions.confirm(result);
                                }
                            });

                        return modalInstance;
                    }
                };
            }
        ])
        .controller("ModalCtrl", [
            "$modalInstance",
            "title",
            "majorMessage",
            "minorMessage",
            "glyphicon",
            "buttons",
            "Session",
            "Notify",
            "$stateParams",
            ModalCtrl
        ]);

    function ModalCtrl($modalInstance, title, majorMessage, minorMessage, glyphicon, buttons, Session, notify, $stateParams) {
        this.$modalInstance = $modalInstance;
        this.title = title;
        this.majorMessage = majorMessage;
        this.minorMessage = minorMessage;
        //this.glyphicon = glyphicon;
        this.buttons = buttons;
        this.session = Session;
        this.notify = notify;
        this.$stateParams = $stateParams;
        this.uploader = {
            fileTypes: "image",
            hideUploadButton: true,
            showTable: true,
            selectButtonLabel: "Upload property photo",
            fileLimit: true,
            url: "/api/v1/images",
            jwt: this.session.getJwt(),
            multiSelect: false,
            uploadOnAdd: true
        };
        this.is_uploading = false;
        this.button_message = this.$stateParams["binderId"] ? "Update Binder" : "Create Binder";
        this.init();
    }

    ModalCtrl.prototype = {

        init: function() {
            angular.forEach(this.buttons, function(button) {
                if (button.isPrimary) {
                    button.cls = "btn btn-primary";
                }
                else if (button.isDanger) {
                    button.cls = "btn btn-danger";
                }
                else {
                    button.cls = "btn btn-default";
                }
            });
        },

        click: function(button) {
            if (button.action == "close") {
                this.$modalInstance.close(button.result);
            }
            else {
                this.$modalInstance.dismiss(button.reason ? button.reason : undefined);
            }
        }
    };
})();