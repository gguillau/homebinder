describe("ModalCtrl", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        $modalInstance,
        title,
        majorMessage,
        minorMessage,
        binder,
        partner,
        glyphicon,
        buttons,
        Session,
        Notify,
        ModalService,
        $modal,
        $q;

    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        ModalService = _$injector_.get("ModalService");
        $modal = _$injector_.get("$modal");
        $controller = _$controller_;
        $q = _$injector_.get("$q");

        title = "This is a test title";

        majorMessage = "This is a test title";

        minorMessage = "This is a test title";

        glyphicon = "glyphicon glyphicon-user";

        binder = {
            appliances: null,
            created_at: "2015-09-06T04:29:36Z",
            id: 1,
            name: "This is a fake home",
            primary: null,
            details: "this is a home",
            last_recall: null,
            partner: null,
            vendor: null,
            recalls: 0,
            documents: [{
                file_size: 506986,
                id: 950,
                location: "https://homebinder.com",
                name: "document_name.pdf",
                seller_report_item: null,
                tags: []
            }],
            maintenance_items: null,
            binder_contractors: null,
            property: null,
            subscription: null,
            seller_report: null,
            transfer: {
                binder_id: 1359,
                created_at: "2015-07-05T13:55:10Z",
                id: 498,
                status: "hold",
                transfer_to: "faketransferemail@gmail.com",
                transfer_type: "ownership",
                updated_at: "2015-07-05T18:23:37Z",
                user_id: 278
            },
            permissions: {
                can_create: false,
                can_create_seller_report: false,
                can_destroy: false,
                can_edit_seller_report: false,
                can_read: false,
                can_share: false,
                can_subscribe: false,
                can_transfer: false,
                can_view_master_report: false,
                can_write: false
            }
        };

        partner = {
            address: null,
            coupons: [],
            tags: [],
            id: 100,
            name: "John Smith Inspection Services",
            partner_type: "inspector",
            code: "JohnFREEFORLIFE",
            contact: "",
            phone: "",
            email: "johnsmith@gmail.com",
            binder_logo_id: null,
            sellers_logo_id: null,
            binder_count: 0,
            vendors: [],
            created_at: "2015-09-21 19:09:37.484248",
            settings: {
                navigation: {
                    binders: true,
                    users: true,
                    vendors: false
                }
            }
        };

        buttons = [{
                label: "Upload",
                isPrimary: true,
                isFocused: true,
                action: "close",
                result: "confirm"
            },
            {
                label: "Cancel",
                action: "dismiss"
            },
            {
                label: "Test",
                isDanger: true,
                action: "dismiss"
            }
        ];

        Session = {
            getJwt: function() {
                return "ereroro";
            }
        };

        Notify = {
            success: function(msg) {},
            error: function(msg) {}
        };

        $modalInstance = {
            close: function(button) {},
            dismiss: function(button) {}
        };

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("ModalCtrl", {
            "$modalInstance": $modalInstance,
            "title": title,
            "majorMessage": majorMessage,
            "minorMessage": minorMessage,
            "binder": binder,
            "partner": partner,
            "glyphicon": glyphicon,
            "buttons": buttons,
            "Session": Session,
            "Notify": Notify
        });

        $scope.ctrl = ctrl;

    }

    describe('ctrl.init', function() {

        it('should init buttons', function() {

            createController();
            $rootScope.$apply();
            expect(ctrl.buttons[0].cls).toEqual("btn btn-primary");
            expect(ctrl.buttons[1].cls).toEqual("btn btn-default");
        });

    });

    describe('ctrl.click', function() {

        it('should call close function of modalInstance', function() {

            spyOn($modalInstance, "close");

            createController();
            $rootScope.$apply();
            ctrl.click(ctrl.buttons[0]);

            expect(ctrl.$modalInstance.close).toHaveBeenCalled();

        });

        it('should call dismiss function of modalInstance', function() {

            spyOn($modalInstance, "dismiss");

            createController();
            $rootScope.$apply();
            ctrl.click(ctrl.buttons[1]);

            expect(ctrl.$modalInstance.dismiss).toHaveBeenCalled();

        });

    });

    describe('ModalService', function() {
        it('should call show and then $modal open', function() {
            var args = { onClosed: function() {} },
                deferred = $q.defer(),
                modalInstance = {
                    result: deferred.promise
                };

            spyOn($modal, "open").and.returnValue(modalInstance);

            ModalService.show(args);

            deferred.resolve({});
            $rootScope.$apply();
            expect($modal.open).toHaveBeenCalled();
        });

        it('should call show and then $modal open', function() {
            var args = { closed: function() {} },
                deferred = $q.defer(),
                modalInstance = {
                    result: deferred.promise
                };

            spyOn($modal, "open").and.returnValue(modalInstance);
            spyOn(args, "closed");

            ModalService.show(args);

            deferred.resolve({});
            $rootScope.$apply();

            expect(args.closed).toHaveBeenCalled();
            expect($modal.open).toHaveBeenCalled();
        });

        it('should call show and then $modal open', function() {
            var args = { onClosed: function() {}, resolveData: { closed: function() {} } },
                deferred = $q.defer(),
                modalInstance = {
                    result: deferred.promise
                };

            spyOn(args.resolveData, "closed");
            spyOn($modal, "open").and.returnValue(modalInstance);

            ModalService.show(args);

            deferred.resolve({});
            $rootScope.$apply();

            expect(args.resolveData.closed).toHaveBeenCalled();
            expect($modal.open).toHaveBeenCalled();
        });

        it('should call show and then $modal open', function() {
            var args = { rejected: function() {} },
                deferred = $q.defer();

            $q.reject({});
            deferred.reject({});
            var modalInstance = {
                result: deferred.promise
            };
            spyOn(args, "rejected");
            spyOn($modal, "open").and.returnValue(modalInstance);

            ModalService.show(args);

            $rootScope.$apply();
            expect($modal.open).toHaveBeenCalled();
            expect(args.rejected).toHaveBeenCalled();
        });

        it('should call confirm and then $modal open', function() {
            var args = { confirm: function() {}, cancel: function() {} },
                deferred = $q.defer(),
                modalInstance = {
                    result: deferred.promise,
                    dismiss: function() {}
                };

            spyOn(args, "cancel");
            spyOn($modal, "open").and.returnValue(modalInstance);

            ModalService.confirm(args);

            deferred.resolve({});
            $rootScope.$apply();
            expect($modal.open).toHaveBeenCalled();
        });

        it('should call cancel', function() {
            var args = { confirm: function() {}, cancel: function() {} },
                deferred = $q.defer();
            $q.reject({});
            deferred.reject({});
            var modalInstance = {
                result: deferred.promise,
                dismiss: function() {}
            };

            spyOn(args, "cancel");
            spyOn($modal, "open").and.returnValue(modalInstance);

            ModalService.confirm(args);

            $rootScope.$apply();
            expect($modal.open).toHaveBeenCalled();
            expect(args.cancel).toHaveBeenCalled();
        });

        it('should call alert and then $modal open', function() {
            var args = { confirm: function() {} },
                deferred = $q.defer(),
                modalInstance = {
                    result: deferred.promise
                };

            spyOn($modal, "open").and.returnValue(modalInstance);

            ModalService.alert(args);

            deferred.resolve({});
            $rootScope.$apply();
            expect($modal.open).toHaveBeenCalled();
        });
    });
});