describe('ngEnter directive:', function() {
 
    var $compile;
    var $rootScope;
 
    beforeEach(function() {
        module('homebinder');
        inject(function(_$compile_, _$rootScope_){
            $compile = _$compile_;
            $rootScope = _$rootScope_;
        });
    });
 
    it('should call the given method on an ng-enter-event', function() {
        $rootScope.test = jasmine.createSpy('test');
        var element = $compile('<input ng-enter="test()"></input>')($rootScope);
        $rootScope.$digest();
        var e = $.Event("keydown");
        e.which = 13;
 
        element.triggerHandler(e);
 
        expect($rootScope.test).toHaveBeenCalled();
    });
});