describe("ToolbarController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("ToolbarController", {});
        $scope.ctrl = ctrl;

    }

    describe('ctrl.search', function() {
        it('calls cfg search', function() {
            createController();
            ctrl.cfg = {
                search: function() {},
                searchValue: "search"
            };
            spyOn(ctrl.cfg, "search");
            ctrl.search();

            expect(ctrl.cfg.search).toHaveBeenCalled();
        });
    });

    describe('ctrl.clearSearch', function() {
        it('sets searchValue to null', function() {
            createController();
            ctrl.cfg = {
                search: function() {},
                searchValue: "search"
            };
            spyOn(ctrl.cfg, "search");
            ctrl.clearSearch();

            expect(ctrl.cfg.searchValue).toBeNull();
        });
    });

    describe('ctrl.sort', function() {
        it('calls execute', function() {
            createController();
            ctrl.cfg = {
                search: function() {},
                searchValue: "search",
                sort: {
                    execute: function() {}
                }
            };
            spyOn(ctrl.cfg.sort, "execute");
            ctrl.sort();

            expect(ctrl.cfg.sort.execute).toHaveBeenCalled();
        });
    });

});