(function() {
    "use strict";
    
    angular
        .module("hb.components.toolbar", [])
        .directive("hbToolbar", function() {
            return {
                restrict: "E",
                templateUrl: "components/toolbar/toolbar.tpl.html",
                controller: "ToolbarController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                  cfg: "="
                }
            };
        })
        .controller("ToolbarController", [
            ToolbarController                           
        ]);
        
        function ToolbarController() {
            this.searchTip = "Search";
            this.clearSearchTip = "Clear search";
        }
        
        ToolbarController.prototype = {
            search: function() {
                if (this.cfg.search) {
                    this.cfg.search(this.cfg.searchValue);
                }
            },
            clearSearch: function() {
                this.cfg.searchValue = null;
                this.cfg.search(null);
            },
            sort: function() {
                if (this.cfg.sort.execute) {
                    this.cfg.sort.execute(this.cfg.sort.sortOption);
                }
            }
        };
})();