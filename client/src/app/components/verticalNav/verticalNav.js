(function() {
    "use strict";

    angular
        .module("hb.components.verticalNav", [])
        .directive("hbVerticalNav", function() {
            return {
                restrict: "E",
                templateUrl: "components/verticalNav/verticalNav.tpl.html",
                controller: "VerticalNavController",
                controllerAs: "ctrl",
                bindToController: true,
                scope: {
                    cfg: "="
                }
            };
        })
        .controller("VerticalNavController", [
            "$state",
            "$stateParams",
            "Loading",
            "$log",
            "Notify",
            "hb.api",
            "$scope",
            VerticalNavController
        ]);

    function VerticalNavController($state, $stateParams, loading, $log, notify, api, $scope) {
        this.$state = $state;
        this.$stateParams = $stateParams;
        this.loading = loading;
        this.$log = $log;
        this.notify = notify;
        this.api = api;
        this.logo_loaded = false;
        this.is_partner = false;
        this.$scope = $scope;
        this.$scope.$on('partner-logo-changed', angular.bind(this, function(event, args) {
            if (this.cfg.partner) {
                this.cfg.partner.logo_file = args.logo;
            }
        }));
    }

})();