describe("VerticalNavController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
        $q = _$injector_.get("$q");

    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("VerticalNavController", {
            "$scope": $rootScope
        }, { cfg: {} });
        $scope.ctrl = ctrl;

    }

    describe('ctrl.init', function() {
        it('inits the controller', function() {
            createController();
            expect(ctrl.is_partner).toBe(false);
        });
    });

    describe("logo change", function() {
        it("updates the logo", function() {
            createController();
            expect(ctrl.cfg).toEqual({});
            ctrl.cfg = {
                partner: {
                    logo_file: "current_logo"
                }
            };

            expect(ctrl.cfg.partner.logo_file).toEqual("current_logo");

            $rootScope.$broadcast("partner-logo-changed", {
                logo: "updated_logo"
            });

            expect(ctrl.cfg.partner.logo_file).toEqual("updated_logo");
        });
    });
});

describe('hbVerticalNav', function() {
    var $scope, $compile, $stateParams, element;

    beforeEach(module('homebinder'));
    beforeEach(inject(function($rootScope, _$compile_, _$injector_) {
        $scope = $rootScope.$new(), $compile = _$compile_;
        $stateParams = _$injector_.get("$stateParams");

        $stateParams.partnerId = 1;
        $scope.cfg = {
            partner: {
                id: 1,
                contact: "Test Partner"
            }
        };
        element = $compile('<hb-vertical-nav cfg="cfg"></hb-vertical-nav>')($scope);

        $scope.$digest();
    }));

    describe('when the page compiles the hb-vertical-nav directive', function() {
        it('adds the partner contact to the html', function() {
            expect(element.html()).toContain("Test Partner");
        });
    });
});