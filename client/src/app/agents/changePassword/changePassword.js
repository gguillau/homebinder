(function() {
	"use strict";

	angular
		.module("hb.agents.changePassword", [])
		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("agent.changePassword", {
					url: "^/agents/:userId/changePassword",
					templateUrl: 'components/users/edit/password/password.tpl.html',
					controller: 'UserSettingsProfileController as ctrl',
					access: {
						requiresLogin: true
					}
				});
		}]);
})();