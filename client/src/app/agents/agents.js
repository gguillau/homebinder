(function () {
	"use strict";

	angular.module("hb.agents", [
			"hb.agents.binders",
			"hb.agents.changeEmail",
			"hb.agents.changePassword",
			"hb.agents.welcome",
			"hb.agents.settings",
			"hb.agents.repair_pricer",
			"hb.agents.homepros"
		])
		.config(['$stateProvider', function ($stateProvider) {
			$stateProvider
				.state("agent", {
					abstract: true,
					url: "^/agents/:userId",
					templateUrl: "agents/agents.tpl.html",
					controller: "AgentController",
					controllerAs: "ctrl"
				});
		}])
		.controller("AgentController", [
			AgentController
		]);

	function AgentController() {
		this.navCfg = {
			navList: []
		};
		this.init();
	}

	AgentController.prototype = {
		init: function () {
			this.navCfg.navList.push({
				id: "binders",
				state: "agent.binders",
				label: "Binders",
				icon: "glyphicon glyphicon-home"
			});

			this.navCfg.navList.push({
				id: "settings",
				state: "agent.settings",
				label: "Settings",
				icon: "glyphicon glyphicon-cog"
			});

			this.navCfg.navList.push({
				id: "change_password",
				state: "agent.changePassword",
				label: "Change Password",
				icon: "glyphicon glyphicon-pencil"
			});

			this.navCfg.navList.push({
				id: "change_email",
				state: "agent.changeEmail",
				label: "Change Email",
				icon: "glyphicon glyphicon-envelope"
			});

			this.navCfg.navList.push({
				id: "repair_pricer",
				state: "agent.repair_pricer",
				label: "Repair Pricer",
				icon: "glyphicon glyphicon-list-alt"
			});

			this.navCfg.navList.push({
				id: "repair_pricer",
				state: "agent.homepros",
				label: "Preferred Home Pros",
				icon: "glyphicon glyphicon-book"
			});
		}

	};
})();
