describe("AgentsRepairPricerController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl,
        api,
        $q,
        notify,
        RepairPricerModal;


    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
    }));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        api = _$injector_.get("hb.api");
        $q = _$injector_.get("$q");
        notify = _$injector_.get("Notify");
        RepairPricerModal = _$injector_.get("RepairPricerModal");
        $controller = _$controller_;
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("AgentsRepairPricerController", {
            "hb.api": api,
            "Notify": notify,
            "RepairPricerModal": RepairPricerModal
        });
        $scope.ctrl = ctrl;

    }

    describe('ctrl.init', function() {
        it('calls user get', function() {
            spyOn(api.user, "get").and.returnValue($q.when({ data: {} }));
            createController();
            expect(api.user.get).toHaveBeenCalled();
        });
    });

    describe('ctrl.userSuccess', function() {
        it('sets the user', function() {
            createController();
            ctrl.userSuccess({ data: { id: 1 } });

            expect(ctrl.user.id).toEqual(1);
        });
    });

    describe('ctrl.userError', function() {
        it('calls notify', function() {
            spyOn(notify, "error");
            createController();
            ctrl.userError({ data: "error" });

            expect(notify.error).toHaveBeenCalled();
        });
    });

    describe('ctrl.order', function() {
        it('calls RepairPricerModal', function() {
            spyOn(RepairPricerModal, "show");
            createController();
            ctrl.user = {
                user_profile_attributes: {}
            };
            ctrl.order();

            expect(RepairPricerModal.show).toHaveBeenCalled();
        });
    });
});