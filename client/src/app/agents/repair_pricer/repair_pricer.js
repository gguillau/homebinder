(function() {
	"use strict";

	angular
		.module("hb.agents.repair_pricer", [])
		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("agent.repair_pricer", {
					url: "^/agents/:userId/repair_pricer",
					templateUrl: 'agents/repair_pricer/repair_pricer.tpl.html',
					controller: 'AgentsRepairPricerController',
					controllerAs: "ctrl",
					access: {
						requiresLogin: true
					}
				});
		}])
		.controller("AgentsRepairPricerController", [
			"RepairPricerModal",
			"Session",
			"hb.api",
			"Notify",
			AgentsRepairPricerController
		]);

	function AgentsRepairPricerController(RepairPricerModal, Session, api, notify) {
		this.RepairPricerModal = RepairPricerModal;
		this.session = Session;
		this.user = null;
		this.api = api;
		this.notify = notify;
		this.init();
	}

	AgentsRepairPricerController.prototype = {

		init: function() {
			this.api.user.get(this.session.getUser().id).then(
				angular.bind(this, this.userSuccess),
				angular.bind(this, this.userError));
		},

		userSuccess: function(response) {
			this.user = response.data;
		},

		userError: function(response) {
			this.notify.error(response.data);
		},

		order: function() {
			this.RepairPricerModal.show({
				user: this.user,
				buyer_agent: {
					first_name: this.user.user_profile_attributes.first_name,
					last_name: this.user.user_profile_attributes.last_name,
					email: this.user.email
				},
				binder_id: null,
				partner_id: null,
				repair_pricer_pre_paid_reports: false
			});
		}
	};
})();