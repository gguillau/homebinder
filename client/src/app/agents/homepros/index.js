(function() {
    "use strict";

    angular
        .module("hb.agents.homepros", [
            "hb.agents.homepros.edit"
        ])
        .config(['$stateProvider', function($stateProvider) {
            $stateProvider
                .state('agent.homepros', {
                    url: '^/agents/:userId/homepros',
                    templateUrl: "agents/homepros/index.tpl.html",
                    controller: "AgentHomeProsController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .controller("AgentHomeProsController", [
            "hb.framework.indexBase",
            "hb.resources",
            "Session",
            "$stateParams",
            "hb.api",
            "PartnerTemplatesItemsEditModal",
            AgentHomeProsController
        ]);

    function AgentHomeProsController(IndexBase, resources, session, $stateParams, api, PartnerTemplatesItemsEditModal) {
        var Model = function() {
            // call the parent class
            IndexBase.call(this);
            this.currentUser = session.getUser();
            this.resources = angular.extend({}, this.resources, resources.partnerProsIndex);
            this.itemsPerPage = 10;
            this.proArgs = {
                user_id: $stateParams.userId
            };
            // delete prop
            this.nameProperty = "name";
            this.sortOptions = [{
                orderBy: "user_contractors.created_at",
                order: "desc",
                desc: this.resources.creationDate + " - " + this.resources.descending
            }, {
                orderBy: "user_contractors.created_at",
                order: "asc",
                desc: this.resources.creationDate + " - " + this.resources.ascending
            }, {
                orderBy: "user_contractors.name",
                order: "desc",
                desc: this.resources.name + " - " + this.resources.descending
            }, {
                orderBy: "user_contractors.name",
                order: "asc",
                desc: this.resources.name + " - " + this.resources.ascending
            }];
            this.sortOption = this.sortOptions[0];
            this.orderBy = this.sortOption.orderBy;
            this.order = this.sortOption.order;
            // set the refresh API call
            this.refreshCall = api.userContractor.all;
            // set the delete API call
            this.deleteCall = api.userContractor.destroy;
            this.setHeaders();
            this.refresh();
        };

        Model.prototype = Object.create(IndexBase.prototype);

        angular.extend(Model.prototype, {

            setHeaders: function() {
                this.headers = this.resources.tableAttributes.map(angular.bind(this, function(attribute) {
                    var sortable = false,
                        orderBy = null,
                        sorted = false;
                    if (attribute === "ID") {
                        sorted = true;
                        sortable = true;
                        orderBy = "user_contractors.id";
                    }
                    else if (attribute === "Name") {
                        sortable = true;
                        orderBy = "user_contractors.name";
                    }
                    else if (attribute === "Email") {
                        sortable = true;
                        orderBy = "user_contractors.email";
                    }
                    return {
                        name: attribute,
                        sortable: sortable,
                        sorted: sorted,
                        orderBy: orderBy,
                        order: "desc"
                    };
                }));
                this.headerOptions = angular.copy(this.headers);
                this.sortOption = this.headers[0];
            },

            addQueryArgs: function() {
                this.addArgs("user_id");
            },

            addArgs: function(arg) {
                if (this.proArgs[arg]) {
                    this.queryArgs[arg] = this.proArgs[arg];
                }
                else {
                    delete this.queryArgs[arg];
                }
            },

            // send user to new item view
            newItem: function() {
                var item = {
                    name: "",
                    email: "",
                    notes: "",
                    user_contractor_types: [],
                    user_contractor_sub_types: [],
                    address: {},
                    phone: {},
                    secondary_name: "",
                    secondary_email: "",
                    secondary_phone: {},
                    send_email_invitation: true
                };

                PartnerTemplatesItemsEditModal.show({
                    templateUrl: "agents/homepros/edit/edit.tpl.html",
                    controller: "AgentHomeProsEditController as ctrl",
                    item: item,
                    index: -1,
                    onSave: angular.bind(this, this.insertContractor)
                });
            },

            edit: function(item) {
                var index = this.items.indexOf(item);
                PartnerTemplatesItemsEditModal.show({
                    templateUrl: "agents/homepros/edit/edit.tpl.html",
                    controller: "AgentHomeProsEditController as ctrl",
                    item: item,
                    index: index,
                    onSave: angular.bind(this, this.insertContractor)
                });
            },

            insertContractor: function(item, index) {
                if (index > -1) {
                    this.items[index] = item;
                }
                else {
                    this.items.push(item);
                    this.totalItems += 1;
                }
            }
        });

        this.model = new Model();
    }

})();
