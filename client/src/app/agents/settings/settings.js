(function() {
	"use strict";

	angular
		.module("hb.agents.settings", [])
		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("agent.settings", {
					url: "^/agents/:userId/settings",
					templateUrl: 'components/users/form/profile/profile.tpl.html',
					controller: 'AgentsProfileController',
					controllerAs: "ctrl",
					access: {
						requiresLogin: true
					}
				});
		}])
		.controller("AgentsProfileController", [
			"UserProfileController",
			AgentsProfileController
		]);

	function AgentsProfileController(UserProfileController) {
		var Model = function() {
			// call the parent class
			UserProfileController.call(this);
			this.refresh();
		};

		Model.prototype = Object.create(UserProfileController.prototype);

		angular.extend(Model.prototype);

		this.model = new Model();
	}
})();