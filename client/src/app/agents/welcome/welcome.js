(function() {
	"use strict";

	angular
		.module("hb.agents.welcome", [
			"hb.components"
		])
		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("agent.welcome", {
					url: "^/agents/:accessToken/welcome",
					views: {
						'@': {
							templateUrl: "components/users/welcome/welcome.tpl.html",
							controller: "AgentsWelcomeController",
							controllerAs: "ctrl"
						}
					}
				});
		}])
		.controller("AgentsWelcomeController", [
			"UsersWelcomeController",
			"hb.resources",
			AgentsWelcomeController
		]);

	function AgentsWelcomeController(UsersWelcomeController, resources) {
		var Model = function() {
			// call the parent class
			UsersWelcomeController.call(this);
			this.transferText = this.resources.choosePassword;
			this.submitLabel = this.resources.getStarted;
			this.refresh();
		};

		Model.prototype = Object.create(UsersWelcomeController.prototype);

		angular.extend(Model.prototype);

		this.model = new Model();
	}
})();