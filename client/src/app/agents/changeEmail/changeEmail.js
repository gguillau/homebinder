(function() {
	"use strict";

	angular
		.module("hb.agents.changeEmail", [])
		.config(['$stateProvider', function($stateProvider) {
			$stateProvider
				.state("agent.changeEmail", {
					url: "^/agents/:userId/changeEmail",
					templateUrl: 'components/users/edit/email/email.tpl.html',
					controller: 'UserSettingsProfileController as ctrl',
					access: {
						requiresLogin: true
					}
				});
		}]);
})();
