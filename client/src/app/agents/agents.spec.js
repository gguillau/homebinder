describe("AgentController", function() {
    var $rootScope,
        $scope,
        $controller,
        ctrl;


    beforeEach(module("homebinder"));
    beforeEach(inject(function(_$controller_, _$injector_) {
        $rootScope = _$injector_.get("$rootScope");
        $controller = _$controller_;
    }));

    function createController() {
        $scope = $rootScope.$new();
        ctrl = $controller("AgentController", {});
        $scope.ctrl = ctrl;

    }

    describe('ctrl.init', function() {
        it('inits the navList', function() {
            createController();
            expect(ctrl.navCfg.navList.length).toEqual(6);
        });
    });
});
