describe("AgentsBindersIndexController", function() {
    var controller,
        base,
        api,
        resources;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the loading service
        $provide.factory("Loading", function() {
            return {
                show: function() {},
                close: function() {}
            };
        });
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        base = $injector.get("hb.framework.indexBase");
        api = $injector.get("hb.api");
        resources = $injector.get("hb.resources");

        spyOn(api.binder, "all").and.returnValue($q.when({
            data: {
                total: 1,
                items: [{
                    id: 1,
                    name: "name"
                }]
            }
        }));

        controller = $controller("AgentsBindersIndexController", {
            "hb.framework.indexBase": base,
            "hb.api": api,
            "hb.resources": resources
        });

        $rootScope.$apply();
    }));

    describe("refresh", function() {
        it("calls api", function() {
            controller.model.searchValue = "search";
            controller.model.refresh();
            expect(api.binder.all).toHaveBeenCalled();
        });
    });
});