(function() {
    "use strict";

    angular.module('hb.agents.binders.new', [
            "hb.agents.binders.new.client"
        ])
        .config(['$stateProvider', agentBindersNewConfig])
        .controller("AgentBindersNewController", [
            "BindersNewController",
            "$stateParams",
            "Context",
            AgentBindersNewController
        ]);

    function AgentBindersNewController(BindersNewController, $stateParams, context) {

        var Model = function() {
            // call the parent class
            BindersNewController.call(this);
            this.$stateParams = $stateParams;
            // set the index state and index params
            this.indexState = "agent.binders";
            this.indexParams = {
                userId: $stateParams.userId
            };
            this.editState = "agent.edit_binder.client";
            this.editParams = {
                userId: $stateParams.userId,
                binderId: null
            };
            this.formCfg.agentOptions = [{
                    name: "buyer_agent",
                    value: "Buyer Agent"
                },
                {
                    name: "seller_agent",
                    value: "Seller Agent"
                }
            ];
            this.init();
        };

        Model.prototype = Object.create(BindersNewController.prototype);

        angular.extend(Model.prototype, {
            init: function() {
                this.formCfg.settings.showSellerReportBranding = true;
                this.formCfg.settings.showAgentForm = false;
                this.formCfg.settings.showConfigForm = true;
                this.formCfg.settings.showPartner = false;
                this.getUser();
            },

            initiate: function() {
                this.navigation_links = [{
                    name: "Binder Info",
                    links: [
                        { name: "Client/Property Info", value: "client", state: "agent.new_binder.client", allowed: true }
                    ]
                }];
            },

            getUser: function() {
                this.api.user.get($stateParams.userId).then(
                    angular.bind(this, this.getUserSuccess),
                    angular.bind(this, this.getUserError));
            },

            getUserSuccess: function(response) {
                this.formCfg.binderOwner = response.data;
                this.formCfg.binderBrandings.maintenance = this.formCfg.binderOwner;
                this.formCfg.binderBrandings.recall = this.formCfg.binderOwner;
                this.formCfg.binderBrandings.binder = this.formCfg.binderOwner;
                this.formCfg.binderBrandings.sellerReport = this.formCfg.binderOwner;
            },

            getUserError: function(response) {
                this.notify.error(response.data);
            }
        });

        this.model = new Model();
    }

    function agentBindersNewConfig($stateProvider) {
        $stateProvider
            .state('agent.new_binder', {
                url: "^/agents/:userId/binders/new",
                templateUrl: "components/binders/form/form.tpl.html",
                controller: "AgentBindersNewController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
    }

})();