describe("AgentBindersNewController", function() {
    var controller,
        base,
        $q_,
        api;

    // load the homebinder module
    beforeEach(module("homebinder"));
    beforeEach(module(function($provide) {
        // mock the session service
        $provide.factory("Session", function() {
            return {
                getUser: function() {
                    return { role: "admin" };
                },
                getLocale: function() {
                    return null;
                },
                setLocale: function() {},
                getJwt: function() {}
            };
        });
    }));
    // create the controller
    beforeEach(inject(function($controller, $injector, $q, $rootScope) {
        $q_ = $q;
        base = $injector.get("BindersNewController");
        api = $injector.get("hb.api");

        controller = $controller("AgentBindersNewController", {
            "BindersNewController": base
        });
    }));

    describe("init", function() {
        it("sets the indexState", function() {
            expect(controller.model.indexState).toEqual("agent.binders");
        });
    });

    describe("getUser", function() {
        it("calls user get", function() {
            spyOn(api.user, "get").and.returnValue($q_.when({ data: {} }));
            controller.model.getUser();

            expect(api.user.get).toHaveBeenCalled();
        });
    });

    describe("getUserSuccess", function() {
        it("sets the user", function() {
            var user = { id: 1, email: "agent@gmail.com" };
            controller.model.getUserSuccess({ data: user });

            expect(controller.model.formCfg.binderOwner).toEqual(user);
        });
    });

    describe("getUserError", function() {
        it("calls notify", function() {
            spyOn(controller.model.notify, "error");
            controller.model.getUserError({ data: "error" });

            expect(controller.model.notify.error).toHaveBeenCalled();
        });
    });

});