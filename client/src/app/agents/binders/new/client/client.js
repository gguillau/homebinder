(function() {
    "use strict";

    angular.module('hb.agents.binders.new.client', [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('agent.new_binder.client', {
                    url: '^/agents/:userId/binders/new/client',
                    templateUrl: 'components/binders/form/clientInformation/clientInformation.tpl.html',
                    controller: "AgentBindersNewController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();