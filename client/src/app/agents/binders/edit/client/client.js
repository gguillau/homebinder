(function() {
    "use strict";

    angular.module('hb.agents.binders.edit_binder.client', [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('agent.edit_binder.client', {
                    url: '^/agents/:userId/binders/:binderId/client',
                    templateUrl: 'components/binders/form/clientInformation/clientInformation.tpl.html',
                    controller: "AgentBindersEditController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();