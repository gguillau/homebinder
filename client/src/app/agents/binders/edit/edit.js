(function() {
    "use strict";

    angular.module('hb.agents.binders.edit_binder', [
            "hb.agents.binders.edit_binder.client",
            "hb.agents.binders.edit_binder.images",
            "hb.agents.binders.edit_binder.appliances",
            "hb.agents.binders.edit_binder.maintenanceItems",
            "hb.agents.binders.edit_binder.contractors",
            "hb.agents.binders.edit_binder.documents"
        ])
        .config(['$stateProvider', agentsBindersEditConfig])
        .controller("AgentBindersEditController", [
            "BindersEditController",
            "$stateParams",
            AgentBindersEditController
        ]);

    function AgentBindersEditController(BindersEditController, $stateParams) {

        var Model = function() {
            // call the parent class
            BindersEditController.call(this);
            this.indexState = "agent.binders";
            this.indexParams = { userId: $stateParams.userId };
            this.formCfg.settings.showBinderAgents = false;
            this.formCfg.settings.showBinderEmails = false;
            this.formCfg.settings.showBinderUsers = false;
            this.formCfg.settings.showBinderPartners = false;
            this.formCfg.settings.showBinderBrandings = false;
            this.formCfg.settings.showBinderTransfers = false;
            this.initiate();
            this.refresh();
        };

        Model.prototype = Object.create(BindersEditController.prototype);

        angular.extend(Model.prototype, {
            initiate: function() {
                this.navigation_links = [{
                        name: "Binder Info",
                        links: [
                            { name: "Client/Property Info", value: "client", state: "agent.edit_binder.client", allowed: true }
                        ]
                    },
                    {
                        name: "Binder Items",
                        links: [
                            { name: "Images", value: "images", state: "agent.edit_binder.images", allowed: this.formCfg.settings.showAdditionalLinks },
                            { name: "Appliances", value: "appliances", state: "agent.edit_binder.appliances", allowed: this.formCfg.settings.showAdditionalLinks },
                            { name: "Maintenance Items", value: "maintenance_items", state: "agent.edit_binder.maintenanceItems", allowed: this.formCfg.settings.showAdditionalLinks },
                            { name: "Preferred Home Pros", value: "home_pros", state: "agent.edit_binder.contractors", allowed: this.formCfg.settings.showAdditionalLinks },
                            { name: "Documents", value: "documents", state: "agent.edit_binder.documents", allowed: this.formCfg.settings.showAdditionalLinks }
                        ]
                    }
                ];
            }
        });

        this.model = new Model();
    }

    function agentsBindersEditConfig($stateProvider) {
        $stateProvider
            .state('agent.edit_binder', {
                url: '^/agents/:userId/binders/:binderId/edit',
                abstract: true,
                templateUrl: 'components/binders/form/form.tpl.html',
                controller: "AgentBindersEditController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                },
                resolve: {
                    binder: ["Context", "$stateParams", function(Context, $stateParams) {
                        return Context.getBinder($stateParams.binderId);
                    }]
                }
            });
    }

})();