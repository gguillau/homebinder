(function() {
    "use strict";

    angular.module('hb.agents.binders.edit_binder.images', [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('agent.edit_binder.images', {
                url: '^/agents/:userId/binders/:binderId/images',
                templateUrl: 'components/binders/form/images/images.tpl.html',
                controller: "BinderFormImagesController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();