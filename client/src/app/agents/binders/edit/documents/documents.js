(function() {
    "use strict";

    angular.module("hb.agents.binders.edit_binder.documents", [])
		.config(["$stateProvider", function($stateProvider) {
			$stateProvider
            .state('agent.edit_binder.documents', {
                url: '^/agents/:userId/binders/:binderId/documents',
                templateUrl: 'components/binders/form/documents/documents.tpl.html',
                controller: "BinderFormDocumentsController",
                controllerAs: "ctrl",
                access: {
                    requiresLogin: true
                }
            });
		}]);
})();