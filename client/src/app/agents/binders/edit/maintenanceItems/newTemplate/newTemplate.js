(function() {
    "use strict";

    angular.module("hb.agents.binders.edit_binder.maintenanceItems.newTemplate", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state("agent.edit_binder.maintenanceItemsNewTemplate", {
                    url: "^/agents/:userId/binders/:binderId/maintenanceItems/new/{templateId:int}",
                    templateUrl: "homebinders/homebinder/maintenanceItems/form/form.tpl.html",
                    controller: "AgentBindersMaintenanceItemsFormController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();