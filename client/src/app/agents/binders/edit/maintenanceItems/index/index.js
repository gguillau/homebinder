(function() {
    "use strict";

    angular.module("hb.agents.binders.edit_binder.maintenanceItems.index", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('agent.edit_binder.maintenanceItems', {
                    url: '^/agents/:userId/binders/:binderId/maintenanceItems',
                    templateUrl: 'components/binders/form/maintenanceItems/maintenanceItems.tpl.html',
                    controller: "AgentBindersMaintenanceItemsController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .controller("AgentBindersMaintenanceItemsController", [
            "BinderFormMaintenanceItemsController",
            "$state",
            "$stateParams",
            AgentBindersMaintenanceItemsController
        ]);

    function AgentBindersMaintenanceItemsController(BinderFormMaintenanceItemsController, $state, $stateParams) {
        var Model = function() {
            // call the parent class
            BinderFormMaintenanceItemsController.call(this);
            this.itemNewState = "agent.edit_binder.maintenanceItemsNew";
            this.itemNewTemplateState = "agent.edit_binder.maintenanceItemsNewTemplate";
        };

        Model.prototype = Object.create(BinderFormMaintenanceItemsController.prototype);

        angular.extend(Model.prototype, {
            // send user back to editState
            edit: function(item) {
                $state.go("agent.edit_binder.maintenanceItemsEdit", { id: item.id, binderId: $stateParams.binderId, userId: $stateParams.userId });
            },

            showForm: function(item) {
                if (item) {
                    $state.go(this.itemNewTemplateState, { binderId: this.binder.id, templateId: item.id, userId: $stateParams.userId });
                }
                else {
                    $state.go(this.itemNewState, { binderId: $stateParams.binderId, userId: $stateParams.userId });
                }
            }
        });

        this.model = new Model();
    }
})();