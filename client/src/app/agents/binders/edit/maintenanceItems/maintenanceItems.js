(function() {
    "use strict";

    angular
        .module("hb.agents.binders.edit_binder.maintenanceItems", [
            "hb.agents.binders.edit_binder.maintenanceItems.index",
            "hb.agents.binders.edit_binder.maintenanceItems.edit",
            "hb.agents.binders.edit_binder.maintenanceItems.new",
            "hb.agents.binders.edit_binder.maintenanceItems.newTemplate"
        ]);
})();