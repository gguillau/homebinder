(function() {
    "use strict";

    angular.module("hb.agents.binders.edit_binder.maintenanceItems.edit", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('agent.edit_binder.maintenanceItemsEdit', {
                    url: '^/partners/:userId/binders/:binderId/maintenanceItems/{id:int}/edit',
                    templateUrl: "homebinders/homebinder/maintenanceItems/form/form.tpl.html",
                    controller: "AgentBindersMaintenanceItemsFormController",
                    controllerAs: "ctrl",
                    bindToController: true,
                    scope: {},
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();