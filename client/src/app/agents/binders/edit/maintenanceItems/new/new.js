(function() {
    "use strict";

    angular.module("hb.agents.binders.edit_binder.maintenanceItems.new", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('agent.edit_binder.maintenanceItemsNew', {
                    url: '^/agents/:userId/binders/:binderId/maintenanceItems/new',
                    templateUrl: "homebinders/homebinder/maintenanceItems/form/form.tpl.html",
                    controller: "AgentBindersMaintenanceItemsFormController",
                    controllerAs: "ctrl",
                    bindToController: true,
                    scope: {},
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        // Model backing the appliance modal form
        .controller("AgentBindersMaintenanceItemsFormController", [
            "MaintenanceItemFormController",
            "$stateParams",
            AgentBindersMaintenanceItemsFormController
        ]);

    function AgentBindersMaintenanceItemsFormController(MaintenanceItemFormController, $stateParams) {
        var Model = function() {
            this.indexState = "agent.edit_binder.maintenanceItems";
            this.indexParams = { binderId: $stateParams.binderId, userId: $stateParams.userId };
            this.editState = "agent.edit_binder.maintenanceItemsEdit";
            MaintenanceItemFormController.call(this);
        };

        Model.prototype = Object.create(MaintenanceItemFormController.prototype);

        angular.extend(Model.prototype, {});

        this.model = new Model();
    }
})();