(function() {
    "use strict";

    angular.module("hb.agents.binders.edit_binder.contractors.new", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('agent.edit_binder.contractorsNew', {
                    url: '^/agents/:userId/binders/:binderId/contractors/new',
                    templateUrl: "homebinders/homebinder/binderContractors/form/form.tpl.html",
                    controller: "AgentBindersContractorFormController",
                    controllerAs: "ctrl",
                    bindToController: true,
                    scope: {},
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .controller("AgentBindersContractorFormController", [
            "BinderContractorFormController",
            "$stateParams",
            AgentBindersContractorFormController
        ]);

    function AgentBindersContractorFormController(BinderContractorFormController, $stateParams) {
        var Model = function() {
            this.indexState = "agent.edit_binder.contractors";
            this.indexParams = { binderId: $stateParams.binderId, userId: $stateParams.userId };
            this.editState = "agent.edit_binder.contractorsEdit";
            BinderContractorFormController.call(this);
        };

        Model.prototype = Object.create(BinderContractorFormController.prototype);

        angular.extend(Model.prototype, {});

        this.model = new Model();
    }
})();