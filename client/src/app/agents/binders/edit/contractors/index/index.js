(function() {
    "use strict";

    angular.module("hb.agents.binders.edit_binder.contractors.index", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('agent.edit_binder.contractors', {
                    url: '^/agents/:userId/binders/:binderId/contractors',
                    templateUrl: 'components/binders/form/contractors/contractors.tpl.html',
                    controller: "AgentBindersContractorsController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .controller("AgentBindersContractorsController", [
            "BinderFormBinderContractorsController",
            "$state",
            "$stateParams",
            AgentBindersContractorsController
        ]);

    function AgentBindersContractorsController(BinderFormBinderContractorsController, $state, $stateParams) {
        var Model = function() {
            // call the parent class
            BinderFormBinderContractorsController.call(this);
            this.itemNewState = "agent.edit_binder.contractorsNew";
            this.itemNewTemplateState = "agent.edit_binder.contactorsNewTemplate";
        };

        Model.prototype = Object.create(BinderFormBinderContractorsController.prototype);

        angular.extend(Model.prototype, {
            // send user back to editState
            edit: function(item) {
                $state.go("agent.edit_binder.contractorsEdit", { id: item.id, binderId: $stateParams.binderId, userId: $stateParams.userId });
            },

            showForm: function(item) {
                if (item) {
                    $state.go(this.itemNewTemplateState, { binderId: this.binder.id, templateId: item.id, userId: $stateParams.userId });
                }
                else {
                    $state.go(this.itemNewState, { binderId: $stateParams.binderId, userId: $stateParams.userId });
                }
            }
        });

        this.model = new Model();
    }
})();