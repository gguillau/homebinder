(function() {
    "use strict";

    angular.module("hb.agents.binders.edit_binder.contractors.newTemplate", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state("agent.edit_binder.contractorsNewTemplate", {
                    url: "^/agents/:userId/binders/:binderId/contractors/new/{templateId:int}",
                    templateUrl: "homebinders/homebinder/binderContractors/form/form.tpl.html",
                    controller: "AgentBindersContractorFormController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();