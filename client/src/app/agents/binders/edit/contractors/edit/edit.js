(function() {
    "use strict";

    angular.module("hb.agents.binders.edit_binder.contractors.edit", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('agent.edit_binder.contractorsEdit', {
                    url: '^/agents/:userId/binders/:binderId/contractors/{id:int}/edit',
                    templateUrl: "homebinders/homebinder/binderContractors/form/form.tpl.html",
                    controller: "AgentBindersContractorFormController",
                    controllerAs: "ctrl",
                    bindToController: true,
                    scope: {},
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();