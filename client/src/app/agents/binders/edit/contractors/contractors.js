(function() {
    "use strict";

    angular
        .module("hb.agents.binders.edit_binder.contractors", [
            "hb.agents.binders.edit_binder.contractors.index",
            "hb.agents.binders.edit_binder.contractors.edit",
            "hb.agents.binders.edit_binder.contractors.new",
            "hb.agents.binders.edit_binder.contractors.newTemplate"
        ]);
})();