(function() {
    "use strict";

    angular.module("hb.agents.binders.edit_binder.appliances.index", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('agent.edit_binder.appliances', {
                    url: '^/agents/:userId/binders/:binderId/appliances',
                    templateUrl: 'components/binders/form/appliances/appliances.tpl.html',
                    controller: "AgentBindersAppliancesController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        .controller("AgentBindersAppliancesController", [
            "BinderFormAppliancesController",
            "$state",
            "$stateParams",
            AgentBindersAppliancesController
        ]);

    function AgentBindersAppliancesController(BinderFormAppliancesController, $state, $stateParams) {
        var Model = function() {
            // call the parent class
            BinderFormAppliancesController.call(this);
            this.itemNewState = "agent.edit_binder.appliancesNew";
            this.itemNewTemplateState = "agent.edit_binder.appliancesNewTemplate";
        };

        Model.prototype = Object.create(BinderFormAppliancesController.prototype);

        angular.extend(Model.prototype, {
            // send user back to editState
            edit: function(item) {
                $state.go("agent.edit_binder.appliancesEdit", { id: item.id, binderId: $stateParams.binderId, userId: $stateParams.userId });
            },

            showForm: function(item) {
                if (item) {
                    $state.go(this.itemNewTemplateState, { binderId: this.binder.id, templateId: item.id, userId: $stateParams.userId });
                }
                else {
                    $state.go(this.itemNewState, { binderId: $stateParams.binderId, userId: $stateParams.userId });
                }
            }
        });

        this.model = new Model();
    }
})();