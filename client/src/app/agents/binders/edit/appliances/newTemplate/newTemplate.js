(function() {
    "use strict";

    angular.module("hb.agents.binders.edit_binder.appliances.newTemplate", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state("agent.edit_binder.appliancesNewTemplate", {
                    url: "^/agents/:userId/binders/:binderId/appliances/new/{templateId:int}",
                    templateUrl: "homebinders/homebinder/appliances/form/form.tpl.html",
                    controller: "AgentBindersApplianceFormController",
                    controllerAs: "ctrl",
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();