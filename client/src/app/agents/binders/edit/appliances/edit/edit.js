(function() {
    "use strict";

    angular.module("hb.agents.binders.edit_binder.appliances.edit", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('agent.edit_binder.appliancesEdit', {
                    url: '^/agents/:userId/binders/:binderId/appliances/{id:int}/edit',
                    templateUrl: "homebinders/homebinder/appliances/form/form.tpl.html",
                    controller: "AgentBindersApplianceFormController",
                    controllerAs: "ctrl",
                    bindToController: true,
                    scope: {},
                    access: {
                        requiresLogin: true
                    }
                });
        }]);
})();