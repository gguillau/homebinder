(function() {
    "use strict";

    angular.module("hb.agents.binders.edit_binder.appliances.new", [])
        .config(["$stateProvider", function($stateProvider) {
            $stateProvider
                .state('agent.edit_binder.appliancesNew', {
                    url: '^/agents/:userId/binders/:binderId/appliances/new',
                    templateUrl: "homebinders/homebinder/appliances/form/form.tpl.html",
                    controller: "AgentBindersApplianceFormController",
                    controllerAs: "ctrl",
                    bindToController: true,
                    scope: {},
                    access: {
                        requiresLogin: true
                    }
                });
        }])
        // Model backing the appliance modal form
        .controller("AgentBindersApplianceFormController", [
            "ApplianceFormController",
            "$stateParams",
            AgentBindersApplianceFormController
        ]);

    function AgentBindersApplianceFormController(ApplianceFormController, $stateParams) {
        var Model = function() {
            this.indexState = "agent.edit_binder.appliances";
            this.indexParams = { binderId: $stateParams.binderId, userId: $stateParams.userId };
            this.editState = "agent.edit_binder.appliancesEdit";
            ApplianceFormController.call(this);
        };

        Model.prototype = Object.create(ApplianceFormController.prototype);

        angular.extend(Model.prototype, {});

        this.model = new Model();
    }
})();