(function() {
    "use strict";

    angular
        .module("hb.agents.binders.edit_binder.appliances", [
            "hb.agents.binders.edit_binder.appliances.index",
            "hb.agents.binders.edit_binder.appliances.edit",
            "hb.agents.binders.edit_binder.appliances.new",
            "hb.agents.binders.edit_binder.appliances.newTemplate"
        ]);
})();