(function() {
	"use strict";

	angular.module('hb.agents.binders', [
			"hb.agents.binders.new",
			"hb.agents.binders.edit_binder"
		])
		.config(['$stateProvider', agentBindersConfig])
		.controller("AgentsBindersIndexController", [
			"BindersIndexController",
			"$stateParams",
			"Context",
			"RepairPricerModal",
			"$sce",
			"Event",
			"hb.api",
			AgentsBindersIndexController
		]);

	function AgentsBindersIndexController(BindersIndexController, $stateParams, Context, RepairPricerModal, $sce, event, api) {

		var Model = function() {
			// call the parent class
			BindersIndexController.call(this);
			this.$stateParams = $stateParams;
			this.editState = "agent.edit_binder.client";
			this.newState = "agent.new_binder.client";
			this.newParams = { userId: $stateParams.userId };
			// remove owner header
			this.headers.splice(4, 1);
			// remove recall header
			this.headers.splice(4, 1);
			this.headers.push({
				name: this.resources.createdBy,
				order: "desc",
				orderBy: null,
				show: true,
				sortable: false,
				sorted: false
			});
			this.headers.push({
				name: this.resources.sellerReport,
				order: "desc",
				orderBy: null,
				show: true,
				sortable: false,
				sorted: false
			});
			this.searchTypes = [{
				name: "all",
				value: this.resources.allBinders
			}, {
				name: "transferred",
				value: this.resources.transferredBinders
			}, {
				name: "shared",
				value: this.resources.sharedBinders
			}, {
				name: "created",
				value: this.resources.nonTransferredBinders
			}];
			this.searchType = this.searchTypes[3].name;
			angular.extend(this.toolbarCfg, {
				button: {
					title: this.resources.newButton,
					click: angular.bind(this, this.newBinder)
				}
			});
			this.refresh();
		};

		Model.prototype = Object.create(BindersIndexController.prototype);

		angular.extend(Model.prototype, {

			refresh: function() {
				var opts = {
					page: this.currentPage,
					count: this.itemsPerPage,
					order: this.sortOption.order,
					orderBy: this.sortOption.orderBy,
					searchMethod: "for_user",
					userId: this.$stateParams.userId
				};

				if (this.searchValue) {
					opts.search = this.searchValue;
				}

				var promise;
				this.loading.show(this.resources.loading);

				// get all binders for the agent
				promise = this.api.binder.all(opts);

				promise.then(
					angular.bind(this, this.onRefreshSuccess),
					angular.bind(this, this.onRefreshError)
				);
			}
		});

		this.model = new Model();
	}

	function agentBindersConfig($stateProvider) {
		$stateProvider
			.state('agent.binders', {
				url: '^/agents/:userId/binders',
				templateUrl: 'agents/binders/index.tpl.html',
				controller: "AgentsBindersIndexController",
				controllerAs: "ctrl",
				access: {
					requiresLogin: true
				}
			});
	}

})();