# The `src/less` Directory

This folder is actually fairly self-explanatory: it contains your LESS/CSS files to be compiled during the build. 
The only important thing to note is that *only* `main.less` will be processed during the build, meaning that all
other stylesheets must be *imported* into that one.

This should operate somewhat like the routing; the `main.less` file contains all of the site-wide styles, while
any styles that are route-specific should be imported into here from LESS files kept alongside the JavaScript
and HTML sources of that component. For example, the `home` section of the site has some custom styles, which
are imported like so:

```css
@import '../app/home/home.less';
```