# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [21.3.0] - 2018-02-05
### Added

bf40b51 - Girgy :Girgy  Sprint-24 - client side spec tests
ba39780 - Girgy :Girgy  Sprint-24 - agent settings spec tests
1fdbe23 - Girgy :Girgy  Sprint-24 - admin partners spec tests
551e2e7 - Girgy :Girgy  Sprint-24 - partner users new controller spec tests
1855799 - Girgy :Girgy  Sprint-24 - Partners Binders New controller spec tests
990f58e - Girgy :Girgy  Sprint-24 - partner resources spec tests
c8b9cd9 - Girgy :Girgy  Sprint-24 - PartnersResourcesController spec tests
e88d43c - Girgy :Girgy  Sprint-24 - AdminSellerReportCtrl spec tests
0c89fc3 - Girgy :Girgy  Sprint-24 - Organization Marketing Resources spec tests
eb50aa2 - Girgy :Girgy  Sprint-24 - update spec files
834a2bf - Girgy :Girgy  Sprint-24 - client side spec tests
e0d297b - Girgy :Girgy  Sprint-24 - util.address tests
4946476 - Girgy :Girgy  Sprint-24 - PartnerBindersIndexController spec tests
b87e4d7 - Girgy :Girgy  Sprint-24 - updated sell side functionality
6bd6e1f - Girgy :Girgy  Sprint-24 - updates to sell side workflow
7aebd87 - Girgy :Girgy  Sprint-24 - update spec tests
05eae21 - Girgy :Girgy  Sprint-24 - set default template transfer note
11ebab3 - Girgy :Girgy  Sprint-24 - update monthly email subject line
d600872 - Girgy :Girgy  Sprint-24 - additional resource strings test
e9a7885 - Girgy :Girgy  Sprint-24 - initial client side tests for api calls
be3e8ef - Girgy :Girgy  Sprint-24 - initial client side tests for resources folder
b82e336 - Girgy :Girgy  Sprint-24 - edit spec test
6cb754b - Girgy :Girgy  Sprint-24 - modalBase tests
68bb8c6 - Girgy :Girgy  Sprint-24 - framework indexbase spec tests
9fe9449 - Girgy :Girgy  Sprint-24 - updates to stripe payment failed webhook
1161511 - Girgy :Girgy  Sprint-24 - first pass at stripe payment failed webhook
a44cadb - Girgy :Girgy  Sprint-24 - partner client login downloads
e8477ce - Girgy :Girgy  Sprint-24 - update pending sprint items
25ae343 - Girgy :Girgy  Sprint-24 - set reset password token and date to nil upon signing in
4379a88 - Girgy :Girgy  Sprint-24 - account mailer spec
f250904 - Girgy :Girgy  Sprint-24 - update partner registration mailer
2fed76b - Girgy :Girgy  Sprint-24 - update test
545125e - Girgy :Girgy  Sprint-24 - make sure maintenance item mailer receives correct do date
2faaa82 - Girgy :Girgy  Sprint-24 - prevent user from transferring binder to self
7214cd7 - Girgy :Girgy  Sprint-24 - display issue for maintenance item frequency in binder form
8c53c25 - Girgy :Girgy  Sprint-24 - homebinder controller spec test
8118f3a - Girgy :Girgy  Sprint-24 - check for nil user in partner get_admin
31e79a8 - Girgy :Girgy  Sprint-24 - check for nil objects in appliance recall serializer
2f4b46c - Girgy :Girgy  Sprint-24 - check for nil user in transfer service
2496376 - Girgy :Girgy  Sprint-24 - fix for SubscriptionSerializer
e7ca3ad - Girgy :Girgy  Sprint-24 - remove tag_scope in tag service
a745ee8 - Girgy :Girgy  Sprint-24 - rescue_from ActiveModel::UnknownAttributeError
87b9ab8 - Girgy :Girgy  Sprint-24 - update ABS
3c6cd89 - Girgy :Girgy  Sprint-24 - remove call to new relic in abs error actions
0ec6b48 - Girgy :Girgy  Sprint-24 - update keen service
224e5e0 - Girgy :Girgy  Sprint-24 - sort maintenance items by due date
162a171 - Girgy :Girgy  Sprint-24 - update password reset template
f26f31a - Girgy :Girgy  Sprint-24 - set password reset time frame as an ENV setting
2333145 - Girgy :Girgy  Sprint-24 - update Invite email for agents to follow template under HB-644
a96df6d - Girgy :Girgy  Sprint-24 - Update blank binder text to include new integrations
aa59d24 - Girgy :Girgy  Sprint-24 - Close item when you click save, intstead of showing in view and then having to click close
df4b952 - Girgy :Girgy  Sprint-24 - change price on inspector landing page to reflect the .14...6 for 0-50 inspections, 9 for 50-200 inspections
f25ce8b - Girgy :Girgy  Sprint-24 - Increase Binder items per page to 40
7049269 - Girgy :Girgy  Sprint-24 - Change the price of premium homebinder for homeowners
f33a6c8 - Girgy :Girgy  Sprint-24 - password reset updates
94246f7 - Girgy :Girgy  Merge branch 'master' into sprint-24
f1b555e - Girgy :Girgy  Sprint-24 - master merge
d27e097 - Girgy :Girgy  Sprint-24 - update binder service spec test
4db2ceb - Girgy :Girgy  Sprint-24 - update buildfax tests to mock API calls
b04f573 - Girgy :Girgy  Sprint-24 - tests for refactored binder items
eb847df - Girgy :Girgy  Sprint-24 - first pass at refactoring binder items
84b119a - Girgy :Girgy  Sprint-24 - HomeownerWelcomeController spec tests
e87647b - Girgy :Girgy  Sprint-24 - recalls spec tests
2a3bc61 - Girgy :Girgy  Sprint-24 - AdminRecallsFormCtrl spec tests
d6f7f1e - Girgy :Girgy  Sprint-24 - RecallsController spec tests
02ed520 - Girgy :Girgy  Sprint-24 - PartnerLookupController spec tests
433f97b - Girgy :Girgy  Sprint-24 - ApplicationHeaderController updates and spec tests
f47116c - Girgy :Girgy  Sprint-24 - remove unused gridInlineAddress filter
d5375e2 - Girgy :Girgy  Sprint-24 - hb.utils.filters.enum spec tests
0ffa382 - Girgy :Girgy  Sprint-24 - PartnerEditWarrantyConfigController spec tests
494845d - Girgy :Girgy  Sprint-24 - forward pending users to welcome page
07d8662 - Girgy :Girgy  Sprint-24 - update server tests
cd3818b - Girgy :Girgy  Sprint-24 - run new relic send error in background
0b95993 - Girgy :Girgy  Merge branch 'master' into sprint-24
f68665d - Girgy :Girgy  Sprint-24 - add bang to create calls
258fcb1 - Girgy :Girgy  Sprint-24 - add bang to save calls
aab6b16 - Girgy :Girgy  Sprint-24 - add retry to delayed job objects
aba2d83 - Girgy :Girgy  Sprint-24 - update partner permissions
8bba9f5 - Girgy :Girgy  Sprint-24 - allow multiple co owners to receive maintenance reminders
553aeea - Girgy :Girgy  Sprint-24 - update seed rake task and README.md
c12e6c1 - Girgy :Girgy  Sprint-24 - add time to binder table created at
6c4aec7 - Girgy :Girgy  Sprint-24 - master merge
b82cedd - Girgy :Girgy  Sprint-24 - add rails_helper to spec file
3bd456e - Girgy :Girgy  Sprint-24 - run additional binder item actions in background
ff9bdfd - Girgy :Girgy  Sprint-24 - PermitModalController spec tests
b061570 - Girgy :Girgy  Sprint-24 - HomeProLookupController spec tests
22b3c8b - Girgy :Girgy  Sprint-24 - UserLookupController spec tests
f00cd5c - Girgy :Girgy  Sprint-24 - agents binder form spec tests
3a51283 - Girgy :Girgy  Sprint-24 - WidgetPicker spec tests
b9ab4a0 - Girgy :Girgy  Sprint-24 - DashboardModalController spec tests
2421b6d - Girgy :Girgy  Sprint-23 - binder form changes