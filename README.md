# ![alt text](https://s3.amazonaws.com/homebinderstatic/img/flatlogo2s.png) HomeBinder [ ![Codeship Status for homebindercominc/homebinder](https://app.codeship.com/projects/b9a19400-9a92-0135-3d75-2e393d67ec8d/status?branch=master)](https://app.codeship.com/projects/252431)

HomeBinder is a full service home management system that focuses on home sales.
Sellers can convey the value of their home in HomeBinder reports and buyers can get
valuable details to help them manage their new home.

The product helps homeowners store all their home, property, project, and contractor
information in one place. HomeBinder will provide reminders for maintenance items, offer
up information on appliance recalls and enhance the process of selling a home when the
time comes.

Depending on the scenario, the broker or homeowner is responsible for initial information
gathering. Some data is automatically pulled into the binder through public records and
MLS type data, but agents and homeowners are responsible for more granular information
such maintenance scheduling (chimney, septic tank, etc.), appliance details, contractor
records, etc.

Of course, HomeBinder makes this process simple. The product is built such that users can
easily select and input information. It leads the homeowner in the right direction,
ensuring all pertinent details are logged.

## Built With

* Ruby on Rails
* AngularJS
* GruntJS
* NodeJS
* Heroku

## Getting Started

After you clone this repo to your desktop, go to its root folder and run the
following command:

1. `bash install.sh`

## Log In

To log in for the first, run the following commands in your terminal

1. `rails c`
2. `user = User.find_by_email("dev@homebinder.com")`
2. `user.password = ` set the password to something you'll remember
3. `user.save`
4. Log in

## Running the tests

To run the client side unit tests, from the root of the repository, enter the client with `cd client`
folder and run `grunt karma`

To run the server side unit tests, from the root of the repository, run `rspec`

# Kibana

sudo vim /etc/kibana/kibana.yml

## Documentation

Look under the `doc` folder to read documentation on our server side and client side code.

## Authors

* **Jim Walker**            -  **jim@homebinder.com**
* **Jon Durante**           -  **jon@homebinder.com**
* **Riley Roberts**         -  **riley@homebinder.com**
* **Gerlin Guillaume**      -  **gerlin@homebinder.com**
* **Matthew Young**         -  **matt@homebinder.com**