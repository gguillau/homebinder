# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20191009075840) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "abs_files", force: :cascade do |t|
    t.integer  "binder_id",                        null: false
    t.string   "payload_file_name",                null: false
    t.string   "payload_content_type",             null: false
    t.integer  "payload_file_size",                null: false
    t.datetime "payload_updated_at",               null: false
    t.integer  "status",               default: 0, null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.index ["binder_id"], name: "index_abs_files_on_binder_id", using: :btree
  end

  create_table "accounts", force: :cascade do |t|
    t.integer  "manager_id"
    t.string   "account_status",               limit: 50,                  null: false
    t.string   "account_sub_status",           limit: 50
    t.string   "payment_type",                 limit: 50
    t.string   "account_type",                 limit: 50,                  null: false
    t.string   "account_sub_type",             limit: 50,                  null: false
    t.datetime "trial_expiration"
    t.datetime "billing_activation"
    t.integer  "subscription_amount_cents",                default: 0,     null: false
    t.string   "subscription_amount_currency",             default: "USD", null: false
    t.integer  "transaction_amount_cents",                 default: 0,     null: false
    t.string   "transaction_amount_currency",              default: "USD", null: false
    t.string   "name",                         limit: 500
    t.text     "notes"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "account_number",               limit: 12
    t.string   "billing_frequency",            limit: 50
    t.string   "stripe_customer_id"
    t.index ["manager_id"], name: "index_accounts_on_manager_id", using: :btree
  end

  create_table "addresses", force: :cascade do |t|
    t.string  "addressable_type"
    t.integer "addressable_id"
    t.string  "name"
    t.string  "country",                 default: "US"
    t.string  "address1"
    t.string  "address2"
    t.string  "city"
    t.string  "state"
    t.string  "zip"
    t.float   "lat"
    t.float   "long"
    t.integer "build_fax_report_id"
    t.integer "partner_id"
    t.integer "user_profile_id"
    t.integer "warranty_id"
    t.integer "contractor_id"
    t.integer "contractor_template_id"
    t.integer "repair_pricer_report_id"
    t.integer "partner_contractor_id"
    t.integer "user_contractor_id"
    t.index ["addressable_id", "addressable_type"], name: "index_addresses_on_addressable_id_and_addressable_type", using: :btree
    t.index ["build_fax_report_id"], name: "index_addresses_on_build_fax_report_id", using: :btree
    t.index ["contractor_id"], name: "index_addresses_on_contractor_id", using: :btree
    t.index ["contractor_template_id"], name: "index_addresses_on_contractor_template_id", using: :btree
    t.index ["partner_contractor_id"], name: "index_addresses_on_partner_contractor_id", using: :btree
    t.index ["partner_id"], name: "index_addresses_on_partner_id", using: :btree
    t.index ["repair_pricer_report_id"], name: "index_addresses_on_repair_pricer_report_id", using: :btree
    t.index ["user_contractor_id"], name: "index_addresses_on_user_contractor_id", using: :btree
    t.index ["user_profile_id"], name: "index_addresses_on_user_profile_id", using: :btree
    t.index ["warranty_id"], name: "index_addresses_on_warranty_id", using: :btree
  end

  create_table "annual_property_review_capital_items", force: :cascade do |t|
    t.integer "annual_property_review_id"
    t.string  "name",                      limit: 255,             null: false
    t.integer "eul",                                   default: 0, null: false
    t.integer "cost_range",                            default: 0, null: false
    t.text    "comments"
    t.index ["annual_property_review_id"], name: "apr_capital_index", using: :btree
  end

  create_table "annual_property_review_findings", force: :cascade do |t|
    t.integer  "annual_property_review_id"
    t.string   "name",                      limit: 255,             null: false
    t.integer  "priority",                              default: 0, null: false
    t.integer  "estimated_cost",                        default: 0, null: false
    t.text     "details"
    t.integer  "recommended_homepro_id"
    t.integer  "selected_homepro_id"
    t.integer  "status",                                default: 0, null: false
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.boolean  "homeowner_item"
    t.index ["annual_property_review_id"], name: "apr_finding_index", using: :btree
  end

  create_table "annual_property_review_photos", force: :cascade do |t|
    t.integer  "annual_property_review_id"
    t.integer  "annual_property_review_finding_id"
    t.integer  "annual_property_review_capital_item_id"
    t.string   "name",                                   limit: 255, null: false
    t.text     "description"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.index ["annual_property_review_capital_item_id"], name: "apr_photo_capital_item_index", using: :btree
    t.index ["annual_property_review_finding_id"], name: "apr_photo_finding_index", using: :btree
    t.index ["annual_property_review_id"], name: "apr_photo_index", using: :btree
  end

  create_table "annual_property_reviews", force: :cascade do |t|
    t.integer  "binder_id"
    t.string   "uuid",                       limit: 32,                  null: false
    t.string   "client_first_name",          limit: 100,                 null: false
    t.string   "client_last_name",           limit: 100,                 null: false
    t.string   "client_email",               limit: 100,                 null: false
    t.string   "client_phone",               limit: 30,                  null: false
    t.string   "address1",                   limit: 100,                 null: false
    t.string   "address2",                   limit: 100
    t.string   "city",                       limit: 100,                 null: false
    t.string   "state",                      limit: 100,                 null: false
    t.string   "zip",                        limit: 100,                 null: false
    t.string   "country",                    limit: 100,                 null: false
    t.integer  "status",                                 default: 0,     null: false
    t.integer  "reviewer_id"
    t.integer  "reviewer_company_id"
    t.datetime "scheduled_for"
    t.datetime "scheduled_on"
    t.datetime "completed_on"
    t.datetime "on_hold_on"
    t.datetime "canceled_on"
    t.text     "client_comments"
    t.integer  "score",                                  default: 0
    t.text     "cancel_reason"
    t.integer  "amount_charged_cents",                   default: 0,     null: false
    t.string   "amount_charged_currency",                default: "USD", null: false
    t.integer  "payment_status",                         default: 0
    t.datetime "reviewed_on"
    t.text     "property_reviewer_comments"
    t.text     "admin_reviewer_comments"
    t.string   "stripe_charge_id",           limit: 50
    t.string   "stripe_refund_id",           limit: 50
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.string   "snapshot_file_name"
    t.string   "snapshot_content_type"
    t.integer  "snapshot_file_size"
    t.datetime "snapshot_updated_at"
    t.string   "promo_code_name"
    t.integer  "promo_code_amount_off"
    t.index ["binder_id"], name: "index_annual_property_reviews_on_binder_id", using: :btree
    t.index ["client_email"], name: "index_annual_property_reviews_on_client_email", using: :btree
    t.index ["reviewer_company_id"], name: "index_annual_property_reviews_on_reviewer_company_id", using: :btree
    t.index ["reviewer_id"], name: "index_annual_property_reviews_on_reviewer_id", using: :btree
    t.index ["uuid"], name: "index_annual_property_reviews_on_uuid", using: :btree
  end

  create_table "api_keys", force: :cascade do |t|
    t.string   "company_name"
    t.string   "application_name"
    t.string   "key"
    t.string   "contact_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "partner_id"
    t.index ["key"], name: "index_api_keys_on_key", using: :btree
    t.index ["partner_id"], name: "index_api_keys_on_partner_id", using: :btree
  end

  create_table "appliance_manufacturers", force: :cascade do |t|
    t.string  "name"
    t.integer "created_by"
    t.boolean "verified"
  end

  create_table "appliance_models", force: :cascade do |t|
    t.string  "name"
    t.integer "created_by"
    t.boolean "verified"
  end

  create_table "appliance_recalls", force: :cascade do |t|
    t.integer  "appliance_id"
    t.integer  "recall_id"
    t.integer  "strength"
    t.string   "status",        limit: 25
    t.datetime "status_date"
    t.integer  "notifications"
    t.string   "category"
    t.string   "sub_category"
    t.index ["appliance_id"], name: "index_appliance_recalls_on_appliance_id", using: :btree
    t.index ["recall_id"], name: "index_appliance_recalls_on_recall_id", using: :btree
  end

  create_table "appliance_templates", force: :cascade do |t|
    t.integer  "binder_template_id"
    t.boolean  "library_template",                   default: false
    t.integer  "library_source_id"
    t.string   "name",                  limit: 100,                  null: false
    t.string   "notes",                 limit: 5000
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.string   "video_file_name"
    t.string   "video_content_type"
    t.integer  "video_file_size"
    t.datetime "video_updated_at"
    t.string   "document_file_name"
    t.string   "document_content_type"
    t.integer  "document_file_size"
    t.datetime "document_updated_at"
    t.text     "description",                        default: "",    null: false
    t.index ["binder_template_id"], name: "index_appliance_templates_on_binder_template_id", using: :btree
    t.index ["library_source_id"], name: "index_appliance_templates_on_library_source_id", using: :btree
    t.index ["library_template"], name: "index_appliance_templates_on_library_template", using: :btree
  end

  create_table "appliance_types", force: :cascade do |t|
    t.string  "name"
    t.integer "created_by"
    t.boolean "verified"
  end

  create_table "appliances", force: :cascade do |t|
    t.integer  "binder_id"
    t.string   "name"
    t.integer  "appliance_type_id"
    t.integer  "appliance_manufacturer_id"
    t.integer  "appliance_model_id"
    t.string   "model_number"
    t.string   "serial_no"
    t.string   "warranty"
    t.string   "user_guide_url"
    t.text     "details"
    t.integer  "created_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "last_recall_check"
    t.string   "upc"
    t.string   "appliance_type"
    t.string   "manufacturer"
    t.string   "model"
    t.date     "install_date"
    t.integer  "library_source_id"
    t.boolean  "verified",                  default: true
    t.index ["binder_id"], name: "index_appliances_on_binder_id", using: :btree
  end

  create_table "area_types", force: :cascade do |t|
    t.string  "name"
    t.integer "created_by"
    t.boolean "verified"
  end

  create_table "areas", force: :cascade do |t|
    t.integer  "binder_id"
    t.string   "name"
    t.integer  "structure_id"
    t.text     "details"
    t.string   "dimensions"
    t.integer  "area_type_id"
    t.integer  "created_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "area_type"
    t.boolean  "verified",     default: true
    t.index ["binder_id"], name: "index_areas_on_binder_id", using: :btree
  end

  create_table "areas_paints", id: false, force: :cascade do |t|
    t.integer "area_id"
    t.integer "paint_id"
  end

  create_table "automation_errors", force: :cascade do |t|
    t.string   "partner_name"
    t.string   "binder_template_id"
    t.string   "client_first"
    t.string   "client_last"
    t.string   "client_email"
    t.string   "property_address"
    t.string   "property_address2"
    t.string   "property_city"
    t.string   "property_state"
    t.string   "property_postalcode"
    t.string   "property_country"
    t.string   "agent_name"
    t.string   "agent_contact"
    t.string   "agent_phone"
    t.string   "agent_email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "error_message"
    t.string   "api_key"
  end

  create_table "binder_brandings", force: :cascade do |t|
    t.string   "scope",            limit: 255, null: false
    t.integer  "binder_id",                    null: false
    t.integer  "user_branding_id",             null: false
    t.integer  "partner_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.index ["binder_id"], name: "index_binder_brandings_on_binder_id", using: :btree
    t.index ["partner_id"], name: "index_binder_brandings_on_partner_id", using: :btree
    t.index ["user_branding_id"], name: "index_binder_brandings_on_user_branding_id", using: :btree
  end

  create_table "binder_contractor_sub_types", force: :cascade do |t|
    t.integer  "binder_contractor_id",       null: false
    t.integer  "contractor_sub_category_id", null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["binder_contractor_id", "contractor_sub_category_id"], name: "index_bcontractors_stype_on_bc_id_and_csc_id", using: :btree
    t.index ["binder_contractor_id"], name: "bc_contractor_sub_type_index", using: :btree
    t.index ["contractor_sub_category_id"], name: "contractor_sub_type_bc_contractor_index", using: :btree
  end

  create_table "binder_contractor_types", force: :cascade do |t|
    t.integer  "binder_contractor_id",   null: false
    t.integer  "contractor_category_id", null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["binder_contractor_id", "contractor_category_id"], name: "index_bcontractors_type_on_bc_id_and_cc_id", using: :btree
    t.index ["binder_contractor_id"], name: "index_binder_contractor_types_on_binder_contractor_id", using: :btree
    t.index ["contractor_category_id"], name: "index_binder_contractor_types_on_contractor_category_id", using: :btree
  end

  create_table "binder_contractors", force: :cascade do |t|
    t.integer  "binder_id"
    t.integer  "contractor_id"
    t.integer  "created_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "details"
    t.string   "account_number"
    t.string   "contact"
    t.integer  "library_source_id"
    t.boolean  "verified",              default: true
    t.boolean  "worked_on_property",    default: false
    t.string   "secondary_name"
    t.string   "secondary_email"
    t.string   "secondary_phone"
    t.text     "secondary_details"
    t.boolean  "send_email_invitation", default: true
    t.integer  "user_contractor_id"
    t.index ["binder_id"], name: "index_binder_contractors_on_binder_id", using: :btree
    t.index ["contractor_id"], name: "index_binder_contractors_on_contractor_id", using: :btree
    t.index ["user_contractor_id"], name: "index_binder_contractors_on_user_contractor_id", using: :btree
  end

  create_table "binder_items", force: :cascade do |t|
    t.integer  "binder_id"
    t.integer  "user_id"
    t.string   "item_type"
    t.integer  "item_id"
    t.boolean  "verified",             default: false
    t.boolean  "dismissed",            default: false
    t.datetime "last_notify_date"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "appliance_id"
    t.integer  "area_id"
    t.integer  "binder_contractor_id"
    t.integer  "finish_id"
    t.integer  "inventory_item_id"
    t.integer  "maintenance_item_id"
    t.integer  "paint_id"
    t.integer  "property_id"
    t.integer  "receipt_id"
    t.integer  "structure_id"
    t.integer  "image_id"
    t.integer  "document_id"
    t.integer  "permit_id"
    t.integer  "project_id"
    t.index ["appliance_id"], name: "index_binder_items_on_appliance_id", using: :btree
    t.index ["area_id"], name: "index_binder_items_on_area_id", using: :btree
    t.index ["binder_contractor_id"], name: "index_binder_items_on_binder_contractor_id", using: :btree
    t.index ["binder_id"], name: "index_binder_items_on_binder_id", using: :btree
    t.index ["document_id"], name: "index_binder_items_on_document_id", using: :btree
    t.index ["finish_id"], name: "index_binder_items_on_finish_id", using: :btree
    t.index ["image_id"], name: "index_binder_items_on_image_id", using: :btree
    t.index ["inventory_item_id"], name: "index_binder_items_on_inventory_item_id", using: :btree
    t.index ["item_type", "item_id"], name: "index_binder_items_on_item_type_and_item_id", using: :btree
    t.index ["maintenance_item_id"], name: "index_binder_items_on_maintenance_item_id", using: :btree
    t.index ["paint_id"], name: "index_binder_items_on_paint_id", using: :btree
    t.index ["permit_id"], name: "index_binder_items_on_permit_id", using: :btree
    t.index ["project_id"], name: "index_binder_items_on_project_id", using: :btree
    t.index ["property_id"], name: "index_binder_items_on_property_id", using: :btree
    t.index ["receipt_id"], name: "index_binder_items_on_receipt_id", using: :btree
    t.index ["structure_id"], name: "index_binder_items_on_structure_id", using: :btree
    t.index ["user_id"], name: "index_binder_items_on_user_id", using: :btree
  end

  create_table "binder_templates", force: :cascade do |t|
    t.integer  "partner_configuration_id"
    t.string   "name",                     limit: 100,                  null: false
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.string   "transfer_note",            limit: 1000
    t.boolean  "system",                                default: false
    t.boolean  "default",                               default: false
    t.boolean  "partner",                               default: false
    t.boolean  "use_default_pros",                      default: true
    t.index ["partner_configuration_id"], name: "index_binder_templates_on_partner_configuration_id", using: :btree
  end

  create_table "binder_transactions", force: :cascade do |t|
    t.string   "tracking_number",  limit: 100
    t.money    "transaction_cost",              scale: 2
    t.string   "transaction_type", limit: 20
    t.datetime "transaction_date"
    t.string   "description",      limit: 1000
    t.integer  "binder_id",                               null: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.index ["binder_id"], name: "index_binder_transactions_on_binder_id", using: :btree
  end

  create_table "binders", force: :cascade do |t|
    t.string   "name"
    t.boolean  "primary"
    t.boolean  "active",               default: true
    t.integer  "created_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "details"
    t.integer  "hero_image_id"
    t.string   "create_method"
    t.string   "status",               default: "active"
    t.integer  "total_storage",        default: 0
    t.string   "custom_support_email"
    t.decimal  "completeness_score",   default: "0.0"
    t.integer  "binder_template_id"
    t.index ["binder_template_id"], name: "index_binders_on_binder_template_id", using: :btree
    t.index ["created_by"], name: "index_binders_on_created_by", using: :btree
    t.index ["hero_image_id"], name: "index_binders_on_hero_image_id", using: :btree
  end

  create_table "binders_users", id: false, force: :cascade do |t|
    t.integer "binder_id"
    t.integer "user_id"
    t.index ["binder_id"], name: "index_binders_users_on_binder_id", using: :btree
    t.index ["user_id"], name: "index_binders_users_on_user_id", using: :btree
  end

  create_table "build_fax_reports", force: :cascade do |t|
    t.integer  "partner_id"
    t.integer  "binder_id"
    t.string   "status",              default: "not_paid", null: false
    t.string   "report_file_name"
    t.string   "report_content_type"
    t.integer  "report_file_size"
    t.datetime "report_updated_at"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.index ["binder_id"], name: "index_build_fax_reports_on_binder_id", using: :btree
    t.index ["partner_id"], name: "index_build_fax_reports_on_partner_id", using: :btree
  end

  create_table "building_types", force: :cascade do |t|
    t.string  "name"
    t.integer "created_by"
    t.boolean "verified"
  end

  create_table "construction_styles", force: :cascade do |t|
    t.string  "name"
    t.integer "created_by"
    t.boolean "verified"
  end

  create_table "construction_types", force: :cascade do |t|
    t.string  "name"
    t.integer "created_by"
    t.boolean "verified"
  end

  create_table "consumer_recalls", force: :cascade do |t|
    t.text     "list"
    t.datetime "refreshed_at"
  end

  create_table "contractor_categories", force: :cascade do |t|
    t.string  "name"
    t.integer "created_by"
    t.boolean "verified"
  end

  create_table "contractor_category_types", force: :cascade do |t|
    t.integer  "contractor_type_id",      null: false
    t.integer  "contractor_sub_types_id", null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["contractor_sub_types_id"], name: "index_contractor_category_types_on_contractor_sub_types_id", using: :btree
    t.index ["contractor_type_id"], name: "index_contractor_category_types_on_contractor_type_id", using: :btree
  end

  create_table "contractor_contractor_abilities", id: false, force: :cascade do |t|
    t.integer "contractor_id"
    t.integer "contractor_ability_id"
  end

  create_table "contractor_sub_categories", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "contractor_sub_types", force: :cascade do |t|
    t.integer  "contractor_id",              null: false
    t.integer  "contractor_sub_category_id", null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["contractor_id"], name: "index_contractor_sub_types_on_contractor_id", using: :btree
    t.index ["contractor_sub_category_id"], name: "index_contractor_sub_types_on_contractor_sub_category_id", using: :btree
  end

  create_table "contractor_templates", force: :cascade do |t|
    t.integer  "binder_template_id"
    t.boolean  "library_template",      default: false
    t.integer  "library_source_id"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "partner_contractor_id"
    t.index ["binder_template_id"], name: "index_contractor_templates_on_binder_template_id", using: :btree
    t.index ["library_source_id"], name: "index_contractor_templates_on_library_source_id", using: :btree
    t.index ["library_template"], name: "index_contractor_templates_on_library_template", using: :btree
    t.index ["partner_contractor_id"], name: "index_contractor_templates_on_partner_contractor_id", using: :btree
  end

  create_table "contractor_types", force: :cascade do |t|
    t.integer  "contractor_id",          null: false
    t.integer  "contractor_category_id", null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["contractor_category_id"], name: "index_contractor_types_on_contractor_category_id", using: :btree
    t.index ["contractor_id"], name: "index_contractor_types_on_contractor_id", using: :btree
  end

  create_table "contractors", force: :cascade do |t|
    t.integer  "contractor_type_id"
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.string   "url"
    t.string   "account_number"
    t.string   "contact"
    t.text     "details"
    t.integer  "created_by"
    t.boolean  "verified"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "contractor_type"
    t.integer  "user_id"
    t.index ["created_at"], name: "index_contractors_on_created_at", using: :btree
    t.index ["created_by"], name: "index_contractors_on_created_by", using: :btree
    t.index ["email"], name: "index_contractors_on_email", using: :btree
    t.index ["name"], name: "index_contractors_on_name", using: :btree
    t.index ["phone"], name: "index_contractors_on_phone", using: :btree
    t.index ["user_id"], name: "index_contractors_on_user_id", using: :btree
  end

  create_table "coupons", force: :cascade do |t|
    t.integer  "partner_id"
    t.string   "code"
    t.string   "duration"
    t.datetime "expires_on"
    t.integer  "max_redemptions"
    t.boolean  "active"
    t.integer  "percent_off"
    t.integer  "amount_off"
    t.integer  "duration_months"
    t.index ["partner_id"], name: "index_coupons_on_partner_id", using: :btree
  end

  create_table "dashboard_widgets", force: :cascade do |t|
    t.integer  "dashboard_id"
    t.integer  "widget_id"
    t.integer  "index"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["dashboard_id"], name: "index_dashboard_widgets_on_dashboard_id", using: :btree
    t.index ["widget_id"], name: "index_dashboard_widgets_on_widget_id", using: :btree
  end

  create_table "dashboards", force: :cascade do |t|
    t.string   "name",            limit: 100
    t.string   "description",     limit: 1000
    t.integer  "organization_id"
    t.integer  "partner_id"
    t.integer  "binder_id"
    t.boolean  "default",                      default: false
    t.integer  "user_id"
    t.boolean  "system",                       default: false
    t.string   "scope"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["organization_id"], name: "index_dashboards_on_organization_id", using: :btree
    t.index ["partner_id"], name: "index_dashboards_on_partner_id", using: :btree
    t.index ["user_id"], name: "index_dashboards_on_user_id", using: :btree
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0
    t.integer  "attempts",   default: 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree
  end

  create_table "document_templates", force: :cascade do |t|
    t.integer  "binder_template_id"
    t.integer  "library_source_id"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.string   "notes",              limit: 5000
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.boolean  "library_template",                default: false
    t.text     "description",                     default: "",    null: false
    t.integer  "document_type_id",                default: 11,    null: false
    t.index ["binder_template_id"], name: "index_document_templates_on_binder_template_id", using: :btree
    t.index ["document_type_id"], name: "index_document_templates_on_document_type_id", using: :btree
    t.index ["library_source_id"], name: "index_document_templates_on_library_source_id", using: :btree
  end

  create_table "document_types", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "documents", force: :cascade do |t|
    t.string   "documentable_type"
    t.integer  "documentable_id"
    t.string   "name"
    t.string   "location"
    t.string   "key"
    t.string   "bucket"
    t.string   "etag"
    t.text     "details"
    t.integer  "created_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "binder_id"
    t.integer  "file_size"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.integer  "library_source_id"
    t.boolean  "verified",             default: true
    t.integer  "structure_id"
    t.integer  "area_id"
    t.integer  "maintenance_item_id"
    t.integer  "project_id"
    t.integer  "appliance_id"
    t.integer  "finish_id"
    t.integer  "paint_id"
    t.integer  "inventory_item_id"
    t.integer  "receipt_id"
    t.integer  "permit_id"
    t.integer  "binder_contractor_id"
    t.integer  "document_type_id"
    t.boolean  "accessible",           default: true
    t.string   "url"
    t.index ["binder_id"], name: "index_documents_on_binder_id", using: :btree
    t.index ["document_type_id"], name: "index_documents_on_document_type_id", using: :btree
  end

  create_table "finish_makes", force: :cascade do |t|
    t.string  "name"
    t.integer "created_by"
    t.boolean "verified"
  end

  create_table "finish_models", force: :cascade do |t|
    t.string  "name"
    t.integer "created_by"
    t.boolean "verified"
  end

  create_table "finishes", force: :cascade do |t|
    t.integer  "binder_id"
    t.string   "name"
    t.integer  "finish_make_id"
    t.integer  "finish_model_id"
    t.string   "style_color"
    t.text     "details"
    t.integer  "created_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "make"
    t.string   "model"
    t.string   "style",           limit: 100
    t.string   "color",           limit: 50
    t.boolean  "verified",                    default: true
    t.index ["binder_id"], name: "index_finishes_on_binder_id", using: :btree
  end

  create_table "geographies", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "heat_sources", force: :cascade do |t|
    t.string  "name"
    t.integer "created_by"
    t.boolean "verified"
  end

  create_table "heat_types", force: :cascade do |t|
    t.string  "name"
    t.integer "created_by"
    t.boolean "verified"
  end

  create_table "images", force: :cascade do |t|
    t.string   "imageable_type"
    t.integer  "imageable_id"
    t.string   "location"
    t.string   "key"
    t.string   "bucket"
    t.string   "etag"
    t.text     "details"
    t.integer  "created_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "binder_id"
    t.integer  "file_size"
    t.string   "name"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.boolean  "verified",             default: true
    t.integer  "structure_id"
    t.integer  "area_id"
    t.integer  "maintenance_item_id"
    t.integer  "project_id"
    t.integer  "appliance_id"
    t.integer  "finish_id"
    t.integer  "paint_id"
    t.integer  "inventory_item_id"
    t.integer  "receipt_id"
    t.integer  "permit_id"
    t.integer  "binder_contractor_id"
    t.index ["binder_id"], name: "index_images_on_binder_id", using: :btree
  end

  create_table "inspection_report_jobs", force: :cascade do |t|
    t.string   "job_id"
    t.integer  "status",                         default: 0, null: false
    t.string   "inspection_report_file_name"
    t.string   "inspection_report_content_type"
    t.integer  "inspection_report_file_size"
    t.datetime "inspection_report_updated_at"
    t.integer  "binder_id"
    t.integer  "partner_id"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.index ["binder_id"], name: "index_inspection_report_jobs_on_binder_id", using: :btree
    t.index ["partner_id"], name: "index_inspection_report_jobs_on_partner_id", using: :btree
  end

  create_table "inventory_item_types", force: :cascade do |t|
    t.string  "name"
    t.integer "created_by"
    t.boolean "verified"
  end

  create_table "inventory_items", force: :cascade do |t|
    t.integer  "binder_id"
    t.integer  "inventory_item_type_id"
    t.string   "name"
    t.integer  "value_cents",            default: 0,     null: false
    t.string   "value_currency",         default: "USD", null: false
    t.text     "details"
    t.integer  "created_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "inventory_item_type"
    t.boolean  "verified",               default: true
    t.index ["binder_id"], name: "index_inventory_items_on_binder_id", using: :btree
  end

  create_table "isn_users", force: :cascade do |t|
    t.integer  "partner_configuration_id"
    t.string   "company_key",              limit: 50
    t.string   "api_endpoint",             limit: 500
    t.string   "username",                 limit: 50
    t.string   "encrypted_password",       limit: 200
    t.datetime "endpoint_refreshed_at"
    t.datetime "datetime"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "name"
    t.index ["partner_configuration_id"], name: "index_isn_users_on_partner_configuration_id", using: :btree
  end

  create_table "maintenance_cycles", force: :cascade do |t|
    t.string "name"
  end

  create_table "maintenance_events", force: :cascade do |t|
    t.integer  "maintenance_item_id"
    t.integer  "contractor_id"
    t.date     "do_date"
    t.date     "completed_date"
    t.integer  "created_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.date     "last_notify_date"
    t.string   "email_status"
    t.integer  "clicks"
    t.integer  "opens"
    t.index ["maintenance_item_id"], name: "index_maintenance_events_on_maintenance_item_id", using: :btree
  end

  create_table "maintenance_items", force: :cascade do |t|
    t.integer  "binder_id"
    t.integer  "appliance_id"
    t.integer  "area_id"
    t.integer  "structure_id"
    t.integer  "maintenance_type_id"
    t.integer  "maintenance_cycle_id"
    t.string   "name"
    t.text     "details"
    t.integer  "interval"
    t.integer  "created_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "maintenance_type"
    t.string   "maintenance_cycle"
    t.boolean  "email_notifications",  default: true
    t.integer  "library_source_id"
    t.boolean  "verified",             default: true
    t.index ["binder_id"], name: "index_maintenance_items_on_binder_id", using: :btree
  end

  create_table "maintenance_template_contractor_types", force: :cascade do |t|
    t.integer  "maintenance_template_id", null: false
    t.integer  "contractor_category_id",  null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["contractor_category_id"], name: "contractor_type_mt_index", using: :btree
    t.index ["maintenance_template_id"], name: "mt_contractor_type_index", using: :btree
  end

  create_table "maintenance_templates", force: :cascade do |t|
    t.integer  "binder_template_id"
    t.boolean  "library_template",                       default: false
    t.integer  "library_source_id"
    t.string   "name",                      limit: 100
    t.string   "frequency",                 limit: 50
    t.datetime "due_date"
    t.string   "notes",                     limit: 5000
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.integer  "initial_due_date_interval"
    t.string   "video_file_name"
    t.string   "video_content_type"
    t.integer  "video_file_size"
    t.datetime "video_updated_at"
    t.string   "document_file_name"
    t.string   "document_content_type"
    t.integer  "document_file_size"
    t.datetime "document_updated_at"
    t.text     "description",                            default: "",    null: false
    t.integer  "estimated_cost"
    t.string   "estimated_time"
    t.string   "contractor_type"
    t.integer  "contractor_type_id"
    t.string   "video_url"
    t.index ["binder_template_id"], name: "index_maintenance_templates_on_binder_template_id", using: :btree
    t.index ["library_source_id"], name: "index_maintenance_templates_on_library_source_id", using: :btree
    t.index ["library_template"], name: "index_maintenance_templates_on_library_template", using: :btree
  end

  create_table "maintenance_types", force: :cascade do |t|
    t.string  "name"
    t.integer "interval"
    t.integer "maintenance_cycle_id"
    t.integer "created_by"
    t.boolean "verified"
  end

  create_table "marketing_resources", force: :cascade do |t|
    t.string   "name",                  limit: 100
    t.text     "description"
    t.string   "document_file_name"
    t.string   "document_content_type"
    t.integer  "document_file_size"
    t.datetime "document_updated_at"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.string   "video_file_name"
    t.string   "video_content_type"
    t.integer  "video_file_size"
    t.datetime "video_updated_at"
    t.string   "url"
    t.integer  "resource_type"
    t.integer  "user_type"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "document_template_id"
  end

  create_table "notes", force: :cascade do |t|
    t.text     "content"
    t.string   "notable_type"
    t.integer  "notable_id"
    t.integer  "access_level"
    t.integer  "created_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "binder_id"
    t.integer  "appliance_id"
    t.integer  "area_id"
    t.integer  "binder_contractor_id"
    t.integer  "finish_id"
    t.integer  "inventory_item_id"
    t.integer  "maintenance_item_id"
    t.integer  "paint_id"
    t.integer  "project_id"
    t.integer  "property_id"
    t.integer  "receipt_id"
    t.integer  "structure_id"
    t.integer  "maintenance_event_id"
    t.index ["appliance_id"], name: "index_notes_on_appliance_id", using: :btree
    t.index ["area_id"], name: "index_notes_on_area_id", using: :btree
    t.index ["binder_contractor_id"], name: "index_notes_on_binder_contractor_id", using: :btree
    t.index ["binder_id"], name: "index_notes_on_binder_id", using: :btree
    t.index ["finish_id"], name: "index_notes_on_finish_id", using: :btree
    t.index ["inventory_item_id"], name: "index_notes_on_inventory_item_id", using: :btree
    t.index ["maintenance_event_id"], name: "index_notes_on_maintenance_event_id", using: :btree
    t.index ["maintenance_item_id"], name: "index_notes_on_maintenance_item_id", using: :btree
    t.index ["paint_id"], name: "index_notes_on_paint_id", using: :btree
    t.index ["project_id"], name: "index_notes_on_project_id", using: :btree
    t.index ["property_id"], name: "index_notes_on_property_id", using: :btree
    t.index ["receipt_id"], name: "index_notes_on_receipt_id", using: :btree
    t.index ["structure_id"], name: "index_notes_on_structure_id", using: :btree
  end

  create_table "organization_partners", force: :cascade do |t|
    t.integer  "organization_id"
    t.integer  "partner_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["organization_id"], name: "index_organization_partners_on_organization_id", using: :btree
    t.index ["partner_id"], name: "index_organization_partners_on_partner_id", using: :btree
  end

  create_table "organization_resources", force: :cascade do |t|
    t.integer  "marketing_resource_id"
    t.integer  "organization_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["marketing_resource_id"], name: "index_organization_resources_on_marketing_resource_id", using: :btree
    t.index ["organization_id"], name: "index_organization_resources_on_organization_id", using: :btree
  end

  create_table "organization_users", force: :cascade do |t|
    t.integer  "organization_id"
    t.integer  "user_id"
    t.string   "role",            limit: 100
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["organization_id"], name: "index_organization_users_on_organization_id", using: :btree
    t.index ["user_id"], name: "index_organization_users_on_user_id", using: :btree
  end

  create_table "organizations", force: :cascade do |t|
    t.string "name",        limit: 250
    t.string "description", limit: 1000
  end

  create_table "paint_manufacturers", force: :cascade do |t|
    t.string  "name"
    t.integer "created_by"
    t.boolean "verified"
  end

  create_table "paint_sheens", force: :cascade do |t|
    t.string  "name",       limit: 50
    t.integer "created_by"
    t.boolean "verified"
  end

  create_table "paint_types", force: :cascade do |t|
    t.string  "name"
    t.integer "created_by"
    t.boolean "verified"
  end

  create_table "paints", force: :cascade do |t|
    t.integer  "created_by"
    t.integer  "binder_id"
    t.string   "name"
    t.integer  "paint_manufacturer_id"
    t.string   "code"
    t.string   "makeup"
    t.text     "details"
    t.integer  "paint_type_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "paint_type"
    t.string   "manufacturer"
    t.string   "sheen",                 limit: 50
    t.boolean  "verified",                         default: true
    t.index ["binder_id"], name: "index_paints_on_binder_id", using: :btree
  end

  create_table "partner_binder_transactions", force: :cascade do |t|
    t.integer  "binder_transaction_id", null: false
    t.integer  "partner_id",            null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["binder_transaction_id"], name: "index_partner_binder_transactions_on_binder_transaction_id", using: :btree
    t.index ["partner_id"], name: "index_partner_binder_transactions_on_partner_id", using: :btree
  end

  create_table "partner_binders", force: :cascade do |t|
    t.integer  "partner_id"
    t.integer  "binder_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "role",                  limit: 100
    t.integer  "dashboard_id"
    t.integer  "client_id"
    t.boolean  "repair_pricer_enabled",             default: true
    t.integer  "partner_user_id"
    t.boolean  "accepted",                          default: false
    t.boolean  "thanked",                           default: false
    t.index ["binder_id"], name: "index_partner_binders_on_binder_id", using: :btree
    t.index ["client_id"], name: "index_partner_binders_on_client_id", using: :btree
    t.index ["partner_id"], name: "index_partner_binders_on_partner_id", using: :btree
    t.index ["partner_user_id"], name: "index_partner_binders_on_partner_user_id", using: :btree
  end

  create_table "partner_clients", force: :cascade do |t|
    t.integer  "partner_id",                         null: false
    t.integer  "client_id",                          null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.date     "last_maintenance_notification_date"
    t.index ["client_id"], name: "index_partner_clients_on_client_id", using: :btree
    t.index ["partner_id", "client_id"], name: "index_partner_clients_on_partner_id_and_client_id", unique: true, using: :btree
    t.index ["partner_id"], name: "index_partner_clients_on_partner_id", using: :btree
  end

  create_table "partner_configurations", force: :cascade do |t|
    t.integer "partner_id"
    t.string  "maintenance_note",                     limit: 1000
    t.integer "default_binder_template_id"
    t.string  "default_binder_action",                limit: 250,  default: "transfer"
    t.boolean "automation_binder_action",                          default: false
    t.integer "binder_action_delay_length",                        default: 1
    t.integer "send_agents_transfer_notification",                 default: 0
    t.boolean "show_isn_tab",                                      default: false
    t.integer "default_user_branding_id"
    t.string  "default_user_branding_option",                      default: "co_brand"
    t.boolean "can_perform_apr",                                   default: false
    t.integer "annual_property_review_cost_cents",                 default: 19900,      null: false
    t.string  "annual_property_review_cost_currency",              default: "USD",      null: false
    t.boolean "allow_client_calls",                                default: true
    t.boolean "active_warranty_account",                           default: false
    t.string  "default_transaction_type",                          default: "buy_side"
    t.boolean "repair_pricer_enabled",                             default: false
    t.string  "repair_pricer_id"
    t.boolean "repair_pricer_pre_paid_reports",                    default: false
    t.boolean "repair_pricer_messaging_enabled",                   default: true
    t.string  "default_locale",                                    default: "en-us"
    t.string  "broker_mint_api_key"
    t.boolean "client_text_messaging_enabled",                     default: true
    t.string  "default_repair_pricer_report_type",                 default: "F"
    t.boolean "isn_report_url",                                    default: false
    t.index ["default_binder_template_id"], name: "index_partner_configurations_on_default_binder_template_id", using: :btree
    t.index ["partner_id"], name: "index_partner_configurations_on_partner_id", using: :btree
  end

  create_table "partner_contractor_sub_types", force: :cascade do |t|
    t.integer  "partner_contractor_id",      null: false
    t.integer  "contractor_sub_category_id", null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["contractor_sub_category_id"], name: "contractor_sub_type_p_contractor_index", using: :btree
    t.index ["partner_contractor_id"], name: "partner_contractor_sub_type_index", using: :btree
  end

  create_table "partner_contractor_types", force: :cascade do |t|
    t.integer  "partner_contractor_id",  null: false
    t.integer  "contractor_category_id", null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["contractor_category_id"], name: "contractor_type_p_contractor_index", using: :btree
    t.index ["partner_contractor_id"], name: "partner_contractor_type_index", using: :btree
  end

  create_table "partner_contractors", force: :cascade do |t|
    t.integer  "partner_id"
    t.integer  "contractor_id"
    t.string   "name",                  limit: 100
    t.string   "phone",                 limit: 20
    t.string   "email",                 limit: 200
    t.string   "notes",                 limit: 5000
    t.string   "secondary_name",        limit: 100
    t.string   "secondary_email",       limit: 100
    t.string   "secondary_phone",       limit: 100
    t.text     "secondary_notes"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "website",               limit: 250
    t.boolean  "send_email_invitation",              default: true
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.index ["contractor_id"], name: "index_partner_contractors_on_contractor_id", using: :btree
    t.index ["partner_id"], name: "index_partner_contractors_on_partner_id", using: :btree
  end

  create_table "partner_resources", force: :cascade do |t|
    t.integer  "marketing_resource_id"
    t.integer  "partner_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["marketing_resource_id"], name: "index_partner_resources_on_marketing_resource_id", using: :btree
    t.index ["partner_id"], name: "index_partner_resources_on_partner_id", using: :btree
  end

  create_table "partner_users", force: :cascade do |t|
    t.integer  "partner_id"
    t.integer  "user_id"
    t.string   "role",                 limit: 100
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.boolean  "access_repair_pricer",             default: true
    t.index ["partner_id"], name: "index_partner_users_on_partner_id", using: :btree
    t.index ["user_id"], name: "index_partner_users_on_user_id", using: :btree
  end

  create_table "partner_warranty_plans", force: :cascade do |t|
    t.money    "price",            scale: 2, null: false
    t.integer  "partner_id",                 null: false
    t.integer  "warranty_plan_id",           null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["partner_id"], name: "index_partner_warranty_plans_on_partner_id", using: :btree
    t.index ["warranty_plan_id"], name: "index_partner_warranty_plans_on_warranty_plan_id", using: :btree
  end

  create_table "partner_widgets", force: :cascade do |t|
    t.integer "partner_id",    null: false
    t.integer "widget_id",     null: false
    t.text    "configuration"
    t.index ["partner_id"], name: "index_partner_widgets_on_partner_id", using: :btree
    t.index ["widget_id"], name: "index_partner_widgets_on_widget_id", using: :btree
  end

  create_table "partners", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.string   "contact"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "code"
    t.string   "partner_type"
    t.string   "website",              limit: 250
    t.string   "affiliate_code",       limit: 50
    t.string   "email_display_name",   limit: 50
    t.integer  "account_id"
    t.string   "partner_key"
    t.boolean  "completed_onboarding",             default: false
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.integer  "contractor_id"
    t.index ["contractor_id"], name: "index_partners_on_contractor_id", using: :btree
  end

  create_table "permits", force: :cascade do |t|
    t.string   "status",               limit: 255
    t.string   "proposed_use",         limit: 255
    t.string   "work_class",           limit: 255
    t.string   "permit_type",          limit: 255
    t.string   "permit_number",        limit: 255,                           null: false
    t.money    "valuation_amount",                  scale: 2
    t.string   "details",              limit: 1000
    t.datetime "permit_date"
    t.integer  "created_by"
    t.integer  "binder_contractor_id"
    t.integer  "binder_id",                                                  null: false
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
    t.boolean  "verified",                                    default: true
    t.index ["binder_contractor_id"], name: "index_permits_on_binder_contractor_id", using: :btree
    t.index ["binder_id"], name: "index_permits_on_binder_id", using: :btree
  end

  create_table "project_statuses", force: :cascade do |t|
    t.string "name"
  end

  create_table "project_types", force: :cascade do |t|
    t.string  "name"
    t.integer "created_by"
    t.boolean "verified"
  end

  create_table "projects", force: :cascade do |t|
    t.integer  "binder_id"
    t.integer  "project_type_id"
    t.integer  "project_status_id"
    t.string   "name"
    t.text     "details"
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "cost_cents",        default: 0,     null: false
    t.string   "cost_currency",     default: "USD", null: false
    t.integer  "created_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "project_type"
    t.string   "status"
    t.boolean  "verified",          default: true
    t.index ["binder_id"], name: "index_projects_on_binder_id", using: :btree
  end

  create_table "promo_codes", force: :cascade do |t|
    t.string   "name",            limit: 100,             null: false
    t.integer  "percent_off",                 default: 0, null: false
    t.integer  "amount_off",                  default: 0, null: false
    t.datetime "expiration_date"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
  end

  create_table "properties", force: :cascade do |t|
    t.integer  "binder_id"
    t.integer  "property_type_id"
    t.string   "apn"
    t.string   "country"
    t.string   "address1"
    t.string   "address2"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "county"
    t.float    "lat"
    t.float    "long"
    t.text     "details"
    t.decimal  "acres"
    t.integer  "created_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "property_type"
    t.integer  "sq_ft"
    t.integer  "year_built"
    t.string   "geography"
    t.integer  "purchase_price"
    t.date     "purchase_date"
    t.index ["binder_id"], name: "index_properties_on_binder_id", using: :btree
  end

  create_table "property_types", force: :cascade do |t|
    t.string  "name"
    t.integer "created_by"
    t.boolean "verified"
  end

  create_table "purchases", force: :cascade do |t|
    t.integer  "store_id"
    t.date     "date"
    t.integer  "price_cents",       default: 0,     null: false
    t.string   "price_currency",    default: "USD", null: false
    t.integer  "created_by"
    t.string   "purchaseable_type"
    t.integer  "purchaseable_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "receipt_id"
    t.integer  "paint_id"
    t.integer  "finish_id"
    t.integer  "appliance_id"
    t.index ["appliance_id"], name: "index_purchases_on_appliance_id", using: :btree
    t.index ["finish_id"], name: "index_purchases_on_finish_id", using: :btree
    t.index ["paint_id"], name: "index_purchases_on_paint_id", using: :btree
    t.index ["purchaseable_id", "purchaseable_type"], name: "index_purchases_on_purchaseable_id_and_purchaseable_type", using: :btree
    t.index ["receipt_id"], name: "index_purchases_on_receipt_id", using: :btree
  end

  create_table "recall_keywords", force: :cascade do |t|
    t.string   "keyword"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "recalls", force: :cascade do |t|
    t.string   "number"
    t.date     "recall_date"
    t.text     "models"
    t.boolean  "verified"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "ignore"
    t.text     "details"
  end

  create_table "receipts", force: :cascade do |t|
    t.integer  "binder_id"
    t.string   "name"
    t.text     "details"
    t.boolean  "deductable"
    t.integer  "created_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "verified",   default: true
    t.index ["binder_id"], name: "index_receipts_on_binder_id", using: :btree
  end

  create_table "repair_pricer_report_findings", force: :cascade do |t|
    t.string   "name",                    null: false
    t.string   "pg",                      null: false
    t.string   "code",                    null: false
    t.string   "contractor_type",         null: false
    t.string   "action",                  null: false
    t.integer  "defective_price"
    t.integer  "cosmetic_price"
    t.integer  "complete_price"
    t.integer  "potential_price"
    t.integer  "repair_pricer_report_id", null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["repair_pricer_report_id"], name: "index_repair_pricer_report_findings_on_repair_pricer_report_id", using: :btree
  end

  create_table "repair_pricer_report_photos", force: :cascade do |t|
    t.string   "file_file_name",                  null: false
    t.string   "file_content_type",               null: false
    t.integer  "file_file_size",                  null: false
    t.datetime "file_updated_at",                 null: false
    t.integer  "repair_pricer_report_finding_id", null: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.index ["repair_pricer_report_finding_id"], name: "index_rpr_photos_on_rpr_finding_id", using: :btree
  end

  create_table "repair_pricer_reports", force: :cascade do |t|
    t.string   "report_id"
    t.integer  "status",                           default: 0,   null: false
    t.integer  "binder_id"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.string   "client_first"
    t.string   "client_last"
    t.string   "client_email"
    t.string   "client_phone"
    t.string   "buyer_agent_first"
    t.string   "buyer_agent_last"
    t.string   "buyer_agent_email"
    t.integer  "amount_charged_cents"
    t.string   "amount_charged_currency"
    t.integer  "payment_status"
    t.string   "stripe_charge_id"
    t.string   "stripe_refund_id"
    t.date     "closing_date"
    t.integer  "creator_id"
    t.integer  "partner_id"
    t.date     "delivery_date"
    t.boolean  "order_pool_report"
    t.boolean  "order_home_history_report"
    t.boolean  "rush_report"
    t.string   "inspection_report_file_name"
    t.string   "inspection_report_content_type"
    t.integer  "inspection_report_file_size"
    t.datetime "inspection_report_updated_at"
    t.string   "pool_report_file_name"
    t.string   "pool_report_content_type"
    t.integer  "pool_report_file_size"
    t.datetime "pool_report_updated_at"
    t.string   "home_history_report_file_name"
    t.string   "home_history_report_content_type"
    t.integer  "home_history_report_file_size"
    t.datetime "home_history_report_updated_at"
    t.string   "repair_report_file_name"
    t.string   "repair_report_content_type"
    t.integer  "repair_report_file_size"
    t.datetime "repair_report_updated_at"
    t.string   "report_type",                      default: "F"
    t.index ["binder_id"], name: "index_repair_pricer_reports_on_binder_id", using: :btree
  end

  create_table "seller_report_items", force: :cascade do |t|
    t.string  "seller_reportable_type"
    t.integer "seller_reportable_id"
    t.boolean "include"
    t.integer "sort_index"
    t.integer "appliance_id"
    t.integer "area_id"
    t.integer "binder_contractor_id"
    t.integer "finish_id"
    t.integer "inventory_item_id"
    t.integer "maintenance_item_id"
    t.integer "paint_id"
    t.integer "receipt_id"
    t.integer "structure_id"
    t.integer "image_id"
    t.integer "document_id"
    t.integer "permit_id"
    t.integer "project_id"
    t.index ["appliance_id"], name: "index_seller_report_items_on_appliance_id", using: :btree
    t.index ["area_id"], name: "index_seller_report_items_on_area_id", using: :btree
    t.index ["binder_contractor_id"], name: "index_seller_report_items_on_binder_contractor_id", using: :btree
    t.index ["document_id"], name: "index_seller_report_items_on_document_id", using: :btree
    t.index ["finish_id"], name: "index_seller_report_items_on_finish_id", using: :btree
    t.index ["image_id"], name: "index_seller_report_items_on_image_id", using: :btree
    t.index ["inventory_item_id"], name: "index_seller_report_items_on_inventory_item_id", using: :btree
    t.index ["maintenance_item_id"], name: "index_seller_report_items_on_maintenance_item_id", using: :btree
    t.index ["paint_id"], name: "index_seller_report_items_on_paint_id", using: :btree
    t.index ["permit_id"], name: "index_seller_report_items_on_permit_id", using: :btree
    t.index ["project_id"], name: "index_seller_report_items_on_project_id", using: :btree
    t.index ["receipt_id"], name: "index_seller_report_items_on_receipt_id", using: :btree
    t.index ["seller_reportable_id", "seller_reportable_type"], name: "index_seller_report_items_on_sr_id_and_sr_type", using: :btree
    t.index ["structure_id"], name: "index_seller_report_items_on_structure_id", using: :btree
  end

  create_table "seller_reports", force: :cascade do |t|
    t.integer  "binder_id"
    t.string   "code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "public"
    t.boolean  "show_project_costs", default: false
    t.index ["binder_id"], name: "index_seller_reports_on_binder_id", using: :btree
  end

  create_table "sessions", force: :cascade do |t|
    t.string   "token"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "expires_at"
    t.integer  "user_id"
    t.string   "device_type"
    t.string   "device_name"
    t.string   "device_version"
    t.string   "operating_system"
    t.integer  "status",           default: 0
    t.index ["user_id"], name: "index_sessions_on_user_id", using: :btree
  end

  create_table "shares", force: :cascade do |t|
    t.integer  "shared_by_id"
    t.integer  "shared_with_id"
    t.string   "shared_with_email"
    t.string   "sharable_type"
    t.integer  "sharable_id"
    t.string   "role_name"
    t.string   "status",                   default: "created"
    t.boolean  "expirable"
    t.date     "expires_on"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "binder_id"
    t.date     "last_notified_at"
    t.date     "completed_at"
    t.string   "access_token"
    t.date     "text_message_status_date"
    t.string   "text_message_status"
    t.string   "text_message_sid"
    t.string   "acceptance_method"
    t.index ["access_token"], name: "index_shares_on_access_token", unique: true, using: :btree
    t.index ["binder_id"], name: "index_shares_on_binder_id", using: :btree
    t.index ["shared_by_id"], name: "index_shares_on_shared_by_id", using: :btree
    t.index ["shared_with_email"], name: "index_shares_on_shared_with_email", using: :btree
    t.index ["shared_with_id"], name: "index_shares_on_shared_with_id", using: :btree
  end

  create_table "stores", force: :cascade do |t|
    t.string   "name"
    t.integer  "created_by"
    t.boolean  "verified"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "structures", force: :cascade do |t|
    t.integer  "binder_id"
    t.string   "name"
    t.integer  "year_built"
    t.integer  "beds"
    t.decimal  "baths"
    t.integer  "sq_ft"
    t.integer  "building_type_id"
    t.integer  "construction_style_id"
    t.integer  "construction_type_id"
    t.integer  "heat_type_id"
    t.integer  "heat_source_id"
    t.text     "details"
    t.integer  "created_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "building_type"
    t.string   "construction_type"
    t.string   "construction_style"
    t.string   "heat_type"
    t.string   "heat_source"
    t.boolean  "verified",              default: true
    t.index ["binder_id"], name: "index_structures_on_binder_id", using: :btree
  end

  create_table "subscriptions", force: :cascade do |t|
    t.integer  "binder_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "plan_id"
    t.string   "customer_id"
    t.string   "payment_status"
    t.string   "code"
    t.index ["binder_id"], name: "index_subscriptions_on_binder_id", using: :btree
  end

  create_table "tags", force: :cascade do |t|
    t.string   "taggable_type"
    t.integer  "taggable_id"
    t.string   "tag"
    t.boolean  "auto_generated", default: true
    t.integer  "created_by"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "scope"
    t.text     "metadata"
    t.index ["taggable_id", "taggable_type"], name: "index_tags_on_taggable_id_and_taggable_type", using: :btree
  end

  create_table "template_building_types", force: :cascade do |t|
    t.integer  "appliance_template_id"
    t.integer  "maintenance_template_id"
    t.integer  "building_type_id",        null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["appliance_template_id"], name: "index_template_building_types_on_appliance_template_id", using: :btree
    t.index ["building_type_id"], name: "index_template_building_types_on_building_type_id", using: :btree
    t.index ["maintenance_template_id"], name: "index_template_building_types_on_maintenance_template_id", using: :btree
  end

  create_table "template_geographies", force: :cascade do |t|
    t.integer  "appliance_template_id"
    t.integer  "maintenance_template_id"
    t.integer  "geography_id",            null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.index ["appliance_template_id"], name: "index_template_geographies_on_appliance_template_id", using: :btree
    t.index ["geography_id"], name: "index_template_geographies_on_geography_id", using: :btree
    t.index ["maintenance_template_id"], name: "index_template_geographies_on_maintenance_template_id", using: :btree
  end

  create_table "transfers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "binder_id"
    t.string   "transfer_to"
    t.string   "transfer_type"
    t.string   "status",                   default: "created"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "last_notified"
    t.integer  "sender_id"
    t.integer  "receiver_id"
    t.date     "transferred_at"
    t.date     "delivery_failed_at"
    t.date     "completed_at"
    t.string   "access_token"
    t.date     "text_message_status_date"
    t.string   "text_message_status"
    t.string   "text_message_sid"
    t.string   "acceptance_method"
    t.index ["access_token"], name: "index_transfers_on_access_token", unique: true, using: :btree
    t.index ["binder_id"], name: "index_transfers_on_binder_id", using: :btree
    t.index ["receiver_id"], name: "index_transfers_on_receiver_id", using: :btree
    t.index ["sender_id"], name: "index_transfers_on_sender_id", using: :btree
  end

  create_table "user_binder_transactions", force: :cascade do |t|
    t.integer  "binder_transaction_id", null: false
    t.integer  "user_id",               null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["binder_transaction_id"], name: "index_user_binder_transactions_on_binder_transaction_id", using: :btree
    t.index ["user_id"], name: "index_user_binder_transactions_on_user_id", using: :btree
  end

  create_table "user_binders", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "binder_id"
    t.string   "role",                 limit: 100
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.integer  "dashboard_id"
    t.boolean  "access_repair_pricer",             default: true
    t.string   "repair_pricer_token"
    t.index ["binder_id"], name: "index_user_binders_on_binder_id", using: :btree
    t.index ["repair_pricer_token"], name: "index_user_binders_on_repair_pricer_token", unique: true, using: :btree
    t.index ["user_id"], name: "index_user_binders_on_user_id", using: :btree
  end

  create_table "user_contractor_sub_types", force: :cascade do |t|
    t.integer  "user_contractor_id",         null: false
    t.integer  "contractor_sub_category_id", null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["contractor_sub_category_id"], name: "contractor_sub_type_u_contractor_index", using: :btree
    t.index ["user_contractor_id"], name: "user_contractor_sub_type_index", using: :btree
  end

  create_table "user_contractor_types", force: :cascade do |t|
    t.integer  "user_contractor_id",     null: false
    t.integer  "contractor_category_id", null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["contractor_category_id"], name: "contractor_type_u_contractor_index", using: :btree
    t.index ["user_contractor_id"], name: "user_contractor_type_index", using: :btree
  end

  create_table "user_contractors", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "contractor_id"
    t.string   "name",                  limit: 100
    t.string   "phone",                 limit: 20
    t.string   "email",                 limit: 200
    t.string   "notes",                 limit: 5000
    t.string   "secondary_name",        limit: 100
    t.string   "secondary_email",       limit: 100
    t.string   "secondary_phone",       limit: 100
    t.text     "secondary_notes"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "website",               limit: 250
    t.boolean  "send_email_invitation",              default: true
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.index ["contractor_id"], name: "index_user_contractors_on_contractor_id", using: :btree
    t.index ["user_id"], name: "index_user_contractors_on_user_id", using: :btree
  end

  create_table "user_merges", force: :cascade do |t|
    t.integer  "user_id",                            null: false
    t.string   "old_agent_first_name"
    t.string   "old_agent_last_name"
    t.string   "old_agent_email"
    t.string   "footprint_file_name",                null: false
    t.string   "footprint_content_type",             null: false
    t.integer  "footprint_file_size",                null: false
    t.datetime "footprint_updated_at",               null: false
    t.integer  "status",                 default: 0, null: false
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.index ["user_id"], name: "index_user_merges_on_user_id", using: :btree
  end

  create_table "user_profiles", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "home_phone"
    t.string   "mobile_phone"
    t.date     "dob"
    t.string   "sex"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "monthly_email",                                  default: true
    t.string   "company",                           limit: 255
    t.string   "website",                           limit: 255
    t.text     "bio"
    t.string   "message",                           limit: 1000
    t.string   "head_shot_file_name"
    t.string   "head_shot_content_type"
    t.integer  "head_shot_file_size"
    t.datetime "head_shot_updated_at"
    t.string   "logo_file_name"
    t.string   "logo_content_type"
    t.integer  "logo_file_size"
    t.datetime "logo_updated_at"
    t.string   "default_locale",                                 default: "en-us"
    t.boolean  "text_messaging_enabled",                         default: true
    t.string   "transfer_message_to_homeowner",     limit: 1000
    t.string   "transfer_message_to_agent",         limit: 1000
    t.string   "transfer_message_to_prepaid_agent", limit: 1000
    t.index ["user_id"], name: "index_user_profiles_on_user_id", using: :btree
  end

  create_table "user_resources", force: :cascade do |t|
    t.integer  "marketing_resource_id"
    t.integer  "user_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["marketing_resource_id"], name: "index_user_resources_on_marketing_resource_id", using: :btree
    t.index ["user_id"], name: "index_user_resources_on_user_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                              default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.integer  "sign_in_count",                      default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "create_method",          limit: 30
    t.date     "accepted_transfer_at"
    t.string   "role",                   limit: 100
    t.integer  "created_by"
    t.string   "salesforce_id"
    t.string   "intercom_id"
    t.boolean  "completed_onboarding",               default: false
    t.datetime "last_intercom_error"
    t.string   "password_digest"
    t.string   "access_token"
    t.index ["created_by"], name: "index_users_on_created_by", using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree
  end

  create_table "warranties", force: :cascade do |t|
    t.string   "client_first",     limit: 50
    t.string   "client_last",      limit: 50
    t.string   "client_email",     limit: 100
    t.string   "client_phone",     limit: 20
    t.string   "status",           limit: 20
    t.datetime "expiration_date"
    t.integer  "binder_id",                    null: false
    t.integer  "warranty_plan_id",             null: false
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "order_id"
    t.index ["binder_id"], name: "index_warranties_on_binder_id", using: :btree
    t.index ["warranty_plan_id"], name: "index_warranties_on_warranty_plan_id", using: :btree
  end

  create_table "warranty_companies", force: :cascade do |t|
    t.string   "name",            limit: 100
    t.string   "contact",         limit: 100
    t.string   "email",           limit: 100
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "secondary_email"
  end

  create_table "warranty_configurations", force: :cascade do |t|
    t.boolean  "automatic_purchases",             default: false
    t.string   "account_number",      limit: 100,                 null: false
    t.integer  "warranty_plan_id",                                null: false
    t.integer  "warranty_company_id",                             null: false
    t.integer  "partner_id",                                      null: false
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.index ["partner_id"], name: "index_warranty_configurations_on_partner_id", using: :btree
    t.index ["warranty_company_id"], name: "index_warranty_configurations_on_warranty_company_id", using: :btree
    t.index ["warranty_plan_id"], name: "index_warranty_configurations_on_warranty_plan_id", using: :btree
  end

  create_table "warranty_plans", force: :cascade do |t|
    t.string   "name",                limit: 100
    t.integer  "duration"
    t.string   "cycle",               limit: 10
    t.string   "description",         limit: 1000
    t.money    "price",                            scale: 2, null: false
    t.integer  "warranty_company_id",                        null: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.index ["warranty_company_id"], name: "index_warranty_plans_on_warranty_company_id", using: :btree
  end

  create_table "warranty_transactions", force: :cascade do |t|
    t.integer  "binder_transaction_id", null: false
    t.integer  "warranty_id",           null: false
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["binder_transaction_id"], name: "index_warranty_transactions_on_binder_transaction_id", using: :btree
    t.index ["warranty_id"], name: "index_warranty_transactions_on_warranty_id", using: :btree
  end

  create_table "widget_exclusions", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "partner_id"
    t.integer  "organization_id"
    t.integer  "widget_id"
    t.string   "widget_category", limit: 100
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["organization_id"], name: "index_widget_exclusions_on_organization_id", using: :btree
    t.index ["partner_id"], name: "index_widget_exclusions_on_partner_id", using: :btree
    t.index ["user_id"], name: "index_widget_exclusions_on_user_id", using: :btree
    t.index ["widget_id"], name: "index_widget_exclusions_on_widget_id", using: :btree
  end

  create_table "widgets", force: :cascade do |t|
    t.string   "name",                        limit: 100
    t.string   "key",                         limit: 100
    t.string   "description",                 limit: 1000
    t.string   "category",                    limit: 100
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "configuration"
    t.integer  "organization_restriction_id"
    t.integer  "partner_restriction_id"
    t.boolean  "admin_restriction",                        default: false
  end

  add_foreign_key "partner_clients", "users", column: "client_id"
end
