class AddIsnUsernamePassword < ActiveRecord::Migration
  def change
    add_column :isn_configurations, :username, :string, :limit => 50
    add_column :isn_configurations, :password, :string, :limit => 50
  end
end
