class ChangeBelongToOfIsnConfiguration < ActiveRecord::Migration
  def change
    remove_column :isn_configurations, :partner_id
    add_column :isn_configurations, :partner_configuration_id, :integer, index: true
  end
end
