class AddCompletenessScoreToBinders < ActiveRecord::Migration[5.0]
  def change
    add_column :binders, :completeness_score,  :decimal, :default => 0.0
  end
end
