class FixCouponColumnTypes < ActiveRecord::Migration
  def change
    remove_column :coupons, :percent_off
    remove_column :coupons, :amount_off
    remove_column :coupons, :duration_months
    add_column :coupons, :percent_off, :integer
    add_column :coupons, :amount_off, :integer
    add_column :coupons, :duration_months, :integer
  end
end
