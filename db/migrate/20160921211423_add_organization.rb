class AddOrganization < ActiveRecord::Migration
  def change
    # create the organizations table
    create_table :organizations do |t|
      t.string    :name,      :limit => 250,    :nil => false
    end
    
    # add organization id to to users and partners
    add_column :users,    :organization_id, :integer, :nil => false
    add_column :partners, :organization_id, :integer, :nil => false
    
    # add a join table to join binders to organization
    create_table :organization_binders do |t|
      t.references :organization
      t.references :binder
      t.timestamps
    end
    
    # add join table to join binders to a partner
    create_table :partner_binders do |t|
      t.references :partner
      t.references :binder
      t.timestamps
    end
    
    # drop polymorphic associations from dashboards
    remove_column :dashboards, :dashboardable_type
    remove_column :dashboards, :dashboardable_id
    
    # add ownership references to dashboards
    add_column :dashboards, :organization_id, :integer, :nil => false
    add_column :dashboards, :partner_id,      :integer, :nil => false
    
  end
end
