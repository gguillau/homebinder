class DropFreeTrialsTable < ActiveRecord::Migration[5.0]
  def up
    drop_table :free_trials
  end
  
  def down
    create_table :free_trials
  end
end
