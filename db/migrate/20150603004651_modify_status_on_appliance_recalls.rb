class ModifyStatusOnApplianceRecalls < ActiveRecord::Migration
  def change
    add_column :appliance_recalls, :status, :string, :limit => 25
    add_column :appliance_recalls, :status_date, :datetime
    add_column :appliance_recalls, :notifications, :integer
    remove_column :appliance_recalls, :repaired
    remove_column :appliance_recalls, :repair_date
    remove_column :appliance_recalls, :replaced
    remove_column :appliance_recalls, :replace_date
    remove_column :appliance_recalls, :ignore
    remove_column :appliance_recalls, :notified
  end
end
