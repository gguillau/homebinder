class AddFieldsForPartnerAnalytics < ActiveRecord::Migration[5.0]
  def change
    add_reference   :partner_binders, :partner_user,  index: true
    
    add_column      :partner_binders, :accepted,                            :boolean, :default => false
    add_column      :partner_clients, :last_maintenance_notification_date,  :date
  end
end
