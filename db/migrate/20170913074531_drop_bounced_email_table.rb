class DropBouncedEmailTable < ActiveRecord::Migration[5.0]
    def up
        drop_table :bounced_emails
    end
    
    def down
        create_table :bounced_emails
    end
end
