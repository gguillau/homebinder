class CreateAccount < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.integer     :manager_id, :null => true
      t.string      :account_status, :limit => 50, :null => false
      t.string      :account_sub_status, :limit => 50, :null => true
      t.string      :payment_type, :limit => 50, :null => true
      t.string      :account_type, :limit => 50, :null => false
      t.string      :account_sub_type, :limit => 50, :null => false
      t.datetime    :trial_expiration, :null => true
      t.datetime    :billing_activation, :null => true
      t.monetize       :subscription_amount, :null => true
      t.monetize       :transaction_amount, :null => true
      t.string      :name, :limit => 500
      t.integer     :account_number
      t.text        :notes
      t.timestamps
    end
    
    add_column :partners, :account_id, :integer, :null => true, :index => true
  end
end
