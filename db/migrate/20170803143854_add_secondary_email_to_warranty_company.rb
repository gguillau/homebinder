class AddSecondaryEmailToWarrantyCompany < ActiveRecord::Migration[5.0]
  def change
    add_column :warranty_companies, :secondary_email, :string
  end
end
