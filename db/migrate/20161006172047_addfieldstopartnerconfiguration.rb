class Addfieldstopartnerconfiguration < ActiveRecord::Migration
  def change
    add_column :partner_configurations, :default_binder_action, :string, :limit => 250, :default => "transfer"
    add_column :partner_configurations, :automation_binder_action, :boolean, :default => false
    add_column :partner_configurations, :binder_action_delay_length, :integer, :default => 5
  end
end
