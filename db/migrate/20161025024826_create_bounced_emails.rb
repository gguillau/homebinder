class CreateBouncedEmails < ActiveRecord::Migration
  def change
    create_table :bounced_emails do |t|
      t.integer     :binder_id,             :null => false    
      t.string      :email_type,            :null => false
      t.string      :sender,                :null => false
      t.string      :receiver,              :null => false
      t.string      :bounce_description,    :null => false
      t.timestamps
    end
  end
end
