class AddShowProjectCostsToSellerReports < ActiveRecord::Migration[5.0]
  def change
    add_column :seller_reports, :show_project_costs, :boolean, :default => false
  end
end
