class AddSqFtAndYearBuiltToProperties < ActiveRecord::Migration[5.0]
  def change
    add_column :properties, :sq_ft, :integer
    add_column :properties, :year_built, :integer
  end
end
