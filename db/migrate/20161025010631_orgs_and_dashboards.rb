class OrgsAndDashboards < ActiveRecord::Migration
  def change
    # column isn't needed on the admin side
    remove_column :widgets, :size
    
    # table mapping org defaults
    create_table :organization_assignments do |t|
      t.references :organization
      t.string :object_type
    end
  end
end
