class UpdateBinderTemplate < ActiveRecord::Migration[5.0]
  def change
    # update binder templates
    add_column :binder_templates, :system,    :boolean, :default => false
    add_column :binder_templates, :default,   :boolean, :default => false
    add_column :binder_templates, :partner,   :boolean, :default => false
    
    # binder properties
    add_column :properties, :geography,  :string
    
    # geographies
    create_table :geographies do |t|
      t.string      :name,    :null => false
      t.timestamps
    end
    
    # join table for geographies
    create_table :template_geographies do |t|
      t.references  :appliance_template,    :null => true
      t.references  :maintenance_template,  :null => true
      t.references  :geography,             :null => false
      t.timestamps
    end
    
    # join table for building_types
    create_table :template_building_types do |t|
      t.references  :appliance_template,    :null => true
      t.references  :maintenance_template,  :null => true
      t.references  :building_type,         :null => false
      t.timestamps
    end
    
    # contractor_sub_types
    create_table :contractor_sub_types do |t|
      t.string      :name,            :null => false
      t.timestamps
    end
    
    # joins table
    create_table :contractor_category_types do |t|
      t.references  :contractor_type, :null => false
      t.references  :contractor_sub_types, :null => false
      t.timestamps
    end
    
    # add fields to appliance/maintenance templates
    add_attachment  :appliance_templates, :video
    add_attachment  :appliance_templates, :document
    
    add_attachment  :maintenance_templates, :video
    add_attachment  :maintenance_templates, :document
    
    add_column  :appliance_templates,   :description, :text, :null => false, :default => ""
    add_column  :maintenance_templates, :description, :text, :null => false, :default => ""
  end
end
