class AddRecallJsonToRecalls < ActiveRecord::Migration
  def change
    add_column :recalls, :details, :text, :limit => nil
    remove_column :recalls, :upc
    remove_column :recalls, :recall_url
    remove_column :recalls, :y2k
    remove_column :recalls, :manufacturer
    remove_column :recalls, :category
    remove_column :recalls, :product_name
    remove_column :recalls, :hazard
    remove_column :recalls, :man_country
  end
end
