class AddIsnReportUrlToPartnerConfigurations < ActiveRecord::Migration[5.0]
  def change
    add_column  :partner_configurations, :isn_report_url,  :boolean, :default => false
  end
end
