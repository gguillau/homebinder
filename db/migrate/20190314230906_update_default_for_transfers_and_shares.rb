class UpdateDefaultForTransfersAndShares < ActiveRecord::Migration[5.0]
  def up
    change_column :transfers, :status, :string, :default => "created"
    change_column :shares,    :status, :string, :default => "created"
    
    Binder::Transfer.joins(:binder => [:partners => :account]).where("accounts.account_status = 'closed' OR accounts.account_status = 'suspended' OR accounts.account_status = 'expired' OR accounts.account_status = 'canceled'").where(:status => "not_sent").update_all("status = 'created'")
    Binder::Transfer.joins(:binder).where(:status => "not_sent", :binders => {:status => "orphan"}).update_all(:status => "created")
    Binder::Transfer.joins("INNER JOIN users on users.id = transfers.receiver_id INNER JOIN user_binders on user_binders.user_id = users.id").where("user_binders.binder_id = transfers.binder_id AND user_binders.role = 'owner' AND users.role = 'homeowner'").where("transfers.status = 'not_sent'").where(:created_at => 4.weeks.ago..Time.now).find_each do |transfer|
      TransferService.new(transfer.id).send_email(false)
    end
    Binder::Transfer.where(:status => "not_sent").update_all("status = 'created'")
  end
  
  
  def down
    change_column :transfers, :status, :string, :default => nil
    change_column :shares,    :status, :string, :default => nil
    
    Binder::Transfer.joins(:binder => [:partners => :account]).where("accounts.account_status = 'closed' OR accounts.account_status = 'suspended' OR accounts.account_status = 'expired' OR accounts.account_status = 'canceled'").where(:status => "created").update_all("status = 'not_sent'")
    Binder::Transfer.joins(:binder).where(:status => "created", :binders => {:status => "orphan"}).update_all(:status => "not_sent")
    Binder::Transfer.where(:status => "created").update_all("status = 'not_sent'")
  end
end
