class MakeAccountNumberString < ActiveRecord::Migration
  def change
    remove_column :accounts, :account_number
    add_column :accounts, :account_number, :string, :limit => 12, :nil => false
  end
end
