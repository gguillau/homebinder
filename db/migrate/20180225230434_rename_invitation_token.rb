class RenameInvitationToken < ActiveRecord::Migration[5.0]
  def change
    rename_column :users, :invitation_token,    :access_token
  end
end
