class AddEmailFieldsUserBranding < ActiveRecord::Migration[5.0]
  def change
    add_column  :user_profiles, :transfer_message_to_homeowner,     :string, :null => true, :limit => 1000
    add_column  :user_profiles, :transfer_message_to_agent,         :string, :null => true, :limit => 1000
    add_column  :user_profiles, :transfer_message_to_prepaid_agent, :string, :null => true, :limit => 1000
  end
end
