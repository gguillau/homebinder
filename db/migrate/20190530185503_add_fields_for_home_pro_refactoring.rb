class AddFieldsForHomeProRefactoring < ActiveRecord::Migration[5.0]
    def up
        create_table :partner_contractors do |t|
            t.belongs_to    :partner,         index: true
            t.belongs_to    :contractor,      index: true
            t.string        :name,            limit: 100,   null: false
            t.string        :phone,           limit: 20,    null: true
            t.string        :email,           limit: 200,   null: true
            t.string        :notes,           limit: 5000,  null:true
            t.string        :secondary_name,  limit: 100,   null: true
            t.string        :secondary_email, limit: 100,   null: true
            t.string        :secondary_phone, limit: 100,   null: true
            t.text          :secondary_notes, null: true
            t.string        :name,            limit: 100,   null: true
            t.attachment    :logo
            t.string        :website, :limit => 250
            t.boolean       :send_email_invitation, :default => true
            t.timestamps null: false
        end

        # add types join table to contractors
        create_table :partner_contractor_types do |t|
            t.references  :partner_contractor,   :null => false, :index => {:name => "partner_contractor_type_index"}
            t.references  :contractor_category,   :null => false, :references => :contractor_categories, :index => {:name => "contractor_type_p_contractor_index"}
            t.timestamps
        end

        # add sub_types join table to contractors
        create_table :partner_contractor_sub_types do |t|
            t.references  :partner_contractor,         :null => false, :index => {:name => "partner_contractor_sub_type_index"}
            t.references  :contractor_sub_category, :null => false, :references => :contractor_sub_categories, :index => {:name => "contractor_sub_type_p_contractor_index"}
            t.timestamps
        end

        add_column :binder_templates, :use_default_pros, :boolean, :default => true

        add_reference   :contractor_templates, :partner_contractor,  index: true
        add_reference   :binders, :binder_template,  index: true
        add_reference   :addresses,             :partner_contractor, index: true

        change_column :contractor_templates, :name, :string, :null => true
    end

    def down
        change_column :contractor_templates, :name, :string, :null => false

        remove_reference :addresses, :partner_contractor, index: true
        remove_reference :binders, :binder_template, index: true
        remove_reference :contractor_templates, :partner_contractor, index: true

        remove_column :binder_templates, :use_default_pros

        drop_table :partner_contractor_sub_types
        drop_table :partner_contractor_types
        drop_table :partner_contractors
    end
end
