class AnnualPropertyReviews < ActiveRecord::Migration[5.0]
  def change
    create_table :annual_property_reviews do |t|
      t.references  :binder
      t.string      :uuid,                limit: 32,    null: false
      t.string      :client_first_name,   limit: 100,   null: false
      t.string      :client_last_name,    limit: 100,   null: false
      t.string      :client_email,        limit: 100,   null: false
      t.string      :client_phone,        limit: 30,    null: false
      t.string      :address1,            limit: 100,   null: false
      t.string      :address2,            limit: 100,   null: true
      t.string      :city,                limit: 100,   null: false
      t.string      :state,               limit: 100,   null: false
      t.string      :zip,                 limit: 100,   null: false
      t.string      :country,             limit: 100,   null: false
      t.integer     :status,                            null: false,  default: 0
      t.integer     :reviewer_id,                       null: true
      t.integer     :reviewer_company_id,               null: true
      t.datetime    :scheduled_for,                     null: true
      t.datetime    :scheduled_on,                      null: true
      t.datetime    :completed_on,                      null: true
      t.datetime    :on_hold_on,                        null: true
      t.datetime    :canceled_on,                       null: true
      t.text        :client_comments,                   null: true
      t.integer     :score,                             null: true,   default: 0
      t.text        :cancel_reason,                     null: true
      t.monetize    :amount_charged,                    null: true
      t.integer     :payment_status,                    null: true,   default: 0
      t.datetime    :reviewed_on,                       null: true
      t.text        :property_reviewer_comments,        null: true
      t.text        :admin_reviewer_comments,           null: true
      t.string      :stripe_charge_id,    limit: 50,    null: true
      t.string      :stripe_refund_id,    limit: 50,    null: true
      t.timestamps
    end
    
    add_attachment :annual_property_reviews, :snapshot
    
    add_index :annual_property_reviews, :client_email
    add_index :annual_property_reviews, :reviewer_id
    add_index :annual_property_reviews, :reviewer_company_id
    add_index :annual_property_reviews, :uuid
    
    create_table :annual_property_review_findings do |t|
      t.references  :annual_property_review, index: {:name => "apr_finding_index"}
      t.string      :name,                limit: 255,     null: false
      t.integer     :priority,                            null: false,  default: 0
      t.integer     :estimated_cost,                      null: false,  default: 0
      t.text        :details,                             null: true
      t.integer     :recommended_homepro_id,              null: true
      t.integer     :selected_homepro_id,                 null: true
      t.integer     :status,                              null: false,  default: 0
      t.timestamps
    end
    
    create_table :annual_property_review_photos do |t|
      t.references  :annual_property_review, index: {:name => "apr_photo_index"}
      t.references  :annual_property_review_finding, index: {:name => "apr_photo_finding_index"}
      t.references  :annual_property_review_capital_item, index: {:name => "apr_photo_capital_item_index"}
      t.string      :name,                limit: 255,     null: false
      t.text        :description,                         null: true
    end
    
    add_attachment :annual_property_review_photos, :file
    
    create_table :annual_property_review_capital_items do |t|
      t.references  :annual_property_review, index: {:name => "apr_capital_index"}
      t.string      :name,                limit: 255,     null: false
      t.integer     :eul,                                 null: false,  default: 0
      t.integer     :cost_range,                          null: false,  default: 0
      t.text        :comments,                            null: true
    end
    
    add_column   :partner_configurations, :can_perform_apr, :boolean, :default => false
    add_monetize :partner_configurations, :annual_property_review_cost
  end
end
