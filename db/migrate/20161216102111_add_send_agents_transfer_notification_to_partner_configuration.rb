class AddSendAgentsTransferNotificationToPartnerConfiguration < ActiveRecord::Migration[5.0]
  def change
    add_column :partner_configurations, :send_agents_transfer_notification, :boolean, :default => false
  end
end
