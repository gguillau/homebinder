class DropUnusedTables < ActiveRecord::Migration[5.0]
    def up
        drop_table :vendors
        drop_table :logos
        drop_table :uploaders, if_exists: true
        drop_table :pdfs
    end
    
    def down
        create_table :vendors
        create_table :logos
        create_table :uploaders
        create_table :pdfs
    end
end
