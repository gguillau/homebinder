class AddFieldsToBinderItems < ActiveRecord::Migration[5.0]
  def change
    # appliances
    add_column :appliances,         :verified,  :boolean, :default => true
    # maintenance_items
    add_column :maintenance_items,  :verified,  :boolean, :default => true
    # projects
    add_column :projects,           :verified,  :boolean, :default => true
    # images
    add_column :images,             :verified,  :boolean, :default => true
    # documents
    add_column :documents,          :verified,  :boolean, :default => true
    # paints
    add_column :paints,             :verified,  :boolean, :default => true
    # permits
    add_column :permits,            :verified,  :boolean, :default => true
    # structures
    add_column :structures,         :verified,  :boolean, :default => true
    # areas
    add_column :areas,              :verified,  :boolean, :default => true
    # binder_contractors
    add_column :binder_contractors, :verified,  :boolean, :default => true
    # finishes
    add_column :finishes,           :verified,  :boolean, :default => true
    # inventory_items
    add_column :inventory_items,    :verified,  :boolean, :default => true
    # receipts
    add_column :receipts,           :verified,  :boolean, :default => true
  end
end
