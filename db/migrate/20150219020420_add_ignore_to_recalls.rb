class AddIgnoreToRecalls < ActiveRecord::Migration
  def change
    add_column :recalls, :ignore, :boolean
  end
end
