class AddLastErrorToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :last_intercom_error, :datetime
  end
end
