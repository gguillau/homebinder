class AddDescriptionToOrganization < ActiveRecord::Migration
  def change
    add_column :organizations, :description, :string, :limit => 1000
  end
end
