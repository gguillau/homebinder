class RemoveTransferNoteFromPartnerConfigurations < ActiveRecord::Migration
  def up
    remove_column :partner_configurations, :transfer_note
  end

  def down
    add_column :partner_configurations, :transfer_note, :string
  end
end
