class CreateTemplatesAndLibraries < ActiveRecord::Migration
  def change
    create_table :binder_templates do |t|
      t.belongs_to  :partner_configuration, index: true
      t.string      :name, limit: 100, null: false
      t.timestamps null: false
    end
    
    create_table :appliance_templates do |t|
      t.belongs_to    :binder_template, index: true
      t.boolean       :library_template, index: true, :default => false
      t.references    :library_source, index: true
      t.string        :name,  limit: 100, null: false
      t.string        :notes, limit: 5000, null: true
      t.attachment    :image
      t.timestamps null: false
    end
    
    create_table :contractor_templates do |t|
      t.belongs_to    :binder_template, index: true
      t.boolean       :library_template, index: true, :default => false
      t.references    :library_source, index: true
      t.string        :name,  limit: 100, null: false
      t.string        :contractor_type, limit: 100, null: true
      t.string        :phone, limit: 20, null: true
      t.string        :email, limit: 200, null: true
      t.string        :notes, limit: 5000, null:true
      t.attachment    :image
      t.timestamps null: false
    end
    
    create_table :maintenance_templates do |t|
      t.belongs_to    :binder_template, index: true
      t.boolean       :library_template, index: true, :default => false
      t.references    :library_source, index: true
      t.string        :name, limit: 100
      t.string        :frequency, limit: 50
      t.datetime      :due_date, null: true
      t.string        :notes, limit: 5000
      t.attachment    :image
      t.timestamps null: false
    end
    
    create_table :document_templates do |t|
      t.belongs_to    :binder_template, index: true
      t.references    :library_source, index: true
      t.attachment    :file
      t.string        :notes, limit: 5000
      t.timestamps null: false
    end
    
    add_column :appliances, :library_source_id, :integer, index: true
    add_column :binder_contractors, :library_source_id, :integer, index: true
    add_column :maintenance_items, :library_source_id, :integer, index: true
    add_column :documents, :library_source_id, :integer, index: true
  end
end
