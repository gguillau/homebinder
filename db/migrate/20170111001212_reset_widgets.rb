class ResetWidgets < ActiveRecord::Migration[5.0]
  def change
    # dashboard table
    remove_column :dashboards, :template
    
    add_column :dashboards, :user_id, :integer
    #add_column :dashboards, :organization_id, :integer
    #add_column :dashboards, :partner_id, :integer
    #add_column :dashboards, :default, :boolean, default: false
    add_column :dashboards, :active,  :boolean, default: false
    add_column :dashboards, :system,  :boolean, default: false
    add_column :dashboards, :scope,   :string
    add_index  :dashboards, :user_id
    add_index  :dashboards, :partner_id
    add_index  :dashboards, :organization_id
    
    # user binders table
    add_column :user_binders, :dashboard_id, :integer
    
    # partner bindes table
    add_column :partner_binders, :dashboard_id, :integer
    
  end
end
