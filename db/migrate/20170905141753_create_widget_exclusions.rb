class CreateWidgetExclusions < ActiveRecord::Migration[5.0]
  def change
    create_table :widget_exclusions do |t|
      t.references    :user,              :null => true
      t.references    :partner,           :null => true
      t.references    :organization,      :null => true
      t.references    :widget,            :null => true
      t.string        :widget_category,   :limit => 100
      t.timestamps
    end
  end
end
