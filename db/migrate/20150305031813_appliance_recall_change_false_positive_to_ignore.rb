class ApplianceRecallChangeFalsePositiveToIgnore < ActiveRecord::Migration
  def change
    remove_column :appliance_recalls, :false_positive
    add_column :appliance_recalls, :ignore, :boolean, :default => true
  end
end
