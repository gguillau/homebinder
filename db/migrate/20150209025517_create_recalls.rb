class CreateRecalls < ActiveRecord::Migration
  def change
    create_table :recalls do |t|
      t.references    :appliance
      t.integer       :recall_id
      t.datetime      :recall_date
      t.string        :product_name
      t.string        :upc_code
      t.string        :document_path
      t.timestamps
    end
  end
end
