class AddPartnerKeyToPartners < ActiveRecord::Migration
  def change
    add_column :partners, :partner_key, :string
  end
end
