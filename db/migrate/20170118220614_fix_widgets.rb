class FixWidgets < ActiveRecord::Migration[5.0]
  def change
    add_column :widgets, :configuration, :text
    add_column :widgets, :organization_restriction_id, :integer
    add_column :widgets, :partner_restriction_id, :integer
    add_column :widgets, :admin_restriction, :boolean, default: false
    
    remove_column :widgets, :created_by
    remove_column :dashboard_widgets, :configuration
  end
end
