class AddLastNotifyToMaintenance < ActiveRecord::Migration
  def change
    add_column :maintenance_events, :last_notify_date, :date
  end
end
