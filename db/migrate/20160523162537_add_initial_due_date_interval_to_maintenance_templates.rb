class AddInitialDueDateIntervalToMaintenanceTemplates < ActiveRecord::Migration
  def change
    add_column :maintenance_templates, :initial_due_date_interval, :integer
  end
end
