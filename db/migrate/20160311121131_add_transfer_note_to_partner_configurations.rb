class AddTransferNoteToPartnerConfigurations < ActiveRecord::Migration
  def change
    add_column :partner_configurations, :transfer_note, :string, :limit => 1000
  end
end
