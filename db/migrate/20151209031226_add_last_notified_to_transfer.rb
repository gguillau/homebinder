class AddLastNotifiedToTransfer < ActiveRecord::Migration
  def change
    add_column :transfers, :last_notified, :datetime
  end
end
