class RemoveDeviseColumns < ActiveRecord::Migration[5.0]
    def up
        add_column      :users, :password_digest,    :string
        remove_column   :users, :confirmation_token
        remove_column   :users, :confirmed_at
        remove_column   :users, :confirmation_sent_at
        remove_column   :users, :unconfirmed_email
        remove_column   :users, :account_type
        remove_column   :users, :admin
        
        User.update_all("password_digest = encrypted_password")
        
        remove_column   :users, :encrypted_password
    end
    
    def down
        add_column  :users, :confirmation_token,    :string
        add_column  :users, :confirmed_at,          :datetime
        add_column  :users, :confirmation_sent_at,  :datetime
        add_column  :users, :unconfirmed_email,     :string
        add_column  :users, :account_type,          :string
        add_column  :users, :admin,                 :boolean
        add_column  :users, :encrypted_password,    :string
        remove_column  :users, :password_digest, :string
    end
end
