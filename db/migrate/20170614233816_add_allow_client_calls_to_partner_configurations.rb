class AddAllowClientCallsToPartnerConfigurations < ActiveRecord::Migration[5.0]
  def change
    add_column :partner_configurations, :allow_client_calls, :boolean, :default => true
  end
end
