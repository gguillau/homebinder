class ChangeRecallsModelsToText < ActiveRecord::Migration[5.0]
  def change
    change_column :recalls, :models, :text
  end
end
