class AddThankedToPartnerBinder < ActiveRecord::Migration[5.0]
  def change
    add_column :partner_binders, :thanked, :boolean, :default => false
  end
end
