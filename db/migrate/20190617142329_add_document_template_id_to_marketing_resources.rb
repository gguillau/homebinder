class AddDocumentTemplateIdToMarketingResources < ActiveRecord::Migration[5.0]
  def change
    add_column :marketing_resources, :document_template_id, :integer
  end
end
