class CreateWarrantyPlans < ActiveRecord::Migration[5.0]
  def change
    create_table :warranty_plans do |t|
      t.string        :name,                :limit => 100
      t.integer       :duration
      t.string        :cycle,               :limit => 10
      t.string        :description,         :limit => 1000
      t.money         :price,               :null => false
      t.references    :warranty_company,    :null => false
      t.timestamps
    end
  end
end
