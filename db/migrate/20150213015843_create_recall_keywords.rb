class CreateRecallKeywords < ActiveRecord::Migration
  def change
    create_table :recall_keywords do |t|
      t.string  :keyword
      t.timestamps
    end
  end
end
