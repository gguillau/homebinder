class AddExpiresAtToSessions < ActiveRecord::Migration[5.0]
    def up
        add_column      :sessions, :expires_at, :datetime
        add_column      :sessions, :user_id,    :integer
        remove_column   :sessions, :binder_id
        remove_column   :sessions, :user_token
    end
    
    def down
        remove_column   :sessions, :expires_at
        remove_column   :sessions, :user_id
        add_column      :sessions, :binder_id,  :integer
        add_column      :sessions, :user_token, :string
    end
end
