class AddPartnerIdToApiKeys < ActiveRecord::Migration
  def change
    add_column :api_keys, :partner_id, :integer
  end
end
