class AddConfigurationToDashboardWidgets < ActiveRecord::Migration
  def change
    add_column :dashboard_widgets, :configuration, :text
  end
end
