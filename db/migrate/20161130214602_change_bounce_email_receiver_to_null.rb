class ChangeBounceEmailReceiverToNull < ActiveRecord::Migration[5.0]
  def change
    change_column :bounced_emails, :receiver, :string, :null => true
  end
end
