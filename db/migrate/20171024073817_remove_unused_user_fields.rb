class RemoveUnusedUserFields < ActiveRecord::Migration[5.0]
    def up
        remove_column   :users, :remember_created_at
        remove_column   :users, :current_sign_in_ip
        remove_column   :users, :last_sign_in_ip
        remove_column   :users, :authentication_token
    end
    
    def down
        add_column   :users, :remember_created_at,      :boolean
        add_column   :users, :current_sign_in_ip,       :string
        add_column   :users, :last_sign_in_ip,          :string
        add_column   :users, :authentication_token,     :string
    end
end
