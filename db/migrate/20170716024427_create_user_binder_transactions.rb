class CreateUserBinderTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :user_binder_transactions do |t|
      t.references    :binder_transaction,      :null => false
      t.references    :user,                    :null => false
      t.timestamps
    end
  end
end
