class MakeIsnCredentialsLonger < ActiveRecord::Migration
  def change
    change_column :isn_configurations, :encrypted_username, :string, :limit => 300
    change_column :isn_configurations, :encrypted_password, :string, :limit => 300
  end
end
