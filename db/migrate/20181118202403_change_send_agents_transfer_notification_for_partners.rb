class ChangeSendAgentsTransferNotificationForPartners < ActiveRecord::Migration[5.0]
  def up
    true_ids = Partner::Configuration.where(:send_agents_transfer_notification => true).pluck(:id)
    false_ids = Partner::Configuration.where(:send_agents_transfer_notification => false).pluck(:id)
    
    change_column_default :partner_configurations, :send_agents_transfer_notification, nil
    change_column :partner_configurations, :send_agents_transfer_notification, :integer, :default => 0, using: "case when send_agents_transfer_notification then 0 else 1 end"
    
    Partner::Configuration.where(:id => true_ids).update_all(:send_agents_transfer_notification => "always")
    Partner::Configuration.where(:id => false_ids).update_all(:send_agents_transfer_notification => "never")
  end
  
  def down
    true_ids = Partner::Configuration.where("send_agents_transfer_notification = 0 OR send_agents_transfer_notification = 1").pluck(:id)
    false_ids = Partner::Configuration.where(:send_agents_transfer_notification => 2).pluck(:id)
    
    add_column :partner_configurations, :send_agents_transfer_notification2, :boolean
    
    Partner::Configuration.where(:id => true_ids).update_all(:send_agents_transfer_notification2 => true)
    Partner::Configuration.where(:id => false_ids).update_all(:send_agents_transfer_notification2 => false)

    remove_column :partner_configurations, :send_agents_transfer_notification
    rename_column :partner_configurations, :send_agents_transfer_notification2, :send_agents_transfer_notification
  end
end
