class AddApiKeyToAutomationErrors < ActiveRecord::Migration
  def change
    add_column :automation_errors, :api_key, :string
  end
end
