class AddContractorIdToPartners < ActiveRecord::Migration[5.0]
  def change
    add_reference   :partners,  :contractor,  index: true
  end
end
