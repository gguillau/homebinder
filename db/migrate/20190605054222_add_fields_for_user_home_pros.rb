class AddFieldsForUserHomePros < ActiveRecord::Migration[5.0]
    def up
        create_table :user_contractors do |t|
            t.belongs_to    :user,         index: true
            t.belongs_to    :contractor,      index: true
            t.string        :name,            limit: 100,   null: false
            t.string        :phone,           limit: 20,    null: true
            t.string        :email,           limit: 200,   null: true
            t.string        :notes,           limit: 5000,  null:true
            t.string        :secondary_name,  limit: 100,   null: true
            t.string        :secondary_email, limit: 100,   null: true
            t.string        :secondary_phone, limit: 100,   null: true
            t.text          :secondary_notes, null: true
            t.string        :name,            limit: 100,   null: true
            t.attachment    :logo
            t.string        :website, :limit => 250
            t.boolean       :send_email_invitation, :default => true
            t.timestamps null: false
        end

        # add types join table to contractors
        create_table :user_contractor_types do |t|
            t.references  :user_contractor,   :null => false, :index => {:name => "user_contractor_type_index"}
            t.references  :contractor_category,   :null => false, :references => :contractor_categories, :index => {:name => "contractor_type_u_contractor_index"}
            t.timestamps
        end

        # add sub_types join table to contractors
        create_table :user_contractor_sub_types do |t|
            t.references  :user_contractor,         :null => false, :index => {:name => "user_contractor_sub_type_index"}
            t.references  :contractor_sub_category, :null => false, :references => :contractor_sub_categories, :index => {:name => "contractor_sub_type_u_contractor_index"}
            t.timestamps
        end

        add_reference   :addresses,             :user_contractor,   index: true
        add_reference   :binder_contractors,    :user_contractor,   index: true
        
        # remove fields for contractor_templates
        remove_column :contractor_templates, :name
        remove_column :contractor_templates, :contractor_type
        remove_column :contractor_templates, :phone
        remove_column :contractor_templates, :email
        remove_column :contractor_templates, :notes
        remove_column :contractor_templates, :contractor_id
        remove_column :contractor_templates, :secondary_name
        remove_column :contractor_templates, :secondary_email
        remove_column :contractor_templates, :secondary_phone
        remove_column :contractor_templates, :secondary_notes
        remove_column :contractor_templates, :send_email_invitation
        remove_column :contractor_templates, :website

        # remove types for contractor_templates
        drop_table :template_contractor_types
        drop_table :template_contractor_sub_types

        # remove attachment
        remove_attachment :contractor_templates, :image
    end

    def down
        add_attachment :contractor_templates, :image

        # add types join table to template contractors
        create_table :template_contractor_types do |t|
            t.references  :template_contractor,   :null => false, :index => {:name => "template_contractor_type_index"}
            t.references  :contractor_category,   :null => false, :references => :contractor_categories, :index => {:name => "contractor_sub_type_t_contractor_index"}
            t.timestamps
        end

        # add sub_types join table to template contractors
        create_table :template_contractor_sub_types do |t|
            t.references  :template_contractor,       :null => false, :index => {:name => "t_contractor_sub_type_index"}
            t.references  :contractor_sub_category, :null => false, :references => :contractor_sub_categories, :index => {:name => "contractor_sub_type_t_contractor_sub_type_index"}
            t.timestamps
        end

        add_column :contractor_templates, :website, :string
        add_column :contractor_templates, :send_email_invitation, :boolean
        add_column :contractor_templates, :secondary_notes, :text
        add_column :contractor_templates, :secondary_phone, :string
        add_column :contractor_templates, :secondary_email, :string
        add_column :contractor_templates, :secondary_name, :string

        add_reference   :contractor_templates, :contractor,  index: true

        add_column :contractor_templates, :notes, :text
        add_column :contractor_templates, :email, :string
        add_column :contractor_templates, :phone, :string
        add_column :contractor_templates, :contractor_type, :string
        add_column :contractor_templates, :name, :string

        remove_reference :addresses, :user_contractor, index: true
        remove_reference :binder_contractors, :user_contractor, index: true
        
        drop_table :user_contractor_sub_types
        drop_table :user_contractor_types
        drop_table :user_contractors
    end
end
