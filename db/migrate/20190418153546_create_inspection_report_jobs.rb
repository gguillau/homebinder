class CreateInspectionReportJobs < ActiveRecord::Migration[5.0]
  def change
    create_table :inspection_report_jobs do |t|
      t.string        :job_id,            :null => true
      t.integer       :status,            :null => false, :default => 0
      t.attachment    :inspection_report
      t.references    :binder,            :null => true
      t.references    :partner,           :null => true
      t.timestamps
    end
  end
end
