class AddDefaultBinderTemplateIdToPartnerConfigurations < ActiveRecord::Migration
  def change
    add_column :partner_configurations, :default_binder_template_id, :integer
  end
end
