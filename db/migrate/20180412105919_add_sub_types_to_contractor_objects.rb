class AddSubTypesToContractorObjects < ActiveRecord::Migration[5.0]
  def up
    rename_table :contractor_types,     :contractor_categories
    rename_table :contractor_sub_types, :contractor_sub_categories
    
    # add types join table to contractors
    create_table :contractor_types do |t|
      t.references  :contractor,            :null => false
      t.references  :contractor_category,   :null => false, :references => :contractor_categories
      t.timestamps
    end
    
    # add sub_types join table to contractors
    create_table :contractor_sub_types do |t|
      t.references  :contractor,              :null => false
      t.references  :contractor_sub_category, :null => false, :references => :contractor_sub_categories
      t.timestamps
    end
    
    # add types join table to binder contractors
    create_table :binder_contractor_types do |t|
      t.references  :binder_contractor,     :null => false
      t.references  :contractor_category,   :null => false, :references => :contractor_categories
      t.timestamps
    end
    
    # add sub_types join table to binder contractors
    create_table :binder_contractor_sub_types do |t|
      t.references  :binder_contractor,         :null => false, :index => {:name => "bc_contractor_sub_type_index"}
      t.references  :contractor_sub_category, :null => false, :references => :contractor_sub_categories, :index => {:name => "contractor_sub_type_bc_contractor_index"}
      t.timestamps
    end

    # add types join table to template contractors
    create_table :template_contractor_types do |t|
      t.references  :template_contractor,   :null => false, :index => {:name => "template_contractor_type_index"}
      t.references  :contractor_category,   :null => false, :references => :contractor_categories, :index => {:name => "contractor_sub_type_t_contractor_index"}
      t.timestamps
    end
    
    # add sub_types join table to template contractors
    create_table :template_contractor_sub_types do |t|
      t.references  :template_contractor,       :null => false, :index => {:name => "t_contractor_sub_type_index"}
      t.references  :contractor_sub_category, :null => false, :references => :contractor_sub_categories, :index => {:name => "contractor_sub_type_t_contractor_sub_type_index"}
      t.timestamps
    end
  end
  
  def down
    drop_table :contractor_types
    drop_table :contractor_sub_types
    
    drop_table :binder_contractor_types
    drop_table :binder_contractor_sub_types
    
    drop_table :template_contractor_types
    drop_table :template_contractor_sub_types
    
    rename_table :contractor_categories,     :contractor_types
    rename_table :contractor_sub_categories, :contractor_sub_types
  end
end
