class AddAcceptedTransferAtToUsers < ActiveRecord::Migration
  def change
    add_column :users, :accepted_transfer_at, :date
  end
end
