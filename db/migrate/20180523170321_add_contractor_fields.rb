class AddContractorFields < ActiveRecord::Migration[5.0]
  def change
    add_reference   :contractor_templates,  :contractor,          index: true
    add_reference   :contractors,           :user#,                index: true
    add_reference   :addresses,             :contractor_template, index: true
    
    add_column :binder_contractors, :worked_on_property,  :boolean, :default => false
    add_column :binder_contractors, :secondary_name,      :string
    add_column :binder_contractors, :secondary_email,     :string
    add_column :binder_contractors, :secondary_phone,     :string
    add_column :binder_contractors, :secondary_details,   :text
    
    add_column :contractor_templates, :secondary_name,      :string
    add_column :contractor_templates, :secondary_email,     :string
    add_column :contractor_templates, :secondary_phone,     :string
    add_column :contractor_templates, :secondary_notes,     :text
  end
end
