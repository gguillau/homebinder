class AddEmailNotificationFieldsToContractors < ActiveRecord::Migration[5.0]
  def change
    add_column :binder_contractors,   :send_email_invitation,  :boolean, :default => true
    add_column :contractor_templates, :send_email_invitation,  :boolean, :default => true
  end
end
