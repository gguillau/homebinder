class RecreateRecalls < ActiveRecord::Migration
  def up
    create_table :recalls do |t|
      t.string      :number
      t.string      :upc
      t.string      :recall_url
      t.date        :recall_date
      t.string      :y2k
      t.string      :manufacturer
      t.string      :category
      t.string      :product_name
      t.string      :hazard
      t.string      :man_country
      t.string      :models
      t.boolean     :verified
      t.timestamps
    end
  end
end
