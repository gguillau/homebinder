class AddCreateMethodToUser < ActiveRecord::Migration
  def change
    add_column :users, :create_method, :string, :limit => 30
  end
end
