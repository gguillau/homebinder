class AddFieldsForTranslation < ActiveRecord::Migration[5.0]
  def change
    add_column :partner_configurations,   :default_locale,  :string, :default => "en-us"
    add_column :user_profiles,            :default_locale,  :string, :default => "en-us"
  end
end