class AddMissingIndexes < ActiveRecord::Migration[5.0]
    def change
        add_index :accounts, :manager_id
        add_index :addresses, [:addressable_id, :addressable_type]
        add_index :api_keys, :partner_id
        add_index :appliance_recalls, :appliance_id
        add_index :appliance_recalls, :recall_id
        add_index :appliances, :binder_id
        add_index :areas, :binder_id
        add_index :binder_contractors, :binder_id
        add_index :binder_contractors, :contractor_id
        add_index :binders, :created_by
        add_index :binders_users, :binder_id
        add_index :binders_users, :user_id
        add_index :coupons, :partner_id
        add_index :dashboard_widgets, :dashboard_id
        add_index :dashboard_widgets, :widget_id
        add_index :finishes, :binder_id
        add_index :images, :binder_id
        add_index :inventory_items, :binder_id
        add_index :isn_users, :partner_configuration_id
        add_index :maintenance_events, :maintenance_item_id
        add_index :maintenance_items, :binder_id
        add_index :paints, :binder_id
        add_index :partner_binders, :binder_id
        add_index :partner_binders, :partner_id
        add_index :partner_configurations, :partner_id
        add_index :projects, :binder_id
        add_index :properties, :binder_id
        add_index :purchases, [:purchaseable_id, :purchaseable_type]
        add_index :receipts, :binder_id
        add_index :seller_report_items, [:seller_reportable_id, :seller_reportable_type], :name => 'index_seller_report_items_on_sr_id_and_sr_type'
        add_index :seller_reports, :binder_id
        add_index :shares, :binder_id
        add_index :structures, :binder_id
        add_index :subscriptions, :binder_id
        add_index :tags, [:taggable_id, :taggable_type]
        add_index :transfers, :binder_id
        add_index :transfers, :receiver_id
        add_index :transfers, :sender_id
        add_index :user_profiles, :user_id
    end
end
