class AddNotifiedToBouncedEmail < ActiveRecord::Migration[5.0]
  def change
    add_column :bounced_emails, :notified, :boolean, :default => false
  end
end
