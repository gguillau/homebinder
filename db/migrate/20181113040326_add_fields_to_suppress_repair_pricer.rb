class AddFieldsToSuppressRepairPricer < ActiveRecord::Migration[5.0]
  def change
    add_column :user_binders,       :access_repair_pricer,    :boolean, :default => true
    add_column :partner_users,      :access_repair_pricer,    :boolean, :default => true
    add_column :partner_binders,    :repair_pricer_enabled,   :boolean, :default => true
  end
end
