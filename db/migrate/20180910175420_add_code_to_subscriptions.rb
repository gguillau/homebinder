class AddCodeToSubscriptions < ActiveRecord::Migration[5.0]
  def change
    add_column :subscriptions,   :code,  :string
    
    change_column_default :addresses, :country, "US"
  end
end
