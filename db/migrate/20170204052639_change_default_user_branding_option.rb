class ChangeDefaultUserBrandingOption < ActiveRecord::Migration[5.0]
  def change
    change_column_default :partner_configurations, :default_user_branding_option, "user"

    rename_column :user_profiles, :company_logo_file_name,     :logo_file_name
    rename_column :user_profiles, :company_logo_file_size,     :logo_file_size
    rename_column :user_profiles, :company_logo_content_type,  :logo_content_type
    rename_column :user_profiles, :company_logo_updated_at,    :logo_updated_at
    
    add_attachment  :partners, :logo
  end
end
