class AddMissingIndexesAgain < ActiveRecord::Migration[5.0]
  def change
    add_index :binder_contractor_sub_types, [:binder_contractor_id, :contractor_sub_category_id], :name => 'index_bcontractors_stype_on_bc_id_and_csc_id'
    add_index :binder_contractor_types, [:binder_contractor_id, :contractor_category_id], :name => 'index_bcontractors_type_on_bc_id_and_cc_id'
    add_index :binders, :hero_image_id
    add_index :contractors, :created_by
    add_index :contractors, :email
    add_index :document_templates, :document_type_id
    add_index :documents, :binder_id
    add_index :documents, :document_type_id
    add_index :partner_configurations, :default_binder_template_id
    add_index :sessions, :user_id
    add_index :users, :created_by
  end
end
