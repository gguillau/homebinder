class CreateUserMerges < ActiveRecord::Migration[5.0]
  def change
    create_table :user_merges do |t|
      t.references    :user,                  :null => false
      t.string        :old_agent_first_name,  :null => true
      t.string        :old_agent_last_name,   :null => true
      t.string        :old_agent_email,       :null => true
      t.attachment    :footprint,             :null => false
      t.integer       :status,                :null => false, :default => 0
      t.timestamps
    end
  end
end
