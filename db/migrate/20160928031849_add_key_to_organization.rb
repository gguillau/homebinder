class AddKeyToOrganization < ActiveRecord::Migration
  def change
    add_column :organizations, :key, :string, :nil => false, :limit => 100, :index => true
  end 
end
