class ChangeUserProfileBioToText < ActiveRecord::Migration[5.0]
  def change
    change_column :user_profiles, :bio, :text
  end
end
