class CreateWarrantyConfigurations < ActiveRecord::Migration[5.0]
  def change
    create_table :warranty_configurations do |t|
      t.boolean       :automatic_purchases,     :default => false
      t.string        :account_number,          :null => false, :limit => 100
      t.references    :warranty_plan,           :null => false
      t.references    :warranty_company,        :null => false
      t.references    :partner,                 :null => false
      t.timestamps
    end
    
    add_column :partner_configurations, :active_warranty_account, :boolean, :default => false
  end
end
