class CreateRepairPricerReport < ActiveRecord::Migration[5.0]
  def change
    create_table :repair_pricer_reports do |t|
      t.string        :report_id,   :null => false
      t.integer       :status,      :null => false, :default => 10
      t.attachment    :report
      t.references    :binder,      :null => true
      t.timestamps
    end
    
    create_table :repair_pricer_report_findings do |t|
      t.string        :name,  :null => false
      t.string        :pg,  :null => false
      t.string        :code,  :null => false
      t.string        :contractor_type,  :null => false
      t.string        :action,  :null => false
      t.integer       :defective_price,  :null => true
      t.integer       :cosmetic_price,  :null => true
      t.integer       :complete_price,  :null => true
      t.integer       :potential_price,  :null => true
      t.references    :repair_pricer_report,  :null => false
      t.timestamps
    end
    
    create_table :repair_pricer_report_photos do |t|
      t.attachment    :file,  :null => false
      t.references    :repair_pricer_report_finding,  :null => false, :index => {:name => "index_rpr_photos_on_rpr_finding_id"}
      t.timestamps
    end
    
    # address
    add_reference   :addresses, :repair_pricer_report,  index: true
  end
end
