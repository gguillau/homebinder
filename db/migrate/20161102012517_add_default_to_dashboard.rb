class AddDefaultToDashboard < ActiveRecord::Migration
  def change
    add_column :dashboards, :default, :boolean, :default => false
  end
end
