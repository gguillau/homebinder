class AddOrderIdToWarranties < ActiveRecord::Migration[5.0]
  def change
    add_column :warranties, :order_id,  :string
  end
end
