class CreateVendor < ActiveRecord::Migration
  def change
    create_table :vendors do |t|
      t.references  :partner
      t.string      :name
      t.string      :vendor_type
      t.string      :phone
      t.string      :email
      t.string      :website
      t.string      :contact
      t.string      :details
    end
  end
end
