class AddTextMessagingFields < ActiveRecord::Migration[5.0]
  def change
    add_column :user_profiles,          :text_messaging_enabled,         :boolean, :default => true
    add_column :partner_configurations, :client_text_messaging_enabled,  :boolean, :default => true
    
    add_column :transfers,  :text_message_status_date,  :date
    add_column :transfers,  :text_message_status,       :string
    add_column :transfers,  :text_message_sid,          :string
    add_column :transfers,  :acceptance_method,         :string
    
    add_column :shares,  :text_message_status_date,  :date
    add_column :shares,  :text_message_status,       :string
    add_column :shares,  :text_message_sid,          :string
    add_column :shares,  :acceptance_method,         :string
  end
end
