class ChangeMonthlyEmailDefaultToTrue < ActiveRecord::Migration[5.0]
  def change
    change_column :user_profiles, :monthly_email, :boolean, :default => true  
  end
end
