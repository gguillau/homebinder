class AddRepairPricerTokenToUserBinders < ActiveRecord::Migration[5.0]
  def change
    add_column  :user_binders, :repair_pricer_token, :string
    add_index   :user_binders, :repair_pricer_token, unique: true
  end
end
