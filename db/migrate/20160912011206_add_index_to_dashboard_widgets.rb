class AddIndexToDashboardWidgets < ActiveRecord::Migration
  def change
    add_column :dashboard_widgets, :index, :integer, :nil => false
  end
end
