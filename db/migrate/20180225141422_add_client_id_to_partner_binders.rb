class AddClientIdToPartnerBinders < ActiveRecord::Migration[5.0]
  def change
    add_reference   :partner_binders, :client,  index: true
  end
end
