class CreateMarketingResources < ActiveRecord::Migration[5.0]
  def change
    create_table :marketing_resources do |t|
      t.string        :name,         limit: 100
      t.text          :description
      t.attachment    :document
      t.attachment    :image
      t.attachment    :video
      t.string        :url
      t.integer       :resource_type
      t.integer       :user_type
      t.timestamps
    end

    create_table :partner_resources do |t|
      t.references :marketing_resource,   nil: false
      t.references :partner,              nil: false
      t.timestamps
    end

    create_table :organization_resources do |t|
      t.references :marketing_resource,   nil: false
      t.references :organization,         nil: false
      t.timestamps
    end

    create_table :user_resources do |t|
      t.references :marketing_resource,   nil: false
      t.references :user,                 nil: false
      t.timestamps
    end
  end
end
