class CreateIsnConfigurations < ActiveRecord::Migration
  def change
    create_table :isn_configurations do |t|
      t.references :partner
      t.string :company_key, :limit => 50
      t.string :api_endpoint, :limit => 200
    end
  end
end
