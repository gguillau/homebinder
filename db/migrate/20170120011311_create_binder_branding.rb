class CreateBinderBranding < ActiveRecord::Migration[5.0]
  def change
    create_table :binder_brandings do |t|
      t.string      :scope,           :null => false, :limit => 255
      t.references  :binder,          :null => false
      t.references  :user_branding,   :null => false
      t.references  :partner,         :null => true
      t.timestamps
    end
  end
end
