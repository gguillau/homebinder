class CreateBinderTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :binder_transactions do |t|
      t.string        :tracking_number,       :limit => 100
      t.money         :transaction_cost,      :null => true
      t.string        :transaction_type,      :limit => 20
      t.datetime      :transaction_date,      :null => true
      t.string        :description,           :limit => 1000
      t.references    :binder,                :null => false
      t.timestamps
    end
    
    create_table :warranty_transactions do |t|
      t.references    :binder_transaction, :null => false
      t.references    :warranty,   :null => false
      t.timestamps
    end
  end
end
