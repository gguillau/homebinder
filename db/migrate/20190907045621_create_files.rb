class CreateFiles < ActiveRecord::Migration[5.0]
    def change
        create_table :abs_files do |t|
            t.references    :binder,          :null => false
            t.attachment    :payload,         :null => false
            t.integer       :status,          :null => false, :default => 0
            t.timestamps
        end
    end
end
