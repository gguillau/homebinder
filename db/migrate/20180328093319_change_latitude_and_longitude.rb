class ChangeLatitudeAndLongitude < ActiveRecord::Migration[5.0]
  def change
    change_column :properties, :lat,  "float USING CAST(lat AS float)"
    change_column :properties, :long, "float USING CAST(long AS float)"
    
    change_column :addresses, :lat,   "float USING CAST(lat AS float)"
    change_column :addresses, :long,  "float USING CAST(long AS float)"
  end
end
