class CreateBinderItems < ActiveRecord::Migration[5.0]
  def change
    create_table :binder_items do |t|
      t.references  :binder,          :null => true
      t.references  :user,            :null => true
      t.references  :item,            :polymorphic => :true, :null => true
      t.boolean     :verified,        :default => false
      t.boolean     :dismissed,       :default => false
      t.datetime    :last_notify_date
      t.timestamps
    end
  end
end
