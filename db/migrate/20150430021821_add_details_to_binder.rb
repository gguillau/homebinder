class AddDetailsToBinder < ActiveRecord::Migration
  def change
    add_column :binders, :details, :text, :limit => nil
  end
end
