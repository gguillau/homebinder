class OrgReset < ActiveRecord::Migration[5.0]
  def change
    # clean up references
    remove_column :users, :organization_id
    remove_column :users, :partner_id
    remove_column :partners, :organization_id
    remove_column :organizations, :key
    
    # drop the organization_assignments table
    drop_table :organization_assignments
    drop_table :organization_binders
    
    # create the organization_users table
    create_table :organization_users do |t|
      t.references :organization, nil: false
      t.references :user,         nil: false
      t.string     :role,         limit: 100
      t.timestamps
    end
      
    # create the organization_partners table
    create_table :organization_partners do |t|
      t.references :organization, nil: false
      t.references :partner,      nil: false
      t.timestamps
    end
    
    # create the partner_users table
    create_table :partner_users do |t|
      t.references :partner,  nil: false
      t.references :user,     nil: false
      t.string     :role,     limit: 100
      t.timestamps
    end
    
    # partner binders already exists. add role to describe relationship
    add_column :partner_binders, :role, :string, limit: 100
    
    # create user_binders table
    create_table :user_binders do |t|
      t.references :user
      t.references :binder
      t.string     :role,   limit: 100
      t.timestamps
    end
    
    # add role to user
    add_column :users, :role, :string, limit: 100
  end
end
