class AddRepairPricerPaidCustomerToPartnerConfigurations < ActiveRecord::Migration[5.0]
  def change
    # allow partner clients to purchase free reports
    add_column :partner_configurations, :repair_pricer_pre_paid_reports, :boolean, :default => false
    # allow homebinder to send messages to clients regarding RP
    add_column :partner_configurations, :repair_pricer_messaging_enabled, :boolean, :default => false
  end
end
