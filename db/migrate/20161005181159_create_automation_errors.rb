class CreateAutomationErrors < ActiveRecord::Migration
  def change
    create_table :automation_errors do |t|
      t.string      :partner_name, :null => true
      t.string      :binder_template_id, :null => true
      t.string      :client_first, :null => true
      t.string      :client_last, :null => true
      t.string      :client_email, :null => true
      t.string      :property_address, :null => true
      t.string      :property_address2, :null => true
      t.string      :property_city, :null => true
      t.string      :property_state, :null => true
      t.string      :property_postalcode, :null => true
      t.string      :property_country, :null => true
      t.string      :agent_name, :null => true
      t.string      :agent_contact, :null => true
      t.string      :agent_phone, :null => true
      t.string      :agent_email, :null => true
      t.timestamps
    end
  end
end
