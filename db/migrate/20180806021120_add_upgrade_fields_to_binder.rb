class AddUpgradeFieldsToBinder < ActiveRecord::Migration[5.0]
  def change
    # paperclip stores file_file_size as an integer so we'll use the same
    # type for binder storage attribute
    add_column :binders, :total_storage,  :integer, :default => 0
    add_column :binders, :custom_support_email,  :string
    add_column :properties, :purchase_price,  :integer
    add_column :properties, :purchase_date,  :date
    add_column :documents, :accessible,  :boolean, :default => true
  end
end