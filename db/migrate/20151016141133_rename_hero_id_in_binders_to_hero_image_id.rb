class RenameHeroIdInBindersToHeroImageId < ActiveRecord::Migration
  def change
    rename_column :binders, :hero_id, :hero_image_id
  end
end
