class ChangeDefaultAprCost < ActiveRecord::Migration[5.0]
  def change
    change_column_default :partner_configurations, :annual_property_review_cost_cents, 19900
  end
end
