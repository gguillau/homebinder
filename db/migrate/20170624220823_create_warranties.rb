class CreateWarranties < ActiveRecord::Migration[5.0]
  def change
    create_table :warranties do |t|
      t.string        :client_first,          :limit => 50
      t.string        :client_last,           :limit => 50
      t.string        :client_email,          :limit => 100
      t.string        :client_phone,          :limit => 20
      t.string        :status,                :limit => 20
      t.datetime      :expiration_date,       :null => true
      t.references    :binder,                :null => false
      t.references    :warranty_plan,         :null => false
      t.timestamps
    end
  end
end
