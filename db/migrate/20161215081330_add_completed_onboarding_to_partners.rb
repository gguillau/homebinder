class AddCompletedOnboardingToPartners < ActiveRecord::Migration[5.0]
  def change
    add_column :partners, :completed_onboarding, :boolean, :default => false
  end
end
