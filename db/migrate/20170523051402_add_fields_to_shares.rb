class AddFieldsToShares < ActiveRecord::Migration[5.0]
  def change
    add_column :shares, :binder_id, :integer
    add_column :shares, :last_notified_at, :date
    add_column :shares, :completed_at, :date
    add_column :shares, :access_token, :string
    add_index :shares, :access_token, unique: true
  end
end
