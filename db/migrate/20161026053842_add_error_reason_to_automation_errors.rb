class AddErrorReasonToAutomationErrors < ActiveRecord::Migration
  def change
    add_column :automation_errors, :error_message, :string
  end
end
