class AddAdditionalFieldsToRepairPricer < ActiveRecord::Migration[5.0]
  def up
    # client first
    add_column :repair_pricer_reports, :client_first,               :string
    # client last
    add_column :repair_pricer_reports, :client_last,                :string
    # client email
    add_column :repair_pricer_reports, :client_email,               :string
    # client phone
    add_column :repair_pricer_reports, :client_phone,               :string
    # agent first
    add_column :repair_pricer_reports, :buyer_agent_first,          :string
    # agent last
    add_column :repair_pricer_reports, :buyer_agent_last,           :string
    # agent email
    add_column :repair_pricer_reports, :buyer_agent_email,          :string
    # amount_charged_cents
    add_column :repair_pricer_reports, :amount_charged_cents,       :integer
    # amount_charged_currency
    add_column :repair_pricer_reports, :amount_charged_currency,    :string
    # payment_status
    add_column :repair_pricer_reports, :payment_status,             :integer
    # stripe_charge_id
    add_column :repair_pricer_reports, :stripe_charge_id,           :string
    # stripe_refund_id
    add_column :repair_pricer_reports, :stripe_refund_id,           :string
    # closing date
    add_column :repair_pricer_reports, :closing_date,               :date
    # creator
    add_column :repair_pricer_reports, :creator_id,                 :integer
    # partner_id
    add_column :repair_pricer_reports, :partner_id,                 :integer
    # delivery_date
    add_column :repair_pricer_reports, :delivery_date,              :date
    # pool_report
    add_column :repair_pricer_reports, :order_pool_report,          :boolean
    # pool_report
    add_column :repair_pricer_reports, :order_home_history_report,  :boolean
    # rush_report
    add_column :repair_pricer_reports, :rush_report,                :boolean
    # change null
    change_column :repair_pricer_reports, :report_id,               :string, :null => true
    # inspection report
    add_attachment  :repair_pricer_reports, :inspection_report
    # pool report
    add_attachment  :repair_pricer_reports, :pool_report
    # home history report
    add_attachment  :repair_pricer_reports, :home_history_report
    # change default
    change_column_default :repair_pricer_reports,                   :status, 0
    # rename table
    add_attachment :repair_pricer_reports, :repair_report
    remove_attachment :repair_pricer_reports, :report
  end
  
  def down
    remove_column   :repair_pricer_reports, :client_first
    remove_column   :repair_pricer_reports, :client_last
    remove_column   :repair_pricer_reports, :client_email
    remove_column   :repair_pricer_reports, :client_phone
    remove_column   :repair_pricer_reports, :buyer_agent_first
    remove_column   :repair_pricer_reports, :buyer_agent_last
    remove_column   :repair_pricer_reports, :buyer_agent_email
    remove_column   :repair_pricer_reports, :amount_charged_cents
    remove_column   :repair_pricer_reports, :amount_charged_currency
    remove_column   :repair_pricer_reports, :payment_status
    
    remove_column   :repair_pricer_reports, :stripe_charge_id
    remove_column   :repair_pricer_reports, :stripe_refund_id
    remove_column   :repair_pricer_reports, :closing_date
    remove_column   :repair_pricer_reports, :creator_id
    remove_column   :repair_pricer_reports, :partner_id
    
    remove_column   :repair_pricer_reports, :delivery_date
    remove_column   :repair_pricer_reports, :order_pool_report
    remove_column   :repair_pricer_reports, :order_home_history_report
    remove_column   :repair_pricer_reports, :rush_report
    
    # change null
    RepairPricerReport.where(:report_id => nil).update_all(:report_id => "TEMPORARY")
    change_column :repair_pricer_reports, :report_id, :string, :null => false
    
    remove_attachment :repair_pricer_reports, :inspection_report
    remove_attachment :repair_pricer_reports, :pool_report
    remove_attachment :repair_pricer_reports, :home_history_report
    remove_attachment :repair_pricer_reports, :repair_report
    add_attachment :repair_pricer_reports, :report
    
    # change default
    change_column_default :repair_pricer_reports, :status, 10
  end
end
