class AddRepairPricerReportTypeToPartnerConfiguration < ActiveRecord::Migration[5.0]
  def change
    add_column :partner_configurations, :default_repair_pricer_report_type,  :string, :default => "F"
    add_column :repair_pricer_reports, :report_type,  :string, :default => "F"
  end
end
