class AddCompletedOnboardingToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :completed_onboarding, :boolean, :default => false
  end
end
