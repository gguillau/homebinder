class AddBillingFrequencyToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :billing_frequency, :string, :limit => 50, :nil => true
  end
end
