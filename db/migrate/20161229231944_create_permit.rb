class CreatePermit < ActiveRecord::Migration[5.0]
  def change
    create_table :permits do |t|
      t.string      :status, :null => true, :limit => 255
      t.string      :proposed_use, :null => true, :limit => 255
      t.string      :work_class, :null => true, :limit => 255
      t.string      :permit_type, :null => true, :limit => 255
      t.string      :permit_number, :null => false, :limit => 255
      t.money       :valuation_amount, :null => true
      t.string      :details, :null => true, :limit => 1000
      t.datetime    :permit_date, :null => true
      t.integer     :created_by, :null => true
      t.references  :binder_contractor, :null => true
      t.references  :binder, :null => false
      t.timestamps
    end
  end
end