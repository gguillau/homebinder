class CreateApplianceRecalls < ActiveRecord::Migration
  def change
    create_table :appliance_recalls do |t|
      t.references  :appliance
      t.references  :recall
      t.boolean     :false_positive
      t.integer     :strength
    end
  end
end
