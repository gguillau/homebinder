class AddTimestampsToDashboards < ActiveRecord::Migration[5.0]
  
  def up
    add_column :dashboards, :created_at, :datetime
    add_column :dashboards, :updated_at, :datetime
  end
  
  def down
    remove_column :dashboards, :created_at
    remove_column :dashboards, :updated_at
  end
end
