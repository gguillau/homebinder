class AddCoupons < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
      t.references  :partner
      t.string      :code
      t.string      :amount_off
      t.string      :percent_off
      t.string      :duration
      t.string      :duration_months
      t.datetime    :expires_on
      t.integer     :max_redemptions
      t.boolean     :active
    end
  end
end
