class DropRolesTable < ActiveRecord::Migration[5.0]
    def up
      drop_table :roles
    end
    
    def down
      create_table :roles
    end
end
