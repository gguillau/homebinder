class AddHomeownerItemToAnnualPropertyReviewFindings < ActiveRecord::Migration[5.0]
  def change
    add_column :annual_property_review_findings, :homeowner_item, :boolean
  end
end
