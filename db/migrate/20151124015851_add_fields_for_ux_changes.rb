class AddFieldsForUxChanges < ActiveRecord::Migration
  def change
    add_column :partners, :website, :string, :limit => 250
    add_column :paints, :sheen, :string, :limit => 50
    add_column :finishes, :style, :string, :limit => 100
    add_column :finishes, :color, :string, :limit => 50
    
    create_table :paint_sheens do |t|
      t.string :name, :limit => 50
      t.integer :created_by
      t.boolean :verified
    end
  end
end
