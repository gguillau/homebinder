class AddAdditionalFieldsToSessions < ActiveRecord::Migration[5.0]
  def change
    add_column :sessions,   :device_type,       :string
    add_column :sessions,   :device_name,       :string
    add_column :sessions,   :device_version,    :string
    add_column :sessions,   :operating_system,  :string
    add_column :sessions,   :status,            :integer, :default => 0
    
    Session.update_all(:device_type => "web", :status => 0)
  end
end
