class AddTransferNoteToBinderTemplates < ActiveRecord::Migration
  def change
    add_column :binder_templates, :transfer_note, :string, :limit => 1000
  end
end
