class CreatePartnerWidgets < ActiveRecord::Migration[5.0]
  def change
    create_table :partner_widgets do |t|
      t.references :partner,  :null => false
      t.references :widget,   :null => false
      t.text :configuration   # json of data to display on the widget
    end
  end
end
