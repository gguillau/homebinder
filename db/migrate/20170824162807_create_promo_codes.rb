class CreatePromoCodes < ActiveRecord::Migration[5.0]
  def change
    create_table :promo_codes do |t|
      t.string        :name,                :limit => 100, null: false
      t.integer       :percent_off,         null: false,  default: 0
      t.integer       :amount_off,          null: false,  default: 0
      t.datetime      :expiration_date,     null: true
      t.timestamps
    end
    
    add_column :annual_property_reviews, :promo_code_name,        :string
    add_column :annual_property_reviews, :promo_code_amount_off,  :integer
    
  end
end
