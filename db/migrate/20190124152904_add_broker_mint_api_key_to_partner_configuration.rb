class AddBrokerMintApiKeyToPartnerConfiguration < ActiveRecord::Migration[5.0]
  def change
    add_column :partner_configurations, :broker_mint_api_key, :string
  end
end
