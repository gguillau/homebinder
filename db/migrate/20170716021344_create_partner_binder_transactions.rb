class CreatePartnerBinderTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :partner_binder_transactions do |t|
      t.references    :binder_transaction,     :null => false
      t.references    :partner,                :null => false
      t.timestamps
    end
  end
end
