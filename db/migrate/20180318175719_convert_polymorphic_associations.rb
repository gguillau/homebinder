class ConvertPolymorphicAssociations < ActiveRecord::Migration[5.0]
  def change
    # address
    add_reference   :addresses, :build_fax_report,  index: true
    add_reference   :addresses, :partner,           index: true
    add_reference   :addresses, :user_profile,      index: true
    add_reference   :addresses, :warranty,          index: true
    add_reference   :addresses, :contractor,        index: true
    
    Address.where(:addressable_type => "BuildFaxReport").update_all("build_fax_report_id = addressable_id")
    Address.where(:addressable_type => "Partner").update_all("partner_id = addressable_id")
    Address.where(:addressable_type => "UserProfile").update_all("user_profile_id = addressable_id")
    Address.where(:addressable_type => "Warranty").update_all("warranty_id = addressable_id")
    Address.where(:addressable_type => "Contractor").update_all("contractor_id = addressable_id")
    
    # note
    add_reference   :notes, :binder,            index: true
    add_reference   :notes, :appliance,         index: true
    add_reference   :notes, :area,              index: true
    add_reference   :notes, :binder_contractor, index: true
    add_reference   :notes, :finish,            index: true
    add_reference   :notes, :inventory_item,    index: true
    add_reference   :notes, :maintenance_item,  index: true
    add_reference   :notes, :paint,             index: true
    add_reference   :notes, :project,           index: true
    add_reference   :notes, :property,          index: true
    add_reference   :notes, :receipt,           index: true
    add_reference   :notes, :structure,         index: true
    add_reference   :notes, :maintenance_event, index: true
    
    Note.where(:notable_type => "Binder").update_all("binder_id = notable_id")
    Note.where(:notable_type => "Binder::Appliance").update_all("appliance_id = notable_id")
    Note.where(:notable_type => "Binder::Area").update_all("area_id = notable_id")
    Note.where(:notable_type => "Binder::BinderContractor").update_all("binder_contractor_id = notable_id")
    Note.where(:notable_type => "Binder::Finish").update_all("finish_id = notable_id")
    Note.where(:notable_type => "Binder::InventoryItem").update_all("inventory_item_id = notable_id")
    Note.where(:notable_type => "Binder::MaintenanceItem").update_all("maintenance_item_id = notable_id")
    Note.where(:notable_type => "Binder::Paint").update_all("paint_id = notable_id")
    Note.where(:notable_type => "Binder::Project").update_all("project_id = notable_id")
    Note.where(:notable_type => "Binder::Property").update_all("property_id = notable_id")
    Note.where(:notable_type => "Binder::Receipt").update_all("receipt_id = notable_id")
    
    # binder_items
    add_reference   :binder_items, :appliance,          index: true
    add_reference   :binder_items, :area,               index: true
    add_reference   :binder_items, :binder_contractor,  index: true
    add_reference   :binder_items, :finish,             index: true
    add_reference   :binder_items, :inventory_item,     index: true
    add_reference   :binder_items, :maintenance_item,   index: true
    add_reference   :binder_items, :paint,              index: true
    add_reference   :binder_items, :property,           index: true
    add_reference   :binder_items, :receipt,            index: true
    add_reference   :binder_items, :structure,          index: true
    add_reference   :binder_items, :image,              index: true
    add_reference   :binder_items, :document,           index: true
    add_reference   :binder_items, :permit,             index: true
    add_reference   :binder_items, :project,            index: true
    
    BinderItem.where(:item_type => "Binder::Appliance").update_all("appliance_id = item_id")
    BinderItem.where(:item_type => "Binder::BinderContractor").update_all("binder_contractor_id = item_id")
    BinderItem.where(:item_type => "Binder::Image").update_all("image_id = item_id")
    BinderItem.where(:item_type => "Binder::MaintenanceItem").update_all("maintenance_item_id = item_id")
    BinderItem.where(:item_type => "Binder::Document").update_all("document_id = item_id")
    
    # purchase
    add_reference   :purchases, :receipt,    index: true
    add_reference   :purchases, :paint,      index: true
    add_reference   :purchases, :finish,     index: true
    add_reference   :purchases, :appliance,  index: true
    
    Purchase.where(:purchaseable_type => "Binder::Appliance").update_all("appliance_id = purchaseable_id")
    Purchase.where(:purchaseable_type => "Binder::Receipt").update_all("receipt_id = purchaseable_id")
    Purchase.where(:purchaseable_type => "Binder::Finish").update_all("finish_id = purchaseable_id")
    Purchase.where(:purchaseable_type => "Binder::Paint").update_all("paint_id = purchaseable_id")
    
    # seller_report_item
    add_reference   :seller_report_items, :appliance,         index: true
    add_reference   :seller_report_items, :area,              index: true
    add_reference   :seller_report_items, :binder_contractor, index: true
    add_reference   :seller_report_items, :finish,            index: true
    add_reference   :seller_report_items, :inventory_item,    index: true
    add_reference   :seller_report_items, :maintenance_item,  index: true
    add_reference   :seller_report_items, :paint,             index: true
    add_reference   :seller_report_items, :receipt,           index: true
    add_reference   :seller_report_items, :structure,         index: true
    add_reference   :seller_report_items, :image,             index: true
    add_reference   :seller_report_items, :document,          index: true
    add_reference   :seller_report_items, :permit,            index: true
    add_reference   :seller_report_items, :project,           index: true
    
    SellerReportItem.where(:seller_reportable_type => "Binder::Appliance").update_all("appliance_id = seller_reportable_id")
    SellerReportItem.where(:seller_reportable_type => "Binder::Area").update_all("area_id = seller_reportable_id")
    SellerReportItem.where(:seller_reportable_type => "Binder::BinderContractor").update_all("binder_contractor_id = seller_reportable_id")
    SellerReportItem.where(:seller_reportable_type => "Binder::Finish").update_all("finish_id = seller_reportable_id")
    SellerReportItem.where(:seller_reportable_type => "Binder::InventoryItem").update_all("inventory_item_id = seller_reportable_id")
    SellerReportItem.where(:seller_reportable_type => "Binder::MaintenanceItem").update_all("maintenance_item_id = seller_reportable_id")
    SellerReportItem.where(:seller_reportable_type => "Binder::Paint").update_all("paint_id = seller_reportable_id")
    SellerReportItem.where(:seller_reportable_type => "Binder::Receipt").update_all("receipt_id = seller_reportable_id")
    SellerReportItem.where(:seller_reportable_type => "Binder::Structure").update_all("structure_id = seller_reportable_id")
    SellerReportItem.where(:seller_reportable_type => "Binder::Image").update_all("image_id = seller_reportable_id")
    SellerReportItem.where(:seller_reportable_type => "Binder::Document").update_all("document_id = seller_reportable_id")
    SellerReportItem.where(:seller_reportable_type => "Binder::Permit").update_all("permit_id = seller_reportable_id")
    SellerReportItem.where(:seller_reportable_type => "Binder::Project").update_all("project_id = seller_reportable_id")
  end
end
