class AddShowIsnTagToPartnerConfigurations < ActiveRecord::Migration[5.0]
  def change
    add_column :partner_configurations, :show_isn_tab, :boolean, :default => false
  end
end
