class CreateBuildFaxReports < ActiveRecord::Migration[5.0]
    def up
        create_table :build_fax_reports do |t|
            t.references    :partner, :null => true
            t.references    :binder,  :null => true
            t.string        :status,  :null => false, :default => "not_paid"
            t.attachment    :report
            t.timestamps
        end
    end
    
    def down
        drop_table :build_fax_reports
    end
end
