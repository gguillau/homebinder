class AddEndPointRefreshedAtToIsnConfigurations < ActiveRecord::Migration
  def change
    add_column :isn_configurations, :endpoint_refreshed_at, :datetime
  end
end
