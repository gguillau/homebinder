class ChangeDefaultValueForRepairPricer < ActiveRecord::Migration[5.0]
  def change
    change_column_default :partner_configurations, :repair_pricer_messaging_enabled, true
  end
end
