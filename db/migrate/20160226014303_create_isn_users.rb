class CreateIsnUsers < ActiveRecord::Migration
  def change
    create_table :isn_users do |t|
      t.references  :partner_configuration
      t.string      :company_key,  :limit => 50
      t.string      :api_endpoint, :limit => 500
      t.string      :username,     :limit => 50
      t.string      :encrypted_password, :limit => 200
      t.datetime    :endpoint_refreshed_at, :datetime
      t.timestamps  null: false
    end
    
    drop_table :isn_configurations
  end
end
