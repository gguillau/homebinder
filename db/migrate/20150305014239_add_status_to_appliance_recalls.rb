class AddStatusToApplianceRecalls < ActiveRecord::Migration
  def change
    add_column :appliance_recalls, :repaired, :boolean, :default => false
    add_column :appliance_recalls, :repair_date, :datetime
    add_column :appliance_recalls, :replaced, :boolean, :default => false
    add_column :appliance_recalls, :replace_date, :datetime
  end
end
