class RemoveBinderAndSellerLogoIdFromPartners < ActiveRecord::Migration[5.0]
    def up
        remove_column   :partners, :sellers_logo_id
        remove_column   :partners, :binder_logo_id
    end
    
    def down
        add_column      :partners, :sellers_logo_id
        add_column      :partners, :binder_logo_id
    end
end
