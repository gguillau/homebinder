class CreatePartnerWarrantyPlans < ActiveRecord::Migration[5.0]
  def change
    create_table :partner_warranty_plans do |t|
      t.money         :price,           :null => false
      t.references    :partner,         :null => false
      t.references    :warranty_plan,   :null => false
      t.timestamps
    end
  end
end
