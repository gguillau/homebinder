class AddCreateMethodToBinders < ActiveRecord::Migration
  def change
    add_column :binders, :create_method, :string
  end
end
