class AddUserBranding < ActiveRecord::Migration[5.0]
  def change
    add_column      :user_profiles, :company,  :string, :null => true, :limit => 255
    add_column      :user_profiles, :website,  :string, :null => true, :limit => 255
    add_column      :user_profiles, :bio,      :string, :null => true, :limit => 1000
    add_column      :user_profiles, :message,  :string, :null => true, :limit => 1000
    add_attachment  :user_profiles, :head_shot
    add_attachment  :user_profiles, :company_logo
  end
end