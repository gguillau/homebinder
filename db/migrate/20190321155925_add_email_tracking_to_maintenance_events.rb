class AddEmailTrackingToMaintenanceEvents < ActiveRecord::Migration[5.0]
  def change
    add_column :maintenance_events, :email_status,  :string
    add_column :maintenance_events, :clicks,        :integer#,   :default => 0
    add_column :maintenance_events, :opens,         :integer#,   :default => 0
  end
end
