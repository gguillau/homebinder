class AddNotifiedToApplianceRecalls < ActiveRecord::Migration
  def change
    add_column :appliance_recalls, :notified, :boolean, :default => false    
  end
end
