class CreateWidgetTables < ActiveRecord::Migration
  def change
    create_table :widgets do |t|
      t.string  :name,          :limit => 100,    :nil => false
      t.string  :key,           :limit => 100,    :nil => false
      t.string  :description,   :limit => 1000
      t.string  :category,      :limit => 100
      t.string  :size,          :limit => 2
      t.integer :created_by
      t.timestamps
    end
    
    create_table :dashboards do |t|
      t.references  :dashboardable,  :polymorphic => true
      t.string      :name,        :limit => 100,      :nil => false
      t.string      :description, :limit => 1000,     :nil => false
      t.boolean     :template,    :default => false,  :nil => false
    end
    
    create_table :dashboard_widgets do |t|
      t.references  :dashboard
      t.references  :widget
    end
  end
end
