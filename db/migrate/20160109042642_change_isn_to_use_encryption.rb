class ChangeIsnToUseEncryption < ActiveRecord::Migration
  def change
    rename_column :isn_configurations, :username, :encrypted_username
    rename_column :isn_configurations, :password, :encrypted_password
  end
end
