class ChangePartnerConfigurationsDefaultUserBrandingOption < ActiveRecord::Migration[5.0]
  def change
    change_column_default :partner_configurations, :default_user_branding_option, "co_brand"
  end
end
