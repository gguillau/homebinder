class ChangeMonthlyEmailDefault < ActiveRecord::Migration
  def change
    change_column :user_profiles, :monthly_email, :boolean, :default => FALSE  
  end
end
