class AddFieldToPartnerConfigurations < ActiveRecord::Migration[5.0]
  def change
    add_column :partner_configurations, :default_transaction_type, :string, :default => "buy_side"
  end
end
