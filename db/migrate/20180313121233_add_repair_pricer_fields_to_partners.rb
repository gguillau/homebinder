class AddRepairPricerFieldsToPartners < ActiveRecord::Migration[5.0]
  def change
    add_column :partner_configurations, :repair_pricer_enabled, :boolean, :default => false
    add_column :partner_configurations, :repair_pricer_id, :string
  end
end
