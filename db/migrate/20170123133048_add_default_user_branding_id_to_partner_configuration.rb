class AddDefaultUserBrandingIdToPartnerConfiguration < ActiveRecord::Migration[5.0]
  def change
    add_column :partner_configurations, :default_user_branding_id, :integer
  end
end
