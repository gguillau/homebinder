class CreateAccessTokens < ActiveRecord::Migration
  def change
    create_table :access_tokens do |t|
      t.references  :access_tokenable, :polymorphic => true
      t.string      :token
      t.datetime    :expiration_date
    end
  end
end
