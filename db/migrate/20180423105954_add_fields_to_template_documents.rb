class AddFieldsToTemplateDocuments < ActiveRecord::Migration[5.0]
  def change
    # add document_types
    create_table :document_types do |t|
      t.string      :name,  :null => false
      t.timestamps
    end
    
    document_types = DocumentType.create!(
      [
        {:name => "Home Inspection Report"},
        {:name => "Appraisal"},
        {:name => "Home Reference Manual"},
        {:name => "Radon Report"},
        {:name => "Well Test Report"},
        {:name => "Checklist"},
        {:name => "Plot Map"},
        {:name => "Invoice"},
        {:name => "Inspection Agreement"},
        {:name => "Pest/Insect Report"},
        {:name => "Other"}
      ]
    )
    
    # update document_templates templates
    add_column :document_templates, :library_template,    :boolean, :default => false
    add_column :document_templates, :description,         :text,    :null => false,   :default => ""
    add_column :document_templates, :document_type_id,    :integer, :null => false,   :default => document_types.last.id
    
    # update documents
    add_column :documents, :document_type_id, :integer
  end
end
