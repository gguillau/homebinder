class ChangePartnerConfigurationsDefaultTransferDelay < ActiveRecord::Migration[5.0]
  def change
    change_column_default :partner_configurations, :binder_action_delay_length, 1
  end
end
