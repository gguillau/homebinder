class AddFileForPaperclip < ActiveRecord::Migration
  def change
    add_attachment :documents, :file
    add_attachment :images, :file
    add_attachment :logos, :file
  end
end
