class CreatePartnerConfigurations < ActiveRecord::Migration
  def change
    create_table :partner_configurations do |t|
      t.references  :partner
      t.string      :maintenance_note, :limit => 1000
    end
  end
end
