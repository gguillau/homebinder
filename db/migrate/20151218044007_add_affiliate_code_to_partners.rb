class AddAffiliateCodeToPartners < ActiveRecord::Migration
  def change
    add_column :partners, :affiliate_code, :string, :limit => 50
  end
end
