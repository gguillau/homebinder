class MakeRecallModelsLengthUnlimited < ActiveRecord::Migration
  def change
    change_column :recalls, :models, :string, :limit => 2000
  end
end
