class MakeIsnUsesrNameNotRequired < ActiveRecord::Migration
  def change
    change_column :isn_users, :name, :string, :null => true
  end
end
