class AddTimestampsToDashboardWidgets < ActiveRecord::Migration[5.0]
  def change
    add_column :dashboard_widgets, :created_at, :datetime
    add_column :dashboard_widgets, :updated_at, :datetime
  end
end
