class AddColumnsToMaintenanceTemplates < ActiveRecord::Migration[5.0]
  def change
    add_column  :maintenance_templates, :estimated_cost,      :integer
    add_column  :maintenance_templates, :estimated_time,      :string
    add_column  :maintenance_templates, :contractor_type,     :string
    add_column  :maintenance_templates, :contractor_type_id,  :integer
    add_column  :maintenance_templates, :video_url,           :string

    create_table :maintenance_template_contractor_types do |t|
      t.references  :maintenance_template,  :null => false, :index => {:name => "mt_contractor_type_index"}
      t.references  :contractor_category,   :null => false, :references => :contractor_categories, :index => {:name => "contractor_type_mt_index"}
      t.timestamps
    end
  end
  
end
