class CreateConsumerRecallsTable < ActiveRecord::Migration
  def change
    create_table :consumer_recalls do |t|
      t.text      :list, :limit => nil
      t.datetime  :refreshed_at
    end
  end
end
