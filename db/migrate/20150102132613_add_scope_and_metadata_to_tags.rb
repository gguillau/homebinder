class AddScopeAndMetadataToTags < ActiveRecord::Migration
  def change
    add_column :tags, :scope, :string
    add_column :tags, :metadata, :string
  end
end
