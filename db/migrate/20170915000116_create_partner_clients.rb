class CreatePartnerClients < ActiveRecord::Migration[5.0]
    def change
        create_table :partner_clients do |t|
            t.references    :partner, :null => false
            t.references    :client,  :null => false
            t.timestamps
        end
    
        add_foreign_key :partner_clients, :users, column: :client_id
        add_index :partner_clients, [:partner_id, :client_id], unique: true
    end
end
