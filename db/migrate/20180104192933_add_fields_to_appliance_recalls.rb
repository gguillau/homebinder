class AddFieldsToApplianceRecalls < ActiveRecord::Migration[5.0]
  def up
    add_column :appliance_recalls, :category, :string
    add_column :appliance_recalls, :sub_category, :string
    
    Binder::Appliance::ApplianceRecall.where(:status => ["new", nil]).update_all(:status => "unverified")
    Binder::Appliance::ApplianceRecall.where.not(:status => "unverified").update_all("sub_category = status, status = 'verified'")
  end
  
  def down
    remove_column :appliance_recalls, :category, :string
    remove_column :appliance_recalls, :sub_category, :string
  end
end
