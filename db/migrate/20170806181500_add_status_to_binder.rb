class AddStatusToBinder < ActiveRecord::Migration[5.0]
  def change
    add_column :binders, :status, :string, :default => "active"
  end
end
