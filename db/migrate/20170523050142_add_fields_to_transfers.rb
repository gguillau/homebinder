class AddFieldsToTransfers < ActiveRecord::Migration[5.0]
  def change
    add_column :transfers, :sender_id, :integer
    add_column :transfers, :receiver_id, :integer
    add_column :transfers, :transferred_at, :date
    add_column :transfers, :delivery_failed_at, :date
    add_column :transfers, :completed_at, :date
    add_column :transfers, :access_token, :string
    add_index :transfers, :access_token, unique: true
  end
end
