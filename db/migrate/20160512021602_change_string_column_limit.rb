class ChangeStringColumnLimit < ActiveRecord::Migration
  def change
    change_column :tags, :metadata, :text, :limit => nil
  end
end
