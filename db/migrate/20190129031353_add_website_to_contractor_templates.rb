class AddWebsiteToContractorTemplates < ActiveRecord::Migration[5.0]
  def change
    add_column :contractor_templates, :website, :string, :limit => 250
  end
end
