class CreatePartnersUsers < ActiveRecord::Migration
  def change
    create_table :partners_users, :id => false do |t|
      t.references :partner
      t.references :user
    end
  end
end
