class AddEmailNotificationToMaintenanceItems < ActiveRecord::Migration
  def change
    add_column :maintenance_items, :email_notifications, :boolean, :default => TRUE
  end
end
