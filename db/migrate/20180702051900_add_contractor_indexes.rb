class AddContractorIndexes < ActiveRecord::Migration[5.0]
  def change
    add_index :contractors, :name
    add_index :contractors, :phone
    add_index :contractors, :created_at
  end
end
