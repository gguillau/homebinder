class CreateWarrantyCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :warranty_companies do |t|
      t.string        :name,          :limit => 100
      t.string        :contact,       :limit => 100
      t.string        :email,         :limit => 100
      t.timestamps
    end
  end
end
