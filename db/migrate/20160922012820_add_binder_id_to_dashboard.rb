class AddBinderIdToDashboard < ActiveRecord::Migration
  def change
    add_column :dashboards, :binder_id, :integer
  end
end
