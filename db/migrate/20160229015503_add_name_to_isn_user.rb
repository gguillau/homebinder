class AddNameToIsnUser < ActiveRecord::Migration
  def change
    add_column :isn_users, :name, :string, :null => false, :limit => 100
  end
end
