class AddBinderItemIdsToImagesAndDocs < ActiveRecord::Migration[5.0]
  def change
    add_column :images, :structure_id,          :integer
    add_column :images, :area_id,               :integer
    add_column :images, :maintenance_item_id,   :integer
    add_column :images, :project_id,            :integer
    add_column :images, :appliance_id,          :integer
    add_column :images, :finish_id,             :integer
    add_column :images, :paint_id,              :integer
    add_column :images, :inventory_item_id,     :integer
    add_column :images, :receipt_id,            :integer
    add_column :images, :permit_id,             :integer
    add_column :images, :binder_contractor_id, :integer

    add_column :documents, :structure_id,           :integer
    add_column :documents, :area_id,                :integer
    add_column :documents, :maintenance_item_id,    :integer
    add_column :documents, :project_id,             :integer
    add_column :documents, :appliance_id,           :integer
    add_column :documents, :finish_id,              :integer
    add_column :documents, :paint_id,               :integer
    add_column :documents, :inventory_item_id,      :integer
    add_column :documents, :receipt_id,             :integer
    add_column :documents, :permit_id,              :integer
    add_column :documents, :binder_contractor_id,  :integer
  end
end
