class AddHeroIdToBinders < ActiveRecord::Migration
  def change
    add_column :binders, :hero_id, :integer
  end
end
