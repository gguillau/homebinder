class AddEmailDisplayNameToPartners < ActiveRecord::Migration
  def change
    add_column :partners, :email_display_name, :string, :limit => 50
  end
end
