require 'rspec/core'

count = 100
while count > 0 do
    RSpec::Core::Runner.run(['spec/controllers/registrations_controller_spec.rb'])
    RSpec.clear_examples
    count -= 1
end
