require File.expand_path(File.dirname(__FILE__) + "/environment")

# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
# 1.minute 1.hour 1.day 1.week 1.month 1.year is supported
set :environment, ENV["RAILS_ENV"] || ENV["RACK_ENV"] 
set :output, "log/cron.log"

# 10 minute tasks

every 10.minutes do
    rake "homebinder_inbox_scanner"
end

every 10.minutes do
    rake "homebinder_process_inspection_reports"
end

# hourly tasks

every 1.hour do
  rake "homebinder_automatic_transfers"
end

every 1.hour do
  rake "homebinder_automatic_transfers"
end

every 1.hour do
    rake "homebinder_notify_unverified_items"
end

every 1.hour do
    rake "homebinder_reset_passwords_fields"
end

every 1.hour do
    rake "homebinder_check_repair_pricer_status"
end

every 1.hour do
    rake "homebinder_upgrade_binders"
end

every 1.hour do
    rake "homebinder_call_broker_mint"
end

every 1.hour do
    rake "homebinder_upgrade_binders"
end

every 1.hour do
    rake "homebinder_check_inspection_report_jobs"
end

# try to put tasks in chronological order

every 1.day, at: '12:00 am' do
  rake "homebinder_billing_account_check"
end

every 1.day, at: '2:00 am' do
  rake "homebinder_recall_check"
end

every 1.day, at: '3:00 am' do
  rake "homebinder_remove_expired_sessions"
end

every 1.day, at: '4:00 am' do
  rake "homebinder_update_partner_accounts"
end

every 1.day, at: '5:00 am' do
  rake "homebinder_update_partner_billing_accounts"
end

every 1.day, at: '6:00 am' do
  rake "homebinder_daily_maintenance_check"
end

every 1.day, at: '7:00 am' do
  rake "homebinder_metric_emails"
end

every 1.day, at: '8:00 am' do
  rake "homebinder_reset_maintenance_template_dates"
end

every 1.day, at: '10:00 am' do
    rake "homebinder_monthly_email"
end

every 1.day, at: '11:00 am' do
    rake "homebinder_monthly_agent_email"
end

every 1.day, at: '12:00 pm' do
    rake "homebinder_increase_billing"
end

every 1.day, at: '11:00 pm' do
    rake "homebinder_remove_repair_pricer_widgets"
end