# See https://devcenter.heroku.com/articles/deploying-rails-applications-with-the-puma-web-server

workers Integer(ENV['WEB_CONCURRENCY'] || 1)
threads_count = Integer(ENV['MAX_THREADS'] || 5)
threads threads_count, threads_count

worker_timeout 20
worker_shutdown_timeout 8

preload_app!

rackup      DefaultRackup
port        ENV['PORT']     || 3000
environment ENV['RACK_ENV'] || 'development'

if ENV.fetch("AWS_ENABLED", 'false') === 'true'
  bind 'unix:///var/run/puma/my_app.sock'
  pidfile '/var/run/puma/puma.pid'
  state_path '/var/log/puma/puma.state'
  stdout_redirect '/var/log/puma/error.log', '/var/log/puma/access.log', true
end

on_worker_boot do
  # Valid on Rails up to 4.1 the initializer method of setting `pool` size
  ActiveSupport.on_load(:active_record) do
    config = ActiveRecord::Base.configurations[Rails.env] ||
                Rails.application.config.database_configuration[Rails.env]
    config['pool'] = ENV['MAX_THREADS'] || 5
    ActiveRecord::Base.establish_connection(config)
  end
end
