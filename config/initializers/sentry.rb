require "raven/base"
require "raven/integrations/rails"
require "raven/integrations/sidekiq"

Raven.configure do |config|
    config.dsn = "https://46752fc3a1b142488083dfa2a16ce20a:d953e371879b474f9e22635046160eb9@sentry.io/1435376"
    config.sanitize_fields = Rails.application.config.filter_parameters.map(&:to_s)
    config.environments = %w[ production testing staging ]
end