require 'countries/global'

module CoreExtensions
    module CountryInstanceMethods
        
        # finds a state/subdivision by name
        # returns nil if not found
        
        def subdivision(name)
            return nil if name.nil?
            code = states.detect {|state| state.first.downcase === name.downcase }
            return code.first if code.present?
            code = states.detect {|state| state.second.name.downcase === name.downcase }
            return code if code.nil?
            return code.first
        end
    end
    
    module CountryClassMethods
        
        # finds a country by any value whether 'US', 'USA', 'United States'
        # the method does 4 searches and returns nil if the country is not found
        
        def find_by_value(value)
            c = Country[value]
            return c unless c.nil?
            c = find_country_by_name(value)
            return c unless c.nil?
            c = find_country_by_alpha2(value)
            return c unless c.nil?
            c = find_country_by_alpha3(value)
            return c unless c.nil?
            return nil
        end
    end
    
    module PaperclipInstanceMethods
        
        # can't seem to override the expiring_url method so creating a 
        # one specifically for cloudfront
        # https://stackoverflow.com/questions/23924894/how-to-s3-object-url-that-works-with-cloudfront
        def expiring_cloud_front_url(expires_in = 3600, style = :original)

            path = url(style, timestamp: false)
            
            # AWS works on UTC, so make sure you are not using local time
            expires = (Time.now.getutc + expires_in).to_i.to_s
            
            begin
                key = File.read(ENV["CLOUD_FRONT_PRIVATE_KEY_FILE_PATH"]).dup
            rescue
                key = ENV["CLOUD_FRONT_PRIVATE_KEY"].dup
            end
            
            key = key.gsub "\\n", "\n"
            private_key = OpenSSL::PKey::RSA.new(key)
            
            # path should be your S3 path without a leading slash and without a file extension.
            # e.g. files/private/52
            policy = %Q[{"Statement":[{"Resource":"#{path}","Condition":{"DateLessThan":{"AWS:EpochTime":#{expires}}}}]}]
            signature = Base64.strict_encode64(private_key.sign(OpenSSL::Digest::SHA1.new, policy))
            
            # I'm not sure exactly why this is required, but it's in Amazon's perl script and seems necessary
            # Different base64 implementations maybe? 
            signature.tr!("+=/", "-_~")
            
            "#{path}?Expires=#{expires}&Signature=#{signature}&Key-Pair-Id=#{ENV["CLOUD_FRONT_KEY_PAIR_ID"]}"
        end
    end
end

Country.include CoreExtensions::CountryInstanceMethods
Country.extend  CoreExtensions::CountryClassMethods
Paperclip::Attachment.include CoreExtensions::PaperclipInstanceMethods