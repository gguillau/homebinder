Paperclip::Attachment.default_options[:storage] = :s3
Paperclip::Attachment.default_options[:bucket] =  ENV[S3FileService::S3_BUCKET]
Paperclip::Attachment.default_options[:s3_region] = ENV[S3FileService::S3_REGION]
Paperclip::Attachment.default_options[:url] = ':s3_alias_url'
Paperclip::Attachment.default_options[:s3_host_alias] = ENV["CLOUD_FRONT_DOMAIN"]
Paperclip::Attachment.default_options[:use_timestamp] = false
Paperclip::Attachment.default_options[:s3_permissions]  = :private
Paperclip::Attachment.default_options[:s3_protocol] = :https
Paperclip::Attachment.default_options[:s3_url_options] =  {
    virtual_host: true
}
Paperclip::Attachment.default_options[:s3_credentials]  = {
    s3_host_name: ENV[S3FileService::S3_URL],
    access_key_id: ENV[S3FileService::S3_ACCESS_KEY],
    secret_access_key: ENV[S3FileService::S3_SECRET_KEY]
}
