Paperclip.interpolates :binder_id do |attachment, style|
  attachment.instance.binder_id
end

Paperclip.interpolates :partner_id do |attachment, style|
  attachment.instance.partner_id
end

Paperclip.interpolates :binder_template_id do |attachment, style|
  attachment.instance.binder_template_id
end

Paperclip.interpolates :style do |attachment, style|
  style
end