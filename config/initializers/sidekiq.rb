Sidekiq.configure_server do |config|

  config.redis = { url: ENV["REDIS_URL"] }
  
  config.error_handlers << Proc.new {|exception, context| ErrorService.perform_async(exception, context) }
end

Sidekiq.configure_client do |config|
  config.redis = { url: ENV["REDIS_URL"] }
end

Sidekiq.default_worker_options = { 'backtrace' => true }
Sidekiq::Extensions.enable_delay!