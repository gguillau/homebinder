require File.expand_path('../boot', __FILE__)

require 'active_record/railtie'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'active_model/railtie'
require 'sprockets/railtie'
require 'rack/cors'
require 'rack/rewrite'
require 'rack-timeout'
require 'paperclip'
require 'elasticsearch/model'
require 'elasticsearch/persistence'
require 'elasticsearch/persistence/model'
require 'geocoder'
require 'money-rails'

if defined?(Bundler)
 # If you precompile assets before deploying to production, use this line
  Bundler.require(*Rails.groups(:assets => %w(development test)))
  # If you want your assets lazily compiled in production, use this line
  # Bundler.require(:default, :assets, Rails.env)
end

module HomeBinder
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # enable garbage collection instrumentation for NewRelic
    GC::Profiler.enable

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    config.time_zone = 'Eastern Time (US & Canada)'
    config.active_record.default_timezone = :local

    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = "utf-8"

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:password]

    # Enable the asset pipeline
    config.assets.enabled = false

    # get rid of field_with_error
    config.action_view.field_error_proc = Proc.new { |html_tag, instance| html_tag.html_safe }

    config.middleware.use ActiveRecord::Migration::CheckPending
    config.middleware.use Rack::Deflater

    # Cross Domain Request
    config.middleware.insert_before ActionDispatch::Static, Rack::Cors do
      allow do
        origins '*'
        resource '*', :headers => :any, :methods => [:get, :post, :put, :delete, :options]
        resource '/assets/*', headers: :any, methods: [:get]
      end
    end

    # access token
    config.access_tokens = {
      # defines how long an aws url is good for
      :token_refresh_interval => 2.hours,
      # 7 day token
      :week_long_token_refresh_interval => 7.days,
      # gives longer access to an aws url for emails
      :long_token_refresh_interval => 90.days
    }

    config.transfers = {
       :schedule => [3,7,15,21,30, 15],
       :max_days => 120
    }

    # recall checks
    config.recall_check = {
      :minimum_strength => 4,
      :word_length => 3,
      :source => "homebinder"
    }

    # uploads
    config.uploads = {
      # 10 MB limit for homeowners who sign up off the street
      :max_size => 10000000
    }

    # apr
    config.apr = {
      # price of an apr in cents
      cost: 14900,
      max_cancel_hours: 48,
      max_hold_hours: 48
    }

    # password reset timeframe (in hours)
    config.password_reset_time_frame = ENV["PASSWORD_RESET_TIME_FRAME"] || 6

    config.generators do |g|
      g.test_framework :rspec,
        :fixtures => true,
        :view_specs => false,
        :helper_specs => false,
        :routing_specs => true,
        :controller_specs => true,
        :request_specs => true
      g.fixture_replacement :factory_girl, :dir => "spec/factories"
    end

    config.middleware.insert_before ActionDispatch::Static, Rack::Rewrite do
      rewrite %r{^(?!/sidekiq|\/api/).*}, '/', :not => %r{(.*\..*)}
    end

    # rails api
    config.api_only = true
    
    
    # config/application.rb
    config.exceptions_app = self.routes # a Rack Application
    
    config.active_job.queue_adapter = :sidekiq
  end
end
