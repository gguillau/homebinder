HomeBinder::Application.routes.draw do
  
  require 'sidekiq/web'
  
  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    username == ENV['SIDEKIQ_USERNAME'] && password == ENV['SIDEKIQ_PASSWORD']
  end
  
  mount Sidekiq::Web => '/sidekiq'
  
  scope :constraints => Constraint::HomeBinder do
    namespace :api, :defaults => {format: :json} do
      namespace :v1 do
        
        resources :accounts do
          member do
            post    "/partners",              to: "accounts#add_partners"
            delete  "/partners/:partner_id",  to: "accounts#remove_partners"
          end
        end
        resources :api_keys,                :only => [:create]
        resources :appliances do
          resources :notes
            member do
            # send test recall notification email
            post "/email",               :to => "appliances#send_test_email"
            post "/recalls",             :to => "appliances#check_for_recalls"
          end
        end
        resources :appliance_recalls do
          member do
            # send recall email
            post "/email",               :to => "appliance_recalls#send_email"
            post "/recalls",             :to => "appliance_recalls#check_for_recalls"
          end
        end
        resources :annual_property_review_findings
        resources :annual_property_review_capital_items
        resources :annual_property_review_photos
        resources :annual_property_reviews do
          member do
            post  "request_quote",                    :to => "annual_property_reviews#request_quote"
            get   "download",                         :to => "annual_property_reviews#download"
            post  "messages",                         :to => "annual_property_reviews#messages"
          end
        end
        resources :appliance_templates
        resources :areas do
          resources :notes
        end
        resources :automation_errors,       :only => [:index, :update, :destroy]
        
        resources :binder_brandings
        resources :binder_items
        resources :binder_transactions
        resources :binder_templates do
          resources :appliance_templates, :only => [:index, :create]
          resources :contractor_templates, :only => [:index, :create]
          resources :maintenance_templates, :only => [:index, :create]
          resources :document_templates, :only => [:index, :create]
          member do
            # copy template
            get "/copy", to: "binder_templates#copy"
          end
        end
        resources :binders do
          member do
            post "/acl",                            :to => "binders#add_user"
            get  "/acl",                            :to => "binders#get_acl"
            delete "/acl/:user_id",                 :to => "binders#delete_acl"
            # notifications
            get "/notifications",                   :to => "binders#get_notifications"
            # dashboard
            get "/dashboard",                       :to => "dashboards#for_binder"
            # partner binders
            post "/partner",                        :to => "binders#add_partner"
            get "/partner",                         :to => "binders#get_partner"
            delete "/partner/:partner_binder_id",   :to => "binders#delete_partner"
            put "/orphan",                          :to =>  "binders#orphan"
            put "/feedback",                        :to =>  "binders#sendFeedback"
            put "/services",                        :to =>  "binders#requestServices"
            put "/solar_services",                  :to =>  "binders#requestSolarServices"
            put "/secure24",                        :to =>  "binders#secure24"
            delete "/services",                     :to =>  "binders#declineServices"
            put "/aprWidget",                       :to =>  "binders#learn_more_apr"
            get "inventory_report",                 :to =>  "binders#inventory_report"
            get "capital_expense_report",           :to =>  "binders#capital_expense_report"
          end
          collection do
            post   "/batch",          :to => "binders#batch"
          end
          resources :annual_property_reviews
          resources :shares,                :only => [:index, :create]

        end
        post  "/binders/property",                :to => "binders#find_binder_by_property"
        resources :binder_contractors do
          resources :notes
          member do
            # send test maintenance reminder
            post "/contact",               :to => "binder_contractors#contact"
          end
        end
        get "buildfax/:binderId",           to: "buildfax#get_permits"
        post "buildfax/:binderId",          to: "buildfax#create_permits"
        resources :build_fax_reports
        post "build_fax_reports/search",    to: "build_fax_reports#search"
        
        resources :contractors
        resources :contractor_sub_types
        resources :contractor_templates
        resources :contractor_types
        resources :coupons,                       :only => [:show]
        resources :customers
        
        resources :dashboards
        resources :dashboard_widgets
        resources :documents
        resources :document_templates
        resources :document_types
        
        resources :events,                        :only => [:index, :create]
        
        resources :free_trials,             :only => [:create]
        resources :finishes do
          resources :notes
        end
        
        resources :images
        resources :inspection_report_jobs
        resources :inventory_items do
          resources :notes
        end
        resources :inventory_report_pdfs,   :only => [:show]
        resources :isn_users
        
        match 'kpi/binders'                     => 'kpi#get_binders',             :via => :get
        match 'kpi/users'                       => 'kpi#get_users',               :via => :get
        match 'kpi/homeowners'                  => 'kpi#get_homeowners',          :via => :get
        match 'kpi/engagement/:partner_type'    => 'kpi#get_engagement',          :via => :get
        match 'kpi/download'                    => 'kpi#download',                :via => :get
        match 'kpi/account_summaries'           => 'kpi#account_summaries',       :via => :get
        match 'kpi/summary'                     => 'kpi#get_summary',             :via => :get
        match 'kpi/hon'                         => 'kpi#homeowner_report',             :via => :post
        
        
        resources :maintenance_events,      :only => [:index, :show, :create, :update, :destroy]
        resources :maintenance_items do
          resources :notes
          member do
            # send test maintenance reminder
            post "/email",               :to => "maintenance_items#send_test_email"
          end
        end
        resources :marketing_resources
        resources :maintenance_templates do
          member do
            # send recall email
            post "/email", :to => "maintenance_templates#send_test_email"
          end
        end
        
        resources :notes
        
        resources :organizations do
          member do
            get    '/partners',               to: "organizations#get_partners"
            post   '/partners',               to: "organizations#add_partners"
            delete '/partners/:partner_id',   to: "organizations#remove_partner"
            get    '/users',                  to: "organizations#get_users"
            post   '/users',                  to: "organizations#add_users"
            delete '/users/:user_id',         to: "organizations#remove_user"
            get '/dashboards',                to: "dashboards#owned_by_organization"
            get '/binders',                   to: "organizations#get_binders"
            get  "resources",                 to: "organizations#resources"
          end
        end
        
        resources :paints do
          resources :notes
        end
        resources :partner_binders
        resources :partner_users
        resources :partner_widgets
        resources :partner_clients
        resources :partner_contractors
        resources :partner_configurations,  :only => [:index, :show, :update]
        resources :partners do
          member do
            # binders
            get "/binders", to: "partners#get_binders"
            # user routes
            get "/users", to: "partners#get_users"
            post "/users", to: "partners#add_user"
            delete "/users/:user_id", to: "partners#remove_user"
            put "/users/:user_id/role", to: "partners#update_role"
            # dashboards
            get '/dashboards',  to:   "dashboards#owned_by_partner"
            put '/onboarding', to:   "partners#complete_onboarding"
            # account --> future
            #get "/account", to: "accounts#show"
            #put "/account", to: "accounts#update"
            # import agents
            post "/import", to: "partners#import_users"
            get "/automation",  to:   "partners#automation"
            # apr config route
            get "/apr_config", to: "partners#show_apr_config"
            # isn
            get "/footprints",  to: "partners#footprints"
            get "/warranty_plans",  to:   "partners#warranty_plans"
            get "/widget_exclusions",  to:   "partners#widget_exclusions"
            post "/warranties/enable", to: "partners#enable_warranty_account"
            put "/users/:user_id/role", to: "partners#update_role"
          end
          resource :analytics do
            member do
              get "/overview",                          to: "partners#analytics_overview"
              get "/logins",                            to: "partners#analytics_logins"
              get "/maintenance_reminders",             to: "partners#analytics_reminders"
              get "/agents",                            to: "partners#analytics_agents"
              get "/maintenance_reminders/download",    to: "partners#download_reminders"
              get "/logins/download",                   to: "partners#download_logins"
            end
          end
          resources :binder_templates, :only => [:index, :create]
        end
        resources :permits do
          resources :notes
        end
        resources :plans,                   :only => [:show]
        # transfer/share welcome has a user id to check for waiting binders
        get    "pending_users/:id/binders", :to => "pending_users#get_waiting_binders_by_id"
        # user onboading has to post an email to check for waiting binders
        post   "pending_users/binders",     :to => "pending_users#get_waiting_binders_by_email"
        delete "pending_users/:id",         :to => "pending_users#optout"
        resources :projects do
          resources :notes
        end
        resources :project_statuses,        :only => [:index]
        resources :promo_codes
        post "/promo_codes/verify",                to: "promo_codes#verify"
        resources :property_types,          :only => [:index]
        
        resources :recalls
        post "/recalls/download",           to: "recalls#download"
        post "/recalls/run_service",        to: "recalls#run_service"
        resources :recall_report_pdf,       :only => [:index]
        resources :recall_keywords,         :only => [:index, :create, :destroy]
        resources :receipts do
          resources :notes
        end
        resources :registrations, only: [:create]
        resources :repair_pricer_reports
        
        get '/service_keys',  :to => "service_keys#index"
        resources :sessions
        resources :seller_reports,          :only => [:index, :create, :show, :update, :destroy]
        resources :seller_report_pdfs,      :only => [:show]
        resources :shares do
          member do
            put "/resend",                   :to => "shares#resend"
          end
        end
        resources :stores,                  :only => [:index]
        resources :structures do
          resources :notes
        end
        resources :subscriptions
        match 'system/dashboards'           => 'dashboards#owned_by_system',    :via => :get
        
        resources :transfers do
          member do
            put "/execute",                  :to => "transfers#execute"
            put "/resend",                   :to => "transfers#resend"
            put "/cancel",                   :to => "transfers#cancel"
          end
        end
        
        resources :user_binders do
          member do
            get  "/repair_pricer_token",     :to => "user_binders#find_by_access_token"
          end
          collection do
            get  "/property",                :to => "user_binders#find_by_property_address"
          end
        end
        resources :user_contractors
        resources :user_merges,             :only => [:index, :show, :update] do
          member do
            put "/revert",                  :to => "user_merges#revert"
          end
        end
        resources :user_profiles,           :only => [:index, :update]
        resources :users,                         :only => [:create, :index, :show, :update, :destroy] do
          member do
            put "/password",                    :to => "users#change_password"
            put "/email",                       :to => "users#change_email"
            get "/dashboards",                  :to => "dashboards#owned_by_user"
            get "/binders",                    :to => "users#binders"
            get "/welcome",                   :to => "users#welcome"
            put "/invite",                  :to => "users#invite"
            put "/confirm_password",        :to => "users#confirm_password"
          end
        end
        post  "/users/passwords",                 :to => "users#reset_password"
        put   "/users/passwords/update",          :to => "users#update_password"
        post  "/users/email",                     :to => "users#find_user_by_email"
        post "/users/merge_agents",               :to => "users#merge_agents"
        resources :user_tokens,                   :only => [:create, :destroy]

        post "/warranties/download",          to: "warranties#download"
        resources :warranty_companies
        resources :warranty_configurations do
          member do
            post "/orders", to: "warranty_configurations#orders"
          end
        end
        resources :warranty_plans
        resources :warranties
        resources :widgets
        resources :widget_exclusions,       :only => [:create, :destroy]


        # map all typeahead requests to the typeahead controller

        get "construction_styles",      to: "typeaheads#get_typeaheads", defaults: { klass: "Binder::Structure::ConstructionStyle" }
        get "construction_types",       to: "typeaheads#get_typeaheads", defaults: { klass: "Binder::Structure::ConstructionType" }
        get "heat_types",               to: "typeaheads#get_typeaheads", defaults: { klass: "Binder::Structure::HeatType" }
        get "heat_sources",             to: "typeaheads#get_typeaheads", defaults: { klass: "Binder::Structure::HeatSource" }
        get "area_types",               to: "typeaheads#get_typeaheads", defaults: { klass: "Binder::Area::Category" }
        get "paint_manufacturers",      to: "typeaheads#get_typeaheads", defaults: { klass: "Binder::Paint::Manufacturer" }
        get "paint_types",              to: "typeaheads#get_typeaheads", defaults: { klass: "Binder::Paint::Category" }
        get "paint_sheens",             to: "typeaheads#get_typeaheads", defaults: { klass: "Binder::Paint::Sheen" }
        get "project_types",            to: "typeaheads#get_typeaheads", defaults: { klass: "Binder::Project::Category" }
        get "appliance_manufacturers",  to: "typeaheads#get_typeaheads", defaults: { klass: "Binder::Appliance::Manufacturer" }
        get "appliance_models",         to: "typeaheads#get_typeaheads", defaults: { klass: "Binder::Appliance::Model" }
        get "inventory_item_types",     to: "typeaheads#get_typeaheads", defaults: { klass: "Binder::InventoryItem::Category" }
        get "finish_makes",             to: "typeaheads#get_typeaheads", defaults: { klass: "Binder::Finish::Make" }
        get "finish_models",            to: "typeaheads#get_typeaheads", defaults: { klass: "Binder::Finish::Model" }

        # retrieve unverified typeahead for admins
        get "appliance_manufacturers/unverified",  to: "typeaheads#get_unverified_typeaheads", defaults: { klass: "Binder::Appliance::Manufacturer" }

        # save unverified typeahead for admins
        put "appliance_manufacturers/:id",  to: "typeaheads#save_unverified_typeahead", defaults: { klass: "Binder::Appliance::Manufacturer" }

        # remove unverified typeahead for admins
        delete "appliance_manufacturers/:id",  to: "typeaheads#remove_unverified_typeahead", defaults: { klass: "Binder::Appliance::Manufacturer" }
      end
    end
  end

  scope :constraints => Constraint::Partner do
    namespace :api, :defaults => {format: :json}do
      namespace :v1 do
        namespace :partners do
          match ":key/binders"      => "partners#create",     :via => :post
          match ":key"              => "partners#verify",     :via => :get
        end
      end
    end
  end

  scope :constraints => Constraint::Mandrill do
    namespace :api, :defaults => {format: :json} do
      namespace :v1 do
        namespace :mandrill do
          match "webhooks"     => "webhooks#update", :via => :post
        end
      end
    end
  end

  namespace :api, :defaults => {format: :json} do
    namespace :v1 do
      namespace :stripe do
        match "webhooks"     => "webhooks#update", :via => :post
      end
    end
  end

  namespace :api, :defaults => {format: :json} do
    namespace :v1 do
      namespace :twilio do
        match "webhooks"     => "webhooks#create", :via => :post
      end
    end
  end

  match "*a", :to => "homebinder#routing_error", :via => [:get, :post]
end
